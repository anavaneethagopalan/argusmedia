import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { select } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthGuard implements CanActivate {
  @select(['webapi_login_session', 'session', 'IsAuthenticated']) authenticated$: Observable<boolean>;

  constructor(private router: Router) { }

  canActivate(): Observable<boolean> {
    return this.authenticated$.take(1)
      .do(_ => { if (!_) { this.router.navigate(['/login']); } });
  }
}
