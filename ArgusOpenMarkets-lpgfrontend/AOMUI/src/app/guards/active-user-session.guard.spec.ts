import { TestBed, async, inject } from '@angular/core/testing';

import { ActiveUserSessionGuard } from './active-user-session.guard';

describe('ActiveUserSessionGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActiveUserSessionGuard]
    });
  });

  it('should create ActiveUserSessionGuard', inject([ActiveUserSessionGuard], (guard: ActiveUserSessionGuard) => {
    expect(guard).toBeTruthy();
  }));
});
