import { TestBed, async, inject } from '@angular/core/testing';

import { PendingChangesGuard, CanComponentDeactivate  } from './pending-changes.guard';
import { Router } from '@angular/router';
import { DashboardModule } from '../dashboard/dashboard.module';

describe('PendingChangesGuard', () => {
  const component = {
    canDeactivate: jasmine.createSpy('canDeactivate')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PendingChangesGuard],
      imports: [DashboardModule]
    });
  });

  it('should ...', inject([PendingChangesGuard], (guard: PendingChangesGuard, router: Router) => {
    expect(guard).toBeTruthy();
  }), 10000);

  it('checks if a user is valid', async(inject([PendingChangesGuard], (guard: PendingChangesGuard) => {
      expect(guard.canDeactivate(component)).toBeFalsy();
      expect(component.canDeactivate).toHaveBeenCalled();
    })
  ), 10000);
});
