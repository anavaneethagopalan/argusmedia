import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { select } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ActiveUserSessionGuard implements CanActivate {
  @select(['webapi_login_session', 'session', 'IsAuthenticated']) authenticated$: Observable<boolean>;

  // This guard is for preventing navigation to pages that shouldn't be accessible when the user
  // has an active session, e.g. login pages

  canActivate(): Observable<boolean> {
    return this.authenticated$.map((auth) => !auth);
  }
}
