import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/lib/testing';
import { NgRedux } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';

import { AuthGuard } from './auth.guard';
import { RootEpics } from '../store/epics';

describe('AuthGuard', () => {
  let mockEpics: RootEpics;
  let mockNgRedux: NgRedux<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AuthGuard
      ]
    }).compileComponents().then(() => {
      mockEpics = {
        createEpics() { return []; }
      } as RootEpics;

      mockNgRedux = MockNgRedux.getInstance();
      MockNgRedux.reset();
    });
  });

  it('should create auth guard', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should not activate if if store webapi_login_session->session->IsAuthenticated is null', inject([AuthGuard], (guard: AuthGuard) => {
    const stub = MockNgRedux.getSelectorStub(['webapi_login_session', 'session', 'IsAuthenticated']);
    stub.next(null);
    stub.complete();

    const navigateSpy = spyOn((<any>guard).router, 'navigate');
    expect(navigateSpy).not.toHaveBeenCalled();
  }));

  it('should activate if store webapi_login_session->session->IsAuthenticated emits true', inject([AuthGuard], (guard: AuthGuard) => {
    const stub = MockNgRedux.getSelectorStub(['webapi_login_session', 'session', 'IsAuthenticated']);
    stub.next(false);
    stub.next(true);
    stub.complete();

    guard.authenticated$
      .toArray()
      .subscribe(
      actualSequence => expect(actualSequence).toEqual([false, true]),
      null);

    expect(guard.canActivate()).toBeTruthy();
  }));
});
