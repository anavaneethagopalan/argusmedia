import { Injectable, HostListener } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

/*
   if there are no pending changes, just allow deactivation; else confirm first
   Note: this warning message will only be shown when navigating elsewhere within your angular app;
   when navigating away from your angular app, the browser will show a generic warning message
*/
@Injectable()
export class PendingChangesGuard implements CanDeactivate<CanComponentDeactivate> {
  canDeactivate(component: CanComponentDeactivate) {
    return component.canDeactivate ? component.canDeactivate() : true;
  }

}
