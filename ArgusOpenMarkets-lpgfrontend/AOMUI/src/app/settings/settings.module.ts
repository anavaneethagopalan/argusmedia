import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { Ng2SelectizeModule } from 'ng2-selectize';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

import { SettingsComponent } from './settings.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { MySettingsComponent } from './my-settings/my-settings.component';
import { CompanySettingsComponent } from './company-settings/company-settings.component';
import { MultiSelectComponent } from '../shared/multiselect.component';
import { SelectizeComponent } from '../shared/selectize.component';
import { AddNotificationComponent } from './company-settings/add-notification/add-notification.component';
import { CounterpartyPermissionsComponent } from './counterparty-permissions/counterparty-permissions.component';
import { BrokerPermissionsComponent } from './broker-permissions/broker-permissions.component';
import { SystemInformationComponent } from './system-information/system-information.component';
import { ProductSettingsComponent } from './product-settings/product-settings.component';
import { TenorInfoComponent } from './product-settings/tenor-info/tenor-info.component';

import { AccordionModule, TabsModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    AccordionModule.forRoot(),
    TabsModule.forRoot(),
    FormsModule,
    ModalModule.forRoot(), 
    Ng2SelectizeModule,
    MultiselectDropdownModule
  ],
  declarations: [
    SettingsComponent,
    MySettingsComponent,
    CompanySettingsComponent,
    AddNotificationComponent,
    CounterpartyPermissionsComponent,
    BrokerPermissionsComponent,
    SystemInformationComponent,
    ProductSettingsComponent,
    TenorInfoComponent,
    MultiSelectComponent,
    SelectizeComponent
  ],
  entryComponents: [
    AddNotificationComponent //required for bootstrap modal to work
  ]
})
export class SettingsModule { }
