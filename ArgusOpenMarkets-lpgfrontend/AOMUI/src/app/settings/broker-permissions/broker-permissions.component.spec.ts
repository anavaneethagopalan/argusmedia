import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokerPermissionsComponent } from './broker-permissions.component';

describe('BrokerPermissionsComponent', () => {
  let component: BrokerPermissionsComponent;
  let fixture: ComponentFixture<BrokerPermissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrokerPermissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
