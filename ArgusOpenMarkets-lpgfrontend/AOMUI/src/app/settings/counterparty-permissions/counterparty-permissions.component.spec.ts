import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterpartyPermissionsComponent } from './counterparty-permissions.component';

describe('CounterpartyPermissionsComponent', () => {
  let component: CounterpartyPermissionsComponent;
  let fixture: ComponentFixture<CounterpartyPermissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterpartyPermissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterpartyPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
