import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { AccordionModule } from 'ngx-bootstrap';

import { ConnectionStatus } from '../../models/connection-status';
import { SystemInformationComponent } from './system-information.component';

describe('SystemInformationComponent', () => {
  let component: SystemInformationComponent;
  let fixture: ComponentFixture<SystemInformationComponent>;

  const socketProviderStub = {
    get connectionStatus$() { return Observable.of(ConnectionStatus.Reconnected); },
    close: () => undefined
  };

  const userServiceStub = {
    get Username() { return "TestUSer" },
    get UserId() { return 123; },
    get Company() {
        return {
            id: 321,
            name: "TestCompany"
        };
    }
  };

  const configServiceStub = {
      get version() { return "1.0.0" }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AccordionModule.forRoot() ],
        declarations: [ SystemInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
