import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';

import { Observable } from "rxjs/Observable";
import * as Platform from 'platform';
import * as diffusion from 'diffusion';

import { selectConnectionStatus } from '../../store/model';
import { IUser } from '../../api/models/user/user';
import { Config } from '../../models/config';

@Component({
  selector: 'aom-system-information',
  templateUrl: './system-information.component.html',
  styleUrls: ['./system-information.component.scss'],
})
export class SystemInformationComponent implements OnInit {
  
  // for the accordion loading with panels open or closed
  public status: any = {
    isFirstOpen: true,
    isOpen: true
  };

  @select(selectConnectionStatus)
  public connectionStatus$: Observable<string>;

  @select(['webapi_login_session', 'session', 'User'])
  public user$ : Observable<IUser>;

  @select()
  public config$ : Observable<Config>;

  @select()
  public platform$ : Observable<Platform>;

  public get diffusionVersion(): string {
    return diffusion.version;
  }

  constructor() { }

  ngOnInit() {
  }

}
