import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux, NgReduxTestingModule } from '@angular-redux/store/testing';

import { BsModalService } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap';

import { UserService } from '../../shared/user.service';
import { MessageDistributionService } from '../../shared/message-distribution.service';
import { Message } from '../../models/message';
import { IOrganisationSystemNotificationMapping } from '../../models/organisation-system-notification-mapping';
import { CompanySettingsComponent } from './company-settings.component';

describe('CompanySettingsComponent', () => {
  let component: CompanySettingsComponent;
  let fixture: ComponentFixture<CompanySettingsComponent>;
  const mapping: IOrganisationSystemNotificationMapping = {
    id: 1,
    eventType: 1,
    emailAddress: '',
    organisationId: 1,
    productId: 1,
    userId: 1
  };
  const modalServiceStub = {}
  const messageDistributionServiceStub = {
    sendMessage(topic: string, message: Message) { }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AccordionModule.forRoot()],
      providers: [
        UserService,
        { provide: BsModalService, useValue: modalServiceStub },
        { provide: MessageDistributionService, useValue: messageDistributionServiceStub },
        { provide: NgRedux, useValue: MockNgRedux },
      ],
      declarations: [CompanySettingsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('toggle checkbox works for empty selection property', () => {
    component.selection = []
    component.toggleSelection(mapping);
    expect(component.selection[0]).toEqual(mapping);
  });

  it('toggle checkbox adds new mapping to selection', () => {
    component.toggleSelection(mapping);
    expect(component.selection[0]).toEqual(mapping);
  });

  it('toggle checkbox removes mapping from selection', () => {
    component.selection.push(mapping);
    component.toggleSelection(mapping);
    expect(component.selection).toEqual([]);
  });
});
