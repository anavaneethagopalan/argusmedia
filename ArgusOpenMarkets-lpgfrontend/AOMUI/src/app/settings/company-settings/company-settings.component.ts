import { Component } from '@angular/core';
import { select } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';
import { BsModalService } from 'ngx-bootstrap/modal';

import { UserOrganisation } from '../../api/models/user/user-organisation';
import { OrganisationSystemNotification } from '../../models/organisation-system-notification';
import { IOrganisationSystemNotificationMapping } from '../../models/organisation-system-notification-mapping';
import { selectOrganisationNotifications } from '../../store/model';
import { UserService } from '../../shared/user.service';
import { AddNotificationComponent } from './add-notification/add-notification.component';

@Component({
  selector: 'aom-company-settings',
  templateUrl: './company-settings.component.html',
  styleUrls: ['./company-settings.component.scss']
})
// @WithSubStore({
//   basePathMethodName: 'getBasePath',
//   localReducer: organisationDetails
// })
export class CompanySettingsComponent {
  // for the accordion loading with panels open or closed
  public status: any = {
    isFirstOpen: true,
    isOpen: true
  };

  @select(['webapi_login_session', 'session', 'User', 'userOrganisation'])
  public userOrganisation$: Observable<UserOrganisation>;

  @select(['webapi_login_session', 'session', 'User', 'systemPrivileges', 'privileges'])
  public currentSystemPrivileges$: Observable<string[]>;

  @select(selectOrganisationNotifications)
  public organisationSystemNotifications$: Observable<OrganisationSystemNotification>;

  public get editEnabled$(): Observable<boolean> {
    return this.currentSystemPrivileges$.map((priv) => priv.indexOf("CompanySettings_Amend") > -1);
  }

  public get deleteDisabled(): boolean {
    return this.selection.length == 0;
  }

  public selection : IOrganisationSystemNotificationMapping[] = [];

  constructor(private modalService: BsModalService, private userService: UserService) { }

  // getBasePath(): PathSelector {
  //   return ['webapi_login_session', 'session', 'User'];
  // }

  toggleSelection(mapping : IOrganisationSystemNotificationMapping) {
    let idx = this.selection.findIndex((x) => x.id === mapping.id);
    if (idx > -1){
      this.selection.splice(idx, 1);
    }
    else {
      this.selection.push(mapping);
    }
  }

  addMapping() {
    this.modalService.show(AddNotificationComponent);
  }

  delMapping() {
    this.selection.forEach((mapping) => {
      this.userService.sendDeletedSystemNotification(mapping);
    });
  }
}