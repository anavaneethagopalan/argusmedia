import { Injectable } from "@angular/core";
import { dispatch } from '@angular-redux/store';

// import { FluxStandardAction } from "flux-standard-action";

import { OrganisationSystemNotificationTypeMessage } from '../../models/organisation-system-notification-type'
import { OrganisationSystemNotificationMappingMessage } from '../../models/organisation-system-notification-mapping'

@Injectable()
export class OrganisationDetailsActions {
    static readonly SET_ORGANISATION_SYSTEM_NOTIFICATIONS = "SET_ORGANISATION_SYSTEM_NOTIFICATIONS";
    static readonly DELETE_ORGANISATION_SYSTEM_NOTIFICATIONS = "DELETE_ORGANISATION_SYSTEM_NOTIFICATIONS";
    static readonly SET_ORGANISATION_SYSTEM_NOTIFICATION_TYPES = "SET_ORGANISATION_SYSTEM_NOTIFICATION_TYPES";

    //TODO: add strong types with FluxStandardAction
    @dispatch()
    setOrganisationSystemNotifications(message: OrganisationSystemNotificationMappingMessage)  {
        return {
            type: OrganisationDetailsActions.SET_ORGANISATION_SYSTEM_NOTIFICATIONS,
            payload: message
        }
    }
    
    @dispatch()
    deleteOrganisationSystemNotification(notificationId : number){
        return {
            type: OrganisationDetailsActions.DELETE_ORGANISATION_SYSTEM_NOTIFICATIONS,
            payload: notificationId
        }
    }
    
    @dispatch()
    setOrganisationSystemNotificationTypes(message: OrganisationSystemNotificationTypeMessage) {
        return {
            type: OrganisationDetailsActions.SET_ORGANISATION_SYSTEM_NOTIFICATION_TYPES,
            payload: message
        }
    }
}