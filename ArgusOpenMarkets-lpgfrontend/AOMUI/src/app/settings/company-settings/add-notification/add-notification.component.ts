import { Component, ViewChild } from '@angular/core';
import { select } from '@angular-redux/store';

import 'selectize';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Observable } from 'rxjs/Observable';

import { IOrganisationSystemNotificationType } from '../../../models/organisation-system-notification-type';
import { REGEX_EMAIL } from '../../../models/regex';
import { UserService } from '../../../shared/user.service';
import { selectProductDefinitions } from '../../../store/model';

@Component({
  selector: 'add-notification',
  templateUrl: './add-notification.component.html',
  styleUrls: ['./add-notification.component.scss'],
})
export class AddNotificationComponent {

  @select(selectProductDefinitions)
  public products$: Observable<any>;

  @select()
  public organisationSystemNotificationTypes$: Observable<IOrganisationSystemNotificationType[]>;

  @ViewChild("formCtrl")
  public form : any;

  public readonly emailSelectizeConfig: Selectize.IOptions<string, any> = {
    persist: false,
    maxItems: null,
    labelField: 'email',
    valueField: 'email',      
    // use "render" to add custom HTML to options in dropdown and selected items
    // render: {
    //   item: function(item, escape) {
    //       return '<div>' +
    //           (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
    //           (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
    //       '</div>';
    //   },
    //   option: function(item, escape) {
    //       var label = item.name || item.email;
    //       var caption = item.name ? item.email : null;
    //       return '<div>' +
    //           '<span class="label">' + escape(label) + '</span>' +
    //           (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
    //       '</div>';
    //   }
    // },
    createFilter: new RegExp('^' + REGEX_EMAIL + '$', 'i'),
    create: function(input: string) {
      if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
          return { email: input + ';' };
      }
      alert('Invalid email address.');
      return false;
    }
  }

  constructor(public bsModalRef: BsModalRef, private userService: UserService) {
  }

  submitForm(): void {
    let result: any = this.form.value;
    result.selectedProducts.forEach((p: number) => {
      result.selectedSystemNotificationTypes.forEach((snt: number) => {
        result.selectedEmails.forEach((email: string) => {
          this.userService.sendNewSystemNotification({
            eventType: snt,
            productId: p,            
            emailAddress: email
          });
        });        
      });      
    });

    this.bsModalRef.hide();
  }  
}