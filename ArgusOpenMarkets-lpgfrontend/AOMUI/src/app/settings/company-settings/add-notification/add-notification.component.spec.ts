import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux, NgReduxTestingModule } from '@angular-redux/store/testing';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { Ng2SelectizeModule } from 'ng2-selectize';

import { Message } from '../../../models/message';
import { selectProductDefinitions } from '../../../store/model';
import { UserService } from '../../../shared/user.service';
import { MessageDistributionService } from '../../../shared/message-distribution.service';
import { MultiSelectComponent } from '../../../shared/multiselect.component';
import { SelectizeComponent } from '../../../shared/selectize.component';
import { AddNotificationComponent } from './add-notification.component';

describe('AddNotificationComponent', () => {
  let component: AddNotificationComponent;
  let fixture: ComponentFixture<AddNotificationComponent>;

  let modalRefStub: BsModalRef = {
    hide() { }
  };
  let messageDistributionServiceStub = {
    sendMessage(topic: string, message: Message) { }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MultiselectDropdownModule,
        Ng2SelectizeModule
      ],
      providers: [
        { provide: BsModalRef, useValue: modalRefStub },
        UserService,
        { provide: MessageDistributionService, useValue: messageDistributionServiceStub },
        { provide: NgRedux, useValue: MockNgRedux },
      ],
      declarations: [
        AddNotificationComponent,
        MultiSelectComponent,
        SelectizeComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    const productDefinitionStub = MockNgRedux.getSelectorStub(selectProductDefinitions);
    productDefinitionStub.next([]);
    productDefinitionStub.complete();

    const organisationSystemNotificationTypeStub = MockNgRedux.getSelectorStub('organisationSystemNotificationTypes');
    organisationSystemNotificationTypeStub.next([{
      id: 2,
      description: 'Naptha CIF TEST TEST'
    }]);
    organisationSystemNotificationTypeStub.complete();

    fixture = TestBed.createComponent(AddNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
