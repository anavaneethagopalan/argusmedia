import { IOrganisationSystemNotificationType } from '../../models/organisation-system-notification-type';
import { OrganisationSystemNotification } from '../../models/organisation-system-notification';
import { OrganisationDetailsActions } from './actions';

export function organisationSystemNotificationTypes(state: IOrganisationSystemNotificationType[] = [], action){
    switch(action.type) {
        case OrganisationDetailsActions.SET_ORGANISATION_SYSTEM_NOTIFICATION_TYPES:
            return [
                ...state,
                action.payload
            ];
        
        default:
            return state;
    }
}

export function organisationSystemNotifications(state: OrganisationSystemNotification[] = [], action) {
    switch(action.type) {
        case OrganisationDetailsActions.SET_ORGANISATION_SYSTEM_NOTIFICATIONS:
            return [
                ...state,
                action.payload
            ];

        case OrganisationDetailsActions.DELETE_ORGANISATION_SYSTEM_NOTIFICATIONS:
            let result = [ ...state ];
            result.splice(result.findIndex((osn) => osn.id === action.payload), 1);
            return result;
        
        default:
            return state;
    }
}