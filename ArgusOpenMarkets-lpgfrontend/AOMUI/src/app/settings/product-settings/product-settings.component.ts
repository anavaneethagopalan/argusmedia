import { Component } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';

import{ Observable } from 'rxjs/Observable';

import { IProductDefinition } from '../../products/models/product';
import { IAppState, selectProductDefinitionsSorted } from '../../store/model';
import { TenorInfoComponent } from './tenor-info/tenor-info.component';

@Component({
  selector: 'aom-product-settings',
  templateUrl: './product-settings.component.html',
  styleUrls: ['./product-settings.component.scss']
})
export class ProductSettingsComponent {

  @select(selectProductDefinitionsSorted)
  public productDefinitions$: Observable<IProductDefinition>;

  constructor() { }
}
