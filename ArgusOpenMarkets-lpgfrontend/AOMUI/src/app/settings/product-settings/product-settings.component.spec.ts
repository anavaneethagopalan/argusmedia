import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgRedux } from '@angular-redux/store';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/lib/testing';

import { Observable } from 'rxjs/Observable';
import { AccordionModule } from 'ngx-bootstrap';

import { ProductSettingsComponent } from './product-settings.component';
import { TenorInfoComponent } from './tenor-info/tenor-info.component';

describe('ProductSettingsComponent', () => {
  let component: ProductSettingsComponent;
  let fixture: ComponentFixture<ProductSettingsComponent>;

  const mockNgRedux = MockNgRedux.getInstance();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgReduxTestingModule, AccordionModule.forRoot()],
      declarations: [ ProductSettingsComponent, TenorInfoComponent ],
    })
    .compileComponents()
    .then(() => {
      MockNgRedux.reset();
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
