import { Component, Input } from '@angular/core';

@Component({
  selector: 'aom-tenor-info',
  templateUrl: './tenor-info.component.html',
  styleUrls: ['./tenor-info.component.scss']
})
export class TenorInfoComponent {

  @Input()
  public productTenor;

  constructor() { }
}
