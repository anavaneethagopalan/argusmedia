import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionModule } from 'ngx-bootstrap';

import { TenorInfoComponent } from './tenor-info.component';

describe('TenorInfoComponent', () => {
  let component: TenorInfoComponent;
  let fixture: ComponentFixture<TenorInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AccordionModule.forRoot() ],
      declarations: [ TenorInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenorInfoComponent);
    component = fixture.componentInstance;
    const productTenor = {};
    component.productTenor = productTenor;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
