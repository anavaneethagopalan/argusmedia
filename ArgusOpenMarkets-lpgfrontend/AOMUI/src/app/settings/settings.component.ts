import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'aom-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  // This message is displayed to the user in IE and Edge when they navigate without using Angular routing (type another URL/close the browser/etc)
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    console.log($event);
    // if (!this.canDeactivate()) {
         $event.returnValue = 'Changes that you made may not be saved.';
    // }
  }

}
