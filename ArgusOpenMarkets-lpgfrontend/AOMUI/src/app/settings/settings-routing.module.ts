import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './settings.component';
import { MySettingsComponent } from './my-settings/my-settings.component';
import { CompanySettingsComponent } from './company-settings/company-settings.component';
import { CounterpartyPermissionsComponent } from './counterparty-permissions/counterparty-permissions.component';
import { BrokerPermissionsComponent } from './broker-permissions/broker-permissions.component';
import { SystemInformationComponent } from './system-information/system-information.component';
import { ProductSettingsComponent } from './product-settings/product-settings.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      { path: '', component: MySettingsComponent },
      { path: 'mysettings', component: MySettingsComponent },
      { path: 'companysettings', component: CompanySettingsComponent },
      { path: 'counterpartypermissions', component: CounterpartyPermissionsComponent },
      { path: 'brokerpermissions', component: BrokerPermissionsComponent },
      { path: 'systeminformation', component: SystemInformationComponent },
      { path: 'productsettings', component: ProductSettingsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
