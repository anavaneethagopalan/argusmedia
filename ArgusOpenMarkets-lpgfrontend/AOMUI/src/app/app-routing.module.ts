import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { ActiveUserSessionGuard } from './guards/active-user-session.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/login/login.module#LoginModule',
    canActivate: [ActiveUserSessionGuard]
  },
  {
    path: 'ResetPassword',
    redirectTo: 'login/ResetPassword',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: 'app/shell/shell.module#ShellModule',
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    // enableTracing: true, // for debug, please comment it out before pushing
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
