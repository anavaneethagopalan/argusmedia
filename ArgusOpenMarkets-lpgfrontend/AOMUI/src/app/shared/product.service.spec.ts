import { TestBed, inject } from '@angular/core/testing';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux, NgReduxTestingModule } from '@angular-redux/store/testing';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { ProductService } from './product.service';
import { MessageDistributionService } from './message-distribution.service';

describe('ProductService', () => {

  const messageDistributionServiceStub = {
    get productTopicMessages$() { return Observable.of({}); },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProductService,
        { provide: NgRedux, useValue: MockNgRedux },
        { provide: MessageDistributionService, useValue: messageDistributionServiceStub }
      ]
    });
  });

  it('should be created', inject([ProductService], (service: ProductService) => {
    expect(service).toBeTruthy();
  }));
});
