import { TestBed, inject } from '@angular/core/testing';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux, NgReduxTestingModule } from '@angular-redux/store/testing';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { NewsAPIActions } from '../api/news/actions';
import { OrderAPIActions } from '../api/orders/actions';
import { PermissionSummaryAPIActions } from '../api/permissions-summary/actions';
import { OrganisationDetailsActions } from '../settings/company-settings/actions';
import { MessageDistributionService } from './message-distribution.service';
import { SocketProvider } from './socket-provider';
import { ToastService } from './toast/toast.service';

describe('MessageDistributionService', () => {

  const socketProviderStub = {
    get closed$() { return Observable.of({}); },
    close: () => undefined
  };
  const toastServiceStub = {
    connectionLostMessage: () => undefined,
    showMarketCloseNotification: () => undefined
  };
  const organisationDetailsActionsStub = {
    setOrganisationSystemNotificationTypes: () => undefined,
    setOrganisationSystemNotifications: () => undefined
  };
  const newsAPIActionsStub = {
    deleteNews: () => undefined
  };
  const orderAPIActionsStub = {};

  const permissionSummaryAPIActionsStub = {
    setPermissionSummary: () => undefined
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MessageDistributionService,
        { provide: SocketProvider, useValue: socketProviderStub },
        { provide: ToastService, useValue: toastServiceStub },
        { provide: NgRedux, useValue: MockNgRedux },
        { provide: OrganisationDetailsActions, useValue: organisationDetailsActionsStub },
        { provide: NewsAPIActions, useValue: newsAPIActionsStub },
        { provide: OrderAPIActions, useValue: orderAPIActionsStub },
        { provide: PermissionSummaryAPIActions, useValue: permissionSummaryAPIActionsStub }
      ]
    });
  });

  it('should be created', inject([MessageDistributionService], (service: MessageDistributionService) => {
    expect(service).toBeTruthy();
  }));
});
