import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

import * as diffusion from 'diffusion';

import { IAppState } from '../store/model';
import { setConnectionStatus } from '../store/actions/connection-status';
import { ConnectionStatus } from '../models/connection-status';
import { ConfigService } from './config.service';
import { Message, MessageType, MessageAction } from '../models/message';

@Injectable()
export class SocketProvider {
    private diffusionSession: any;

    // Set maximum timeout duration to 1 minute
    private readonly maximumTimeoutDuration = 60000;
    private numberReconnectionAttempts = 0;
    private maxReconnectAttempt = 3;

    private closed = new Subject();

    constructor(private configService: ConfigService, private store: NgRedux<IAppState>) { }

    public connect(email: string, password: string): Observable<any> {
        const connectionDetails = {
            host: this.configService.get('DiffusionHostName'),
            port: this.configService.get('WsPort'),
            secure: this.configService.get('DiffusionSecure'),
            principal: email,
            credentials: password,
            reconnect: {
                timeout: this.maximumTimeoutDuration,
                strategy: this.reconnectionStrategy.bind(this)
            }
        };
        console.log('diffusion connection details', connectionDetails);
        return Observable.create((observer: any) => {
            diffusion.connect(connectionDetails).then(
                (session) => {
                    console.log('Connected to Diffusion.');
                    this.diffusionSession = session;
                    this.numberReconnectionAttempts = 0;
                    this.store.dispatch(setConnectionStatus(ConnectionStatus.Connected));
                    observer.complete();

                    session.on('disconnect', function () {
                        // Client has been disconnected.
                        console.log('Client session disconnected');
                        this.store.dispatch(setConnectionStatus(ConnectionStatus.Disconnected));
                    }.bind(this));

                    session.on('reconnect', function () {
                        // Session hs been re-connected.
                        console.log('Client session has reconnected');
                        this.numberReconnectionAttempts = 0;
                        this.store.dispatch(setConnectionStatus(ConnectionStatus.Reconnected));
                    }.bind(this));

                    session.on('close', function () {
                        console.log('Client session has been closed');
                        this.store.dispatch(setConnectionStatus(ConnectionStatus.Closed));
                        this.closed.next();
                    }.bind(this));
                },
                (err) => {
                    console.log('Error occured trying to connect to Diffusion');
                    observer.error(err);
                });
        });
    }

    public close(): void {
        if (this.diffusionSession) {
            this.diffusionSession = this.diffusionSession.close();
        }
    }

    public get closed$() {
        return this.closed.asObservable();
    }

    public reconnectionStrategy(start: Function, abort: Function) {
        console.log('Connnection Lost - Attempting Reconnect');
        if (this.numberReconnectionAttempts > this.maxReconnectAttempt) {
            abort();
        }
        this.numberReconnectionAttempts++;
        setTimeout(start, 1000);
    }

    public stream(topicPath: string, callback: Function) {
        this.diffusionSession.stream(topicPath, callback);
    }

    public streamJsonTopic(topicPath: string, callback: Function) {
        this.diffusionSession
            .stream(topicPath)
            .asType(diffusion.datatypes.json())
            .on('value', (path: string, specification, newValue, oldValue) => {
                callback(newValue.get(), path);
            });
    }

    public streamJsonTopicWithDeleteNotification(topicPath: string, callback: Function, deleteCallback: Function){
        this.diffusionSession
        .stream(topicPath)
        .asType(diffusion.datatypes.json())
        .on('value', (path: string, specification, newValue, oldValue) => {
            callback(newValue.get());
        })
        .on('unsubscribe', function(path: string, topicType, reason){
            console.log('TOPIC DELETED - ' + path);
            deleteCallback(path);
        });
    }

    public sendMessage(topic: string, messageType: MessageType, messageAction: MessageAction, messageBody: any) {

        let message: Message = {
            messageType: MessageType[messageType],
            messageAction: MessageAction[messageAction],
            messageBody: JSON.stringify(messageBody)
        };

        this.diffusionSession.messages.send(topic, JSON.stringify(message));
    }

    public subscribe(topicPath: string, callback: Function) {
        this.diffusionSession.subscribe(topicPath, callback);
    }

    public listen(topicPath: string, callback: Function) {
        this.diffusionSession.messages.listen(topicPath, callback);
    }

    public isConnected(): boolean {
        if (this.diffusionSession && this.diffusionSession.isConnected()) {
            return true;
        }
        return false;
    }
}


