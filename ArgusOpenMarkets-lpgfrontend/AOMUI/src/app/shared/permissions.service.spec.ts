import { TestBed, inject } from '@angular/core/testing';
import { MockNgRedux, NgReduxTestingModule } from '@angular-redux/store/testing';

import { IUser } from '../api/models/user/user';
import { IOrder } from '../api/orders/models';
import { IProductDefinition, MarketStatus } from '../products/models/product';
import { IPermissionSummary } from '../api/permissions-summary/models';
import { PermissionsService } from './permissions.service';

describe('PermissionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PermissionsService]
    })
    .compileComponents()
    .then(() => { 
      MockNgRedux.reset();

      //#region *** MOCK USER ***
      const userStub = MockNgRedux.getSelectorStub(['webapi_login_session', 'session', 'User']);
      const user = {
        id: 1,
        organisationId: 3,
        productPrivileges: [{
          productId: 1,
          privileges: {}
        },
        {
          productId: 2,
          privileges: {
            "order_Create_Principal": true,
            "abc": false
          }
        }]
      }
      userStub.next(user);
      userStub.complete();
      //#endregion
    });
  });

  it('should be created', inject([PermissionsService], (service: PermissionsService) => {
    expect(service).toBeTruthy();
  }));

  describe('can create order -> ', function() {
    it('cannot create order when have privileges for another product', inject([PermissionsService], (service: PermissionsService) => {    
      let result = true;
      service.canCreateOrder$(1).subscribe(_ => result = _);
      expect(result).toBeFalsy();
    }));

    it('can create order', inject([PermissionsService], (service: PermissionsService) => {
      let result = true;
      service.canCreateOrder$(2).subscribe(_ => result = _);
      expect(result).toBeTruthy();
    }));
  });

  describe('is order mine -> ', function() {
    it('is order not mine', inject([PermissionsService], (service: PermissionsService) => {
      let orderStub: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'BID',
        quantity: 270,
        notes: '',
        productTenors: [],
        brokerOrganisationId: 11, // <=
        brokerShortCode: '',
        brokerOrganisation: {},
        orderStatus: 'A',
        enteredByUserId: '0',
        brokerRestriction: '',
        coBrokering: false,
        orderPriceLines: [],
        productId: 1,
        principalOrganisationId: 10, // <=
        principalOrganisationShortCode: ''
      };

      let result = true;
      service.isOrderMine$(orderStub).subscribe(_ => result = _);
      expect(result).toBeFalsy();
    }));

    it('is order mine - principal', inject([PermissionsService], (service: PermissionsService) => {
      let orderStub: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'BID',
        quantity: 270,
        notes: '',
        productTenors: [],
        brokerOrganisationId: 4,
        brokerShortCode: '',
        brokerOrganisation: {},
        orderStatus: 'A',
        enteredByUserId: '0',
        brokerRestriction: '',
        coBrokering: false,
        orderPriceLines: [],
        productId: 1,
        principalOrganisationId: 3, // <=
        principalOrganisationShortCode: ''
      };

      let result = false;
      service.isOrderMine$(orderStub).subscribe(_ => result = _);
      expect(result).toBeTruthy();
    }));

    it('is order mine - broker', inject([PermissionsService], (service: PermissionsService) => {
      const orderStub: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'BID',
        quantity: 270,
        notes: '',
        productTenors: [],
        brokerOrganisationId: 3, // <=
        brokerShortCode: '',
        brokerOrganisation: {},
        orderStatus: 'A',
        enteredByUserId: '0',
        brokerRestriction: '',
        coBrokering: false,
        orderPriceLines: [],
        productId: 1,
        principalOrganisationId: 1000,
        principalOrganisationShortCode: ''
      };

      let result = false;
      service.isOrderMine$(orderStub).subscribe(_ => result = _);
      expect(result).toBeTruthy();
    }));
  });

  describe('can trade -> ', function() {
    beforeEach(() => {
      MockNgRedux.reset();
    });

    it('broker - no permission', inject([PermissionsService], (service: PermissionsService) => {
      const userStub: IUser = {
        id: 1,
        username: '',
        name: '',
        email: '',
        organisationId: 3,
        isBlocked: false,
        isActive: true,
        isDeleted: false,
        telephone: '',
        userOrganisation: {
          id: 1,
          name: '',
          organisationType: 'Brokerage',
          shortCode: '',
          legalName: '',
          email: '',
          isDeleted: false,
          isBroker: true
        },
        productPrivileges: [],
        systemPrivileges: {
          privileges: [],
          organisationType: 0
        },
      }

      //#region *** MOCK PERMISSIONS ***
      const permissionsSummaryStoreMock = MockNgRedux.getSelectorStub('permissionsSummary');
      const invalidPermission: IPermissionSummary = {
        productId: 1,
        organisationId: 1000,
        buyOrSell: 'buy',
        permissionType: 'CounterpartyPermission'
      };
      permissionsSummaryStoreMock.next([invalidPermission]);
      //#endregion

      const orderStub: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'BID',
        quantity: 270,
        notes: '',
        productTenors: [],
        brokerOrganisationId: 3, 
        brokerShortCode: '',
        brokerOrganisation: {},
        orderStatus: 'A',
        enteredByUserId: '0',
        brokerRestriction: '',
        coBrokering: true, // <=
        orderPriceLines: [],
        productId: 1,
        principalOrganisationId: 1000,
        principalOrganisationShortCode: ''
      };

      const productDefinitionStub: IProductDefinition = {
        productId: 1,
        productName: '',
        productTitle: '',
        isInternal: false,
        hasAssesment: false,
        status: MarketStatus.Open,
        openTime: new Date(Date.now()),
        closeTime: new Date(Date.now()),
        lastOpenTime: new Date(Date.now()),
        lastCloseTime: new Date(Date.now()),
        commodity: {
          id: 1,
          name: '',
        },
        deliveryLocation: {
          id: 1,
          name: '',
        },
        contractSpecification: {
          volume: {
            default: 1,
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            volumeUnitCode: '',
            commonQuantities: {},
            commonQuantityValues: [],
            volumeUnitName: '',
            volumeUnitDescription: ''
          },
          pricing: {
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            pricingCurrencyCode: '',
            pricingCurrencyDisplayName: '',
            pricingUnitName: '',
            pricingUnitDescription: ''
          },
          productTenors: [],
          contractType: '',
        },
        bidAskStackNumberRows: 23,
        coBrokering: true, // <=
        productLineTemplates: []
      };

      let result = true;
      service.canTrade$(productDefinitionStub, orderStub, userStub).subscribe(_ => result = _);
      expect(result).toBeFalsy();
    }));

    it('broker - can trade', inject([PermissionsService], (service: PermissionsService) => {
      const userStub: IUser = {
        id: 1,
        username: '',
        name: '',
        email: '',
        organisationId: 3,
        isBlocked: false,
        isActive: true,
        isDeleted: false,
        telephone: '',
        userOrganisation: {
          id: 3,
          name: '',
          organisationType: 'Brokerage',
          shortCode: '',
          legalName: '',
          email: '',
          isDeleted: false,
          isBroker: true
        },
        productPrivileges: [],
        systemPrivileges: {
          privileges: [],
          organisationType: 0
        },
      }

      //#region *** MOCK PERMISSIONS ***
      const permissionsSummaryStoreMock = MockNgRedux.getSelectorStub('permissionsSummary');
      const permissionStub: IPermissionSummary = {
        productId: 1,
        organisationId: 10,
        buyOrSell: 'sell',
        permissionType: 'BrokerPermission'
      };
      permissionsSummaryStoreMock.next([permissionStub]);
      //#endregion

      const orderStub: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'BID', // <=
        quantity: 270,
        notes: '',
        productTenors: [],
        brokerOrganisationId: 3, 
        brokerShortCode: '',
        brokerOrganisation: {},
        orderStatus: 'A',
        enteredByUserId: '0',
        brokerRestriction: '',
        coBrokering: true, // <=
        orderPriceLines: [],
        productId: 1, // <=
        principalOrganisationId: 10,
        principalOrganisationShortCode: ''
      };

      const productDefinitionStub: IProductDefinition = {
        productId: 1, // <=
        productName: '',
        productTitle: '',
        isInternal: false,
        hasAssesment: false,
        status: MarketStatus.Open,
        openTime: new Date(Date.now()),
        closeTime: new Date(Date.now()),
        lastOpenTime: new Date(Date.now()),
        lastCloseTime: new Date(Date.now()),
        commodity: {
          id: 1,
          name: '',
        },
        deliveryLocation: {
          id: 1,
          name: '',
        },
        contractSpecification: {
          volume: {
            default: 1,
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            volumeUnitCode: '',
            commonQuantities: {},
            commonQuantityValues: [],
            volumeUnitName: '',
            volumeUnitDescription: ''
          },
          pricing: {
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            pricingCurrencyCode: '',
            pricingCurrencyDisplayName: '',
            pricingUnitName: '',
            pricingUnitDescription: ''
          },
          productTenors: [],
          contractType: '',
        },
        bidAskStackNumberRows: 23,
        coBrokering: true, // <=
        productLineTemplates: []
      };

      let result = true;
      service.canTrade$(productDefinitionStub, orderStub, userStub).subscribe(_ => result = _);
      expect(result).toBeTruthy();
    }));

    it('principal - cannot trade with broker, but can with principal', inject([PermissionsService], (service: PermissionsService) => {
      const userStub: IUser = {
        id: 1,
        username: '',
        name: '',
        email: '',
        organisationId: 3,
        isBlocked: false,
        isActive: true,
        isDeleted: false,
        telephone: '',
        userOrganisation: {
          id: 3,
          name: '',
          organisationType: 'not broker',
          shortCode: '',
          legalName: '',
          email: '',
          isDeleted: false,
          isBroker: false
        },
        productPrivileges: [],
        systemPrivileges: {
          privileges: [],
          organisationType: 0
        },
      }

      //#region *** MOCK PERMISSIONS ***
      const permissionsSummaryStoreMock = MockNgRedux.getSelectorStub('permissionsSummary');
      const permission: IPermissionSummary = {
        productId: 1,
        organisationId: 10,
        buyOrSell: 'buy',
        permissionType: 'CounterpartyPermission'
      };
      permissionsSummaryStoreMock.next([permission]);
      //#endregion

      const orderStub: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'ASK', // <=
        quantity: 270,
        notes: '',
        productTenors: [],
        brokerOrganisationId: 3, 
        brokerShortCode: '',
        brokerOrganisation: {},
        orderStatus: 'A',
        enteredByUserId: '0',
        brokerRestriction: '',
        coBrokering: true, // <=
        orderPriceLines: [],
        productId: 1, // <=
        principalOrganisationId: 10,
        principalOrganisationShortCode: ''
      };

      const productDefinitionStub: IProductDefinition = {
        productId: 1, // <=
        productName: '',
        productTitle: '',
        isInternal: false,
        hasAssesment: false,
        status: MarketStatus.Open,
        openTime: new Date(Date.now()),
        closeTime: new Date(Date.now()),
        lastOpenTime: new Date(Date.now()),
        lastCloseTime: new Date(Date.now()),
        commodity: {
          id: 1,
          name: '',
        },
        deliveryLocation: {
          id: 1,
          name: '',
        },
        contractSpecification: {
          volume: {
            default: 1,
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            volumeUnitCode: '',
            commonQuantities: {},
            commonQuantityValues: [],
            volumeUnitName: '',
            volumeUnitDescription: ''
          },
          pricing: {
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            pricingCurrencyCode: '',
            pricingCurrencyDisplayName: '',
            pricingUnitName: '',
            pricingUnitDescription: ''
          },
          productTenors: [],
          contractType: '',
        },
        bidAskStackNumberRows: 23,
        coBrokering: true, // <=
        productLineTemplates: []
      };

      let result = true;
      service.canTrade$(productDefinitionStub, orderStub, userStub).subscribe(_ => result = _);
      expect(result).toBeFalsy();
    }));

    it('principal - cannot trade with principal, but can with broker', inject([PermissionsService], (service: PermissionsService) => {
      const userStub: IUser = {
        id: 1,
        username: '',
        name: '',
        email: '',
        organisationId: 3,
        isBlocked: false,
        isActive: true,
        isDeleted: false,
        telephone: '',
        userOrganisation: {
          id: 3,
          name: '',
          organisationType: 'not broker',
          shortCode: '',
          legalName: '',
          email: '',
          isDeleted: false,
          isBroker: false
        },
        productPrivileges: [],
        systemPrivileges: {
          privileges: [],
          organisationType: 0
        },
      }

      //#region *** MOCK PERMISSIONS ***
      const permissionsSummaryStoreMock = MockNgRedux.getSelectorStub('permissionsSummary');
      const permission: IPermissionSummary = {
        productId: 1,
        organisationId: 10,
        buyOrSell: 'buy',
        permissionType: 'BrokerPermission'
      };
      permissionsSummaryStoreMock.next([permission]);
      //#endregion

      const orderStub: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'ASK', // <=
        quantity: 270,
        notes: '',
        productTenors: [],
        brokerOrganisationId: 3, 
        brokerShortCode: '',
        brokerOrganisation: {},
        orderStatus: 'A',
        enteredByUserId: '0',
        brokerRestriction: '',
        coBrokering: true, // <=
        orderPriceLines: [],
        productId: 1, // <=
        principalOrganisationId: 10,
        principalOrganisationShortCode: ''
      };

      const productDefinitionStub: IProductDefinition = {
        productId: 1, // <=
        productName: '',
        productTitle: '',
        isInternal: false,
        hasAssesment: false,
        status: MarketStatus.Open,
        openTime: new Date(Date.now()),
        closeTime: new Date(Date.now()),
        lastOpenTime: new Date(Date.now()),
        lastCloseTime: new Date(Date.now()),
        commodity: {
          id: 1,
          name: '',
        },
        deliveryLocation: {
          id: 1,
          name: '',
        },
        contractSpecification: {
          volume: {
            default: 1,
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            volumeUnitCode: '',
            commonQuantities: {},
            commonQuantityValues: [],
            volumeUnitName: '',
            volumeUnitDescription: ''
          },
          pricing: {
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            pricingCurrencyCode: '',
            pricingCurrencyDisplayName: '',
            pricingUnitName: '',
            pricingUnitDescription: ''
          },
          productTenors: [],
          contractType: '',
        },
        bidAskStackNumberRows: 23,
        coBrokering: true, // <=
        productLineTemplates: []
      };

      let result = true;
      service.canTrade$(productDefinitionStub, orderStub, userStub).subscribe(_ => result = _);
      expect(result).toBeFalsy();
    }));

    it('principal - can trade with principal and broker', inject([PermissionsService], (service: PermissionsService) => {
      const userStub: IUser = {
        id: 1,
        username: '',
        name: '',
        email: '',
        organisationId: 3,
        isBlocked: false,
        isActive: true,
        isDeleted: false,
        telephone: '',
        userOrganisation: {
          id: 3,
          name: '',
          organisationType: 'not broker',
          shortCode: '',
          legalName: '',
          email: '',
          isDeleted: false,
          isBroker: false
        },
        productPrivileges: [],
        systemPrivileges: {
          privileges: [],
          organisationType: 0
        },
      }

      //#region *** MOCK PERMISSIONS ***
      const permissionsSummaryStoreMock = MockNgRedux.getSelectorStub('permissionsSummary');
      const permissionPrincipal: IPermissionSummary = {
        productId: 1,
        organisationId: 10,
        buyOrSell: 'buy',
        permissionType: 'CounterpartyPermission'
      };
      const permissionBroker: IPermissionSummary = {
        productId: 1,
        organisationId: 3,
        buyOrSell: 'buy',
        permissionType: 'BrokerPermission'
      };
      permissionsSummaryStoreMock.next([permissionPrincipal, permissionBroker]);
      //#endregion

      const orderStub: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'ASK', // <=
        quantity: 270,
        notes: '',
        productTenors: [],
        brokerOrganisationId: 3, 
        brokerShortCode: '',
        brokerOrganisation: {},
        orderStatus: 'A',
        enteredByUserId: '0',
        brokerRestriction: '',
        coBrokering: true, // <=
        orderPriceLines: [],
        productId: 1, // <=
        principalOrganisationId: 10,
        principalOrganisationShortCode: ''
      };

      const productDefinitionStub: IProductDefinition = {
        productId: 1, // <=
        productName: '',
        productTitle: '',
        isInternal: false,
        hasAssesment: false,
        status: MarketStatus.Open,
        openTime: new Date(Date.now()),
        closeTime: new Date(Date.now()),
        lastOpenTime: new Date(Date.now()),
        lastCloseTime: new Date(Date.now()),
        commodity: {
          id: 1,
          name: '',
        },
        deliveryLocation: {
          id: 1,
          name: '',
        },
        contractSpecification: {
          volume: {
            default: 1,
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            volumeUnitCode: '',
            commonQuantities: {},
            commonQuantityValues: [],
            volumeUnitName: '',
            volumeUnitDescription: ''
          },
          pricing: {
            minimum: 1,
            maximum: 1,
            increment: 1,
            decimalPlaces: 1,
            pricingCurrencyCode: '',
            pricingCurrencyDisplayName: '',
            pricingUnitName: '',
            pricingUnitDescription: ''
          },
          productTenors: [],
          contractType: '',
        },
        bidAskStackNumberRows: 23,
        coBrokering: true, // <=
        productLineTemplates: []
      };

      let result = true;
      service.canTrade$(productDefinitionStub, orderStub, userStub).subscribe(_ => result = _);
      expect(result).toBeTruthy();
    }));
  });
});
