import { FormGroup, FormControl, ValidatorFn } from '@angular/forms';

export class DeliveryPeriodValidator {

    constructor() { }

    static isValidRange(minimumDeliveryRange: number): ValidatorFn {
        return (control: FormGroup) => {
            const deliveryStartDateControl = control.get('deliveryStartDate');
            const deliveryEndDateControl = control.get('deliveryEndDate');
            if (deliveryStartDateControl.value && deliveryEndDateControl.value) {
                const noOfDays = Math.round((deliveryEndDateControl.value.getTime() - deliveryStartDateControl.value.getTime())
                    / (1000 * 60 * 60 * 24)) + 1;
                if (noOfDays >= minimumDeliveryRange) {
                    return null;
                }
            }

            return {
                invalidRange: 'Minimum delivery period days is ' + minimumDeliveryRange
            };
        };
    }
}
