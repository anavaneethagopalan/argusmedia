import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { DeliveryPeriodDateRange, DeliveryPeriodDropdown, isDeliveryPeriodDropdown } from '../../api/create-order/models';

@Component({
  selector: 'delivery-period',
  templateUrl: './delivery-period.component.html',
  styleUrls: ['./delivery-period.component.scss']
})
export class DeliveryPeriodComponent implements OnInit {

  @Input() public delvieryPeriodConfig: DeliveryPeriodDateRange | DeliveryPeriodDropdown;
  @Input() public parentForm: FormGroup;

  bsConfig: Partial<BsDatepickerConfig>;
  noOfDaysSelected = 0;

  constructor() { }

  ngOnInit() {
    console.log('delvieryPeriodConfig', this.delvieryPeriodConfig);
    this.bsConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      showWeekNumbers: false,
      dateInputFormat: 'DD/MM/YYYY',
    });
    if (!this.hasDeliveryPeriodOptions) {
      this.noOfDaysSelected = this.calculateNoOfDaysSelected();
      this.deliveryStartDate.valueChanges.subscribe(() => {
        this.noOfDaysSelected = this.calculateNoOfDaysSelected();
      });
      this.deliveryEndDate.valueChanges.subscribe(() => {
        this.noOfDaysSelected = this.calculateNoOfDaysSelected();
      });
    }
  }

  datePickerValueChanged() {
    this.noOfDaysSelected = this.calculateNoOfDaysSelected();
  }

  public calculateNoOfDaysSelected() {
    if (this.deliveryStartDate.value != null && this.deliveryEndDate.value != null) {
      return Math.round((this.deliveryEndDate.value.getTime() - this.deliveryStartDate.value.getTime())
        / (1000 * 60 * 60 * 24)) + 1;
    }
    return 0;
  }

  get deliveryStartDate() {
    return this.parentForm.get('deliveryPeriod').get('deliveryStartDate');
  }

  get deliveryEndDate() {
    return this.parentForm.get('deliveryPeriod').get('deliveryEndDate');
  }

  get hasDeliveryPeriodOptions(): boolean {
    return isDeliveryPeriodDropdown(this.delvieryPeriodConfig);
  }
}
