import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';

import { BsDatepickerModule } from 'ngx-bootstrap';

import { DeliveryPeriodComponent } from './delivery-period.component';

describe('DeliveryPeriodComponent', () => {
  let component: DeliveryPeriodComponent;
  let fixture: ComponentFixture<DeliveryPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, BsDatepickerModule.forRoot()],
      declarations: [DeliveryPeriodComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryPeriodComponent);
    component = fixture.componentInstance;
    component.parentForm = new FormGroup({
      deliveryPeriod: new FormGroup({
        deliveryPeriodOption: new FormControl('')
      })
    });
    component.delvieryPeriodConfig = {
      deliveryPeriodOptions: []
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
