import { Component, Input, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';

/*
*   Wrapper for ss-multiselect-dropdown, which fails to detect a change when all checkboxes are deselected
*/
@Component({
    selector: 'multi-select',
    template:
        `
            <ss-multiselect-dropdown [options]='dropdownItems' [disabled]='disabled'
                [settings]='multiDropdownSettings' [texts]='multiDropdownTexts' (ngModelChange)='onChange($event)'
                [(ngModel)]='selection'></ss-multiselect-dropdown>
        `,
    providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => MultiSelectComponent),
          multi: true,
        }
    ]
})
export class MultiSelectComponent implements OnInit, ControlValueAccessor {

    @Input()
    public items: IMultiSelectOption[];

    public dropdownItems: IMultiSelectOption[];

    @Input()
    public valueProperty : string;

    @Input()
    public textProperty : string;

    @Input()
    public defaultTitle : string;

    @Input()
    public name : string;

    public disabled : boolean;

    private propagateChange = (_: any) => { };

    public selection : any[] = [];

    public readonly multiDropdownSettings: IMultiSelectSettings = {
        buttonClasses: 'btn btn-multi-select',
        checkedStyle: 'fontawesome',
        //fixedTitle: true
        showCheckAll: true,
        showUncheckAll: true
    };

    public multiDropdownTexts: IMultiSelectTexts = { };

    constructor() {
    }

    ngOnInit(): void {
        this.dropdownItems = this.items.map((x) => { return { id: x[this.valueProperty], name: x[this.textProperty] }});
        this.multiDropdownTexts = {
            defaultTitle: this.defaultTitle
        };
    }

    public onChange(selectedItems: any[]) {
        this.propagateChange(selectedItems);
    }

    //#region ControlValueAccessor interface implementation
    writeValue(obj: any): void {
        if(obj) {
            this.selection = obj;
        }
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {

    }

    setDisabledState?(isDisabled: boolean): void {
        this.disabled == isDisabled;
    }
    //#endregion
}
