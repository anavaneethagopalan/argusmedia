import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { QuantityConfig } from '../../api/create-order/models';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'quantity',
  templateUrl: './quantity.component.html',
  styleUrls: ['./quantity.component.scss']
})
export class QuantityComponent implements OnInit {

  @Input() public quantityConfig: QuantityConfig;
  @Input() public parentForm: FormGroup;
  @Output() public quantityChange: EventEmitter<number | string> = new EventEmitter<number | string>();
  // @Output() public quantityIncrement: EventEmitter<any> = new EventEmitter<any>();
  // @Output() public quantityDecrement: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    console.log('quantityConfig', this.quantityConfig);
  }

  public get quantitySelected() {
    return this.parentForm.get('quantity');
  }

  setQuantityOption(option) {
    this.setQuantity(option);
  }

  increment() {
    const quantity = Number(this.quantitySelected.value);
    if (!isNaN(quantity)) {
      const incrementedValue = quantity + this.quantityConfig.increment;
      if (incrementedValue <= this.quantityConfig.maximum) {
        this.setQuantity(incrementedValue);
      }
    } else {
      this.setQuantity(this.quantityConfig.minimum + this.quantityConfig.increment);
    }
  }

  decrement() {
    const quantity = Number(this.quantitySelected.value);
    if (!isNaN(quantity)) {
      const decrementedValue = quantity - this.quantityConfig.increment;
      if (decrementedValue >= this.quantityConfig.minimum) {
        this.setQuantity(decrementedValue);
      }
    } else {
      this.setQuantity(this.quantityConfig.maximum - this.quantityConfig.increment);
    }
  }

  private setQuantity(quantity) {
    this.quantitySelected.markAsDirty();
    this.quantityChange.emit(quantity);
  }

  get sortedQuantityOptions() {
    return this.quantityConfig.quantityOptions.sort((n1, n2) => n1.displayOrder - n2.displayOrder);
  }
}
