import { FormGroup, FormControl, ValidatorFn } from '@angular/forms';

export class QuantityValidator {

    constructor() { }

    static isValidValue(options: any[]): ValidatorFn {
        return (control: FormControl) => {
            if (options.find(o => o === control.value)) {
                return null;
            }
            const quantity = Number(control.value);
            if (!isNaN(quantity)) {
                return null;
            }
            return {
                invalidValue: 'Quantity must be numeric or one of the predefined options'
            };
        };
    }

    static minimum(min: number): ValidatorFn {
        return (control: FormControl) => {
            const quantity = Number(control.value);
            if (typeof quantity === 'number' && quantity < min) {
                return {
                    minimum: 'Quantity must be at least ' + min
                };
            }
            return null;
        };
    }

    static maximum(max: number): ValidatorFn {
        return (control: FormControl) => {
            const quantity = Number(control.value);
            if (typeof quantity === 'number' && quantity > max) {
                return {
                    maximum: 'Quantity must not exceed ' + max
                };
            }
            return null;
        };
    }
}
