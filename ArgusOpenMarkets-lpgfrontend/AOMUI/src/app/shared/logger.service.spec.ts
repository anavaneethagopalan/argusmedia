import { TestBed, inject } from '@angular/core/testing';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux, NgReduxTestingModule } from '@angular-redux/store/testing';

import { Message } from '../models/message';
import { ClientLogMessage } from '../models/client-log-message';
import { LoggerService } from './logger.service';
// import { MessageDistributionService } from './message-distribution.service';
import { UserService } from './user.service';

describe('LoggerService', () => {

  const userId = 123;

  // const messageDistributionServiceSub = {
  //   sendMessage: () => undefined
  // };

  const userServiceStub = {
    sendClientLog: (clientLog) => undefined
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgReduxTestingModule],
      providers: [
        LoggerService,
        // { provide: MessageDistributionService, useValue: messageDistributionServiceSub },
        { provide: UserService, useValue: userServiceStub },
        { provide: NgRedux, useValue: MockNgRedux }
      ]
    });
  });

  it('should be created', inject([LoggerService], (service: LoggerService) => {
    expect(service).toBeTruthy();
  }));

  it('should send ClientLog error message with userId to message distribution service', () => {
    const service = TestBed.get(LoggerService);    
    spyOn(service, 'getUserIdFromStore').and.returnValue(userId);

    const userService = TestBed.get(UserService);
    const sendClientLogSpy = spyOn(userService, 'sendClientLog');
    const errorMessage = 'something went wrong!';

    service.error(errorMessage);
    const expectedMessage: ClientLogMessage = {
      message: 'ERROR: ' + errorMessage,
      userId: userId
    };
    
    expect(sendClientLogSpy).toHaveBeenCalledWith(expectedMessage);
  });
});
