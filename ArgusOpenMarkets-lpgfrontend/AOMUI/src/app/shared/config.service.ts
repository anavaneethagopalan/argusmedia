import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { NgRedux } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';

import { IProductDefinition } from '../products/models/product';
import { IAppState } from '../store/model';
import { loadConfig } from '../store/actions/config';
import { UserSession } from '../models/user-session';
import { Config } from '../models/config';

@Injectable()
export class ConfigService {

  private configFromFile: any;
  private supportedBrowserList: string[] = [
    'Chrome',
    'Firefox',
    'Microsoft Edge',
    'IE'
  ];

  constructor(private http: Http, private store: NgRedux<IAppState>) {
  }

  public load() {
    return this.http.get('config.json').map((res) => res.json())
      .subscribe((configFromFile) => {
        this.configFromFile = configFromFile;
        console.log(this.configFromFile);

        const config: Config = Object.assign(new Config(), this.configFromFile);
        config.webApi = this.webApi;
        config.version = this.version;
        this.store.dispatch(loadConfig(config));
      });
  }

  public get(key: string) {
    return this.configFromFile[key];
  }

  public get webApi() {
    return this.get('WsProtocol') + '://'
      + this.get('WebServiceHostName') + '/'
      + this.get('WebApi');
  }

  public get version() {
    return this.trimSuffix('0.', this.get('Version'));
  }

  public unsupportedBrowserPreventLogin(browserName: string, browserVersion: string): boolean {
    if (browserName === 'IE') {
      const version = parseInt(browserVersion, 0);
      if (version && version < 11) {
        return true;
      }
    }
    return false;
  }

  public unsupportedBrowserShowWarning(browserName: string, browserVersion: string): boolean {
    if (this.supportedBrowserList.indexOf(browserName) >= 0) {
      return false;
    }
    return true;
  }

  private trimSuffix(suffix: string, input: string): string {
    if (input.indexOf(suffix) === 0) {
      return this.trimSuffix(suffix, input.substring(suffix.length, input.length));
    } else {
      return input;
    }
  }

  public getUserSession() : UserSession {
    
    let session = this.store.getState().webapi_login_session.session;
    let userId = session.userId;
    let sessionToken = session.sessionToken;

    let userSession = new UserSession();
    userSession.session = sessionToken;
    userSession.userId = userId.toString();

    return userSession;
  }

  public getProductDefinitionConfig(productId: number) : Observable<IProductDefinition> {

    let userSession = this.getUserSession();

    let myHeaders = new Headers();
    myHeaders.append('token', userSession.session);
    myHeaders.append('userId', userSession.userId);

    let myParams: URLSearchParams = new URLSearchParams();
    myParams.set('productId', productId.toString());

    let options = new RequestOptions({ headers: myHeaders, params: myParams });
    return this.http
        .get(this.webApi + '/getProductDefinition', options)
        .map((response) => response.json())
        .switchMap(resp => (resp.Error)
            ? Observable.throw({
                statusText: resp.message
            })
            : Observable.of(resp));
          }
    }
    

export function configServiceFactory(configService: ConfigService) {
  return () => configService.load();
}
