import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from './config.service';

@Injectable()
export class TradersService {

  constructor(private configService: ConfigService,
    private http: Http) { }

  
  public getPermissionedPrincipalsOrBrokers(userType: string, productId: number, orderType: string) : Observable<any> {

    let apiCall = '';
    if(userType.toUpperCase() === "T"){
        // User is a trader - get brokers.
        apiCall = '/getAllBrokersBuyOrSell';
    } else{
        apiCall = '/getAllPrincipalsBuyOrSell';
    }
    
    // TODO: OrderType - create type - can be "A" - Ask  / "B" - Bid
    let userSession = this.configService.getUserSession();
    let myHeaders = new Headers();
    myHeaders.append('token', userSession.session);
    myHeaders.append('userId', userSession.userId);
    
    let myParams: URLSearchParams = new URLSearchParams();
    myParams.set('userId', userSession.userId);
    myParams.set('productId', productId.toString());
    myParams.set('orderType', orderType);
    
    let options = new RequestOptions({ headers: myHeaders, params: myParams });
    return this.http
        .get(this.configService.webApi + apiCall, options)
        .map((response) => response.json())
        .switchMap(resp => (resp.Error)
            ? Observable.throw({
            statusText: resp.message
        })
        : Observable.of(resp));
    }
}
