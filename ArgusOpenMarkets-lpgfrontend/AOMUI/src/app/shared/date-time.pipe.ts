import { Pipe, PipeTransform,  } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'dateTimePipe'
})
export class DateTimePipe extends DatePipe implements PipeTransform {


  transform(value: Date, args?: any): any {
    
    if (!value) {
      return "";
    }
    
    let todaysDate = new Date();
    var dateToFormat = new Date(value);

    if (dateToFormat.setHours(0, 0, 0, 0) === todaysDate.setHours(0, 0, 0, 0)) {
      return super.transform(value, 'HH:mm');
    } else {
      return super.transform(value, 'd-MMM-y');
    }
    
  }

}