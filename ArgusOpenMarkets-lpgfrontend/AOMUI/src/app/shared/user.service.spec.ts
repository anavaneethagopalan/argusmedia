import { TestBed, inject } from '@angular/core/testing';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux, NgReduxTestingModule } from '@angular-redux/store/testing';

import { Subject } from 'rxjs/Subject';

import { IAppState } from '../store/model';
import { ILoginSessionState } from '../api/session/models';
import { Message } from '../models/message';
import { ICompanyDetailsMessage } from '../models/company-details-message';
import { UserService } from './user.service';
import { MessageDistributionService } from './message-distribution.service';

describe('UserService', () => {
  const messageDistributionServiceStub = {
    sendMessage(topic: string, message: Message) { }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ NgReduxTestingModule ],
      providers: [
        UserService,
        { provide: NgRedux, useValue: MockNgRedux.getInstance() },
        { provide: MessageDistributionService, useValue: messageDistributionServiceStub }
      ]
    }).compileComponents()
      .then(() => {
        MockNgRedux.reset();
      });
  });

  it('should be created', () => {
    expect(messageDistributionServiceStub).toBeTruthy();
  });

  it('sendNewSystemNotification should call messageDistributionService.sendMessage() with correct path', 
    inject([UserService, MessageDistributionService], 
      (service: UserService, messageDistibutionService: MessageDistributionService) => {
        spyOn(MockNgRedux.getInstance(), 'getState').and.returnValue({
          webapi_login_session: {
            loading: false,
            error: '',
            session: {
              userId: 3
            }
          }          
        });

        let systemNotification : ICompanyDetailsMessage = {
          productId: 1,
          emailAddress: '1@1.com',
          eventType: 2
        };
        let sendMessage = spyOn(messageDistibutionService, 'sendMessage');
        service.sendNewSystemNotification(systemNotification);
        expect(sendMessage).toHaveBeenCalledWith('AOM/Users/3', 16, 0, jasmine.any(Object));
    })
  );
});
