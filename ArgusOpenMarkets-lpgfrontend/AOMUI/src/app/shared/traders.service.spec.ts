import { TestBed, inject } from '@angular/core/testing';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { TradersService } from './traders.service';
import { ConfigService } from './config.service';

describe('TradersService', () => {
  const configServiceStub = {};
  
      beforeEach(() => {
          TestBed.configureTestingModule({
              providers: [
                  TradersService,
                  { provide: ConfigService, useValue: configServiceStub },
                  { provide: Http, useValue: {} }
              ]
          });
      });
  

  it('should be created', inject([TradersService], (service: TradersService) => {
    expect(service).toBeTruthy();
  }));
});
