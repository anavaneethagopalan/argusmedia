import { DateTimePipe } from './date-time.pipe';

const mockDatePipe = {
  transform: (value) => undefined
};

function getExpectedTime(dateTime: Date){
  
  let hours = dateTime.getHours().toString();
  if(hours.length === 1){
    hours = '0' + hours;
  }
  let minutes = dateTime.getMinutes().toString();
  if(minutes.length === 1){
    minutes = '0' + minutes;
  }

  return hours + ':' + minutes;
}

function getExpectedDate(dateTime: Date){
  let days = dateTime.getDate();
  let month = dateTime.getMonth();
  let year = dateTime.getFullYear();

  let fullMonth = '';
  switch(month){
    case 0: 
      fullMonth = 'Jan';
      break;
    case 1:
      fullMonth = 'Feb';
      break;
    case 2:
      fullMonth = 'Mar';
      break;
    case 3:
      fullMonth = 'Apr';
      break;
    case 4:
      fullMonth = 'May';
      break;
    case 5:
      fullMonth = 'Jun';
      break;
    case 6:
      fullMonth = 'Jul';
      break;
    case 7:
      fullMonth = 'Aug';
      break;
    case 8:
      fullMonth = 'Sep';
      break;
    case 9:
      fullMonth = 'Oct';
      break;
    case 10:
      fullMonth = 'Nov';
      break;
    case 11:
      fullMonth = 'Dec';
      break;
  }

  return days + '-' + fullMonth + '-' + year;
}

describe('DateTimePipe', () => {
  it('create an instance', () => {

    const pipe = new DateTimePipe(this.mockDatePipe);
    expect(pipe).toBeTruthy();
  });

  it('should return just the time portion of the date if todays date', () => {
    const pipe = new DateTimePipe(this.mockDatePipe);
    let todaysDate = new Date();
    let result = pipe.transform(todaysDate);

    let expectedTime = getExpectedTime(todaysDate);

    expect(result).toEqual(expectedTime);
  });

  it('should return just the date portion of the date if not todays date', () => {
    const pipe = new DateTimePipe(this.mockDatePipe);
    let oldDate = new Date(2010, 1, 1, 13, 0, 0);
    let result = pipe.transform(oldDate);

    let expectedDate = getExpectedDate(oldDate);

    expect(result).toEqual(expectedDate);
  });

  it('should return an empty string if a null value is passed in', () => {
    const pipe = new DateTimePipe(this.mockDatePipe);
    let result = pipe.transform(null);

    let expectedResult = '';

    expect(result).toEqual(expectedResult);
  });
});
