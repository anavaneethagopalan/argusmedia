import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MetaDataDropdown, MetaDataText, isMetaDataDropdown } from '../../api/create-order/models';

@Component({
  selector: 'metadata',
  templateUrl: './metadata.component.html',
  styleUrls: ['./metadata.component.scss']
})
export class MetadataComponent implements OnInit {

  @Input() public metadataConfig: (MetaDataDropdown | MetaDataText)[];
  @Input() public parentForm: FormGroup;

  constructor() { }

  ngOnInit() {
  }

  isDropdown = (metadata: MetaDataDropdown | MetaDataText) => isMetaDataDropdown(metadata);
}
