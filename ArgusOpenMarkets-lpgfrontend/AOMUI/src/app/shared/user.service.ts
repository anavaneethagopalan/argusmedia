import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';

import { IOrder } from '../api/orders/models';
import { IAppState } from '../store/model';
import { Message, MessageType, MessageAction } from '../models/message';
import { ICompanyDetailsMessage } from '../models/company-details-message';
import { ClientLogMessage } from '../models/client-log-message';
import { MessageDistributionService } from './message-distribution.service';

@Injectable()
export class UserService {

  constructor(private messageDistributionService: MessageDistributionService, 
    private store: NgRedux<IAppState>) {
  }

  private sendStandardFormatMessageToUsersTopic(messageType: MessageType, messageAction: MessageAction, bodyObject: any) {
    let session = this.store.getState().webapi_login_session.session;
    let topicName = 'AOM/Users/' + session.userId;

    // Copied from the old site
    // if (messageType === MessageType.Order || messageType === MessageType.ExternalDeal  || messageType ===  MessageType.Deal ) {
    //   try {
    //       this.adjustOffset(bodyObject);
    //   } catch (err) {
    //       console.log(err);
    //   }
    // }

    this.messageDistributionService.sendMessage(topicName, messageType, messageAction, bodyObject);
  }

  public sendNewSystemNotification(systemNotification : ICompanyDetailsMessage) {
    this.sendStandardFormatMessageToUsersTopic(MessageType.OrganisationNotificationDetail, MessageAction.Create, systemNotification);
  }

  public sendDeletedSystemNotification(systemNotification : ICompanyDetailsMessage) {
    this.sendStandardFormatMessageToUsersTopic(MessageType.OrganisationNotificationDetail, MessageAction.Delete, systemNotification);
  }

  public sendClientLog(clientLog: ClientLogMessage) {
    this.sendStandardFormatMessageToUsersTopic(MessageType.ClientLog, MessageAction.Create, clientLog);
  }

  public createOrder(order: IOrder){
    this.sendStandardFormatMessageToUsersTopic(MessageType.Order, MessageAction.Create, order);
  }

  // Copied from the old site
  // adjustOffset(orderOrDeal) {
  //   let offset = 0;
  //   if(orderOrDeal && orderOrDeal.deliveryStartDate){
  //     offset = orderOrDeal.deliveryStartDate.getTimezoneOffset();
  //   }
  //   if (offset !== 0) {
  //       orderOrDeal.deliveryStartDate.setMinutes(offset * -1);
  //   }

  //   offset = 0;
  //   if(orderOrDeal && orderOrDeal.deliveryEndDate){
  //     offset = orderOrDeal.deliveryEndDate.getTimezoneOffset();
  //   }
  //   if (offset !== 0) {
  //       orderOrDeal.deliveryEndDate.setMinutes(offset * -1);
  //   }
  // };
}
