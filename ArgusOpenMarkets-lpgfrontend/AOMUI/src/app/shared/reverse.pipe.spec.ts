import { ReversePipe } from './reverse.pipe';

describe('ReversePipe', () => {
    it('create an instance', () => {
        const pipe = new ReversePipe();
        expect(pipe).toBeTruthy();
    });

    it('should reverse the value', () => {

        this.students = [{
            name: 'Adam'
          },{
            name: 'David'
          },{
            name: 'Edward'
          }]

        const pipe = new ReversePipe();

        let result = pipe.transform(this.students);
        let firstItem = result[0].name;

        expect(firstItem).toBe('Edward');
    });

});