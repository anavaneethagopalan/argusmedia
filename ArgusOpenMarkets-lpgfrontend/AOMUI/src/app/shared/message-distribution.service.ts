import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { Observable, Subscribable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

import { IAppState } from '../store/model';
import { SocketProvider } from './socket-provider';
import { ToastService } from './toast/toast.service';
import { Message, MessageAction, MessageType } from '../models/message';
import { ProductMessage } from '../models/product-message';
import { NewsMessage } from '../api/news/models';
import { OrganisationSystemNotificationMappingMessage } from '../models/organisation-system-notification-mapping';
import { OrganisationSystemNotificationTypeMessage } from '../models/organisation-system-notification-type';
import { OrganisationDetailsActions } from '../settings/company-settings/actions';
import { NewsAPIActions } from '../api/news/actions';
import { OrderAPIActions } from '../api/orders/actions';
import { IOrderMessage } from '../api/orders/models';
import { IPermissionSummaryMessage, IPermissionSummary } from '../api/permissions-summary/models';
import { PermissionSummaryAPIActions } from '../api/permissions-summary/actions';

@Injectable()
export class MessageDistributionService {

  private disconnect = new Subject();
  private closedSubscription: Subscription;

  constructor(
    private socketProvider: SocketProvider,
    private toastService: ToastService,
    private organisationDetailsActions: OrganisationDetailsActions,
    private newsAPIActions: NewsAPIActions,
    private orderAPIActions: OrderAPIActions,
    private permissionSummaryAPIActions: PermissionSummaryAPIActions,
    private store: NgRedux<IAppState>
  ) {
    // this.toastService.showMarketCloseNotification(['Fuel Oil', 'Naptha']);
  }

  private userMessages = new Subject<any>();
  private userTopicMessages = new Subject<any>();
  private productTopicMessages = new Subject<ProductMessage>();

  public get userTopicMessages$(): Observable<any> {
    return this.userTopicMessages.asObservable();
  }

  public get productTopicMessages$(): Observable<ProductMessage> {
    return this.productTopicMessages.asObservable();
  }
  

  public connect(email: string, password: string): Observable<boolean> {
    this.closedSubscription = this.socketProvider.closed$.subscribe(() => this.handleCloseEvent());
    return Observable.create((observer: any) => {
      this.socketProvider.connect(email, password).subscribe(
        (x) => undefined,
        (err) => {
          observer.error(err);
        },
        () => {
          observer.complete();
          // connection successful
          const organisationId = this.store.getState().webapi_login_session.session.User.userOrganisation.id;
          //this.socketProvider.streamJsonTopic('?AOM/Users//', this.userTopicCallback.bind(this));
          this.socketProvider.listen('?AOM/Users//', this.userMessageCallback.bind(this));
          this.socketProvider.streamJsonTopic('?AOM/Products//', this.productTopicCallback.bind(this));
          this.socketProvider.streamJsonTopicWithDeleteNotification('?AOM/News//', this.newsTopicCallback.bind(this), this.newsTopicDeleted.bind(this));
          this.socketProvider.streamJsonTopic(
            '?AOM/Organisations/' + organisationId + '/SystemNotificationTypes/',
            this.systemNotificationTypeTopicCallback.bind(this)
          );
          this.socketProvider.streamJsonTopicWithDeleteNotification(
            '?AOM/Organisations/' + organisationId + '/SystemNotifications/',
            this.systemNotificationTopicCallback.bind(this),
            this.systemNotificationTopicDeletedCallback.bind(this)
          );
          this.socketProvider.streamJsonTopic(
            '?AOM/Organisations/' + organisationId + '/PermissionsMatrix/Summary/Products/', 
            this.permissionSummaryTopicCallback.bind(this)
          );
        });
    });
  }

  public get disconnect$() {
    return this.disconnect.asObservable();
  }

  public close(): void {
    this.closedSubscription.unsubscribe();
    this.socketProvider.close();
  }

  public userMessageCallback(message) {
    const messageContent = JSON.parse(message.content);
    console.log('userMessageCallback message: ', messageContent);
    if (messageContent.eventName === 'TerminateResponse') {
      this.disconnect.next();
      this.close();
      this.toastService.sessionTerminationMessage();
    }
  }

  public productTopicCallback(message: ProductMessage) {
    try {
      // console.log('Diffusion product json', JSON.stringify(productMessage));
      this.productTopicMessages.next(message);

      if(message.productTopicType == 'MarketStatus'){
        this.socketProvider.streamJsonTopic('?AOM/Orders/Products/' + message.productId + '/', this.orderTopicCallback.bind(this));
      }
    } catch (err) {
      // console.error('Failed to parse product message', err);
    }
  }

  public newsTopicCallback(message: NewsMessage) {
    try {
      this.store.dispatch(this.newsAPIActions.setNews(message.news));
    } catch (err) {
      // console.error('Failed to parse product message', err);
    }
  }

  public orderTopicCallback(message: IOrderMessage): void {
    this.orderAPIActions.setOrders(message.order);
  }

  public getCmsIdFromNewsTopic(topic: string): string {
    if (topic) {
      const lastslashindex = topic.lastIndexOf('/');
      return topic.substring(lastslashindex + 1);
    }

    return '';
  }

  public newsTopicDeleted(newsTopicDeleted: string) {

    // TODO: We need to handle a news topic being deleted.
    const cmsId = this.getCmsIdFromNewsTopic(newsTopicDeleted);
    if (cmsId) {
      // TODO: Implement the action - DeleteNews.
      this.store.dispatch(this.newsAPIActions.deleteNews(cmsId));
    }
  }

  public systemNotificationTypeTopicCallback(message: OrganisationSystemNotificationTypeMessage) {
    try {
      // console.log("Diffusion my organisation system notification type JSON: " + JSON.stringify(message));
      this.organisationDetailsActions.setOrganisationSystemNotificationTypes(message);
    } catch (err) {
      // console.error('Failed to parse product message', err);
    }
  }

  public systemNotificationTopicCallback(message: OrganisationSystemNotificationMappingMessage) {
    try {
      // console.log("Diffusion my organisation system notification JSON: " + JSON.stringify(message));
      this.organisationDetailsActions.setOrganisationSystemNotifications(message);
    } catch (err) {
      // console.error('Failed to parse product message', err);
    }
  }

  public systemNotificationTopicDeletedCallback(path: string) {
    try {
      const pathParts = path.split('/');
      this.organisationDetailsActions.deleteOrganisationSystemNotification(parseInt(pathParts[pathParts.length - 1]));
    } catch (err) {
      // console.error('Failed to parse product message', err);
    }
  }

  public permissionSummaryTopicCallback(message: IPermissionSummaryMessage, topicPath: string) {
    try {
      let splitPath = topicPath.split("/");
      let productId = parseInt(splitPath[splitPath.indexOf("Products") + 1]);
      let buyOrSell: 'buy' | 'sell' = splitPath[splitPath.indexOf("Buy")] ? "buy" : "sell";

      let tradePermission: IPermissionSummary = {
        productId: productId,
        permissionType: <'CounterpartyPermission' | 'BrokerPermission'>message.message.messageType,
        //canTrade: message.canTrade, // not being used, if permission is denied, it will be removed from the array
        organisationId: message.organisationId,
        buyOrSell: buyOrSell
      };
      this.permissionSummaryAPIActions.setPermissionSummary(tradePermission);
    } catch (err) {
      // console.error('Failed to parse product message', err);
    }
  }

  public isConnected(): boolean {
    return this.socketProvider.isConnected();
  }

  public sendMessage(topic: string, messageType: MessageType, messageAction: MessageAction, messageBody: string) {
    
    this.socketProvider.sendMessage(topic, messageType, messageAction, messageBody);
  }

  private handleCloseEvent() {
    this.disconnect.next(false);
    this.close();
    this.toastService.connectionLostMessage();
  }
}
