import { Injectable, ViewContainerRef } from '@angular/core';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Injectable()
export class ToastService {

  constructor(public toastr: ToastsManager) { }

  sessionTerminationMessage() {
    this.toastr.warning('You have been logged out due to a connection problem, please try to log in again',
      'Session Termination Notification');
  }

  connectionLostMessage() {
    this.toastr.error('Please try to log in again', 'AOM Connection Lost');
  }

  showMarketCloseNotification(closedMarkets: string[]) {
    let formattedclosedMarkets = '';
    if ( closedMarkets instanceof Array) {
      formattedclosedMarkets = closedMarkets.reduce((acc, cur, index, array): string => {
            if (index === 0) {
              acc = '['.concat(cur);
              return acc;
            } else if (index < array.length - 1 ) {
              acc = acc.concat(',', cur);
              return acc;
            } else if (index < 2 ) {
              acc = acc.concat(',', cur, ']');
              return acc;
            } else {
              acc = acc.concat(cur, ']');
              return acc;
            }
            } , '[]'
      );
    }
    const formattedNotification = 'The following market(s): ' .concat(formattedclosedMarkets,
                                  ' are currently closed. Any bids or asks created whilst the market is closed will be placed into a held state. You will need to manually reinstate these bids/asks when the market reopens.');
    this.toastr.info(formattedNotification, 'User Notification', { timeout: 3000,
                                                                   closeButton: true,
                                                                   positionClass: 'toast-bottom-full-width'
                                                                 });
  }
}
