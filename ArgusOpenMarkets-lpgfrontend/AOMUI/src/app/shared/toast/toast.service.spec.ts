import { TestBed, inject } from '@angular/core/testing';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ToastService } from './toast.service';

describe('ToastService', () => {

  const toastManagerStub = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ToastService,
        { provide: ToastsManager, useValue: toastManagerStub }
      ]
    });
  });

  it('should be created', inject([ToastService], (service: ToastService) => {
    expect(service).toBeTruthy();
  }));
});
