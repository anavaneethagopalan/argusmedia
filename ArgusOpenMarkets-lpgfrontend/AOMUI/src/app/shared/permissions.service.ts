import { Injectable } from '@angular/core';
import { select } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';

import { IAppState } from '../store/model';
import { IOrder } from '../api/orders/models';
import { IUser } from '../api/models/user/user';
import { IPermissionSummary } from '../api/permissions-summary/models';
import { IProductDefinition } from '../products/models/product';

@Injectable()
export class PermissionsService {

  @select(['webapi_login_session', 'session', 'User']) readonly user$: Observable<IUser>;
  @select() private readonly permissionsSummary$: Observable<IPermissionSummary[]>;

  constructor() { }  
  
  canCreateOrder$(productId: number): Observable<boolean> {
    let privilegeNameArray = ["order_Create_Any", "order_Create_Broker", "order_Create_Principal"];
    return this.user$.map(user => {
      let productPrivileges = user.productPrivileges.find(priv => priv.productId === productId).privileges;
      for (let i = 0; i < privilegeNameArray.length; i++) {
        if (productPrivileges[privilegeNameArray[i]]) {
          return true;
        }
      }
      return false;
    });
  }

  isOrderMine(order: IOrder, user: IUser): boolean {
    return user.organisationId == order.principalOrganisationId || user.organisationId == order.brokerOrganisationId;
  }
  
  isOrderMine$(order: IOrder): Observable<boolean> {
    if(order) {
      return this.user$.map(user => this.isOrderMine(order, user));
    }
    return Observable.of(false);
  }
  
  isNonTradeable$(order: IOrder, product: IProductDefinition): Observable<boolean> {
    return this.user$.switchMap(user => {
      let agressBrokerPrivilege = user.productPrivileges["order_Aggress_Broker"];
      if(agressBrokerPrivilege && this.isOrderMine(order, user)) {
        return Observable.of(false);   
      }
      else {
        return this.canTrade$(product, order, user).map(_ => !_);
      }
    });
  }

  isBrokerRestricted$(order: IOrder): Observable<boolean> {
    return this.user$.map(user => user.userOrganisation.isBroker && order.brokerRestriction === 'None');
  }

  isCoBrokered$(order: IOrder): Observable<boolean> {
    return this.user$.map(user => user.userOrganisation.isBroker && order.coBrokering);
  }

  public canTrade$(product: IProductDefinition, order: IOrder, user: IUser): Observable<boolean> { 
    return this.permissionsSummary$.map(permissions => {
      let buyOrSell: 'buy' | 'sell' = order.orderType === "ASK" ? "sell" : "buy";
      // invert as you are doing the opposite to what is on the order -> if they are selling you are buying
      buyOrSell = buyOrSell === "sell" ? "buy" : "sell";
      let coBrokeredOrder = order.coBrokering && product.coBrokering;

      if(user.userOrganisation.isBroker) {
          if(coBrokeredOrder || (!coBrokeredOrder && !order.brokerOrganisationId)){
            return this.checkTradePermissions(permissions, product.productId, 'BrokerPermission', buyOrSell, order.principalOrganisationId);
          }
          else {
            return false;
          }
      }
      else {
        let canTradeWithBroker = !order.brokerOrganisationId 
          ? true 
          : this.checkTradePermissions(permissions, product.productId, 'BrokerPermission', buyOrSell, order.brokerOrganisationId);
        let canTradeWithCounterparty = this.checkTradePermissions(permissions, product.productId, 'CounterpartyPermission', buyOrSell, order.principalOrganisationId);
        return canTradeWithBroker && canTradeWithCounterparty;
      }
    });
  }

  private checkTradePermissions(permissions: IPermissionSummary[], productId: number, permissionType: 'BrokerPermission' | 'CounterpartyPermission', 
    buyOrSell: 'buy' | 'sell', organisationId: number): boolean {
      return permissions.findIndex(permission => 
        permission.productId === productId 
          && permission.permissionType === permissionType
          && permission.buyOrSell === buyOrSell
          && permission.organisationId === organisationId) > -1;
  }
}
