import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { IAppState } from '../store/model';
import { Message } from '../models/message';
import { UserService } from './user.service';

@Injectable()
export class LoggerService {

  constructor(private userService: UserService, private store: NgRedux<IAppState>) { }

  public info(message: string): void {
    this.sendClientLogMessage('INFO: ' + message);
  }

  public warn(message: string): void {
    this.sendClientLogMessage('WARN: ' + message);
  }

  public error(message: string): void {
    this.sendClientLogMessage('ERROR: ' + message);
  }

  private sendClientLogMessage(message: string): void {
    const userId = this.getUserIdFromStore();
    const clientLog = {
        userId: userId,
        message: message
    };
    this.userService.sendClientLog(clientLog);
  }

  getUserIdFromStore() {
    return this.store.getState().webapi_login_session.session.userId;
  }
}
