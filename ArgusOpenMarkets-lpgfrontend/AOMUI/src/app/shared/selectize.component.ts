import { Component, Input, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import * as $ from 'jquery';
import 'selectize';

@Component({
    selector: 'ng2-selectize-fixed',
    template: '<ng2-selectize [name]="name" [options]="options" [config]="_config" [placeholder]="placeholder" [disabled]="disabled" required ngModel></ng2-selectize>',
    providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => SelectizeComponent),
          multi: true,
        }
    ]
})
/**
 * Wrapper for original <ng2-selectize> component, which has a bug: ngModel is not notified when an element is deleted, thus creating validation issues.
*/
export class SelectizeComponent implements OnInit, ControlValueAccessor {
    
    @Input()
    public id : string;

    public _config : any;

    @Input()
    public config : any;

    @Input()
    public name : string;

    @Input()
    public options : any[];

    @Input()
    public placeholder : string;

    public disabled : boolean;

    private propagateChange = (_: any) => { };

    constructor() {       
    }

    ngOnInit() {
        let parent = this;
        this._config = {
            ...this.config,
            onItemAdd: function(value: any, $item: any) {
                parent.propagateChange([...this.items]);
            },
            onItemRemove: function(value: any) {
                parent.propagateChange([...this.items]);
            }
        }
    }

    //#region ControlValueAccessor interface implementation
    writeValue(obj: any): void {
        if(obj) {
            this.options = obj;
        }
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
        
    }

    setDisabledState?(isDisabled: boolean): void {
        this.disabled == isDisabled;
    }
    //#endregion
}