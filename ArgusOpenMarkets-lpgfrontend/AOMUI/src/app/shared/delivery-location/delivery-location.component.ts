import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { DeliveryLocationDropdown } from '../../api/create-order/models';

@Component({
  selector: 'delivery-location',
  templateUrl: './delivery-location.component.html',
  styleUrls: ['./delivery-location.component.scss']
})
export class DeliveryLocationComponent implements OnInit {

  @Input() public deliveryLocationConfig: DeliveryLocationDropdown;
  @Input() public parentForm: FormGroup;

  constructor() { }

  ngOnInit() {
    console.log('deliveryLocationConfig', this.deliveryLocationConfig);
  }

}
