import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { IAppState } from '../store/model';
import { setProductAssessment, setProductContentStreams, setProductDefinition, setProductMarketStatus, setProductMetaData } from '../store/actions/products';
import { ProductMessage } from '../models/product-message';
import { MessageDistributionService } from './message-distribution.service';

@Injectable()
export class ProductService {

  constructor(private messageDistributionService: MessageDistributionService, private store: NgRedux<IAppState>) {
    this.messageDistributionService.productTopicMessages$.subscribe((productMessage) => this.productMessageRecieved(productMessage));
  }

  public productMessageRecieved(productMessage: ProductMessage) {
    switch (productMessage.productTopicType) {
      case 'Assessment':
        this.store.dispatch(setProductAssessment(productMessage.productId, productMessage.content));
        break;
      case 'ContentStream':
        this.store.dispatch(setProductContentStreams(productMessage.productId, productMessage.content));
        break;
      case 'Definition':
        this.store.dispatch(setProductDefinition(productMessage.productId, productMessage.content));
        break;
      case 'MarketStatus':
        this.store.dispatch(setProductMarketStatus(productMessage.productId, productMessage.content));
        break;
      case 'MetaData':
        this.store.dispatch(setProductMetaData(productMessage.productId, productMessage.content));
        break;
      default:
        console.log('Unknown product message type', productMessage.productTopicType);
        break;
    }
  }

  public getProduct(productId: number) {
    return this.store.getState().products.find((p) => p.productId === productId);
  }

  public get products$() {
    return this.store.select((store) => store.products);
  }
}
