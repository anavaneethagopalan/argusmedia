import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';

import { TradersComponent } from './traders.component';

describe('TradersComponent', () => {
  let component: TradersComponent;
  let fixture: ComponentFixture<TradersComponent>;
  let traderConfig =  {
    coBrokeringEnabled: false, 
    traders: []
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ TradersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradersComponent);
    component = fixture.componentInstance;
    component.parentForm = new FormGroup({
      trader: new FormControl('')
    });
    component.traderConfig = traderConfig;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not show the co brokering flag if coBrokering not enabled', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#allowOtherBrokers')).toBeFalsy();
  });

  it('should show the co brokering flag if coBrokering is enabled', () => {
    component.traderConfig = {
      coBrokeringEnabled: true, 
      traders: []
    };
    fixture.detectChanges();


    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('#allowOtherBrokers')).toBeTruthy();
  });

  it('should show the co brokering flag and set to enabled if co brokering enabled and the logged on user is a broker', () => {
    let traders = [{
      "organisation": {
        "id": -1,
        "name": "Any Broker",
        "organisationType": 4,
        "shortCode": "",
        "legalName": "",
        "brokerRestriction": 1
      },
      "canTrade": true
    }];

    component.traderConfig = {
      coBrokeringEnabled: true, 
      traders: traders
    };
    fixture.detectChanges();
    

    component.parentForm.controls['trader'].setValue(traders[0]);
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    var selectedTrader = compiled.querySelector('#listTraders');
    var coBrokerCheckBox = compiled.querySelector('#allowOtherBrokers')

    expect(coBrokerCheckBox.disabled).toBeFalsy();
  });

  it('should show the co brokering flag and set to disabled if co brokering enabled and the logged on user is a trader with Any Broker selected', () => {
    let traders = [{
      "organisation": {
        "id": -1,
        "name": "Any Broker",
        "organisationType": 3,
        "shortCode": "",
        "legalName": "",
        "brokerRestriction": 1
      },
      "canTrade": true
    }];

    component.traderConfig = {
      coBrokeringEnabled: true, 
      traders: traders
    };
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    var tradersDropDown = compiled.querySelector('#listTraders');

    let selectedTrader = traders[0];
    component.parentForm.controls['trader'].setValue(selectedTrader);
    fixture.detectChanges();

    var coBrokerCheckBox = compiled.querySelector('#allowOtherBrokers')

    expect(coBrokerCheckBox.disabled).toBeTruthy();
  });
});
