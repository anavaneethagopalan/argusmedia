import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ITraderDropDown, ITraderConfig } from '../../api/create-order/models';

@Component({
  selector: 'traders',
  templateUrl: './traders.component.html',
  styleUrls: ['./traders.component.scss']
})
export class TradersComponent implements OnInit {
  
  @Input() public traderConfig: ITraderConfig;
  @Input() public parentForm: FormGroup;

  constructor() { }

  ngOnInit() {
  }

  coBrokeringFlagDisabled(): boolean{

    if(! this.traderConfig.coBrokeringEnabled){
      // Co brokering is not enabled 
      return true;
    }
    // TODO: Leave co-brokering for now - not required until post demo.
    let selectedTrader = this.parentForm.get('trader');

    if(selectedTrader){
      let traderValue = selectedTrader.value;

      if(traderValue.organisation){

        switch(traderValue.organisation.organisationType){
          case 1: 
            // Not specified
            return true;
          case 2: 
            // Argus
            return true;
          case 3:
            // Trade Oprganisation
            if(traderValue.organisation.brokerRestriction === 3){
              // This is a specific broker selected - we can enable Can Trade.
              return false;
            } else{
              return true;
            }
          case 4:
            // Organisation Type - Brokerage - we can enable Co-Brokering.
            return false;
          
        }
      } 
    }
  }

}
