export const LOAD_PLATFORM = 'LOAD_PLATFORM';

export function loadPlatform(platform: Platform) {
    return {
        type: LOAD_PLATFORM,
        payload: platform
    };
}