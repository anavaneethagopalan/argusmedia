import { Config } from '../../models/config';

export const LOAD_CONFIG = 'LOAD_CONFIG';

export function loadConfig(config: Config) {
    return {
        type: LOAD_CONFIG,
        payload: config
    };
}