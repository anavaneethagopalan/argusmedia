import { ConnectionStatus } from '../../models/connection-status';

export const SET_CONNECTION_STATUS = 'SET_CONNECTION_STATUS';

export function setConnectionStatus(connectionStatus: ConnectionStatus) {
    return {
        type: SET_CONNECTION_STATUS,
        payload: connectionStatus
    };
}

