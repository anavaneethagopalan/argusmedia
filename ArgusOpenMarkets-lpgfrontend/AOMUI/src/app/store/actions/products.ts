export const SET_PRODUCT_ASSESSMENT = 'SET_PRODUCT_ASSESSMENT';
export const SET_PRODUCT_CONTENT_STREAMS = 'SET_PRODUCT_CONTENT_STREAMS';
export const SET_PRODUCT_DEFINITION = 'SET_PRODUCT_DEFINITION';
export const SET_PRODUCT_MARKET_STATUS = 'SET_PRODUCT_MARKET_STATUS';
export const SET_PRODUCT_METADATA = 'SET_PRODUCT_METADATA';

export function setProductAssessment(productId: number, assessment: any) {
    return {
        type: SET_PRODUCT_ASSESSMENT,
        productId,
        payload: assessment
    };
}

export function setProductContentStreams(productId: number, contentStreams: any) {
    return {
        type: SET_PRODUCT_CONTENT_STREAMS,
        productId,
        payload: contentStreams
    };
}

export function setProductDefinition(productId: number, definition: any) {
    return {
        type: SET_PRODUCT_DEFINITION,
        productId,
        payload: definition
    };
}

export function setProductMarketStatus(productId: number, marketStatus: any) {
    return {
        type: SET_PRODUCT_MARKET_STATUS,
        productId,
        payload: marketStatus
    };
}

export function setProductMetaData(productId: number, metaData: any) {
    return {
        type: SET_PRODUCT_METADATA,
        productId,
        payload: metaData
    };
}
