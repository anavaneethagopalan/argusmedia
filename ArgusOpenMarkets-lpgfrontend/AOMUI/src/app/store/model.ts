import { Observable } from 'rxjs/Observable';

import { IMarketStatus } from '../products/models/marketstatus/market-status';
import { IProductMetaData } from '../products/models/metadata/product-meta-data';
import { ConnectionStatus } from '../models/connection-status';
import { IPermissionSummary } from '../api/permissions-summary/models';
import { Config } from '../models/config';
import { IOrganisationSystemNotificationType } from '../models/organisation-system-notification-type';
import { IOrganisationSystemNotificationMapping } from '../models/organisation-system-notification-mapping';
import { OrganisationSystemNotification } from '../models/organisation-system-notification';
import { ILoginSessionState } from '../api/session/models';
import { IForgottenPasswordSessionState } from '../api/forgotten-password/models';
import { IResetPasswordSessionState } from '../api/reset-password/models';
import { INewsState } from '../api/news/models';
import { IProduct, IProductDefinition } from '../products/models/product';
import { IOrder, IPriceBasis } from '../api/orders/models';
import { ICreateOrderState } from '../api/create-order/models';


export interface IAppState {
    connectionStatus: ConnectionStatus;
    products: IProduct[];
    news: INewsState;
    config: Config;
    platform: Platform;
    permissionsSummary: IPermissionSummary[];
    webapi_login_session: ILoginSessionState;
    webapi_forgotten_password_session: IForgottenPasswordSessionState;
    webapi_reset_password_session: IResetPasswordSessionState;
    organisationSystemNotificationTypes: IOrganisationSystemNotificationType[];
    organisationSystemNotifications: IOrganisationSystemNotificationMapping[];
    priceBases: IPriceBasis[];
    orders: IOrder[];
    createOrderState: ICreateOrderState;
}

//AOT is not compatible with @select expressions
export function selectConnectionStatus(store: IAppState): string {
    return ConnectionStatus[store.connectionStatus];
}

export function selectProductDefinitions(store: IAppState): IProductDefinition[]
{
  return store.products.map((product) => product.definition);
}

export function selectProductDefinitionsSorted(store: IAppState): IProductDefinition[] {
    return selectProductDefinitions(store).sort((p1, p2) => p1.productName.toLocaleLowerCase().localeCompare(p2.productName.toLocaleLowerCase()));
}

// export function selectProductDefinitionsWithTenorsSorted(store: IAppState): IProductDefinition[]
// {
//     return selectProductDefinitions(store).map((productDefinition) => { 
//         return {
//             ...productDefinition,
//             productTenors: productDefinition.contractSpecification.productTenors.sort((p1, p2) => p2.deliveryDateEnd.valueOf() - p1.deliveryDateEnd.valueOf())
//         }
//     });
// }

export function selectOrganisationNotifications(store: IAppState):OrganisationSystemNotification[]  {
    return store.organisationSystemNotificationTypes.map((osnt) => {
        return {
            ...osnt,
            mappings: store.organisationSystemNotifications.filter((osn) => osn.eventType == osnt.id)
        };
    });
}

