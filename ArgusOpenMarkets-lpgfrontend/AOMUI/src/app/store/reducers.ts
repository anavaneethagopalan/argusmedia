import { routerReducer } from '@angular-redux/router';
import { composeReducers, defaultFormReducer } from '@angular-redux/form';

import { combineReducers } from 'redux';

import { IAppState } from './model';
import { config } from './reducers/config';
import { connectionStatus } from './reducers/connection-status';
import { products } from './reducers/products';
import { news } from '../api/news/reducers';
import { platform } from './reducers/platform';
import { createLoginSessionAPIReducer } from '../api/session/reducer';
import { forgottenPasswordAPIReducer } from '../api/forgotten-password/reducer';
import { resetPasswordAPIReducer } from '../api/reset-password/reducer';
import { organisationSystemNotificationTypes, organisationSystemNotifications } from '../settings/company-settings/reducers';
import { orders, priceBases } from '../api/orders/reducers';
import { createOrderAPIReducer } from '../api/create-order/reducer';
import { permissionsSummary } from '../api/permissions-summary/reducers';

export const rootReducer = composeReducers(
    defaultFormReducer(),
    combineReducers({
        connectionStatus,
        products,
        news,
        config,
        platform,
        permissionsSummary,
        webapi_login_session: createLoginSessionAPIReducer(),
        webapi_forgotten_password_session: forgottenPasswordAPIReducer,
        webapi_reset_password_session: resetPasswordAPIReducer,
        router: routerReducer,
        organisationSystemNotificationTypes,
        organisationSystemNotifications,
        orders,
        priceBases,
        createOrderState: createOrderAPIReducer
    })
);