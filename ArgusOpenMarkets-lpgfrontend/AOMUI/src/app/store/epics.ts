import { Injectable } from '@angular/core';

import { SessionAPIEpics } from '../api/session/epics';
import { ForgottenPasswordEpic } from '../api/forgotten-password/epics';
import { ResetPasswordEpic } from '../api/reset-password/epics';
import { NewsEpic } from '../api/news/epics';
import { CreateOrderEpic } from '../api/create-order/epics';

@Injectable()
export class RootEpics {
    constructor(private sessionEpics: SessionAPIEpics, private forgottenPasswordEpic: ForgottenPasswordEpic,
        private resetPasswordEpic: ResetPasswordEpic, private newsEpic: NewsEpic, private createOrderEpic: CreateOrderEpic) {
    }

    public createEpics() {
        return [
            this.sessionEpics.createLoginEpic(),
            this.sessionEpics.createLogoutEpic(),
            this.forgottenPasswordEpic.createEpic(),
            this.resetPasswordEpic.createEpic(),
            this.newsEpic.createEpic(),
            this.createOrderEpic.createEpic()
        ];
    }
}
