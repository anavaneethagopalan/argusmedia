import { NgModule } from '@angular/core';
import { provideReduxForms } from '@angular-redux/form';
import { NgReduxRouter, NgReduxRouterModule } from '@angular-redux/router';
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';

import { createLogger } from 'redux-logger';

import { ConnectionStatus } from '../models/connection-status';
import { IAppState } from './model';
import { rootReducer } from './reducers';
import { RootEpics } from './epics';

import { IOrder, IOrderPriceLine, IPriceLine, IPriceBasis } from '../api/orders/models';

const priceBasesTest: IPriceBasis[] = [{
  id: 1,
  shortName: 'FIXED',
  name: 'Fixed'
},
{
  id: 2,
  shortName: 'CP',
  name: 'CP'
}];

const testOrders : IOrder[] = [{
  id: 1,
  orderStatus: 'A',
  lastUpdated: new Date(Date.now()),
  orderType: 'BID',
  quantity: 27000,
  notes: 'test notes',
  principalOrganisationId: 2,
  principalOrganisationShortCode: 'BP',
  brokerOrganisationId: 6,
  brokerShortCode: 'GIN',
  brokerOrganisation: 'Gintama',
  brokerRestriction: 'None',
  coBrokering: false,
  enteredByUserId: '5062',
  productId: 13, //shouldn't be here
  productTenors: null,
  orderPriceLines: [{
    id: 1,
    orderId: 1,
    orderStatusCode: 'A',
    priceLine: {
      id: 1,
      sequenceNo: 0,
      priceLinesBases: [{
        id: 1,
        sequenceNo: 0,
        description: 'price line basis 1',
        percentageSplit: 40,
        pricingPeriodFrom: new Date(Date.now()),
        pricingPeriodTo: new Date(Date.now()),
        priceLineBasisValue: 123000,
        priceBasisId: 1
      },
      {
        id: 2,
        sequenceNo: 1,
        description: 'price line basis 2',
        percentageSplit: 60,
        pricingPeriodFrom: new Date(Date.now()),
        pricingPeriodTo: new Date(Date.now()),
        priceLineBasisValue: 132000,
        priceBasisId: 2
      }]
    }
  },
  {
    id: 2,
    orderId: 1,
    orderStatusCode: 'A',
    priceLine: {
      id: 2,
      sequenceNo: 1,
      priceLinesBases: [{
        id: 3,
        sequenceNo: 0,
        description: 'price line basis 1',
        percentageSplit: 25,
        pricingPeriodFrom: new Date(Date.now()),
        pricingPeriodTo: new Date(Date.now()),
        priceLineBasisValue: 543000,
        priceBasisId: 1
      },
      {
        id: 4,
        sequenceNo: 1,
        description: 'price line basis 2',
        percentageSplit: 75,
        pricingPeriodFrom: new Date(Date.now()),
        pricingPeriodTo: new Date(Date.now()),
        priceLineBasisValue: 345000,
        priceBasisId: 1
      }]
    }
  }]
},
{
  id: 2,
  orderStatus: 'A',
  lastUpdated: new Date(Date.now()),
  orderType: 'BID',
  quantity: 27000,
  notes: 'test notes',  
  principalOrganisationId: 2,
  principalOrganisationShortCode: 'BP',
  brokerOrganisationId: 3,
  brokerShortCode: 'GIN',
  brokerOrganisation: 'Gintama',
  brokerRestriction: 'A',
  coBrokering: false,
  enteredByUserId: '5062',
  productId: 14, //shouldn't be here
  productTenors: null,
  orderPriceLines: [{
    id: 1,
    orderId: 1,
    orderStatusCode: 'A',
    priceLine: {
      id: 1,
      sequenceNo: 0,
      priceLinesBases: [{
        id: 1,
        sequenceNo: 0,
        description: 'price line basis 1',
        percentageSplit: 40,
        pricingPeriodFrom: new Date(Date.now()),
        pricingPeriodTo: new Date(Date.now()),
        priceLineBasisValue: 123000,
        priceBasisId: 1
      },
      {
        id: 2,
        sequenceNo: 1,
        description: 'price line basis 2',
        percentageSplit: 60,
        pricingPeriodFrom: new Date(Date.now()),
        pricingPeriodTo: new Date(Date.now()),
        priceLineBasisValue: 132000,
        priceBasisId: 2
      }]
    }
  },
  {
    id: 2,
    orderId: 1,
    orderStatusCode: 'A',
    priceLine: {
      id: 2,
      sequenceNo: 1,
      priceLinesBases: [{
        id: 3,
        sequenceNo: 0,
        description: 'price line basis 3',
        percentageSplit: 25,
        pricingPeriodFrom: new Date(Date.now()),
        pricingPeriodTo: new Date(Date.now()),
        priceLineBasisValue: 543000,
        priceBasisId: 1
      },
      {
        id: 4,
        sequenceNo: 1,
        description: 'price line basis 4',
        percentageSplit: 75,
        pricingPeriodFrom: new Date(Date.now()),
        pricingPeriodTo: new Date(Date.now()),
        priceLineBasisValue: 345000,
        priceBasisId: 1
      }]
    }
  }]
}];

const INITIAL_STATE: IAppState = {
  connectionStatus: ConnectionStatus.Disconnected,
  products: [],
  news: { newsStories: [], newsStory: null },
  config: null,
  platform: null,
  permissionsSummary: [],
  webapi_login_session: null,
  webapi_forgotten_password_session: null,
  webapi_reset_password_session: null,
  organisationSystemNotificationTypes: [],
  organisationSystemNotifications: [],
  priceBases: priceBasesTest,
  orders: testOrders,
  createOrderState: null
};

@NgModule({
  imports: [NgReduxModule, NgReduxRouterModule],
  providers: [RootEpics]
})
export class StoreModule {
  constructor(
    public store: NgRedux<IAppState>,
    devTools: DevToolsExtension,
    ngReduxRouter: NgReduxRouter,
    rootEpics: RootEpics
  ) {
    store.configureStore(
      rootReducer,
      INITIAL_STATE,
      [createLogger(), ...rootEpics.createEpics()],
      devTools.isEnabled() ? [devTools.enhancer()] : []
    );

    // Enable syncing of Angular router state with the Redux store
    if (ngReduxRouter) {
      ngReduxRouter.initialize();
    }

    // Enable syncing of Angular form state with the Redux store
    provideReduxForms(store);
  }
}
