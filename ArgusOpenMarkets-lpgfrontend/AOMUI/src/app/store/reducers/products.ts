import { IProduct } from '../../products/models/product';
import {
    SET_PRODUCT_ASSESSMENT,
    SET_PRODUCT_CONTENT_STREAMS,
    SET_PRODUCT_DEFINITION,
    SET_PRODUCT_MARKET_STATUS,
    SET_PRODUCT_METADATA
} from '../actions/products';

function updateProductProperty(state: IProduct[], action, propertyName: string) {
    if (state.some((product) => product.productId === action.productId)) {
        return state.map((product) => {
            if (product.productId === action.productId) {
                return {
                    ...product,
                    [propertyName]: action.payload
                };
            }
            return product;
        });
    } else {
        return [...state, { productId: action.productId, [propertyName]: action.payload }];
    }
}

/*
* As LastCloseDate is displayed in user's local time (potentially +1 day), openTime/closeTime must be parsed from JSON as a date object and converted to local time as well
*/
function updateProductDefinition(state: IProduct[], action) {
    if (state.some((product) => product.productId === action.productId)) {
        return state.map((product) => {
            if (product.productId === action.productId) {
                return {
                    ...product,
                    definition: {
                        ...product.definition,
                        ...action.payload,
                        openTime: Date.UTC(2017, 0, 1, ...action.payload.openTime.split(':')),
                        closeTime: Date.UTC(2017, 0, 1, ...action.payload.closeTime.split(':'))
                    }
                };
            }
            return product;
        });
    } else {
        return [...state, { 
            productId: action.productId, 
            definition: {
                ...action.payload,
                openTime: Date.UTC(2017, 0, 1, ...action.payload.openTime.split(':')),
                closeTime: Date.UTC(2017, 0, 1, ...action.payload.closeTime.split(':'))
            }
        }];
    }
}

export function products(state: IProduct[] = [], action) {
    switch (action.type) {
        case SET_PRODUCT_ASSESSMENT:
            return updateProductProperty(state, action, 'assessment');
        case SET_PRODUCT_CONTENT_STREAMS:
            return updateProductProperty(state, action, 'contentStreams');
        case SET_PRODUCT_DEFINITION:
            return updateProductDefinition(state, action);
        case SET_PRODUCT_MARKET_STATUS:
            return updateProductProperty(state, action, 'marketStatus');
        case SET_PRODUCT_METADATA:
            return updateProductProperty(state, action, 'metaData');
        default:
            return state;
    }
}
