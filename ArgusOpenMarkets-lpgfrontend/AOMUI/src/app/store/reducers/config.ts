import { Config } from '../../models/config';
import { LOAD_CONFIG } from '../actions/config';

export function config(state: Config = null, action) {
    switch (action.type) {
        case LOAD_CONFIG:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
}