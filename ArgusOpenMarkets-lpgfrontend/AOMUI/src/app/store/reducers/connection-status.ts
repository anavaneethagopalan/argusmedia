import { ConnectionStatus } from '../../models/connection-status';
import { SET_CONNECTION_STATUS } from '../actions/connection-status';

export function connectionStatus(state: ConnectionStatus = ConnectionStatus.Disconnected, action) {
    switch (action.type) {
        case SET_CONNECTION_STATUS:
            return action.payload;
        default:
            return state;
    }
}