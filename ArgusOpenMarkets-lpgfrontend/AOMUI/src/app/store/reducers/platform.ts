import { LOAD_PLATFORM } from '../actions/platform';

export function platform(state: Platform = {}, action) {
    switch (action.type) {
        case LOAD_PLATFORM:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
}