import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgRedux, DevToolsExtension, } from '@angular-redux/store';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/lib/testing';

import { BsModalService, BsDatepickerModule } from 'ngx-bootstrap';

import { DashboardComponent } from './dashboard.component';
import { IAppState } from '../store/model';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  const mockNgRedux = MockNgRedux.getInstance();
  const bsModalServiceStub = {
    show() { }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgReduxTestingModule, BsDatepickerModule.forRoot()],
      declarations: [DashboardComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: NgRedux, useValue: mockNgRedux },
        { provide: BsModalService, useValue: bsModalServiceStub }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    const productsStub = MockNgRedux.getSelectorStub('products');
    productsStub.next([]);
    productsStub.complete();

    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
