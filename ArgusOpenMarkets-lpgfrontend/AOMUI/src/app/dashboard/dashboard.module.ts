import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { PendingChangesGuard } from '../guards/pending-changes.guard';
import { AssessmentComponent } from '../assessment/assessment.component';
import { NewsComponent } from '../news/news.component';
import { NewsService } from '../api/news/service';
import { SafeHtmlPipe } from '../shared/safe-html.pipe';
import { DateTimePipe } from '../shared/date-time.pipe';


import { BidAskModule } from '../bid-ask/bid-ask.module';

import { AccordionModule, AlertModule, ButtonsModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    BidAskModule
  ],
  providers: [
    PendingChangesGuard,
    NewsService
  ],
  declarations: [
    DashboardComponent,
    AssessmentComponent,
    NewsComponent,
    SafeHtmlPipe,
    DateTimePipe
    
  ]
})
export class DashboardModule { }
