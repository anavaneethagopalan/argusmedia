import { Component, OnInit, TemplateRef, HostListener, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { select, select$ } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { delay } from 'rxjs/operator/delay';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/Rx';
import { IAppState } from '../store/model';

// for the modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ProductPrivilege } from '../products/models/user/product-privilege';
import { IProduct } from '../products/models/product';

@Component({
  selector: 'aom-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {
  public modalRef: BsModalRef;
  public config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false
  };

  // datepicker
  minDate: Date;
  maxDate: Date;
  datepickerModel: Date;

  // radio as button set
  public singleModel = '1';

  @select(['webapi_login_session', 'session', 'User', 'productPrivileges']) productPrivileges$: Observable<ProductPrivilege[]>;
  public productsWithAssessment$: Observable<ProductPrivilege[]> = Observable.of([]);
  @select('products') products$: Observable<IProduct[]>;
  public productWithDefinition$: Observable<any[]> = Observable.of([]);

  constructor(private modalService: BsModalService) {
    window.onbeforeunload = function (e) {
      return 'Unsaved changes will be lost!.';
    };

    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() - 1);
  }

  ngOnInit(): void {
    this.productsWithAssessment$ = this.productPrivileges$.map(_ => _.filter(prod_priviledges => prod_priviledges.hasAssessment))
      .startWith([]);
    // this.productWithDefinition$ = this.products$.map(_ => _.filter(prod => prod.definition));
  }


  canDeactivate() {
    if (1 !== 1) {
      return window.confirm('Discard changes?');
    } else {
      return true;
    }
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  // This message is displayed to the user in IE and Edge when they navigate without using Angular routing (type another URL/close the browser/etc)
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    console.log($event);
    // if (!this.canDeactivate()) {
    $event.returnValue = 'Changes that you made may not be saved.';
    // }
  }
}
