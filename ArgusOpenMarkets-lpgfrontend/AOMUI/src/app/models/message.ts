export class Message {
    public messageType: string;
    public messageAction: string;
    public messageBody: any;
}

export enum MessageType{
    Assessment,
    Deal,
    Error,
    ExternalDeal,
    Info,
    Logoff,
    MarketTickerItem,
    News,
    Order,
    Product,
    User,
    UserMessage,
    BrokerPermission,
    CounterpartyPermission,
    BrokerPermissionDetail,
    CounterpartyPermissionDetail,
    OrganisationNotificationDetail, 
    TopicSubscription,
    ClientLog
}

export enum MessageAction{
    Create,
    Execute,
    Hold,
    Kill,
    Register,
    Refresh,
    Reinstate,
    Request,
    Update,
    Void,
    Verify,
    ConfigurationChange,
    Open,
    Close,
    Purge,
    ConfigurePurge,
    Send,
    Check,
    Delete,
    List,
    Clear
}