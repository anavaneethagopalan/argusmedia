export interface ICompanyDetailsMessage {
    // id: number;
    eventType: number;
    productId: number;
    emailAddress: string;
}