export interface IOrganisationSystemNotificationMapping {
    id: number;
    emailAddress: string;
    eventType: number;
    productId: number;
    organisationId: number;
    userId: number;
}

export class OrganisationSystemNotificationMappingMessage {
    public id: number;
    public emailAddress: string;
    public eventType: number;
    public productId: number;
    public organisationId: number;
    public userId: number;
}