import { IOrganisationSystemNotificationType } from './organisation-system-notification-type';
import { IOrganisationSystemNotificationMapping } from './organisation-system-notification-mapping';

export class OrganisationSystemNotification implements IOrganisationSystemNotificationType {
    public id: number;
    public description: string;
    mappings: IOrganisationSystemNotificationMapping[];
}