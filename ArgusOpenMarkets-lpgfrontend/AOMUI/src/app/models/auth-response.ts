export class AuthResponse {
    public credentialType: string;
    public isAuthenticated: boolean;
    public message: string;
    public sessionToken: string;
    public token: string;
    public userId: number;
    public user: any;
}
