export enum ConnectionStatus {
    Connected,
    Reconnected,
    Disconnected,
    Closed
}
