

export class Config {
    public protocol: string;
    public endpoint: string;
    public port: number;
    public apiPath: string;
    public diffusionHost: string;
    public diffusionPort: number;
    public diffusionSecure: boolean;
    public version: string;
    public webApi: string;    
    public bidAskStackStep: number;
} 

