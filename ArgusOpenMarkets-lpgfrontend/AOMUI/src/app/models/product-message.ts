export class ProductMessage {
    public productId: number;
    public productTopicType: string;
    public content: any;
}
