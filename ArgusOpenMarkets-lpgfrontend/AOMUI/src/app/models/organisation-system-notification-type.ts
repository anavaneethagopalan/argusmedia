export interface IOrganisationSystemNotificationType {
    id: number;
    description: string;
}

export class OrganisationSystemNotificationTypeMessage {
    public id: number;
    public description: string;
}