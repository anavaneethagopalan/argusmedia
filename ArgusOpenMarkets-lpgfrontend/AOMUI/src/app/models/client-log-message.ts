export interface ClientLogMessage {
    userId: number,
    message: string
};