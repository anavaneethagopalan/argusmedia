import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgRedux } from '@angular-redux/store';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/lib/testing';
import { By } from '@angular/platform-browser';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/skipWhile';
import 'rxjs/add/operator/startWith';

import { AssessmentComponent } from './assessment.component';

describe('AssessmentComponent', () => {
  let component: AssessmentComponent;
  let fixture: ComponentFixture<AssessmentComponent>;

  const PRODUCT_PRIVILEGE = {
    productId: 1,
    displayOrder: 0,
    hasAssessment: true,
    name: 'TEST PRODUCT',
    privileges: null
  };

  const ASSESSMENT = {
    today: {
      assessmentStatus: 'NONE',
      businessDate: new Date(2016, 2, 18),
      priceHigh: 10,
      priceLow: 0
    },
    previous: {
      assessmentStatus: 'FINAL',
      businessDate: new Date(2016, 2, 17),
      priceHigh: 123,
      priceLow: 4
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssessmentComponent],
      imports: [NgReduxTestingModule],
    }).compileComponents()
      .then(() => {
        MockNgRedux.reset();
      });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentComponent);
    component = fixture.componentInstance;
    component.product = PRODUCT_PRIVILEGE;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('panel heading should be product name', () => {
    const debugElement = fixture.debugElement.query(By.css('#assessment-heading'));
    expect(debugElement.nativeElement.textContent).toBe(PRODUCT_PRIVILEGE.name);
  });

  it('should display assessment info when returned from store', () => {
    const stub = MockNgRedux.getSelectorStub('products');
    stub.next([{
      productId: PRODUCT_PRIVILEGE.productId,
      marketStatus: null,
      definition: null,
      contentStreams: null,
      assessment: ASSESSMENT,
      metaData: null
    }]);
    stub.complete();
    fixture.detectChanges();

    const assertAssessmentElement = (elementId, expectedValue) => {
      const debugElement = fixture.debugElement.query(By.css(elementId));
      expect(debugElement.nativeElement.textContent).toBe(expectedValue);
    };

    assertAssessmentElement('#today-assessment-status', ASSESSMENT.today.assessmentStatus);
    assertAssessmentElement('#today-business-date', '18-Mar');
    assertAssessmentElement('#today-price-low', ASSESSMENT.today.priceLow.toString());
    assertAssessmentElement('#today-price-high', ASSESSMENT.today.priceHigh.toString());
    assertAssessmentElement('#previous-assessment-status', ASSESSMENT.previous.assessmentStatus);
    assertAssessmentElement('#previous-business-date', '17-Mar');
    assertAssessmentElement('#previous-price-low', ASSESSMENT.previous.priceLow.toString());
    assertAssessmentElement('#previous-price-high', ASSESSMENT.previous.priceHigh.toString());
  });
});
