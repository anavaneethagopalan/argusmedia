import { Component, Input, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/skipWhile';
import 'rxjs/add/operator/startWith';

import { ProductService } from '../shared/product.service';
import { IAppState } from '../store/model';
import { ProductPrivilege } from '../products/models/user/product-privilege';
import { Assessment } from '../products/models/assessment/assessment';
import { IProduct } from '../products/models/product';

@Component({
  selector: 'aom-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent implements OnInit {
  @Input() public product: ProductPrivilege;
  @select() public products$: Observable<IProduct[]>;

  public assessment$: Observable<Assessment>;
  public loading$: Observable<boolean>;

  constructor() { }

  ngOnInit() {
    this.assessment$ = this.products$.mergeMap((products) => products.filter(p => p.productId === this.product.productId))
      .skipWhile(p => typeof p.assessment === 'undefined' || p.assessment === null)
      .map(p => p.assessment);
    this.loading$ = this.assessment$.mergeMap(_ => Observable.of(false))
      .startWith(true);
  }

  formatPrice(price: any) {
    return (typeof price === 'number') ? price : '-';
  }
}
