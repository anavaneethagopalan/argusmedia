import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'aom-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  // This message is displayed to the user in IE and Edge when they navigate without using Angular routing (type another URL/close the browser/etc)
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    console.log($event);
    // if (!this.canDeactivate()) {
         $event.returnValue = 'Changes that you made may not be saved.';
    // }
  }

}
