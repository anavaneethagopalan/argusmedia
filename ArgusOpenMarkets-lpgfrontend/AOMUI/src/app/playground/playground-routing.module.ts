import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlaygroundComponent } from './playground.component';
import { FormElementsComponent } from './form-elements/form-elements.component';
import { ModalComponentComponent } from './modal-component/modal-component.component';
import { htmlElementsComponent } from './html-elements/html-elements.component';
import { bidAskStackComponent } from './bid-ask-stack/bid-ask-stack.component';
import { bidAskStackComplexComponent } from './bid-ask-stack-complex/bid-ask-stack-complex.component';

const routes: Routes = [
    {
        path: '',
        component: PlaygroundComponent,
        children: [
            {
                path: 'form-elements',
                component: FormElementsComponent,
            },
            {
                path: 'modal-component',
                component: ModalComponentComponent,
            },
            {
                path: 'html-elements',
                component: htmlElementsComponent,
            },
            {
                path: 'bid-ask-stack',
                component: bidAskStackComponent,
            },
            {
                path: 'bid-ask-stack-complex',
                component: bidAskStackComplexComponent,
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlaygroundRoutingModule { }
