import { Component, TemplateRef, OnInit } from '@angular/core';
// for the modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';


@Component({
  selector: 'aom-form-elements',
  templateUrl: './form-elements.component.html',
  styleUrls: ['./form-elements.component.scss']
})
export class FormElementsComponent implements OnInit {

  minDate: Date;
  maxDate: Date;
  public modalRef: BsModalRef;

  constructor(private modalService: BsModalService) {}

  ngOnInit() {
  }

  // apply the border to the selected radio buttons parent label
  public radioSelectedValue = 0;
  // addLabelBorder(value) {
  //   this.radioSelectedValue = value;
  // }
  // apply the border to the selected radio buttons parent label
  addLabelBorder(value: number) {
    this.radioSelectedValue = value;
  }

  isRadioSelected(value: number) {
    return this.radioSelectedValue === value;
  }

// OLD STUFF BELOW HERE



  // alerts example
  public alerts: any = [
    {
      type: 'success',
      msg: `You successfully read this important alert message.`
    },
    {
      type: 'info',
      msg: `This alert needs your attention, but it's not super important.`
    }
  ];

  public reset(): void {
    this.alerts = this.alerts.map((alert: any) => Object.assign({}, alert));
  }

  // the accordion  example
  public oneAtATime: boolean = true;

  //popover
  public htmlBid:string = `
    <table class="table">
      <tbody>
        <tr>
          <td>Order Id:</td>
          <td>2796</td>
        </tr>
        <tr>
          <td>Status:</td>
          <td>Held</td>
        </tr>
        <tr>
          <td>Tradeable:</td>
          <td>No</td>
        </tr>
        <tr>
          <td>Co-brokering enabled:</td>
          <td>No</td>
        </tr>
        <tr>
          <td>Price:</td>
          <td>222,222.00</td>
        </tr>
        <tr>
          <td>Quantity:</td>
          <td>200,000</td>
        </tr>
        <tr>
          <td>Principal:</td>
          <td>Delta Traders (DKT)</td>
        </tr>
        <tr>
          <td>Broker:</td>
          <td>Any Broker</td>
        </tr>
        <tr>
          <td>Loading Planet:</td>
          <td>Krypton</td>
        </tr>
        <tr>
          <td>Loading Period:</td>
          <td>17-Sep - 10-Oct</td>
        </tr>
        <tr>
          <td>Notes:</td>
          <td>Note</td>
        </tr>
        <tr>
          <td>Last Updated:</td>
          <td>13-Sep 4:55</td>
        </tr>
      </tbody>
    </table>
  `;
  public titleAsk:string = 'Ask';
  public htmlAsk:string = `
    <table class="table">
      <tbody>
        <tr>
          <td>Order Id:</td>
          <td>2796</td>
        </tr>
        <tr>
          <td>Status:</td>
          <td>Held</td>
        </tr>
        <tr>
          <td>Tradeable:</td>
          <td>No</td>
        </tr>
        <tr>
          <td>Co-brokering enabled:</td>
          <td>No</td>
        </tr>
        <tr>
          <td>Price:</td>
          <td>222,222.00</td>
        </tr>
        <tr>
          <td>Quantity:</td>
          <td>200,000</td>
        </tr>
        <tr>
          <td>Principal:</td>
          <td>Delta Traders (DKT)</td>
        </tr>
        <tr>
          <td>Broker:</td>
          <td>Any Broker</td>
        </tr>
        <tr>
          <td>Loading Planet:</td>
          <td>Krypton</td>
        </tr>
        <tr>
          <td>Loading Period:</td>
          <td>17-Sep - 10-Oct</td>
        </tr>
        <tr>
          <td>Notes:</td>
          <td>Note</td>
        </tr>
        <tr>
          <td>Last Updated:</td>
          <td>13-Sep 4:55</td>
        </tr>
      </tbody>
    </table>
  `;
}
