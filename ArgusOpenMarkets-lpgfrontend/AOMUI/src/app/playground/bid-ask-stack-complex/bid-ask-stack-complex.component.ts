import { Component, TemplateRef, OnInit } from '@angular/core';
// for the modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'aom-bid-ask-stack-complex',
  templateUrl: './bid-ask-stack-complex.component.html',
  styleUrls: ['./bid-ask-stack-complex.component.scss']
})
export class bidAskStackComplexComponent implements OnInit {
  public modalRef: BsModalRef;

  constructor(private modalService: BsModalService) {}

  ngOnInit() {
  }

  // Demo part 1 - Open the simple Bid Modal
  public openModalBidComplex(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-bid modal-lg'}));
  }
  // Demo part 2 - Open the simple Ask Modal
  public openModalAskComplex(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-ask modal-lg'}));
  }
  // Demo part 4 - Open the Pricing Modal
  public openModalPricingComplex(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-lg'}));
  }

  // simplistic hiding and showing elements for demo purposes
  hideDiv: false;
  showDiv: false;

  // toggle the Pricing component > Basis element
  togglePricingBasis: false;

  // apply the border to the selected radio buttons parent label
  public radioSelectedValue = 0;
  addLabelBorder(value: number) {
    this.radioSelectedValue = value;
  }






}
