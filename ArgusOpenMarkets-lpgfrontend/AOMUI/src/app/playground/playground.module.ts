import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaygroundComponent } from './playground.component';
import { PlaygroundRoutingModule } from './playground-routing.module';
import { FormElementsComponent } from './form-elements/form-elements.component';
import { ModalComponentComponent } from './modal-component/modal-component.component';
import { htmlElementsComponent } from './html-elements/html-elements.component';
import { bidAskStackComponent } from './bid-ask-stack/bid-ask-stack.component';
import { bidAskStackComponent2 } from './bid-ask-stack2/bid-ask-stack2.component';
import { bidAskStackComplexComponent } from './bid-ask-stack-complex/bid-ask-stack-complex.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AlertModule, AccordionModule, BsDatepickerModule, PopoverModule, ModalModule, ButtonsModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
      PlaygroundRoutingModule,
      FlexLayoutModule,
      AlertModule.forRoot(),
      AccordionModule.forRoot(),
      BsDatepickerModule.forRoot(),
      PopoverModule.forRoot(),
      ModalModule.forRoot(),
      ButtonsModule.forRoot()
  ],
  declarations: [
    PlaygroundComponent,
    FormElementsComponent,
    ModalComponentComponent,
    htmlElementsComponent,
    bidAskStackComponent,
    bidAskStackComponent2,
    bidAskStackComplexComponent
  ]
})
export class PlaygroundModule { }
