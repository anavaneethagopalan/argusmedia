import { Component, TemplateRef, OnInit } from '@angular/core';
// for the modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'aom-bid-ask-stack2',
  templateUrl: './bid-ask-stack2.component.html',
  styleUrls: ['./bid-ask-stack2.component.scss']
})
export class bidAskStackComponent2 implements OnInit {
  public modalRef: BsModalRef;

  constructor(private modalService: BsModalService) {}

  ngOnInit() {
  }

  // public openModal(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template);
  // }
  // public openModalAsk(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-ask'}));
  // }
  // public openModalBid(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-bid'}));
  // }
  //
  //
  //
  // public openModalAskSimple(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-ask'}));
  // }
  // public openModalAskComplex(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-lg modal-ask'}));
  // }


  // Demo part 1 - Open the simple Bid Modal
  public openModalBidSimple(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-bid'}));
  }
  // Demo part 2 - Open the simple Ask Modal
  public openModalAskSimple(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-ask'}));
  }

  // public openModalBidComplex(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-lg modal-bid'}));
  // }

  // simplistic hiding and showing elements for demo purposes
  //hideDiv: false;
  //showDiv: false;

  // toggle the Pricing component > Basis element
  //togglePricingBasis: false;

}
