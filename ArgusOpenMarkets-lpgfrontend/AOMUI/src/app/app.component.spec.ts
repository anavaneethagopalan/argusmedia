import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/lib/testing';
import { NgRedux } from '@angular-redux/store';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let mockNgRedux: NgRedux<any>;

  const toastManagerStub = {
    setRootViewContainerRef: () => undefined
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgReduxTestingModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: ToastsManager, useValue: toastManagerStub }
      ]
    }).compileComponents().then( () => {
      mockNgRedux = MockNgRedux.getInstance();
    });
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should have copyrightYear as 2020 when set with date 05/04/2020', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.setCopyrightYear(new Date(2020, 4, 5));
    expect(app.copyrightYear).toEqual(2020);
  }));

  it('should create footer tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('footer')).toBeTruthy();
  }));
});
