import { TestBed, inject } from '@angular/core/testing';

import { CookieService } from 'ng2-cookies';

import { UserCookieService } from './user-cookie.service';

describe('UserCookieService', () => {

  const cookieServiceStub = {
    check: () => undefined,
    get: () => undefined,
    set: () => undefined,
    delete: () => undefined
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserCookieService,
        { provide: CookieService, useValue: cookieServiceStub },
      ]
    });
  });

  it('should be created', inject([UserCookieService], (service: UserCookieService) => {
    expect(service).toBeTruthy();
  }));
});
