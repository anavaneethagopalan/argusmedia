import { FormGroup, FormControl } from '@angular/forms';

import { PasswordValidator } from './password-validator';

describe('PasswordValidator', () => {
    it('should return areEqual error if passwords are different', () => {
        const passwordFormGroup = new FormGroup({
            newPassword: new FormControl('password123'),
            confirmPassword: new FormControl('differentpassword')
        });
        expect(PasswordValidator.areEqual(passwordFormGroup)).toEqual({ areNotEqual: true });
    });

    it('should return null if passwords are the same', () => {
        const passwordFormGroup = new FormGroup({
            newPassword: new FormControl('samepassword'),
            confirmPassword: new FormControl('samepassword')
        });
        expect(PasswordValidator.areEqual(passwordFormGroup)).toBeNull();
    });

    it('should return invalidPasswordLength error if password null, undefined or empty string', () => {
        const lessThanEightChars = PasswordValidator.passwordLength(8);
        const newPasswordControlNull = new FormControl(null);
        const newPasswordControlUndefined = new FormControl(undefined);
        const newPasswordControlEmpty = new FormControl('');
        expect(lessThanEightChars(newPasswordControlNull)).toEqual({ invalidPasswordLength: true });
        expect(lessThanEightChars(newPasswordControlUndefined)).toEqual({ invalidPasswordLength: true });
        expect(lessThanEightChars(newPasswordControlEmpty)).toEqual({ invalidPasswordLength: true });
    });

    it('should return invalidPasswordLength error if password length less than 8 characters', () => {
        const lessThanEightChars = PasswordValidator.passwordLength(8);
        const newPasswordControl = new FormControl('6chars');
        expect(lessThanEightChars(newPasswordControl)).toEqual({ invalidPasswordLength: true });
    });

    it('should return null if password length exactly 8 characters', () => {
        const lessThanEightChars = PasswordValidator.passwordLength(8);
        const newPasswordControl = new FormControl('12345678');
        expect(lessThanEightChars(newPasswordControl)).toBeNull();
    });

    it('should return null if password length greater than 8 characters', () => {
        const lessThanEightChars = PasswordValidator.passwordLength(8);
        const newPasswordControl = new FormControl('passwordwithlotsandlotsofcharacters');
        expect(lessThanEightChars(newPasswordControl)).toBeNull();
    });

    it('should return invalidNumberOrSpecial error if password only contains letters', () => {
        const newPasswordControl = new FormControl('onlyletters');
        expect(PasswordValidator.numberOrSpecial(newPasswordControl)).toEqual({ invalidNumberOrSpecial: true});
    });

    it('should return null if password contains numbers', () => {
        const newPasswordControl = new FormControl('password123');
        expect(PasswordValidator.numberOrSpecial(newPasswordControl)).toBeNull();
    });

    it('should return null if password contains special characters', () => {
        const newPasswordControl = new FormControl('password£$%');
        expect(PasswordValidator.numberOrSpecial(newPasswordControl)).toBeNull();
    });

    it('should return invalidUpperAndLowerCase error if password doesn\'t contain bother upper and lower case letters', () => {
        const newPasswordControlLower = new FormControl('onlylowercase');
        const newPasswordControlUpper = new FormControl('ONLYUPPERCASE');
        expect(PasswordValidator.upperAndLowerCase(newPasswordControlLower)).toEqual({ invalidUpperAndLowerCase: true});
        expect(PasswordValidator.upperAndLowerCase(newPasswordControlUpper)).toEqual({ invalidUpperAndLowerCase: true});
    });

    it('should return null if password contains both upper and lower case letters', () => {
        const newPasswordControl = new FormControl('bothUPPERandlowercase');
        expect(PasswordValidator.upperAndLowerCase(newPasswordControl)).toBeNull();
    });
});
