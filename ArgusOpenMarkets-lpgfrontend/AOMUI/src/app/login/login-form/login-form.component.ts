import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../shared/config.service';
import { LoginSessionAPIActions } from '../../api/session/actions';
import { ICredential, ILoginSessionState, ILoginSession } from '../../api/session/models';
import { IAppState } from '../../store/model';
import { UserCookieService } from '../user-cookie.service';

@Component({
  selector: 'aom-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  public loginForm: FormGroup;
  public unsupportedBrowserPreventLogin = false;
  public unsupportedBrowserShowWarning = false;

  @select(['webapi_login_session']) session$: Observable<ILoginSessionState>;
  @select(['platform']) platform$: Observable<Platform>;

  constructor(private fb: FormBuilder, private cookieService: UserCookieService, private configService: ConfigService,
    private actions: LoginSessionAPIActions) {
  }

  ngOnInit() {
    this.initialiseLoginForm();
    if (this.cookieService.checkUsernameCookie()) {
      this.loginForm.controls.email.setValue(this.cookieService.getUsernameCookie());
      this.loginForm.controls.rememberMe.setValue(true);
    }
    this.platform$.skipWhile(p => typeof p === 'undefined' || p === null)
      .subscribe((p: Platform) => {
        this.unsupportedBrowserPreventLogin = this.configService.unsupportedBrowserPreventLogin(p.name, p.version);
        this.unsupportedBrowserShowWarning = this.configService.unsupportedBrowserShowWarning(p.name, p.version);
      });
  }

  public login(): void {
    const credential: ICredential = {
      username: this.loginForm.controls.email.value,
      password: this.loginForm.controls.password.value,
      loginSource: 'aom',
      rememberMe: this.loginForm.controls.rememberMe.value
    };
    this.actions.loginUser({ credential });

  }

  public isLoginDisabled() {
    return this.loginForm.invalid;
  }

  private initialiseLoginForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      rememberMe: [false]
    });
  }

  // apply the border to the "remember me" checkbox parent label when the fake checkbox is selected
  public checkboxSelectedValue = 0;
  addLabelBorder(value: number) {
    this.checkboxSelectedValue = value;
  }

}
