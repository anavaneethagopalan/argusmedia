import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { LoginFormComponent } from './login-form.component';
import { AuthResponse } from '../../models/auth-response';
import { ResponsiveModule, ResponsiveState } from 'ng2-responsive';
import { ConfigService } from '../../shared/config.service';
import { Payload, LoginSessionAPIAction } from '../../api/session/models';
import { LoginSessionAPIActions } from '../../api/session/actions';
import { UserCookieService } from '../user-cookie.service';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;

  const configServiceStub = {
    IsSupportedBrowser: true
  };

  const actionsStub = {
    loginUser: (payload: Payload): LoginSessionAPIAction => undefined
  };

  const userCookieServiceStub = {
    checkUsernameCookie: () => undefined,
    getUsernameCookie: () => undefined
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, NoopAnimationsModule, ReactiveFormsModule],
      declarations: [LoginFormComponent],
      providers: [
        { provide: ConfigService, useValue: configServiceStub },
        { provide: LoginSessionAPIActions, useValue: actionsStub },
        { provide: UserCookieService, useValue: userCookieServiceStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set username from cookie and remember me as true if username cookie exists', () => {
    const cookieService = TestBed.get(UserCookieService);
    spyOn(cookieService, 'checkUsernameCookie').and.returnValue(true);
    spyOn(cookieService, 'getUsernameCookie').and.returnValue('STORED_USERNAME');
    component.ngOnInit();
    expect(component.loginForm.controls.email.value).toBe('STORED_USERNAME');
    expect(component.loginForm.controls.rememberMe.value).toBe(true);
  });

  // const createAuthResponse = (isAuthenticated: boolean, message: string): Observable<AuthResponse> => {
  //   return Observable.of<AuthResponse>({
  //     isAuthenticated,
  //     message,
  //     credentialType: 'P',
  //     sessionToken: '',
  //     token: '',
  //     userId: 0,
  //     user: {
  //       userOrganisation: {}
  //     }
  //   });
  // };

  // it('should display error message on failed authentication', () => {
  //   const authService = TestBed.get(AuthService);
  //   spyOn(authService, 'login').and.returnValue(createAuthResponse(false, 'AUTHENTICATION FAILED!'));
  //   component.login();
  //   expect(component.errorMessage).toBe('AUTHENTICATION FAILED!');
  // });

  // it('should not navigate to root url on failed authentication', () => {
  //   const authService = TestBed.get(AuthService);
  //   spyOn(authService, 'login').and.returnValue(createAuthResponse(false, 'AUTHENTICATION FAILED!'));
  //   const navigateSpy = spyOn((<any>component).router, 'navigate');
  //   component.login();
  //   expect(navigateSpy).not.toHaveBeenCalled();
  // });

  // it('should not display error message on successful authentication', () => {
  //   const authService = TestBed.get(AuthService);
  //   spyOn(authService, 'login').and.returnValue(createAuthResponse(true, 'THIS MESSAGE SHOULD NOT APPEAR'));
  //   component.login();
  //   expect(component.errorMessage).toBeFalsy();
  // });

  // it('should navigate to root url on succesful authentication', () => {
  //   const authService = TestBed.get(AuthService);
  //   spyOn(authService, 'login').and.returnValue(createAuthResponse(true, 'THIS MESSAGE SHOULD NOT APPEAR'));
  //   const navigateSpy = spyOn((<any>component).router, 'navigate');
  //   component.login();
  //   expect(navigateSpy).toHaveBeenCalledWith(['/']);
  // });
});
