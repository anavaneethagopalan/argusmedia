import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../shared/logger.service';

@Component({
  selector: 'aom-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private loggerService: LoggerService) { }

  ngOnInit() {
  }

  // public thisUserAgent($event) {
  //   console.log($event);
  //   this.loggerService.info(JSON.stringify($event));
  // }
}
