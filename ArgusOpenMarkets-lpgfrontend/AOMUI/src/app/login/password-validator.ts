import { FormGroup, FormControl } from '@angular/forms';

export class PasswordValidator {

    constructor() { }

    static areEqual(group: FormGroup) {
        let valid = true;

        let valueToCheck = '';
        for (const name in group.controls) {
            if (group.controls.hasOwnProperty(name)) {
                const value = group.controls[name].value;
                if (!valueToCheck) {
                    valueToCheck = value;
                } else {
                    if (value !== valueToCheck) {
                        valid = false;
                    }
                }
            }
        }

        if (valid) {
            return null;
        }

        return {
            areNotEqual: true
        };
    }

    static passwordLength(minLength: number) {
        return (control: FormControl) => {
            if (control.value && control.value.length >= minLength) {
                return null;
            }
            return {
                invalidPasswordLength: true
            };
        };
    }

    static numberOrSpecial(control: FormControl) {
        const numberRegex = new RegExp('[0-9]');
        const specialRegex = new RegExp('[^0-9a-zA-Z]');
        if (numberRegex.test(control.value) || specialRegex.test(control.value)) {
            return null;
        }
        return {
            invalidNumberOrSpecial: true
        };
    }

    static upperAndLowerCase(control: FormControl) {
        const lowerRegex = new RegExp('[a-z]');
        const upperRegex = new RegExp('[A-Z]');
        if (lowerRegex.test(control.value) && upperRegex.test(control.value)) {
            return null;
        }
        return {
            invalidUpperAndLowerCase: true
        };
    }

    static tempPasswordMatch(tempPassword: string) {
        return (control: FormControl) => {
            if (tempPassword && tempPassword === control.value) {
                return {
                    tempPasswordMatch: true
                };
            }
            return null;
        };
    }
}
