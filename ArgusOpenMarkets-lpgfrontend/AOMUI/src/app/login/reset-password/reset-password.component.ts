import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { select } from '@angular-redux/store';

import { PasswordValidator } from '../password-validator';
import { ResetPasswordSessionAPIActions } from '../../api/reset-password/actions';

@Component({
  selector: 'aom-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  private tempPasswordToken: string;
  private tempPassword?: string;
  public resetPasswordForm: FormGroup;
  @select(['webapi_reset_password_session']) session$;
  @select(['webapi_login_session', 'session']) loginSession$;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private resetPasswordActions: ResetPasswordSessionAPIActions) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      if (params['t']) {
        console.log('?t=', params['t']);
        this.tempPasswordToken = params['t'];
      }
      this.resetPasswordActions.resetPasswordInit();
      this.initialiseResetPasswordForm();
    });
    this.loginSession$.subscribe((ls) => {
      this.tempPassword = (ls) ? ls.tempPassword : null;
      this.initialiseResetPasswordForm();
    });
  }

  public submit(): void {
    const payload = {
      tempPasswordToken: this.tempPasswordToken,
      newPassword: this.resetPasswordForm.controls.newPassword.value
    };
    this.resetPasswordActions.resetPassword(payload);
  }

  private initialiseResetPasswordForm() {
    this.resetPasswordForm = this.fb.group({
      newPassword: ['', [Validators.required, PasswordValidator.passwordLength(8),
      PasswordValidator.numberOrSpecial, PasswordValidator.upperAndLowerCase,
      PasswordValidator.tempPasswordMatch(this.tempPassword)
      ]],
      confirmPassword: ['', Validators.required]
    },
      {
        validator: PasswordValidator.areEqual
      });
  }
}
