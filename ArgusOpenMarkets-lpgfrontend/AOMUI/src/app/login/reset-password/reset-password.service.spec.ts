import { TestBed, inject } from '@angular/core/testing';
import { Http } from '@angular/http';

import { ResetPasswordService } from './reset-password.service';
import { ConfigService } from '../../shared/config.service';

describe('ResetPasswordService', () => {

  const configServiceStub = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ResetPasswordService,
        { provide: Http, useValue: {} },
        { provide: ConfigService, useValue: configServiceStub }
      ]
    });
  });

  it('should be created', inject([ResetPasswordService], (service: ResetPasswordService) => {
    expect(service).toBeTruthy();
  }));
});
