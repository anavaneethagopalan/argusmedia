import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { ConfigService } from '../../shared/config.service';

@Injectable()
export class ResetPasswordService {

  constructor(private http: Http, private configService: ConfigService) { }

  // TODO: User service was removed, use session epic instead

  // public resetPassword(newPassword: string): Observable<any> {
  //   const headers = this.getHeaders();
  //   // if the user has been authenticated
  //   if (headers) {
  //     return this.webServiceUpdateDetails(newPassword, headers);
  //   } else {
  //     return this.webServiceResetPassword(newPassword);
  //   }
  // }

  // public get tempPassword(): string {
  //   return this.userService.TempPassword;
  // }

  // public setTempPassword(tempPassword: string) {
  //   this.userService.setTempPassword(tempPassword);
  // }

  // private getHeaders(): Headers | null {
  //   if (this.userService.SessionToken && this.userService.UserId) {
  //     const headers = new Headers();
  //     headers.append('token', this.userService.SessionToken);
  //     headers.append('userid', this.userService.UserId.toString());
  //     return headers;
  //   }
  //   return null;
  // }

  // private webServiceUpdateDetails(newPassword: string, headers: Headers): Observable<any> {
  //   return this.http
  //     .post(this.configService.webApi + '/updateDetails',
  //     {
  //       username: this.userService.Username,
  //       oldPassword: this.userService.TempPassword,
  //       newPassword
  //     },
  //     {
  //       headers: headers
  //     })
  //     .map((response) => response.json());
  // }

  // private webServiceResetPassword(newPassword: string): Observable<any> {
  //   return this.http
  //     .post(this.configService.webApi + '/resetPassword',
  //     {
  //       oldPassword: this.userService.TempPassword,
  //       newPassword
  //     })
  //     .map((response) => response.json());
  // }
}
