import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotSupportedBrowserComponent } from './not-supported-browser.component';
import { ConfigService } from '../../../shared/config.service';

describe('NotSupportedBrowserComponent', () => {
  let component: NotSupportedBrowserComponent;
  let fixture: ComponentFixture<NotSupportedBrowserComponent>;

  const configServiceStub = {
    IsSupportedBrowser: true
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotSupportedBrowserComponent ],
      providers: [
        { provide: ConfigService, useValue: configServiceStub }
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotSupportedBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
