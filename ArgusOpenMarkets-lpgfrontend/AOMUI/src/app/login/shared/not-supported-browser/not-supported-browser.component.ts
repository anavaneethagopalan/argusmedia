import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';
import * as Platform from 'platform';

@Component({
  selector: 'aom-not-supported-browser',
  templateUrl: './not-supported-browser.component.html',
  styleUrls: ['./not-supported-browser.component.scss']
})
export class NotSupportedBrowserComponent implements OnInit {
  @select(['platform']) platform$: Observable<Platform>;
  ngOnInit() {
  }
}
