import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CookieService } from 'ng2-cookies';
import { ResponsiveModule } from 'ng2-responsive';

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { ForgottenPasswordComponent } from './forgotten-password/forgotten-password.component';
import { RegisterComponent } from './shared/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResetPasswordService } from './reset-password/reset-password.service';
import { NotSupportedBrowserComponent } from './shared/not-supported-browser/not-supported-browser.component';
import { UserCookieService } from './user-cookie.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    ResponsiveModule
  ],
  declarations: [
    LoginComponent,
    LoginFormComponent,
    ForgottenPasswordComponent,
    RegisterComponent,
    ResetPasswordComponent,
    NotSupportedBrowserComponent,
  ],
  providers: [
    CookieService,
    ResetPasswordService,
    UserCookieService
  ],
})
export class LoginModule { }
