import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select } from '@angular-redux/store';

import { ForgottenPasswordSessionAPIActions } from '../../api/forgotten-password/actions';

@Component({
  selector: 'aom-forgotten-password',
  templateUrl: './forgotten-password.component.html',
  styleUrls: ['./forgotten-password.component.scss']
})
export class ForgottenPasswordComponent implements OnInit {
  public forgottenPasswordForm = this.fb.group({
    email: ['', Validators.required]
  });

  readonly succesfulResponseText = `Thank you for your request. An email has been sent to you
                                            with a link to a page where you can change your password.`;
  @select(['webapi_forgotten_password_session']) session$;

  constructor(private fb: FormBuilder, private actions: ForgottenPasswordSessionAPIActions) { }

  ngOnInit() {
  }

  public submit(): void {
    this.actions.forgottenPassword(this.forgottenPasswordForm.controls.email.value);
  }
}
