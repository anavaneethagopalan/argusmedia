import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import { ForgottenPasswordComponent } from './forgotten-password.component';
import { ForgottenPasswordSessionAPIActions } from '../../api/forgotten-password/actions';

describe('ForgottenPasswordComponent', () => {
  let component: ForgottenPasswordComponent;
  let fixture: ComponentFixture<ForgottenPasswordComponent>;

  const forgottenPasswordActionsStub = {
    forgottenPassword: () => undefined
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, NoopAnimationsModule, ReactiveFormsModule],
      declarations: [ForgottenPasswordComponent],
      providers: [
        { provide: ForgottenPasswordSessionAPIActions, useValue: forgottenPasswordActionsStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgottenPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // it('should display success message on successful forgotten password response', () => {
  //   const forgottenPasswordService = TestBed.get(ForgottenPasswordService);
  //   spyOn(forgottenPasswordService, 'webServiceForgottenPassword').and.returnValue(Observable.of({
  //     isAuthenticated: true,
  //     message: 'THIS MESSAGE SHOULD NOT APPEAR'
  //   }));
  //   component.submit();
  //   expect(component.message).toBe(component.succesfulResponseText);
  // });

  // it('should display response message on invalid forgotten password response', () => {
  //   const forgottenPasswordService = TestBed.get(ForgottenPasswordService);
  //   const messageText = 'Invalid username';
  //   spyOn(forgottenPasswordService, 'webServiceForgottenPassword').and.returnValue(Observable.of({
  //     isAuthenticated: false,
  //     message: messageText
  //   }));
  //   component.submit();
  //   expect(component.message).toBe(messageText);
  // });

  // it('should display error when forgotten password service throws an error', () => {
  //   const forgottenPasswordService = TestBed.get(ForgottenPasswordService);
  //   spyOn(forgottenPasswordService, 'webServiceForgottenPassword').and.returnValue(Observable.throw('ERROR!'));
  //   component.submit();
  //   expect(component.message).toBe(component.errorResponseText);
  // });
});
