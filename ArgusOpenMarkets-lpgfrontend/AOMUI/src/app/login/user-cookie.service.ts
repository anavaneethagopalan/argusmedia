import { Injectable } from '@angular/core';

import { CookieService } from 'ng2-cookies';

@Injectable()
export class UserCookieService {

  private readonly USERNAME_COOKIE_KEY = 'aom-username';
  private readonly NUM_EXPIRY_DAYS = 30;

  constructor(private cookieService: CookieService) { }

  public checkUsernameCookie(): boolean {
    return this.cookieService.check(this.USERNAME_COOKIE_KEY);
  }

  public getUsernameCookie(): string {
    return atob(this.cookieService.get(this.USERNAME_COOKIE_KEY));
  }

  public updateUsernameCookie(username: string): void {
    this.cookieService.set(this.USERNAME_COOKIE_KEY, btoa(username), this.NUM_EXPIRY_DAYS);
  }

  public deleteUsernameCookie(): void {
    this.cookieService.delete(this.USERNAME_COOKIE_KEY);
  }

}
