import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { LoginComponent } from './login.component';
import { LoggerService } from '../shared/logger.service';
import { inject } from '@angular/core/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const loggerServiceStub = {
    info: () => undefined
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [LoginComponent],
      providers: [{provide: LoggerService, useValue: loggerServiceStub}],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', inject([LoggerService], (service: LoggerService) => {
    const loggerService = TestBed.get(LoggerService);
    expect(component).toBeTruthy();
    expect(service).toBeTruthy();
    const loggerInfoSpy = spyOn(loggerService, 'info');
  }));

  it('should create header tag', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('header')).toBeTruthy();
  }));

});
