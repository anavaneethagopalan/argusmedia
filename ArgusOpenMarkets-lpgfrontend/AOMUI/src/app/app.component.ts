import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import * as platform from 'platform';
import { Observable } from 'rxjs/Observable';

import { IAppState } from './store/model';
import { loadPlatform } from './store/actions/platform';

@Component({
  selector: 'aom-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public copyrightYear: number;

  constructor(private toastManager: ToastsManager, private vcr: ViewContainerRef,
    private store: NgRedux<IAppState>) {
    this.toastManager.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    this.setCopyrightYear(new Date());
    this.store.dispatch(loadPlatform(platform));
  }

  public setCopyrightYear(date: Date) {
    this.copyrightYear = date.getFullYear();
  }
}
