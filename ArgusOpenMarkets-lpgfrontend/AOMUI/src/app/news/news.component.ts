import { Component, Input, OnInit } from '@angular/core';
import { NewsService } from '../api/news/service';
import { NewsMessage } from '../api/news/models';
import { select, select$, NgRedux } from '@angular-redux/store';
import { IAppState } from '../store/model';
import { Observable } from 'rxjs/Observable';
import { NewsAPIActions } from '../api/news/actions';
import { INewsState, News } from '../api/news/models';
import { DateTimePipe } from '../shared/date-time.pipe';

@Component({
  selector: 'aom-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  // the accordion  example
  public oneAtATime: boolean = true;

  newsStories$: Observable<News[]>;
  newsStory$: Observable<News>;
  
  public loading$: Observable<boolean>;

  constructor(private newsService: NewsService, 
    private store: NgRedux<IAppState>, 
    private newsAPIActions: NewsAPIActions) {}

  ngOnInit() {
    console.log('News Component Init');

    setTimeout(() => {
      let news = this.store.getState().news.newsStories;
      if(!news.length){
        this.store.dispatch(this.newsAPIActions.noNewsYet());
      }
    }, 10000)
    this.newsStories$ = this.store
      .select(store => store.news.newsStories)
      .delay(2000);

    this.newsStory$ = this.store
      .select(store => store.news.newsStory);

    this.loading$  = this.newsStories$
      .do(a => {
        console.log('got some news', a)
      })
      .filter(a => a !== null && a.length > 0)
      .do(log => {
        console.log('got some real news after filter', log)
      })
      .flatMap( _ => Observable.of(false))
      .startWith(true)
      .do(_ => {
        console.log('on ngInit Loading = ', _);
      })
  }

  showNewsBody(news: News, event: any){
    if(news.cmsId !== '-1'){
      let newsFullStory = { ... news };
      newsFullStory.story = 'Loading...';
      this.store.dispatch(this.newsAPIActions.getNewsBody(newsFullStory));
    }
  }
}
