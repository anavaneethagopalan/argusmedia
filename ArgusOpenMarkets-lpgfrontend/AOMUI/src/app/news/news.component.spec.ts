import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/lib/testing';

import { AccordionModule } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';

import { NewsAPIActions } from '../api/news/actions';
import { NewsComponent } from './news.component';
import { NewsService } from '../api/news/service';
import { SafeHtmlPipe } from '../shared/safe-html.pipe';
import { DateTimePipe } from '../shared/date-time.pipe';

describe('NewsComponent', () => {
  let component: NewsComponent;
  let fixture: ComponentFixture<NewsComponent>;
  
  const newsAPIActionsStub = {
    getNewsBody: (news) => undefined
  };

  const newsServiceStub = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgReduxTestingModule, AccordionModule.forRoot()],
      declarations: [NewsComponent, SafeHtmlPipe, DateTimePipe],
      providers: [
        { provide: NgRedux, useValue: MockNgRedux.getInstance() },
        { provide: NewsService, useValue: newsServiceStub }, 
        { provide: NewsAPIActions, useValue: newsAPIActionsStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });


  it('should contain a null news story upon initialization', async(() => {
    const stub = MockNgRedux.getSelectorStub(['news', 'newsStory']);
    stub.next(null);
    stub.complete();

    component.newsStory$.subscribe( result => {         
      expect(result).toBeNull();
    });   
  }));

  it('news stories should read the news stories from the store', async(() => {
    const stub = MockNgRedux.getSelectorStub(['news', 'newsStories']);
    stub.next([{
      cmsId: 1, 
      headline: 'headline',
      story: 'Ihe BFG'
    }, {
      cmsId: 2,
      headline: 'headline',
      story: 'Star Wars'
    }]);
    stub.complete();

    component.newsStories$.subscribe( result => {         
      expect(result.length).toEqual(2);
    });   
  }));

  it('should return loading true when no news stories', async(() => {
    const stub = MockNgRedux.getSelectorStub(['news', 'newsStories']);
    stub.next([]);
    stub.complete();

    component.loading$.subscribe( result => {         
      expect(result).toEqual(true);
    });   
  }));

  it('should return loading false when we have news stories', () => {
    const stub = MockNgRedux.getSelectorStub(['news', 'newsStories']);
    stub.next([{
      cmsId: 1, 
      headline: 'headline',
      story: 'Ihe BFG'
    }]);
    stub.complete();

    component.loading$.skip(1).subscribe(result => {
      expect(result).toEqual(false);
    });
  });
});
