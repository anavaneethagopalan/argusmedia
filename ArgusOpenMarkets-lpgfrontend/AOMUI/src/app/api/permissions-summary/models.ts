import { Message } from '../../models/message';

export interface IPermissionSummaryMessage {
    canTrade: boolean;
    message: Message;
    organisationId: number;
}

export interface IPermissionSummary {
    productId: number;
    permissionType: 'BrokerPermission' | 'CounterpartyPermission';
    buyOrSell: 'buy' | 'sell';
    //canTrade: boolean; // not being used, if permission is denied, it will be removed from the array
    organisationId: number;
}