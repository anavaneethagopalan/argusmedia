import { FluxStandardAction } from "flux-standard-action";

import { IPermissionSummary } from './models';
import { SET_PERMISSIONS_SUMMARY, DELETE_PERMISSIONS_SUMMARY } from './actions';

export function permissionsSummary(state: IPermissionSummary[] = [], action: FluxStandardAction<IPermissionSummary, any>): IPermissionSummary[] {
    
    switch (action.type) {
        case SET_PERMISSIONS_SUMMARY: {
            let index = state.findIndex(permission => 
                permission.productId === action.payload.productId 
                && permission.permissionType === action.payload.permissionType
                && permission.organisationId === action.payload.organisationId
                && permission.buyOrSell === action.payload.buyOrSell
            );
            if(index > -1) {
                return [
                    ...state.splice(index, 1, action.payload),
                ];
            }
            else {
                return [
                    ...state,
                    action.payload
                ];
            }
        }

        //not tested
        // case DELETE_PERMISSIONS_SUMMARY: {
        //     let index = state.findIndex(permission => 
        //         permission.productId === action.payload.productId 
        //         && permission.permissionType === action.payload.permissionType
        //         && permission.organisationId === action.payload.organisationId
        //         && permission.buyOrSell === action.payload.buyOrSell
        //     );
        //     if(index > 0) {
        //         return [
        //             ...state.splice(index, 1),
        //         ];
        //     }
        //     else {
        //         return state;
        //     }
        // }            

        default:
            return state;
    }
}