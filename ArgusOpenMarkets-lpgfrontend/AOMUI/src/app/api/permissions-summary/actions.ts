import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';

import { IPermissionSummary } from './models';

export const SET_PERMISSIONS_SUMMARY = 'SET_PERMISSIONS_SUMMARY';
export const DELETE_PERMISSIONS_SUMMARY = 'DELETE_PERMISSIONS_SUMMARY';

@Injectable()
export class PermissionSummaryAPIActions {

    @dispatch()
    setPermissionSummary = function (permissionSummary: IPermissionSummary) {
        return {
            type: SET_PERMISSIONS_SUMMARY,
            payload: permissionSummary
        }
    }

    @dispatch()
    deletePermissionSummary = function (permissionSummary: IPermissionSummary) {
        return {
            type: DELETE_PERMISSIONS_SUMMARY,
            payload: permissionSummary
        }
    }
}