import { ResetPasswordSessionAPIAction, IResetPasswordSessionState } from './models';
import { RESET_PASSWORD_INIT, RESET_PASSWORD_STARTED, RESET_PASSWORD_SUCCEEDED, RESET_PASSWORD_FAILED } from './actions';

const INITIAL_STATE_RESET_PASSWORD: IResetPasswordSessionState = {
    success: false,
    loading: false,
    error: null,
};

export function resetPasswordAPIReducer(state: IResetPasswordSessionState = INITIAL_STATE_RESET_PASSWORD,
    action: ResetPasswordSessionAPIAction): IResetPasswordSessionState {

    switch (action.type) {

        case RESET_PASSWORD_INIT:
            return INITIAL_STATE_RESET_PASSWORD;
        case RESET_PASSWORD_STARTED:
            return {
                success: false,
                loading: true,
                error: null,
            };
        case RESET_PASSWORD_SUCCEEDED:
            return {
                success: true,
                loading: false,
                error: null,
            };
        case RESET_PASSWORD_FAILED:
            return {
                success: false,
                loading: false,
                error: action.error,
            };
        default:
            return state;
    }
}
