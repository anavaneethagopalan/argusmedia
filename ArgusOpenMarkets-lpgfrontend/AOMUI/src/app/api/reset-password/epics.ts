import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { createEpicMiddleware, Epic } from 'redux-observable';
import { of } from 'rxjs/observable/of';

import { ResetPasswordSessionAPIActions, RESET_PASSWORD } from './actions';
import { ResetPasswordSessionAPIService } from './service';
import { ResetPasswordSessionAPIAction } from './models';

import { IAppState } from '../../store/model';

@Injectable()
export class ResetPasswordEpic {

    constructor(
        private resetPasswordService: ResetPasswordSessionAPIService,
        private resetPasswordActions: ResetPasswordSessionAPIActions
    ) { }

    public createEpic() {
        return createEpicMiddleware(this.createLoadResetPasswordSessionEpic());
    }

    private createLoadResetPasswordSessionEpic(): Epic<ResetPasswordSessionAPIAction, IAppState> {
        return (action$, store) => action$
            .ofType(RESET_PASSWORD)
            .switchMap(({ payload }) => this.resetPasswordService.resetPassword(payload.newPassword, payload.tempPasswordToken)
                .map(_ => this.resetPasswordActions.resetPasswordSucceeded(_.payload))
                .catch(error => of(this.resetPasswordActions.resetPasswordFailed(this.handleError(error))))
                .startWith(this.resetPasswordActions.resetPasswordStarted()));
    }

    private handleError = (error) => {
        return (error.status === 0 && error.statusText === '') ? { ...error, statusText: 'Unknown error occured' } : error;
    }
}
