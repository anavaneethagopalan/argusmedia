import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../shared/config.service';
import { IAppState } from '../../store/model';
import { ResetPasswordRequest, UpdateDetailsRequest } from './models';

@Injectable()
export class ResetPasswordSessionAPIService {

    constructor(private http: Http, private configService: ConfigService, private store: NgRedux<IAppState>) { }

    public resetPassword(newPassword: string, tempPasswordToken: string): Observable<any> {
        // TODO: move to common GetUserSession method somewhere
        const userSession = this.getUserSession();
        if (userSession) {
            // TODO: move to common GetUserHeaders method somewhere
            const headers = new Headers();
            headers.append('token', userSession.sessionToken);
            headers.append('userid', userSession.userId.toString());
            return this.webServiceUpdateDetails({ username: userSession.User.username, oldPassword: userSession.tempPassword, newPassword }, headers);
        } else {
            return this.webServiceResetPassword({ newPassword, oldPassword: tempPasswordToken });
        }
    }

    webServiceResetPassword(resetPasswordRequest: ResetPasswordRequest) {
        return this.http
            .post(this.configService.webApi + '/resetPassword', resetPasswordRequest)
            .map((response) => response.json())
            .switchMap(resp => (!resp.isAuthenticated)
                ? Observable.throw({
                    statusText: resp.message
                })
                : Observable.of(resp));
    }

    webServiceUpdateDetails(updateDetailsRequest: UpdateDetailsRequest, headers: Headers) {
        return this.http
            .post(this.configService.webApi + '/updateDetails', updateDetailsRequest, { headers })
            .map((response) => response.json())
            .switchMap(resp => (!resp.isAuthenticated)
                ? Observable.throw({
                    statusText: resp.message
                })
                : Observable.of(resp));
    }

    getUserSession() {
        const loginSession = this.store.getState().webapi_login_session;
        if (loginSession && loginSession.session) {
            return loginSession.session;
        }
        return null;
    }
}
