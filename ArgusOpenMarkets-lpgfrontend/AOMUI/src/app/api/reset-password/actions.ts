import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';
import { ResetPasswordSessionAPIAction } from './models';

export const RESET_PASSWORD_INIT = 'RESET_PASSWORD_INIT';
export const RESET_PASSWORD = 'RESET_PASSWORD';
export const RESET_PASSWORD_STARTED = 'RESET_PASSWORD_STARTED';
export const RESET_PASSWORD_SUCCEEDED = 'RESET_PASSWORD_SUCCEEDED';
export const RESET_PASSWORD_FAILED = 'RESET_PASSWORD_FAILED';

@Injectable()
export class ResetPasswordSessionAPIActions {


    @dispatch()
    resetPassword = (payload): ResetPasswordSessionAPIAction => ( {
        type: RESET_PASSWORD,
        payload,
    })

    resetPasswordStarted = (): ResetPasswordSessionAPIAction => ({
        type: RESET_PASSWORD_STARTED,
        payload: null,
    })

    resetPasswordSucceeded = (payload): ResetPasswordSessionAPIAction => ( {
        type: RESET_PASSWORD_SUCCEEDED,
        payload: payload
    })

    resetPasswordFailed = (error): ResetPasswordSessionAPIAction => ( {
        type: RESET_PASSWORD_FAILED,
        payload: null,
        error
    })

    @dispatch()
    resetPasswordInit = (): ResetPasswordSessionAPIAction => ( {
        type: RESET_PASSWORD_INIT,
        payload: null
    })
}
