export interface ResetPasswordSessionAPIAction {
    type: string;
    payload: any;
    error?: any;
}

export interface IResetPasswordSessionState {
    success: boolean;
    loading: boolean;
    error: any;
}

export interface ResetPasswordRequest {
    oldPassword: string; // temp password token
    newPassword: string;
}

export interface UpdateDetailsRequest {
    username: string;
    oldPassword: string;
    newPassword: string;
}
