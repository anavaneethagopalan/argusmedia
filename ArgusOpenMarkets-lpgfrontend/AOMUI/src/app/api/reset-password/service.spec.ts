import { TestBed, inject } from '@angular/core/testing';
import { Http, Headers } from '@angular/http';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux } from '@angular-redux/store/testing';
import { Observable } from 'rxjs/Observable';

import { ResetPasswordSessionAPIService } from './service';
import { ConfigService } from '../../shared/config.service';
import { IAppState } from '../../store/model';
import { ResetPasswordRequest, UpdateDetailsRequest } from './models';

describe('ResetPasswordSessionAPIService', () => {

    const configServiceStub = {};

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ResetPasswordSessionAPIService,
                { provide: Http, useValue: {} },
                { provide: ConfigService, useValue: configServiceStub },
                { provide: NgRedux, useValue: MockNgRedux }
            ]
        });
    });

    it('should be created', inject([ResetPasswordSessionAPIService], (service: ResetPasswordSessionAPIService) => {
        expect(service).toBeTruthy();
    }));

    it('should call resetPassword when user session is null', inject([ResetPasswordSessionAPIService], (service: ResetPasswordSessionAPIService) => {
        const testPassword = 'TestPassword12';
        const tempPasswordToken = 'EQJlRwQ';
        const expectedRequest: ResetPasswordRequest = {
            oldPassword: tempPasswordToken,
            newPassword: testPassword
        };
        spyOn(service, 'getUserSession').and.returnValue(null);
        const resetPassword = spyOn(service, 'webServiceResetPassword');
        service.resetPassword(testPassword, tempPasswordToken);
        expect(resetPassword).toHaveBeenCalledWith(expectedRequest);
    }));

    it('should call updateDetails when user session exists', inject([ResetPasswordSessionAPIService], (service: ResetPasswordSessionAPIService) => {
        const username = 'TestUsername';
        const tempPassword = 'TempPassword11';
        const testPassword = 'TestPassword12';
        const userId = 123;
        const sessionToken = 'EQJlRwQ';
        const expectedRequest: UpdateDetailsRequest = {
            username,
            oldPassword: tempPassword,
            newPassword: testPassword
        };
        const expectedHeaders = new Headers();
        expectedHeaders.append('token', sessionToken);
        expectedHeaders.append('userid', userId.toString());
        spyOn(service, 'getUserSession').and.returnValue({
            sessionToken,
            userId,
            User : {
                username
            },
            tempPassword
        });
        const resetPassword = spyOn(service, 'webServiceUpdateDetails');
        service.resetPassword(testPassword, '');
        expect(resetPassword).toHaveBeenCalledWith(expectedRequest, expectedHeaders);
    }));
});
