import { Volume } from './volume';
import { Pricing } from './pricing';
import { Tenor } from './tenor';

export interface ContractSpecification {
    volume: Volume;
    pricing: Pricing;
    tenors: Tenor[];
    contractType: string;
}
