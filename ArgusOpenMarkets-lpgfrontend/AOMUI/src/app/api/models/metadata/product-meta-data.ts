import { Commodity } from './commodity';
import { ContractSpecification } from './contract-specification';

export interface IProductMetaData {
    productId: number;
    productName: string;
    productTitle: string;
    isInternal: boolean;
    hasAssessment: boolean;
    status: number;
    commodity: Commodity;
    contractSpecification: ContractSpecification;
    bidAskStackNumberRows: number;
    allowNegativePriceForExternalDeals: boolean;
    displayOptionalPriceDetail: boolean;
    contentStreamIds: any[];
    locationLabel: string;
    deliveryPeriodLabel: string;
    displayOrder: number;
    coBrokering: boolean;
}
