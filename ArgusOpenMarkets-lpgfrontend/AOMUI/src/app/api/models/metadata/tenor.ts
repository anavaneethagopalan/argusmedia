import { DeliveryLocation } from './delivery-location';

export interface Tenor {
    deliveryLocation: DeliveryLocation;
    deliveryStartDate: string;
    deliveryEndDate: string;
    rollDate: string;
    id: number;
    minimumDeliveryRange: number;
    defaultStartDate: string;
    defaultEndDate: string;
}
