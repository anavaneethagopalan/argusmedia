export interface DeliveryLocation {
    id: number;
    name: string;
    dateCreated: Date;
}
