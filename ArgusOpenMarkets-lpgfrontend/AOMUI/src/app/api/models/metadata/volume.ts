export interface Volume {
    default: number;
    minimum: number;
    maximum: number;
    increment: number;
    decimalPlaces: number;
    volumeUnitCode: string;
    commonQuantities: any[];
}
