export interface Commodity {
    id: number;
    name: string;
    enteredByUserId: number;
    lastUpdatedUserId: number;
    dateCreated: Date;
    lastUpdated: Date;
}
