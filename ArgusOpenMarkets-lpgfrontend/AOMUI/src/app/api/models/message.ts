export interface IMessage {
    messageType: string;
    messageAction: string;
    messageBody?: any;
}