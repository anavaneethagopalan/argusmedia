import { Privileges } from './privileges';


export interface ProductPrivilege {
    productId: number;
    displayOrder: number;
    name: string;
    hasAssessment: boolean;
    privileges: Privileges;
}
