import { UserOrganisation } from './user-organisation';
import { ProductPrivilege } from './product-privilege';
import { SystemPrivileges } from './system-privileges';


export interface IUser {
    id: number;
    username: string;
    name: string;
    email: string;
    organisationId: number;
    isBlocked: boolean;
    isActive: boolean;
    isDeleted: boolean;
    token?: any;
    telephone: string;
    title?: any;
    argusCrmUsername?: any;
    userOrganisation: UserOrganisation;
    productPrivileges: ProductPrivilege[];
    systemPrivileges: SystemPrivileges;
}
