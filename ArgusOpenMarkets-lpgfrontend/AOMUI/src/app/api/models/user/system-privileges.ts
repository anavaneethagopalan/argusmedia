export interface SystemPrivileges {
    privileges: string[];
    defaultRoleGroup?: any;
    organisationType: number;
}
