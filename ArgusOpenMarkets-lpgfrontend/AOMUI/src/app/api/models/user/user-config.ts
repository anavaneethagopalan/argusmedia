import { IProductWidget } from './productwidget';

export interface IUserConfig {
    productWidgets: IProductWidget[];    
}

export class ProductWidget implements IProductWidget {
    productId: number;
    numberOfRows: number;

}


/*
const UserConfig: IUserConfig = {
    
    productWidgets: [{
          productId: 1,
          NumberOfRows: 6
        },
        {
          productId: 2,
          NumberOfRows: 3
        },
        {
          productId: 3,
          NumberOfRows: 12
        }
      ],          
    };
    */