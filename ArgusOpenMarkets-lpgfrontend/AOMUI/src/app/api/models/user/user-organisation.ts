export interface UserOrganisation {
    id: number;
    name: string;
    organisationType: string;
    shortCode: string;
    legalName: string;
    address?: string;
    email: string;
    dateCreated?: Date;
    description?: string;
    isDeleted: boolean;
    isBroker: boolean; //populated in reducer
}
