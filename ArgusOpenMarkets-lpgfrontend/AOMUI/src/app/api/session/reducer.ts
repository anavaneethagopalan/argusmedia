import { ILoginSessionState, LoginSessionAPIAction, ILoginSession } from './models';
import { Action } from 'redux';
import { LoginSessionAPIActions } from './actions';
import { IUser } from '../models/user/user';
import { IAppState } from '../../store/model';

const INITIAL_STATE_LOGIN: ILoginSessionState = {
    session: null,
    loading: false,
    error: null,
};

const sessionfromPayload = (record: any): ILoginSession => ({
    IsAuthenticated: record.isAuthenticated,
    EndPointAvailable: true,
    sessionToken: record.sessionToken,
    userId: record.userId,
    token: record.token,
    credentialType: record.credentialType,
    serverMessage: record.message,
    tempPassword: (record.credentialType === 'T') ? record.credential.password : null,
    User: userfromPayload(record)
});

const setLoginSuccessfromPayload = (state: ILoginSession, record: any): ILoginSession => (
    { ...state, LoginSuccess: (record.credentialType === 'P' && record.isAuthenticated) }
);

const userfromPayload = (record: any): IUser => ({
    ...<IUser>record.user,
    userOrganisation: {
        ...record.user.userOrganisation,
        isBroker: record.user.userOrganisation.organisationType === "Brokerage"
    }
});

export function createLoginSessionAPIReducer() {
    return function sessionReducer(state: ILoginSessionState = INITIAL_STATE_LOGIN,
        a: Action): ILoginSessionState {

        const action = a as LoginSessionAPIAction;

        switch (action.type) {
            case LoginSessionAPIActions.LOGIN_USER_STARTED:
                return {
                    ...state,
                    session: null,
                    loading: true,
                    error: null,
                };
            case LoginSessionAPIActions.LOGIN_API_CONNECTION_ESTABLISHED:
                return {
                    ...state,
                    session: sessionfromPayload(action.payload),
                    loading: false,
                    error: null,
                };
            case LoginSessionAPIActions.LOGIN_USER_SUCCEEDED:
                return {
                    ...state,
                    session: setLoginSuccessfromPayload(sessionfromPayload(action.payload), action.payload),
                    loading: false,
                    error: null,
                };
            case LoginSessionAPIActions.LOGIN_USER_FAILED:
                return {
                    ...state,
                    session: null,
                    loading: false,
                    error: action.error,
                };
            case LoginSessionAPIActions.CLEAR_USER_SESSION:
                return {
                    ...state,
                    session: null,
                    loading: false,
                    error: null,
                };
            default:
                return state;
        }
    };
}
