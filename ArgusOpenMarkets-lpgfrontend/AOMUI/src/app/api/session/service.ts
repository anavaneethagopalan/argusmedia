import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { ILoginSession, fromServer, Payload } from './models';
import { ConfigService } from '../../shared/config.service';
import { Router } from '@angular/router';
import { IAppState } from '../../store/model';
import {
  UNAUTHORISED_STATUS_CODE, TIMEOUT_STATUS_CODE, TIMEOUT_STATUS_TEXT,
  mockresponse_loginapi$
} from './constants';
import { UserCookieService } from '../../login/user-cookie.service';
import { MessageDistributionService } from '../../shared/message-distribution.service';

@Injectable()
export class LoginSessionAPIService {
  constructor(private http: Http, private configService: ConfigService, private router: Router, private userCookieService: UserCookieService,
    private messageDistributionService: MessageDistributionService) { }

  loginUser = (payload: Payload): Observable<ILoginSession> => {

    const wsUrl = this.configService.webApi + '/login';

    return this.http.post(wsUrl, payload.credential)
      .map(resp => resp.json())
      .do(records => console.log('SessionAPIService loginUser http response', records))
      .do(records => console.log('SessionAPIService loginUser map fromServer ', records))
      .switchMap(resp => (!resp.isAuthenticated)
        ? Observable.throw({
          status: UNAUTHORISED_STATUS_CODE,
          statusText: resp.message.replace('[[EMAILADDRESS]]', 'AOMSupport@argusmedia.com')
        })
        : Observable.of({ ...resp, credential: payload.credential }))
      .timeoutWith(10000, Observable.throw({
        status: TIMEOUT_STATUS_CODE,
        statusText: TIMEOUT_STATUS_TEXT
      }));
  }

  logoutUser = () => this.messageDistributionService.close();

  navigateToAuthorizedUrl = (session: ILoginSession) => {
    if (session.credentialType === 'P') {
      this.router.navigateByUrl('/');
    } else {
      this.router.navigateByUrl('/login/ResetPassword');
    }
  }

  navigateToLogin = () => {
    this.router.navigateByUrl('/login');
  }

  registerEmail = (payload: Payload): Observable<ILoginSession> => {
    return Observable.of(mockresponse_loginapi$).delay(8000)
      .timeoutWith(10000, Observable.throw(
        {
          status: TIMEOUT_STATUS_CODE,
          statusText: TIMEOUT_STATUS_TEXT
        }));
  }

  updateUserCookies = (payload: Payload): void => {
    if (payload.credential && payload.credential.rememberMe) {
      this.userCookieService.updateUsernameCookie(payload.credential.username);
    } else {
      this.userCookieService.deleteUsernameCookie();
    }
  }
}
