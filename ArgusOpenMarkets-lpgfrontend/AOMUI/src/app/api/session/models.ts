import { Observable } from 'rxjs/Observable';
import { IUser } from '../models/user/user';

export interface ICredential {
    username: string;
    password?: string;
    loginSource?: string;
    url?: string;
    rememberMe: boolean;
}
export type Payload = ILoginSession;

export interface LoginSessionAPIAction {
    type: string;
    payload: Payload;
    error?: any;
}

export interface ILoginSession {
    IsAuthenticated?: boolean;
    EndPointAvailable?: boolean;
    LoginSuccess?: boolean;
    ShouldChangePassword?: boolean;
    IsIncorrectlogin?: boolean;
    sessionToken?: string;
    token?: string;
    userId?: number;
    credentialType?: string;
    serverMessage?: string;
    tempPassword?: string;
    credential?: ICredential;
    User?: IUser;
}

export interface ILoginSessionState {
    session: ILoginSession;
    loading: boolean;
    error: any;
}

export const fromServer = (record: any): Observable<ILoginSession> => {
    return Observable.of<ILoginSession>({
    IsAuthenticated: record.isAuthenticated,
    sessionToken: record.sessionToken,
    userId: record.userId,
    token: record.token
    });
};
