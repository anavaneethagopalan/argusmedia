import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';

import { ILoginSession, Payload, LoginSessionAPIAction } from './models';

@Injectable()
export class LoginSessionAPIActions {
    static readonly LOGIN_USER = 'LOGIN_USER';
    static readonly LOGIN_USER_STARTED = 'LOGIN_USER_STARTED';
    static readonly LOGIN_API_CONNECTION_ESTABLISHED = 'LOGIN_API_CONNECTION_ESTABLISHED';
    static readonly LOGIN_API_CONNECTION_TIMEOUT = 'LOGIN_API_CONNECTION_TIMEOUT';
    static readonly LOGIN_USER_SUCCEEDED = 'LOGIN_USER_SUCCEEDED';
    static readonly LOGIN_USER_FAILED = 'LOGIN_USER_FAILED';
    static readonly LOGOUT_USER = 'LOGOUT_USER';
    static readonly CLEAR_USER_SESSION= 'CLEAR_USER_SESSION';
    @dispatch()
    loginUser = (payload: Payload): LoginSessionAPIAction => ({
        type: LoginSessionAPIActions.LOGIN_USER,
        payload: payload,
    })

    loginUserStarted = (): LoginSessionAPIAction => ({
        type: LoginSessionAPIActions.LOGIN_USER_STARTED,
        payload: null,
    })

    loginUserSucceeded = (payload: Payload): LoginSessionAPIAction => ( {
        type: LoginSessionAPIActions.LOGIN_USER_SUCCEEDED,
        payload: payload
    })

    loginAPIConnectionEstablished = (payload: Payload): LoginSessionAPIAction => ( {
        type: LoginSessionAPIActions.LOGIN_API_CONNECTION_ESTABLISHED,
        payload: payload
    })

    loginUserFailed = (error): LoginSessionAPIAction => ( {
        type: LoginSessionAPIActions.LOGIN_USER_FAILED,
        payload: null,
        error
    })

    @dispatch()
    logoutUser = (): LoginSessionAPIAction => ( {
        type: LoginSessionAPIActions.LOGOUT_USER,
        payload: null,
    })

    clearUserSession = (): LoginSessionAPIAction => ( {
        type: LoginSessionAPIActions.CLEAR_USER_SESSION,
        payload: null,
    })
}
