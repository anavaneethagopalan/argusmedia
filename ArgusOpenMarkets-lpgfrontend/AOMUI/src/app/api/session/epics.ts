import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import { createEpicMiddleware, Epic } from 'redux-observable';

import { LoginSessionAPIActions } from './actions';
import { LoginSessionAPIService } from './service';
import { ILoginSessionState, LoginSessionAPIAction } from './models';
import { IAppState } from '../../store/model';

@Injectable()
export class SessionAPIEpics {

    constructor(
        private loginservice: LoginSessionAPIService,
        private loginactions: LoginSessionAPIActions,
        private router: Router
    ) { }

    public createLoginEpic() {
        return createEpicMiddleware(this.createLoadLoginSessionEpic());
    }

    public createLogoutEpic() {
        return createEpicMiddleware(this.createLoadLogoutSessionEpic());
    }

    private createLoadLoginSessionEpic(): Epic<LoginSessionAPIAction, IAppState> {
        return (action$, store) => action$
            .ofType(LoginSessionAPIActions.LOGIN_USER)
            .switchMap(({ payload }) => this.loginservice.loginUser(payload)
                .map(data => this.loginactions.loginAPIConnectionEstablished(data))
                .do(_ => this.loginservice.navigateToAuthorizedUrl(_.payload))
                .map(_ => this.loginactions.loginUserSucceeded(_.payload))
                .do(_ => this.loginservice.updateUserCookies(_.payload))
                .catch(error => Observable.of(this.loginactions.loginUserFailed(this.handleError(error))))
                .startWith(this.loginactions.loginUserStarted()));
    }

    private createLoadLogoutSessionEpic(): Epic<LoginSessionAPIAction, IAppState> {
        return (action$, store) => action$
            .ofType(LoginSessionAPIActions.LOGOUT_USER)
            .map(_ => this.loginactions.clearUserSession())
            .do(_ => {
                this.loginservice.logoutUser();
                this.loginservice.navigateToLogin();
            });
    }

    private handleError = (error) => {
        return (error.status === 0 && error.statusText === '') ? { ...error, statusText: 'Unknown error occured' } : error;
    }
}
