import { ILoginSession } from '../../api/session/models';

export const UNAUTHORISED_STATUS_CODE = 401;
export const TIMEOUT_STATUS_CODE = 408;
export const TIMEOUT_STATUS_TEXT = `Timeout has occured. Please try again. If this persists, please contact AOM Support.
                                    Email: AOMSupport@argusmedia.com. Tel: +44 020 7199 9430`;

export const mockresponse_loginapi$: ILoginSession = {
    IsAuthenticated: true,
    EndPointAvailable: true,
    ShouldChangePassword: true,
    IsIncorrectlogin: true,
    sessionToken: 'abcdefgh',
    token: 'dummytoken',
    userId: 1234,
    credentialType: 'P',
    serverMessage: 'login successful',
    User: null
};

// Not used
// export const mockresponse_registeremail_api$ = {
//     IsAuthenticated: true,
//     EndPointAvailable: true,
//     ShouldChangePassword: true,
//     IsIncorrectlogin: true,
//     sessionToken: 'abcdefgh',
//     token: 'dummytoken',
//     userId: 1234,
//     credentialType: 'P',
//     message: `Thank you for your request. An email has been sent to you with a link to a page where you can change your password.`,
//     User: null
// };

