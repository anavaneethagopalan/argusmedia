import { IOrder } from './models';
import { SET_ORDERS } from './actions';

export function orders(state: IOrder[] = [], action){
    switch(action.type) {
        case SET_ORDERS:
            let index = state.findIndex(o => o.id === action.order.id);

            if(index > -1) {
                // order exists
                return [
                    ...state.splice(index, 1),
                    action.order
                ];
            }
            else {
                return [
                    ...state,
                    action.order
                ];
            }
        
        default:
            return state;
    }
}

export function priceBases(state: IOrder[] = [], action) {
    return state;
}