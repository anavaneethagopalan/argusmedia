import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';

import { IOrder } from './models';

export const SET_ORDERS = 'SET_ORDERS';

@Injectable()
export class OrderAPIActions {

    @dispatch()
    setOrders = function (order: IOrder) {
        return {
            type: SET_ORDERS,
            order
        }
    }
}