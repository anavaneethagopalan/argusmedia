import { IMessage } from '../models/message';

export interface IOrderMessage {
    order: IOrder;
    eventName: string;
    message: IMessage;
}

export interface IOrder {
    id: number;
    lastUpdated: Date;
    orderType: 'BID' | 'ASK';
    quantity: number | string;
    notes: string;
    productTenors: any[];//TODO
    brokerOrganisationId: number;
    brokerShortCode: string;
    brokerOrganisation: any;//TODO
    orderStatus: string;
    enteredByUserId: string;
    brokerRestriction: string;
    coBrokering: boolean;
    orderPriceLines: IOrderPriceLine[];
    productId : number;
    principalOrganisationId: number;
    principalOrganisationShortCode: string;
}

export interface IOrderPriceLine {
    id: number;
    orderId: number;
    orderStatusCode: string;
    priceLine: IPriceLine;
}

export interface IPriceLine {
    id: number;
    sequenceNo: number;
    priceLinesBases: IPriceLineBasis[];
}

export interface IPriceLineBasis {
    id: number;
    description: string;
    sequenceNo: number;
    percentageSplit: number;
    priceLineBasisValue: number;
    pricingPeriodFrom: Date;
    pricingPeriodTo: Date;
    priceBasisId: number;
    //priceBasis: IPriceBasis;
}

export interface IPriceBasis {
    id: number;
    shortName: string;
    name: string;
    //TODO
}

// export interface IPriceBasis {
//     id: number;
//     shortName: string;
//     name: string;
//     priceMinimum: number;
//     priceMaximum: number;
//     priceIncrement: number;
//     priceDecimalPlaces: number;
//     priceSignificantFigures: number;
// }