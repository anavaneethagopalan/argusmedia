import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { createEpicMiddleware, Epic } from 'redux-observable';
import { of } from 'rxjs/observable/of';

import { News, GetNewsBodyAPIAction } from './models';
import { NewsService } from './service';
import { NewsAPIActions, GET_NEWS_BODY } from './actions';
import { IAppState } from '../../store/model';

@Injectable()
export class NewsEpic {

    constructor(
        private newsService: NewsService,
        private newsAPIActions: NewsAPIActions,
        private router: Router
    ) { }

    public createEpic() {
        return createEpicMiddleware(this.createGetNewsBodySessionEpic());
    }

    private createGetNewsBodySessionEpic(): Epic<GetNewsBodyAPIAction, IAppState> {
        return (action$, store) => action$
            .ofType(GET_NEWS_BODY)
            .switchMap(({ news }) => this.newsService.webServiceNewsStory(news)
                .map(_ => this.newsAPIActions.getNewsBodySucceeded(_))
                .catch(error => of(this.newsAPIActions.getNewsBodyFailed(this.handleError(error))))
                .startWith(this.newsAPIActions.getNewsBodyStarted()));
    }

    private handleError = (error) => {
        return (error.status === 0 && error.statusText === '') ? { ...error, statusText: 'Unknown error occured' } : error;
    }
}
