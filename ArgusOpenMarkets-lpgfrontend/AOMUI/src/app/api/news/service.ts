import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../shared/config.service';

import { IAppState } from '../../store/model';
import { NewsAPIActions } from './actions';
import { NewsMessage, News } from './models';

import { MessageDistributionService } from '../../shared/message-distribution.service';

@Injectable()
export class NewsService {

  constructor(private messageDistributionService: MessageDistributionService, 
    private store: NgRedux<IAppState>, 
    private http: Http, 
    private configService: ConfigService,
    private newsAPIActions: NewsAPIActions,
  )
  {}

  public newsMessageRecieved(newsMessage: NewsMessage) {
    this.store.dispatch(this.newsAPIActions.setNews(newsMessage.news));
  }

  public getPermissionedBrokers(productId: number){
    

  }

  public webServiceNewsStory(news: News): Observable<any> {
    let cmsId = news.cmsId;
    let userSession = this.configService.getUserSession();

    let myHeaders = new Headers();
    myHeaders.append('token', userSession.session);
    myHeaders.append('userId', userSession.userId);

    let myParams: URLSearchParams = new URLSearchParams();
    myParams.set('id', cmsId);
    myParams.set('userId', userSession.userId);

    let options = new RequestOptions({ headers: myHeaders, params: myParams });
    return this.http
        .get(this.configService.webApi + '/newsStory', options)
        .map((response) => response.json())
        .switchMap(resp => (resp.Error)
            ? Observable.throw({
                statusText: resp.message
            })
            : Observable.of(resp));
  }
}
