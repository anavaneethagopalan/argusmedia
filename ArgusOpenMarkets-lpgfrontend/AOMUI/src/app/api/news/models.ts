    import { IMessage } from '../models/message';
    
    export interface News {
        cmsId: string;
        headline: string;
        publicationDate: Date;
        itemUrl: string;
        story: string;
        fullStory: string;
        newsType: string;
        isFree: boolean;
        contentStreams: number[];
        commodityId: number;
    }

    export interface NewsMessage {
        news: News;
        eventName: string;
        message: IMessage;
    }

    export interface INewsState{ 
        newsStories: News[];
        newsStory: News;
    }

    export interface GetNewsBodyAPIAction {
        type: string;
        news: News;
        error?: any;
    }