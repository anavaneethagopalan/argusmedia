import {
    SET_NEWS, GET_NEWS_BODY, GET_NEWS_BODY_SUCCEEDED, GET_NEWS_BODY_FAILED, DELETE_NEWS, NO_NEWS_YET
} from './actions';

import { News } from './models';

function updateNewsProperty(state, action, news: News) {
    
    // News story already exists
    let index = -1;
    for(var i=0; i<state.newsStories.length; i++){
        if(state.newsStories[i].cmsId === action.news.cmsId){
            index = i;
            break;
        }
    }

    if(index > -1){
        return { ...state, newsStories: [...state.newsStories.slice(0, index), ...state.newsStories.slice(index + 1), action.news]}
    }

    return { ...state, newsStories: [...state.newsStories, news] };
}

function updateNewsBody(state, action, propertyName){

    return { ...state, newsStory: action.news };
}

function updateNewsBodyStory(state, newsStory, propertyName){

    let ns = { ... state.newsStory };
    ns.story = newsStory;
    return { ...state, newsStory: ns };
}

function deleteNewsStory(state, action, propertyName) {

    let index = -1;
    for(var i=0; i<state.newsStories.length; i++){
        if(state.newsStories[i].cmsId === action.cmsId){
            index = i;
            break;
        }
    }

    if(index > -1){
        return { ...state, newsStories: [...state.newsStories.slice(0, index), ...state.newsStories.slice(index + 1)]};
    }
}

function addNoNewsStories(state, action){
    if (state.newsStories.length === 0){
        // Stil no news stories.
        let noNewsStory: News = {
            cmsId: '-1',
            headline: 'There is currently no news availalbe',
            publicationDate: new Date(), 
            itemUrl: '', 
            story: 'We are working on generating some news for your market',
            fullStory: 'We are working on generating some news for your market.  If this problem persists then please contact support.',
            newsType: '',
            isFree: true,
            contentStreams: null,
            commodityId: -1
        };
        return updateNewsProperty(state, action, noNewsStory )
    }
}

export function news(state = [], action) {
    switch (action.type) {
        case SET_NEWS:
            return updateNewsProperty(state, action, action.news);
        case GET_NEWS_BODY: 
            return updateNewsBody(state, action, 'news.news');
        case GET_NEWS_BODY_SUCCEEDED: 
            return updateNewsBodyStory(state, action.news.story, 'news.news');
        case GET_NEWS_BODY_FAILED: 
            return updateNewsBodyStory(state, action, 'news.news');
        case DELETE_NEWS: 
            return deleteNewsStory(state, action, 'news.news');
        case NO_NEWS_YET:
            return addNoNewsStories(state, action);
        default:
            return state;
    }
}