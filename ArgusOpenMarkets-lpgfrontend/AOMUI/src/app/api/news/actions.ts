import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';
import { News } from './models';

export const SET_NEWS = 'SET_NEWS';
export const GET_NEWS_BODY = 'GET_NEWS_BODY';
export const GET_NEWS_BODY_STARTED = 'GET_NEWS_BODY_STARTED';
export const GET_NEWS_BODY_SUCCEEDED = 'GET_NEWS_BODY_SUCCEEDED';
export const GET_NEWS_BODY_FAILED = 'GET_NEWS_BODY_FAILED';
export const DELETE_NEWS = 'DELETE_NEWS';
export const NO_NEWS_YET = 'NO_NEWS_YET';


@Injectable()
export class NewsAPIActions {

    @dispatch()
    setNews = function (news: News) {
        return {
            type: SET_NEWS,
            news
        };
    }

    getNewsBody = function(news: News) {
            return {
                type: GET_NEWS_BODY, 
                news
            };
        }

    getNewsBodyStarted = function(){
        return { 
            type: GET_NEWS_BODY_STARTED, 
            news: null
        }
    }

    getNewsBodySucceeded(news: News){
        return {
            type: GET_NEWS_BODY_SUCCEEDED, 
            news
        };
    }

    getNewsBodyFailed(error){
        return {
            type: GET_NEWS_BODY_FAILED, 
            news: null
        };
    }

    deleteNews(cmsId){
        return { 
            type: DELETE_NEWS, 
            cmsId: cmsId
        }
    }

    noNewsYet(){
        return { 
            type: NO_NEWS_YET
        }
    }
}