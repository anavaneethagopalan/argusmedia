import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CookieService } from 'ng2-cookies';

import { SessionAPIEpics } from './session/epics';
import { LoginSessionAPIActions } from './session/actions';
import { LoginSessionAPIService } from './session/service';
import { ForgottenPasswordEpic } from './forgotten-password/epics';
import { ForgottenPasswordSessionAPIActions } from './forgotten-password/actions';
import { ForgottenPasswordSessionAPIService } from './forgotten-password/service';
import { ResetPasswordEpic } from './reset-password/epics';
import { ResetPasswordSessionAPIActions } from './reset-password/actions';
import { ResetPasswordSessionAPIService } from './reset-password/service';
import { NewsEpic } from './news/epics';
import { NewsAPIActions } from './news/actions';
import { NewsService } from './news/service';
import { CreateOrderEpic } from './create-order/epics';
import { CreateOrderAPIActions } from './create-order/actions';
import { CreateOrderAPIService } from './create-order/service';

import { UserCookieService } from '../login/user-cookie.service';

const LOGIN_SESSION = [SessionAPIEpics, LoginSessionAPIActions, LoginSessionAPIService];
const FORGOTTEN_PASSWORD = [ForgottenPasswordEpic, ForgottenPasswordSessionAPIActions, ForgottenPasswordSessionAPIService];
const RESET_PASSWORD = [ResetPasswordEpic, ResetPasswordSessionAPIActions, ResetPasswordSessionAPIService];
const NEWS = [NewsEpic, NewsAPIActions, NewsService];
const CREATE_ORDER = [CreateOrderEpic, CreateOrderAPIActions, CreateOrderAPIService];

const COOKIE_SERVICES = [CookieService, UserCookieService];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ...LOGIN_SESSION,
    ...FORGOTTEN_PASSWORD,
    ...RESET_PASSWORD,
    ...NEWS,
    ...CREATE_ORDER,
    ...COOKIE_SERVICES
  ]
})
export class ApiModule { }
