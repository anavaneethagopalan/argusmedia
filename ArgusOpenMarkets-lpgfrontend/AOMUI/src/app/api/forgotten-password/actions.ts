import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';
import { ForgottenPasswordSessionAPIAction } from './models';

export const FORGOTTEN_PASSWORD_INIT = 'FORGOTTEN_PASSWORD_INIT';
export const FORGOTTEN_PASSWORD = 'FORGOTTEN_PASSWORD';
export const FORGOTTEN_PASSWORD_STARTED = 'FORGOTTEN_PASSWORD_STARTED';
export const FORGOTTEN_PASSWORD_SUCCEEDED = 'FORGOTTEN_PASSWORD_SUCCEEDED';
export const FORGOTTEN_PASSWORD_FAILED = 'FORGOTTEN_PASSWORD_FAILED';

@Injectable()
export class ForgottenPasswordSessionAPIActions {


    @dispatch()
    forgottenPassword = (payload): ForgottenPasswordSessionAPIAction => ( {
        type: FORGOTTEN_PASSWORD,
        payload,
    })

    forgottenPasswordStarted = (): ForgottenPasswordSessionAPIAction => ({
        type: FORGOTTEN_PASSWORD_STARTED,
        payload: null,
    })

    forgottenPasswordSucceeded = (payload): ForgottenPasswordSessionAPIAction => ( {
        type: FORGOTTEN_PASSWORD_SUCCEEDED,
        payload: payload
    })

    forgottenPasswordFailed = (error): ForgottenPasswordSessionAPIAction => ( {
        type: FORGOTTEN_PASSWORD_FAILED,
        payload: null,
        error
    })
}
