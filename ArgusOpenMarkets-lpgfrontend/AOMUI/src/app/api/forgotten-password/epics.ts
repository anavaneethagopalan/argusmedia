import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { createEpicMiddleware, Epic } from 'redux-observable';
import { of } from 'rxjs/observable/of';

import { ForgottenPasswordSessionAPIActions, FORGOTTEN_PASSWORD } from './actions';
import { ForgottenPasswordSessionAPIService } from './service';
import { ForgottenPasswordSessionAPIAction } from './models';

import { IAppState } from '../../store/model';

@Injectable()
export class ForgottenPasswordEpic {

    constructor(
        private forgottenPasswordService: ForgottenPasswordSessionAPIService,
        private forgottenPasswordActions: ForgottenPasswordSessionAPIActions
    ) { }

    public createEpic() {
        return createEpicMiddleware(this.createLoadForgottenPasswordSessionEpic());
    }

    private createLoadForgottenPasswordSessionEpic(): Epic<ForgottenPasswordSessionAPIAction, IAppState> {
        return (action$, store) => action$
            .ofType(FORGOTTEN_PASSWORD)
            .switchMap(({ payload }) => this.forgottenPasswordService.webServiceForgottenPassword(payload)
                .map(_ => this.forgottenPasswordActions.forgottenPasswordSucceeded(_.payload))
                .catch(error => of(this.forgottenPasswordActions.forgottenPasswordFailed(this.handleError(error))))
                .startWith(this.forgottenPasswordActions.forgottenPasswordStarted()));
    }

    private handleError = (error) => {
        return (error.status === 0 && error.statusText === '') ? { ...error, statusText: 'Unknown error occured' } : error;
    }
}
