import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../shared/config.service';

@Injectable()
export class ForgottenPasswordSessionAPIService {

    constructor(private http: Http, private configService: ConfigService) { }

    public webServiceForgottenPassword(username: string): Observable<any> {
        console.log('webServiceForgottenPassword, username:', username);
        return this.http
            .post(this.configService.webApi + '/forgotPassword',
            {
                username,
                url: this.getUrl()
            })
            .map((response) => response.json())
            .switchMap(resp => (!resp.isAuthenticated)
                ? Observable.throw({
                    statusText: resp.message
                })
                : Observable.of(resp));
    }

    private getUrl(): string {
        return window.location.origin;
    }
}
