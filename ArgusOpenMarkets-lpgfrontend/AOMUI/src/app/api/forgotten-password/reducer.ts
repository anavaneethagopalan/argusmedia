import { ForgottenPasswordSessionAPIAction, IForgottenPasswordSessionState } from './models';
import { FORGOTTEN_PASSWORD_INIT, FORGOTTEN_PASSWORD_STARTED, FORGOTTEN_PASSWORD_SUCCEEDED, FORGOTTEN_PASSWORD_FAILED } from './actions';

const INITIAL_STATE_FORGOTTEN_PASSWORD: IForgottenPasswordSessionState = {
    success: false,
    loading: false,
    error: null,
};

export function forgottenPasswordAPIReducer(state: IForgottenPasswordSessionState = INITIAL_STATE_FORGOTTEN_PASSWORD,
    action: ForgottenPasswordSessionAPIAction): IForgottenPasswordSessionState {

    switch (action.type) {

        case FORGOTTEN_PASSWORD_INIT:
            return INITIAL_STATE_FORGOTTEN_PASSWORD;
        case FORGOTTEN_PASSWORD_STARTED:
            return {
                success: false,
                loading: true,
                error: null,
            };
        case FORGOTTEN_PASSWORD_SUCCEEDED:
            return {
                success: true,
                loading: false,
                error: null,
            };
        case FORGOTTEN_PASSWORD_FAILED:
            return {
                success: false,
                loading: false,
                error: action.error,
            };
        default:
            return state;
    }
}
