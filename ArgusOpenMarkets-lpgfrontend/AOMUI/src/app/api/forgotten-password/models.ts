export interface ForgottenPasswordSessionAPIAction {
    type: string;
    payload: any;
    error?: any;
}

export interface IForgottenPasswordSessionState {
    success: boolean;
    loading: boolean;
    error: any;
}
