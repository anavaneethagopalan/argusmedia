import { CreateOrderAPIAction, ICreateOrderState } from './models';
import {
    CREATE_ORDER_STARTED, CREATE_ORDER_SUCCEEDED, CREATE_ORDER_FAILED
} from './actions';

const INITIAL_STATE_CREATE_ORDER: ICreateOrderState = null;

export function createOrderAPIReducer(state: ICreateOrderState = INITIAL_STATE_CREATE_ORDER,
    action: CreateOrderAPIAction): ICreateOrderState {

    switch (action.type) {

        case CREATE_ORDER_STARTED:
            return {
                loading: true,
                error: null,
                config: null,
                order: null
            };
        case CREATE_ORDER_SUCCEEDED:
            return {
                loading: false,
                error: null,
                config: action.payload.createOrderConfig,
                order: action.payload.order
            };
        case CREATE_ORDER_FAILED:
            return {
                loading: false,
                error: action.error,
                config: null,
                order: null
            };
        default:
            return state;
    }
}
