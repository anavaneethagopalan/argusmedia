import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { createEpicMiddleware, Epic } from 'redux-observable';
import { of } from 'rxjs/observable/of';

import { CreateOrderAPIActions, CREATE_NEW_ORDER } from './actions';
import { CreateOrderAPIService } from './service';
import { CreateOrderAPIAction } from './models';

import { IAppState } from '../../store/model';

@Injectable()
export class CreateOrderEpic {

    constructor(
        private createOrderService: CreateOrderAPIService,
        private createOrderActions: CreateOrderAPIActions
    ) { }

    public createEpic() {
        return createEpicMiddleware(this.createCreateOrderEpic());
    }

    private createCreateOrderEpic(): Epic<CreateOrderAPIAction, IAppState> {
        return (action$, store) => action$
            .ofType(CREATE_NEW_ORDER)
            .switchMap(({ payload }) => this.createOrderService.createNewOrderState(payload)
                .map(createOrderState => this.createOrderActions.createOrderSucceeded(createOrderState))
                .catch(error => of(this.createOrderActions.createOrderFailed(error)))
                .startWith(this.createOrderActions.createOrderStarted()));
    }
}
