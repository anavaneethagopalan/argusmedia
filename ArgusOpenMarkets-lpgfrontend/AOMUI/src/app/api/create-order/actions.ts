import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';

import { CreateOrderAPIAction, CreateOrderResponse, CreateOrderRequest } from './models';

export const CREATE_NEW_ORDER = 'CREATE_NEW_ORDER';
export const CREATE_ORDER_STARTED = 'CREATE_ORDER_STARTED';
export const CREATE_ORDER_SUCCEEDED = 'CREATE_ORDER_SUCCEEDED';
export const CREATE_ORDER_FAILED = 'CREATE_ORDER_FAILED';

@Injectable()
export class CreateOrderAPIActions {

    @dispatch()
    createNewOrder = (productId: number, orderType: 'BID' | 'ASK'): CreateOrderAPIAction => {
        const request: CreateOrderRequest = {
            productId,
            orderType
        };
        return {
            type: CREATE_NEW_ORDER,
            payload: request
        };
    }

    createOrderStarted = (): CreateOrderAPIAction => ({
        type: CREATE_ORDER_STARTED,
        payload: null
    })

    createOrderSucceeded = (response: CreateOrderResponse) => ({
        type: CREATE_ORDER_SUCCEEDED,
        payload: {
            createOrderConfig: response.config,
            order: response.order,
        }
    })

    createOrderFailed = (error): CreateOrderAPIAction => ({
        type: CREATE_ORDER_FAILED,
        payload: null,
        error
    })
}
