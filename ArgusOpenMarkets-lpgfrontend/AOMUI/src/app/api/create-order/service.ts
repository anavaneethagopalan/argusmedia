import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { NgRedux } from '@angular-redux/store';

import { Observable } from 'rxjs/Observable';
import { forkJoin } from "rxjs/observable/forkJoin";

import { ConfigService } from '../../shared/config.service';
import { TradersService } from '../../shared/traders.service';

import { IAppState } from '../../store/model';
import { IProductDefinition } from '../../products/models/product';
import { CreateOrderConfig, CreateOrderRequest, CreateOrderResponse, ITraderDropDown } from './models';
import { IOrder } from '../orders/models';

@Injectable()
export class CreateOrderAPIService {

    constructor(private http: Http,
        private configService: ConfigService,
        private store: NgRedux<IAppState>,
        private tradersService: TradersService) { }

    public createNewOrderState(payload: CreateOrderRequest): Observable<CreateOrderResponse> {
        // TODO: API call to get product defintion and principles/brokers
        const productDefintion = this.store.getState().products.find(p => p.productId === payload.productId).definition;


        let traders = this.tradersService.getPermissionedPrincipalsOrBrokers('T', productDefintion.productId, payload.orderType);
        let realProductDefinition = this.configService.getProductDefinitionConfig(productDefintion.productId);

        return forkJoin([realProductDefinition, traders]).map(results => {
            return this.buildCreateOrderState(results[0], results[1], payload.orderType);
        }).catch((error) => Observable.throw(error));
    }

    public buildCreateOrderState(productDefintion: IProductDefinition, traders: ITraderDropDown[], orderType: 'BID' | 'ASK'): CreateOrderResponse {
        // Build create order state from productDefintion
        const minDeliveryPeriod = new Date(2017, 11, 3);
        const maxDeliveryPeriod = new Date(2017, 11, 28);
        const minimumDeliveryRange = 5;
        const createOrderConfig: CreateOrderConfig = {
            productId: productDefintion.productId,
            productName: productDefintion.productName,
            quantity: {
                decimalPlaces: 2,
                increment: 50,
                default: 150,
                minimum: 0,
                maximum: 3000,
                volumeUnitCode: 't',
                quantityOptions: [
                    {
                        displayOrder: 10,
                        value: 200
                    },
                    {
                        displayOrder: 20,
                        value: 400
                    },
                    {
                        displayOrder: 30,
                        value: 750
                    },
                    {
                        displayOrder: 50,
                        value: 'ToT'
                    }
                ]
            },
            deliveryPeriod: {
                // deliveryPeriodOptions: [{
                //     id: 0,
                //     name: 'Delivery option 1'
                // },
                // {
                //     id: 1,
                //     name: 'Delivery option 2'
                // },
                // {
                //     id: 2,
                //     name: 'Delivery option 3'
                // }]
                defaultStartDate: new Date(2017, 11, 6),
                minStartDate: minDeliveryPeriod,
                maxStartDate: this.subractDays(maxDeliveryPeriod, minimumDeliveryRange - 1),
                defaultEndDate: new Date(2017, 11, 13),
                minEndDate: this.addDays(minDeliveryPeriod, minimumDeliveryRange - 1),
                maxEndDate: maxDeliveryPeriod,
                minimumDeliveryRange
            },
            deliveryLocation: {
                deliveryLocationOptions: [{
                    id: 0,
                    name: 'Delivery location 1'
                },
                {
                    id: 1,
                    name: 'Delivery location 2'
                }]
            },
            trader: { traders: traders, coBrokeringEnabled: productDefintion.coBrokering },
            metaData: [{
                labelText: 'Dropdown example',
                metaDataOptions: []
            },
            {
                labelText: 'Textbox example'
            },
            {
                labelText: 'Dropdown example 2',
                metaDataOptions: []
            }],
            pricing: null,
            notes: null,
            buttonActions: null
        };

        const order: IOrder = {
            id: -1,
            lastUpdated: new Date,
            orderType,
            quantity: createOrderConfig.quantity.default,
            notes: null,
            productTenors: null,
            brokerOrganisationId: -1,
            brokerOrganisation: null,
            brokerShortCode: '',
            orderStatus: null,
            enteredByUserId: null,
            brokerRestriction: null,
            coBrokering: null,
            orderPriceLines: null,
            productId: createOrderConfig.productId,
            principalOrganisationId: -1,
            principalOrganisationShortCode: ''
        };
        return { config: createOrderConfig, order };
    }

    private addDays(date: Date, days: number): Date {
        const newDate = new Date(date);
        newDate.setDate(newDate.getDate() + days);
        return newDate;
    }
    private subractDays(date: Date, days: number): Date {
        const newDate = new Date(date);
        newDate.setDate(newDate.getDate() - days);
        return newDate;
    }
}
