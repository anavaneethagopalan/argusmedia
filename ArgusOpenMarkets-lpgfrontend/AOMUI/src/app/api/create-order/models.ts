import { IOrder } from '../orders/models';

export interface CreateOrderAPIAction {
    type: string;
    payload: any;
    error?: any;
}

export interface ICreateOrderState {
    loading: boolean;
    error: any;
    config: CreateOrderConfig;
    order: IOrder;
}

export interface CreateOrderConfig {
    productId: number;
    productName: string;
    quantity: QuantityConfig;
    deliveryPeriod: DeliveryPeriodDateRange | DeliveryPeriodDropdown;
    deliveryLocation: DeliveryLocationDropdown;
    trader: ITraderConfig;
    metaData: (MetaDataDropdown | MetaDataText)[];
    pricing: any;
    notes: NoteDetails;
    buttonActions: ButtonActions[];
}

export interface QuantityConfig {
    default: number;
    minimum: number;
    maximum: number;
    increment: number;
    decimalPlaces: number;
    volumeUnitCode: string;
    quantityOptions: QuantityOption[];
}

export interface QuantityOption {
    value: number | string;
    displayOrder: number;
}

export interface DeliveryPeriodDateRange {
    defaultStartDate: Date;
    minStartDate: Date;
    maxStartDate: Date;
    defaultEndDate: Date;
    minEndDate: Date;
    maxEndDate: Date;
    minimumDeliveryRange: number;
}

export interface DeliveryPeriodDropdown {
    deliveryPeriodOptions: DropdownOption[];
}

export function isDeliveryPeriodDropdown(deliveryPeriod: DeliveryPeriodDropdown | DeliveryPeriodDateRange): deliveryPeriod is DeliveryPeriodDropdown {
    return (<DeliveryPeriodDropdown>deliveryPeriod).deliveryPeriodOptions !== undefined;
}

export interface DeliveryLocationDropdown {
    deliveryLocationOptions: DropdownOption[];
}

export interface DropdownOption {
    id: number;
    name: string;
}

export interface ITraderConfig{ 
    traders: ITraderDropDown[], 
    coBrokeringEnabled: boolean
}

export interface ITraderDropDownOrganisation{
    id: number, 
    name: string, 
    organisationType: number, 
    shortCode: string, 
    legalName: string, 
    brokerRestriction: number
}

export interface ITraderDropDown {
    organisation: ITraderDropDownOrganisation,
    canTrade: boolean
}

// export interface BrokerDropdown {
//     displayText: string;
//     brokerOptions: DropdownOption[];
//     allowOtherBrokers: boolean;
// }

// export interface PrincipalDropdown {
//     displayText: string;
//     princpalOptions: DropdownOption[];
// }

// export function isPrincipalDropdown(trader: PrincipalDropdown | BrokerDropdown): trader is PrincipalDropdown {
//     return (<PrincipalDropdown>trader).princpalOptions !== undefined;
// }

export interface MetaDataDropdown {
    labelText: string;
    metaDataOptions: DropdownOption[];
}

export interface MetaDataText {
    labelText: string;
}

export function isMetaDataDropdown(metaData: MetaDataDropdown | MetaDataText): metaData is MetaDataDropdown {
    return (<MetaDataDropdown>metaData).metaDataOptions !== undefined;
}

export interface NoteDetails {
    maxChars: number;
}

export interface ButtonActions {
    buttonText: string;
    validToInvoke: boolean;
    actionToDispatch: string;
}

export interface CreateOrderRequest {
    productId: number;
    orderType: 'BID' | 'ASK';
    order?: IOrder;
}

export interface CreateOrderResponse {
    config: CreateOrderConfig;
    order: IOrder;
}