import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgRedux } from '@angular-redux/store';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/lib/testing';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { BsDropdownModule } from 'ngx-bootstrap';

import { ShellComponent } from './shell.component';
import { MessageDistributionService } from '../shared/message-distribution.service';
import { ConfigService } from '../shared/config.service';
import { LoggerService } from '../shared/logger.service';
import { LoginSessionAPIActions } from '../api/session/actions';

describe('ShellComponent', () => {
  let component: ShellComponent;
  let fixture: ComponentFixture<ShellComponent>;
  let navigateSpy;

  const messageDistributionServiceStub = {
    isConnected: () => true,
    get disconnect$() { return Observable.of({}); }
  };
  const loggerServiceStub = {
    info: () => { }
  };

  const loginSessionsActionsStub = {
    logoutUser: () => undefined
  };

  const configServiceStub = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, BsDropdownModule.forRoot()
      ],
      declarations: [ShellComponent],
      providers: [
        { provide: MessageDistributionService, useValue: messageDistributionServiceStub },
        { provide: LoggerService, useValue: loggerServiceStub },
        { provide: LoginSessionAPIActions, useValue: loginSessionsActionsStub },
        { provide: NgRedux, useValue: MockNgRedux.getInstance() },
        { provide: ConfigService, useValue: configServiceStub }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShellComponent);
    component = fixture.componentInstance;
    navigateSpy = spyOn((<any>component).router, 'navigate');
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should create header tag', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('header')).toBeTruthy();
  }));

  it('should navigate to logout page on logout', () => {
    component.logout();
    expect(navigateSpy).toHaveBeenCalledWith(['/logout']);
  });
});
