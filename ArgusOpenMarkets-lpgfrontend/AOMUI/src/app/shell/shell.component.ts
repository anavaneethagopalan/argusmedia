import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { select, NgRedux } from '@angular-redux/store';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { IAppState } from '../store/model';
import { MessageDistributionService } from '../shared/message-distribution.service';
import { LoggerService } from '../shared/logger.service';
import { ConfigService } from '../shared/config.service';
import { ILoginSession } from '../api/session/models';
import { LoginSessionAPIActions } from '../api/session/actions';

@Component({
  selector: 'aom-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit, OnDestroy {

  welcomeName: string;
  organisationName: string;

  @select(['webapi_login_session', 'session']) session$: Observable<ILoginSession>;
  private messageDistributionSubscription: Subscription;
  canDeactivate(): boolean | Observable<boolean> {
    return false;
  }

  constructor(
    private router: Router,
    private messageDistributionService: MessageDistributionService,
    private loggerService: LoggerService,
    private loginactions: LoginSessionAPIActions,
    private configService: ConfigService,
    private store: NgRedux<IAppState>
  ) {
    this.session$.take(1).filter(_ => _ !== null && _.IsAuthenticated === true && !this.messageDistributionService.isConnected())
      .subscribe(_ => {
        if (_) {
          this.welcomeName = _.User.name;
          this.organisationName = _.User.userOrganisation.name;
          
          this.messageDistributionSubscription = this.messageDistributionService.connect('AOM-' + _.User.username, _.token)
            .catch((error => {
              console.log('shell: connection error', error);
              return Observable.of<boolean>(false);
            }))
            .subscribe(
            (x) => { },
            (err) => {
              console.error('shell: connection error', err);
            },
            () => {
              this.loggerService.info('Successfully connected to Diffusion');
            });
        }
      });
  }

  ngOnInit() {
    this.messageDistributionService.disconnect$.subscribe(() => this.logout());
  }

  ngOnDestroy() {
    this.messageDistributionSubscription.unsubscribe();
  }

  public downloadHelpFile(): void {
    const helpFileUrl = this.configService.webApi + '/getHelpFile?token=' + this.store.getState().webapi_login_session.session.sessionToken + '&uid=' + this.store.getState().webapi_login_session.session.userId;
    window.open(helpFileUrl, '_blank');
  }

  public logout(): void {
    this.router.navigate(['/logout']);
  }
}
