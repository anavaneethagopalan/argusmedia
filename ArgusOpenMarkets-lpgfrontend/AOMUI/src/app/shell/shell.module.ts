import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellComponent } from './shell.component';
import { ShellRoutingModule } from './shell-routing.module';
import { BsDropdownModule } from 'ngx-bootstrap';

import { ProductService } from '../shared/product.service';
import { LogoutComponent } from './logout/logout.component';


@NgModule({
  imports: [
    CommonModule,
    ShellRoutingModule,
    BsDropdownModule.forRoot()
  ],
  declarations: [ShellComponent, LogoutComponent],
  providers: [ProductService]
})
export class ShellModule {
  constructor(productService: ProductService) {}
}
