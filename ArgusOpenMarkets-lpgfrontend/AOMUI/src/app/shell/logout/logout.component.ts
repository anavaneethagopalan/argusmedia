import { Component, OnInit } from '@angular/core';

import { LoginSessionAPIActions } from '../../api/session/actions';

@Component({
  selector: 'aom-logout',
  template: '',
})
export class LogoutComponent implements OnInit {

  constructor(private actions: LoginSessionAPIActions) { }

  ngOnInit() {
    this.actions.logoutUser();
  }

}
