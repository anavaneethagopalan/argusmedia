import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShellComponent } from './shell.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
    {
        path: '',
        component: ShellComponent,
        children: [
            {
                path: '',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule',
            },
            {
                path: 'settings',
                loadChildren: 'app/settings/settings.module#SettingsModule',
            },
            {
                path: 'playground',
                loadChildren: 'app/playground/playground.module#PlaygroundModule',
            },
            {
                path: 'logout',
                component: LogoutComponent
            },
            {
                path: '**',
                redirectTo: '/'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShellRoutingModule { }
