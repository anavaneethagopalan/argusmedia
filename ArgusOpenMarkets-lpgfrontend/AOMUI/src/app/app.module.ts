import { BrowserModule } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpModule } from '@angular/http';
import { NgReduxModule } from '@angular-redux/store';

import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './guards/auth.guard';
import { ActiveUserSessionGuard } from './guards/active-user-session.guard';
import { MessageDistributionService } from './shared/message-distribution.service';
import { SocketProvider } from './shared/socket-provider';
import { TradersService } from './shared/traders.service';
import { ConfigService, configServiceFactory } from './shared/config.service';
import { ToastService } from './shared/toast/toast.service';
import { CustomToastOptions } from './shared/toast/custom-toast-options';
import { LoggerService } from './shared/logger.service';
import { StoreModule } from './store/store.module';
import { MarketStatusService } from './shared/marketstatus/market-status.service';
import { NewsComponent } from './news/news.component';
import { ApiModule } from './api/api.module';
import { OrganisationDetailsActions } from './settings/company-settings/actions';
import { UserService } from './shared/user.service';
import { OrderAPIActions } from './api/orders/actions';
import { PermissionSummaryAPIActions } from './api/permissions-summary/actions';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutingModule,
    ToastModule.forRoot(),
    NgReduxModule,
    StoreModule,
    ApiModule
  ],
  providers: [
    AuthGuard,
    ActiveUserSessionGuard,
    MessageDistributionService,
    SocketProvider,
    TradersService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService],
      multi: true
    },
    ConfigService,
    ToastService,
    {
      provide: ToastOptions,
      useClass: CustomToastOptions
    },
    LoggerService,
    MarketStatusService,
    OrganisationDetailsActions,
    BsModalService,
    UserService,
    OrderAPIActions,
    PermissionSummaryAPIActions
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
