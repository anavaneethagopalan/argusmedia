export interface Assessment {
    productId: number;
    today: AssessmentDay;
    previous: AssessmentDay;
}

export interface AssessmentDay {
    id: number;
    enteredByUserId: number;
    lastUpdatedUserId: number;
    dateCreated: Date;
    lastUpdated: Date;
    priceHigh: number;
    priceLow: number;
    productId: number;
    product: any;
    businessDate: Date;
    trendFromPreviousAssessment?: string;
    assessmentStatus: string;
}
