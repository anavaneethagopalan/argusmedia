import { Assessment } from './assessment/assessment';

export interface IProduct {
    productId: number;
    marketStatus: any;
    definition: IProductDefinition;
    contentStreams: any;
    assessment: Assessment;
    metaData: any;
}

export interface IProductDefinition {
    productId: number;
    productName: string;
    productTitle: string;
    isInternal: boolean;
    hasAssesment: boolean;
    status: MarketStatus;
    openTime: Date;
    closeTime: Date;
    lastOpenTime: Date;
    lastCloseTime: Date;
    // purgeTimeOfDay: string; //TIME???
    // purgeFrequency: number;
    commodity: ICommodity;
    deliveryLocation: IDeliveryLocation;
    contractSpecification: IProductContractSpecification;
    bidAskStackNumberRows: number;
    coBrokering: boolean;
    productLineTemplates: IProductLineTemplate[];
}

export interface ICommodity {
    id: number;
    name: string;
}

export interface IDeliveryLocation {
    id: number;
    name: string;
}

export interface IProductContractSpecification {
    volume: IVolume;
    pricing: IPricing;
    productTenors: IProductTenor[];
    contractType: string;
}

export interface IVolume {
    default: number;
    minimum: number;
    maximum: number;
    increment: number;
    decimalPlaces: number;
    volumeUnitCode: string;
    commonQuantities: any; //TODO
    commonQuantityValues: ICommonQuantityValue[];
    volumeUnitName: string;
    volumeUnitDescription: string;
}

export interface ICommonQuantityValue {
    quantity: number;
    quantityText: string;
    behaviour: string;
    displayOrder: number;
}

export interface IPricing {
    minimum: number;
    maximum: number;
    increment: number;
    decimalPlaces: number;
    pricingCurrencyCode: string;
    pricingCurrencyDisplayName: string;
    pricingUnitName: string;
    pricingUnitDescription: string;
}

export interface IProductTenor {
    id: number;
    rollDate: string;
    parentTenor: ITenor;
    name: string;
    deliveryDateStart: Date;
    deliveryDateEnd: Date;
    minimumDeliveryRange: number;
    deliveryLocation: IDeliveryLocation;
    defaultStartDate: Date;
    defaultEndDate: Date;
}

export interface ITenor {
    id: number;
    name: string;
    periodCode: string;
}

export interface IProductLineTemplate {
    id: number;
    productId: number;
    sequenceNo: number;
    percentageOptionList: string;
    productLineBasisTemplates: any[]; //TODO
}

export enum MarketStatus {
    Closed = 0,
    Open = 1
}
