export interface IMarketStatus {
    marketStatus: number;
    productName: string;
}
