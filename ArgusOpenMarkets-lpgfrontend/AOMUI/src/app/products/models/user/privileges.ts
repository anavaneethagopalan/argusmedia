export interface Privileges {
    view_MarketTicker_Widget: boolean;
    view_BidAsk_Widget: boolean;
    view_NewsAnalysis_Widget: boolean;
    view_Assessment_Widget: boolean;
    marketTicker_Create: boolean;
    order_Amend_OwnOrg: boolean;
    order_Aggress_Principal: boolean;
    deal_Create_Principal: boolean;
    order_Amend_Own: boolean;
    order_Create_Principal: boolean;
    order_Amend_Kill: boolean;
}
