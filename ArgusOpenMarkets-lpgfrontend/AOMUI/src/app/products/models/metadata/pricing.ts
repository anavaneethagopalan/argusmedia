export interface Pricing {
    minimum: number;
    maximum: number;
    increment: number;
    decimalPlaces: number;
    pricingUnitCode: string;
    pricingCurrencyCode: string;
}
