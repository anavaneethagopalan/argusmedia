import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { BsModalService } from 'ngx-bootstrap/modal';

import { ComplexMarketPricingBasisComponent } from './pricing-basis.component';

describe('ComplexMarketPricingBasisComponent', () => {
  let component: ComplexMarketPricingBasisComponent;
  let fixture: ComponentFixture<ComplexMarketPricingBasisComponent>;

  const modalRefStub = {};
  const modalServiceStub = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComplexMarketPricingBasisComponent],
      providers: [
        { provide: BsModalRef, useValue: modalRefStub },
        { provide: BsModalService, useValue: modalServiceStub },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplexMarketPricingBasisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
