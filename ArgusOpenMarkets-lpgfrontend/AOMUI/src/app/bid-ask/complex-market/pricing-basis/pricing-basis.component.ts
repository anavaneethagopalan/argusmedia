import { Component, TemplateRef, OnInit, ViewEncapsulation, Input, OnDestroy } from '@angular/core';
import { select } from '@angular-redux/store';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
// for the modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { IPriceLineBasis, IPriceBasis } from '../../../api/orders/models';


@Component({
  selector: 'aom-complex-market-pricing-basis',
  templateUrl: './pricing-basis.component.html',
  styleUrls: ['./pricing-basis.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ComplexMarketPricingBasisComponent implements OnInit {

  @Input() showHeader?: boolean;
  @Input() isBid: boolean;
  @Input() priceLineBasis: IPriceLineBasis;
  // @Input() templateType : string;
  @Input() priceLineSequenceNo: number;

  @select() priceBases$: Observable<IPriceBasis[]>;

  currentPriceBasis: string;
  currentPriceSubscription: Subscription;

  ngOnInit() {
    // let parent = this;
    if(this.priceLineBasis) {
      this.currentPriceSubscription = this.priceBases$.subscribe(priceBases => {
        //console.log('Price basis id: ' + this.priceLineBasis.priceBasisId);
        this.currentPriceBasis = priceBases.filter(priceBasis => priceBasis.id === this.priceLineBasis.priceBasisId)[0].shortName
      });
    }    

    //console.log('currentPriceBasis' + this.currentPriceBasis)
    // let debug = this.priceBases$.map((priceBases) => priceBases.filter((priceBasis) => priceBasis.id === this.priceLineBasis.priceBasisId)[0]);
  }

  ngOnDestroy(): void {

    if (this.currentPriceSubscription) {
      this.currentPriceSubscription.unsubscribe();
    }
  }

  // Example 4 - Open the Pricing Modal
  public modalRef: BsModalRef;
  constructor(private modalService: BsModalService) { }

  public openModalPricingComplex(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-lg' }));
  }

}
