import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgRedux } from '@angular-redux/store';
import { MockNgRedux, NgReduxTestingModule } from '@angular-redux/store/testing';

import { BsModalService } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap';

import { CreateOrderAPIActions } from '../../api/create-order/actions';
import { IOrder } from '../../api/orders/models';
import { PermissionsService } from '../../shared/permissions.service';
import { ReversePipe } from '../../shared/reverse.pipe';
import { IProductDefinition, MarketStatus } from '../../products/models/product';
import { IAppState } from '../../store/model';
import { BidAskCookieService } from '../bid-ask.cookie.service'
import { ComplexMarketOrderInfoComponent } from '../complex-market/order-Info/order-info.component';
import { ComplexMarketPricingBasisComponent } from '../complex-market/pricing-basis/pricing-basis.component';
import { ComplexMarketComponent } from './complex-market.component';
import { ComplexTableRowPopupComponent } from './complex-table-row-popup.component';

describe('ComplexMarketComponent', () => {
    let component: ComplexMarketComponent;
    let fixture: ComponentFixture<ComplexMarketComponent>;

    const modalServiceStub = {};
    
    const createAPIActionsStub = {
        createNewOrder: () => undefined
    };

    const createBidAskCookieServiceStub = {
        getBidAskStackNumberOfOrders(productId: number) {},        
        updateBidAskStackNumberOfOrders(productid: number, rows: number) {}
    };

    const permissionServiceStub = {
        canCreateOrder$: (productId) => undefined
    };

    const productDefinitionStub: IProductDefinition = {
        productId: 1,
        productName: 'Naptha test product',
        productTitle: '',
        isInternal: false,
        hasAssesment: false,
        status: MarketStatus.Open,
        openTime: new Date(Date.now()),
        closeTime: new Date(Date.now()),
        lastOpenTime: new Date(Date.now()),
        lastCloseTime: new Date(Date.now()),
        // purgeTimeOfDay: string; //TIME???
        // purgeFrequency: number;
        commodity: null,
        deliveryLocation: null,
        contractSpecification: null,
        bidAskStackNumberRows: 6,
        coBrokering: true,
        productLineTemplates: null
    }

    const orderDefault: IOrder = {
        id: 1,
        lastUpdated: new Date(Date.now()),
        orderType: 'BID',
        quantity: 5000,
        notes: '',
        productTenors: null,
        brokerOrganisation: null,
        brokerShortCode: 'GIN',
        orderStatus: 'A',
        enteredByUserId: '2',
        brokerOrganisationId: 6,
        brokerRestriction: 'A',
        coBrokering: true,
        productId : 1,
        principalOrganisationId: 3,
        principalOrganisationShortCode: 'BP',
        orderPriceLines: [{
            id: 1,
            orderId: 1,
            orderStatusCode: 'A',
            priceLine: {
                id: 1,
                sequenceNo: 0,
                priceLinesBases: [{
                    id: 1,
                    description: '',
                    sequenceNo: 0,
                    percentageSplit: 40,
                    priceLineBasisValue: 50000,
                    pricingPeriodFrom: new Date(Date.now()),
                    pricingPeriodTo: new Date(Date.now()),
                    priceBasisId: 1
                }]
            }
        },
        {
            id: 2,
            orderId: 1,
            orderStatusCode: 'ASK',
            priceLine: {
                id: 1,
                sequenceNo: 1,
                priceLinesBases: [{
                    id: 3,
                    description: '',
                    sequenceNo: 0,
                    percentageSplit: 25,
                    priceLineBasisValue: 50000,
                    pricingPeriodFrom: new Date(Date.now()),
                    pricingPeriodTo: new Date(Date.now()),
                    priceBasisId: 1
                },
                {
                    id: 4,
                    description: '',
                    sequenceNo: 1,
                    percentageSplit: 75,
                    priceLineBasisValue: 70000,
                    pricingPeriodFrom: new Date(Date.now()),
                    pricingPeriodTo: new Date(Date.now()),
                    priceBasisId: 1
                }]
            }
        }]
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ComplexMarketComponent,
                ComplexMarketOrderInfoComponent,
                ComplexMarketPricingBasisComponent,
                ComplexTableRowPopupComponent,
                ReversePipe
            ],
            providers: [
                { provide: BsModalService, useValue: modalServiceStub },
                { provide: CreateOrderAPIActions, useValue: createAPIActionsStub },
                { provide: NgRedux, useValue: MockNgRedux.getInstance() },
                { provide: BidAskCookieService, useValue: createBidAskCookieServiceStub },
                { provide: PermissionsService, useValue: permissionServiceStub }             
            ],
            imports :[
                PopoverModule.forRoot()
            ]
        })
        .compileComponents()
        .then(() => {
            MockNgRedux.reset();            
            spyOn(MockNgRedux.getInstance(), 'getState').and.returnValue({
                config : {
                    bidAskStackStep: 3
                }        
            });
        });
    }));

    it('should be created', () => {
        fixture = TestBed.createComponent(ComplexMarketComponent);
        component = fixture.componentInstance;
        component.productDefinition = productDefinitionStub;    
            
        fixture.detectChanges();

        expect(component).toBeTruthy();
    });

    it('can be collapsed on load, when default visible rows number is greater than height change step', () => {        
        fixture = TestBed.createComponent(ComplexMarketComponent);
        component = fixture.componentInstance;
        component.productDefinition =  { ...productDefinitionStub, bidAskStackNumberRows: 6 };        
        fixture.detectChanges();

        const ordersStub = MockNgRedux.getSelectorStub('orders');
        let order2 = { ...orderDefault, id: 3 };
        ordersStub.next([orderDefault, order2]);
        ordersStub.complete();

        expect(component.canStackCollapse()).toBeTruthy();
        expect(component.canStackExpand()).toBeFalsy();
        expect(component.hiddenOrdersNo).toBeFalsy();
    });

    it('cannot be collapsed on load, when default visible rows number is equal than height change step', () => {
        fixture = TestBed.createComponent(ComplexMarketComponent);
        component = fixture.componentInstance;
        component.productDefinition =  { ...productDefinitionStub, bidAskStackNumberRows: 2 };        
        fixture.detectChanges();

        const ordersStub = MockNgRedux.getSelectorStub('orders');
        let order2 = { ...orderDefault, id: 3 };
        ordersStub.next([orderDefault, order2]);
        ordersStub.complete();

        expect(component.canStackCollapse()).toBeFalsy();
        expect(component.canStackExpand()).toBeFalsy();
        expect(component.hiddenOrdersNo).toBeFalsy();
    });

    it('can be expanded on load, when default visible rows number is less than total number of orders', () => {        
        fixture = TestBed.createComponent(ComplexMarketComponent);
        component = fixture.componentInstance;
        component.productDefinition =  { ...productDefinitionStub, bidAskStackNumberRows: 3 };        
        fixture.detectChanges();

        const ordersStub = MockNgRedux.getSelectorStub('orders');
        ordersStub.next([orderDefault, orderDefault, orderDefault, orderDefault]);
        ordersStub.complete();

        expect(component.canStackCollapse()).toBeFalsy();
        expect(component.canStackExpand()).toBeTruthy();
        expect(component.hiddenOrdersNo).toBe(1);
    });

    it('can be collapsed again after collapsing once, when default visible rows number is greater than 2 x height change step', () => {        
        fixture = TestBed.createComponent(ComplexMarketComponent);
        component = fixture.componentInstance;
        component.productDefinition =  { ...productDefinitionStub, bidAskStackNumberRows: 7 };        
        fixture.detectChanges();

        const ordersStub = MockNgRedux.getSelectorStub('orders');
        ordersStub.next([orderDefault]);
        ordersStub.complete();

        component.toggleStackCollapse();

        expect(component.canStackCollapse()).toBeTruthy();
        expect(component.canStackExpand()).toBeFalsy();
        expect(component.hiddenOrdersNo).toBeFalsy();
    });

    it('cannot be collapsed again after collapsing once, when default visible rows number is less or equal than 2 x height change step', () => {        
        fixture = TestBed.createComponent(ComplexMarketComponent);
        component = fixture.componentInstance;
        component.productDefinition =  { ...productDefinitionStub, bidAskStackNumberRows: 6 };        
        fixture.detectChanges();

        const ordersStub = MockNgRedux.getSelectorStub('orders');
        ordersStub.next([orderDefault]);
        ordersStub.complete();

        component.toggleStackCollapse();

        expect(component.canStackCollapse()).toBeFalsy();
        expect(component.canStackExpand()).toBeFalsy();
        expect(component.hiddenOrdersNo).toBeFalsy();
    });

    it('can be expanded again after expanding once, when default visible rows number is less than 2 x height change step', () => {        
        fixture = TestBed.createComponent(ComplexMarketComponent);
        component = fixture.componentInstance;
        component.productDefinition =  { ...productDefinitionStub, bidAskStackNumberRows: 2 };        
        fixture.detectChanges();

        const ordersStub = MockNgRedux.getSelectorStub('orders');
        ordersStub.next([orderDefault, orderDefault, orderDefault, orderDefault, orderDefault, orderDefault, orderDefault, orderDefault]);
        ordersStub.complete();

        component.toggleStackExpand();

        expect(component.canStackCollapse()).toBeTruthy();
        expect(component.canStackExpand()).toBeTruthy();
        expect(component.hiddenOrdersNo).toBe(3);
    });
});
