import { Component, TemplateRef, ViewEncapsulation, Input } from '@angular/core';
import { select } from '@angular-redux/store';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { IOrder } from '../../api/orders/models';
import { IPriceLineBasis, IPriceBasis } from '../../api/orders/models';

@Component({
    selector: 'complex-table-row-popup',
    templateUrl: './complex-table-row-popup.component.html',
    styleUrls: ['./complex-table-row-popup.component.scss'],

})
export class ComplexTableRowPopupComponent {

    @select() priceBases$: Observable<IPriceBasis[]>;
    priceSubscription: Subscription;
    @Input() isBid: boolean;
    @Input() order?: IOrder;
    @Input() isTradeable : boolean;

    ngOnInit(){
        //this.isTradeable.subscribe(x => this.trade = x);
        //console.log("Istradeable : " +  this.isTradeable);
    }

    GetCurrentPricingBasis(priceLineBasis): string {

        let currentPriceBasis: string = "";
        if (priceLineBasis) {
            this.priceSubscription = this.priceBases$.subscribe(priceBases => {
                //console.log('Price basis id: ' + this.priceLineBasis.priceBasisId);
                currentPriceBasis = priceBases.find(priceBasis => priceBasis.id === priceLineBasis.priceBasisId).shortName
            });
        }
        return currentPriceBasis;

    }
    

    ngOnDestroy(): void {

        if (this.priceSubscription) {
            this.priceSubscription.unsubscribe();
        }
    }

}
