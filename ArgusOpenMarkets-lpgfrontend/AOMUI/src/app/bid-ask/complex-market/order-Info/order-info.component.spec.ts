import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsModalService } from 'ngx-bootstrap/modal';

import { ComplexMarketOrderInfoComponent } from './order-info.component';

describe('ComplexMarketOrderInfoComponent', () => {
  let component: ComplexMarketOrderInfoComponent;
  let fixture: ComponentFixture<ComplexMarketOrderInfoComponent>;

  const modalServiceStub = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComplexMarketOrderInfoComponent],
      providers: [
        { provide: BsModalService, useValue: modalServiceStub }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplexMarketOrderInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
