import { Component, TemplateRef, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { IOrder } from '../../../api/orders/models';
// for the modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'aom-complex-market-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ComplexMarketOrderInfoComponent implements OnInit {

  @Input() isBid: boolean;
  @Input() order?: IOrder;
  @Input() showHeader?: boolean;

  
  ngOnInit() {}

  // Example 4 - Open the Pricing Modal
  public modalRef: BsModalRef;
  constructor(private modalService: BsModalService) {
  }

  ngOnChanges(){
    
  }

  public openModalPricingComplex(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-lg'}));
  }

}
