import { Component, TemplateRef, OnInit, ViewEncapsulation, Input, OnDestroy, OnChanges } from '@angular/core';
import { select, NgRedux } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { IProductDefinition } from '../../products/models/product';
import { IOrder } from '../../api/orders/models';

import { IAppState } from '../../store/model';

// for the modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { OrderFormComponent } from '../modals/order-form/order-form.component';
import { CreateOrderAPIActions } from '../../api/create-order/actions';
import { BidAskCookieService } from '../bid-ask.cookie.service'
import { PermissionsService } from '../../shared/permissions.service';

@Component({
  selector: 'aom-complex-market-container',
  templateUrl: './complex-market.component.html',
  styleUrls: ['./complex-market.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ComplexMarketComponent implements OnInit, OnDestroy {

  // Example 4 - Open the Pricing Modal
  modalRef: BsModalRef;

  @Input() productDefinition = <IProductDefinition>null; // angular-cli bug: https://github.com/angular/angular-cli/issues/2034#issuecomment-260976971
  @select('orders') orders$: Observable<IOrder[]>;

  private bidAskStackStep: number;

  private bidSubscription: Subscription;
  // private askSubscription: Subscription;

  public bidOrders: IOrder[];
  // public askOrders: IOrder[];

  //public rows: number;
  public emptyRows: number[];

  public bidsMaxPricingBasisNo: any[] = [{}];
  // public asksMaxPricingBasisNo: number = 0;

  private totalOrdersNo: number;
  private visibleOrdersNo: number;
  public hiddenOrdersNo: number;
  
  constructor(private modalService: BsModalService, private createOrderAPIActions: CreateOrderAPIActions, private store: NgRedux<IAppState>,
    private bidAskCookieService: BidAskCookieService, public permissionsService: PermissionsService) { }

  ngOnInit() {

    let numberOfRows = this.bidAskCookieService.getBidAskStackNumberOfOrders(this.productDefinition.productId);

    this.visibleOrdersNo = numberOfRows ? numberOfRows : this.productDefinition.bidAskStackNumberRows;
    this.SubscribeToOrders();

    this.bidAskStackStep = this.store.getState().config.bidAskStackStep;
  }

  SubscribeToOrders(): void {

    this.bidSubscription = this.orders$.map(_ => _.filter(order => order.productId == this.productDefinition.productId && order.orderType == "BID"))
      .subscribe(orders => {
        this.totalOrdersNo = orders.length;
        this.bidOrders = this.getOrdersInStack(orders);
        //get max number of price bases, for rendering empty rows
        this.bidOrders.map((order) => {
          let maxPriceBases = Math.max(...order.orderPriceLines.map((opl) => opl.priceLine.priceLinesBases.length));
          this.bidsMaxPricingBasisNo = new Array(maxPriceBases);
        });
      });



    // this.askSubscription = this.orders$.map(_ => _.filter(order => order.productId == this.productDefintion.productId && order.orderType == "A"))
    //   .subscribe(ord => {
    //     this.askOrders = this.getOrdersInStack(ord);
    //     // get max number of price bases, for rendering empty rows
    //     // this.askOrders.map((order) => 
    //     //   this.asksMaxPricingBasisNo = Math.max(...order.orderPriceLines.map((opl) => opl.priceLine.priceLinesBases.length))
    //     // );
    //   });

  }

  private getOrdersInStack(ord: IOrder[]): IOrder[] {
    let rows = 0;
    if (ord.length < this.visibleOrdersNo) {
      rows = this.visibleOrdersNo - ord.length;
    }
    this.emptyRows = Array(rows);
    this.hiddenOrdersNo = ord.length > this.visibleOrdersNo ? ord.length - this.visibleOrdersNo : 0;
    return ord.slice(0, this.visibleOrdersNo - this.emptyRows.length);
  };

  toggleStackCollapse(): void {

    this.visibleOrdersNo -= this.bidAskStackStep;
    this.SubscribeToOrders();
    this.bidAskCookieService.updateBidAskStackNumberOfOrders(this.productDefinition.productId, this.visibleOrdersNo);
  };

  toggleStackExpand(): void {

    this.visibleOrdersNo += this.bidAskStackStep;
    this.SubscribeToOrders();
    this.bidAskCookieService.updateBidAskStackNumberOfOrders(this.productDefinition.productId, this.visibleOrdersNo);
  };

  canStackExpand(): boolean {
    return this.totalOrdersNo > this.visibleOrdersNo;
  }

  canStackCollapse(): boolean {
    return this.visibleOrdersNo > this.bidAskStackStep;
  }

  getOrderPermissionCssClasses$(productDefinition: IProductDefinition, order: IOrder): Observable<string> {
    return Observable.combineLatest(
      this.permissionsService.isNonTradeable$(order, productDefinition).map(_ => _ ? 'isNonTradeable' : ''),
      this.permissionsService.isBrokerRestricted$(order).map(_ => _ ? 'isBrokerRestricted' : ''),
      this.permissionsService.isCoBrokered$(order).map(_ => _ ? 'isCoBrokered' : ''),
      this.permissionsService.isOrderMine$(order).map(_ => _ ? 'isOrderMine' : ''),
      (isNonTradeable, isBrokerRestricted, isCoBrokered, isOrderMine) =>
        [
          isNonTradeable,
          isBrokerRestricted,
          isOrderMine,
          (order.orderStatus === 'H' ? 'isHeld' : ''),
          isCoBrokered
        ].filter(_ => _ !== '').join(' ')
    );
  }

  isTradeable$(productDefinition: IProductDefinition, order: IOrder) : Observable<boolean> {
    return this.permissionsService.isNonTradeable$(order, productDefinition).map(_ => _ ? false : true)
  }

  ngOnDestroy(): void {

    if (this.bidSubscription) {
      this.bidSubscription.unsubscribe();
    }

    // if (this.askSubscription) {
    //   this.askSubscription.unsubscribe();
    // }
  }

  openModalPricingComplex(template: TemplateRef<any>): void {
    //this.modalService.show(AddNotificationComponent);
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-lg' }));
  }

  openModalBid(): void {
    this.createOrderAPIActions.createNewOrder(this.productDefinition.productId, 'BID');
    this.modalRef = this.modalService.show(OrderFormComponent, Object.assign({}, { class: 'modal-lg modal-bid' }));
  }

  openModalAsk(): void {
    this.createOrderAPIActions.createNewOrder(this.productDefinition.productId, 'ASK');
    this.modalRef = this.modalService.show(OrderFormComponent, Object.assign({}, { class: 'modal-lg modal-ask' }));
  }

  

}
