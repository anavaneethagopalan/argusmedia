import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ComplexTableRowPopupComponent } from './complex-table-row-popup.component';
import { IOrder } from '../../api/orders/models';

describe('ComplexTableRowPopupComponent', () => {
  let component: ComplexTableRowPopupComponent;
  let fixture: ComponentFixture<ComplexTableRowPopupComponent>;

  const orderDefault: IOrder = {
    id: 1,
    lastUpdated: new Date(Date.now()),
    orderType: 'BID',
    quantity: 5000,
    notes: '',
    productTenors: null,
    brokerOrganisation: null,
    brokerShortCode: 'GIN',
    orderStatus: 'A',
    enteredByUserId: '2',
    brokerOrganisationId: 6,
    brokerRestriction: 'A',
    coBrokering: true,
    productId : 1,
    principalOrganisationId: 3,
    principalOrganisationShortCode: 'BP',
    orderPriceLines: [{
        id: 1,
        orderId: 1,
        orderStatusCode: 'A',
        priceLine: {
            id: 1,
            sequenceNo: 0,
            priceLinesBases: [{
                id: 1,
                description: '',
                sequenceNo: 0,
                percentageSplit: 40,
                priceLineBasisValue: 50000,
                pricingPeriodFrom: new Date(Date.now()),
                pricingPeriodTo: new Date(Date.now()),
                priceBasisId: 1
            }]
        }
    },
    {
        id: 2,
        orderId: 1,
        orderStatusCode: 'ASK',
        priceLine: {
            id: 1,
            sequenceNo: 1,
            priceLinesBases: [{
                id: 3,
                description: '',
                sequenceNo: 0,
                percentageSplit: 25,
                priceLineBasisValue: 50000,
                pricingPeriodFrom: new Date(Date.now()),
                pricingPeriodTo: new Date(Date.now()),
                priceBasisId: 1
            },
            {
                id: 4,
                description: '',
                sequenceNo: 1,
                percentageSplit: 75,
                priceLineBasisValue: 70000,
                pricingPeriodFrom: new Date(Date.now()),
                pricingPeriodTo: new Date(Date.now()),
                priceBasisId: 1
            }]
        }
    }]
}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComplexTableRowPopupComponent],
      providers: [
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplexTableRowPopupComponent);
    component = fixture.componentInstance;
    component.order = orderDefault;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
