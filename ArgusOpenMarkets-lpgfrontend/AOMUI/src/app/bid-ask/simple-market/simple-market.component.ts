import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';


@Component({
  selector: 'aom-simple-market',
  templateUrl: './simple-market.component.html',
  styleUrls: ['./simple-market.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SimpleMarketComponent implements OnInit {

  @Input() isBid:boolean;
  constructor() { }

  ngOnInit() {
    
  }

}
