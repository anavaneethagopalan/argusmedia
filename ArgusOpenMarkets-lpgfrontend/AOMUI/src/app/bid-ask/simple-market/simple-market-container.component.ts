import { Component, OnInit, ViewEncapsulation, TemplateRef, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { IProductDefinition } from '../../products/models/product';

@Component({
  selector: 'aom-simple-market-container',
  templateUrl: './simple-market-container.component.html',
  styleUrls: ['./simple-market.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SimpleMarketContainerComponent implements OnInit {

  public modalRef: BsModalRef;

  @Input() productDefinition = <IProductDefinition>null; // angular-cli bug: https://github.com/angular/angular-cli/issues/2034#issuecomment-260976971

  constructor(private modalService: BsModalService) { }

  ngOnInit() { }

  public openModalAsk(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-ask' }));
  }
  public openModalBid(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-bid' }));
  }

}
