import { Component, OnInit } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'aom-execute-order',
  templateUrl: './execute-order.component.html',
  styleUrls: ['./execute-order.component.scss']
})
export class ExecuteOrderComponent implements OnInit {

  public radioSelectedValue = 0;

  constructor(public modalRef: BsModalRef) { }

  ngOnInit() {
  }

  // apply the border to the selected radio buttons parent label
  addLabelBorder(value: number) {
    this.radioSelectedValue = value;
  }

  isRadioSelected(value: number) {
    return this.radioSelectedValue === value;
  }
}
