import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { ExecuteOrderComponent } from './execute-order.component';

describe('ExecuteOrderComponent', () => {
  let component: ExecuteOrderComponent;
  let fixture: ComponentFixture<ExecuteOrderComponent>;

  const modalRefStub = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExecuteOrderComponent],
      providers: [
        { provide: BsModalRef, useValue: modalRefStub }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecuteOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
