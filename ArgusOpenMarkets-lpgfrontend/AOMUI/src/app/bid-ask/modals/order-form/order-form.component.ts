import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/skipWhile';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { ICreateOrderState, CreateOrderConfig, DeliveryPeriodDateRange, DeliveryPeriodDropdown } from '../../../api/create-order/models';
import { IOrder } from '../../../api/orders/models';
import { CreateOrderAPIActions } from '../../../api/create-order/actions';
import { QuantityValidator } from '../../../shared/quantity/quantity-validator';
import { DeliveryPeriodValidator } from '../../../shared/delivery-period/delivery-period-validator';

@Component({
  selector: 'aom-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent implements OnInit {

  showDiv = false;
  togglePricingBasis = false;
  minDate: Date;
  maxDate: Date;
  traders$: Observable<any>;

  public orderForm: FormGroup;

  @select('createOrderState') createOrderState$: Observable<ICreateOrderState>;
  public createOrderConfig$: Observable<CreateOrderConfig> = this.createOrderState$.map(c => c.config);
  public createOrderError$: Observable<any> = this.createOrderState$.map(c => c.error);

  constructor(public modalRef: BsModalRef, private fb: FormBuilder, private createOrderAPIActions: CreateOrderAPIActions) { }

  ngOnInit() {
    this.createOrderState$.subscribe((state) => {
      if (state.config) {
        this.orderForm = this.fb.group({
          quantity: [state.order ? state.order.quantity : state.config.quantity.default,
          [Validators.required, QuantityValidator.minimum(state.config.quantity.minimum), QuantityValidator.maximum(state.config.quantity.maximum),
          QuantityValidator.isValidValue(state.config.quantity.quantityOptions.map(o => o.value))]],
          deliveryPeriod: this.createDeliveryPeriodFormGroup(state.config, state.order),
          deliveryLocation: [state.config.deliveryLocation.deliveryLocationOptions[0].id],
          trader: [state.config.trader.traders[0]]
        });
      }
    });
    this.createOrderError$.subscribe((error) => this.modalRef.hide());
  }

  createDeliveryPeriodFormGroup(config: CreateOrderConfig, order: IOrder) {
    if (config.deliveryPeriod.hasOwnProperty('deliveryPeriodOptions')) {
      return this.fb.group({
        deliveryPeriodOption: [(config.deliveryPeriod as DeliveryPeriodDropdown).deliveryPeriodOptions[0].id, Validators.required]
      });
    } else {
      const deliveryPeriodConfig = config.deliveryPeriod as DeliveryPeriodDateRange;
      return this.fb.group({
        deliveryStartDate: [deliveryPeriodConfig.defaultStartDate, Validators.required],
        deliveryEndDate: [deliveryPeriodConfig.defaultEndDate, Validators.required]
      }, { validator: DeliveryPeriodValidator.isValidRange(deliveryPeriodConfig.minimumDeliveryRange) });

    }
  }

  toggle() {
    this.togglePricingBasis = !this.togglePricingBasis;
  }

  setTogglePricingBasis(value) {
    this.togglePricingBasis = value;
  }

  setShowDiv(value) {
    this.showDiv = value;
  }

  onQuantityChange(quantity) {
    this.orderForm.get('quantity').setValue(quantity);
  }

  get quantityErrors() {
    const errors = [];

    if (this.orderForm.get('quantity').hasError('required')) {
      errors.push('Quantity is required');
    }
    if (this.orderForm.get('quantity').hasError('minimum')) {
      errors.push(this.orderForm.get('quantity').getError('minimum'));
    }
    if (this.orderForm.get('quantity').hasError('maximum')) {
      errors.push(this.orderForm.get('quantity').getError('maximum'));
    }
    if (this.orderForm.get('quantity').hasError('invalidValue')) {
      errors.push(this.orderForm.get('quantity').getError('invalidValue'));
    }

    return errors;
  }

  get deliveryPeriodErrors() {
    const errors = [];
    const deliveryPeriodControl = this.orderForm.get('deliveryPeriod');
    const deliveryStartDateControl = deliveryPeriodControl.get('deliveryStartDate');
    const deliveryEndDateControl = deliveryPeriodControl.get('deliveryEndDate');
    if (deliveryStartDateControl && deliveryStartDateControl.touched && deliveryStartDateControl.hasError('required')) {
      errors.push('Delivery period start date required');
    }
    if (deliveryEndDateControl && deliveryEndDateControl.touched && deliveryEndDateControl.hasError('required')) {
      errors.push('Delivery period end date required');
    }
    if (deliveryPeriodControl.hasError('invalidRange')) {
      errors.push(deliveryPeriodControl.getError('invalidRange'));
    }
    return errors;
  }
}
