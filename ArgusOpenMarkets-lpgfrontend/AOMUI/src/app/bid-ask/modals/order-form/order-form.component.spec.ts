import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { BsDatepickerModule } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { OrderFormComponent } from './order-form.component';
import { CreateOrderAPIActions } from '../../../api/create-order/actions';

describe('OrderFormComponent', () => {
  let component: OrderFormComponent;
  let fixture: ComponentFixture<OrderFormComponent>;

  const modalRefStub = {};
  const createOrderActionsStub = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BsDatepickerModule.forRoot(), ReactiveFormsModule],
      declarations: [OrderFormComponent],
      providers: [
        { provide: BsModalRef, useValue: modalRefStub },
        { provide: CreateOrderAPIActions, useValue: createOrderActionsStub}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFormComponent);
    component = fixture.componentInstance;
    component.orderForm = new FormGroup({
      quantity: new FormControl(),
      deliveryPeriod: new FormGroup({})
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
