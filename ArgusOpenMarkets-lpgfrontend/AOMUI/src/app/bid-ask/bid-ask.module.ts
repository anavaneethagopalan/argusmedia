import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgReduxFormModule } from '@angular-redux/form';

import { BsDatepickerModule, ButtonsModule, ModalModule, PopoverModule } from 'ngx-bootstrap';

import { PermissionsService } from '../shared/permissions.service';
import { SimpleMarketComponent } from './simple-market/simple-market.component';
import { SimpleMarketContainerComponent } from './simple-market/simple-market-container.component';
import { ComplexMarketComponent } from './complex-market/complex-market.component';
import { ComplexMarketOrderInfoComponent } from './complex-market/order-Info/order-info.component';
import { ComplexMarketPricingBasisComponent } from './complex-market/pricing-basis/pricing-basis.component';
import { OrderFormComponent } from './modals/order-form/order-form.component';
import { ExecuteOrderComponent } from './modals/execute-order/execute-order.component';
import { ReversePipe } from '../shared/reverse.pipe';
import { QuantityComponent } from '../shared/quantity/quantity.component';
import { BidAskCookieService } from '../bid-ask/bid-ask.cookie.service';
import { DeliveryPeriodComponent } from '../shared/delivery-period/delivery-period.component';
import { DeliveryLocationComponent } from '../shared/delivery-location/delivery-location.component';
import { TradersComponent } from '../shared/traders/traders.component';
import { MetadataComponent } from '../shared/metadata/metadata.component';
import { ComplexTableRowPopupComponent } from './complex-market/complex-table-row-popup.component'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgReduxFormModule,
        BsDatepickerModule.forRoot(),
        ButtonsModule.forRoot(),
        ModalModule.forRoot(),
        PopoverModule.forRoot()
    ],
    declarations: [
        SimpleMarketComponent,
        SimpleMarketContainerComponent,
        ComplexMarketComponent,
        ComplexMarketOrderInfoComponent,
        ComplexMarketPricingBasisComponent,
        OrderFormComponent,
        ExecuteOrderComponent,
        ReversePipe,
        QuantityComponent,
        DeliveryPeriodComponent,
        DeliveryLocationComponent,
        TradersComponent,
        MetadataComponent,
        ComplexTableRowPopupComponent
    ],
    exports: [
        SimpleMarketContainerComponent,
        ComplexMarketComponent
    ],
    providers : [
        BidAskCookieService,
        PermissionsService
    ],
    entryComponents: [
        OrderFormComponent,
        ExecuteOrderComponent
    ]
})
export class BidAskModule { }
