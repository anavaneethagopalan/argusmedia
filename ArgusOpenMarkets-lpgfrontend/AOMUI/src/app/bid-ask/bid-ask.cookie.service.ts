import { Injectable } from '@angular/core';

import { CookieService } from 'ng2-cookies';

import { IUserConfig } from '../api/models/user/user-config'
import { IProductWidget } from '../api/models/user/productwidget'

@Injectable()
export class BidAskCookieService {

    private readonly BIDSTACK_COOKIE_KEY = 'aom-bidstack';

    constructor(private cookieService: CookieService) { }

    public getBidAskStackNumberOfOrders(productId: number): number {
        try {
            let cookie = this.cookieService.get(this.BIDSTACK_COOKIE_KEY);
            
            if (cookie) {
                let userConfig: IUserConfig = JSON.parse(cookie);
    
                let isBidAskWidgets = userConfig.productWidgets.find(x => x.productId == productId);
                if (isBidAskWidgets) {    
                    return isBidAskWidgets.numberOfRows;
                }    
            }
        }
        catch (error) {
            this.cookieService.delete(this.BIDSTACK_COOKIE_KEY);
        }        

        return null;
    }

    private createProductWidget(productId: number, rows: number): void {
        let bidAskWidget: IProductWidget = {
            productId: productId,
            numberOfRows: rows
        }

        let bidAskWidgets: IProductWidget[] = [];
        bidAskWidgets.push(bidAskWidget);

        let userConfig: IUserConfig = {
            productWidgets: bidAskWidgets
        }

        this.saveBidAskStackCookie(userConfig);
    }

    private saveBidAskStackCookie(userConfig: IUserConfig): void {
        try {
            let json = JSON.stringify(userConfig);
            this.cookieService.set(this.BIDSTACK_COOKIE_KEY, json);
        }
        catch(error) { }
    }

    public updateBidAskStackNumberOfOrders(productid: number, rows: number): void {
        try {
            let cookie: string = this.cookieService.get(this.BIDSTACK_COOKIE_KEY);

            if (cookie) {
                let userConfig: IUserConfig = JSON.parse(cookie);
                let bidAskWidgetConfig = userConfig.productWidgets.find(x => x.productId == productid);

                if (bidAskWidgetConfig) {
                    bidAskWidgetConfig.numberOfRows = rows;
                }
                else {
                    let bidAskWidgets = userConfig.productWidgets;

                    bidAskWidgets = bidAskWidgets ? bidAskWidgets : [];

                    let updatedBidAskWidgetConfig: IProductWidget = {
                        productId: productid,
                        numberOfRows: rows
                    }

                    bidAskWidgets.push(updatedBidAskWidgetConfig);
                }
                this.saveBidAskStackCookie(userConfig);
            }
            else {
                this.createProductWidget(productid, rows);
            }
        }
        catch (error) { }
    }
}