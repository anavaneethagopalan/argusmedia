import { TestBed, inject } from '@angular/core/testing';

import { CookieService } from 'ng2-cookies';

import { BidAskCookieService } from './bid-ask.cookie.service';

describe('BidAskCookieService', () => {

    var service: BidAskCookieService;

    const cookieServiceStub = {
        check: () => undefined,

        get: () => undefined,
        set: () => undefined,
        delete: () => undefined
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [BidAskCookieService,
                { provide: CookieService, useValue: cookieServiceStub },
            ]
        })

        service = TestBed.get(BidAskCookieService);
    });

    it('should be created', inject([CookieService], (service: CookieService) => {
        expect(service).toBeTruthy();
      }));

      it('should return correct productId from bidStack cookie', inject([BidAskCookieService, CookieService], (bidAskService: BidAskCookieService, service: CookieService) => {
        spyOn(service, 'get').and.returnValue('{"productWidgets":[{"productId":1,"numberOfRows":1},{"productId":2,"numberOfRows":5}]}');

        var productId = bidAskService.getBidAskStackNumberOfOrders(1);
        expect(productId).toBe(1);
        
      }));

      it('should call update of BidStack cookie', inject([BidAskCookieService, CookieService], (bidAskService: BidAskCookieService, service: CookieService) => {
        
        spyOn(service, 'get').and.returnValue('{"productWidgets":[{"productId":1,"numberOfRows":1},{"productId":2,"numberOfRows":5}]}');
        spyOn(service, 'set').and.callFake(cookieServiceStub.set)

        bidAskService.updateBidAskStackNumberOfOrders(1, 3);
        expect(service.set).toHaveBeenCalled();
        
      }));

});
