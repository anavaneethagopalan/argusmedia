import { AomPage } from './app.po';

describe('aom App', () => {
  let page: AomPage;

  beforeEach(() => {
    page = new AomPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to aom!');
  });
});
