import { browser, by, element } from 'protractor';

export class AomPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('aom-root h1')).getText();
  }
}
