var gulp = require('gulp'),
    octo = require('@octopusdeploy/gulp-octo')

gulp.task('publish', function () {
    var argv = require('yargs').argv;

    return gulp.src(['dist/**/*'])
        .pipe(octo.pack({ version: argv.buildNumber }))
        .pipe(octo.push({ apiKey: 'API-SXQUSN8EVLL2BCWCWQBNAFVTYOC', host: 'https://deploy.dev.argusmedia.com' }));
});

// define tasks here
gulp.task('default', function () {
    // run tasks here
    // set up watch handlers here
});