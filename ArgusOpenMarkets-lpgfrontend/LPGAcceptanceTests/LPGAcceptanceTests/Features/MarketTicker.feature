﻿Feature: MarketTicker
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@ignore
Scenario Outline: 01. Orders created by me appear on the Market Ticker
 Given a "<OrderType>" for product "<Product>" is created by myself
 Then I expect to see it on the Market Ticker
 And the colour of the Market Ticker to be "Orange"
Examples:
| OrderType | Product |
| bid       | X       |
| ask       | Y       |


@ignore
Scenario: 02. As a trader, Orders from other allowed trader from different org are shown in Market Ticker
 Given a "Bid" for product "Product" is created by another allowed trader
 Then I expect to see it on the Market Ticker
 And the colour of the Market Ticker to be "White" 


@ignore
 Scenario: 03. As a trader, Orders from other non-allowed trader from different org are shown in Market Ticker
 Given a "Ask" for product "Product" is created by another non-allowed trader
 Then I expect to see it on the Market Ticker
 And the colour of the Market Ticker to be "Grey"
 
 
  @ignore
Scenario: 04. As a broker, Orders with Any broker from allowed trader are shown in Market Ticker
 Given a "Bid" for "Product" is created with "Any" broker by allowed trader
 Then I expect to see it on the Market Ticker
 And the colour of the Market Ticker to be "White" 


  @ignore
Scenario: 05. As a broker, Orders with named broker from allowed trader are shown in Market Ticker
 Given a "Ask" for "Product" is created with broker1 by allowed trader
 Then I expect to see it on the Market Ticker
 And the colour of the Market Ticker to be "Orange" 


   @ignore
Scenario: 06. As a broker, Orders from other broker allowed trader are shown in Market Ticker
 Given a "Bid" for "Product" is created with broker2 by another allowed trader
 Then I expect to see it on the Market Ticker
 And the colour of the Market Ticker to be "Grey" 

 

@ignore
Scenario: 07. As a broker2, Co-Brokering Orders from allowed broker1 are shown in Market Ticker
 Given a "Bid" for product "Product" is created by allowed broker1 with cobrokering on
 Then I expect to see it on the Market Ticker
 And the colour of the Market Ticker to be "Grey"
 And the cobrokering icon should be displayed

 
@ignore
Scenario: 08. As a broker2, Co-Brokering Orders from allowed trader1 are shown in Market Ticker
 Given a "Ask" for product "Product" is created by allowed trader with cobrokering on
 Then I expect to see it on the Market Ticker
 And the colour of the Market Ticker to be "Grey"
 And the cobrokering icon should be displayed