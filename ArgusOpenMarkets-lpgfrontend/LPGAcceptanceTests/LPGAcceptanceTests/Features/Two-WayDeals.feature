﻿Feature: Two-WayDeals
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers


@ignore
Scenario: 01. T1 and T2 allows buysell to each other for a product
Given i create a "Bid" as "Trader1" for "Product" 
When the order is executed by "Trader2"
Then the order should disappear from bidask stack

@ignore
Scenario: 02. T1 and T2 denies buysell to each other
Given i create a "Bid" as "Trader1" for "Product" 
Then "Trader2" is not allowed to execute an order

@ignore
Scenario: 03. T1 allows only buy to T2
Given i create a "Bid" as "Trader1" for "Product" 
When the order is executed by "Trader2"
Then the order should disappear from bidask stack

@ignore
Scenario: 04. T1 allows only buy to T2
Given i create a "Ask" as "Trader1" for "Product" 
Then "Trader2" is not allowed to execute an order

@ignore
Scenario: 05. T1 allows only sell to T2
Given i create a "Ask" as "Trader1" for "Product" 
When the order is executed by "Trader2"
Then the order should disappear from bidask stack

@ignore
Scenario: 06. T1 allows only sell to T2
Given i create a "Bid" as "Trader1" for "Product" 
Then "Trader2" is not allowed to execute an order




