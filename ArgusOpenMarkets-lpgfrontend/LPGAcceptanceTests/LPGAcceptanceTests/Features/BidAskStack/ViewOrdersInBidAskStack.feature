﻿@TR1
@Ignore
Feature: ViewOrdersInBidAskStack
         Can I see orders on the bid ask stack
Background: 
Given I am on dashboard page

@AOMK-710
Scenario Outline: 1. ViewOrdersInBidAskStack - Product x - Bid - a created order with different pricelines and basis
When I create a "bid" for product "<product>" with "<numberofpricelines>" pricelines and "<numberofbasis>" basis
Then the order should appear in the bid stack
And the hoverbox shows the relevant information
Examples:
| product | numberofpricelines | numberofbasis |
| x       | 1                  | 1             |
| x       | 2                  | 1             |
| x       | 2                  | 2             |
| x       | 2                  | 1             |

@AOMK-710
Scenario Outline: 2. ViewOrdersInBidAskStack - Product x - Ask - a created order with different pricelines and basis
When I create a "ask" for product "<product>" with "<numberofpricelines>" pricelines and "<numberofbasis>" basis
Then the order should appear in the ask stack
And the hoverbox shows the relevant information
Examples:
| product | numberofpricelines | numberofbasis |
| x       | 1                  | 1             |
| x       | 2                  | 1             |
| x       | 2                  | 2             |
| x       | 2                  | 1             |

@AOMK-710
Scenario: 3. ViewOrdersInBidAskStack - Cobrokering
When I create an order with co-brokering on
Then the hoverbox should show cobrokering enabled

@AOMK-710
Scenario Outline: 4. ViewOrdersInBidAskStack - Click Bid and Ask buttons
When I click on the "bid" button for product x
Then I should see the relevant modal window should appear
Examples: 
| ordertype |
| bid       |
| ask       |

@AOMK-710
Scenario: 5. ViewOrdersInBidAskStack - Notes Icon
When I create an order that contains notes
Then I should see the notes icon in bid/ask stack

@AOMK-710
Scenario Outline: 6. ViewOrdersInBidAskStack - Orders with TOT or FOF display correctly
When I create an order with a quantity text value of "<QuantityTextValue>"
Then I should see "<QuantityTextValue>" on the bid/ask stack
Examples: 
| QuantityTextValue |
| TOT               |
| FOF               |

@AOMK-710
Scenario: 7. ViewOrdersInBidAskStack - Negative prices should be displayed
When I create an order with a negative price
Then I should see the negative price on the bid/ask stack

