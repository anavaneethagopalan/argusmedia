﻿Feature: CreatePlusDeal
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@ignore
#Front End
# Trader/Broker creates a +DEAL
Scenario: As a user, i create a plus deal
Given I fill in form details for +DEAL
When i create +DEAL
Then i can see the plus deal details in market ticker with Status as "PENDING"
And the color of the plus deal in market ticker should be "ORANGE"


@ignore
Scenario: As a user, plus deal created by another user is not visible to me
Given a +DEAL is created by another user
Then I cannot see the plus deal details in market ticker


@ignore
Scenario: As a Editor, plus deal created by another user is visible to me
Given a +DEAL is created by another user
Then i can see the plus deal details in market ticker with Status as "PENDING"
And the color of the plus deal in market ticker should be "YELLOW"



@ignore
Scenario: As an Editor, Free form deal field is visible
Given i fill in form details for +DEAL 
Then the free form deal field is filled with all the details
