﻿Feature: PendingPlusDeal
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers




@ignore
# Front End
Scenario: As an Editor, i am able to create a pending +DEAL
Given I fill in form details for +DEAL
When i create a +DEAL as pending
Then i cannot see the plus deal details in market ticker with Status as "PENDING"
And the color of the plus deal in market ticker should be "YELLOW"



@ignore
Scenario: As an Editor, i am able to see pending +DEAL created by other Editors
Given a pending +DEAL is created by another Editor
Then i can see the plus deal details in market ticker with Status as "PENDING"
And the color of the plus deal in market ticker should be "YELLOW"


@ignore
Scenario: As an User, i am cannot see pending +DEAL created by an Editors
Given a pending +DEAL is created by another Editor
Then i cannot see the plus deal details in market ticker




