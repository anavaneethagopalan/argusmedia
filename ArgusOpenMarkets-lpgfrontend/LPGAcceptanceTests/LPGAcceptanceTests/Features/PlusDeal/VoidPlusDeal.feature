﻿Feature: VoidPlusDeal
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers



@ignore
Scenario: As a User, plus deal created by me can be voided by an Editor
Given I create a +DEAL
When the plus deal is voided by an Editor with a "Reason"
Then i can see the plus deal details in market ticker with Status as "VOIDED"
And the color of the plus deal in market ticker should be "ORANGE"


@ignore
Scenario: As a User, i am unable to see a voided plus deal created by another user
Given a +DEAL is created by another user
When the plus deal is voided by an Editor with a "Reason"
Then i cannot see the plus deal details in market ticker 
