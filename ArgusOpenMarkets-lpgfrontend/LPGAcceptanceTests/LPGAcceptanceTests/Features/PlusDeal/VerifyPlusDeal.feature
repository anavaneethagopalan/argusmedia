﻿Feature: VerifyPlusDeal
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@ignore
Scenario: As a User, plus deal created by me can be verified by an Editor
Given I create a +DEAL
When the plus deal is verified by an Editor
Then i can see the plus deal details in market ticker with Status as ""
And the color of the plus deal in market ticker should be "ORANGE"


@ignore
Scenario: As a User, i am able to see a verified plus deal created by another user
Given a +DEAL is created by another user
When the plus deal is verified by an Editor
Then i can see the plus deal details in market ticker with Status as ""
And the color of the plus deal in market ticker should be "WHITE"



@ignore
Scenario: As an Editor, i am able to verify a pending deal created by me 
Given I create a +DEAL as pending
When I verify the pending deal
Then i can see the plus deal details in market ticker with Status as ""
And the color of the plus deal in market ticker should be "ORANGE"


@ignore
Scenario: As an Editor, i am able to see a plus deal verified by another Editor
Given a pending +DEAL is created by another Editor
When the pending +DEAL is verified by another Editor
Then i can see the plus deal details in market ticker with Status as ""
And the color of the plus deal in market ticker should be "ORANGE"


@ignore
Scenario: As an User, i am able to see a plus deal verified by another Editor
Given a pending +DEAL is created by another Editor
When the pending +DEAL is verified by another Editor
Then i can see the plus deal details in market ticker with Status as ""
And the color of the plus deal in market ticker should be "WHITE"