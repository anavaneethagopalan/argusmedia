﻿Feature: UpdatePlusDeal
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers


@ignore
Scenario: As a User, plus deal created by me can be updated by an Editor
Given I create a +DEAL
When the plus deal is updated by an Editor
Then i can see the plus deal details in market ticker with Status as "PENDING"
And the color of the plus deal in market ticker should be "ORANGE"


@ignore
Scenario: As a User, i cannot see the plus deal created by another user and updated by an Editor
Given a +DEAL is created by another user
When the plus deal is updated by an Editor
Then i cannot see the plus deal details in market ticker



@ignore
Scenario: As an Editor, i can update a pending plus deal created by me
Given I create a +DEAL as pending
When I update the pending deal
Then i can see the plus deal details in market ticker with Status as "PENDING"
And the color of the plus deal in market ticker should be "YELLOW"


@ignore
Scenario: As an Editor, i can see updated plus deal created by another Editor
Given a pending +DEAL is created by another Editor
When the editor updates the plus deal
Then i can see the plus deal details in market ticker with Status as "PENDING"
And the color of the plus deal in market ticker should be "YELLOW"



@ignore
Scenario: As an User, i cannot see updated plus deal created by an Editor
Given a pending +DEAL is created by an Editor
When the editor updates the plus deal
Then i cannot see the plus deal details in market ticker 