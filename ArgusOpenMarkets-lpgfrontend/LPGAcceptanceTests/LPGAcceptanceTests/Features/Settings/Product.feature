﻿Feature: Product
	As a user, I want to see the product definitions for products I am entitled to

@Ignore 
#Waiting for Id's to be put in for the accordion panels
@TR1
Scenario: I can see the products I have subscribed to
	When I go to the Product Settings Page
	Then I should see the products I am subscribed to
