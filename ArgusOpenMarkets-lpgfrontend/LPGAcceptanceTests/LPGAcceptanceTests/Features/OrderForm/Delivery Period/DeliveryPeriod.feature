﻿
@AOMK-636
@AOMK-635 - Day Range Delivery Period
Feature: DeliveryPeriod - Day Range
		To verify Delivery Period day Ranges


@ignore
Scenario: Verify Label for Day Range appears correct
Given I am on "BID" Form for "Product"
Then the Delivery Period label matches the one setup for the "Product"


@ignore
Scenario: Delivery Start Date is equal to default start date 
Given I am on "ASK" Form for "Product"
Then the Delivey Start Date is set to default start date set for the "Product"


@ignore
Scenario: Minimum value for Start date is equal to Product Delivery Start date
Given I am on "BID" Form for "Product"
Then the minimum value for start date is equal to delivery start date set for the "Product"


@ignore
Scenario: Maximum value for Start date is equal to Product Delivery End date - (minimum delivery range - 1)
Given I am on "ASK" Form for "Product"
Then the maximum value for start date is equal to delivery end date minus (minimum delivery range minus 1) set for the "Product"


@ignore
Scenario: Delivery End Date is equal to default End date 
Given I am on "BID" Form for "Product"
Then the Delivey End Date is set to default end date set for the "Product"


@ignore
Scenario: Minimum value for end date is equal to Product Delivery Start date + (minimum delivery range -1 )
Given I am on "BID" Form for "Product"
Then the minimum value for start date is equal to delivery start date plus (minimum delivery range minus 1 ) set for the "Product"


@ignore
Scenario: Maximum value for End date is equal to Product Delivery End date
Given I am on "ASK" Form for "Product"
Then the maximum value for start date is equal to delivery end date set for the "Product"


@ignore
Scenario: Number of days selected 
Given I am on "BID" Form for "Product"
Then number of days selected should be equal to displayed end date minus displayed start date plus one 


@ignore
Scenario: Difference between displayed start and end date can not be less than product minimum delivery range - 1 
Given I am on "BID" Form for "Product"
Then the difference between start and end dates is not less than minimum delivery range minus one set for the "Product"


