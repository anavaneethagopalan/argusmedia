﻿@ignore
@AOMK-647
@Trader1
Feature: BrokerField
		To test feature for Principal select Broker dropdown


Scenario: As an Trader, I should see a list of available Brokers
Given I am on "BID" Form for "Product" 
When i click on Broker dropdown
Then a list of permissioned brokers should be available

Scenario: As an Trader, Cobrokering checkbox disabled for Any Broker
Given I am on "BID" Form for "Product"
When i select broker as "Any Broker"
Then the cobrokering checkbox is disabled



Scenario: As an Trader, Cobrokering checkbox disabled for Bilateral/No Broker
Given I am on "BID" Form for "Product"
When i select broker as "Bilateral Only (No Broker)"
Then the cobrokering checkbox is disabled



Scenario: As an Trader, Cobrokering checkbox enabled for Specified Broker
Given I am on "BID" Form for "Product"
When i select broker as "Specified/Named Broker"
Then the cobrokering checkbox is enabled 
And the cobrokering checkbox is checked by default


Scenario: As an Trader, Principal Organisation Name is Read Only
Given I am on "BID" Form for "Product"
Then I can see the Principal Organisation Name as Read only