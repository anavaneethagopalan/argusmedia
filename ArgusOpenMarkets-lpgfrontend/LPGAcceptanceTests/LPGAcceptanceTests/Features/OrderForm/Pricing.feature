﻿@ignore
@AOMK-652
Feature: Pricing
		To test functionality for Pricing for Complex Order Form


Scenario: Decrement Price Values
Given I am on "BID" Form for "Product"
And I enter a "value" in Price Field
When I click on decrement button
Then the value in Price field should be decremented as per the "Product" setup


Scenario: Increment Price Values
Given I am on "ASK" Form for "Product"
And I enter a "value" in Price Field
When I click on increment button
Then the value in Price field should be incremented as per the "Product" setup



Scenario: Minimum Price Values
Given I am on "BID" Form for "Product"
And I enter a "value" in Price Field lower than the Price Volume Minimum set for the Product
Then an error message should be displayed for Price



Scenario: Maximum Price Values
Given I am on "ASK" Form for "Product"
And I enter a "value" in Price Field higher than the Price Volume Maximum set for the Product
Then an error message should be displayed for Price


Scenario: Blank Price Values
Given I am on "BID" Form for "Product"
And I enter a "blankvalue" in Price Field 
Then an error message should be displayed for Price


#Scenario: Verify BID/ASK choices are set up as per the Product Configuration
#Given I am on "BID" Form for "Product"
#Then the number of BID/ASK choices is equal to the number setup for the "Product"

