﻿
@AOMK-636 - Main JIRA
@AOMK-656
@AOMK-1136

Feature: QuantityField
		To check the Quantity feature for  Bid/Ask Order Form


@ignore
Scenario: Display Order for Common Quantity Values
Given I am on "BID" Form for "Product"
Then the Common Quantity Values are displayed based on the display order in database

@ignore
Scenario: Common Quantity Values
Given I am on "ASK" Form for "Product"
When I select a "value" from the Common Quantity Values
Then the Quantity field should be filled with "value"

@ignore
Scenario: Common Quantity Values upto 5 values
Given I am on "ASK" Form for "Product"
Then the Quantity field should show upto 5 common quantity values 


@ignore
Scenario: Decrement Quantity Values
Given I am on "BID" Form for "Product"
And I enter a "value" in Quantity Field
When I click on decrement button
Then the value in Quantity field should be decremented as per the "Product" setup


@ignore
Scenario: Increment Quantity Values
Given I am on "ASK" Form for "Product"
And I enter a "value" in Quantity Field
When I click on increment button
Then the value in Quantity field should be incremented as per the "Product" setup


@ignore
Scenario: Minimum Quantity Values
Given I am on "BID" Form for "Product"
And I enter a "value" in Quantity Field lower than the Order Volume Minimum set for the Product
Then an error message should be displayed for Quantity


@ignore
Scenario: Maximum Quantity Values
Given I am on "ASK" Form for "Product"
And I enter a "value" in Quantity Field higher than the Order Volume Maximum set for the Product
Then an error message should be displayed for Quantity


@ignore
Scenario: Default Quantity Values
Given I am on "ASK" Form for "Product"
Then I verify that the default Quantity value is equal to the Order Volumne default set for the Product


@ignore
@AOMK-656
Scenario: Blank Quantity Values
Given I am on "ASK" Form for "Product"
And I enter a "blankvalue" in Quantity Field 
Then an error message should be displayed for Quantity


@ignore
Scenario: Tot (Ten-over-Three) Quantity Value
Given I am on "BID" Form for "Product"
When i select "Tot" field as Quanity value
Then the Quantity Field should be set to blank


@ignore
Scenario: Fot (Fifteen-over-Three) Quantity Value
Given I am on "ASK" Form for "Product"
When i select "Fot" field as Quanity value
Then the Quantity Field should be set to blank


@ignore
@AOMK-656
Scenario: Decrement Quantity Values when Tot is selected
Given I am on "BID" Form for "Product"
And I select a "Tot" in Quantity Field
When I click on decrement button
Then the value Tot in Quantity field becomes un-selected
And the value in Quantity field should be decremented as per the "Product" setup



@ignore
Scenario: Increment Quantity Values when Tot is selected
Given I am on "ASK" Form for "Product"
And I select a "Tot" in Quantity Field
When I click on incremented button
Then the value Tot in Quantity field becomes un-selected
And the value in Quantity field should be incremented as per the "Product" setup


