﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.2.0.0
//      SpecFlow Generator Version:2.2.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace LPGAcceptanceTests.Features.OrderForm
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.2.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute()]
    public partial class QuantityFieldFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
        private Microsoft.VisualStudio.TestTools.UnitTesting.TestContext _testContext;
        
#line 1 "QuantityField.feature"
#line hidden
        
        public virtual Microsoft.VisualStudio.TestTools.UnitTesting.TestContext TestContext
        {
            get
            {
                return this._testContext;
            }
            set
            {
                this._testContext = value;
            }
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitializeAttribute()]
        public static void FeatureSetup(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext testContext)
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner(null, 0);
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "QuantityField", "\t\tTo check the Quantity feature for  Bid/Ask Order Form", ProgrammingLanguage.CSharp, new string[] {
                        "AOMK-636",
                        "-",
                        "Main",
                        "JIRA",
                        "AOMK-656",
                        "AOMK-1136"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassCleanupAttribute()]
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestInitializeAttribute()]
        public virtual void TestInitialize()
        {
            if (((testRunner.FeatureContext != null) 
                        && (testRunner.FeatureContext.FeatureInfo.Title != "QuantityField")))
            {
                global::LPGAcceptanceTests.Features.OrderForm.QuantityFieldFeature.FeatureSetup(null);
            }
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCleanupAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<Microsoft.VisualStudio.TestTools.UnitTesting.TestContext>(TestContext);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Display Order for Common Quantity Values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void DisplayOrderForCommonQuantityValues()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Display Order for Common Quantity Values", new string[] {
                        "ignore"});
#line 11
this.ScenarioSetup(scenarioInfo);
#line 12
testRunner.Given("I am on \"BID\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 13
testRunner.Then("the Common Quantity Values are displayed based on the display order in database", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Common Quantity Values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void CommonQuantityValues()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Common Quantity Values", new string[] {
                        "ignore"});
#line 16
this.ScenarioSetup(scenarioInfo);
#line 17
testRunner.Given("I am on \"ASK\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 18
testRunner.When("I select a \"value\" from the Common Quantity Values", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 19
testRunner.Then("the Quantity field should be filled with \"value\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Common Quantity Values upto 5 values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void CommonQuantityValuesUpto5Values()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Common Quantity Values upto 5 values", new string[] {
                        "ignore"});
#line 22
this.ScenarioSetup(scenarioInfo);
#line 23
testRunner.Given("I am on \"ASK\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 24
testRunner.Then("the Quantity field should show upto 5 common quantity values", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Decrement Quantity Values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void DecrementQuantityValues()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Decrement Quantity Values", new string[] {
                        "ignore"});
#line 28
this.ScenarioSetup(scenarioInfo);
#line 29
testRunner.Given("I am on \"BID\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 30
testRunner.And("I enter a \"value\" in Quantity Field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 31
testRunner.When("I click on decrement button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 32
testRunner.Then("the value in Quantity field should be decremented as per the \"Product\" setup", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Increment Quantity Values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void IncrementQuantityValues()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Increment Quantity Values", new string[] {
                        "ignore"});
#line 36
this.ScenarioSetup(scenarioInfo);
#line 37
testRunner.Given("I am on \"ASK\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 38
testRunner.And("I enter a \"value\" in Quantity Field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 39
testRunner.When("I click on increment button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 40
testRunner.Then("the value in Quantity field should be incremented as per the \"Product\" setup", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Minimum Quantity Values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void MinimumQuantityValues()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Minimum Quantity Values", new string[] {
                        "ignore"});
#line 44
this.ScenarioSetup(scenarioInfo);
#line 45
testRunner.Given("I am on \"BID\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 46
testRunner.And("I enter a \"value\" in Quantity Field lower than the Order Volume Minimum set for t" +
                    "he Product", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 47
testRunner.Then("an error message should be displayed for Quantity", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Maximum Quantity Values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void MaximumQuantityValues()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Maximum Quantity Values", new string[] {
                        "ignore"});
#line 51
this.ScenarioSetup(scenarioInfo);
#line 52
testRunner.Given("I am on \"ASK\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 53
testRunner.And("I enter a \"value\" in Quantity Field higher than the Order Volume Maximum set for " +
                    "the Product", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 54
testRunner.Then("an error message should be displayed for Quantity", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Default Quantity Values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void DefaultQuantityValues()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Default Quantity Values", new string[] {
                        "ignore"});
#line 58
this.ScenarioSetup(scenarioInfo);
#line 59
testRunner.Given("I am on \"ASK\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 60
testRunner.Then("I verify that the default Quantity value is equal to the Order Volumne default se" +
                    "t for the Product", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Blank Quantity Values")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        public virtual void BlankQuantityValues()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Blank Quantity Values", new string[] {
                        "ignore",
                        "AOMK-656"});
#line 65
this.ScenarioSetup(scenarioInfo);
#line 66
testRunner.Given("I am on \"ASK\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 67
testRunner.And("I enter a \"blankvalue\" in Quantity Field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 68
testRunner.Then("an error message should be displayed for Quantity", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Tot (Ten-over-Three) Quantity Value")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void TotTen_Over_ThreeQuantityValue()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Tot (Ten-over-Three) Quantity Value", new string[] {
                        "ignore"});
#line 72
this.ScenarioSetup(scenarioInfo);
#line 73
testRunner.Given("I am on \"BID\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 74
testRunner.When("i select \"Tot\" field as Quanity value", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 75
testRunner.Then("the Quantity Field should be set to blank", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Fot (Fifteen-over-Three) Quantity Value")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void FotFifteen_Over_ThreeQuantityValue()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Fot (Fifteen-over-Three) Quantity Value", new string[] {
                        "ignore"});
#line 79
this.ScenarioSetup(scenarioInfo);
#line 80
testRunner.Given("I am on \"ASK\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 81
testRunner.When("i select \"Fot\" field as Quanity value", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 82
testRunner.Then("the Quantity Field should be set to blank", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Decrement Quantity Values when Tot is selected")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        public virtual void DecrementQuantityValuesWhenTotIsSelected()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Decrement Quantity Values when Tot is selected", new string[] {
                        "ignore",
                        "AOMK-656"});
#line 87
this.ScenarioSetup(scenarioInfo);
#line 88
testRunner.Given("I am on \"BID\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 89
testRunner.And("I select a \"Tot\" in Quantity Field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 90
testRunner.When("I click on decrement button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 91
testRunner.Then("the value Tot in Quantity field becomes un-selected", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 92
testRunner.And("the value in Quantity field should be decremented as per the \"Product\" setup", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Increment Quantity Values when Tot is selected")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "QuantityField")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-636")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("-")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("Main")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("JIRA")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-656")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("AOMK-1136")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.IgnoreAttribute()]
        public virtual void IncrementQuantityValuesWhenTotIsSelected()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Increment Quantity Values when Tot is selected", new string[] {
                        "ignore"});
#line 97
this.ScenarioSetup(scenarioInfo);
#line 98
testRunner.Given("I am on \"ASK\" Form for \"Product\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 99
testRunner.And("I select a \"Tot\" in Quantity Field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 100
testRunner.When("I click on incremented button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 101
testRunner.Then("the value Tot in Quantity field becomes un-selected", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 102
testRunner.And("the value in Quantity field should be incremented as per the \"Product\" setup", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
