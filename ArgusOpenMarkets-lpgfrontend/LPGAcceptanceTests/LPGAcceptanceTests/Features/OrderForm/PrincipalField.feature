﻿
@ignore
@AOMK-641
@Broker1
Feature: PrincipalField
		To test feature for Broker select Principal dropdown


Scenario: As an Broker, Trader Organisation Name is Read Only
Given I am on "ASK" Form for "Product"
Then I can see the Broker Organisation Name as Read only


Scenario: As an Broker, Cobrokering checkbox is set by default
Given I am on "ASK" Form for "Product"
Then i can see cobrokering checkbox enabled by default as per the product setup


Scenario: As an Broker, I should see a list of Available Traders
Given I am on "ASK" Form for "Product" 
When i click on Principal dropdown
Then a list of permissioned traders should be available


Scenario: As an Broker, i cannot create an order without a principal
Given I am on "ASK" Form for "Product" 
When i fill in the "ASK" Form without a principal selected
Then the broker cannot create an order