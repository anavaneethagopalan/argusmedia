﻿Feature: CreateMarketInfo
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@ignore
Scenario: 01. As an user, i create a market info
Given I create a market info
Then the details appear in the market ticker with status as "PENDING"
And the color of the info in market ticker is "ORANGE"

@ignore
Scenario: 02. As an user, i am not able to see a market info created by another user
Given a market info is created by another user
Then i cannot see the market info details in market ticker

@ignore
Scenario: 03. As an Editor, i am able to see a market info created by any user
Given a market info is created by any user
Then i cannot see the market info details in market ticker with Status as "PENDING"
And the color of the info in market ticker is "YELLOW"




