﻿Feature: VerifyMarketInfo
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers



@ignore
Scenario: 05. As an Editor, i am able to verify a pending market info
Given a pending market info is created by any Editor/user
Then i can see the market info details in market ticker with Status as "NEW"
And the color of the info in market ticker is "ORANGE"


@ignore
Scenario: 07. As an Editor, i am able to create a verified market info
Given I create a market info as VERIFIED
Then the details appear in the market ticker with status as "NEW"
And the color of the info in market ticker is "ORANGE"

@ignore
Scenario: 08. As an Editor, i am able to see a verified market info created by any Editor
Given a verified market info is created by any Editor
Then the details appear in the market ticker with status as "NEW"
And the color of the info in market ticker is "ORANGE"

@ignore
Scenario: 09. As an User, i am able to see a verified market info created by an Editor
Given a verified market info is created by an Editor
Then the details appear in the market ticker with status as "NEW"
And the color of the info in market ticker is "WHITE"