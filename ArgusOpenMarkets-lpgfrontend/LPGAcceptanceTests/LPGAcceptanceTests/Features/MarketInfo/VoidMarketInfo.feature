﻿Feature: VoidMarketInfo
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers


@ignore
Scenario: 01. As an Editor, i am able to void a market info created by an user
Given a market info is created by an user
When i void the market info
Then the details are updated in market ticker with Status as "VOIDED"
And the color of the info in market ticker is "WHITE"

@ignore
Scenario: 02. As an User, i am able see my market info voided by an Editor
Given I create a market info
When the Editor void the market info
Then the details are updated in market ticker with Status as "VOIDED"
And the color of the info in market ticker is "ORANGE"

@ignore
Scenario: 03. As a User, i am unable to see a voided market info created by another user
Given a pending market info is created by an user
When the market info is voided by an Editor
Then i cannot see the voided market info in market ticker


@ignore
Scenario: 04. As an Editor, i am able to void a pending market info created by myself
Given I create a market info as PENDING
When i void the market info
Then the details are updated in market ticker with Status as "VOIDED"
And the color of the info in market ticker is "WHITE"

@ignore
Scenario: 05. As an Editor, i am able to see a market info voided by any Editor
Given a pending market info is created by Editor
And the pending market info is voided by Editor
Then i should see the voided market info details in market ticker with STATUS as "VOIDED"
And the color of the info in market ticker is "WHITE"


@ignore
Scenario: 06. As an User, i am able to see a market info voided by an Editor
Given a pending market info is created by Editor
And the pending market info is voided by Editor
Then i should see the voided market info in market ticker with Status as "VOIDED"
And the color of the info in market ticker is "ORANGE"



@ignore
Scenario: : 07. As an Editor, i am able to void a verified market info
Given I create a market info as VERIFIED
When I void the verified market info
Then i can see the updated details in the market ticker as "VOIDED"
And the color of the info in market ticker is "ORANGE"


@ignore
Scenario: : 08. As an User, i am able to see a verified market info voided by Editor
Given a verified market info is created by an Editor
When I void the verified market info
Then i can see the voided market info details in the market ticker as "VOIDED"
And the color of the info in market ticker is "WHITE"
