﻿Feature: UpdateMarketInfo
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@ignore
Scenario: 01. As an Editor, i am able to update the market info created by an user
Given a market info is created by any user
When editor update the market info 
Then the details are updated in the market ticker with status as "PENDING"
And the color of the market ticker is "YELLOW"

@ignore
Scenario: 02. As an User, i am able to see the my market info updated by Editor
Given I create a market info
When editor update the market info
Then i can see the updated details in the market ticker with status as "PENDING" 
And the color of the market ticker is "YELLOW"

@ignore
Scenario: 03. As an Editor, i am able to update my pending market info
Given I create a market info as PENDING
When i update the pending market info
Then i can see the updated details in the market ticker as "PENDING"
And the color of the info in market ticker is "YELLOW"


@ignore
Scenario: 04. As an Editor, i am able to update a verified market info
Given I create a market info as VERIFIED
When I update the verified market info
Then i can see the updated details in the market ticker as "UPDATED"
And the color of the info in market ticker is "ORANGE"





