﻿Feature: PendingMarketInfo
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers


@ignore
Scenario: 04. As an Editor, i am able to create a pending market info
Given I create a market info as PENDING
Then the details appear in the market ticker with status as "PENDING"
And the color of the info in market ticker is "YELLOW"

@ignore
Scenario: 05. As an user, i am not able to see a pending market info created by an Editor
Given a pending market info is created by an User
Then i cannot see the market info details in market ticker

@ignore
Scenario: 06. As an Editor, i am able to see a pending market info created by any Editor
Given a pending market info is created by any Editor
Then i can see the market info details in market ticker with Status as "PENDING"
And the color of the info in market ticker is "YELLOW"