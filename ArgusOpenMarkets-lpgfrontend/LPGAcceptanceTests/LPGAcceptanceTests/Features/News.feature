﻿
Feature: News
	As a AOM user, when news is published by censhare, I want to see it on the AOM dashboard

@TR1	
Scenario: News published on AOM subscribed stream
	When a news article is published that I am subscribed to
	Then I can see the news article appear on the dashboard widget


@TR1	
Scenario: News published on AOM unsubcribed stream
	 When a news article is published that I am not subscribed to
	 Then I should not see the news article appear on the dashboard widget

@TR1	
Scenario: News Updated to unsubcribed stream
	 Given a news article is published that I am subscribed to
	 And I can see the news article appear on the dashboard widget
	 When this news article content stream is updated to an AOM non subscribed version 
	 Then the news article should be removed from the dashboard widget

@TR1
Scenario: News Deleted 
	Given a news article is published that I am subscribed to
	When the news article is deleted
	Then the news article should be removed from the dashboard widget 





