﻿Feature: Login


Scenario Outline: 01.Login with Correct credentials
Given I navigate to the loginPage
When I successfully login with correct details
Then i should be see my "<fullname>" on dashboard page	
Examples:
 | username | password  | fullname |
 | TR1      | Password% | LPG Trader 1 |

Scenario Outline: 02.Login with Incorrect credentials
Given I navigate to the loginPage
When I enter incorrect "<username>" and "<password>" 
Then i should see error message on login page	
Examples:
 | username | password  |
 | LT141    | Password% |
 | LT34     | Password  |


 
Scenario Outline: 03.Login with blocked user credentials
Given I navigate to the loginPage
When I enter incorrect "<username>" and "<password>" 
Then i should see blocked error message on login page	
Examples:
 | username | password  |
 | BlockedUser     | Password% |

#Bug has been raised on this item as it sometimes fails and get the website in an incorrect state.
 @AMOK-1151
 Scenario Outline: 04.Login with Blank username or password
Given I navigate to the loginPage
When I enter incorrect "<username>" and "<password>" 
Then Sign In button should be greyed out
Examples:
 | username | password  |
 | LT14     |           |
 |          | Password% |


 Scenario Outline: 05.Verify Forgotton Password functionality works
Given I am on Forgotten Password Page
When I enter "<username>" and click on submit
Then a forgotten password message should be displayed
And an email notification is sent to "<username>"
Examples:
| username |
| ae1_old  |

Scenario Outline: 06. Forgotton Password Functionality for incorrect username
Given I am on Forgotten Password Page
When I enter "<username>" and click on submit
Then an error message should be displayed
Examples:
| username |
| xyz      |


Scenario Outline: 07. Verify Remember me on this computer functionality works
Given I navigate to the loginPage
When I enable Remember me on this computer checkbox
And I successfully login with correct details
Then the username cookie should be populated with correct "<username>" value
Examples:
| username | password  |
| TR1      | Password% |

@AOMK-620
@Ignore
Scenario Outline: 08. Login - Temp Password user should not be allowed to login to dashboard
Given I navigate to the loginPage
When I enter temp username "<username>" and password "<password>" 
And I go to the dashboard page without logging on
Then I should be on the login page
Examples:
| username | password  |
| Temp     | Password% |

@AOMK-620

Scenario Outline: 09. Login - User with temp password should be able to change password and login
Given I have a temporary password set for "<username>"
When I change my password on the reset Password Page
Then I should be able to login with my new password
Examples: 
| username |
| Temp     |



