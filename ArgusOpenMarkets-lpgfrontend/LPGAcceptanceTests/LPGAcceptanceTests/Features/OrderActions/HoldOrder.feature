﻿Feature: HoldOrder
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers


@ignore
Scenario: 01. As a trader, orders created by me can be Held
Given a "bid" for product "X" is created by myself
When I hold the order 
Then the order is striked-through on the stack 
And the market ticker status is "Withdrawn"



@ignore
Scenario: 02. As a broker, orders created by a trader can be held
Given a "bid" for product "X" is created by trader
When I hold the order
Then the order is striked-through on the stack
And the market ticker status is "Withdrawn"


@ignore
Scenario: 03. I cannot hold the order created by another trader from different org
Given a "bid" for product "<Product>" is created by another allowed trader
Then i can't hold the order