﻿Feature: KillOrder
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@ignore
Scenario: 01. As a trader, i can kill my own order
Given a "bid" for product "<Product>" is created by myself
When i kill the order
Then the order disappears from the bid ask stack
And the market ticker status is shown as "WITHDRAWN"

@ignore
Scenario: 02. As a broker, i can kill an order created by an allowed trader
Given a "ask" for product "<Product>" is created by an allowed trader
When i kill the order
Then the order disappears from the bid ask stack
And the market ticker status is shown as "WITHDRAWN"

@ignore
Scenario: 03. I cannot kill the order created by another trader from different org
Given a "bid" for product "<Product>" is created by another allowed trader
Then i can't kill the order