﻿@Ignore
Feature: CreateOrder
	As a user I should be able to create an order so it can be acted upon it

##CREATE ORDER VIA FRONT END
@ignore
@AOMK-647
Scenario: Trader can create a bid with Any Broker
	Given I am logged in as "Trader1"
	When the bid form is filled in with Any Broker
	And create an order
	Then the order should appear in the appropriate stack
	And the hoverbox shows the correct order details


@ignore
@AOMK-647
Scenario: Trader can create a bid with No Broker
	Given I am logged in as "Trader1"
	When the bid form is filled in with No Broker
	And create an order
	Then the order should appear in the appropriate stack
	And the hoverbox shows the correct order details

@ignore
@AOMK-647
Scenario: Trader can create a bid with Specified Broker
	Given I am logged in as "Trader1"
	When the bid form is filled in with Specified Broker
	And create an order
	Then the order should appear in the appropriate stack
	And the hoverbox shows the correct order details

@ignore
Scenario: Broker can create an bid
	Given I am logged in as "Broker1"
	When the ask form is filled in
	And create an order
	Then the order should appear in the appropriate stack

@ignore
Scenario: Editor can't create an Bid
	Given I am logged in as "Editor1"
	Then I should not be able to create an order



# European Product
@ignore
@AOMK-656
Scenario: Submitted order with “ToT” a 20,500 numeric value should be saved to Quantity within the DB Order table
Given I am logged in as "Trader1"
When the bid form is filled in for the "Product" with a Tot Quantity value
And create an order
Then the order should appear in the appropriate stack
And a value of 20500 should be saved in database table

@ignore
@AOMK- 635
Scenario: Display error message on incorrect delivery dates
Given I am logged in as "Trader1"
When the bid form is filled in for the "Product" with a start and end date less than minimum delivery range minus one
And I create an order 
Then toastr message should be displayed
And the order should not be created in bid/ask stack


