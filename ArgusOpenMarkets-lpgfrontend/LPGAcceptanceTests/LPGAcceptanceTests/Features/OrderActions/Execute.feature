﻿Feature: Execute


@ignore
Scenario: 01. As a trader, orders created by me cannot be executed
Given a "bid" for product "X" is created by myself
Then the order cannot be executed


@ignore
Scenario: 02. As a broker, orders with Any broker created by allowed trader can be executed
Given a "bid" for product "X" is created with "Any" broker by another allowed trader 
When i execute the order with a principal
Then the order disappears from the bid ask stack
Then the order should appear in market ticker with STATUS "NEW"


@ignore
Scenario: 03. As a broker1, orders with named broker created by allowed trader can be executed
Given a "bid" for product "X" is created with broker1 by another allowed trader 
When i execute the order with a principal
Then the order disappears from the bid ask stack
Then the order should appear in market ticker with STATUS "NEW"


@ignore
Scenario: 04. As a broker1, orders with other broker created by allowed trader can be executed
Given a "bid" for product "X" is created with broker2 by another allowed trader 
Then i cannot execute the order


@ignore
Scenario: 05. As a trader, orders created by allowed trader can be excuted
Given a "bid" for product "X" is created by allowed trader
When i execute the order with No broker
Then the order disappears from the bid ask stack
Then the order should appear in market ticker with STATUS "NEW"


@ignore
Scenario: 06. As a trader, orders created with NO broker by allowed trader can be excuted
Given a "bid" for product "X" is created with "NO" broker by allowed trader
When i execute the order
Then the order disappears from the bid ask stack
Then the order should appear in market ticker with STATUS "NEW"

@ignore
Scenario: 07. As a trader, orders created with named broker by allowed trader can be excuted
Given a "bid" for product "X" is created with named broker by allowed trader
When i execute the order
Then the order disappears from the bid ask stack
Then the order should appear in market ticker with STATUS "NEW"

@ignore
Scenario: 08. As a trader, orders created with ANY broker by allowed trader can be excuted
Given a "bid" for product "X" is created with "ANY" broker by allowed trader
When i execute the order with a named broker
Then the order disappears from the bid ask stack
Then the order should appear in market ticker with STATUS "NEW"

@ignore
Scenario: 09. As a trader, orders created by non-allowed trader cannot be excuted
Given a "bid" for product "X" is created by non-allowed trader
Then i cannot execute the order