﻿
@ignore

Feature: UpdateOrder


Scenario: 01. As a trader, orders created by me can be updated
Given a "bid" for product "X" is created by myself
When I update the order 
Then the updated order appears in the Market Ticker
And the hoverbox has been updated


Scenario: 02. As a allowed broker, order created by trader can be update
Given a "bid" for product "Product" is created by another allowed trader
When I update the order
Then the update order appears in the Market Ticker


Scenario: 03. As a allowed broker, order created by me can be updated
Given a "bid" for product "Product" is created by me
When I update the order
Then the update order appears in the Market Ticker

@AOMK-641
Scenario: As a allowed broker, order created by me can be updated
Given a "bid" for product "Product" is created by me
When I update the order
Then the principal field is non editable and greyed out


Scenario: I cannot update the order created by another trader from different org
Given a "bid" for product "Product" is created by another allowed trader
Then I can't update the order