﻿Feature: ReinstateOrder
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers


@ignore
Scenario: 01. As a trader, i can reinstate my own held order
Given i have "bid" for product "X" in held state
When i reinstate the order
Then the order reappears on the bid ask stack
And the market ticker status is shown as "NEW"

@ignore
Scenario: 02. As a broker, i can reinstate the order held by allowed trader
Given i have "bid" for product "X" in held state
When I reinstate the order
Then the order reappears on the bid ask stack
And the market ticker status is shown as "NEW"

@ignore
Scenario: 03. I cannot reinstate the order created by another trader from different org
Given a "bid" for product "<Product>" is created by another allowed trader
Then i can't reinstate the order