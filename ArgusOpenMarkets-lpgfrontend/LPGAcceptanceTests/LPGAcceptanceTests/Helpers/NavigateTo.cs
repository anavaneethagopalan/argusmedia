﻿using LPGAcceptanceTests.Helpers.Context;
using LPGAcceptanceTests.Pages;
using System;
using System.Configuration;
using System.Linq;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Helpers
{
    public class NavigateTo
    {
        private readonly string baseurl = ConfigurationManager.AppSettings["TestUrl"];
        private readonly string loginpage = "/login";
        private readonly string logout = "/logout";
        private readonly string dashboard = "/";
        private readonly string forgottenPasswordPage = "/login/forgotten-password";

        #region SettingsUrl's

        private readonly string settingspage = "/settings";
        private string Mysettingspage => $"{settingspage}/mysettings";
        private string Companysettings => $"{settingspage}/companysettings";
        private string Counterpartypermissions => $"{settingspage}/counterpartypermissions";
        private string Brokerpermissions => $"{settingspage}/brokerpermissions";
        private string Systeminformation => $"{settingspage}/systeminformation";
        private string Productsettings => $"{settingspage}/productsettings";

        #endregion SettingsUrl's

        private readonly string _trader1user = ConfigurationManager.AppSettings["Trader1"];
        private readonly string _trader2user = ConfigurationManager.AppSettings["Trader2"];
        private readonly string _broker1user = ConfigurationManager.AppSettings["Broker1"];
        private readonly string _broker2user = ConfigurationManager.AppSettings["Broker2"];
        private readonly string _editor1user = ConfigurationManager.AppSettings["Editor1"];
        private readonly string _password = ConfigurationManager.AppSettings["Password"];

        public NavigateTo(bool logmein = true)
        {
            if (!logmein)
            {
                if (DriverManager.Driver.Url.Contains(baseurl) && !DriverManager.Driver.Url.Contains(loginpage))
                    DriverManager.Driver.Navigate().GoToUrl(baseurl + logout);

                return;
            }
            //Already Logged in
            if (DriverManager.Driver.Url.Contains(baseurl))
            {
                if (!ScenarioContext.Current.ScenarioInfo.Tags.Contains(UserContext.Username))
                {
                    DriverManager.Driver.Navigate().GoToUrl(baseurl + logout);
                    LoginUserFromTag();
                }
                else
                {
                    Console.WriteLine("User already logged in so do nothing");
                }
            }
            //Not Logged in:
            else
            {
                LoginUserFromTag();
            }
        }

        private void LoginUserFromTag()
        {
            if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("BR1"))
            {
                NavigateToLoginPage();
                var loginpage = new LoginPage();

                loginpage.EnterSuccessfulLoginDetails(_broker1user, _password);
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("BR2"))
            {
                NavigateToLoginPage();
                new LoginPage().EnterSuccessfulLoginDetails(_broker2user, _password);
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("TR2"))
            {
                NavigateToLoginPage();
                new LoginPage().EnterSuccessfulLoginDetails(_trader2user, _password);
            }
            else if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("ED1"))
            {
                NavigateToLoginPage();
                new LoginPage().EnterSuccessfulLoginDetails(_editor1user, _password);
            }
            else
            {
                NavigateToLoginPage();
                new LoginPage().EnterSuccessfulLoginDetails(_trader1user, _password);
            }
        }

        public void NavigateToLoginPage()
        {
            DriverManager.Driver.Navigate().GoToUrl(baseurl + loginpage);
        }

        public void NavigateToForgottenPasswordPage()
        {
            DriverManager.Driver.Navigate().GoToUrl(baseurl + forgottenPasswordPage);
        }

        public void NavigateToDashBoardPage()
        {
            if (!DriverManager.Driver.Url.Equals(baseurl + dashboard))
            {
                DriverManager.Driver.Navigate().GoToUrl(baseurl + dashboard);
            }
        }

        #region NavigateToSettingsPages

        public void NavigateToMySettingsPage()
        {
            DriverManager.Driver.Navigate().GoToUrl(baseurl + Mysettingspage);
        }

        public void NavigateToCompanySettingsPage()
        {
            DriverManager.Driver.Navigate().GoToUrl(baseurl + Companysettings);
        }

        public void NavigateToCounterPartyPermission()
        {
            DriverManager.Driver.Navigate().GoToUrl(baseurl + Counterpartypermissions);
        }

        public void NavigateToBrokerPermission()
        {
            DriverManager.Driver.Navigate().GoToUrl(baseurl + Brokerpermissions);
        }

        public void NavigateToSysteminformation()
        {
            DriverManager.Driver.Navigate().GoToUrl(baseurl + Systeminformation);
        }

        public void NavigateToProductionSettings()
        {
            DriverManager.Driver.Navigate().GoToUrl(baseurl + Productsettings);
        }

        #endregion NavigateToSettingsPages
    }
}