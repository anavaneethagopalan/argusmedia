﻿using AOM.Transport.Events.News;

namespace LPGAcceptanceTests.Helpers
{
    using Argus.Transport;
    using Argus.Transport.Configuration;
    using Argus.Transport.Infrastructure;
    using Argus.Transport.Messages.Dtos;
    using Argus.Transport.Messages.Events;
    using Argus.Transport.Serializers;
    using System;

    internal class NewsBusHelper : IDisposable
    {
        private readonly IBus _newsbus;

        private readonly IBus _aomBus;

        public NewsBusHelper()
        {
            _newsbus = Transport.CreateBus(ConnectionConfiguration.FromConnectionStringName("news.transport.bus.local"), new JilSerializer());
            var aomConnection = ConnectionConfiguration.FromConnectionStringName("aombus");
            _aomBus = Transport.CreateBus(aomConnection, new JilSerializer());
        }

        public void Publish<T>(T message) where T : class
        {
            _newsbus.Publish(message);
        }

        public void PublishPurgeAomNews()
        {
            var request = new PurgeNewsRequest
            {
                DaysToKeep = 0
            };

            _aomBus.Publish(request);
        }

        public string PublishNews(string cmsid)
        {
            var news = DefaultArticle(cmsid);
            //news.CmsId = "0";

            Publish(new NewsPersisted
            {
                News = news
            });
            return news.Headline;
        }

        private PersistedArticle DefaultArticle(string cmsId)
        {
            var currentdatetime = DateTime.Now.ToUniversalTime();
            return new PersistedArticle
            {
                CmsId = cmsId,
                CmsVersion = "8",
                ContentStreams = new[]
                {
                    new PersistedMetaTypeDatum
                    {
                        DomainId = 95028,
                        DomainTypeId = 95,
                        Name = "Argus European Products"
                    },
                },
                Contexts = new PersistedMetaTypeDatum[0],
                DomainId = 200112028,
                Headline = $"Breaking News! Testers have inserted a news articles with {cmsId}",
                IsFeatured = false,
                IsFree = false,
                Language = new PersistedLanguage
                {
                    DomainId = 1,
                    CultureName = "en-GB",
                    Description = "English",
                    IsoDescription = "en"
                },
                NewsType = new PersistedMetaDatum
                {
                    DomainId = 1,
                    Name = "Daily news"
                },
                PublicationDate = new DateTime(currentdatetime.Year, currentdatetime.Month, currentdatetime.Day, currentdatetime.Hour, currentdatetime.Minute, currentdatetime.Second, DateTimeKind.Utc),
                Regions = new[]
                {
                    new PersistedMetaTypeDatum(),
                },
                Sectors = new[]
                {
                    new PersistedMetaTypeDatum(),
                },
                Story =
                    "<html xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:my=\"http://www.censhare.com/xml/3.0.0/my\" xmlns:cs=\"http://www.censhare.com/xml/3.0.0/xpath-functions\"><head><link href=\"../../ArgusStaticContent/news.css\" type=\"text/css\" rel=\"stylesheet\"></link><title >Nathan Loves you! French gas storage bookings edge up</title><meta charset=\"utf-8\"></meta></head><body><p class=\"lead\">London, 9 October (Argus) — Allocated storage capacity at Storengy sites has increased with more bookings in southern France.</p><p>Reserved capacity at Serene Sud was 4TWh today, up from 3.8TWh <a href=\"https://direct.argusmedia.com/newsandanalysis/article/1547224\">previously</a>.</p><p>The TRS day-ahead market expanded its discount to the November contract to €4.05/MWh on 6 October, wider than the cost of capacity at Serene Sud of €3.50/MWh.</p><p>This would offer an incentive to book capacity to inject early this month and withdraw the gas next month, rather than in the first quarter when the stockdraw is typically the quickest.</p><p>TRS near-curve prices have climbed in recent days as French nuclear plant outages have been <a href=\"https://direct.argusmedia.com/newsandanalysis/article/1547212\">extended</a>.</p><p>The November contract climbed to a premium to the first-quarter 2018 market at the previous close and the day-ahead market\'s discount to the first-quarter 2018 contract was €3.45/MWh, just tighter than the cost of Serene Sud storage capacity.</p><p>Aggregate French stocks could be 99.6TWh at the start of the heating season if all booked capacity is filled — including <a href=\"https://direct.argusmedia.com/newsandanalysis/article/1538998\">performance gas</a>. This would be down from the three-year 1 November average of 122TWh.</p><p>4690768</p><div class=\"article-footer\"><p><br></br> Send comments to <a href=\"mailto:feedback@argusmedia.com\" target=\"_parent\"> feedback@argusmedia.com </a></p><p><u><a href=\"http://view.argusmedia.com/GLO-WEB-2016-07-ALL-News-Webforms_03-News-on-Argus-Direct.html\" target=\"_TOP\"> Request more information </a></u> about Argus\' energy and commodity news, data and analysis services. </p><p><i> Copyright © 2017 Argus Media Ltd - <a href=\"http://www.argusmedia.com/\" target=\"_TOP\"> www.argusmedia.com </a> - All rights reserved. </i></p></div></body></html>",
                Timestamp = new DateTime(currentdatetime.Year, currentdatetime.Month, currentdatetime.Day, currentdatetime.Hour, currentdatetime.Minute, currentdatetime.Second, DateTimeKind.Utc)
            };
        }

        public void PublishNewsUpdateHeadline(int cmsId)
        {
            var news = DefaultArticle(cmsId.ToString());

            news.Headline = $"Breaking News! Testers have Updated the news article for {news.CmsId}";
            Publish(new NewsUpdatePersisted
            {
                News = news,
                PreviousVersion = DefaultArticle(cmsId.ToString())
                // PreviousVersion = new PersistedArticle()
            });
        }

        public void PublishNewsUpdateToUnsubscribed(string cmsId)
        {
            var news = DefaultArticle(cmsId);
            news.ContentStreams[0].DomainId = 95509;
            news.ContentStreams[0].DomainTypeId = 95;
            news.ContentStreams[0].Name = "News Test";

            Publish(new NewsUpdatePersisted
            {
                News = news,
                PreviousVersion = DefaultArticle(cmsId)
            });
        }

        public void PublishNewsUnsubscribed(int cmsId)
        {
            var news = DefaultArticle(cmsId.ToString());
            //news.CmsId = "4697903";
            news.ContentStreams[0].DomainId = 95509;
            news.ContentStreams[0].DomainTypeId = 95;
            news.ContentStreams[0].Name = "News Test";

            Publish(new NewsPersisted
            {
                News = news
            });
        }

        public void PublishNewsDeleted(string cmsId)
        {
            Publish(new PersistedNewsDeleted()//PersistedNewsDeleted
            {
                News = DefaultArticle(cmsId)
                // News = DefaultArticle(),
            });
        }

        public void PublishNewsRefresh(string cmsId)
        {
            Publish(new NewsPersistedRefresh
            {
                News = DefaultArticle(cmsId)
            });
        }

        public void Dispose()
        {
            _newsbus?.Dispose();
            _aomBus?.Dispose();
        }
    }
}