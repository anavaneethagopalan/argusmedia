﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;

namespace LPGAcceptanceTests.Helpers
{
    public class SQLHelper
    {
        private readonly string _environment = ConfigurationManager.AppSettings["Environment"];
        public string _tablePrefix = ConfigurationManager.AppSettings["TablePrefix"];

        private readonly MySqlConnection _con;
        public MySqlConnection aomsqlconnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["aom"].ConnectionString);
        public MySqlConnection crmsqlconnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["crm"].ConnectionString);

        public SQLHelper(bool crmuser = false)
        {
            if (!crmuser)
            {
                _con = aomsqlconnection;
                _con.Open();
            }
            else
            {
                _con = crmsqlconnection;
                _con.Open();
            }
        }

        private int? RunScalarSql(string sqlquery)
        {
            MySqlCommand command = new MySqlCommand(sqlquery, _con);
            var returnvalue = command.ExecuteScalar();
            return Convert.ToInt32(returnvalue);
        }

        public void ExecuteNonQuery(string sqlquery)
        {
            MySqlCommand command = new MySqlCommand(sqlquery, _con);
            var x = command.ExecuteNonQuery();
        }

        public DataTable ExecuteSql(string sqlquery)
        {
            var results = new DataTable();
            using (MySqlCommand command = new MySqlCommand(sqlquery, _con))
            {
                using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command))
                {
                    dataAdapter.Fill(results);
                }
            }

            return results;
        }

        public int GetUserIdFromUserName(string username)
        {
            var sqlquery = $@"SELECT id
                                FROM `crm.{_tablePrefix}`.UserInfo ui
                                WHERE ui.Username = '{username}'; ";
            return Convert.ToInt32(RunScalarSql(sqlquery));
        }

        public DataTable GetEmailDetails()
        {
            var sqlquery = $@"SELECT Id,Body
                     FROM `aom.{_tablePrefix}`.Email
                     WHERE body like ('%reset%password%')
                     ORDER BY Id DESC
                     LIMIT 1;";
            return ExecuteSql(sqlquery);
        }

        public DataTable GetSubscribedProductsForUserId(int userId)
        {
            var sqlquery = $@"Select DISTINCT p.Name 'Product'
                            From `crm.{_tablePrefix}`.UserInfo ui
                            , `crm.{_tablePrefix}`.Organisation o
                            , `aom.{_tablePrefix}`.Product p
                            , `crm.{_tablePrefix}`.SubscribedProduct sp
                            , `crm.{_tablePrefix}`.UserInfoRole uir
                            , `crm.{_tablePrefix}`.OrganisationRole org
                            , `crm.{_tablePrefix}`.ProductRolePrivilege prp
                            , `crm.{_tablePrefix}`.SubscribedProductPrivilege spp
                            Where ui.Id = {userId}
                            And ui.organisation_Id_fk = o.Id
                            And o.id = sp.Organisation_Id_fk
                            And sp.Product_id_fk = p.id
                            And ui.id = uir.UserInfo_Id_fk
                            And sp.Product_id_fk = spp.Product_id_fk
                            And prp.Product_id_fk = spp.Product_id_fk
                            And prp.ProductPrivilege_id_fk = spp.ProductPrivilege_id_fk
                            And prp.Organisation_Id_fk = spp.Organisation_Id_fk
                            And uir.Role_id_fk = prp.Role_id_fk
                            And uir.Role_id_fk = org.Id
                            And prp.Organisation_Id_fk = org.Organisation_Id_fk
                            Group by org.Name, p.Name";
            return ExecuteSql(sqlquery);
        }

        public void InsertNewPerformanceRow(Pages page, double elapsedTotalSeconds, string version)
        {
            var sql = $"Insert into `aom.{_tablePrefix}`.PerfomanceTests " +
                      $"values ('{version}', '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', '{page}', {elapsedTotalSeconds});";

            ExecuteNonQuery(sql);
        }

        public void UpdateTPassword(int userid, string hashedpassword)
        {
            var sql = $@"Set autocommit= 1;
                        Update `crm.ci`.UserCredentials
                        Set CredentialType = 'T', Password = '{hashedpassword}'
                        Where UserInfo_Id_fk = {userid};";

            //TODO: Commit update maybe
            ExecuteNonQuery(sql);
        }

        public long GetProductIdFromName(string productname)
        {
            throw new NotImplementedException();
        }
    }
}