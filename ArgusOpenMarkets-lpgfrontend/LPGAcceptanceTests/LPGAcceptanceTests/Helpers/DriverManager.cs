﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.ObjectModel;
using System.Configuration;

//using Protractor;

namespace LPGAcceptanceTests.Helpers
{
    public class DriverManager
    {
        public static IWebDriver Driver = null;

        public void SetDriver()
        {
            if (Driver == null)
            {
                switch (ConfigurationManager.AppSettings["Browser"])
                {
                    case "IE":
                        var ieoptions = new InternetExplorerOptions();
                        ieoptions.EnsureCleanSession = true;
                        ieoptions.IgnoreZoomLevel = true;
                        ieoptions.EnablePersistentHover = false;
                        ieoptions.EnableNativeEvents = false;
                        // ieoptions.SetLoggingPreference(LogType.Browser,LogLevel.All);
                        Driver = new InternetExplorerDriver(ieoptions);
                        Console.WriteLine("STARTED ON -- IE");
                        //    NgDriver = new NgWebDriver(Driver);
                        break;

                    case "Firefox":
                        //  var firefoxoptions = new FirefoxOptions();
                        // firefoxoptions.SetLoggingPreference(LogType.Browser,LogLevel.All);
                        Driver = new FirefoxDriver();
                        Console.WriteLine("STARTED ON -- FIREFOX");
                        break;

                    case "Chrome":
                        var chromeoptions = new ChromeOptions();
                        var service = ChromeDriverService.CreateDefaultService();
                        chromeoptions.SetLoggingPreference(LogType.Browser, LogLevel.All);
                        Driver = new ChromeDriver(service, chromeoptions, TimeSpan.FromSeconds(300));
                        Console.WriteLine("STARTED ON -- CHROME");
                        break;

                    case "Edge":
                        Driver = new EdgeDriver();
                        break;

                    default:
                        throw new Exception("Driver Not set Properly");
                }
                Driver.Manage().Window.Maximize();
                Driver.Manage().Cookies.DeleteAllCookies();
                Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            }
        }

        public void ExitDriver()
        {
            if (Driver != null)
            {
                Console.WriteLine("Exiting Driver ......");
                Driver.Quit();
                Driver.Dispose();
                Driver = null;
            }
        }

        public ReadOnlyCollection<LogEntry> GenerateLogs()
        {
            return Driver.Manage().Logs.GetLog(LogType.Browser);
        }
    }
}