﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPGAcceptanceTests.Helpers
{
  public  class EncryptionHelper
    {
        private char Encode(char input)
        {
            switch (input)
            {
                case '0':
                    return 'Z';

                case '1':
                    return 'Y';

                case '2':
                    return 'X';

                case '3':
                    return 'W';

                case '4':
                    return 'V';

                case '5':
                    return 'e';

                case '6':
                    return 'd';

                case '7':
                    return 'c';

                case '8':
                    return 'b';

                default:
                    return 'a';
            }
        }

        private char Decode(char input)
        {
            switch (input)
            {
                case 'Z':
                    return '0';

                case 'Y':
                    return '1';

                case 'X':
                    return '2';

                case 'W':
                    return '3';

                case 'V':
                    return '4';

                case 'e':
                    return '5';

                case 'd':
                    return '6';

                case 'c':
                    return '7';

                case 'b':
                    return '8';

                default:
                    return '9';
            }

        }

        public int DecodeService(string input)
        {
            var items = input.ToCharArray();
            var resultBuilder = new StringBuilder();
            foreach (var i in items)
            {
                resultBuilder.Append(Decode(i));
            }
            return Convert.ToInt32(resultBuilder.ToString());
        }


        public string Encode(long input)
        {
            var items = input.ToString(CultureInfo.InvariantCulture).ToCharArray();
            var resultBuilder = new StringBuilder();
            foreach (var item in items)
            {
                resultBuilder.Append(Encode(item));
            }

            return resultBuilder.ToString();
        }
    }
}

