﻿using System.Diagnostics;
using System.Linq;
using System.Reflection;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Helpers
{
    public class PerformanceGraph
    {
        public void PerformanceReport(Pages page, Stopwatch stopwatch)
        {
            if (!ScenarioContext.Current.ScenarioInfo.Tags.Contains("LiveSmoke"))
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                var version = AssemblyName.GetAssemblyName(assembly.Location).Version.ToString();
                if (!version.Equals("1.0.0.0"))
                {
                    new SQLHelper().InsertNewPerformanceRow(page, stopwatch.Elapsed.TotalSeconds, version);
                }
            }
        }
    }
}