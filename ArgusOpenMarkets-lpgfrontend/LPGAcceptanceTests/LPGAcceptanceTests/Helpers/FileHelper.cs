﻿using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace LPGAcceptanceTests.Helpers
{
    public class FileHelper
    {
        public static void WriteToFile(string fullpath, List<string> list)
        {
            string lastFolderName = Path.GetFileName(Path.GetDirectoryName(fullpath));

            CreateDirectory(lastFolderName);

            File.WriteAllLines(fullpath, list);
        }

        public static void CreateDirectory(string directorypath)
        {
            if (!Directory.Exists(directorypath))
            {
                Directory.CreateDirectory(directorypath);
            }
        }

        public static void GrantAccess(string fullPath)
        {
            DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
            //return true;
        }

        public static void Empty(string filepath, string filetype = "")
        {
            DirectoryInfo directory = new DirectoryInfo(filepath);
            if (directory.Exists)
                foreach (FileInfo file in directory.GetFiles(filetype, SearchOption.AllDirectories))
                    file.Delete();
        }
    }
}