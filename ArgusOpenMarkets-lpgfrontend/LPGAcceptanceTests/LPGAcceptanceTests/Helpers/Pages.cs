﻿namespace LPGAcceptanceTests.Helpers
{
    public enum Pages
    {
        LoginPage,
        SettingsPage,
        DashboardPage,
        CounterPartyPage,
        ResetPasswordPage,
        ForgottenPasswordPage,
        SystemInformationPage
    }
}