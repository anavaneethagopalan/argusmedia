﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace LPGAcceptanceTests.Helpers
{
    public class SeleniumHelper
    {
        private IWebDriver driver = DriverManager.Driver;

        public void WaitForElementTobeDisplayed(IWebElement element, int timeinsec)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeinsec));
            wait.Until(d => element.Displayed);
        }

        public string GetCookie(string cookiename)
        {
            string cookievalue;

            try
            {
                cookievalue = driver.Manage().Cookies.GetCookieNamed(cookiename).Value;
            }
            catch (NullReferenceException e)
            {
                return null;
            }

            return cookievalue;
        }

        public void WaitForElementToDisappear(string elementclasstext, int timeinsec)
        {
            //TODO: The explicit wait is not working here? So implicity set it here...
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
            var spinners = driver.FindElements((By.ClassName(elementclasstext)));

            var spinnersdissappeared = false;
            if (spinners.Count > 0)
            {
                for (int i = 0; i < timeinsec * 2; i++)
                {
                    Thread.Sleep(500);

                    if (driver.FindElements((By.ClassName(elementclasstext))).Count == 0)
                    {
                        spinnersdissappeared = true;
                        break;
                    }
                }
                Assert.IsTrue(spinnersdissappeared, "Spinners still present on page");
            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        public void WaitforTextToAppear(string elementlocator, string text, int timeinsec)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeinsec));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.CssSelector(elementlocator), text));
        }
    }
}