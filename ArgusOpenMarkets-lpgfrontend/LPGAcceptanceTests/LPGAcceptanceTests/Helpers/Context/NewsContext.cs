﻿namespace LPGAcceptanceTests.Helpers.Context
{
    public class NewsContext
    {
        public string CmsId { get; set; }
        public string Heading { get; set; }
    }
}