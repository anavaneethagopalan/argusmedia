﻿using LPGAcceptanceTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Hooks
{
    [Binding]
    public class Events
    {
        public static string screenshotFolder = ConfigurationManager.AppSettings["WebDriverScreenshotsFolder"];
        public IWebDriver Driver = DriverManager.Driver;

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            using (var news = new NewsBusHelper())
            {
                news.PublishPurgeAomNews();
                news.PublishNews("1");
                news.Dispose();
            }
            Console.WriteLine("Initializing Test URL: " + ConfigurationManager.AppSettings["TestUrl"]);
            Console.WriteLine("Initializing Driver: " + ConfigurationManager.AppSettings["Environment"]);
            Console.WriteLine("Browser: " + ConfigurationManager.AppSettings["Browser"]);
            Console.WriteLine("Deleting Screenshots from previous Run");
            Console.WriteLine("Application Path " + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            //  string filepath = Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), screenshotFolder);
            if (Directory.Exists(screenshotFolder))
            {
                FileHelper.Empty(screenshotFolder, "*.jpeg");
            }
            else
            {
                Console.WriteLine("File Does not exist....Creating File");
                FileHelper.CreateDirectory(screenshotFolder);
                Console.WriteLine("File Created");
            }
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            new DriverManager().SetDriver();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            var testcontext = ScenarioContext.Current.ScenarioContainer.Resolve<TestContext>();
            var title = ScenarioContext.Current.ScenarioInfo.Title;

            var screenshotfilename = string.Format(@"{0}\{1}_{2}.jpeg", screenshotFolder, title,
                DateTime.Now.ToString(DateTime.Now.ToString("ddMMyyHHmmssff")));

            if (ScenarioContext.Current.TestError != null)
            {
                Console.WriteLine("Taking Screenshots");
                var image = TakeScreenShot();
                image.Save(screenshotfilename, ImageFormat.Jpeg);
                testcontext.AddResultFile(screenshotfilename);

                //Browser logs works only for Chrome
                if (ConfigurationManager.AppSettings["Browser"] == "Chrome")
                    testcontext.AddResultFile(GetBrowserLogsAndAttachToTest());

                new DriverManager().ExitDriver();
            }
        }

        private static string GetBrowserLogsAndAttachToTest()
        {
            var logitems = new List<string>();
            var logs = new DriverManager().GenerateLogs();
            foreach (var log in logs)
            {
                logitems.Add(string.Format(log.Timestamp + " " + log.Level + " " + log.Message + Environment.NewLine));
            }

            var filepath = screenshotFolder + "browser_log" + DateTime.Now.ToString(DateTime.Now.ToString("ddMMyyHHmmssff"));
            FileHelper.WriteToFile(filepath, logitems);
            return filepath;
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            new DriverManager().ExitDriver();
        }

        private Bitmap TakeScreenShot()
        {
            Bitmap stitchedImage = null;
            try
            {
                long totalwidth1 = (long)((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript("return document.body.offsetWidth");//documentElement.scrollWidth");

                long totalHeight1 = (long)((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript("return  document.body.parentNode.scrollHeight");

                int totalWidth = (int)totalwidth1;
                int totalHeight = (int)totalHeight1;

                // Get the Size of the Viewport
                long viewportWidth1 = (long)((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript("return document.body.clientWidth");//documentElement.scrollWidth");
                long viewportHeight1 = (long)((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript("return window.innerHeight");//documentElement.scrollWidth");

                int viewportWidth = (int)viewportWidth1;
                int viewportHeight = (int)viewportHeight1;

                // Split the Screen in multiple Rectangles
                List<Rectangle> rectangles = new List<Rectangle>();
                // Loop until the Total Height is reached
                for (int i = 0; i < totalHeight; i += viewportHeight)
                {
                    int newHeight = viewportHeight;
                    // Fix if the Height of the Element is too big
                    if (i + viewportHeight > totalHeight)
                    {
                        newHeight = totalHeight - i;
                    }
                    // Loop until the Total Width is reached
                    for (int ii = 0; ii < totalWidth; ii += viewportWidth)
                    {
                        int newWidth = viewportWidth;
                        // Fix if the Width of the Element is too big
                        if (ii + viewportWidth > totalWidth)
                        {
                            newWidth = totalWidth - ii;
                        }

                        // Create and add the Rectangle
                        Rectangle currRect = new Rectangle(ii, i, newWidth, newHeight);
                        rectangles.Add(currRect);
                    }
                }

                // Build the Image
                stitchedImage = new Bitmap(totalWidth, totalHeight);
                // Get all Screenshots and stitch them together
                Rectangle previous = Rectangle.Empty;
                foreach (var rectangle in rectangles)
                {
                    // Calculate the Scrolling (if needed)
                    if (previous != Rectangle.Empty)
                    {
                        int xDiff = rectangle.Right - previous.Right;
                        int yDiff = rectangle.Bottom - previous.Bottom;
                        // Scroll
                        //selenium.RunScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        ((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        System.Threading.Thread.Sleep(200);
                    }

                    // Take Screenshot
                    var screenshot = ((ITakesScreenshot)DriverManager.Driver).GetScreenshot();

                    // Build an Image out of the Screenshot
                    Image screenshotImage;
                    using (MemoryStream memStream = new MemoryStream(screenshot.AsByteArray))
                    {
                        screenshotImage = Image.FromStream(memStream);
                    }

                    // Calculate the Source Rectangle
                    Rectangle sourceRectangle = new Rectangle(viewportWidth - rectangle.Width, viewportHeight - rectangle.Height, rectangle.Width, rectangle.Height);

                    // Copy the Image
                    using (Graphics g = Graphics.FromImage(stitchedImage))
                    {
                        g.DrawImage(screenshotImage, rectangle, sourceRectangle, GraphicsUnit.Pixel);
                    }

                    // Set the Previous Rectangle
                    previous = rectangle;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error on taking screenshots: " + e);
            }
            return stitchedImage;
        }
    }
}