﻿using AOM.App.Domain.Entities;
using LPGAcceptanceTests.Helpers;
using LPGAcceptanceTests.Helpers.Context;
using LPGAcceptanceTests.RabbitMQHelpers;
using System;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Steps.BidAskStack
{
    [Binding]
    public sealed class ViewOrdersInBidAskStack
    {
        private UserContext _usercontext;

        public ViewOrdersInBidAskStack(UserContext usercontext)
        {
            _usercontext = usercontext;
        }

        [When(@"I create a ""(.*)"" for product ""(.*)"" with ""(.*)"" pricelines and ""(.*)"" basis")]
        public void WhenICreateABidForProductWithPricelinesAndBasis(string ordertype, string productname, int numberofpricelines, int numberofbasis)
        {
            ScenarioContext.Current.Pending();
            //Create a default message type
            var complexOrder = new ComplexMessage();
            var defaultorder = complexOrder.DefaultOrder();
            defaultorder.ProductName = productname;
            defaultorder.ProductId = new SQLHelper().GetProductIdFromName(productname);
            defaultorder.OrderType = (OrderType)Enum.Parse(typeof(OrderType), ordertype);
            complexOrder.CreateNumberPricelines(defaultorder, numberofpricelines);
            complexOrder.CreateNumberofBasis(defaultorder, numberofbasis);
        }

        [Then(@"the order should appear in the bid stack")]
        public void ThenTheOrderShouldAppearInTheBidStack()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"the hoverbox shows the relevant information")]
        public void ThenTheHoverboxShowsTheRelevantInformation()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"the order should appear in the ask stack")]
        public void ThenTheOrderShouldAppearInTheAskStack()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I create an order with co-brokering on")]
        public void WhenICreateAnOrderWithCo_BrokeringOn()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"the hoverbox should show cobrokering enabled")]
        public void ThenTheHoverboxShouldShowCobrokeringEnabled()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I click on the ""(.*)"" button for product x")]
        public void WhenIClickOnTheButtonForProductX(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should see the relevant modal window should appear")]
        public void ThenIShouldSeeTheRelevantModalWindowShouldAppear()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I create an order that contains notes")]
        public void WhenICreateAnOrderThatContainsNotes()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should see the notes icon in bid/ask stack")]
        public void ThenIShouldSeeTheNotesIconInBidAskStack()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I create an order with a quantity text value of ""(.*)""")]
        public void WhenICreateAnOrderWithAQuantityTextValueOf(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should see ""(.*)"" on the bid/ask stack")]
        public void ThenIShouldSeeOnTheBidAskStack(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I create an order with a negative price")]
        public void WhenICreateAnOrderWithANegativePrice()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should see the negative price on the bid/ask stack")]
        public void ThenIShouldSeeTheNegativePriceOnTheBidAskStack()
        {
            ScenarioContext.Current.Pending();
        }
    }
}