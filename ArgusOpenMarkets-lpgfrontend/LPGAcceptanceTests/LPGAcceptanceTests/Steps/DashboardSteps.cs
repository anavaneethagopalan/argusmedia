﻿using LPGAcceptanceTests.Helpers;
using LPGAcceptanceTests.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Steps
{
    [Binding]
    public class DashboardSteps
    {
        [Given(@"I am on dashboard page")]
        public void GivenIAmOnDashboardPage()
        {
            new NavigateTo().NavigateToDashBoardPage();
        }

        [When(@"I go to the dashboard page without logging on")]
        public void WhenIGoToTheDashboardPage()
        {
            new NavigateTo(false).NavigateToDashBoardPage();
        }

        [When(@"I navigate back from dashboard page")]
        public void WhenINavigateBackFromDashboardPage()
        {
            var page = new DashboardPage();
            page.NavigateBack();
        }

        [Then(@"a warning message should appear on the AOM screen")]
        public void ThenAWarningMessageShouldAppearOnTheAOMScreen()
        {
            var page = new DashboardPage();
            var expected = "Changes you made may not be saved";
            var actual = page.GetAlertText();
            // Assert.AreEqual(expected,actual,"Alert not present");
            Assert.IsNotNull(actual, "Warning message not present");
        }
    }
}