﻿using LPGAcceptanceTests.Helpers;
using LPGAcceptanceTests.Helpers.Context;
using LPGAcceptanceTests.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Steps
{
    [Binding]
    public class LoginSteps
    {
        private UserContext _usercontext;

        public LoginSteps(UserContext usercontext)
        {
            _usercontext = usercontext;
        }

        [Given(@"I navigate to the loginPage")]
        public void GivenINavigateToTheLoginPage()
        {
            new NavigateTo(false).NavigateToLoginPage();
        }

        [When(@"I enter incorrect ""(.*)"" and ""(.*)""")]
        public void WhenIEnterIncorrectAnd(string username, string password)
        {
            var page = new LoginPage();
            page.EnterIncorrectDetails(username, password);
        }

        [When(@"I successfully login with correct details")]
        public void WhenISuccessfullyLoginWithCorrectDetails()
        {
            var page = new LoginPage();
            UserContext.Username = ConfigurationManager.AppSettings["Trader1"];
            UserContext.password = ConfigurationManager.AppSettings["Password"];
            page.EnterSuccessfulLoginDetails(UserContext.Username, UserContext.password);
        }

        [When(@"I enter temp username ""(.*)"" and password ""(.*)""")]
        public void WhenIEnterIncorrectUsernameAndPassword(string username, string password)
        {
            var page = new LoginPage();
            page.EnterTempDetails(username, password);
        }

        [Then(@"I should be on the login page")]
        public void ThenIShouldBeOnTheLoginPage()
        {
            var loginpage = new LoginPage();
        }

        [Then(@"i should be see my ""(.*)"" on dashboard page")]
        public void ThenIShouldBeSeeMyOnHomePage(string fullname)
        {
            var page = new DashboardPage();
            page.WaitforSpinner();
            var actual = page.GetFullName();
            Assert.AreEqual(fullname, actual, "You are not the logged in user");
        }

        [Then(@"i should see error message on login page")]
        public void ThenIShouldBeSeeErrorMessageOnHomePage()
        {
            var page = new LoginPage();
            string expected = "Invalid login details, please try again.";
            string actual = page.GetErrorMessage();
            Assert.AreEqual(expected, actual);
        }

        [Then(@"i should see blocked error message on login page")]
        public void ThenIShouldSeeBlockedErrorMessageOnHomePage()
        {
            var page = new LoginPage();
            string expected = "This account is currently blocked by AOM. Please contact the Argus AOM administration team. Email: AOMSupport@argusmedia.com. Tel +44 020 7199 9430";
            string actual = page.GetErrorMessage();
            Assert.AreEqual(expected, actual);
        }

        [Then(@"Sign In button should be greyed out")]
        public void ThenSignInButtonShouldBeGreyedOut()
        {
            var page = new LoginPage();

            Assert.IsFalse(page.VerifyEnabled(), "Sign In button is enabled");
        }

        [Given(@"I am on Forgotten Password Page")]
        public void GivenIAmOnForgottenPasswordPage()
        {
            new NavigateTo(false).NavigateToForgottenPasswordPage();
        }

        [When(@"I enter ""(.*)"" and click on submit")]
        public void WhenIEnterAndClickOnSubmit(string username)
        {
            //Get Emailid before forgotten password email is sent by user
            var emailDetails = new SQLHelper().GetEmailDetails();
            _usercontext.emailId = Convert.ToInt32(emailDetails.Rows[0]["Id"]);
            UserContext.Username = username;
            var page = new ForgottenPasswordPage();
            page.EnterUsername(username);
            page.ClickOnSubmit();
        }

        [Then(@"a forgotten password message should be displayed")]
        public void ThenAForgottenPasswordMessageShouldBeDisplayed()
        {
            var page = new ForgottenPasswordPage();
            var expected =
                "Thank you for your request. An email has been sent to you with a link to a page where you can change your password.";
            var actual = page.GetForgottenPasswordMessage();
            Assert.AreEqual(expected, actual, "Forgotton Password message does not appear");
        }

        [Then(@"an error message should be displayed")]
        public void ThenAnErrorMessageShouldBeDisplayed()
        {
            var page = new ForgottenPasswordPage();
            var expected = "Invalid user name: " + UserContext.Username;
            var actual = page.GetForgottenPasswordErrorMessage();
        }

        [Given(@"I have a temp password set")]
        public void GivenIHaveATempPasswordSet()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"an email notification is sent to ""(.*)""")]
        public void ThenAnEmailNotificationIsSentTo(string username)
        {
            var helper = new SQLHelper();
            var emailTable = helper.GetEmailDetails();
            var emailId = Convert.ToInt32(emailTable.Rows[0]["Id"]);

            // Verify Email id is greater than previous one
            Assert.IsTrue((emailId) > _usercontext.emailId, "Email message not sent");

            var messagebody = emailTable.Rows[0]["Body"].ToString();
            var index = messagebody.Trim().LastIndexOf('@');

            // Verify email message is generated
            Assert.IsFalse(index < 0, "Email Body empty");

            //Replace /t/n/r from the email body
            var message = Regex.Replace(messagebody.Substring(index + 1), @"\t|\n|\r", "");

            //Get userid from email message body
            var actualuserid = new EncryptionHelper().DecodeService(message);
            var expecteduserid = helper.GetUserIdFromUserName(username);

            // Verify password reset request email is sent to correct username
            Assert.AreEqual(expecteduserid, actualuserid, "Password reset email sent to wrong user");
        }

        [When(@"I enable Remember me on this computer checkbox")]
        public void WhenIEnableRememberMeOnThisComputerCheckbox()
        {
            var page = new LoginPage();
            page.CheckRememberMe();
        }

        [Then(@"the username cookie should be populated with correct ""(.*)"" value")]
        public void ThenTheUsernameCookieShouldBePopulatedWithCorrectValue(string username)
        {
            var helper = new SeleniumHelper();
            var actual = helper.GetCookie("aom-username");

            var encodedTextBytes = Convert.FromBase64String(actual);

            var actualUsernameTextValue = Encoding.UTF8.GetString(encodedTextBytes);

            Assert.AreEqual(username, actualUsernameTextValue, "Username cookie is not stored");
        }

        [Given(@"I have a temporary password set for ""(.*)""")]
        public void GivenIHaveATemporaryPasswordSetFor(string username)
        {
            var userid = new SQLHelper().GetUserIdFromUserName(username);
            //This password for Temp User should be = Password12
            new SQLHelper(true).UpdateTPassword(userid, "fnQxcot7rNSnoB20TGs1qH4sDH8YGPY10cO3/rTpy20=");
            UserContext.Username = username;
            UserContext.password = "Password12";
        }

        [When(@"I change my password on the reset Password Page")]
        public void WhenIChangeMyPasswordOnTheResetPasswordPage()
        {
            new NavigateTo(false).NavigateToLoginPage();
            var resetPasswordPage = new LoginPage().EnterTempDetails(UserContext.Username, UserContext.password);
            var newpassword = "PermPass12";

            resetPasswordPage.SubmitResetPassword(newpassword);
            UserContext.password = newpassword;
        }

        [Then(@"I should be able to login with my new password")]
        public void ThenIShouldBeAbleToLoginWithMyNewPassword()
        {
            new NavigateTo(false).NavigateToLoginPage();

            new LoginPage().EnterSuccessfulLoginDetails(UserContext.Username, UserContext.password);
        }
    }
}