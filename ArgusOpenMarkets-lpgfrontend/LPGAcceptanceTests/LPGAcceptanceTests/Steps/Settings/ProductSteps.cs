﻿using LPGAcceptanceTests.Helpers;
using LPGAcceptanceTests.Helpers.Context;
using LPGAcceptanceTests.Pages.Settings;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Steps.Settings
{
    [Binding]
    public sealed class ProductSteps
    {
        [When(@"I go to the Product Settings Page")]
        public void WhenIGoToTheProductSettingsPage()
        {
            new NavigateTo().NavigateToProductionSettings();
        }

        [Then(@"I should see the products I am subscribed to")]
        public void ThenIShouldSeeTheProductsIAmSubscribedTo()
        {
            var sqlhelper = new SQLHelper();
            var id = sqlhelper.GetUserIdFromUserName(UserContext.Username);
            //TODO: this is bringing back a dataTable, but might be better to use a list?
            var subscribedproducts = new SQLHelper().GetSubscribedProductsForUserId(id);

            var actualproductsonUI = new ProductsSettingsPage().GetAllProductsDisplayed();

            actualproductsonUI.CompareRows(subscribedproducts);
        }
    }
}