﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LPGAcceptanceTests.Helpers;
using LPGAcceptanceTests.Pages.Settings;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Steps.Settings
{
    [Binding]
    public sealed class SystemInformationSteps
    {
        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef

        [When(@"I go to the System Information Page")]
        public void WhenIGoToTheSystemInformationPage()
        {
            new NavigateTo().NavigateToSysteminformation();
        }

        [Then(@"I expect to see the System Information Page")]
        public void ThenIExpectToSeeTheHeading()
        {
            //The constructor already checks that we are on the correct page
            var systeminformationPage = new SystemInformationPage();
        }
    }
}