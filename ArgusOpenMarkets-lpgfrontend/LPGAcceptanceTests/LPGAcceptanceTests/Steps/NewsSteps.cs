﻿using LPGAcceptanceTests.Helpers;
using LPGAcceptanceTests.Helpers.Context;
using LPGAcceptanceTests.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TechTalk.SpecFlow;

namespace LPGAcceptanceTests.Steps
{
    [Binding]
    public sealed class NewsSteps
    {
        private readonly NewsContext _news;

        public NewsSteps(NewsContext news)
        {
            _news = news;
        }

        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef

        [Given(@"a news article is published that I am subscribed to")]
        [When(@"a news article is published that I am subscribed to")]
        public void WhenANewsArticleIsPublishedThatIAmSubscribedTo()
        {
            var cmsId = new Random().Next().ToString();
            using (var newsbus = new NewsBusHelper())
            {
                newsbus.PublishNewsDeleted(cmsId);
                var newsheadline = newsbus.PublishNews(cmsId);
                _news.Heading = newsheadline;
            }
            _news.CmsId = cmsId;
        }

        [Given(@"I can see the news article appear on the dashboard widget")]
        [Then(@"I can see the news article appear on the dashboard widget")]
        public void ThenIShouldSeeTheNewsArticleAppearOnTheDashboardWidget()
        {
            //TODO: Check for header and Body match
            new NavigateTo().NavigateToDashBoardPage();
            var dashboard = new DashboardPage();

            Assert.IsTrue(dashboard.DoesNewArticleAppear(_news.CmsId), $"News Article with cmsId {_news.CmsId} is not shown in news widget");
        }

        [When(@"a news article is published that I am not subscribed to")]
        public void WhenANewsArticleIsPublishedThatIAmNotSubscribedTo()
        {
            var cmsId = new Random().Next();
            using (var newsbus = new NewsBusHelper())
            {
                newsbus.PublishNewsUnsubscribed(cmsId);
            }
            _news.CmsId = cmsId.ToString();
        }

        [Then(@"I should not see the news article appear on the dashboard widget")]
        [Then(@"the news article should be removed from the dashboard widget")]
        public void ThenIShouldNotSeeTheNewsArticleAppearOnTheDashboardWidget()
        {
            new NavigateTo().NavigateToDashBoardPage();

            Assert.IsTrue(new DashboardPage().DoesNewArticleAppear("1", 30), $"News Article with cmsId {_news.CmsId} is not shown in news widget");
            Assert.IsFalse(new DashboardPage().DoesNewArticleAppear(_news.CmsId, 30, false), $"News Article with {_news.CmsId} is shown in news widget");
        }

        [When(@"this news article content stream is updated to an AOM non subscribed version")]
        public void WhenThisNewsArticleContentStreamIsUpdatedToAnAOMNonSubscribedVersion()
        {
            using (var newsbus = new NewsBusHelper())
            {
                newsbus.PublishNewsUpdateToUnsubscribed(_news.CmsId);
            }
        }

        [When(@"the news article is deleted")]
        public void WhenTheNewsArticleIsDeleted()
        {
            using (var newsbus = new NewsBusHelper())
            {
                newsbus.PublishNewsDeleted(_news.CmsId);
            }
        }
    }
}