﻿using LPGAcceptanceTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;

namespace LPGAcceptanceTests.Pages
{
    public class ResetPasswordPage : BasePage
    {
        public ResetPasswordPage()
        {
            var stopwatch = Stopwatch.StartNew();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.TagName("h4"), "Reset Password"));
            stopwatch.Stop();
            new PerformanceGraph().PerformanceReport(Helpers.Pages.ResetPasswordPage, stopwatch);
        }

        [FindsBy(How = How.TagName, Using = "h4")] private IWebElement _resetPasswordHeading;

        [FindsBy(How = How.Id, Using = "reset-new-password")] private IWebElement _resetPasswordNew;

        [FindsBy(How = How.Id, Using = "confirm-new-password")] private IWebElement _resetPasswordConfirm;

        [FindsBy(How = How.Id, Using = "reset-submit")] private IWebElement _resetSubmitButton;

        [FindsBy(How = How.Id, Using = "reset-message")] private IWebElement _resetmessage;

        public void SubmitResetPassword(string newpassword)
        {
            _resetPasswordNew.SendKeys(newpassword);
            _resetPasswordConfirm.SendKeys(newpassword);
            _resetSubmitButton.Click();

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TextToBePresentInElement(_resetmessage, "Your password has been changed successfully."));
        }
    }
}