﻿using LPGAcceptanceTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Diagnostics;
using System.Linq;

namespace LPGAcceptanceTests.Pages
{
    public class ForgottenPasswordPage : BasePage
    {
        public ForgottenPasswordPage()
        {
            var stopwatch = Stopwatch.StartNew();
            Assert.AreEqual("Forgotten Password", header.Text, "You are not on Forgotten Password Page");
            stopwatch.Stop();
            new PerformanceGraph().PerformanceReport(Helpers.Pages.ForgottenPasswordPage, stopwatch);
        }

#pragma warning disable 0649

        [FindsBy(How = How.TagName, Using = "h4")]
        private IWebElement header;

        [FindsBy(How = How.Id, Using = "forgotten-password-email")]
        private IWebElement username;

        private IWebElement submitBtn => Driver.FindElements(By.TagName("button")).Single(x => x.GetAttribute("type").Equals("submit"));

        [FindsBy(How = How.Id, Using = "forgotten-password-message")]
        private IWebElement forgottenpwdmessage;

        [FindsBy(How = How.Id, Using = "error-message")]
        private IWebElement forgottenpwderrmessage;

#pragma warning restore 0649

        public ForgottenPasswordPage EnterUsername(string user)
        {
            username.Clear();
            username.SendKeys(user);
            return this;
        }

        public ForgottenPasswordPage ClickOnSubmit()
        {
            submitBtn.Click();
            return new ForgottenPasswordPage();
        }

        public string GetForgottenPasswordMessage()
        {
            new SeleniumHelper().WaitforTextToAppear("#forgotten-password-message p", "Thank you", 10);
            return forgottenpwdmessage.Text;
        }

        public string GetForgottenPasswordErrorMessage()
        {
            new SeleniumHelper().WaitforTextToAppear("#error-message", "Invalid", 10);
            return forgottenpwderrmessage.Text;
        }
    }
}