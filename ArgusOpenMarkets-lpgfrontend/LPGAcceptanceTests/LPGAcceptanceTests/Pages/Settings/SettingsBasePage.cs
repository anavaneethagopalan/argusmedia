﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LPGAcceptanceTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace LPGAcceptanceTests.Pages.Settings
{
    public class SettingsBasePage : BasePage
    {
        [FindsBy(How = How.LinkText, Using = "My Settings")] private IWebElement mysettingslink;

        public SettingsBasePage()
        {
            new SeleniumHelper().WaitForElementTobeDisplayed(mysettingslink, 30);
            Assert.AreEqual("My Settings", mysettingslink.Text, "You are not on the settings page");
        }
    }
}