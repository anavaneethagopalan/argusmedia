﻿using LPGAcceptanceTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System.Diagnostics;
using System.Linq;

namespace LPGAcceptanceTests.Pages.Settings
{
    public class SystemInformationPage : SettingsBasePage
    {
        private IWebElement _systemInformationHeader => Driver.FindElements(By.TagName("h1")).Single(x => x.Text.Contains("System"));

        public SystemInformationPage()
        {
            var stopwatch = Stopwatch.StartNew();
            new SeleniumHelper().WaitForElementTobeDisplayed(_systemInformationHeader, 30);
            Assert.AreEqual("System Information", _systemInformationHeader.Text, "You are not on the System Information Page");
            stopwatch.Stop();
            new PerformanceGraph().PerformanceReport(Helpers.Pages.SystemInformationPage, stopwatch);
        }
    }
}