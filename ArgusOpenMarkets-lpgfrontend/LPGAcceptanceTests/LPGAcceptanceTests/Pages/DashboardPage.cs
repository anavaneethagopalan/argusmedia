﻿using LPGAcceptanceTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace LPGAcceptanceTests.Pages
{
    public class DashboardPage : BasePage
    {
        public DashboardPage()
        {
            var stopwatch = Stopwatch.StartNew();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.TagName("a"), "Argus Open Markets"));
            Assert.AreEqual("Argus Open Markets", pgheader.Text, "You are not on the dashboard page");
            new SeleniumHelper().WaitForElementToDisappear(spinnerClassString, 30);
            stopwatch.Stop();
            new PerformanceGraph().PerformanceReport(Helpers.Pages.DashboardPage, stopwatch);
        }

#pragma warning disable 0649

        [FindsBy(How = How.TagName, Using = "a")]
        private IWebElement pgheader;

        [FindsBy(How = How.TagName, Using = "button")]
        private IWebElement logout;

        private string SpinnerClass = "glyphicon-loading-spinner";

        [FindsBy(How = How.Id, Using = "news-container")]
        private IWebElement news_container;

        private ReadOnlyCollection<IWebElement> news_headings =>
            news_container.FindElements(By.TagName("accordion-group"));

        // [FindsBy(How = How.ClassName, Using = "glyphicon-loading-spinner")]
        private string spinnerClassString = "glyphicon-loading-spinner";

        [FindsBy(How = How.ClassName, Using = "list-unstyled")]
        private IWebElement fullname;

#pragma warning restore 0649

        public string GetText()
        {
            return logout.Text;
        }

        public string GetFullName()
        {
            var nametext = fullname.Text.Split('\r')[0].Replace("\n", "");
            return nametext;
        }

        public DashboardPage NavigateBack()
        {
            Driver.Navigate().Back();
            return this;
        }

        public string GetAlertText()
        {
            var alert = Driver.SwitchTo().Alert();
            if (alert.Text != null)
                return alert.Text;
            return null;
        }

        public DashboardPage WaitforSpinner()
        {
            new SeleniumHelper().WaitForElementToDisappear(spinnerClassString, 30);

            return new DashboardPage();
        }

        public bool DoesNewArticleAppear(string cmsId, int timeout = 30, bool waittodisappear = true)
        {
            var mynews = news_headings.Any(x => x.Text.Contains(cmsId));

            for (int i = 0; i < timeout; i++)
            {
                if (mynews == waittodisappear)
                {
                    return mynews;
                }

                Thread.Sleep(500);
                mynews = Driver.FindElements(By.TagName("accordion-group")).Any(x => x.Text.Contains(cmsId));
            }

            return mynews;
        }

        public bool WaitForNewsArticleRemoved(string cmsId, int timeout = 30)
        {
            var mynews = news_headings.Any(x => x.Text.Contains(cmsId));

            for (int i = 0; i < timeout; i++)
            {
                if (!mynews)
                {
                    return true;
                }
                Thread.Sleep(500);
                mynews = Driver.FindElements(By.TagName("accordion-group")).Any(x => x.Text.Contains(cmsId));
            }

            return false;
        }
    }
}