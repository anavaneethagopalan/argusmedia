﻿using LPGAcceptanceTests.Helpers;
using LPGAcceptanceTests.Helpers.Context;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.Linq;

namespace LPGAcceptanceTests.Pages
{
    public class LoginPage : BasePage
    {
        public LoginPage()
        {
            var stopwatch = Stopwatch.StartNew();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            try
            {
                wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.TagName("h4"), "Already registered? Login below"));
            }
            catch (WebDriverTimeoutException e)
            {
                throw new Exception("You are not on the Login Page");
            }
            stopwatch.Stop();
            new PerformanceGraph().PerformanceReport(Helpers.Pages.LoginPage, stopwatch);
        }

#pragma warning disable 0649

        [FindsBy(How = How.TagName, Using = "h4")]
        private IWebElement header;

        [FindsBy(How = How.Id, Using = "login-form-email")]
        private IWebElement username;

        [FindsBy(How = How.Id, Using = "login-form-password")]
        private IWebElement password;

        [FindsBy(How = How.Id, Using = "login-form-sign-in")]
        private IWebElement SignInBtn;

        [FindsBy(How = How.Id, Using = "login-form-message")]
        private IWebElement errorMessage;

        //[FindsBy(How = How.Id, Using = "rememberMe")]
        //private IWebElement rememberMeCheckbox;
        private IWebElement rememberMeCheckbox => Driver.FindElements(By.TagName("label")).Single(x => x.Text.Contains("Remember"));

#pragma warning disable 0649

        public DashboardPage EnterSuccessfulLoginDetails(string user, string pwd)
        {
            username.Clear();
            username.SendKeys(user);

            password.Clear();
            password.SendKeys(pwd);

            SignInBtn.Submit();
            UserContext.Username = user;
            UserContext.password = pwd;
            var dashboard = new DashboardPage();
            return dashboard;
        }

        public LoginPage EnterIncorrectDetails(string user, string pwd)
        {
            EnterDetailsAndSubmit(user, pwd);
            UserContext.Username = user;
            UserContext.password = pwd;
            return new LoginPage();
        }

        private void EnterDetailsAndSubmit(string user, string pwd)
        {
            username.Clear();
            username.SendKeys(user);

            password.Clear();
            password.SendKeys(pwd);

            SignInBtn.Submit();
        }

        public ResetPasswordPage EnterTempDetails(string user, string pwd)
        {
            EnterDetailsAndSubmit(user, pwd);
            UserContext.Username = user;
            UserContext.password = pwd;
            return new ResetPasswordPage();
        }

        public string GetErrorMessage()
        {
            new SeleniumHelper().WaitForElementTobeDisplayed(errorMessage, 5);
            return errorMessage.Text;
        }

        public bool VerifyEnabled()
        {
            return SignInBtn.Enabled;
        }

        public LoginPage CheckRememberMe()
        {
            if (!rememberMeCheckbox.Selected)
            {
                rememberMeCheckbox.Click();
                //Actions builder = new Actions(Driver);
                //builder.MoveToElement(rememberMeCheckbox)
                //    .Click().Build().Perform();

                //  rememberMeCheckbox.Click();
            }
            return this;
        }
    }
}