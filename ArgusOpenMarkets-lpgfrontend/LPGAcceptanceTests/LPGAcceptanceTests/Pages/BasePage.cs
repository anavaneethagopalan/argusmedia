﻿using LPGAcceptanceTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.PageObjects;

namespace LPGAcceptanceTests.Pages
{
    public abstract class BasePage
    {
        protected IWebDriver Driver = DriverManager.Driver;
        // protected NgWebDriver NgDriver = DriverManager.NgDriver;

        public BasePage()
        {
            PageFactory.InitElements(Driver, this);
            WaitForAllAngular2();
        }

        private void WaitForAllAngular2()
        {
            //Lifted from protractor.net project as the wait for angular 2 project.
            const string waitForAngular2JavaScript = @"
            var waitForAllAngular2 = function(callback) {
                try {
                    var testabilities = window.getAllAngularTestabilities();
                    var count = testabilities.length;
                    var decrement = function() {
                        count--;
                        if (count === 0) {
                            callback();
                        }
                    };
                    testabilities.forEach(function(testability) {
                        testability.whenStable(decrement);
                    });
                } catch (err) {
                    callback(err.message);
                }
            };
            var callback = arguments[0];
            waitForAllAngular2(callback);
            ";

            Driver.ExecuteJavaScript(waitForAngular2JavaScript);
        }
    }
}