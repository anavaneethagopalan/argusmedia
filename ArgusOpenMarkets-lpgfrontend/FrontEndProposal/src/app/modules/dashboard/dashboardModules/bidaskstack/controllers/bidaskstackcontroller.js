angular.module('argusOpenMarkets.dashboard.bidaskstack')
.controller('bidAskStackController', function bidAskStackController($scope) {
	var initController = function(){
    $scope.bids = [{Broker: "ABC", Principal: "DEF",Delivery:"1st Oct",Quantity: 1000, Price:999 },
    				{Broker: "ABC", Principal: "DEF",Delivery:"1st Oct",Quantity: 1000, Price:999 },
    				{Broker: "ABC", Principal: "DEF",Delivery:"1st Oct",Quantity: 1000, Price:999 },
    				{Broker: "ABC", Principal: "DEF",Delivery:"1st Dec",Quantity: 1000, Price:999 },
    				{Broker: "ABC", Principal: "DEF",Delivery:"1st Dec",Quantity: 1000, Price:999 }];

	};

	$scope.bidGridOptions = {
		data:"bids",
		enableColumnReordering: true,
	    enableColumnResize: false,
	    jqueryUIDraggable:  true


	};

	initController();
});