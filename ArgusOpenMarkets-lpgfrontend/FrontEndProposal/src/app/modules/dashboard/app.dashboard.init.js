angular.module('argusOpenMarkets.dashboard',[
	'gridster',
	'argusOpenMarkets.dashboard.bidaskstack'
]).config(function config($stateProvider) {
	$stateProvider.state('Dashboard', {
		url: '/Dashboard',
		views: {
			"": {
					templateUrl: 'src/app/modules/dashboard/views/dashboard.part.html',
					controller:'dashboardController'
				},
			"bidAskStack@Dashboard": {
					templateUrl: 'src/app/modules/dashboard/dashboardModules/bidaskstack/views/bidAskStack.part.html',
					controller: 'bidAskStackController'
				},
			"ladder@Dashboard": {
					templateUrl: 'src/app/modules/dashboard/dashboardModules/ladder/views/ladder.part.html',
				},
			"marketTicker@Dashboard": {
					templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/marketTicker.part.html',
				},
			"newsFeed@Dashboard": {
					templateUrl: 'src/app/modules/dashboard/dashboardModules/newsfeed/views/newsFeed.part.html',
				}
			}
	});
});