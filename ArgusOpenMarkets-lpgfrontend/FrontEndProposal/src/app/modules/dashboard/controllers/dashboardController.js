angular.module('argusOpenMarkets.dashboard')
.controller('dashboardController', function dashboardController($scope, diffusionProvider) {
	var initController = function(){
      $scope.userDashboardConfig = getDashboardLayout(tileConfig, userDashboardConfig.tiles);
  };

  var diffusionClient = diffusionProvider.getDiffusionClient();

  var topicRoot = "RemoteControlPublisher/Commodoties/";
  var connectionDetails = {
     onDataFunction: null,
     onCallbackFunction: registerConnect,
     topic: topicRoot,
     serverHost:"localhost:8080",
     wsURL:"ws://localhost:8080",
     flashURL:"localhost:8080",
     serverPort : 8080 ,
     flashPort:8080,
     debug:false
  };


     // Now that everything is configured, lets connect!
        diffusionClient.connect(connectionDetails);


  function registerConnect(isConnected, isReconnect){
     if(isConnected){
         diffusionClient.addTopicListener("^"+topicRoot+"*", newPrice,this);
         diffusionClient.subscribe(topicRoot+"Refined/Naphtha/Naphtha-CIF-NWE-DIFF/FrontMonth");
       $scope.isConnected = true;
     }
  }




  function newPrice(msg){
     $scope.EURUSD = msg.records[0].fields[0];
     $scope.$apply();
  }



  //{topic:"RemoteControlPublisher/CurrencyExchange/EURUSD"}

  var tileConfig =  {
                      bidAskStack : {sizeX:4, sizeY:2, view:"bidAskStack"},
                      marketTicker : {sizeX:8, sizeY:3, view:"marketTicker"},
                      newsFeed : {sizeX:8, sizeY:3, view:"newsFeed"},
                      ladder: {sizeX:2, sizeY:2,view:"ladder"},
                      columns: 12
                    };

   var userDashboardConfig = {tiles : ["bidAskStack" ,"bidAskStack","ladder", "marketTicker", "newsFeed"]};

  var getDashboardLayout = function(tileConfig, tiles){
      var dashboardLayout = [];
      var column = 0;
      var row = 0;
      angular.forEach(tiles , function(configItem){
          if (tileConfig.hasOwnProperty(configItem)){
              var currentTileConfig = tileConfig[configItem];
              if(column + currentTileConfig.sizeX > tileConfig.columns){
                row += currentTileConfig.sizeY;
                column =0;
              }
              dashboardLayout.push({tile:{sizeX: currentTileConfig.sizeX, sizeY : currentTileConfig.sizeY, row: row, col: column }, view:currentTileConfig.view});
              column += currentTileConfig.sizeX;
          }
          else{
            throw "Error reading dashboard config: " + configItem;
          }
      });
      return dashboardLayout;
  };






    $scope.gridsterOptions = {
      minRows: 1, // the minimum height of the grid, in rows
      maxRows: 40,
      columns: tileConfig.columns, // the width of the grid, in columns
      colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
      rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
      margins: [10, 10], // the pixel distance between each widget
      defaultSizeX: 2, // the default width of a gridster item, if not specifed
      defaultSizeY: 1, // the default height of a gridster item, if not specified
      resizable: {
         enabled: true,

      },
      draggable: {
         enabled: true
      }
    };



	initController();
});