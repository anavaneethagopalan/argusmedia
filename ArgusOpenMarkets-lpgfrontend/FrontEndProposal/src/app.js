angular.module('argusOpenMarkets', [
  'ui.utils',
  'ui.route',
  'ui.router.state',
  'argusOpenMarkets.dashboard',
  'argusOpenMarkets.shared',
  'toastr',
  'diffusion'
  ])

.config( function () {
})

.run( function run () {
})

.controller( 'applicationController', function AppCtrl ( $scope,toastr ) {
	toastr.success('Welcome!' ,"Argus Open Markets Dashboard", {  
  		messageClass: 'toast-message',
  		positionClass: 'toast-bottom-right',
  		timeOut: 3000,
  		extendTimeout: 3000
	});
});

