ArgusOpenMarkets
================
______________

Argus Open Markets is price discovery system used by Traders and Brokers to link market participants. AOM will cover a wide array of commodity markets.

Technology:
-----------
* UI: [AngularJS](https://docs.angularjs.org/guide), [HTML5](http://www.html5rocks.com/en/resources), [Bootstrap 3](http://getbootstrap.com/getting-started/), [Angular UI](http://angular-ui.github.io/), [Toastr](https://github.com/jirikavi/AngularJS-Toaster), [Gridster](http://gridster.net/)
* UI Testing: [AngularMock](https://docs.angularjs.org/api/ngMock/), [Jasmine](http://jasmine.github.io/), [Protractor with Selenium](https://github.com/angular/protractor), [PhantomJS](http://phantomjs.org/)
* Build: [Team City](http://www.jetbrains.com/teamcity/), [Octopus](https://octopusdeploy.com/), [Grunt](http://gruntjs.com/), [Karma](http://karma-runner.github.io/0.12/index.html), [JsHint](http://www.jshint.com/)
* Package Management: [Bower](http://bower.io/), [Npm](https://www.npmjs.org/), [Nuget](http://www.nuget.org/)
* Server Side: [C# .Net](http://msdn.microsoft.com/en-us/library/618ayhy6.aspx), [RabbitMq](https://www.rabbitmq.com)
* ORM - Entity Framework
* Server Side Testing: NUnit, Moq
* Messaging: [Diffusion](http://docs.pushtechnology.com/)
* Database: [MySql](http://dev.mysql.com/downloads/mysql/)


Project Structure:
---------------------
The project will be broadly split into the UI, server side code and database scripts. The UI code can be found in the "AOM UI" folder. I recomend using Webstorm 8 as an IDE for this as it provides good support for Angular, HTML5 and Grunt.
The UI project is split into modules. The modules are based on functional area; there is a common module where shared components are stored. 



Setup Instructions:
-----------
The developer setup instructions are located here:
<a href="http://github.com/Argus-Media/ArgusOpenMarkets/wiki/Developer-Setup" target="_blank">Developer Setup</a>

Nathan Bellamore.  
