﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading;

namespace AutomateDailyEnvironmentChecks
{
    internal class Program
    {
        private static int ConcurrentUsers = 0;
        private static int MaxConcurrentUsers = 0;

        private static StringBuilder ConsoleOutput = new StringBuilder();

        private static void Main(string[] args)
        {
            var logFile = GetLogFileName();
            try
            {
                ClearFile(logFile);
                RunNodeApp();
                EMailFileContent(logFile);
            }
            catch (Exception)
            {
                
            }

            Environment.Exit(0);
        }

        private static string GetLogFileName()
        {
            var logFile = Assembly.GetExecutingAssembly().Location;
            logFile = logFile.Replace("AutomateDailyEnvironmentChecks.exe", "output.txt");

            return logFile;
        }

        private static void ClearFile(string logFile)
        {
            if (File.Exists(logFile))
            {
                File.Delete(logFile);
            }
        }

        private static void TimerCallback(object o)
        {
            var logFile = GetLogFileName();
            ClearFile(logFile);
            RunNodeApp();
            EMailFileContent(logFile);
        }

        private static void EMailFileContent(string logFile)
        {


            var diffusionData = File.ReadAllLines(logFile);
            var sb = new StringBuilder();
            foreach (var l in diffusionData)
            {
                sb.Append(l + "<br/>");
            }

            SendEmailNotification(sb.ToString());
        }

        private static void SendEmailNotification(string diffusionData)
        {
            Console.WriteLine("SENDING EMAIL");
            var client = new SmtpClient("10.21.1.83", 25);

            var from = new MailAddress("donotreply@argusmedia.com");
            // var to = new MailAddress("aomdevteam@argusmedia.com");
            var to = new MailAddress("nathan.bellamore@argusmedia.com");

            StringBuilder lines = new StringBuilder();
            foreach (var l in diffusionData)
            {
                lines.Append(l);
            }

            var message = new MailMessage(from, to)
            {
                Body = lines.ToString(),
                BodyEncoding = Encoding.UTF8,
                Subject = "AOM Daily Checks",
                SubjectEncoding = Encoding.UTF8, 
                IsBodyHtml = true
            };

            try
            {
                client.Send(message);
            }
            catch (Exception e)
            {
                var error = e.ToString();
            }

            if (client != null)
            {
                client.Dispose();
            }
        }

        private static void RunNodeApp()
        {
            var fileName = @"cmd.exe";
            var arguments = @"C:\projects\ArgusOpenMarkets\ArgusOpenMarkets\AOM\AOMMonitoring\DailyEnvironmentChecks\RunAomDailyChecks.bat";

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = "node.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.RedirectStandardOutput = true;
            // startInfo.Arguments = arguments;
            startInfo.Arguments =
                "C:\\projects\\ArgusOpenMarkets\\ArgusOpenMarkets\\AOM\\AOMMonitoring\\DailyEnvironmentChecks\\src\\runAllChecks.js";


            
            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                Process exeProcess = Process.Start(startInfo);

                // exeProcess.ErrorDataReceived += (sender, errorLine) => { if (errorLine.Data != null) data.Append(errorLine.Data); };
                exeProcess.OutputDataReceived += (sender, outputLine) => { ConsoleOutput.Append(outputLine.Data); };
                // exeProcess.BeginErrorReadLine();
                // exeProcess.BeginOutputReadLine();

                exeProcess.WaitForExit(10000);

            }
            catch (Exception ex)
            {
                ConsoleOutput.Append(ex.ToString());
            }
        }
    }
}
