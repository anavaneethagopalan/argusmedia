describe('Login Live as a user', function () {

    beforeEach(function() {
        browser.ignoreSynchronization = false;
    });

    function userLogonTest(username, password, fullName) {
        // browser.manage().timeouts().pageLoadTimeout(50);

        browser.manage().deleteAllCookies();
        browser.get('https://aomuat.argusmedia.com/#/');

        browser.waitForAngular();

        expect(browser.getTitle()).toEqual('Argus Open Markets');

        element(by.model('username')).sendKeys(username);
        element(by.model('password')).sendKeys(password);

        element(by.id('signInBtn')).click().then(function () {

            browser.wait(function () {
                return browser.isElementPresent(By.id("header-logged-on-user"));
            }, 25000);


            browser.findElement(By.id("header-logged-on-user")).then(function (elem) {
                elem.getText().then(function (txt) {
                    expect(txt).toEqual(fullName);

                    element(by.id('logout')).click().then(function () {
                        browser.driver.sleep(5000);
                    });

                    done();
                });

            });
        });
    }

    it('should logon batch one users', function(){

        var data = [
            { "username": "as", password:"Password12", "fullName": "Frederic Meeus"},
            { "username": "bs", password: "Password12", "fullName": "Volker Probst"},
            { "username": "zs", password:"Password12", "fullName": 'Andr� Altmann'}
        ];

        for (var i = 0; i < data.length; i++) {
            userLogonTest(data[i].username, data[i].password, data[i].fullName);
        }
    });

});