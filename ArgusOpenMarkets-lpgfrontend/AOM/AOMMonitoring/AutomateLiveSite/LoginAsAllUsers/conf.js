exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['login-live-all-users.spec.js'],
    allScriptsTimeout: 100000000,
    capabilities: {
        'browserName': 'chrome'
    }
}