﻿namespace SpeechMonitor
{
    class Program
    {
        static SpeechSynth _speech = new SpeechSynth();

        static void Main(string[] args)
        {

            Startup();
        }

        private static void Startup()
        {
            _speech.Say("AOM Monitoring Initializing");

            // Do Stuff
            // var lastLogin = GetLastLogin();

            _speech.Say("Trader2Atm has just logged into LIVE");

            _speech.Say("Application waiting for events");
        }
    }
}
