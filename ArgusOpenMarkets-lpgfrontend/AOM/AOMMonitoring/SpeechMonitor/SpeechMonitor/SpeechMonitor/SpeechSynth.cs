﻿using System.CodeDom;
using System.Speech.Synthesis;

namespace SpeechMonitor
{
    public class SpeechSynth
    {
        private SpeechSynthesizer synthesizer;
        public SpeechSynth()
        {
            synthesizer = new SpeechSynthesizer();
            synthesizer.Volume = 100;  // 0...100
            synthesizer.Rate = -2;     // -10...10
        }

        public void Say(string text)
        {
            // Synchronous
            synthesizer.Speak(text);
        }
    }
}
