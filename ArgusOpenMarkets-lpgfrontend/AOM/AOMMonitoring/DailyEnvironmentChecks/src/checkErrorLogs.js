var elasticsearch = require('elasticsearch');
var queries = require('./errorLogQueries.json');
var _ = require('lodash');

// ***** Config *****
var clientConfig = {
	host: "http://10.11.0.17:9200"
};

var maxNumberOfMessagesToRetrieve = 100;

var toQuery = [
	{
		key: "processors",
		name: "AOM-Processors"
	},
	{
		key:"controlClient",
		name: "AOM ControlClient"
	},
	{
		key: "diffusion",
		name: "Diffusion"
	}
];
var toComplete = toQuery.length;
// ***** Config end *****

var client;
var logData = [];
var errors = [];
var result = {messages: [], appendix: []};


// returns message time in the local timezone, but that's fine for sorting as it's used here
// example message:
// 2015-07-24 09:00:00,369 [EasyNetQ consumer dispatch thread] ERROR AOM - "You are not a super user"

var toMessageString = function(queryHit) {
	var source = queryHit["_source"];
	return "{" + source.source_server + "} " + source.message;
};

var processErrorLogContents = function(data, callback) {
	try {
		var hits = data.hits.hits;
		hits = _.sortBy(hits, function(h) {
			// the @timestamp property isn't always the same as the time in the message,
			// so pull out the time string from the log message (first 23 characters)
			return h._source.message.slice(0, 23);
		});
		var messages = _.map(hits, toMessageString);
		callback(null, messages);
	} 
	catch (err) {
		callback(err);
	}
};

var isTimeoutError = function(err) {
    return err.indexOf("Request Timeout after") != -1;
};

var getLogErrors = function(query, callback, retries) {
    if (!retries) {
        retries = 3;
    }
	client.search({
		body: query,
		size: maxNumberOfMessagesToRetrieve
	}).then(
		function(resp) {
			processErrorLogContents(resp, callback)
		},
		function (err) {
            if (retries > 1 && isTimeoutError(err)) {
                getLogErrors(query, callback, --retries);
            } else {
                callback(err);
            }
		}
	);
};

var addMessagesToResult = function(d) {
	var nMessages = d.messages.length;
	result.messages.push(nMessages + " log Error messages for " + d.name);
	if (nMessages === maxNumberOfMessagesToRetrieve) {
		result.messages.push("WARNING: " + maxNumberOfMessagesToRetrieve + " is the maximum number of messages this script retrieves, please check the logs manually to see if there are extra error messages");
	}
	if (nMessages) {
		result.messages.push("See Appendix for log Error messages");
		result.appendix.push("Messages for " + d.name);
		result.appendix.push("--");
		result.appendix = result.appendix.concat(d.messages);
		result.appendix.push("--");
	}
	result.messages.push("");
};

var onLogErrorsReturned = function(name, err, queueMessages, callback) {
	toComplete = toComplete - 1;
	if (err) {
		errors.push("**ERROR**: error getting logs for " + name + ": " + err);
	} else {
		result.messages.push(name + " query complete");
		logData.push({"name":name, "messages":queueMessages});
	}
	if (toComplete == 0) {
		if (errors.length > 0) {
			callback(errors.join("\r\n"), null);
		} else {
			result.messages.push("All queries completed, results:");
			result.messages.push("");
			logData = _.sortBy(logData, function(d) { return d["name"]; });
			logData.forEach(function(d) { addMessagesToResult(d); });
			callback(null, result);
		}
	}
};

var isTodayMonday = function() {
	var today = new Date();
	return today.getDay() === 1;
};

var runChecks = function(callback) {
	try {
		client = new elasticsearch.Client(clientConfig);
	} catch (err) {
		callback("**ERROR**: error creating elasticsearch client: " + err, null);
		return;
	}
	var isMonday = isTodayMonday();
	toQuery.forEach(function(q) {
		var key = q["key"];
		if (isMonday) {
			key += 'Monday';
		}
		getLogErrors(queries[key], function(err, data) {
			onLogErrorsReturned(q["name"], err, data, callback);
		});
	});
};

module.exports = runChecks;