// As part of the AOM Daily Environment Checks this checks that:
// * the database is reachable
// * there are no unsent emails
// * outputs a list of failed logins to the screen

var mysql = require('mysql');
var _ = require('lodash');

var liveConnection, londonConnection;

var connectionData = {
	"london_ro_backup": {
		"connectionParams": {
			host: "10.11.0.56",
			user: "argusro",
			port: "3306",
			password: "3n7ctC9z"
		}
	},
	"live": {
		"connectionParams": {
			host: "aom-mysql.cs3w6qomvukg.eu-west-1.rds.amazonaws.com",
			user: "nathanro",
			port: "3306",
			password: "sD4g9m97"
		}
	},
};

var failedLoginsForLastDayQuery = "SELECT IpAddress, Username, count(*) as Count FROM `crm`.`AuthenticationHistory` WHERE `DateCreated` >= DATE_SUB(CURTIME(), INTERVAL 1 DAY) AND `SuccessfulLogin` =0 group by Username;";
var unsentEmailQuery = "SELECT * FROM `aom`.`Email` WHERE `Status` <> 'S';"

var createConnection = function(connectionName) {
	var connectionDetails = connectionData[connectionName];
	if (!connectionDetails) {
		throw new Error("Unable to find connection details for " + connectionName);
	}
	var connection = mysql.createConnection(connectionDetails.connectionParams);
	connection.connect();
	return connection;
};

var closeConnection = function(connection) {
	if (connection) {
		connection.end();
	}
}

var queryData = function(connection, queryString, callback) {
	connection.query(queryString, function(error, results, fields) {
		if (error) {
			callback(error, null);
		} else {
			callback(null, results);
		}
	});
}

var testLondonConnectionAndOutputData = function(callback) {
	var messages = [],
		error,
		leftToCheck = 2;
	var createCallback = function(description) {
		return function(err, data) {
			--leftToCheck;
			if (err && !error) {
				error = "**Error** getting " + description + " from London DB: " + err;
			} else {
				messages.push(description + ":");
				if (!data || data.length == 0) {
					messages.push("None");
				} else {
					data.forEach(function(d) {
						messages.push(JSON.stringify(d));
					});
				}
			}
			if (!leftToCheck) {
				if (error) {
					callback(err, null);
				} else {
					callback(null, messages);
				}
			}
		}
	}
	try {
		messages.push("Creating connection to London read-only backup");
		londonConnection = createConnection("london_ro_backup");
		messages.push("Connection created OK");
		queryData(londonConnection, unsentEmailQuery, createCallback("Unsent Emails"));
		queryData(londonConnection, failedLoginsForLastDayQuery, createCallback("Failed Logins"));
	}
	catch (err) {
		callback("**Error** testing London DB: " + err, null);
	}
	finally {
		closeConnection(londonConnection);
	}
};

var testLiveConnection = function(callback) {
	try {
		var results = [];
		results.push("Creating read-only connection to Live");
		liveConnection = createConnection("live");
		results.push("Connection created OK");
		callback(null, results);
	}
	catch (err) {
		callback("**Error** testing Live DB: " + err, null);
	}
	finally {
		closeConnection(liveConnection);
	}
};


var runChecks = function(callback) {
	var checksToRun = 2;
	var errors = [],
		result = {messages: []};
	var checksCallback = function(err, res) {
		--checksToRun
		if (err) {
			errors.push(err);
		} else {
			result.messages = result.messages.concat(res);
		}
		if (!checksToRun) {
			if (errors.length > 0) {
				callback(errors.join("\r\n"), null);
			} else {
				callback(null, result);
			}
		}
	};
	try {
		testLiveConnection(checksCallback);
		testLondonConnectionAndOutputData(checksCallback);
	}
	catch(err) {
		callback(err, null);
	}
}

module.exports = runChecks;