var request = require("request"),
    cheerio = require("cheerio"),
    url1 = "http://10.31.4.5:9980/",
    url2 = "http://10.31.5.5:9980/";

var getStatus = function($, url, serverName) {
    var result = {
        "isError": false
    };
    var data = $("a[name='diffusion_tcp/" + serverName + "']").closest('tr');
    var headingRow = data.prevAll('.titre').first();
    var headings = headingRow.find('th');

    var headerIndex;
    headings.each(function(i, elem) {
        if ($(this).text().toLowerCase() === "status") {
            headerIndex = i;
            return false;
        }
    });
    var statusIndex = headerIndex + 1; // Extra server name column has no header

    var statusText = data.children().eq(statusIndex).text();
    var outputText = "HAProxy " + url + " | " + serverName + ": " + statusText;
    if (statusText.indexOf("UP") !== -1) {
        result.message = "SUCCESS: Server is UP: " + outputText;
    } else if (statusText.indexOf("DOWN") !== -1) {
        result.message("** WARNING: Server DOWN: " + outputText);
    } else {
        result.message = "ERROR: Server Status not found in page!: " + outputText;
        result.isError = true;
    }
    return result;
}

var processRequest = function(url, callback) {
    var messages = [];
    request({
        uri: url
    }, function (error, response, body) {
        var result;
        if (error) {
            callback(error, null);
        } else {
            var $ = cheerio.load(body);
            result = getStatus($, url, "AWS-AOM-Diffusion1a");
            if (result.isErr) {
                error = result.message;
            } else {
                messages.push(result.message);
            }
            if (!error) {
                result = getStatus($, url, "AWS-AOM-Diffusion1b");
            }
            if (result.isErr) {
                error = result.message;
            } else {
                messages.push(result.message);
            }
            if (error) {
                callback(error, null);
            } else {
                callback(null, messages);
            }
        }
    });
}

var runChecks = function() {

    var processRequestCallback = function(err, res) {
        if (err) {
            console.log (err);
        } else {
            console.log(res.toString().replace(",","\r\n"));
        }
    };
    processRequest(url1, processRequestCallback);
    processRequest(url2, processRequestCallback);
}

runChecks();