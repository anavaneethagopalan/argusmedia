var http = require('http');
var _ = require('lodash');
var fs = require('fs');

var url = 'http://aomlive:nx097050A@10.31.7.9:15672/api/queues';

var isQueueEmpty = function(q) {
    return q.messages_ready === 0 && q.messages_unacknowledged === 0;
};

var isHealthcheckQueue = function(q) {
    return q.name.toLowerCase().indexOf('healthcheck') > -1;
};

// Since there are transient healthcheck messages added and removed from the queues
// all the time when the system is running we are only interested in healthcheck
// queues if they have 2 or more messages on the queue
var shouldCreateAlertForQueue = function(q) {
    if (isQueueEmpty(q)) {
        return false;
    }
    if (!isHealthcheckQueue(q)) {
        return true;
    }
    return q.messages_ready > 1 || q.messages_unacknowledged > 1;
}

var runChecks = function(callback) {
    http.get(url, function(res) {
        var body = '';

        res.on('data', function(chunk) {
            body += chunk;
        });

        res.on('error', function(err) { callback(err, null); });

        res.on('end', function() {
            var result = {messages: []};
            var response = JSON.parse(body);

            result.messages.push("Received data for " + response.length + " queues");

            var toAlertQueues = _.filter(response, shouldCreateAlertForQueue);

            if (toAlertQueues.length === 0) {
                result.messages.push("All queues are empty");
            } else {
                result.messages.push("The following queues are not empty:")
				fs.appendFile('output.txt', "The following queues are not empty:\r\n");
                toAlertQueues.forEach(function(q) {
                    var messageObj = {
                        "name": q.name,
                        "ready": q.messages_ready,
                        "unacked": q.messages_unacknowledged
                    };
					
					fs.appendFile('output.txt', JSON.stringify(messageObj) + "\r\n");
                    result.messages.push(JSON.stringify(messageObj));
                });
            }
            callback(null, result);
        });
    }).on('error', function(e) {
        callback(e, null);
    });
}

module.exports = runChecks;
