var diffusion = require('diffusion');
var _ = require('lodash');
var moment = require('moment');

var externalCallback;
//var client;

var expectedControlClientCount = 4;

var result = {messages: []};
var subscribed = [];
var validation = [];

var isControlClient = function(c) {
    return c.toUpperCase().indexOf("DOTNET_CLIENT") != -1
};

var topics = [];
topics.push({
    "topic": "Diffusion/MBeans/com/pushtechnology/diffusion/name=Server/Attributes/LicenseExpiryDate",
     "friendly": "License Expiry Date",
     "validation": function(value) {
        var expiryTime = moment(value, "ddd MMM DD HH:mm:ss [UTC] YYYY");
        var untilExpiryWeeks = expiryTime.diff(moment(), 'weeks');
        if (untilExpiryWeeks < 0) {
            validation.push("** ERROR **: Licence HAS EXPIRED on " + expiryTime);
        } else if (untilExpiryWeeks < 4) {
            validation.push("WARNING Licence expiring soon, expires " + expiryTime.fromNow());
        } else {
            validation.push("Licence Expiry OK, expires " + expiryTime.fromNow());
        }
     }
});
topics.push({
    "topic": "Diffusion/Clients",
    "validation": function(value) {
        var clients = value;
        if (!Array.isArray(value)) {
            clients = [value];
        }
        var dotNetClientCount = _.reduce(clients, function(total, c) {
            return isControlClient(c) ? total+1 : total;
        }, 0);
        if (dotNetClientCount === expectedControlClientCount) {
            validation.push("Expected number of control clients connected (" + dotNetClientCount + ")");
        } else {
            validation.push(
                "** ERROR **: " + dotNetClientCount + " control clients connected however expected " + expectedControlClientCount);
            validation.push("Raw clients string: ");
            validation.push(value);
        }
    }
});
topics.push({"topic": "Diffusion/Metrics/server/topics/count", "friendly": "Topic Count"});
topics.push({"topic": "Diffusion/Metrics/server/clients/concurrent", "friendly": "Number of Concurrent Clients"});
topics.push({"topic": "Diffusion/Metrics/server/clients/concurrent_max", "friendly": "Maximum Concurrent Clients"});

var closeDiffusionConnection = function() {
    // They should really provide a function for this, but they don't. Hence the _socket.end() call
    result.messages.push("Closing connection to Diffusion");
    diffusion._socket.end();
}

var callbacks = {
    onConnect: function (connected) {
        result.messages.push('Client ' + this.getClientId() + ' connected: ' + connected);
		result.messages.push('Here');
        topics.forEach(function (topic) {
            try {
                this.subscribe(topic.topic);
                this.fetch(topic.topic);
                subscribed.push(topic.topic);
            } catch (err) {
                console.error("Error in onConnect callback: " + err);
            }
        }, this);
    },

    onMessage: function (message) {
        var topicData = _.find(topics, 'topic', message._topic);
        if (!topicData) {
            externalCallback("Could not find topicData for received message: " + JSON.stringify(message));
            return;
        }
        if (topicData.friendly) {
            result.messages.push(topicData.friendly + ': ' + message._body);
        }
        diffusion.unsubscribe(message._topic);
        subscribed = _.without(subscribed, message._topic);
        if (topicData.validation) {
            topicData.validation(message._body);
        }
        if (subscribed.length === 0) {
            closeDiffusionConnection();
            validation.forEach(function(m) { result.messages.push(m); });
            externalCallback(null, result);
        }
    }
};

var runChecks = function(callback) {
    if (topics.length === 0) {
        callback('No topic specified', null);
    } else {
        externalCallback = callback;
        //client = diffusion.createClient(callbacks);
        diffusion.connect({host: '10.31.4.6', port: 8080, secure : false, principal: 'admin', credentials: 'password'});
    }
}

module.exports = runChecks;
