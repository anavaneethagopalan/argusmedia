var diffusion = require('diffusion');
var _ = require('lodash');
var moment = require('moment');
var fs = require('fs');

var externalCallback;
//var client;

var result = {messages: []};
var subscribed = [];
var validation = [];

var isControlClient = function(c) {
    return c.toUpperCase().indexOf("DOTNET_CLIENT") != -1
};


var topics = [];
topics.push({"topic": "Diffusion/Metrics/server/clients/concurrent", "friendly": "[[NumberConnections]]", "result": {} });
topics.push({"topic": "Diffusion/Metrics/server/clients/concurrent_max", "friendly": "[[MaxConcurrentConnections]]", "result": {} });

var closeDiffusionConnection = function() {
    // They should really provide a function for this, but they don't. Hence the _socket.end() call
    result.messages.push("Closing connection to Diffusion");
    diffusion._socket.end();
}

var callbacks = {
    onConnect: function (connected) {
		console.log("We are connected to Diffusion");
        result.messages.push('Client ' + this.getClientId() + ' connected: ' + connected);
        topics.forEach(function (topic) {
            try {
                this.subscribe(topic.topic);
                this.fetch(topic.topic);
                subscribed.push(topic.topic);
            } catch (err) {
                console.error("Error in onConnect callback: " + err);
            }
        }, this);
    },

    onMessage: function (message) {
		var messageTopic = "";
		var messageBody = "";
		var topicDataFriendly;
		var counter;
		var topicData;
		
		console.log("OnMessage received: " + JSON.stringify(message));
		
		if(message){
			
			try{
				messageTopic = message._topic;
				messageBody = message._body;
			}
			catch(e){
				console.log("Error parsing object returned from diffusion-" + e);
			}

			for(counter = 0; counter < topics.length; counter++){
				if(topics[counter].topic === messageTopic){
					topicData = topics[counter];
					break;
				}
			}
			
			if (!topicData) {
				externalCallback("Could not find topicData for received message: " + JSON.stringify(message));
				return;
			}
			
			if (topicData.friendly) {
				result.messages.push(topicData.friendly + ': ' + messageBody);
			}
			
			fs.appendFile('output.txt', messageTopic + ":" + messageBody + "\r\n");
			
			diffusion.unsubscribe(messageTopic);
			subscribed = _.without(subscribed, messageTopic);
			if (topicData.validation) {
				topicData.validation(messageBody);
			}
			if (subscribed.length === 0) {
				closeDiffusionConnection();
				validation.forEach(function(m) { result.messages.push(m); });
				externalCallback(null, result);
			}
		}
    }
};

var runChecks = function(callback) {
    if (topics.length === 0) {
        callback('No topic specified', null);
    } else {
		fs.writeFile('output.txt', '');
		
		console.log("Topics " + topics.length + " found")
        externalCallback = callback;
        //client = diffusion.createClient(callbacks);
	
		try
		{
			diffusion.connect({host: '10.31.4.6', port: 8080, principal: 'admin', credentials: 'password'});
		}
		catch(e){
			console.log("Error-" + e);
		}
		console.log("Connected to Diffusion");
    }
}

module.exports = runChecks;
