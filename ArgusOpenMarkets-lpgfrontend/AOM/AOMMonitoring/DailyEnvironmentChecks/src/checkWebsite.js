// Checks the website health page and that the main website page is up

var https = require('https');
var bl = require('bl');

var healthCheckUrl = "https://aom.argusmedia.com/app/api/healthcheck";
var mainWebsiteUrl = "https://aom.argusmedia.com/";

var checksToComplete = 2;

var result = {messages: []};
var errors = [];

var getDataFromUrl = function(url, dataCallback, externalCallback) {
	https.get(url, function(response) {
		response.pipe(bl(function(err, data) {
			if (err) {
				dataCallback(err, null, externalCallback);
			} else {
				dataCallback(null, data.toString(), externalCallback);
			}
		}));
    }).on('error', function(err) {
    	dataCallback(err, null, externalCallback);
    });
}

var dataReturned = function(externalCallback) {
	if (!(--checksToComplete)) {
		if (errors.length > 0) {
			externalCallback(errors.join("\r\n"), null);
		} else {
			externalCallback(null, result);
		}
	}
}

var onHealthCheckData = function(err, data, externalCallback) {
	if (err) {
		errors.push("Healthcheck page error: " + JSON.stringify(err));
	} else if (data === "200") {
		result.messages.push("Health check OK (returned 200)");	
	} else {
		errors.push("Health check error: returned " + data);
	}
	dataReturned(externalCallback);
};

var onMainPageData = function(err, data, externalCallback) {
	if (err) {
		errors.push("Main website page error: " + JSON.stringify(err));
	} else {
		result.messages.push("Main website URL OK");
	}
	dataReturned(externalCallback);
}


var runChecks = function(callback) {
	getDataFromUrl(healthCheckUrl, onHealthCheckData, callback);	
	getDataFromUrl(mainWebsiteUrl, onMainPageData, callback);
};

module.exports = runChecks;
