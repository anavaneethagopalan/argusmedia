var request = require("request"),
    cheerio = require("cheerio"),
    url1 = "http://10.31.4.15:9980/",
    url2 = "http://10.31.5.15:9980/";

var getStatus = function($, url, serverName) {
    var result = {
        "isError": false
    };
    var data = $("a[name='Diffusion/" + serverName + "']").closest('tr');
    var headingRow = data.prevAll('.titre').first();
    var headings = headingRow.find('th');
    
    var headerIndex;
    headings.each(function(i, elem) {
        if ($(this).text().toLowerCase() === "status") {
            headerIndex = i;
            return false;
        }
    });
    var statusIndex = headerIndex + 1; // Extra server name column has no header

    var statusText = data.children().eq(statusIndex).text();
    var outputText = "HAProxy " + url + " | " + serverName + ": " + statusText;
    if (statusText.indexOf("UP") !== -1) {
        result.message = "OK Server UP: " + outputText;
    } else if (statusText.indexOf("DOWN") !== -1) {
        result.message("** WARNING: Server DOWN: " + outputText);
    } else {
        result.message = "** ERROR: Server Status not found in page!: " + outputText;
        result.isError = true;
    }
    return result;
}

var processRequest = function(url, callback) {
    var messages = [];
    request({
        uri: url
    }, function (error, response, body) {
        var result;
        if (error) {
            callback(error, null);
        } else {
            var $ = cheerio.load(body);
            result = getStatus($, url, "DiffusionA");
            if (result.isErr) {
                error = result.message;
            } else {
                messages.push(result.message);
            }
            if (!error) {
                result = getStatus($, url, "DiffusionB");
            }
            if (result.isErr) {
                error = result.message;
            } else {
                messages.push(result.message);
            }
            if (error) {
                callback(error, null);
            } else {
                callback(null, messages);
            }
        }
    });
}

var runChecks = function(callback) {
    var errors = [];
    var result = { messages: [] };
    var remainingResults = 2;
    var processRequestCallback = function(err, res) {
        --remainingResults;
        if (err) {
            errors.push(err);
        } else {
            result.messages = result.messages.concat(res);
        }
        if (!remainingResults) {
            if (errors.length > 0) {
                callback(errors.join("\r\n"), null);
            } else {
                callback(null, result);
            }
        }
    };
    processRequest(url1, processRequestCallback);
    processRequest(url2, processRequestCallback);
}

module.exports = runChecks;