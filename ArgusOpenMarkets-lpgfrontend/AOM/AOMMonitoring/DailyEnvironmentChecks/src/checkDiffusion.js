var diffusion = require('DiffusionClient');
var _ = require('lodash');
var moment = require('moment');
var async = require('async');

var externalCallback;
var client;
var expectedControlClientCount = 4;
var result = {messages: []};
var subscribed = [];
var validation = [];

var isControlClient = function(c) {
    return c.toUpperCase().indexOf("DOTNET_CLIENT") != -1
};


var topics = [];

topics.push({
    "topic": "Diffusion/MBeans/com/pushtechnology/diffusion/name=Server/Attributes/LicenseExpiryDate",
    "friendly": "License Expiry Date",
    "validation": function(value) {
        var expiryTime = moment(value, "ddd MMM DD HH:mm:ss [UTC] YYYY");
        var untilExpiryWeeks = expiryTime.diff(moment(), 'weeks');

        if (untilExpiryWeeks < 0) {
            console.log("ERROR: Licence HAS EXPIRED on " + expiryTime);
        } else if (untilExpiryWeeks < 4) {
            console.log("WARNING Licence expiring soon, expires " + expiryTime.fromNow() + " on " + value);
        } else {
            console.log("SUCCESS: Licence expires " + expiryTime.fromNow() + " on " + value);
        }
     }
});

topics.push ({
    "topic": "Diffusion/Clients",
    "friendly": "Client Count",
    "validation": function(value) {
        var clients = value;
        if (!Array.isArray(value)) {
            clients = [value];
        }
        var dotNetClientCount = _.reduce(clients, function(total, c) {
            return isControlClient(c) ? total+1 : total;
        }, 0);

        if (dotNetClientCount === expectedControlClientCount) {
            console.log("SUCCESS: The expected number of control clients are still connected: "  + dotNetClientCount);
        } else {
            console.log("ERROR: " + dotNetClientCount + " control clients are connected, however expected " + expectedControlClientCount);
            console.log(value);
        }
    }
});

topics.push ({
    "topic": "Diffusion/Metrics/server/topics/count",
    "friendly": "Topic Count",
    "validation": function(value) {
        var topicCount = value;

        if (topicCount > 2000) {
            console.log("SUCCESS: topic count: " + topicCount);
        } else {
            console.log("ERROR: topic count is: " + topicCount);
        }
    }
});

 //topics.push({"topic": "Diffusion/Metrics/server/clients/concurrent", "friendly": "Number of Concurrent Clients"});
// topics.push({"topic": "Diffusion/Metrics/server/clients/concurrent_max", "friendly": "Maximum Concurrent Clients"});

var closeDiffusionConnection = function() {
    // They should really provide a function for this, but they don't. Hence the _socket.end() call
    result.messages.push("Closing connection to Diffusion");
    console.log("Closing connection to Diffusion");
    client._socket.end();
}

var callbacks;
callbacks = {
    onConnect: function (connected) {
        result.messages.push('Client ' + this.getClientId() + ' connected: ' + connected);
        console.log('Client ' + this.getClientId() + ' connected: ' + connected);

        topics.forEach(function (topic) {
            try {
                this.subscribe(topic.topic);
                this.fetch(topic.topic);
                subscribed.push(topic.topic);
                console.log("Client subscribed to topic: " + topic.topic);
            } catch (err) {
                console.error("ERROR in onConnect callback: " + err);
            }
        }, this);
    },

    onMessage: function (message) {
        var topicData = _.find(topics, {'topic': message._topic});
        if (!topicData) {
            externalCallback("Could not find topicData for received message: " + JSON.stringify(message));
            return;
        }
        //if (topicData.friendly) {
          //  result.messages.push(topicData.friendly + ': ' + message._body);
        //}

        client.unsubscribe(message._topic);
        subscribed = _.without(subscribed, message._topic);

        if (topicData.validation) {
            topicData.validation(message._body);
        }

        if (subscribed.length == 0) {
            closeDiffusionConnection();
        }
    }
};

var runChecks = function(callback) {
    client = diffusion.createClient(callbacks);
    client.connect({host: '10.31.4.16', port: 8080});
    //client.connect({host: 'localhost', port: 8080});
}

runChecks();
