var checkDiffusion = require('./liveDiffusionConnections');

var totalNumberOfChecks;
var appendixMessages = [];

var createUnderline = function(s) {
	return new Array(s.length + 1).join("=");
}

var outputWithUnderline = function(s) {
	console.log(s + "\n" + createUnderline(s));
}

var outputAppendixMessages = function() {
	if (appendixMessages.length > 0) {
		console.log("");
		console.log("");
		outputWithUnderline("Appendices");
		console.log("");
	}
	appendixMessages.forEach(function(am) {
		outputWithUnderline("Appendix for " + am.title);
		am.messages.forEach(function(m) { console.log(m); });
		console.log("");
	});
}

var createCallback = function(title) {
	return function(err, result) {
		--totalNumberOfChecks;
		if (!title) {
			title = "<untitled>";
		}
		outputWithUnderline(title);
		if (err) {
			console.error("** ERROR **");
			console.error(err);
		} else if (!result) {
			console.error("** ERROR **: check returned no result");
		} else {
			if (result.messages) {
				result.messages.forEach(function(message) { console.log(message); });
			}
			if (result.appendix) {
				appendixMessages.push({title: title, messages: result.appendix});
			}
		}
		console.log("--");
		console.log("Finished");
		console.log("");
		console.log("");
		if (totalNumberOfChecks == 0) {
			console.log("All checks finished");
			outputAppendixMessages();
		}
	}
}

// Each checking function should return either an error string in the err argument of the
// callback, or a object {messages:[], appendix: []} where both fields are optional
// messages will be written as soon as the check function completes, appendix messages
// are written out after all checks have finished.
var toCheck = [
	{"fn":checkDiffusion, "title":"Diffusion Checks"}
];
totalNumberOfChecks = toCheck.length;
toCheck.forEach(function(c) { c.fn(createCallback(c.title)); });
