var app = angular.module('AomMonitoring', ['ngMaterial'])

app.controller('IndexCtrl', ['$scope', '$mdSidenav', function ($scope, $mdSidenav) {
    $scope.toggleSidenav = function (menuId) {
        // $mdSidenav(menuId).toggle();
    };
}]);

app.controller('processorStatusCtrl', ['$scope', '$mdSidenav', '$http', function ($scope, $mdSidenav, $http) {

    $scope.lastCheck = new Date();
    $scope.processorSummary = [];
    $scope.processorStatus = [];
    $scope.processorStatusRolled = [];
    $scope.expectedNumberProcessors = 3;

    var makeProcessorStatusClass = function(processorName, machines) {

        var countGreen = 0,
            countAmber = 0,
            countRed = 0;

        angular.forEach(machines, function (machine) {

            switch (machine.processorStatus) {
                case 0:
                    countGreen = countGreen + 1;
                    break;
                case 1:
                    countAmber = countAmber + 1;
                    break;
                case 2:
                    countRed = countRed + 1;
                    break;
            }
        });

        if (countRed > 0) {
            return "circleRed";
        }

        if (countAmber > 0) {
            return "circleAmber";
        }

        if (processorName.toLowerCase().indexOf('processor') !== -1) {
            if (machines.length !== $scope.expectedNumberProcessors) {
                return "circleAmber";
            }
        }

        return "circleGreen";
    };

    var getMachineStatusClass = function(processorStatus){

        var machineClass = "";
        switch (processorStatus) {
            case 0:
                machineClass = "status-green";
                break;
            case 1:
                machineClass = "status-amber";
                break;
            case 2:
                machineClass = "status-red";
                break;
        }

        return machineClass;
    };

    var makeMachineStatus = function (processor) {

        return {
            "processorStatus": processor.processorStatus,
            "responseTime": processor.responseTime,
            "responseTimeAge": processor.responseTimeAge,
            "processorStatusText": processor.processorStatusText,
            "machineName": processor.machineName,
            "processorStatusClass": getMachineStatusClass(processor.processorStatus)
        }
    };

    var generateRolledUpProcessorStatus = function () {

        var processorMatched = false;
        var machineMatched = false;

        angular.forEach($scope.processorStatus, function (procStatus) {

            processorMatched = false;

            for (var i = 0; i < $scope.processorStatusRolled.length; i++) {
                if ($scope.processorStatusRolled[i].processor == procStatus.processor) {

                    processorMatched = true;
                    $scope.processorStatusRolled[i].processorStatusClass = makeProcessorStatusClass($scope.processorStatusRolled[i].processor, $scope.processorStatusRolled[i].machines);

                    machineMatched = false;
                    for (var machineCount = 0; machineCount < $scope.processorStatusRolled[i].machines.length; machineCount++) {
                        if ($scope.processorStatusRolled[i].machines[machineCount].machineName.toLowerCase() === procStatus.machineName.toLowerCase()) {
                            // We have found our machine.
                            machineMatched = true;
                            $scope.processorStatusRolled[i].machines[machineCount].processorStatus = procStatus.processorStatus;
                            $scope.processorStatusRolled[i].machines[machineCount].responseTime = procStatus.responseTime;
                            $scope.processorStatusRolled[i].machines[machineCount].responseTimeAge = procStatus.responseTimeAge;
                            $scope.processorStatusRolled[i].machines[machineCount].processorStatusText = procStatus.processorStatusText;
                            $scope.processorStatusRolled[i].machines[machineCount].processorStatusClass = getMachineStatusClass(procStatus.processorStatus);
                            break;
                        }
                    }

                    if (!machineMatched) {
                        $scope.processorStatusRolled[i].machines.push(makeMachineStatus(procStatus));
                    }
                }
            }

            if (!processorMatched) {
                // We need to add the processor into the list.
                var processorStatusRolled = {
                    "processorFullName": procStatus.processorFullName,
                    "processor": procStatus.processor
                };
                processorStatusRolled.machines = [];
                processorStatusRolled.machines.push(makeMachineStatus(procStatus));
                processorStatusRolled.processorStatusClass = makeProcessorStatusClass(processorStatusRolled.processor, processorStatusRolled.machines);

                $scope.processorStatusRolled.push(processorStatusRolled);
            }
        });
    }

    var generateProcessorSummary = function () {

        var processSummary = [],
            green = 0,
            amber = 0,
            red = 0,
            controlClientsActive = 0,
            controlClientsCss = "";

        angular.forEach($scope.processorStatus, function (processor) {

            if (processor.controlClientActive) {
                controlClientsActive = controlClientsActive + 1;
            }

            if (processor) {
                switch (processor.processorStatus) {
                    case 0:
                        green += 1;
                        break;
                    case 1:
                        amber += 1;
                        break;
                    case 2:
                        red += 1;
                        break;
                }
            }
        });

        if (controlClientsActive !== 1) {
            controlClientsCss = "circleRed"
        } else {
            controlClientsCss = "";
        }

        processSummary.push({
            "procStatus": "Green (ok)",
            count: green,
            cssClass: "circleGreen",
            "id": "green",
            headerText: ""
        });
        processSummary.push({
            "procStatus": "Amber (potential warning)",
            count: amber,
            cssClass: "circleAmber",
            "id": "amber",
            headerText: ""
        });
        processSummary.push({
            "procStatus": "Red (investigate)",
            count: red,
            cssClass: "circleRed",
            "id": "red",
            headerText: ""
        });
        processSummary.push({
            "procStatus": "Control Clients Active",
            count: controlClientsActive,
            "id": "control",
            "cssClass": controlClientsCss,
            headerText: ""
        });

        if ($scope.processorSummary.length === 0) {
            $scope.processorSummary.push(processSummary[0]);
            $scope.processorSummary.push(processSummary[1]);
            $scope.processorSummary.push(processSummary[2]);
            $scope.processorSummary.push(processSummary[3]);
        } else {
            angular.forEach(processSummary, function (procSum) {

                for (var i = 0; i < $scope.processorSummary.length; i++) {
                    if (procSum.id === $scope.processorSummary[i].id) {
                        $scope.processorSummary[i].count = procSum.count;
                        $scope.processorSummary[i].cssClass = procSum.cssClass;
                        $scope.processorSummary[i].headerText = "Running";
                    }
                }
            });
        }
    };

    generateProcessorSummary();

    var setProcessorClass = function (processor) {

        if (processor) {
            switch (processor.processorStatus) {
                case 0:
                    processor.processorStatusClass = "circleGreen";
                    processor.processorStatusText = "";
                    break;
                case 1:
                    processor.processorStatusClass = "circleAmber";
                    processor.processorStatusText = "status-amber";
                    break;
                case 2:
                    processor.processorStatusClass = "circleRed";
                    processor.processorStatusText = "status-red";
                    break;
            }
        }

        var processorName = processor.processor;
        var names = processorName.split('.');
        processor.processor = names[names.length - 1];

        return processor;
    };

    setInterval(function () {

        var endPoint = "http://localhost:57166/api/processorstatus";
        var endPoint = "https://aomuat.argusmedia.com/app/api/processorstatus";

        $scope.lastCheck = new Date();

        $http.get(endPoint).success(function (data, status) {

            var itemFound;
            angular.forEach(data.listProcessorHealthChecks, function (processor) {

                if (processor) {
                    itemFound = false;
                    for (var i = 0; i < $scope.processorStatus.length; i++) {
                        if ($scope.processorStatus[i]) {
                            if ($scope.processorStatus[i].processorFullName === processor.processorFullName && $scope.processorStatus[i].machineName === processor.machineName) {

                                itemFound = true;
                                $scope.processorStatus[i].responseTimeAge = processor.responseTimeAge;
                                $scope.processorStatus[i].processorStatus = processor.processorStatus;

                                setProcessorClass($scope.processorStatus[i]);
                            }
                        }
                    }

                    if (!itemFound) {

                        $scope.processorStatus.push(setProcessorClass(processor));
                    }
                }
            });

            generateRolledUpProcessorStatus();
            generateProcessorSummary();
        }).error(function () {
            // The call to the web service has errored.
            $scope.processorStatus = [];

            angular.forEach($scope.processorSummary, function (procSum) {
                procSum.count = -1;
                procSum.headerText = "Web service error";
            });
        });

    }, 5000);
}]);