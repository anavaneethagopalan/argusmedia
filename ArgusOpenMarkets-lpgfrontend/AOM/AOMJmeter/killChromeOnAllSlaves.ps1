$sessions = New-PSSession -ComputerName 10.22.1.117 -Credential QAAdmin\ArgusM3d1a
$batfilename = 'c:\ChromeDriverKillTasks.bat'

Invoke-Command -Session $sessions -ScriptBlock {
  param($batfilename)
  & cmd.exe /c '$batfilename'
} -ArgumentList $batfilename -AsJob