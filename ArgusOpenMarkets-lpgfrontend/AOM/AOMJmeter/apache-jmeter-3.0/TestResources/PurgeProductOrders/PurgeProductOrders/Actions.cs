﻿using AOMIntegrationTests.Helpers;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PurgeProductOrders
{
    public class Actions
    {
        public int GetProductId(string sqlquery, string productname)
        {
            // var sqlquery = "select * from products where id = 5";
            // var productId = new List<long>();
            int productId = 0;

            EnvironmentSetting helper = new EnvironmentSetting();
            var con = helper.GetSQLConnection();

            con.Open();
            MySqlCommand command = new MySqlCommand(sqlquery, con);
            command.Parameters.Add("@name", MySqlDbType.VarChar).Value = productname;
            ;
            MySqlDataReader myReader = command.ExecuteReader();

            if (myReader.HasRows)
            {
                while (myReader.Read())
                {
                    // querydesc.Add(myReader["Id"].ToString());
                    //productId.Add(Convert.ToInt32(myReader["Id"]));
                    productId = Convert.ToInt32(myReader["Id"]);

                }
            }
            Console.WriteLine("Product Id is " + productId + " for" + productname);

            return productId;
        }
    }
}
