﻿using System;
using Argus.Transport.Configuration;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace AOMIntegrationTests.Helpers
{
    public class EnvironmentSetting
    {

    
        public MySqlConnection GetSQLConnection()
        {
            MySqlConnection aomsqlConnection = null;

            var environmentToUse = ConfigurationManager.AppSettings["Environment"];

            switch (environmentToUse.ToUpper())
            {
                case "LOCAL":
                    Console.WriteLine("Local Connection");
                    break;
                case "UAT":
                    aomsqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["aomuat"].ToString());
                    break;
                default:
                    Console.WriteLine("Sql Aom Connection not been set");
                    break;
            }


            return aomsqlConnection;
        }
    }
}