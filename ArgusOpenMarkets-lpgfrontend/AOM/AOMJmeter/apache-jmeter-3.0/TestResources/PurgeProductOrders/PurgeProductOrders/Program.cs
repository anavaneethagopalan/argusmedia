﻿using AOMIntegrationTests.AomBus;
using System.Collections.Generic;

namespace PurgeProductOrders
{
  public class Purge
    {
        static void Main(string[] args)
        {

       /* string query = "select * from Product where name = @name";
        string productName = "RED FAME 0 FOB ARA RANGE";
        Actions act = new Actions();
         var productId = act.GetProductId(query, productName);
         Console.WriteLine("Product ID is " + productId + " for " +productName);
            if (productId == 0)
                Console.WriteLine("Product does not Exist"); */

            var busPurgeOrders = new BusPurgeOrders();
            //List<long> productId = new List<long> { 1, 2, 3, 4, 5, 6, 7 };
            var productIds = new List<long> { 1, 2, 3, 4, 5, 6, 7 };

            busPurgeOrders.PublishPurgeAomProductOrders(productIds);
        }
    }
}
