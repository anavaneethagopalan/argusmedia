﻿using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.Processor.Common.ArgusBus;
using Argus.Transport.Infrastructure;
using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Configuration;
using ConnectionConfiguration = Argus.Transport.Configuration.ConnectionConfiguration;
using IBus = Argus.Transport.Infrastructure.IBus;

namespace AOMIntegrationTests.AomBus
{

    public class BusPurgeOrders
    {
        protected readonly IBus _aomBus;

        public static ConnectionConfiguration GetRabbitBusConnection()
        {
            ConnectionConfiguration aomConnection = null;
            //string aomConnection = null;

            var environmentToUse = ConfigurationManager.AppSettings["Environment"];

            switch (environmentToUse.ToUpper())
            {
                case "LOCAL":
                    //aomConnection = ConfigurationManager.AppSettings["aom.transport.bus.local"];
                    aomConnection = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus.local");
                    break;
                case "UAT":
                    //aomConnection = ConfigurationManager.AppSettings["aom.transport.bus"];
                    aomConnection = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus.uat");
                    break;
                default:
                    Console.WriteLine("Rabbit Bus Connection not been set");
                    break;
            }


            return aomConnection;
        }


        public BusPurgeOrders()
        {
            var aomConnection = GetRabbitBusConnection();
            _aomBus = Argus.Transport.Transport.CreateBus(aomConnection, null, new BusLogger(), new AdvancedBusEventHandlers());
        }

      
        public long ProcessorUserId
        {
            get
            {
                return ProcessorUserId;
            }
        }

        public void PublishPurgeAomProductOrders(List<long> productIds)
        {

            foreach (var product in productIds)
            {
                var request = new AomProductPurgeOrdersRequest
                {
                    ClientSessionInfo =
                        new ClientSessionInfo
                        {
                            UserId = -1
                        },
                    MessageAction = MessageAction.Purge,
                    Product = new Product { ProductId = product }
                };


                _aomBus.Publish(request);
            }

            _aomBus.Dispose();
        }



    }
}

