﻿namespace PerformanceTestGraph.ViewModel
{
    public class PageNameGroupByVm
    {
        public string AssemblyVersion { get; set; }
        public double?[] Pages { get; set; }
    }
}