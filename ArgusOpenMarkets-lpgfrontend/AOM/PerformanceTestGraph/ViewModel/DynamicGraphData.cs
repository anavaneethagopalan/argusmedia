﻿using System.Collections.Generic;

namespace PerformanceTestGraph.ViewModel
{
    public class DynamicGraphData
    {
        public string[] DynamicColumnLabels { get; set; }
        public List<PageNameGroupByVm> ColumnValues { get; set; }
    }
}