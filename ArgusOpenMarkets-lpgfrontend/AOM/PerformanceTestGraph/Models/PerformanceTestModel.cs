﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace PerformanceTestGraph.Models
{
    public class PerformanceTestModel : DbContext
    {
        public PerformanceTestModel() : base("aomconnection") { }

        public virtual IDbSet<PerformanceTest> PerformanceTests { get; set; }

        public List<PerformanceTestGrouping> GetAveragePerformanceTestResultsforVersion()
        {
            return (from pt in this.PerformanceTests
                    group pt by new { pt.AssemblyVersion, pt.PageName } into grp
                    select new PerformanceTestGrouping()
                    {
                        AssemblyVersion = grp.FirstOrDefault().AssemblyVersion,
                        //DateCreated = grp.Max(x => x.DateCreated),
                        PageName = grp.FirstOrDefault().PageName,
                        ElapsedTime = grp.Select(x => x.ElapsedTime).Average()
                    })
                .ToList() // LINQ to Entities cannot translate int.parse(), so we put results in memory first
                .OrderBy(x => int.Parse(x.AssemblyVersion.Replace(".", ""))).ThenBy(x => x.PageName)
                .ToList();
        }

    }
}