﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PerformanceTestGraph.Models
{
    [Table("aom.PerfomanceTests")]
    public class PerformanceTest
    {
        [Column(Order = 0)]
        [Key]
        [StringLength(45)]
        public string AssemblyVersion { get; set; }

        [Column(Order = 1)]
        [Key]
        public DateTime DateCreated { get; set; }
        
        [StringLength(45)]
        public string PageName { get; set; }

        [Column]
        public decimal ElapsedTime { get; set; }
    }
}