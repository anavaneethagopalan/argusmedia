﻿using System;

namespace PerformanceTestGraph.Models
{
    public class PerformanceTestGrouping
    {
        public string AssemblyVersion { get; set; }
        
        //public DateTime DateCreated { get; set; }
        
        public string PageName { get; set; }
        
        public decimal ElapsedTime { get; set; }
    }
}