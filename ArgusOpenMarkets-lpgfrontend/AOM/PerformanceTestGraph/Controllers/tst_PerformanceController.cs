﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PerformanceTestGraph.Models;
using PerformanceTestGraph.ViewModel;

namespace GoogleCharts.Controllers
{
    public class tst_PerformanceController : Controller
    {
        // GET: tst_Performance
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            using (var db = new PerformanceTestModel())
            {
                var performanceTestResultGroupings = db.GetAveragePerformanceTestResultsforVersion();

                var result = new DynamicGraphData
                {
                    DynamicColumnLabels = performanceTestResultGroupings.Select(pr => pr.PageName).Distinct().OrderBy(pr => pr).ToArray(),
                    ColumnValues = new List<PageNameGroupByVm>()
                };

                foreach (var distinctAssembly in performanceTestResultGroupings.Select(pr => pr.AssemblyVersion).Distinct())
                {
                    var group = new PageNameGroupByVm()
                    {
                        AssemblyVersion = distinctAssembly,
                        Pages = new double?[result.DynamicColumnLabels.Length],
                    };

                    for (var i = 0; i < result.DynamicColumnLabels.Length; i++)
                    {
                        group.Pages[i] = performanceTestResultGroupings.Where(pr => pr.AssemblyVersion == distinctAssembly && pr.PageName == result.DynamicColumnLabels[i])
                            .Select(pr => (double?)Convert.ToDouble(pr.ElapsedTime))
                            .SingleOrDefault();
                    }
                    result.ColumnValues.Add(group);
                }
                return View(result);
            }
        }
    }
}