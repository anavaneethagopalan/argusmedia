#!/bin/sh

rmq_user_name=aomuser
rmq_password=password
rmq_vhost=aomlive

if [ -n "$1" ]
then
	rmq_vhost=$1
fi

sudo rabbitmqctl add_vhost $rmq_vhost
sudo rabbitmqctl add_user $rmq_user_name $rmq_password 
sudo rabbitmqctl set_user_tags $rmq_user_name administrator
sudo rabbitmqctl set_permissions -p $rmq_vhost $rmq_user_name ".*" ".*" ".*"

#only one policy ever applies to a queue - greatest priority policy applies
sudo rabbitmqctl set_policy -p $rmq_vhost easynetq "^EasyNetQ_Default_Error_Queue$" '{"max-length":5000, "ha-mode":"all", "ha-sync-mode":"automatic"}' --priority 20 --apply-to all
sudo rabbitmqctl set_policy -p $rmq_vhost aom  ".*" '{"max-length":500000,"ha-mode":"all","ha-sync-mode":"automatic", "expires":432000000}' --priority 10 --apply-to all



