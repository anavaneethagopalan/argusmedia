﻿using System.IO;
using AomTaskScheduler.Interfaces;

namespace AomTaskScheduler
{
    public class FileChecker : IFileChecker
    {
        public bool FileExists(string fileName)
        {
            return File.Exists(fileName);
        }
    }
}
