﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using AomTaskScheduler.Interfaces;
using PushTechnology.ClientInterface.Client.Factories;
using PushTechnology.ClientInterface.Client.Session;
using Utils.Logging.Utils;

namespace AomTaskScheduler.DiffusionLibrary
{
    public class DiffusionSessionConnection : IDiffusionSessionConnection
    {
        /// <summary>
        /// This can be used if the app is Running 
        /// </summary>
        public bool Running { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool StopRequested { get; set; }

        /// <summary>
        /// Gets the Diffusion ISession for this connection
        /// </summary>
        public ISession Session { get; private set; }


        public bool ConnectAndStartSession()
        {
            StopRequested = false;

            CreateSessionAndStart();

            //allow some time to connect before we give up
            if (Session == null || !Session.State.Connected)
            {
                if (!RetryConnect())
                {
                    Debug.WriteLine("Could not connect to diffusion server...");
                    Running = false;
                    Dispose();
                }
            }

            if (Session != null)
            {
                return Session.State.Connected;
            }

            return false;
        }

        public void Disconnect()
        {
            Debug.WriteLine("Disconnect called...");
            StopRequested = true;
            Dispose();
            Debug.WriteLine("Closing...");
        }

        private void CreateSessionAndStart()
        {
            Debug.WriteLine("Creating connection...");
            //sessionFactory.SessionStateChanged += SessionStateChanged;

            ISessionFactory sessionFactory =
                Diffusion.Sessions.Principal(ConfigurationManager.AppSettings["DiffusionUsername"])
                    .Password(ConfigurationManager.AppSettings["DiffusionPassword"]);

            // DIFFUSION 5.8
            // OLD CODE 5.1.5
//            sessionFactory.SessionStateChanged +=
//                (sender, eventArgs) =>
//                    Log.InfoFormat("Session state changed from '{0} to {1}. ", eventArgs.OldState, eventArgs.NewState);
//
//            sessionFactory.ErrorNotified +=
//                (sender, eventArgs) => Debug.WriteLine(string.Format("Error: '{0}'.", eventArgs.Error));

            try
            {
                Session = sessionFactory.Open(ConfigurationManager.AppSettings["DiffusionEndPoint"]);
                
               // Diffusion - 5.8 Deprecreate 
                // Session.Start();
                Debug.WriteLine("Session started...");
            }
            catch (Exception exception)
            {
                Log.Warn(string.Format("Could not connect to diffusion -> {0}", exception));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool RetryConnect()
        {
            int retry = 0;
            const int maxRetry = 5;

            while (Session == null || (!Session.State.Connected && retry < maxRetry && !StopRequested))
            {
                retry++;
                Thread.Sleep(100);

                //test to keep the client alive 
                CreateSessionAndStart();
            }

            return (Session == null || Session.State.Connected);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Session != null)
                {
                    try
                    {
                        Session.Close();
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine(exception);
                        throw;
                    }
                }

                Running = false;
            }
        }
    }
}