﻿using System.Threading;
using AOM.App.Domain.Entities;
using Newtonsoft.Json;
using PushTechnology.ClientInterface.Client.Features;
using Utils.Logging.Utils;

namespace AomTaskScheduler.DiffusionLibrary
{
    public class DiffusionJsonChecker : IFetchStream
    {
        public ManualResetEvent WaitHandle { get; private set; }
        public bool DatesCorrect { get; private set; }

        private readonly string _expectedStartDateOffset;
        private readonly string _expectedEndDateOffset;

        public DiffusionJsonChecker(string expectedStartOffset, string expectedEndOffset)
        {
            _expectedStartDateOffset = expectedStartOffset;
            _expectedEndDateOffset = expectedEndOffset;
            WaitHandle = new ManualResetEvent(false);
            DatesCorrect = false;
        }

        public void OnFetchReply(string topicPath, PushTechnology.ClientInterface.Client.Content.IContent content)
        {
            Log.Debug("FETCH REPLY: " + content.AsString());

            var productDefinition = JsonConvert.DeserializeObject<ProductDefinitionItem>(content.AsString());

            Log.Debug("Tenor Start!: " + productDefinition.ContractSpecification.Tenors[0].DeliveryStartDate);
            Log.Debug("Tenor End!: " + productDefinition.ContractSpecification.Tenors[0].DeliveryEndDate);


            DatesCorrect = (productDefinition.ContractSpecification.Tenors[0].DeliveryStartDate == _expectedStartDateOffset) &&
                           (productDefinition.ContractSpecification.Tenors[0].DeliveryEndDate == _expectedEndDateOffset);

            WaitHandle.Set();
        }

        public void OnClose()
        {
            Log.Warn("OnClose()!");
        }

        public void OnDiscard()
        {
            Log.Warn("OnDiscard()!");
        }
    }
}