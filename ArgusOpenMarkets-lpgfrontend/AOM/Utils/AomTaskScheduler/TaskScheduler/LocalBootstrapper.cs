﻿using AomRepository;
using AomRepository.Repositories;
using AomTaskScheduler.DiffusionLibrary;
using AomTaskScheduler.Interfaces;
using AomTaskScheduler.TaskHandlers;
using AOM.Services.EmailService;
using AOM.Services.ProductService;
using AOM.Transport.Service.Processor.Common.ArgusBus;
using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;
using Ninject;
using Ninject.Modules;

namespace AomTaskScheduler
{
    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<IAssessmentRepository>().To<AssessmentRepository>();
            kernel.Bind<IProductRepository>().To<ProductRepository>();

            kernel.Bind<ICheckMarketOpenCloseTaskHandler>().To<CheckMarketOpenCloseTaskHandler>();

            kernel.Bind<ITaskEmailer>().To<TaskEMailer>();
            kernel.Bind<ITaskScheduler>().To<TaskScheduler>();
            // Tasks
            kernel.Bind<ITaskHandler>().To<AssessmentDatesTaskHandler>(); 
            kernel.Bind<ITaskHandler>().To<IgnoreMarketStatusTriggersTaskHandler>();
            kernel.Bind<ITaskHandler>().To<TerminateUsersTaskHandler>();
            kernel.Bind<ITaskHandler>().To<AssessmentDatesTaskHandler>();

            kernel.Bind<ITaskLoader>().To<TaskLoader>().InSingletonScope();
            kernel.Bind<IBusLogger>().To<BusLogger>();
            kernel.Bind<IDiffusionSessionConnection>().To<DiffusionSessionConnection>().InSingletonScope();
            kernel.Bind<IFileChecker>().To<FileChecker>();
            kernel.Bind<IProductService>().To<ProductService>();
            kernel.Bind<ISmtpService>().To<SmtpService>();
            kernel.Bind<IClock>().To<Clock>();
            var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
            kernel.Bind<IBus>()
                .ToMethod(
                    x => Argus.Transport.Transport.CreateBus(aomConnectionConfiguration,
                        null,
                        x.Kernel.Get<IBusLogger>())).InThreadScope();
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}