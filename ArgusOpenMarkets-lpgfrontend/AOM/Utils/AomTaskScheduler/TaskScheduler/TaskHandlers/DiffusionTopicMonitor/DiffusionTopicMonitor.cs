﻿using System;
using System.Configuration;
using AomTaskScheduler.Interfaces;
using AOM.RollProductDeliveryPeriods;
using Utils.Logging.Utils;

namespace AomTaskScheduler.TaskHandlers.DiffusionTopicMonitor
{
    public class DiffusionTopicMonitor : IDiffusionTopicMonitor
    {
        private ITaskEmailer _taskEmailer;
        private IDiffusionSessionConnection _diffusionAConnection;
        private IDiffusionSessionConnection _diffusionBConnection;

        private IFetchCallback _fetchBCallback;
        private IFetchCallback _fetchACallback;

        public DiffusionTopicMonitor(ITaskEmailer taskEmailer)
        {
            _taskEmailer = taskEmailer;
            ConnectToLiveDiffusionServers();
        }

        private void ConnectToLiveDiffusionServers()
        {
            _diffusionAConnection = new DiffusionSessionConnection();
            _diffusionBConnection = new DiffusionSessionConnection();

            var diffusionA = ConfigurationManager.AppSettings["LiveDiffusionA"];
            var diffusionB = ConfigurationManager.AppSettings["LiveDiffusionB"];
            _diffusionAConnection.ConnectAndStartSession(diffusionA);
            _diffusionBConnection.ConnectAndStartSession(diffusionB);

            _fetchACallback = new FetchCallback();
            _fetchBCallback = new FetchCallback();
        }

        public void GetTopicCounts()
        {
            if (_fetchACallback.NumberTopics != _fetchBCallback.NumberTopics)
            {
                // The Topic count does NOT match.  We need to send an email.
                string body =
                    string.Format(
                        "Looking @ AOM/Orders.  Diffusion A Count: {0}.   Diffusion B Count{1}.   *** URGENT *** NOTE: This is an urgent issues which should be investigated",
                        _fetchACallback.NumberTopics, _fetchBCallback.NumberTopics);
                _taskEmailer.SendTaskEmail(
                    "LIVE Diffusion - Diffusion A Topic count Different to Diffusion B Topic Count", body);
            }
            else
            {
                Log.InfoFormat("Checking Diffusion Topics - Diffusion A Topic Count: {0}    Diffusion B Topic Count:{1}", _fetchACallback.NumberTopics, _fetchBCallback.NumberTopics);
            }

            if (DateTime.Now.Minute == 8  && DateTime.UtcNow.Second < 5)
            {
                // The Topic count does NOT match.  We need to send an email.
                string body = string.Format("Looking @ AOM/Orders.  Diffusion A Count: {0}.   Diffusion B Count{1}.",
                    _fetchACallback.NumberTopics, _fetchBCallback.NumberTopics);
                _taskEmailer.SendTaskEmail("LIVE Diffusion Topic Monitoring is Running", body);
            }

            _fetchACallback.NumberTopics = 0;
            _fetchBCallback.NumberTopics = 0;

            if (_diffusionAConnection != null)
            {
                var session = _diffusionAConnection.Session;
                var topics = session.GetTopicsFeature();

                topics.Fetch("?AOM/Orders//", _fetchACallback);
            }

            if (_diffusionBConnection != null)
            {
                var session = _diffusionBConnection.Session;
                var topics = session.GetTopicsFeature();

                topics.Fetch("?AOM/Orders//", _fetchBCallback);
            }
        }
    }
}
