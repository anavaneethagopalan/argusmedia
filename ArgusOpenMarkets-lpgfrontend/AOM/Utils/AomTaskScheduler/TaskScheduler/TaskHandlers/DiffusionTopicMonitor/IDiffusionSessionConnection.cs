﻿using System;
using PushTechnology.ClientInterface.Client.Session;

namespace AomTaskScheduler.TaskHandlers.DiffusionTopicMonitor
{
    public interface IDiffusionSessionConnection : IDisposable
    {
        bool ConnectAndStartSession(string diffusionEndPoint);

        void Disconnect();

        ISession Session { get; }
    }
}