﻿namespace AomTaskScheduler.TaskHandlers.DiffusionTopicMonitor
{
    public interface IDiffusionTopicMonitor
    {
        void GetTopicCounts();
    }
}