﻿namespace AomTaskScheduler.TaskHandlers.DiffusionTopicMonitor
{
    public class FetchCallback : IFetchCallback
    {
        public long NumberTopics { get; set; }

        public void OnFetchReply(string topicPath, PushTechnology.ClientInterface.Client.Content.IContent content)
        {
            NumberTopics++;

            var dataMessage = content.AsString();
            // Do we want to do anything with the data?
        }

        public void OnClose()
        {
        }

        public void OnDiscard()
        {
        }
    }
}
