﻿using PushTechnology.ClientInterface.Client.Features;

namespace AomTaskScheduler.TaskHandlers.DiffusionTopicMonitor
{
    public interface IFetchCallback : IFetchStream
    {
        long NumberTopics { get; set; }
    }
}