﻿

using System;
using System.Configuration;
using System.Diagnostics;
using AomTaskScheduler.TaskHandlers.DiffusionTopicMonitor;
using PushTechnology.ClientInterface.Client.Factories;
using PushTechnology.ClientInterface.Client.Session;
using Utils.Logging.Utils;

namespace AOM.RollProductDeliveryPeriods
{
    public class DiffusionSessionConnection : IDiffusionSessionConnection
    {
        /// <summary>
        /// This can be used if the app is Running 
        /// </summary>
        public bool Running { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool StopRequested { get; set; }

        /// <summary>
        /// Gets the Diffusion ISession for this connection
        /// </summary>
        public ISession Session { get; private set; }


        public bool ConnectAndStartSession(string diffusionEndPoint)
        {
            StopRequested = false;

            CreateSessionAndStart(diffusionEndPoint);

            //allow some time to connect before we give up
            if (Session == null || !Session.State.Connected)
            {
            }

            if (Session != null)
            {
                return Session.State.Connected;
            }

            return false;
        }

        public void Disconnect(string diffusionEndPoint)
        {
            Debug.WriteLine("Disconnect called...");
            StopRequested = true;
            Dispose();
            Debug.WriteLine("Closing...");
        }

        private void CreateSessionAndStart(string diffusionEndPoint)
        {
            Debug.WriteLine("Creating connection...");
            //sessionFactory.SessionStateChanged += SessionStateChanged;

            string diffUsername = ConfigurationManager.AppSettings["DiffusionUsername"];
            if (string.IsNullOrEmpty(diffUsername))
            {
                diffUsername = "AuthenticationAdmin";
            }

            var diffPassword = ConfigurationManager.AppSettings["DiffusionPassword"];
            if (string.IsNullOrEmpty(diffPassword))
            {
                diffPassword = "vjt7676FKZ1qaz2wsx";
            }

            ISessionFactory sessionFactory =
                Diffusion.Sessions.Principal(diffUsername)
                    .Password(diffPassword);

            try
            {
                Session = sessionFactory.Open(diffusionEndPoint);

                // Diffusion - 5.8 Deprecreate 
                // Session.Start();
                Debug.WriteLine("Session started...");
            }
            catch (Exception exception)
            {
                Log.Warn(string.Format("Could not connect to diffusion -> {0}", exception));
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Session != null)
                {
                    try
                    {
                        Session.Close();
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine(exception);
                        throw;
                    }
                }

                Running = false;
            }
        }


        public void Disconnect()
        {
            Debug.WriteLine("Disconnect called...");
            StopRequested = true;
            Dispose();
            Debug.WriteLine("Closing...");
        }
    }
}