﻿
using System;
using System.Text;
using AomRepository.Repositories;
using AomTaskScheduler.Interfaces;
using Utils.Logging.Utils;

namespace AomTaskScheduler.TaskHandlers
{
    public class CheckMarketOpenCloseTaskHandler : ICheckMarketOpenCloseTaskHandler
    {
        protected readonly IProductRepository _productRepository;
        protected readonly ITaskEmailer _taskEmailer;

        public CheckMarketOpenCloseTaskHandler(IProductRepository productRepository, ITaskEmailer taskEmailer)
        {
            _productRepository = productRepository;
            _taskEmailer = taskEmailer;
        }

        public void CheckMarketsOpenOrClosed()
        {
            Log.InfoFormat(
                "We are checking all products to see if the Product Status: O or C is actually correct.  If not an email will be sent");

            StringBuilder errorText = new StringBuilder();
            var dateTimeNow = DateTime.UtcNow;
            var products = _productRepository.GetProducts();
            foreach (var product in products)
            {
                if (!product.IgnoreAutomaticMarketStatusTriggers)
                {
                    if (!product.IsInternal)
                    {
                        // Ok - we need to check to see if the product status is correct.
                        var statusOk = CheckMarketStatus(product.Status, product.OpenTime, product.CloseTime, dateTimeNow);

                        if (!statusOk)
                        {
                            // Oh dear - we've got a product that should be open but is closed, or that is closed which should be open.
                            errorText.Append(
                                string.Format("Product: {0}  Currently has a Status of:{1}, but based upon the Open Time of:{2} and Close Time of {3} we believe this is incorrect{4}",
                                product.Name, product.Status, product.OpenTime, product.CloseTime, Environment.NewLine));
                        }
                    }
                }
            }

            if(!string.IsNullOrEmpty(errorText.ToString()))
            {
                _taskEmailer.SendTaskEmail("Product Market Status - Open/Closed Incorrect", errorText.ToString());
            }
        }

        private bool CheckMarketStatus(string actualProductStatus, TimeSpan openTime, TimeSpan closeTime, DateTime dateTimeNow)
        {
            var productStatus = "";
            var timePart = dateTimeNow.TimeOfDay;
            //    04:00       13:00         13:00      03:59
            bool timeBetween;
            if (openTime < closeTime)
            {
                timeBetween = (openTime <= timePart && timePart <= closeTime);
            }
            else
            {
                // start is after end, so do the inverse comparison
                timeBetween = !(closeTime < timePart && timePart < closeTime);
            }

            if (timeBetween) productStatus = "O";
            else productStatus = "C";

            return (productStatus == actualProductStatus);
        }
    }
}
