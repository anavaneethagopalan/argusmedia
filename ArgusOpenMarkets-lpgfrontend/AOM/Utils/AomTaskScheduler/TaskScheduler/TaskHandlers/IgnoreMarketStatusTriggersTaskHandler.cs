﻿using System;
using AomTaskScheduler.Interfaces;
using AomTaskScheduler.Tasks;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AomTaskScheduler.TaskHandlers
{
    public class IgnoreMarketStatusTriggersTaskHandler : ITaskHandler
    {
        private static IProductService _productService;


        public IgnoreMarketStatusTriggersTaskHandler(IProductService productService)
        {
            _productService = productService;
        }
        public void Run(ITask task)
        {
            try
            {
                Log.InfoFormat(
                    "We are running the task: IgnoreAutomaticMarketStatusTrigger T-Name:{0}  T-TimeToRun:{1}",
                    task.TaskName, task.TimeToRun);
                var ignoreMarketStatusTriggersTask = new IgnoreMarketStatustriggersTask(task);

                var product = _productService.GetProduct(ignoreMarketStatusTriggersTask.ProductId);
                if (product != null)
                {
                    Log.InfoFormat("We have found the product: {0}, with Id:{1}", product.Name, product.ProductId);
                    product.IgnoreAutomaticMarketStatusTriggers = ignoreMarketStatusTriggersTask.CheckedOrUnchecked;

                    //publish request
                    var request = new AomMarketStatusChangeRequest
                    {
                        ClientSessionInfo =
                            new ClientSessionInfo
                            {
                                UserId = -1,
                                OrganisationId = 1
                            },
                        MessageAction = MessageAction.Update,
                        Product = product
                    };

                    var bus = CreateBus();
                    bus.Publish(request);
                    Log.InfoFormat(
                        "We have just published the event - AomMarketStatusChangeRequest onto the bus.  We are setting the IgnoreAutomaticMarketStatusTriggers to: {0}",
                        ignoreMarketStatusTriggersTask.CheckedOrUnchecked);
                }
                else
                {
                    Log.InfoFormat("We could not find a product with the id: {0}",
                        ignoreMarketStatusTriggersTask.ProductId);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error Occurred - Running: IgnoreMarketStatusTriggersTaskHandler", ex);
            }
        }

        private IBus CreateBus()
        {
            var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
            var bus = Argus.Transport.Transport.CreateBus(aomConnectionConfiguration, null, null);
            return bus;
        }

        public string TaskName
        {
            get { return "IgnoreMarketStatusTriggersTask"; }
        }
    }
}
