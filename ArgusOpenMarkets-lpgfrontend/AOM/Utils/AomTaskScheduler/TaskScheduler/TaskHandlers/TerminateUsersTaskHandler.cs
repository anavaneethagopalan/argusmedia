﻿using AomTaskScheduler.Interfaces;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;

namespace AomTaskScheduler.TaskHandlers
{
    public class TerminateUsersTaskHandler : ITaskHandler
    {
        private static IBus _bus;

        protected readonly ITaskEmailer _taskEmailer;

        public TerminateUsersTaskHandler(IBus bus, ITaskEmailer taskEmailer)
        {
            _taskEmailer = taskEmailer;
            _bus = bus;
        }

        public void Run(ITask task)
        {
            // Ok - we need to terminate the users supplied via this task...
            var usersToTerminate = task.GetAdditionalParamAsString(0);

            if (string.IsNullOrEmpty(usersToTerminate))
            {
                // Nothing to do.

            }
            else
            {
                var users = usersToTerminate.Split('|');
                foreach (var user in users)
                {
                    // Ok - let's terminate this user.
                    var terminateUserRequest = new TerminateUserRequest {UserName = user};
                    _bus.Publish(terminateUserRequest);
                    // ProductMetaDataDtoExtensions.Log.InfoFormat("We have sent a Terminate User request, for the user:{0}", user);
                }
            }

            _taskEmailer.SendTaskEmail("TerminteUsers", string.Format("We have sent a Terminate User request, for the users:{0}", usersToTerminate));
        }

        public string TaskName
        {
            get { return "TerminateUsersTask"; }
        }
    }
}