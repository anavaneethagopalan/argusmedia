﻿using System;
using System.Collections.Generic;
using System.Text;
using AomRepository;
using AomRepository.Entity;
using AomRepository.Entity.Helpers;
using AomRepository.Repositories;
using AomTaskScheduler.Interfaces;
using AomTaskScheduler.Tasks;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AomTaskScheduler.TaskHandlers
{
    public class AssessmentDatesTaskHandler : ITaskHandler
    {
        protected readonly IAssessmentRepository _assessmentRepository;

        protected readonly ITaskEmailer _taskEmailer;

        public AssessmentDatesTaskHandler(IAssessmentRepository assessmentRepository, ITaskEmailer taskEmailer)
        {
            _assessmentRepository = assessmentRepository;
            _taskEmailer = taskEmailer;
        }

        public void Run(ITask task)
        {
            // TOOD: What magic do we need to implement for Assessment Date Tasks.
            DapperAssessment todaysAssessment = null;
            DapperAssessment previousAssessment = null;

            var assessmentDateTask = new AssessmentDateTask(task);
            var todaysBusinessDate = DateTime.UtcNow;
            var previousBusinessDate = todaysBusinessDate.AddDays(assessmentDateTask.NumberDaysToGoBack);

            List<DapperAssessment> productAssessments = new List<DapperAssessment>();
            try
            {
                productAssessments = _assessmentRepository.GetAssessmentsByProductId(assessmentDateTask.ProductId);
            }
            catch (Exception ex)
            {
                SendError("GetAssessmentsByProductId", ex);
            }
            foreach (var pa in productAssessments)
            {
                if (pa.BusinessDate == previousBusinessDate.Date && pa.AssessmentStatus != "n" && previousAssessment == null)
                {
                    previousAssessment = pa;
                }
                else if (pa.BusinessDate == todaysBusinessDate && pa.AssessmentStatus != "n" && todaysAssessment == null)
                {
                    // We have an assessment for today's date - lets just use that and leave in tact.
                    todaysAssessment = pa;
                }
                else
                {
                    // We don't care about this assessment - let's just delete it.
                    _assessmentRepository.Delete(pa);
                }
            }

            if (previousAssessment == null)
            {
                // NO Previous Assessment exists - lets create one.
                SendRollAssessmentNoPreviousValue(assessmentDateTask);
                CreateAssessment(previousBusinessDate, assessmentDateTask.ProductId, 0, 0);
            }

            if (todaysAssessment == null)
            {
                CreateAssessment(todaysBusinessDate, assessmentDateTask.ProductId, assessmentDateTask.TodayHigh, assessmentDateTask.TodayLow);
            }

            PublishRefreshAssessmentMessage(assessmentDateTask.ProductId, previousBusinessDate, todaysBusinessDate);
        }

        private void PublishRefreshAssessmentMessage(long productId, DateTime previousDate, DateTime todaysDate)
        {
            Assessment previousAssessment = new Assessment();
            Assessment todaysAssessment = new Assessment();

            var productAssessments = _assessmentRepository.GetAssessmentsByProductId(productId);
            foreach (var ass in productAssessments)
            {
                if (ass.BusinessDate == previousDate.Date)
                {
                    previousAssessment = ass.ToEntity();
                }

                if (ass.BusinessDate == todaysDate.Date)
                {
                    todaysAssessment = ass.ToEntity();
                }
            }
            var combinesAssessment = new CombinedAssessment {Previous = previousAssessment, ProductId = productId, Today = todaysAssessment};
            var bus = CreateBus();
            bus.Publish(new AomNewAssessmentResponse
            {
                Assessment = combinesAssessment,
                ProductId = productId,
                Message = new Message {MessageType = MessageType.Assessment}
            });

            SendPublishedNewAssessmentMessage(combinesAssessment);
        }

        private IBus CreateBus()
        {
            var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
            var bus = Argus.Transport.Transport.CreateBus(aomConnectionConfiguration, null, null);
            return bus;
        }

        private int CreateAssessment(DateTime businessDate, long productId, decimal previousPriceHigh, decimal previousPriceLow)
        {
            // We are missing the previous assessment - create in DB - so at least the editors can 
            // set the values.  
            var previousAssessment = new DapperAssessment
            {
                AssessmentStatus = "F",
                BusinessDate = businessDate.Date,
                DateCreated = businessDate,
                EnteredByUserId = -1,
                LastUpdated = DateTime.UtcNow,
                ProductId = productId,
                LastUpdatedUserId = -1,
                PriceHigh = previousPriceHigh,
                PriceLow = previousPriceLow
            };

            return _assessmentRepository.Add(previousAssessment);
        }

        private void SendPublishedNewAssessmentMessage(CombinedAssessment combinesAssessment)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("{0}\t", "Assessment Dates Task Handler"));
            sb.AppendLine(string.Format("{0}\t", "Date Executed: " + DateTime.UtcNow));
            sb.AppendLine(string.Format("{0}\t", "Successfully sent a new Assessment into diffusion"));
            sb.AppendLine(string.Format("{0}\t", "Todays Assessment Id" + combinesAssessment.Today.Id));
            sb.AppendLine(string.Format("{0}\t", "Previous Assessment Id" + combinesAssessment.Previous.Id));

            Log.Info(sb.ToString());
            _taskEmailer.SendTaskEmail("Assessment Date Task Handler - Successfully sent rebuild assessment", sb.ToString());
        }

        private void SendRollAssessmentNoPreviousValue(AssessmentDateTask assessmentDateTask)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("{0}\t", "Assessment Dates Task Handler"));
            sb.AppendLine(string.Format("{0}\t", "Date Executed: " + DateTime.UtcNow));
            sb.AppendLine(string.Format("{0}\t", "Product Id:" + assessmentDateTask.ProductId));
            sb.AppendLine(string.Format("{0}\t", "Number Days To Go Back: " + assessmentDateTask.NumberDaysToGoBack));
            sb.AppendLine(string.Format("{0}\t", "No Previous Assessment exists in the db.  We will have to set the price high/low to 0."));
            sb.AppendLine(string.Format("{0}\t", "A previous assessment will be created - with the above price high/low values."));

            Log.Info(sb.ToString());
            _taskEmailer.SendTaskEmail("Assessment Date Task Handler - Error No Previous Assessment", sb.ToString());
        }

        private void SendError(string methodCall, Exception ex)
        {
            _taskEmailer.SendTaskEmail("Assessment Date Task Handler - Unhandled Exception", "Method: " + methodCall + "- " + ex.ToString());
        }

        public string TaskName
        {
            get { return "AssessmentDatesTask"; }
        }
    }
}