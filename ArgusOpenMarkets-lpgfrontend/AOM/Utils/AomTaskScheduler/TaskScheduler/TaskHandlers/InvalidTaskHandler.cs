﻿using AomTaskScheduler.Interfaces;

namespace AomTaskScheduler.TaskHandlers
{
    public class InvalidTaskHandler : ITaskHandler
    {
        private readonly ITaskEmailer _taskEmailer;

        public InvalidTaskHandler(ITaskEmailer taskEmailer)
        {
            _taskEmailer = taskEmailer;
        }
        public void Run(ITask taskDetail)
        {
            var body = taskDetail.ToString();
            _taskEmailer.SendTaskEmail("Invalid Task Encountered", body);
        }

        public string TaskName
        {
            get { return "InvalidTask"; }
        }
    }
}
