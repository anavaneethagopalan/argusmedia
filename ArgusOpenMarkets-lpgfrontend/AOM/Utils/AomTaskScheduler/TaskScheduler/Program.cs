﻿using System;
using System.ServiceProcess;
using AomTaskScheduler.Interfaces;
using AOM.Repository.MySql;
using AOM.Services;
using Ninject;
using Topshelf;

namespace AomTaskScheduler
{
    internal class Program
    {
        private static ITaskScheduler _scheduledTaskService;

        public const string ServiceName = "AomTaskScheduler";

        public static StandardKernel _kernel { get; set; }

        public class TssService : ServiceBase
        {
            public TssService()
            {
                ServiceName = Program.ServiceName;
            }

            public void Start()
            {
                using (var kernel = new StandardKernel())
                {
                    kernel.Load(
                        new EFModelBootstrapper(),
                        new ServiceBootstrap(),
                        new LocalBootstrapper());

                    _kernel = kernel;

                    _scheduledTaskService = _kernel.Get<ITaskScheduler>();
                    _scheduledTaskService.Start();
                }
            }

            protected void Stop()
            {
                _scheduledTaskService.Stop();
            }
        }

        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<TssService>(s =>
                {
                    s.ConstructUsing(name => new TssService());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("AomTaskScheduler");
                x.SetDisplayName("AomTaskScheduler");
                x.SetServiceName("AomTaskScheduler");
            }); 
//            using (var kernel = new StandardKernel())
//            {
//                kernel.Load(
//                    new EFModelBootstrapper(),
//                    new ServiceBootstrap(),
//                    new LocalBootstrapper());
//
//                _kernel = kernel;
//
//                _scheduledTaskService = _kernel.Get<ITaskScheduler>();
//
//
//                if (!Environment.UserInteractive)
//                    // running as service
//                    using (var service = new TssService())
//                        ServiceBase.Run(service);
//                else
//                {
//                    // running as console app
//                    Start();
//
//                    Console.WriteLine("Press any key to stop...");
//                    Console.ReadKey(true);
//
//                    Stop();
//                }
//            }
        }

        private static void Start()
        {
            _scheduledTaskService.Start();
        }

        private static void Stop()
        {
            _scheduledTaskService.Stop();
        }
    }
}
