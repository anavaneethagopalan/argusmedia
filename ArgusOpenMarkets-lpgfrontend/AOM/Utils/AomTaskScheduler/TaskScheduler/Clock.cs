﻿using System;
using AomTaskScheduler.Interfaces;

namespace AomTaskScheduler
{
    public class Clock : IClock
    {
        public DateTime UtcNow()
        {
            return DateTime.UtcNow;
        }
    }
}
