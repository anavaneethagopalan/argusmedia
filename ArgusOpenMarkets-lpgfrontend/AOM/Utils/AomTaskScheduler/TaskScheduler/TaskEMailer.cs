﻿using System;
using AomTaskScheduler.Interfaces;
using AOM.App.Domain.Entities;
using AOM.Services.EmailService;
using Utils.Logging.Utils;

namespace AomTaskScheduler
{
    class TaskEMailer : ITaskEmailer
    {
        private static ISmtpService _smtpService;

        public TaskEMailer(ISmtpService smtpService)
        {
            _smtpService = smtpService;
        }

        public void SendTaskEmail(string subject, string body)
        {
            _smtpService.SendEmail(new Email
            {
                Body = body,
                DateCreated = DateTime.UtcNow,
                DateSent = DateTime.UtcNow,
                Id = -1,
                Recipient = "aomdevteam@argusmedia.com",
                Status = EmailStatus.Pending,
                Subject = subject
            });

            Log.InfoFormat(DateTime.UtcNow + ":  Sending email with:  Subject:{0}   Body:{1}", subject, body);
        }
    }
}
