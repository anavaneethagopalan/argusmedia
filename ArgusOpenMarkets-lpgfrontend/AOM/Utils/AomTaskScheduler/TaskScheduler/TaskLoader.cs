﻿using System;
using System.Collections.Generic;
using System.IO;
using AomTaskScheduler.Interfaces;

namespace AomTaskScheduler
{
    public class TaskLoader : ITaskLoader
    {
        public List<string> LoadTasks(string filePath)
        {
            List<string> tasks = new List<string>();

            using (StreamReader sr = new StreamReader(filePath))
            {
                string currentLine;
                // currentLine will be null when the StreamReader reaches the end of file
                while ((currentLine = sr.ReadLine()) != null)
                {
                    tasks.Add(currentLine);
                }
            }

            return tasks;
        }

        public void LoadTasks(CsvHelper.ICsvReader csvReader)
        {
            throw new NotImplementedException();
        }
    }
}