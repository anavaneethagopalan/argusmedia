﻿namespace AomTaskScheduler.Interfaces
{
    public interface ITaskEmailer
    {
        void SendTaskEmail(string subject, string body);
    }
}