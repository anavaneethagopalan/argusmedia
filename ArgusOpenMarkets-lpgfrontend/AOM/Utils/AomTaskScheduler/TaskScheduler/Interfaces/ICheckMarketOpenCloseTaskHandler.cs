﻿namespace AomTaskScheduler.Interfaces
{
    public interface ICheckMarketOpenCloseTaskHandler
    {
        void CheckMarketsOpenOrClosed();
    }
}
