﻿
namespace AomTaskScheduler.Interfaces
{
    public interface IFileChecker
    {
        bool FileExists(string fileName);
    }
}
