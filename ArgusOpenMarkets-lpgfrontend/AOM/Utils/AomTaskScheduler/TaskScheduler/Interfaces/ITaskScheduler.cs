﻿namespace AomTaskScheduler.Interfaces
{
    public interface ITaskScheduler
    {
        void Start();

        void Stop();
    }
}
