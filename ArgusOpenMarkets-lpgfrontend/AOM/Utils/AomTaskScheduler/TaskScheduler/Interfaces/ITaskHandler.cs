﻿namespace AomTaskScheduler.Interfaces
{
    public interface ITaskHandler
    {
        string TaskName { get; }
        void Run(ITask task);
    }
}