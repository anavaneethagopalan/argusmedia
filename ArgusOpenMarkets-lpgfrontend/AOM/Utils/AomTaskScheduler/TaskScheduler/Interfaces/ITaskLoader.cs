﻿using System.Collections.Generic;
using CsvHelper;

namespace AomTaskScheduler.Interfaces
{
    public interface ITaskLoader
    {
        List<string> LoadTasks(string filePath);
        void LoadTasks(ICsvReader csvReader);
    }
}