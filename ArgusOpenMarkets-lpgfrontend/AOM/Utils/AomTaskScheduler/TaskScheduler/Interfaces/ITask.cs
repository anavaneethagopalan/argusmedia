﻿using System;
using System.Collections.Generic;

namespace AomTaskScheduler.Interfaces
{
    public interface ITask
    {
        DayOfWeek DayOfWeekToRun { get; }
        string DayToRun { get; set; }
        int HourToRun { get; }
        int MinuteToRun { get; }
        string TimeToRun { get; set; }
        string TaskName { get; set; }
        List<string> AdditionalParameters { get; set; }
        long GetAdditionalParamAsLong(int additionalParamIndex);
        string GetAdditionalParamAsString(int additionalParamIndex);
        string ToString();
    }
}