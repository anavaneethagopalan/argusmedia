﻿using System;

namespace AomTaskScheduler.Interfaces
{
    public interface IClock
    {
        DateTime UtcNow();
    }
}
