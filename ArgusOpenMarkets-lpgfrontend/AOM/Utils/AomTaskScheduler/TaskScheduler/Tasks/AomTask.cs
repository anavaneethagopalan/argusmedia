﻿using System;
using System.Collections.Generic;
using AomTaskScheduler.Interfaces;
using AOM.App.Domain;

namespace AomTaskScheduler.Tasks
{
    public class AomTask : ITask
    {
        public AomTask()
        {
            
        }

        public AomTask(string taskLine)
        {
            // Sample line: Friday,10:05,IgnoreAutomaticMarketStatusTriggers,Checked
            var taskParts = taskLine.Split(',');

            if (taskParts.Length >= 3)
            {
                // We have the required information.
                DayToRun = taskParts[0];
                TimeToRun = taskParts[1];
                TaskName = taskParts[2];
            }

            AdditionalParameters = new List<string>();
            if (taskParts.Length > 3)
            {
                for (var i = 3; i < taskParts.Length; i++)
                {
                    AdditionalParameters.Add(taskParts[i]);
                }
            }
        }

        public string TimeToRun { get; set; }

        public string TaskName { get; set; }

        public List<string> AdditionalParameters { get; set; }

        public DayOfWeek DayOfWeekToRun
        {
            get
            {
                switch (DayToRun.ToUpper())
                {
                    case "M":
                    case "MON":
                    case "MONDAY":
                        return DayOfWeek.Monday;
                    case "TUE":
                    case "TUESDAY":
                        return DayOfWeek.Tuesday;
                    case "W":
                    case "WED":
                    case "WEDNESDAY":
                        return DayOfWeek.Wednesday;
                    case "THU":
                    case "THURSDAY":
                        return DayOfWeek.Thursday;
                    case "F":
                    case "FRI":
                    case "FRIDAY":
                        return DayOfWeek.Friday;
                    case "SAT":
                    case "SATURDAY":
                        return DayOfWeek.Saturday;
                    case "SUN":
                    case "SUNDAY":
                        return DayOfWeek.Sunday;
                }

                throw new BusinessRuleException("INVALID DAY OF WEEK SELECTED");
            }
        }

        public string DayToRun { get; set; }

        public int HourToRun
        {
            get
            {
                var timeParts = TimeToRun.Split(':');
                var hourPart = timeParts[0];
                Int32 hour;
                Int32.TryParse(hourPart, out hour);

                return hour;
            }
        }

        public int MinuteToRun
        {
            get
            {
                var timeParts = TimeToRun.Split(':');
                var timeToRun = timeParts[1];
                int time;
                Int32.TryParse(timeToRun, out time);
                return time;
            }
        }

        public long GetAdditionalParamAsLong(int additionalParamIndex)
        {
            string paramStringValue = string.Empty;
            long paramValue = -1;

            if (AdditionalParameters != null && AdditionalParameters.Count >= additionalParamIndex)
            {
                paramStringValue = AdditionalParameters[additionalParamIndex];
            }

            if (!String.IsNullOrEmpty(paramStringValue))
            {
                long.TryParse(paramStringValue, out paramValue);
            }

            return paramValue;
        }

        public string GetAdditionalParamAsString(int additionalParamIndex)
        {
            string paramStringValue = string.Empty;

            if (AdditionalParameters != null && AdditionalParameters.Count >= additionalParamIndex)
            {
                paramStringValue = AdditionalParameters[additionalParamIndex];
            }

            return paramStringValue;
        }

        public override string ToString()
        {
            return "TaskName:" + TaskName + "|DayToRun:" + DayOfWeekToRun + "|Hour:" + HourToRun + "|Minute:" +
                   MinuteToRun;
        }
    }
}