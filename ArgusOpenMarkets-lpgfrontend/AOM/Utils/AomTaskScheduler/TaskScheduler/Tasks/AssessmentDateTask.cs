﻿using AomTaskScheduler.Interfaces;

namespace AomTaskScheduler.Tasks
{
    public class AssessmentDateTask
    {
        private readonly long _productId;

        private readonly decimal? _todayHigh;

        private readonly decimal? _todayLow;

        private readonly long _numberDaysToGoBack;

        public ITask Task { get; set; }

        public AssessmentDateTask(ITask task)
        {
            Task = task;
            // Format of task definition in csv file: 
            // Tuesday,14:25,AssessmentDatesTask,1,null,null,-1,last_high,last_low
            _productId = Task.GetAdditionalParamAsLong(0);
            var todayHighStr = Task.GetAdditionalParamAsString(1);

            if (todayHighStr.ToLower() == "null")
            {
                _todayHigh = null;
            }
            else
            {
                decimal th;
                decimal.TryParse(todayHighStr, out th);
                _todayHigh = th;
            }

            var todayLowStr = Task.GetAdditionalParamAsString(2);
            if (todayLowStr.ToLower() == "null")
            {
                _todayLow = null;
            }
            else
            {
                decimal tl;
                decimal.TryParse(todayLowStr, out tl);
                _todayLow = tl;
            }

            _numberDaysToGoBack = Task.GetAdditionalParamAsLong(3);
        }

        public long ProductId
        {
            get { return _productId; }
        }

        public decimal TodayHigh
        {
            get
            {
                if (_todayHigh.HasValue)
                {
                    return _todayHigh.Value;
                }
                return 0;
            }
        }

        public decimal TodayLow
        {
            get
            {
                if (_todayLow.HasValue)
                {
                    return _todayLow.Value;
                }
                return 0;
            }
        }

        public long NumberDaysToGoBack
        {
            get { return _numberDaysToGoBack; }
        }
    }
}