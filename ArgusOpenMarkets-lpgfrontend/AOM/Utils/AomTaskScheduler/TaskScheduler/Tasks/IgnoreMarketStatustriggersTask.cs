﻿using AomTaskScheduler.Interfaces;

namespace AomTaskScheduler.Tasks
{
    public class IgnoreMarketStatustriggersTask
    {
        private readonly long _productId;
        private readonly bool _checkedOrUnchecked;

        public IgnoreMarketStatustriggersTask(ITask task)
        {
            var task1 = task;
            _productId = task1.GetAdditionalParamAsLong(0);
            string checkedOrUnchecked = task1.GetAdditionalParamAsString(1);
            _checkedOrUnchecked = checkedOrUnchecked.ToLower() == "checked";
        }

        public long ProductId
        {
            get { return _productId; }
        }

        public bool CheckedOrUnchecked
        {
            get { return _checkedOrUnchecked; }
        }
    }
}