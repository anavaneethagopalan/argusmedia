﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Timers;
using AomTaskScheduler.Interfaces;
using AomTaskScheduler.TaskHandlers;
using AomTaskScheduler.TaskHandlers.DiffusionTopicMonitor;
using AomTaskScheduler.Tasks;
using AOM.Transport.Events.Emails;
using Utils.Logging.Utils;
using Timer = System.Timers.Timer;

namespace AomTaskScheduler
{
    public class TaskScheduler : ITaskScheduler
    {
        public static ITaskLoader _taskLoader { get; set; }

        private static List<string> _taskDefinitions = new List<string>();

        private static ITaskHandler[] _tasks;

        private static List<AomTask> _taskDetails = new List<AomTask>();

        private static Timer _taskTimer;

        private const string TaskFilename = "tasks.csv";

        private static ITaskEmailer _taskEmailer;

        private IDiffusionTopicMonitor _diffusionTopicManager;

        protected readonly ICheckMarketOpenCloseTaskHandler _checkMarketOpenCloseTaskHandler;


        public TaskScheduler(ITaskLoader taskLoader, ITaskEmailer taskEmailer, ITaskHandler[] tasks, ICheckMarketOpenCloseTaskHandler checkMarketOpenCloseTaskHandler)
        {
            _checkMarketOpenCloseTaskHandler = checkMarketOpenCloseTaskHandler;
            _tasks = tasks;
            _taskLoader = taskLoader;
            _taskEmailer = taskEmailer;

            LoadTasks();
            LogStartupEmail(_taskDetails);

            _diffusionTopicManager = new DiffusionTopicMonitor(_taskEmailer);
        }

        public void Start()
        {
            var timeNow = DateTime.UtcNow;
            LoadTasks();
            _taskTimer = new Timer();
            _taskTimer.Elapsed += OnTimedEvent;
            _taskTimer.Interval = CalculateInterval();
            _taskTimer.Enabled = true;
            CheckForJobsToRun(timeNow);


            //var timeNow = DateTime.UtcNow;
            //CheckMarketOpenClosedTimes(timeNow);
        }

        private double CalculateInterval()
        {
            // Interval is set to 5 seconds.
            var secondsNow = DateTime.UtcNow.Second;
            var secondsNowRemainderFive = secondsNow%5;
            var nextTimeToRun = 5 - secondsNowRemainderFive;
            return nextTimeToRun * 1000;
        }

        public void Stop()
        {
            _taskTimer.Enabled = false;
        }

        private void LogStartupEmail(List<AomTask> tasks)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("{0}\t", "AOM Task Scheduler"));
            sb.AppendLine(string.Format("{0}\t", "Date Started: " + DateTime.UtcNow));

            foreach (var task in tasks)
            {
                sb.AppendLine(string.Format("{0}\t",
                    "Task Name: " + task.TaskName + "Task Day:" + task.DayToRun + " Task Time:" +
                    task.TimeToRun));
            }

            _taskEmailer.SendTaskEmail("AomTaskScheduler", sb.ToString());
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            var dateTimeUtcNow = DateTime.UtcNow;
            var currentSecond = dateTimeUtcNow.Second;
            
            // We need to run the diffusion checker.  NOTE: This will run approx every 5 seconds.
            _diffusionTopicManager.GetTopicCounts();

            if (currentSecond > 28 && currentSecond < 32)
            {
                CheckForJobsToRun(dateTimeUtcNow);
            }

            if (dateTimeUtcNow.Minute == 1 || dateTimeUtcNow.Minute == 16 || dateTimeUtcNow.Minute == 31 || dateTimeUtcNow.Minute == 46)
            {
                if (currentSecond > 28 && currentSecond < 32)
                {
                    CheckMarketOpenClosedTimes(dateTimeUtcNow);
                }
            }


            // Re-adjust the timer - so that we're always hitting around 30 seconds on the minute.
            _taskTimer.Interval = CalculateInterval();

        }

        private void CheckMarketOpenClosedTimes(DateTime dateTimeUtcNow)
        {
            bool taskShouldRun = true;
            DayOfWeek dayOfWeek = dateTimeUtcNow.DayOfWeek;
            if (dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
            {
                taskShouldRun = false;
            }

            if (taskShouldRun)
            {
                _checkMarketOpenCloseTaskHandler.CheckMarketsOpenOrClosed();
            }
        }

        private void CheckForJobsToRun(DateTime timeNow)
        {
            Log.Info("Checking to see if task should run?");

            foreach (var taskDetail in _taskDetails)
            {
                if (TaskShouldRunNow(taskDetail, timeNow))
                {
                    _taskTimer.Enabled = false;
                    EmailTaskShouldRun(taskDetail);
                    Log.Info(DateTime.UtcNow + ": Task:" + taskDetail.TaskName + " - should Execute Now: " + timeNow);

                    GetTaskHandler(taskDetail.TaskName).Run(taskDetail);

                    _taskTimer.Interval = CalculateInterval();
                    _taskTimer.Enabled = true;
                }
            }
        }

        private ITaskHandler GetTaskHandler(string taskHandlerName)
        {
            foreach (ITaskHandler th in _tasks)
            {
                if (th.TaskName == taskHandlerName)
                {
                    return th;
                }
            }

            // TODO: Return Task NOT Found handler.
            return new InvalidTaskHandler(_taskEmailer);
        }



        private static void EmailTaskShouldRun(AomTask taskDetail)
        {
            // We should be running this task now.
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("{0}\t", "AOM Task Scheduler"));
            sb.AppendLine(string.Format("{0}\t", "We've identified the following task should run now!"));
            sb.AppendLine(string.Format("{0}\t", "Task Name: " + taskDetail.TaskName));
            sb.AppendLine(string.Format("{0}\t", "Task Day: " + taskDetail.DayToRun));
            sb.AppendLine(string.Format("{0}\t", "Task Time: " + taskDetail.TimeToRun));

            try
            {
                sb.AppendLine(string.Format("{0}\t", "Product Id: " + taskDetail.GetAdditionalParamAsString(0)));
            }
            catch (Exception ex)
            {
                Log.Error("EmailTaskShouldRun", ex);
            }

            try
            {
                sb.AppendLine(string.Format("{0}\t",
                    "Checked or Unchecked Id: " + taskDetail.GetAdditionalParamAsString(1)));
            }
            catch (Exception ex)
            {
                Log.Error("EmailTaskShouldRun", ex);
            }

            sb.AppendLine(string.Format("{0}\t", "Time Now: " + DateTime.UtcNow));
            _taskEmailer.SendTaskEmail("AomTaskScheduler - Task Should Run Now", sb.ToString());
        }

        public bool TaskShouldRunNow(AomTask taskDetail, DateTime timeNow)
        {
            try
            {
                if (timeNow.DayOfWeek == taskDetail.DayOfWeekToRun)
                {
                    // We are on the right day...
                    if (timeNow.Hour == taskDetail.HourToRun)
                    {
                        // The hour has cometh - does the minutes match:
                        if (timeNow.Minute == taskDetail.MinuteToRun)
                        {
                            // BANG - this job needs to be executed.
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _taskEmailer.SendTaskEmail("Error", ex.ToString());
            }

            return false;
        }

        private void LoadTasks()
        {
            Log.Debug("------------------------------------------------------");
            Log.Debug("LOAD TASKS");
            Log.Debug("Loading Tasks: " + TaskFilename);

            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            try
            {
                _taskDefinitions = _taskLoader.LoadTasks(TaskFilename);
            }
            catch (Exception ex)
            {
                Log.Info("Error loading tasks", ex);
                _taskDetails = new List<AomTask>();
            }

            _taskDetails = new List<AomTask>();
            foreach (var taskLine in _taskDefinitions)
            {
                var taskDetail = new AomTask(taskLine);
                _taskDetails.Add(taskDetail);
            }

            Log.InfoFormat("Loaded {0} task details for processing", _taskDetails.Count);
        }
    }
}