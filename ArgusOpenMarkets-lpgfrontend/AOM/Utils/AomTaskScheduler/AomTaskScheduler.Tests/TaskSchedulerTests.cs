﻿using System;
using System.Collections.Generic;
using AomTaskScheduler.Interfaces;
using AomTaskScheduler.Tasks;
using Moq;
using NUnit.Framework;

namespace AomTaskScheduler.Tests
{
    [TestFixture]
    public class TaskSchedulerTests
    {
        private TaskScheduler _taskScheduler;

        private Mock<ITaskLoader> _mockTaskLoader;

        private Mock<ITaskEmailer> _mockTaskEmailer;

        private List<ITaskHandler> _listTasks;

        private Mock<ICheckMarketOpenCloseTaskHandler> _mockCheckMarketOpenClose;

        [SetUp]
        public void Setup()
        {
            _listTasks = new List<ITaskHandler>();
            _mockTaskLoader = new Mock<ITaskLoader>();
            _mockTaskEmailer = new Mock<ITaskEmailer>();
            _mockCheckMarketOpenClose = new Mock<ICheckMarketOpenCloseTaskHandler>();
        }

        [Test]
        public void InitialisingTheTaskHandlerShouldSendAnEmailThatTaskHandlerHasStarted()
        {
            var listTasks = new List<string>();
            listTasks.Add("Wednesday,14:50,AssessmentDatesTask,1");
            _mockTaskLoader.Setup(m => m.LoadTasks(It.IsAny<string>())).Returns(listTasks);
            _taskScheduler = new TaskScheduler(_mockTaskLoader.Object, 
                _mockTaskEmailer.Object, 
                _listTasks.ToArray(), 
                _mockCheckMarketOpenClose.Object);

            _mockTaskEmailer.Verify(m => m.SendTaskEmail(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void CheckingIfTaskShouldRunNowShouldReturnTrueIfDateTimeMatches()
        {
            var listTasks = new List<string>();
            listTasks.Add("Wednesday,14:50,AssessmentDatesTask,1");
            _mockTaskLoader.Setup(m => m.LoadTasks(It.IsAny<string>())).Returns(listTasks);
            _taskScheduler = new TaskScheduler(_mockTaskLoader.Object, _mockTaskEmailer.Object, _listTasks.ToArray(), _mockCheckMarketOpenClose.Object);
            ;
            var aomTask = new AomTask{ DayToRun="Monday", TaskName = "AA", TimeToRun = "10:10"};
            var dateTimeNow = new DateTime(2017, 3, 6, 10, 10, 12);
            var taskShouldRunNow = _taskScheduler.TaskShouldRunNow(aomTask, dateTimeNow);

            Assert.That(taskShouldRunNow, Is.True);
        }

        [Test]
        public void CheckingIfTaskShouldRunNowShouldReturnFalseIfDateTimeDoesNotMatch()
        {
            var listTasks = new List<string>();
            listTasks.Add("Wednesday,14:50,AssessmentDatesTask,1");
            _mockTaskLoader.Setup(m => m.LoadTasks(It.IsAny<string>())).Returns(listTasks);
            _taskScheduler = new TaskScheduler(_mockTaskLoader.Object, _mockTaskEmailer.Object, _listTasks.ToArray(), _mockCheckMarketOpenClose.Object);
            ;
            var aomTask = new AomTask { DayToRun = "Monday", TaskName = "AA", TimeToRun = "10:10" };
            var dateTimeNow = new DateTime(2017, 3, 6, 10, 11, 12);
            var taskShouldRunNow = _taskScheduler.TaskShouldRunNow(aomTask, dateTimeNow);

            Assert.That(taskShouldRunNow, Is.False);
        }
    }
}
