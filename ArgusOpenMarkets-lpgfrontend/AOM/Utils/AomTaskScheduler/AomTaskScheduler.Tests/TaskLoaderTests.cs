﻿using NUnit.Framework;

namespace AomTaskScheduler.Tests
{
    [TestFixture]
    public class TaskLoaderTests
    {
        public void ShouldLoadTasksFromCsvFile()
        {
            var taskLoader = new TaskLoader();
            Assert.That(taskLoader, Is.Not.Null);
        }
    }
}
