﻿using System;
using System.Collections.Generic;
using AomTaskScheduler.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace AomTaskScheduler.Tests.Tasks
{
    [TestFixture]
    public class AomTaskTests
    {
        public void ShouldInitializeAomTask()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask, Is.Not.Null);
        }

        [Test]
        public void ShouldInitializeAomTaskWithNoAdditionalParameters()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask, Is.Not.Null);
            Assert.That(aomTask.AdditionalParameters, Is.EqualTo(new List<string>()));
        }

        [Test]
        public void ShouldInitializeAomTaskWithDayToExecuteSet()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask, Is.Not.Null);
            Assert.That(aomTask.DayOfWeekToRun, Is.EqualTo(DayOfWeek.Wednesday));
        }

        [Test]
        public void ShouldInitializeAomTaskWithHourToRunSet()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask, Is.Not.Null);
            Assert.That(aomTask.HourToRun, Is.EqualTo(14));
        }

        [Test]
        public void ShouldInitializeAomTaskWithMinuteToRunSet()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask, Is.Not.Null);
            Assert.That(aomTask.MinuteToRun, Is.EqualTo(50));
        }

        [Test]
        public void ShouldInitializeAomTaskWithSingleAdditionalParameter()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask,AddionalParam1";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask.AdditionalParameters, Is.Not.Null);
            Assert.That(aomTask.AdditionalParameters.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldInitializeAomTaskAndCallingGetParamAsStringShouldReturnValue()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask,AddionalParam1";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask.AdditionalParameters, Is.Not.Null);
            Assert.That(aomTask.GetAdditionalParamAsString(0), Is.EqualTo("AddionalParam1"));
        }

        [Test]
        public void ShouldInitializeAomTaskAndCallingGetParamAsLongShouldReturnZeroIfNaN()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask,AddionalParam1";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask.AdditionalParameters, Is.Not.Null);
            Assert.That(aomTask.GetAdditionalParamAsLong(0), Is.EqualTo(0));
        }

        [Test]
        public void ShouldInitializeAomTaskAndCallingGetParamAsLongShouldReturnValueIfNumber()
        {
            var taskLine = "Wednesday,14:50,AssessmentDatesTask,100";
            var aomTask = new AomTask(taskLine);

            Assert.That(aomTask.AdditionalParameters, Is.Not.Null);
            Assert.That(aomTask.GetAdditionalParamAsLong(0), Is.EqualTo(100));
        }
    }
}
