﻿using System.Collections.Generic;
using AomTaskScheduler.Tasks;
using NUnit.Framework;

namespace AomTaskScheduler.Tests.Tasks
{
    [TestFixture]
    public class AssessmentDateTaskTests
    {
        [Test]
        public void ShouldCreateAssessmentDateTaskFromGenericTask()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            var assessmentTask = new AssessmentDateTask(baseTask);

            Assert.That(assessmentTask, Is.Not.Null);
        }

        [Test]
        public void ShouldPopulateProductIdForAssessmentTask()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            var assessmentTask = new AssessmentDateTask(baseTask);

            Assert.That(assessmentTask.ProductId, Is.EqualTo(1));
        }

        [Test]
        public void ShouldPopulateTodaysPriceHigh()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            var assessmentTask = new AssessmentDateTask(baseTask);

            Assert.That(assessmentTask.TodayHigh, Is.EqualTo(0));
        }

        [Test]
        public void ShouldPopulateTodaysPriceLow()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            var assessmentTask = new AssessmentDateTask(baseTask);

            Assert.That(assessmentTask.TodayLow, Is.EqualTo(0));
        }

        [Test]
        public void ShouldPopulateNumberDaysToGoBackToPreviousDate()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            var assessmentTask = new AssessmentDateTask(baseTask);

            Assert.That(assessmentTask.NumberDaysToGoBack, Is.EqualTo(-1));
        }

        private AomTask CreateBaseTask()
        {
            var additionalParameters = new List<string>();
            additionalParameters.Add("1");
            additionalParameters.Add("0");
            additionalParameters.Add("0");
            additionalParameters.Add("-1");

            var baseTask = new AomTask {DayToRun = "Monday", AdditionalParameters = additionalParameters, TaskName = "AssessmentDatesTask", TimeToRun = "12:00"};

            return baseTask;
        }
    }
}