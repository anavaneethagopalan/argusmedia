﻿using System.Collections.Generic;
using AomTaskScheduler.Tasks;
using NUnit.Framework;

namespace AomTaskScheduler.Tests.Tasks
{
    [TestFixture]
    public class IgnoreMarketStatustriggersTaskTests
    {
        [Test]
        public void ShouldCreateIgnoreMarketStatusTriggersTaskFromGenericTask()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            var marketStatusTask = new IgnoreMarketStatustriggersTask(baseTask);

            Assert.That(marketStatusTask, Is.Not.Null);
        }

        [Test]
        public void ShouldAssignProductIdToMarketStatusTask()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            var marketStatusTask = new IgnoreMarketStatustriggersTask(baseTask);

            Assert.That(marketStatusTask.ProductId, Is.EqualTo(1));
        }

        [Test]
        public void ShouldCheckedOrUncheckedToCheckedForMarketStatusTask()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            var marketStatusTask = new IgnoreMarketStatustriggersTask(baseTask);

            Assert.That(marketStatusTask.CheckedOrUnchecked, Is.EqualTo(true));
        }

        [Test]
        public void ShouldCheckedOrUncheckedToCheckedAllUppercaseShouldReturnTrueForMarketStatusTask()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            baseTask.AdditionalParameters[1] = "CHECKED";
            var marketStatusTask = new IgnoreMarketStatustriggersTask(baseTask);

            Assert.That(marketStatusTask.CheckedOrUnchecked, Is.EqualTo(true));
        }

        [Test]
        public void ShouldCheckedOrUncheckedToUnCheckedForMarketStatusTask()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            baseTask.AdditionalParameters[1] = "unchecked";
            var marketStatusTask = new IgnoreMarketStatustriggersTask(baseTask);

            Assert.That(marketStatusTask.CheckedOrUnchecked, Is.EqualTo(false));
        }

        [Test]
        public void SettingCheckedOrUnCheckedToAnythingOtherThanCheckedWillResultInFalse()
        {
            // Sample Assessment Task definition:
            // Wednesday,14:50,AssessmentDatesTask,1,null,null,-1
            var baseTask = CreateBaseTask();
            baseTask.AdditionalParameters[1] = "nathan";
            var marketStatusTask = new IgnoreMarketStatustriggersTask(baseTask);

            Assert.That(marketStatusTask.CheckedOrUnchecked, Is.EqualTo(false));
        }

        private AomTask CreateBaseTask()
        {
            var additionalParameters = new List<string>();
            additionalParameters.Add("1");
            additionalParameters.Add("checked");

            var baseTask = new AomTask { DayToRun = "Monday", AdditionalParameters = additionalParameters, TaskName = "AssessmentDatesTask", TimeToRun = "12:00" };

            return baseTask;
        }
    }
}
