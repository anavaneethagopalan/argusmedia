﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.XPath;

namespace AppConfigConStrUtil
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            textBoxLookup.Text = ConfigurationManager.AppSettings["ConfigPath"];
            textBoxConnStr.Text = ConfigurationManager.AppSettings["AomConnStr"];
            textBoxCrmConnStr.Text = ConfigurationManager.AppSettings["CrmConnStr"];
        }

        #region Event Handlers
        private void buttonLookup_Click(object sender, EventArgs e)
        {
            var configDlg = new FolderBrowserDialog();
            configDlg.SelectedPath = textBoxLookup.Text;
            configDlg.Description = "Please select an exising project folder which contains App.Config file(s):";
            configDlg.ShowNewFolderButton = false;
            if (configDlg.ShowDialog(this) != DialogResult.OK) return;

            textBoxLookup.Text = configDlg.SelectedPath;
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "You are about to modify all the App.config files using the new Connection String!\r\n\r\nPlease click OK button to confirm.", "Modify App.config File", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.OK)
                return;

            textBoxLog.Clear();
            try
            {
                UpdateAppConfig();
            }
            catch (Exception ex)
            {
                textBoxLog.AppendText(string.Format("ERROR: {0} due to: {1}\r\n", ex.Message, ex.StackTrace));
            }
        }


        private void buttonRevert_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "You are about to Restore all the App.config files!\r\n\r\nPlease click OK button to confirm.", "Modify App.config File", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.OK)
                return;

            textBoxLog.Clear();
            try
            {
                UpdateAppConfig(true);
            }
            catch (Exception ex)
            {
                textBoxLog.AppendText(string.Format("ERROR: {0} due to: {1}\r\n", ex.Message, ex.StackTrace));
            }
        }
        #endregion

        #region Methods
        private void UpdateAppConfig(bool isRevert = false)
        {
            var lookup = textBoxLookup.Text;
            var aomConnStr = textBoxConnStr.Text;
            var crmConnStr = textBoxCrmConnStr.Text;

            var lookupDir = new DirectoryInfo(lookup);
            var appConfigFiles = lookupDir.EnumerateFiles("App.config", SearchOption.AllDirectories)
                .Concat(lookupDir.EnumerateFiles("connectionStrings.config", SearchOption.AllDirectories))
                .Where(file => !file.FullName.Contains("bin") && !file.FullName.Contains("Test"));

            var count = 0;
            foreach (var appConfigFile in appConfigFiles)
            {
                if (UpdateConnStr(appConfigFile.FullName, aomConnStr, crmConnStr, isRevert))
                {
                    textBoxLog.AppendText(string.Format("{0} -- Updated!\r\n", appConfigFile.FullName));
                    count++;
                }
            }
            textBoxLog.AppendText(string.Format("{0} App.Config File(s) Updated!\r\n", count));
        }

        private bool UpdateConnStr(string configFile, string aomConnStr, string crmConnStr, bool isRevert)
        {
            var result = false;
            var configBackupFile = string.Format("{0}.TXT", configFile);
            if (isRevert)
            {
                result = File.Exists(configBackupFile);
                if (result)
                {
                    File.Delete(configFile);
                    File.Move(configBackupFile, configFile);
                    File.Delete(configBackupFile);
                }

                return result;
            }

            if (File.Exists(configBackupFile)) return false;

            var configDoc = XDocument.Load(configFile);
            var aomConfig = configDoc.XPathSelectElement("configuration/connectionStrings/add[@name='AomModel']");
            var crmConfig = configDoc.XPathSelectElement("configuration/connectionStrings/add[@name='CrmModel']");

            aomConfig = aomConfig ?? configDoc.XPathSelectElement("connectionStrings/add[@name='AomModel']");
            crmConfig = crmConfig ?? configDoc.XPathSelectElement("connectionStrings/add[@name='CrmModel']");

            if (aomConfig != null || crmConfig != null) File.Copy(configFile, configBackupFile, true);

            if (aomConfig != null)
            {
                result = true;
                aomConfig.SetAttributeValue("connectionString", aomConnStr);
            }
            if (crmConfig != null)
            {
                result = true;
                crmConfig.SetAttributeValue("connectionString", crmConnStr);
            }

            if (result) configDoc.Save(configFile);

            return result;
        }
        #endregion
    }
}
