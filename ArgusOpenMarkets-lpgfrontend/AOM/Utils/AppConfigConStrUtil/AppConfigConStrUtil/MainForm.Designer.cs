﻿namespace AppConfigConStrUtil
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.labelLookup = new System.Windows.Forms.Label();
            this.textBoxLookup = new System.Windows.Forms.TextBox();
            this.buttonLookup = new System.Windows.Forms.Button();
            this.labelConnStr = new System.Windows.Forms.Label();
            this.textBoxConnStr = new System.Windows.Forms.TextBox();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.buttonGo = new System.Windows.Forms.Button();
            this.labelCrmConnStr = new System.Windows.Forms.Label();
            this.textBoxCrmConnStr = new System.Windows.Forms.TextBox();
            this.buttonRevert = new System.Windows.Forms.Button();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // labelLookup
            // 
            this.labelLookup.AutoSize = true;
            this.labelLookup.Location = new System.Drawing.Point(12, 16);
            this.labelLookup.Name = "labelLookup";
            this.labelLookup.Size = new System.Drawing.Size(169, 14);
            this.labelLookup.TabIndex = 4;
            this.labelLookup.Text = "Where to Locate App.Config:";
            // 
            // textBoxLookup
            // 
            this.textBoxLookup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLookup.Location = new System.Drawing.Point(187, 12);
            this.textBoxLookup.Name = "textBoxLookup";
            this.textBoxLookup.ReadOnly = true;
            this.textBoxLookup.Size = new System.Drawing.Size(549, 22);
            this.textBoxLookup.TabIndex = 5;
            // 
            // buttonLookup
            // 
            this.buttonLookup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLookup.Location = new System.Drawing.Point(742, 12);
            this.buttonLookup.Name = "buttonLookup";
            this.buttonLookup.Size = new System.Drawing.Size(30, 23);
            this.buttonLookup.TabIndex = 0;
            this.buttonLookup.Text = "...";
            this.buttonLookup.UseVisualStyleBackColor = true;
            this.buttonLookup.Click += new System.EventHandler(this.buttonLookup_Click);
            // 
            // labelConnStr
            // 
            this.labelConnStr.AutoSize = true;
            this.labelConnStr.Location = new System.Drawing.Point(12, 45);
            this.labelConnStr.Name = "labelConnStr";
            this.labelConnStr.Size = new System.Drawing.Size(168, 14);
            this.labelConnStr.TabIndex = 6;
            this.labelConnStr.Text = "New AOM Connection String:";
            // 
            // textBoxConnStr
            // 
            this.textBoxConnStr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxConnStr.Location = new System.Drawing.Point(187, 41);
            this.textBoxConnStr.Name = "textBoxConnStr";
            this.textBoxConnStr.Size = new System.Drawing.Size(549, 22);
            this.textBoxConnStr.TabIndex = 1;
            // 
            // textBoxLog
            // 
            this.textBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLog.Location = new System.Drawing.Point(12, 97);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(760, 453);
            this.textBoxLog.TabIndex = 8;
            // 
            // buttonGo
            // 
            this.buttonGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonGo.BackgroundImage")));
            this.buttonGo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonGo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo.ForeColor = System.Drawing.Color.White;
            this.buttonGo.Location = new System.Drawing.Point(742, 39);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(30, 24);
            this.buttonGo.TabIndex = 3;
            this.toolTipMain.SetToolTip(this.buttonGo, "Start Update");
            this.buttonGo.UseVisualStyleBackColor = false;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // labelCrmConnStr
            // 
            this.labelCrmConnStr.AutoSize = true;
            this.labelCrmConnStr.Location = new System.Drawing.Point(12, 73);
            this.labelCrmConnStr.Name = "labelCrmConnStr";
            this.labelCrmConnStr.Size = new System.Drawing.Size(165, 14);
            this.labelCrmConnStr.TabIndex = 7;
            this.labelCrmConnStr.Text = "New CRM Connection String:";
            // 
            // textBoxCrmConnStr
            // 
            this.textBoxCrmConnStr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCrmConnStr.Location = new System.Drawing.Point(187, 69);
            this.textBoxCrmConnStr.Name = "textBoxCrmConnStr";
            this.textBoxCrmConnStr.Size = new System.Drawing.Size(549, 22);
            this.textBoxCrmConnStr.TabIndex = 2;
            // 
            // buttonRevert
            // 
            this.buttonRevert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRevert.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonRevert.BackgroundImage")));
            this.buttonRevert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonRevert.Location = new System.Drawing.Point(742, 65);
            this.buttonRevert.Name = "buttonRevert";
            this.buttonRevert.Size = new System.Drawing.Size(30, 24);
            this.buttonRevert.TabIndex = 9;
            this.toolTipMain.SetToolTip(this.buttonRevert, "Revert Update");
            this.buttonRevert.UseVisualStyleBackColor = true;
            this.buttonRevert.Click += new System.EventHandler(this.buttonRevert_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.buttonRevert);
            this.Controls.Add(this.textBoxCrmConnStr);
            this.Controls.Add(this.labelCrmConnStr);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.textBoxConnStr);
            this.Controls.Add(this.labelConnStr);
            this.Controls.Add(this.buttonLookup);
            this.Controls.Add(this.textBoxLookup);
            this.Controls.Add(this.labelLookup);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "App Config Conn Str Localizer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelLookup;
        private System.Windows.Forms.TextBox textBoxLookup;
        private System.Windows.Forms.Button buttonLookup;
        private System.Windows.Forms.Label labelConnStr;
        private System.Windows.Forms.TextBox textBoxConnStr;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.Label labelCrmConnStr;
        private System.Windows.Forms.TextBox textBoxCrmConnStr;
        private System.Windows.Forms.Button buttonRevert;
        private System.Windows.Forms.ToolTip toolTipMain;
    }
}

