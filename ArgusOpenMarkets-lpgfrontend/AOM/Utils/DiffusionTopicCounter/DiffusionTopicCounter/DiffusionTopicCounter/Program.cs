﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PushTechnology.ClientInterface.Client.Factories;
using PushTechnology.ClientInterface.Client.Features;
using PushTechnology.ClientInterface.Client.Topics;

namespace DiffusionTopicCounter
{
    static class TopicCounter
    {
        private static object _lock = new object();
        private static long _topicsCreated = 0;
        private static long _dataWrittenToTopic = 0;

        public static void IncreaseTopicsCreated()
        {
            Interlocked.Increment(ref _topicsCreated);
            Console.Write("TC+:" + _topicsCreated);
        }

        public static void DecreaseTopicsCreated()
        {
            Interlocked.Decrement(ref _topicsCreated);
            Console.Write("TC-:" + _topicsCreated);
        }
    }

    public class DiffusionStream : TopicStreamDefault
    {
        public override void OnSubscription(string topicPath, ITopicDetails details)
        {
            TopicCounter.IncreaseTopicsCreated();
        }

        public override void OnUnsubscription(string topicPath, TopicUnsubscribeReason reason)
        {
            TopicCounter.DecreaseTopicsCreated();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ConnectToDiffusion();
            Console.ReadLine();

        }

        private static void ConnectToDiffusion()
        {
            var session = Diffusion.Sessions.Principal("AuthenticationAdmin")
                .Password("vjt7676FKZ1qaz2wsx")
                .Open("dpt://10.22.5.16:8080");

            session.GetTopicsFeature().Subscribe("?AOM//", new TopicsCompletionCallbackDefault());
            session.GetTopicsFeature().AddTopicStream("?AOM//",new DiffusionStream());
        }
    }
}
