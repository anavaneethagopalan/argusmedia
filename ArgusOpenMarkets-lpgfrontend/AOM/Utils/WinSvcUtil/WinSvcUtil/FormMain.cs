﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinSvcUtil
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();

            Initialize();
        }

        private ConcurrentQueue<string> FailedSvc { get; set; }

        #region Event Handlers
        private void buttonLoadService_Click(object sender, EventArgs e)
        {
            try
            {
                listViewServices.Items.Clear();
                buttonStartService.Enabled = false;
                buttonStopService.Enabled = false;
                var machineName = comboBoxMachineName.Text.Trim();
                var serviceNamePattern = comboBoxServiceNamePattern.Text.Trim();
                var services = string.IsNullOrEmpty(machineName)
                                   ? ServiceController.GetServices()
                                   : ServiceController.GetServices(machineName);
                foreach (var service in services)
                {
                    if (string.IsNullOrEmpty(serviceNamePattern))
                    {
                        listViewServices.Items.Add(PopulateServiceItem(service));
                    }
                    else if (service.ServiceName.ToLower().Contains(serviceNamePattern.ToLower()))
                    {
                        listViewServices.Items.Add(PopulateServiceItem(service));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Load Services", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void checkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem serviceItem in listViewServices.Items)
            {
                serviceItem.Checked = checkBoxSelectAll.Checked;
            }
        }

        private void listViewServices_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            SyncButton();
        }

        private void buttonStartService_Click(object sender, EventArgs e)
        {
            ControlService(true);
        }

        private void buttonStopService_Click(object sender, EventArgs e)
        {
            ControlService(false);
        }
        #endregion

        #region Methods
        private ListViewItem PopulateServiceItem(ServiceController service)
        {
            var serviceItem = new ListViewItem(service.ServiceName);
            serviceItem.SubItems.Add(service.MachineName);
            serviceItem.SubItems.Add(service.Status.ToString());
            serviceItem.BackColor = GetStatusColor(service.Status, listViewServices.BackColor);
            serviceItem.Tag = service;
            serviceItem.Checked = checkBoxSelectAll.Checked;

            return serviceItem;
        }
        private void ControlService(bool isStart)
        {
            if (listViewServices.CheckedItems.Count <= 0)
            {
                return;
            }

            var message = isStart
                              ? "You are about to START all the selected services!\r\nPlease click OK button to confirm."
                              : "You are about to STOP all the selected services!\r\nPlease click OK button to confirm.";
            var title = isStart ? "Start Services" : "Stop Services";

            if (MessageBox.Show(this, message, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning,
                                MessageBoxDefaultButton.Button2) == DialogResult.Cancel)
            {
                return;
            }

            Cursor = Cursors.WaitCursor;
            var selectedServices = new ListViewItem[listViewServices.CheckedItems.Count];
            listViewServices.CheckedItems.CopyTo(selectedServices, 0);
            FailedSvc = new ConcurrentQueue<string>();

            Parallel.ForEach(selectedServices, serviceItem =>
            {
                var service = serviceItem.Tag as ServiceController;
                try
                {
                    if (isStart)
                    {
                        if (service.Status == ServiceControllerStatus.Stopped)
                        {
                            service.Start();
                            serviceItem.SubItems[2].Text = "Starting";
                            service.WaitForStatus(ServiceControllerStatus.Running);
                        }
                    }
                    else
                    {
                        if (service.Status == ServiceControllerStatus.Running)
                        {
                            service.Stop();
                            serviceItem.SubItems[2].Text = "Stopping";
                            service.WaitForStatus(ServiceControllerStatus.Stopped);
                        }
                    }
                }
                catch
                {
                    FailedSvc.Enqueue(service.ServiceName);
                }
            });

            foreach (ListViewItem selectedItem in listViewServices.CheckedItems)
            {
                var service = selectedItem.Tag as ServiceController;
                selectedItem.SubItems[2].Text = service.Status.ToString();
                selectedItem.BackColor = GetStatusColor(service.Status, listViewServices.BackColor);
            }

            SyncButton();

            message = isStart ? "Services Start is Completed." : "Services Stop is Completed.";
            if (FailedSvc.Count > 0)
            {
                var faileSvsBuilder = new StringBuilder();
                var serviceName = string.Empty;
                while (FailedSvc.TryDequeue(out serviceName))
                {
                    if (string.IsNullOrEmpty(serviceName)) continue;
                    faileSvsBuilder.AppendLine(serviceName);
                }
                message = string.Format("{0}\r\n\r\n{1}", message,
                          string.Format("Following Service(s) can NOT be {0}:\r\n{1}",
                          isStart ? "Started" : "Stopped", faileSvsBuilder));
            }
            Cursor = Cursors.Default;
            MessageBox.Show(this, message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private Color GetStatusColor(ServiceControllerStatus status, Color defaultColor)
        {
            return status == ServiceControllerStatus.Running ? Color.LimeGreen :
                   status == ServiceControllerStatus.Stopped ? Color.Red : defaultColor;
        }
        private void SyncButton()
        {
            var selectedItems = new ListViewItem[listViewServices.CheckedItems.Count];
            listViewServices.CheckedItems.CopyTo(selectedItems, 0);
            buttonStartService.Enabled = selectedItems.Any(item => (item.Tag as ServiceController).Status == ServiceControllerStatus.Stopped);
            buttonStopService.Enabled = selectedItems.Any(item => (item.Tag as ServiceController).Status == ServiceControllerStatus.Running);
        }

        private void Initialize()
        {
            comboBoxMachineName.Items.AddRange(ConfigurationManager.AppSettings["Machine"].Split(';'));
            comboBoxServiceNamePattern.Items.AddRange(ConfigurationManager.AppSettings["Service"].Split(';'));

            comboBoxMachineName.Text = comboBoxMachineName.Items.Count > 0 ? comboBoxMachineName.Items[0].ToString() : "localhost";
            comboBoxServiceNamePattern.Text = comboBoxServiceNamePattern.Items.Count > 0 ? comboBoxServiceNamePattern.Items[0].ToString() : "aom";
        }
        #endregion
    }
}
