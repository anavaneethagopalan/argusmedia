﻿namespace WinSvcUtil
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.labelMachineName = new System.Windows.Forms.Label();
            this.comboBoxMachineName = new System.Windows.Forms.ComboBox();
            this.labelServiceName = new System.Windows.Forms.Label();
            this.comboBoxServiceNamePattern = new System.Windows.Forms.ComboBox();
            this.listViewServices = new System.Windows.Forms.ListView();
            this.columnHeaderServiceName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderMachineName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonLoadService = new System.Windows.Forms.Button();
            this.buttonStartService = new System.Windows.Forms.Button();
            this.buttonStopService = new System.Windows.Forms.Button();
            this.checkBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // labelMachineName
            // 
            this.labelMachineName.AutoSize = true;
            this.labelMachineName.Location = new System.Drawing.Point(12, 16);
            this.labelMachineName.Name = "labelMachineName";
            this.labelMachineName.Size = new System.Drawing.Size(90, 14);
            this.labelMachineName.TabIndex = 6;
            this.labelMachineName.Text = "Machine Name:";
            // 
            // comboBoxMachineName
            // 
            this.comboBoxMachineName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMachineName.FormattingEnabled = true;
            this.comboBoxMachineName.Location = new System.Drawing.Point(142, 12);
            this.comboBoxMachineName.Name = "comboBoxMachineName";
            this.comboBoxMachineName.Size = new System.Drawing.Size(264, 22);
            this.comboBoxMachineName.TabIndex = 0;
            // 
            // labelServiceName
            // 
            this.labelServiceName.AutoSize = true;
            this.labelServiceName.Location = new System.Drawing.Point(12, 44);
            this.labelServiceName.Name = "labelServiceName";
            this.labelServiceName.Size = new System.Drawing.Size(130, 14);
            this.labelServiceName.TabIndex = 7;
            this.labelServiceName.Text = "Service Name Pattern:";
            // 
            // comboBoxServiceNamePattern
            // 
            this.comboBoxServiceNamePattern.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxServiceNamePattern.FormattingEnabled = true;
            this.comboBoxServiceNamePattern.Location = new System.Drawing.Point(142, 40);
            this.comboBoxServiceNamePattern.Name = "comboBoxServiceNamePattern";
            this.comboBoxServiceNamePattern.Size = new System.Drawing.Size(264, 22);
            this.comboBoxServiceNamePattern.TabIndex = 1;
            // 
            // listViewServices
            // 
            this.listViewServices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewServices.CheckBoxes = true;
            this.listViewServices.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderServiceName,
            this.columnHeaderMachineName,
            this.columnHeaderStatus});
            this.listViewServices.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewServices.FullRowSelect = true;
            this.listViewServices.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewServices.Location = new System.Drawing.Point(12, 70);
            this.listViewServices.MultiSelect = false;
            this.listViewServices.Name = "listViewServices";
            this.listViewServices.Size = new System.Drawing.Size(610, 373);
            this.listViewServices.TabIndex = 8;
            this.listViewServices.UseCompatibleStateImageBehavior = false;
            this.listViewServices.View = System.Windows.Forms.View.Details;
            this.listViewServices.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewServices_ItemChecked);
            // 
            // columnHeaderServiceName
            // 
            this.columnHeaderServiceName.Text = "Service Name";
            this.columnHeaderServiceName.Width = 360;
            // 
            // columnHeaderMachineName
            // 
            this.columnHeaderMachineName.Text = "Machine Name";
            this.columnHeaderMachineName.Width = 120;
            // 
            // columnHeaderStatus
            // 
            this.columnHeaderStatus.Text = "Status";
            this.columnHeaderStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderStatus.Width = 90;
            // 
            // buttonLoadService
            // 
            this.buttonLoadService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoadService.Location = new System.Drawing.Point(412, 12);
            this.buttonLoadService.Name = "buttonLoadService";
            this.buttonLoadService.Size = new System.Drawing.Size(101, 23);
            this.buttonLoadService.TabIndex = 2;
            this.buttonLoadService.Text = "Load Services";
            this.buttonLoadService.UseVisualStyleBackColor = true;
            this.buttonLoadService.Click += new System.EventHandler(this.buttonLoadService_Click);
            // 
            // buttonStartService
            // 
            this.buttonStartService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStartService.Enabled = false;
            this.buttonStartService.Location = new System.Drawing.Point(519, 12);
            this.buttonStartService.Name = "buttonStartService";
            this.buttonStartService.Size = new System.Drawing.Size(101, 23);
            this.buttonStartService.TabIndex = 4;
            this.buttonStartService.Text = "Start Services";
            this.buttonStartService.UseVisualStyleBackColor = true;
            this.buttonStartService.Click += new System.EventHandler(this.buttonStartService_Click);
            // 
            // buttonStopService
            // 
            this.buttonStopService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStopService.Enabled = false;
            this.buttonStopService.Location = new System.Drawing.Point(519, 41);
            this.buttonStopService.Name = "buttonStopService";
            this.buttonStopService.Size = new System.Drawing.Size(101, 23);
            this.buttonStopService.TabIndex = 5;
            this.buttonStopService.Text = "Stop Services";
            this.buttonStopService.UseVisualStyleBackColor = true;
            this.buttonStopService.Click += new System.EventHandler(this.buttonStopService_Click);
            // 
            // checkBoxSelectAll
            // 
            this.checkBoxSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxSelectAll.AutoSize = true;
            this.checkBoxSelectAll.Checked = true;
            this.checkBoxSelectAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSelectAll.Location = new System.Drawing.Point(412, 43);
            this.checkBoxSelectAll.Name = "checkBoxSelectAll";
            this.checkBoxSelectAll.Size = new System.Drawing.Size(82, 18);
            this.checkBoxSelectAll.TabIndex = 3;
            this.checkBoxSelectAll.Text = "Select All?";
            this.checkBoxSelectAll.UseVisualStyleBackColor = true;
            this.checkBoxSelectAll.CheckedChanged += new System.EventHandler(this.checkBoxSelectAll_CheckedChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 455);
            this.Controls.Add(this.checkBoxSelectAll);
            this.Controls.Add(this.buttonStopService);
            this.Controls.Add(this.buttonStartService);
            this.Controls.Add(this.buttonLoadService);
            this.Controls.Add(this.listViewServices);
            this.Controls.Add(this.comboBoxServiceNamePattern);
            this.Controls.Add(this.labelServiceName);
            this.Controls.Add(this.comboBoxMachineName);
            this.Controls.Add(this.labelMachineName);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Windows Service Util";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMachineName;
        private System.Windows.Forms.ComboBox comboBoxMachineName;
        private System.Windows.Forms.Label labelServiceName;
        private System.Windows.Forms.ComboBox comboBoxServiceNamePattern;
        private System.Windows.Forms.ListView listViewServices;
        private System.Windows.Forms.ColumnHeader columnHeaderServiceName;
        private System.Windows.Forms.ColumnHeader columnHeaderMachineName;
        private System.Windows.Forms.ColumnHeader columnHeaderStatus;
        private System.Windows.Forms.Button buttonLoadService;
        private System.Windows.Forms.Button buttonStartService;
        private System.Windows.Forms.Button buttonStopService;
        private System.Windows.Forms.CheckBox checkBoxSelectAll;
    }
}

