﻿using System.Collections.Generic;
using AomRepository.Entity;

namespace AomRepository.Repositories
{
    public interface IProductRepository : IRepository<DapperProduct>
    {
        List<DapperProduct> GetProducts();
    }
}
