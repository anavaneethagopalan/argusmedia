﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using AomRepository.Entity;
using Dapper;

namespace AomRepository.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private IDbConnection _db;

        private string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["AomModel"].ConnectionString; }
        }

        public ProductRepository()
        {
            _db =
                new MySql.Data.MySqlClient.MySqlConnection(
                    ConfigurationManager.ConnectionStrings["AomModel"].ConnectionString);
        }

        public List<DapperProduct> GetProducts()
        {
            List<DapperProduct> products;
            using (_db = new MySql.Data.MySqlClient.MySqlConnection(ConnectionString))
            {
                products = _db.Query<DapperProduct>(@"SELECT `Product`.`Id`,
                    `Product`.`Name`,
                    `Product`.`IgnoreAutomaticMarketStatusTriggers`,
                    `Product`.`OpenTime`,
                    `Product`.`CloseTime`,
                    `Product`.`LastOpenDate`,
                    `Product`.`LastCloseDate`,
                    `Product`.`IsInternal`,
                    `Product`.`Status`
                FROM `Product`").ToList();
            }

            return products;
        }

        public DapperProduct Get(int id)
        {
            return null;

        }

        public int Add(DapperProduct entity)
        {
            return 1;
        }

        public void Delete(DapperProduct entity)
        {
        }

        public void Update(DapperProduct entity)
        {

        }
    }
}