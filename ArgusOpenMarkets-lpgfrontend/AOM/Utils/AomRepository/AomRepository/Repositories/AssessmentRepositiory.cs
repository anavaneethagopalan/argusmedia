﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using AomRepository.Entity;
using Dapper;

namespace AomRepository.Repositories
{
    public class AssessmentRepository : IAssessmentRepository
    {
        private IDbConnection _db;

        private string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["AomModel"].ConnectionString; }
        }

        public AssessmentRepository()
        {
            _db = new MySql.Data.MySqlClient.MySqlConnection(ConfigurationManager.ConnectionStrings["AomModel"].ConnectionString);
        }

        public List<DapperAssessment> GetAssessmentsByProductId(long productId)
        {
            List<DapperAssessment> assessments;
            using (_db = new MySql.Data.MySqlClient.MySqlConnection(ConnectionString))
            {
                assessments = _db.Query<DapperAssessment>(@"SELECT `Assessment`.`Id`,
                `Assessment`.`Product_Id_fk`,
                `Assessment`.`EnteredByUser_Id`,
                `Assessment`.`LastUpdatedUser_Id`,
                `Assessment`.`PriceHigh`,
                `Assessment`.`PriceLow`,
                `Assessment`.`BusinessDate`,
                `Assessment`.`LastUpdated`,
                `Assessment`.`DateCreated`,
                `Assessment`.`AssessmentStatus`
            FROM `Assessment`
            WHERE `Assessment`.`Product_Id_fk` = @productId",
                    new {productId}).ToList();
            }

            return assessments;
        }

        public DapperAssessment Get(int id)
        {
            return _db.Query<DapperAssessment>(@"SELECT `Assessment`.`Id`,
                `Assessment`.`Product_Id_fk`,
                `Assessment`.`EnteredByUser_Id`,
                `Assessment`.`LastUpdatedUser_Id`,
                `Assessment`.`PriceHigh`,
                `Assessment`.`PriceLow`,
                `Assessment`.`BusinessDate`,
                `Assessment`.`LastUpdated`,
                `Assessment`.`DateCreated`,
                `Assessment`.`AssessmentStatus`
            FROM `Assessment`
            WHERE `Assessment`.`Id` = @id",
                new {id}).SingleOrDefault();

        }

        public int Add(DapperAssessment entity)
        {
            int newId;

            string insertQuery = @"INSERT INTO `Assessment`
                (`Product_Id_fk`,
                `EnteredByUser_Id`,
                `LastUpdatedUser_Id`,
                `PriceHigh`,
                `PriceLow`,
                `BusinessDate`,
                `LastUpdated`,
                `DateCreated`,
                `AssessmentStatus`)
                VALUES
                (@ProductId,
                @EnteredByUserId,
                @LastUpdatedUserId,
                @PriceHigh,
                @PriceLow,
                @BusinessDate,
                @LastUpdated,
                @DateCreated,
                @AssessmentStatus);SELECT LAST_INSERT_ID();";

            using (_db = new MySql.Data.MySqlClient.MySqlConnection(ConnectionString))
            {
                newId = _db.Query<int>(insertQuery, entity).Single();
            }

            return newId;
        }

        public void Delete(DapperAssessment entity)
        {
            var deleteQuery = @"DELETE FROM `Assessment`
                WHERE `Assessment`.`Id` = @id";

            _db.Execute(deleteQuery, entity);
        }

        public void Update(DapperAssessment entity)
        {
            string updateQuery = @"UPDATE `Assessment`
                SET
                `LastUpdatedUser_Id` = @LastUpdatedUserId,
                `PriceHigh` = @PriceHigh,
                `PriceLow` = @PriceLow,
                `BusinessDate` = @BusinessDate,
                `LastUpdated` = @LastUpdated,
                `AssessmentStatus` = @AssessmentStatus
                WHERE `Id` = @Id;";

            using (_db = new MySql.Data.MySqlClient.MySqlConnection(ConnectionString))
            {
                _db.Execute(updateQuery, entity);
            }
        }
    }
}