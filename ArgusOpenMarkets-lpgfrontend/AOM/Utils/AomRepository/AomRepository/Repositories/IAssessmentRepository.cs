﻿using System.Collections.Generic;
using AomRepository.Entity;

namespace AomRepository.Repositories
{
    public interface IAssessmentRepository : IRepository<DapperAssessment>
    {
        List<DapperAssessment> GetAssessmentsByProductId(long productId);
    }
}