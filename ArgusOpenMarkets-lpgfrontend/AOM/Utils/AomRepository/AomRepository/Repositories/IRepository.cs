﻿namespace AomRepository.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(int id);
        int Add(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
    }
}