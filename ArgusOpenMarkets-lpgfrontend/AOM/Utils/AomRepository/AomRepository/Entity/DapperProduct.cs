﻿using System;

namespace AomRepository.Entity
{
    public class DapperProduct
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public bool IgnoreAutomaticMarketStatusTriggers { get; set; }

        public TimeSpan OpenTime { get; set; }

        public TimeSpan CloseTime { get; set; }

        public DateTime LastOpenDate { get; set; }

        public DateTime LastCloseDate { get; set; }

        public bool IsInternal { get; set; }

        public string Status { get; set; }
    }
}
