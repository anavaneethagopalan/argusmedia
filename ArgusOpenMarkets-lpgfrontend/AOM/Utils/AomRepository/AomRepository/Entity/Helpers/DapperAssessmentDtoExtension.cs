﻿using AOM.App.Domain.Entities;

namespace AomRepository.Entity.Helpers
{
    public static class DapperAssessmentExtensions
    {
        public static DapperAssessment ToDto(this Assessment entity, DapperAssessment passedDto = null)
        {
            var dto = passedDto ?? new DapperAssessment();
            dto.Id = entity.Id;
            dto.ProductId = entity.ProductId;
            dto.BusinessDate = entity.BusinessDate;
            dto.DateCreated = entity.DateCreated;
            dto.EnteredByUserId = entity.EnteredByUserId;
            dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
            dto.PriceHigh = entity.PriceHigh;
            dto.PriceLow = entity.PriceLow;
            dto.AssessmentStatus = DtoMappingAttribute.GetValueFromEnum(entity.AssessmentStatus);

            return dto;
        }

        public static Assessment ToEntity(this DapperAssessment passedDto)
        {
            AssessmentStatus assSt = AssessmentStatus.Final;

            switch (passedDto.AssessmentStatus)
            {
                case "n":
                    assSt = AssessmentStatus.None;
                    break;
                case "C":
                    assSt = AssessmentStatus.Corrected;
                    break;
                case "R":
                    assSt = AssessmentStatus.RunningVwa;
                    break;
                case "F":
                    assSt = AssessmentStatus.Final;
                    break;
            }

//            None = 0,
//
//        [DtoMapping("C")]
//        Corrected = 1,
//
//        [DtoMapping("R")]
//        RunningVwa = 2,
//
//        [DtoMapping("F")]
//        Final = 3

            var entity = new Assessment
            {
                Id = passedDto.Id,
                ProductId = passedDto.ProductId,
                BusinessDate = passedDto.BusinessDate,
                DateCreated = passedDto.DateCreated,
                EnteredByUserId = passedDto.EnteredByUserId,
                LastUpdated = passedDto.LastUpdated,
                LastUpdatedUserId = passedDto.LastUpdatedUserId,
                PriceHigh = passedDto.PriceHigh,
                PriceLow = passedDto.PriceLow,
                AssessmentStatus = assSt
            };

            return entity;
        }

        public static Assessment CopyAssessment(this Assessment passed)
        {
            return new Assessment
            {
                LastUpdated = passed.LastUpdated,
                LastUpdatedUserId = passed.LastUpdatedUserId,
                PriceHigh = passed.PriceHigh,
                PriceLow = passed.PriceLow,
                ProductId = passed.ProductId,
                Product = passed.Product,
                BusinessDate = passed.BusinessDate,
                DateCreated = passed.DateCreated,
                EnteredByUserId = passed.EnteredByUserId,
                AssessmentStatus = passed.AssessmentStatus
            };
        }
    }
}
