﻿using System;

namespace AomRepository.Entity
{
    public class DapperAssessment
    {
        public long Id { get; set; }

        public long EnteredByUserId { get; set; }

        public long LastUpdatedUserId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime LastUpdated { get; set; }

        public decimal? PriceHigh { get; set; }

        public decimal? PriceLow { get; set; }

        public long ProductId { get; set; }

        public DateTime BusinessDate { get; set; }

        public string TrendFromPreviousAssesment { get; set; }

        public string AssessmentStatus { get; set; }
    }
}
