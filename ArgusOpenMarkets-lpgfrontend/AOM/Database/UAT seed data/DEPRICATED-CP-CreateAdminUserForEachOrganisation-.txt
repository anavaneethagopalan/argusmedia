USE crm;
START TRANSACTION ;

-- CREATE ADMIN USERS
INSERT INTO `UserInfo`
	(`Organisation_Id_fk`
	,`Name`
	,`Email`
	,`Title`
	,`Telephone`
	,`Username`
	,`IsActive`
	,`IsDeleted`
	,`IsBlocked`
	,`ArgusCrmUsername`)
SELECT 
	o.id `Organisation_Id_fk`
	, CONCAT('Admin at ', o.LegalName) `Name`
	,'catchall@argus-labs.com'
	, null
	, null
	, CONCAT(o.ShortCode , 'Admin') `Username`
	, true  
	, false
	, false
	, null
FROM Organisation o
WHERE o.ShortCode IS NOT NULL
ON DUPLICATE KEY UPDATE IsBlocked=IsBlocked;

-- Ensure user credentials are in place
INSERT INTO `UserCredentials` (`UserInfo_Id_fk`,`Password`,`FirstTimeLogin`,`DefaultExpirationInMonths`,`DateCreated`,`Expiration`,`CredentialType`)
SELECT u.Id, 'NeedToSet', 0, 12, Now(), '2015-12-01 15:00:00.000000', 'P' FROM `UserInfo` u
ON DUPLICATE KEY UPDATE FirstTimeLogin=FirstTimeLogin;

-- Clean up
DELETE FROM `UserInfoRole` 
WHERE Role_Id_fk IN (SELECT Id FROM `OrganisationRole` WHERE Name in ('CounterParty Permissions Admin','CounterParty Permissions - View Only','Broker Permissions - Admin','Broker Permissions - View Only'));

-- DELETE FROM `OrganisationRole` WHERE Name in ('CounterParty Permissions Admin','CounterParty Permissions - View Only','Broker Permissions - Admin','Broker Permissions - View Only');

-- CREATE ORGANISATION ROLES FOR Counterpart and Broker Permissions
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId
FROM crm.Organisation o,
(
SELECT       'CounterParty Permissions Admin' RoleName, true CptyOnly
UNION SELECT 'CounterParty Permissions - View Only' RoleName, true CptyOnly
UNION SELECT 'Broker Permissions - Admin' RoleName, false CptyOnly
UNION SELECT 'Broker Permissions - View Only' RoleName, false CptyOnly) roles
WHERE o.OrganisationType='T' or (o.OrganisationType='B' and not roles.CptyOnly)
ON DUPLICATE KEY UPDATE Organisation_Id_fk=Organisation_Id_fk;


-- Put Admin users into new roles
INSERT INTO `UserInfoRole` (`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT u.Id, matrixPerms.RoleId FROM `UserInfo` u ,
(SELECT Organisation_Id_fk, id RoleId  FROM `OrganisationRole` WHERE Name like 'Counterparty Permissions%' or Name Like 'Broker Permissions%') matrixPerms
WHERE u.Organisation_Id_fk=matrixPerms.Organisation_Id_fk AND u.UserName like '%Admin'
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;


DELETE FROM SystemRolePrivilege WHERE Role_Id_fk IN (SELECT id FROM `OrganisationRole` WHERE Name like 'Counterparty Permissions%' or Name Like 'Broker Permissions%');

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='CPMatrix_View') FROM OrganisationRole WHERE Name in ('CounterParty Permissions Admin', 'CounterParty Permissions - View Only')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='CPMatrix_Update') FROM OrganisationRole WHERE Name in ( 'CounterParty Permissions Admin')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='BPMatrix_View') FROM OrganisationRole WHERE Name in ('Broker Permissions - Admin', 'Broker Permissions - View Only')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='BPMatrix_Update') FROM OrganisationRole WHERE Name in ('Broker Permissions - Admin')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;


COMMIT;