START TRANSACTION;

SET SQL_SAFE_UPDATES = 0;

-- NOTE Passswords CANNOT be created by this script - hashs using the User Id - will need to be set using the Admin Tool.  
-- USERS WERE CREATING
-- editoratm
-- trader1atm
-- trader2atm
-- brokeratm

DELETE FROM `crm`.`ProductRolePrivilege` WHERE Product_Id_fk = (SELECT Id FROM `aom`.Product where name = 'Argus Test Market');

DELETE FROM `crm`.`SubscribedProductPrivilege` WHERE Product_Id_fk = (SELECT Id FROM `aom`.Product where name = 'Argus Test Market');

DELETE FROM `crm`.`SubscribedProduct` WHERE Product_Id_fk IN (SELECT Id FROM `aom`.Product where name = 'Argus Test Market');

DELETE FROM `crm`.`UserToken` WHERE UserInfo_Id_fk IN ( SELECT Id FROM `crm`.`UserInfo` WHERE Telephone = '9988899'); 

DELETE FROM `crm`.`UserCredentials` WHERE UserInfo_Id_fk IN ( SELECT Id FROM `crm`.`UserInfo` WHERE Telephone = '9988899'); 

DELETE FROM `crm`.`UserInfoRole` WHERE UserInfo_Id_fk IN ( SELECT Id FROM `crm`.`UserInfo` WHERE Username IN ('editoratm', 'trader1atm', 'trader2atm', 'brokeratm'));

DELETE FROM `crm`.`UserInfo` Where Telephone = '9988899';

DELETE FROM `crm`.`OrganisationRole` WHERE Description = 'Argus Test Market Role';

DELETE FROM `crm`.`OrganisationRole` WHERE Organisation_Id_fk in (SELECT Id from `crm`.`Organisation` where LegalName LIKE '%Argus Test Market Admin Organisation%');

DELETE FROM `crm`.`Organisation` WHERE ShortCode = 'XXX';
DELETE FROM `crm`.`Organisation` WHERE ShortCode = 'XAX';
DELETE FROM `crm`.`Organisation` WHERE ShortCode = 'XTX';
DELETE FROM `crm`.`Organisation` WHERE ShortCode = 'XTT';
DELETE FROM `crm`.`Organisation` WHERE ShortCode = 'XBX';

DELETE FROM `aom`.`ProductTenor` WHERE Product_Id_fk IN ( SELECT id FROM `aom`.`Product` where name = 'Argus Test Market');

DELETE FROM `aom`.`Product` WHERE name = 'Argus Test Market';


INSERT INTO `aom`.`Product`
	(
	`Name`,
	`IsDeleted`,
	`IsInternal`,
	`LastUpdated`,
	`LastUpdated_User_Id`,
	`Commodity_Id_fk`,
	`Market_Id_fk`,
	`Contracttype_Id_fk`,
	`Location_Id_fk`,
	`Currency_Code_fk`,
	`VolumeUnit_code_fk`,
	`PriceUnit_Code_fk`,
	`OrderVolumeDefault`,
	`OrderVolumeMinimum`,
	`OrderVolumeMaximum`,
	`OrderVolumeIncrement`,
	`OrderVolumeDecimalPlaces`,
	`OrderVolumeSignificantFigures`,
	`OrderPriceMinimum`,
	`OrderPriceMaximum`,
	`OrderPriceIncrement`,
	`OrderPriceDecimalPlaces`,
	`OrderPriceSignificantFigures`,
	`BidAskStackNumberRows`,
	`Status`,
	`LastOpenDate`,
	`LastCloseDate`,
	`OpenTime`,
	`CloseTime`,
	`DateCreated`,
	`PurgeFrequency`,
	`PurgeDate`,
	`PurgeTimeOfDay`,
	`IgnoreAutomaticMarketStatusTriggers`,
	`AllowNegativePriceForExternalDeals`,
	`DisplayOptionalPriceDetail`,
	`HasAssessment`)
	VALUES
	(
	'Argus Test Market',
	0,
	1,
	'2015-01-01 10:00:00',
	-1,
	(SELECT Max(Id) FROM `aom`.Commodity),
	(SELECT Max(Id) FROM `aom`.Market),
	(SELECT Min(Id) FROM `aom`.ContractType),
	(SELECT Min(Id) FROM `aom`.DeliveryLocation),
	(SELECT Code FROM `aom`.Currency LIMIT 1),
	(SELECT Code FROM `aom`.Unit Limit 1),
	(SELECT Code FROM `aom`.Unit Limit 1),
	500,
	100,
	10000,
	1,
	2,
	8,
	100,
	10000,
	1,
	2,
	8,
	6,
	'O',
	'2015-01-01 10:00:00',
	'2015-01-01 10:00:00',
	'05:00:00',
	'08:00:00',
	'2015-01-01 10:00:00',
	7,
	'2015-01-01 10:00:00',
	'23:00:00',
	0,
	0,
	0,
	0);


INSERT INTO `aom`.`ProductTenor`
(`Name`,
`DateCreated`,
`LastUpdated`,
`LastUpdated_User_Id`,
`DisplayName`,
`DeliveryDateStart`,
`DeliveryDateEnd`,
`RollDate`,
`RollDateRule`,
`Product_Id_fk`,
`MinimumDeliveryRange`)
VALUES
(
'Prompt',
'2015-01-01 10:00:00',
'2015-01-01 10:00:00',
-1,
'Prompt',
'+1d',
'+20d',
'DAILY', 
'ACTUAL', 
(SELECT Max(Id) FROM `aom`.Product),
3);

-- ==============================================================================
-- ==============================================================================
-- CREATED EDITOR ORGANISATION AND EDITOR USER
-- CREATE AN ADMIN ORGANISATION
INSERT INTO `crm`.`Organisation`
(`ShortCode`,
`Name`,
`LegalName`,
`Address`,
`Email`,
`OrganisationType`,
`Description`,
`IsDeleted`,
`DateCreated`)
VALUES
(
'XAX',
'Argus Test Market Admin Organisation',
'Argus Test Market Admin Organisation',
'Argus Test Market House',
'catchall@argus-labs.com',
'A',
'Argus Test Market Admin Organisation',
0,
'2015-01-01 10:00:00');

-- CREATE EDITOR USER LINKED TO THE EDITOR ORGANISATION ABOVE XAX
-- NOTE: WE SHALL CLEAN UP THIS DATA USING THE Telephone Field - if 9988899 Then it will be deleted.
INSERT INTO `crm`.`UserInfo`
(`Id`,
`Organisation_Id_fk`,
`Email`,
`Name`,
`Title`,
`Telephone`,
`Username`,
`IsActive`,
`IsDeleted`,
`IsBlocked`,
`ArgusCrmUsername`)
SELECT
(SELECT CAST(CONV(SUBSTRING(CAST(SHA('editoratm') AS CHAR), 1, 16), 16, 10 ) /9999999999999 as unsigned)),
o.Id ,
'catchall@argus-labs.com',
'Editor Argus Test Market',
'Mr',
'9988899',
'editoratm',
1,
0,
0,
'aomtest@argusmedia.com'
FROM Organisation o
WHERE o.ShortCode = 'XAX';

-- Organisation Role
INSERT INTO `crm`.`OrganisationRole`
(`Name`,
`Description`,
`Organisation_Id_fk`)
VALUES
('AdminArgusTestMarketRole',
'Argus Test Market Role',
(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XAX'));

commit;
-- USER INFO ROLE
INSERT INTO `crm`.`UserInfoRole`
(`UserInfo_Id_fk`,
`Role_Id_fk`)
VALUES(
(SELECT id from `crm`.`UserInfo` WHERE username = 'editoratm' ), 
(SELECT Id FROM `crm`.`OrganisationRole` WHERE name = 'AdminArgusTestMarketRole')
);


-- Create User Credentials for this user.
INSERT INTO `crm`.`UserCredentials`
(`UserInfo_Id_fk`,
`Password`,
`FirstTimeLogin`,
`DefaultExpirationInMonths`,
`DateCreated`,
`Expiration`,
`CredentialType`)
VALUES
(
(SELECT id from `crm`.`UserInfo` WHERE username = 'editoratm' ), 
'NeedToSet',
false,
12,
'2015-01-01 10:00:00',
'2020-01-01 10:00:00',
'P');


#UPDATE THE Password



#subscribe company to all products
INSERT INTO `crm`.`SubscribedProduct`
VALUES (
(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XAX')
); 


# SubscribedProductPrivilege 
INSERT INTO `crm`.`SubscribedProductPrivilege`
(Product_Id_fk, 
Organisation_Id_fk, 
ProductPrivilege_id_fk, 
CreatedBy, 
DateCreated)
SELECT
	(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
	(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XAX'),
	pp.Id, 
	'AOM Admin',
	UTC_TIMESTAMP()
FROM
	`crm`.`ProductPrivilege` pp;


# Product Role Privilege
INSERT INTO `crm`.`ProductRolePrivilege`
(
`Organisation_Id_fk`,
`Role_Id_fk`,
`Product_Id_fk`,
`ProductPrivilege_Id_fk`
)
SELECT
	(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XAX'),
	(SELECT Id FROM `crm`.`OrganisationRole` WHERE name = 'AdminArgusTestMarketRole'), 
	(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
	PP.Id
FROM 
	`crm`.`ProductPrivilege` PP;



-- ==============================================================================
-- CREATED TRADER ORGANISATION #1 AND TRADER #1 LINKED TO THIS ORGANISATION
-- CREATE A TRADER ORGANISATION
INSERT INTO `crm`.`Organisation`
(`ShortCode`,
`Name`,
`LegalName`,
`Address`,
`Email`,
`OrganisationType`,
`Description`,
`IsDeleted`,
`DateCreated`)
VALUES
(
'XTX',
'Argus Test Market Trader1 Organisation',
'Argus Test Market Trader1 Organisation',
'Argus Test Market House',
'trader.one@argusmedia.com',
'T',
'Argus Test Market Trader 1 Organisation',
0,
'2015-01-01 10:00:00');


-- CREATE TRADER #1 USER LINKED TO THE TRADER 1 ORGANISATION ABOVE XTX
-- NOTE: WE SHALL CLEAN UP THIS DATA USING THE Telephone Field - if 9988899 Then it will be deleted.
INSERT INTO `crm`.`UserInfo`
(`Id`,
`Organisation_Id_fk`,
`Email`,
`Name`,
`Title`,
`Telephone`,
`Username`,
`IsActive`,
`IsDeleted`,
`IsBlocked`,
`ArgusCrmUsername`)
SELECT 
(SELECT CAST(CONV(SUBSTRING(CAST(SHA('trader1atm') AS CHAR), 1, 16), 16, 10 ) /9999999999999 as unsigned)),
o.id,
'catchall@argus-labs.com',
'Trader1 Argus Test Market',
'Mr',
'9988899',
'trader1atm',
1,
0,
0,
'aomtest@argusmedia.com'
FROM Organisation o
WHERE o.ShortCode = 'XTX';

-- Organisation Role
INSERT INTO `crm`.`OrganisationRole`
(`Name`,
`Description`,
`Organisation_Id_fk`)
VALUES
('Trader1ArgusTestMarketRole',
'Argus Test Market Role',
(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XTX'));

commit;

-- USER INFO ROLE
INSERT INTO `crm`.`UserInfoRole`
(`UserInfo_Id_fk`,
`Role_Id_fk`)
VALUES(
(SELECT id from `crm`.`UserInfo` WHERE username = 'trader1atm' ), 
(SELECT Id FROM `crm`.`OrganisationRole` WHERE name = 'Trader1ArgusTestMarketRole')
);


-- Create User Credentials for this user.
INSERT INTO `crm`.`UserCredentials`
(`UserInfo_Id_fk`,
`Password`,
`FirstTimeLogin`,
`DefaultExpirationInMonths`,
`DateCreated`,
`Expiration`,
`CredentialType`)
VALUES
(
(SELECT id from `crm`.`UserInfo` WHERE username = 'trader1atm' ), 
'NeedToSet',
false,
12,
'2015-01-01 10:00:00',
'2020-01-01 10:00:00',
'P');


#subscribe company to all products
INSERT INTO `crm`.`SubscribedProduct`
VALUES (
(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XTX')
); 

# SubscribedProductPrivilege 
INSERT INTO `crm`.`SubscribedProductPrivilege`
(Product_Id_fk, 
Organisation_Id_fk, 
ProductPrivilege_id_fk, 
CreatedBy, 
DateCreated)
SELECT
	(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
	(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XTX'),
	pp.Id, 
	'AOM Admin',
	UTC_TIMESTAMP()
FROM
	`crm`.`ProductPrivilege` pp;

# Product Role Privilege
INSERT INTO `crm`.`ProductRolePrivilege`
(
`Organisation_Id_fk`,
`Role_Id_fk`,
`Product_Id_fk`,
`ProductPrivilege_Id_fk`
)
SELECT
	(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XTX'),
	(SELECT Id FROM `crm`.`OrganisationRole` WHERE name = 'Trader1ArgusTestMarketRole'), 
	(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
	PP.Id
FROM 
	`crm`.`ProductPrivilege` PP;

-- ==============================================================================
-- CREATED TRADER ORGANISATION #2 AND TRADER #2 LINKED TO THIS ORGANISATION
-- CREATE A TRADER ORGANISATION #2
INSERT INTO `crm`.`Organisation`
(`ShortCode`,
`Name`,
`LegalName`,
`Address`,
`Email`,
`OrganisationType`,
`Description`,
`IsDeleted`,
`DateCreated`)
VALUES
(
'XTT',
'Argus Test Market Trader2 Organisation',
'Argus Test Market Trader2 Organisation',
'Argus Test Market House 2',
'trader.two@argusmedia.com',
'T',
'Argus Test Market Trader 1 Organisation',
0,
'2015-01-01 10:00:00');


-- CREATE TRADER #2 USER LINKED TO THE TRADER 1 ORGANISATION ABOVE XTX
-- NOTE: WE SHALL CLEAN UP THIS DATA USING THE Telephone Field - if 9988899 Then it will be deleted.
INSERT INTO `crm`.`UserInfo`
(
`Id`,
`Organisation_Id_fk`,
`Email`,
`Name`,
`Title`,
`Telephone`,
`Username`,
`IsActive`,
`IsDeleted`,
`IsBlocked`,
`ArgusCrmUsername`)
SELECT
(SELECT CAST(CONV(SUBSTRING(CAST(SHA('trader2atm') AS CHAR), 1, 16), 16, 10 ) /9999999999999 as unsigned)),
o.id,
'catchall@argus-labs.com',
'Trader2 Argus Test Market',
'Mr',
'9988899',
'trader2atm',
1,
0,
0,
'aomtest@argusmedia.com'
FROM Organisation o
WHERE o.ShortCode = 'XTT' ;

-- Organisation Role
INSERT INTO `crm`.`OrganisationRole`
(`Name`,
`Description`,
`Organisation_Id_fk`)
VALUES
('Trader2ArgusTestMarketRole',
'Argus Test Market Role',
(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XTT'));


commit;

-- USER INFO ROLE
INSERT INTO `crm`.`UserInfoRole`
(`UserInfo_Id_fk`,
`Role_Id_fk`)
VALUES(
(SELECT id from `crm`.`UserInfo` WHERE username = 'trader2atm' ), 
(SELECT Id FROM `crm`.`OrganisationRole` WHERE name = 'Trader2ArgusTestMarketRole')
);

-- Create User Credentials for this user.
INSERT INTO `crm`.`UserCredentials`
(`UserInfo_Id_fk`,
`Password`,
`FirstTimeLogin`,
`DefaultExpirationInMonths`,
`DateCreated`,
`Expiration`,
`CredentialType`)
VALUES
(
(SELECT id from `crm`.`UserInfo` WHERE username = 'trader2atm' ), 
'NeedToSet',
false,
12,
'2015-01-01 10:00:00',
'2020-01-01 10:00:00',
'P');

#subscribe company to all products
INSERT INTO `crm`.`SubscribedProduct`
VALUES (
(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XTT')
); 

# SubscribedProductPrivilege 
INSERT INTO `crm`.`SubscribedProductPrivilege`
(Product_Id_fk, 
Organisation_Id_fk, 
ProductPrivilege_id_fk, 
CreatedBy, 
DateCreated)
SELECT
	(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
	(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XTT'),
	pp.Id, 
	'AOM Admin',
	UTC_TIMESTAMP()
FROM
	`crm`.`ProductPrivilege` pp;

# Product Role Privilege
INSERT INTO `crm`.`ProductRolePrivilege`
(
`Organisation_Id_fk`,
`Role_Id_fk`,
`Product_Id_fk`,
`ProductPrivilege_Id_fk`
)
SELECT
	(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XTT'),
	(SELECT Id FROM `crm`.`OrganisationRole` WHERE name = 'Trader2ArgusTestMarketRole'), 
	(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
	PP.Id
FROM 
	`crm`.`ProductPrivilege` PP;

-- ==============================================================================
-- CREATED BROKER ORGANISATION AND BROKER USER LINKED TO THIS ORGANISATION
-- CREATE A TRADER ORGANISATION #2
INSERT INTO `crm`.`Organisation`
(`ShortCode`,
`Name`,
`LegalName`,
`Address`,
`Email`,
`OrganisationType`,
`Description`,
`IsDeleted`,
`DateCreated`)
VALUES
(
'XBX',
'Argus Test Market Broker Organisation',
'Argus Test Market Broker Organisation',
'Argus Test Market House 2',
'broker@argusmedia.com',
'B',
'Argus Test Market Broker Organisation',
0,
'2015-01-01 10:00:00');

-- CREATE TRADER #2 USER LINKED TO THE TRADER 1 ORGANISATION ABOVE XTX
-- NOTE: WE SHALL CLEAN UP THIS DATA USING THE Telephone Field - if 9988899 Then it will be deleted.
INSERT INTO `crm`.`UserInfo`
(
`Id`,
`Organisation_Id_fk`,
`Email`,
`Name`,
`Title`,
`Telephone`,
`Username`,
`IsActive`,
`IsDeleted`,
`IsBlocked`,
`ArgusCrmUsername`)
SELECT

(SELECT CAST(CONV(SUBSTRING(CAST(SHA('brokeratm') AS CHAR), 1, 16), 16, 10 ) /9999999999999 as unsigned)),
o.id,
'catchall@argus-labs.com',
'Broker Argus Test Market',
'Mr',
'9988899',
'brokeratm',
1,
0,
0,
'aomtest@argusmedia.com'
FROM Organisation o
WHERE o.ShortCode = 'XBX';

-- Organisation Role
INSERT INTO `crm`.`OrganisationRole`
(`Name`,
`Description`,
`Organisation_Id_fk`)
VALUES
('BrokerArgusTestMarketRole',
'Argus Test Market Role',
(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XBX'));

commit;

-- USER INFO ROLE
INSERT INTO `crm`.`UserInfoRole`
(`UserInfo_Id_fk`,
`Role_Id_fk`)
VALUES(
(SELECT id from `crm`.`UserInfo` WHERE username = 'brokeratm' ), 
(SELECT Id FROM `crm`.`OrganisationRole` WHERE name = 'BrokerArgusTestMarketRole')
);


-- Create User Credentials for this user.
INSERT INTO `crm`.`UserCredentials`
(`UserInfo_Id_fk`,
`Password`,
`FirstTimeLogin`,
`DefaultExpirationInMonths`,
`DateCreated`,
`Expiration`,
`CredentialType`)
VALUES
(
(SELECT id from `crm`.`UserInfo` WHERE username = 'brokeratm' ), 
'NeedToSet',
false,
12,
'2015-01-01 10:00:00',
'2020-01-01 10:00:00',
'P');

#subscribe company to all products
INSERT INTO `crm`.`SubscribedProduct`
VALUES (
(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XBX')
); 

# SubscribedProductPrivilege 
INSERT INTO `crm`.`SubscribedProductPrivilege`
(Product_Id_fk, 
Organisation_Id_fk, 
ProductPrivilege_id_fk, 
CreatedBy, 
DateCreated)
SELECT
	(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
	(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XBX'),
	pp.Id, 
	'AOM Admin',
	UTC_TIMESTAMP()
FROM
	`crm`.`ProductPrivilege` pp;


# Product Role Privilege
INSERT INTO `crm`.`ProductRolePrivilege`
(
`Organisation_Id_fk`,
`Role_Id_fk`,
`Product_Id_fk`,
`ProductPrivilege_Id_fk`
)
SELECT
	(SELECT Id FROM `crm`.`Organisation` WHERE ShortCode = 'XBX'),
	(SELECT Id FROM `crm`.`OrganisationRole` WHERE name = 'BrokerArgusTestMarketRole'), 
	(SELECT Id FROM `aom`.`Product` where name = 'Argus Test Market'),
	PP.Id
FROM 
	`crm`.`ProductPrivilege` PP;
	

###########################   SET PASSWORD FOR THE NEW USERS    ############################

#set passwords for Test Product users
UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '3PKvieUDA/GEkSomkIZ5zhakJ7MjM/VYXwyv4IleCg0=' where ui.UserName = 'editoratm';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'avN8JMQ4OQfq/S+rwCkQ3WCx1bsFlikCxTbZXk+TmiA=' where ui.UserName = 'trader1atm';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'cfgJgwBr7HoZPYGeXGZpKR0BC1/YGlrBD++bkxi8o38=' where ui.UserName = 'trader2atm';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '/AI7QE4pkhS91I1VIMyUytI3ifAhREsziQytADWkxzI=' where ui.UserName = 'brokeratm';



# ==========================================================================================
# ==========================================================================================
SET SQL_SAFE_UPDATES = 1;

COMMIT;