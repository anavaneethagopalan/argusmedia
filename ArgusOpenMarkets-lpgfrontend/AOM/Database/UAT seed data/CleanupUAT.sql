SET SQL_SAFE_UPDATES = 0;
use crm;

DELETE FROM `crm`.`UserToken` WHERE UserInfo_Id_fk IN (SELECT Id FROM `crm`.`UserInfo` WHERE Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'))); 

DELETE FROM `crm`.`UserInfoRole` WHERE UserInfo_Id_fk IN (SELECT Id FROM `crm`.`UserInfo` WHERE Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1')));

DELETE FROM `crm`.`UserCredentials` WHERE UserInfo_Id_fk IN (SELECT Id FROM `crm`.`UserInfo` WHERE Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1')));

DELETE FROM `crm`.`UserInfo` Where Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

#DELETE WHI
DELETE FROM `crm`.`CanTradeWith` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`SystemEventNotifications`  where OrganisationId_fk in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`ProductRolePrivilege` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`SubscribedProductPrivilege`  where Organisation_Id_fk in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`SubscribedProduct`  where Organisation_Id_fk in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`SystemRolePrivilege` where Role_Id_fk in (SELECT Id from`crm`.`OrganisationRole` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1')));

DELETE FROM `crm`.`ProductRolePrivilege` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`OrganisationRole` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`CounterpartyPermission` where OurOrganisation IN (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1')) or TheirOrganisation in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`BrokerPermission` where OurOrganisation IN (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1')) or TheirOrganisation in (SELECT id from Organisation where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1'));

DELETE FROM `crm`.`Organisation` Where ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1');


DELETE  FROM CounterpartyPermission where Id in (
SELECT Id FROM (
SELECT bp.Id Id, ProductId, OurOrganisation, org.ShortCode, BuyOrSell, TheirOrganisation, (Select ShortCode from Organisation org where org.Id = TheirOrganisation) TheirOrgCode, AllowOrDeny FROM crm.CounterpartyPermission bp join Organisation o on bp.OurOrganisation = o.Id 
join Organisation org on org.Id = bp.OurOrganisation
join Organisation org1 on org1.Id = bp.TheirOrganisation
where  org.ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1', null) or org1.ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1', null)
) tmpTable);

DELETE  FROM BrokerPermission where Id in (
SELECT Id FROM (
SELECT bp.Id Id, ProductId, OurOrganisation, org.ShortCode, BuyOrSell, TheirOrganisation, (Select ShortCode from Organisation org where org.Id = TheirOrganisation) TheirOrgCode, AllowOrDeny FROM crm.BrokerPermission bp join Organisation o on bp.OurOrganisation = o.Id 
join Organisation org on org.Id = bp.OurOrganisation
join Organisation org1 on org1.Id = bp.TheirOrganisation
where  org.ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1', null) or org1.ShortCode in ('TR1', 'TR2', 'BR1','BR2', 'GR1', null)
) tmpTable);


########################################################################################################################
#clean up CP and Broker Permissions
########################################################################################################################
START TRANSACTION;

INSERT INTO BrokerPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny)
	SELECT 
		Now() LastUpdated, -1 LastUpdatedUserId
			, o1.SubscribedProductId ProductId, o1.OrganisationId OurOrganisation
			, bs.BuySell BuyOrSell
			, o2.OrganisationId TheirOrganisation,  'A' AllowOrDeny
	FROM
		(SELECT 'B' BuySell UNION SELECT 'S') bs, 
		(SELECT o.Id OrganisationId, sp.Product_Id_fk SubscribedProductId, o.OrganisationType
		FROM   Organisation o 
		INNER JOIN SubscribedProduct sp ON sp.Organisation_Id_fk=o.Id where o.OrganisationType <> 'A') o1,

		(SELECT o.Id OrganisationId, sp.Product_Id_fk SubscribedProductId, o.OrganisationType
		FROM   Organisation o 
		INNER JOIN SubscribedProduct sp ON sp.Organisation_Id_fk=o.Id
		where o.OrganisationType <> 'A'
		) o2
	WHERE o1.SubscribedProductId=o2.SubscribedProductId and o1.OrganisationId<>o2.OrganisationId and o1.OrganisationType<>o2.OrganisationType
			
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;

INSERT INTO CounterpartyPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny)
	SELECT 
		Now() LastUpdated, -1 LastUpdatedUserId
			, o1.SubscribedProductId ProductId, o1.OrganisationId OurOrganisation
			, bs.BuySell BuyOrSell
			, o2.OrganisationId TheirOrganisation,  'A' AllowOrDeny
	FROM
		(SELECT 'B' BuySell UNION SELECT 'S') bs, -- cross product of B & S
		(SELECT o.Id OrganisationId, sp.Product_Id_fk SubscribedProductId
		FROM   Organisation o 
		INNER JOIN SubscribedProduct sp ON sp.Organisation_Id_fk=o.Id
		WHERE  o.OrganisationType='T' or o.OrganisationType='' ) o1,

		(SELECT o.Id OrganisationId, sp.Product_Id_fk SubscribedProductId
		FROM   Organisation o 
		INNER JOIN SubscribedProduct sp ON sp.Organisation_Id_fk=o.Id
		WHERE  o.OrganisationType='T' ) o2

	WHERE o1.SubscribedProductId=o2.SubscribedProductId and o1.OrganisationId<>o2.OrganisationId
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;

COMMIT;

UPDATE CounterpartyPermission SET AllowOrDeny = 'A';
UPDATE BrokerPermission SET AllowOrDeny = 'A';

UPDATE CounterpartyPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ALP')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'DLT');

UPDATE CounterpartyPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'DLT')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ALP');


#Set up Assymet for Echo
#Product 1
UPDATE CounterpartyPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'DLT') and BuyOrSell='B' and ProductId = 1;
UPDATE CounterpartyPermission SET  AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'DLT')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ECH') and BuyOrSell='S' and ProductId= 1;

UPDATE CounterpartyPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'BRA') and BuyOrSell='S' and ProductId = 1;
UPDATE CounterpartyPermission SET BuyOrSell='B', AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'BRA')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ECH') and BuyOrSell='B' and ProductId = 1;


#Product 2
UPDATE CounterpartyPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'BRA') and BuyOrSell='B' and ProductId = 2;
UPDATE CounterpartyPermission SET  AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'BRA')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and BuyOrSell='S' and ProductId = 2;


UPDATE CounterpartyPermission SET  AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'DLT') and BuyOrSell='S' and ProductId = 2;
UPDATE CounterpartyPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'DLT')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ECH') and BuyOrSell='B' and ProductId = 2;


#Bs cannot trade with whi
UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'BRA')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'WHI') ;
UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'WHI')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'BRA') ;

#DLT cannot trade with ZUL
UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'DLT')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ZUL') ;
UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ZUL')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'DLT') ;

#BrokerPermissions
#Set up Assymet for Echo and ZUL and WHI
#Product 1
UPDATE BrokerPermission SET AllowOrDeny = 'D' 
where OurOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ZUL') and ProductId = 1 and BuyOrSell = 'B';

UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ZUL')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ECH') and ProductId = 1 and BuyOrSell = 'B';

UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'WHI') and ProductId = 1 and BuyOrSell = 'S';
UPDATE BrokerPermission SET  AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'WHI')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ECH') and ProductId = 1 and BuyOrSell = 'S';

#Product 2
UPDATE BrokerPermission SET  AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'WHI') and ProductId = 2 and BuyOrSell = 'B';
UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'WHI')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ECH') and ProductId = 2 and BuyOrSell = 'B';


UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ECH')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ZUL') and ProductId = 2 and BuyOrSell = 'S';
UPDATE BrokerPermission SET AllowOrDeny = 'D'
where OurOrganisation = (select Id from Organisation where ShortCode = 'ZUL')  and TheirOrganisation = (select Id from Organisation where ShortCode = 'ECH') and ProductId = 2 and BuyOrSell = 'S';
