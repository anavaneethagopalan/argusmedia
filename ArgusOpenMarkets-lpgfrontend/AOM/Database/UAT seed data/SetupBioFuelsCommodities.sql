-- Data has been supplied by Denis Komov
-- COMMODITY CLASS - ENERGY - Should already exist, but defensive sql.  
INSERT INTO `CommodityClass` (`LastUpdatedUser_Id`, `EnteredByUser_Id`, `LastUpdated`, `DateCreated`, `Name`) 
	SELECT -1, 
		-1, 
		'2015-09-01', 
		'2015-09-01', 
		'Energy' 
	FROM DUAL
WHERE NOT EXISTS 
  (SELECT Name FROM `CommodityClass` WHERE Name='Energy');

  
-- Commodity Group - Biofuels
INSERT INTO `CommodityGroup` (`Name`, `CommodityClass_Id_fk`)
	SELECT 
		'Biofuels', 
		(SELECT Id FROM CommodityClass where Name = 'Energy')
	FROM DUAL
WHERE NOT EXISTS 
	(SELECT Name FROM `CommodityGroup` WHERE Name = 'Biofuels');

-- Commodity Type
INSERT INTO `CommodityType` (`Name`, `CommodityGroup_Id_fk`)
	SELECT
		'Biodiesel', 
		(SELECT Id FROM `CommodityGroup` Where Name = 'Biofuels')
	FROM DUAL
WHERE NOT EXISTS
	(SELECT Id FROM CommodityType Where Name = 'Biodiesel');

-- Commodity
INSERT INTO `Commodity` (`Name`, `CommodityType_Id_fk`, `DateCreated`, `LastUpdatedUser_Id`, `EnteredByUser_Id`, `LastUpdated`)
	SELECT
		'Biofuels', 
		( SELECT Id FROM CommodityType Where Name = 'Biodiesel' ), 
		'2015-09-01', 
		-1, 
		-1, 
		'2015-09-01'
	FROM DUAL 
WHERE NOT EXISTS 
	( SELECT Id FROM Commodity where Name = 'Biofuels' );

-- Commodity Content Streams
INSERT INTO `CommodityContentStreams` (`CommodityId`, `ContentStreamId`)
	SELECT
		( SELECT Id FROM Commodity Where Name = 'Biofuels' ) , 
		95074	
	FROM DUAL 
WHERE NOT EXISTS
	( SELECT CommodityId FROM CommodityContentStreams Where ContentStreamId = 95074 );