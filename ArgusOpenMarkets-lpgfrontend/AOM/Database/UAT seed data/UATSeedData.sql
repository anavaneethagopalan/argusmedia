Delete from CanTradeWith;
Delete FROM crm.ProductRolePrivilege;
Delete from SubscribedProductPrivilege;
Delete from SubscribedProduct;
Delete from UserInfoRole;
Delete from SystemRolePrivilege;
Delete from OrganisationRole;
Delete from UserToken;
Delete from UserInfo;
Delete from Organisation;
Delete from UserCredentials;


LOCK TABLES `Organisation` WRITE;
/*!40000 ALTER TABLE `Organisation` DISABLE KEYS */;

INSERT INTO `crm`.`Organisation`
(`Id`,
`ShortCode`,
`Name`,
`LegalName`,
`Address`,
`Email`,
`OrganisationType`,
`Description`,
`IsDeleted`,
`DateCreated`)
VALUES
('8', 'TRA', 'TraderCo A', 'TraderCo A Ltd', '100 City Rd, London, EC1 7AO, United Kingdom', NULL, 'T', 'TraderCo A is a distilate trading company based in London', 0, '2014-10-16 13:41:02.763000')
,('30', 'TRB', 'TraderCo B', 'TraderCo B Ltd', '55 Cannon St London, EC2 7SB', NULL, 'T', 'Mid Distilate Trader', 0, '2014-10-20 09:28:40.239000')
,('47', 'TRC', 'TraderCo C', 'TraderCo C Ltd', '161 Farringdon Rd', NULL, 'T', 'Distillate Trading Company C', 0, '2014-10-20 10:39:10.761000')
,('51', 'BC1', 'BrokerCo 1', 'BrokerCo1 GmBh', 'Volksstrasse, Wien', NULL, 'B', 'Dilstilate Broker - Based in Austria', 0, '2014-10-20 15:14:12.868000')
,('52', 'BC2', 'BrokerCo 2', 'BrokerCo 2  Ltd', '100 Strand London', NULL, 'B', 'Naphtha Broker', 0, '2014-10-20 15:33:47.230000')
,('53','ARG','Argus Media','Argus Media Ltd','Argus House, 175 St John Street, London EC1V 4LW','AOM@ArgusMedia.com','A','Argus Media Company','\0','2014-01-01 00:00:00.000000');
/*!40000 ALTER TABLE `Organisation` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `OrganisationRole` WRITE;
/*!40000 ALTER TABLE `OrganisationRole` DISABLE KEYS */;
INSERT INTO `OrganisationRole` VALUES (12,'Naphtha Outright and Diff Trader','Naphtha Outright and Diff Trader',8);
INSERT INTO `OrganisationRole` VALUES (13,'Naphtha Outright and Dif Trader','Naphtha Outright and Dif Trader',30);
INSERT INTO `OrganisationRole` VALUES (14,'Naphtha Outright and Diff Trader','Naphtha Outright and Diff Trader',47);
INSERT INTO `OrganisationRole` VALUES (15,'Naphtha Outright','Naphtha Outright',8);
INSERT INTO `OrganisationRole` VALUES (16,'Naphtha Diff','Naphtha Diff',8);
INSERT INTO `OrganisationRole` VALUES (17,'Naphtha Broker Diff and Outright','Naphtha Broker Diff and Outright',51);
INSERT INTO `OrganisationRole` VALUES (18,'Naphtha Broker Outright','Naphtha Broker Outright',51);
INSERT INTO `OrganisationRole` VALUES (19,'Naphtha Broker Diff','Naphtha Broker Diff',51);
INSERT INTO `OrganisationRole` VALUES (20,'Naphtha Broker Diff and Outright','Naphtha Broker Diff and Outright',52);
INSERT INTO `OrganisationRole` VALUES (21,'Naphtha Broker Diff','Naphtha Broker Diff',52);
INSERT INTO `OrganisationRole` VALUES (22,'Naphtha Broker Outright','Naphtha Broker Outright',52);
INSERT INTO `OrganisationRole` VALUES (23,'Argus Editor - Naphtha Outright and Diff','Argus Editor - Naphtha Outright and Diff',53);
INSERT INTO `OrganisationRole` VALUES (24,'Argus Editor - Naphtha Outright','Argus Editor - Naphtha Outright',53);
INSERT INTO `OrganisationRole` VALUES (25,'Argus Editor - Naphtha Diff','Argus Editor - Naphtha Diff',53);
INSERT INTO `OrganisationRole` VALUES (26,'Naphtha Diff','Naphtha Diff',30);
INSERT INTO `OrganisationRole` VALUES (27,'Naphtha Diff Trader','Naphtha Diff Trader',47);
INSERT INTO `OrganisationRole` VALUES (28,'Admin Tool','Admin Tool',53);
INSERT INTO `OrganisationRole` VALUES (1,'SysAdmin','SysAdmin',53);
/*!40000 ALTER TABLE `OrganisationRole` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `UserInfo` WRITE;
/*!40000 ALTER TABLE `UserInfo` DISABLE KEYS */;
INSERT INTO `UserInfo` VALUES (-1,53,'ProcessorUser','Argus Processor User','Mr','+44 1234567890','pu1','','1234567890ABCDEF','\0','\0',NULL);
INSERT INTO `UserInfo` VALUES (6496,8,'B.Jones@tradercoa.com','Bob Jones',NULL,NULL,'TraderA1','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6497,30,'kevin.gutridge@argusmedia.com','John Smith',NULL,NULL,'TraderB1','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6498,47,'kevin.gutridge@argusmedia.com','Harry Higgs',NULL,NULL,'TraderC1','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6500,8,'J.Brown@Ta2.com','James Brown',NULL,NULL,'TraderA2','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6501,51,'Helmut.Schnider@BrokerCo1.com','Helmut Schnider',NULL,NULL,'Broker1A','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6502,52,'Simon.Templar@Broker2A.co.uk','Simon Templar',NULL,NULL,'Broker2A','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6503,53,'brian.short@argusmedia.com','Brian Short',NULL,NULL,'Editor1','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6504,30,'Terry.Walsh@TB2.co.uk','Terry Walsh',NULL,NULL,'TraderB2','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6505,47,'G.McGee@TC.co.uk','Giles McGee',NULL,NULL,'TraderC2','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6506,51,'gert.pessendorfer@BC1.com','Gert Pessendorfer',NULL,NULL,'Broker1B','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6507,52,'Brian.Castle@Broker1A.co.uk','Brian Castle',NULL,NULL,'Broker2B','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6508,53,'tim.england@argusmedia.com','Tim England',NULL,NULL,'Editor2','',NULL,'\0','\0','aomtest@argusmedia.com');
INSERT INTO `UserInfo` VALUES (6509,53,'james.administrator@argusmedia.com','James Administrator',NULL,NULL,'Admin1','',NULL,'\0','\0','aomtest@argusmedia.com');
/*!40000 ALTER TABLE `UserInfo` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `UserCredentials` WRITE;
/*!40000 ALTER TABLE `UserCredentials` DISABLE KEYS */;
INSERT INTO `UserCredentials` VALUES (1,-1,'NotSaved',0,12,'2014-10-16 13:57:13.443000','2015-10-16 15:53:06.604000');
INSERT INTO `UserCredentials` VALUES (14,6496,'Fw3NPRqkEgn89KvyD1our3TLDu28QT5+jtPrFhfUPBs=',0,12,'2014-10-16 13:57:13.443000','2015-10-16 15:53:06.604000');
INSERT INTO `UserCredentials` VALUES (15,6497,'AkX2ddrwLForp2RE1f14QtcquxFChmX76tPC5LvBM/8=',0,12,'2014-10-20 09:46:13.676000','2015-10-20 09:48:57.201000');
INSERT INTO `UserCredentials` VALUES (16,6498,'aRKKtQXrAWOGjrkRf1LvO1GWGZGRXdr6e3VnTmQ4Yg0=',0,12,'2014-10-20 10:44:14.310000','2015-10-20 11:05:49.031000');
INSERT INTO `UserCredentials` VALUES (18,6500,'6lKtOm4ZeQQjs6UMT8FkD1FcGDS5auS9yKfy7jTc6/E=',0,12,'2014-10-20 14:44:15.766000','2015-10-20 15:52:02.383000');
INSERT INTO `UserCredentials` VALUES (19,6501,'/EP2uBRbG7JJB8RsV0W8jrg5MjXjK74im1V3CHjyV7Y=',0,12,'2014-10-20 15:19:01.057000','2015-10-20 15:28:49.465000');
INSERT INTO `UserCredentials` VALUES (20,6502,'stk7nE0DHthlIl4vmHaVLkTEuFfEKfeaU4Eb7dOF8iI=',0,12,'2014-10-20 15:36:51.406000','2015-10-20 15:45:24.112000');
INSERT INTO `UserCredentials` VALUES (21,6503,'1M+GQUvsArGnG2LBqUM6rsU74Bt//s+ByqHn5bwmaEw=',0,12,'2014-10-20 16:21:57.137000','2015-10-20 16:28:54.105000');
INSERT INTO `UserCredentials` VALUES (22,6504,'GCItDO+JRod3r43feyujBcUByaNln/k7eKZXHWsT2i8=',0,12,'2014-10-21 10:25:43.128000','2015-10-21 10:28:54.208000');
INSERT INTO `UserCredentials` VALUES (23,6505,'E4n3J/mI/pORQeildI0Io/EVNG9Bye2eNECK+5aMLuY=',1,12,'2014-10-21 10:37:36.770000','2015-10-23 10:37:37.162000');
INSERT INTO `UserCredentials` VALUES (24,6506,'F6o6SqoxdjsR8mBiiv3O2yYh/ATQZgSR+s2mTHjnElc=',1,12,'2014-10-21 10:44:06.040000','2015-10-23 10:44:06.438000');
INSERT INTO `UserCredentials` VALUES (25,6507,'AspA9yKbaAzEXiZ/OXVDezbDGTjJZnbZIkO14sQFMfw=',1,12,'2014-10-21 10:48:07.326000','2015-10-23 10:48:07.750000');
INSERT INTO `UserCredentials` VALUES (26,6508,'sf+5DrUOqpQjJOLfak0r1d25YpU+/72nx9cCNp9eWTo=',1,12,'2014-10-21 10:51:09.607000','2015-10-23 10:51:10.050000');
INSERT INTO `UserCredentials` VALUES (27,6509,'OgntAfPF5T8UFCft5QPOdDAnwdKcvrLSobYbcxLwAiM=',0,12,'2014-10-21 10:59:21.313000','2015-10-23 10:59:21.772000');
/*!40000 ALTER TABLE `UserCredentials` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `UserInfoRole` WRITE;
/*!40000 ALTER TABLE `UserInfoRole` DISABLE KEYS */;
INSERT INTO `UserInfoRole` VALUES (-1,1);
INSERT INTO `UserInfoRole` VALUES (6496,12);
INSERT INTO `UserInfoRole` VALUES (6497,13);
INSERT INTO `UserInfoRole` VALUES (6498,14);
INSERT INTO `UserInfoRole` VALUES (6500,15);
INSERT INTO `UserInfoRole` VALUES (6501,17);
INSERT INTO `UserInfoRole` VALUES (6506,18);
INSERT INTO `UserInfoRole` VALUES (6502,20);
INSERT INTO `UserInfoRole` VALUES (6507,21);
INSERT INTO `UserInfoRole` VALUES (6503,23);
INSERT INTO `UserInfoRole` VALUES (6508,23);
INSERT INTO `UserInfoRole` VALUES (6509,23);
INSERT INTO `UserInfoRole` VALUES (6504,26);
INSERT INTO `UserInfoRole` VALUES (6505,27);
INSERT INTO `UserInfoRole` VALUES (6509,28);
/*!40000 ALTER TABLE `UserInfoRole` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `SubscribedProduct` WRITE;
/*!40000 ALTER TABLE `SubscribedProduct` DISABLE KEYS */;
INSERT INTO `SubscribedProduct` VALUES (1,8);
INSERT INTO `SubscribedProduct` VALUES (2,8);
INSERT INTO `SubscribedProduct` VALUES (1,30);
INSERT INTO `SubscribedProduct` VALUES (2,30);
INSERT INTO `SubscribedProduct` VALUES (1,47);
INSERT INTO `SubscribedProduct` VALUES (2,47);
INSERT INTO `SubscribedProduct` VALUES (1,51);
INSERT INTO `SubscribedProduct` VALUES (2,51);
INSERT INTO `SubscribedProduct` VALUES (1,52);
INSERT INTO `SubscribedProduct` VALUES (2,52);
INSERT INTO `SubscribedProduct` VALUES (1,53);
INSERT INTO `SubscribedProduct` VALUES (2,53);
INSERT INTO `SubscribedProduct` VALUES (3,53);
/*!40000 ALTER TABLE `SubscribedProduct` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `SubscribedProductPrivilege` WRITE;
/*!40000 ALTER TABLE `SubscribedProductPrivilege` DISABLE KEYS */;
INSERT INTO `crm`.`SubscribedProductPrivilege`
(`Product_Id_fk`,
`Organisation_Id_fk`,
`ProductPrivilege_id_fk`,
`CreatedBy`,
`DateCreated`)
VALUES
('1', '8', '1', NULL, '2014-10-16 15:08:47.063000')
,('1', '8', '4', NULL, '2014-10-16 15:08:40.644000')
,('1', '8', '7', NULL, '2014-10-16 15:08:43.415000')
,('1', '8', '9', NULL, '2014-10-16 15:08:44.623000')
,('1', '8', '12', NULL, '2014-10-16 15:08:34.766000')
,('1', '8', '17', NULL, '2014-10-16 15:08:38.067000')
,('1', '8', '20', NULL, '2014-10-16 15:08:28.198000')
,('1', '8', '21', NULL, '2014-10-16 15:08:31.080000')
,('1', '8', '22', NULL, '2014-10-16 15:08:10.687000')
,('1', '8', '23', NULL, '2014-10-16 15:08:08.967000')
,('1', '8', '26', NULL, '2014-10-16 15:08:42.357000')
,('1', '30', '1', NULL, '2014-10-20 10:37:09.624000')
,('1', '30', '4', NULL, '2014-10-20 10:36:44.625000')
,('1', '30', '7', NULL, '2014-10-20 10:36:57.041000')
,('1', '30', '9', NULL, '2014-10-20 10:37:02.358000')
,('1', '30', '12', NULL, '2014-10-20 10:36:32.893000')
,('1', '30', '17', NULL, '2014-10-20 10:36:37.200000')
,('1', '30', '20', NULL, '2014-10-20 10:37:28.302000')
,('1', '30', '21', NULL, '2014-10-20 10:37:20.052000')
,('1', '30', '22', NULL, '2014-10-20 10:37:31.059000')
,('1', '30', '23', NULL, '2014-10-20 10:37:33.376000')
,('1', '30', '26', NULL, '2014-10-20 10:36:51.809000')
,('1', '47', '1', NULL, '2014-10-20 11:45:21.722000')
,('1', '47', '4', NULL, '2014-10-20 11:45:13.323000')
,('1', '47', '7', NULL, '2014-10-20 11:45:18.115000')
,('1', '47', '9', NULL, '2014-10-20 11:45:19.987000')
,('1', '47', '12', NULL, '2014-10-20 11:45:00.789000')
,('1', '47', '17', NULL, '2014-10-20 11:45:10.030000')
,('1', '47', '20', NULL, '2014-10-20 11:45:25.202000')
,('1', '47', '21', NULL, '2014-10-20 11:45:23.330000')
,('1', '47', '22', NULL, '2014-10-20 11:45:26.795000')
,('1', '47', '23', NULL, '2014-10-20 11:45:29.027000')
,('1', '47', '26', NULL, '2014-10-20 11:45:16.180000')
,('1', '51', '2', NULL, '2014-10-20 16:20:36.451000')
,('1', '51', '5', NULL, '2014-10-20 16:19:37.284000')
,('1', '51', '7', NULL, '2014-10-20 16:20:34.639000')
,('1', '51', '9', NULL, '2014-10-20 16:20:35.593000')
,('1', '51', '13', NULL, '2014-10-20 16:19:32.114000')
,('1', '51', '17', NULL, '2014-10-20 16:19:33.363000')
,('1', '51', '20', NULL, '2014-10-20 16:20:38.137000')
,('1', '51', '21', NULL, '2014-10-20 16:20:37.279000')
,('1', '51', '22', NULL, '2014-10-20 16:20:39.202000')
,('1', '51', '23', NULL, '2014-10-20 16:20:39.998000')
,('1', '51', '26', NULL, '2014-10-20 16:19:38.986000')
,('1', '52', '2', NULL, '2014-10-20 16:37:44.897000')
,('1', '52', '5', NULL, '2014-10-20 16:37:34.748000')
,('1', '52', '6', NULL, '2014-10-20 16:37:35.639000')
,('1', '52', '7', NULL, '2014-10-20 16:37:43.633000')
,('1', '52', '9', NULL, '2014-10-20 16:37:42.462000')
,('1', '52', '13', NULL, '2014-10-20 16:37:32.330000')
,('1', '52', '17', NULL, '2014-10-20 16:37:33.454000')
,('1', '52', '20', NULL, '2014-10-20 16:37:47.019000')
,('1', '52', '21', NULL, '2014-10-20 16:37:45.911000')
,('1', '52', '22', NULL, '2014-10-20 16:37:47.910000')
,('1', '52', '23', NULL, '2014-10-20 16:37:48.737000')
,('1', '52', '26', NULL, '2014-10-20 16:37:36.357000')
,('1', '53', '11', NULL, '2014-10-20 17:23:27.732000')
,('1', '53', '14', NULL, '2014-10-20 17:22:25.467000')
,('1', '53', '15', NULL, '2014-10-20 17:22:22.515000')
,('1', '53', '16', NULL, '2014-10-20 17:22:24.443000')
,('1', '53', '17', NULL, '2014-10-20 17:22:33.574000')
,('1', '53', '18', NULL, '2014-10-20 17:22:34.356000')
,('1', '53', '19', NULL, '2014-10-20 17:23:23.492000')
,('1', '53', '20', NULL, '2014-10-20 17:23:30.867000')
,('1', '53', '21', NULL, '2014-10-20 17:23:28.787000')
,('1', '53', '22', NULL, '2014-10-20 17:23:31.956000')
,('1', '53', '23', NULL, '2014-10-20 17:23:35.162000')
,('1', '53', '24', NULL, '2014-10-20 17:22:23.437000')
,('1', '53', '25', NULL, '2014-10-20 17:23:19.283000')
,('1', '53', '27', NULL, '2014-10-20 17:23:22.435000')
,('2', '8', '1', NULL, '2014-10-16 15:10:19.171000')
,('2', '8', '4', NULL, '2014-10-16 15:10:26.888000')
,('2', '8', '7', NULL, '2014-10-16 15:10:17.389000')
,('2', '8', '9', NULL, '2014-10-16 15:10:18.360000')
,('2', '8', '12', NULL, '2014-10-16 15:10:24.108000')
,('2', '8', '17', NULL, '2014-10-16 15:10:25.120000')
,('2', '8', '20', NULL, '2014-10-16 15:10:21.533000')
,('2', '8', '21', NULL, '2014-10-16 15:10:20.332000')
,('2', '8', '22', NULL, '2014-10-16 15:10:22.285000')
,('2', '8', '23', NULL, '2014-10-16 15:10:23.132000')
,('2', '8', '26', NULL, '2014-10-16 15:10:15.906000')
,('2', '30', '1', NULL, '2014-10-20 10:38:00.352000')
,('2', '30', '4', NULL, '2014-10-20 10:37:56.427000')
,('2', '30', '7', NULL, '2014-10-20 10:37:58.584000')
,('2', '30', '9', NULL, '2014-10-20 10:37:59.450000')
,('2', '30', '12', NULL, '2014-10-20 10:37:54.339000')
,('2', '30', '17', NULL, '2014-10-20 10:37:55.415000')
,('2', '30', '20', NULL, '2014-10-20 10:38:04.034000')
,('2', '30', '21', NULL, '2014-10-20 10:38:01.384000')
,('2', '30', '22', NULL, '2014-10-20 10:38:04.976000')
,('2', '30', '23', NULL, '2014-10-20 10:38:05.879000')
,('2', '30', '26', NULL, '2014-10-20 10:37:57.732000')
,('2', '47', '1', NULL, '2014-10-20 11:46:01.839000')
,('2', '47', '4', NULL, '2014-10-20 11:45:55.471000')
,('2', '47', '7', NULL, '2014-10-20 11:45:58.921000')
,('2', '47', '9', NULL, '2014-10-20 11:46:00.683000')
,('2', '47', '12', NULL, '2014-10-20 11:45:50.977000')
,('2', '47', '17', NULL, '2014-10-20 11:45:53.504000')
,('2', '47', '20', NULL, '2014-10-20 11:46:05.287000')
,('2', '47', '21', NULL, '2014-10-20 11:46:03.478000')
,('2', '47', '22', NULL, '2014-10-20 11:46:10.254000')
,('2', '47', '23', NULL, '2014-10-20 11:46:17.330000')
,('2', '47', '26', NULL, '2014-10-20 11:45:57.172000')
,('2', '51', '2', NULL, '2014-10-20 16:20:52.310000')
,('2', '51', '5', NULL, '2014-10-20 16:20:48.544000')
,('2', '51', '7', NULL, '2014-10-20 16:20:50.528000')
,('2', '51', '9', NULL, '2014-10-20 16:20:51.355000')
,('2', '51', '13', NULL, '2014-10-20 16:20:46.369000')
,('2', '51', '17', NULL, '2014-10-20 16:20:47.258000')
,('2', '51', '20', NULL, '2014-10-20 16:20:54.188000')
,('2', '51', '21', NULL, '2014-10-20 16:20:53.216000')
,('2', '51', '22', NULL, '2014-10-20 16:20:54.952000')
,('2', '51', '23', NULL, '2014-10-20 16:20:55.784000')
,('2', '51', '26', NULL, '2014-10-20 16:20:49.544000')
,('2', '52', '2', NULL, '2014-10-20 16:38:05.931000')
,('2', '52', '5', NULL, '2014-10-20 16:37:59.594000')
,('2', '52', '6', NULL, '2014-10-20 16:38:00.703000')
,('2', '52', '7', NULL, '2014-10-20 16:38:03.732000')
,('2', '52', '9', NULL, '2014-10-20 16:38:05.151000')
,('2', '52', '13', NULL, '2014-10-20 16:37:57.754000')
,('2', '52', '17', NULL, '2014-10-20 16:37:58.487000')
,('2', '52', '20', NULL, '2014-10-20 16:38:07.513000')
,('2', '52', '21', NULL, '2014-10-20 16:38:06.789000')
,('2', '52', '22', NULL, '2014-10-20 16:38:08.232000')
,('2', '52', '23', NULL, '2014-10-20 16:38:09.108000')
,('2', '52', '26', NULL, '2014-10-20 16:38:02.748000')
,('2', '53', '11', NULL, '2014-10-20 17:24:11.758000')
,('2', '53', '14', NULL, '2014-10-20 17:23:55.294000')
,('2', '53', '15', NULL, '2014-10-20 17:23:53.075000')
,('2', '53', '16', NULL, '2014-10-20 17:23:54.218000')
,('2', '53', '17', NULL, '2014-10-20 17:23:59.293000')
,('2', '53', '18', NULL, '2014-10-20 17:23:58.039000')
,('2', '53', '19', NULL, '2014-10-20 17:24:09.346000')
,('2', '53', '20', NULL, '2014-10-20 17:24:13.335000')
,('2', '53', '21', NULL, '2014-10-20 17:24:12.555000')
,('2', '53', '22', NULL, '2014-10-20 17:24:14.270000')
,('2', '53', '23', NULL, '2014-10-20 17:24:15.138000')
,('2', '53', '24', NULL, '2014-10-20 17:23:51.842000')
,('2', '53', '25', NULL, '2014-10-20 17:24:07.666000')
,('2', '53', '27', NULL, '2014-10-20 17:24:08.555000')
,('3', '53', '11', NULL, '2014-10-20 17:22:23.437000')
,('3', '53', '25', NULL, '2014-10-20 17:23:19.283000')
,('3', '53', '27', NULL, '2014-10-20 17:23:22.435000')
;
/*!40000 ALTER TABLE `SubscribedProductPrivilege` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `ProductRolePrivilege` WRITE;
/*!40000 ALTER TABLE `ProductRolePrivilege` DISABLE KEYS */;
INSERT INTO `ProductRolePrivilege` VALUES (293,8,12,1,1);
INSERT INTO `ProductRolePrivilege` VALUES (294,8,12,1,4);
INSERT INTO `ProductRolePrivilege` VALUES (295,8,12,1,7);
INSERT INTO `ProductRolePrivilege` VALUES (296,8,12,1,12);
INSERT INTO `ProductRolePrivilege` VALUES (297,8,12,1,17);
INSERT INTO `ProductRolePrivilege` VALUES (298,8,12,1,20);
INSERT INTO `ProductRolePrivilege` VALUES (299,8,12,1,21);
INSERT INTO `ProductRolePrivilege` VALUES (300,8,12,1,22);
INSERT INTO `ProductRolePrivilege` VALUES (301,8,12,1,23);
INSERT INTO `ProductRolePrivilege` VALUES (302,8,12,1,26);
INSERT INTO `ProductRolePrivilege` VALUES (303,8,12,2,1);
INSERT INTO `ProductRolePrivilege` VALUES (304,8,12,2,4);
INSERT INTO `ProductRolePrivilege` VALUES (305,8,12,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (306,8,12,2,12);
INSERT INTO `ProductRolePrivilege` VALUES (307,8,12,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (308,8,12,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (309,8,12,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (310,8,12,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (311,8,12,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (312,8,12,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (313,8,12,2,26);
INSERT INTO `ProductRolePrivilege` VALUES (314,8,12,1,9);
INSERT INTO `ProductRolePrivilege` VALUES (316,30,13,1,1);
INSERT INTO `ProductRolePrivilege` VALUES (317,30,13,1,4);
INSERT INTO `ProductRolePrivilege` VALUES (318,30,13,1,9);
INSERT INTO `ProductRolePrivilege` VALUES (319,30,13,1,12);
INSERT INTO `ProductRolePrivilege` VALUES (320,30,13,1,17);
INSERT INTO `ProductRolePrivilege` VALUES (321,30,13,1,20);
INSERT INTO `ProductRolePrivilege` VALUES (322,30,13,1,21);
INSERT INTO `ProductRolePrivilege` VALUES (323,30,13,1,22);
INSERT INTO `ProductRolePrivilege` VALUES (324,30,13,1,23);
INSERT INTO `ProductRolePrivilege` VALUES (325,30,13,1,26);
INSERT INTO `ProductRolePrivilege` VALUES (326,30,13,1,7);
INSERT INTO `ProductRolePrivilege` VALUES (327,30,13,2,1);
INSERT INTO `ProductRolePrivilege` VALUES (328,30,13,2,4);
INSERT INTO `ProductRolePrivilege` VALUES (329,30,13,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (330,30,13,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (331,30,13,2,12);
INSERT INTO `ProductRolePrivilege` VALUES (332,30,13,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (333,30,13,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (334,30,13,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (335,30,13,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (336,30,13,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (337,30,13,2,26);
INSERT INTO `ProductRolePrivilege` VALUES (339,47,14,1,1);
INSERT INTO `ProductRolePrivilege` VALUES (340,47,14,1,4);
INSERT INTO `ProductRolePrivilege` VALUES (341,47,14,1,7);
INSERT INTO `ProductRolePrivilege` VALUES (342,47,14,1,9);
INSERT INTO `ProductRolePrivilege` VALUES (343,47,14,1,12);
INSERT INTO `ProductRolePrivilege` VALUES (344,47,14,1,17);
INSERT INTO `ProductRolePrivilege` VALUES (345,47,14,1,20);
INSERT INTO `ProductRolePrivilege` VALUES (346,47,14,1,21);
INSERT INTO `ProductRolePrivilege` VALUES (347,47,14,1,22);
INSERT INTO `ProductRolePrivilege` VALUES (348,47,14,1,23);
INSERT INTO `ProductRolePrivilege` VALUES (349,47,14,1,26);
INSERT INTO `ProductRolePrivilege` VALUES (350,47,14,2,1);
INSERT INTO `ProductRolePrivilege` VALUES (351,47,14,2,4);
INSERT INTO `ProductRolePrivilege` VALUES (352,47,14,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (353,47,14,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (354,47,14,2,12);
INSERT INTO `ProductRolePrivilege` VALUES (355,47,14,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (356,47,14,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (357,47,14,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (358,47,14,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (359,47,14,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (360,47,14,2,26);
INSERT INTO `ProductRolePrivilege` VALUES (363,8,15,1,1);
INSERT INTO `ProductRolePrivilege` VALUES (364,8,15,1,4);
INSERT INTO `ProductRolePrivilege` VALUES (365,8,15,1,7);
INSERT INTO `ProductRolePrivilege` VALUES (366,8,15,1,9);
INSERT INTO `ProductRolePrivilege` VALUES (367,8,15,1,12);
INSERT INTO `ProductRolePrivilege` VALUES (368,8,15,1,17);
INSERT INTO `ProductRolePrivilege` VALUES (369,8,15,1,20);
INSERT INTO `ProductRolePrivilege` VALUES (370,8,15,1,21);
INSERT INTO `ProductRolePrivilege` VALUES (371,8,15,1,22);
INSERT INTO `ProductRolePrivilege` VALUES (372,8,15,1,23);
INSERT INTO `ProductRolePrivilege` VALUES (373,8,15,1,26);
INSERT INTO `ProductRolePrivilege` VALUES (374,51,17,1,2);
INSERT INTO `ProductRolePrivilege` VALUES (375,51,17,1,5);
INSERT INTO `ProductRolePrivilege` VALUES (376,51,17,1,7);
INSERT INTO `ProductRolePrivilege` VALUES (377,51,17,1,9);
INSERT INTO `ProductRolePrivilege` VALUES (378,51,17,1,13);
INSERT INTO `ProductRolePrivilege` VALUES (379,51,17,1,17);
INSERT INTO `ProductRolePrivilege` VALUES (380,51,17,1,20);
INSERT INTO `ProductRolePrivilege` VALUES (381,51,17,1,21);
INSERT INTO `ProductRolePrivilege` VALUES (382,51,17,1,22);
INSERT INTO `ProductRolePrivilege` VALUES (383,51,17,1,23);
INSERT INTO `ProductRolePrivilege` VALUES (384,51,17,1,26);
INSERT INTO `ProductRolePrivilege` VALUES (385,51,17,2,2);
INSERT INTO `ProductRolePrivilege` VALUES (386,51,17,2,5);
INSERT INTO `ProductRolePrivilege` VALUES (387,51,17,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (388,51,17,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (389,51,17,2,13);
INSERT INTO `ProductRolePrivilege` VALUES (390,51,17,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (391,51,17,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (392,51,17,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (393,51,17,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (394,51,17,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (395,51,17,2,26);
INSERT INTO `ProductRolePrivilege` VALUES (396,51,18,1,2);
INSERT INTO `ProductRolePrivilege` VALUES (397,51,18,1,5);
INSERT INTO `ProductRolePrivilege` VALUES (398,51,18,1,7);
INSERT INTO `ProductRolePrivilege` VALUES (399,51,18,1,9);
INSERT INTO `ProductRolePrivilege` VALUES (400,51,18,1,13);
INSERT INTO `ProductRolePrivilege` VALUES (401,51,18,1,17);
INSERT INTO `ProductRolePrivilege` VALUES (402,51,18,1,20);
INSERT INTO `ProductRolePrivilege` VALUES (403,51,18,1,21);
INSERT INTO `ProductRolePrivilege` VALUES (404,51,18,1,22);
INSERT INTO `ProductRolePrivilege` VALUES (405,51,18,1,23);
INSERT INTO `ProductRolePrivilege` VALUES (406,51,18,1,26);
INSERT INTO `ProductRolePrivilege` VALUES (407,51,19,2,2);
INSERT INTO `ProductRolePrivilege` VALUES (408,51,19,2,5);
INSERT INTO `ProductRolePrivilege` VALUES (409,51,19,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (410,51,19,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (411,51,19,2,13);
INSERT INTO `ProductRolePrivilege` VALUES (412,51,19,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (413,51,19,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (414,51,19,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (415,51,19,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (416,51,19,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (417,51,19,2,26);
INSERT INTO `ProductRolePrivilege` VALUES (418,52,20,1,2);
INSERT INTO `ProductRolePrivilege` VALUES (419,52,20,1,5);
INSERT INTO `ProductRolePrivilege` VALUES (420,52,20,1,6);
INSERT INTO `ProductRolePrivilege` VALUES (421,52,20,1,7);
INSERT INTO `ProductRolePrivilege` VALUES (422,52,20,1,9);
INSERT INTO `ProductRolePrivilege` VALUES (423,52,20,1,17);
INSERT INTO `ProductRolePrivilege` VALUES (424,52,20,1,13);
INSERT INTO `ProductRolePrivilege` VALUES (425,52,20,1,20);
INSERT INTO `ProductRolePrivilege` VALUES (426,52,20,1,21);
INSERT INTO `ProductRolePrivilege` VALUES (427,52,20,1,22);
INSERT INTO `ProductRolePrivilege` VALUES (428,52,20,1,23);
INSERT INTO `ProductRolePrivilege` VALUES (429,52,20,1,26);
INSERT INTO `ProductRolePrivilege` VALUES (430,52,20,2,2);
INSERT INTO `ProductRolePrivilege` VALUES (431,52,20,2,5);
INSERT INTO `ProductRolePrivilege` VALUES (432,52,20,2,6);
INSERT INTO `ProductRolePrivilege` VALUES (433,52,20,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (434,52,20,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (435,52,20,2,13);
INSERT INTO `ProductRolePrivilege` VALUES (436,52,20,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (437,52,20,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (438,52,20,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (439,52,20,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (440,52,20,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (441,52,20,2,26);
INSERT INTO `ProductRolePrivilege` VALUES (442,53,23,1,11);
INSERT INTO `ProductRolePrivilege` VALUES (443,53,23,1,14);
INSERT INTO `ProductRolePrivilege` VALUES (444,53,23,1,15);
INSERT INTO `ProductRolePrivilege` VALUES (445,53,23,1,16);
INSERT INTO `ProductRolePrivilege` VALUES (446,53,23,1,17);
INSERT INTO `ProductRolePrivilege` VALUES (447,53,23,1,18);
INSERT INTO `ProductRolePrivilege` VALUES (448,53,23,1,19);
INSERT INTO `ProductRolePrivilege` VALUES (449,53,23,1,20);
INSERT INTO `ProductRolePrivilege` VALUES (450,53,23,1,21);
INSERT INTO `ProductRolePrivilege` VALUES (451,53,23,1,22);
INSERT INTO `ProductRolePrivilege` VALUES (452,53,23,1,23);
INSERT INTO `ProductRolePrivilege` VALUES (453,53,23,1,24);
INSERT INTO `ProductRolePrivilege` VALUES (454,53,23,1,25);
INSERT INTO `ProductRolePrivilege` VALUES (455,53,23,1,27);
INSERT INTO `ProductRolePrivilege` VALUES (456,53,23,2,11);
INSERT INTO `ProductRolePrivilege` VALUES (457,53,23,2,14);
INSERT INTO `ProductRolePrivilege` VALUES (458,53,23,2,15);
INSERT INTO `ProductRolePrivilege` VALUES (459,53,23,2,16);
INSERT INTO `ProductRolePrivilege` VALUES (460,53,23,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (461,53,23,2,18);
INSERT INTO `ProductRolePrivilege` VALUES (462,53,23,2,19);
INSERT INTO `ProductRolePrivilege` VALUES (463,53,23,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (464,53,23,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (465,53,23,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (466,53,23,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (467,53,23,2,24);
INSERT INTO `ProductRolePrivilege` VALUES (468,53,23,2,25);
INSERT INTO `ProductRolePrivilege` VALUES (469,53,23,2,27);
INSERT INTO `ProductRolePrivilege` VALUES (1,53,1,1,11);
INSERT INTO `ProductRolePrivilege` VALUES (2,53,1,1,25);
INSERT INTO `ProductRolePrivilege` VALUES (3,53,1,1,27);
INSERT INTO `ProductRolePrivilege` VALUES (4,53,1,2,11);
INSERT INTO `ProductRolePrivilege` VALUES (5,53,1,2,25);
INSERT INTO `ProductRolePrivilege` VALUES (6,53,1,2,27);
INSERT INTO `ProductRolePrivilege` VALUES (7,53,1,3,11);
INSERT INTO `ProductRolePrivilege` VALUES (8,53,1,3,25);
INSERT INTO `ProductRolePrivilege` VALUES (9,53,1,3,27);
INSERT INTO `ProductRolePrivilege` VALUES (470,30,26,2,1);
INSERT INTO `ProductRolePrivilege` VALUES (471,30,26,2,4);
INSERT INTO `ProductRolePrivilege` VALUES (472,30,26,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (473,30,26,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (474,30,26,2,12);
INSERT INTO `ProductRolePrivilege` VALUES (475,30,26,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (476,30,26,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (477,30,26,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (478,30,26,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (479,30,26,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (480,30,26,2,26);
INSERT INTO `ProductRolePrivilege` VALUES (481,47,27,2,1);
INSERT INTO `ProductRolePrivilege` VALUES (482,47,27,2,4);
INSERT INTO `ProductRolePrivilege` VALUES (483,47,27,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (484,47,27,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (485,47,27,2,12);
INSERT INTO `ProductRolePrivilege` VALUES (486,47,27,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (487,47,27,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (488,47,27,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (489,47,27,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (490,47,27,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (491,47,27,2,26);
INSERT INTO `ProductRolePrivilege` VALUES (494,52,21,2,5);
INSERT INTO `ProductRolePrivilege` VALUES (498,52,21,2,6);
INSERT INTO `ProductRolePrivilege` VALUES (499,52,21,2,7);
INSERT INTO `ProductRolePrivilege` VALUES (500,52,21,2,2);
INSERT INTO `ProductRolePrivilege` VALUES (501,52,21,2,9);
INSERT INTO `ProductRolePrivilege` VALUES (502,52,21,2,13);
INSERT INTO `ProductRolePrivilege` VALUES (503,52,21,2,17);
INSERT INTO `ProductRolePrivilege` VALUES (504,52,21,2,20);
INSERT INTO `ProductRolePrivilege` VALUES (505,52,21,2,21);
INSERT INTO `ProductRolePrivilege` VALUES (506,52,21,2,22);
INSERT INTO `ProductRolePrivilege` VALUES (507,52,21,2,23);
INSERT INTO `ProductRolePrivilege` VALUES (508,52,21,2,26);
/*!40000 ALTER TABLE `ProductRolePrivilege` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `SystemRolePrivilege` WRITE;
/*!40000 ALTER TABLE `SystemRolePrivilege` DISABLE KEYS */;
INSERT INTO `crm`.`SystemRolePrivilege`
(`System_Role_Privilege_Id`,
`Role_Id_fk`,
`Privilege_Id_fk`)
VALUES
(1,28,10
);
/*!40000 ALTER TABLE `SystemRolePrivilege` ENABLE KEYS */;
UNLOCK TABLES;


/* =========================
   ENSURE ALL CAN TRADE 
   ========================= */
DELETE FROM crm.CanTradeWith where id>-9999;

INSERT INTO crm.CanTradeWith (Organisation_Id_fk,Counterparty_Id,IncludeOrExclude,AsBrokerOrPrincipal)
SELECT Id,-1,'I','P' FROM crm.Organisation;

INSERT INTO crm.CanTradeWith (Organisation_Id_fk,Counterparty_Id,IncludeOrExclude,AsBrokerOrPrincipal)
SELECT Id,-1,'I','B' FROM crm.Organisation;

/* =========================
   Make sure all organisations have an email  
   ========================= */
UPDATE crm.Organisation
SET email='kevin.gutridge@argusmedia.com' 
WHERE email is null AND id>0