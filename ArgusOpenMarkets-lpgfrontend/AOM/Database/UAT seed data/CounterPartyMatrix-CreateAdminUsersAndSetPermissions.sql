
########################################################################################################
###############################		This script creates Admin Users and		############################
###############################		allows all others users to see the 		############################
###############################		Permission Matrix  						############################
###############################		USE FOR TESTING PURPOSES ONLY!!! 		############################
########################################################################################################


########################################################################################################
#####################################   CREATE ADMIN USERS   ###########################################
########################################################################################################

use `crm`;
SET SQL_SAFE_UPDATES = 0;
START transaction;

#0. Create the Admin Users
-- CREATE ADMIN USERS

	INSERT INTO `UserInfo`
		(`Id`
		,`Organisation_Id_fk`
		,`Name`
		,`Email`
		,`Title`
		,`Telephone`
		,`Username`
		,`IsActive`
		,`IsDeleted`
		,`IsBlocked`
		,`ArgusCrmUsername`)
	SELECT 
		#ConvertToInt(CONCAT(o.ShortCode , 'Admin'))
		(SELECT CAST(CONV(SUBSTRING(CAST(SHA(CONCAT(o.ShortCode , 'Admin')) AS CHAR), 1, 16), 16, 10 ) /9999999999999 as unsigned))
		,o.id `Organisation_Id_fk`
		, CONCAT('Admin at ', o.LegalName) `Name`
		,'catchall@argus-labs.com'
		, null
		, null
		, CONCAT(o.ShortCode , 'Admin') `Username`
		, true  
		, false
		, false
		, null
	FROM Organisation o
	WHERE o.ShortCode IS NOT NULL
	ON DUPLICATE KEY UPDATE IsBlocked=IsBlocked;

INSERT INTO `UserCredentials` (`UserInfo_Id_fk`,`Password`,`FirstTimeLogin`,`DefaultExpirationInMonths`,`DateCreated`,`Expiration`,`CredentialType`)
SELECT u.Id, 'NeedToSet', 0, 12, Now(), '2015-12-01 15:00:00.000000', 'P' FROM `UserInfo` u 
ON DUPLICATE KEY UPDATE FirstTimeLogin=FirstTimeLogin;

-- Clean up
DELETE FROM `UserInfoRole` 
WHERE Role_Id_fk IN (SELECT Id FROM `OrganisationRole` WHERE Name in ('CounterParty Permissions Admin','CounterParty Permissions - View Only','Broker Permissions - Admin','Broker Permissions - View Only'));


########################################################################################################
#####################################   	ORGANISATION ROLES   #######################################
########################################################################################################

#cleanup
SET foreign_key_checks = 0;
DELETE FROM `OrganisationRole` WHERE Name in ('CounterParty Permissions Admin','CounterParty Permissions - View Only','Broker Permissions - Admin','Broker Permissions - View Only');
SET foreign_key_checks = 1;

#1. create BP for BP readonly -first
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId
FROM Organisation o, UserInfo ui, (SELECT 'Broker Permissions - View Only' RoleName) roles
WHERE o.OrganisationType ='B' 
ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;

#SELECT * FROM `OrganisationRole`;
#2. CP reaonly permissions for all traders
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId
FROM Organisation o, UserInfo ui, (SELECT 'CounterParty Permissions - View Only'  RoleName UNION SELECT 'Broker Permissions - View Only' RoleName ) roles
WHERE o.OrganisationType ='T' 
ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;

#SELECT * FROM `OrganisationRole`;
#3. CS reaonly permissions for all traders
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId
FROM Organisation o, UserInfo ui, (SELECT 'Company Settings Permissions - View Only'  RoleName UNION SELECT 'Broker Permissions - View Only' RoleName ) roles
WHERE o.OrganisationType ='T' 
ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;

#4. Insert Admin BP roles for Admin brokers
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId
FROM Organisation o, UserInfo ui, (SELECT 'Broker Permissions - Admin' RoleName) roles
WHERE o.OrganisationType ='B' and ui.Username LIKE '%Admin%'
ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;

#5. Insert Admin CP roles for Admin traders
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId
FROM Organisation o, UserInfo ui, (SELECT 'CounterParty Permissions - Admin' RoleName UNION SELECT 'Broker Permissions - Admin' RoleName ) roles
WHERE o.OrganisationType ='T' and ui.Username LIKE '%Admin%'
ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;

#6. Insert Admin CS roles for Admin traders
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId
FROM Organisation o, UserInfo ui, (SELECT 'Company Settings Permissions - Admin' RoleName UNION SELECT 'Broker Permissions - Admin' RoleName ) roles
WHERE o.OrganisationType ='T' and ui.Username LIKE '%Admin%'
ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;

## 6.1 Insert Editor permissions 
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId
FROM Organisation o, UserInfo ui, (SELECT 'CounterParty Permissions - View Only'  RoleName UNION SELECT 'Broker Permissions - View Only' RoleName ) roles
WHERE o.OrganisationType ='A' 
ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;


########################################################################################################
############			 Sysadmin privileges for company CompanySettings               #################
########################################################################################################

INSERT INTO `SystemPrivilege` (`Id`,`Name`,`Description`,`ArgusUseOnly`,`DefaultRoleGroup`,`OrganisationType`) 
	VALUES(15, 'CompanySettings_View', 'View the Company Settings for your organisation', 0, 'Local Administrator',' ')
	ON DUPLICATE KEY UPDATE `Name`=`Name`;
INSERT INTO `SystemPrivilege` (`Id`,`Name`,`Description`,`ArgusUseOnly`,`DefaultRoleGroup`,`OrganisationType`)
	VALUES(16, 'CompanySettings_Amend', 'Amend the Company Settings for your organisation', 0, 'Local Administrator',' ')
	ON DUPLICATE KEY UPDATE `Name`=`Name`;



########################################################################################################
#####################################   USER INFO ROLES   ##############################################
########################################################################################################

#5.Inser roles for brokers - BP readaonly
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
#(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%CounterParty Permissions - View Only%') matrixPerms
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Broker Permissions - View Only%') matrixPerms
WHERE o.OrganisationType ='B' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;


#5.Inser roles for traders - CP readaonly
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Counterparty Permissions - View Only%' or `Name` LIKE '%Broker Permissions - View Only%') matrixPerms
WHERE o.OrganisationType ='T' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

#5.1 Inser roles for traders - CS readaonly
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Company Settings Permissions - View Only%' or `Name` LIKE '%Broker Permissions - View Only%') matrixPerms
WHERE o.OrganisationType ='T' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

#6.Insert roles for brokers - BP - Admin
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
#(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Counterparty Permissions - Admin%') matrixPerms
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Broker Permissions - Admin%') matrixPerms
WHERE o.OrganisationType ='B' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId and 
		(u.Username LIKE '%Admin%' )
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;


#7.Insert roles for traders - CP - Admin
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Counterparty Permissions - Admin%' or `Name` LIKE '%Broker Permissions - Admin%') matrixPerms
WHERE o.OrganisationType ='T' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId and 
		(u.Username LIKE '%Admin%' )
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

#7.1 Insert roles for traders - CP - Admin
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Company Settings Permissions - Admin%' or `Name` LIKE '%Broker Permissions - Admin%') matrixPerms
WHERE o.OrganisationType ='T' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId and 
		(u.Username LIKE '%Admin%' )
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

#7.2 Insert roles for traders and brokers - Readonly product roles
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Read Only: %' ) matrixPerms
WHERE o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId and (u.Username LIKE '%Admin%' )
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

#7.3 - Insert roles for Editors - CP - Admin
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Counterparty Permissions - View Only%' or `Name` LIKE '%Broker Permissions - View Only%') matrixPerms
WHERE o.OrganisationType ='A' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;


########################################################################################################
#####################################   SYSTEM ROLE PRIVILEGE   ########################################
########################################################################################################

#8 CREATE SYSTEM ROLE PRIVILEGES FOR ALL ORGANISATION ROLES PRESENT
INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='CPMatrix_View') FROM OrganisationRole WHERE Name in ('CounterParty Permissions Admin', 'CounterParty Permissions - View Only')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='CPMatrix_Update') FROM OrganisationRole WHERE Name in ( 'CounterParty Permissions - Admin')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='BPMatrix_View') FROM OrganisationRole WHERE Name in ('Broker Permissions - Admin', 'Broker Permissions - View Only')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='BPMatrix_Update') FROM OrganisationRole WHERE Name in ('Broker Permissions - Admin')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='CompanySettings_View') FROM OrganisationRole WHERE Name in ('Company Settings Permissions - Admin', 'Company Settings Permissions - View Only')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='CompanySettings_Amend') FROM OrganisationRole WHERE Name in ('Company Settings Permissions - Admin')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;


#########################################################################################################
#						Add passwords for the new Admin users 											#
#	the script below relies on the users being given the same id from the the ConvertToInt function  	#
#	 	If you don't run the function or change the usernames, it will not work and the pass 			#
#							need to be set manually														#
#########################################################################################################
UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'JHkygSn3KkE/atv32mDdQm/f8IVTV+nC67BK8c/EMA0=' where ui.UserName = 'ECHAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'i1/S6eJk8QRzMUkEpVk5+Sjss2aqeX687h/xomDKDbY=' where ui.UserName = 'GR1Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'U6GyEaEJQFet9PVmitOvN/8Y/1kATY8v1TJYOa5M0q0=' where ui.UserName = 'DLTAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'W46lMK5nFKuceqIUXFFtrnWmsFTm6vxRFhRpD8Hs73U=' where ui.UserName = 'BRAAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'bolQBj4mvsGIvi0yer8Na1uW4Ua8PqsXnc7uWTGk1y4=' where ui.UserName = 'TR1Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'jROTpiYcEb2688gd2Mu/ndJ/F4MkV35OpiYXvWuma6A=' where ui.UserName = 'BR2Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'qUbbtALe7ypy25WjcrXwhWlF7DkM/xcaRdTDrvVU7ek=' where ui.UserName = 'EXCAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'Wpoeor2P8fwvnrCjtlsL7V9cDyTsOX2UozW7ne04waw=' where ui.UserName = 'VICAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '9lC7l2rbOg0gXwmSYtcgVcamF6xJcouSNuCp3I+5gq0=' where ui.UserName = 'ALPAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'Nw19HvlfkyUHuz3FPVa3P3BPN1fCXUgdWadOUeXcST0=' where ui.UserName = 'WHIAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'zJhzdN2/sIbnx75s+t84Og4EVGEpUe63l6IZq8arXsM=' where ui.UserName = 'TR2Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'g4Lm+o785N7ikIlPN/8wQ3qWCtUvrDE2sViCZ/UmWqs=' where ui.UserName = 'ZULAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'm6OgoW+W60IVUTpoSHCy3Z6j7c8Atjh2y0hb5ZVhFzI=' where ui.UserName = 'BR1Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '6vP44uFCBvrWy3XN6pGQIDvdb3d9+YmXKYODHjtIWLI=' where ui.UserName = 'ARGAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '5Q5VzABBoQzzijd42a5N3X1ubwuo5hfg9X/4QuY+2GY=' where ui.UserName = 'BC2Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'xgFqMRiKCq6LfqJCEu+D1OVC/ckgcnjhHxRYG6FnuwQ=' where ui.UserName = 'BPTAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '6PEzxumi1sr9Z66Y0AFvwVaP4o461qogqpagmrRZfag=' where ui.UserName = 'TRBAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'iWOe8KR9RluE0bPkblGyJ45hbWRY4wiiJxSordr1vt8=' where ui.UserName = 'TRCAdmin';


COMMIT;


