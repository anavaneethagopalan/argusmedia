
use crm;

#########################################################################################################
#						Add passwords for the new Admin users 											#
#	the script below relies on the users being given the same id from the the ConvertToInt function  	#
#	 	If you don't run the function or change the usernames, it will not work and the pass 			#
#							need to be set manually														#
#########################################################################################################
UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'JHkygSn3KkE/atv32mDdQm/f8IVTV+nC67BK8c/EMA0=' where ui.UserName = 'ECHAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'i1/S6eJk8QRzMUkEpVk5+Sjss2aqeX687h/xomDKDbY=' where ui.UserName = 'GR1Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'U6GyEaEJQFet9PVmitOvN/8Y/1kATY8v1TJYOa5M0q0=' where ui.UserName = 'DLTAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'W46lMK5nFKuceqIUXFFtrnWmsFTm6vxRFhRpD8Hs73U=' where ui.UserName = 'BRAAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'bolQBj4mvsGIvi0yer8Na1uW4Ua8PqsXnc7uWTGk1y4=' where ui.UserName = 'TR1Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'jROTpiYcEb2688gd2Mu/ndJ/F4MkV35OpiYXvWuma6A=' where ui.UserName = 'BR2Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'qUbbtALe7ypy25WjcrXwhWlF7DkM/xcaRdTDrvVU7ek=' where ui.UserName = 'EXCAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'UXPSv3hnZFP03ZGMsnE2jjRDYXWKOK9UFqTz/b7+f6s=' where ui.UserName = 'VICAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '9lC7l2rbOg0gXwmSYtcgVcamF6xJcouSNuCp3I+5gq0=' where ui.UserName = 'ALPAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'Nw19HvlfkyUHuz3FPVa3P3BPN1fCXUgdWadOUeXcST0=' where ui.UserName = 'WHIAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'zJhzdN2/sIbnx75s+t84Og4EVGEpUe63l6IZq8arXsM=' where ui.UserName = 'TR2Admin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'MAjGmpgjUgg0NTkZKae7zI8+loFeleQ6JzQbL6e5ZYo=' where ui.UserName = 'ZULAdmin';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'm6OgoW+W60IVUTpoSHCy3Z6j7c8Atjh2y0hb5ZVhFzI=' where ui.UserName = 'BR1Admin';



#set passwords for Johns users for WHI broker
UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'KydmeQJgURpOVKk2i08vznp2R67Jj1a7+DMbdRd84TM=' where ui.UserName = 'WS';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'JQCOPDWtYWfG1QAcisgfOEB/Ru1Eo8BSoQdeEGLwzXs=' where ui.UserName = 'WB';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'k2L4vYKoYNBKfRTKBTFSzcdcVEJ25FgTAaUbng9i1JA=' where ui.UserName = 'WJ';

#set passwords for Test Product users
UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '3PKvieUDA/GEkSomkIZ5zhakJ7MjM/VYXwyv4IleCg0=' where ui.UserName = 'editoratm';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'avN8JMQ4OQfq/S+rwCkQ3WCx1bsFlikCxTbZXk+TmiA=' where ui.UserName = 'trader1atm';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'cfgJgwBr7HoZPYGeXGZpKR0BC1/YGlrBD++bkxi8o38=' where ui.UserName = 'trader2atm';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = '/AI7QE4pkhS91I1VIMyUytI3ifAhREsziQytADWkxzI=' where ui.UserName = 'brokeratm';