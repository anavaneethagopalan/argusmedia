use crm;

START TRANSACTION;

INSERT INTO BrokerPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny)
	SELECT 
		Now() LastUpdated, -1 LastUpdatedUserId
			, o1.SubscribedProductId ProductId, o1.OrganisationId OurOrganisation
			, bs.BuySell BuyOrSell
			, o2.OrganisationId TheirOrganisation,  'A' AllowOrDeny
	FROM
		(SELECT 'B' BuySell UNION SELECT 'S') bs, 
		(SELECT o.Id OrganisationId, sp.Product_Id_fk SubscribedProductId, o.OrganisationType
		FROM   Organisation o 
		INNER JOIN SubscribedProduct sp ON sp.Organisation_Id_fk=o.Id where o.OrganisationType <> 'A') o1,

		(SELECT o.Id OrganisationId, sp.Product_Id_fk SubscribedProductId, o.OrganisationType
		FROM   Organisation o 
		INNER JOIN SubscribedProduct sp ON sp.Organisation_Id_fk=o.Id
		where o.OrganisationType <> 'A'
		) o2
	WHERE o1.SubscribedProductId=o2.SubscribedProductId and o1.OrganisationId<>o2.OrganisationId and o1.OrganisationType<>o2.OrganisationType
			
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;

INSERT INTO CounterpartyPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny)
	SELECT 
		Now() LastUpdated, -1 LastUpdatedUserId
			, o1.SubscribedProductId ProductId, o1.OrganisationId OurOrganisation
			, bs.BuySell BuyOrSell
			, o2.OrganisationId TheirOrganisation,  'A' AllowOrDeny
	FROM
		(SELECT 'B' BuySell UNION SELECT 'S') bs, -- cross product of B & S
		(SELECT o.Id OrganisationId, sp.Product_Id_fk SubscribedProductId
		FROM   Organisation o 
		INNER JOIN SubscribedProduct sp ON sp.Organisation_Id_fk=o.Id
		WHERE  o.OrganisationType='T' or o.OrganisationType='' ) o1,

		(SELECT o.Id OrganisationId, sp.Product_Id_fk SubscribedProductId
		FROM   Organisation o 
		INNER JOIN SubscribedProduct sp ON sp.Organisation_Id_fk=o.Id
		WHERE  o.OrganisationType='T' ) o2

	WHERE o1.SubscribedProductId=o2.SubscribedProductId and o1.OrganisationId<>o2.OrganisationId
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;

COMMIT;
