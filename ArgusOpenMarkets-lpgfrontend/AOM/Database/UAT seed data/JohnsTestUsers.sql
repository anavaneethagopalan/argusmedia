#################################################################
###		Script to create the Whisky test users for John           #
#################################################################

use `crm`;
START TRANSACTION ;


#CLEANUP WHI
#DELETE USERS
SET SQL_SAFE_UPDATES = 0;
DELETE FROM `UserToken` WHERE UserInfo_Id_fk IN (SELECT Id FROM `UserInfo` WHERE Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode='WHI')); 

DELETE FROM `UserInfoRole` WHERE UserInfo_Id_fk IN (SELECT Id FROM `UserInfo` WHERE Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode='WHI'));

DELETE FROM `UserCredentials` WHERE UserInfo_Id_fk IN (SELECT Id FROM `UserInfo` WHERE Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode='WHI'));

DELETE FROM `UserInfo` Where Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode='WHI');

#DELETE WHI
DELETE FROM `SystemEventNotifications`  where OrganisationId_fk in (SELECT id from Organisation where ShortCode='WHI');

DELETE FROM `ProductRolePrivilege` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode='WHI');

DELETE FROM `SubscribedProductPrivilege`  where Organisation_Id_fk in (SELECT id from Organisation where ShortCode='WHI');

DELETE FROM `SubscribedProduct`  where Organisation_Id_fk in (SELECT id from Organisation where ShortCode='WHI');

DELETE FROM `SystemRolePrivilege` where Role_Id_fk in (SELECT Id from`OrganisationRole` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode='WHI'));

DELETE FROM `ProductRolePrivilege` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode='WHI');

DELETE FROM `OrganisationRole` where Organisation_Id_fk in (SELECT id from Organisation where ShortCode='WHI');

DELETE FROM `CounterpartyPermission` where OurOrganisation IN (SELECT id from Organisation where ShortCode='WHI') or TheirOrganisation in (SELECT id from Organisation where ShortCode='WHI');

DELETE FROM `BrokerPermission` where OurOrganisation IN (SELECT id from Organisation where ShortCode='WHI') or TheirOrganisation in (SELECT id from Organisation where ShortCode='WHI');

DELETE FROM `Organisation` Where ShortCode= 'WHI';

SET SQL_SAFE_UPDATES = 1;

###################################################   Create WHI			###################################################
#Add brokerage company, Whisky (WHI).
INSERT INTO `Organisation` VALUES (999,'WHI','Whisky Brokerage','Whisky Brokerage','','catchall@argus-labs.com','B','','\0','2014-12-12 14:17:30.589000')ON DUPLICATE KEY UPDATE Id=Id;


#Add users William Smith (WS), Warren Brown (WB) and Walter Jones (WJ)
INSERT INTO `UserInfo` VALUES (9991,(SELECT id TheirId from Organisation where ShortCode='WHI'),'catchall@argus-labs.com','William Smith','Mr',NULL,'WS',1,0,0,'aomtest@argusmedia.com') ON DUPLICATE KEY UPDATE IsBlocked=IsBlocked;
INSERT INTO `UserInfo` VALUES (9992,(SELECT id TheirId from Organisation where ShortCode='WHI'),'catchall@argus-labs.com','Warren Brown','Mr',NULL,'WB',1,0,0,'aomtest@argusmedia.com') ON DUPLICATE KEY UPDATE IsBlocked=IsBlocked;
INSERT INTO `UserInfo` VALUES (9993,(SELECT id TheirId from Organisation where ShortCode='WHI'),'catchall@argus-labs.com','Walter Jones','Mr',NULL,'WJ',1,0,0,'aomtest@argusmedia.com') ON DUPLICATE KEY UPDATE IsBlocked=IsBlocked;
INSERT INTO `UserInfo` VALUES (9994,(SELECT id TheirId from Organisation where ShortCode='WHI'),'catchall@argus-labs.com','Walter Jones','Mr',NULL,'WHIAdmin',1,0,0,'aomtest@argusmedia.com') ON DUPLICATE KEY UPDATE IsBlocked=IsBlocked;

#assign credentials
INSERT INTO `UserCredentials` (`UserInfo_Id_fk`,`Password`,`FirstTimeLogin`,`DefaultExpirationInMonths`,`DateCreated`,`Expiration`,`CredentialType`)
SELECT u.Id, 'NeedToSet', 0, 12, Now(), '2015-12-01 15:00:00.000000', 'P' FROM `UserInfo` u
where u.Organisation_Id_fk in (SELECT id TheirId from Organisation where ShortCode='WHI')
ON DUPLICATE KEY UPDATE FirstTimeLogin=FirstTimeLogin;

#subscribe company to all products
INSERT INTO `SubscribedProduct` VALUES (1,(SELECT id TheirId from Organisation where ShortCode='WHI')) ON DUPLICATE KEY UPDATE Product_Id_fk = Product_Id_fk ;
INSERT INTO `SubscribedProduct` VALUES (2,(SELECT id TheirId from Organisation where ShortCode='WHI')) ON DUPLICATE KEY UPDATE Product_Id_fk = Product_Id_fk ;
INSERT INTO `SubscribedProduct` VALUES (3,(SELECT id TheirId from Organisation where ShortCode='WHI')) ON DUPLICATE KEY UPDATE Product_Id_fk = Product_Id_fk ;

# SubscribedProductPrivilege  - same as ZUL
INSERT INTO `SubscribedProductPrivilege` (Product_Id_fk, Organisation_Id_fk, ProductPrivilege_id_fk, CreatedBy, DateCreated)
SELECT res.ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI'), res.PrivId, 'AOM Admin', UTC_TIMESTAMP()
FROM ( 
		SELECT Product_Id_fk ProductId, ProductPrivilege_Id_fk PrivId 
		FROM `SubscribedProductPrivilege` spp
		Inner join `Organisation` o on o.Id = spp.Organisation_Id_fk
		WHERE o.ShortCode = 'ZUL') res
ON DUPLICATE KEY UPDATE ProductPrivilege_id_fk = ProductPrivilege_id_fk;

#create BP for BP readonly for Whisky (WHI) to see the BP
## assign same roles as ZUL

INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT orgRole.nm name, orgRole.nm descr, (SELECT Id from `Organisation` o Where o.ShortCode = 'WHI') id
FROM (
	SELECT `Name` nm from OrganisationRole where Organisation_Id_fk =(SELECT Id from Organisation where ShortCode = 'ZUL') ) orgRole
	UNION SELECT 'Broker Permissions - View Only','Broker Permissions - View Only', (SELECT Id from `Organisation` o Where o.ShortCode = 'WHI'
	)
	ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;

insert into ProductRolePrivilege (Organisation_Id_fk, Role_Id_fk, Product_Id_fk, ProductPrivilege_Id_fk)
SELECT spp.Organisation_Id_fk, (SELECT Id from OrganisationRole where `Name` = 'Broker: all products' and Organisation_Id_fk in 
		(SELECT Id from Organisation where Shortcode ='WHI')),  spp.Product_Id_fk, spp.ProductPrivilege_Id_fk 
FROM (SELECT Organisation_Id_fk, Product_Id_fk, ProductPrivilege_Id_fk FROM SubscribedProductPrivilege WHERE Organisation_Id_fk IN 
		(SELECT Id from Organisation where ShortCode = 'WHI') ) spp;


#USER INFO ROLE for Whisky (WHI) to see the BP
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole`) matrixPerms
WHERE o.ShortCode ='WHI' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

#8 CREATE SYSTEM ROLE PRIVILEGES FOR Whisky (WHI) 
INSERT INTO `SystemRolePrivilege` (Role_Id_fk, Privilege_Id_fk)
SELECT orgRole.id Role_Id, (SELECT sp.Id FROM SystemPrivilege sp WHERE sp.`Name`='BPMatrix_View') PrivId
FROM OrganisationRole orgRole
INNER JOIN `Organisation` o on o.Id = orgRole.Organisation_Id_fk
WHERE orgRole.`Name` in ('Broker Permissions - View Only') and o.shortCode = 'WHI'
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;


INSERT INTO `SystemRolePrivilege` (Role_Id_fk, Privilege_Id_fk)
SELECT orgRole.id Role_Id, (SELECT sp.Id FROM SystemPrivilege sp WHERE sp.`Name`='BPMatrix_Update') PrivId
FROM OrganisationRole orgRole
INNER JOIN `Organisation` o on o.Id = orgRole.Organisation_Id_fk
WHERE orgRole.`Name` in ('Broker Permissions - Admin') and o.shortCode = 'WHI'
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

############################################################################################################################
#######################   				SET UP 2- WAY RELATIONSHIPS 							############################
#######################   				CP AND BP PERMISSIONS tables							############################
############################################################################################################################

########################## SET UP  BROKERS - WHI, ZUL and VIC		#######################################################

#SET UP WHI - trade with ALP, and DLT, EXCLUDE BRA
INSERT INTO BrokerPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny) 
SELECT Now() LastUpdated, -1 LastUpdatedUserId, bp.ProductId, bp.OurOrg, BuyOrSell, bp.TheirOrg, 'A'
FROM (	
		#Our Org is Whi - 999
		SELECT ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ALP') TheirOrg FROM BrokerPermission
		UNION SELECT ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='DLT') TheirOrg FROM BrokerPermission
		# flip - their org is WHI - 999
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='ALP') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI')  TheirOrg FROM BrokerPermission
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='DLT')  OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg FROM BrokerPermission
		
		) bp
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;

# SET UP VIC - trade (ALP, BRA) 	not(DLT)
INSERT INTO BrokerPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny) 
SELECT Now() LastUpdated, -1 LastUpdatedUserId, bp.ProductId, bp.OurOrg, BuyOrSell, bp.TheirOrg, 'A'
FROM (	
		#Our Org is Whi - 999
		SELECT ProductId, (SELECT id TheirId from Organisation where ShortCode='VIC') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ALP') TheirOrg FROM BrokerPermission
		UNION SELECT ProductId, (SELECT id TheirId from Organisation where ShortCode='VIC') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='BRA') TheirOrg FROM BrokerPermission
		# flip - their org is WHI - 999
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='ALP') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='VIC')  TheirOrg FROM BrokerPermission
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='BRA')  OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='VIC') TheirOrg FROM BrokerPermission
		
		) bp
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;


# SET UP ZUL - trade (ALP, BRA) 	not(DLT)
INSERT INTO BrokerPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny) 
SELECT Now() LastUpdated, -1 LastUpdatedUserId, bp.ProductId, bp.OurOrg, BuyOrSell, bp.TheirOrg, 'A'
FROM (	
		#Our Org is Whi - 999
		SELECT ProductId, (SELECT id TheirId from Organisation where ShortCode='ZUL') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ALP') TheirOrg FROM BrokerPermission
		UNION SELECT ProductId, (SELECT id TheirId from Organisation where ShortCode='ZUL') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='BRA') TheirOrg FROM BrokerPermission
		# flip - their org is WHI - 999
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='ALP') OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ZUL')  TheirOrg FROM BrokerPermission
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='BRA')  OurOrg, BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ZUL') TheirOrg FROM BrokerPermission
		
		) bp
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;

#############  Asymetric permissions for ECH with WHI (Commented out until asymmetric broker permissions are supported)	#############################
#	INSERT INTO BrokerPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny) 
#		SELECT Now() LastUpdated, -1 LastUpdatedUserId, bp.ProductId, bp.OurOrg, BuyOrSell, bp.TheirOrg, bp.AllowOrDeny
#		FROM (
#		#Our Org is 999 - prod 1 B - A, S-D
#		SELECT 1 ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, 'B' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ECH') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
#		UNION SELECT 1 ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, 'S' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ECH') TheirOrg, 'D' AllowOrDeny FROM BrokerPermission
#		UNION SELECT 2 ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, 'B' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ECH') TheirOrg, 'D' AllowOrDeny FROM BrokerPermission
#		UNION SELECT 2 ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, 'S' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ECH') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
#		# flip - their org is WHI - 999
#		UNION SELECT 1 ProductId, (SELECT id TheirId from Organisation where ShortCode='ECH') OurOrg, 'B' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
#		UNION SELECT 1 ProductId, (SELECT id TheirId from Organisation where ShortCode='ECH') OurOrg, 'S' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg, 'D' AllowOrDeny FROM BrokerPermission
#		UNION SELECT 2 ProductId, (SELECT id TheirId from Organisation where ShortCode='ECH') OurOrg, 'B' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg, 'D' AllowOrDeny FROM BrokerPermission
#		UNION SELECT 2 ProductId, (SELECT id TheirId from Organisation where ShortCode='ECH') OurOrg, 'S' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
#		) bp
#ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;

#############  Symmetric allow permissions for ECH with WHI (remove if asymmetric perms are used)	#############################
	INSERT INTO BrokerPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny) 
		SELECT Now() LastUpdated, -1 LastUpdatedUserId, bp.ProductId, bp.OurOrg, BuyOrSell, bp.TheirOrg, bp.AllowOrDeny
		FROM (
		#Our Org is 999 - prod 1 B - A, S-D
		SELECT 1 ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, 'B' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ECH') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
		UNION SELECT 1 ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, 'S' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ECH') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
		UNION SELECT 2 ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, 'B' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ECH') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
		UNION SELECT 2 ProductId, (SELECT id TheirId from Organisation where ShortCode='WHI') OurOrg, 'S' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ECH') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
		# flip - their org is WHI - 999
		UNION SELECT 1 ProductId, (SELECT id TheirId from Organisation where ShortCode='ECH') OurOrg, 'B' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
		UNION SELECT 1 ProductId, (SELECT id TheirId from Organisation where ShortCode='ECH') OurOrg, 'S' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
		UNION SELECT 2 ProductId, (SELECT id TheirId from Organisation where ShortCode='ECH') OurOrg, 'B' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
		UNION SELECT 2 ProductId, (SELECT id TheirId from Organisation where ShortCode='ECH') OurOrg, 'S' BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='WHI') TheirOrg, 'A' AllowOrDeny FROM BrokerPermission
		) bp
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;



###################################################################################################################################
############################## 		SET UP TRADERS -  ALP, DLA and BRA	###########################################################
###################################################################################################################################


#SET UP ALP - trade with BRA and EXCLUDE DLT
INSERT INTO CounterpartyPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny) 
SELECT Now() LastUpdated, -1 LastUpdatedUserId, bp.ProductId, bp.OurOrg, BuyOrSell, bp.TheirOrg, 'A'
FROM (	
		SELECT ProductId, (SELECT id from Organisation where ShortCode='ALP') OurOrg ,BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='BRA') TheirOrg FROM CounterpartyPermission
		#flip
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='BRA') OurOrg ,BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ALP') TheirOrg FROM CounterpartyPermission
) bp
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;

#SET UP BRA -  trade with ALP,DLT,VIC,ZUL and exclude WHI
INSERT INTO CounterpartyPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny) 
SELECT Now() LastUpdated, -1 LastUpdatedUserId, bp.ProductId, bp.OurOrg, BuyOrSell, bp.TheirOrg, 'A'
FROM (	
		SELECT ProductId, (SELECT id from Organisation where ShortCode='BRA') OurOrg ,BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='ALP') TheirOrg FROM CounterpartyPermission
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='BRA') OurOrg ,BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='DLT') TheirOrg FROM CounterpartyPermission
		#flip
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='ALP') OurOrg ,BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='BRA') TheirOrg FROM CounterpartyPermission
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='DLT') OurOrg ,BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='BRA') TheirOrg FROM CounterpartyPermission
		
) bp
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;


#SET UP DLT -  trade with BRA and exclude ALP
INSERT INTO CounterpartyPermission (LastUpdated,LastUpdatedUserId,ProductId,OurOrganisation,BuyOrSell,TheirOrganisation,AllowOrDeny) 
SELECT Now() LastUpdated, -1 LastUpdatedUserId, bp.ProductId, bp.OurOrg, BuyOrSell, bp.TheirOrg, 'A'
FROM (	
		SELECT ProductId, (SELECT id from Organisation where ShortCode='DLT') OurOrg ,BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='BRA') TheirOrg FROM CounterpartyPermission
		#flip
		UNION SELECT ProductId, (SELECT id from Organisation where ShortCode='BRA') OurOrg ,BuyOrSell, (SELECT id TheirId from Organisation where ShortCode='DLT') TheirOrg FROM CounterpartyPermission
) bp
ON DUPLICATE KEY UPDATE LastUpdated=LastUpdated;


###################################################################################################################################
#########################		SystemEventNotifications for all products for deal confirmation			#######################
###################################################################################################################################

#Add SystemEventNotifications for all products for deal confirmation
INSERT INTO `SystemEventNotifications`(`SystemEventId`, `OrganisationId_fk`,`ProductId`, `NotificationEmailAddress`) 
SELECT 0, orgId.Id,prod.id, 'catchall@argus-labs.com'
from  ( SELECT 1 id UNION (SELECT 2 id) UNION (SELECT 3 id)) prod, (SELECT Id from Organisation) orgId
ON DUPLICATE KEY UPDATE SystemEventId = SystemEventId;


###########################   SET PASSWORD FOR THE NEW USERS    ############################


UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'KydmeQJgURpOVKk2i08vznp2R67Jj1a7+DMbdRd84TM=' where ui.UserName = 'WS';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'JQCOPDWtYWfG1QAcisgfOEB/Ru1Eo8BSoQdeEGLwzXs=' where ui.UserName = 'WB';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'k2L4vYKoYNBKfRTKBTFSzcdcVEJ25FgTAaUbng9i1JA=' where ui.UserName = 'WJ';

UPDATE `UserCredentials` uc inner join UserInfo ui on ui.Id = uc.UserInfo_Id_fk
SET `Password` = 'Xf8YiWfq5nK6vBYqo54ZQEpCNJV0OY69mvaOv5ZNZY0=' where ui.UserName = 'WHIAdmin';


##########################################################################################
#####				Set up WHIAdmin													######

#4. Insert Admin BP roles for Admin brokers
INSERT INTO `OrganisationRole`(`Name`,`Description`,`Organisation_Id_fk`)
SELECT roles.RoleName, roles.RoleName, o.Id OrganisationId

FROM `Organisation` o, `UserInfo` ui, (SELECT 'Broker Permissions - Admin' RoleName) roles
WHERE o.OrganisationType ='B' and ui.Username LIKE '%Admin%'
ON DUPLICATE KEY UPDATE `OrganisationRole`.Organisation_Id_fk=`OrganisationRole`.Organisation_Id_fk;


#6.Insert roles for brokers - BP - Admin
INSERT INTO `UserInfoRole`(`UserInfo_Id_fk`,`Role_Id_fk`)
SELECT DISTINCT u.Id,  matrixPerms.RoleId
FROM `UserInfo` u,`Organisation` o, `OrganisationRole` orgRole,
(SELECT Id RoleId, Organisation_Id_fk OrgId FROM `OrganisationRole` WHERE `Name` LIKE '%Broker Permissions - Admin%') matrixPerms
WHERE o.OrganisationType ='B' and o.Id = matrixPerms.OrgId and u.Organisation_Id_fk = matrixPerms.OrgId and 
		(u.Username LIKE '%Admin%' )
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;

INSERT INTO SystemRolePrivilege (Role_Id_fk, Privilege_Id_fk)
SELECT id Role_Id, (SELECT id FROM SystemPrivilege WHERE Name='BPMatrix_Update') FROM OrganisationRole WHERE Name in ('Broker Permissions - Admin', 'Broker Permissions - View Only')
ON DUPLICATE KEY UPDATE Role_Id_fk=Role_Id_fk;


##########################################################################################
#####                   Add Editor1 and Editor2                                      #####

INSERT INTO `UserInfo`(`Id`, `Organisation_Id_fk`, `Email`, `Name`, `Title`, `Telephone`, `Username`, `IsActive`, `IsDeleted`, `IsBlocked`, `ArgusCrmUsername`)
VALUES (
	10011,
	(SELECT Id from `Organisation` WHERE Name='Argus Media'),
	'aom.editor1@argusmedia.com',
	'Argus Editor 1',
	NULL,
	NULL,
	'Editor1',
	1,
	0,
	0,
	'aomtest@argusmedia.com')
ON DUPLICATE KEY UPDATE Id=Id;

INSERT INTO `UserInfo`(`Id`, `Organisation_Id_fk`, `Email`, `Name`, `Title`, `Telephone`, `Username`, `IsActive`, `IsDeleted`, `IsBlocked`, `ArgusCrmUsername`)
VALUES (
	10012,
	(SELECT Id from `Organisation` WHERE Name='Argus Media'),
	'aom.editor2@argusmedia.com',
	'Argus Editor 2',
	NULL,
	NULL,
	'Editor2',
	1,
	0,
	0,
	'aomtest@argusmedia.com')
ON DUPLICATE KEY UPDATE Id=Id;

# Assign user roles of Argus Editor
INSERT INTO `UserInfoRole` VALUES(10011, (SELECT Id from `OrganisationRole` WHERE Name='Argus Editor')) ON DUPLICATE KEY Update UserInfo_Id_fk=UserInfo_Id_fk;
INSERT INTO `UserInfoRole` VALUES(10012, (SELECT Id from `OrganisationRole` WHERE Name='Argus Editor')) ON DUPLICATE KEY Update UserInfo_Id_fk=UserInfo_Id_fk;

# Add user credentials
INSERT INTO `UserCredentials`(`UserInfo_Id_fk`, `Password`, `FirstTimeLogin`, `DefaultExpirationInMonths`, `DateCreated`, `Expiration`, `CredentialType`)
VALUES(10011, 'gJH9E2hzZUpsCOnBd2BkdzAEHkDLmJwdJolFXnQC4lE=', 0, 120, sysdate(), '2025-12-31 23:59:59', 'P')
ON DUPLICATE KEY Update UserInfo_Id_fk=UserInfo_Id_fk;

INSERT INTO `UserCredentials`(`UserInfo_Id_fk`, `Password`, `FirstTimeLogin`, `DefaultExpirationInMonths`, `DateCreated`, `Expiration`, `CredentialType`)
VALUES(10012, 'hQsepgx4HVoJE/bs+7ZXEKSJnFrn1FKnMlp9/manbFE=', 0, 120, sysdate(), '2025-12-31 23:59:59', 'P')
 ON DUPLICATE KEY Update UserInfo_Id_fk=UserInfo_Id_fk;


##########################################################################################
#####                                   FIN                                          #####

COMMIT;
#ROLLBACK;




