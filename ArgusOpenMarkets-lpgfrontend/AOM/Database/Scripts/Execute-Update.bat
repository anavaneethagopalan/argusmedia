::execute a liquibase update command
Set dbname=%1
SET targetEnvironment=%2

IF "%3"==""  (
  set dbFolderName=%1
) ELSE (
  set dbFolderName=%3
)

IF "%dbname%x"=="x" GOTO MISSINGDBNAME
SET schema_folder=%~dp0..\Schemas\%dbFolderName%

call %~dp0mysql_config.bat %targetEnvironment%
call %~dp0liquibase_config.bat

SET dbChangelog=db-%dbFolderName%-master-1.0.xml
SET connectionUrl=%mysqlurl%/%dbname%

SET datacontext="seed,test,devonly"

pushd %schema_folder%\
call %liquibase_exe% --contexts=%datacontext% --changeLogFile=%dbChangelog% --username=%mysql_user% --password=%mysql_password% --url=%connectionUrl% --driver=%driver% --logLevel=%log_level% update
popd
exit /b errorlevel


