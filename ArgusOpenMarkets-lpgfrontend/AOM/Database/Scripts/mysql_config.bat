@echo OFF

::set context variables for mysql execution
::change to match local settings
SET mysql_host="localhost"
SET mysql_user="root"
SET mysql_password="root"

REM Following supports using a default override for a specific computer if environment variable isn't set

SET propsFile=..\properties\%COMPUTERNAME%_mysql_config.bat

IF NOT "%1x"=="x" SET propsFile=..\properties\%1_mysql_config.bat
ECHO Environment File=%propsFile%

REM If the user specified an environment and the file is missing then error
IF NOT "%1x"=="x" (
	IF NOT EXIST %propsFile% GOTO PROPSFILEMISSING
)
IF EXIST %propsFile% CALL %propsFile%

SET mysqlurl=jdbc:mysql://%mysql_host%:3306

GOTO END

:PROPSFILEMISSING
ECHO TARGET ENVIRONMENT BAT FILE %propsFile% IS MISSING
EXIT /b 1

:END
EXIT /b 0