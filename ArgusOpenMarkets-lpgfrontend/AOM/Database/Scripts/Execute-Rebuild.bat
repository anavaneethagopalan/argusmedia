@echo off
::rebuild the aom db and run the change log in config for liquibase, this can be configured to run a single change log
::this will execute a migrate order for liquibase

SET dbname=%1
SET targetEnvironment=%2

IF "%3"==""  (
  set dbFolderName=%1
) ELSE (
  set dbFolderName=%3
)

IF "%dbname%x"=="x" GOTO MISSINGDBNAME
IF "%dbFolderName%x"=="x" GOTO MISSINGFOLDERNAME

IF "%dbFolderName%x" == "x" SET %dbFolderName% = %dbname%
SET schema_folder=%~dp0..\Schemas\%dbFolderName%

echo DB NAME - %dbname%
echo DB FOLDER NAME - %dbFolderName%
@ECHO SCHEMA FOLDER - %schema_folder%

call %~dp0mysql_config.bat %targetEnvironment%
IF ERRORLEVEL 1 GOTO FAILEDTOSETENVIRONMENT
call %~dp0Recreate-db.bat %dbName% %dbFolderName%
call %~dp0liquibase_config.bat

SET dbChangelog=db-%dbFolderName%-master-1.0.xml
SET connectionUrl=%mysqlurl%/%dbname%
SET outputFolder=%~dp0..\Output

pushd %schema_folder%\
call %liquibase_exe% --username=%mysql_user% --password=%mysql_password% --url=%connectionUrl% --driver=%driver% --logLevel=%log_level% dropAll 

REM Migration script to be obtained from normal service build
REM call %liquibase_exe% --contexts=%datacontext% --changeLogFile=%dbChangelog% --username=%mysql_user% --password=%mysql_password% --url=%connectionUrl% --driver=%driver% --logLevel=%log_level% updateSQL > %outputFolder%\%dbname%.full.sql

SET datacontext="seed,test,devonly"

call %liquibase_exe% --contexts=%datacontext% --changeLogFile=%dbChangelog% --username=%mysql_user% --password=%mysql_password% --url=%connectionUrl% --driver=%driver% --logLevel=%log_level% migrate
popd
exit /b errorlevel

:MISSINGDBNAME
@ECHO DBNAME IS MISSING
GOTO ERROREND

:MISSINGFOLDERNAME
@ECHO DBFOLDERNAME IS MISSING
GOTO ERROREND

:FAILEDTOSETENVIRONMENT
@ECHO FAILED TO SET ENVIRNOMENT, ABORTING
GOTO ERROREND

:ERROREND
exit /b 1