::set context variables for liquibase execution
SET version=3.2.2
SET driver=com.mysql.jdbc.Driver
SET log_level=INFO
::default contexts set to run all changes sets
::combinations for contexts 
::"test" - just run test and those without a context 
::"seed" - just run seed and those without a context 
::"test,seed" - run seed and test contexts and those without a context
SET datacontext="seed,test" 
SET liquibase_exe=%~dp0..\bin\Liquibase\%version%\liquibase