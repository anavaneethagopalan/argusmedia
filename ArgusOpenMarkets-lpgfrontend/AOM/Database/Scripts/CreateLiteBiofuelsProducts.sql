START TRANSACTION;

# SQL Safe updates
SET @currentSafeUpdate=@@sql_safe_updates;
SET @@sql_safe_updates=0;

SET autocommit=0;

###################################################################
# Market and Commodity
###################################################################
INSERT INTO `aom`.`Market`(`Name`, `DateCreated`) VALUES('FOB ARA', sysdate());
INSERT INTO `aom`.`CommodityGroup`(`Name`, `CommodityClass_Id_fk`) SELECT 'Biofuels', Id FROM `aom`.`CommodityClass` WHERE Name='Energy';
INSERT INTO `aom`.`CommodityType`(`Name`, `CommodityGroup_Id_fk`) SELECT 'Biodiesel', Id FROM `aom`.`CommodityGroup` WHERE Name='Biofuels';

INSERT INTO `aom`.`Commodity`(`Name`, `CommodityType_Id_fk`, `DateCreated`, `LastUpdatedUser_Id`, `EnteredByUser_Id`, `LastUpdated`)
SELECT 'Rapeseed RME', Id , sysdate(), -1, -1, sysdate() FROM `aom`.`CommodityType` WHERE Name='Biodiesel';

INSERT INTO `aom`.`Commodity`(`Name`, `CommodityType_Id_fk`, `DateCreated`, `LastUpdatedUser_Id`, `EnteredByUser_Id`, `LastUpdated`)
SELECT 'FAME 0', Id , sysdate(), -1, -1, sysdate() FROM `aom`.`CommodityType` WHERE Name='Biodiesel';


###################################################################
# Commodity Content Streams
###################################################################
SET @biofuelsContentStreamTableId=(SELECT (max(Id) +1) FROM `crm`.`ContentStream`);
INSERT INTO `crm`.`ContentStream`(`Id`, `Description`, `ContentStreamId`) VALUES (@biofuelsContentStreamTableId, 'Argus Biofuels', 95074);
INSERT INTO `aom`.`CommodityContentStreams`(`CommodityId`, `ContentStreamId`) VALUES ((SELECT Id FROM `aom`.`Commodity` WHERE Name='Rapeseed RME'), 95074);
INSERT INTO `aom`.`CommodityContentStreams`(`CommodityId`, `ContentStreamId`) VALUES ((SELECT Id FROM `aom`.`Commodity` WHERE Name='FAME 0'), 95074);


###################################################################
# Contract type
###################################################################
INSERT INTO `aom`.`ContractType` (`Name`, `DateCreated`, `LastUpdatedUser_Id`, `EnteredByUser_Id`, `LastUpdated`) 
VALUES ('Diff', sysdate(), '-1', '-1', sysdate());


###################################################################
# Products
###################################################################
INSERT INTO `aom`.`Product`(
`Name`, `IsDeleted`, `IsInternal`, `LastUpdated`, `LastUpdated_User_Id`, `Commodity_Id_fk`, `Market_Id_fk`, `Contracttype_Id_fk`, `Location_Id_fk`, `Currency_Code_fk`, `VolumeUnit_code_fk`, `PriceUnit_Code_fk`, `OrderVolumeDefault`, `OrderVolumeMinimum`, `OrderVolumeMaximum`, `OrderVolumeIncrement`, `OrderVolumeDecimalPlaces`, `OrderVolumeSignificantFigures`, `OrderPriceMinimum`, `OrderPriceMaximum`, `OrderPriceIncrement`, `OrderPriceDecimalPlaces`, `OrderPriceSignificantFigures`, `BidAskStackNumberRows`, `Status`, `LastOpenDate`, `LastCloseDate`, `OpenTime`, `CloseTime`, `DateCreated`, `PurgeFrequency`, `PurgeDate`, `PurgeTimeOfDay`,`IgnoreAutomaticMarketStatusTriggers`, `AllowNegativePriceForExternalDeals`, `DisplayOptionalPriceDetail`) 
VALUES(
	'RED RME fob ARA range',
    0,
    0,
    sysdate(),
    -1,
    (SELECT Id FROM `aom`.`Commodity` WHERE Name='Rapeseed RME'),
    (SELECT Id FROM `aom`.`Market` WHERE Name='FOB ARA'),
    (SELECT Id FROM `aom`.`ContractType` WHERE Name='Diff'),
	(SELECT Id FROM `aom`.`DeliveryLocation` WHERE Name='Rotterdam'),
    'USD',
    'MT',
    'MT',
    1000,
    1000,
    5000,
    100,
    0,
    99,
    1,
    99999,
    0.25,
    2,
    8,
    6,
    'O',
    sysdate(),
    sysdate(),
	'08:00:00',
	'17:30:00',
	sysdate(),
	7,
	'2014-09-19 00:00:00.000000',
	'20:00:00',
	1,
	1,
	0
);

INSERT INTO `aom`.`Product`(
`Name`, `IsDeleted`, `IsInternal`, `LastUpdated`, `LastUpdated_User_Id`, `Commodity_Id_fk`, `Market_Id_fk`, `Contracttype_Id_fk`, `Location_Id_fk`, `Currency_Code_fk`, `VolumeUnit_code_fk`, `PriceUnit_Code_fk`, `OrderVolumeDefault`, `OrderVolumeMinimum`, `OrderVolumeMaximum`, `OrderVolumeIncrement`, `OrderVolumeDecimalPlaces`, `OrderVolumeSignificantFigures`, `OrderPriceMinimum`, `OrderPriceMaximum`, `OrderPriceIncrement`, `OrderPriceDecimalPlaces`, `OrderPriceSignificantFigures`, `BidAskStackNumberRows`, `Status`, `LastOpenDate`, `LastCloseDate`, `OpenTime`, `CloseTime`, `DateCreated`, `PurgeFrequency`, `PurgeDate`, `PurgeTimeOfDay`,`IgnoreAutomaticMarketStatusTriggers`, `AllowNegativePriceForExternalDeals`, `DisplayOptionalPriceDetail`) 
VALUES(
    'RED FAME 0 fob ARA range',
    0,
    0,
    sysdate(),
    -1,
    (SELECT Id FROM `aom`.`Commodity` WHERE Name='FAME 0'),
    (SELECT Id FROM `aom`.`Market` WHERE Name='FOB ARA'),
    (SELECT Id FROM `aom`.`ContractType` WHERE Name='Diff'),
	(SELECT Id FROM `aom`.`DeliveryLocation` WHERE Name='Rotterdam'),
    'USD',
    'MT',
    'MT',
    1000,
    1000,
    5000,
    100,
    0,
    99,
    1,
    99999,
    0.25,
    2,
    8,
    6,
    'O',
    sysdate(),
    sysdate(),
	'08:00:00',
	'17:30:00',
	sysdate(),
	7,
	'2014-09-19 00:00:00.000000',
	'20:00:00',
	1,
	1,
	0
);

INSERT INTO `aom`.`Product`(
`Name`, `IsDeleted`, `IsInternal`, `LastUpdated`, `LastUpdated_User_Id`, `Commodity_Id_fk`, `Market_Id_fk`, `Contracttype_Id_fk`, `Location_Id_fk`, `Currency_Code_fk`, `VolumeUnit_code_fk`, `PriceUnit_Code_fk`, `OrderVolumeDefault`, `OrderVolumeMinimum`, `OrderVolumeMaximum`, `OrderVolumeIncrement`, `OrderVolumeDecimalPlaces`, `OrderVolumeSignificantFigures`, `OrderPriceMinimum`, `OrderPriceMaximum`, `OrderPriceIncrement`, `OrderPriceDecimalPlaces`, `OrderPriceSignificantFigures`, `BidAskStackNumberRows`, `Status`, `LastOpenDate`, `LastCloseDate`, `OpenTime`, `CloseTime`, `DateCreated`, `PurgeFrequency`, `PurgeDate`, `PurgeTimeOfDay`,`IgnoreAutomaticMarketStatusTriggers`, `AllowNegativePriceForExternalDeals`, `DisplayOptionalPriceDetail`, `HasAssessment`) 
VALUES(
    'Other Biofuel',
    0,
    1,
    sysdate(),
    -1,
    (SELECT Id FROM `aom`.`Commodity` WHERE Name='Other'),
    (SELECT Id FROM `aom`.`Market` WHERE Name='FOB ARA'),
    (SELECT Id FROM `aom`.`ContractType` WHERE Name='Diff'),
	-1,
    'USD',
    'MT',
    'MT',
    1000,
    0,
    99999999.999999,
    1,
    0,
    99,
    -999999.000000,
    999999.000000,
    0.01,
    2,
    8,
    6,
    'O',
    sysdate(),
    sysdate(),
	'08:00:00',
	'17:30:00',
	sysdate(),
	7,
	'2014-09-19 00:00:00.000000',
	'20:00:00',
	0,
	1,
	1,
	0
);


###################################################################
# Product Tenors
###################################################################
INSERT INTO `aom`.`ProductTenor`(`Name`, `DateCreated`, `LastUpdated`, `LastUpdated_User_Id`, `DisplayName`, `DeliveryDateStart`, `DeliveryDateEnd`, `RollDate`, `RollDateRule`, `Product_Id_fk`, `MinimumDeliveryRange`) 
VALUES (
	'Prompt', UTC_DATE(), sysdate(), -1, 'Prompt', '+7d', '+28d', 'DAILY', 'ACTUAL', (SELECT Id FROM `aom`.`Product` WHERE Name='RED RME fob ARA range'), 3
);
INSERT INTO `aom`.`ProductTenor`(`Name`, `DateCreated`, `LastUpdated`, `LastUpdated_User_Id`, `DisplayName`, `DeliveryDateStart`, `DeliveryDateEnd`, `RollDate`, `RollDateRule`, `Product_Id_fk`, `MinimumDeliveryRange`) 
VALUES (
	'Prompt', UTC_DATE(), sysdate(), -1, 'Prompt', '+7d', '+28d', 'DAILY', 'ACTUAL', (SELECT Id FROM `aom`.`Product` WHERE Name='RED FAME 0 fob ARA range'), 3
);

###################################################################
# Create temp tables
###################################################################

CREATE TEMPORARY TABLE IF NOT EXISTS `aom`.`BiofuelsProds_1349` AS
SELECT * FROM (
	SELECT (SELECT Id FROM `aom`.`Product` WHERE Name='RED RME fob ARA range') AS Id, 'RED RME fob ARA range' AS Name
	UNION 
	SELECT (SELECT Id FROM `aom`.`Product` WHERE Name='RED FAME 0 fob ARA range') AS Id, 'RED FAME 0 fob ARA range' AS Name
    UNION
    SELECT (SELECT Id FROM `aom`.`Product` WHERE Name='Other Biofuel') AS Id, 'Other Biofuel' AS Name
) AS tmp;

CREATE TEMPORARY TABLE IF NOT EXISTS `crm`.`LiteUserPrivs_1349` AS
SELECT * FROM `crm`.`ProductPrivilege` WHERE Name IN ('View_Assessment_Widget', 'View_MarketTicker_Widget', 'View_NewsAnalysis_Widget', 'MarketTicker_PlusInfo_Create');

############################################################################
# Need to add the subscriptions for the new products/privs to Argus/Editors
############################################################################
INSERT INTO `crm`.`SubscribedProduct`(`Product_Id_fk`, `Organisation_Id_fk`)
SELECT * FROM (
	SELECT P.Id as PrId, O.Id as OrgId
	FROM `aom`.`BiofuelsProds_1349` as P, `crm`.`Organisation` as O
	WHERE O.Name LIKE '%Argus Media%'
) as tmp;

INSERT INTO `crm`.`SubscribedProductPrivilege`(`Product_Id_fk`, `Organisation_Id_fk`, `ProductPrivilege_id_fk`, `CreatedBy`, `DateCreated`)
SELECT PrId, OrgId, PPId, 'System', sysdate() FROM (
	SELECT P.Id AS PrId, O.Id as OrgId, PP.Id as PPId 
	FROM `aom`.`BiofuelsProds_1349` as P, `crm`.`Organisation` as O, `crm`.`ProductPrivilege` as PP
	WHERE O.Name LIKE '%Argus Media%'
) as tmp;


###################################################################
# Add the new product privs for the processor user
# Since we've already subscribed to the privileges then we just
###################################################################
INSERT INTO `crm`.`ProductRolePrivilege`(`Organisation_Id_fk`, `Role_Id_fk`, `Product_Id_fk`, `ProductPrivilege_Id_fk`)
SELECT * FROM (
	SELECT O.Id as OrgId, R.Id as RoleId, P.Id as ProdId, PP.Id as PrivId
	FROM `crm`.`Organisation` as O, `crm`.`OrganisationRole` as R, `aom`.`BiofuelsProds_1349` as P, `crm`.`ProductPrivilege` as PP
    WHERE O.Name LIKE '%Argus Media%' AND
    	  R.Name='Super Dev/Test User'
) as tmp
ON DUPLICATE KEY UPDATE `Organisation_Id_fk`=`Organisation_Id_fk`;

###################################################################
# Cleanup
###################################################################
DROP TABLE `crm`.`LiteUserPrivs_1349`;
DROP TABLE `aom`.`BiofuelsProds_1349`;


SET @@sql_safe_updates = @currentSafeUpdate;

COMMIT;