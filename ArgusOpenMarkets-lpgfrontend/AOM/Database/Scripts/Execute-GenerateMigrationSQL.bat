@echo off
::
::

Set dbname=%1
SET targetEnvironment=%2

IF "%3"==""  (
  set dbFolderName=%1
) ELSE (
  set dbFolderName=%3
)

IF "%dbname%x"=="x" GOTO MISSINGDBNAME
IF "%dbFolderName%x"=="x" GOTO MISSINGFOLDERNAME

IF "%dbFolderName%x" == "x" SET %dbFolderName% = %dbname%
SET schema_folder=%~dp0..\Schemas\%dbFolderName%

echo DB NAME - %dbname%
echo DB FOLDER NAME - %dbFolderName%

call %~dp0mysql_config.bat %targetEnvironment%
REM call %~dp0Recreate-db.bat %dbName% %dbFolderName%
call %~dp0liquibase_config.bat

SET dbChangelog=db-%dbFolderName%-master-1.0.xml
SET connectionUrl=%mysqlurl%/%dbname%

SET outputFolder=%~dp0..\Output
SET schemaSqlfile=%outputFolder%\%fileprefix%%dbFolderName%-%buildnumber%.sql
IF "%buildnumber%x"=="x" SET schemaSqlfile=%outputFolder%\%dbname%-NoBuildNumber.sql

REM we exclude "devonly" and "test" context 
SET datacontext="seed"

echo Generating sql file: %schemaSqlfile%
pushd %schema_folder%\
call %liquibase_exe% --contexts=%datacontext%  --changeLogFile=%dbChangelog% --username=%mysql_user% --password=%mysql_password% --url=%connectionUrl% --driver=%driver% --logLevel=%log_level% updateSQL > %schemaSqlfile%
popd
exit /b errorlevel

:MISSINGDBNAME