INSERT INTO `crm.master`.SubscribedProductPrivilege
SELECT * FROM (
	SELECT p.Id, 1, (SELECT Id from `crm.master`.ProductPrivilege WHERE `Name`='ExternalOrder_Create') as ppId, 'system', '2015-12-07' FROM `aom.master`.Product as p
) as tmp;

INSERT INTO `crm.master`.ProductRolePrivilege(`Organisation_Id_fk`, `Role_Id_fk`, `Product_Id_fk`, `ProductPrivilege_Id_fk`)
SELECT * FROM (
	SELECT 1 as orgId, 1 as roleId, p.Id as productId, (SELECT Id from `crm.master`.ProductPrivilege WHERE `Name`='ExternalOrder_Create') as ppId FROM `aom.master`.Product as p
) as tmp;