::recreate requested db

Set dbname=%1
Set dbFolderName=%2
IF "%dbname%x"=="x" GOTO MISSINGDBNAME
IF "%dbFolderName%x"=="x" SET %dbFolderName = %1

SET recreate_sql=%~dp0\..\Schemas\%dbFolderName%\recreate-%dbname%.sql
SET mysqlexe=mysql.exe
IF EXIST "C:\Program Files\MySQL\MySQL Server 5.6\bin\mysql.exe" SET mysqlexe="C:\Program Files\MySQL\MySQL Server 5.6\bin\mysql.exe"

IF NOT EXIST %mysqlexe% GOTO MISSING_MYSQL
@ECHO EXECUTING RECREATE DB SQL SCRIPT - %recreate_sql%
call %mysqlexe% -h%mysql_host% -u%mysql_user% -p%mysql_password% < %recreate_sql%
GOTO END

:MISSING_MYSQL
@ECHO MySQL is not installed locally

:MISSINGDBNAME
:END
