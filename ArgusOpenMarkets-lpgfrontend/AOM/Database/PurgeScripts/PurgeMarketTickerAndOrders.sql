SET SQL_SAFE_UPDATES=0;

DELETE FROM aom.MarketTickerItem_Log;
COMMIT;

DELETE FROM aom.MarketTickerItem;
COMMIT;

DELETE FROM aom.`Order_Log`;
COMMIT;

DELETE FROM aom.`Order`;
COMMIT;

DELETE FROM aom.Deal_Log;
COMMIT;

DELETE FROM aom.Deal;
COMMIT;

DELETE FROM aom.MarketInfoItem_Log;
COMMIT;

DELETE FROM aom.MarketInfoItem;