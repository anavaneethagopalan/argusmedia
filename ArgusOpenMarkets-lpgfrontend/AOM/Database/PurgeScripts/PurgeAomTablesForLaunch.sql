SET SQL_SAFE_UPDATES=0;

use `aom`;

DELETE FROM MarketTickerItemProductChannel;

DELETE FROM MarketTickerItem_Log;
DELETE FROM MarketTickerItem;
COMMIT;

DELETE FROM ExternalDeal_Log;
DELETE FROM ExternalDeal;
COMMIT;

DELETE FROM Deal_Log;
DELETE FROM Deal;
COMMIT;

DELETE FROM Order_Log;
DELETE FROM `Order`;
COMMIT;

DELETE FROM MarketInfoProductChannel;

DELETE FROM MarketInfoItem_Log;
DELETE FROM MarketInfoItem;
COMMIT;

DELETE FROM Assessment_Log;
DELETE FROM Assessment;
COMMIT;

DELETE FROM Product_Log;
DELETE FROM ProductTenor_Log;
COMMIT;

SET SQL_SAFE_UPDATES=1;