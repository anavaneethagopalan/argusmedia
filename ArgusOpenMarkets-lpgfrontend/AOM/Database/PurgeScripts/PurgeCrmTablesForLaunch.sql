SET SQL_SAFE_UPDATES=0;

DELETE FROM crm.AuthenticationHistory;
COMMIT;

-- DELETE FROM crm.CanTradeWith_Log;
-- DELETE FROM crm.CanTradeWith;
-- COMMIT;

DELETE FROM crm.Organisation_Log;
DELETE FROM crm.Organisation;
COMMIT;

DELETE FROM crm.OrganisationRole_Log;
DELETE FROM crm.OrganisationRole;
COMMIT;

DELETE FROM crm.ProductRolePrivilege_Log;
-- DELETE FROM crm.ProductRolePrivilege;
COMMIT;

DELETE FROM crm.SubscribedProduct_Log;
-- DELETE FROM crm.SubscribedProduct;
COMMIT;

DELETE FROM crm.SubscribedProductPrivilege_Log;
DELETE FROM crm.SubscribedProductPrivilege;
COMMIT;

DELETE FROM crm.UserCredentials_Log;
DELETE FROM crm.UserCredentials;
COMMIT;

DELETE FROM crm.UserInfo_Log;
DELETE FROM crm.UserInfo;
COMMIT;

DELETE FROM crm.UserInfoRole_Log;
DELETE FROM crm.UserInfoRole;
COMMIT;

DELETE FROM crm.UserToken;
COMMIT;

-- SHOULD WE CLEAR OUT THIS TABLE?  
-- DELETE FROM crm.UserModule;
-- COMMIT;

SET SQL_SAFE_UPDATES=1;