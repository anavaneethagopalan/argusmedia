SET SQL_SAFE_UPDATES = 0;

-- NOTE: Change the Database you're using for different DB Schemas.  
USE `crm`;

UPDATE Organisation SET Email = 'catchall@argus-labs.com';

UPDATE SystemEventNotifications SET NotificationEmailAddress = 'catchall@argus-labs.com';

UPDATE UserInfo SET Email = 'catchall@argus-labs.com';

UPDATE `aom`.Email SET Recipient = 'catchall@argus-labs.com';

SET SQL_SAFE_UPDATES = 1;