LOCK TABLES `MetaDataType` WRITE;
INSERT INTO `MetaDataType` VALUES 
    (10,'IntegerEnum'),
    (20,'String');
UNLOCK TABLES;


-- METADATALIST
LOCK TABLES `MetaDataList` WRITE;
INSERT INTO `MetaDataList` (Description, IsDeleted)
VALUES 
    ('ArgusTestMarketList','\0');
UNLOCK TABLES;

select id into @MetadataListTest from `MetaDataList` where Description = 'ArgusTestMarketList';


-- METADATALISTITEM
LOCK TABLES `MetaDataListItem` WRITE;
INSERT INTO `MetaDataListItem` (MetaDataList_Id_fk, DisplayOrder, ValueText, ValueInt, IsDeleted) 
VALUES 
    (@MetadataListTest,1,'Test Item 1',1,'\0'),
    (@MetadataListTest,2,'Test Item 2',2,'\0'),
    (@MetadataListTest,3,'Test Item 3',3,'\0');
UNLOCK TABLES;

select id into @ProductTestMarket from `Product` where name = 'Argus Test Market';

-- PRODUCTMETADATAITEM
LOCK TABLES `ProductMetaDataItem` WRITE;
INSERT INTO `ProductMetaDataItem` (Product_Id_fk, DisplayOrder, DisplayName, MetaDataType_Id_fk, MetaDataList_Id_fk, ValueMinimum, ValueMaximum, IsDeleted, LastUpdated) 
VALUES 
    (@ProductTestMarket,1,'Test List',10,@MetadataListTest,NULL,NULL,'\0',now()),
    (@ProductTestMarket,2,'Test String',20,NULL,5,100,'\0',now());
UNLOCK TABLES;


