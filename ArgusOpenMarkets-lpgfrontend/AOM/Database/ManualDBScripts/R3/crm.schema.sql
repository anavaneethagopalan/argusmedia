DROP TABLE IF EXISTS `DisconnectHistory`;
			
			CREATE TABLE `DisconnectHistory` (
				`Id` int(10) NOT NULL AUTO_INCREMENT,
				`UserId` BIGINT(20) NOT NULL, 
				`DisconnectReason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
				`DateCreated` datetime(6) NOT NULL,
				PRIMARY KEY (`Id`)
			) ENGINE=InnoDB AUTO_INCREMENT=4119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;