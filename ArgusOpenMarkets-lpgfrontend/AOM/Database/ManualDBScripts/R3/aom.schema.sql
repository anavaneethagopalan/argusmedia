
ALTER TABLE `Order`
ADD COLUMN `MetaData` VARCHAR(5000) NULL DEFAULT NULL AFTER `CoBrokering`;

ALTER TABLE `Order_Log`
ADD COLUMN `MetaData` VARCHAR(5000) NULL DEFAULT NULL AFTER `CoBrokering`;

ALTER TABLE `ExternalDeal`
ADD COLUMN `MetaData` VARCHAR(5000) NULL DEFAULT NULL AFTER `PreviousDealStatus`;

ALTER TABLE `ExternalDeal_Log`
ADD COLUMN `MetaData` VARCHAR(5000) NULL DEFAULT NULL AFTER `PreviousDealStatus`;

ALTER TABLE `MarketTickerItem` MODIFY `Message` VARCHAR(2000);        
ALTER TABLE `MarketTickerItem_Log` MODIFY `Message` VARCHAR(2000);


UNLOCK TABLES;
DROP TABLE if exists `ProductMetaDataItem`;
DROP TABLE if exists `ProductMetaDataItem_Log`;
DROP TABLE if exists `MetaDataListItem`;
DROP TABLE if exists `MetaDataListItem_Log`;
DROP TABLE if exists `MetaDataList`;
DROP TABLE if exists `MetaDataList_Log`;
DROP TABLE if exists `MetaDataType`;

CREATE TABLE `MetaDataType` (
    `Id` bigint(20) NOT NULL,	
    `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,						
    PRIMARY KEY (`Id`)
);
    
CREATE TABLE `MetaDataList` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,	
    `Description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,		
    PRIMARY KEY (`Id`),
    UNIQUE KEY `idx_Description` (`Description`)
);

CREATE TABLE `MetaDataList_Log` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,	
    `Description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`, `LastUpdated`)
);		

CREATE TABLE `MetaDataListItem` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `MetaDataList_Id_fk` bigint(20) NOT NULL,
    `DisplayOrder` bigint(20) NOT NULL DEFAULT 0,
    `ValueText` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `ValueInt` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`),
    CONSTRAINT `fk_MetaDataList` FOREIGN KEY (`MetaDataList_Id_fk`) REFERENCES `MetaDataList` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `MetaDataListItem_Log` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `MetaDataList_Id_fk` bigint(20) NOT NULL,
    `DisplayOrder` bigint(20) NOT NULL DEFAULT 0,
    `ValueText` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `ValueInt` bigint(20) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`, `LastUpdated`)
);

CREATE TABLE `ProductMetaDataItem` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `Product_Id_fk` bigint(20) NOT NULL,
    `DisplayOrder` bigint(20) NOT NULL DEFAULT 0,
    `DisplayName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `MetaDataType_Id_fk`  bigint(20) NOT NULL,
    `MetaDataList_Id_fk`  bigint(20) NULL,
    `ValueMinimum`  bigint(20) NULL,
    `ValueMaximum`  bigint(20) NULL,
    `IsDeleted` bit(1) NOT NULL DEFAULT 0,
    `LastUpdated` datetime(6) NOT NULL,
    PRIMARY KEY (`Id`),
    UNIQUE KEY `idx_ProductNameTypeList` (`Product_Id_fk`,`DisplayName`,`MetaDataList_Id_fk`,`MetaDataType_Id_fk`),
    CONSTRAINT `fk_ProductId` FOREIGN KEY (`Product_Id_fk`) REFERENCES `Product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `fk_MetaDataType` FOREIGN KEY (`MetaDataType_Id_fk`) REFERENCES `MetaDataType` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ProductMetaDataItem_Log` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `Product_Id_fk` bigint(20) NOT NULL,
    `DisplayOrder` bigint(20) NOT NULL DEFAULT 0,
    `DisplayName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `MetaDataType_Id_fk`  bigint(20) NOT NULL,
    `MetaDataList_Id_fk`  bigint(20) NULL,
    `ValueMinimum`  bigint(20) NULL,
    `ValueMaximum`  bigint(20) NULL,
    `IsDeleted` bit(1) NOT NULL DEFAULT 0,    
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`, `LastUpdated`)
);

ALTER TABLE `MetaDataListItem` ADD COLUMN `IsDeleted` BIT DEFAULT 0;        
ALTER TABLE `MetaDataListItem_Log` ADD COLUMN `IsDeleted` BIT DEFAULT 0;

ALTER TABLE `MetaDataList` ADD COLUMN `IsDeleted` BIT DEFAULT 0;        
ALTER TABLE `MetaDataList_Log` ADD COLUMN `IsDeleted` BIT DEFAULT 0;
