--  ALTER TABLE `crm`.`UserLoginAttempts` DEFAULT COLLATE utf8_unicode_ci;
-- 
-- SHOW TABLE STATUS FROM `crm`;
-- 
--  SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, COLLATION_NAME
--  FROM INFORMATION_SCHEMA.COLUMNS
--  where table_schema in ('crm','crm.demo')
--  and table_name = 'UserLoginAttempts';


use `crm`;

DROP PROCEDURE IF EXISTS DropForeignKeyIfExists;
DROP PROCEDURE IF EXISTS DropColumnIfExists;

DELIMITER $$

CREATE PROCEDURE DropForeignKeyIfExists(IN tableName VARCHAR(64), IN constraintName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.table_constraints
        WHERE  table_schema = DATABASE() 
        AND    table_name = tableName collate utf8_unicode_ci
        AND    constraint_name = constraintName collate utf8_unicode_ci
        AND    constraint_type = 'FOREIGN KEY')
    THEN
        SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP FOREIGN KEY ', constraintName, ';');
        -- SELECT @query;
        PREPARE stmt FROM @query; 
        EXECUTE stmt; 
        DEALLOCATE PREPARE stmt; 
    END IF; 
END$$

CREATE PROCEDURE DropColumnIfExists(IN tableName VARCHAR(64), IN columnName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.columns
        WHERE  table_schema = DATABASE()
        AND    table_name = tableName collate utf8_unicode_ci
        AND    column_name = columnName collate utf8_unicode_ci) 
    THEN
        SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP COLUMN ', columnName, ';');
        -- SELECT @query;
        PREPARE stmt FROM @query; 
        EXECUTE stmt; 
        DEALLOCATE PREPARE stmt; 
    END IF; 
END$$

DELIMITER ;

CALL DropForeignKeyIfExists('UserLoginAttempts', 'FK_UserLoginAttempts_UserLoginSource');
CALL DropForeignKeyIfExists('AuthenticationHistory', 'FK_AuthenticationHistory_UserLoginSource');
CALL DropForeignKeyIfExists('UserLoginSource', 'FK_UserLoginSource_OrganisationId');            
CALL DropForeignKeyIfExists('UserLoginExternalAccount', 'FK_ExternalAccount_UserLoginSource');
CALL DropForeignKeyIfExists('UserLoginExternalAccount', 'FK_ExternalAccount_UserInfoId');

DROP TABLE IF EXISTS `UserLoginSource`;
CREATE TABLE `UserLoginSource` (
    Id BIGINT NOT NULL AUTO_INCREMENT,
    Code VARCHAR(10) NOT NULL,
    Name VARCHAR(100) NOT NULL,
    Organisation_Id_fk BIGINT NOT NULL,
    UNIQUE INDEX `Code_UNIQUE` (`Code` ASC),
    UNIQUE INDEX `Name_UNIQUE` (`Name` ASC),
    UNIQUE INDEX `Id_UNIQUE` (`Id` ASC),
    PRIMARY KEY (`Id`),
    CONSTRAINT `FK_UserLoginSource_OrganisationId`
    FOREIGN KEY (`Organisation_Id_fk`) REFERENCES `Organisation` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION);


CALL DropColumnIfExists('UserLoginAttempts', 'UserLoginSource_Id_fk');
ALTER TABLE `UserLoginAttempts` 
ADD COLUMN `UserLoginSource_Id_fk` BIGINT NULL,
ADD CONSTRAINT `FK_UserLoginAttempts_UserLoginSource`
    FOREIGN KEY (`UserLoginSource_Id_fk`)
    REFERENCES `UserLoginSource` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

CALL DropColumnIfExists('AuthenticationHistory', 'UserLoginSource_Id_fk'); 
ALTER TABLE `AuthenticationHistory` 
ADD COLUMN `UserLoginSource_Id_fk` BIGINT NULL,
ADD CONSTRAINT `FK_AuthenticationHistory_UserLoginSource`
    FOREIGN KEY (`UserLoginSource_Id_fk`)
    REFERENCES `UserLoginSource` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

    
DROP TABLE IF EXISTS `UserLoginExternalAccount`;
CREATE TABLE `UserLoginExternalAccount` (
    Id BIGINT NOT NULL AUTO_INCREMENT,
    UserLoginSource_Id_fk BIGINT NOT NULL,
    UserInfo_Id_fk BIGINT NOT NULL,
    ExternalUserCode VARCHAR(100) NOT NULL,
    ExternalUserName VARCHAR(100) NOT NULL,
    ExternalUserEmail VARCHAR(100) NULL,
    SSOCredential VARCHAR(2000) NULL,
    DateCreated DATETIME NOT NULL DEFAULT NOW(),
    IsDeleted BIT NOT NULL DEFAULT 0,
    PRIMARY KEY (`Id`),
    UNIQUE KEY `Id_UNIQUE` (`Id` ASC),
    UNIQUE KEY `ExternalUserCode_UNIQUE` (`ExternalUserCode`),
    CONSTRAINT `FK_ExternalAccount_UserLoginSource`
    FOREIGN KEY (`UserLoginSource_Id_fk`)
    REFERENCES `UserLoginSource` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_ExternalAccount_UserInfoId`
    FOREIGN KEY (`UserInfo_Id_fk`) REFERENCES `UserInfo` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION);
    
DROP TABLE IF EXISTS `UserLoginExternalAccount_Log`;
CREATE TABLE `UserLoginExternalAccount_Log` (
    Id BIGINT NOT NULL,
    UserLoginSource_Id_fk BIGINT NOT NULL,
    UserInfo_Id_fk BIGINT NOT NULL,
    ExternalUserCode VARCHAR(100) NOT NULL,
    ExternalUserName VARCHAR(100) NOT NULL,
    ExternalUserEmail VARCHAR(100) NULL,
    SSOCredential VARCHAR(2000) NULL,
    DateCreated DATETIME NOT NULL,
    IsDeleted BIT NOT NULL DEFAULT 0,
    LastUpdated DATETIME NOT NULL,
    LastUpdatedUserId BIGINT NOT NULL,
    PRIMARY KEY (`Id`));

DROP PROCEDURE IF EXISTS DropForeignKeyIfExists;
DROP PROCEDURE IF EXISTS DropColumnIfExists;

INSERT INTO `UserLoginSource` (Code, Name, Organisation_Id_fk) values ('AOM', 'Argus Open Markets', 1);
INSERT INTO `UserLoginSource` (Code, Name, Organisation_Id_fk) values ('CME', 'CME Direct', 45);

UPDATE `UserLoginAttempts` SET UserLoginSource_Id_fk = 1;
UPDATE `AuthenticationHistory` SET UserLoginSource_Id_fk = 1;

DROP TABLE IF EXISTS `ClientLog`;
CREATE TABLE `ClientLog` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId` bigint(20) NOT NULL,
  `RunJob` bigint(20) DEFAULT NULL,
  `Message` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LoginPageLoadedClient` datetime(6) DEFAULT NULL,
  `LoginButtonClickedClient` datetime(6) DEFAULT NULL,
  `WebSvcAuthenticatedClient` datetime(6) DEFAULT NULL,
  `DiffusionAuthenticatedClient` datetime(6) DEFAULT NULL,
  `WebSktUserJsonReceivedClient` datetime(6) DEFAULT NULL,
  `WebSktProdDefJsonReceivedClient` datetime(6) DEFAULT NULL,
  `WebSktAllJsonReceivedClient` datetime(6) DEFAULT NULL,
  `TransitionToDashboardClient` datetime(6) DEFAULT NULL,
  `DashboardRenderedClient` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`Id`))