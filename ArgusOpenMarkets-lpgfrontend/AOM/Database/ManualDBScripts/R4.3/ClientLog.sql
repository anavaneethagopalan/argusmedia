CREATE TABLE `ClientLog` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId` bigint(20) NOT NULL,
  `RunJob` bigint(20) DEFAULT NULL,
  `Message` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LoginPageLoadedClient` datetime(6) DEFAULT NULL,
  `LoginButtonClickedClient` datetime(6) DEFAULT NULL,
  `WebSvcAuthenticatedClient` datetime(6) DEFAULT NULL,
  `DiffusionAuthenticatedClient` datetime(6) DEFAULT NULL,
  `WebSktUserJsonReceivedClient` datetime(6) DEFAULT NULL,
  `WebSktProdDefJsonReceivedClient` datetime(6) DEFAULT NULL,
  `WebSktAllJsonReceivedClient` datetime(6) DEFAULT NULL,
  `TransitionToDashboardClient` datetime(6) DEFAULT NULL,
  `DashboardRenderedClient` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`Id`))
  
  
  SELECT  UserId,
        RunJob,
        Message,
        LoginButtonClickedClient,
        DashboardRenderedClient,
        round(TIMESTAMPDIFF(microsecond, LoginPageLoadedClient, LoginButtonClickedClient) / 1000, 0)  as TimeTakenToClickLogin,
        round(TIMESTAMPDIFF(microsecond, LoginButtonClickedClient, WebSvcAuthenticatedClient)  / 1000, 0) as WebSvcAuthenticatedTime,
        round(TIMESTAMPDIFF(microsecond, WebSvcAuthenticatedClient, DiffusionAuthenticatedClient)  / 1000, 0) as DiffusionAuthenticatedTime,
        round(TIMESTAMPDIFF(microsecond, DiffusionAuthenticatedClient, WebSktUserJsonReceivedClient)  / 1000, 0) as UserJsonDownloadTime,
        round(TIMESTAMPDIFF(microsecond, WebSktUserJsonReceivedClient, WebSktProdDefJsonReceivedClient)  / 1000, 0) as ProdDefJsonDownloadTime,
        -- round(TIMESTAMPDIFF(microsecond, WebSktProdDefJsonReceivedClient, WebSktAllJsonReceivedClient)  / 1000, 0) as AllJsonDownloadTime,
        round(TIMESTAMPDIFF(microsecond, TransitionToDashboardClient, DashboardRenderedClient)  / 1000, 0) as DashboardRenderedTime,
        round(TIMESTAMPDIFF(microsecond, LoginButtonClickedClient, DashboardRenderedClient)  / 1000, 0) as TotalTime
        
FROM  `crm`.ClientLog
-- where RunJob = xxx