-- USE `aom.temp`;

 -- PERIOD ACTION CODE
-- ===================
INSERT INTO `PeriodActionCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('F','Fixed',-1);
INSERT INTO `PeriodActionCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('R','Rolls dates every given period based on current date',-1);
INSERT INTO `PeriodActionCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('RL','Rolls dates every given period based on current date and returns list of periods',-1);
INSERT INTO `PeriodActionCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('RFL','Rolls on RollDate and returns fixed dates and list of periods until next RollDate',-1);


-- PRICE TYPE
-- ==========
INSERT INTO `PriceType` (`Id`,`Name`) VALUES (1, 'FIXED');
INSERT INTO `PriceType` (`Id`,`Name`) VALUES (2, 'DIFF');
INSERT INTO `PriceType` (`Id`,`Name`) VALUES (3, 'PERCENT');


-- TENOR CODE
-- ==========
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('BD','Business Days',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('BoM','Balance of Month (excludes today)',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('BoM_1','Balance of Month (includes today)',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('BoW','Balance of Week',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('D','Day',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('H','Hour',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('HM','Half Month',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('HM28','Half Month (28 day month)',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('HM29','Half Month (29 day month - leap year)',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('HM30','Half Month (30 day month)',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('HM31','Half Month (31 day month)',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('M','Month',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('M_BoM','Month + Balance of Month (excluding today)',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('NA','Not Applicable',-1);
-- INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('P','Prompt',-1);
-- INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('PM','Prompt Months',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('QTR','Quarter',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('SUM','Summer',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('WE','Weekend',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('WIN','Winter',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('WK','Week',-1);
INSERT INTO `TenorCode` (`Code`,`Name`,`LastUpdated_User_Id`) VALUES ('Y','Year',-1);
 
INSERT INTO `TenorCode_Log` select * from `TenorCode`;


-- PERIOD
-- ======                       
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (-1,'NA','F','','Default for fixed','',0,0,0,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (1,'D','R','','Daily rolling (+5 to +30)','',3,5,30,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (2,'M','RL','','Month rolling (+1 to +4)','',0,1,4,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (3,'HM28','RFL','1-H','1st Half month rolling Asian LPG Cargoes (28 day month)','4',0,1,3,'01','15',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (4,'HM30','RFL','1-H','1st Half month rolling Asian LPG Cargoes (30 day month)','6',0,1,3,'01','15',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (5,'HM31','RFL','1-H','1st Half month rolling Asian LPG Cargoes (31 day month)','7',0,1,3,'01','15',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (6,'HM29','RFL','1-H','1st Half month rolling Asian LPG Cargoes (29 day month)','5',0,1,3,'01','15',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (7,'D','R','','Daily rolling (+3 to +60)','',3,3,60,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (8,'HM28','RFL','2-H','2nd Half month rolling Asian LPG Cargoes (28 day month)','19',0,1,2,'16','28',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (9,'HM30','RFL','2-H','2nd Half month rolling Asian LPG Cargoes (30 day month)','21',0,1,3,'16','30',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (10,'HM31','RFL','2-H','2nd Half month rolling Asian LPG Cargoes (31 day month)','22',0,1,2,'16','31',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (11,'HM29','RFL','2-H','2nd Half month rolling Asian LPG Cargoes (29 day month)','20',0,1,2,'16','29',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (12,'M_BoM','RL','','Months and Balance of Month - filtered to show 4 forward months','',0,0,3,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (13,'M','RL','','Month rolling (0 to +2)','',0,0,2,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (14,'D','R','','Daily rolling (+1 to +30)','',3,1,30,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (15,'D','R','','Daily rolling (+3 to +20)','',3,3,20,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (16,'D','R','','Daily rolling (+5 to +20)','',3,5,20,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (17,'D','R','','Daily rolling (+7 to +28)','',22,7,28,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (18,'D','R','','Daily rolling (+5 to +15)','',3,5,15,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (19,'D','R','','Daily rolling (+5 to +40)','',3,5,40,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (20,'D','R','','Daily rolling (+30 to +40)','',2,30,40,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (21,'D','R','','Daily rolling (+21 to +30)','',2,21,30,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (22,'D','R','','Daily rolling (+1 to +28)','',3,1,30,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (23,'D','R','','Daily rolling (+3 to +15)','',3,3,15,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (24,'HM','RFL','1-H','1st Half month rolling on fixed dates each month.  3 HM forward','1',0,3,3,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (25,'HM','RFL','2-H','2nd Half month rolling on fixed dates each month. 3 HM forward','16',0,3,3,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (26,'HM','RFL','1-H','1st Half month rolling on fixed dates each month.  4 HM forward','1',0,4,4,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (27,'HM','RFL','2-H','2nd Half month rolling on fixed dates each month. 4 HM forward','16',0,4,4,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (28,'HM','RFL','1-H','1st Half month rolling on fixed dates each month.  5 HM forward','1',0,5,5,'','',-1);
INSERT INTO `Period` (`Id`,`TenorCode_fk`,`PeriodActionCode_fk`,`Code`,`Name`,`RollDate`,`MinimumDeliveryRange`,`RollingPeriodFrom`,`RollingPeriodTo`,`FixedDateFrom`,`FixedDateTo`,`LastUpdated_User_Id`) VALUES (29,'HM','RFL','2-H','2nd Half month rolling on fixed dates each month. 5 HM forward','16',0,5,5,'','',-1);

INSERT INTO `Period_Log` select * from `Period`;
 
 
-- PRICE BASIS
-- ===========
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('Fixed','Fixed',1,'MT','USD',1.0,9999.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('cif ARA','cif ARA',2,'MT','USD',-100.0,100.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('Nap dif','Differential to Naphtha',2,'MT','USD',-100.0,100.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('Nap %','% of Naphtha',3,'MT','USD',0.0,200.0,0.5,1.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('C3 CP','Asian Propane CP',2,'MT','USD',-100.0,200.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('AFEI','Asian Far East Index',2,'MT','USD',-100.0,200.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('MOPJ','Diff to Japan',2,'MT','USD',-100.0,200.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('C4 CP','Asian Butane CP',2,'MT','USD',-100.0,200.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('C3 AFEI','Propane Asian Far East Index',2,'MT','USD',-100.0,200.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('C4 AFEI','Butane Asian Far East Index',2,'MT','USD',-100.0,200.0,0.25,2.0,10.0,-1);
INSERT INTO `PriceBasis` (`ShortName`,`Name`,`PriceType_Id_fk`,`PriceUnit_Code_fk`,`Currency_Code_fk`,`PriceMinimum`,`PriceMaximum`,`PriceIncrement`,`PriceDecimalPlaces`,`PriceSignificantFigures`,`LastUpdated_User_Id`) VALUES ('CP','CP',2,'MT','USD',-100.0,200.0,0.25,2.0,10.0,-1);

INSERT INTO `PriceBasis_Log` select * from `PriceBasis`;


-- PRODUCT PRICE LINE
-- ==================
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (11,0,1,'Fixed Basis - 1','25|50|75|100','1',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (11,0,2,'Differential - 2','25|50|75|100','0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (11,0,1,'Fixed Basis - 3','25|50|75|100','0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (11,0,2,'Differential - 4','25|50|75|100','0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (12,0,1,'Propane',null,'1',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (12,0,2,'Butane',null,'1',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (1,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (2,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (4,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (5,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (6,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (7,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (8,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (9,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);
INSERT INTO `ProductPriceLine` (`ProductTenor_Id_fk`,`BasisProduct_Id_fk`,`SequenceNo`,`Description`,`PercentageOptionList`,`DefaultDisplay`,`LastUpdated_User_Id`) VALUES (10,0,1,'Pricing Basis {SequenceNo}',null,'0',-1);

INSERT INTO `ProductPriceLine_Log` select * from `ProductPriceLine`;


-- PRODUCT PRICE LINE BASIS PERIOD
-- ===============================
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (1,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (2,2,12,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (2,3,12,20,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (2,4,12,30,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (3,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (4,2,12,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (4,3,12,20,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (4,4,12,30,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (5,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (5,6,13,20,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (5,7,13,30,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (5,11,13,40,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (6,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (6,5,13,20,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (6,6,13,30,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (6,7,13,40,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (6,9,13,50,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (6,10,13,60,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (7,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (8,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (9,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (10,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (11,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (12,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (13,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (14,1,-1,10,-1);
INSERT INTO `ProductPriceLineBasisPeriod` (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`,`Period_Id_fk`,`DisplayOrder`,`LastUpdated_User_Id`) VALUES (15,1,-1,10,-1);

INSERT INTO `ProductPriceLineBasisPeriod_Log` select * from `ProductPriceLineBasisPeriod`;


-- PRODUCT TENOR PERIOD
-- ==================
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (1,14,14,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (2,15,15,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (4,22,16,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (5,17,17,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (6,17,17,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (7,18,23,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (8,19,19,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (9,20,20,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (10,21,21,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (11,7,1,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (12,3,-1,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (12,4,-1,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (12,5,-1,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (12,6,-1,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (12,8,-1,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (12,9,-1,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (12,10,-1,-1);
INSERT INTO `ProductTenorPeriod` (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated_User_Id`) VALUES (12,11,-1,-1);

INSERT INTO `ProductTenorPeriod_Log` select * from `ProductTenorPeriod`;

