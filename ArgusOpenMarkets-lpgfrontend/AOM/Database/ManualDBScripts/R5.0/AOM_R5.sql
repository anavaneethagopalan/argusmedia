USE `aom.temp`;

DROP PROCEDURE IF EXISTS DropForeignKeyIfExists;
DROP PROCEDURE IF EXISTS DropColumnIfExists;
DROP PROCEDURE IF EXISTS DropIndexIfExists;

DELIMITER $$
CREATE PROCEDURE DropForeignKeyIfExists(IN tableName VARCHAR(64), IN constraintName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.table_constraints
        WHERE  table_schema = DATABASE() 
        AND    table_name = tableName collate utf8_unicode_ci
        AND    constraint_name = constraintName collate utf8_unicode_ci
        AND    constraint_type = 'FOREIGN KEY')
    THEN
        SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP FOREIGN KEY ', constraintName, ';');
        -- SELECT @query;
        PREPARE stmt FROM @query; 
        EXECUTE stmt; 
        DEALLOCATE PREPARE stmt; 
    END IF; 
END$$

CREATE PROCEDURE DropColumnIfExists(IN tableName VARCHAR(64), IN columnName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.columns
        WHERE  table_schema = DATABASE() 
        AND    table_name = tableName collate utf8_unicode_ci
        AND    column_name = columnName collate utf8_unicode_ci)
    THEN
        SET @query = CONCAT('ALTER TABLE `', tableName, '` DROP COLUMN ', columnName, ';');
        -- SELECT @query;
        PREPARE stmt FROM @query; 
        EXECUTE stmt; 
        DEALLOCATE PREPARE stmt; 
    END IF; 
END$$

CREATE PROCEDURE DropIndexIfExists(IN tableName VARCHAR(64), IN indexName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.statistics
        WHERE  table_schema = DATABASE() 
        AND    table_name = tableName collate utf8_unicode_ci
        AND    index_name = indexName collate utf8_unicode_ci)
    THEN
        SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP INDEX ', indexName, ';');
        -- SELECT @query;
        PREPARE stmt FROM @query; 
        EXECUTE stmt; 
        DEALLOCATE PREPARE stmt; 
    END IF; 
END$$

DELIMITER ;

-- ======== PERIOD =======
CALL DropForeignKeyIfExists('Tenor', 'FK_Tenor_PeriodCode');
CALL DropForeignKeyIfExists('PricePeriod', 'FK_PricePeriod_PeriodCode');

DROP TABLE IF EXISTS `PeriodCode`;
CREATE TABLE `PeriodCode` (
  `Code` varchar(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`Code`),
  UNIQUE INDEX `PeriodCode_Unique` (`Code` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('H','HOUR');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('D','DAY');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('BD','BUSINESS DAY');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('WE','WEEKEND');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('WK','WEEK');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('BoW','BALANCE OF WEEK');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('HM','HALF MONTH');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('M','MONTH');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('BoM','BALANCE OF MONTH');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('QTR','QUARTER');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('SUM','SUMMER');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('WIN','WINTER');
INSERT INTO `PeriodCode` (`Code`,`Name`)
VALUES ('Y','YEAR');


-- ======== TENOR ========
CALL DropForeignKeyIfExists('ProductTenor', 'FK_ProductTenor_Tenor');
CALL DropForeignKeyIfExists('PriceLineBasis', 'FK_PriceLineBasis_Tenor');
CALL DropForeignKeyIfExists('ProductLineBasisTemplate', 'FK_ProductLineBasisTemplate_Tenor');

DROP TABLE IF EXISTS `Tenor`;

CREATE TABLE `Tenor` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `PeriodCode_fk` varchar(5) NOT NULL,
  `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `FK_Tenor_PeriodCode` FOREIGN KEY (`PeriodCode_fk`) REFERENCES `PeriodCode` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `Tenor` (`Name`,`PeriodCode_fk`,`DateCreated`,`LastUpdated`,`LastUpdated_User_Id`) VALUES('Prompt','D',Now(),Now(),-1);
INSERT INTO `Tenor` (`Name`,`PeriodCode_fk`,`DateCreated`,`LastUpdated`,`LastUpdated_User_Id`) VALUES('Prompt','BD',Now(),Now(),-1);
INSERT INTO `Tenor` (`Name`,`PeriodCode_fk`,`DateCreated`,`LastUpdated`,`LastUpdated_User_Id`) VALUES('Monthly','M',Now(),Now(),-1);
INSERT INTO `Tenor` (`Name`,`PeriodCode_fk`,`DateCreated`,`LastUpdated`,`LastUpdated_User_Id`) VALUES('Half Month','HM',Now(),Now(),-1);


DROP TABLE IF EXISTS `Tenor_Log`;
CREATE TABLE `Tenor_Log` (
  `Id` bigint(20) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `PeriodCode_fk` varchar(5) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `LastUpdated` datetime(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== PRODUCTTENOR ========
CALL DropColumnIfExists('ProductTenor', 'Tenor_Id_fk');

ALTER TABLE `ProductTenor` 
    CHANGE COLUMN `Product_Id_fk` `Product_Id_fk` BIGINT(20) NOT NULL AFTER `Id`,
    ADD COLUMN `Tenor_Id_fk` bigint(20) NULL AFTER `Product_Id_fk`,
    CHANGE COLUMN `DateCreated` `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL AFTER `DefaultEndDate`,
    CHANGE COLUMN `LastUpdated` `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL AFTER `DateCreated`,
    CHANGE COLUMN `LastUpdated_User_Id` `LastUpdated_User_Id` BIGINT(20) NOT NULL AFTER `LastUpdated`;

UPDATE `ProductTenor` SET Tenor_Id_fk = 2;
ALTER TABLE `ProductTenor` MODIFY COLUMN `Tenor_Id_fk` bigint(20) NOT NULL;

ALTER TABLE `ProductTenor` 
	ADD INDEX `ProductTenor_TenorId_idx` (`Tenor_Id_fk` ASC),
	ADD CONSTRAINT `FK_ProductTenor_Tenor` FOREIGN KEY (`Tenor_Id_fk`) REFERENCES `Tenor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

CALL DropColumnIfExists('ProductTenor_Log', 'Tenor_Id_fk');

ALTER TABLE `ProductTenor_Log` 
    CHANGE COLUMN `Product_Id_fk` `Product_Id_fk` BIGINT(20) NOT NULL AFTER `Id`,
    ADD COLUMN `Tenor_Id_fk` bigint(20) NULL AFTER `Product_Id_fk`,
    CHANGE COLUMN `DateCreated` `DateCreated` datetime(6) NOT NULL AFTER `DefaultEndDate`,
    CHANGE COLUMN `LastUpdated` `LastUpdated` datetime(6) NOT NULL AFTER `DateCreated`,
    CHANGE COLUMN `LastUpdated_User_Id` `LastUpdated_User_Id` BIGINT(20) NOT NULL AFTER `LastUpdated`;

UPDATE `ProductTenor_Log` SET Tenor_Id_fk = 2;
ALTER TABLE `ProductTenor_Log` MODIFY COLUMN `Tenor_Id_fk` bigint(20) NOT NULL;

-- CALL DropColumnIfExists('ProductTenor', 'DisplayName');
-- CALL DropColumnIfExists('ProductTenor_Log', 'DisplayName');


-- ======== BIDASKSTACKSTYLE ========
CALL DropForeignKeyIfExists('Product', 'fk_product_bidaskstackstyle');
DROP TABLE IF EXISTS `BidAskStackStyle`;

CREATE TABLE `BidAskStackStyle` (
  `Code` char(1) NOT NULL,
  `Name` varchar(20) DEFAULT NULL,
  -- Consider adding sort order here
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `BidAskStackStyle` (`Code`,`Name`) VALUES ('N','Normal');
INSERT INTO `BidAskStackStyle` (`Code`,`Name`) VALUES ('W','Wide');

CALL DropColumnIfExists('Product', 'BidAskStackStyle_Code_fk');

ALTER TABLE `Product` 
	ADD COLUMN `BidAskStackStyle_Code_fk` VARCHAR(1) collate utf8_unicode_ci NOT NULL DEFAULT 'N' AFTER `BidAskStackNumberRows`,
    CHANGE COLUMN `DateCreated` `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    CHANGE COLUMN `LastUpdated` `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
	ADD CONSTRAINT `fk_product_bidaskstackstyle` FOREIGN KEY (`BidAskStackStyle_Code_fk`) REFERENCES `BidAskStackStyle` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION; 


CALL DropColumnIfExists('Product_Log', 'BidAskStackStyle_Code_fk');

ALTER TABLE `Product_Log` 
	ADD COLUMN `BidAskStackStyle_Code_fk` VARCHAR(1) collate utf8_unicode_ci NOT NULL DEFAULT 'N' AFTER `BidAskStackNumberRows`;
    
    
-- ======== COMMONQUANTITYVALUES ========
CALL DropColumnIfExists('CommonQuantityValues', 'QuantityText');
CALL DropColumnIfExists('CommonQuantityValues', 'DisplayOrder');
CALL DropColumnIfExists('CommonQuantityValues', 'DisplayQuantityText');
CALL DropIndexIfExists('CommonQuantityValues', 'CommonQuantityValues_Unique');
CALL DropForeignKeyIfExists('CommonQuantityValues', 'Product_Id_fk');

ALTER TABLE `CommonQuantityValues` 
	CHANGE COLUMN `Product_Id_Fk` `Product_Id_Fk` BIGINT(20) NOT NULL,
	ADD COLUMN `QuantityText` VARCHAR(50) collate utf8_unicode_ci NULL COMMENT 'Override text to be used for display' AFTER `Quantity`,
	ADD COLUMN `DisplayOrder` INT NOT NULL DEFAULT 0 AFTER `QuantityText`,
	ADD COLUMN `DisplayQuantityText` BIT(1) NULL DEFAULT false COMMENT 'Hides the QuantityValue field and displays the QuantityText' AFTER `DisplayOrder`,
	ADD UNIQUE INDEX `CommonQuantityValues_Unique` (`Product_Id_Fk`,`Quantity` ASC),
	ADD CONSTRAINT `Product_Id_fk` FOREIGN KEY (`Product_Id_fk`) REFERENCES `Product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION; 

UPDATE `CommonQuantityValues` c1
JOIN (SELECT id, quantity, 
            (SELECT COUNT(*) 
            FROM    `CommonQuantityValues` as s2
            WHERE   s2.Quantity < s1.Quantity 
            AND     s2.Product_Id_fk = s1.Product_Id_fk) NewDisplayOrder 
      FROM `CommonQuantityValues` s1 
      WHERE IFNULL(QuantityText, 'x') <> 'ToT') c2
      ON c1.Id = c2.Id 
SET   c1.DisplayOrder = (c2.NewDisplayOrder + 1) * 10;


DELETE FROM `CommonQuantityValues`
WHERE  Quantity = '20500';

INSERT INTO `CommonQuantityValues` (`LastUpdatedUser_Id`,`EnteredByUser_Id`,`LastUpdated`,`DateCreated`,`Quantity`,`QuantityText`,`DisplayOrder`,`DisplayQuantityText`,`Product_Id_Fk`) 
VALUES(-1,-1,now(),now(),20500,'ToT',1000,true, 2); -- TODO = CHANGE PRODUCT ID 


CALL DropColumnIfExists('CommonQuantityValues_Log', 'Product_Id_Fk');
CALL DropColumnIfExists('CommonQuantityValues_Log', 'QuantityText');
CALL DropColumnIfExists('CommonQuantityValues_Log', 'DisplayOrder');
CALL DropColumnIfExists('CommonQuantityValues_Log', 'DisplayQuantityText');

ALTER TABLE `CommonQuantityValues_Log`
	ADD COLUMN `QuantityText` VARCHAR(50) collate utf8_unicode_ci NULL AFTER `Quantity`,
	ADD COLUMN `DisplayOrder` INT NOT NULL DEFAULT 0 AFTER `QuantityText`,
    ADD COLUMN `DisplayQuantityText` BIT(1) NULL DEFAULT false AFTER `DisplayOrder`,
    ADD COLUMN `Product_Id_Fk` BIGINT(20) NOT NULL;

    
-- ======== ORDER ========
CALL DropColumnIfExists('Order', 'QuantityText');
ALTER TABLE `Order` 
	ADD COLUMN `QuantityText` VARCHAR(50) collate utf8_unicode_ci NULL COMMENT 'Override display value for quantity value e.g. TOT' AFTER `Quantity`,
    MODIFY COLUMN `DateCreated` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
    MODIFY COLUMN `LastUpdated` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6);
    -- CHANGE COLUMN `Price` `OldPrice` DECIMAL(14,6) NULL DEFAULT NULL COMMENT 'To be dropped in future release';

CALL DropColumnIfExists('Order_Log', 'QuantityText');
ALTER TABLE `Order_Log` 
	ADD COLUMN `QuantityText` VARCHAR(50) collate utf8_unicode_ci NULL AFTER `Quantity`;
    -- CHANGE COLUMN `Price` `OldPrice` DECIMAL(14,6) NULL DEFAULT NULL;

    
-- ======== DEAL ========
ALTER TABLE `Deal` 
    MODIFY COLUMN `DateCreated` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
    MODIFY COLUMN `LastUpdated` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6);
    
    
-- ======== PRICETYPE ========
CALL DropForeignKeyIfExists('PriceBasis', 'FK_PriceBasis_PriceType');
DROP TABLE IF EXISTS `PriceType`;

CREATE TABLE `PriceType` (
	`Id` bigint(20) NOT NULL ,
	`Name` varchar(20) NOT NULL,
	PRIMARY KEY (`Id`)
);

INSERT INTO `PriceType` (`Id`,`Name`) VALUES (1, 'FIXED');
INSERT INTO `PriceType` (`Id`,`Name`) VALUES (2, 'DIFF');
INSERT INTO `PriceType` (`Id`,`Name`) VALUES (3, 'PERCENT');


-- ======== PRICEBASIS ========
CALL DropForeignKeyIfExists('PriceLineBasis', 'FK_PriceLineBasis_PriceBasis');
CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_PriceBasis');

DROP TABLE IF EXISTS `PriceBasis`;

CREATE TABLE `PriceBasis` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ShortName` varchar(20) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `PriceType_Id_fk` bigint(20) NOT NULL,  
  `PriceMinimum` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `PriceMaximum` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `PriceIncrement` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `PriceDecimalPlaces` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `PriceSignificantFigures` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `PriceBasis_idx` (`PriceType_Id_fk` ASC),
  CONSTRAINT `FK_PriceBasis_PriceType` FOREIGN KEY (`PriceType_Id_fk`) REFERENCES `PriceType` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `PriceBasis_Log`;

CREATE TABLE `PriceBasis_Log` (
  `Id` bigint(20) NOT NULL,
  `ShortName` varchar(20) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `PriceType_Id_fk` bigint(20) NOT NULL,  
  `PriceMinimum` decimal(14,6) NOT NULL,
  `PriceMaximum` decimal(14,6) NOT NULL,
  `PriceIncrement` decimal(14,6) NOT NULL,
  `PriceDecimalPlaces` decimal(14,6) NOT NULL,
  `PriceSignificantFigures` decimal(14,6) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `LastUpdated` datetime(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== PricePeriod ========
CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_PricePeriod');

DROP TABLE IF EXISTS `PricePeriod`;

CREATE TABLE `PricePeriod` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `PeriodCode_fk` varchar(5) NOT NULL,
  `ShortName` varchar(10) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `PricePeriodFrom` DATE NOT NULL,
  `PricePeriodTo` DATE NOT NULL,
  `EffectiveFrom` DATE NOT NULL,
  `EffectiveTo` DATE NOT NULL,
  `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
   PRIMARY KEY (`Id`),
   UNIQUE INDEX `PricePeriod_unique` (`Id` ASC),
   CONSTRAINT `FK_PricePeriod_PeriodCode` FOREIGN KEY (`PeriodCode_fk`) REFERENCES `PeriodCode` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `PricePeriod_Log`;

CREATE TABLE `PricePeriod_Log` (
  `Id` bigint(20) NOT NULL,
  `PeriodCode_fk` varchar(5) NOT NULL,
  `ShortName` varchar(10) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `PricePeriodFrom` DATE NOT NULL,
  `PricePeriodTo` DATE NOT NULL,
  `EffectiveFrom` DATE NOT NULL,
  `EffectiveTo` DATE NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `LastUpdated` datetime(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
   PRIMARY KEY (`Id`),
   UNIQUE INDEX `PricePeriod_unique` (`Id` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== ProductPriceLine ========
CALL DropForeignKeyIfExists('ProductPriceLine', 'FK_ProductPriceLine_ProductTenor');
CALL DropForeignKeyIfExists('ProductPriceLine', 'FK_ProductPriceLine_BasisProduct');

CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_ProductPriceLine');
CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_PriceBasis');
CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_PricePeriod');

DROP TABLE IF EXISTS `ProductPriceLine`;

CREATE TABLE `ProductPriceLine` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  -- `Product_Id_fk` bigint(20) NOT NULL COMMENT 'ProductTenor this applies to',
  `ProductTenor_Id_fk` bigint(20) NOT NULL  COMMENT 'ProductTenor this applies to',
  `BasisProduct_Id_fk` bigint(20) NULL COMMENT 'Underlying Product Constinuent e.g for Mixed Cargo products',
  `SequenceNo` bigint(20) NOT NULL DEFAULT 1 COMMENT 'Identifies the pricing choice 1 or 2',
  `Description` VARCHAR(100) NULL DEFAULT 'Pricing Basis {SequenceNo}',
  `PercentageOptionList` varchar(100) NULL COMMENT 'Restrict values to items from list e.g. 10|25|50|100', -- Consider becoming separate table
   PRIMARY KEY (`Id`),
   UNIQUE INDEX `ProductPriceLine_unique` (`Id` ASC),
   INDEX `ProductPriceLine_ProductTenor_idx` (`ProductTenor_Id_fk` ASC),
   CONSTRAINT `FK_ProductPriceLine_ProductTenor` FOREIGN KEY (`ProductTenor_Id_fk`) REFERENCES `ProductTenor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `FK_ProductPriceLine_BasisProduct` FOREIGN KEY (`BasisProduct_Id_fk`) REFERENCES `Product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== ProductPriceLineBasisPeriod ========
DROP TABLE IF EXISTS `ProductPriceLineBasisPeriod`;

CREATE TABLE `ProductPriceLineBasisPeriod` (
    `ProductPriceLine_Id_fk` bigint(20) NOT NULL,
    `PriceBasis_Id_fk` bigint(20) NOT NULL, 
    `PricePeriod_Id_fk` bigint(20) NOT NULL, 
    PRIMARY KEY (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`, `PricePeriod_Id_fk`),
    CONSTRAINT `FK_ProductPriceLineBasisPeriod_ProductPriceLine` FOREIGN KEY (`ProductPriceLine_Id_fk`) REFERENCES `ProductPriceLine` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_ProductPriceLineBasisPeriod_PriceBasis` FOREIGN KEY (`PriceBasis_Id_fk`) REFERENCES `PriceBasis` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_ProductPriceLineBasisPeriod_PricePeriod` FOREIGN KEY (`PricePeriod_Id_fk`) REFERENCES `PricePeriod` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


/*
-- OPTION 1 - MODEL Price Line referencing Order and External Deal Directly and introduce a boolean to indicate which line is select when order is executed
    CREATE TABLE `PriceLine` (
        `Id` bigint(20) NOT NULL AUTO_INCREMENT,
        `Order_Id_fk` bigint(20)  NULL,
        `ExternalDeal_Id_fk` bigint(20)  NULL,
        `LineSelected` CHAR(1) NOT NULL,      
      PRIMARY KEY (`Id`)
    );

    ALTER TABLE `PriceLine` 
        ADD INDEX `FK_PriceLine_ExternalDealId_idx` (`ExternalDeal_Id_fk` ASC);
    ALTER TABLE `PriceLine` 
        ADD CONSTRAINT `FK_PriceLine_ExternalDealId` FOREIGN KEY (`ExternalDeal_Id_fk`) REFERENCES `ExternalDeal` (`Id`)  ON DELETE NO ACTION  ON UPDATE NO ACTION;
    ALTER TABLE `PriceLine` 
        ADD INDEX `FK_PriceLine_OrderId_idx` (`Order_Id_fk` ASC);
    ALTER TABLE `PriceLine` 
        ADD CONSTRAINT `FK_PriceLine_OrderId` FOREIGN KEY (`Order_Id_fk`) REFERENCES `Order` (`Id`)  ON DELETE NO ACTION  ON UPDATE NO ACTION;
*/

-- OPTION 2 - MODEL Price Line as an entity that is separately mapped to Order or ExternalDeal 
    -- ======== PriceLine ========
    CALL DropForeignKeyIfExists('OrderPriceLine', 'FK_OrderPriceLine_PriceLine');
    CALL DropForeignKeyIfExists('ExternalDeal', 'FK_ExternalDeal_PriceLine');
    CALL DropForeignKeyIfExists('PriceLineBasis', 'FK_PriceLineBasis_PriceLine');
    DROP TABLE IF EXISTS `PriceLine`;

    CREATE TABLE `PriceLine` (
        `Id` bigint(20) NOT NULL AUTO_INCREMENT,
        `SequenceNo` INT NOT NULL DEFAULT 1,
        `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
        `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
        `LastUpdated_User_Id` bigint(20) NOT NULL,
        PRIMARY KEY (`Id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    DROP TABLE IF EXISTS `PriceLine_Log`;
    CREATE TABLE `PriceLine_Log` (
        `Id` bigint(20) NOT NULL,
        `SequenceNo` INT NOT NULL,
        `DateCreated` datetime(6) NOT NULL,
        `LastUpdated` datetime(6) NOT NULL,
        `LastUpdated_User_Id` bigint(20) NOT NULL,
        PRIMARY KEY (`Id`,`LastUpdated`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    
    -- ======== OrderPriceLine ========
    DROP TABLE IF EXISTS `OrderPriceLine`;    
    -- Map a priceline to an order
    CREATE TABLE `OrderPriceLine` (
        `Id` bigint(20) NOT NULL AUTO_INCREMENT,
        `Order_Id_fk` bigint(20) NOT NULL,    	
        `PriceLine_Id_fk` bigint(20) NOT NULL,
        `OrderStatus_Code_fk` CHAR(1) NOT NULL,
        `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
        `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
        `LastUpdated_User_Id` bigint(20) NOT NULL,
        PRIMARY KEY (`Id`),
        UNIQUE INDEX `OrderPriceLine_UNIQUE` (`PriceLine_Id_fk` ASC), -- Effectively a 1-1 mapping table so priceline should only be mapped once
        INDEX `OrderPriceLine_OrderStatusCode_idx` (`OrderStatus_Code_fk` ASC),
        INDEX `OrderPriceLine_OrderId_idx` (`Order_Id_fk` ASC),
        INDEX `OrderPriceLine_PriceLineId_idx` (`PriceLine_Id_fk` ASC),
        CONSTRAINT `FK_OrderPriceLine_OrderStatusCode` FOREIGN KEY (`OrderStatus_Code_fk`) REFERENCES `OrderStatus` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `FK_OrderPriceLine_Order` FOREIGN KEY (`Order_Id_fk`) REFERENCES `Order` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `FK_OrderPriceLine_PriceLine` FOREIGN KEY (`PriceLine_Id_fk`) REFERENCES `PriceLine` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    

    DROP TABLE IF EXISTS `OrderPriceLine_Log`;    
    -- Map a priceline to an order
    CREATE TABLE `OrderPriceLine_Log` (
        `Id` bigint(20) NOT NULL,
        `Order_Id_fk` bigint(20) NOT NULL,    	
        `PriceLine_Id_fk` bigint(20) NOT NULL,
        `OrderStatus_Code_fk` CHAR(1) NOT NULL,
        `DateCreated` datetime(6) NOT NULL,
        `LastUpdated` datetime(6) NOT NULL,
        `LastUpdated_User_Id` bigint(20) NOT NULL,
        PRIMARY KEY (`Id`,`LastUpdated`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    
    -- ======== ExternalDeal ========
    CALL DropForeignKeyIfExists('ExternalDeal', 'FK_ExternalDeal_PriceLine');
    CALL DropColumnIfExists('ExternalDeal', 'QuantityText');
    CALL DropColumnIfExists('ExternalDeal', 'PriceLine_Id_fk');
    
    ALTER TABLE `ExternalDeal` 
        ADD COLUMN `QuantityText` VARCHAR(50) NULL COMMENT 'Override display value for quantity value e.g. TOT' AFTER `Quantity`,
        ADD COLUMN `PriceLine_Id_fk` bigint(20) NULL,
        -- CHANGE COLUMN `Price` `OldPrice` DECIMAL(14,6) NULL DEFAULT NULL COMMENT 'To be dropped in future release';
        MODIFY COLUMN `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
        MODIFY COLUMN `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
        ADD INDEX `ExternalDeal_PriceLineId_idx` (`PriceLine_Id_fk` ASC),
        ADD CONSTRAINT `FK_ExternalDeal_PriceLine` FOREIGN KEY (`PriceLine_Id_fk`) REFERENCES `PriceLine` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


    CALL DropColumnIfExists('ExternalDeal_Log', 'QuantityText');
    CALL DropColumnIfExists('ExternalDeal_Log', 'PriceLine_Id_fk');
    
    ALTER TABLE `ExternalDeal_Log` 
        ADD COLUMN `QuantityText` VARCHAR(50) NULL AFTER `Quantity`,
        ADD COLUMN `PriceLine_Id_fk` bigint(20) NULL;

        
-- ======== PriceLineBasis ========
DROP TABLE  IF EXISTS `PriceLineBasis` ;

CREATE TABLE `PriceLineBasis` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `PriceLine_Id_fk` bigint(20) NOT NULL,  
    `PriceBasis_Id_fk` BIGINT(20) NOT NULL,
    `SequenceNo` INT NOT NULL DEFAULT 1 ,
    `Product_Id_fk` BIGINT(20) NULL COMMENT 'Product Constinuent for Mixed Cargo products',
    `Description` VARCHAR(400) NULL COMMENT 'Derived field - e.g. Product Bases 1',
    `PercentageSplit` INT NOT NULL DEFAULT 100,
    `Tenor_Id_fk`  BIGINT(20) NOT NULL,
    `PricingPeriodFrom` DATE NOT NULL,
    `PricingPeriodTo` DATE NOT NULL,
    `PriceLineBasisValue` decimal(14,6) NOT NULL,
    `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`),  
    UNIQUE INDEX `PriceLineBasis_Unique` (`PriceLine_Id_fk` ASC, `SequenceNo` ASC),
    INDEX `PriceLineBasis_TenorId_idx` (`Tenor_Id_fk` ASC),
    INDEX `PriceLineBasis_PriceLineId_idx` (`PriceLine_Id_fk` ASC),
    INDEX `PriceLineBasis_PriceBasisId_idx` (`PriceBasis_Id_fk` ASC),
    INDEX `PriceLineBasis_ProductId_idx` (`Product_Id_fk` ASC),
    CONSTRAINT `FK_PriceLineBasis_Tenor` FOREIGN KEY (`Tenor_Id_fk`) REFERENCES `Tenor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_PriceLineBasis_PriceLine` FOREIGN KEY (`PriceLine_Id_fk`) REFERENCES `PriceLine` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_PriceLineBasis_PriceBasis` FOREIGN KEY (`PriceBasis_Id_fk`) REFERENCES `PriceBasis` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_PriceLineBasis_Product` FOREIGN KEY (`Product_Id_fk`) REFERENCES `Product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE  IF EXISTS `PriceLineBasis_Log` ;

CREATE TABLE `PriceLineBasis_Log` (
  `Id` bigint(20) NOT NULL,
  `PriceLine_Id_fk` bigint(20) NOT NULL,  
  `SequenceNo` INT NOT NULL DEFAULT 1 ,
  `Product_Id_fk` BIGINT(20) NULL,
  `Description` VARCHAR(400) NULL,
  `PercentageSplit` INT NOT NULL DEFAULT 100,
  `PriceBasis_Id_fk` BIGINT(20) NOT NULL,
  `Tenor_Id_fk`  BIGINT(20) NOT NULL,
  `PricingPeriodFrom` DATE NOT NULL,
  `PricingPeriodTo` DATE NOT NULL,
  `PriceLineBasisValue` decimal(14,6) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `LastUpdated` datetime(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    

-- #########################################################################################################
-- DROP COLUMNS NOW REPLACED BY NEW STRUCTURE

-- This drop action will probably be deffered to a future release...
-- ALTER TABLE `Product` 
--  DROP COLUMN `OrderPriceSignificantFigures`,
--  DROP COLUMN `OrderPriceDecimalPlaces`,
--  DROP COLUMN `OrderPriceIncrement`,
--  DROP COLUMN `OrderPriceMaximum`,
--  DROP COLUMN `OrderPriceMinimum`;
    

-- #########################################################################################################
-- PUT IN SOME SAMPLE DATA



DROP PROCEDURE IF EXISTS DropForeignKeyIfExists;
DROP PROCEDURE IF EXISTS DropColumnIfExists;
DROP PROCEDURE IF EXISTS DropIndexIfExists;
