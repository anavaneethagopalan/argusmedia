-- USE `aom.temp`;

DROP PROCEDURE IF EXISTS DropForeignKeyIfExists;
DROP PROCEDURE IF EXISTS DropColumnIfExists;
DROP PROCEDURE IF EXISTS DropIndexIfExists;

DELIMITER $$
CREATE PROCEDURE DropForeignKeyIfExists(IN tableName VARCHAR(64), IN constraintName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.table_constraints
        WHERE  table_schema = DATABASE() 
        AND    table_name = tableName collate utf8_unicode_ci
        AND    constraint_name = constraintName collate utf8_unicode_ci
        AND    constraint_type = 'FOREIGN KEY')
    THEN
        SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP FOREIGN KEY ', constraintName, ';');
        -- SELECT @query;
        PREPARE stmt FROM @query; 
        EXECUTE stmt; 
        DEALLOCATE PREPARE stmt; 
    END IF; 
END$$

CREATE PROCEDURE DropColumnIfExists(IN tableName VARCHAR(64), IN columnName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.columns
        WHERE  table_schema = DATABASE() 
        AND    table_name = tableName collate utf8_unicode_ci
        AND    column_name = columnName collate utf8_unicode_ci)
    THEN
        SET @query = CONCAT('ALTER TABLE `', tableName, '` DROP COLUMN ', columnName, ';');
        -- SELECT @query;
        PREPARE stmt FROM @query; 
        EXECUTE stmt; 
        DEALLOCATE PREPARE stmt; 
    END IF; 
END$$

CREATE PROCEDURE DropIndexIfExists(IN tableName VARCHAR(64), IN indexName VARCHAR(64))
BEGIN
    IF EXISTS(
        SELECT * FROM information_schema.statistics
        WHERE  table_schema = DATABASE() 
        AND    table_name = tableName collate utf8_unicode_ci
        AND    index_name = indexName collate utf8_unicode_ci)
    THEN
        SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP INDEX ', indexName, ';');
        -- SELECT @query;
        PREPARE stmt FROM @query; 
        EXECUTE stmt; 
        DEALLOCATE PREPARE stmt; 
    END IF; 
END$$

DELIMITER ;


-- ======== TENOR ========
-- CALL DropForeignKeyIfExists('ProductTenor', 'FK_ProductTenor_TenorCode');
CALL DropForeignKeyIfExists('Period', 'FK_Period_TenorCode');
CALL DropForeignKeyIfExists('PriceLineBasis', 'FK_PriceLineBasis_TenorCode');

DROP TABLE IF EXISTS `TenorCode`;
CREATE TABLE `TenorCode` (
  `Code` varchar(10) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `TenorCode_Log`;
CREATE TABLE `TenorCode_Log` (
    `Code` varchar(10) NOT NULL,
    `Name` varchar(50) DEFAULT NULL,
    `DateCreated` datetime(6) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Code`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== PERIOD CHANGE CODE ========
CALL DropForeignKeyIfExists('Period', 'FK_Period_PeriodActionCode');

DROP TABLE IF EXISTS `PeriodActionCode`;
CREATE TABLE `PeriodActionCode` (
  `Code` varchar(10) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== PRODUCT TENOR ========
-- Recreate ProductTenor table from backup


-- CREATE BACKUP PRODUCTENOR TABLE
CREATE TABLE IF NOT EXISTS `ProductTenorBackup` LIKE ProductTenor;
INSERT INTO `ProductTenorBackup` SELECT * FROM ProductTenor WHERE NOT EXISTS (select id from ProductTenorBackup);

CALL DropColumnIfExists('ProductTenor', 'DeliveryDateStart');
CALL DropColumnIfExists('ProductTenor', 'DeliveryDateEnd');
CALL DropColumnIfExists('ProductTenor', 'DefaultStartDate');
CALL DropColumnIfExists('ProductTenor', 'DefaultEndDate');
CALL DropColumnIfExists('ProductTenor', 'RollDate');
CALL DropColumnIfExists('ProductTenor', 'RollDateRule');
CALL DropColumnIfExists('ProductTenor', 'MinimumDeliveryRange');

ALTER TABLE `ProductTenor` 
    CHANGE COLUMN `Product_Id_fk` `Product_Id_fk` BIGINT(20) NOT NULL AFTER `Id`,
    CHANGE COLUMN `DateCreated` `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL AFTER `DisplayName`,
    CHANGE COLUMN `LastUpdated` `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL AFTER `DateCreated`,
    CHANGE COLUMN `LastUpdated_User_Id` `LastUpdated_User_Id` BIGINT(20) NOT NULL AFTER `LastUpdated`;

    
CALL DropColumnIfExists('ProductTenor_Log', 'DeliveryDateStart');
CALL DropColumnIfExists('ProductTenor_Log', 'DeliveryDateEnd');
CALL DropColumnIfExists('ProductTenor_Log', 'DefaultStartDate');
CALL DropColumnIfExists('ProductTenor_Log', 'DefaultEndDate');
CALL DropColumnIfExists('ProductTenor_Log', 'RollDate');
CALL DropColumnIfExists('ProductTenor_Log', 'RollDateRule');
CALL DropColumnIfExists('ProductTenor_Log', 'MinimumDeliveryRange');
    
ALTER TABLE `ProductTenor_Log` 
    CHANGE COLUMN `Product_Id_fk` `Product_Id_fk` BIGINT(20) NOT NULL AFTER `Id`,
    CHANGE COLUMN `DateCreated` `DateCreated` datetime(6) NOT NULL AFTER `DisplayName`,
    CHANGE COLUMN `LastUpdated` `LastUpdated` datetime(6) NOT NULL AFTER `DateCreated`,
    CHANGE COLUMN `LastUpdated_User_Id` `LastUpdated_User_Id` BIGINT(20) NOT NULL AFTER `LastUpdated`;


-- ======== PERIOD ========
CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_Period');
CALL DropForeignKeyIfExists('Period', 'FK_Period_TenorCode');
CALL DropForeignKeyIfExists('ProductTenorPeriod', 'FK_ProductTenorPeriod_DeliveryPeriod');
CALL DropForeignKeyIfExists('ProductTenorPeriod', 'FK_ProductTenorPeriod_DefaultPeriod');

DROP TABLE IF EXISTS `Period`;
CREATE TABLE `Period` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `TenorCode_fk` varchar(10) NOT NULL,
    `PeriodActionCode_fk` varchar(10) NOT NULL,
    `Code` varchar(20) DEFAULT NULL,
    `Name` varchar(100) DEFAULT NULL,
    `MinimumDeliveryRange` bigint(20) DEFAULT NULL,
    `RollDate` varchar(50) DEFAULT NULL,
    -- `RollDateRule` varchar(50) DEFAULT NULL,
    `RollingPeriodFrom` int(11) DEFAULT NULL,
    `RollingPeriodTo` int(11) DEFAULT NULL,
    `FixedDateFrom` varchar(20) DEFAULT NULL,
    `FixedDateTo` varchar(20) DEFAULT NULL,
    `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`),
    CONSTRAINT `FK_Period_TenorCode` FOREIGN KEY (`TenorCode_fk`) REFERENCES `TenorCode` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_Period_PeriodActionCode` FOREIGN KEY (`PeriodActionCode_fk`) REFERENCES `PeriodActionCode` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Period_Log`;
CREATE TABLE `Period_Log` (
    `Id` bigint(20) NOT NULL,
    `TenorCode_fk` varchar(10) NOT NULL,
    `PeriodActionCode_fk` varchar(10) NOT NULL,
    `Code` varchar(20) DEFAULT NULL,
    `Name` varchar(100) DEFAULT NULL,
    `MinimumDeliveryRange` bigint(20) DEFAULT NULL,
    `RollDate` varchar(50) DEFAULT NULL,
    -- `RollDateRule` varchar(50) DEFAULT NULL,
    `RollingPeriodFrom` int(11) DEFAULT NULL,
    `RollingPeriodTo` int(11) DEFAULT NULL,
    `FixedDateFrom` varchar(20) DEFAULT NULL,
    `FixedDateTo` varchar(20) DEFAULT NULL,
    `DateCreated` datetime(6) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== PRODUCT TENOR PERIOD =======
DROP TABLE IF EXISTS `ProductTenorPeriod`;
CREATE TABLE `ProductTenorPeriod` (
    `ProductTenor_Id_fk` bigint(20) NOT NULL,
    `DeliveryPeriod_Id_fk` bigint(20) NOT NULL,
    `DefaultPeriod_Id_fk` bigint(20) NOT NULL,
    `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`),
    CONSTRAINT `FK_ProductTenorPeriod_ProductTenor` FOREIGN KEY (`ProductTenor_Id_fk`) REFERENCES `ProductTenor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_ProductTenorPeriod_DeliveryPeriod` FOREIGN KEY (`DeliveryPeriod_Id_fk`) REFERENCES `Period` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_ProductTenorPeriod_DefaultPeriod` FOREIGN KEY (`DefaultPeriod_Id_fk`) REFERENCES `Period` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `ProductTenorPeriod_Log`;
CREATE TABLE `ProductTenorPeriod_Log` (
    `ProductTenor_Id_fk` bigint(20) NOT NULL,
    `DeliveryPeriod_Id_fk` bigint(20) NOT NULL,
    `DefaultPeriod_Id_fk` bigint(20) NOT NULL,
    `DateCreated` datetime(6) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`ProductTenor_Id_fk`,`DeliveryPeriod_Id_fk`,`DefaultPeriod_Id_fk`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ======== BIDASKSTACKSTYLE ========
CALL DropForeignKeyIfExists('Product', 'FK_Product_BidAskStackStyle');

DROP TABLE IF EXISTS `BidAskStackStyle`;
CREATE TABLE `BidAskStackStyle` (
  `Code` char(1) NOT NULL,
  `Name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `BidAskStackStyle` (`Code`,`Name`) VALUES ('N','Normal');
INSERT INTO `BidAskStackStyle` (`Code`,`Name`) VALUES ('W','Wide');

-- ======== PRODUCT ========
CALL DropForeignKeyIfExists('Product', 'fk_product_location1');
CALL DropIndexIfExists('Product', 'fk_product_location1');
CALL DropColumnIfExists('Product', 'BidAskStackStyle_Code_fk');
-- CALL DropColumnIfExists('Product', 'OrderPriceSignificantFigures');
-- CALL DropColumnIfExists('Product', 'OrderPriceDecimalPlaces');
-- CALL DropColumnIfExists('Product', 'OrderPriceIncrement');
-- CALL DropColumnIfExists('Product', 'OrderPriceMaximum');
-- CALL DropColumnIfExists('Product', 'OrderPriceMinimum');
-- CALL DropColumnIfExists('Product', 'Location_Id_fk');
-- CALL DropColumnIfExists('Product', 'LocationLabel');
-- CALL DropColumnIfExists('Product', 'PriceUnit_Code_fk');

ALTER TABLE `Product` 
	ADD COLUMN `BidAskStackStyle_Code_fk` VARCHAR(1) collate utf8_unicode_ci NOT NULL DEFAULT 'N' AFTER `BidAskStackNumberRows`,
    ADD COLUMN `DisplayContractName`  BIT NOT NULL DEFAULT false,
    CHANGE COLUMN `DateCreated` `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    CHANGE COLUMN `LastUpdated` `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
	ADD CONSTRAINT `FK_Product_BidAskStackStyle` FOREIGN KEY (`BidAskStackStyle_Code_fk`) REFERENCES `BidAskStackStyle` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    DROP FOREIGN KEY `fk_product_currency1`,
    DROP FOREIGN KEY `fk_product_unit2`;

    
CALL DropColumnIfExists('Product_Log', 'BidAskStackStyle_Code_fk');
-- This drop action will probably be deffered to a future release...
-- CALL DropColumnIfExists('Product_Log', 'OrderPriceSignificantFigures');
-- CALL DropColumnIfExists('Product_Log', 'OrderPriceDecimalPlaces');
-- CALL DropColumnIfExists('Product_Log', 'OrderPriceIncrement');
-- CALL DropColumnIfExists('Product_Log', 'OrderPriceMaximum');
-- CALL DropColumnIfExists('Product_Log', 'OrderPriceMinimum');
-- CALL DropColumnIfExists('Product_Log', 'Location_Id_fk');
-- CALL DropColumnIfExists('Product_Log', 'LocationLabel');
-- CALL DropColumnIfExists('Product_Log', 'PriceUnit_Code_fk');

ALTER TABLE `Product_Log` 
	ADD COLUMN `BidAskStackStyle_Code_fk` VARCHAR(1) collate utf8_unicode_ci NOT NULL DEFAULT 'N' AFTER `BidAskStackNumberRows`,
    ADD COLUMN `DisplayContractName`  BIT NOT NULL;
    
    
-- ======== DELIVERY LOCATION ========
CALL DropColumnIfExists('DeliveryLocation', 'LocationLabel');

ALTER TABLE `DeliveryLocation` 
	ADD COLUMN `LocationLabel` VARCHAR(50) DEFAULT NULL collate utf8_unicode_ci;


-- ======== PRODUCT DELIVERY LOCATION ========
CALL DropForeignKeyIfExists('ProductDeliveryLocation', 'ProductDeliveryLocation_Product_fk');
CALL DropForeignKeyIfExists('ProductDeliveryLocation', 'ProductDeliveryLocation_DeliveryLocation_fk');

DROP TABLE IF EXISTS `ProductDeliveryLocation`;
CREATE TABLE `ProductDeliveryLocation` (
    `Product_Id_fk` bigint(20) NOT NULL,
    `DeliveryLocation_Id_fk` bigint(20) NOT NULL,
    PRIMARY KEY (`Product_Id_fk`,`DeliveryLocation_Id_fk`),
    CONSTRAINT `ProductDeliveryLocation_Product_fk` FOREIGN KEY (`Product_Id_fk`) REFERENCES `Product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `ProductDeliveryLocation_DeliveryLocation_fk` FOREIGN KEY (`DeliveryLocation_Id_fk`) REFERENCES `DeliveryLocation` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- ======== COMMONQUANTITYVALUES ========
CALL DropColumnIfExists('CommonQuantityValues', 'QuantityText');
CALL DropColumnIfExists('CommonQuantityValues', 'DisplayOrder');
CALL DropColumnIfExists('CommonQuantityValues', 'DisplayQuantityText');
CALL DropIndexIfExists('CommonQuantityValues', 'CommonQuantityValues_Unique');
CALL DropForeignKeyIfExists('CommonQuantityValues', 'Product_Id_fk');
CALL DropForeignKeyIfExists('CommonQuantityValues', 'CommonQuantityValues_Product_fk');

ALTER TABLE `CommonQuantityValues` 
	CHANGE COLUMN `Product_Id_Fk` `Product_Id_Fk` BIGINT(20) NOT NULL,
	ADD COLUMN `QuantityText` VARCHAR(50) collate utf8_unicode_ci NULL COMMENT 'Override text to be used for display' AFTER `Quantity`,
	ADD COLUMN `DisplayOrder` INT NOT NULL DEFAULT 0 AFTER `QuantityText`,
	ADD COLUMN `DisplayQuantityText` BIT(1) NULL DEFAULT false COMMENT 'Hides the QuantityValue field and displays the QuantityText' AFTER `DisplayOrder`,
	ADD UNIQUE INDEX `CommonQuantityValues_Unique` (`Product_Id_Fk`,`Quantity` ASC),
	ADD CONSTRAINT `CommonQuantityValues_Product_fk` FOREIGN KEY (`Product_Id_fk`) REFERENCES `Product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION; 

UPDATE `CommonQuantityValues` c1
JOIN (SELECT id, quantity, 
            (SELECT COUNT(*) 
            FROM    `CommonQuantityValues` as s2
            WHERE   s2.Quantity < s1.Quantity 
            AND     s2.Product_Id_fk = s1.Product_Id_fk) NewDisplayOrder 
      FROM `CommonQuantityValues` s1 
      WHERE IFNULL(QuantityText, 'x') <> 'ToT') c2
      ON c1.Id = c2.Id 
SET   c1.DisplayOrder = (c2.NewDisplayOrder + 1) * 10;

DELETE FROM `CommonQuantityValues`
WHERE  Quantity = '20500';

INSERT INTO `CommonQuantityValues` (`LastUpdatedUser_Id`,`EnteredByUser_Id`,`LastUpdated`,`DateCreated`,`Quantity`,`QuantityText`,`DisplayOrder`,`DisplayQuantityText`,`Product_Id_Fk`) 
VALUES(-1,-1,now(),now(),20500,'ToT',1000,true, 2); -- TODO = CHANGE PRODUCT ID 


CALL DropColumnIfExists('CommonQuantityValues_Log', 'Product_Id_Fk');
CALL DropColumnIfExists('CommonQuantityValues_Log', 'QuantityText');
CALL DropColumnIfExists('CommonQuantityValues_Log', 'DisplayOrder');
CALL DropColumnIfExists('CommonQuantityValues_Log', 'DisplayQuantityText');

ALTER TABLE `CommonQuantityValues_Log`
	ADD COLUMN `QuantityText` VARCHAR(50) collate utf8_unicode_ci NULL AFTER `Quantity`,
	ADD COLUMN `DisplayOrder` INT NOT NULL DEFAULT 0 AFTER `QuantityText`,
    ADD COLUMN `DisplayQuantityText` BIT(1) NULL DEFAULT false AFTER `DisplayOrder`,
    ADD COLUMN `Product_Id_Fk` BIGINT(20) NOT NULL;

    
-- ======== ORDER ========
CALL DropColumnIfExists('Order', 'QuantityText');
CALL DropColumnIfExists('Order', 'DeliveryPeriod');

ALTER TABLE `Order` 
	ADD COLUMN `QuantityText` VARCHAR(50) COLLATE utf8_unicode_ci NULL COMMENT 'Override display value for quantity value e.g. TOT' AFTER `Quantity`,
    ADD COLUMN `DeliveryPeriod` VARCHAR(50) COLLATE utf8_unicode_ci NULL COMMENT 'Delivery Period Description' AFTER `Price`,
    MODIFY COLUMN `Message` VARCHAR(1000) COLLATE utf8_unicode_ci NULL,
    MODIFY COLUMN `DateCreated` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
    MODIFY COLUMN `LastUpdated` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6);
    -- CHANGE COLUMN `Price` `OldPrice` DECIMAL(14,6) NULL DEFAULT NULL COMMENT 'To be dropped in future release';

    
CALL DropColumnIfExists('Order_Log', 'QuantityText');
CALL DropColumnIfExists('Order_Log', 'DeliveryPeriod');

ALTER TABLE `Order_Log` 
	ADD COLUMN `QuantityText` VARCHAR(50) collate utf8_unicode_ci NULL AFTER `Quantity`,
    ADD COLUMN `DeliveryPeriod` VARCHAR(50) COLLATE utf8_unicode_ci NULL AFTER `Price`,
    MODIFY COLUMN `Message` VARCHAR(1000) COLLATE utf8_unicode_ci NULL,
    MODIFY COLUMN `DateCreated` DATETIME(6) NOT NULL,
    MODIFY COLUMN `LastUpdated` DATETIME(6) NOT NULL;
        -- CHANGE COLUMN `Price` `OldPrice` DECIMAL(14,6) NULL DEFAULT NULL;
    
-- ======== DEAL ========
ALTER TABLE `Deal` 
    MODIFY COLUMN `DateCreated` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
    MODIFY COLUMN `LastUpdated` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6);
    
    
-- ======== PRICETYPE ========
CALL DropForeignKeyIfExists('PriceBasis', 'FK_PriceBasis_PriceType');

DROP TABLE IF EXISTS `PriceType`;
CREATE TABLE `PriceType` (
	`Id` bigint(20) NOT NULL ,
	`Name` varchar(20) NOT NULL,
	PRIMARY KEY (`Id`)
);


-- ======== PRICEBASIS ========
CALL DropForeignKeyIfExists('PriceLineBasis', 'FK_PriceLineBasis_PriceBasis');
CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_PriceBasis');

DROP TABLE IF EXISTS `PriceBasis`;
CREATE TABLE `PriceBasis` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ShortName` varchar(20) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `PriceType_Id_fk` bigint(20) NOT NULL,  
  `PriceUnit_Code_fk` varchar(10) NOT NULL,
  `Currency_Code_fk` varchar(3) NOT NULL,
  `PriceMinimum` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `PriceMaximum` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `PriceIncrement` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `PriceDecimalPlaces` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `PriceSignificantFigures` decimal(14,6) NOT NULL DEFAULT '0.000000',-- From Product
  `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `PriceBasis_idx` (`PriceType_Id_fk` ASC),
  CONSTRAINT `FK_PriceBasis_PriceType` FOREIGN KEY (`PriceType_Id_fk`) REFERENCES `PriceType` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PriceBasis_Unit` FOREIGN KEY (`PriceUnit_Code_fk`) REFERENCES `Unit` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PriceBasis_Currency` FOREIGN KEY (`Currency_Code_fk`) REFERENCES `Currency` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `PriceBasis_Log`;
CREATE TABLE `PriceBasis_Log` (
  `Id` bigint(20) NOT NULL,
  `ShortName` varchar(20) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `PriceType_Id_fk` bigint(20) NOT NULL,
  `PriceUnit_Code_fk` varchar(10) NOT NULL,
  `Currency_Code_fk` varchar(3) NOT NULL,
  `PriceMinimum` decimal(14,6) NOT NULL,
  `PriceMaximum` decimal(14,6) NOT NULL,
  `PriceIncrement` decimal(14,6) NOT NULL,
  `PriceDecimalPlaces` decimal(14,6) NOT NULL,
  `PriceSignificantFigures` decimal(14,6) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `LastUpdated` datetime(6) NOT NULL,
  `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== ProductPriceLine ========
CALL DropForeignKeyIfExists('ProductPriceLine', 'FK_ProductPriceLine_ProductTenor');
CALL DropForeignKeyIfExists('ProductPriceLine', 'FK_ProductPriceLine_BasisProduct');
CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_ProductPriceLine');
CALL DropForeignKeyIfExists('ProductPriceLineBasisPeriod', 'FK_ProductPriceLineBasisPeriod_PriceBasis');

DROP TABLE IF EXISTS `ProductPriceLine`;
CREATE TABLE `ProductPriceLine` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    -- `Product_Id_fk` bigint(20) NOT NULL COMMENT 'ProductTenor this applies to',
    `ProductTenor_Id_fk` bigint(20) NOT NULL  COMMENT 'ProductTenor this applies to',
    `BasisProduct_Id_fk` bigint(20) NULL COMMENT 'Underlying Product Constinuent e.g for Mixed Cargo products',
    `SequenceNo` bigint(20) NOT NULL DEFAULT 1 COMMENT 'Identifies the pricing choice 1 or 2',
    `Description` VARCHAR(100) NULL DEFAULT 'Pricing Basis {SequenceNo}',
    `PercentageOptionList` varchar(100) NULL COMMENT 'Restrict values to items from list e.g. 10|25|50|100', -- Consider becoming separate table
    `DefaultDisplay` bit NOT NULL DEFAULT b'0',
    `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`),
    UNIQUE INDEX `ProductPriceLine_unique` (`Id` ASC),
    INDEX `ProductPriceLine_ProductTenor_idx` (`ProductTenor_Id_fk` ASC),
    CONSTRAINT `FK_ProductPriceLine_ProductTenor` FOREIGN KEY (`ProductTenor_Id_fk`) REFERENCES `ProductTenor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_ProductPriceLine_BasisProduct` FOREIGN KEY (`BasisProduct_Id_fk`) REFERENCES `Product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `ProductPriceLine_Log`;
CREATE TABLE `ProductPriceLine_Log` (
    `Id` bigint(20) NOT NULL,
    -- `Product_Id_fk` bigint(20) NOT NULL,
    `ProductTenor_Id_fk` bigint(20) NOT NULL,
    `BasisProduct_Id_fk` bigint(20) NULL,
    `SequenceNo` bigint(20) NOT NULL,
    `Description` VARCHAR(100) NULL,
    `PercentageOptionList` varchar(100) NULL,
    `DefaultDisplay` bit NOT NULL,
    `DateCreated` datetime(6) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== ProductPriceLineBasisPeriod ========
DROP TABLE IF EXISTS `ProductPriceLineBasisPeriod`;
CREATE TABLE `ProductPriceLineBasisPeriod` (
    `ProductPriceLine_Id_fk` bigint(20) NOT NULL,
    `PriceBasis_Id_fk` bigint(20) NOT NULL, 
    `Period_Id_fk` bigint(20) NOT NULL, 
    `DisplayOrder` INT NOT NULL DEFAULT 0,
    `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`, `Period_Id_fk`),
    CONSTRAINT `FK_ProductPriceLineBasisPeriod_ProductPriceLine` FOREIGN KEY (`ProductPriceLine_Id_fk`) REFERENCES `ProductPriceLine` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_ProductPriceLineBasisPeriod_PriceBasis` FOREIGN KEY (`PriceBasis_Id_fk`) REFERENCES `PriceBasis` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_ProductPriceLineBasisPeriod_Period` FOREIGN KEY (`Period_Id_fk`) REFERENCES `Period` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `ProductPriceLineBasisPeriod_Log`;
CREATE TABLE `ProductPriceLineBasisPeriod_Log` (
    `ProductPriceLine_Id_fk` bigint(20) NOT NULL,
    `PriceBasis_Id_fk` bigint(20) NOT NULL, 
    `Period_Id_fk` bigint(20) NOT NULL,
    `DisplayOrder` INT NOT NULL DEFAULT 0,
    `DateCreated` datetime(6) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`ProductPriceLine_Id_fk`,`PriceBasis_Id_fk`, `Period_Id_fk`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== PriceLine ========
CALL DropForeignKeyIfExists('OrderPriceLine', 'FK_OrderPriceLine_PriceLine');
CALL DropForeignKeyIfExists('ExternalDeal', 'FK_ExternalDeal_PriceLine');
CALL DropForeignKeyIfExists('PriceLineBasis', 'FK_PriceLineBasis_PriceLine');

DROP TABLE IF EXISTS `PriceLine`;
CREATE TABLE `PriceLine` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `SequenceNo` INT NOT NULL DEFAULT 1,
    `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `PriceLine_Log`;
CREATE TABLE `PriceLine_Log` (
    `Id` bigint(20) NOT NULL,
    `SequenceNo` INT NOT NULL,
    `DateCreated` datetime(6) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ======== OrderPriceLine ========
DROP TABLE IF EXISTS `OrderPriceLine`;    
CREATE TABLE `OrderPriceLine` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `Order_Id_fk` bigint(20) NOT NULL,    	
    `PriceLine_Id_fk` bigint(20) NOT NULL,
    `OrderStatus_Code_fk` CHAR(1) NOT NULL,
    `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`),
    UNIQUE INDEX `OrderPriceLine_UNIQUE` (`PriceLine_Id_fk` ASC), -- Effectively a 1-1 mapping table so priceline should only be mapped once
    INDEX `OrderPriceLine_OrderStatusCode_idx` (`OrderStatus_Code_fk` ASC),
    INDEX `OrderPriceLine_OrderId_idx` (`Order_Id_fk` ASC),
    INDEX `OrderPriceLine_PriceLineId_idx` (`PriceLine_Id_fk` ASC),
    CONSTRAINT `FK_OrderPriceLine_OrderStatusCode` FOREIGN KEY (`OrderStatus_Code_fk`) REFERENCES `OrderStatus` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_OrderPriceLine_Order` FOREIGN KEY (`Order_Id_fk`) REFERENCES `Order` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_OrderPriceLine_PriceLine` FOREIGN KEY (`PriceLine_Id_fk`) REFERENCES `PriceLine` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `OrderPriceLine_Log`;    
CREATE TABLE `OrderPriceLine_Log` (
    `Id` bigint(20) NOT NULL,
    `Order_Id_fk` bigint(20) NOT NULL,    	
    `PriceLine_Id_fk` bigint(20) NOT NULL,
    `OrderStatus_Code_fk` CHAR(1) NOT NULL,
    `DateCreated` datetime(6) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ======== ExternalDeal ========
CALL DropForeignKeyIfExists('ExternalDeal', 'FK_ExternalDeal_PriceLine');
CALL DropColumnIfExists('ExternalDeal', 'QuantityText');
CALL DropColumnIfExists('ExternalDeal', 'PriceLine_Id_fk');

ALTER TABLE `ExternalDeal` 
    ADD COLUMN `QuantityText` VARCHAR(50) NULL COMMENT 'Override display value for quantity value e.g. TOT' AFTER `Quantity`,
    ADD COLUMN `PriceLine_Id_fk` bigint(20) NULL,
    -- CHANGE COLUMN `Price` `OldPrice` DECIMAL(14,6) NULL DEFAULT NULL COMMENT 'To be dropped in future release';
    MODIFY COLUMN `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    MODIFY COLUMN `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    ADD INDEX `ExternalDeal_PriceLineId_idx` (`PriceLine_Id_fk` ASC),
    ADD CONSTRAINT `FK_ExternalDeal_PriceLine` FOREIGN KEY (`PriceLine_Id_fk`) REFERENCES `PriceLine` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


CALL DropColumnIfExists('ExternalDeal_Log', 'QuantityText');
CALL DropColumnIfExists('ExternalDeal_Log', 'PriceLine_Id_fk');

ALTER TABLE `ExternalDeal_Log` 
    ADD COLUMN `QuantityText` VARCHAR(50) NULL AFTER `Quantity`,
    ADD COLUMN `PriceLine_Id_fk` bigint(20) NULL;

        
-- ======== PriceLineBasis ========
DROP TABLE  IF EXISTS `PriceLineBasis` ;
CREATE TABLE `PriceLineBasis` (
    `Id` bigint(20) NOT NULL AUTO_INCREMENT,
    `PriceLine_Id_fk` bigint(20) NOT NULL,  
    `PriceBasis_Id_fk` BIGINT(20) NOT NULL,
    `SequenceNo` INT NOT NULL DEFAULT 1 ,
    `Product_Id_fk` BIGINT(20) NULL COMMENT 'Product Constinuent for Mixed Cargo products',
    `Description` VARCHAR(400) NULL COMMENT 'Derived field - e.g. Product Bases 1',
    `PercentageSplit` INT NOT NULL DEFAULT 100,
    `TenorCode_fk` VARCHAR(10) NOT NULL,
    `PricingPeriod` VARCHAR(50) NULL,
    `PricingPeriodFrom` DATE NOT NULL,
    `PricingPeriodTo` DATE NOT NULL,
    `PriceLineBasisValue` decimal(14,6) NOT NULL,
    `DateCreated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
    PRIMARY KEY (`Id`),  
    UNIQUE INDEX `PriceLineBasis_Unique` (`PriceLine_Id_fk` ASC, `SequenceNo` ASC),
    INDEX `PriceLineBasis_TenorCode_idx` (`TenorCode_fk` ASC),
    INDEX `PriceLineBasis_PriceLineId_idx` (`PriceLine_Id_fk` ASC),
    INDEX `PriceLineBasis_PriceBasisId_idx` (`PriceBasis_Id_fk` ASC),
    INDEX `PriceLineBasis_ProductId_idx` (`Product_Id_fk` ASC),
    CONSTRAINT `FK_PriceLineBasis_TenorCode` FOREIGN KEY (`TenorCode_fk`) REFERENCES `TenorCode` (`Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_PriceLineBasis_PriceLine` FOREIGN KEY (`PriceLine_Id_fk`) REFERENCES `PriceLine` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_PriceLineBasis_PriceBasis` FOREIGN KEY (`PriceBasis_Id_fk`) REFERENCES `PriceBasis` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_PriceLineBasis_Product` FOREIGN KEY (`Product_Id_fk`) REFERENCES `Product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE  IF EXISTS `PriceLineBasis_Log` ;
CREATE TABLE `PriceLineBasis_Log` (
    `Id` bigint(20) NOT NULL,
    `PriceLine_Id_fk` bigint(20) NOT NULL,  
    `PriceBasis_Id_fk` BIGINT(20) NOT NULL,
    `SequenceNo` INT NOT NULL DEFAULT 1 ,
    `Product_Id_fk` BIGINT(20) NULL,
    `Description` VARCHAR(400) NULL,
    `PercentageSplit` INT NOT NULL DEFAULT 100,
    `TenorCode_fk` VARCHAR(10) NOT NULL,
    `PricingPeriod` VARCHAR(50) NULL,
    `PricingPeriodFrom` DATE NOT NULL,
    `PricingPeriodTo` DATE NOT NULL,
    `PriceLineBasisValue` decimal(14,6) NOT NULL,
    `DateCreated` datetime(6) NOT NULL,
    `LastUpdated` datetime(6) NOT NULL,
    `LastUpdated_User_Id` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`,`LastUpdated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    


DROP PROCEDURE IF EXISTS DropForeignKeyIfExists;
DROP PROCEDURE IF EXISTS DropColumnIfExists;
DROP PROCEDURE IF EXISTS DropIndexIfExists;
