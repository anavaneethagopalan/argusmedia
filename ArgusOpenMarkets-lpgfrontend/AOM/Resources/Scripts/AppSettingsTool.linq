<Query Kind="Program" />

/// <sum/// Top folder- starting path
        /// </summary>
void Main()
{

	CreateAGitStashFile();
	Go();	
    Console.WriteLine("number of changes : " + numberOfChanges);
    Console.ReadLine();
}

 		/// <summary>
        /// Top folder- starting path
        /// </summary>
        private static string topProjectFolderPath = @"C:\Projects\AOM\ArgusOpenMarkets\AOM\AOMService\AOM";
		
		/// <summary>
        /// Creates a Git checkout App.conf file for all the App.config files found in the project
		/// put one folder up so u dont check it in
		/// use before checkin to not have to deal with merge issues
        /// </summary>
		private static string gitCheckoutFilePath = @"C:\\Projects\\AOM\\checkoutApp.bat";
		
        //local settings
        private static Dictionary<string, string> replacebleItems = new Dictionary<string, string>()
        {
            //{"aom.transport.bus" , @"<add name=""aom.transport.bus"" connectionString=""host=10.23.1.16;username=aomivan;password=aomivan;virtualHost=aomivan;"" />"},
            //{"argus.transport.bus", @"<add name=""argus.transport.bus"" connectionString=""host=10.23.1.16;username=AOM;password=AOM;virtualHost=AOM;"" />"},
            {"AomModel", @"<add name=""AomModel"" connectionString=""server=localhost;user id=root;password=pass;persistsecurityinfo=True;database=aom"" providerName=""MySql.Data.MySqlClient"" />"},
            {"CrmModel", @"<add name=""CrmModel"" connectionString=""server=localhost;user id=root;password=pass;persistsecurityinfo=True;database=crm"" providerName=""MySql.Data.MySqlClient"" />"}
        };

        // remote settings
        // private static Dictionary<string, string> replacebleItems = new Dictionary<string, string>()
        // {
        //    {"aom.transport.bus" , @"<add name=""aom.transport.bus"" connectionString=""host=10.23.1.16;username=aomivan;password=aomivan;virtualHost=aomivan;"" />"},
        //    {"argus.transport.bus", @"<add name=""argus.transport.bus"" connectionString=""host=10.23.1.16;username=AOM;password=AOM;virtualHost=AOM;"" />"},
        //    {"AomModel", @"<add name=""AomModel"" connectionString=""server=aominstancedev.co46fwzab7bh.eu-west-1.rds.amazonaws.com;user id=aom_user;password=Argu5Med1a;persistsecurityinfo=True;database=aom;persistsecurityinfo=True;database=aom"" providerName=""MySql.Data.MySqlClient"" />"},
        //    {"CrmModel", @"<add name=""CrmModel"" connectionString=""server=aominstancedev.co46fwzab7bh.eu-west-1.rds.amazonaws.com;user id=aom_user;password=Argu5Med1a;persistsecurityinfo=True;database=aom;persistsecurityinfo=True;database=crm"" providerName=""MySql.Data.MySqlClient"" />"}
        // };

       
        private static int numberOfChanges = 0;
		
        public static void CreateAGitStashFile()
        {
            var filePattern = "*.config";
            var filesFound = GetAllFiles(topProjectFolderPath, filePattern);
            var newFiles = new List<string>();
            foreach (var f in filesFound)
            {
                var file = f.Replace("\\App.config", "").Replace("\\app.config", "").Replace("\\Web.config", "").Replace("\\","/");
                newFiles.Add("cd " + file + " \n  git checkout App.config");
				newFiles.Add("cd " + file + " \n  git checkout app.config");
				newFiles.Add("cd " + file + " \n  git checkout Web.config");
            }

            File.WriteAllLines(gitCheckoutFilePath, newFiles.ToArray());
        }

	   
        public static void Go(){
            var filePattern = "*.config";
            var filesFound = GetAllFiles(topProjectFolderPath, filePattern);
			foreach(var f in filesFound){
				Console.Out.WriteLine(f);
			}
            Console.WriteLine("Files found: " + filesFound.Count);
            //for each file
            for (var f =0; f <filesFound.Count; f++)
            {
                var file = filesFound[f];
				
                var lines = new List<string>(System.IO.File.ReadAllLines(file));
                var newLines = new List<string>();
                var line = string.Empty;

                //for each line
                for (var lineIndex = 0; lineIndex < lines.Count; lineIndex++)
                {
                    line = lines[lineIndex];
                    //newLines.Add(line);
                    foreach (var kvp in replacebleItems)
                    {
                        var commentedLine = CommentOutLine(line);
                        if (line.Contains(kvp.Key) && !IsCommentedOut(line))
                        {
                            if (line == commentedLine || line == kvp.Value)
                                break;
                            //coment out old line
                            line = commentedLine;
                            newLines.Add(kvp.Value);
                            numberOfChanges ++;
                            break;
                        }
                    }
                    newLines.Add(line);
                }
                //all done, save file
                SaveFile(file, newLines);
                Console.WriteLine("Saving " + file);
            }
        }

        private static string CommentOutLine(string line)
        {
            return String.Format("<!-- {0} -->", line);
        }
        static List<string> lstFilesFound = new List<string>();
        static List<string> GetAllFiles(string startDir, string fileName)
        {
            try
            {
                foreach (string d in Directory.GetDirectories(startDir))
                {
                    foreach (string f in Directory.GetFiles(d, fileName))
                    {
                        if (f.Contains("App") || f.Contains("app") || f.Contains("Web"))
                        {
                            lstFilesFound.Add(f);
                        }
                    }
                    GetAllFiles(d, fileName);
                }
            }
            catch (System.Exception excpt)
            {
                // Console.WriteLine(excpt.Message);
            }
            return lstFilesFound;
        }

        private static bool IsCommentedOut(string line)
        {
            return line.Contains("<!--") || line.Contains("-->");
        }

        private static void SaveFile(string filePath, List<string> text)
        {
            File.WriteAllLines(filePath, text.ToArray());
        }