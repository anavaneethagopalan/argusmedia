
Server.xml
----------------------------------

Add this Server.xml to the 'etc' folder of your installation to enable authentication. 
Or alternatively, add the <authentication-handlers> elements to the 'security' section:

<security>
	<!-- This is the full name of a class, on the class path, which implements 
			the AuthorisationHandler interface in the Java Publisher API. -->
			
	<authentication-handlers>
		<authentication-handler class="test.LocalAuthenticationHandler"/>
                <control-authentication-handler handler-name="dotnet-remote-control-authentication-handler-name"/>
        </authentication-handlers>            
          
 </security>

You will also need to add the included jar file to your server's 'ext' folder. 

----------------------------------