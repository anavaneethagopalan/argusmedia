import {Component, View} from 'angular2/core';
import {Router, RouteConfig, RouterLink, RouterOutlet} from 'angular2/router';
import {LoginService} from './login.service';

@Component({
    selector: 'login',
    templateUrl: 'app/login/login.html',
    providers: [LoginService]
})

export class LoginComponent {
    // We inject the router via DI
    router: Router;
    password: string = '';
    username: string = '';
    errorMessage: string = '';
    loginService: LoginService;

    constructor(loginService: LoginService, router: Router) {

        this.loginService = loginService;
        this.router = router;
    }

    login(event, username, password) {
        // TODO : Need to authenticate the user.
        if(this.loginService.login(username, password)){
            localStorage.setItem('jwt', username);
            this.router.parent.navigateByUrl('/home');
        }else{
            localStorage.clear();
            console.log('Error logging user on');
        }
    }
}