import {RouteDefinition} from 'angular2/router';
import {HomeComponent} from './home/home.component';
import {TodolistComponent} from './todolist/todolist.component';
import {SimplebindComponent} from './simplebind/simplebind.component';
import {UserComponent} from './users/users.component'
import {UserService} from'./users/user.service';
import {LoginComponent} from './login/login.component';
import {OrganisationsComponent} from './organisations/organisations.component';

export var APP_ROUTES: RouteDefinition[] = [
    { path: '/login', name: 'Login', component: LoginComponent },
    { path: '/home', name: 'Home', component: HomeComponent, useAsDefault: true },
    // { path: '/simplebind', name: 'Simplebind', component: SimplebindComponent },
    // { path: '/todolist', name: 'Todolist', component: TodolistComponent },
    { path: '/organisations', name: 'Organisations', component: OrganisationsComponent},
    { path: '/users', name: 'Users', component: UserComponent }
];
