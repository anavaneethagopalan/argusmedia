System.register(['./user.model'], function(exports_1) {
    var user_model_1;
    var UserService;
    return {
        setters:[
            function (user_model_1_1) {
                user_model_1 = user_model_1_1;
            }],
        execute: function() {
            // import {Http, Response, HTTP_PROVIDERS} from "angular2/http";
            // import 'rxjs/add/operator/map';
            "use strict";
            UserService = (function () {
                // constructors do dependency injection in Angular2
                function UserService() {
                    this.serviceEndPoint = 'http://192.168.56.1:8012/api';
                }
                UserService.prototype.getUsers = function (cb, ctx) {
                    user: user_model_1.User;
                    users: Array[user_model_1.User];
                    var dataReq = jQuery.get(this.serviceEndPoint + '/users');
                    dataReq.promise().then(function (data) {
                        if (data) {
                            this.users = new Array();
                            data = JSON.parse(data);
                            for (var i = 0; i < data.length; i++) {
                                var name_1 = data[i].name + ".";
                                this.user = new user_model_1.User(data[i].id, name_1, data[i].isActive, data[i].isBlocked, data[i].organisation, data[i].username, data[i].lastSeen);
                                this.users.push(this.user);
                            }
                            if (cb) {
                                cb(this.users, ctx);
                            }
                        }
                    });
                };
                UserService.prototype.userOperation = function (user, operation) {
                    switch (operation) {
                        case "terminate":
                            this.terminateUser(user);
                            break;
                        case "reset":
                            this.resetPassword(user);
                            break;
                        case "blcok":
                            this.blockUser(user);
                            break;
                    }
                };
                UserService.prototype.resetPassword = function (user) {
                    var userAction = { 'action': 'reset', adminUser: user };
                    if (user) {
                        jQuery.ajax({
                            type: 'POST',
                            url: this.serviceEndPoint + '/useraction',
                            data: userAction
                        });
                    }
                };
                UserService.prototype.terminateUser = function (user) {
                    if (user) {
                        var adminAction = { 'action': 'terminate', adminUser: user };
                        jQuery.ajax({
                            type: 'POST',
                            url: this.serviceEndPoint + '/useraction',
                            data: adminAction
                        });
                    }
                };
                UserService.prototype.blockUser = function (user) {
                    if (user) {
                        var adminAction = { 'action': 'block', adminUser: user };
                        jQuery.ajax({
                            type: 'POST',
                            url: this.serviceEndPoint + '/useraction',
                            data: adminAction
                        });
                    }
                };
                return UserService;
            })();
            exports_1("UserService", UserService);
        }
    }
});

//# sourceMappingURL=user.service.js.map
