export class User {
    public id: number;
    public name: string;
    public isActive: boolean;
    public isBlocked: boolean;
    public organisation: string;
    public username: string;
    public lastSeen: Date;

    static create(user:User) {
        return new User(user.id, user.name, user.isActive, user.isBlocked, user.organisation, user.username, user.lastSeen);
    }

    constructor(id:number, name:string, isActive:boolean, isBlocked: boolean, organisation:string, username:string, lastSeen:Date) {
        this.id = id;
        this.name = name;
        this.isActive = isActive;
        this.isBlocked = isBlocked;
        this.organisation = organisation;
        this.username = username;

        if (lastSeen) {
            this.lastSeen = new Date(lastSeen);
        }
    }

    clear() {
        this.id = -1;
        this.name = '';
        this.isActive = false;
        this.isBlocked = false;
        this.organisation = '';
        this.username = '';
        this.lastSeen = null;
    }
}