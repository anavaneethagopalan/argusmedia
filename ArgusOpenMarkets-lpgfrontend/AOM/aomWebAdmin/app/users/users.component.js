System.register(['angular2/core', 'angular2/common', './user.service', '../pipes/search-pipe/search-pipe', '../directives/search-box/search-box', '../directives/popup/popup-layer', '../directives/popup/modalComponent'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, user_service_1, search_pipe_1, search_box_1, popup_layer_1, modalComponent_1;
    var UserComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (search_pipe_1_1) {
                search_pipe_1 = search_pipe_1_1;
            },
            function (search_box_1_1) {
                search_box_1 = search_box_1_1;
            },
            function (popup_layer_1_1) {
                popup_layer_1 = popup_layer_1_1;
            },
            function (modalComponent_1_1) {
                modalComponent_1 = modalComponent_1_1;
            }],
        execute: function() {
            UserComponent = (function () {
                function UserComponent(userService) {
                    // this.modalComponent = modalComponent;
                    this.userService = userService;
                    this.listUsers = new Array();
                    this.self = this;
                    this.userService.getUsers(this.usersLoaded, this);
                }
                UserComponent.prototype.usersLoaded = function (users, ctx) {
                    // this.listUsers = new Array<User>();
                    ctx.listUsers = new Array();
                    for (var i = 0; i < users.length; i++) {
                        ctx.listUsers.push(users[i]);
                    }
                };
                UserComponent.prototype.resetPassword = function (user) {
                    // this.modalComponent.open();
                    this.userService.resetPassword(user);
                    this.refreshUsers();
                };
                UserComponent.prototype.terminateUser = function (user) {
                    this.userService.terminateUser(user);
                    this.refreshUsers();
                };
                UserComponent.prototype.blockUser = function (user) {
                    this.userService.blockUser(user);
                    this.refreshUsers();
                };
                UserComponent.prototype.refreshUsers = function () {
                    this.userService.getUsers(this.usersLoaded, this);
                };
                UserComponent.prototype.disableTerminateUser = function (user) {
                    var disabled = true;
                    if (user) {
                        if (user.lastSeen) {
                            disabled = false;
                        }
                    }
                    return disabled;
                };
                UserComponent.prototype.disableBlockUser = function (user) {
                    var disabled = true;
                    if (user) {
                        disabled = user.isBlocked;
                    }
                    return disabled;
                };
                UserComponent.prototype.disableResetPassword = function (user) {
                    var disabled = true;
                    if (user) {
                        disabled = user.isBlocked;
                    }
                    return disabled;
                };
                UserComponent = __decorate([
                    core_1.Component({
                        pipes: [search_pipe_1.SearchPipe],
                        selector: 'users',
                        templateUrl: 'app/users/users.html',
                        styleUrls: [
                            'app/users/users.css'
                        ],
                        directives: [common_1.CORE_DIRECTIVES, search_box_1.SearchBox, popup_layer_1.ModalLayer, modalComponent_1.ModalComponent],
                        providers: [user_service_1.UserService]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService])
                ], UserComponent);
                return UserComponent;
            })();
            exports_1("UserComponent", UserComponent);
        }
    }
});

//# sourceMappingURL=users.component.js.map
