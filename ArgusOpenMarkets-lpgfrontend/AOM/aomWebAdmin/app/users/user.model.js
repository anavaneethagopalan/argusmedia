System.register([], function(exports_1) {
    var User;
    return {
        setters:[],
        execute: function() {
            User = (function () {
                function User(id, name, isActive, isBlocked, organisation, username, lastSeen) {
                    this.id = id;
                    this.name = name;
                    this.isActive = isActive;
                    this.isBlocked = isBlocked;
                    this.organisation = organisation;
                    this.username = username;
                    if (lastSeen) {
                        this.lastSeen = new Date(lastSeen);
                    }
                }
                User.create = function (user) {
                    return new User(user.id, user.name, user.isActive, user.isBlocked, user.organisation, user.username, user.lastSeen);
                };
                User.prototype.clear = function () {
                    this.id = -1;
                    this.name = '';
                    this.isActive = false;
                    this.isBlocked = false;
                    this.organisation = '';
                    this.username = '';
                    this.lastSeen = null;
                };
                return User;
            })();
            exports_1("User", User);
        }
    }
});

//# sourceMappingURL=user.model.js.map
