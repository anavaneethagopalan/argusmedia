import {Directive, Attribute, ElementRef, DynamicComponentLoader} from 'angular2/core';
import {Router, RouterOutlet, ComponentInstruction} from 'angular2/router';
import {LoginComponent} from '../../login/login.component';

@Directive({
    selector: 'router-outlet'
})

export class SecureRouter extends RouterOutlet {

    publicRoutes: any;
    private parentRouter: Router;

    constructor(_elementRef: ElementRef, _loader: DynamicComponentLoader,
                _parentRouter: Router, @Attribute('name') nameAttr: string) {
        super(_elementRef, _loader, _parentRouter, nameAttr);

        this.parentRouter = _parentRouter;
        this.publicRoutes = {
            '/login': true
        };
    }

    activate(instruction: ComponentInstruction) {
        var currentUrl = this.parentRouter.lastNavigationAttempt;
        var requestedUrl = instruction.urlPath;
        var loginCredentials = localStorage.getItem('jwt')

        if(requestedUrl != 'login'  && loginCredentials == null){
            // todo: redirect to Login, may be there a better way?
            this.parentRouter.navigateByUrl('/login');
        }
        return super.activate(instruction);
    }
}