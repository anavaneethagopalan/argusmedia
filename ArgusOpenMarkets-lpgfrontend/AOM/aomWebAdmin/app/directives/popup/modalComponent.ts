import {Component} from "angular2/core";
import {View} from "angular2/core";
import {NgIf} from "angular2/common";
import {UserService} from "../../users/user.service";;

@Component({
    selector: 'modal',
    template: `
    <div class="modalPopup container" *ngIf="isOpen">
        <div class="row">
        <span class="col-md-10"><h1>{{operation}} user?</h1></span></div>
      <ng-content></ng-content>

      <div class="row">
      <p>Are you sure you want to {{operation}}  the user: {{username}}</p>
      </div>
      <div class="row">
      <button (click)="yesOperation()" class="btn btn-primary col-xs-2">Yes</button>
      <button (click)="noOperation()" class="btn btn-primary col-xs-2">No</button>
      </div>
    </div>
    <div class="overlay" *ngIf="isOpen" (click)="close()"></div>
  `,
    styles: [`
        .modalPopup {
          background-color: #A9D5EF;
          position: relative;
          z-index: 1000;
        }

        h1{
            text-transform:capitalize
        }


        .overlay {
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          position: fixed;
          background: rgba(0, 0, 0, 0.5);
          z-index: 1;
        }
    `],
    directives: [NgIf]
})
export class ModalComponent {

    userService: UserService;
    user: any;
    operation:string = "Unknown";
    isOpen:boolean = false;
    username:string = "Unknown";

    constructor(userService: UserService) {

        this.userService = userService;
    }

    public  open(user, operation) {
        this.isOpen = true;
        this.operation = operation;
        if (user) {
            this.user = user;
            this.username = user.username;
        }
    }

    camelCase(title){

    }

    yesOperation(){
        if(this.user){
            this.userService.userOperation(this.user, this.operation);
        }

        this.close();
    }

    noOperation(){

        if(this.user){

        }

        this.close();
    }

    public close() {
        this.isOpen = false;
    }
}