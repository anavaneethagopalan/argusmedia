######## Generate Pickles Report ######## 
Install Pickles Command Line via NuGet. NUnit 3 is my test runner.
Once built we can start making use of it by running the executable with our desired parameters:

�feature-directory: Where your Feature Files live, relative to the executable
�output-directory: Where you want your Pickles report to generate
�documentation-format: Documentation format (DHTML, HTML, Word, Excel or JSON)
�link-results-file: Path to NUnit Results.xml (this will allows graphs and metrics)
�test-results-format: nunit, nunit3, xunit, xunit2, mstest, cucumberjson, specrun, vstest


for more information: http://docs.picklesdoc.com/en/latest/StartSimple/


E.g.

cd C:\Projects\ArgusOpenMarkets\AOM\AOMIntegrationTests\packages\Pickles.CommandLin
e.2.16.0\tools>
pickles.exe --feature-directory=C:\Projects\ArgusOpenMarkets\AOM\
AOMIntegrationTests\AOMFeatures --output-directory=C:\Temp\MyDocumentation --doc
umentation-format=Html