﻿@AOM
Feature: BrokerPermissions
	To ensure that broker permission changes are reflected
@Broker
Scenario Outline: 01.Update Broker Permissions for Bid_TraderCreatesBidAndBrokerTriesToExecute
Given "<Trader>" and "<Broker>" broker permissions for "<ProductName>" set to "Allow"
And "<Trader2>" and "<Broker>" broker permissions for "<ProductName>" set to "Allow"
And the broker permissions are set as "<Trader>" to "<Broker>" for, "<ProductName>" to "<StartPermission>"
When I change the Broker permission from "<Trader>" to "<Broker>" for "<ProductName>" to "<AllowOrDeny>"
And I have a "bid" created by "<Trader>" for product "<ProductName>"
And I am logged into "AOM" as user "<Broker>"
Then I am "<Execute>" to execute an order selecting first Trader in the list as the principle
Examples: 
| Description         | ProductName | StartPermission | AllowOrDeny | Trader | Broker | Execute | Trader2 |
| AllowSellFromAllow  | AUTO MTBE   | Allow           | Allow       | ALT64  | AB64   | Allowed | ALT74   |
| DenySellFromAllow   | AUTO MTBE   | Allow           | Deny        | ALT64  | AB64   | Denied  | ALT74   |
| NotSetSellFromAllow | AUTO MTBE   | Allow           | notset      | ALT64  | AB64   | Denied  | ALT74   |
##
| AllowSellFromDeny   | AUTO MTBE   | Deny            | Allow       | ALT64  | AB64   | Allowed | ALT74   |
| DenySellFromDeny    | AUTO MTBE   | Deny            | Deny        | ALT64  | AB64   | Denied  | ALT74   |
| NotSetSellFromNDeny | AUTO MTBE   | Deny            | notset      | ALT64  | AB64   | Denied  | ALT74   |
##
| AllowSellFromNotSet | AUTO MTBE   | notset          | Allow       | ALT64  | AB64   | Allowed | ALT74   |
| DenyFromNotSet      | AUTO MTBE   | notset          | Deny        | ALT64  | AB64   | Denied  | ALT74   |
| NotSetFromNotSet    | AUTO MTBE   | notset          | notset      | ALT64  | AB64   | Denied  | ALT74   |