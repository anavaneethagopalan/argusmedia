﻿@AOM
Feature: CounterPartyPermission
	To ensure that counterparty permission changes are reflect

Scenario Outline: 01. Update CounterParty Permissions for Bid_User2CreatesBidAndUser1TriesToExecute
Given "<user1>" and "<user2>" counterparty permissions for "<ProductName>" buy and sell are set to "Allow"
And the counterparty permissions are set from "<user1>" to "<user2>" for "sell", "<ProductName>" to "<SellStartPermission>"
When I change the Counter Party permission from "<user1>" to "<user2>" for "<BuyOrSell>", "<ProductName>" to "<AllowOrDeny>"
And I have a "bid" created by "<user2>" for product "<ProductName>"
And I am logged into "AOM" as user "<user1>"
Then I am "<Execute>" to execute an order 

Examples: 
| Description         | ProductName | SellStartPermission | AllowOrDeny | BuyOrSell | theirOrganisationName | user1 | user2 | Execute |
| AllowSellFromAllow  | AUTO MTBE   | Allow               | Allow       | Sell      | Auto Test 7           | ALT74 | ALT64 | Allowed |
| DenyBuyFromAllow    | AUTO MTBE   | Allow               | Deny        | Buy       | Auto Test 7           | ALT74 | ALT64 | Allowed |
| DenySellFromAllow   | AUTO MTBE   | Allow               | Deny        | Sell      | Auto Test 7           | ALT74 | ALT64 | Denied  |
| AllowBuyFromAllow   | AUTO MTBE   | Allow               | Allow       | Buy       | Auto Test 7           | ALT74 | ALT64 | Allowed |
| NotSetSellFromAllow | AUTO MTBE   | Allow               | notset      | Sell      | Auto Test 7           | ALT74 | ALT64 | Denied  |
| NotSetBuyFromAllow  | AUTO MTBE   | Allow               | notset      | Buy       | Auto Test 7           | ALT74 | ALT64 | Allowed |
#
| AllowSellFromDeny    | AUTO MTBE | Deny                | Allow       | Sell      | Auto Test 7           | ALT74  | ALT64  | Allowed |
| DenyBuyFromDeny      | AUTO MTBE | Deny                | Deny        | Buy       | Auto Test 7           | ALT74  | ALT64  | Denied  |
| DenySellFromDeny     | AUTO MTBE | Deny                | Deny        | Sell      | Auto Test 7           | ALT74  | ALT64  | Denied  |
| AllowBuyFromDeny     | AUTO MTBE | Deny                | Allow       | Buy       | Auto Test 7           | ALT74  | ALT64  | Denied  |
| NotSetSellFromDeny   | AUTO MTBE | Deny                | notset      | Sell      | Auto Test 7           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromDeny    | AUTO MTBE | Deny                | notset      | Buy       | Auto Test 7           | ALT74  | ALT64  | Denied  |
#
| AllowSellFromNotSet  | AUTO MTBE | notset              | Allow       | Sell      | Auto Test 7           | ALT74  | ALT64  | Allowed |
| DenyBuyFromNotSet    | AUTO MTBE | notset              | Deny        | Buy       | Auto Test 7           | ALT74  | ALT64  | Denied  |
| DenySellFromNotSet   | AUTO MTBE | notset              | Deny        | Sell      | Auto Test 7           | ALT74  | ALT64  | Denied  |
| AllowBuyFromNotSet   | AUTO MTBE | notset              | Allow       | Buy       | Auto Test 7           | ALT74  | ALT64  | Denied  |
| NotSetSellFromNotset | AUTO MTBE | notset              | notset      | Sell      | Auto Test 7           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromNotset  | AUTO MTBE | notset              | notset      | Buy       | Auto Test 7           | ALT74  | ALT64  | Denied  |

###############
Scenario Outline: 02. Update CounterParty Permissions for Bid_User1CreatesBidAndUser2TriesToExecute
Given "<user1>" and "<user2>" counterparty permissions for "<ProductName>" buy and sell are set to "Allow"
And the counterparty permissions are set from "<user1>" to "<user2>" for "buy", "<ProductName>" to "<SellStartPermission>"
When I change the Counter Party permission from "<user1>" to "<user2>" for "<BuyOrSell>", "<ProductName>" to "<AllowOrDeny>"
And I have a "bid" created by "<user1>" for product "<ProductName>"
And I am logged into "AOM" as user "<user2>"
Then I am "<Execute>" to execute an order 
#And I Logout of Aom

Examples: 
| Description          | ProductName               | SellStartPermission | AllowOrDeny | BuyOrSell | theirOrganisationName | user1 | user2 | Execute |
| AllowSellFromAllow   | AUTO MTBE | Allow               | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| DenyBuyFromAllow     | AUTO MTBE | Allow               | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenySellFromAllow    | AUTO MTBE | Allow               | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| AllowBuyFromAllow    | AUTO MTBE | Allow               | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetSellFromAllow  | AUTO MTBE | Allow               | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetBuyFromAllow   | AUTO MTBE | Allow               | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
#
| AllowSellFromDeny    | AUTO MTBE | Deny                | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenyBuyFromDeny      | AUTO MTBE | Deny                | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenySellFromDeny     | AUTO MTBE | Deny                | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| AllowBuyFromDeny     | AUTO MTBE | Deny                | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetSellFromDeny   | AUTO MTBE | Deny                | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromDeny    | AUTO MTBE | Deny                | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
#
| AllowSellFromNotSet  | AUTO MTBE | notset              | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenyBuyFromNotSet    | AUTO MTBE | notset              | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenySellFromNotSet   | AUTO MTBE | notset              | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| AllowBuyFromNotSet   | AUTO MTBE | notset              | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetSellFromNotset | AUTO MTBE | notset              | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromNotset  | AUTO MTBE | notset              | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |


Scenario Outline: 03. Update CounterParty Permissions for Ask_User2CreatesBidAndUser1TriesToExecute
Given "<user1>" and "<user2>" counterparty permissions for "<ProductName>" buy and sell are set to "Allow"
And the counterparty permissions are set from "<user1>" to "<user2>" for "buy", "<ProductName>" to "<SellStartPermission>"
When I change the Counter Party permission from "<user1>" to "<user2>" for "<BuyOrSell>", "<ProductName>" to "<AllowOrDeny>"
And I have a "ask" created by "<user2>" for product "<ProductName>"
And I am logged into "AOM" as user "<user1>"
Then I am "<Execute>" to execute an order 

Examples: 
| Description          | ProductName               | SellStartPermission | AllowOrDeny | BuyOrSell | theirOrganisationName | user1 | user2 | Execute |
| AllowSellFromAllow   | AUTO MTBE | Allow               | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| DenyBuyFromAllow     | AUTO MTBE | Allow               | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenySellFromAllow    | AUTO MTBE | Allow               | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| AllowBuyFromAllow    | AUTO MTBE | Allow               | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetSellFromAllow  | AUTO MTBE | Allow               | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetBuyFromAllow   | AUTO MTBE | Allow               | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
#						
| AllowSellFromDeny    | AUTO MTBE | Deny                | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenyBuyFromDeny      | AUTO MTBE | Deny                | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenySellFromDeny     | AUTO MTBE | Deny                | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| AllowBuyFromDeny     | AUTO MTBE | Deny                | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetSellFromNotset | AUTO MTBE | Deny                | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromNotset  | AUTO MTBE | Deny                | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
#																										   
| AllowSellFromNotset  | AUTO MTBE | notset              | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenyBuyFromNotset    | AUTO MTBE | notset              | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenySellFromNotset   | AUTO MTBE | notset              | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| AllowBuyFromNotset   | AUTO MTBE | notset              | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetSellFromNotset | AUTO MTBE | notset              | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromNotset  | AUTO MTBE | notset              | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |

Scenario Outline: 04.  Update CounterParty Permissions for Ask_User1CreatesBidAndUser2TriesToExecute
Given "<user1>" and "<user2>" counterparty permissions for "<ProductName>" buy and sell are set to "Allow"
And the counterparty permissions are set from "<user1>" to "<user2>" for "sell", "<ProductName>" to "<SellStartPermission>"
When I change the Counter Party permission from "<user1>" to "<user2>" for "<BuyOrSell>", "<ProductName>" to "<AllowOrDeny>"
And I have a "ask" created by "<user1>" for product "<ProductName>"
And I am logged into "AOM" as user "<user2>"
Then I am "<Execute>" to execute an order 

Examples: 
| Description          | ProductName               | SellStartPermission | AllowOrDeny | BuyOrSell | theirOrganisationName | user1 | user2 | Execute |
| AllowSellFromAllow   | AUTO MTBE | Allow               | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| DenyBuyFromAllow     | AUTO MTBE | Allow               | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
| DenySellFromAllow    | AUTO MTBE | Allow               | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| AllowBuyFromAllow    | AUTO MTBE | Allow               | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
| NotSetSellFromAllow  | AUTO MTBE | Allow               | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromAllow   | AUTO MTBE | Allow               | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Allowed |
#						
| AllowSellFromDeny    | AUTO MTBE | Deny                | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| DenyBuyFromDeny      | AUTO MTBE | Deny                | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenySellFromDeny     | AUTO MTBE | Deny                | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| AllowBuyFromDeny     | AUTO MTBE | Deny                | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetSellFromNotset | AUTO MTBE | Deny                | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromNotset  | AUTO MTBE | Deny                | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
#																										   
| AllowSellFromNotset  | AUTO MTBE | notset              | Allow       | Sell      | Load Test 2           | ALT74  | ALT64  | Allowed |
| DenyBuyFromNotset    | AUTO MTBE | notset              | Deny        | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| DenySellFromNotset   | AUTO MTBE | notset              | Deny        | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| AllowBuyFromNotset   | AUTO MTBE | notset              | Allow       | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetSellFromNotset | AUTO MTBE | notset              | notset      | Sell      | Load Test 2           | ALT74  | ALT64  | Denied  |
| NotSetBuyFromNotset  | AUTO MTBE | notset              | notset      | Buy       | Load Test 2           | ALT74  | ALT64  | Denied  |

Scenario Outline: 05. Update permissions when order screen open for Bid_User2CreatesUser1Executes
Given "<user1>" and "<user2>" counterparty permissions for "<ProductName>" buy and sell are set to "Allow"
And I have a "bid" created by "<user2>" for product "<ProductName>"
And I am logged into "AOM" as user "<user1>"
When I open the "bid" created for "<ProductName>"
And I change the Counter Party permission from "<user1>" to "<user2>" for "<BuyOrSell>", "<ProductName>" to "<AllowOrDeny>"
And I click the execute button for the open order
Then I should see a toaster message that contains "There is no bilateral agreement in place" 

Examples: 
| Description | ProductName               | AllowOrDeny | BuyOrSell | user1 | user2 |
| Bid_Sell_Deny        | AUTO MTBE | Deny        | Sell      | ALT74  | ALT64  |
| Bid_Sell_notset      | AUTO MTBE | notset      | Sell      | ALT74  | ALT64  |

Scenario Outline: 06. Update permissions when order screen open for Bid_User1CreatesUser2Executes
Given "<user1>" and "<user2>" counterparty permissions for "<ProductName>" buy and sell are set to "Allow"
And I have a "bid" created by "<user1>" for product "<ProductName>"
And I am logged into "AOM" as user "<user2>"
When I open the "bid" created for "<ProductName>"
And I change the Counter Party permission from "<user1>" to "<user2>" for "<BuyOrSell>", "<ProductName>" to "<AllowOrDeny>"
And I click the execute button for the open order
Then I should see a toaster message that contains "There is no bilateral agreement in place" 

Examples: 
| Description    | ProductName               | AllowOrDeny | BuyOrSell | user1 | user2 |
| Bid_Buy_Deny   | AUTO MTBE | Deny        | Buy       | ALT74  | ALT64  |
| Bid_Buy_NotSet | AUTO MTBE | notset      | Buy       | ALT74  | ALT64  |

Scenario Outline: 07. Update permissions when order screen open for Ask_User2CreatesUser1Executes
Given "<user1>" and "<user2>" counterparty permissions for "<ProductName>" buy and sell are set to "Allow"
And I have a "ask" created by "<user2>" for product "<ProductName>"
And I am logged into "AOM" as user "<user1>"
When I open the "ask" created for "<ProductName>"
And I change the Counter Party permission from "<user1>" to "<user2>" for "<BuyOrSell>", "<ProductName>" to "<AllowOrDeny>"
And I click the execute button for the open order
Then I should see a toaster message that contains "There is no bilateral agreement in place" 

Examples: 
| Description    | ProductName               | AllowOrDeny | BuyOrSell | user1 | user2 |
| Ask_Buy_Deny   | AUTO MTBE | Deny        | Buy       | ALT74  | ALT64  |
| Ask_Buy_NotSet | AUTO MTBE | notset      | Buy       | ALT74  | ALT64  |

Scenario Outline:08. Update permissions when order screen open for Ask_User1CreatesUser2Executes
Given "<user1>" and "<user2>" counterparty permissions for "<ProductName>" buy and sell are set to "Allow"
And I have a "ask" created by "<user1>" for product "<ProductName>"
And I am logged into "AOM" as user "<user2>"
When I open the "ask" created for "<ProductName>"
And I change the Counter Party permission from "<user1>" to "<user2>" for "<BuyOrSell>", "<ProductName>" to "<AllowOrDeny>"
And I click the execute button for the open order
Then I should see a toaster message that contains "There is no bilateral agreement in place" 

Examples: 
| Description    | ProductName               | AllowOrDeny | BuyOrSell | user1 | user2 |
| Ask_Buy_Deny   | AUTO MTBE | Deny        | Sell      | ALT74  | ALT64  |
| Ask_Buy_NotSet | AUTO MTBE | notset      | Sell      | ALT74  | ALT64  |