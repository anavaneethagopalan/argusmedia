﻿@AOM

#Editor1 ED14 and Editor2 ED24 both are created as Argus Editors



Feature: AOM PlusDeal
	To Test Extenal/PLus Deal functionality


######## Editor creates an External Deal #########

Scenario Outline: 01. Editor creates a Pending External Deal and is not visible to other users except other Editor
Given I am logged into "AOM" as user "<editor1>"
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Pending
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Given I am logged into "AOM" as user "<user1>"
Then I should be on the dashboard page <loggedinasuser1>
Then the order with <ordertype>,<dealstatus>,<price> should not be visible in market ticker for <user1>
Given I am logged into "AOM" as user "<editor2>"
Then I should be on the dashboard page <loggedinaseditor2>
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
Examples:
| editor1 | loggedinaseditor1 | contract      | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | user1 | user1pwd  | loggedinasuser1 | editor2 |  loggedinaseditor2 |
| ED14    | Argus Editor 1    | AUTO RED FAME | 856.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | YELLOW     | PENDING    | + DEAL    | ALT34 | Password% | Auto Test 3     | ED24    |  Argus Editor 2    |


Scenario Outline: 02. Editor creates a Verified External Deal and is visible to all users subscribed to that product 
Given I am logged into "AOM" as user "<editor1>"
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Verified
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Given I am logged into "AOM" as user "<user1>"
Then I should be on the dashboard page <loggedinasuser1>
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract      | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | user1 | user1pwd  | loggedinasuser1 | Stkclrother | editor2 | editor2pwd | loggedinaseditor2 |
| ED14    | Password   | Argus Editor 1    | AUTO RED FAME | 857.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | ORANGE     | VERIFIED   | + DEAL    | ALT34 | Password% | Auto Test 3     | WHITE       | ED24    | Password   | Argus Editor 2    |



Scenario Outline: 03. Editor creates a Pending External Deal and then Updates 
Given I am logged into "AOM" as user "<editor1>"
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Pending
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
And I change the product <newproduct> field
And I update the Notes <newnotes> field
Then I click on Update Button 
Then the updated order with <ordertype>, <newproduct>, <newnotes> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract      | price | quantity | location | buyer | seller | broker | notes | Stkclrmine | ordertype | newproduct | newnotes |
| ED14    | Password   | Argus Editor 1    | AUTO RED FAME | 858.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | YELLOW     | + DEAL    | AUTO RED RME | Automation External Deal Updated |



Scenario Outline: 04. Editor creates a Pending External Deal and then Voides 
Given I am logged into "AOM" as user "<editor1>"
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Pending
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
When I Void the product
Then i should be taken to Void Reason selection screen
| title              |
| ENTER VOID REASON: |
And I enter the void reason <voidreason>
And I confirm and click on void
Then the voided order with <ordertype>, <voidstatus>, <voidreason>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract      | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | ordertype | voidreason        | Stkclrother | voidstatus |
| ED14    | Password   | Argus Editor 1    | AUTO RED FAME | 859.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | YELLOW     | + DEAL    | Deal not verified | ORANGE      | VOIDED     |
| ED14    | Password   | Argus Editor 1    | AUTO BIOFUEL  | 860.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | YELLOW     | + DEAL    | Other             | ORANGE      | VOIDED     |


Scenario Outline: 05. Editor creates a Pending External Deal and then Verifies 
Given I am logged into "AOM" as user "<editor1>"	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Pending
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
Then I click on Verify button
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract     | price | quantity | location | buyer | seller | broker | notes | Stkclrmine | ordertype | Stkclrother |
| ED14    | Password   | Argus Editor 1    | AUTO RED RME | 861.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | YELLOW     | + DEAL    | ORANGE      |

#AOMK-447
Scenario Outline: 06. Editor creates a Verified External Deal and then should not be able to Update
Given I am logged into "AOM" as user "<editor1>"
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Verified
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Void Deal Page
| title                       |
| VOID DEAL (EXTERNAL TO AOM) |
Then the update button should not be present
Then I close the order screen
#And I change the product <newproduct> field
#And I update the Notes <newnotes> field
#Then I click on Update Button 
#Then the updated order with <ordertype>, <newproduct>, <notes> should appear in market ticker
#And the color of the market ticker item should be <Stkclrmine>
Examples:
| editor1 | loggedinaseditor1 | contract     | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | ordertype | newproduct   | newnotes                         |
| ED14    | Argus Editor 1    | AUTO RED RME | 862.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | ORANGE     | + DEAL    | AUTO RED RME | Automation External Deal Updated |


#AOMK-447
Scenario Outline: 07. Editor creates a Verified External Deal and Voides
Given I am logged into "AOM" as user "<editor1>"	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Verified
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Void Deal Page
| title                       |
| VOID DEAL (EXTERNAL TO AOM) |
When I Void the product
Then i should be taken to Void Reason selection screen
| title              |
| ENTER VOID REASON: |
And I enter the void reason <voidreason>
And I confirm and click on void
Then the voided order with <ordertype>, <voidstatus>, <voidreason>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Examples:
| editor1 | loggedinaseditor1 | contract     | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | ordertype | voidreason | voidstatus |
| ED14    | Argus Editor 1    | AUTO BIOFUEL | 863.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | ORANGE     | + DEAL    | Other      | VOIDED     |


Scenario Outline: 08. Editor creates a External Deal and Voides
Given I am logged into "AOM" as user "<editor1>"
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Verified
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Void Deal Page
| title                       |
| VOID DEAL (EXTERNAL TO AOM) |
When I Void the product
Then i should be taken to Void Reason selection screen
| title              |
| ENTER VOID REASON: |
And I do not enter the void reason <voidreason>
Then I verify that void button is disabled
Then I cancel the void reason selection screen
And I exit the void screen
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract     | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | ordertype | voidreason | voidstatus |
| ED14    | Password   | Argus Editor 1    | AUTO BIOFUEL | 864.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | ORANGE     | + DEAL    | Other      | VOIDED     |

#@ignore
#Needs modification 
#Scenario Outline: 09. Products that are subscribed to an organisation are listed in Contract drop down Menu
#Given I am logged into "AOM" as user "<editor1>"
#Then I should be on the dashboard page <loggedinaseditor1>
#When I click on the Plus Deal
#Then I should be taken to the Plus Deal page
#| title                         |
#| REPORT DEAL (EXTERNAL TO AOM) |
#Then I verify all the products are available in the Contract drop down Menu
#| Products                                           |
#| AUTO RED RME,AUTO RED FAME,AUTO BIOFUEL,AUTO OTHER |
#Then I exit the Plus Deal Screen
#Examples:
#| editor1 | editor1pwd | loggedinaseditor1 | 
#| ED14    | Password   | Argus Editor 1    |



######## Trader creates an External Deal #########

Scenario Outline: 09. Trader creates a new External Deal and is not visible to other users except editor
Given I am logged into "AOM" as user "<user1>"
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Given I am logged into "AOM" as user "<user2>"
Then I should be on the dashboard page <loggedinasuser2>
Then the order with <ordertype>,<dealstatus>,<price> should not be visible in market ticker for <user2>
Given I am logged into "AOM" as user "<editor>"	
Then I should be on the dashboard page <loggedinaseditor>
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclreditor>
Then I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract      | price | quantity | location | buyer | seller | broker | notes | Stkclrmine | dealstatus | ordertype | user2 | user2pwd | loggedinasuser2 | editor | editorpwd | loggedinaseditor | Stkclreditor |
| ALT54 | Password% | Auto Test 5     | AUTO RED FAME | 865.00 | 267      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | PENDING    | + DEAL    | ALT34 | Password% | Auto Test 3     | ED14   | Password  | Argus Editor 1   | YELLOW       |




@211
@Ignore
Scenario Outline: 10. Trader creates a new External Deal and attempts to update it
Given I am logged into "AOM" as user "<user1>"	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
And I change the product <newproduct> field
And I update the Notes <newnotes> field
Then I click on Update Button 
Then the updated order with <ordertype>, <newproduct>, <newnotes> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract      | price | quantity | location | buyer | seller | broker | notes | Stkclrmine | dealstatus | ordertype | Stkclreditor |
| ALT54 | Password% | Auto Test 5     | AUTO RED FAME | 866.00 | 267      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | PENDING    | + DEAL    | WHITE        |

@211
@Ignore
Scenario Outline: 11. Trader creates a new External Deal and unable to Void it
Given I am logged into "AOM" as user "<user1>"
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
When I Void the product
Then i should be taken to Void Reason selection screen
| title              |
| ENTER VOID REASON: |
And I enter the void reason <voidreason>
And I confirm and click on void
Then the voided order with <ordertype>, <voidstatus>, <voidreason>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract     | price  | quantity | location | buyer   | seller   | broker   | notes                   | Stkclrmine | voidreason | ordertype | Stkclrother | voidstatus | dealstatus |
| ALT54 | Password% | Auto Test 5     | AUTO RED RME | 867.00 | 267      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | Other      | + DEAL    | WHITE       | VOIDED     | PENDING    |


Scenario Outline: 12. Trader creates a new External Deal and Editor verifies it
Given I am logged into "AOM" as user "<user1>"
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Given I am logged into "AOM" as user "<editor1>"
Then I should be on the dashboard page <loggedinaseditor1>
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclreditor>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
Then I click on Verify button
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract     | price  | quantity | location | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | editor1 | editor1pwd | loggedinaseditor1 | Stkclreditor | Stkclrother |
| ALT54 | Password% | Auto Test 5     | AUTO RED RME | 868.00 | 267      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | PENDING    | + DEAL    | ED14    | Password   | Argus Editor 1    | YELLOW       | WHITE       |
