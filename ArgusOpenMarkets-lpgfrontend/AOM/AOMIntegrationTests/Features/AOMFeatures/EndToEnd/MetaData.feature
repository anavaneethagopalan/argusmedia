﻿@AOM

# NEW AUTO OTHER product created with METADATA values Ports (Singapore), Grades(Bitumen Grades), Manual (String of 1-100)
# AUTO OTHER is subscribed to ALT34, ALT44, ALT54, AB34, AB44, AB54
# ALT34 allows buy/sell to ALT44 for AUTO OTHER product
# AB34 allows buy/sell to ALT34 and ALT44 for AUTO OTHER product

Feature:  AOM Metadata
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers


Scenario Outline: 01. Create an Order with MetaDataItem on the Product
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then I Logout of Aom
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3 | orderbroker | orderprincipal | Stkclrmine | status |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO OTHER  | 5650.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated | *           | AT3            | ORANGE     | NEW    |





Scenario Outline: 02. Verify that User cannot create an order with empty Metadata fields 
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
#And I verify that metadata fields displays an error message
#| message                                    |
#| String should have length between 1 and 10 |
And I verify create button is not enabled
Then I close the order screen
Then I Logout of Aom
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3 |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO OTHER  | 5651.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 |           |



Scenario Outline: 03. User enters invalid data into Metadata field
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
And I verify that metadata fields displays an error message
| message                                    |
| String should have length between 1 and 10 |
And I verify create button is not enabled
Then I close the order screen
Then I Logout of Aom
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3    |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO OTHER  | 5652.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated 34 |




Scenario Outline: 04. Trader will not see metadata fields on Non-metadata Product
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And metadata fields should not be visible
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname   |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO RED FAME |


# Need to sort out update Metadata3 value issue
Scenario Outline: 05. Update an Order with MetaDataItem on the Product 
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then I open the existing order with <price> and <ordertype>
Then I update the order details with <newprice>, <newmetadata1>, <newmetadata2>, <newmetadata3>
And I click on order Update Button
Then the order with the price <newprice> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the metadata details <newprice>, <newmetadata1>, <newmetadata2>, <metadata3>, <newstatus>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <newmetadata1>, <newmetadata2>, <metadata3>
Then I Logout of Aom
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3 | orderbroker | orderprincipal | Stkclrmine | status | newprice | newmetadata1    | newmetadata2     | newmetadata3      | newstatus |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO OTHER  | 5653.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated | *           | AT3            | ORANGE     | NEW    | 6123.00  | Tanjung Langsat | Pen grade 50/110 | Updated Metdata 3 | UPDATED   |


Scenario Outline: 06. Hold an Order with MetaDataItem on the Product 
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then I open the existing order with <price> and <ordertype>
And I click on order hold Button
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <newstatus>, <ordertype> should appear in market ticker 
Then I Logout of Aom
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3 | orderbroker | orderprincipal | Stkclrmine | status | newstatus |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO OTHER  | 5654.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated | *           | AT3            | ORANGE     | NEW    | WITHDRAWN |



Scenario Outline: 07. Reinstate an Order with MetaDataItem on the Product 
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then I open the existing order with <price> and <ordertype>
And I click on order hold Button
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <newstatus>, <ordertype> should appear in market ticker 
Then I open the existing order with <price> and <ordertype>
And I click on order reinstate Button
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then I Logout of Aom
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3 | orderbroker | orderprincipal | Stkclrmine | status | newstatus |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO OTHER  | 5655.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated | *           | AT3            | ORANGE     | NEW    | WITHDRAWN |




Scenario Outline: 08. Kill an Order with MetaDataItem on the Product 
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then I open the existing order with <price> and <ordertype>
And I click on order kill Button
Then the order no longer exists on the bid/ask stack for the <productname>
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <newstatus>, <ordertype> should appear in market ticker 
Then I Logout of Aom
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3 | orderbroker | orderprincipal | Stkclrmine | status | newstatus |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO OTHER  | 5656.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated | *           | AT3            | ORANGE     | NEW    | WITHDRAWN |



Scenario Outline: 09. Trader creates an Order with MetaDataItem on the Product and Another Trader Execute the Order
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then I Logout of Aom
And I enter username <user2> and password <user2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack
And the color of the order should be <Stkclrother>	
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then the observer executes the order
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker              | notes                 | metadata1 | metadata2        | metadata3 | orderbroker | orderprincipal | Stkclrmine | status | user2 | user2pwd  | loggedinasuser2 | Stkclrother |
| ALT34    | Password% | Auto Test 3 | BID       | AUTO OTHER  | 5657.00 | 1000     | Auto Broker 3 (AB3) | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated | AB3         | AT3            | ORANGE     | NEW    | ALT44 | Password% | Auto Test 4     | WHITE       |


Scenario Outline: 10. Trader creates an Order with MetaDataItem on the Product and Broker Execute the Order
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then I Logout of Aom
And I enter username <user2> and password <user2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack
And the color of the order should be <Stkclrother>	
Then the broker executes the order with <buysellprincipal> as principal
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3 | orderbroker | orderprincipal | Stkclrmine | status | user2 | user2pwd  | loggedinasuser2 | Stkclrother | buysellprincipal  |
| ALT34    | Password% | Auto Test 3 | ASK       | AUTO OTHER  | 5658.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated | *           | AT3            | ORANGE     | NEW    | AB34  | Password% | Auto Broker 3   | WHITE       | Auto Test 4 (AT4) |


Scenario Outline: 11. Trader creates an Order with MetaDataItem on the Product and Broker is unable to edit it
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <inituser> and password <initpwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinas>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes> 
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Then the hoverbox shows metadata as <metadata1>, <metadata2>, <metadata3>
Then I Logout of Aom
And I enter username <user2> and password <user2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack
And the color of the order should be <Stkclrother>	
Then I open the existing order with <price> and <ordertype>
Then the metadata details appears read only
And the metadata details <price>, <metadata1>, <metadata2>, <metadata3>, <status>, <ordertype> should appear in market ticker 
Examples:
| inituser | initpwd   | loggedinas  | ordertype | productname | price   | quantity | broker | notes                 | metadata1 | metadata2        | metadata3 | orderbroker | orderprincipal | Stkclrmine | status | user2 | user2pwd  | loggedinasuser2 | Stkclrother | buysellprincipal  |
| ALT34    | Password% | Auto Test 3 | ASK       | AUTO OTHER  | 5659.00 | 1000     | *      | Automation Test Order | Kemaman   | Pen grade 80/100 | Automated | *           | AT3            | ORANGE     | NEW    | AB34  | Password% | Auto Broker 3   | WHITE       | Auto Test 4 (AT4) |





# Plus Deal Metadata
Scenario Outline: 12. Trader creates a new External Deal with a MetaData Item on the product
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <user1> and password <user1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
When I click on create Deal
Then the plus deal metadata details with <ordertype>, <dealstatus>, <price>, <metadata1>, <metadata2>, <metadata3> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract   | price  | quantity | location | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | metadata1 | metadata2        | metadata3 |
| ALT54 | Password% | Auto Test 5     | AUTO OTHER | 926.00 | 297      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | PENDING    | + DEAL    | Kemaman   | Pen grade 80/100 | Automated |


# Free Form Text 
Scenario Outline: 13. Editor creates a Pending External Deal with a MetaData Item on the product
Given I navigate to url
| loginbox             |
|  Argus Open Markets  |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
And I populate the metadta fields with <metadata1>, <metadata2>, <metadata3>
Then the Free Form Deal displays <price>, <quantity>, <buyer>, <seller>, <broker>, <metadata1>, <metadata2>, <metadata3>
When I click on create Deal as Pending
Then the plus deal metadata details with <ordertype>, <dealstatus>, <price>, <metadata1>, <metadata2>, <metadata3> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract   | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | metadata1 | metadata2        | metadata3 |
| ED14    | Password   | Argus Editor 1    | AUTO OTHER | 712.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | YELLOW     | PENDING    | + DEAL    | Kemaman   | Pen grade 80/100 | Automated |
