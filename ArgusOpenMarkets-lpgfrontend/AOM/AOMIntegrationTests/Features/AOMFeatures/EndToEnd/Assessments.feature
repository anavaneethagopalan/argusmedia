﻿@AOM
Feature: AOM Assessments
	To Test functionality of Assessments

@Editor
Scenario Outline:01.AOM Clear Todays Assessment
Given I am logged into "AOM" as user "<username>"
When I clear "<period>" Assessment for "<product>"
Then the prices <lowprice>, <highprice>, <status> are cleared from the Todays Assessments window
Examples:
| username |product      | lowprice | highprice | status | period |
| ED24     |AUTO RED RME | -        | -         | N/A    | Today  |

@Editor
Scenario Outline:02.AOM Clear Previous Assessment
Given I am logged into "AOM" as user "<username>"
When I clear "<period>" Assessment for "<product>"
Then the prices <lowprice>, <highprice>, <status> are cleared from the Previous Assessments window
Examples:
| username | password | product      | lowprice | highprice | status | period   |
| ED24     | Password | AUTO RED RME | -        | -         | N/A    | Previous |

@Editor
Scenario Outline: 03.AOM Input prices for todays assessment
Given I am logged into "AOM" as user "<username>"
When I enter assessment for "<period>" and "<product>" with "<lowprice>" "<highprice>" "<status>"  
Then the prices <lowprice>, <highprice>, <status> are displayed correctly for Todays assessments
#And the details <lowprice>, <highprice> are updated in database
Examples:
| username | password | loggedinas     | product      | lowprice | highprice | status      | period |
| ED24     | Password | Argus Editor 2 | AUTO RED RME | 356.00   | 435.00    | RUNNING VWA | Today  |
| ED24     | Password | Argus Editor 2 | AUTO RED RME | 512.00   | 555.00    | FINAL       | Today  |
| ED24     | Password | Argus Editor 2 | AUTO RED RME | 311.00   | 498.00    | CORRECTED   | Today  |

@Editor
Scenario Outline:04.AOM Input prices for previous assessment
Given I am logged into "AOM" as user "<username>"
When I enter assessment for "<period>" and "<product>" with "<lowprice>" "<highprice>" "<status>"
Then the prices <lowprice>, <highprice>, <status> are displayed correctly for Previous assessments
#And the details <lowprice>, <highprice> are updated in database
And I Logout of Aom
Examples:
| username | password | loggedinas     | product      | lowprice | highprice | status      | period   |
| ED24     | Password | Argus Editor 2 | AUTO RED RME | 412.00   | 467.00    | RUNNING VWA | Previous |
| ED24     | Password | Argus Editor 2 | AUTO RED RME | 634.00   | 652.00    | FINAL       | Previous |
| ED24     | Password | Argus Editor 2 | AUTO RED RME | 765.00   | 821.00    | CORRECTED   | Previous |