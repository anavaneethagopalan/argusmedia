﻿@AOM

# ALT24 allows buy/sell to ALT14 and vice versa for all products
# ALT24 allows buy/sell to ALT34 and vice versa for AUTO RED FAME only??
# ALT24 allows buy to ALT44 and ALT44 allows sell for AUTO RED FAME only
# ALT24 allows buy/sell to AB14 and vice versa for all products
# ALT24 allows buy/sell to AB24 and vice versa for AUTO RED FAME product only??
# ALT14 denies buy/sell to AB14 and vice versa for AUTO RED FAME product only
# ALT14 allows buy/sell to AB24 and vice versa for AUTO RED FAME product only
# ALT24 denies buy/sell to AB24, ALT34 and vice versa for all products


Feature: AOM DealsThreeWay
	To check 3-Way Deals are working as expected between Traders and Broker

# 3- Way Deals with No Restrictions
Scenario Outline: 01. T1 creates order naming B1 as seller broker and B1 aggresses
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Given I am logged into "AOM" as user "<observeruser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 	
	Then the hoverbox shows tradeable as <tradeableobserver>
	And the order can only be executable by <observeruser>
	Then I close the order screen
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableobserver>
	Then the broker executes the order naming <buysellprincipal> as principal
	And the order no longer exists on the bid/ask stack for the <productname>
	And the order with <dealstatus>,<status>,<price> should appear in market ticker 	
	And the color of the market ticker item should be <Stkclrmine>	
Examples:
| inituser | initpwd   | ordertype | productname  | price   | quantity | broker              | notes                 | Stkclrmine | Stkclrother | observeruser | observerpwd | principal | orderbroker | status | loggedinas  | loggedinobserver | loggedinbroker | tradeableuser | tradeableobserver | dealstatus | brokeruser | brokerpwd | buysellprincipal  |
| ALT24    | Password% | BID       | AUTO RED RME | 7650.00 | 1000     | Auto Broker 1 (AB1) | Automation Test Order | Orange     | White       | ALT14        | Password%   | AT2       | AB1         | NEW    | Auto Test 2 | Auto Test 1      | Auto Broker 1  | No            | Yes               | DEAL       | AB14       | Password% | Auto Test 1 (AT1) |

Scenario Outline: 02. B1 creates order naming T1 as seller principal and T2 aggresses
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details for broker with <ordertype> ,<price> ,<quantity> ,<principal> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableobserver>
	Given I am logged into "AOM" as user "<observeruser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 	
	Then the hoverbox shows tradeable as <tradeableuser>
	And the order cannot be executed by <observeruser>
	Then I close the order screen
	Then I Logout of Aom
	Given I am logged into "AOM" as user "<traderuser>"
	Then I should be on the dashboard page <loggedintrader>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableobserver>
	Then the trader executes the order 
	And the order no longer exists on the bid/ask stack for the <productname>
	And the order with <dealstatus>,<status>,<price> should appear in market ticker 	
	And the color of the market ticker item should be <Stkclrmine>	
Examples:
| inituser | initpwd   | ordertype | productname  | price   | quantity | principal         | notes                 | Stkclrmine | Stkclrother | observeruser | observerpwd | orderprincipal | orderbroker | status | loggedinas    | loggedinobserver | loggedintrader | tradeableuser | tradeableobserver | dealstatus | traderuser | traderpwd | buysellprincipal  |
| AB14     | Password% | ASK       | AUTO RED RME | 7651.00 | 1000     | Auto Test 2 (AT2) | Automation Test Order | Orange     | White       | ALT24        | Password%   | AT2            | AB1         | NEW    | Auto Broker 1 | Auto Test 2      | Auto Test 1    | No            | Yes               | DEAL       | ALT14       | Password% | Auto Test 1 (AT1) |



Scenario Outline: 03. T1 creates order - B1 Aggressess naming T2 as Buying Principal
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Given I am logged into "AOM" as user "<observeruser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 	
	Then the hoverbox shows tradeable as <tradeableobserver>
	And the order can only be executable by <observeruser>
	Then I close the order screen
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableobserver>
	Then the broker executes the order with <buysellprincipal> as principal
	And the order no longer exists on the bid/ask stack for the <productname>
	And the order with <dealstatus>,<status>,<price> should appear in market ticker 	
	And the color of the market ticker item should be <Stkclrmine>	
Examples:
| inituser | initpwd   | ordertype | productname  | price   | quantity | broker | notes                 | Stkclrmine | Stkclrother | observeruser | observerpwd | principal | orderbroker | status | loggedinas  | loggedinobserver | loggedinbroker | tradeableuser | tradeableobserver | dealstatus | brokeruser | brokerpwd | buysellprincipal  |
| ALT24    | Password% | BID       | AUTO RED RME | 7652.00 | 1000     | *      | Automation Test Order | Orange     | White       | ALT14        | Password%   | AT2       | *           | NEW    | Auto Test 2 | Auto Test 1      | Auto Broker 1  | No            | Yes               | DEAL       | AB14       | Password% | Auto Test 1 (AT1) |



Scenario Outline: 04. T1 creates order - T2 Aggresses naming B1 as Buyer Broker
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 	
	Then the hoverbox shows tradeable as <tradeableobserver>
	And the order can be executed by "<brokeruser>" by selecting "<loggedinobserver>" as principal
	Then I close the order screen
	Given I am logged into "AOM" as user "<traderuser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableobserver>
	Then the trader executes the order with <buysellbroker> as broker
	And the order no longer exists on the bid/ask stack for the <productname>
	And the order with <dealstatus>,<status>,<price> should appear in market ticker 	
	And the color of the market ticker item should be <Stkclrmine>	
Examples:
| inituser | initpwd   | ordertype | productname  | price   | quantity | broker | notes                 | Stkclrmine | Stkclrother | brokeruser | brokerpwd | principal | orderbroker | status | loggedinas  | loggedinobserver | loggedinbroker | tradeableuser | tradeableobserver | dealstatus | traderuser | traderpwd | buysellbroker       |
| ALT24    | Password% | BID       | AUTO RED RME | 7653.00 | 1000     | *      | Automation Test Order | Orange     | White       | AB14         | Password%   | AT2       | *           | NEW    | Auto Test 2 | Auto Test 1      | Auto Broker 1  | No            | Yes               | DEAL       | ALT14      | Password% | Auto Broker 1 (AB1) |

##### 3 Way Deals with B1 and T2 Restrictions

Scenario Outline: 05. T1 creates order naming B1 as Seller Broker and B1 denies T2
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableobserver>	
	Given I am logged into "AOM" as user "<traderuser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrnonuser>
	And the color of the market ticker item should be <Stkclrnonuser>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	And the order should not be editable or executable by <traderuser>	
	Then I close the order screen
Examples:
| inituser | initpwd   | ordertype | productname       | price   | quantity | broker              | notes                 | Stkclrmine | Stkclrother | traderuser | traderpwd | principal | orderbroker | status | loggedinas  | loggedinobserver | loggedinbroker | tradeableuser | tradeableobserver | brokeruser | brokerpwd | Stkclrnonuser |
| ALT24    | Password% | ASK       | AUTO RED FAME | 7654.00 | 1000     | Auto Broker 1 (AB1) | Automation Test Order | Orange     | White       | ALT14        | Password%   | AT2       | AB1         | NEW    | Auto Test 2 | Auto Test 1      | Auto Broker 1  | No            | Yes               | AB14       | Password% | DoveGrey     |



Scenario Outline: 06. B1 creates order naming T1 as Seller Principal and B1 denies T2
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details for broker with <ordertype> ,<price> ,<quantity> ,<principal> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableobserver>
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 	
	Then the hoverbox shows tradeable as <tradeableuser>
	And the order cannot be executed by <observeruser>
	Then I close the order screen
	Given I am logged into "AOM" as user "<traderuser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrnonuser>
	And the color of the market ticker item should be <Stkclrnonuser>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	And the order should not be editable or executable by <traderuser>	
	Then I close the order screen
Examples:
| inituser | initpwd   | ordertype | productname       | price   | quantity | broker              | notes                 | Stkclrmine | Stkclrother | traderuser | traderpwd | orderprincipal | orderbroker | status | loggedinas  | loggedinobserver | loggedinbroker | tradeableuser | tradeableobserver | brokeruser | brokerpwd | Stkclrnonuser | principal  |
| ALT24    | Password% | ASK       | AUTO RED FAME  | 7655.00 | 1000     | Auto Broker 1 (AB1) | Automation Test Order | Orange     | White       | ALT14        | Password%   | AT2       | AB1         | NEW    | Auto Test 2 | Auto Test 1      | Auto Broker 1  | No            | Yes               | AB14       | Password% | DoveGrey     |Auto Test 2 (AT2) |


Scenario Outline: 07. T1 creates order Any Broker and B1 Aggresses naming T2 as Buying Principal (B1 denies T2)
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Given I am logged into "AOM" as user "<traderuser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	Then the hoverbox shows tradeable as <tradeableobserver>
	And the order can be executed by "<traderuser>"
	Then I close the order screen
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	Then the hoverbox shows tradeable as <tradeableobserver>
	And the <brokeruser> should not be able to select principal <buysellprinciple>
	Then I close the order screen
Examples:
| inituser | initpwd   | ordertype | productname  | price   | quantity | broker | notes                 | Stkclrmine | Stkclrother | brokeruser | brokerpwd | principal | orderbroker | status | loggedinas  | loggedinobserver | loggedinbroker | tradeableuser | tradeableobserver | dealstatus | traderuser | traderpwd | buysellprinciple       |
| ALT24    | Password% | BID       | AUTO RED FAME | 7656.00 | 1000     | *      | Automation Test Order | Orange     | White       | AB14         | Password%   | AT2       | *           | NEW    | Auto Test 2 | Auto Test 1      | Auto Broker 1  | No            | Yes               | DEAL       | ALT14      | Password% | Auto Test 1 (AT1) |



Scenario Outline: 08. T1 creats order as Any Broker and T2 Aggresses naming B1 as Broker - B1 denies T2
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	Then the hoverbox shows tradeable as <tradeableobserver>
	Given I am logged into "AOM" as user "<traderuser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	Then the hoverbox shows tradeable as <tradeableobserver>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then the <traderuser> should not be able to select broker <buysellbroker>	
	Then I close the order screen
Examples:
| inituser | initpwd   | ordertype | productname   | price   | quantity | broker | notes                 | Stkclrmine | Stkclrother | brokeruser | brokerpwd | principal | orderbroker | status | loggedinas  | loggedinobserver | loggedinbroker | tradeableuser | tradeableobserver | dealstatus | traderuser | traderpwd | buysellbroker       |
| ALT24    | Password% | BID       | AUTO RED FAME | 7657.00 | 1000     | *      | Automation Test Order | Orange     | White       | AB14       | Password% | AT2       | *           | NEW    | Auto Test 2 | Auto Test 1      | Auto Broker 1  | No            | Yes               | DEAL       | ALT14      | Password% | Auto Broker 1 (AB1) |


##### 3 Way Deals with B1 and T1 Restrictions

Scenario Outline: 09. T1 creates order naming B1 as Seller Broker - T1 denies B1
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	And the <inituser> should not be able to select broker <buysellbroker>			
	Then I close the order screen
Examples:
| inituser | initpwd   | ordertype | productname   | buysellbroker       | loggedinas  |
| ALT24    | Password% | BID       | AUTO RED FAME | Auto Broker 2 (AB2) | Auto Test 2 |


Scenario Outline: 10. B1 names T1 as Seller Principal  (T1 denies B1)
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |	
	And the <brokeruser> should not be able to choose <buysellprinciple>	
	Then I close the order screen
Examples:
| brokeruser | brokerpwd | ordertype | productname   | buysellprinciple  | loggedinbroker |
| AB24       | Password% | BID       | AUTO RED FAME | Auto Test 2 (AT2) | Auto Broker 2  |


Scenario Outline: 11. T1 creates order - B1 Aggresses naming T2 as Buying Principal - T1 denies B1
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Given I am logged into "AOM" as user "<traderuser>"
	Then I should be on the dashboard page <loggedintrader>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	Then the hoverbox shows tradeable as <tradeableobserver>
	And the order can be executed by "<traderuser>"
	Then I close the order screen
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrobserver>
	And the color of the market ticker item should be <Stkclrobserver>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	Then the hoverbox shows tradeable as <tradeableuser>
	And the order should not be editable or executable by <brokeruser>	
	Then I close the order screen
Examples:
| inituser | initpwd   | ordertype | productname   | loggedinas  | price   | quantity | broker | notes                 | principal | Stkclrmine | tradeableuser | traderuser | traderpwd | loggedintrader | orderbroker | Stkclrother | tradeableobserver | brokeruser | brokerpwd | loggedinbroker | Stkclrobserver | status |
| ALT24    | Password% | BID       | AUTO RED FAME | Auto Test 2 | 7658.00 | 1000     | *      | Automation Test Order | AT2       | Orange     | No            | ALT14      | Password% | Auto Test 1    | *           | White       | Yes               | AB24       | Password% | Auto Broker 2  | DoveGrey       | NEW    |

Scenario Outline: 12. T1 creates order - T2 Aggresses naming B1 as Buying Broker - T1 denies B1 
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrobserver>
	And the color of the market ticker item should be <Stkclrobserver>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	Then the hoverbox shows tradeable as <tradeableuser>
	And the order should not be editable or executable by <brokeruser>	
	Then I close the order screen
	Given I am logged into "AOM" as user "<traderuser>"
	Then I should be on the dashboard page <loggedintrader>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrother>
	And the color of the market ticker item should be <Stkclrother>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	Then the hoverbox shows tradeable as <tradeableobserver>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then the <traderuser> should not be able to select broker <buysellbroker>	
	Then I close the order screen
Examples:
| inituser | initpwd   | ordertype | productname   | loggedinas  | price   | quantity | broker | notes                 | principal | Stkclrmine | tradeableuser | traderuser | traderpwd | loggedintrader | orderbroker | Stkclrother | tradeableobserver | brokeruser | brokerpwd | loggedinbroker | Stkclrobserver | status |
| ALT24    | Password% | BID       | AUTO RED FAME | Auto Test 2 | 7659.00 | 1000     | *      | Automation Test Order | AT2       | Orange     | No            | ALT14      | Password% | Auto Test 1    | *           | White       | Yes               | AB24       | Password% | Auto Broker 2  | DoveGrey       | NEW    |

#AOMK-472
Scenario Outline: 13. B1 creates order naming T1 as Seller Principal and T1 updates the order
	Given I am logged into "AOM" as user "<brokeruser>"
	Then I should be on the dashboard page <loggedinbroker>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details for broker with <ordertype> ,<price> ,<quantity> ,<principal> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableobserver>
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 	
	Then the hoverbox shows tradeable as <tradeableuser>
	Then I change the broker to <newbroker>
	And I update the order
	Then the order with the price <price> quantity <quantity> broker <newbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 	
	Then the hoverbox shows tradeable as <tradeableuser>
Examples:
| inituser | initpwd   | ordertype | productname   | price   | quantity | broker              | notes                 | Stkclrmine | Stkclrother | traderuser | traderpwd | orderprincipal | orderbroker | status | loggedinas  | loggedinobserver | loggedinbroker | tradeableuser | tradeableobserver | brokeruser | brokerpwd | Stkclrnonuser | principal         | newbroker |
| ALT24    | Password% | ASK       | AUTO RED FAME | 7655.00 | 1000     | Auto Broker 1 (AB1) | Automation Test Order | Orange     | White       | ALT14      | Password% | AT2            | AB1         | NEW    | Auto Test 2 | Auto Test 1      | Auto Broker 1  | No            | Yes               | AB14       | Password% | DoveGrey      | Auto Test 2 (AT2) | *         |

