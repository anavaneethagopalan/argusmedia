﻿@AOM

Feature: AOM DealsTwoWay
	To check 2-Way Deals are working as expected

Scenario Outline: 01. T1 can buysell to T2 - No Restrictions
	#Given I navigate to url
	#| loginbox             |
	#|  Argus Open Markets  |
	#And I enter username <inituser> and password <initpwd>
	#When I click on SignIn on Login Page	
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	Then I Logout of Aom
	#And I enter username <observeruser> and password <observerpwd> again	
	#When I click on SignIn on Login Page	
	Given I am logged into "AOM" as user "<observeruser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack
	And the color of the order should be <Stkclrother>	
	Then the hoverbox shows tradeable as <tradeableobserver>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	And the color of the market ticker item should be <Stkclrother>	
	Then the observer executes the order 
	And the order no longer exists on the bid/ask stack for the <productname>
	And the order with <dealstatus>,<status>,<price> should appear in market ticker 	
	And the color of the market ticker item should be <Stkclrmine>	
	#Then I Logout of Aom
Examples:
	| inituser | initpwd   | ordertype | productname   | price   | quantity | broker                     | notes                 | Stkclrmine | Stkclrother | observeruser | observerpwd | principal | orderbroker | status | loggedinas  | loggedinobserver | tradeableuser | tradeableobserver | dealstatus |
	| ALT24    | Password% | BID       | AUTO RED RME  | 7120.00 | 1000     | *                          | Automation Test Order | Orange     | White       | ALT14        | Password%   | AT2       | *           | NEW    | Auto Test 2 | Auto Test 1      | No            | Yes               | DEAL       |
	| ALT24    | Password% | ASK       | AUTO RED FAME | 7121.00 | 1000     | Bilateral Only (No Broker) | Automation Test Order | Orange                | White       | ALT14        | Password%   | AT2       |             | NEW    | Auto Test 2 | Auto Test 1      | No            | Yes               | DEAL       |




	Scenario Outline: 02. T1 cannot buysell to T2 - Full Restrictions
	#Given I navigate to url
	#| loginbox             |
	#|  Argus Open Markets  |
	#And I enter username <inituser> and password <initpwd>
	#When I click on SignIn on Login Page	
	Given I am logged into "AOM" as user "<inituser>"
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order of type <ordertype> for the Product <productname>
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	Then the hoverbox shows tradeable as <tradeableuser>
	#Then I Logout of Aom
	#And I enter username <observeruser> and password <observerpwd> again	
	#When I click on SignIn on Login Page	
	Given I am logged into "AOM" as user "<observeruser>"
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack
	And the color of the order should be <Stkclrother>	
	Then the hoverbox shows tradeable as <tradeableobserver>
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	And the color of the market ticker item should be <Stkclrother>	
	And the order should not be editable or executable by <observeruser>
	Then I close the order screen
	#Then I Logout of Aom
Examples:
	| inituser | initpwd   | ordertype | productname  | price   | quantity | broker | notes                 | Stkclrmine | Stkclrother | observeruser | observerpwd | principal | orderbroker | status | loggedinas  | loggedinobserver | tradeableuser | tradeableobserver | dealstatus |
	| ALT24    | Password% | BID       | AUTO RED RME | 7122.00 | 1000     | *      | Automation Test Order | Orange     | DOVEGREY    | ALT34        | Password%   | AT2       | *           | NEW    | Auto Test 2 | Auto Test 3      | No            | No                | DEAL       |
	| ALT24    | Password% | ASK       | AUTO RED RME | 7123.00 | 1000     | Bilateral Only (No Broker) | Automation Test Order | Orange     | DOVEGREY    | ALT34        | Password%   | AT2       |             | NEW    | Auto Test 2 | Auto Test 3      | No            | No                | DEAL       |




#Scenario: T1 allows buy but denies sell to T2 (Partial Restrictions) -- Auto Biofuel -- ALT24, ALT14

