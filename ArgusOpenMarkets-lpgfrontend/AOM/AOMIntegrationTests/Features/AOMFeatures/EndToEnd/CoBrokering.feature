﻿@AOM

# Co-Brokering flag for Auto Biofuel is set to True
# ALT34 allows buy/sell to ALT44 and vice versa for Auto Biofuel
# ALT34 allows buy/sell to AB34 and AB44 and vice versa for Auto Biofuel
# ALT44 allows buy/sell to AB34 and AB44 and vice versa for Auto Biofuel
# ALT34 denies buy/sell to ALT54 and vice versa for Auto Biofuel
# ALT34 allows buy/sell to AB54 and vice versa for Auto Biofuel
# ALT44 denies buy/sell to AB54 and ALT54 and vice versa for Auto Biofuel


Feature: AOM Cobrokering
In order to avoid silly mistakes
As a math idiot
I want to be told the sum of two numbers


Scenario Outline:01. T1 creates order naming B1 as broker B2 aggresses naming T2 as principal
Given I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
And I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
And I am logged into "AOM" as user "<trader2>"
Then I should be on the dashboard page <loggedintrader2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver>
And the color of the market ticker item should be <Stkclrobserver>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver2>
And the color of the market ticker item should be <Stkclrobserver3>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
Then the broker executes the order with <buysellprincipal> as principal
And the order no longer exists on the bid/ask stack for the {productname}
And the order with <dealstatus>,<status>,<price> should appear in market ticker 
And the color of the market ticker item should be <Stkclrmine>
And cobrokering icon should be displayed in market ticker
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | broker              | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellprincipal  | cobrokerflag | dealstatus |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6690.00 | 1000     | Auto Broker 3 (AB3) | Cobrokering Test Order | AT3            | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DEEPPINK        | DOVEGREY        | NEW    | Auto Test 4 (AT4) | Yes          | DEAL       |


Scenario Outline:02. T1 creates order naming B1 as broker T2 aggresses naming B2 as broker
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <trader1> and password <trader1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <broker1> and password <broker1pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver2>
And the color of the market ticker item should be <Stkclrobserver3>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader2> and password <trader2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader2>"
Then I should be on the dashboard page <loggedintrader2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver>
And the color of the market ticker item should be <Stkclrobserver>
And the order with <ordertype>,<status>,<price> should appear in market ticker
And the hoverbox shows CoBrokering Enabled as <cobrokerflag>
Then the trader executes the order with <buysellbroker> as broker
And the order no longer exists on the bid/ask stack for the {productname}
And the order with <dealstatus>,<status>,<price> should appear in market ticker 
And the color of the market ticker item should be <Stkclrmine>
And cobrokering icon should be displayed in market ticker
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | broker              | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellbroker       | cobrokerflag | dealstatus |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6691.00 | 1000     | Auto Broker 3 (AB3) | Cobrokering Test Order | AT3             | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DEEPPINK        | DOVEGREY        | NEW    | Auto Broker 4 (AB4) | Yes          | DEAL       |


Scenario Outline:03. B1 creates order naming T1 as broker B2 aggresses naming T2 as principal
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <broker1> and password <broker1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details for broker with <ordertype> ,<price> ,<quantity> ,<principal> ,<notes>
And I enable co-brokering flag 
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader1> and password <trader1pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader2> and password <trader2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader2>"
Then I should be on the dashboard page <loggedintrader2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver>
And the color of the market ticker item should be <Stkclrobserver>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver2>
And the color of the market ticker item should be <Stkclrobserver3>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
Then the broker executes the order with <buysellprincipal> as principal
And the order no longer exists on the bid/ask stack for the {productname}
And the order with <dealstatus>,<status>,<price> should appear in market ticker 
And the color of the market ticker item should be <Stkclrmine>
And cobrokering icon should be displayed in market ticker
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | principal    | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellprincipal  | cobrokerflag | dealstatus |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6692.00 | 1000     | Auto Test 3 (AT3) | Cobrokering Test Order | AT3       | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DEEPPINK        | DOVEGREY        | NEW    | Auto Test 4 (AT4) | Yes          | DEAL       |


Scenario Outline:04. B1 creates order naming T1 as broker T2 aggresses naming B2 as broker
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <broker1> and password <broker1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details for broker with <ordertype> ,<price> ,<quantity> ,<principal> ,<notes>
And I enable co-brokering flag 
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader1> and password <trader1pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver2>
And the color of the market ticker item should be <Stkclrobserver3>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader2> and password <trader2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader2>"
Then I should be on the dashboard page <loggedintrader2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver>
And the color of the market ticker item should be <Stkclrobserver>
And the order with <ordertype>,<status>,<price> should appear in market ticker
And the hoverbox shows CoBrokering Enabled as <cobrokerflag>
Then the trader executes the order with <buysellbroker> as broker
And the order no longer exists on the bid/ask stack for the {productname}
And the order with <dealstatus>,<status>,<price> should appear in market ticker 
And the color of the market ticker item should be <Stkclrmine>
And cobrokering icon should be displayed in market ticker
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | principal    | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellbroker       | cobrokerflag | dealstatus |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6693.00 | 1000     | Auto Test 3 (AT3) | Cobrokering Test Order | AT3       | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DEEPPINK        | DOVEGREY        | NEW    | Auto Broker 4 (AB4) | Yes          | DEAL       |



Scenario Outline:05. T1 creates cobrokered order naming B1 and B1 disables cobrokering on the Order 
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <trader1> and password <trader1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
And I enable co-brokering flag 
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <broker1> and password <broker1pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
And I disable co-brokering flag as <broker1>
And I update the order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
And the cobrokering icon should not be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag2>
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver2>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag2>
And the <broker2> cannot execute the order 
Then I close the order screen
#Then I Logout of Aom
#And I enter username <trader2> and password <trader2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader2>"
Then I should be on the dashboard page <loggedintrader2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag2>
Then the trader executes the order
And the order no longer exists on the bid/ask stack for the <productname>
And the order with <dealstatus>,<status>,<price> should appear in market ticker 
And the color of the market ticker item should be <Stkclrmine>
And the cobrokering icon should not be displayed in market ticker
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | broker              | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellprincipal  | cobrokerflag | dealstatus | updatestatus | cobrokerflag2 |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6694.00 | 1000     | Auto Broker 3 (AB3) | Cobrokering Test Order | AT3            | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DOVEGREY        | DOVEGREY        | NEW    | Auto Test 4 (AT4) | Yes          | DEAL       | UPDATED      | NO            |


Scenario Outline:06. B1 creates cobrokered order naming T1 and T1 disables cobrokering on the Order 
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <broker1> and password <broker1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details for broker with <ordertype> ,<price> ,<quantity> ,<principal> ,<notes>
And I enable co-brokering flag 
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader1> and password <trader1pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
And I disable co-brokering flag as <broker1>
And I update the order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
And the cobrokering icon should not be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag2>
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver2>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag2>
And the <broker2> cannot execute the order 
Then I close the order screen
#Then I Logout of Aom
#And I enter username <trader2> and password <trader2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader2>"
Then I should be on the dashboard page <loggedintrader2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag2>
Then the trader executes the order
And the order no longer exists on the bid/ask stack for the <productname>
And the order with <dealstatus>,<status>,<price> should appear in market ticker 
And the color of the market ticker item should be <Stkclrmine>
And the cobrokering icon should not be displayed in market ticker
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | principal         | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellprincipal  | cobrokerflag | dealstatus | updatestatus | cobrokerflag2 |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6695.00 | 1000     | Auto Test 3 (AT3) | Cobrokering Test Order | AT3            | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DOVEGREY        | DOVEGREY        | NEW    | Auto Test 4 (AT4) | Yes          | DEAL       | UPDATED      | NO            |



Scenario Outline:07. T1 creates non-cobrokered order naming B1 as broker T2 aggresses naming B2 as broker
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <trader1> and password <trader1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
And I disable co-brokering flag
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And the cobrokering icon should not be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag2>
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver3>
And the color of the market ticker item should be <Stkclrobserver3>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag2>
#Then I Logout of Aom
#And I enter username <broker1> and password <broker1pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
And I enable co-brokering flag as <broker1>
And I update the order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver2>
And the color of the market ticker item should be <Stkclrobserver3>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader2> and password <trader2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader2>"
Then I should be on the dashboard page <loggedintrader2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver>
And the color of the market ticker item should be <Stkclrobserver>
And the order with <ordertype>,<updatestatus>,<price> should appear in market ticker
And the hoverbox shows CoBrokering Enabled as <cobrokerflag>
Then the trader executes the order with <buysellbroker> as broker
And the order no longer exists on the bid/ask stack for the {productname}
And the order with <dealstatus>,<status>,<price> should appear in market ticker 
And the color of the market ticker item should be <Stkclrmine>
And cobrokering icon should be displayed in market ticker
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | broker              | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellbroker       | cobrokerflag | dealstatus | cobrokerflag2 | updatestatus |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6696.00 | 1000     | Auto Broker 3 (AB3) | Cobrokering Test Order | AT3            | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DEEPPINK        | DOVEGREY        | NEW    | Auto Broker 4 (AB4) | Yes          | DEAL       | No            | UPDATED      |


Scenario Outline:08. B1 creates cobrokered order naming T1. T1 holds the order and B1 reinstates order
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <broker1> and password <broker1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details for broker with <ordertype> ,<price> ,<quantity> ,<principal> ,<notes>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader1> and password <trader1pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
Then I hold the order as <trader1>
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<holdstatus>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <broker1> and password <broker1pwd> again
#When I click on SignIn on Login Page	
And I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
Then I reinstate the order as <trader1>
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrobserver2>
And the color of the market ticker item should be <Stkclrobserver3>
And the order with <ordertype>,<status>,<price> should appear in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
Then the broker executes the order with <buysellprincipal> as principal
And the order no longer exists on the bid/ask stack for the {productname}
And the order with <dealstatus>,<status>,<price> should appear in market ticker 
And the color of the market ticker item should be <Stkclrmine>
And cobrokering icon should be displayed in market ticker
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | principal         | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellprincipal  | cobrokerflag | dealstatus | cobrokerflag2 | updatestatus | holdstatus |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6697.00 | 1000     | Auto Test 3 (AT3) | Cobrokering Test Order | AT3            | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DEEPPINK        | DOVEGREY        | NEW    | Auto Test 4 (AT4) | Yes          | DEAL       | No            | UPDATED      | WITHDRAWN  |


Scenario Outline:09. T1 creates cobrokerable order naming B1. B2 denies buysell to T2
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <trader1> and password <trader1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <broker2> and password <broker2pwd> again
#When I click on SignIn on Login Page	
And I am logged into "AOM" as user "<broker2>"
Then I should be on the dashboard page <loggedinbroker2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the <broker2> should not be able to select principal <buysellprinciple>
Then I close the order screen
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | broker              | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellprincipal  | cobrokerflag | dealstatus |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6698.00 | 1000     | Auto Broker 3 (AB3) | Cobrokering Test Order | AT3            | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB54    | Password%  | Auto Broker 5   | DEEPPINK        | DOVEGREY        | NEW    | Auto Test 4 (AT4) | Yes          | DEAL       |



Scenario Outline: 10. B1 creates cobrokerable order naming T1. T2 denies buysell to B2
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <broker1> and password <broker1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<broker1>"
Then I should be on the dashboard page <loggedinbroker1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
Then I fill in the order details for broker with <ordertype> ,<price> ,<quantity> ,<principal> ,<notes>
When I click on create Order
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
And the color of the order should be <Stkclrmine>
And the color of the market ticker item should be <Stkclrmine>
And the order with <ordertype>,<status>,<price> should appear in market ticker 
And cobrokering icon should be displayed in market ticker
Then the hoverbox shows CoBrokering Enabled as <cobrokerflag>
#Then I Logout of Aom
#And I enter username <trader2> and password <trader2pwd> again
#When I click on SignIn on Login Page	
And I am logged into "AOM" as user "<trader2>"
Then I should be on the dashboard page <loggedintrader2>
Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <orderprincipal> should be displayed in <productname> bidask stack	
When I click on the Order of type <ordertype> for the Product <productname>
Then the <traderuser> should not be able to select broker <buysellbroker>
Then I close the order screen
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 | price   | quantity | principal         | notes                  | orderprincipal | Stkclrmine | broker1 | broker1pwd | loggedinbroker1 | trader2 | trader2pwd | loggedintrader2 | orderbroker | Stkclrobserver | broker2 | broker2pwd | loggedinbroker2 | Stkclrobserver2 | Stkclrobserver3 | status | buysellbroker       | cobrokerflag | dealstatus |
| ALT34   | Password%  | BID       | AUTO BIOFUEL | Auto Test 3     | 6699.00 | 1000     | Auto Test 3 (AT3) | Cobrokering Test Order | AT3            | ORANGE     | AB34    | Password%  | Auto Broker 3   | ALT44   | Password%  | Auto Test 4     | AB3         | WHITE          | AB44    | Password%  | Auto Broker 4   | DEEPPINK        | DOVEGREY        | NEW    | Auto Broker 5 (AT5) | Yes          | DEAL       |



Scenario Outline:11. T1 should not see cobrokering checkbox when cobrokering is disabled for a Product
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets  |
#And I enter username <trader1> and password <trader1pwd>
#When I click on SignIn on Login Page	
Given I am logged into "AOM" as user "<trader1>"
Then I should be on the dashboard page <loggedintrader1>
When I click on the Order of type <ordertype> for the Product <productname>
Then I should be taken to the New Order Page for the product
| title                   |
| BID ASK - <productname> |
And cobrokering checkbox should not be available
Then I close the order screen
#Then I Logout of Aom
Examples:
| trader1 | trader1pwd | ordertype | productname  | loggedintrader1 |
| ALT34   | Password%  | BID       | AUTO RED RME | Auto Test 3     |
