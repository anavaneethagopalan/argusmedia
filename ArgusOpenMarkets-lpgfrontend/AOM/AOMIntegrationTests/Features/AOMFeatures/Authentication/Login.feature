﻿@AOM

Feature: AOM Login
	In order to ensure users can login
	As an Aom User
	I want to login to aom
Scenario Outline: 01. User logs into aom with correct login details
	Given I navigate to url
	| loginbox             |
	|  Argus Open Markets  |
	And I enter username <username> and password <password>
	When I click on SignIn on Login Page	
	Then I should be on the dashboard page <loggedinuser>
	Then I Logout of Aom
Examples:
	| username | password  | loggedinuser |
	| ALT24    | Password% | Auto Test 2  |


Scenario Outline: 02. User logs into aom with incorrect username and password
	Given I navigate to url
	| loginbox             |
	|  Argus Open Markets  |
	And I enter username <username> and password <password>
	When I click on SignIn on Login Page	
	Then user should see the error <errormsg>
Examples:
	| username | password | errormsg                                 |
	| ALT14    |          | Invalid login details, please try again. |
	| ALT24    | 123      | Invalid login details, please try again. |
	| AL111    | 456      | Invalid login details, please try again. |