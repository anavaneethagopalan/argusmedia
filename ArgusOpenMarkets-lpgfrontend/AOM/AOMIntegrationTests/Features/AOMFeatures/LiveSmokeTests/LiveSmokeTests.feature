﻿Feature: LiveSmokeTests
	To Run SmokeTest on live Market to help with Monitoring of site

@LiveSmoke
Scenario Outline: 01. User logs into aom with correct login details
	Given I navigate to url
	| loginbox             |
	|  Argus Open Markets  |
	And I enter username <username> and password <password>
	When I click on SignIn on Login Page	
	Then I should be on the dashboard page <loggedinuser>
	And the market ticker contains more than 1 item
	And I see the assessment window
	And the news items contains more than 1 item
	And Bid/Ask stack contains 2 products 
	And I Logout of Aom
Examples:
	| username   | password   | loggedinuser                           |
	| trader2atm | Argu5Med1a | Argus Test Market Trader2 Organisation |


@LiveSmoke
Scenario Outline: 02. T1 can buysell to T2 
	Given I navigate to url
	| loginbox             |
	|  Argus Open Markets  |
	And I enter username <inituser> and password <initpwd>
	When I click on SignIn on Login Page	
	Then I should be on the dashboard page <loggedinas>
	When I click on the Order type <ordertype> for the Product <productname> for live tests
	Then I should be taken to the New Order Page for the product
	| title                   |
	| BID ASK - <productname> |
	Then I fill in the order details with <ordertype>, <price> ,<quantity> ,<broker> ,<notes>
	And I fill in the test notes field with "Auotmated Test Notes"
	When I click on create Order
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack	
	And the color of the order should be <Stkclrmine>
	And the color of the market ticker item should be <Stkclrmine>
	And the order with <ordertype>,<status>,<price> should appear in market ticker
	##Order Created
	Then I Logout of Aom
	And I enter username <observeruser> and password <observerpwd> again	
	When I click on SignIn on Login Page	
	Then I should be on the dashboard page <loggedinobserver>
	Then the order with the price <price> quantity <quantity> broker <orderbroker> and principal <principal> should be displayed in <productname> bidask stack
	And the color of the order should be <Stkclrother>	
	And the order with <ordertype>,<status>,<price> should appear in market ticker 
	And the color of the market ticker item should be <Stkclrother>	
	Then the observer executes the order 
	And the order no longer exists on the bid/ask stack for the <productname>
	And the order with <dealstatus>,<status>,<price> should appear in market ticker 	
	#And the color of the market ticker item should be <Stkclrmine>	
	Then I Logout of Aom
Examples:
| inituser   | initpwd    | ordertype | productname       | price   | quantity | broker | notes                 | Stkclrmine | Stkclrother | principal | orderbroker | status | loggedinas                             | observeruser | observerpwd | loggedinobserver              |dealstatus |
| trader2atm | Argu5Med1a | BID       | ARGUS TEST MARKET | 7120.00 | 1000     | *      | Automation Test Order | Orange     | White       | XTT       | *           | NEW    | Argus Test Market Trader2 Organisation | trader3atm   | Argu5Med1a  | Argus Test Market Trader3 Org |DEAL       |

	