﻿Feature: DeadLock
	Trying to ensure no deadlocks occur with multiple concurrent orders

###This is more of an integration test than a Acceptance Test
Scenario: multiple orders
	Given I purge the open orders
	And I count the orders and market ticker items in SQL
	When I create 100 bids in parrallel for user "LT24", product ""Naphtha CIF NWE - Cargoes" 
	Then I expect the orders and market ticker tables count to increase by 100

