﻿@CME
Feature: CME MarketInfo
	To test MarketInfo functionality


# *********** USER CREATES MARKET INFO **************
Scenario Outline: 01. User creates a new Market Info and is not visible to other users except Editors
Given I navigate to url
| loginbox             |
|  Argus Open Markets - CME Login |
And I enter username <user1> and password <user1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrmine> 
Then I Logout of Aom
And I enter username <user2> and password <user2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser2>
Then the market info details <ordertype>, <text>, <status> should not be displayed in market ticker
Then I Logout of Aom
And I enter username <editor1> and password <editor1pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | text                            | Stkclrmine | status  | ordertype | user2 | user2pwd  | loggedinasuser2 | editor1 | editor1pwd | loggedinaseditor1 | Stkclreditor |
| AB54  | Password% | Auto Broker 5   | Automated Market Information 01 | ORANGE     | PENDING | INFO      | AB44  | Password% | Auto Broker 4   | ED14    | Password   | Argus Editor 1    | PUREBLUE     |


Scenario Outline: 02. User creates a new Market Info and Editor verifies it
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <user1> and password <user1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrmine> 
Then I Logout of Aom
And I enter username <editor1> and password <editor1pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I open up the existing info
Then I should be taken to Verify Market Info Page
| title                     |
| VERIFY MARKET INFORMATION |
Then I click on Market info verify button
Then the market info details <ordertype>, <text>, <verifiedstatus> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrother> 
Then I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | text                            | Stkclrmine | status  | ordertype | editor1 | editor1pwd | loggedinaseditor1 | Stkclreditor | Stkclrother | verifiedstatus |
| AB54  | Password% | Auto Broker 5   | Automated Market Information 02 | ORANGE     | PENDING | INFO      | ED14    | Password   | Argus Editor 1    | PUREBLUE     | Grey       | NEW            |



@ignore
#AOMK - 211 ---Errors out
Scenario Outline: 03. User creates a new Market Info and attempts to Update it
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <user1> and password <user1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrmine> 
Then I open up the existing info
Then I should be taken to Verify Market Info Page
| title                     |
| VERIFY MARKET INFORMATION |
Then I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | text                            | Stkclrmine | status  | ordertype |
| AB54  | Password% | Auto Broker 5   | Automated Market Information 03 | ORANGE     | PENDING | INFO      |

#Scenario Outline: 04. User creates a new Market Info and unable to Verify it

#Scenario Outline: 05. User creates a new Market Info and unable to Void it

# *********** EDITOR CREATES MARKET INFO **************


Scenario Outline: 06. Editor creates a Pending Market info and is not visible to other users except other editors
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info as Pending
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I Logout of Aom
And I enter username <user1> and password <user1pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
Then the market info details <ordertype>, <text>, <status> should not be displayed in market ticker
Then I Logout of Aom
And I enter username <editor2> and password <editor2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor2>
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | text                            | status  | ordertype | user1 | user1pwd  | loggedinasuser1 | editor2 | editor2pwd | loggedinaseditor2 | Stkclreditor |
| ED14    | Password   | Argus Editor 1    | Automated Market Information 06 | PENDING | INFO      | AB44  | Password% | Auto Broker 4   | ED24    | Password   | Argus Editor 2    | PUREBLUE     |


Scenario Outline: 07. Editor creates a Verified Market info and is visible to all users subscribed to the product
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info as Verfied
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I Logout of Aom
And I enter username <user1> and password <user1pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrother> 
Then I Logout of Aom
And I enter username <editor2> and password <editor2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor2>
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrother> 
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | text                            | status | ordertype | user1 | user1pwd  | loggedinasuser1 | editor2 | editor2pwd | loggedinaseditor2 | Stkclreditor | Stkclrother |
| ED14    | Password   | Argus Editor 1    | Automated Market Information 07 | NEW    | INFO      | AB44  | Password% | Auto Broker 4   | ED24    | Password   | Argus Editor 2    | ORANGE       | Grey        |


Scenario Outline: 08. Editor creates a Pending Market info and Updates
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info as Pending
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I open up the existing info
Then I should be taken to Verify Market Info Page
| title                     |
| VERIFY MARKET INFORMATION |
Then I update the market info details with <newText>
When I click on update Market Info
Then the market info details <ordertype>, <newText>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | text                            | status  | ordertype | Stkclreditor | newText                                 |
| ED14    | Password   | Argus Editor 1    | Automated Market Information 08 | PENDING | INFO      | PUREBLUE     | Updated Automated Market Information 08 |



Scenario Outline: 09. Editor creates a Pending Market info and Verifies
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info as Pending
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I open up the existing info
Then I should be taken to Verify Market Info Page
| title                     |
| VERIFY MARKET INFORMATION |
#Then I update the market info details with <newText>
When I click on verify Market Info
Then the market info details <ordertype>, <text>, <newStatus> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrother> 
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | text                            | status  | ordertype | Stkclreditor | newText                                 | newStatus | Stkclrother |
| ED14    | Password   | Argus Editor 1    | Automated Market Information 09 | PENDING | INFO      | PUREBLUE     | Updated Automated Market Information 09 | NEW       | ORANGE      |


Scenario Outline: 10. Editor creates a Pending Market info and Voids
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info as Pending
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I open up the existing info
Then I should be taken to Verify Market Info Page
| title                     |
| VERIFY MARKET INFORMATION |
When I click on void Market Info
Then the market info details <ordertype>, <text>, <newStatus> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrother1> 
Then I Logout of Aom
And I enter username <editor2> and password <editor2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor2>
Then the market info details <ordertype>, <text>, <newStatus> should be displayed in market ticker
And the color of the market ticker item should be <Stkclrother2> 
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | text                            | status  | ordertype | Stkclreditor | newStatus | Stkclrother1 | editor2 | editor2pwd | loggedinaseditor2 | Stkclrother2 |
| ED14    | Password   | Argus Editor 1    | Automated Market Information 10 | PENDING | INFO      | PUREBLUE     | VOIDED    | ORANGE       | ED24    | Password   | Argus Editor 2    | Grey         |


Scenario Outline: 11. Editor creates a Verified Market info and Updates
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info as Verfied
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I open up the existing info
Then I should be taken to Verify Market Info Page
| title                     |
| VERIFY MARKET INFORMATION |
Then I update the market info details with <newText>
When I click on update Market Info
Then the market info details <ordertype>, <newText>, <updatedstatus> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | text                            | status | ordertype | Stkclreditor | newText                                 | updatedstatus |
| ED14    | Password   | Argus Editor 1    | Automated Market Information 11 | NEW    | INFO      | ORANGE       | Updated Automated Market Information 11 | UPDATED       |




Scenario Outline: 12. Editor creates a Verified Market info and Voides
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Market Info
Then I should be taken to the Market Info page
| title                     |
| REPORT MARKET INFORMATION |
Then i fill in the market info details with <text>
When I click on create Market Info as Verfied
Then the market info details <ordertype>, <text>, <status> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I open up the existing info
Then I should be taken to Verify Market Info Page
| title                     |
| VERIFY MARKET INFORMATION |
When I click on void Market Info
Then the market info details <ordertype>, <text>, <updatedstatus> should be displayed in market ticker
And the color of the market ticker item should be <Stkclreditor> 
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | text                            | status | ordertype | Stkclreditor | updatedstatus |
| ED14    | Password   | Argus Editor 1    | Automated Market Information 11 | NEW    | INFO      | ORANGE       | VOIDED        |
