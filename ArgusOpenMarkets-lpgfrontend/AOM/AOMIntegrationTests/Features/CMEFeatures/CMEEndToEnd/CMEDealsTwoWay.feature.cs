﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.2.0.0
//      SpecFlow Generator Version:2.2.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace AOMIntegrationTests.Features.CMEFeatures.CMEEndToEnd
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.2.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute()]
    public partial class CMEDealsTwoWayFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "CMEDealsTwoWay.feature"
#line hidden
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitializeAttribute()]
        public static void FeatureSetup(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext testContext)
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner(null, 0);
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "CME DealsTwoWay", "\tTo check 2-Way Deals are working as expected", ProgrammingLanguage.CSharp, new string[] {
                        "CME"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassCleanupAttribute()]
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestInitializeAttribute()]
        public virtual void TestInitialize()
        {
            if (((testRunner.FeatureContext != null) 
                        && (testRunner.FeatureContext.FeatureInfo.Title != "CME DealsTwoWay")))
            {
                global::AOMIntegrationTests.Features.CMEFeatures.CMEEndToEnd.CMEDealsTwoWayFeature.FeatureSetup(null);
            }
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCleanupAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void _01_T1CanBuysellToT2_NoRestriction(
                    string inituser, 
                    string initpwd, 
                    string ordertype, 
                    string productname, 
                    string price, 
                    string quantity, 
                    string broker, 
                    string notes, 
                    string stkclrmine, 
                    string stkclrother, 
                    string mkttickclr, 
                    string observeruser, 
                    string observerpwd, 
                    string principal, 
                    string orderbroker, 
                    string status, 
                    string loggedinas, 
                    string loggedinobserver, 
                    string tradeableuser, 
                    string tradeableobserver, 
                    string dealstatus, 
                    string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01. T1 can buysell to T2 - No Restriction", exampleTags);
#line 6
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "loginbox"});
            table1.AddRow(new string[] {
                        "Argus Open Markets - CME Login"});
#line 7
 testRunner.Given("I navigate to url", ((string)(null)), table1, "Given ");
#line 10
 testRunner.And(string.Format("I enter username {0} and password {1}", inituser, initpwd), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 11
 testRunner.When("I click on SignIn on Login Page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 12
 testRunner.Then(string.Format("I should be on the dashboard page {0}", loggedinas), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 13
 testRunner.When(string.Format("I click on the Order of type {0} for the Product {1}", ordertype, productname), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "title"});
            table2.AddRow(new string[] {
                        string.Format("BID ASK - {0}", productname)});
#line 14
 testRunner.Then("I should be taken to the New Order Page for the product", ((string)(null)), table2, "Then ");
#line 17
 testRunner.Then(string.Format("I fill in the order details with {0}, {1} ,{2} ,{3} ,{4}", ordertype, price, quantity, broker, notes), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 18
 testRunner.When("I click on create Order", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 19
 testRunner.Then(string.Format("the order with the price {0} quantity {1} broker {2} and principal {3} should be " +
                        "displayed in {4} bidask stack", price, quantity, orderbroker, principal, productname), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 20
 testRunner.And(string.Format("the color of the order should be {0}", stkclrmine), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 21
 testRunner.And(string.Format("the color of the market ticker item should be {0}", stkclrmine), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 22
 testRunner.And(string.Format("the order with {0},{1},{2} should appear in market ticker", ordertype, status, price), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 23
 testRunner.Then(string.Format("the hoverbox shows tradeable as {0}", tradeableuser), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 24
 testRunner.Then("I Logout of Aom", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 25
 testRunner.And(string.Format("I enter username {0} and password {1} again", observeruser, observerpwd), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 26
 testRunner.When("I click on SignIn on Login Page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 27
 testRunner.Then(string.Format("I should be on the dashboard page {0}", loggedinobserver), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 28
 testRunner.Then(string.Format("the order with the price {0} quantity {1} broker {2} and principal {3} should be " +
                        "displayed in {4} bidask stack", price, quantity, orderbroker, principal, productname), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 29
 testRunner.And(string.Format("the color of the order should be {0}", stkclrother), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 30
 testRunner.Then(string.Format("the hoverbox shows tradeable as {0}", tradeableobserver), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 31
 testRunner.And(string.Format("the order with {0},{1},{2} should appear in market ticker", ordertype, status, price), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 32
 testRunner.And(string.Format("the color of the market ticker item should be {0}", mkttickclr), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 33
 testRunner.Then("the observer executes the order", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 34
 testRunner.And(string.Format("the order no longer exists on the bid/ask stack for the {0}", productname), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 35
 testRunner.And(string.Format("the order with {0},{1},{2} should appear in market ticker", dealstatus, status, price), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 36
 testRunner.And(string.Format("the color of the market ticker item should be {0}", stkclrmine), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 37
 testRunner.Then("I Logout of Aom", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("01. T1 can buysell to T2 - No Restriction: Variant 0")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "CME DealsTwoWay")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("CME")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("VariantName", "Variant 0")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:inituser", "ALT24")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:initpwd", "Password%")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:ordertype", "BID")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:productname", "AUTO RED RME")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:price", "7120.00")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:quantity", "1000")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:broker", "*")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:notes", "Automation Test Order")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:Stkclrmine", "Orange")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:Stkclrother", "Black")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:mkttickclr", "Grey")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:observeruser", "ALT14")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:observerpwd", "Password%")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:principal", "AT2")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:orderbroker", "*")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:status", "NEW")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:loggedinas", "Auto Test 2")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:loggedinobserver", "Auto Test 1")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:tradeableuser", "No")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:tradeableobserver", "Yes")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:dealstatus", "DEAL")]
        public virtual void _01_T1CanBuysellToT2_NoRestriction_Variant0()
        {
#line 6
this._01_T1CanBuysellToT2_NoRestriction("ALT24", "Password%", "BID", "AUTO RED RME", "7120.00", "1000", "*", "Automation Test Order", "Orange", "Black", "Grey", "ALT14", "Password%", "AT2", "*", "NEW", "Auto Test 2", "Auto Test 1", "No", "Yes", "DEAL", ((string[])(null)));
#line hidden
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("01. T1 can buysell to T2 - No Restriction: Variant 1")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "CME DealsTwoWay")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("CME")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("VariantName", "Variant 1")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:inituser", "ALT24")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:initpwd", "Password%")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:ordertype", "ASK")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:productname", "AUTO RED FAME")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:price", "7121.00")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:quantity", "1000")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:broker", "Bilateral Only (No Broker)")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:notes", "Automation Test Order")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:Stkclrmine", "Orange")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:Stkclrother", "Black")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:mkttickclr", "Grey")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:observeruser", "ALT14")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:observerpwd", "Password%")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:principal", "AT2")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:orderbroker", "")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:status", "NEW")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:loggedinas", "Auto Test 2")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:loggedinobserver", "Auto Test 1")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:tradeableuser", "No")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:tradeableobserver", "Yes")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:dealstatus", "DEAL")]
        public virtual void _01_T1CanBuysellToT2_NoRestriction_Variant1()
        {
#line 6
this._01_T1CanBuysellToT2_NoRestriction("ALT24", "Password%", "ASK", "AUTO RED FAME", "7121.00", "1000", "Bilateral Only (No Broker)", "Automation Test Order", "Orange", "Black", "Grey", "ALT14", "Password%", "AT2", "", "NEW", "Auto Test 2", "Auto Test 1", "No", "Yes", "DEAL", ((string[])(null)));
#line hidden
        }
        
        public virtual void _02_T1CannotBuysellToT2_FullRestrictions(
                    string inituser, 
                    string initpwd, 
                    string ordertype, 
                    string productname, 
                    string price, 
                    string quantity, 
                    string broker, 
                    string notes, 
                    string stkclrmine, 
                    string stkclrother, 
                    string observeruser, 
                    string observerpwd, 
                    string principal, 
                    string orderbroker, 
                    string status, 
                    string loggedinas, 
                    string loggedinobserver, 
                    string tradeableuser, 
                    string tradeableobserver, 
                    string dealstatus, 
                    string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("02. T1 cannot buysell to T2 - Full Restrictions", exampleTags);
#line 46
 this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "loginbox"});
            table3.AddRow(new string[] {
                        "Argus Open Markets - CME Login"});
#line 47
 testRunner.Given("I navigate to url", ((string)(null)), table3, "Given ");
#line 50
 testRunner.And(string.Format("I enter username {0} and password {1}", inituser, initpwd), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 51
 testRunner.When("I click on SignIn on Login Page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 52
 testRunner.Then(string.Format("I should be on the dashboard page {0}", loggedinas), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 53
 testRunner.When(string.Format("I click on the Order of type {0} for the Product {1}", ordertype, productname), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            TechTalk.SpecFlow.Table table4 = new TechTalk.SpecFlow.Table(new string[] {
                        "title"});
            table4.AddRow(new string[] {
                        string.Format("BID ASK - {0}", productname)});
#line 54
 testRunner.Then("I should be taken to the New Order Page for the product", ((string)(null)), table4, "Then ");
#line 57
 testRunner.Then(string.Format("I fill in the order details with {0}, {1} ,{2} ,{3} ,{4}", ordertype, price, quantity, broker, notes), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 58
 testRunner.When("I click on create Order", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 59
 testRunner.Then(string.Format("the order with the price {0} quantity {1} broker {2} and principal {3} should be " +
                        "displayed in {4} bidask stack", price, quantity, orderbroker, principal, productname), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 60
 testRunner.And(string.Format("the color of the order should be {0}", stkclrmine), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 61
 testRunner.And(string.Format("the color of the market ticker item should be {0}", stkclrmine), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 62
 testRunner.And(string.Format("the order with {0},{1},{2} should appear in market ticker", ordertype, status, price), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 63
 testRunner.Then(string.Format("the hoverbox shows tradeable as {0}", tradeableuser), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 64
 testRunner.Then("I Logout of Aom", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 65
 testRunner.And(string.Format("I enter username {0} and password {1} again", observeruser, observerpwd), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 66
 testRunner.When("I click on SignIn on Login Page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 67
 testRunner.Then(string.Format("I should be on the dashboard page {0}", loggedinobserver), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 68
 testRunner.Then(string.Format("the order with the price {0} quantity {1} broker {2} and principal {3} should be " +
                        "displayed in {4} bidask stack", price, quantity, orderbroker, principal, productname), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 69
 testRunner.And(string.Format("the color of the order should be {0}", stkclrother), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 70
 testRunner.Then(string.Format("the hoverbox shows tradeable as {0}", tradeableobserver), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 71
 testRunner.And(string.Format("the order with {0},{1},{2} should appear in market ticker", ordertype, status, price), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 72
 testRunner.And(string.Format("the color of the market ticker item should be {0}", stkclrother), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 73
 testRunner.And(string.Format("the order should not be editable or executable by {0}", observeruser), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 74
 testRunner.Then("I close the order screen", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 75
 testRunner.Then("I Logout of Aom", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("02. T1 cannot buysell to T2 - Full Restrictions: Variant 0")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "CME DealsTwoWay")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("CME")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("VariantName", "Variant 0")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:inituser", "ALT24")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:initpwd", "Password%")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:ordertype", "BID")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:productname", "AUTO RED RME")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:price", "7122.00")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:quantity", "1000")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:broker", "*")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:notes", "Automation Test Order")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:Stkclrmine", "Orange")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:Stkclrother", "DOVEGREY")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:observeruser", "ALT34")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:observerpwd", "Password%")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:principal", "AT2")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:orderbroker", "*")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:status", "NEW")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:loggedinas", "Auto Test 2")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:loggedinobserver", "Auto Test 3")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:tradeableuser", "No")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:tradeableobserver", "No")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:dealstatus", "DEAL")]
        public virtual void _02_T1CannotBuysellToT2_FullRestrictions_Variant0()
        {
#line 46
 this._02_T1CannotBuysellToT2_FullRestrictions("ALT24", "Password%", "BID", "AUTO RED RME", "7122.00", "1000", "*", "Automation Test Order", "Orange", "DOVEGREY", "ALT34", "Password%", "AT2", "*", "NEW", "Auto Test 2", "Auto Test 3", "No", "No", "DEAL", ((string[])(null)));
#line hidden
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("02. T1 cannot buysell to T2 - Full Restrictions: Variant 1")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "CME DealsTwoWay")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("CME")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("VariantName", "Variant 1")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:inituser", "ALT24")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:initpwd", "Password%")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:ordertype", "ASK")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:productname", "AUTO RED RME")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:price", "7123.00")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:quantity", "1000")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:broker", "Bilateral Only (No Broker)")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:notes", "Automation Test Order")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:Stkclrmine", "Orange")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:Stkclrother", "DOVEGREY")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:observeruser", "ALT34")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:observerpwd", "Password%")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:principal", "AT2")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:orderbroker", "")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:status", "NEW")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:loggedinas", "Auto Test 2")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:loggedinobserver", "Auto Test 3")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:tradeableuser", "No")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:tradeableobserver", "No")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("Parameter:dealstatus", "DEAL")]
        public virtual void _02_T1CannotBuysellToT2_FullRestrictions_Variant1()
        {
#line 46
 this._02_T1CannotBuysellToT2_FullRestrictions("ALT24", "Password%", "ASK", "AUTO RED RME", "7123.00", "1000", "Bilateral Only (No Broker)", "Automation Test Order", "Orange", "DOVEGREY", "ALT34", "Password%", "AT2", "", "NEW", "Auto Test 2", "Auto Test 3", "No", "No", "DEAL", ((string[])(null)));
#line hidden
        }
    }
}
#pragma warning restore
#endregion
