﻿@CME
#Editor1 ED14 and Editor2 ED24 both are created as Argus Editors



Feature: CME PlusDeal
	To Test Extenal/PLus Deal functionality


######## Editor creates an External Deal #########

Scenario Outline: 01. Editor creates a Pending External Deal and is not visible to other users except other Editor
Given I navigate to url
| loginbox             |
|  Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Pending
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
And I enter username <user1> and password <user1pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
Then the order with <ordertype>,<dealstatus>,<price> should not be visible in market ticker for <user1>
Then I Logout of Aom
And I enter username <editor2> and password <editor2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor2>
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract      | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | user1 | user1pwd  | loggedinasuser1 | editor2 | editor2pwd | loggedinaseditor2 |
| ED14    | Password   | Argus Editor 1    | AUTO RED FAME | 856.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | Pureblue   | PENDING    | + DEAL    | ALT34 | Password% | Auto Test 3     | ED24    | Password   | Argus Editor 2    |


Scenario Outline: 02. Editor creates a Verified External Deal and is visible to all users subscribed to that product 
Given I navigate to url
| loginbox             |
|  Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Verified
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
And I enter username <user1> and password <user1pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract      | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | user1 | user1pwd  | loggedinasuser1 | Stkclrother | editor2 | editor2pwd | loggedinaseditor2 |
| ED14    | Password   | Argus Editor 1    | AUTO RED FAME | 857.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | ORANGE     | VERIFIED   | + DEAL    | ALT34 | Password% | Auto Test 3     | Grey        | ED24    | Password   | Argus Editor 2    |



Scenario Outline: 03. Editor creates a Pending External Deal and then Updates 
Given I navigate to url
| loginbox             |
|  Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Pending
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
And I change the product <newproduct> field
And I update the Notes <newnotes> field
Then I click on Update Button 
Then the updated order with <ordertype>, <newproduct>, <newnotes> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
And I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract      | price | quantity | location | buyer | seller | broker | notes | Stkclrmine | ordertype | newproduct | newnotes |
| ED14    | Password   | Argus Editor 1    | AUTO RED FAME | 858.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | Pureblue     | + DEAL    | AUTO RED RME | Automation External Deal Updated |



Scenario Outline: 04. Editor creates a Pending External Deal and then Voides 
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Pending
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
When I Void the product
Then i should be taken to Void Reason selection screen
| title              |
| ENTER VOID REASON: |
And I enter the void reason <voidreason>
And I confirm and click on void
Then the voided order with <ordertype>, <voidstatus>, <voidreason>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
And I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract      | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | ordertype | voidreason        | Stkclrother | voidstatus |
| ED14    | Password   | Argus Editor 1    | AUTO RED FAME | 859.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | Pureblue     | + DEAL    | Deal not verified | ORANGE      | VOIDED     |
| ED14    | Password   | Argus Editor 1    | AUTO BIOFUEL  | 860.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | Pureblue     | + DEAL    | Other             | ORANGE      | VOIDED     |


Scenario Outline: 05. Editor creates a Pending External Deal and then Verifies 
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Pending
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
Then I click on Verify button
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
And I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract     | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | ordertype | Stkclrother |
| ED14    | Password   | Argus Editor 1    | AUTO RED RME | 861.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | Pureblue   | + DEAL    | ORANGE      |

#AOMK-447
Scenario Outline: 06. Editor creates a Verified External Deal and then should not be able to Update
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Verified
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Void Deal Page
| title                       |
| VOID DEAL (EXTERNAL TO AOM) |
Then the update button should not be present
Then I close the order screen
#And I change the product <newproduct> field
#And I update the Notes <newnotes> field
#Then I click on Update Button 
#Then the updated order with <ordertype>, <newproduct>, <notes> should appear in market ticker
#And the color of the market ticker item should be <Stkclrmine>
And I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract     | price | quantity | location | buyer | seller | broker | notes | Stkclrmine | ordertype | newproduct | newnotes |
| ED14    | Password   | Argus Editor 1    | AUTO RED RME | 862.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | ORANGE     | + DEAL    | AUTO RED RME | Automation External Deal Updated |


#AOMK-447
Scenario Outline: 07. Editor creates a Verified External Deal and Voides
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Verified
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Void Deal Page
| title                       |
| VOID DEAL (EXTERNAL TO AOM) |
When I Void the product
Then i should be taken to Void Reason selection screen
| title              |
| ENTER VOID REASON: |
And I enter the void reason <voidreason>
And I confirm and click on void
Then the voided order with <ordertype>, <voidstatus>, <voidreason>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract     | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | ordertype | voidreason | voidstatus |
| ED14    | Password   | Argus Editor 1    | AUTO BIOFUEL | 863.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | ORANGE     | + DEAL    | Other      | VOIDED     |


Scenario Outline: 08. Editor creates a External Deal and Voides
Given I navigate to url
| loginbox             |
|  Argus Open Markets - CME Login |
And I enter username <editor1> and password <editor1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal as Verified
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Void Deal Page
| title                       |
| VOID DEAL (EXTERNAL TO AOM) |
When I Void the product
Then i should be taken to Void Reason selection screen
| title              |
| ENTER VOID REASON: |
And I do not enter the void reason <voidreason>
Then I verify that void button is disabled
Then I cancel the void reason selection screen
And I exit the void screen
Then I Logout of Aom
Examples:
| editor1 | editor1pwd | loggedinaseditor1 | contract     | price  | quantity | location  | buyer   | seller   | broker   | notes                   | Stkclrmine | ordertype | voidreason | voidstatus |
| ED14    | Password   | Argus Editor 1    | AUTO BIOFUEL | 864.00 | 367      | Editor 14 | Buyer02 | Seller02 | Broker02 | Automated External Deal | ORANGE     | + DEAL    | Other      | VOIDED     |


#Needs modification 
#Scenario Outline: 09. Products that are subscribed to an organisation are listed in Contract drop down Menu
#Given I navigate to url
#| loginbox             |
#|  Argus Open Markets - CME Login |
#And I enter username <editor1> and password <editor1pwd>
#When I click on SignIn on Login Page	
#Then I should be on the dashboard page <loggedinaseditor1>
#When I click on the Plus Deal
#Then I should be taken to the Plus Deal page
#| title                         |
#| REPORT DEAL (EXTERNAL TO AOM) |
#Then I verify all the products are available in the Contract drop down Menu
#| Products                                           |
#| AUTO RED RME,AUTO RED FAME,AUTO BIOFUEL,AUTO OTHER |
#Then I exit the Plus Deal Screen
#Then I Logout of Aom
#Examples:
#| editor1 | editor1pwd | loggedinaseditor1 | 
#| ED14    | Password   | Argus Editor 1    |



######## Trader creates an External Deal #########

Scenario Outline: 09. Trader creates a new External Deal and is not visible to other users except editor
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <user1> and password <user1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
And I enter username <user2> and password <user2pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser2>
Then the order with <ordertype>,<dealstatus>,<price> should not be visible in market ticker for <user2>
Then I Logout of Aom
And I enter username <editor> and password <editorpwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor>
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclreditor>
Then I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract      | price  | quantity | location | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | user2 | user2pwd  | loggedinasuser2 | editor | editorpwd | loggedinaseditor | Stkclreditor |
| ALT54 | Password% | Auto Test 5     | AUTO RED FAME | 865.00 | 267      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | PENDING    | + DEAL    | ALT34 | Password% | Auto Test 3     | ED14   | Password  | Argus Editor 1   | Pureblue     |


@ignore
#AOMK - 211
Scenario Outline: 10. Trader creates a new External Deal and attempts to update it
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <user1> and password <user1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
And I change the product <newproduct> field
And I update the Notes <newnotes> field
Then I click on Update Button 
Then the updated order with <ordertype>, <newproduct>, <newnotes> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
Then I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract      | price  | quantity | location | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | Stkclreditor |
| ALT54 | Password% | Auto Test 5     | AUTO RED FAME | 866.00 | 267      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | PENDING    | + DEAL    | Grey         |

@ignore
#AOMK - 211
Scenario Outline: 11. Trader creates a new External Deal and unable to Void it
Given I navigate to url
| loginbox             |
| Argus Open Markets - CME Login |
And I enter username <user1> and password <user1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
When I Void the product
Then i should be taken to Void Reason selection screen
| title              |
| ENTER VOID REASON: |
And I enter the void reason <voidreason>
And I confirm and click on void
Then the voided order with <ordertype>, <voidstatus>, <voidreason>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
And I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract     | price  | quantity | location | buyer   | seller   | broker   | notes                   | Stkclrmine | voidreason | ordertype | Stkclrother | voidstatus | dealstatus |
| ALT54 | Password% | Auto Test 5     | AUTO RED RME | 867.00 | 267      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | Other      | + DEAL    | Grey        | VOIDED     | PENDING    |


Scenario Outline: 12. Trader creates a new External Deal and Editor verifies it
Given I navigate to url
| loginbox             |
|  Argus Open Markets - CME Login |
And I enter username <user1> and password <user1pwd>
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinasuser1>
When I click on the Plus Deal
Then I should be taken to the Plus Deal page
| title                         |
| REPORT DEAL (EXTERNAL TO AOM) |
Then I fill in the Deal details with <contract> ,<price> ,<quantity> ,<location> ,<buyer>, <seller>, <broker>, <notes>
When I click on create Deal
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclrmine>
Then I Logout of Aom
And I enter username <editor1> and password <editor1pwd> again
When I click on SignIn on Login Page	
Then I should be on the dashboard page <loggedinaseditor1>
Then the order with <ordertype>,<dealstatus>,<price> should appear in market ticker
And the color of the market ticker item should be <Stkclreditor>
Then I open up the existing deal
Then I should be taken to Verify Deal Page
| title                         |
| VERIFY DEAL (EXTERNAL TO AOM) |
Then I click on Verify button
Then the order with <ordertype>, <price> should appear in market ticker
And the color of the market ticker item should be <Stkclrother>
And I Logout of Aom
Examples:
| user1 | user1pwd  | loggedinasuser1 | contract     | price  | quantity | location | buyer   | seller   | broker   | notes                   | Stkclrmine | dealstatus | ordertype | editor1 | editor1pwd | loggedinaseditor1 | Stkclreditor | Stkclrother |
| ALT54 | Password% | Auto Test 5     | AUTO RED RME | 868.00 | 267      | Test 54  | Buyer01 | Seller01 | Broker01 | Automated External Deal | ORANGE     | PENDING    | + DEAL    | ED14    | Password   | Argus Editor 1    | Pureblue       | Grey        |
