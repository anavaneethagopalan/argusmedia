﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOMIntegrationTests.Helpers.CounterPartyPermission;
using RestSharp;

namespace AOMIntegrationTests.Api
{
    public class CounterPartyPermissionsApi : BaseApi
    {
        private string UpdateCppUrl => baseApiUrl + "updateCounterpartyPermissions";

        public CounterPartyPermissionsApi(string username, string password) : base(username, password)
        {
        }

        public bool UpdatePermissions(int productId, AllowOrDeny allowOrDeny, BuyOrSell buyOrSell, int ourOrganisation, int theirOrganisation)
        {
            var json = getPermissionJsonMessage(productId, allowOrDeny, buyOrSell, ourOrganisation, theirOrganisation);
            
            return PermissionResponseUpdateSuccessful(json, UpdateCppUrl);
        }

       
    }

   
}


