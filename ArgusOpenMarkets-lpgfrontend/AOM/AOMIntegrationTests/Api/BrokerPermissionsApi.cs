﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOM.App.Domain.Entities;
using AOMIntegrationTests.Helpers.CounterPartyPermission;

namespace AOMIntegrationTests.Api
{
    public class BrokerPermissionsApi : BaseApi
    {
        public string BrokerPermUpdateUrl => baseApiUrl + "updateBrokerPermissions";
        public BrokerPermissionsApi(string username, string password) : base(username, password)
        {
        }

        public bool UpdatePermissions(int productId, AllowOrDeny allowOrDeny, BuyOrSell buyOrSell,  int ourOrganisation,
            int theirOrganisation)
        {
            var json = getPermissionJsonMessage(productId, allowOrDeny, buyOrSell, ourOrganisation, theirOrganisation, true);

            return PermissionResponseUpdateSuccessful(json, BrokerPermUpdateUrl);
        }




    }
}
