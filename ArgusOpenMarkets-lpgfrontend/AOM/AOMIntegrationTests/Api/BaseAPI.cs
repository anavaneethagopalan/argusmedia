﻿using System.Collections.Generic;
using System.Configuration;
using System.Net;
using AOM.App.Domain.Entities;
using AOMIntegrationTests.Helpers.CounterPartyPermission;
using RabbitMQ.Client.Impl;
using RestSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AOMIntegrationTests.Api
{
    public class BaseApi
    {
        public User user = new User();
        protected readonly string baseApiUrl = ConfigurationManager.AppSettings["BaseApiUrl"];
        protected AuthenticatedUser authenticatedUser = null;

        //  private ISession _session;
        public BaseApi(string username, string password, string source = "aom")
        {
            user.Username = username;
            user.Password = password;
            user.LoginSource = source;

            var client = new RestClient(baseApiUrl + "login");
            var request = new RestRequest(Method.POST);
            request.AddJsonBody(user);

            IRestResponse<AuthenticatedUser> response = client.Execute<AuthenticatedUser>(request);
            authenticatedUser = response.Data;

            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                "Status code returned from Web authentication is {0}", response.StatusCode);

            // _session = Diffusion.Sessions.ConnectionTimeout(30000).Principal("AOM-" + user.Username).Password(authenticatedUser.token).Open("ws://10.23.1.197:8080");
        }

        protected string getPermissionJsonMessage(int productId, AllowOrDeny allowOrDeny, BuyOrSell buyOrSell, int ourOrganisation, int theirOrganisation, bool brokerperm = false)
        {
            var message = new CounterPartyRequest()
            {
                Permissions = new List<CounterPartyPermissions>()
                    {
                        new CounterPartyPermissions()
                        {
                            productId = productId,
                            AllowOrDeny = allowOrDeny,
                            BuyOrSell = buyOrSell,
                            LastUpdatedUserId = -1,
                            ourOrganisation = ourOrganisation,
                            theirOrganisation = theirOrganisation,
                            BrokerUpdate = brokerperm
                        }
                    },
                OrganisationId = ourOrganisation
            };

            var jsonserial = Newtonsoft.Json.JsonConvert.SerializeObject(message);

            return jsonserial;
        }

        protected bool PermissionResponseUpdateSuccessful(string json, string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("token", authenticatedUser.sessionToken);
            request.AddHeader("userid", authenticatedUser.userId);

            request.AddParameter("application/json;charset=UTF-8", json, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            var permissionResponse =
                Newtonsoft.Json.JsonConvert.DeserializeObject<CounterPartyPermissionsResponse>(response.Content);

            return permissionResponse.UpdateSuccessful;
        }
    }
}