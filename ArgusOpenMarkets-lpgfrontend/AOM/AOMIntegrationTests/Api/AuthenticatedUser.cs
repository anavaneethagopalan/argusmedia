﻿namespace AOMIntegrationTests.Api
{
    public class AuthenticatedUser
    {
        public string token { get; set; }

        public string message { get; set; }

        public string userId { get; set; }

        public string isAuthenticated { get; set; }

        public string sessionToken { get; set; }

    }
}
