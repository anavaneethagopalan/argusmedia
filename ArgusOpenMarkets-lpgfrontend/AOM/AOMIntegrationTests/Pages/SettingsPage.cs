﻿using AOMIntegrationTests.Helpers;
using MySql.Data.MySqlClient;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMIntegrationTests.Pages
{
    public class SettingsPage : Utilities
    {
        public SettingsPage()
        {
            PageFactory.InitElements(Driver, this);
        }

        // General Elements

        public string sysselectquery =
        " SELECT BuyOrSell, AllowOrDeny FROM `crm.ci`.CounterpartyPermission where LastUpdatedUserId = (Select id from `crm.ci`.UserInfo where Username = @username) and productid = (select id from `aom.ci`.Product where name = @prodname) and TheirOrganisation = (select id from `crm.ci`.Organisation where ShortCode = @theirorg)";

        public string uatselectquery =
         " SELECT  BuyOrSell,AllowOrDeny FROM `crm.uat`.CounterpartyPermission where LastUpdatedUserId = (Select id from `crm.uat`.UserInfo where Username = @username) and productid = (select id from `aom.uat`.Product where name = @prodname) and TheirOrganisation = (select id from `crm.uat`.Organisation where ShortCode = @theirorg)";

        public string sysdeletequery =
        "DELETE FROM `crm.ci`.CounterpartyPermission  where LastUpdatedUserId = (Select id from `crm.ci`.UserInfo where Username = @username) and productid = (select id from `aom.ci`.Product where name = @prodname)";

        public string uatdeletequery =
        "DELETE FROM `crm.uat`.CounterpartyPermission  where LastUpdatedUserId = (Select id from `crm.uat`.UserInfo where Username = @username) and productid = (select id from `aom.uat`.Product where name = @prodname)";

        [FindsBy(How = How.CssSelector, Using = ".settings-link.top")]
        public IWebElement mySettingsPage;

        [FindsBy(How = How.CssSelector, Using = ".pull-right.settings-section.ng-scope>div>h1")]
        public IWebElement cpPermissionsTitle;

        [FindsBy(How = How.CssSelector, Using = ".last-updated.ng-binding")]
        public IWebElement cplastupdated;

        // Populating the permissions matrix. Please wait ...
        [FindsBy(How = How.CssSelector, Using = ".wait-message")]
        public IWebElement waitMessage;

        [FindsBy(How = How.CssSelector, Using = ".us[ng-click=\"permissionChangeClick(product.buy)\"]")]
        public IWebElement cpBuyPermmission;

        [FindsBy(How = How.CssSelector, Using = "#confirmSuccess .success-message")]
        public IWebElement successMessage;

        [FindsBy(How = How.CssSelector, Using = ".us[ng-click=\"permissionChangeClick(product.sell)\"]")]
        public IWebElement cpSellPermmission;

        [FindsBy(How = How.CssSelector, Using = ".fa-check.allow-counterparty")]
        public IWebElement allowallPermissions;

        [FindsBy(How = How.ClassName, Using = "ban-counterparty")]
        public IWebElement denyallPermissions;

        [FindsBy(How = How.CssSelector, Using = ".confirmation-dialog>h2")]
        public IWebElement cpConfirmationTitle;

        [FindsBy(How = How.CssSelector, Using = ".counterparty.ng-binding")]
        public IWebElement orgname;

        [FindsBy(How = How.CssSelector, Using = ".product.ng-binding")]
        public IWebElement productname;

        [FindsBy(How = How.CssSelector, Using = ".buy.Allow")]
        public IWebElement buyAllow;

        [FindsBy(How = How.CssSelector, Using = ".sell.Allow")]
        public IWebElement sellAllow;

        [FindsBy(How = How.CssSelector, Using = ".buy.Deny")]
        public IWebElement buyDeny;

        [FindsBy(How = How.CssSelector, Using = ".sell.Deny")]
        public IWebElement sellDeny;

        // hoverbox Elements

        [FindsBy(How = How.CssSelector, Using = ".hoverContentRow td:nth-child(2)")]
        public IWebElement buyhoverbox;

        // Button Elements

        [FindsBy(How = How.CssSelector, Using = ".menuItemContainer[title = \"Settings\"]")]
        public IWebElement settingsButton;

        [FindsBy(How = How.CssSelector, Using = ".settings-link[ui-sref=\"UserSettings.CounterpartyPermissions\"]")]
        public IWebElement cpPermissionsButton;

        [FindsBy(How = How.CssSelector, Using = "#btnUpdate")]
        public IWebElement cpUpdateButton;

        [FindsBy(How = How.CssSelector, Using = "#btnCancel")]
        public IWebElement cpCancelButton;

        [FindsBy(How = How.CssSelector, Using = "#btnUpdateConfirm")]
        public IWebElement cpConfirmButton;

        [FindsBy(How = How.CssSelector, Using = "#btnUpdateCancel")]
        public IWebElement cpConfirmCancelButton;

        //Methods

        public void ResetPermissionForSys(string sqlquery, string username, string productname)
        {
            // var sqlquery = "select * from products where id = 5";
            // var productId = new List<long>();
            try
            {
                EnvironmentHelper helper = new EnvironmentHelper();
                var con = helper.GetSQLConnection();

                con.Open();
                MySqlCommand command = new MySqlCommand(sqlquery, con);
                command.Parameters.Add("@prodname", MySqlDbType.VarChar).Value = productname;
                command.Parameters.Add("@username", MySqlDbType.VarChar).Value = username;

                MySqlDataReader myReader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                Console.WriteLine("SQL Error: " + e);
            }
        }

        public void ResetPermissionForUat(string sqlquery, string username, string productname)
        {
            // var sqlquery = "select * from products where id = 5";
            // var productId = new List<long>();
            try
            {
                EnvironmentHelper helper = new EnvironmentHelper();
                var con = helper.GetCrmSQLConnection();

                con.Open();
                MySqlCommand command = new MySqlCommand(sqlquery, con);
                command.Parameters.Add("@prodname", MySqlDbType.VarChar).Value = productname;
                command.Parameters.Add("@username", MySqlDbType.VarChar).Value = username;

                MySqlDataReader myReader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                Console.WriteLine("SQL Error: " + e);
            }
        }

        public Dictionary<string, string> GetPermission(string sqlquery, string username, string productname, string orgname)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            string BuyOrSell;
            string AllowOrDeny;

            EnvironmentHelper helper = new EnvironmentHelper();
            var con = helper.GetSQLConnection();

            con.Open();
            MySqlCommand command = new MySqlCommand(sqlquery, con);
            command.Parameters.Add("@prodname", MySqlDbType.VarChar).Value = productname;
            command.Parameters.Add("@username", MySqlDbType.VarChar).Value = username;
            command.Parameters.Add("@theirorg", MySqlDbType.VarChar).Value = orgname;

            MySqlDataReader myReader = command.ExecuteReader();

            if (myReader.HasRows)
            {
                while (myReader.Read())
                {
                    //  productId = Convert.ToInt32(myReader["Id"]);

                    BuyOrSell = myReader["BuyOrSell"].ToString();
                    AllowOrDeny = myReader["AllowOrDeny"].ToString();
                    results.Add(BuyOrSell, AllowOrDeny);
                }
            }

            Console.WriteLine("Results for buy " + results["B"]);
            Console.WriteLine("Results for sell " + results["S"]);

            return results;
        }

        public void hoverAroundPermission(string permissiontype)
        {
            string scriptToRun;
            IJavaScriptExecutor jse = (IJavaScriptExecutor)Driver;
            if (permissiontype.ToUpper() == "BUY")
                scriptToRun = ("$(\'.us[ng-click=\"permissionChangeClick(product.buy)\"]\').mouseover()");
            else
                scriptToRun = ("$(\'.us[ng-click=\"permissionChangeClick(product.sell)\"]\').mouseover()");
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }
    }
}