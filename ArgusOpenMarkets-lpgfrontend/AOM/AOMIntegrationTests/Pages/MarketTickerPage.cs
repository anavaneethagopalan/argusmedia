﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AOMIntegrationTests.Helpers;
using OpenQA.Selenium.Support.PageObjects;

namespace AOMIntegrationTests.Pages
{
    using OpenQA.Selenium;

    public class MarketTickerPage : Utilities
    {

        public MarketTickerPage()
        {
            PageFactory.InitElements(Driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = ".panel-body-height.ng-isolate-scope")]
        public IList<IWebElement> marketTicker;

        [FindsBy(How = How.CssSelector, Using = "#mTContainer p")]
        public IList<IWebElement> marketTickerItems;

        [FindsBy(How = How.CssSelector, Using = "#mTContainer p")]
        public IWebElement marketTickerSingleItem;


        [FindsBy(How = How.CssSelector, Using = "#mTContainer .panel-body-market-info-container p")]
        public IList<IWebElement> marketTickercontent;


        [FindsBy(How = How.CssSelector, Using = "#mTContainer .panel-body-btn-container")]
        public IList<IWebElement> marketTickerDealStatus;

        // For cme, the selector has to be ".market-ticker-item-cobrokering-icon .fa" 
        [FindsBy(How = How.CssSelector, Using = ".market-ticker-item-cobrokering-icon>img")]
        public IWebElement cobrokeringIcon;

        [FindsBy(How = How.CssSelector, Using = ".market-ticker-item-cobrokering-icon>.fa")]
        public IWebElement cmecobrokeringIcon;

        public List<MarketTickerDisplayItem> GetMarketTickerItems()
        {
            List<MarketTickerDisplayItem> mkttickerItems = new List<Utilities.MarketTickerDisplayItem>();
            var MtItems = Driver.FindElement(By.Id("mTContainer")).FindElements(By.TagName("p"));
            var paras = MtItems;
             Console.WriteLine("Count of Market Ticker Items: " + paras.Count);

            int j = 0;
            for (int i = 0; i < 5; i++)
            {
                if (paras.Count <= j+2)
                    break;
                var bidOrAsk = string.Empty;
                var time = string.Empty;
                var mtcontent = string.Empty;

                bidOrAsk = paras[j].Text;
                time = paras[j + 1].Text;
                mtcontent = paras[j + 2].Text;

                var mtItem = new MarketTickerDisplayItem
                {
                    BidOrAsk = bidOrAsk,
                    DateTimeCreated = time,
                    MarketTickerItem = mtcontent
                };

                mkttickerItems.Add(mtItem);
                j=j+3;
            }

            return mkttickerItems;
        }

        public string GetMarketTickerItemColor()
        {
            var items = marketTickercontent.First();

            return (items.GetCssValue("color"));
        }

        public string GetMarketTickerItemColor(MarketTickerDisplayItem item)
        {
            var element = marketTickercontent.First(x => x.Text.Contains(item.MarketTickerItem));

            return (element.GetCssValue("color"));
        }

        public void GetDealStatus()
        {
            var dealstatus = marketTickerDealStatus.First();
            Console.WriteLine("Deal Status: " + dealstatus.Text);
        }

        public bool CoBrokeringIconDisplayed(string order, string status, string price)
        {
            switch (order.ToLower())
            {
                case "bid":
                    order = "bids";
                    break;
                case "deal":
                    order = "sells";
                    break;
                case "ask":
                    order = "asks";
                    break;
            }

           var markettickerElement = GetMarketTickerElement(order, status, price);

              var iconelements = markettickerElement.FindElements((By.CssSelector("*"))).Where(x => !string.IsNullOrEmpty(x.GetAttribute("ng-style")));

            var marketTickerElement =
                iconelements.Single(x => x.GetAttribute("ng-style").Contains("coBrokering"));

            if (!marketTickerElement.GetAttribute("style").Contains("hidden"))
            {
                return true;
            }
            return false;
        }

        public IWebElement GetMarketTickerElement(string order, string status, string price)
        {
            return Driver.FindElements(By.ClassName("panel-body-market-info-container"))
                .First(x => x.Text.Contains(price) && x.Text.ToLower().Contains(order.ToLower()) && x.Text.ToLower().Contains(status.ToLower()));

        }
    }
}



/*
 *ï»¿namespace AOMIntegrationTests.Pages
{
    using AOMIntegrationTests.Pages.Widgets.MarketTickerWidget;
    using OpenQA.Selenium;
    using System;
    using System.Collections.Generic;

    public class MarketTickerWidget : BasePage
    {

        public MarketTickerWidget(IWebDriver webDriver)
            : base(webDriver)
        {

        }

        private void InitialisePageObjects()
        {
            // TODO: Need to Initialise objects.
        }

        internal List<MarketTickerDisplayItem> GetMarketTickerItems()
        {
            List<MarketTickerDisplayItem> marketTickerItems = new List<MarketTickerDisplayItem>();
            // $('#mTContainer p')
            var paras = WebDriver.FindElements(By.Id("mTContainer p"));

            for (int i = 0; i < paras.Count - 1; i = i + 3)
            {
                var bidOrAsk = string.Empty;
                var time = string.Empty;
                var marketTickerItem = string.Empty;
                try
                {
                    bidOrAsk = paras[i].Text;
                }
                catch (Exception)
                {
                }
                try
                {
                    time = paras[i + 1].Text;
                }
                catch (Exception)
                {
                }
                try
                {
                    marketTickerItem = paras[i + 2].Text;
                }
                catch (Exception)
                {
                }

                var mtItem = new MarketTickerDisplayItem
                {
                    BidOrAsk = bidOrAsk,
                    DateTimeCreated = time,
                    MarketTickerItem = marketTickerItem
                };

                marketTickerItems.Add(mtItem);
            }

            return marketTickerItems;
        }

        internal bool VerifyMarketTickerExistsWithPrice(decimal price)
        {
            var priceExists = false;
            var retryExceeded = false;
            var retryCount = 0;


            while (!retryExceeded)
            {
                var mtItems = GetMarketTickerItems();
                if (mtItems[0].MarketTickerItem.Contains(price.ToString()))
                {
                    priceExists = true;
                }

                retryCount ++;
                if (retryCount > 5)
                {
                    retryExceeded = true;
                }
                else
                {
                    retryExceeded = priceExists;
                }
            }

            return priceExists;
        }
    }
}


*/
