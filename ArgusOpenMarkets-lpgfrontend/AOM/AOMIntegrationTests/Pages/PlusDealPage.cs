﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AOM.App.Domain.Entities;
using AOMIntegrationTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AOMIntegrationTests.Pages
{
   public class PlusDealPage : Utilities
    {
        public PlusDealPage()
        {
            PageFactory.InitElements(Driver,this);
        }

// General
        [FindsBy(How = How.CssSelector, Using = ".modal-header>h3")]
        public IWebElement plusDealHeader;

        [FindsBy(How = How.CssSelector, Using = ".modal-header>h3:nth-child(3)")]
        public IWebElement verifyDealHeader;

        [FindsBy(How = How.CssSelector, Using = ".modal-header>h3:nth-child(2)")]
        public IWebElement voidDealHeader;


        [FindsBy(How = How.CssSelector, Using = ".chosen-single")]
        public IWebElement plusDealContract;

        [FindsBy(How = How.CssSelector, Using = ".chosen-results .active-result")]
        public IList<IWebElement> plusDealContractList;

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine[ng-required='selectedContract.isInternal']")]
        public IWebElement plusDealContractName;

        [FindsBy(How = How.Name, Using = "price")]
        public IWebElement plusDealPrice;

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine[ name='pricedetail']")] //.ng-pristine[ng-model='deal.optionalPriceDetail']
        public IWebElement plusDealOptionalPriceDetail;


        [FindsBy(How = How.Name, Using = "quantity")]
        public IWebElement plusDealQuantity;

        [FindsBy(How = How.Name, Using = "location")]
        public IWebElement plusDealLocation;

 /*       [FindsBy(How = How.CssSelector, Using = "")]
        public IWebElement plusDealDeliveryPeriod;       */

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine[ng-model='deal.buyerName']")]
        public IWebElement plusDealBuyer;

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine[ng-model='deal.sellerName']")]
        public IWebElement plusDealSeller;

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine[ng-model='deal.brokerName']")]
        public IWebElement plusDealBroker;

        [FindsBy(How = How.Name, Using = "notes")]
        public IWebElement plusDealNotes;

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine[ng-model='deal.freeFormDealMessage']")]
        public IWebElement freeFromDeal;

        [FindsBy(How = How.CssSelector, Using = "#voidReasonSelection>label")]
        public IWebElement voidSelectionReason;

        // Button Elements

        [FindsBy(How = How.CssSelector, Using = ".ticker-header-deal .btn.btn-primary.btn-deal")]
        public IWebElement plusDealButton;

        [FindsBy(How = How.CssSelector, Using = "#btnNewDealCreate")]
        public IWebElement plusDealCreateButton;

        [FindsBy(How = How.CssSelector, Using = "#btnAuthExtDealAmend")]
        public IWebElement plusDealUpdateButton;

        [FindsBy(How = How.CssSelector, Using = "#btnAuthExtDealAuthenticate")]
        public IWebElement plusDealVerifyButton;


        [FindsBy(How = How.CssSelector, Using = "#btnAuthExtDealVoid")]
        public IWebElement plusDealVoidButton;

        [FindsBy(How = How.CssSelector, Using = "#btnVoidConfirm")]
        public IWebElement voidConfirmationButton;

        [FindsBy(How = How.CssSelector, Using = "#btnVoidCancel")]
        public IWebElement voidCancelButton;

        [FindsBy(How = How.CssSelector, Using = "#btnNewDealCreateV")]
        public IWebElement createAsVerifiedButton;

        [FindsBy(How = How.CssSelector, Using = "#btnNewDealCreateP")]
        public IWebElement createAsPendingButton;

        [FindsBy(How = How.CssSelector, Using = "#btnNewDealExit")]
        public IWebElement plusDealExitButton;


        [FindsBy(How = How.CssSelector, Using = "#btnNewExtDealExit")]
        public IWebElement voidDealExitButton;

        // Radio group and Checkboxes

        [FindsBy(How = How.CssSelector, Using = ".radio-group input[value='deal_not_verified']")]
        public IWebElement deal_not_verified;

        [FindsBy(How = How.CssSelector, Using = ".radio-group input[value='deal_not_completed']")]
        public IWebElement deal_not_completed ;
        
        [FindsBy(How = How.CssSelector, Using = ".radio-group input[value='deal_terms_amended']")]
        public IWebElement deal_terms_ammended;

        [FindsBy(How = How.CssSelector, Using = ".radio-group input[value='other']")]
        public IWebElement deal_other;

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine.ng-untouched.ng-valid[ng-model='voidReason.reasonText']")]
        public IWebElement enterReason ;

        [FindsBy(How = How.CssSelector, Using = ".radio>label")]
        public IList<IWebElement> voidReasonList;

        // Methods

        public void SelectContract(string productname)
        {
            plusDealContract.Click();

            foreach (var t in plusDealContractList)
            {
                if (t.Text.Equals(productname))
                {
                    t.Click();
                    break;
                }
            }
        }

        public void SelectVoidReason(string reason)
        {

            switch (reason)
            {
                case "Deal not verified":
                    deal_not_verified.Click();
                    break;
                case "Deal not completed":
                    deal_not_completed.Click();
                    break;
                case "Deal terms amended":
                    deal_terms_ammended.Click();
                    break;
                case "Other":
                    deal_other.Click();
                    enterReason.SendKeys(reason);
                    break;
                default:
                    Console.WriteLine("Void Reason not provided");
                    break;
            }

            

        }

        public string GetFreeFormDealText()
        {
           

            var scriptToRun = ("return $(\'.ng-pristine[ng-model=\"deal.freeFormDealMessage\"]\').val();");
            var text = ((IJavaScriptExecutor) Driver).ExecuteScript(scriptToRun).ToString();
            return text;


        }


    }
}
