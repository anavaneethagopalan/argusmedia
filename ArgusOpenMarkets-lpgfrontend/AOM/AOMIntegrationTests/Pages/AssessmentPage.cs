﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMIntegrationTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AOMIntegrationTests.Pages
{
   public class AssessmentPage:Utilities
    {
        public  AssessmentPage()
        {
            PageFactory.InitElements(Driver, this);
        }

// General Elements
        [FindsBy(How = How.CssSelector, Using = ".assessment-title")]
        public IWebElement productAssessment;

        // Assessment Price Input
        [FindsBy(How = How.CssSelector, Using = ".modal-header>h3")]
        public IWebElement assessmentHeader;

     
        [FindsBy(How = How.CssSelector, Using = ".input-medium[name=\"lowPrice\"]")]
        public IWebElement lowPrice;

     
        [FindsBy(How = How.CssSelector, Using = ".input-medium[name=\"highPrice\"]")]
        public IWebElement highPrice;

        [FindsBy(How = How.CssSelector, Using = ".chosen-single.chosen-default")]
        public IWebElement statusDropDown;

        [FindsBy(How = How.CssSelector, Using = ".chosen-drop .chosen-results .active-result")]
        public IList<IWebElement> statusSelect;

        [FindsBy(How = How.CssSelector, Using = "#dark-select:nth-last-of-type(3) .chosen-single")]
        public IWebElement periodDropDown;
        
        [FindsBy(How = How.CssSelector, Using = "#dark-select:nth-last-of-type(3) .chosen-results .active-result")]
        public IList<IWebElement> periodSelect;


        [FindsBy(How = How.CssSelector, Using = ".ng-isolate-scope[assessmentlabel=\"Today:\"] .ng-binding[ng-class=\"getPriceClass(assessment)\"]")]
        public IList<IWebElement> todaysPrice;

        [FindsBy(How = How.CssSelector, Using = "[assessmentlabel=\"Today:\"] tr:nth-child(2) .ng-binding:nth-child(1)")]
        public IWebElement todaysDate;


        [FindsBy(How = How.CssSelector, Using = ".ng-isolate-scope[assessmentlabel=\"Prev:\"] .ng-binding[ng-class=\"getPriceClass(assessment)\"]")]
        public IList<IWebElement> previousPrice;

        [FindsBy(How = How.CssSelector, Using = "[assessmentlabel=\"Prev:\"] tr:nth-child(2) .ng-binding:nth-child(1)")]
        public IWebElement previousDate;

        [FindsBy(How = How.CssSelector, Using = ".assessment-status-display-name:nth-child(2)")]
        public IWebElement assessmentTodayStatus;

        [FindsBy(How = How.CssSelector, Using = ".assessment-status-display-name:nth-child(1)")]
        public IWebElement assessmentPreviousStatus;


        // Button Elements

        [FindsBy(How = How.CssSelector, Using = ".btn.btn-lg.ng-binding")]
        public IWebElement clearAssessment;

        [FindsBy(How = How.CssSelector, Using = ".btn.btn-lg.btn-green.selected")]
        public IWebElement inputCurrentEODPrices;


        [FindsBy(How = How.CssSelector, Using = "")]
        public IWebElement rollPrices;


        public void WaitForassessmentTodayStatusTextContains(string status)
        {
            WaitForCssElementToContain(".assessment-status-display-name:nth-child(2)", 20, status);
        }

        public void WaitForassessmentPreviousStatusTextContains(string status)
        {
            WaitForCssElementToContain(".assessment-status-display-name:nth-child(1)", 20, status);
        }

        public void ClickOnproductAssessment()
        {
            productAssessment.FindElement(By.TagName("td")).Click();

        }
    }
}
