﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMIntegrationTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AOMIntegrationTests.Pages
{
   public class MarketInfoPage : Utilities
    {
        public MarketInfoPage()
        {
            PageFactory.InitElements(Driver, this);
        }


        // General

        [FindsBy(How = How.CssSelector, Using = ".modal-header>h3")]
        public IWebElement marketInfoHeader;


        [FindsBy(How = How.CssSelector, Using = ".ng-pristine[ng-model='info.marketInfoText']")]
        public IWebElement marketInfoText;

       // [FindsBy(How = How.CssSelector, Using = ".ng-valid.ng-valid-required[ng-model=\"info.info\"]")]
      //  public IWebElement marketInfoUpdateText;


        [FindsBy(How = How.TagName, Using = "textarea")]
        public IWebElement marketInfoUpdateText;

        // Button Elements

        [FindsBy(How = How.CssSelector, Using = ".btn-market")]
        public IWebElement marketInfoButton;

        [FindsBy(How = How.CssSelector, Using = ".btn.btn-sm.btn-green[ng-click=\"newInfo(\'Pending\')\"]")]
        public IWebElement marketInfoCreateButton;

        [FindsBy(How = How.CssSelector, Using = ".pull-right.btn.btn-sm.btn-grey")]
        public IWebElement marketInfoExitButton;

        [FindsBy(How = How.CssSelector, Using = "#btnAuthInfoAuthenticate")]
        public IWebElement marketInfoVerifyButton;

        [FindsBy(How = How.CssSelector, Using = "#btnAuthInfoAmend")]
        public IWebElement marketInfoUpdateButton;

        [FindsBy(How = How.CssSelector, Using = "#btnAuthInfoVoid")]
        public IWebElement marketInfoVoidButton;

     /*   [FindsBy(How = How.CssSelector, Using = ".btn.btn-md.btn-green:nth-of-type(2)")]
        public IWebElement created_as_verifiedButton; */

        [FindsBy(How = How.CssSelector, Using = ".btn.btn-md.btn-green[ng-click=\"newInfo(\'Active\')\"]")]
        public IWebElement created_as_verifiedButton;


     /*   [FindsBy(How = How.CssSelector, Using = ".btn.btn-md.btn-green:nth-of-type(3)")]
        public IWebElement create_as_pendingButton; */

        [FindsBy(How = How.CssSelector, Using = ".btn.btn-md.btn-green[ng-click=\"newInfo(\'Pending\')")]
        public IWebElement create_as_pendingButton;


        public void CreateMarketInfo()
        { 

            var scriptToRun = "$('.btn.btn-sm.btn-green').click()";
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public void ExitMarketInfo()
        {

            var scriptToRun = "$('.pull-right.btn.btn-sm.btn-grey').click()";
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public void CreateAsVerified()
        {

            var scriptToRun = "$('.btn.btn-md.btn-green[ng-show=\"canCreateVerified()\"]').click()";
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public void CreateAsPending()
        {

            var scriptToRun = "$('.btn.btn-md.btn-green[ng-show=\"canCreateVerified()\"]').click()";
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }
    }
}
