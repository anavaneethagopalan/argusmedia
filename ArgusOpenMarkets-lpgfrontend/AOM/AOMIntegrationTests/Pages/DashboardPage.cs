﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using AOM.App.Domain.Entities;
using AOMIntegrationTests.Helpers;
using Argus.Transport.Configuration;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow;

namespace AOMIntegrationTests.Pages
{
    using OpenQA.Selenium;
    using System;
    using System.Collections.ObjectModel;
    using MySql.Data.MySqlClient;

    public class DashboardPage : Utilities
    {

        public DashboardPage()
        {
            PageFactory.InitElements(Driver, this);
        }

        //[FindsBy(How = How.ClassName, Using = "userDetails")]
        //public IWebElement UserDetailsContainer;

        [FindsBy(How = How.ClassName, Using = "header-user-role")] public IWebElement loggedUsername;


        [FindsBy(How = How.LinkText, Using = "Argus direct")]
        public IWebElement headerIcon;

        [FindsBy(How = How.CssSelector, Using = ".ngRow")]
        public IList<IWebElement> allOrders;

        [FindsBy(How = How.CssSelector, Using = ".ngCellText")]
        public IList<IWebElement> allCells;

        [FindsBy(How = How.CssSelector, Using = ".ng-binding.ng-isolate-scope")]
        public IList<IWebElement> productname;

        [FindsBy(How = How.CssSelector, Using = ".toast.toast-info")]
        public IWebElement toastmsg;

        [FindsBy(How = How.ClassName, Using = "toast-error")]
        public IWebElement toastError;

        [FindsBy(How = How.ClassName, Using = "hoverContentRow")]
        public IWebElement hoverbox;

        [FindsBy(How = How.CssSelector, Using = ".hoverContentRow:nth-child(4)>.ng-binding")]
        public IWebElement coBrokeringhover;


        [FindsBy(How = How.CssSelector, Using = ".hoverContentRow:nth-child(10)>.ng-binding")]
        public IWebElement hoverboxMetadata1;

        [FindsBy(How = How.CssSelector, Using = ".hoverContentRow:nth-child(11)>.ng-binding")]
        public IWebElement hoverboxMetadata2;

        [FindsBy(How = How.CssSelector, Using = ".hoverContentRow:nth-child(12)>.ng-binding")]
        public IWebElement hoverboxMetadata3;

        [FindsBy(How = How.ClassName, Using = "news-data")] public IWebElement NewsData;


        // Button Elements
        [FindsBy(How = How.Id, Using = "logout")]
       // [FindsBy(How = How.CssSelector, Using = "#logout")]
        public IWebElement signOutButton;

        [FindsBy(How = How.CssSelector, Using = ".fa.fa-cog")]
        public IWebElement settingsButton;


        [FindsBy(How = How.CssSelector, Using = "#bidAskGridUpButton")]
        public IWebElement bidGridUpButton;


        [FindsBy(How = How.CssSelector, Using = "#bidAskGridDownButton")]
        public IWebElement bidGridDownButton;

        [FindsBy(How = How.ClassName, Using = "bidAskStackHeaderContainer")]
        public IList<IWebElement> BidAskStackHeaders;

       // public string query = "select * from `aom.ci`.Product where name = @name";

        //public string query2 = "select product.Id, product.Name, pt.DeliveryDateStart,pt.DeliveryDateEnd from `aom.uat`.ProductTenor pt, .Product product where pt.Product_id_fk =  product.Id";

        public string metadataquery = "";


        // Methods

        public void OpenOrderDialogForOrderTypeAndProduct(string ordertype, string productname)
        {
            var buttonToClick = "newAskButton";


            if (ordertype.ToLower() == "bid")
            {
                buttonToClick = "newBidButton";
            }

            var scriptToRun = string.Format("$('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('#{1}').click()", productname, buttonToClick);
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public int GetProductId(string sqlquery, string productname)
        {
            // var sqlquery = "select * from products where id = 5";
            // var productId = new List<long>();
            int productId = 0;

            EnvironmentHelper helper = new EnvironmentHelper();
            var con = helper.GetSQLConnection();

            con.Open();
            MySqlCommand command = new MySqlCommand(sqlquery, con);
            command.Parameters.Add("@name", MySqlDbType.VarChar).Value = productname;
            ;
            MySqlDataReader myReader = command.ExecuteReader();

            if (myReader.HasRows)
            {
                while (myReader.Read())
                {
                    // querydesc.Add(myReader["Id"].ToString());
                    //productId.Add(Convert.ToInt32(myReader["Id"]));
                    productId = Convert.ToInt32(myReader["Id"]);

                }
            }
            Console.WriteLine("Product Id is " + productId + " for" + productname);

            return productId;

        }

        public bool GetOrderCount(string productname)
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)Driver;
            string scriptToRun;
            //$(".bidAskStackHeaderContainer:contains(ARGUS TEST MARKET)").parent().find(".ngRow")!=null
            scriptToRun = string.Format("return $('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngRow').text()==0;", productname);
            //var elements = Driver.ExecuteScript(scriptToRun);

            bool result = (bool)Driver.ExecuteScript(scriptToRun);
            Console.WriteLine("Element: " + result);
            return result;


        }

        public IEnumerable<string> GetOrders(string ordertype, string productname)
        {
            string scriptToRun;
            IJavaScriptExecutor jse = (IJavaScriptExecutor)Driver;
            if (ordertype.ToUpper() == "BID")
                //$('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngCellBidGridOverride .ngCellText').get();)
                scriptToRun = string.Format("return $('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngRow').first().find('.ngCellBidGridOverride .ngCellText').get();", productname);
            else
                scriptToRun = string.Format("return $('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngCanvas:eq(1) .ngRow').first().find('.ngCellAskGridOverride .ngCellText').get();", productname);

            for (int i = 0; i < 20; i++)
            {
                ReadOnlyCollection<IWebElement> result = (ReadOnlyCollection<IWebElement>)Driver.ExecuteScript(scriptToRun);
                //  var orders = result.Select(x => x.Text).Where(x => !String.IsNullOrEmpty(x));
                var orders = result.Select(x => x.Text);

                if (!orders.ElementAt(0).Equals(""))
                {
                    foreach (var r in orders)
                    {
                        Console.WriteLine("Result: " + r);
                    }
                    return orders;
                }

                ////Wait 500ms
                Thread.Sleep(500);
            }

            return null;


        }

        public void hoverAroundOrder(string ordertype, string productname)
        {
           // string scriptToRun;
            IJavaScriptExecutor jse = (IJavaScriptExecutor)Driver;
            if (ordertype.ToUpper() == "BID")
            {
                WaitElementTobeClickableClasslookup("bidAskStacksContainer", 30);
                var heading = Driver.FindElements(By.ClassName("bidAskStacksContainer"))
                    .Single(x => x.Text.Contains(productname.ToUpper()));
                var bidtohover = heading.FindElement(By.Id("btnOpenBid"));

                MoveToElementUsingActions(bidtohover);
            }
            //scriptToRun = string.Format("$('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngRow').first().find('.ngCellBidGridOverride .ngCellText.col1').mouseover();", productname);
            else
            {
                WaitElementTobeClickableClasslookup("bidAskStacksContainer", 30);
                var heading = Driver.FindElements(By.ClassName("bidAskStacksContainer"))
                    .Single(x => x.Text.Contains(productname.ToUpper()));
                var asktohover = heading.FindElement(By.Id("btnOpenAsk"));
                MoveToElementUsingActions(asktohover);
            }
               // scriptToRun = string.Format("$('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngCanvas:eq(1) .ngRow').first().find('.ngCellAskGridOverride .ngCellText.col1').mouseover();", productname);
            //((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public void MoveOutofOrder(string ordertype, string productname)
        {

            string scriptToRun;
            IJavaScriptExecutor jse = (IJavaScriptExecutor)Driver;
            if (ordertype.ToUpper() == "BID")
                scriptToRun = string.Format("$('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngRow').first().find('.ngCellBidGridOverride .ngCellText.col1').mouseout();", productname);
            else
                scriptToRun = string.Format("$('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngCanvas:eq(1) .ngRow').first().find('.ngCellAskGridOverride .ngCellText.col1').mouseout();", productname);
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public string GetBidAskOrderColor(string ordertype, string productname)
        {
            string rgbcolor;
            WaitElementTobeClickableClasslookup("bidAskStacksContainer", 30);

            var heading = Driver.FindElements(By.ClassName("bidAskStacksContainer"))
                  .Single(x => x.Text.Contains(productname.ToUpper()));
            if (ordertype.ToUpper() == "BID")
            {
                var bidcellText = heading.FindElement(By.ClassName("ngCellBidGridOverride"));
               // var bidcellText = heading.FindElement(By.CssSelector(".ngCellBidGridOverride .ngCellText"));
                rgbcolor = bidcellText.GetCssValue("color");
                
               }
            else
            {
                var askcellText = heading.FindElement(By.CssSelector(".ngCellAskGridOverride"));
                rgbcolor = askcellText.GetCssValue("color");
            }
            Console.WriteLine("Color: " + rgbcolor);

            return rgbcolor;


        /*    string scriptToRun;
            WaitElementTobeClickableClasslookup("bidAskStacksContainer", 30);
            IJavaScriptExecutor jse = (IJavaScriptExecutor)Driver;
            if (ordertype.ToUpper() == "BID")
                // $(".bidAskStackHeaderContainer:contains(ARGUS TEST MARKET)").parent().find(".ngCellBidGridOverride .ngCellText").first().css("color")
                scriptToRun = string.Format("return $('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngCellBidGridOverride .ngCellText').first().css('color');", productname.ToUpper());
            else
                scriptToRun = string.Format("return $('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngCellAskGridOverride .ngCellText').first().css('color');", productname.ToUpper());

            string rgbcolor = Driver.ExecuteScript(scriptToRun).ToString();

            return rgbcolor;
           */
        }


        public void openExistingOrder(string ordertype, string productname)
        {

            if (ordertype.ToUpper() == "BID")
                //scriptToRun = string.Format("$('.bidAskStackHeaderContainer:contains(\"{0}\")').parent().find('.ngCellBidGridOverride .ngCellText').first().click();", productname.ToUpper());
            {
                WaitElementTobeClickableClasslookup("bidAskStacksContainer", 30);
                var heading = Driver.FindElements(By.ClassName("bidAskStacksContainer"))
                    .Single(x => x.Text.Contains(productname.ToUpper()));
                var bidtoclick = heading.FindElement(By.Id("btnOpenBid"));
                bidtoclick.Click();
            }

            else
            {
                WaitElementTobeClickableClasslookup("bidAskStacksContainer", 30);
                var heading = Driver.FindElements(By.ClassName("bidAskStacksContainer"))
                    .Single(x => x.Text.Contains(productname.ToUpper()));
                var bidtoclick = heading.FindElement(By.Id("btnOpenAsk"));
                bidtoclick.Click();
            }
           
        }

        public void clickGridUpbutton()
        {
            string scriptToRun;

            scriptToRun = ("$('#bidAskGridUpButton').click()");
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public void clickGridDownbutton()
        {
            string scriptToRun;

            scriptToRun = ("$('#bidAskGridDownButton').click()");
            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public void signOutClick()
        {
           var logout = Driver.FindElement(By.Id("logout"));

            logout.Click();
        }

        public DataTable GetHoverelements()
        {

          var tableHeaderElements = Driver.FindElements(By.ClassName("hoverContentRowHeader"));
            var tooltipTable = new DataTable();

            foreach (var tablerheaders in tableHeaderElements)
            {
                
                tooltipTable.Columns.Add(tablerheaders.Text.Replace(":",""));
            }

            var qtipelements = Driver.FindElement(By.ClassName("qtip-content")).FindElements(By.ClassName("ng-binding"));

            var tableValues = qtipelements.Where(x => !x.TagName.Contains("b"));
            
            int i = 0;

           var datarow = tooltipTable.NewRow();
            foreach (var elementValues in tableValues)
            {
                datarow[i] = elementValues.Text;
                i++;
            }

            tooltipTable.Rows.Add(datarow);
            return tooltipTable;
        }

        public int GetNewsItemsCount()
        {
            var newitemscount = NewsData.FindElements(By.TagName("li"));

            return newitemscount.Count();

        }

        public int GetBidAskStackHeaderCounter()
        {
            var bidaskstackheadersCount = BidAskStackHeaders.Count;
            return bidaskstackheadersCount;
        }
    }
}
