﻿using System;
using System.Linq;
using AOMIntegrationTests.Helpers;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AOMIntegrationTests.Pages
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using OpenQA.Selenium;
    using System.Collections.Generic;

    public class NewOrderPage : Utilities
    {
        public NewOrderPage()
        {
            PageFactory.InitElements(Driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = ".modal-header")] public IWebElement orderPageHeader;

        [FindsBy(How = How.Name, Using = "price")] public IWebElement priceDetails;

        [FindsBy(How = How.Name, Using = "quantity")] public IWebElement quantityDetails;

        [FindsBy(How = How.CssSelector, Using = "#brokers_chosen .chosen-single")] public IWebElement brokerDropdown;

        [FindsBy(How = How.CssSelector, Using = "#brokers_chosen .chosen-search>input")]
        public IWebElement
            brokerDropdownSearch;

        [FindsBy(How = How.CssSelector, Using = ".active-result.highlighted")] public IWebElement brokerDetails;

        [FindsBy(How = How.CssSelector, Using = ".chosen-single")] public IWebElement principalDropdown;

        [FindsBy(How = How.CssSelector, Using = ".chosen-search>input")] public IWebElement principalDropdownSearch;

        [FindsBy(How = How.CssSelector, Using = ".active-result.highlighted")] public IWebElement principalDetails;
        private IWebElement PrincipalSelectElement => Driver.FindElements(By.TagName("select")).Single(x => x.GetAttribute("name").Equals("principalsSelector"));

        [FindsBy(How = How.CssSelector, Using = "#principals_chosen .chosen-single")] public IWebElement principalDrop;

        [FindsBy(How = How.CssSelector, Using = "#principals_chosen .chosen-search>input")]
        public IWebElement
            principalDropSearch;

        [FindsBy(How = How.CssSelector, Using = ".active-result.highlighted")] public IWebElement principalresults;

        [FindsBy(How = How.CssSelector, Using = ".active-result")] public IList<IWebElement> principalList;

        [FindsBy(How = How.CssSelector, Using = ".active-result")] public IList<IWebElement> brokerprincipalList;

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine[name='notes']")] public IWebElement orderNotes;

        // Metadata Elements

        [FindsBy(How = How.CssSelector,
             Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(1) .chosen-single")]
        public IWebElement metaData1Drop;

        [FindsBy(How = How.CssSelector,
             Using =
                 ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(1) .chosen-search>input")]
        public IWebElement metaData1DropdownSearch;

        [FindsBy(How = How.CssSelector,
             Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(1) .active-result.highlighted")]
        public IWebElement metaData1ResultSelected;

        [FindsBy(How = How.CssSelector,
             Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(1) .active-result")]
        public IWebElement metaData1Results;

        [FindsBy(How = How.CssSelector,
            Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(1) .ng-scope")]
        public IWebElement metaData1ReadOnly;

        [FindsBy(How = How.CssSelector,
             Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(2) .chosen-single")]
        public IWebElement metaData2Drop;

        [FindsBy(How = How.CssSelector,
             Using =
                 ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(2) .chosen-search>input")]
        public IWebElement metaData2DropdownSearch;

        [FindsBy(How = How.CssSelector,
             Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(2) .active-result.highlighted")]
        public IWebElement metaData2ResultSelected;

        [FindsBy(How = How.CssSelector,
             Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(2) .active-result")]
        public IWebElement metaData2Results;

        [FindsBy(How = How.CssSelector,
         Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(2) .ng-scope")]
        public IWebElement metaData2ReadOnly;

        [FindsBy(How = How.CssSelector, Using = ".ng-pristine.ng-isolate-scope[type=\"text\"]")] public IWebElement metaData3Element;

        //.ng-scope[ng-repeat="metadataProperty in metadataCollection"]:nth-of-type(3) .ng-pristine
        [FindsBy(How = How.CssSelector, Using = ".large-input.ng-isolate-scope.ng-dirty.ng-valid-parse.ng-touched.ng-invalid")]
        public IWebElement metaData3Element2;

        [FindsBy(How = How.CssSelector,
         Using = ".ng-scope[ng-repeat=\"metadataProperty in metadataCollection\"]:nth-of-type(3) .ng-scope")]
        public IWebElement metaData3ReadOnly;

        [FindsBy(How = How.CssSelector,
             Using = ".ng-scope:nth-of-type(3) .errorMessageUnderInput.ng-binding.ng-isolate-scope")]
        public
            IWebElement metaData3ErrorMsg;

        // Button Elements

        [FindsBy(How = How.CssSelector, Using = ".btn-group .btn.btn-sm:nth-child(1)")] public IWebElement bidButton;

        [FindsBy(How = How.CssSelector, Using = ".btn-group .btn.btn-sm:nth-child(2)")] public IWebElement askButton;

        [FindsBy(How = How.CssSelector, Using = "#btnNewOrderCreate")] public IWebElement createButton;

        [FindsBy(How = How.Id, Using = "btnEditOrderExit")] public IWebElement exitButton;

        [FindsBy(How = How.Id, Using = "btnExecuteOrderExit")] public IWebElement exitButtonExecuteOrderForm;

        [FindsBy(How = How.CssSelector, Using = "#btnExecuteOrder")] public IWebElement executeButton;

        [FindsBy(How = How.CssSelector, Using = "#btnEditOrderExecuteMode")] public IWebElement editorderexecuteButton;

        [FindsBy(How = How.CssSelector, Using = ".closeButton")] public IWebElement closeButton;

        [FindsBy(How = How.CssSelector, Using = "#btnEditOrderHold")] public IWebElement holdButton;

        [FindsBy(How = How.CssSelector, Using = "#btnEditOrderEdit")] public IWebElement updateButton;

        [FindsBy(How = How.CssSelector, Using = "#btnEditOrderKill")] public IWebElement killButton;

        [FindsBy(How = How.CssSelector, Using = "#btnEditOrderReinstate")] public IWebElement reinstateButton;

        [FindsBy(How = How.CssSelector, Using = "#btnEditOrderExit")] public IWebElement editorderexitButton;

        // Checkbox

        [FindsBy(How = How.CssSelector, Using = ".co-brokered-checkbox")] public IWebElement cobrokerCheckBox;

        [FindsBy(How = How.Id, Using = "tabular-layout")] public IWebElement MetaDataFields;
        // Methods

        public void SetNewPrice(string price)
        {
            priceDetails.Clear();
            priceDetails.SendKeys(price);
        }

        public void SetNewQuantity(string quantity)
        {
            quantityDetails.Clear();
            quantityDetails.SendKeys(quantity);
        }

        public void SetOrderNotes(string notes)
        {
            if (orderNotes != null)
            {
                orderNotes.Clear();
                orderNotes.Click();
            }
            orderNotes.Click();
        }

        public void SetBroker(string broker)
        {
            if (broker.Equals("*"))
                broker = "Any Broker";

            var brokerSelectElement = Driver.FindElement(By.Id("brokers"));

            WaitForDropdownSpinnerToBeInvisible(brokerSelectElement);

            brokerDropdown.Click();

            brokerDropdownSearch.SendKeys(broker);
            brokerDetails.Click();
        }

        private void WaitForDropdownSpinnerToBeInvisible(IWebElement selectelement)
        {
            //This for some reason does not have an Id.

            var waitCompletedSuccessfully = WaitUntilAttributeNotPresent(selectelement, "disabled");
            if (!waitCompletedSuccessfully)
            {
                Assert.Fail("Timeout reached while waiting for for dropdown to finish loading");
            }
        }

        public void SetPrincipal(string principal)
        {
            WaitForDropdownSpinnerToBeInvisible(PrincipalSelectElement);
            principalDropdown.Click();
            principalDropdownSearch.SendKeys(principal);
            principalDetails.Click();
        }

        public bool IsPrincipalSelectEnabled()
        {
            return PrincipalSelectElement.Enabled;
        }

        public void SelectPrincipal(string principal)
        {
            var principalSelectElement = Driver.FindElement(By.Id("principals"));
            WaitForDropdownSpinnerToBeInvisible(principalSelectElement);
            principalDrop.Click();
            principalDropSearch.SendKeys(principal);
            principalresults.Click();
        }

        public void SelectMetadata1(string metadata1)
        {
            metaData1Drop.Click();
            //    WaitForElementToPresent(metaData1DropdownSearch, 10);
            metaData1DropdownSearch.SendKeys(metadata1);
            metaData1ResultSelected.Click();
        }

        public void SelectMetadata2(string metadata2)
        {
            metaData2Drop.Click();
            //    WaitForElementToPresent(metaData2DropdownSearch, 10);
            metaData2DropdownSearch.SendKeys(metadata2);
            metaData2ResultSelected.Click();
        }

        public void SelectMetadata3(string metadata3)
        {
            metaData3Element.Clear();
            //  metaData3Element.Click();
            WaitForElementToPresent(metaData3Element, 5);
            metaData3Element.SendKeys(metadata3);

            /*     NewOrderPage.metaData3Element.Clear();
                    var scriptToRun = string.Format("$('.large-input.ng-isolate-scope.ng-dirty.ng-valid-parse.ng-touched.ng-invalid[type=\"text\"]').click().val(\"{0}\");", newmetadata3);

                    ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun); */
        }

        public void OpenOrder(double price, string ordertype)
        {
            var newprice = price.ToString("#,##0");
            Console.WriteLine("Formatted Price: " + newprice);

            string scriptToRun;
            if (ordertype.ToUpper() == "BID")
                scriptToRun =
                   string.Format("$('.bidAskStackHeaderContainer').parent().find('.bidPrice:contains({0})').click()", newprice);
            else
                scriptToRun =
                   string.Format("$('.bidAskStackHeaderContainer').parent().find('.askPrice:contains({0})').click()", newprice);

            ((IJavaScriptExecutor)Driver).ExecuteScript(scriptToRun);
        }

        public void SelectFirstPrincipal()
        {
            orderPageHeader.Click();
            principalDropdown.Click();
            principalList[1].Click();
        }

        public void FillInTestNotes(string testnotes)
        {
            // var testnoteselement = MetaDataFields.FindElements(By.TagName("input")).Single(x => x.);
            var testStringRow = Driver.FindElements(By.TagName("row")).First(x => x.Text.Equals("Test String"));
            var testnoteselement = testStringRow.FindElement(By.TagName("input"));

            testnoteselement.Clear();
            testnoteselement.SendKeys(testnotes);
        }
    }
}