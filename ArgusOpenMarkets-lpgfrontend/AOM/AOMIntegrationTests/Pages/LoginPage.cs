﻿using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;

namespace AOMIntegrationTests.Pages
{
    using Helpers;
    using OpenQA.Selenium;

    public class LoginPage : Utilities
    {
        public LoginPage()
        {
            PageFactory.InitElements(Driver, this);
        }

        [FindsBy(How = How.ClassName, Using = "direct-blue")]
        public IWebElement loginbox;

        [FindsBy(How = How.CssSelector, Using = "#username")]
        public IWebElement _username;

        [FindsBy(How = How.CssSelector, Using = "#password")]
        public IWebElement _password;

        [FindsBy(How = How.CssSelector, Using = ".bidAskStacksContainer.ng-scope")]
        public IList<IWebElement> itest;

        [FindsBy(How = How.CssSelector, Using = ".ng-binding")]
        public IWebElement errorText;

        // Button Elements

        [FindsBy(How = How.CssSelector, Using = "#signInBtn")]
        public IWebElement signInButton;

        // Methods

        public bool VerifyOnLoginPage()
        {
            if (Driver.Url.ToLower().Contains("#"))
                return true;
            return false;
        }
    }
}