﻿using AOM.Transport.Events.Products;

namespace AOMIntegrationTests.AomBus
{
    using AOM.App.Domain.Entities;
    using System.Collections.Generic;

    public class ProductSettings : BusRequestor
    {
        public List<ProductSetting> GetAllAomProductSettings()
        {
            var productSettingsResponse =
                _aomBus.Request<GetProductSettingsRequestRpc, GetProductSettingsResponse>(
                    new GetProductSettingsRequestRpc {UserId = ProcessorUserId});

            return productSettingsResponse.ProductSettings;
        }

        internal string GetProductFullName(string partialProductName)
        {
            var allProductSettings = GetAllAomProductSettings();

            foreach (var prod in allProductSettings)
            {
                if (prod.Product.Name.ToUpper().Contains(partialProductName))
                {
                    return prod.Product.Name;
                }
            }

            return string.Empty;
        }
    }
}