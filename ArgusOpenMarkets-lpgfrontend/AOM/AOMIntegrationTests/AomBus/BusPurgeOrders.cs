﻿using System;
using System.Threading;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Events.Orders;
using AOM.Transport.Events.Products;
using AOMIntegrationTests.Helpers;
using Argus.Transport.Infrastructure;
using EasyNetQ.ConnectionString;

namespace AOMIntegrationTests.AomBus
{
    public class BusPurgeOrders
    {
        protected static IBus _aomBus;
        public long OrderId = 0;
        public string GuidNotes;
        private IDisposable OrderEvents;

        public BusPurgeOrders()
        {
            if (_aomBus == null)
            {
                var aomConnection = EnvironmentHelper.GetRabbitBusConnection();
                _aomBus = Argus.Transport.Transport.CreateBus(aomConnection, null, null, null);
            }
        }

        public long ProcessorUserId
        {
            get { return ProcessorUserId; }
        }

        public void PublishPurgeAomProductOrders(int productId)
        {
            var request = new AomProductPurgeOrdersRequest
            {
                ClientSessionInfo =
                    new ClientSessionInfo
                    {
                        UserId = -1
                    },
                MessageAction = MessageAction.Purge,
                Product = new Product { ProductId = productId }
            };

            _aomBus.Publish(request);

            //_aomBus.Dispose();
        }

        public void DisposeAOM()
        {
            _aomBus.Dispose();
        }

        public void PurgeMarketTicker()
        {
            var request = new PurgeMarketTickerRequest
            {
                DaysToKeep = 0
            };
            _aomBus.Publish(request);
        }

        // Orders are Published through RabbitMQ. But no acknowledgment received
        public void PublishOrder(OrderMessage orderMessage, bool subscribeToEvent = true)
        {
            // PublishPurgeAomProductOrders(1);
            if (subscribeToEvent)
            {
                SubscribeToOrderEvent();
            }

            GuidNotes = Guid.NewGuid().ToString();
            orderMessage.notes = GuidNotes;

            var jsonserial = Newtonsoft.Json.JsonConvert.SerializeObject(orderMessage);

            var parsedMessage = new Message()
            {
                MessageType = MessageType.Order,
                MessageAction = MessageAction.Create,
                MessageBody = jsonserial,
                ClientSessionInfo = new ClientSessionInfo()
                {
                    UserId = orderMessage.principalUserId,
                    OrganisationId = orderMessage.principalOrganisationId,
                }
            };

            var ordermessageResponse = new AomOrderMessageResponse()
            {
                Message = parsedMessage,
                //ObjectJson = parsedMessage.MessageBody.ToString()
                ObjectJson = jsonserial
            };

            _aomBus.Publish(ordermessageResponse);

            if (subscribeToEvent)
            {
                WaitForOrderToAppearOnBus(20);
            }
        }

        private void WaitForOrderToAppearOnBus(int timeoutseconds)
        {
            var loopcounter = 0;
            //Poll every 100ms for order to appear
            do
            {
                Thread.Sleep(100);
                loopcounter++;
            } while (OrderId == 0 && loopcounter * 10 < timeoutseconds);
        }

        private void SubscribeToOrderEvent()
        {
            var subscriptionId = "Gary";
            OrderEvents = _aomBus.Subscribe<AomOrderResponse>(subscriptionId, ForwardOrderResponse);
        }

        private void ForwardOrderResponse<T>(T response) where T : AomOrderResponse
        {
            if (response.Order != null)
            {
                if (response.Order.Notes == GuidNotes)
                {
                    OrderId = response.Order.Id;
                    OrderEvents.Dispose();
                }
            }
        }
    }
}