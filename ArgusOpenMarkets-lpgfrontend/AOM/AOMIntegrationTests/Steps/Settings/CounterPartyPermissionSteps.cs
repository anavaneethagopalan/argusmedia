﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AOM.App.Domain.Entities;
using AOMIntegrationTests.AomBus;
using AOMIntegrationTests.Api;
using AOMIntegrationTests.Helpers;
using AOMIntegrationTests.Helpers.CounterPartyPermission;
using AOMIntegrationTests.Pages;
using AOMIntegrationTests.Steps.Authentication;
using AOMIntegrationTests.Steps.context;
using AOMIntegrationTests.Steps.EndToEnd;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.UI;

namespace AOMIntegrationTests.Steps.CounterPartyPermission
{
    [Binding]
    public class CounterPartyPermissionSteps : Utilities
    {
        private readonly MarketTickerExpectedInfo _mtMarketTickerExpectedInfo;
        private readonly UserContext _usercontext;

        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef
        public CounterPartyPermissionSteps(MarketTickerExpectedInfo mtMarketTickerExpectedInfo, UserContext usercontext)
        {
            _mtMarketTickerExpectedInfo = mtMarketTickerExpectedInfo;
            _usercontext = usercontext;
        }

        [Given(@"I have a ""(.*)"" created by ""(.*)"" for product ""(.*)""")]
        [When(@"I have a ""(.*)"" created by ""(.*)"" for product ""(.*)""")]
        public void GivenIHaveAnOrderCreatedByForProduct(string ordertype, string user, string productname)
        {
            var sqlqueries = new SQLQueries();
            var productId = sqlqueries.GetProductIdFromName(productname);

            ScenarioContext.Current["OrderType"] = ordertype.ToUpper();
            ScenarioContext.Current["ProductName"] = productname;

            var orgId = sqlqueries.GetOrgFromUserName(user);
            var userId = sqlqueries.GetUserIdFromUserName(user);
            var tenorid = sqlqueries.GetTenorIdFromProductId(productId);

            Assert.IsTrue(productId != 0, "Product not present in database");

            var busPurgeOrders = new BusPurgeOrders();
            busPurgeOrders.PublishPurgeAomProductOrders(productId);

            //TODO: Wait for orders to clear
            Thread.Sleep(1000);

            var myorder = new OrderMessage()
            {
                productId = productId,
                principalUserId = userId,
                // OrderType = OrderType.Bid,
                OrderType = (OrderType)Enum.Parse(typeof(OrderType), ordertype, true),
                principalOrganisationId = orgId,
                tenor = new TenorId() { Id = tenorid },
            };

            _mtMarketTickerExpectedInfo.Order = ScenarioContext.Current["OrderType"].ToString();
            _mtMarketTickerExpectedInfo.Price = myorder.price;
            _mtMarketTickerExpectedInfo.Status = "NEW";

            ScenarioContext.Current["Price"] = myorder.price;

            busPurgeOrders.PublishOrder(myorder);

            //TODO: wait for orders to appear
        }

        [When(@"I click the execute button for the open order")]
        public void WhenIClickTheExecuteButtonForTheOpenOrder()
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(NewOrderPage.executeButton));
            NewOrderPage.executeButton.Click();
        }

        [When(@"I open the ""(.*)"" created for ""(.*)""")]
        public void WhenIOpenTheOrder(string ordertype, string productname)
        {
            DashboardPage.openExistingOrder(ordertype, productname);
        }
   

        [Then(@"I am logged into ""(.*)"" as user ""(.*)""")]
        [When(@"I am logged into ""(.*)"" as user ""(.*)""")]
        [Given(@"I am logged into ""(.*)"" as user ""(.*)""")]
        public void GivenIAmLoggedIntoAOMAsUser(string AOMorCME, string username)
        {
            //TODO: store in res file or lookup in DB and decode password from encryption field(if possible)
            var password = "Password%";
            if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("LiveSmoke"))
            {
                password = "Argu5Med1a";
            }

            _usercontext.password = password;

            var loginSteps = new LoginSteps(ScenarioContext.Current);

            Table table = new Table("loginbox");
            if (AOMorCME.ToUpper().Equals("AOM"))
            {
                table.AddRow("Argus Open Markets");
            }
            else
            {
                table.AddRow("Argus Open Markets - CME Login");
            }

            if (!Driver.Url.Contains("Dashboard") || username != UserContext.Username)
            {
                UserContext.Username = username;
                loginSteps.GivenINavigateToUrl(table);

                loginSteps.GivenIEnterUsernameLTAndPasswordPassword(username, password);
                loginSteps.WhenIClickOnSignInOnLoginPage();
            }
        /*    else
            {
                loginSteps.ThenILogoutOfAom();
            }*/

            if (IsElementPresent(DashboardPage.toastError))
                DashboardPage.toastError.Click();
            _usercontext.user = username;
            // loginSteps.ThenIShouldBeOnTheDashboardPage(username);
        }

        [When(@"I execute the order")]
        public void WhenIExecuteTheOrder()
        {
            var ordertype = ScenarioContext.Current["OrderType"].ToString();
            var productname = ScenarioContext.Current["ProductName"].ToString();
            var price = ScenarioContext.Current["Price"].ToString();

            var dashboard = new DashboardPage();

            dashboard.openExistingOrder(ordertype, productname);

            WaitForElementToPresent(NewOrderPage.orderPageHeader, 5);
            IsTrue(NewOrderPage.executeButton.Enabled, "Order is not Executable");

            NewOrderPage.executeButton.Click();
            WaitForElementToPresent(DashboardPage.headerIcon, 5);

            _mtMarketTickerExpectedInfo.Order = "DEAL";
        }

        [Given(@"""(.*)"" and ""(.*)"" counterparty permissions for ""(.*)"" buy and sell are set to ""(.*)""")]
        public void GivenAndCounterpartyPermissionsForBuyAndSellAreSetTo(string user2, string user1, string productName, string permissionsforbuyandSell)
        {
            var sqlqueries = new SQLQueries();
            var productId = sqlqueries.GetProductIdFromName(productName);
            var ourOrgId = sqlqueries.GetOrgFromUserName(user2);
            var theirOrgId = sqlqueries.GetOrgFromUserName(user1);

            //var buyorSellEnum = BuyOrSell.Buy; //(BuyOrSell)Enum.Parse(typeof(BuyOrSell), buy, true);
            var allowOrDenyEnum = (AllowOrDeny)Enum.Parse(typeof(AllowOrDeny), permissionsforbuyandSell, true);

            var counterpartyupdate = new CounterPartyPermissionsApi(user2, "Password%");

            Assert.IsTrue(counterpartyupdate.UpdatePermissions(productId, allowOrDenyEnum, BuyOrSell.Buy, ourOrgId, theirOrgId), "Update to permissons was not successfull");
            Assert.IsTrue(counterpartyupdate.UpdatePermissions(productId, allowOrDenyEnum, BuyOrSell.Sell, ourOrgId, theirOrgId), "Update to permissons was not successfull");

            //Do the opposite now

            ourOrgId = sqlqueries.GetOrgFromUserName(user1);
            theirOrgId = sqlqueries.GetOrgFromUserName(user2);
            var counterpartyupdate2 = new CounterPartyPermissionsApi(user1, "Password%");
            Assert.IsTrue(counterpartyupdate2.UpdatePermissions(productId, allowOrDenyEnum, BuyOrSell.Buy, ourOrgId, theirOrgId), "Update to permissons was not successfull");
            Assert.IsTrue(counterpartyupdate2.UpdatePermissions(productId, allowOrDenyEnum, BuyOrSell.Sell, ourOrgId, theirOrgId), "Update to permissons was not successfull");
        }

        [Given(@"the counterparty permissions are set from ""(.*)"" to ""(.*)"" for ""(.*)"", ""(.*)"" to ""(.*)""")]
        [When(@"I change the Counter Party permission from ""(.*)"" to ""(.*)"" for ""(.*)"", ""(.*)"" to ""(.*)""")]
        public void GivenIChangeTheCounterPartyPermissionForTo(string user1, string user2, string buyOrSell, string productName, string allowOrDeny)
        {
            var sqlqueries = new SQLQueries();

            var productId = sqlqueries.GetProductIdFromName(productName);
            var theirOrgId = sqlqueries.GetOrgFromUserName(user2);
            var ourOrgId = sqlqueries.GetOrgFromUserName(user1);
            var buyorSellEnum = (BuyOrSell)Enum.Parse(typeof(BuyOrSell), buyOrSell, true);
            var allowOrDenyEnum = (AllowOrDeny)Enum.Parse(typeof(AllowOrDeny), allowOrDeny, true);

            var counterpartyupdate = new CounterPartyPermissionsApi(user1, "Password%");

            Assert.IsTrue(counterpartyupdate.UpdatePermissions(productId, allowOrDenyEnum, buyorSellEnum, ourOrgId, theirOrgId), "Update to permissons was not successfull");
        }

        [Then(@"I should see a toaster message that contains ""(.*)""")]
        public void ThenIShouldSeeAToasterMessageThatContains(string toastermessage)
        {
            Assert.IsTrue(DashboardPage.toastError.Text.Contains(toastermessage),
                "toaster message says {0} which does not contain expected of {1}",
                DashboardPage.toastError.Text,
                toastermessage);
            DashboardPage.toastError.Click();
        }

        [Then(@"the order should appear in market ticker and be ""(.*)"" in colour")]
        public void ThenTheOrderShouldAppearInMarketTickerWithTheCorrectColour(string colour)
        {
            var dealTwoWay = new DealsTwoWaySteps(ScenarioContext.Current, _mtMarketTickerExpectedInfo);

            dealTwoWay.ThenTheOrderWithShouldAppearInMarketTicker(_mtMarketTickerExpectedInfo.Order, _mtMarketTickerExpectedInfo.Status, _mtMarketTickerExpectedInfo.Price);
            dealTwoWay.ThenTheColorOfTheMarketTickerItemShouldBeOrange(colour);
        }

        [Then(@"I am ""(.*)"" to execute an order")]
        public void ThenIAmToExecuteAnOrder(string AllowedOrDenied)
        {
            Thread.Sleep(500);
            switch (AllowedOrDenied.ToLower())
            {
                case "allowed":
                    new DealsThreeWaySteps(ScenarioContext.Current, _mtMarketTickerExpectedInfo).ThenTheOrderCanBeExecutedBy(_usercontext.user);
                    NewOrderPage.executeButton.Click();
                    break;

                case "denied":
                    new DealsThreeWaySteps(ScenarioContext.Current, _mtMarketTickerExpectedInfo).ThenTheOrderCannotBeExecutedBy(_usercontext.user);
                    NewOrderPage.exitButtonExecuteOrderForm.Click();
                    break;

                default:
                    Assert.Fail("Parameter can only be Allowed or Denied and is {0}", AllowedOrDenied);
                    break;
            }
            if (IsElementPresent(DashboardPage.toastError))
                Assert.Fail("Toaster Error Message appeared");
        }
    }
}