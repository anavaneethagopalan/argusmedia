﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using AOM.App.Domain.Entities;
using AOMIntegrationTests.Api;
using AOMIntegrationTests.Helpers;
using AOMIntegrationTests.Helpers.CounterPartyPermission;
using AOMIntegrationTests.Pages;
using AOMIntegrationTests.Steps.context;
using AOMIntegrationTests.Steps.EndToEnd;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.UI;

namespace AOMIntegrationTests.Steps.BrokerPermissions
{
    [Binding]
    public sealed class BrokerPermissionSteps : Utilities
    {
        private UserContext _userContext;
        private MarketTickerExpectedInfo _mtMarketTickerExpectedInfo;

        public BrokerPermissionSteps(UserContext userContext, MarketTickerExpectedInfo mtMarketTickerExpectedInfo)
        {
            _userContext = userContext;
            _mtMarketTickerExpectedInfo = mtMarketTickerExpectedInfo;
        }

        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef

        [Given(@"""(.*)"" and ""(.*)"" broker permissions for ""(.*)"" set to ""(.*)""")]
        public void GivenAndBrokerPermissionsForSetTo(string trader, string broker, string productName, string permission)
        {
            var sqlqueries = new SQLQueries();
            var productId = sqlqueries.GetProductIdFromName(productName);
            var ourOrgId = sqlqueries.GetOrgFromUserName(broker);
            var theirOrgId = sqlqueries.GetOrgFromUserName(trader);

            var brokerupdate = new BrokerPermissionsApi(broker, "Password%");

            Assert.IsTrue(brokerupdate.UpdatePermissions(productId, AllowOrDeny.Allow, BuyOrSell.Buy, ourOrgId, theirOrgId), "Update to permissons was not successfull");
            Assert.IsTrue(brokerupdate.UpdatePermissions(productId, AllowOrDeny.Allow, BuyOrSell.Sell, ourOrgId, theirOrgId), "Update to permissons was not successfull");

            //Do the opposite now
            ourOrgId = sqlqueries.GetOrgFromUserName(trader);
            theirOrgId = sqlqueries.GetOrgFromUserName(broker);
            var brokerperms = new BrokerPermissionsApi(trader, "Password%");
            Assert.IsTrue(brokerperms.UpdatePermissions(productId, AllowOrDeny.Allow, BuyOrSell.Buy, ourOrgId, theirOrgId), "Update to permissons was not successfull");
            Assert.IsTrue(brokerperms.UpdatePermissions(productId, AllowOrDeny.Allow, BuyOrSell.Sell, ourOrgId, theirOrgId), "Update to permissons was not successfull");
        }

        [Given(@"the broker permissions are set as ""(.*)"" to ""(.*)"" for, ""(.*)"" to ""(.*)""")]
        [When(@"I change the Broker permission from ""(.*)"" to ""(.*)"" for ""(.*)"" to ""(.*)""")]
        public void GivenTheBrokerPermissionsAreSetAsToForTo(string trader, string broker, string productName, string permission)
        {
            var sqlqueries = new SQLQueries();

            var productId = sqlqueries.GetProductIdFromName(productName);
            var theirOrgId = sqlqueries.GetOrgFromUserName(broker);
            var ourOrgId = sqlqueries.GetOrgFromUserName(trader);
            var allowOrDenyEnum = (AllowOrDeny)Enum.Parse(typeof(AllowOrDeny), permission, true);

            var brokerperms = new BrokerPermissionsApi(trader, "Password%");

            Assert.IsTrue(brokerperms.UpdatePermissions(productId, allowOrDenyEnum, BuyOrSell.Buy, ourOrgId, theirOrgId), "Update to permissons was not successfull");
            Assert.IsTrue(brokerperms.UpdatePermissions(productId, allowOrDenyEnum, BuyOrSell.Sell, ourOrgId, theirOrgId), "Update to permissons was not successfull");
        }

        [Then(@"I am ""(.*)"" to execute an order selecting first Trader in the list as the principle")]
        public void ThenIAmToExecuteAnOrderSelectingFirstTraderAsThePrinciple(string AllowedOrDenied)
        {
            var orderpage = new NewOrderPage();

            ScenarioContext.Current["OrderType"] = _mtMarketTickerExpectedInfo.Order;

            Thread.Sleep(500);
            switch (AllowedOrDenied.ToLower())
            {
                case "allowed":
                    //TODO: Write an select first from dropdown
                    ExecuteOrderBySelectingFirstPrincipleinList();
                    orderpage.executeButton.Click();
                    break;

                case "denied":
                    new DealsTwoWaySteps(scenarioContext, _mtMarketTickerExpectedInfo).ThenTheColorOfTheOrderShouldBe("DOVEGREY");
                    //orderpage.OpenOrder((double) _mtMarketTickerExpectedInfo.Price, _mtMarketTickerExpectedInfo.Order);
                    DashboardPage.openExistingOrder(_mtMarketTickerExpectedInfo.Order, ScenarioContext.Current["ProductName"].ToString());
                    Assert.IsFalse(orderpage.IsPrincipalSelectEnabled(), "Broker is allowed to select the principle Trader");

                    new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(
                        ExpectedConditions.ElementToBeClickable(orderpage.exitButtonExecuteOrderForm));
                    orderpage.exitButtonExecuteOrderForm.Click();
                    break;

                default:
                    Assert.Fail("Parameter can only be Allowed or Denied and is {0}", AllowedOrDenied);
                    break;
            }
            if (IsElementPresent(DashboardPage.toastError))
                Assert.Fail("Toaster Error Message appeared");
        }

        private void ExecuteOrderBySelectingFirstPrincipleinList()
        {
            var dashboardpage = new DashboardPage();
            dashboardpage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(new NewOrderPage().closeButton, 10);
            new NewOrderPage().SelectFirstPrincipal();
            IsTrue(IsElementEnabled(new NewOrderPage().executeButton), "Order is not Executable ");
        }
    }
}