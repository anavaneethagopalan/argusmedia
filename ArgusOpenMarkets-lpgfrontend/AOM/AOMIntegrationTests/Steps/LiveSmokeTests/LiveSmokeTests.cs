﻿using System.Linq;
using AOMIntegrationTests.Pages;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AOMIntegrationTests.Steps.LiveSmokeTests
{
    [Binding]
    public sealed class LiveSmokeTests
    {
        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef
        [Then(@"the market ticker contains more than (.*) item")]
        public void ThenTheMarketTickerContainsMoreThanItem(int count)
        {
            Assert.IsTrue(new MarketTickerPage().marketTickerItems.Count() > count);
        }

        [Then(@"I see the assessment window")]
        public void ThenISeeTheAssessmentWindow()
        {
            var productAssessemnt = new AssessmentPage().productAssessment.Text;

            Assert.IsTrue(productAssessemnt.Contains("TEST"));

        }

        [Then(@"the news items contains more than (.*) item")]
        public void ThenTheNewsItemsContainsMoreThanItem(int count)
        {

            Assert.IsTrue(new DashboardPage().GetNewsItemsCount() > count);
        }

        [Then(@"Bid/Ask stack contains (.*) products")]
        public void ThenBidAskStackContainsProducts(int count)
        {
            Assert.AreEqual(new DashboardPage().GetBidAskStackHeaderCounter(), count);
        }

        [When(@"I click on the Order type (.*) for the Product (.*) for live tests")]
        public void WhenIClickOnTheOrderTypeForTheProductForLiveTests(string ordertype, string productname)
        {
            new DashboardPage().OpenOrderDialogForOrderTypeAndProduct(ordertype, productname);
           ScenarioContext.Current["ProductName"] = productname;
            ScenarioContext.Current["OrderType"] = ordertype;
        }

        [Then(@"I fill in the test notes field with ""(.*)""")]
        public void ThenIFillInTheTestNotesFieldWith(string testnotes)
        {
            new NewOrderPage().FillInTestNotes(testnotes);
        }

    }
}
