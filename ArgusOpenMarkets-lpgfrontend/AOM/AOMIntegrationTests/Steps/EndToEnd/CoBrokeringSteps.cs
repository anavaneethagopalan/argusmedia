﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMIntegrationTests.Helpers;
using TechTalk.SpecFlow;
using System.Configuration;
using AOMIntegrationTests.Steps.context;

namespace AOMIntegrationTests.Steps
{
    [Binding]
    public class CoBrokeringSteps : Utilities
    {
        private readonly MarketTickerExpectedInfo _mtExpectedInfo;

        public CoBrokeringSteps(ScenarioContext scenarioContext, MarketTickerExpectedInfo mtExpectedInfo )
        {
            _mtExpectedInfo = mtExpectedInfo;

            if (scenarioContext == null)
            {
                Console.WriteLine("CoBrokering Steps Scenario Context is null");
                throw new ArgumentNullException(("scenarioContext"));
            }

            this.scenarioContext = scenarioContext;
        }

        [Then(@"I enable co-brokering flag")]
        public void ThenIEnableCo_BrokeringFlag()
        {
            if(!NewOrderPage.cobrokerCheckBox.Selected)
                NewOrderPage.cobrokerCheckBox.Click();
            else
                Console.WriteLine("Cobrokering checkbox is already enabled");

        }

        [Then(@"I enable co-brokering flag as (.*)")]
        public void ThenIEnableCo_BrokeringFlagAs(string user)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
           WaitForElementToPresent(NewOrderPage.closeButton,10);

            if (!NewOrderPage.cobrokerCheckBox.Selected)
            {
                NewOrderPage.orderPageHeader.Click();
                NewOrderPage.cobrokerCheckBox.Click();
            }
            else
                Console.WriteLine("Cobrokering checkbox is already enabled");

        }


        [Then(@"I disable co-brokering flag")]
        public void ThenIDisableCo_BrokeringFlag()
        {
            if (NewOrderPage.cobrokerCheckBox.Selected)
            {
                NewOrderPage.orderPageHeader.Click();
                NewOrderPage.cobrokerCheckBox.Click();
            }
            else
                Console.WriteLine("Cobrokering checkbox is already disabled");

        }

        [Then(@"I disable co-brokering flag as (.*)")]
        public void ThenIDisableCo_BrokeringFlagas(string user)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);
            NewOrderPage.orderPageHeader.Click();
            WaitElementTobeClickable(NewOrderPage.cobrokerCheckBox, 10);

            if (NewOrderPage.cobrokerCheckBox.Selected)

                NewOrderPage.cobrokerCheckBox.Click();
            else
                Console.WriteLine("Cobrokering checkbox is already disabled");

        }

        [Then(@"I update the order")]
        public void ThenUpdateTheOrder()
        {
            WaitElementTobeClickable(NewOrderPage.updateButton, 10);
            NewOrderPage.updateButton.Click();
        }


        [Then(@"the broker aggresses the order naming (.*) as principal")]
        public void ThenTheBrokerAggressesTheOrderNamingAutoTestATAsPrincipal(string principal)
        {

            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());

            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            NewOrderPage.SetPrincipal(principal);
            WaitElementTobeClickable(NewOrderPage.executeButton, 10);
        }

        [Then(@"the hoverbox shows CoBrokering Enabled as (.*)")]
        public void ThenTheHoverboxShowsCoBrokeringEnabledAsYes(string cobrokerflag)
        {
            if (ConfigurationManager.AppSettings["Browser"] != "IE")
            {
                DashboardPage.hoverAroundOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
                WaitForElementToPresent(DashboardPage.hoverbox, 20);

                var tooltipDataTable = DashboardPage.GetHoverelements();
                string actual = tooltipDataTable.Rows[0]["Co-brokering enabled"].ToString().ToUpper();
                string expected = cobrokerflag.ToUpper();

                AreEqualMsg(expected, actual, "Co-Brokering status are not equal: " + actual + " ---- " + expected);

                DashboardPage.MoveOutofOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            }
            else
                Console.WriteLine("Hoverbox not functional in IE");

        }
        [Then(@"cobrokering icon should be displayed in market ticker")]
        public void ThenCobrokeringIconShouldBeDisplayedInMarketticker()
        {       
          var cobrokeringIconDisplayed = MarketTickerPage.CoBrokeringIconDisplayed(_mtExpectedInfo.Order, _mtExpectedInfo.Status, _mtExpectedInfo.Price.ToString());

          IsTrue(cobrokeringIconDisplayed,"Co-brokering Icon is not displayed");
        }


        [Then(@"the cobrokering icon should not be displayed in market ticker")]
        public void ThenTheCobrokeringIconShouldNotBeDisplayedInMarketTicker()
        {
            var cobrokeringIconDisplayed = MarketTickerPage.CoBrokeringIconDisplayed(_mtExpectedInfo.Order, _mtExpectedInfo.Status, _mtExpectedInfo.Price.ToString());

            IsFalse(cobrokeringIconDisplayed, "Co-brokering Icon is displayed");
        }


        [Then(@"the (.*) cannot execute the order")]
        public void ThenobserverCannotExecuteTheOrder(string observer)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            IsFalse(NewOrderPage.executeButton.Enabled, "Order is Executable by " +observer);
        }


        [Then(@"I hold the order as (.*)")]
        public void ThenIHoldTheOrderAsAB(string user)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            WaitElementTobeClickable(NewOrderPage.holdButton, 10);
            NewOrderPage.holdButton.Click();            
        }

        [Then(@"I reinstate the order as (.*)")]
        public void ThenIReinstateTheOrderAsALT(string user)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);


            WaitElementTobeClickable(NewOrderPage.reinstateButton, 10);
            NewOrderPage.reinstateButton.Click();
            
            
        }

        [Then(@"cobrokering checkbox should not be available")]
        public void ThenCobrokeringCheckboxShouldNotBeAvailable()
        {
            WaitForPageToLoad(5);
            IsFalse(NewOrderPage.cobrokerCheckBox.Displayed,"Cobrokering checkbox is available");
        }


    }
}
