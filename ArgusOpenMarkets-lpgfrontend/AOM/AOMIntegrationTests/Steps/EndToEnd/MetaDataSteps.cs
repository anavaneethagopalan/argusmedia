﻿using System;
using System.Globalization;
using AOMIntegrationTests.Helpers;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AOMIntegrationTests.Steps.EndToEnd
{
    [Binding]
    public  class MetaDataSteps : Utilities
    {
        public MetaDataSteps(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null)
            {
                Console.WriteLine("MetaData Steps Scenario Context is null");
                throw new ArgumentNullException(("scenarioContext"));
            }

            this.scenarioContext = scenarioContext;
        }

        [Then(@"I populate the metadta fields with (.*), (.*), (.*)")]
        public void ThenIPopulateTheMetadtaFieldsWith(string metadata1, string metadata2, string metadata3)
        {
           NewOrderPage.SelectMetadata1(metadata1);
           NewOrderPage.SelectMetadata2(metadata2);         
            NewOrderPage.SelectMetadata3(metadata3);


        }

        [Then(@"the hoverbox shows metadata as (.*), (.*), (.*)")]
        public void ThenTheHoverboxShowsMetadataAs(string metadata1, string metadata2, string metadata3)
        {
            DashboardPage.hoverAroundOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(DashboardPage.hoverbox, 5);
           

            AreEqualMsg(metadata1, DashboardPage.hoverboxMetadata1.Text, "Ports are not equal: " + DashboardPage.hoverboxMetadata1.Text + " ---- " + metadata1);

            AreEqualMsg(metadata2, DashboardPage.hoverboxMetadata2.Text, "Grades are not equal: " + DashboardPage.hoverboxMetadata2.Text + " ---- " + metadata2);

            AreEqualMsg(metadata3, DashboardPage.hoverboxMetadata3.Text, "Manual are not equal: " + DashboardPage.hoverboxMetadata3.Text + " ---- " + metadata3);

            DashboardPage.MoveOutofOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());

        }

        [Then(@"the metadata details (.*), (.*), (.*), (.*), (.*), (.*) should appear in market ticker")]
        public void ThenTheMetadataDetailsShouldAppearInMarketTicker(string price, string metadata1, string metadata2, string metadata3, string status, string ordertype)
        {
            WaitForListElementToPresent(MarketTickerPage.marketTicker,5);
           
            var marketTickerItem = DealsTwoWaySteps.WaitForMarketTickerToUpdate(ordertype, status, Convert.ToDecimal(price), metadata1 + ", " + metadata2 + ", " + metadata3);

            if (marketTickerItem == null)
            {
                Assert.Fail("Can't find marketTickerItem after 20 tries");
            }

            IsTrue(marketTickerItem.MarketTickerItem.Contains(price.ToString((CultureInfo.InvariantCulture))), "Price is visible: " + price);
            IsTrue(marketTickerItem.MarketTickerItem.Contains(metadata1), "Metadata1 is not visible: " + metadata1);
            IsTrue(marketTickerItem.MarketTickerItem.Contains(metadata2), "Metadata2 is not visible: " + metadata2);
            IsTrue(marketTickerItem.MarketTickerItem.Contains(metadata3), "Metadata3 is not visible: " + metadata3);
            IsTrue(marketTickerItem.MarketTickerItem.Contains(status), "Status is not visible: " + status);
        }


        [Then(@"I update the order details with (.*), (.*), (.*), (.*)")]
        public void ThenIUpdateTheOrderDetailsWithTanjungLangsatPenGradeUpdatedMetdataUPDATED(string newprice, string newmetadata1, string newmetadata2, string newmetadata3)
        {
           NewOrderPage.SetNewPrice(newprice);
           NewOrderPage.SelectMetadata1(newmetadata1);
           NewOrderPage.SelectMetadata2(newmetadata2);
            //TODO: Update metadata 3
            WaitForPageToLoad(5);
       
        }

        [Then(@"I click on order Update Button")]
        public void ThenIClickOnOrderUpdateButton()
        {
           WaitForElementToPresent(NewOrderPage.updateButton,5);
            NewOrderPage.updateButton.Click();
        }

        [Then(@"I open the existing order with (.*) and (.*)")]
        public void ThenIOpenTheExistingOrderWith(double price, string ordertype)
        {
          DashboardPage.openExistingOrder(ordertype, ScenarioContext.Current["ProductName"].ToString());
        
           WaitForElementToPresent(NewOrderPage.orderPageHeader,5);
           NewOrderPage.orderPageHeader.Click();

        }

        [Then(@"I click on order hold Button")]
        public void ThenIClickOnOrderHoldButton()
        {
            WaitForElementToPresent(NewOrderPage.holdButton,5);
            NewOrderPage.holdButton.Click();
        }

        [Then(@"I click on order reinstate Button")]
        public void ThenIClickOnOrderReinstateButton()
        {
            WaitForElementToPresent(NewOrderPage.reinstateButton, 5);
            NewOrderPage.reinstateButton.Click();
        }

        [Then(@"I click on order kill Button")]
        public void ThenIClickOnOrderKillButton()
        {
            WaitForElementToPresent(NewOrderPage.killButton, 5);
            NewOrderPage.killButton.Click();
        }

        [Then(@"the plus deal metadata details with (.*), (.*), (.*), (.*), (.*), (.*) should be displayed in market ticker")]
        public void ThenThePlusDealMetadataDetailsWithDEALPENDINGKemamanPenGradeAutomatedMetadataShouldNotBeVisibleInMarketTickerFor(string ordertype, string status, string price, string metadata1, string metadata2, string metadata3)
        {
            WaitForElementToPresent(DashboardPage.bidGridUpButton, 5);

            var marketTickerItem = DealsTwoWaySteps.WaitForMarketTickerToUpdate(ordertype, status, Convert.ToDecimal(price), metadata1 +", " + metadata2 + ", " + metadata3);

            if (marketTickerItem == null)
            {
                Assert.Fail("Can't find marketTickerItem after 20 tries");
            }

            WaitForListElementToPresent(MarketTickerPage.marketTicker, 5);
            var marketTickerItems = MarketTickerPage.GetMarketTickerItems();

            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(price.ToString((CultureInfo.InvariantCulture))), "Price is incorrect: " + price);
            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(metadata1), "Metadata1 is incorrect: " + metadata1);
            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(metadata2), "Metadata2 is incorrect: " + metadata2);
            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(metadata3), "Metadata3 is incorrect: " + metadata3);
            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(status), "Status is incorrect: " + status);
            IsTrue(marketTickerItems[0].BidOrAsk.Contains(ordertype), "Order Type is incorrect: " + ordertype);
        }

        [Then(@"the metadata details appears read only")]
        public void ThenTheMetadataDetailsAppearsReadOnly()
        {

            AreEqualMsg("true", NewOrderPage.metaData1ReadOnly.GetAttribute("readonly"), "Metadata 1 is not read only");
            AreEqualMsg("true", NewOrderPage.metaData2ReadOnly.GetAttribute("readonly"), "Metadata 2 is not read only");
            AreEqualMsg("true", NewOrderPage.metaData3ReadOnly.GetAttribute("readonly"), "Metadata 3 is not read only");

            NewOrderPage.closeButton.Click();
        }

        [Then(@"metadata fields should not be visible")]
        public void ThenMetadataFieldsShouldNotBeVisible()
        {
            IsFalse(IsElementPresent(NewOrderPage.metaData1Drop), "Metadata1 is visible");
            IsFalse(IsElementPresent(NewOrderPage.metaData2Drop), "Metadata2 is visible");
            IsFalse(IsElementPresent(NewOrderPage.metaData3Element), "Metadata3 is visible");
        }

        [Then(@"I verify create button is not enabled")]
        public void ThenIVerifyCreateButtonIsNotEnabled()
        {
            IsFalse(NewOrderPage.createButton.Enabled, "Create Button is enabled");
        }

        [Then(@"I verify that metadata fields displays an error message")]
        public void ThenIVerifyThatMetadataFieldsDisplaysAnErrorMessage(Table table)
        {
           

            string expected = table.Rows[0]["message"];

            string actual = NewOrderPage.metaData3ErrorMsg.Text;
            AreEqualMsg(expected,actual,"Metadata 3 is not mandatory");
        }

        [Then(@"the Free Form Deal displays (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)")]
        public void ThenTheFreeFormDealDisplaysBuyerSellerBrokerKemamanPenGradeAutomatedMetadata(string price, string quantity, string buyer, string seller, string broker, string metadata1, string metadata2, string metadata3)
        {

            Console.WriteLine("Free Form: " + PlusDealPage.GetFreeFormDealText());

            var freeformdealText = PlusDealPage.GetFreeFormDealText();

            IsTrue(freeformdealText.Contains(price.ToString(CultureInfo.InvariantCulture)), "Price is not visible: " + price);
            IsTrue(freeformdealText.Contains(quantity), "Quantity is not visible: " + quantity);
            IsTrue(freeformdealText.Contains(buyer), "Buyer is not visible: " + buyer);
            IsTrue(freeformdealText.Contains(seller), "Seller is not visible: " + seller);
            IsTrue(freeformdealText.Contains(metadata1), "Metadata1 is not visible: " + metadata1);
            IsTrue(freeformdealText.Contains(metadata2), "Metadata2 is not visible: " + metadata2);
            IsTrue(freeformdealText.Contains(metadata3), "Metadata3 is not visible: " + metadata3); 
        }

    }
}
