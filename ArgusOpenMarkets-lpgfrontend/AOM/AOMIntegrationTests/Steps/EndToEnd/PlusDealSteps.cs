﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using AOMIntegrationTests.Helpers;
using AOMIntegrationTests.Steps.context;
using AOMIntegrationTests.Steps.EndToEnd;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace AOMIntegrationTests.Steps
{
    [Binding]
    public sealed class PlusDealSteps : Utilities
    {
        private readonly MarketTickerExpectedInfo _marketTickerExpectedInfo;

        public PlusDealSteps(MarketTickerExpectedInfo marketTickerExpectedInfo)
        {
            _marketTickerExpectedInfo = marketTickerExpectedInfo;
        }

        [When(@"I click on the Plus Deal")]
        public void WhenIClickOnThePlusDeal()
        {
            PlusDealPage.plusDealButton.Click();
            WaitForElementToPresent(PlusDealPage.plusDealHeader, 7);
        }


        [Then(@"I should be taken to the Plus Deal page")]
        public void ThenIShouldBeTakenToThePlusDealPage(Table table)
        {
            WaitForElementToPresent(PlusDealPage.plusDealHeader, 10);

            string expected = table.Rows[0]["title"];
            string actual = PlusDealPage.plusDealHeader.Text;

            AreEqualMsg(expected, actual, "You are not External Deal Page " + actual);
        }

        [Then(@"I should be taken to Verify Deal Page")]
        public void ThenIShouldBeTakenToTheVerifyDealPage(Table table)
        {

            WaitForElementToPresent(PlusDealPage.verifyDealHeader, 10);

            string expected = table.Rows[0]["title"];
            string actual = PlusDealPage.verifyDealHeader.Text;

            AreEqualMsg(expected, actual, "You are not Verify Deal Page " + actual);
        }

        [Then(@"I should be taken to Void Deal Page")]
        public void ThenIShouldBeTakenToVoidDealPage(Table table)
        {
            WaitForElementToPresent(PlusDealPage.voidDealHeader, 10);
            string expected = table.Rows[0]["title"];
            string actual = PlusDealPage.voidDealHeader.Text;

            AreEqualMsg(expected, actual, "You are not Void Deal Page " + actual);
        }


        [Then(@"the update button should not be present")]
        public void ThenTheUpdateButtonShouldNotBePresent()
        {
            IsFalse(IsElementPresent(PlusDealPage.plusDealUpdateButton), "Update Button is Visible");
        }



        [Then(@"I fill in the Deal details with (.*) ,(.*) ,(.*) ,(.*) ,(.*), (.*), (.*), (.*)")]
        public void ThenIFillInTheDealDetailsWith(string contract, string price, string quantity, string location, string buyer, string seller, string broker, string notes)
        {
            PlusDealPage.SelectContract(contract);
            PlusDealPage.plusDealPrice.SendKeys(price);
            if (contract.ToUpper() == "AUTO BIOFUEL" || contract.ToUpper() == "AUTO OTHER")
                PlusDealPage.plusDealContractName.SendKeys(contract.ToLower());

            if (contract.ToUpper() == "AUTO BIOFUEL")
                PlusDealPage.plusDealOptionalPriceDetail.SendKeys(price + 1);

            PlusDealPage.plusDealQuantity.SendKeys(quantity);
            PlusDealPage.plusDealLocation.SendKeys(location);
            PlusDealPage.plusDealBuyer.SendKeys(buyer);
            PlusDealPage.plusDealSeller.SendKeys(seller);
            PlusDealPage.plusDealBroker.SendKeys(broker);
            PlusDealPage.plusDealNotes.SendKeys(notes);
            _marketTickerExpectedInfo.Order = "+ Deal";
            _marketTickerExpectedInfo.Price = Convert.ToDecimal(price);
            _marketTickerExpectedInfo.Text = notes;



        }

        [When(@"I click on create Deal")]
        public void WhenIClickOnCreateDeal()
        {
            WaitForElementToPresent(PlusDealPage.plusDealCreateButton, 10);
            PlusDealPage.plusDealCreateButton.Click();
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
        }

        [When(@"I click on create Deal as Pending")]
        public void WhenIClickOnCreateDealAsPending()
        {
            WaitForElementToPresent(PlusDealPage.createAsPendingButton, 10);
            PlusDealPage.createAsPendingButton.Click();

            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
        }

        [When(@"I click on create Deal as Verified")]
        public void WhenIClickOnCreateDealAsVerified()
        {
            WaitForElementToPresent(PlusDealPage.createAsVerifiedButton, 10);
            PlusDealPage.createAsVerifiedButton.Click();

            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);

            _marketTickerExpectedInfo.Status = "";
        }


        [Then(@"the order with (.*),(.*),(.*) should not be visible in market ticker for (.*)")]
        public void ThenTheOrderWithShouldNotBeVisibleInMarketTickerFor(string ordertype, string price, string status, string user)
        {
            WaitForElementToPresent(DashboardPage.bidGridUpButton, 5);
            DashboardPage.bidGridUpButton.Click();
            DashboardPage.bidGridDownButton.Click();

            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 5);
            var marketTickerItems = MarketTickerPage.GetMarketTickerItems();

            IsFalse(marketTickerItems[0].MarketTickerItem.Contains(price.ToString((CultureInfo.InvariantCulture))), "Price is visible: " + price);
            IsFalse(marketTickerItems[0].MarketTickerItem.Contains(status), "Status is visible: " + status);
            // IsFalse(marketTickerItems[0].BidOrAsk.Contains(ordertype), "Order Type is visible: " + ordertype);
        }

        [Then(@"the order with (.*), (.*) should appear in market ticker")]
        public void ThenTheOrderShouldAppearInMarketTicker(string ordertype, string price)
        {
            WaitForElementToPresent(DashboardPage.bidGridUpButton, 10);
            WaitForModalBackdropToDisappear();
            DashboardPage.bidGridUpButton.Click();
            DashboardPage.bidGridDownButton.Click();

            var marketTickerItem = DealsTwoWaySteps.WaitForMarketTickerToUpdate(_marketTickerExpectedInfo.Order, _marketTickerExpectedInfo.Status,
            _marketTickerExpectedInfo.Price, _marketTickerExpectedInfo.Text);

            IsTrue(marketTickerItem.MarketTickerItem.Contains(price.ToString((CultureInfo.InvariantCulture))), "Price is not visible: " + price);
            IsTrue(marketTickerItem.BidOrAsk.Contains(ordertype), "Order Type is not visible: " + ordertype);
        }

        [Then(@"I open up the existing info")]
        [Then(@"I open up the existing deal")]
        public void ThenIOpenUpTheExistingDeal()
        {
            MarketTickerPage.marketTickercontent.First().Click();
            //  WaitForElementToPresent(PlusDealPage.verifyDealHeader, 10);
        }

        [Then(@"I change the product (.*) field")]
        public void ThenIChangeTheProductField(string newproduct)
        {
            PlusDealPage.SelectContract(newproduct);
        }

        [Then(@"I update the Notes (.*) field")]
        public void ThenIUpdateTheNotesAutomationExternalDealUpdatedField(string newnotes)
        {
            PlusDealPage.plusDealNotes.Clear();
            PlusDealPage.plusDealNotes.SendKeys(newnotes);
        }

        [Then(@"I click on Update Button")]
        public void ThenIClickOnUpdateButton()
        {
            WaitForElementToPresent(PlusDealPage.plusDealUpdateButton, 5);
            PlusDealPage.plusDealUpdateButton.Click();
            WaitForModalBackdropToDisappear();
        }

        [Then(@"the updated order with (.*), (.*), (.*) should appear in market ticker")]
        public void ThenTheUpdatedOrderWithUpdatedShouldAppearInMarketTicker(string ordertype, string newproduct, string newnotes)
        {
            WaitForElementToPresent(DashboardPage.bidGridUpButton, 5);
            DashboardPage.bidGridUpButton.Click();
            DashboardPage.bidGridDownButton.Click();

            var marketTickerItems = MarketTickerPage.GetMarketTickerItems();

            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(newproduct), "Updated Product is not visible: " + newproduct);
            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(newproduct), "Updated Notes is not visible: " + newnotes);
            IsTrue(marketTickerItems[0].BidOrAsk.Contains(ordertype), "Order Type is not visible: " + ordertype);
        }

        [When(@"I Void the product")]
        public void WhenIVoidTheProduct()
        {
            PlusDealPage.plusDealVoidButton.Click();
            WaitForElementToPresent(PlusDealPage.voidSelectionReason, 5);
        }

        [Then(@"i should be taken to Void Reason selection screen")]
        public void ThenIShouldBeTakenToVoidReasonSelectionScreen(Table table)
        {
            string expected = table.Rows[0]["title"];
            string actual = PlusDealPage.voidSelectionReason.Text;

            AreEqualMsg(expected, actual, "You are not Enter Void Reason Page " + actual);
        }

        [Then(@"I enter the void reason (.*)")]
        public void ThenIEnterTheVoidReason(string reason)
        {

            PlusDealPage.SelectVoidReason(reason);

        }

        [Then(@"I confirm and click on void")]
        public void ThenIConfirmAndClickOnVoid()
        {
            PlusDealPage.voidConfirmationButton.Click();
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
        }

        [Then(@"the voided order with (.*), (.*), (.*), (.*) should appear in market ticker")]
        public void ThenTheVoidedOrderWithDEALShouldAppearInMarketTicker(string ordertype, string voidstatus, string voidreason, string price)
        {
            WaitForModalBackdropToDisappear();
            WaitForElementToPresent(DashboardPage.bidGridUpButton, 5);
            DashboardPage.bidGridUpButton.Click();
            DashboardPage.bidGridDownButton.Click();

            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 5);
            var marketTickerItems = MarketTickerPage.GetMarketTickerItems();

            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(price.ToString((CultureInfo.InvariantCulture))), "Price is not visible: " + price);
            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(voidstatus.ToUpper()), "Void Status is not visible: " + voidstatus);
            IsTrue(marketTickerItems[0].MarketTickerItem.Contains(voidreason.ToLower()), "Void Reason is not visible: " + voidreason);
            IsTrue(marketTickerItems[0].BidOrAsk.Contains(ordertype), "Order Type is not visible: " + ordertype);


        }

        [Then(@"I click on Verify button")]
        public void ThenIClickOnVerifyButton()
        {
            PlusDealPage.plusDealVerifyButton.Click();
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
            //There is no status for verified external deals
            _marketTickerExpectedInfo.Status = "";

        }

        [Then(@"I do not enter the void reason Other")]
        public void ThenIDoNotEnterTheVoidReasonOther()
        {
            PlusDealPage.deal_other.Click();
        }

        [Then(@"I verify that void button is disabled")]
        public void ThenThenIVerifyThatVoidButtonIsDisabled()
        {
            IsFalse(PlusDealPage.voidConfirmationButton.Enabled, "Void Button is enabled");
        }

        [Then(@"I cancel the void reason selection screen")]
        public void ThenICancelTheVoidReasonSelectionScreen()
        {
            PlusDealPage.voidCancelButton.Click();
        }


        [Then(@"I exit the void screen")]
        public void ThenIExitTheVoidScreen()
        {
            PlusDealPage.voidDealExitButton.Click();
            WaitForModalBackdropToDisappear();
        }

        [Then(@"I verify all the products are available in the Contract drop down Menu")]
        public void ThenIVerifyAllTheProductsAreAvailableInTheContractDropDownMenu(Table table)
        {

            PlusDealPage.plusDealContract.Click();
            var actualContractList = PlusDealPage.plusDealContractList.Select(x => x.Text).ToList();

            var expectedContractList = new List<string>();
            foreach (var row in table.Rows[0]["Products"].Split(','))
            {
                expectedContractList.Add(row);
            }

            AreCollectionEqual(expectedContractList, actualContractList, "All Subscribed Products are not visible");



        }

        [Then(@"I exit the Plus Deal Screen")]
        public void ThenIExitThePlusDealScreen()
        {
            PlusDealPage.plusDealExitButton.Click();
            WaitForElementToPresent(DashboardPage.signOutButton, 5);
        }


        /*   [Then(@"the Free Form Deal displays (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)")]
           public void ThenTheFreeFormDealDisplaysBuyerSellerBrokerKemamanPenGradeAutomatedMetadata(string price, string quantity, string buyer, string seller, string broker, string metadata1, string metadata2, string metadata3)
           {

               Console.WriteLine("Free Form: "+ PlusDealPage.GetFreeFormDealText());

               var freeformdealText = PlusDealPage.GetFreeFormDealText();

               IsTrue(freeformdealText.Contains(price.ToString(CultureInfo.InvariantCulture)), "Price is not visible: " + price);
               IsTrue(freeformdealText.Contains(quantity), "Quantity is not visible: " + quantity);
               IsTrue(freeformdealText.Contains(buyer), "Buyer is not visible: " + buyer);
               IsTrue(freeformdealText.Contains(seller), "Seller is not visible: " + seller);
           }*/


    }
}
