﻿using AOMIntegrationTests.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace AOMIntegrationTests.Steps.EndToEnd
{
    [Binding]
    public sealed class SettingsSteps : Utilities
    {
        public SettingsSteps(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null)
            {
                Console.WriteLine("DealsTwoWay Steps Scenario Context is null");
                throw new ArgumentNullException(("scenarioContext"));
            }

            this.scenarioContext = scenarioContext;
        }

        [When(@"I click on Settings button")]
        public void ThenIClickOnSettingsButton()
        {
            WaitForElementToPresent(SettingsPage.settingsButton, 30);
            SettingsPage.settingsButton.Click();
        }

        [Then(@"I should be on Settings Page")]
        public void ThenIShouldBeOnSettingsPage()
        {
            var stopwatch = Stopwatch.StartNew();
            WaitForElementToPresent(SettingsPage.mySettingsPage, 30);
            IsTrue(SettingsPage.mySettingsPage.Displayed, "You are not on Settings Page");
            stopwatch.Stop();
            PerformanceReport(Helpers.Pages.SettingsPage, stopwatch);
        }

        [When(@"I click on Counterparty Permissions")]
        public void ThenIClickOnCounterpartyPermissions()
        {
            SettingsPage.cpPermissionsButton.Click();
        }

        [Then(@"I should be on (.*) screen")]
        public void ThenIShouldBeOnCounterpartyPermissionsScreen(string cptext)
        {
            var stopwatch = Stopwatch.StartNew();
            WaitForElementToContain(SettingsPage.cplastupdated, 30, "Our last update was on");
            AreEqualMsg(cptext, SettingsPage.cpPermissionsTitle.Text, "You are not on Counterparty Permissions Screen");
            stopwatch.Stop();
            PerformanceReport(Helpers.Pages.CounterPartyPage, stopwatch);
        }

        [When(@"I allow Permission to buy and sell to an organisation")]
        public void ThenIAllowPermissionToBuyAndSellToAnOrganisation()
        {
            SettingsPage.cpBuyPermmission.Click();
            SettingsPage.cpSellPermmission.Click();
        }

        [When(@"I deny Permission to buy and sell to an organisation")]
        public void WhenIDenyPermissionToBuyAndSellToAnOrganisation()
        {
            SettingsPage.denyallPermissions.Click();
        }

        [When(@"I update Permission")]
        public void ThenIUpdatePermission()
        {
            WaitElementTobeClickable(SettingsPage.cpUpdateButton, 10);
            SettingsPage.cpUpdateButton.Click();
        }

        [Then(@"I am on Confirmation Page")]
        public void ThenIAmOnConfirmationPage(Table table)
        {
            string actualText = table.Rows[0]["text"];
            string expectedText = SettingsPage.cpConfirmationTitle.Text;
            AreEqualMsg(expectedText, actualText, "You are not on confirmation page");
        }

        [Then(@"I verify the change (.*), (.*)")]
        public void ThenIVerifyTheChange(string orgname, string product, Table table)
        {
            AreEqualMsg(orgname, SettingsPage.orgname.Text, "Organisation name is incorrect");
            AreEqualMsg(product, SettingsPage.productname.Text, "Product name is incorrect");
            AreEqualMsg(table.Rows[0]["pbuy"], SettingsPage.buyAllow.Text, "Permission to Buy is incorrect" + table.Rows[0]["pbuy"]);
            AreEqualMsg(table.Rows[0]["psell"], SettingsPage.sellAllow.Text, "Permission to Sell is incorrect" + table.Rows[0]["psell"]);
        }

        [Then(@"I verify the permissions are denied (.*), (.*)")]
        public void ThenIVerifyThePermissionsAreDeniedAutoTestATAUTOMTBE(string orgname, string product, Table table)
        {
            AreEqualMsg(orgname, SettingsPage.orgname.Text, "Organisation name is incorrect");
            AreEqualMsg(product, SettingsPage.productname.Text, "Product name is incorrect");
            AreEqualMsg(table.Rows[0]["pbuy"], SettingsPage.buyDeny.Text, "Permission to Buy is incorrect" + table.Rows[0]["pbuy"]);
            AreEqualMsg(table.Rows[0]["psell"], SettingsPage.sellDeny.Text, "Permission to Sell is incorrect" + table.Rows[0]["psell"]);
        }

        [Then(@"i confirm the changes")]
        public void ThenIConfirmTheChanges()
        {
            SettingsPage.cpConfirmButton.Click();
        }

        [Then(@"I get a confirmation message")]
        public void ThenIGetAConfirmationMessage(Table table)
        {
            WaitForElementToPresent(SettingsPage.successMessage, 30);
            string expected = table.Rows[0]["message"];
            string actual = SettingsPage.successMessage.Text;
            AreEqualMsg(expected, actual, "Permissions not updated successfully");
        }

        [Given(@"I reset the permissions for (.*) and (.*)")]
        public void GivenIResetThePermissions(string username, string productname)
        {
            if (scenarioContext["currentEnv"].ToString() == "SYSTEST")
                SettingsPage.ResetPermissionForSys(SettingsPage.sysdeletequery, username, productname);
            else if (scenarioContext["currentEnv"].ToString() == "UAT")
                SettingsPage.ResetPermissionForUat(SettingsPage.uatdeletequery, username, productname);
            else
                Console.WriteLine("Environement not set up properly");
        }

        [Then(@"I verify that the permissions are updated in database for (.*), (.*), (.*)")]
        public void ThenIVerifyThatThePermissionsAreUpdatedInDatabaseForAutoTestATAUTOMTBE(string username, string orgname, string prodname, Table table)
        {
            Console.WriteLine("Database Validation");
            var expected = new Dictionary<string, string>();
            var actual = new Dictionary<string, string>();
            foreach (var row in table.Rows)
            {
                expected.Add(row[0], row[1]);
            }
            if (scenarioContext["currentEnv"].ToString() == "SYSTEST")
                actual = SettingsPage.GetPermission(SettingsPage.sysselectquery, username, prodname, orgname);
            else if (scenarioContext["currentEnv"].ToString() == "UAT")
                actual = SettingsPage.GetPermission(SettingsPage.uatselectquery, username, prodname, orgname);
            else
                Console.WriteLine("Environment not set up properly");

            AreCollectionEqual(expected, actual, "Permissions not updated in database");
        }
    }
}