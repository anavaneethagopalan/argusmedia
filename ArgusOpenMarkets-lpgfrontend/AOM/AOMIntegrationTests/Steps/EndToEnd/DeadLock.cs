﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AOM.App.Domain.Entities;
using AOMIntegrationTests.AomBus;
using AOMIntegrationTests.Helpers;
using AOMIntegrationTests.Steps.context;
using AOMIntegrationTests.Steps.CounterPartyPermission;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AOMIntegrationTests.Steps.EndToEnd
{
    [Binding]
    public sealed class DeadLock
    {

        private int _orderCount;

        private int _marketTickerCount;

       [Given(@"I purge the open orders")]
        public void GivenIPurgeTheOpenOrders()
        {
            var busPurgeOrders = new BusPurgeOrders();

            busPurgeOrders.PublishPurgeAomProductOrders(1);
            Thread.Sleep(5000);
        }

        [Given(@"I count the orders and market ticker items in SQL")]
        public void GivenICountTheOrdersAndMarketTickerItemsInSQL()
        {
            var sql = new SQLQueries();

            _orderCount = sql.GetOrdersCount();
            _marketTickerCount = sql.GetMarketTickerCount();

        }

      
        //TODO: Refactor this with the CounterParty permission code to avoid code duplication
        [When(@"I create (.*) bids in parrallel for user ""(.*)"", product """"(.*)""")]
        public void WhenICreateBidsInParrallel(int numberofOrderToCreate, string user, string productname)
        {

           var ordertype = "bid";

            var sqlqueries = new SQLQueries();
            var productId = sqlqueries.GetProductIdFromName(productname);

            ScenarioContext.Current["OrderType"] = ordertype.ToUpper();
            ScenarioContext.Current["ProductName"] = productname;

            var busPurgeOrders = new BusPurgeOrders();

            var orgId = sqlqueries.GetOrgFromUserName(user);
            var userId = sqlqueries.GetUserIdFromUserName(user);
            var tenorid = sqlqueries.GetTenorIdFromProductId(productId);
            var deliverystartdate = sqlqueries.GetDeliveryStartDate(tenorid);
            var deliveryEndDate = deliverystartdate + 3;

            Assert.IsTrue(productId != 0, "Product not present in database");

            var myorder = new OrderMessage()
            {
                productId = productId,
                principalUserId = userId,
                // OrderType = OrderType.Bid,
                OrderType = (OrderType) Enum.Parse(typeof(OrderType), ordertype, true),
                principalOrganisationId = orgId,
                tenor = new TenorId() {Id = tenorid},
            };

            Parallel.For(0, numberofOrderToCreate, i =>
            {
                busPurgeOrders.PublishOrder(myorder, false);
            });
        }

        [Then(@"I expect the orders and market ticker tables count to increase by (.*)")]
        public void ThenIExpectTheOrdersAndMarketTickerTablesCountToIncreaseBy(int ExpectedOrderAndMarketTickerIncreaseCount)
        {
            Thread.Sleep(5000);

            var sql = new SQLQueries();

            var newOrderCount = sql.GetOrdersCount();
            var newMarketTickerCount = sql.GetMarketTickerCount();

            Assert.AreEqual(_orderCount + 100, newOrderCount);
            Assert.AreEqual(_marketTickerCount + 100, newMarketTickerCount);

            var busPurgeOrders = new BusPurgeOrders();
            busPurgeOrders.PublishPurgeAomProductOrders(1);
        }





    }


}
