﻿using AOMIntegrationTests.AomBus;
using AOMIntegrationTests.Helpers;
using AOMIntegrationTests.Steps.context;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace AOMIntegrationTests.Steps.EndToEnd
{
    [Binding]
    public sealed class DealsTwoWaySteps : Utilities

    {
        private readonly MarketTickerExpectedInfo _mtMarketTickerExpectedInfo;

        public DealsTwoWaySteps(ScenarioContext scenarioContext, MarketTickerExpectedInfo mtMarketTickerExpectedInfo)
        {
            _mtMarketTickerExpectedInfo = mtMarketTickerExpectedInfo;
            if (scenarioContext == null)
            {
                Console.WriteLine("DealsTwoWay Steps Scenario Context is null");
                throw new ArgumentNullException("scenarioContext");
            }

            this.scenarioContext = scenarioContext;
        }

        [When(@"I click on the Order of type (.*) for the Product (.*)")]
        public void WhenIClickOnTheOrderOfTypeForTheProduct(string ordertype, string productname)
        {
            // Remove all Orders from the System for the Product
            scenarioContext["ProductName"] = productname;
            scenarioContext["OrderType"] = ordertype;
            var sqlqueries = new SQLQueries();

            var productId = sqlqueries.GetProductIdFromName(productname);
            IsTrue(productId != 0, "Product not present in database");

            // Purge Orders through Rabbit Bus
            var busPurgeOrders = new BusPurgeOrders();
            busPurgeOrders.PublishPurgeAomProductOrders(productId);

            DashboardPage.clickGridUpbutton();
            DashboardPage.clickGridDownbutton();

            // Verfiy Bid/Ask stack for the Product is empty
            var result = DashboardPage.GetOrderCount(productname);
            IsTrue(result, "Bid/Ask Stack not empty");

            DashboardPage.OpenOrderDialogForOrderTypeAndProduct(ordertype, productname);
        }

        [Then(@"I should be taken to the New Order Page for the product")]
        public void ThenIShouldBeTakenToTheNewOrderPageForTheProduct(Table table)
        {
            var expected = table.Rows[0]["title"];
            WaitForElementToPresent(".modal-header", 30);
            var actual = NewOrderPage.orderPageHeader.Text;
            AreEqualMsg(expected, actual, "Incorrect Order Page");
        }

        [Then(@"I fill in the order details with (.*), (.*) ,(.*) ,(.*) ,(.*)")]
        public void ThenIFillInTheOrderDetails(string ordertype, decimal price, string quantity, string broker, string notes)
        {
            scenarioContext["Price"] = price;

            switch (ordertype)
            {
                case "BID":
                    {
                        NewOrderPage.bidButton.Click();
                        break;
                    }
                case "ASK":
                    {
                        NewOrderPage.askButton.Click();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("No OrderType Specified");
                        break;
                    }
            }

            NewOrderPage.SetNewPrice(price.ToString());
            NewOrderPage.SetNewQuantity(quantity);
            NewOrderPage.SetBroker(broker);
            NewOrderPage.SetOrderNotes(notes);

            _mtMarketTickerExpectedInfo.Price = price;
            _mtMarketTickerExpectedInfo.Order = ordertype.Trim();
            _mtMarketTickerExpectedInfo.Status = "NEW";
        }

        [When(@"I click on create Order")]
        public void WhenIClickOnCreateOrder()
        {
            IsTrue(NewOrderPage.createButton.Enabled, "Create Order Button not Enabled");

            NewOrderPage.createButton.Click();
            _mtMarketTickerExpectedInfo.Status = "NEW";

            WaitElementTobeClickable(DashboardPage.signOutButton, 10);
            DashboardPage.clickGridUpbutton();
            DashboardPage.clickGridDownbutton();
        }

        [Then(@"the order with the price (.*) quantity (.*) broker (.*) and principal (.*) should be displayed in (.*) bidask stack")]
        public void ThenTheOrderWithThePriceQuantityBrokerAndPrincipalShouldBeDisplayedInBidaskStack(decimal price,
            int quantity, string broker, string principal, string productname)
        {
            Thread.Sleep(2000);
            var orders = DashboardPage.GetOrders(scenarioContext["OrderType"].ToString(), productname);
            var enumerable = orders as string[] ?? orders.ToArray();

            IsTrue(enumerable.Contains(broker), "Broker is not correct " + broker);
            IsTrue(enumerable.Contains(principal), "Principal is not correct " + principal);
            IsTrue(enumerable.Contains(price.ToString("N")), "Price is not correct " + price.ToString("N"));
            IsTrue(enumerable.Contains(quantity.ToString("##,###")), "Quantity is not correct " + quantity.ToString("N"));
        }


        [Then(@"the color of the order should be (.*)")]
        public void ThenTheColorOfTheOrderShouldBe(string expectedordercolor)
        {
            string ordertype = scenarioContext["OrderType"].ToString();
            string productname = scenarioContext["ProductName"].ToString();

            var rgbacolor = DashboardPage.GetBidAskOrderColor(ordertype, productname);
            var actualorderColor = colordict[rgbacolor];
            Console.WriteLine("actual Color" + actualorderColor);
            AreEqualMsg(expectedordercolor.ToUpper(), actualorderColor.ToUpper(), "Colors are Different: " + expectedordercolor + "---" + actualorderColor);
        }

        [Then(@"the order should be executable by (.*)")]
        public void ThenTheOrderShouldBeExecutableBy(string observer)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            IsTrue(NewOrderPage.executeButton.Enabled, "Order is not Executable by " + observer);
        }

        [Then(@"the order can only be executable by (.*)")]
        public void ThenTheOrderCanOnlyBeExecutableBy(string observer)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            IsTrue(NewOrderPage.executeButton.Enabled, "Order is not Executable by " + observer);
            IsFalse(IsElementPresent(NewOrderPage.updateButton), "Order is Editable by " + observer);
            IsFalse(IsElementPresent(NewOrderPage.killButton), "Order can be killed by " + observer);
        }

        [Then(@"the order with (.*),(.*),(.*) should appear in market ticker")]
        public void ThenTheOrderWithShouldAppearInMarketTicker(string ordertype, string status, decimal price)
        {
            WaitForElementToPresent(DashboardPage.bidGridUpButton, 5);
            DashboardPage.clickGridUpbutton();
            DashboardPage.clickGridDownbutton();

            _mtMarketTickerExpectedInfo.Order = ordertype;
            _mtMarketTickerExpectedInfo.Price = price;
            _mtMarketTickerExpectedInfo.Status = status;

            //Wait For Market Ticket to Update for 10 Seconds
            var marketTickerItem = WaitForMarketTickerToUpdate(ordertype, status, price);
            if (marketTickerItem == null)
            {
                IsTrue(false, "marketTicker is empty");
            }

            IsTrue(marketTickerItem.MarketTickerItem.Contains(price.ToString((CultureInfo.InvariantCulture))), "Price is incorrect: " + price);
            IsTrue(marketTickerItem.MarketTickerItem.Contains(status), "Status is incorrect: " + status);
            IsTrue(marketTickerItem.BidOrAsk.Contains(ordertype), "Order Type is incorrect: " + ordertype);
        }

        public static MarketTickerDisplayItem WaitForMarketTickerToUpdate(string ordertype, string status, decimal price, string text = "")
        {
            for (int i = 0; i < 20; i++)
            {
                var marketTickerItems = MarketTickerPage.GetMarketTickerItems();
                IEnumerable<MarketTickerDisplayItem> marketTickerElement;

                //This is for items that don't have a price.
                if (price == 0)
                {
                    marketTickerElement =
                        marketTickerItems.Where(
                            x => x.MarketTickerItem.ToLower().Contains(status.ToLower())
                                 && x.BidOrAsk.ToLower().Contains(ordertype.ToLower())
                                 && x.MarketTickerItem.ToLower().Contains(text.ToLower()));
                }
                else
                {
                    marketTickerElement =
                        marketTickerItems.Where(
                            x => x.MarketTickerItem.Contains(price.ToString((CultureInfo.InvariantCulture)))
                                 && x.MarketTickerItem.ToLower().Contains(status.ToLower())
                                 && x.BidOrAsk.ToLower().Contains(ordertype.ToLower())
                                 && x.MarketTickerItem.ToLower().Contains(text.ToLower()));
                }
                if (marketTickerElement.Any())
                {
                    return marketTickerElement.ElementAt(0);
                }

                //Debug Should be removed:
                foreach (var item in marketTickerItems)
                {
                    Console.WriteLine(item.MarketTickerItem);
                    Console.WriteLine(item.BidOrAsk);
                }
                Thread.Sleep(1000);
            }

            return null;
        }

        [Then(@"the color of the market ticker item should be (.*)")]
        public void ThenTheColorOfTheMarketTickerItemShouldBeOrange(string expectedtickercolor)
        {
            //  WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
            var marketTickerItem = WaitForMarketTickerToUpdate(_mtMarketTickerExpectedInfo.Order, _mtMarketTickerExpectedInfo.Status, _mtMarketTickerExpectedInfo.Price, _mtMarketTickerExpectedInfo.Text);

            if (marketTickerItem == null)
            {
                Assert.Fail("Market Ticker Item could not be found");
            }

            var rgbacolor = MarketTickerPage.GetMarketTickerItemColor(marketTickerItem);

            var actualtickerColor = colordict[rgbacolor];
            Console.WriteLine("actual Color" + actualtickerColor);
            AreEqualMsg(expectedtickercolor.ToUpper(), actualtickerColor.ToUpper(), "Colors are Different: " + expectedtickercolor + "---" + actualtickerColor);
        }

        [Then(@"the order should not be editable or executable by (.*)")]
        public void ThenTheOrderShouldNotBeEditableOrExecutableBy(string observer)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);
            IsFalse(NewOrderPage.executeButton.Enabled, "Order is Executable by " + observer);
            IsFalse(IsElementPresent(NewOrderPage.updateButton), "Order is Editable by " + observer);
            IsFalse(IsElementPresent(NewOrderPage.killButton), "Order can be killed by " + observer);
        }

        [Then(@"the order can be held, updated and killed by (.*)")]
        public void ThenTheOrderCanBeHeldUpdatedAndKilledByALT(string observer)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            IsTrue((NewOrderPage.killButton.Enabled), "Order cannot be killed by " + observer);
            IsTrue((NewOrderPage.holdButton.Enabled), "Order cannot be held by " + observer);
            IsTrue(IsElementPresent(NewOrderPage.updateButton), "Order is not Editable by " + observer);
        }

        [Then(@"I close the order screen")]
        public void ThenICloseTheOrderScreen()
        {
            if (NewOrderPage.closeButton.Enabled)
            {
                //A simple way to get rid of the tooltip which appears over the close button
                NewOrderPage.orderPageHeader.Click();
                NewOrderPage.closeButton.Click();
            }
            WaitForElementToPresent(DashboardPage.signOutButton, 10);
            WaitForModalBackdropToDisappear();
        }

        /* ------------------*/

        [Then(@"the trader executes the order")]
        [Then(@"the observer executes the order")]
        public void ThenTheObserverExecutesTheOrder()
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.orderPageHeader, 5);
            IsTrue(NewOrderPage.executeButton.Enabled, "Order is not Executable");

            NewOrderPage.executeButton.Click();
            WaitForElementToPresent(DashboardPage.headerIcon, 5);
        }

        [Then(@"the order no longer exists on the bid/ask stack for the (.*)")]
        public void ThenTheOrderNoLongerExistsOnTheBidAskStackFor(string productname)
        {
            DashboardPage.clickGridUpbutton();
            DashboardPage.clickGridDownbutton();

            var result = DashboardPage.GetOrderCount(productname);
            //      IsTrue(result, "Bid/Ask Stack not empty. Order Exists on bid/ask stack");
        }

        [Then(@"the hoverbox shows tradeable as (.*)")]
        public void ThenTheHoverboxShowsTradeableAs(string tradeable)

        {
            DashboardPage.hoverAroundOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(DashboardPage.hoverbox, 5);

            var tooltipDataTable = DashboardPage.GetHoverelements();

            string actual = tooltipDataTable.Rows[0]["Tradeable"].ToString().ToUpper();
            string expected = tradeable.ToUpper();

            AreEqualMsg(expected, actual, "Tradeable status are not equal: " + actual + " ---- " + expected);

            DashboardPage.MoveOutofOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
        }
    }
}