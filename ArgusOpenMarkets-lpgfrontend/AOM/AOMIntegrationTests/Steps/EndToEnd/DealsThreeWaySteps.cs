﻿using AOMIntegrationTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMIntegrationTests.Pages;
using AOMIntegrationTests.Steps.context;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace AOMIntegrationTests.Steps.EndToEnd
{
    [Binding]
    public class DealsThreeWaySteps : Utilities
    {
        private readonly MarketTickerExpectedInfo _marketTickerExpectedInfo;

        public DealsThreeWaySteps(ScenarioContext scenarioContext, MarketTickerExpectedInfo _marketTickerExpectedInfo)
        {
            this._marketTickerExpectedInfo = _marketTickerExpectedInfo;

            if (scenarioContext == null)
            {
                Console.WriteLine("DealsThreeWay Steps Scenario Context is null");
                throw new ArgumentNullException("scenarioContext");
            }

            this.scenarioContext = scenarioContext;
        }

        [Then(@"the broker executes the order naming (.*) as principal")]
        public void ThenTheBrokerExecutesTheOrderNamingAsPrincipal(string principal)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);
            WaitElementTobeClickable(NewOrderPage.editorderexecuteButton, 10);
            NewOrderPage.editorderexecuteButton.Click();

            WaitForElementToPresent(NewOrderPage.executeButton, 10);

            NewOrderPage.SetPrincipal(principal);
            WaitElementTobeClickable(NewOrderPage.executeButton, 10);
            NewOrderPage.executeButton.Click();


        }

        [Then(@"I fill in the order details for broker with (.*) ,(.*) ,(.*) ,(.*) ,(.*)")]
        public void ThenIFillInTheOrderDetailsForBrokerwith(string ordertype, decimal price, string quantity, string principal, string notes)
        {
            switch (ordertype)
            {

                case "BID":
                    {
                        NewOrderPage.bidButton.Click();
                        break;
                    }
                case "ASK":
                    {
                        NewOrderPage.askButton.Click();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("No OrderType Specified " + ordertype);
                        break;
                    }
            }

            NewOrderPage.SetNewPrice(price.ToString());
            NewOrderPage.SetNewQuantity(quantity);
            NewOrderPage.SelectPrincipal(principal);
            NewOrderPage.SetOrderNotes(notes);

            _marketTickerExpectedInfo.Order = ordertype;
            _marketTickerExpectedInfo.Price = price;
           
            IsTrue(NewOrderPage.createButton.Enabled, "Create Order Button not Enabled");
        }

        [Then(@"the order cannot be executed by (.*)")]
        public void ThenTheOrderCannotBeExecutedBy(string observer)
        {
            
            new DashboardPage().openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(new NewOrderPage().closeButton, 10);
            IsFalse(IsElementEnabled(new NewOrderPage().executeButton), "Order is executable by " + observer);
        }

        [Then(@"the broker executes the order with (.*) as principal")]
        public void ThenTheBrokerExecutesTheOrderWithAutoTestATAsPrincipal(string principal)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());

            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            NewOrderPage.SetPrincipal(principal);

            WaitElementTobeClickable(NewOrderPage.executeButton, 10);
            NewOrderPage.executeButton.Click();
        }

        [Then(@"the trader executes the order with (.*) as broker")]
        public void ThenTheTraderExecutesTheOrderWithAutoBrokerABAsBroker(string broker)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);
            NewOrderPage.SetBroker(broker);
            WaitElementTobeClickable(NewOrderPage.executeButton, 10);
            NewOrderPage.executeButton.Click();

        }

        [Then(@"the order can be executed by ""(.*)""")]
        public void ThenTheOrderCanBeExecutedBy(string observer)
        {
            var dashboardpage = new DashboardPage();
            dashboardpage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(new NewOrderPage().closeButton, 10);
            IsTrue(IsElementEnabled(new NewOrderPage().executeButton), "Order is not Executable by " + observer);
            // IsFalse(IsElementPresent(NewOrderPage.updateButton), "Order is Editable by " + observer);
            // IsFalse(IsElementPresent(NewOrderPage.killButton), "Order can be killed by " + observer);


        }

        [Then(@"the order can be executed by ""(.*)"" by selecting ""(.*)"" as principal")]
        public void ThenTheOrderCanBeExecutedByBySelectingAsPrincipal(string observer, string principalTrader)
        {
            var dashboardpage = new DashboardPage();
            dashboardpage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(new NewOrderPage().closeButton, 10);
            new NewOrderPage().SetPrincipal(principalTrader);
            IsTrue(IsElementEnabled(new NewOrderPage().executeButton), "Order is not Executable by " + observer);
            
        }



        [Then(@"the (.*) can execute the order")]
        public void ThenUserCanExecuteTheOrder(string user)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);
            IsTrue(NewOrderPage.editorderexecuteButton.Enabled, "Order is not Executable by " + user);
            
            // IsFalse(IsElementPresent(NewOrderPage.updateButton), "Order is Editable by " + observer);
            // IsFalse(IsElementPresent(NewOrderPage.killButton), "Order can be killed by " + observer);
        }

        [Then(@"the (.*) should not be able to select principal (.*)")]
        public void ThenTheABShouldNotBeAbleToSelectSellerPrincipalAutoTestAT(string user, string principal)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);
            NewOrderPage.principalDropdown.Click();

            var results = NewOrderPage.brokerprincipalList.Select(x => x.Text);
            foreach (var r in results)
            {
                Console.WriteLine("Records: " + r);
            }

            IsFalse(results.Contains(principal), "Broker " + user + " can select Seller principal " + principal);

        }

        [Then(@"the (.*) should not be able to select broker (.*)")]
        public void ThenTheALTShouldNotBeAbleToSelectBrokerAutoBrokerAB(string user, string broker)
        {
            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            NewOrderPage.brokerDropdown.Click();

            var results = NewOrderPage.brokerprincipalList.Select(x => x.Text);
            foreach (var r in results)
            {
                Console.WriteLine("Records: " + r);
            }

            IsFalse(results.Contains(broker), "Trader " + user + " can select Broker " + broker);

        }

        [Then(@"the (.*) should not be able to choose (.*)")]
        public void ThenTheABShouldNotBeAbleToSelectAutoTestAT(string user, string principal)
        {
           // DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());

            NewOrderPage.principalDrop.Click();
            var results = NewOrderPage.brokerprincipalList.Select(x => x.Text);
            foreach (var r in results)
            {
                Console.WriteLine("Records: " + r);
            }

            IsFalse(results.Contains(principal), "Broker " + user + " can select Seller principal " + principal);
        }

        [Then(@"I change the broker to (.*)")]
        public void ThenIChangeTheBrokerTo(string newbroker)
        {
            DashboardPage.openExistingOrder(scenarioContext["OrderType"].ToString(), scenarioContext["ProductName"].ToString());
            WaitForElementToPresent(NewOrderPage.closeButton, 10);

            NewOrderPage.SetBroker(newbroker);

        }


    }
}


