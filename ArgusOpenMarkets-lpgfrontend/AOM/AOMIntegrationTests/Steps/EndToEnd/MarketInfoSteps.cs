﻿using System.Threading;
using AOMIntegrationTests.Helpers;
using AOMIntegrationTests.Steps.context;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AOMIntegrationTests.Steps.EndToEnd
{
    [Binding]
    public sealed class MarketInfoSteps : Utilities
    {
        private readonly MarketTickerExpectedInfo _marketTickerExpectedInfo;

        public MarketInfoSteps(MarketTickerExpectedInfo marketTickerExpectedInfo)
        {
            _marketTickerExpectedInfo = marketTickerExpectedInfo;
        }

        [When(@"I click on the Market Info")]
        public void WhenIClickOnTheMarketInfo()
        {
            WaitElementTobeClickable(MarketInfoPage.marketInfoButton, 10);
            MarketInfoPage.marketInfoButton.Click();
        }

        [Then(@"I should be taken to Verify Market Info Page")]
        [Then(@"I should be taken to the Market Info page")]
        public void ThenIShouldBeTakenToTheMarketInfoPage(Table table)
        {
            WaitForElementToPresent(MarketInfoPage.marketInfoHeader, 10);

            string expected = table.Rows[0]["title"];
            string actual = MarketInfoPage.marketInfoHeader.Text;

            AreEqualMsg(expected, actual, "You are not on " + expected + " Page");
        }

        [Then(@"i fill in the market info details with (.*)")]
        public void ThenIFillInTheMarketInfoDetailsWithThisIsAMarketInformationCreatedBy(string text)
        {
            MarketInfoPage.marketInfoText.SendKeys(text);
            _marketTickerExpectedInfo.Text = text;
        }

        [When(@"I click on create Market Info")]
        public void WhenIClickOnCreateMarketInfo()
        {
            WaitElementTobeClickable(MarketInfoPage.marketInfoCreateButton, 20);
            MarketInfoPage.marketInfoCreateButton.Click();
            WaitForElementToPresent(DashboardPage.bidGridUpButton, 10);
            _marketTickerExpectedInfo.Order = "INFO";
            _marketTickerExpectedInfo.Status = "Pending";
        }

        [Then(@"the market info details (.*), (.*), (.*) should be displayed in market ticker")]
        public void ThenTheMarketInfoDetailsShouldBeDisplayedInMarketTicker(string ordertype, string text, string status)
        {
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 5);

            var marketTickerItem = DealsTwoWaySteps.WaitForMarketTickerToUpdate(ordertype, status, 0, text);

            if (marketTickerItem == null)
            {
                Assert.Fail("Can't find marketTickerItem after 20 tries");
            }

            WaitForElementToPresent(DashboardPage.bidGridUpButton, 5);

            IsTrue(marketTickerItem.MarketTickerItem.Contains(text), "Text is not visible: " + text);
        }

        [Then(@"the market info details (.*), (.*), (.*) should not be displayed in market ticker")]
        public void ThenTheMarketInfoDetailsShouldNotBeDisplayedInMarketTicker(string ordertype, string text, string status)
        {
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 5);
            WaitForElementToPresent(DashboardPage.bidGridUpButton, 5);

            var marketTickerItems = MarketTickerPage.GetMarketTickerItems();

            IsFalse(marketTickerItems[0].MarketTickerItem.Contains(text), "Text is visible: " + text);
            //   IsFalse(marketTickerItems[0].MarketTickerItem.Contains(status), "Status is visible: " + status);
            //  IsFalse(marketTickerItems[0].BidOrAsk.Contains(ordertype), "Order Type is visible: " + ordertype);
        }

        [Then(@"I click on Market info verify button")]
        public void ThenIClickOnMarketInfoVerifyButton()
        {
            WaitElementTobeClickable(MarketInfoPage.marketInfoVerifyButton, 5);
            MarketInfoPage.marketInfoVerifyButton.Click();
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
            _marketTickerExpectedInfo.Order = "INFO";
            _marketTickerExpectedInfo.Status = "NEW";
        }

        [When(@"I click on create Market Info as Pending")]
        public void WhenIClickOnCreateMarketInfoAsPending()
        {
            WaitElementTobeClickable(MarketInfoPage.create_as_pendingButton, 10);
            MarketInfoPage.create_as_pendingButton.Click();
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
            _marketTickerExpectedInfo.Order = "INFO";
            _marketTickerExpectedInfo.Status = "PENDING";
        }

        [When(@"I click on create Market Info as Verfied")]
        public void WhenIClickOnCreateMarketInfoAsVerfied()
        {
            WaitElementTobeClickable(MarketInfoPage.created_as_verifiedButton, 10);
            MarketInfoPage.created_as_verifiedButton.Click();
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
            _marketTickerExpectedInfo.Order = "INFO";
            _marketTickerExpectedInfo.Status = "NEW";
        }

        [When(@"I click on update Market Info")]
        public void WhenIClickOnUpdateMarketInfo()
        {
            WaitElementTobeClickable(MarketInfoPage.marketInfoUpdateButton, 10);
            MarketInfoPage.marketInfoUpdateButton.Click();
            _marketTickerExpectedInfo.Status = "UPDATED";
        }

        [Then(@"I update the market info details with (.*)")]
        public void ThenIUpdateTheMArketInfoDetailsWith(string newtext)
        {
            MarketInfoPage.marketInfoUpdateText.Clear();
            MarketInfoPage.marketInfoUpdateText.SendKeys(newtext);
            Thread.Sleep(1000);
            _marketTickerExpectedInfo.Text = newtext;

        }

        [When(@"I click on verify Market Info")]
        public void WhenIClickOnVerifyMarketInfo()
        {
            WaitElementTobeClickable(MarketInfoPage.marketInfoVerifyButton, 10);
            MarketInfoPage.marketInfoVerifyButton.Click();
            _marketTickerExpectedInfo.Status = "NEW";
        }

        [When(@"I click on void Market Info")]
        public void WhenIClickOnVoidMarketInfo()
        {
            WaitForElementToPresent(MarketInfoPage.marketInfoVoidButton, 10);
            MarketInfoPage.marketInfoVoidButton.Click();
            WaitForListElementToPresent(MarketTickerPage.marketTickerItems, 10);
            _marketTickerExpectedInfo.Status = "VOIDED";
        }
    }
}