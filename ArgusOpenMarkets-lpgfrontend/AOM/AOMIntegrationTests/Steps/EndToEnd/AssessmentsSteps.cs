﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMIntegrationTests.Helpers;
using AOMIntegrationTests.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace AOMIntegrationTests.Steps.EndToEnd
{
    [Binding]
    public sealed class AssessmentsSteps : Utilities
    {
        public AssessmentsSteps(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null)
            {
                Console.WriteLine("Assessments Steps Scenario Context is null");
                throw new ArgumentNullException(("scenarioContext"));
            }
            this.scenarioContext = scenarioContext;
        }

        [When(@"I click on assessments for (.*)")]
        public void WhenIClickAssessmentsForProduct(string product)
        {
            WaitForElementToContain(AssessmentPage.productAssessment, 10, product.ToUpper().Trim());
            AssessmentPage.ClickOnproductAssessment();
            WaitForElementToPresent(AssessmentPage.assessmentHeader, 10);
        }

        [Then(@"I should be on assessments window")]
        public void ThenIShouldBeOnAssessmentsWindow(string expectedtitle)
        {
            var expected = expectedtitle;
            var actual = AssessmentPage.assessmentHeader.Text;
            AreEqualMsg(expected, actual, "You are not Assessments Window");
        }

        [Then(@"I enter the (.*), (.*) for the assessment")]
        public void ThenIEnterTheRUNNINGVWATodayForTheAssessment(string lowprice, string highprice)
        {
            AssessmentPage.lowPrice.SendKeys(lowprice);
            AssessmentPage.highPrice.SendKeys(highprice);
            AssessmentPage.statusDropDown.Click();
        }

        [Then(@"I enter status as (.*)")]
        public void ThenIEnterStatusAs(string status)
        {
            foreach (var s in AssessmentPage.statusSelect)
            {
                if (s.Text.ToUpper() == status.ToUpper())
                    s.Click();
            }
        }

        [Then(@"i enter period as ""(.*)""")]
        public void ThenIEnterPeriodAs(string period)
        {
            AssessmentPage.periodDropDown.Click();
            foreach (var p in AssessmentPage.periodSelect)
            {
                if (p.Text.ToUpper() == period.ToUpper())
                    p.Click();
            }
        }

        [Then(@"I click on Input Current EOD prices")]
        public void ThenIClickOnInputTodaysEODPrices()
        {
            WaitElementTobeClickable(AssessmentPage.inputCurrentEODPrices, 5);
            AssessmentPage.inputCurrentEODPrices.Click();
            // WaitForElementToPresent(AssessmentPage.productAssessment,10);
        }

        [When(@"I clear ""(.*)"" Assessment for ""(.*)""")]
        public void WhenIClearAssessmentFor(string period, string product)
        {
            WhenIClickAssessmentsForProduct(product);
            ThenIEnterPeriodAs(period);
            ThenIVerifyClearAssessmentButtonText($"Clear {period} Assessment");
            ThenIShouldBeOnAssessmentsWindow("Assessment Price Input");
            ThenIClickOnClearTodayAssessment(period);
        }

        [When(@"I enter assessment for ""(.*)"" and ""(.*)"" with ""(.*)"" ""(.*)"" ""(.*)""")]
        public void WhenIEnterAssessmentForAndWith(string period, string product, Decimal lowprice, Decimal highprice, string status)
        {
            WhenIClickAssessmentsForProduct(product);
            ThenIShouldBeOnAssessmentsWindow("Assessment Price Input");
            ThenIEnterTheRUNNINGVWATodayForTheAssessment(lowprice.ToString(), highprice.ToString());
            ThenIEnterStatusAs(status);
            ThenIEnterPeriodAs(period);
            ThenIClickOnInputTodaysEODPrices();
        }

        [Then(@"the prices (.*), (.*), (.*) are cleared from the Todays Assessments window")]
        [Then(@"the prices (.*), (.*), (.*) are displayed correctly for Todays assessments")]
        public void ThenThePricesreDisplayedCorrectlyForTodaysAssessments(string low, string high, string status)
        {
            AssessmentPage.WaitForassessmentTodayStatusTextContains(status);

            var assessmentPage = new AssessmentPage();

            var actualprice = assessmentPage.todaysPrice.Select(x => x.Text).ToList();
            var actualstatus = assessmentPage.assessmentTodayStatus.Text;
            foreach (var a in actualprice)
            {
                Console.WriteLine("Price: " + a);
            }
            AreEqualMsg(status.ToUpper(), actualstatus.ToUpper(), "Assessment status for Today is incorrect " + status + " , " + actualstatus);
            IsTrue(actualprice.Contains(low), "Today Low Price is incorrect: " + low);
            IsTrue(actualprice.Contains(high), "Today high Price is incorrect: " + high);
        }

        [Then(@"the prices (.*), (.*), (.*) are cleared from the Previous Assessments window")]
        [Then(@"the prices (.*), (.*), (.*) are displayed correctly for Previous assessments")]
        public void ThenThePricesAreDisplayedCorrectlyForPreviousAssessments(string low, string high, string status)
        {
            // WaitElementTobeClickable(DashboardPage.signOutButton, 10);
            AssessmentPage.WaitForassessmentPreviousStatusTextContains(status);
            var actualprice = AssessmentPage.previousPrice.Select(x => x.Text).ToList();
            var actualstatus = AssessmentPage.assessmentPreviousStatus.Text;
            foreach (var a in actualprice)
            {
                Console.WriteLine("Price: " + a);
            }
            AreEqualMsg(status.ToUpper(), actualstatus.ToUpper(), "Assessment status for Previous is incorrect ");
            IsTrue(actualprice.Contains(low), "Previous Low Price is incorrect: ");
            IsTrue(actualprice.Contains(high), "Previous high Price is incorrect: ");
        }

        [Then(@"i verify clear assessment button text")]
        public void ThenIVerifyClearAssessmentButtonText(string buttontext)
        {
            var expectedText = buttontext;
            var actualText = AssessmentPage.clearAssessment.Text;
            AreEqualMsg(expectedText, actualText, "Clear Assessment Button text is incorrect");
        }

        [Then(@"the details (.*), (.*) are updated in database")]
        public void ThenTheDetailsAreUpdatedInDatabase(string lowprice, string highprice)
        {
            //ScenarioContext.Current.Pending();
            Console.WriteLine("Database verification");
        }

        [Then(@"I click on Clear (.*) Assessment")]
        public void ThenIClickOnClearTodayAssessment(string period)
        {
            Console.WriteLine("Clearing " + period + " Assessment");
            AssessmentPage.clearAssessment.Click();
            WaitForPageToLoad(10);
        }

        [Then(@"the prices (.*), (.*), (.*) are cleared from the Previous Assessments window")]
        public void ThenThePrices__NAAreClearedFromThePreviousAssessmentsWindow()
        {
            ScenarioContext.Current.Pending();
        }
    }
}