﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using AOMIntegrationTests.Helpers;
using TechTalk.SpecFlow;
using System.Threading;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AOMIntegrationTests.Steps.Authentication
{
    [Binding]
    public class LoginSteps : Utilities
    {
        public LoginSteps(ScenarioContext scenarioContext)
        {
            if (scenarioContext == null)
            {
                Console.WriteLine("Login Steps Scenario Context is null");
                throw new ArgumentNullException("scenarioContext");
            }

            this.scenarioContext = scenarioContext;
        }

        [Then(@"I navigate to url again")]
        [Given(@"I navigate to url")]
        public void GivenINavigateToUrl(Table table)
        {
            var stopwatch = Stopwatch.StartNew();
            NavigateToUrl(scenarioContext["currentUrl"].ToString());
            WaitForPageToLoad(30);
            var expected = table.Rows[0]["loginbox"];
            WaitForCssElementToContain(".direct-blue", 10, expected);
            var actual = LoginPage.loginbox.Text;
            Assert.IsTrue(actual.Trim().Equals(expected), "Loginbox Title does not match " + actual);
            stopwatch.Stop();
            PerformanceReport(Helpers.Pages.LoginPage, stopwatch);
        }

        [Then(@"I enter username (.*) and password (.*) again")]
        [Given(@"I enter username (.*) and password (.*)")]
        public void GivenIEnterUsernameLTAndPasswordPassword(string username, string password)
        {
            scenarioContext["Username"] = username;
            scenarioContext["Password"] = password;
            WaitElementTobeClickable(LoginPage._username, 10);
            LoginPage._username.Click();
            LoginPage._username.Clear();
            LoginPage._username.SendKeys(username);

            Thread.Sleep(TimeSpan.FromSeconds(1));

            WaitElementTobeClickable(LoginPage._password, 10);
            LoginPage._password.Click();
            LoginPage._password.Clear();
            LoginPage._password.SendKeys(password);
        }

        [When(@"I click on SignIn on Login Page")]
        public void WhenIClickOnSignInOnLoginPage()
        {
            WaitElementTobeClickable(LoginPage.signInButton, 10);
            LoginPage.signInButton.Click();
        }

        [Then(@"I should be on the dashboard page (.*)")]
        public void ThenIShouldBeOnTheDashboardPage(string user)
        {
            var stopwatch = Stopwatch.StartNew();

            WaitForElementToPresent(DashboardPage.loggedUsername, 30);
            var expected = user;
            var actual = DashboardPage.loggedUsername.Text;
            AreEqualMsg(expected, actual, "You are not on Dashboard Page");

            if (IsElementPresent(DashboardPage.toastmsg))
                DashboardPage.toastmsg.Click();
            stopwatch.Stop();
            PerformanceReport(Helpers.Pages.DashboardPage, stopwatch);
        }

        [Then(@"I Logout of Aom")]
        public void ThenILogoutOfAom()
        {
            WaitForModalBackdropToDisappear();
            WaitElementTobeClickable(DashboardPage.signOutButton, 10);
            DashboardPage.signOutButton.Click();
            WaitForElementToPresent(LoginPage._username, 30);
            IsTrue(LoginPage.VerifyOnLoginPage(), "You are not on Login Page");
        }

        [Then(@"user should see the error (.*)")]
        public void ThenIUserShouldSeeTheErrorInvalidLoginDetailsPleaseTryAgain(string errormsg)
        {
            WaitForElementToPresent(LoginPage.errorText, 5);
            AreEqualMsg(errormsg, LoginPage.errorText.Text, "User can login with incorrect details" + scenarioContext["Username"].ToString() + scenarioContext["Password"].ToString());
        }
    }
}