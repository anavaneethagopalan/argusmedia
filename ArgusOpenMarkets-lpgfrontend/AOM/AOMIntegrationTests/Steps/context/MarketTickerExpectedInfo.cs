﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMIntegrationTests.Steps.context
{
    public class MarketTickerExpectedInfo
    {
        public string Order = "";
        public string Status = "";
        public Decimal Price;
        public string Text= "";
    }
}
