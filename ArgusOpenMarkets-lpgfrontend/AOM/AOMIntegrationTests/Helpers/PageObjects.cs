﻿using AOMIntegrationTests.Pages;

namespace AOMIntegrationTests.Helpers
{
    public class PageObjects : BaseClass
    {
        public static LoginPage LoginPage
        {
            get
            {
                var loginpg = new LoginPage();
                return loginpg;
            }
        }

        public static DashboardPage DashboardPage
        {
            get
            {
                var dashboardpg = new DashboardPage();
                return dashboardpg;
            }
        }

        public static NewOrderPage NewOrderPage
        {
            get
            {
                var neworderpg = new NewOrderPage();
                return neworderpg;
            }
        }

        public static MarketTickerPage MarketTickerPage
        {
            get
            {
                var marktpg = new MarketTickerPage();
                return marktpg;
            }
        }

        public static PlusDealPage PlusDealPage
        {
            get
            {
                var plusdealpg = new PlusDealPage();
                return plusdealpg;
            }
        }

        public static MarketInfoPage MarketInfoPage
        {
            get
            {
                var marketinfopg = new MarketInfoPage();
                return marketinfopg;
            }
        }

        public static SettingsPage SettingsPage
        {
            get
            {
                var settingspg = new SettingsPage();
                return settingspg;
            }
        }

        public static AssessmentPage AssessmentPage
        {
            get
            {
                var assessmntpg = new AssessmentPage();
                return assessmntpg;
            }
        }
    }
}
