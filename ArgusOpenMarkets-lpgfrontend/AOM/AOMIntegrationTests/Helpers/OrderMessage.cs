﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOM.App.Domain.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOMIntegrationTests.Helpers
{
    public class OrderMessage
    {
        public TenorId tenor = new TenorId();
        public int productId = 1;
        public string notes = "";
        public decimal quantity = 15000;
        public List<MetaData> metadata { get; set; }
        public DateTime DeliveryStartDate => SetDeliverStartdate();

        public DateTime deliveryEndDate => DeliveryStartDate.AddDays(5);
        public int principalUserId = 5098;
        public int principalOrganisationId = 36;
        public decimal price = 1000;

        [JsonConverter(typeof(StringEnumConverter))] public OrderType OrderType = OrderType.Bid;

        [JsonConverter(typeof(StringEnumConverter))] public ExecutionMode ExecutionMode = ExecutionMode.Internal;

        [JsonConverter(typeof(StringEnumConverter))] public BrokerRestriction BrokerRestriction = BrokerRestriction.Any;
        public bool coBrokering = false;

        public DateTime SetDeliverStartdate()
        {
            var startdaysfromtoday = new SQLQueries().GetDeliveryStartDate(tenor.Id);
            return DateTime.Now.AddDays(startdaysfromtoday);
        }
    }

    public class TenorId
    {
        public int Id = 1;
    }

    public class MetaData
    {
        public long ProductMetaDataId { get; set; }
    }
}