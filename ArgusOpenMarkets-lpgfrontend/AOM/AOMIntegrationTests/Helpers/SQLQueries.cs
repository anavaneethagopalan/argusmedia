﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace AOMIntegrationTests.Helpers
{
    public class SQLQueries
    {
        private readonly string environmentAppSetting = ConfigurationManager.AppSettings["Environment"];
        private string _table_prefix;

        public string table_prefix
        {
            get
            {
                return _table_prefix;
            }
            private set
            {
                switch (value.ToUpper())
                {
                    case "LOCAL":
                        _table_prefix = "ci";
                        break;

                    case "SYSTEST":
                        _table_prefix = "ci";
                        break;

                    case "UAT":
                        _table_prefix = "uat";
                        break;

                    default:
                        Console.WriteLine("SQL AOM Connection not been set");
                        break;
                }
            }
        }

        public SQLQueries()
        {
            table_prefix = environmentAppSetting;
        }

        public int GetOrgFromUserName(string username)
        {
            var sql = string.Format(@"SELECT org.Id
                      FROM `crm.{0}`.Organisation org, `crm.{0}`.UserInfo ui
                      Where ui.Organisation_Id_fk = org.Id
                      And ui.Username = '{1}'", _table_prefix, username);

            return Convert.ToInt32(RunScalarSql(sql));
        }

        public int GetProductIdFromName(string productname)
        {
            var sql = string.Format(@"select Id from `aom.{0}`.Product where name = '{1}'", _table_prefix, productname);

            return Convert.ToInt32(RunScalarSql(sql));
        }

        public int GetOrgIdFromOrgName(string orgName)
        {
            var sql = string.Format(@"Select * From `crm.{0}`.Organisation
                                    Where Name = '{1}'", _table_prefix, orgName);

            return Convert.ToInt32(RunScalarSql(sql));
        }

        public int GetUserIdFromUserName(string username)
        {
            var sql = string.Format(@"SELECT id FROM `crm.{0}`.UserInfo
                                    Where Username = '{1}'", _table_prefix, username);

            return Convert.ToInt32(RunScalarSql(sql));
        }

        private object RunScalarSql(string sqlquery)
        {
            EnvironmentHelper helper = new EnvironmentHelper();
            var con = helper.GetSQLConnection();

            con.Open();
            MySqlCommand command = new MySqlCommand(sqlquery, con);

            return command.ExecuteScalar();
        }

        public int GetTenorIdFromProductId(int productid)
        {
            var sql = string.Format(@"SELECT t.Id from `aom.{0}`.ProductTenor t, `aom.{0}`.Product p
                                    Where t.product_id_fk = p.id
                                    And p.id = {1}", _table_prefix, productid);
            return Convert.ToInt32(RunScalarSql(sql));
        }

        public DateTime GetPermLastUpdateDate(int productId, int ourOrganisation, int theirOrganisation, string buyOrSell, string table)
        {
            var sql = string.Format(@"SELECT LastUpdated FROM `crm.{0}`.{1}
                                    Where productId = {2}
                                    And OurOrganisation = {3}
                                    And TheirOrganisation = {4}
                                    And BuyOrSell = ""{5}""", _table_prefix, table, productId, ourOrganisation, theirOrganisation, buyOrSell);

            var lastupdatedate = RunScalarSql(sql);

            return Convert.ToDateTime(lastupdatedate);
        }

        public int GetDeliveryStartDate(int tenorid)
        {
            var sql = string.Format(@"SELECT DeliveryDateStart
                                    FROM `aom.{0}`.ProductTenor
                                    Where Id = {1}", _table_prefix, tenorid);

            var positivedaysinString = RunScalarSql(sql).ToString();
            var getdaysint = Regex.Match(positivedaysinString, @"\d+").Value;

            return Convert.ToInt32(getdaysint);
        }

        public int GetOrdersCount()
        {
            //  throw new NotImplementedException();

            var sql = string.Format(@"SELECT count(*) from `aom.{0}`.Order", _table_prefix);

            return Convert.ToInt32(RunScalarSql(sql));
        }

        public int GetMarketTickerCount()
        {
            var sql = string.Format(@"SELECT count(*) from `aom.{0}`.MarketTickerItem", _table_prefix);

            return Convert.ToInt32(RunScalarSql(sql));
        }

        public void InsertNewPerformanceRow(Pages page, double elapsedTotalSeconds)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var version = AssemblyName.GetAssemblyName(assembly.Location).Version.ToString();

            var sql = string.Format(
                $"Insert into `aom.{_table_prefix}`.PerfomanceTests " +
                $"values ('{version}', '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', '{page}', {elapsedTotalSeconds});");

            ExecuteNonQuery(sql);
        }

        private void ExecuteNonQuery(string sql)
        {
            EnvironmentHelper helper = new EnvironmentHelper();
            var con = helper.GetSQLConnection();

            con.Open();
            MySqlCommand command = new MySqlCommand(sql, con);

            command.ExecuteNonQuery();
        }
    }
}