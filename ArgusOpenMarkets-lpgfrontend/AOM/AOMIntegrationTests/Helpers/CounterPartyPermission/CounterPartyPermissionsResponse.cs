﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOMIntegrationTests.Helpers.CounterPartyPermission
{
   public class CounterPartyPermissionsResponse
   {
       public int[] AlteredOrganisationIds { get; set; }
       public IList<CounterPartyPermissionResponse> CounterpartyPermissions { get; set; }
       public bool UpdateSuccessful { get; set; }

   }

    public class CounterPartyPermissionResponse
    {
        public int id;
        [JsonConverter(typeof(StringEnumConverter))] public BuyOrSell BuyOrSell = BuyOrSell.Buy;

        [JsonConverter(typeof(StringEnumConverter))] public AllowOrDeny AllowOrDeny = AllowOrDeny.Allow;

        public DateTime LastUpdated;
        public int LastUpdatedUserId = -1;
        public int OurOrganisation;
        public int TheirOrganisation;
        public int ProductId;
        public bool UpdateSuccessful;
        public string UpdateError;

    }

    public class CounterPartyPermissions
    {
       public int ourOrganisation;
       public int theirOrganisation;
       public int productId;
        [JsonConverter(typeof(StringEnumConverter))] public BuyOrSell BuyOrSell = BuyOrSell.Buy;

        [JsonConverter(typeof(StringEnumConverter))] public AllowOrDeny AllowOrDeny = AllowOrDeny.Allow;

        [JsonIgnore]
        public bool BrokerUpdate;
        public DateTime LastUpdated => GetLatestUpdateDate(productId, ourOrganisation, theirOrganisation, BuyOrSell);

        public DateTime GetLatestUpdateDate(int productId, int ourOrganisation, int theirOrganisation, BuyOrSell buyOrSell)
        {
            string tabletoupdate = "CounterpartyPermission";

            if (BrokerUpdate)
            {
                tabletoupdate = "BrokerPermission";
            }

            string buyOrSellChar = null;
            switch (buyOrSell)
            {
                case BuyOrSell.Buy:
                    buyOrSellChar = "B";
                    break;
                case BuyOrSell.Sell:
                    buyOrSellChar = "S";
                    break;
                case BuyOrSell.None:
                    buyOrSellChar = null;
                    break;

            }
           
                return new SQLQueries().GetPermLastUpdateDate(productId, ourOrganisation, theirOrganisation,
                    buyOrSellChar, tabletoupdate);

        }
        public int LastUpdatedUserId = -1;
        
    }

    public enum AllowOrDeny
    {
        Deny,
        Allow,
        NotSet


    }

    public class CounterPartyRequest
    {
        public IList<CounterPartyPermissions> Permissions;
        public int OrganisationId;

    }

}


