﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using TechTalk.SpecFlow;

namespace AOMIntegrationTests.Helpers
{
    public abstract class BaseClass
    {
        public static readonly TimeSpan NoWait = new TimeSpan(0, 0, 0, 0);

        public ScenarioContext scenarioContext;

        public BaseClass()
        {
            scenarioContext = ScenarioContext.Current;
        }

        protected RemoteWebDriver Driver
        {
            get { return scenarioContext.Get<RemoteWebDriver>(); }
        }

        public void NavigateToUrl(string url)
        {
            Driver.Navigate().GoToUrl(url);
            try
            {
                var alert = Driver.SwitchTo().Alert();
                alert.Accept();
                alert = Driver.SwitchTo().Alert();
            }
            catch (NoAlertPresentException)
            {
                Console.WriteLine("Alert Window Not present");
            }
            catch (UnhandledAlertException)
            {
                Console.WriteLine("Unhandled, so switch again");
                Driver.SwitchTo().Alert().Accept();
            }
        }

        public void PerformanceReport(Pages page, Stopwatch stopwatch)
        {
            if (!ScenarioContext.Current.ScenarioInfo.Tags.Contains("LiveSmoke"))
            {
                new SQLQueries().InsertNewPerformanceRow(page, stopwatch.Elapsed.TotalSeconds);
            }
        }

        public IWebElement GetElementUsingLinkText(string linkText)
        {
            return Driver.FindElementByLinkText(linkText);
        }

        public string GetElementText(IWebElement element)
        {
            return element.Text;
        }

        public void ClickBackButton()
        {
            Driver.Navigate().Back();
        }

        public void ClickElementUsingJavaScript(IWebElement element)
        {
            IJavaScriptExecutor jse = Driver;
            const string script1 = @"document.querySelector({0}).click();";
            jse.ExecuteScript(script1, element);
            WaitForPageToLoad(30);
        }

        public void ClickElementUsingActions(IWebElement element)
        {
            Actions actions = new Actions(Driver);
            actions.MoveToElement(element).Click().Build().Perform();
        }

        public void MoveToElementUsingActions(IWebElement element)
        {
            Actions actions = new Actions(Driver);
            actions.MoveToElement(element).Build().Perform();
        }

        public void WaitForPageToLoad(int timespan)
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            try
            {
                wait.Until(
                    d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").ToString()
                        .Equals("complete"));
            }
            catch (UnhandledAlertException)
            {
                Driver.SwitchTo().Alert().Accept();
                wait.Until(
                    d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").ToString()
                        .Equals("complete"));
            }
        }

        public void WaitForElementToPresent(IWebElement element, int timespan)
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            wait.Until(d => element.Displayed);
        }

        public void WaitForElementToPresent(string elementcsslocator, int timespan)
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(elementcsslocator)));
        }

        public void WaitForCssElementToContain(string elementcsslocator, int timespan, string text)
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.CssSelector(elementcsslocator), text));
        }

        public void WaitForElementToContain(IWebElement element, int timespan, string text)
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            wait.Until(d => element.Text.Trim().Contains(text));
        }

        public bool WaitForListElementToPresent(IList<IWebElement> element, int timespan)
        {
            Func<IWebDriver, bool> testCondition = (x) => element.Count > 0;

            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            try
            {
                wait.Until(testCondition);
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine("Exception in WaitforAttribute: " + e);
            }

            // Return a value to indicate if our wait was successful
            return testCondition.Invoke(null);
        }

        public void WaitForModalBackdropToDisappear()
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, 10));

            //set wait timeout to 1 seconds as otherwise it will take 30 seconds to say nothing the modal-backdrop is invisible
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("modal-backdrop")));

            //reset wait to 30 seconds
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
        }

        public void WaitElementTobeClickable(IWebElement element, int timespan)
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public void WaitElementTobeClickableIdlookup(string element, int timespan)
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            var result = wait.Until(ExpectedConditions.ElementToBeClickable(By.Id(element)));
        }

        public void WaitElementTobeClickableClasslookup(string element, int timespan)
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timespan));
            var result = wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName(element)));
        }

        public bool WaitForAttribute(IWebElement element, string attributeName, string attributeValue, int timeOut)
        {
            // Build a function with a signature compatible with the WebDriverWait.Until method
            Func<IWebDriver, bool> testCondition = (x) => element.GetAttribute(attributeName).Equals(attributeValue);

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeOut));

            // Wait until either the test condition is true or timeout
            try
            {
                wait.Until(testCondition);
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine("Exception in WaitforAttribute: " + e);
            }

            // Return a value to indicate if our wait was successful
            return testCondition.Invoke(null);
        }

        public bool WaitUntilAttributeNotPresent(IWebElement element, String attributeName)
        {
            Func<IWebDriver, bool> testCondition = (x) => element.GetAttribute(attributeName) == null;

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            try
            {
                wait.Until(testCondition);
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine("Exception in WaitforAttribute: " + e);
            }

            // Return a value to indicate if our wait was successful
            return testCondition.Invoke(null);
        }

        public void ExplicitWaitHard(int timeinsec)
        {
            Console.WriteLine("Waiting : " + timeinsec);
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeinsec));
        }

        public void ClickElementByLinkText(string linkText)
        {
            Driver.FindElementByLinkText(linkText).Click();
        }

        public static bool Exists(IWebElement element)
        {
            if (element == null)
                return false;
            return true;
        }

        public bool IsElementPresent(IWebElement element)
        {
            Driver.Manage().Timeouts().ImplicitlyWait(NoWait);
            try
            {
                return element.Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            finally
            {
                Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            }
        }

        public bool IsElementEnabled(IWebElement element)
        {
            Driver.Manage().Timeouts().ImplicitlyWait(NoWait);
            try
            {
                return element.Enabled;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void SelectDropDown(IWebElement element, string text)
        {
            SelectElement s = new SelectElement(element);
            s.SelectByText(text);
        }
    }
}