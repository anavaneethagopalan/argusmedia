﻿using System;
using System.Configuration;
using Argus.Transport.Configuration;
using MySql.Data.MySqlClient;

namespace AOMIntegrationTests.Helpers
{
    public class EnvironmentHelper
    {
        public static string environmentToUse = ConfigurationManager.AppSettings["Environment"];

        public static ConnectionConfiguration GetRabbitBusConnection()
        {
            ConnectionConfiguration aomConnection = null;
            Console.WriteLine("Connecting to " + ConfigurationManager.AppSettings["Environment"] + "Rabbit Bus");
            //  var environmentToUse = ConfigurationManager.AppSettings["Environment"];

            switch (environmentToUse.ToUpper())
            {
                case "LOCAL":
                    aomConnection = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus.local");
                    break;

                case "SYSTEST":
                    aomConnection = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus.sys");
                    break;

                case "UAT":
                    aomConnection = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus.uat");
                    break;

                case "LIVE":
                    //TODO: We don't really need this connection so just setting it to UAT
                    aomConnection = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus.uat");
                    break;

                default:
                    Console.WriteLine("Rabbit Bus Connection not been set");
                    break;
            }

            return aomConnection;
        }

        public MySqlConnection GetSQLConnection()
        {
            MySqlConnection aomsqlConnection = null;
            Console.WriteLine("Connecting to " + ConfigurationManager.AppSettings["Environment"] + " Database");

            //  var environmentToUse = ConfigurationManager.AppSettings["Environment"];

            switch (environmentToUse.ToUpper())
            {
                case "LOCAL":
                    Console.WriteLine("Local Connection");
                    aomsqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["aomlocal"].ToString());
                    break;

                case "SYSTEST":
                    aomsqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["aomsys"].ToString());
                    break;

                case "UAT":
                    aomsqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["aomuat"].ToString());
                    break;

                default:
                    Console.WriteLine("SQL AOM Connection not been set");
                    break;
            }

            return aomsqlConnection;
        }

        public MySqlConnection GetCrmSQLConnection()
        {
            MySqlConnection crmsqlConnection = null;
            Console.WriteLine("Connecting to " + ConfigurationManager.AppSettings["Environment"] + "Database");

            //  var environmentToUse = ConfigurationManager.AppSettings["Environment"];

            switch (environmentToUse.ToUpper())
            {
                case "LOCAL":
                    Console.WriteLine("Local Connection");
                    crmsqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["crmlocal"].ToString());
                    break;

                case "SYSTEST":
                    crmsqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["crmsys"].ToString());
                    break;

                case "UAT":
                    crmsqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["crmuat"].ToString());
                    break;

                default:
                    Console.WriteLine("SQL CRM Connection not been set");
                    break;
            }

            return crmsqlConnection;
        }
    }
}