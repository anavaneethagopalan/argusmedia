﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMIntegrationTests.Helpers
{
    public enum Pages
    {
        LoginPage,
        SettingsPage,
        DashboardPage,
        CounterPartyPage
    }
}