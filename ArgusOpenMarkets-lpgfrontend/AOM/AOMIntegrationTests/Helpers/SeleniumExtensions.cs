﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AOMIntegrationTests.Helpers
{
    public static class SeleniumExtensions
    {
        public static RemoteWebDriver Driver = ScenarioContext.Current.Get<RemoteWebDriver>();

        public static void IeSafeClick(this IWebElement element)
        {
            if (ConfigurationManager.AppSettings["Browser"].Equals("IE"))
            {
                //Actions action = new Actions(Driver);
                //action.MoveToElement(element).Perform();
                //action.Click(element).Perform();
                //return;

                element.SendKeys("");
                element.SendKeys(Keys.Enter);
            }

            Assert.IsNotNull(element);

            element.Click();
        }

        public static void ScrollToElement(this IWebElement element)
        {
            Actions action = new Actions(Driver);
            action.MoveToElement(element).Perform();
           
        }

        public static void HoverOverElement(this IWebElement element)
        {
            Actions action = new Actions(Driver);
            action.MoveToElement(element).Perform();

        }

}

}
