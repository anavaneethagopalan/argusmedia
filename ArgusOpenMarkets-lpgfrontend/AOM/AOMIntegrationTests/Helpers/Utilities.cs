﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace AOMIntegrationTests.Helpers
{
    public class Utilities : PageObjects
    {

        public void AreEqual(object expected, object actual)
        {
            Assert.AreEqual(expected, actual);
        }

        public void AreEqualMsg(object expected, object actual, string message)
        {
            Assert.AreEqual(expected, actual, message);
        }

        public void IsTrue(bool condition, string message)
        {
            Assert.IsTrue(condition, message);

        }

        public void IsFalse(bool condition, string message)
        {
            Assert.IsFalse(condition, message);

        }

        public void IsSubsetOf(ICollection smallset, ICollection largeset, string message)
        {
            CollectionAssert.IsSubsetOf(smallset, largeset, message);
        }

        public void AreCollectionEqual(ICollection expected, ICollection actual, string message)
        {
            CollectionAssert.AreEqual(expected, actual, message);
        }

        /*  public void IsCollectionNotEmpty(ICollection set, string message)
          {
              CollectionAssert.IsNotEmpty(set, message, null);
          }

          public void IsCollectionEmpty(ICollection set, string message)
          {
              CollectionAssert.IsEmpty(set, message, null);
          }*/

        public static void Empty(DirectoryInfo directory)
        {
            if (directory.Exists)
                foreach (FileInfo file in directory.GetFiles())
                    file.Delete();

        }

        public static void GrantAccess(string fullPath)
        {
            DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
            //return true;
        }


        public class OrderProperties
        {
            public string Date { get; set; }

            public string BrokerName { get; set; }

            public string OrderType { get; set; }

            public string Principal { get; set; }

            public string ProductName { get; set; }
        }

        public class ScenarioInformation
        {
            public string ordertype { get; set; }
            public string productname { get; set; }
            public string price { get; set; }

        }

        public class MarketTickerDisplayItem
        {
            public string BidOrAsk;

            public string DateTimeCreated;

            public string MarketTickerItem;
        }

        public class PermissionMatrix
        {
            public string buy;
            public string sell;
            public string allowordeny1;
            public string allowordeny2;
        }


        public static readonly Dictionary<string, string> colordict = new Dictionary<string, string>
        {
        {"rgb(6, 6, 6)", "BLACK"},
        {"rgb(8, 80, 112)", "BLUE"},
        {"rgb(23, 47, 58)", "BLUEGREEN"},
        {"rgb(66, 74, 81)", "BLUEGREY"},
        {"rgb(204, 0, 0)", "BRIGHTRED"},
        {"rgb(30, 5, 4)", "BROWN"},
        {"rgb(255, 204, 0)", "CANARYYELLOW"},
        {"rgb(65, 65, 65)", "CHARCOALGREY"},
        {"rgb(82, 32, 30)", "CRIMSONRED"},
        {"rgb(0, 51, 51)", "DARKGREEN"},
        {"rgb(29, 33, 36)", "DARKGREY"},
        {"rgb(102, 102, 102)", "DOVEGREY"},
        {"rgb(0, 111, 163)", "LIGHTBLUE"},
        {"rgb(26, 136, 186)", "LIGHTERBLUE"},
        {"rgb(0, 128, 0)", "LIGHTGREEN"},
        {"rgb(42, 46, 51)", "MARKETTICKERLIGHTBLACK"},
        {"rgb(211, 211, 211)", "MARKETTICKERGREY"},
        {"rgb(129, 6, 0)", "MAROONRED"},
        {"rgb(51, 51, 51)", "MINESHAFTGREY"},
        {"rgb(153, 153, 153)", "NEWSGREY"},
        {"rgb(120, 120, 120)", "NEWSLIGHTGREY"},
        {"rgb(2, 52, 52)", "OLIVE"},
        {"rgb(255, 116, 0)", "ORANGE"},
        {"rgb(204, 204, 204)", "PASTELGREY"},
        {"rgb(75, 154, 49)", "PEAGREEN"},
        {"rgb(49, 27, 28)", "RUSTBROWN"},
        {"rgb(7, 28, 37)", "SEAGREEN"},
        {"rgb(40, 40, 40)", "SHARKGREY"},
        {"rgb(24, 24, 24)", "SLATEGREY"},
        {"rgb(255, 255, 255)", "WHITE"},
        {"rgb(255, 255, 51)", "YELLOW"},
        {"rgb(255, 20, 147)","DEEPPINK"},
        {"rgba(255, 20, 147,1)","DEEPPINK"},
        {"rgba(87, 92, 96, 1)","GREY"},
        {"rgba(255, 116, 0, 1)", "ORANGE"},
        {"rgba(255, 255, 255, 1)", "WHITE"},
        {"rgba(255, 255, 51, 1)", "YELLOW"},
        {"rgba(102, 102, 102, 1)", "DOVEGREY"},
        {"rgba(1, 149, 231, 1)", "PUREBLUE"},
        {"rgba(255, 20, 147, 1)", "DEEPPINK" },
        {"rgba(6, 6, 6, 1)" , "BLACK"}


        };





    }


}
