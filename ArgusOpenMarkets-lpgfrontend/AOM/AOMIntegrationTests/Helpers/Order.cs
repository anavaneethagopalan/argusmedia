﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AOMIntegrationTests.Helpers
{
    public class Order
    {
        public long Id { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public Tenor Tenor { get; set; }
        public OrderType OrderType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public long LastUpdatedUserId { get; set; }
        public long? EnteredByUserId { get; set; }
        public string LastUpdatedUserName { get; set; }
        public long? PrincipalUserId { get; set; }
        public DateTime DeliveryEndDate { get; set; }
        public DateTime DeliveryStartDate { get; set; }
        public long? BrokerId { get; set; }
        public long? BrokerOrganisationId { get; set; }
        public string BrokerOrganisationName { get; set; }
        private string _brokerShortCode;
        public string BrokerShortCode
        {
            get
            {
                if (BrokerRestriction == BrokerRestriction.Any)
                {
                    return "*";
                }

                if (BrokerRestriction == BrokerRestriction.None)
                {
                    return null;
                }

                return _brokerShortCode;
            }

            set
            {
                _brokerShortCode = value;
            }
        }

        public long PrincipalOrganisationId { get; set; }
        public string PrincipalOrganisationName { get; set; }
        public string PrincipalOrganisationShortCode { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public OrderStatus LastOrderStatus { get; set; }
        public string Notes { get; set; }
        public ExecutionMode ExecutionMode { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public Boolean IsVirtual { get; set; }
        public BrokerRestriction BrokerRestriction { get; set; }
        public ExecutionInfo ExecutionInfo { get; set; } // Only ever passed in a client request, not stored.
        public bool CoBrokering { get; set; }
        public MetaData[] MetaData { get; set; }
    }

    public class Tenor
    {
        public DeliveryLocation DeliveryLocation { get; set; }
        public string DeliveryStartDate { get; set; }
        public string DeliveryEndDate { get; set; }
        public string RollDate { get; set; }
        public long Id { get; set; }
        public long MinimumDeliveryRange { get; set; }
        public string DefaultStartDate { get; set; }
        public string DefaultEndDate { get; set; }
    }

    public class DeliveryLocation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
    }

    public class ExecutionInfo
    {
        public long? BrokerOrganisationId { get; set; }
        public long PrincipalOrganisationId { get; set; }
    }

    public class MetaData
    {
        public long ProductMetaDataId { get; set; }
        public string DisplayName { get; set; }
        public string DisplayValue { get; set; }
        public int? ItemValue { get; set; }
        public long? MetaDataListItemId { get; set; }
        public MetaDataTypes ItemType { get; set; }
    }

    public class Message
    {
        public MessageType MessageType { get; set; }
        public MessageAction MessageAction { get; set; }
        public object MessageBody { get; set; }
    }

    public enum MessageType
    {
        Order
    }

    public enum MessageAction
    {
        Create,
        Execute,
        Hold,
        Kill,
    }

    public enum OrderType
    {
        None,
        Bid,
        Ask
    }

    public enum BrokerRestriction
    {
        Any = 1,
        None,
        Specific
    }

    public enum OrderStatus
    {
        None = 1,
        Active = 2,
        Held = 3,
        VoidAfterExecuted = 5,
        Executed = 6,
        Killed = 7
    }

    public enum ExecutionMode
    {
        None,
        Internal,
        External
    }

    public enum MetaDataTypes
    {
        IntegerEnum = 10,
        String = 20,
        Unknown = 0
    }

    public class DateTimeConverter : JavaScriptConverter
    {
        public override IEnumerable<Type> SupportedTypes
        {
            get { return new List<Type>() { typeof(DateTime), typeof(DateTime?) }; }
        }

        public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            if (obj == null) return result;
            result["DateTime"] = ((DateTime)obj).ToString("yyyy-MM-dd HH.mm.ss");
            return result;
        }

        public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            if (dictionary.ContainsKey("DateTime"))
                return new DateTime(long.Parse(dictionary["DateTime"].ToString()), DateTimeKind.Unspecified);
            return null;
        }
    }

}

