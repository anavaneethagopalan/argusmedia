﻿using System;
using System.Configuration;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using TechTalk.SpecFlow;
using System.IO;
using OpenQA.Selenium;
using System.Drawing.Imaging;
using System.Linq;
using OpenQA.Selenium.IE;
using System.Reflection;
using AOMIntegrationTests.AomBus;
using OpenQA.Selenium.Edge;
using System.Drawing;
using System.Collections.Generic;

namespace AOMIntegrationTests.Helpers
{
    [Binding]
    public class Events
    {
        public static RemoteWebDriver Driver = null;

        public ScenarioContext scenarioContext;

        public string Screenshotfolder = ConfigurationManager.AppSettings["WebDriverScreenshotsFolder"];
        private string filepath => Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Screenshotfolder);

        public Events(ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
        }

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            Console.WriteLine("Test URL: " + ConfigurationManager.AppSettings["TestUrl"]);
            Console.WriteLine("Environment " + ConfigurationManager.AppSettings["Environment"]);
            Console.WriteLine("Browser: " + ConfigurationManager.AppSettings["Browser"]);
            Console.WriteLine("Deleting Screenshots from previous Run");
            Console.WriteLine("Application Path " + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            var screenShotsFolderPath = ConfigurationManager.AppSettings["WebDriverScreenshotsFolder"];
            string filepath = Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), screenShotsFolderPath);
            if (Directory.Exists(filepath))
            {
                DirectoryInfo directory = new DirectoryInfo(filepath);
                Utilities.Empty(directory);
            }
            else
            {
                Console.WriteLine("File Does not exist....Creating File");
                Directory.CreateDirectory(filepath);
                Utilities.GrantAccess(filepath);
                Console.WriteLine("File Created");
            }
            // Purge Market Ticker
            if (ConfigurationManager.AppSettings["Environment"] != "Live")
            {
                var purgeorder = new BusPurgeOrders();
                purgeorder.PurgeMarketTicker();
            }
        }

        [BeforeScenario]
        public void InitializeDriver()
        {
            if (FeatureContext.Current.FeatureInfo.Tags.Contains("AOM"))
                scenarioContext["currentUrl"] = ConfigurationManager.AppSettings["AOMUrl"];
            else if (FeatureContext.Current.FeatureInfo.Tags.Contains("CME"))
                scenarioContext["currentUrl"] = ConfigurationManager.AppSettings["CMEUrl"];
            else
            {
                scenarioContext["currentUrl"] = ConfigurationManager.AppSettings["TestUrl"];
            }

            Console.WriteLine("Initializing Test URL: " + scenarioContext["currentUrl"].ToString());
            Console.WriteLine("Initializing Driver: " + ConfigurationManager.AppSettings["Environment"]);
            scenarioContext["currentEnv"] = ConfigurationManager.AppSettings["Environment"];
            if (Driver == null)
            {
                switch (ConfigurationManager.AppSettings["Browser"])
                {
                    case "IE":
                        var ieoptions = new InternetExplorerOptions();
                        ieoptions.EnsureCleanSession = true;
                        ieoptions.IgnoreZoomLevel = true;
                        ieoptions.EnablePersistentHover = false;
                        ieoptions.EnableNativeEvents = false;
                        Driver = new InternetExplorerDriver(ieoptions);
                        //DesiredCapabilities capabilityIe = DesiredCapabilities.InternetExplorer();
                        //SetBrowserCapabilities(capabilityIe);
                        Console.WriteLine("STARTED ON -- IE");
                        break;

                    case "Firefox":
                        Driver = new FirefoxDriver();
                        Console.WriteLine("STARTED ON -- FIREFOX");
                        break;

                    case "Chrome":
                        var chromeoptions = new ChromeOptions();
                        Driver = new ChromeDriver(chromeoptions);
                        /*  DesiredCapabilities capabilityChrome = DesiredCapabilities.Chrome();
                          SetBrowserCapabilities(capabilityChrome);*/
                        Console.WriteLine("STARTED ON -- CHROME");
                        break;

                    case "Edge":
                        Driver = new EdgeDriver();
                        break;

                    default:
                        Console.WriteLine("Browser AppConfig not set properly");
                        break;
                }

                Driver.Manage().Window.Maximize();
                Driver.Manage().Cookies.DeleteAllCookies();
                Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            }
            scenarioContext.Set(Driver);
        }

        private Bitmap TakeScreenShot()
        {
            Bitmap stitchedImage = null;
            try
            {
                long totalwidth1 = (long)((IJavaScriptExecutor)Driver).ExecuteScript("return document.body.offsetWidth");//documentElement.scrollWidth");

                long totalHeight1 = (long)((IJavaScriptExecutor)Driver).ExecuteScript("return  document.body.parentNode.scrollHeight");

                int totalWidth = (int)totalwidth1;
                int totalHeight = (int)totalHeight1;

                // Get the Size of the Viewport
                long viewportWidth1 = (long)((IJavaScriptExecutor)Driver).ExecuteScript("return document.body.clientWidth");//documentElement.scrollWidth");
                long viewportHeight1 = (long)((IJavaScriptExecutor)Driver).ExecuteScript("return window.innerHeight");//documentElement.scrollWidth");

                int viewportWidth = (int)viewportWidth1;
                int viewportHeight = (int)viewportHeight1;

                // Split the Screen in multiple Rectangles
                List<Rectangle> rectangles = new List<Rectangle>();
                // Loop until the Total Height is reached
                for (int i = 0; i < totalHeight; i += viewportHeight)
                {
                    int newHeight = viewportHeight;
                    // Fix if the Height of the Element is too big
                    if (i + viewportHeight > totalHeight)
                    {
                        newHeight = totalHeight - i;
                    }
                    // Loop until the Total Width is reached
                    for (int ii = 0; ii < totalWidth; ii += viewportWidth)
                    {
                        int newWidth = viewportWidth;
                        // Fix if the Width of the Element is too big
                        if (ii + viewportWidth > totalWidth)
                        {
                            newWidth = totalWidth - ii;
                        }

                        // Create and add the Rectangle
                        Rectangle currRect = new Rectangle(ii, i, newWidth, newHeight);
                        rectangles.Add(currRect);
                    }
                }

                // Build the Image
                stitchedImage = new Bitmap(totalWidth, totalHeight);
                // Get all Screenshots and stitch them together
                Rectangle previous = Rectangle.Empty;
                foreach (var rectangle in rectangles)
                {
                    // Calculate the Scrolling (if needed)
                    if (previous != Rectangle.Empty)
                    {
                        int xDiff = rectangle.Right - previous.Right;
                        int yDiff = rectangle.Bottom - previous.Bottom;
                        // Scroll
                        //selenium.RunScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        ((IJavaScriptExecutor)Driver).ExecuteScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        System.Threading.Thread.Sleep(200);
                    }

                    // Take Screenshot
                    var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();

                    // Build an Image out of the Screenshot
                    Image screenshotImage;
                    using (MemoryStream memStream = new MemoryStream(screenshot.AsByteArray))
                    {
                        screenshotImage = Image.FromStream(memStream);
                    }

                    // Calculate the Source Rectangle
                    Rectangle sourceRectangle = new Rectangle(viewportWidth - rectangle.Width, viewportHeight - rectangle.Height, rectangle.Width, rectangle.Height);

                    // Copy the Image
                    using (Graphics g = Graphics.FromImage(stitchedImage))
                    {
                        g.DrawImage(screenshotImage, rectangle, sourceRectangle, GraphicsUnit.Pixel);
                    }

                    // Set the Previous Rectangle
                    previous = rectangle;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error on taking screenshots: " + e);
            }
            return stitchedImage;
        }

        [AfterScenario]
        public void AfteScenario()
        {
            var testcontext = SpecFlowTestContext.TestContext;
            var screenshotfilename = string.Format(@"{0}\{1}_{2}.jpeg", filepath,
                ScenarioContext.Current.ScenarioInfo.Title,
                DateTime.Now.ToString(DateTime.Now.ToString("ddMMyyHHmmssff")));

            if (ScenarioContext.Current.TestError != null)
            {
                var image = TakeScreenShot();

                image.Save(screenshotfilename, ImageFormat.Jpeg);

                testcontext.AddResultFile(screenshotfilename);
                KillDriver();
            }
        }

        /*  [AfterScenario]
          public void AfteScenario()
          {
              Console.WriteLine("Calling After Scenario");
              if (scenarioContext.TestError != null)
              {
                  var screenShotsFolderPath = ConfigurationManager.AppSettings["WebDriverScreenshotsFolder"];
                  string exepath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                  string filepath = Path.Combine(exepath, screenShotsFolderPath);
                  bool exists = Directory.Exists(filepath);
                  if (!exists)
                      Directory.CreateDirectory(filepath);
                  Utilities.GrantAccess(filepath);

                  string timestamp = DateTime.Now.ToString("yyyymmdd_hh-mm-ss");
                  var lengthofScenarioTitle = ScenarioContext.Current.ScenarioInfo.Title.Length;
                  string imagetitle;
                  if (lengthofScenarioTitle > 150)
                  {
                       imagetitle = scenarioContext.ScenarioInfo.Title.Substring(0, 150) + "_" + timestamp;
                  }
                  else
                  {
                       imagetitle = scenarioContext.ScenarioInfo.Title + "_" + timestamp;
                  }

                  var imagepath = Path.Combine(filepath, imagetitle);
                  try
                  {
                      Console.WriteLine("Taking Screenshots of the Error");
                      var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
                      screenshot.SaveAsFile(imagepath + ".png", ImageFormat.Png);
                  }
                  catch (Exception e)
                  {
                      Console.WriteLine("Unable to take screenshots");
                      Console.WriteLine(e.Message);
                  }
                  //Kill the driver and start again for failed test as we could be anywhere.
                  KillDriver();
              }
          } */

        [AfterTestRun]
        public static void AfterTestRun()
        {
            if (ConfigurationManager.AppSettings["Environment"] != "Live")
            {
                new BusPurgeOrders().DisposeAOM();
            }
            if (Driver != null)
            {
                Console.WriteLine("Calling Finally: Driver is not null");
                KillDriver();
                Console.WriteLine("AfterTestRun End");
            }
        }

        private static void KillDriver()
        {
            Driver.Quit();
            Driver.Dispose();
            Driver = null;
        }

        public void SetBrowserCapabilities(DesiredCapabilities capabilities)
        {
            //    var browserstackuser = ConfigurationManager.AppSettings["browserstackuser"];
            //    var browserstackkey = ConfigurationManager.AppSettings["browserstackkey"];

            //    capabilities.SetCapability("browserstack.user", browserstackuser);
            //    capabilities.SetCapability("browserstack.key", browserstackkey);
            //    capabilities.SetCapability("browserstack.debug", "true");
            //    capabilities.SetCapability("browser", ConfigurationManager.AppSettings["Browser"]);
            //    capabilities.SetCapability("browser_version", ConfigurationManager.AppSettings["Browser_version"]);
            // //   capabilities.SetCapability("os", ConfigurationManager.AppSettings["OS"]);
            ////    capabilities.SetCapability("os", ConfigurationManager.AppSettings["OS_Version"]);

            //    Driver = new RemoteWebDriver(new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capabilities);
        }
    }
}