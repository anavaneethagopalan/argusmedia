﻿namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    using AOM.App.Domain.Dates;
    using AOM.Services.ProductService;
    using AOM.Transport.Events.Products;
    using AOM.Transport.Service.ProductsProcessor.Consumers;
    using Argus.Transport.Infrastructure;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using AOM.App.Domain.Entities;
    using AOM.Transport.Events;

    [TestFixture]
    public class TimersProductStatusServiceConsumerTests
    {
        private TimersProductStatusServiceConsumer _consumer;
        private Mock<IBus> _mockBus;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private Mock<IProductService> _mockProductService;
        private Mock<IProductStatusHelper> _mockProductStatusHelper;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockProductService = new Mock<IProductService>();
            _mockProductStatusHelper = new Mock<IProductStatusHelper>();
            _consumer = new TimersProductStatusServiceConsumer(_mockBus.Object,
                _mockProductService.Object,
                _mockDateTimeProvider.Object,
                _mockProductStatusHelper.Object);
        }

        [Test]
        public void ShouldSubscribeToAomProductResponseOnStart()
        {
            _consumer.Start();
            _mockBus.Verify(
                b => b.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomProductResponse>>()));
        }

        [Test]
        [TestCase(MarketStatus.Open)]
        [TestCase(MarketStatus.Closed)]
        public void ShouldNotSendMessageIfProductStatusIsAsExpected(MarketStatus status)
        {
            var fakeProduct = new Product
            {
                IsInternal = false,
                Status = status,
                IgnoreAutomaticMarketStatusTriggers = false
            };
            _mockProductService.Setup(s => s.GetAllProducts()).Returns(new List<Product> {fakeProduct});
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 12, 0, 0));
            _mockProductStatusHelper.Setup(h => h.DetermineExpectedMarketStatus(It.IsAny<Product>())).Returns(status);
            _consumer.InitialiseAndWarmupConsumer();
            _mockBus.Verify(b => b.Publish(It.IsAny<AomMarketStatusChangeRequest>()), Times.Never);
        }


        [Test]
        [TestCase(MarketStatus.Open, MarketStatus.Closed, MessageAction.Close)]
        [TestCase(MarketStatus.Closed, MarketStatus.Open, MessageAction.Open)]
        public void ShouldSendMessageIfProductStatusIsNotAsExpected(
            MarketStatus productStatus, MarketStatus expectedStatus, MessageAction action)
        {
            var fakeProduct = MakeFakeProduct(productStatus, false);
            _mockProductService.Setup(s => s.GetAllProducts()).Returns(new List<Product> {fakeProduct});
            _mockProductService.Setup(s => s.GetProduct(fakeProduct.ProductId)).Returns(fakeProduct);
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 12, 0, 0));
            _mockProductStatusHelper.Setup(h => h.DetermineExpectedMarketStatus(It.IsAny<Product>()))
                .Returns(expectedStatus);
            _mockProductStatusHelper.Setup(h => h.GetNextMarketStatusChangeTime(It.IsAny<Product>()))
                .Returns(new DateTime(2525, 1, 1));
            _consumer.InitialiseAndWarmupConsumer();
            _mockBus.Verify(b => b.Publish(It.Is<AomMarketStatusChangeRequest>(r => r.MessageAction == action)),
                Times.Once);
        }

        [Test]
        [TestCase(MarketStatus.Open, MarketStatus.Closed)]
        [TestCase(MarketStatus.Closed, MarketStatus.Open)]
        public void ShouldNotSendMessageIfProductStatusIsNotAsExpectedButProductIgnoresTriggers(
            MarketStatus productStatus, MarketStatus expectedStatus)
        {
            var fakeProduct = MakeFakeProduct(productStatus, true);
            _mockProductService.Setup(s => s.GetAllProducts()).Returns(new List<Product> {fakeProduct});
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 12, 0, 0));
            _mockProductStatusHelper.Setup(h => h.DetermineExpectedMarketStatus(It.IsAny<Product>()))
                .Returns(expectedStatus);
            _consumer.InitialiseAndWarmupConsumer();
            _mockBus.Verify(b => b.Publish(It.IsAny<AomMarketStatusChangeRequest>()),
                Times.Never);
        }

        [TestCase(MarketStatus.Open, MarketStatus.Closed, DayOfWeek.Saturday)]
        [TestCase(MarketStatus.Closed, MarketStatus.Open, DayOfWeek.Saturday)]
        [TestCase(MarketStatus.Open, MarketStatus.Closed, DayOfWeek.Sunday)]
        [TestCase(MarketStatus.Closed, MarketStatus.Open, DayOfWeek.Sunday)]
        public void ShouldNotSendMessageIfProductStatusIsNotAsExpectedButIsCurrentlyAWeekend(
            MarketStatus productStatus, MarketStatus expectedStatus, DayOfWeek dayOfWeek)
        {
            var fakeProduct = MakeFakeProduct(productStatus, true);
            int day = dayOfWeek == DayOfWeek.Saturday ? 20 : 21;
            _mockProductService.Setup(s => s.GetAllProducts()).Returns(new List<Product> {fakeProduct});
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, day, 12, 0, 0));
            _mockProductStatusHelper.Setup(h => h.DetermineExpectedMarketStatus(It.IsAny<Product>()))
                .Returns(expectedStatus);
            _consumer.InitialiseAndWarmupConsumer();
            _mockBus.Verify(b => b.Publish(It.IsAny<AomMarketStatusChangeRequest>()),
                Times.Never);
        }

        [Test]
        public void StartingProductsProcessorShouldGenerateTheNextTimeToRunFromTheEarliestProductOpenTime()
        {
            List<Product> allProducts = MakeListAllProducts();

            _mockBus = new Mock<IBus>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockProductService = new Mock<IProductService>();

            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2016, 1, 14, 9, 0, 0));

            var productStatusHelper = new ProductStatusHelper(_mockDateTimeProvider.Object);
            _consumer = new TimersProductStatusServiceConsumer(_mockBus.Object,
                _mockProductService.Object,
                _mockDateTimeProvider.Object,
                productStatusHelper);

            _mockProductService.Setup(ps => ps.GetAllProducts()).Returns(allProducts);

            var subs = StartConsumer();

            Assert.That(_consumer.NextTimeToRun, Is.EqualTo(new DateTime(2016, 1, 14, 12, 00, 0)));
        }

        [Test]
        public void StartingProductsProcessorShouldGenerateTheNextTimeToRunRunningOnAWeekend()
        {
            List<Product> allProducts = MakeListAllProducts();

            _mockBus = new Mock<IBus>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockProductService = new Mock<IProductService>();

            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2016, 1, 16, 9, 0, 0));

            var productStatusHelper = new ProductStatusHelper(_mockDateTimeProvider.Object);
            _consumer = new TimersProductStatusServiceConsumer(_mockBus.Object,
                _mockProductService.Object,
                _mockDateTimeProvider.Object,
                productStatusHelper);

            _mockProductService.Setup(ps => ps.GetAllProducts()).Returns(allProducts);

            var subs = StartConsumer();

            Assert.That(_consumer.NextTimeToRun, Is.EqualTo(new DateTime(2016, 1, 16, 12, 00, 0)));
        }

        [Test]
        public void StartingProductsProcessorShouldGenerateTheNextTimeToRunAs12HoursWhenTheNextTimeToRunIsGreaterThan12Hours()
        {
            List<Product> allProducts = new List<Product>();
            allProducts.Add(
                new Product { ProductId = 1, OpenTime = new TimeSpan(15, 00, 00), CloseTime = new TimeSpan(21, 10, 00) });
            allProducts.Add(
                new Product { ProductId = 2, OpenTime = new TimeSpan(15, 10, 00), CloseTime = new TimeSpan(21, 20, 00) });
            allProducts.Add(new Product
            {
                ProductId = 2,
                OpenTime = new TimeSpan(19, 00, 00),
                CloseTime = new TimeSpan(15, 20, 00)
            });

            _mockBus = new Mock<IBus>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockProductService = new Mock<IProductService>();

            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2016, 1, 16, 1, 0, 0));

            var productStatusHelper = new ProductStatusHelper(_mockDateTimeProvider.Object);
            _consumer = new TimersProductStatusServiceConsumer(_mockBus.Object,
                _mockProductService.Object,
                _mockDateTimeProvider.Object,
                productStatusHelper);

            _mockProductService.Setup(ps => ps.GetAllProducts()).Returns(allProducts);

            var subs = StartConsumer();

            Assert.That(_consumer.NextTimeToRun, Is.EqualTo(new DateTime(2016, 1, 16, 13, 00, 0)));
        }

        [Test]
        public void StartingProductsProcessorShouldGenerateTheNextTimeTheTimersNeedToRunWhenANewAomProductResponseIsReceivedAndItsAWeekDay()
        {
            var aomMarketStatusChangeEvent = new AomProductResponse
            {
                Product = new Product {OpenTime = new TimeSpan(14, 30, 00), CloseTime = new TimeSpan(15, 30, 00)},
                Message =
                    new Message
                    {
                        MessageAction = MessageAction.Update,
                        MessageType = MessageType.Product,
                        ClientSessionInfo = new ClientSessionInfo {SessionId = "123"}
                    }
            };

            List<Product> allProducts = new List<Product>();
            allProducts.Add(
                new Product {ProductId = 1, OpenTime = new TimeSpan(15, 00, 00), CloseTime = new TimeSpan(15, 10, 00)});
            allProducts.Add(
                new Product {ProductId = 2, OpenTime = new TimeSpan(15, 10, 00), CloseTime = new TimeSpan(15, 20, 00)});

            _mockBus = new Mock<IBus>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockProductService = new Mock<IProductService>();

            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2016, 1, 14, 12, 0, 0));

            var productStatusHelper = new ProductStatusHelper(_mockDateTimeProvider.Object);
            _consumer = new TimersProductStatusServiceConsumer(_mockBus.Object,
                _mockProductService.Object,
                _mockDateTimeProvider.Object,
                productStatusHelper);

            _mockProductService.Setup(ps => ps.GetAllProducts()).Returns(allProducts);

            var subs = StartConsumer();

            var onAomProductResponse = subs.AomProductResponse;
            onAomProductResponse(aomMarketStatusChangeEvent);

            Assert.That(_consumer.NextTimeToRun, Is.EqualTo(new DateTime(2016, 1, 14, 14, 30, 0)));
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            var subscriptionId = string.Format("{0}.{1}", GetType().Name, System.Environment.MachineName);

            _mockBus.Setup(b => b.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomProductResponse>>()))
                .Callback<string, Action<AomProductResponse>>((s, a) => subs.AomProductResponse = a);

            _consumer.Start();
            _consumer.InitialiseAndWarmupConsumer();

            return subs;
        }

        public class SubscribedActions
        {
            public Action<AomProductResponse> AomProductResponse { get; set; }
        }

        private List<Product> MakeListAllProducts()
        {
            var products = new List<Product>
            {
                new Product {ProductId = 1, OpenTime = new TimeSpan(15, 00, 00), CloseTime = new TimeSpan(15, 10, 00)},
                new Product {ProductId = 2, OpenTime = new TimeSpan(15, 10, 00), CloseTime = new TimeSpan(15, 20, 00)},
                new Product
                {
                    ProductId = 2,
                    OpenTime = new TimeSpan(12, 00, 00),
                    CloseTime = new TimeSpan(15, 20, 00)
                }
            };

            return products;
        }

        private Product MakeFakeProduct(MarketStatus productStatus, bool ignoreMarketStatusTriggers)
        {
            return new Product
            {
                ProductId = 12345,
                IsInternal = false,
                Status = productStatus,
                IgnoreAutomaticMarketStatusTriggers = ignoreMarketStatusTriggers
            };
        }
    }
}