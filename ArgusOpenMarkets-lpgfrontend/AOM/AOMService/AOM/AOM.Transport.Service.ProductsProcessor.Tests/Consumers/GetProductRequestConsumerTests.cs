﻿namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    using AOM.App.Domain.Entities;
    using AOM.Services.ProductService;
    using AOM.Transport.Events.Products;
    using AOM.Transport.Service.ProductsProcessor.Consumers;
    using Argus.Transport.Infrastructure;
    using log4net.Core;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Utils.Logging;

    [TestFixture]
    public class GetProductRequestConsumerTests
    {
        private GetProductRequestConsumer _getProductRequestConsumer;

        private Mock<IProductService> _mockProductService;

        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockProductService = new Mock<IProductService>();
            _getProductRequestConsumer = new GetProductRequestConsumer(_mockProductService.Object, _mockBus.Object);
        }

        [Test]
        public void ConsumeGetProductConfigRequestShouldNotPublishAnythingOnTheBusIfThereAreNoProductDefinitions()
        {
            var emptyList = new List<ProductDefinitionItem>();
            _mockProductService.Setup(m => m.GetAllProductDefinitions()).Returns(emptyList);

            GetProductConfigRequest request = null;
            _getProductRequestConsumer.ConsumeGetProductConfigRequest(request);

            _mockBus.Verify(m => m.Publish(It.IsAny<GetProductConfigResponse>()), Times.Never);

        }

        [Test]
        public void ConsumeGetProductConfigRequestShouldMakePublishAMessageOnTheBusForAllProductDefinitionsReturned()
        {
            var activeProductDefinitions = MakeActiveProductDefinitions();
            _mockProductService.Setup(m => m.GetAllProductDefinitions()).Returns(activeProductDefinitions);

            GetProductConfigRequest request = null;
            _getProductRequestConsumer.ConsumeGetProductConfigRequest(request);

            _mockBus.Verify(m => m.Publish(It.IsAny<GetProductConfigResponse>()), Times.Exactly(2));
        }

        [Test]
        public void ConsumeGetProductConfigRequestShouldMakePublishASingleMessageOnTheBusIfWePassInASingleProductId()
        {
            var activeProductDefinitions = MakeActiveProductDefinitions();
            _mockProductService.Setup(m => m.GetAllProductDefinitions()).Returns(activeProductDefinitions);
            _mockProductService.Setup(m => m.GetProductDefinition(It.IsAny<long>()))
                .Returns(activeProductDefinitions[0]);

            GetProductConfigRequest request = new GetProductConfigRequest {ProductId = 1};
            _getProductRequestConsumer.ConsumeGetProductConfigRequest(request);

            _mockBus.Verify(m => m.Publish(It.IsAny<GetProductConfigResponse>()), Times.Exactly(1));
        }

        [Test]
        public void ShouldSubscribeToGetProductConfigRequestOnStartup()
        {
            _getProductRequestConsumer.Start();
            _mockBus.Verify(b => b.Subscribe(It.IsAny<string>(), It.IsAny<Action<GetProductConfigRequest>>()),
                Times.Once);
        }

        [Test]
        public void ShouldSubscribeToGetProductIdsRequestOnStartup()
        {
            _getProductRequestConsumer.Start();
            _mockBus.Verify(b => b.Subscribe(It.IsAny<string>(), It.IsAny<Action<GetProductIdsRequest>>()),
                Times.Once);
        }

        [Test]
        public void ShouldRegisterToRespondToGetProductRequestRpc()
        {
            VerifyRespondFunctionRegistered<GetProductRequestRpc, AomProductResponse>();
        }

        [Test]
        public void ShouldRegisterToRespondToGetAllActiveProductsRequestRpc()
        {
            VerifyRespondFunctionRegistered<GetAllActiveProductsRequestRpc, List<Product>>();
        }

        [Test]
        public void ShouldRegisterToRespondToGetAllProductDefinitionsRequestRpc()
        {
            VerifyRespondFunctionRegistered<GetAllProductDefinitionsRequestRpc, List<ProductDefinitionItem>>();
        }

        [Test]
        public void ShouldReturnCorrectProductFromGetProductRequest()
        {
            const int fakeProductId = 1;
            _mockProductService.Setup(p => p.GetProduct(fakeProductId)).Returns(MakeProduct(fakeProductId));
            var responseFn = VerifyRespondFunctionRegistered<GetProductRequestRpc, AomProductResponse>();

            var response = responseFn(new GetProductRequestRpc {Id = fakeProductId});

            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Product);
            Assert.AreEqual(fakeProductId, response.Product.ProductId, "Incorrect product ID");
        }

        [Test]
        public void ShouldReturnNullProductIfProductServiceThrowsForGetProduct()
        {
            const int fakeProductId = 1;
            _mockProductService.Setup(p => p.GetProduct(fakeProductId)).Throws<ApplicationException>();
            var responseFn = VerifyRespondFunctionRegistered<GetProductRequestRpc, AomProductResponse>();

            var response = responseFn(new GetProductRequestRpc {Id = fakeProductId});

            Assert.IsNotNull(response);
            Assert.IsNull(response.Product);
        }

        [Test]
        public void ShouldReturnAllProductsFromGetAllActiveProductsRequest()
        {
            const int fakeProductId1 = 1;
            const int fakeProductId2 = 2;
            _mockProductService.Setup(p => p.GetAllProducts())
                .Returns(new List<Product> {MakeProduct(fakeProductId1), MakeProduct(fakeProductId2)});
            var responseFn = VerifyRespondFunctionRegistered<GetAllActiveProductsRequestRpc, List<Product>>();

            var products = responseFn(new GetAllActiveProductsRequestRpc());

            Assert.IsNotNull(products);
            Assert.AreEqual(2, products.Count, "Incorrect number of products returned");
            Assert.AreEqual(1, products.Count(p => p.ProductId == fakeProductId1));
            Assert.AreEqual(1, products.Count(p => p.ProductId == fakeProductId2));
        }

        [Test]
        public void ShouldReturnEmptyListIfProductServiceThrowsForGetAllActiveProducts()
        {
            _mockProductService.Setup(p => p.GetAllProducts()).Throws<ApplicationException>();
            var responseFn = VerifyRespondFunctionRegistered<GetAllActiveProductsRequestRpc, List<Product>>();

            var products = responseFn(new GetAllActiveProductsRequestRpc());

            Assert.IsNotNull(products);
            Assert.IsEmpty(products);
        }

        [Test]
        public void ShouldReturnAllActiveProductDefinitionsFromGetAllProductDefinitionsRequest()
        {
            const int fakeProductId1 = 1;
            const int fakeProductId2 = 2;
            _mockProductService.Setup(p => p.GetAllProductDefinitions())
                .Returns(new List<ProductDefinitionItem>
                {
                    MakeProductDefinition(fakeProductId1),
                    MakeProductDefinition(fakeProductId2)
                });
            var responseFn =
                VerifyRespondFunctionRegistered<GetAllProductDefinitionsRequestRpc, List<ProductDefinitionItem>>();

            var definitions = responseFn(new GetAllProductDefinitionsRequestRpc());

            Assert.IsNotNull(definitions);
            Assert.AreEqual(2, definitions.Count, "Incorrect number of product definitions returned");
            Assert.AreEqual(1, definitions.Count(d => d.ProductId == fakeProductId1));
            Assert.AreEqual(1, definitions.Count(d => d.ProductId == fakeProductId2));
        }

        [Test]
        public void ShouldReturnAnEmptyFromGetAllProductDefinitionsRequestIfProductSericeThrows()
        {
            _mockProductService.Setup(p => p.GetAllProductDefinitions()).Throws<ApplicationException>();
            var responseFn =
                VerifyRespondFunctionRegistered<GetAllProductDefinitionsRequestRpc, List<ProductDefinitionItem>>();

            var definitions = responseFn(new GetAllProductDefinitionsRequestRpc());

            Assert.IsNotNull(definitions);
            Assert.IsEmpty(definitions);
        }

        [Test]
        public void ShouldReturnAllProductIdsFromGetProductIdsRequest()
        {
            var productIds = new[] {111, 222, 333};
            var productDefinitions = productIds.Select(MakeProductDefinition).ToList();
            _mockProductService.Setup(p => p.GetAllProductDefinitions()).Returns(productDefinitions);

            GetProductIdsResponse response = null;
            _mockBus.Setup(b => b.Publish(It.IsAny<GetProductIdsResponse>()))
                .Callback((GetProductIdsResponse r) => response = r);

            _getProductRequestConsumer.ConsumeGetProductIdsRequest(new GetProductIdsRequest());

            _mockBus.Verify(m => m.Publish(It.IsAny<GetProductIdsResponse>()), Times.Once);
            Assert.NotNull(response);
            Assert.That(response.ProductIds, Is.EquivalentTo(productIds));
        }

        [Test]
        public void ShouldNotPublishARepsonseFromGetProductIdsRequestIfProductServiceThrows()
        {
            _mockProductService.Setup(p => p.GetAllProductDefinitions()).Throws<ApplicationException>();
            _getProductRequestConsumer.ConsumeGetProductIdsRequest(new GetProductIdsRequest());
            _mockBus.Verify(m => m.Publish(It.IsAny<GetProductIdsResponse>()), Times.Never);
        }

        [Test]
        public void ShouldLogAnErrorInGetProductIdsRequestIfProductServiceThrows()
        {
            _mockProductService.Setup(p => p.GetAllProductDefinitions()).Throws<ApplicationException>();
            using (var appender = new MemoryAppenderForTests())
            {
                _getProductRequestConsumer.ConsumeGetProductIdsRequest(new GetProductIdsRequest());
                appender.AssertALogMessageContains(Level.Error, "ConsumeGetProductIdsRequest");
            }
        }


        private Func<TRpc, TResponse> VerifyRespondFunctionRegistered<TRpc, TResponse>()
            where TRpc : class
            where TResponse : class
        {
            Func<TRpc, TResponse> responseFunc = null;
            _mockBus.Setup(b => b.Respond(It.IsAny<Func<TRpc, TResponse>>()))
                .Callback<Func<TRpc, TResponse>>(f => responseFunc = f);
            _getProductRequestConsumer.Start();
            _mockBus.Verify(b => b.Respond(It.IsAny<Func<TRpc, TResponse>>()), Times.Once);
            Assert.IsNotNull(responseFunc);
            return responseFunc;
        }

        private static List<ProductDefinitionItem> MakeActiveProductDefinitions()
        {
            var activeDefs = new List<ProductDefinitionItem>
            {
                MakeProductDefinition(1),
                MakeProductDefinition(2)
            };

            return activeDefs;
        }

        private static ProductDefinitionItem MakeProductDefinition(int id)
        {
            return new ProductDefinitionItem {ProductId = id, ProductName = "Product" + id};
        }

        private static Product MakeProduct(int id)
        {
            return new Product {ProductId = id, Name = "Product" + id};
        }
    }
}