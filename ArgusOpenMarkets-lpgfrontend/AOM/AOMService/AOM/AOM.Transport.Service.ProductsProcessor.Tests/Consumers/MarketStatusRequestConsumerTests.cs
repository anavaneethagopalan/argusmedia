﻿using System;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.ProductsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    [TestFixture]
    public class MarketStatusRequestConsumerTests
    {
        private MockAomModel _aomModel;
        private Mock<ICrmModel> _crmModel;
        private MockDbContextFactory _dbFactory;
        private Mock<IBus> _bus;
        private Mock<IProductService> _productService;
        private const string FakeProductName = "fakeProduct";

        [SetUp]
        public void Setup()
        {
            _aomModel = new MockAomModel();
            AomDataBuilder.WithModel(_aomModel)
                          .AddDeliveryLocation("fake delivery location")
                          .AddProduct(FakeProductName);
            _crmModel = new Mock<ICrmModel>();
            _dbFactory = new MockDbContextFactory(_aomModel, _crmModel.Object);
            _bus = new Mock<IBus>();
            _productService = new Mock<IProductService>();
        }

        [Test]
        public void ConsumerSubscribedToMarketStatusChangeRequestsOnStartup()
        {
            // Arrange
            var mockErrorService = new Mock<IErrorService>();
            var consumer = CreateConsumer();

            // Act
            consumer.Start();

            // Assert
            _bus.Verify(
                b => b.Subscribe(It.IsAny<string>(),
                                 It.IsAny<Action<AomMarketStatusChangeRequest>>()),
                Times.Once);
        }

        private MarketStatusRequestConsumer CreateConsumer()
        {
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new MarketStatusRequestConsumer(_dbFactory, _productService.Object, _bus.Object, mockErrorService.Object);
            return consumer;
        }

        [Test]
        public void ConsumerSubscribedToMarketStatusChangeAuthenticationResponsesOnStartup()
        {
            // Arrange
            var consumer = CreateConsumer();

            // Act
            consumer.Start();

            // Assert
            _bus.Verify(
                b => b.Subscribe(It.IsAny<string>(),
                                 It.IsAny<Action<AomMarketStatusChangeAuthenticationResponse>>()),
                Times.Once);
        }

        [Test]
        [TestCase(MessageAction.Open)]
        [TestCase(MessageAction.Close)]
        public void TestThatProductRequestPublishesCorrectAuthenticationRequest(MessageAction messageAction)
        {
            // Arrange
            var consumer = CreateConsumer();

            var req = new AomMarketStatusChangeRequest
            {
                ClientSessionInfo = new ClientSessionInfo { SessionId = "123" },
                MessageAction = messageAction,
                Product = _aomModel.Products.Single().ToEntity(new Product()),
            };

            // Act
            consumer.ConsumeProductRequest<
                AomMarketStatusChangeRequest, AomMarketStatusChangeAuthenticationRequest>(req);

            // Assert
            _bus.Verify(
                b => b.Publish(It.Is<AomMarketStatusChangeAuthenticationRequest>(r => r.MessageAction == messageAction)),
                Times.Once);
        }

        [Test]
        [TestCase(MessageAction.Open)]
        [TestCase(MessageAction.Close)]
        public void TestThatOpenMarketActionCallsProductServiceToOpenTheMarket(MessageAction messageAction)
        {
            // Arrange
            var consumer = CreateConsumer();

            var res = new AomMarketStatusChangeAuthenticationResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo {SessionId = "123"},
                    MessageAction = messageAction
                },
                Product = _aomModel.Products.Single().ToEntity(new Product()),
            };

            var marketStatus = messageAction == MessageAction.Open ? MarketStatus.Open : MarketStatus.Closed;

            // Act
            consumer.ConsumeProductResponse<
                AomMarketStatusChangeAuthenticationResponse, AomProductResponse>(res);

            _productService.Verify(
                p =>
                    p.ChangeMarketStatus(It.IsAny<IAomModel>(),
                                         It.IsAny<AomProductResponse>(),
                                         It.Is<MarketStatus>(s => s == marketStatus)));
        }

        [Test]
        [TestCase(MessageAction.Open)]
        [TestCase(MessageAction.Close)]
        public void TestThatOpenMarketActionPublishesCorrectMarketStatusChangeMessage(MessageAction messageAction)
        {
            // Arrange
            var product = new Product { ProductId = 1, Name = "Name", Status = MarketStatus.Open };
            _productService.Setup(
                m =>
                    m.ChangeMarketStatus(
                        It.IsAny<IAomModel>(),
                        It.IsAny<AomProductResponse>(),
                        It.IsAny<MarketStatus>())).Returns(product);

            var consumer = CreateConsumer();

            var res = new AomMarketStatusChangeAuthenticationResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo { SessionId = "123" },
                    MessageAction = messageAction
                },
                Product = _aomModel.Products.Single().ToEntity(new Product()),
            };

            // Act
            consumer.ConsumeProductResponse<
                AomMarketStatusChangeAuthenticationResponse, AomProductResponse>(res);

            // Assert
            _bus.Verify(
                b => b.Publish(It.Is<AomMarketStatusChange>(c => c.Message.MessageAction == messageAction)),
                Times.Once);
        }

        [Test]
        [TestCase(MessageAction.Open)]
        public void TestThatOpenMarketActionDoesNotPublishAMarketStatusChangeIfTheProductReturnedIsNull(MessageAction messageAction)
        {
            Product product = null;
            // Arrange
            _productService.Setup(
                m =>
                    m.ChangeMarketStatus(
                        It.IsAny<IAomModel>(),
                        It.IsAny<AomProductResponse>(),
                        It.IsAny<MarketStatus>())).Returns(product);

            var consumer = CreateConsumer();

            var res = new AomMarketStatusChangeAuthenticationResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo { SessionId = "123" },
                    MessageAction = messageAction
                },
                Product = _aomModel.Products.Single().ToEntity(new Product()),
            };

            // Act
            consumer.ConsumeProductResponse<
                AomMarketStatusChangeAuthenticationResponse, AomProductResponse>(res);

            // Assert
            _bus.Verify(
                b => b.Publish(It.Is<AomMarketStatusChange>(c => c.Message.MessageAction == messageAction)),
                Times.Never);
        }

        [Test]
        public void TestThatUpdateProductMessageCallsToEditTheProduct()
        {
            // Arrange
            var consumer = CreateConsumer();

            var res = new AomMarketStatusChangeAuthenticationResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo { SessionId = "123" },
                    MessageAction = MessageAction.Update
                },
                Product = _aomModel.Products.Single().ToEntity(new Product()),
            };

            // Act
            consumer.ConsumeProductResponse<
                AomMarketStatusChangeAuthenticationResponse, AomProductResponse>(res);

            _productService.Verify(
                p =>
                    p.EditProduct(It.IsAny<IAomModel>(),
                                  It.Is<AomProductResponse>(r => r.Product.ProductId == res.Product.ProductId)));
        }


    }
}
