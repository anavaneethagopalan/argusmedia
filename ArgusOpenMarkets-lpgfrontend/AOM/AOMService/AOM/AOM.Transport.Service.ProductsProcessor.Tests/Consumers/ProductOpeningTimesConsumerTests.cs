﻿using AOM.App.Domain.Entities;
using AOM.Services.ProductService;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.ProductsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    [TestFixture]
    public class ProductOpeningTimesConsumerTests
    {
        private Mock<IBus> _bus;
        private Mock<IProductService> _productService;

        [SetUp]
        public void Setup()
        {
            _bus = new Mock<IBus>();
            _productService = new Mock<IProductService>();
        }

        [Test]
        public void ConsumerRegistersToRespondToGetOpeningTimesRpcWhenStarting()
        {
            var consumer = new ProductOpeningTimesConsumer(_bus.Object, _productService.Object);
            consumer.Start();
            _bus.Verify(
                b => b.Respond(It.IsAny<Func<GetOpeningTimesRpc, IList<ProductOpeningTimes>>>()));
        }

        [Test]
        public void ConsumerReturnsListOfOpeningTimesAfterRpcRequest()
        {
            var fakeTimes = new List<ProductOpeningTimes>
            {
                new ProductOpeningTimes
                {
                    Id = 1,
                    Name = "TestTime",
                    OpenTime = new TimeSpan(1, 0, 0),
                    CloseTime = new TimeSpan(2, 0, 0)
                }
            };
            _productService.Setup(s => s.GetAllProductOpeningTimes()).Returns(fakeTimes);

            Func<GetOpeningTimesRpc, IList<ProductOpeningTimes>> respondFn = null;
            _bus.Setup(b => b.Respond(It.IsAny<Func<GetOpeningTimesRpc, IList<ProductOpeningTimes>>>()))
                .Callback<Func<GetOpeningTimesRpc, IList<ProductOpeningTimes>>>(f => respondFn = f);

            var consumer = new ProductOpeningTimesConsumer(_bus.Object, _productService.Object);
            consumer.Start();
            Assert.IsNotNull(respondFn);
            var times = respondFn(new GetOpeningTimesRpc());

            Assert.IsNotNull(times);
            Assert.AreEqual(fakeTimes.Count, times.Count, "Product times list has incorrect length");
            var e1 = fakeTimes[0];
            var a1 = times[0];
            Assert.AreEqual(e1.Id, a1.Id);
            Assert.AreEqual(e1.Name, a1.Name);
            Assert.AreEqual(e1.OpenTime, a1.OpenTime);
            Assert.AreEqual(e1.CloseTime, a1.CloseTime);
        }

        [Test]
        public void ConsumerReturnsEmptyListIfProductServiceThrows()
        {
            _productService.Setup(s => s.GetAllProductOpeningTimes()).Throws<ApplicationException>();

            Func<GetOpeningTimesRpc, IList<ProductOpeningTimes>> respondFn = null;
            _bus.Setup(b => b.Respond(It.IsAny<Func<GetOpeningTimesRpc, IList<ProductOpeningTimes>>>()))
                .Callback<Func<GetOpeningTimesRpc, IList<ProductOpeningTimes>>>(f => respondFn = f);

            var consumer = new ProductOpeningTimesConsumer(_bus.Object, _productService.Object);
            consumer.Start();
            Assert.IsNotNull(respondFn);
            var times = respondFn(new GetOpeningTimesRpc());

            Assert.IsEmpty(times);

        }
    }
}
