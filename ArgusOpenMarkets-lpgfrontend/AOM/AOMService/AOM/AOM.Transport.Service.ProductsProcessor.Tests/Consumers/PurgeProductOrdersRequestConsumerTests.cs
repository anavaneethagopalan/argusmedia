﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.ProductsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    [TestFixture]
    public class PurgeProductOrdersRequestConsumerTests
    {
        private MockAomModel _aomModel;
        private Mock<ICrmModel> _crmModel;
        private MockDbContextFactory _dbFactory;
        private Mock<IBus> _bus;
        private Mock<IProductService> _productService;
        private const string FakeProductName1 = "fakeProduct1";
        private const string FakeProductName2 = "fakeProduct2";


        [SetUp]
        public void Setup()
        {
            _aomModel = new MockAomModel();
            AomDataBuilder.WithModel(_aomModel)
                          .AddDeliveryLocation("fake delivery location")
                          .AddProduct(FakeProductName1)
                          .AddProduct(FakeProductName2);
            _crmModel = new Mock<ICrmModel>();
            _dbFactory = new MockDbContextFactory(_aomModel, _crmModel.Object);
            _bus = new Mock<IBus>();
            _productService = new Mock<IProductService>();
        }

        [Test]
        public void ConsumerSubscribedToAomProductPurgeOrdersRequestOnStartup()
        {
            // Arrange
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new PurgeProductOrdersRequestConsumer(_dbFactory, _productService.Object, _bus.Object, mockErrorService.Object);

            // Act
            consumer.Start();

            // Assert
            _bus.Verify(
                b => b.Subscribe(It.IsAny<string>(),
                                 It.IsAny<Action<AomProductPurgeOrdersRequest>>()),
                Times.Once);

        }

        [Test]
        public void ConsumerSubscribedToAomProductPurgeOrdersAuthenticationResponseOnStartup()
        {
            // Arrange
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new PurgeProductOrdersRequestConsumer(_dbFactory, _productService.Object, _bus.Object, mockErrorService.Object);

            // Act
            consumer.Start();

            // Assert
            _bus.Verify(
                b => b.Subscribe(It.IsAny<string>(),
                                 It.IsAny<Action<AomProductPurgeOrdersAuthenticationResponse>>()),
                Times.Once);

        }

        [Test]
        [TestCase(FakeProductName1)]
        [TestCase(FakeProductName2)]
        public void PurgeOrdersRequestForProductPublishesResponseForCorrectProduct(string productName)
        {
            // Arrange
            var product = GetProduct(productName);
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new PurgeProductOrdersRequestConsumer(_dbFactory, _productService.Object, _bus.Object, mockErrorService.Object);
            var authResponse = new AomProductPurgeOrdersAuthenticationResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo {SessionId = "123"},
                    MessageAction = MessageAction.Purge,
                },
                Product = product
            };

            // Act
            ConsumeResponse(consumer, authResponse);

            // Assert
            _bus.Verify(
                b => b.Publish(It.Is<AomProductResponse>(
                    r => r.Product.ProductId == authResponse.Product.ProductId)));
        }

        [Test]
        public void AuthenticationResponseErrorShouldPublishErrorMessage()
        {
            // Arrange
            const string fakeErrorMessage = "I am an error";
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new PurgeProductOrdersRequestConsumer(_dbFactory, _productService.Object, _bus.Object, mockErrorService.Object);
            var authResponse = new AomProductPurgeOrdersAuthenticationResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo { SessionId = "123" },
                    MessageAction = MessageAction.Purge,
                    MessageType = MessageType.Error,
                    MessageBody = fakeErrorMessage
                },
                Product = GetProduct(FakeProductName1)
            };

            // Act
            ConsumeResponse(consumer, authResponse);

            // Assert
            _bus.Verify(
                b => b.Publish(It.Is<AomProductResponse>(
                    r => r.Message.MessageType == MessageType.Error &&
                         fakeErrorMessage.Equals(r.Message.MessageBody) &&
                         r.Message.MessageAction == MessageAction.Purge)));
        }

        [Test]
        public void ShouldSetTheUserIdToMinusOneWhenGeneratingAPurgeRequest()
        {
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new PurgeProductOrdersRequestConsumer(_dbFactory, _productService.Object, _bus.Object, mockErrorService.Object);

            var message = new Message();
            var product = new Product();
            var purgeRequest = consumer.MakePurgeRequest(message, product);

            Assert.That(purgeRequest.UserId, Is.EqualTo(-1));
        }

        private static void ConsumeResponse(PurgeProductOrdersRequestConsumer consumer,
                                            AomProductPurgeOrdersAuthenticationResponse authResponse)
        {
            consumer.ConsumeProductResponse<
                AomProductPurgeOrdersAuthenticationResponse, AomProductResponse>(authResponse);
        }

        private Product GetProduct(string productName)
        {
            return _aomModel.Products.Single(p => p.Name == productName).ToEntity(new Product());
        }
    }
}
