﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Services.ProductMetadataService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.ProductsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    [TestFixture]
    public class ProductMetadataRequestConsumerTests
    {
        private Mock<IProductMetadataService> _mockProductMetadataService;
        private Mock<IBus> _mockBus;
        private ProductMetaDataRequestConsumer _productMetadataRequestConsumer;

        [SetUp]
        public void SetUp()
        {
            _mockBus = new Mock<IBus>();
            _mockProductMetadataService = new Mock<IProductMetadataService>();

            _productMetadataRequestConsumer = new ProductMetaDataRequestConsumer(
                _mockProductMetadataService.Object,
                _mockBus.Object);
        }

        [Test]
        public void ShouldReturnSubscriptionId()
        {
            Assert.That(_productMetadataRequestConsumer.SubscriptionId, Is.EqualTo("AOM"));
        }

        [Test]
        public void
            ConsumeGetProductMetaDataRequest_ShouldNotPublishAnythingOnTheBusIfThereAreNoProductMetaDataDefinitions()
        {
            var emptyList = new List<ProductMetaData>();
            _mockProductMetadataService.Setup(m => m.GetAllProductMetaData()).Returns(emptyList);

            var request = new GetProductMetaDataRequest() {ProductId = null};
            _productMetadataRequestConsumer.ConsumeGetProductMetaDataRequest(request);

            _mockBus.Verify(m => m.Publish(It.IsAny<GetProductMetaDataResponse>()), Times.Never);
            _mockProductMetadataService.VerifyAll();
        }

        [Test]
        public void
            ConsumeGetProductMetaDataRequest_ShouldNotPublishAnythingOnTheBusIfThereAreNoProductMetaDataDefinitionsForASpecificProduct
            ()
        {
            var emptyList = new List<ProductMetaData>();
            _mockProductMetadataService.Setup(m => m.GetAllProductMetaData()).Returns(emptyList);

            GetProductMetaDataResponse response = null;

            _mockBus.Setup(x => x.Publish(It.IsAny<GetProductMetaDataResponse>()))
                .Callback(new Action<GetProductMetaDataResponse>(x => { response = x; }))
                .Verifiable();

            var request = new GetProductMetaDataRequest() {ProductId = -1};
            _productMetadataRequestConsumer.ConsumeGetProductMetaDataRequest(request);

            _mockBus.VerifyAll();

            Assert.That(response, Is.Not.Null);
            Assert.That(response.ProductMetaData, Is.Not.Null);
            Assert.That(response.ProductMetaData.ProductId, Is.EqualTo(-1));
            Assert.That(response.ProductMetaData.Fields, Is.Not.Null);
            Assert.That(response.ProductMetaData.Fields, Is.Empty);
        }

        [Test]
        public void ConsumeGetProductMetaDataRequest_ShouldPublishAMessageOnTheBusForAllProductDefinitionsReturned()
        {
            var twoMetaDatas = new List<ProductMetaData>
            {
                new ProductMetaData() {ProductId = 1},
                new ProductMetaData() {ProductId = 2}
            };

            _mockProductMetadataService.Setup(m => m.GetAllProductMetaData()).Returns(twoMetaDatas);

            var request = new GetProductMetaDataRequest() {ProductId = null};
            _productMetadataRequestConsumer.ConsumeGetProductMetaDataRequest(request);

            _mockBus.Verify(m => m.Publish(It.IsAny<GetProductMetaDataResponse>()), Times.Exactly(2));
        }

        [Test]
        public void ConsumeProductMetadataItemRequest_ShouldAddProductMetadataItemOnCreateRequest()
        {
            var productMetadataItem = new ProductMetaDataItemString();
            var request = new ProductMetadataItemRequest(productMetadataItem)
            {
                MessageAction = MessageAction.Create,
                ClientSessionInfo = new ClientSessionInfo {UserId = 123}
            };

            _productMetadataRequestConsumer.ConsumeProductMetadataItemRequest(request);
            _mockProductMetadataService.Verify(x => x.AddProductMetadataItem(productMetadataItem, 123), Times.Once);
        }

        [Test]
        public void ConsumeProductMetadataItemRequest_ShouldUpdateProductMetadataItemOnCreateRequest()
        {
            var productMetadataItem = new ProductMetaDataItemString();
            var request = new ProductMetadataItemRequest(productMetadataItem)
            {
                MessageAction = MessageAction.Update,
                ClientSessionInfo = new ClientSessionInfo {UserId = 123}
            };

            _productMetadataRequestConsumer.ConsumeProductMetadataItemRequest(request);
            _mockProductMetadataService.Verify(x => x.UpdateProductMetadataItem(productMetadataItem, 123), Times.Once);
        }

        [Test]
        public void ConsumeProductMetadataItemRequest_ShouldDeleteProductMetadataItemOnCreateRequest()
        {
            var productMetadataItem = new ProductMetaDataItemString();
            var request = new ProductMetadataItemRequest(productMetadataItem)
            {
                MessageAction = MessageAction.Delete,
                ClientSessionInfo = new ClientSessionInfo {UserId = 123}
            };

            _productMetadataRequestConsumer.ConsumeProductMetadataItemRequest(request);
            _mockProductMetadataService.Verify(x => x.DeleteProductMetadataItem(productMetadataItem.Id, 123), Times.Once);
        }
    }
}