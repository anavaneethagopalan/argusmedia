﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Transport.Service.ProductsProcessor.Consumers;
using Moq;
using NUnit.Framework;
using System;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    [TestFixture]
    public class ProductStatusHelperTests
    {
        private ProductStatusHelper _helper;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;

        [SetUp]
        public void Setup()
        {
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _helper = new ProductStatusHelper(_mockDateTimeProvider.Object);
        }

        [Test]
        public void ShouldExpectStatusToBeOpenIfCurrentTimeAfterOpenAndBeforeCloseWithOpenBeforeClose()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(10),
                CloseTime = TimeSpan.FromHours(18)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 12, 0, 0));
            var status = _helper.DetermineExpectedMarketStatus(fakeProduct);
            Assert.That(status, Is.EqualTo(MarketStatus.Open));
        }

        [Test]
        public void ShouldExpectStatusToBeClosedIfCurrentTimeAfterCloseWithOpenBeforeClose()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(10),
                CloseTime = TimeSpan.FromHours(18)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 21, 0, 0));
            var status = _helper.DetermineExpectedMarketStatus(fakeProduct);
            Assert.That(status, Is.EqualTo(MarketStatus.Closed));
        }

        [Test]
        public void ShouldExpectStatusToBeOpenIfCurrentTimeAfterOpenAndBeforeCloseWithCloseBeforeOpen()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(18),
                CloseTime = TimeSpan.FromHours(10)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 21, 0, 0));
            var status = _helper.DetermineExpectedMarketStatus(fakeProduct);
            Assert.That(status, Is.EqualTo(MarketStatus.Open));
        }

        [Test]
        public void ShouldExpectStatusToBeClosedIfCurrentTimeAfterCloseAndBeforeOpenWithCloseBeforeOpen()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(18),
                CloseTime = TimeSpan.FromHours(10)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 12, 0, 0));
            var status = _helper.DetermineExpectedMarketStatus(fakeProduct);
            Assert.That(status, Is.EqualTo(MarketStatus.Closed));
        }

        [Test]
        public void ShouldReturnCloseTimeTodayAsNextStatusChangeTimeIfAfterTodaysOpenAndBeforeTodaysClose()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(10),
                CloseTime = TimeSpan.FromHours(18)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 12, 0, 0));
            var nextTime = _helper.GetNextMarketStatusChangeTime(fakeProduct);
            Assert.That(nextTime, Is.EqualTo(new DateTime(2015, 06, 15, 18, 0, 0)));
        }

        [Test]
        public void ShouldReturnOpenTimeTodayAsNextStatusChangeTimeIfBeforeTodaysOpen()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(10),
                CloseTime = TimeSpan.FromHours(18)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 9, 0, 0));
            var nextTime = _helper.GetNextMarketStatusChangeTime(fakeProduct);
            Assert.That(nextTime, Is.EqualTo(new DateTime(2015, 06, 15, 10, 0, 0)));
        }

        [Test]
        public void ShouldReturnCloseTimeTodayAsNextStatusChangeTimeIfBeforeTodaysCloseWithCloseBeforeOpen()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(18),
                CloseTime = TimeSpan.FromHours(10)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 9, 0, 0));
            var nextTime = _helper.GetNextMarketStatusChangeTime(fakeProduct);
            Assert.That(nextTime, Is.EqualTo(new DateTime(2015, 06, 15, 10, 0, 0)));
        }

        [Test]
        public void ShouldReturnOpenTimeTomorrowAsNextStatusChangeTimeIfAfterTodaysClose()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(10),
                CloseTime = TimeSpan.FromHours(18)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 21, 0, 0));
            var nextTime = _helper.GetNextMarketStatusChangeTime(fakeProduct);
            Assert.That(nextTime, Is.EqualTo(new DateTime(2015, 06, 16, 10, 0, 0)));
        }

        [Test]
        public void ShouldReturnTomorrowsCloseTimeTodayAsNextStatusChangeTimeIfAfterTodaysOpenWithCloseBeforeOpen()
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(18),
                CloseTime = TimeSpan.FromHours(10)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, 15, 21, 0, 0));
            var nextTime = _helper.GetNextMarketStatusChangeTime(fakeProduct);
            Assert.That(nextTime, Is.EqualTo(new DateTime(2015, 06, 16, 10, 0, 0)));
        }

        [Test]
        [TestCase(15, 16, Description = "Monday->Tuesday")]
        [TestCase(16, 17, Description = "Tuesday->Wednesday")]
        [TestCase(17, 18, Description = "Wednesday->Thursday")]
        [TestCase(18, 19, Description = "Thursday->Friday")]
        [TestCase(19, 22, Description = "Friday->Monday")]
        public void ShouldReturnNextWorkingDayOpenIfAfterCloseWithOpenBeforeClose(
            int currentDay, int expectedNextDay)
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(10),
                CloseTime = TimeSpan.FromHours(18)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, currentDay, 21, 0, 0));
            var nextTime = _helper.GetNextMarketStatusChangeTime(fakeProduct);
            Assert.That(nextTime, Is.EqualTo(new DateTime(2015, 06, expectedNextDay, 10, 0, 0)));
        }

        [Test]
        [TestCase(15, 16, Description = "Monday->Tuesday")]
        [TestCase(16, 17, Description = "Tuesday->Wednesday")]
        [TestCase(17, 18, Description = "Wednesday->Thursday")]
        [TestCase(18, 19, Description = "Thursday->Friday")]
        [TestCase(19, 22, Description = "Friday->Monday")]
        public void ShouldReturnNextWorkingDayCloseIfAfterOpenWithCloseBeforeOpen(
            int currentDay, int expectedNextDay)
        {
            var fakeProduct = new Product
            {
                OpenTime = TimeSpan.FromHours(18),
                CloseTime = TimeSpan.FromHours(9)
            };
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime(2015, 06, currentDay, 21, 0, 0));
            var nextTime = _helper.GetNextMarketStatusChangeTime(fakeProduct);
            Assert.That(nextTime, Is.EqualTo(new DateTime(2015, 06, expectedNextDay, 9, 0, 0)));
        }
    }
}
