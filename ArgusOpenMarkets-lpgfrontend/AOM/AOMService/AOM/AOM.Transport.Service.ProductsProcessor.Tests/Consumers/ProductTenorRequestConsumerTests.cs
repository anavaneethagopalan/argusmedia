﻿using System;
using System.Collections.Generic;
using System.Linq;

using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.ProductsProcessor.Consumers;

using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    [TestFixture]
    public class ProductTenorRequestConsumerTests
    {
        private IDbContextFactory mockDbFactory;
        private Mock<IProductService> mockProductService;
        private Mock<IBus> mockBus;
        private Mock<IAomModel> mockAomModel;
        private ProductTenorRequestConsumer productTenorRequestConsumer;

        [SetUp]
        public void Setup()
        {
            mockDbFactory = new MockDbContextFactory(new MockAomModel(), new MockCrmModel());
            mockProductService = new Mock<IProductService>();
            mockBus = new Mock<IBus>();
            mockAomModel = new Mock<IAomModel>();
            var mockErrorService = new Mock<IErrorService>();

            productTenorRequestConsumer = new ProductTenorRequestConsumer(mockDbFactory, mockProductService.Object, mockBus.Object, mockErrorService.Object);
        }

        [Test]
        public void ConsumeProductTenorResponseAdditionalProcessingShouldReturnNullIfTheMessageActionIsCheck()
        {
            var authResponse = GenerateProductTenorAuthenticationResponse(MessageAction.Check);
            var product = productTenorRequestConsumer.ConsumeProductTenorResponseAdditionalProcessing(mockAomModel.Object, authResponse);

            Assert.That(product, Is.Null);
        }

        [Test]
        public void ConsumeProductTenorResponseAdditionalProcessingShouldReturnAProductIfTheMessageActionIsUpdate()
        {
            var productTenor = GenerateProductTenor(100);
            mockProductService.Setup(m => m.EditProductTenor(It.IsAny<IAomModel>(), It.IsAny<AomProductTenorResponse>())).Returns(productTenor);

            var authResponse = GenerateProductTenorAuthenticationResponse(MessageAction.Update);
            var actualProductTenor = productTenorRequestConsumer.ConsumeProductTenorResponseAdditionalProcessing(mockAomModel.Object, authResponse);

            Assert.That(actualProductTenor, Is.Not.Null);
            Assert.That(actualProductTenor.Id, Is.EqualTo(productTenor.Id));
        }

        [Test]
        public void ProductTenorRequestConsumerShouldHaveAnEmptyStringForTheSubscriptionId()
        {
            var subscriptionId = productTenorRequestConsumer.SubscriptionId;
            Assert.That(subscriptionId, Is.EqualTo(string.Empty));
        }

        [Test]
        public void ShouldSubscribeToTheCorrectEventsOnTheBus()
        {
            productTenorRequestConsumer.SubscribeToEvents();
            mockBus.Verify(m => m.Subscribe(string.Empty, It.IsAny<Action<AomProductTenorAuthenticationResponse>>()), Times.Once);
            mockBus.Verify(m => m.Subscribe(string.Empty, It.IsAny<Action<AomProductTenorRequest>>()), Times.Once);
        }

        [Test]
        public void ShouldPublishErrorMessageIfPublishingAuthenticationRequestFails()
        {
            var request = new AomProductTenorRequest
            {
                ClientSessionInfo = new ClientSessionInfo {SessionId = "123"},
                MessageAction = MessageAction.Update,
                Tenor = new ProductTenor()
            };
            mockBus.Setup(b => b.Publish(It.IsAny<AomProductTenorRequest>())).Throws(new ApplicationException("The bus barfed"));

            productTenorRequestConsumer.ConsumeProductTenorRequest<AomProductTenorRequest, AomProductTenorAuthenticationRequest>(request);

            mockBus.Verify(b => b.Publish(It.Is<AomProductTenorResponse>(r => r.Message.MessageType == MessageType.Error)), Times.Once);
//            mockBus.Verify(b =>b.Publish(It.Is<AomProductTenorResponse>(r => !string.IsNullOrWhiteSpace((string)r.Message.MessageBody))), Times.Once);
        }

        [Test]
        public void ShouldPublishErrorResponseIfProductResponseIsAnErrorMessage()
        {
            const string fakeErrorMessage = "Fake authentication failure";
            var response = new AomProductTenorAuthenticationResponse
            {
                Message = new Message
                {
                    MessageType = MessageType.Error,
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo {SessionId = "123"},
                    MessageBody = fakeErrorMessage
                }
            };
            productTenorRequestConsumer.ConsumeProductTenorResponse<AomProductTenorAuthenticationResponse, AomProductTenorResponse>(response);

            mockBus.Verify(b => b.Publish(It.Is<AomProductTenorResponse>(r => r.Message.MessageType == MessageType.Error)), Times.Once);
            mockBus.Verify(b => b.Publish(It.Is<AomProductTenorResponse>(r => fakeErrorMessage.Equals(r.Message.MessageBody))), Times.Once);
        }

        [Test]
        public void ShouldPublishErrorMessageIfProductServiceThrowsBusinessRuleException()
        {
            const string fakeExceptionMessage = "A business rule exception";
            mockProductService.Setup(p => p.EditProductTenor(It.IsAny<IAomModel>(), It.IsAny<AomProductTenorAuthenticationResponse>())).Throws(new BusinessRuleException(fakeExceptionMessage));
            var authResponse = GenerateProductTenorAuthenticationResponse(MessageAction.Update);

            productTenorRequestConsumer.ConsumeProductTenorResponse<AomProductTenorAuthenticationResponse, AomProductTenorResponse>(authResponse);

            mockBus.Verify(b => b.Publish(It.Is<AomProductTenorResponse>(r => r.Message.MessageType == MessageType.Error)), Times.Once);
            mockBus.Verify(b => b.Publish( It.Is<AomProductTenorResponse>(r => ((string) r.Message.MessageBody).Contains(fakeExceptionMessage))), Times.Once);
        }

        [Test]
        public void ShouldPublishErrorMessageIfProductServiceThrowsGenericException()
        {
            mockProductService.Setup(p => p.EditProductTenor(It.IsAny<IAomModel>(), It.IsAny<AomProductTenorAuthenticationResponse>())).Throws<ApplicationException>();
            var authResponse = GenerateProductTenorAuthenticationResponse(MessageAction.Update);

            productTenorRequestConsumer.ConsumeProductTenorResponse<AomProductTenorAuthenticationResponse, AomProductTenorResponse>(authResponse);

            mockBus.Verify(b => b.Publish(It.Is<AomProductTenorResponse>(r => r.Message.MessageType == MessageType.Error)), Times.Once);
            mockBus.Verify(b => b.Publish(It.Is<AomProductTenorResponse>(r => !string.IsNullOrWhiteSpace((string) r.Message.MessageBody))), Times.Once);
        }

        [Test]
        public void ShouldRegisterToRespondToProductTenorRequests()
        {
            productTenorRequestConsumer.SubscribeToEvents();
            mockBus.Verify(b => b.Respond(It.IsAny<Func<GetAllProductTenorsRequestRpc, IEnumerable<ProductTenor>>>()), Times.Once);
            mockBus.Verify(b => b.Respond(It.IsAny<Func<GetProductTenorRequestRpc, AomProductTenorResponse>>()), Times.Once);
        }

        [Test]
        public void ShouldSupplyCorrectProductTenorInResponseToGetProductTenorRequest()
        {
            var responseFn = StartConsumerAndGetTenorResponseFunction();
            Assert.IsNotNull(responseFn);

            mockProductService.Setup(p => p.GetProductTenor(FakeProductTenorId1)).Returns(GenerateProductTenor(FakeProductTenorId1));
            mockProductService.Setup(p => p.GetProductTenor(FakeProductTenorId2)).Returns(GenerateProductTenor(FakeProductTenorId2));

            var response = responseFn(new GetProductTenorRequestRpc {Id = FakeProductTenorId2});

            Assert.IsNotNull(response);
            Assert.IsNotNull(response.ProductTenor);
            Assert.AreEqual(FakeProductTenorId2, response.ProductTenor.Id);
        }

        [Test]
        public void ShouldReturnAllProductTenorsInResponseToGetAllProductTenorsRequest()
        {
            var responseFn = StartConsumerAndGetAllTenorsResponseFunction();
            Assert.IsNotNull(responseFn);

            mockProductService.Setup(p => p.GetAllProductTenors()).Returns(new[] {GenerateProductTenor(FakeProductTenorId1), GenerateProductTenor(FakeProductTenorId2)});

            var tenors = responseFn(new GetAllProductTenorsRequestRpc());

            Assert.IsNotNull(tenors);
            var tenorsArray = tenors.ToArray();
            Assert.AreEqual(2, tenorsArray.Count(), "Incorrect number of tenors returned");
            Assert.AreEqual(1, tenorsArray.Count(t => t.Id == FakeProductTenorId1));
            Assert.AreEqual(1, tenorsArray.Count(t => t.Id == FakeProductTenorId2));
        }

        //[Test]
        //public void ShouldReturnANullTenorIfProductServiceThrowsForGetProductTenorRequest()
        //{
        //    var responseFn = StartConsumerAndGetTenorResponseFunction();
        //    Assert.IsNotNull(responseFn);

        //    mockProductService.Setup(p => p.GetTenorCode(It.IsAny<long>())).Throws<ApplicationException>();

        //    var response = responseFn(new GetProductTenorRequestRpc { Id = FakeProductTenorId1 });

        //    Assert.IsNotNull(response);
        //    Assert.IsNull(response.ProductTenor);
        //}

        [Test]
        public void ShouldReturnNullIfProductServiceThrowsForGetAllProductTenorsRequest()
        {
            var responseFn = StartConsumerAndGetAllTenorsResponseFunction();
            Assert.IsNotNull(responseFn);

            mockProductService.Setup(p => p.GetAllProductTenors()).Throws<ApplicationException>();
            var tenors = responseFn(new GetAllProductTenorsRequestRpc());

            Assert.IsNull(tenors);
        }

        private Func<GetAllProductTenorsRequestRpc, IEnumerable<ProductTenor>> StartConsumerAndGetAllTenorsResponseFunction()
        {
            return StartConsumerAndGetRegisteredRpcResponseFunction<GetAllProductTenorsRequestRpc, IEnumerable<ProductTenor>>();
        }

        private Func<GetProductTenorRequestRpc, AomProductTenorResponse> StartConsumerAndGetTenorResponseFunction()
        {
            return StartConsumerAndGetRegisteredRpcResponseFunction<GetProductTenorRequestRpc, AomProductTenorResponse>();
        }

        private Func<TReq, TRes> StartConsumerAndGetRegisteredRpcResponseFunction<TReq, TRes>() where TReq : class where TRes : class
        {
            Func<TReq, TRes> responseFunc = null;
            mockBus.Setup(b => b.Respond(It.IsAny<Func<TReq, TRes>>())).Callback<Func<TReq, TRes>>(f => responseFunc = f);
            productTenorRequestConsumer.Start();
            return responseFunc;
        }

        private static AomProductTenorAuthenticationResponse GenerateProductTenorAuthenticationResponse(MessageAction messageAction)
        {
            return new AomProductTenorAuthenticationResponse
            {
                Message = new Message { MessageAction = messageAction },
                ProductTenor = new ProductTenor()
            };
        }

        private static ProductTenor GenerateProductTenor(int productTenorId)
        {
            return new ProductTenor { Id = productTenorId };
        }

        private const int FakeProductTenorId1 = 1;
        private const int FakeProductTenorId2 = 2;

    }
}
