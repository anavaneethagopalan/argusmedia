﻿namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    using AOM.App.Domain.Entities;
    using AOM.Services.ProductService;
    using AOM.Transport.Events.Products;
    using AOM.Transport.Service.ProductsProcessor.Consumers;
    using Argus.Transport.Infrastructure;
    using Moq;
    using NUnit.Framework;
    using System;

    [TestFixture]
    class ProductMarketCloseRequestConsumerTests
    {
        private Mock<IBus> _mockBus;

        private Mock<IProductService> _mockProductService;

        private AomMarketStatusChangeRequest _aomMarketStatusChangeRequest;

        [Test]
        public void OnProductMarketCloseShouldPublishAomMarketStatusChangeRequestOntoBus()
        {
            _mockBus = new Mock<IBus>();
            _mockProductService = new Mock<IProductService>();

            var product = new Product { ProductId = 1, Name = "Product1" };
            _mockProductService.Setup(m => m.GetProduct(1)).Returns(product);

            var productMarketCloseRequestConsumer = new ProductMarketCloseRequestConsumer(
                _mockBus.Object,
                _mockProductService.Object);

            var subs = StartConsumer();
            productMarketCloseRequestConsumer.Start();

            var onProductMarketCloseRequest = subs.ProductMarketCloseRequest;
            onProductMarketCloseRequest(new ProductMarketCloseRequest { ProductId = 1, UserId = -1 });

            _mockBus.Verify(m => m.Publish(It.IsAny<AomMarketStatusChangeRequest>()), Times.Exactly(1));
        }

        [Test]
        public void OnProductMarketCloseShouldPublishAomMarketStatusChangeRequestContainingProductDetails()
        {
            _mockBus = new Mock<IBus>();
            _mockProductService = new Mock<IProductService>();

            var product = new Product { ProductId = 1, Name = "Product1" };
            _mockProductService.Setup(m => m.GetProduct(1)).Returns(product);

            var productMarketCloseRequestConsumer = new ProductMarketCloseRequestConsumer(
                _mockBus.Object,
                _mockProductService.Object);

            var subs = StartConsumer();
            productMarketCloseRequestConsumer.Start();

            var onProductMarketCloseRequest = subs.ProductMarketCloseRequest;
            onProductMarketCloseRequest(new ProductMarketCloseRequest { ProductId = 1, UserId = -1 });

            _mockBus.Verify(
                m => m.Publish(It.Is<AomMarketStatusChangeRequest>(p => CheckMarketCloseRequest(p))),
                Times.Exactly(1));
            Assert.That(_aomMarketStatusChangeRequest, Is.Not.Null);
            Assert.That(_aomMarketStatusChangeRequest.Product.ProductId, Is.EqualTo(1));
        }

        private bool CheckMarketCloseRequest(AomMarketStatusChangeRequest aomMarketStatusChangeRequest)
        {
            _aomMarketStatusChangeRequest = aomMarketStatusChangeRequest;
            return true;
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("AOM", It.IsAny<Action<ProductMarketCloseRequest>>()))
                .Callback<string, Action<ProductMarketCloseRequest>>((s, a) => subs.ProductMarketCloseRequest = a);

           return subs;
        }

        public class SubscribedActions
        {
            public Action<ProductMarketCloseRequest> ProductMarketCloseRequest { get; set; }
        }
    }
}
