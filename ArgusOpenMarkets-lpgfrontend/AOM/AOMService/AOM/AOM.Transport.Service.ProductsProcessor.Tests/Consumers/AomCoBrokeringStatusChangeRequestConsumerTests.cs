﻿using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.ProductsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    [TestFixture]
    public class AomCoBrokeringStatusChangeRequestConsumerTests
    {
        private MockAomModel _aomModel;
        private Mock<ICrmModel> _crmModel;
        private MockDbContextFactory _dbFactory;
        private Mock<IBus> _bus;
        private Mock<IProductService> _productService;
        private const string FakeProductName = "fakeProduct";

        [SetUp]
        public void Setup()
        {
            _aomModel = new MockAomModel();
            AomDataBuilder.WithModel(_aomModel)
                          .AddDeliveryLocation("fake delivery location")
                          .AddProduct(FakeProductName);
            _crmModel = new Mock<ICrmModel>();
            _dbFactory = new MockDbContextFactory(_aomModel, _crmModel.Object);
            _bus = new Mock<IBus>();
            _productService = new Mock<IProductService>();
        }

        [Test]
        public void ConsumerSubscribedToCoBrokeringStatusChangeRequestsOnStartup()
        {
            // Arrange
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new AomCoBrokeringStatusChangeRequestConsumer(_dbFactory, _productService.Object, _bus.Object, mockErrorService.Object);

            // Act
            consumer.Start();

            // Assert
            _bus.Verify(
                b => b.Subscribe(It.IsAny<string>(),
                                 It.IsAny<Action<AomCoBrokeringStatusChangeRequest>>()),
                Times.Once);
        }


        [Test]
        public void TestThatOnReceivingCoBrokeredStatusChangeResultsInGetProductConfigResponseBeingPublished()
        {
            // Arrange
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new AomCoBrokeringStatusChangeRequestConsumer(_dbFactory, _productService.Object, _bus.Object, mockErrorService.Object);

            var req = new AomCoBrokeringStatusChangeRequest
            {
                ClientSessionInfo = new ClientSessionInfo { SessionId = "123" },
                MessageAction = MessageAction.Update,
                Product = _aomModel.Products.Single().ToEntity(new Product()),
            };

            var editedProduct = _aomModel.Products.Single().ToEntity(new Product());
            _productService.Setup(m => m.EditProduct(It.IsAny<IAomModel>(), It.IsAny<AomProductResponse>()))
                .Returns(editedProduct);

            
            // ACT
            var subs = StartConsumer();
            consumer.Start();
            var onAomCoBrokeringStatusChangeRequest = subs.AomCoBrokeringStatusChangeRequest;
            var product = _aomModel.Products.Single().ToEntity(new Product());

            onAomCoBrokeringStatusChangeRequest(
                new AomCoBrokeringStatusChangeRequest {ClientSessionInfo = new ClientSessionInfo {UserId = -1}, MessageAction = MessageAction.Update, Product = product});

            // Verify
            _bus.Verify(m => m.Publish(It.IsAny<GetProductConfigResponse>()), Times.Exactly(1));
        }


        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _bus.Setup(b => b.Subscribe("AomCoBrokeringStatusChangeRequestConsumer", It.IsAny<Action<AomCoBrokeringStatusChangeRequest>>()))
                .Callback<string, Action<AomCoBrokeringStatusChangeRequest>>((s, a) => subs.AomCoBrokeringStatusChangeRequest = a);

            return subs;
        }

        public class SubscribedActions
        {
            public Action<AomCoBrokeringStatusChangeRequest> AomCoBrokeringStatusChangeRequest { get; set; }
        }
    }
}
