﻿namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    using System;
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql;
    using AOM.Services.ProductService;
    using AOM.Transport.Events.Products;
    using AOM.Transport.Service.ProductsProcessor.Consumers;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class GetProductPrivilegesConsumerTests
    {
        private Mock<IBus> _mockBus;

        [Test]
        public void ShouldReturnASubscriptionId()
        {
            var mockCrmAdministrationService = new Mock<ICrmAdministrationService>();
            _mockBus = new Mock<IBus>();

            var getProductPrivilegesConsumer = new GetProductPrivilegesConsumer(
                mockCrmAdministrationService.Object,
                _mockBus.Object);

            Assert.That(getProductPrivilegesConsumer.SubscriptionId, Is.EqualTo("AOM"));
        }

        [Test]
        public void ShouldReturnAListOfProductAndSubscriberPrivileges()
        {
            var mockCrmAdministrationService = new Mock<ICrmAdministrationService>();
            _mockBus = new Mock<IBus>();

            var getProductPrivilegesConsumer = new GetProductPrivilegesConsumer(
                mockCrmAdministrationService.Object,
                _mockBus.Object);
            var productPrivileges = new List<IProductPrivilege>
            {
                new ProductPrivilege
                {
                    Description = "PRV1",
                    DefaultRoleGroup = "A",
                    Id = 1,
                    MarketMustBeOpen = true,
                    Name = "PRV 1",
                    OrganisationType = OrganisationType.Trading
                }
            };

            var systemPrivileges =
                new List<ISystemPrivilege>();

            systemPrivileges.Add(
                new SystemPrivilege
                {
                    ArgusUseOnly = true,
                    DefaultRoleGroup = "B",
                    Description = "SYSPRV1",
                    Id = 100,
                    Name = "SYSPRV1",
                    OrganisationType = OrganisationType.Brokerage
                });

            mockCrmAdministrationService.Setup(m => m.GetProductPrivileges()).Returns(productPrivileges);
            mockCrmAdministrationService.Setup(m => m.GetSystemPrivileges()).Returns(systemPrivileges);

            Func<GetProductSystemPrivilegesRequestRpc, GetProductSystemPrivilegesResponse> respondFn = null;

            _mockBus.Setup(
                b =>
                    b.Respond(
                        It.IsAny<Func<GetProductSystemPrivilegesRequestRpc, GetProductSystemPrivilegesResponse>>()))
                .Callback<Func<GetProductSystemPrivilegesRequestRpc, GetProductSystemPrivilegesResponse>>(
                    f => respondFn = f);

            getProductPrivilegesConsumer.Start();

            var privileges = respondFn(new GetProductSystemPrivilegesRequestRpc());

            if (privileges != null)
            {
                Assert.That(privileges.ProductPrivileges.Count, Is.EqualTo(1));
                Assert.That(privileges.SystemPrivileges.Count, Is.EqualTo(1));
            }
        }
    }
}