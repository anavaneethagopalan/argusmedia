﻿using AOM.App.Domain.Services;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    using AOM.Services.ProductService;
    using Argus.Transport.Infrastructure;
    using ProductsProcessor.Consumers;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    internal class GetProductSettingsConsumerTests
    {
        [Test]
        public void ShouldReturnSubscriptionId()
        {
            var mockBus = new Mock<IBus>();
            var mockProductService = new Mock<IProductService>();
            var mockUserService = new Mock<IUserService>();

            var consumer = new GetProductSettingsConsumer(mockBus.Object, mockProductService.Object, mockUserService.Object);

            Assert.That(consumer.SubscriptionId, Is.EqualTo("AOM"));
        }
    }
}