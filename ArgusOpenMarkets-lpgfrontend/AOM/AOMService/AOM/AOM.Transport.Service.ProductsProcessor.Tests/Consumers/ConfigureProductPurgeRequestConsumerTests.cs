﻿using AOM.Repository.MySql;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Service.ProductsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.ProductsProcessor.Tests.Consumers
{
    [TestFixture]
    public class ConfigureProductPurgeRequestConsumerTests
    {
        [Test]
        public void ShouldReturnServiceNameOfConfigureProductPurgeRequestConsumer()
        {
            var mockDbContextFactory = new Mock<IDbContextFactory>();
            var mockProductService = new Mock<IProductService>();
            var mockBus = new Mock<IBus>();
            var mockErrorService = new Mock<IErrorService>();

            var configureProductPurgeRequestConsumer = new ConfigureProductPurgeRequestConsumer(
                mockDbContextFactory.Object,
                mockProductService.Object,
                mockBus.Object, mockErrorService.Object);

            Assert.That(configureProductPurgeRequestConsumer.SubscriptionId,
                Is.EqualTo("ConfigureProductPurgeRequestConsumer"));
        }

    }
}