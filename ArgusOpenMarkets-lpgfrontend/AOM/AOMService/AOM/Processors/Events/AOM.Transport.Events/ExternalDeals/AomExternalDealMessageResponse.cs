﻿namespace AOM.Transport.Events.ExternalDeals
{
    using AOM.App.Domain.Entities;

    public class AomExternalDealMessageResponse
    {
        public Message Message { get; set; }

        public ExternalDeal ExternalDeal { get; set; }

        public string ObjectJson { get; set; }
    }
}
