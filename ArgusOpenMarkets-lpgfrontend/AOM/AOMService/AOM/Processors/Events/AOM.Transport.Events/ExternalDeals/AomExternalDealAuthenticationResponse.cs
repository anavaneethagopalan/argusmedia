﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.ExternalDeals
{
    public class AomExternalDealAuthenticationResponse : AomAuthenticationResponse
    {
        public ExternalDeal ExternalDeal { get; set; }
        //public PriceBasis PriceBasis { get; set; }
    }
}