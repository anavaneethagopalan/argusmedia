﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.ExternalDeals
{
    public class AomExternalDealRequest : AomRequest
    {
        public ExternalDeal ExternalDeal { get; set; }
    }
}