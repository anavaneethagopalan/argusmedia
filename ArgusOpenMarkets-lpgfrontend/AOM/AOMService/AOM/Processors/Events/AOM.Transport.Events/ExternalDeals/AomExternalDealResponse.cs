﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.ExternalDeals
{
    public class AomExternalDealResponse : AomResponse
    {
        public ExternalDeal ExternalDeal { get; set; }
        public UserContactDetails UserContactDetails { get; set; }
    }
}