﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.ExternalDeals
{
    public class AomExternalDealAuthenticationRequest : AomAuthenticationRequest
    {
        public ExternalDeal ExternalDeal { get; set; }
    }
}