﻿using System;
using Argus.Transport.Messages.Events;
using PushTechnology.ClientInterface.Client.Session;

namespace AOM.Transport.Events
{
    public class LoggedOnUserInfo
    {
        public DateTime LastSeen { get; set; }
        public long UserId { get; set; }
    }

    public class UserSessionInfo
    {
        public SessionId SessionId;
        public DateTime LastSeen;

        public bool SessionTerminatedByAnotherLogin;
    }

    public interface IClientSessionInfo
    {
        long UserId { get; set; }
        long OrganisationId { get; set; }
        string SessionId { get; set; }
        bool Equals(IClientSessionInfo other);
        int GetHashCode();
        string ToUserOrgString();
    }

    public class ClientSessionInfo : IClientSessionInfo
    {
        protected bool Equals(ClientSessionInfo other)
        {
            return UserId == other.UserId && OrganisationId == other.OrganisationId && string.Equals(SessionId, other.SessionId);
        }

        public long UserId { get; set; }
        public long OrganisationId { get; set; }
        public string SessionId { get; set; }

        public bool Equals(IClientSessionInfo other)
        {
            return UserId == other.UserId && OrganisationId == other.OrganisationId && string.Equals(SessionId, other.SessionId);
        }

        public string ToUserOrgString()
        {
            return String.Format("{0}:{1}", OrganisationId, UserId);
        }

        public override bool Equals(Object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ClientSessionInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = UserId.GetHashCode();
                hashCode = (hashCode * 397) ^ OrganisationId.GetHashCode();
                hashCode = (hashCode * 397) ^ (SessionId != null ? SessionId.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    public class AomRequest : DomainEvent
    {
        public new string EventName { get{return GetType().Name;} }
        public ClientSessionInfo ClientSessionInfo { get; set; }
        public MessageAction MessageAction { get; set; }
    }

    public class AomAuthenticationRequest : AomRequest
    {
    }

    public class AomResponse : DomainEvent
    {
        public new string EventName { get { return GetType().Name; } }
        public Message Message { get; set; }
    }

    public class AomAuthenticationResponse : AomResponse
    {
        public Boolean MarketMustBeOpen { get; set; }
    }
}