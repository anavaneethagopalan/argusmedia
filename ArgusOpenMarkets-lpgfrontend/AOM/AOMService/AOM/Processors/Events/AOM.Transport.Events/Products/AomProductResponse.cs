﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class AomProductResponse : AomResponse
    {
        public Product Product { get; set; }
    }
}