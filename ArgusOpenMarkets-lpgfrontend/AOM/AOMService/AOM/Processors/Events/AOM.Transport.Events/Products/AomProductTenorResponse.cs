using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class AomProductTenorResponse : AomResponse
    {
        public ProductTenor ProductTenor { get; set; }
    }
}