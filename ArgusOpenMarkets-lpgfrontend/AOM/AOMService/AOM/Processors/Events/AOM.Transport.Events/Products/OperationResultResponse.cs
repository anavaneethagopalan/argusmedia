﻿namespace AOM.Transport.Events.Products
{
    public class OperationResultResponse
    {
        public static OperationResultResponse CreateErroResponse(string errorMessage)
        {
            return new OperationResultResponse(false, errorMessage);
        }

        public static OperationResultResponse CreateSuccessResponse()
        {
            return new OperationResultResponse(true, string.Empty);
        }

        private OperationResultResponse(bool isValid, string errorMessage)
        {
            IsValid = isValid;
            ErrorMessage = errorMessage;
        }

        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }
    }
}