﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class GetProductSettingsResponse
    {
        public List<ProductSetting> ProductSettings { get; set; }
    }
}