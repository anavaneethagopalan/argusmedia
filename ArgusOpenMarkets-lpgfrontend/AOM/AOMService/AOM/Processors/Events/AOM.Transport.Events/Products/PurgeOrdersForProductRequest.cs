﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Products
{
    public class PurgeOrdersForProductRequest : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "PurgeOrdersForProductRequest";
            }
        }

        public int ProductId { get; set; }

        public int UserId { get; set; }
    }

    public class PurgeOrdersForProductResponse : DomainEvent
    {
        public new string EventName
        {
            get { return "PurgeOrdersForProductRequest"; }
        }

        public int ProductId { get; set; }

        public int UserId { get; set; }
    }
}
