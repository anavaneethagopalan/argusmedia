﻿using System.Collections.Generic;
using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Products
{
    public class GetProductIdsResponse : DomainEvent
    {
        public List<long> ProductIds { get; set; }
    }
}
