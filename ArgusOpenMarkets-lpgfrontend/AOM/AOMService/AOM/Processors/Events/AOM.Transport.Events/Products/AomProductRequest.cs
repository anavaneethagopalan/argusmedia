﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class AomProductRequest : AomRequest
    {
        public Product Product { get; set; }
    }
}