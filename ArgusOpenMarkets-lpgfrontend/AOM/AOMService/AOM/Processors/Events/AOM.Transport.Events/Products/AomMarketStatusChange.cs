﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class AomMarketStatusChange : AomResponse
    {
        public Product Product { get; set; }
        public MarketStatus Status { get; set; }
    }
}