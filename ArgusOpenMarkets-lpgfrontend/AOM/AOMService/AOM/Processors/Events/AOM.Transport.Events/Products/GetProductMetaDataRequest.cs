﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class GetProductMetaDataRequest : AomRequest
    {
        public GetProductMetaDataRequest()
        {
            MessageAction = MessageAction.Refresh;
        }
        public long? ProductId { get; set; }
    }

    public class GetMetadataListsRequest : AomRequest
    {
    }

    public class GetMetadataListsResponse : AomResponse
    {
        public List<MetaDataList> MetaDataLists { get; set; }
    }

    public class UpdateMetadataListsRequest : AomRequest
    {
        public List<MetaDataList> MetaDataLists { get; set; }
    }

    public class UpdateProductMetadataItemsDisplayOrderRequest : AomRequest
    {
        public UpdateProductMetadataItemsDisplayOrderRequest()
        {
            MessageAction = MessageAction.Update;
        }

        public long ProductId { get; set; }
        public long[] MetadataItemIds { get; set; }
    }
}