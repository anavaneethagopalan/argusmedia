﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class AomProductTenorRequest : AomRequest
    {
        public ProductTenor Tenor { get; set; }
    }
}