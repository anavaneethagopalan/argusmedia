﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class GetProductSystemPrivilegesResponse
    {
        public List<ProductPrivilege> ProductPrivileges { get; set; }

        public List<SystemPrivilege> SystemPrivileges { get; set; }
    }
}