﻿using Argus.Transport.Messages.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AOM.Transport.Events.Products
{
    public class ProductMarketCloseRequest : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "ProductMarketCloseRequest";
            }
        }

        public int ProductId { get; set; }

        public int UserId { get; set; }
    }
}
