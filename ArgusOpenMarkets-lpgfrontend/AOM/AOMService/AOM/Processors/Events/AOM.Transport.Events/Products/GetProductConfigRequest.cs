﻿namespace AOM.Transport.Events.Products
{
    public class GetProductConfigRequest : AomProductRequest
    {
        public long? ProductId { get; set; }
    }
}
	
