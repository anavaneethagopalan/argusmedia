﻿namespace AOM.Transport.Events.Products
{
    public class AomProductPurgeOrdersResponse : AomProductResponse
    {
        public long UserId { get; set; }
    }
}
