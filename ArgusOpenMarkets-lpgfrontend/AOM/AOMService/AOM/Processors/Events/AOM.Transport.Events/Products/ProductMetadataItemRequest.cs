﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Products
{
    public class ProductMetadataItemRequest : AomRequest
    {
        public ProductMetadataItemRequest(ProductMetaDataItem productMetaDataItem)
        {
            ProductMetaDataItem = productMetaDataItem;
        }

        public ProductMetaDataItem ProductMetaDataItem { get; set; }
    }
}