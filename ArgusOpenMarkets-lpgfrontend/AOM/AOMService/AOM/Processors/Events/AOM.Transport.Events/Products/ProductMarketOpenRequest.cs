﻿using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Products
{
    public class ProductMarketOpenRequest : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "ProductMarketOpenRequest";
            }
        }

        public int ProductId { get; set; }

        public int UserId { get; set; }
    }
}
