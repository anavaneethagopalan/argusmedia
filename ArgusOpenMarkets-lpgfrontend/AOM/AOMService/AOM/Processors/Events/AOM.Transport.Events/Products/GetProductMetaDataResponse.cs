﻿using AOM.App.Domain.Entities;
using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Products
{
    public class GetProductMetaDataResponse : DomainEvent
    {       
        public new string EventName
        {
            get
            {
                return "GetProductMetaDataResponse";
            }
        }

        public ProductMetaData ProductMetaData { get; set; }
    }
}