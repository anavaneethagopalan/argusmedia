﻿using AOM.App.Domain.Entities;
using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Products
{
    public class GetProductConfigResponse : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "GetProductConfigResponse";
            }
        }

        public ProductDefinitionItem ProductDefinition { get; set; }
    }
}
