using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketTickers
{
    public class AomMarketTickerContinuousScrollResponseRpc
    {
        public IList<MarketTickerItem> MarketTickerItems { get; set; }
        public long EarliestTickerItemId { get; set; }
        public string ErrorMessage { get; set; }
    }
}