﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketTickers
{
    public class AomNewMarketTickResponse : AomResponse
    {
        public MarketTickerItem MarketTickerItem { get; set; }
        public AdditionalMarketTickerData AdditionalData { get; set; }
    }
}

