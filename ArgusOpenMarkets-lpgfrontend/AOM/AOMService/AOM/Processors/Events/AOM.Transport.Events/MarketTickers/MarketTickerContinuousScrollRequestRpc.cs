﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketTickers
{
    public class AomMarketTickerContinuousScrollRequestRpc
    {
        public ClientSessionInfo ClientSessionInfo { get; set; }
        public long EarliestMarketTickerItemId { get; set; }
        public int NumberOfMarketTickerItemsRequired { get; set; }
        public bool Pending { get; set; }
        public IList<long> ProductIdList { get; set; }
        public IList<MarketTickerItemType> MarketTickerItemTypes { get; set; }
    }
}