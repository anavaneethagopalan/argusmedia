﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketTickers
{
    public class AomNewMarketConfigResponse : AomResponse
    {
        public MarketTickerConfig MarketTickerConfig { get; set; }
    }
}