﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketTickers
{
    public class AomMarketTickerRequest : AomRequest
    {
        public MarketTickerItem MarketTickerItem { get; set; }
    }
}