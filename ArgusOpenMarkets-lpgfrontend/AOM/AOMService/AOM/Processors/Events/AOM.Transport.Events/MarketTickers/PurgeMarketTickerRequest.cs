﻿using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.MarketTickers
{
    public class PurgeMarketTickerRequest : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "PurgeMarketTickerRequest";
            }
        }

        public int DaysToKeep { get; set; }
    }

    public class PurgeMarketTickerResponse : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "PurgeMarketTickerResponse";
            }
        }

        public int DaysToKeep { get; set; }
    }
}
