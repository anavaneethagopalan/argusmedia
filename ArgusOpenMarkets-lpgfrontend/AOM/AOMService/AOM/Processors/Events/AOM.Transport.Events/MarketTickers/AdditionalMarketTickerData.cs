﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketTickers
{
    public class AdditionalMarketTickerData
    {
        public DealStatus? PreviousDealStatus { get; set; }
    }
}