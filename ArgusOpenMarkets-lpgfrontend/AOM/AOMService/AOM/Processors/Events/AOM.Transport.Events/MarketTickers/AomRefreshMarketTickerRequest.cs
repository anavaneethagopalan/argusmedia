﻿using System;

namespace AOM.Transport.Events.MarketTickers
{
    public class AomRefreshMarketTickerRequest : AomMarketTickerRequest
    {
        public DateTime RefreshFromTime { get; set; }
        public DateTime RefreshToTime { get; set; }
    }
}