﻿namespace AOM.Transport.Events.MarketTickers
{
    public class AomMarketTickerItemMessageResponse
    {
        public Message Message { get; set; }
    }
}
