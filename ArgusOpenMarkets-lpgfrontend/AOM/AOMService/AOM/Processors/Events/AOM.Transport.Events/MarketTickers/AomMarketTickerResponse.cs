﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketTickers
{
    public class AomMarketTickerResponse : AomResponse
    {
        public MarketTickerItem MarketTickerItem { get; set; }
    }
}