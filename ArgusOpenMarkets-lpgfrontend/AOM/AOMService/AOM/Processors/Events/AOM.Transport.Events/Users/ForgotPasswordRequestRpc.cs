﻿
namespace AOM.Transport.Events.Users
{
    public class ForgotPasswordRequestRpc
    {
        public string Username { get; set; }
        public string Url { get; set; }
    }
}
