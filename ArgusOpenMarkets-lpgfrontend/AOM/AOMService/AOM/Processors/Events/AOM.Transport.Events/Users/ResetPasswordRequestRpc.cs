﻿
namespace AOM.Transport.Events.Users
{
    public class ResetPasswordRequestRpc
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
