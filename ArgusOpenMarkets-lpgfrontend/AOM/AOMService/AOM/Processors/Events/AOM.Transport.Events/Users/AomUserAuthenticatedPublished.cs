﻿namespace AOM.Transport.Events.Users
{
    using AOM.App.Domain.Entities;
    using Argus.Transport.Messages.Events;

    public class AomUserAuthenticatedPublished : DomainEvent
    {
        public User User { get; set; }

        public bool IsAuthenticated { get; set; }

        public new string EventName
        {
            get { return "AomUserAuthenticatedPublished"; }
        }
    }
}