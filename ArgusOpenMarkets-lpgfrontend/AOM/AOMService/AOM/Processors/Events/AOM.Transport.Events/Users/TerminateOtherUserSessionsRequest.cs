﻿namespace AOM.Transport.Events.Users
{
    public class TerminateOtherUserSessionsRequest
    {
        public long UserId { get; set; }
        public string SessionId { get; set; }
    }
}