﻿namespace AOM.Transport.Events.Users
{
    public class TerminateUserRequest
    {
        public string UserName { get; set; }
        public long UserId { get; set; }
        public string Reason { get; set; }
    }
}
