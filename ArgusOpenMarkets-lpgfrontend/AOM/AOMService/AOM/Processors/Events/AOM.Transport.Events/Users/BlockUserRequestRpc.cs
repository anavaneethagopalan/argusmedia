﻿namespace AOM.Transport.Events.Users
{
    public class BlockUserRequestRpc 
    {
        public string UserName { get; set; }
        public long RequestorUserId { get; set; }
    }
}
