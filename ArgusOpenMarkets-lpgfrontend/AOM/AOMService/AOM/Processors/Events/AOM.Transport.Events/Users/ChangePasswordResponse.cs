﻿namespace AOM.Transport.Events.Users
{
    public class ChangePasswordResponse 
    {        
        public string UserName { get; set; }
        public string MessageBody { get; set; } 
        public bool Success { get; set; }        
    }
}
