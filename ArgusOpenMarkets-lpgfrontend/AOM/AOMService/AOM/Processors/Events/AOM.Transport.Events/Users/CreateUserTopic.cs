﻿namespace AOM.Transport.Events.Users
{
    public class CreateUserTopic
    {
        public string Username { get; set; }

        public long UserId { get; set; }
    }
}
