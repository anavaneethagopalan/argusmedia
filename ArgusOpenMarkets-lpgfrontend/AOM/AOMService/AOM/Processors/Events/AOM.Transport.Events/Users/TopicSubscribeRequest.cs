﻿namespace AOM.Transport.Events.Users
{
    public class TopicSubscribeRequest
    {
        public ClientSessionInfo ClientSessionInfo { get; set; }
    }
}
