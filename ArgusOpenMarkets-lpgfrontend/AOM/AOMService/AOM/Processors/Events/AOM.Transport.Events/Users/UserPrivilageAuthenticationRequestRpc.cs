namespace AOM.Transport.Events.Users
{
    public class UserPrivilageAuthenticationRequestRpc
    {
        public string Username { get; set; }
        public long ProductId { get; set; }
        public long OrganisationId { get; set; }
        public string PrivilegeName { get; set; }
    }
}