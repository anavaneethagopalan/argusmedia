﻿namespace AOM.Transport.Events.Users
{
    public class KillUsersOrderResponseRpc
    {
        public string MessageBody { get; set; }
        public bool Success { get; set; }
    }
}
