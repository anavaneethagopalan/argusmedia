﻿namespace AOM.Transport.Events.Users
{
    public class UnblockUserRequestRpc
    {
        public string UserName { get; set; }
        public long RequestorUserId { get; set; }
    }
}
