﻿namespace AOM.Transport.Events.Users
{
    public class GetUserIdRequestRpc
    {
        public string Username { get; set; }
    }
}