﻿using System.Collections.Generic;

namespace AOM.Transport.Events.Users
{
    public class GetLoggedOnUsersResponse
    {
        public List<LoggedOnUserInfo> LoggedOnUsers { get; set; }
    }
}
