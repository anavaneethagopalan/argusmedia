﻿namespace AOM.Transport.Events.Users
{
    public class TerminateOtherUserSessionsResponse : AomResponse
    {
        public long UserId { get; set; }
        public string SessionId { get; set; }
        public string Reason { get; set; }
        public int LoginSessionsAllowed { get; set; }
    }
}