﻿using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Users
{
    public class GetUserRequest : DomainEvent
	{
		public new string EventName
		{
			get
			{
				return "GetUserRequest";
			}
		}

		public long UserId { get; set; }
        public string Username { get; set; }
	}
}