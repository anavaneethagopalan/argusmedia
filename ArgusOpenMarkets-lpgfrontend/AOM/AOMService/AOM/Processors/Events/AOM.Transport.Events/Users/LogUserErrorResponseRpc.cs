﻿namespace AOM.Transport.Events.Users
{
    public class LogUserErrorResponseRpc
    {
        public string UserErrorMessage { get; set; }
    }
}
