﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Users
{
    public class KillUsersOrderRequestRpc
    {
        public long UserId { get; set; }
        public Order Order { get; set; }
        public long RequestingUserId { get; set; }
    }
}
