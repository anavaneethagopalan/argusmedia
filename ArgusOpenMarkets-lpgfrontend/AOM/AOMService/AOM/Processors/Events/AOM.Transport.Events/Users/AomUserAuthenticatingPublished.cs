﻿using AOM.App.Domain.Entities;
using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Users
{
    public  class AomUserAuthenticatingPublished : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "AomUserAuthenticatingPublished";
            }
        }

        public User User { get; set; }
    }
}
