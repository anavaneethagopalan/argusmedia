﻿namespace AOM.Transport.Events.Users
{
    public class GetUserIdResponseRpc
    {
        public long UserId { get; set; }
    }
}