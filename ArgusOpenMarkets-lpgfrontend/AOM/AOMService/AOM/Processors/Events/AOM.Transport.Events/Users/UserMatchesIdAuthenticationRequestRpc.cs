namespace AOM.Transport.Events.Users
{
    public class UserMatchesIdAuthenticationRequestRpc
    {
        public string Username { get; set; }
        public long UserId { get; set; }
    }
}