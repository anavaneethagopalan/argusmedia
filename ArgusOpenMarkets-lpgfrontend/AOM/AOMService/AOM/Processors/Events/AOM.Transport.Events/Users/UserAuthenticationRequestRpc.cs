namespace AOM.Transport.Events.Users
{
    public class UserAuthenticationRequestRpc
    {
        public string Password { get; set; }
        public string IpAddress { get; set; }
        public string Username { get; set; }
        public string LoginSource { get; set; }
    }
}