﻿using System;

namespace AOM.Transport.Events.Users
{
    public class LogUserErrorRequestRpc
    {
        public string ErrorText { get; set; }

        public string Source { get; set; }

        public Exception Error { get; set; }

        public string AdditionalInformation { get; set; }
    }
}
