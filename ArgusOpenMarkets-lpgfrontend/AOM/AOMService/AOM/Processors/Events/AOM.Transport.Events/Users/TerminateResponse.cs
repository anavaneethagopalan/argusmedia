﻿namespace AOM.Transport.Events.Users
{
    public class TerminateResponse : AomResponse
    {
        public string UserName { get; set; }
        public long UserId { get; set; }
        public string Reason { get; set; }
    }
}
