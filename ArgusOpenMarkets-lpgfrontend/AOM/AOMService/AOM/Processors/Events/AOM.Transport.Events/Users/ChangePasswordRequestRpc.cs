﻿namespace AOM.Transport.Events.Users
{
    public class ChangePasswordRequestRpc 
    {
        public string Username { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public bool CheckOldPassword { get; set; }
        public bool AdminResetPassword { get; set; }
        public int TempPassExpirInHours { get; set; }
        public long RequestorUserId { get; set; }
        public bool IsTemporary { get; set; }
    }
}
