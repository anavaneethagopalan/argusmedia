﻿namespace AOM.Transport.Events.Users
{
    public class UserContentStreamAuthenticationRequestRpc
    {
        public long ContentStreamId { get; set; }
        public string Username { get; set; }
    }
}