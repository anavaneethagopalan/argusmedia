﻿using AOM.App.Domain.Entities;
using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Users
{
    public interface IGetUserResponse
    {
        string EventName { get; }

        User User { get; set; }
    }

    public class GetUserResponse : DomainEvent, IGetUserResponse
    {
        public new string EventName
        {
            get
            {
                return "GetUserResponse";
            }
        }

        public User User { get; set; }
        public long UserId { get; set; }
    }
}