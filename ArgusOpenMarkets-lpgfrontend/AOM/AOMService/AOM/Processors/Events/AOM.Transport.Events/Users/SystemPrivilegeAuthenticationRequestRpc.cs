﻿namespace AOM.Transport.Events.Users
{
    public class SystemPrivilegeAuthenticationRequestRpc
    {
        public long UserId { get; set; }
        public string PrivilegeName { get; set; }
    }
}