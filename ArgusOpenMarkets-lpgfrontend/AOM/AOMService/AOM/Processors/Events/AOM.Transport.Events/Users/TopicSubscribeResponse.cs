﻿using System.Collections.Generic;

namespace AOM.Transport.Events.Users
{
    public class TopicSubscribeResponse
    {
        public ClientSessionInfo ClientSessionInfo { get; set; }
        public List<string> TopicsSubscribe { get; set; }
    }
}
