﻿namespace AOM.Transport.Events.Users
{
    public class AuthenticateUserTokenRequestRpc
    {
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
