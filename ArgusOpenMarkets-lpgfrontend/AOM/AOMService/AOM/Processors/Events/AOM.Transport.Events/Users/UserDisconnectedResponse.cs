﻿namespace AOM.Transport.Events.Users
{
    public class UserDisconnectedResponse
    {
        public long UserId { get; set; }
        public string Username { get; set; }

        public string DisconnectReason { get; set; }
    }
}