﻿namespace AOM.Transport.Events.Users
{
    public class SessionTokenAuthenticatonRequest
    {
        public string SessionTokenString { get; set; }

        public string UserId { get; set; }
    }
}