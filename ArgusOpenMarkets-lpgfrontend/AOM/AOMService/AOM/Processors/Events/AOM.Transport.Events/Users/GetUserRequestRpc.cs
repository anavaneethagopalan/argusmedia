﻿using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Users
{
    public class GetUserRequestRpc : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "GetUserRequestRpc";
            }
        }

        public long UserId { get; set; }
        public string Username { get; set; }
    }
}
