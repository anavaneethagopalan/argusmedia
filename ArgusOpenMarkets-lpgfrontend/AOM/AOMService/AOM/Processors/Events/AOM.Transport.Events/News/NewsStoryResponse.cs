﻿namespace AOM.Transport.Events.News
{
    public class NewsStoryResponse
    {
        public string CmsId { get; set; }

        public string Story { get; set; }
    }
}
