﻿namespace AOM.Transport.Events.News
{
    using Argus.Transport.Messages.Events;

    public class PurgeNewsRequest : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "PurgeArgusNewsRequest";
            }
        }

        public int DaysToKeep { get; set; }
    }
}
