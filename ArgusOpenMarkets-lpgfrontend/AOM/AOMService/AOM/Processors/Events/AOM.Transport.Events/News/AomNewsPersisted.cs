﻿using AOM.App.Domain.Interfaces;
using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.News
{
    public class AomNewsPersisted : DomainEvent
	{
		public new string EventName
		{
			get
			{
				return "AomNewsPersisted";
			}
		}

		public IAomNews News { get; set; }
	}
}
