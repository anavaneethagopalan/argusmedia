﻿namespace AOM.Transport.Events.News
{
    using Argus.Transport.Messages.Events;

    public class ReplayArgusNews : DomainEvent
    {
        public int? DaysToReplay { get; set; }

        public decimal[] ContentStreamsReplay { get; set; }

        public new string EventName
        {
            get
            {
                return "ReplayArgusNews";
            }
        }
    }
}