﻿namespace AOM.Transport.Events.News
{

    public class AomNewsDeleted
    {
        public string CmsId { get; set; }

        public long[] ContentStreams { get; set; }

        public bool IsFree { get; set; }

        public ulong CommodityId { get; set; }
    }
}
