﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.News
{
    public class AomNewsPublished : AomResponse
    {
        public AomNews News { get; set; }
    }
}
