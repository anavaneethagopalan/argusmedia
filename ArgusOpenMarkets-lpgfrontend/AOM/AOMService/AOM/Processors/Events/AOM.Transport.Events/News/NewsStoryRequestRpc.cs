﻿namespace AOM.Transport.Events.News
{
    public class NewsStoryRequestRpc 
    {
        public string CmsId { get; set; }
        public long UserId { get; set; }
    }
}
