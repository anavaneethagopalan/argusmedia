﻿namespace AOM.Transport.Events.News
{
    using Argus.Transport.Messages.Events;

    public class PurgeNewsResponse : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "PurgeArgusNewsResponse";
            }
        }

        public int DaysToKeep { get; set; }
    }
}
