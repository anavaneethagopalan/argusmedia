﻿namespace AOM.Transport.Events.News
{
    using AOM.App.Domain.Entities;

    public class AomNewsUpdated : AomResponse
    {
        public AomNews PreviousNews { get; set; }
        public AomNews UpdatedNews { get; set; }
    }
}
