﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.BrokerPermissions
{
    public class AomBrokerPermissionAuthenticationRequest : AomAuthenticationRequest
    {
        public List<MatrixPermission> BrokerPermissions { get; set; }
    }
}