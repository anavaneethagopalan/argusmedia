﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.BrokerPermissions
{
    public class AomBrokerPermissionRequest : AomRequest
    {
        public List<MatrixPermission> BrokerPermissions { get; set; }
    }
}