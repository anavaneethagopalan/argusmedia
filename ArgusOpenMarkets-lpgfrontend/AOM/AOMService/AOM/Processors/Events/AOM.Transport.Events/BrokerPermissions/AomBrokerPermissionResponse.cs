﻿using System.Collections.Generic;

using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.BrokerPermissions
{
    public class AomBrokerPermissionResponse : AomResponse
    {
        public List<long> AlteredBrokerIds { get; set; }
        public List<MatrixPermission> BrokerPermissions { get; set; }
        public bool UpdateSuccessful { get; set; }
    }
}