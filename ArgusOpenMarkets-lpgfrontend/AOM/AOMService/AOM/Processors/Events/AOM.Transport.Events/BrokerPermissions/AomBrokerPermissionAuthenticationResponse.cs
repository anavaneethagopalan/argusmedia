﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.BrokerPermissions
{
    public class AomBrokerPermissionAuthenticationResponse : AomAuthenticationResponse
    {
        public List<MatrixPermission> BrokerPermissions { get; set; }
    }
}