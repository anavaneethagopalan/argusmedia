﻿namespace AOM.Transport.Events.Orders
{
    using global::System.Collections.Generic;

    public class PurgeOrdersInDiffusionRequest
    {
        public List<long> ProductsToPurgeOrdersFor { get; set; }
    }
}