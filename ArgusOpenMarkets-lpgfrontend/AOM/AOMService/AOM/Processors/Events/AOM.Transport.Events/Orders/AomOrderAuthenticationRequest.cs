﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Orders
{
    public class AomOrderAuthenticationRequest : AomAuthenticationRequest
    {
        public Order Order { get; set; }
    }
}