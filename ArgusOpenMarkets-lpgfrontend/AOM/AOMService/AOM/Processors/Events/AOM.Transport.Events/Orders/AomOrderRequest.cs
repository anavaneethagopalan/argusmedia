﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Orders
{
    public class AomOrderRequest : AomRequest
    {
        public Order Order { get; set; }
    }
}