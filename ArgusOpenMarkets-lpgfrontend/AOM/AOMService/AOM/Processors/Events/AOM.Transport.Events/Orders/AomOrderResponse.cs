﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Orders
{
    public class AomOrderResponse : AomResponse
    {
        public Order Order { get; set; }
    }
}