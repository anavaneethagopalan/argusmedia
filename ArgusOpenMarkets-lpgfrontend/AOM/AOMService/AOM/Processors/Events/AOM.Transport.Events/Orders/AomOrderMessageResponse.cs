﻿namespace AOM.Transport.Events.Orders
{
    using AOM.App.Domain.Entities;

    public class AomOrderMessageResponse
    {
        public Message Message { get; set; }

        public string ObjectJson { get; set; }
    }
}