﻿using System;

namespace AOM.Transport.Events.Orders
{
    public class AomOrderRefreshRequest : AomRequest
    {
        public long ProductId { get; set; }
        public DateTime BusinessDay { get; set; }
    }
}