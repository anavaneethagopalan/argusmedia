﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Orders
{
    public class AomOrderAuthenticationResponse : AomAuthenticationResponse
    {
        public Order Order { get; set; }
    }
}