﻿namespace AOM.Transport.Events.Emails
{
    public class AomSendEmailRequest : AomRequest
    {
        public long EmailId { get; set; }
    }
}
