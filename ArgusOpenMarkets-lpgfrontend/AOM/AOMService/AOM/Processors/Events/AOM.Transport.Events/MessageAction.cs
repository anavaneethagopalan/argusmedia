﻿namespace AOM.Transport.Events
{
    public enum MessageAction
    {
        Create,

        Execute,

        Hold,

        Kill,

        Register,

        Refresh,

        Reinstate,

        Request,

        Update,

        Void,

        Verify,

        ConfigurationChange,

        Open,

        Close,

        Purge,

        ConfigurePurge,

        Send,

        Check,

        Delete,

        List,

        Clear
    }
}
