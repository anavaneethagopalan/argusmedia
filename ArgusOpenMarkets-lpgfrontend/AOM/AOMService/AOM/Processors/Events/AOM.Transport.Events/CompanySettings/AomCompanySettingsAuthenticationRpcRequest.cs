﻿using AOM.App.Domain.Entities;
using AOM.Transport.Events.Organisations;

namespace AOM.Transport.Events.CompanySettings
{
    public class AomCompanySettingsAuthenticationRpcRequest : AomAuthenticationRequest
    {
        public CompanyDetailsRpcRequest CompanyDetails { get; set; }
    }
}