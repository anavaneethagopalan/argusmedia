﻿using AOM.App.Domain.Entities;
using AOM.Transport.Events.Organisations;

namespace AOM.Transport.Events.CompanySettings
{
    public class AomCompanySettingsAuthenticationRpcResponse : AomAuthenticationResponse
    {
        public CompanyDetailsRpcRequest CompanyDetails { get; set; }
    }
}