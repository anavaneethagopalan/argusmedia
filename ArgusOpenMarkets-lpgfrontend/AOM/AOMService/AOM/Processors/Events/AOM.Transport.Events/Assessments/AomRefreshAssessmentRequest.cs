﻿namespace AOM.Transport.Events.Assessments
{
    public class AomRefreshAssessmentRequest : AomRequest
    {
        public long ProductId { get; set; }

        public long LastUpdatedUserId { get; set; }
    }
}