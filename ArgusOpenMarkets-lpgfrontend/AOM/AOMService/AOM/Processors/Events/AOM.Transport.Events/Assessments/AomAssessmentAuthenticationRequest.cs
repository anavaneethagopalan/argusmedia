﻿
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Assessments
{
    public class AomAssessmentAuthenticationRequest : AomAuthenticationRequest
    {
        public long ProductId { get; set; }
        public CombinedAssessment Assessment { get; set; }
    }
}
