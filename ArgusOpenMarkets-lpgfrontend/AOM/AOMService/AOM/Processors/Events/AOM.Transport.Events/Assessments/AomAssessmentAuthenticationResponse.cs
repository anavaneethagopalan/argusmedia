﻿
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Assessments
{
    public class AomAssessmentAuthenticationResponse : AomAuthenticationResponse
    {
        public long ProductId { get; set; }
        public CombinedAssessment Assessment { get; set; }
    }
}
