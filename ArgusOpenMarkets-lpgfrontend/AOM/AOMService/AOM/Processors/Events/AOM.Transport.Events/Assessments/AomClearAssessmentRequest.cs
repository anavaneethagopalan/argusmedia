﻿namespace AOM.Transport.Events.Assessments
{
    public class AomClearAssessmentRequest : AomRequest
    {
        public long ProductId { get; set; }
        public string Period { get; set; }
        public long LastUpdatedUserId { get; set; }
    }
}