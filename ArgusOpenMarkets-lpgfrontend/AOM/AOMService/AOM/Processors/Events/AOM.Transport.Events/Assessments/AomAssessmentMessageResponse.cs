﻿namespace AOM.Transport.Events.Assessments
{
    using AOM.App.Domain.Entities;

    public class AomAssessmentMessageResponse
    {
        public Message Message { get; set; }

        public CombinedAssessment CombinedAssessment { get; set; }

        public AomRefreshAssessmentRequest AomRefreshAssessmentRequest { get; set; }

        public AomClearAssessmentRequest AomClearAssessmentRequest { get; set; }

        public string ObjectJson { get; set; }
    }
}
