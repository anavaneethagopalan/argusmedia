﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOM.Transport.Events
{
    public class Message
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public MessageType MessageType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public MessageAction MessageAction { get; set; }

        public object MessageBody { get; set; }

        [JsonIgnore]
        public ClientSessionInfo ClientSessionInfo { get; set; }
    }
}