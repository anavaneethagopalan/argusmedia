﻿using System.Collections.Generic;

using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.CounterpartyPermissions
{
    public class AomCounterpartyPermissionResponse : AomResponse
    {
        public List<long> AlteredOrganisationIds { get; set; } 
        public List<MatrixPermission> CounterpartyPermissions { get; set; }
        public bool UpdateSuccessful { get; set; }
    }
}