﻿using AOM.App.Domain.Entities;
using System.Collections.Generic;

namespace AOM.Transport.Events.CounterpartyPermissions
{
    public class AomCounterpartyPermissionRequest : AomRequest
    {
        public List<MatrixPermission> CounterpartyPermissions { get; set; }
    }
}