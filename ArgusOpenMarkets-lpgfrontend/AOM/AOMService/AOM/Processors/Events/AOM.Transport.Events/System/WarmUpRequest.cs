﻿namespace AOM.Transport.Events.System
{

    /// <summary>
    /// Used by processors to request initialisation on the EasyNetQ thread
    /// </summary>
    public class WarmUpRequest
    {
    }
}
