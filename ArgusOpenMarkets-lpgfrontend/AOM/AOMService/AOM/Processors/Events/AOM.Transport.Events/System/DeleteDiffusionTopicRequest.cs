﻿namespace AOM.Transport.Events.System
{
    public class DeleteDiffusionTopicRequest
    {
        public string TopicToDelete { get; set; }

        public long LoggedOnUserId { get; set; }
    }
}
