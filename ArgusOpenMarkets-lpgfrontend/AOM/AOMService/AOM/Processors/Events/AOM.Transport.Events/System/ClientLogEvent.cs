﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.System
{
    public class ClientLogEvent
    {
        public ClientLogging ClientLog { get; set; }
    }
}
