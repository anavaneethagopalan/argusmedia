﻿namespace AOM.Transport.Events
{
    public enum MessageType
    {
        Assessment,
        Deal,
        Error,
        ExternalDeal,
        Info,
        Logoff,
        MarketTickerItem,
        News,
        Order,
        Product,
        User,
        UserMessage,
        BrokerPermission,
        CounterpartyPermission,
        BrokerPermissionDetail,
        CounterpartyPermissionDetail,
        OrganisationNotificationDetail, 
        TopicSubscription,
        ClientLog
    }
}