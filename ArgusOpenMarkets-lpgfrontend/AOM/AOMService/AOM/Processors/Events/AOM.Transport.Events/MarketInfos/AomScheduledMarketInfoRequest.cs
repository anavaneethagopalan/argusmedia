﻿namespace AOM.Transport.Events.MarketInfos
{
    public class AomScheduledMarketInfoRequest
    {
        public string MessageText { get; set; }
        public string ProductChannels { get; set; }
    }
}