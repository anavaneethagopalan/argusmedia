﻿namespace AOM.Transport.Events.MarketInfos
{
    using AOM.App.Domain.Entities;

    public class AomMarketInfoMessageResponse
    {
        public Message Message { get; set; }

        public MarketInfo MarketInfo { get; set; }

        public string ObjectJson { get; set; }
    }
}
