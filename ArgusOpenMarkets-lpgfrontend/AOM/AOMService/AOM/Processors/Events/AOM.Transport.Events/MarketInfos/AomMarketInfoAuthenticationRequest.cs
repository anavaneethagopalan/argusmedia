﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketInfos
{
    public class AomMarketInfoAuthenticationRequest : AomAuthenticationRequest
    {
        public MarketInfo MarketInfo { get; set; }
    }
}