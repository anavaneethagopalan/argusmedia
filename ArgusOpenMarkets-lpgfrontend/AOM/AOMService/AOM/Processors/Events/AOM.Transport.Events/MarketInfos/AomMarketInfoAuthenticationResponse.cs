﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketInfos
{
    public class AomMarketInfoAuthenticationResponse : AomAuthenticationResponse
    {
        public MarketInfo MarketInfo { get; set; }
    }
}