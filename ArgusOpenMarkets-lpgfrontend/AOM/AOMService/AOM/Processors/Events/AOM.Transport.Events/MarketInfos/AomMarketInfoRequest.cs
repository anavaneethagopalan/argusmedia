﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketInfos
{
    public class AomMarketInfoRequest : AomRequest
    {
        public MarketInfo MarketInfo { get; set; }
    }
}