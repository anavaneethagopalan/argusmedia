﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.MarketInfos
{
    public class AomMarketInfoResponse : AomResponse
    {
        public MarketInfo MarketInfo { get; set; }
        public UserContactDetails UserContactDetails { get; set; }
    }
}