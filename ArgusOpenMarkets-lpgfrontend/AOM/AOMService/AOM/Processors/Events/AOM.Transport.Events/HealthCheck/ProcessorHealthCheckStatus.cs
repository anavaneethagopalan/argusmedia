﻿namespace AOM.Transport.Events.HealthCheck
{
    using global::System;

    public enum ProcessorStatusEnum
    {
        Green = 0,
        Amber = 1,
        Red = 2
    }

    public class ProcessorHealthCheckStatus
    {
        public ProcessorHealthCheckStatus()
        {
            ControlClientActive = false;
        }

        public string Processor { get; set; }

        public string ProcessorFullName { get; set; }

        public bool Status { get; set; }

        public DateTime ResponseTime { get; set; }

        public string MachineName { get; set; }

        public bool ControlClientActive { get; set; }

        public double ResponseTimeAge
        {
            get
            {
                TimeSpan span = (DateTime.UtcNow - ResponseTime);
                return span.TotalMilliseconds;
            }
        }

        public ProcessorStatusEnum ProcessorStatus
        {
            get
            {
                if (ResponseTimeAge < 30000)
                {
                    return ProcessorStatusEnum.Green;
                }
                if (ResponseTimeAge < 60000)
                {
                    return ProcessorStatusEnum.Amber;
                }

                return ProcessorStatusEnum.Red;
            }
        }
    }
}