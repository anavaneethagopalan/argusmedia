﻿namespace AOM.Transport.Events.HealthCheck
{
    public class HealthCheckStatusResult
    {
        public bool HealthCheckStatusOk { get; set; }
    }
}
