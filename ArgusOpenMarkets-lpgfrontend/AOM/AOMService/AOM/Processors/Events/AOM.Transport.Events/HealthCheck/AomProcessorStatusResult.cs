﻿namespace AOM.Transport.Events.HealthCheck
{
    using AOM.Transport.Events.Users;

    using global::System.Collections.Generic;

    public class AomProcessorStatusResult
    {
        public List<ProcessorHealthCheckStatus> ListProcessorHealthChecks { get; set; }
    }
}
