﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.ScheduledTasks
{
    public class CreateNewScheduledTaskRequestRpc
    {
        public ScheduledTask Task { get; set; }
    }
}
