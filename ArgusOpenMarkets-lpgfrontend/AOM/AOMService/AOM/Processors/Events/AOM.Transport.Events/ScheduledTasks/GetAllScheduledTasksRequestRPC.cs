﻿namespace AOM.Transport.Events.ScheduledTasks
{
    public class GetAllScheduledTasksRequestRpc
    {
        public string TaskType { get; set; }
    }
}
