﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.ScheduledTasks
{
    public class GetAllScheduledTasksResponseRpc
    {
        public List<ScheduledTask> ScheduledTasks { get; set; }
    }
}