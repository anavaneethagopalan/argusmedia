﻿using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.ScheduledTasks
{
    public class ScheduledTaskResponseRpc : DomainEvent
    {
        public ScheduledTaskResponseRpc()
        {
        }

        public ScheduledTaskResponseRpc(bool success, string message)
        {
            Success = success;
            Message = message;
        }

        public string Message { get; set; }
        public bool Success { get; set; }
    }
}