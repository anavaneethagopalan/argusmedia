﻿namespace AOM.Transport.Events.ScheduledTasks
{
    public class DeleteScheduledTaskRequestRpc
    {
        public long TaskId { get; set; }
    }
}