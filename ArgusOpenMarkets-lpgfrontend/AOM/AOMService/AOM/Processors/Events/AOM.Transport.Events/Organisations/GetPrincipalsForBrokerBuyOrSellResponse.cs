﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetPrincipalsForBrokerBuyOrSellResponse
    {
        public List<OrganisationPermissionSummary> Organisations { get; set; }
    }
}
