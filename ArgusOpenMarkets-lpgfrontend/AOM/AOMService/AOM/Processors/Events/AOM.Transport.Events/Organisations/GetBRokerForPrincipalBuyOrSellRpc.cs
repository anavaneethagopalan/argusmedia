﻿using System;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetBrokersForPrincipalBuyOrSellRpc
    {
        public long ProductId { get; set; }
        public long RequestedByUserId { get; set; }
        public OrderType OrderType { get; set; }
    }
}