﻿namespace AOM.Transport.Events.Organisations
{
    public class CompanyDetailsRpcProcessedResponse : AomResponse
    {
        public long Id { get; set; }
        public long EventTypeId { get; set; }
        public long OrganisationId { get; set; }
    }
}