﻿using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Organisations
{
    public class GetCounterpartyPermissionsRefreshRequest : DomainEvent
    {
        public bool ProcessorStartingUp { get; set; }

        public new string EventName
        {
            get { return "GetCounterpartyPermissionsRefreshRequest"; }
        }
    }
}