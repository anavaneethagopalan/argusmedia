﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetOrganisationResponse
    {
        public Organisation Organisation { get; set; }
    }
}