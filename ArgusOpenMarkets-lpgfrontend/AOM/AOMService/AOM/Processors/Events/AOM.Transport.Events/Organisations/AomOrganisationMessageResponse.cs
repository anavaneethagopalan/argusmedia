﻿namespace AOM.Transport.Events.Organisations
{
    public class AomOrganisationMessageResponse
    {
        public Message Message { get; set; }

        public string ObjectJson { get; set; }
    }
}