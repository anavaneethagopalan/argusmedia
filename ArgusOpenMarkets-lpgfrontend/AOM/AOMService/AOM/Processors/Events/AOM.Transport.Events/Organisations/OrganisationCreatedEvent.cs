﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class OrganisationCreatedEvent
    {
        public long UserId { get; set; }

        public OrganisationDetails OrganisationDetails { get; set; }
    }
}