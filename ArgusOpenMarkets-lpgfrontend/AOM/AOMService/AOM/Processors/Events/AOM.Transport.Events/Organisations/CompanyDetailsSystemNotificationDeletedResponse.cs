﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class CompanyDetailsSystemNotificationDeletedResponse
    {
        public long Id { get; set; }

        public long ProductId { get; set; }

        public long OrganisationId { get; set; }

        public long UserId { get; set; }

        public string EmailAddress { get; set; }

        public SystemEventType EventType { get; set; }

        public string Operation { get; set; }
    }
}