using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetCommonPermissionedOrganisationsRpc
    {
        public long ProductId { get; set; }
        public long RequestedByUserId { get; set; }
        public long OrganisationId { get; set; }
        public long? OrganisationBrokerageId { get; set; }
        public BuyOrSell BuyOrSell { get; set; }
    }
}