﻿using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Organisations
{
    public class GetCompanyDetailsRefreshRequest : DomainEvent
    {
        public new string EventName
        {
            get
            {
                return "GetCompanyDetailsRefreshRequest";
            }
        }
    }
}