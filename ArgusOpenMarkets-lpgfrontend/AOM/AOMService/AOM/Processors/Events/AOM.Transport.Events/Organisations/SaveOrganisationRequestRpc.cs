﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class SaveOrganisationRequestRpc
    {
        public Organisation Organisation { get; set; }
        public long UserId { get; set; }
    }
}