using AOM.App.Domain.Entities;
using Argus.Transport.Messages.Events;
using System.Collections.Generic;

namespace AOM.Transport.Events.Organisations
{
    public class GetBrokerPermissionsRefreshResponse : DomainEvent
    {
        public bool ProcessorStartingUp { get; set; }
        public IList<BilateralMatrixPermissionPair> BrokerPermissions { get; set; }
        public IList<long> OrganisationsAffected { get; set; }

        public new string EventName
        {
            get { return "GetBrokerPermissionsRefreshResponse"; }
        }
    }
}