﻿namespace AOM.Transport.Events.Organisations
{
    using AOM.App.Domain.Entities;

    using Argus.Transport.Messages.Events;

    using global::System.Collections.Generic;

    public class GetCompanyDetailsRefreshResponse : DomainEvent
    {
        public List<OrganisationDetails> Details { get; set; }

        public new string EventName
        {
            get
            {
                return "GetCompanyDetailsRefreshResponse";
            }
        }
    }
}