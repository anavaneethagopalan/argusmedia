﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetPrincipalsForBrokerBuyOrSellRpc
    {
        public long ProductId { get; set; }
        public long RequestedByUserId { get; set; }
        public OrderType OrderType { get; set; }
    }
}