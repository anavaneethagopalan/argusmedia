using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetPrincipalsForBrokerResponse
    {
        public OrganisationsWithPermissions PrincipalsWithPermissions { get; set; }
    }
}