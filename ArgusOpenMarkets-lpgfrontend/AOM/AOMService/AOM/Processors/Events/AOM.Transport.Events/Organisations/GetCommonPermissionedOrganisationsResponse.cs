﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetCommonPermissionedOrganisationsResponse
    {
        public List<Organisation> CommonOrganisations { get; set; }
    }
}