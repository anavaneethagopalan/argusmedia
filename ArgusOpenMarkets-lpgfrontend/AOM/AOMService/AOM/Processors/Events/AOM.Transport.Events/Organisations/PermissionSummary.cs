﻿namespace AOM.Transport.Events.Organisations
{
    public class PermissionSummary
    {
        public Message Message { get; set; }
        public bool CanTrade { get; set; }
        public long OrganisationId { get; set; }
    }
}