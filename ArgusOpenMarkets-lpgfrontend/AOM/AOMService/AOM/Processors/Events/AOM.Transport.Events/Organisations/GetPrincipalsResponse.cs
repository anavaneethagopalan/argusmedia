﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetPrincipalsResponse
    {
        public IList<Organisation> Principals { get; set; }
    }
}