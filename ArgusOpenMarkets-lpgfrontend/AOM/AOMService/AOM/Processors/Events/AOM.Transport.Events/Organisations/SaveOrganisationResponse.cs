﻿using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Organisations
{
    public class SaveOrganisationResponse : DomainEvent
	{
		public new string EventName
		{
			get
			{
				return "CreateOrganisationResponse";
			}
		}

        public long OrganisationId { get; set; }
	}
}