﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using Argus.Transport.Messages.Events;

namespace AOM.Transport.Events.Organisations
{
    public class GetCounterpartyPermissionsRefreshResponse:DomainEvent
    {
        public bool ProcessorStartingUp { get; set; }
        public IList<BilateralMatrixPermissionPair> CounterpartyPermissions { get; set; }
        public IList<long> OrganisationsAffected { get; set; }

        public new string EventName
        {
            get { return "GetCounterpartyPermissionsRefreshResponse"; }
        }
    }
}