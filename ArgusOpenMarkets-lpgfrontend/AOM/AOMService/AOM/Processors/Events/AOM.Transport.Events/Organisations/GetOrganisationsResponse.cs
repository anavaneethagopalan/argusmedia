﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetOrganisationsResponse
    {
        public IList<Organisation> Organisations { get; set; }
    }
}