﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetBrokersForPrincipalBuyOrSellResponse
    {
        public List<OrganisationPermissionSummary> BrokersWithPermissions { get; set; }
    }
}