﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetBrokersForPrincipalResponse
    {
        public OrganisationsWithPermissions BrokersWithPermissions { get; set; }        
    }
}