﻿namespace AOM.Transport.Events.Organisations
{
    public class GetPrincipalsForBrokerRpc
    {
        public long ProductId { get; set; }
        public long RequestedByUserId { get; set; }
    }
}