﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class OrganisationUpdatedEvent
    {
        public long UserId { get; set; }

        public Organisation Organisation { get; set; }
    }
}