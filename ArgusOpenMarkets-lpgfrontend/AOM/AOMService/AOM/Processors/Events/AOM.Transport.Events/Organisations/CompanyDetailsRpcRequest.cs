﻿namespace AOM.Transport.Events.Organisations
{
    using AOM.App.Domain.Entities;

    public class CompanyDetailsRpcRequest : AomRequest
    {
        public long Id { get; set; }

        public long ProductId { get; set; }

        public string EmailAddress { get; set; }

        public SystemEventType EventType { get; set; }
    }
}