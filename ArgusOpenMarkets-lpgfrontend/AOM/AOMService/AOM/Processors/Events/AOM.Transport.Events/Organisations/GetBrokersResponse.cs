﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class GetBrokersResponse
    {
        public IList<Organisation> Brokers { get; set; }
    }
}