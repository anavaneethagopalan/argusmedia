﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Organisations
{
    public class CompanyDetailsSystemNotificationRequest : AomRequest
    {
        public long Id { get; set; }

        public long ProductId { get; set; }

        public string EmailAddress { get; set; }

        public SystemEventType EventType { get; set; }
    }
}