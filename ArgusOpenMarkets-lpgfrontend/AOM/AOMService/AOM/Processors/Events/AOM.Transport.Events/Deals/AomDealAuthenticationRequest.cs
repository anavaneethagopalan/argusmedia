﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Deals
{
    public class AomDealAuthenticationRequest : AomAuthenticationRequest
    {
        public Deal Deal { get; set; }
    }
}