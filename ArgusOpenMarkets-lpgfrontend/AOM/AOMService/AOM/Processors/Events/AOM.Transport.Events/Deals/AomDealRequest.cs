﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Deals
{
    public class AomDealRequest : AomRequest
    {
        public Deal Deal { get; set; }
    }
}
