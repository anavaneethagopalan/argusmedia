﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Deals
{
    public class AomDealResponse : AomResponse
    {
        public Deal Deal { get; set; }
        public UserContactDetails UserContactDetails { get; set; }
    }
}
