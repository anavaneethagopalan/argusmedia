﻿using AOM.App.Domain.Entities;

namespace AOM.Transport.Events.Deals
{
    public class AomDealAuthenticationResponse : AomAuthenticationResponse
    {
        public Deal Deal { get; set; }
    }
}