﻿
namespace AOM.Transport.Events.Deals
{
    using AOM.App.Domain.Entities;

    public class AomInternalDealMessageResponse
    {
        public Message Message { get; set; }

        public Deal Deal { get; set; }

        public string ObjectJson { get; set; }
    }
}
