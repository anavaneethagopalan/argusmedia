﻿using AOM.App.Domain.Entities;
using System;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    public interface IProductStatusHelper
    {
        MarketStatus DetermineExpectedMarketStatus(Product product);
        DateTime GetNextMarketStatusChangeTime(Product product);
    }
}
