﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using System.Timers;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    public class TimersPurgeProductOrdersService : IConsumerWithInitialise
    {
        private static readonly object Padlock = new object();

        private readonly IBus _aomBus;

        private readonly IDateTimeProvider _dateTimeProvider;

        private readonly IProductService _productService;

        private DateTime _nextTime = DateTime.MaxValue;

        private Timer _timer;

        public TimersPurgeProductOrdersService(
            IBus aomBus,
            IProductService productService,
            IDateTimeProvider dateTimeProvider)
        {
            _aomBus = aomBus;
            _productService = productService;
            _dateTimeProvider = dateTimeProvider;
        }

        public void Start()
        {
            _aomBus.Subscribe<AomProductConfigurePurgeResponse>(
                SubscriptionUniqueToThisMachineAndConsumerId,
                RefreshTimer);

        }

        public void InitialiseAndWarmupConsumer()
        {
            StopAndRebuildTimers();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void RefreshTimer(AomProductConfigurePurgeResponse response)
        {
            try
            {
                if (response.Message.MessageType == MessageType.Error
                    && response.Message.ClientSessionInfo.SessionId == ToString())
                {
                    Log.Error(String.Format("Error Response Received: {0}", response.Message.MessageBody));
                }
                else
                {
                    if (response.Message.MessageAction == MessageAction.ConfigurePurge && response.Product != null)
                    {
                        DateTime productNextTime = NextTimeForProduct(response.Product);

                        LogDebug(
                            string.Format(
                                "Refreshed timer for product {0}, next event due at: {1}.",
                                response.Product.ProductId,
                                productNextTime));

                        if (productNextTime < _nextTime)
                        {
                            LogDebug(
                                string.Format(
                                    "Next event for product {0} is earlier than existing timer, resetting timer to new time.",
                                    response.Product.ProductId));
                            StopTimerIfRunning();
                            SetupTimer(productNextTime);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("RefreshTimer", e);
            }
        }

        private void OnTimerElapsed(object data, ElapsedEventArgs elapsedEventArgs)
        {
            try
            {
                LogDebug(string.Format("Timer event triggered."));
                StopAndRebuildTimers();
            }
            catch (Exception e)
            {
                Log.Error("OnTimerElapsed", e);
            }
        }

        private void StopAndRebuildTimers()
        {
            StopTimerIfRunning();

            _nextTime = DateTime.MaxValue;

            try
            {
                List<Product> products = _productService.GetAllProducts();

                foreach (Product product in products)
                {
                    DateTime nextPurge = NextTimeForProduct(product);

                    if (nextPurge < _dateTimeProvider.UtcNow)
                    {
                        while (nextPurge < _dateTimeProvider.UtcNow)
                        {
                            nextPurge = nextPurge.AddDays(product.PurgeFrequency);
                        }

                        product.PurgeDate = DateTime.SpecifyKind(nextPurge.Date, DateTimeKind.Utc);

                        PublishPurgeRequest(product.ProductId);
                        PublishPurgeConfigureRequest(product);
                    }

                    _nextTime = Min(nextPurge, _nextTime);
                }
            }
            finally
            {
                SetupTimer(_nextTime);
            }
        }

        private void SetupTimer(DateTime nextTime)
        {
            _nextTime = Min(nextTime, _dateTimeProvider.UtcNow.AddHours(12));
            // wait for a maximum of 12 hours, to avoid timer inaccuracies or issues.
            _timer = new Timer(Math.Max(1, _nextTime.Subtract(_dateTimeProvider.UtcNow).TotalMilliseconds));
            _timer.Elapsed += OnTimerElapsed;
            _timer.Enabled = true;

            LogDebug(
                string.Format("Timer started, next event due at: {0}. Next timer reset at: {1}", nextTime, _nextTime));
        }

        public static DateTime Min(DateTime a, DateTime b)
        {
            return a < b ? a : b;
        }

        public void StopTimerIfRunning()
        {
            LogDebug("StopTimerIfRunning called.");
            lock (Padlock)
            {
                if (_timer != null)
                {
                    //LogDebug("Stopping timer.");
                    _timer.Stop();
                    _timer.Dispose();
                    _timer = null;
                }
            }
        }

        private DateTime NextTimeForProduct(Product product)
        {
            return
                DateTime.SpecifyKind(
                    (product.PurgeDate ?? _dateTimeProvider.UtcNow.Date).Add(
                        product.PurgeTimeOfDay ?? _dateTimeProvider.UtcNow.TimeOfDay),
                    DateTimeKind.Utc);
            //var nextPurge = DateTime.SpecifyKind(product.PurgeDate.Value.Add(product.PurgeTimeOfDay.Value), DateTimeKind.Utc);
        }

        private void PublishPurgeConfigureRequest(Product product)
        {
            var request = new AomProductConfigurePurgeRequest
            {
                ClientSessionInfo =
                    new ClientSessionInfo {UserId = -1},
                MessageAction = MessageAction.ConfigurePurge,
                Product = product
            };

            _aomBus.Publish(request);
        }

        private void PublishPurgeRequest(long productId)
        {
            var request = new AomProductPurgeOrdersRequest
            {
                ClientSessionInfo =
                    new ClientSessionInfo
                    {
                        UserId = -1,
                        SessionId = ToString()
                    },
                MessageAction = MessageAction.Purge,
                Product = _productService.GetProduct(productId)
            };

            _aomBus.Publish(request);
        }

        private void LogDebug(string msg)
        {
            Log.Debug(String.Format("{0}: {1}", GetType().Name, msg));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Log.InfoFormat("{0} Disposing", GetType().Name);
                StopTimerIfRunning();
            }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        public string SubscriptionUniqueToThisMachineAndConsumerId
        {
            get { return string.Format("{0}.{1}", GetType().Name, System.Environment.MachineName); }
        }

    }
}