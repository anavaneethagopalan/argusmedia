﻿using AOM.App.Domain.Entities;
using System;
using AOM.App.Domain.Dates;
using Utils.Javascript.Extension;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    public class ProductStatusHelper : IProductStatusHelper
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        public ProductStatusHelper(IDateTimeProvider dateTimeProvider)
        {
            _dateTimeProvider = dateTimeProvider;
        }

        public MarketStatus DetermineExpectedMarketStatus(Product product)
        {
            DateTime? todaysOpenTime = _dateTimeProvider.UtcNow.Date + product.OpenTime;
            DateTime? todaysCloseTime = _dateTimeProvider.UtcNow.Date + product.CloseTime;

            if (todaysOpenTime > todaysCloseTime)
            {
                return _dateTimeProvider.UtcNow >= todaysCloseTime && _dateTimeProvider.UtcNow < todaysOpenTime
                    ? MarketStatus.Closed
                    : MarketStatus.Open;
            }
            return _dateTimeProvider.UtcNow >= todaysOpenTime && _dateTimeProvider.UtcNow < todaysCloseTime
                ? MarketStatus.Open
                : MarketStatus.Closed;
        }

        public DateTime GetNextMarketStatusChangeTime(Product product)
        {
            var nextOpenTime = (DateTime)(_dateTimeProvider.UtcNow.Date + product.OpenTime);
            var nextCloseTime = (DateTime)(_dateTimeProvider.UtcNow.Date + product.CloseTime);

            if (nextOpenTime < _dateTimeProvider.UtcNow)
            {
                nextOpenTime = nextOpenTime.AddWeekDays(1);
            }

            if (nextCloseTime < _dateTimeProvider.UtcNow)
            {
                nextCloseTime = nextCloseTime.AddWeekDays(1);
            }

            return nextOpenTime < nextCloseTime ? nextOpenTime : nextCloseTime;
        }
    }
}
