﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    public class AomCoBrokeringStatusChangeRequestConsumer : ProductConsumerBase
    {
        public AomCoBrokeringStatusChangeRequestConsumer(IDbContextFactory dbFactory, IProductService productService,
            IBus aomBus, IErrorService errorService)
            : base(dbFactory, productService, aomBus, errorService)
        {
        }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomCoBrokeringStatusChangeRequest>(
                SubscriptionId, OnAomCoBrokeringStatusChangeRequest);
        }

        private void OnAomCoBrokeringStatusChangeRequest(
            AomCoBrokeringStatusChangeRequest aomCoBrokeringStatusChangeRequest)
        {
            if (aomCoBrokeringStatusChangeRequest != null)
            {
                using (IAomModel aomDb = DbFactory.CreateAomModel())
                {
                    var aomProductResponse = new AomProductResponse
                    {
                        Product = aomCoBrokeringStatusChangeRequest.Product,
                        Message = new Message
                        {
                            ClientSessionInfo = aomCoBrokeringStatusChangeRequest.ClientSessionInfo,
                            MessageAction = MessageAction.Update
                        }
                    };

                    // We have a valid request. 
                    var product = ProductService.EditProduct(aomDb, aomProductResponse);
                    PublishCoBrokeringStatusChange(product);
                }
            }
        }

        private void PublishCoBrokeringStatusChange(Product product)
        {
            var productDefinitionItem = ProductService.GetProductDefinition(product.ProductId);
            var productConfigResponse = new GetProductConfigResponse {ProductDefinition = productDefinitionItem};

            // Publish the Product Config Response
            _aomBus.Publish(productConfigResponse);
        }

        internal override string SubscriptionId
        {
            get
            {
                return "AomCoBrokeringStatusChangeRequestConsumer";
            }
        }
    }
}