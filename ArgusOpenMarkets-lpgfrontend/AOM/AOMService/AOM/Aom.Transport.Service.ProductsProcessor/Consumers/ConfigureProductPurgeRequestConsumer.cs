﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    public class ConfigureProductPurgeRequestConsumer : ProductConsumerBase
    {
        public ConfigureProductPurgeRequestConsumer(
            IDbContextFactory dbFactory,
            IProductService productService,
            IBus aomBus,
            IErrorService errorService)
            : base(dbFactory, productService, aomBus, errorService)
        {
        }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomProductConfigurePurgeRequest>(
                SubscriptionId,
                ConsumeProductRequest<AomProductConfigurePurgeRequest, AomProductConfigurePurgeAuthenticationRequest>);

            _aomBus.Subscribe<AomProductConfigurePurgeAuthenticationResponse>(
                SubscriptionId,
                ConsumeProductResponse<AomProductConfigurePurgeAuthenticationResponse, AomProductResponse>);
        }

        internal override Product ConsumeProductResponseAdditionalProcessing<T>(IAomModel aomDb, T response)
        {
            switch (response.Message.MessageAction)
            {
                case MessageAction.ConfigurePurge:
                    var product = ProductService.EditProduct(aomDb, response);
                    PublishProductConfigurePurgeResponse(response);
                    return product;
            }
            return null;
        }

        private void PublishProductConfigurePurgeResponse(AomProductResponse response)
        {
            var purge = new AomProductConfigurePurgeResponse {Message = response.Message, Product = response.Product};

            _aomBus.Publish(purge);
        }

        internal override string SubscriptionId
        {
            get
            {
                return "ConfigureProductPurgeRequestConsumer";
            }
        }
    }
}