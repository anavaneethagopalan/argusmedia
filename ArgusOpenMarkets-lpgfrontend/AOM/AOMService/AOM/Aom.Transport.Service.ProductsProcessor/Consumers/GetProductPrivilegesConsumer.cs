﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Transport.Events.Products;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    using Processor.Common;

    using AOM.App.Domain.Services;
    using Argus.Transport.Infrastructure;

    using System;

    public class GetProductPrivilegesConsumer : IConsumer
    {
        protected readonly ICrmAdministrationService _crmAdministrationService;

        private IBus _aomBus;

        public GetProductPrivilegesConsumer(ICrmAdministrationService crmAdministrationService, IBus aomBus)
        {
            _crmAdministrationService = crmAdministrationService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Respond<GetProductSystemPrivilegesRequestRpc, GetProductSystemPrivilegesResponse>(ConsumeGetProductPrivileges);
        }

        private GetProductSystemPrivilegesResponse ConsumeGetProductPrivileges(GetProductSystemPrivilegesRequestRpc arg)
        {
            var productPrivileges = _crmAdministrationService.GetProductPrivileges();

            var prodPrivs = new List<ProductPrivilege>();
            foreach (var pp in productPrivileges)
            {
                prodPrivs.Add(pp as ProductPrivilege);
            }

            var systemPrivileges = _crmAdministrationService.GetSystemPrivileges();
            var sysPrivs = new List<SystemPrivilege>();
            foreach (var sp in systemPrivileges)
            {
                sysPrivs.Add(sp as SystemPrivilege);
            }

            var response = new GetProductSystemPrivilegesResponse { ProductPrivileges = prodPrivs, SystemPrivileges = sysPrivs };
            return response;
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}