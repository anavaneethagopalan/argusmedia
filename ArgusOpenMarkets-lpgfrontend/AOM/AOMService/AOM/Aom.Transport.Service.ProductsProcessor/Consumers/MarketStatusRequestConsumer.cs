﻿using AOM.Services.ErrorService;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    using App.Domain.Entities;
    using Repository.MySql;
    using Repository.MySql.Aom;
    using Services.ProductService;
    using Events;
    using Events.Products;

    using Argus.Transport.Infrastructure;

    internal class MarketStatusRequestConsumer : ProductConsumerBase
    {
        public MarketStatusRequestConsumer(IDbContextFactory dbFactory, IProductService productService, IBus aomBus, IErrorService errorService)
            : base(dbFactory, productService, aomBus, errorService)
        { }

        internal override string SubscriptionId { get { return "MarketStatusRequestConsumer"; } }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomMarketStatusChangeRequest>(SubscriptionId, ConsumeProductRequest<AomMarketStatusChangeRequest, AomMarketStatusChangeAuthenticationRequest>);
            _aomBus.Subscribe<AomMarketStatusChangeAuthenticationResponse>(SubscriptionId, ConsumeProductResponse<AomMarketStatusChangeAuthenticationResponse, AomProductResponse>);
        }

        internal override Product ConsumeProductResponseAdditionalProcessing<T>(IAomModel aomDb, T response)
        {
            Product product = null;

            switch (response.Message.MessageAction)
            {
                case MessageAction.Open:
                    product = ProductService.ChangeMarketStatus(aomDb, response, MarketStatus.Open);
                    if (product != null)
                    {
                        PublishMarketStatusChange(response, product, MarketStatus.Open);
                    }
                    return product;

                case MessageAction.Close:
                    product = ProductService.ChangeMarketStatus(aomDb, response, MarketStatus.Closed);
                    if (product != null)
                    {
                        PublishMarketStatusChange(response, product, MarketStatus.Closed);

                    }
                    return product;

                case MessageAction.Update:
                    return ProductService.EditProduct(aomDb, response);
            }
            return null;
        }

        private void PublishMarketStatusChange(AomProductResponse response, Product product, MarketStatus status)
        {
            var marketStatusChange = new AomMarketStatusChange { Product = product, Status = status, Message = response.Message };
            _aomBus.Publish(marketStatusChange);
        }
    }
}