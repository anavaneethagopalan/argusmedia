﻿using System.Diagnostics;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.Services.ProductService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Products;
    using AOM.Transport.Service.Processor.Common;
    using Argus.Transport.Infrastructure;
    using System;
    using System.Timers;
    using Utils.Logging.Utils;

    public class TimersProductStatusServiceConsumer : IConsumerWithInitialise, IDisposable
    {
        private static readonly object Padlock = new object();
        private readonly IBus _aomBus;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IProductStatusHelper _productStatusHelper;
        private readonly IProductService _productService;

        private DateTime _nextTime = DateTime.MaxValue;
        private Timer _timer;

        public TimersProductStatusServiceConsumer(IBus aomBus, IProductService productService, IDateTimeProvider dateTimeProvider, IProductStatusHelper productStatusHelper)
        {
            _aomBus = aomBus;
            _productService = productService;
            _dateTimeProvider = dateTimeProvider;
            _productStatusHelper = productStatusHelper;
        }

        public DateTime NextTimeToRun
        {
            get { return _nextTime; }
        }

        public void Start()
        {
            _aomBus.Subscribe<AomProductResponse>(SubscriptionUniqueToThisMachineAndConsumerId, ProcessProductResponse);
            _aomBus.Subscribe<ResetMarketStatusTimers>(SubscriptionUniqueToThisMachineAndConsumerId, ResetMarketStatusTimers);
        }

        private void ResetMarketStatusTimers(ResetMarketStatusTimers obj)
        {
            StopAndRebuildTimers();
        }

        public void InitialiseAndWarmupConsumer()
        {
            StopAndRebuildTimers();
        }

        public void Dispose()
        {
            StackTrace t = new StackTrace();
            Log.Info("DISPOSE CALLED BY MYSTERY PROCESS, current StackTrace: " + t.ToString());
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void ProcessProductResponse(AomProductResponse response)
        {
            try
            {
                if (response.Message.MessageType == MessageType.Error &&
                    response.Message.ClientSessionInfo.SessionId == ToString())
                {
                    Log.Error(String.Format("Error Response Received: {0}", response.Message.MessageBody));
                }
                else
                {
                    if (response.Message.MessageAction == MessageAction.Update && response.Product != null)
                    {
                        DateTime productNextTime = _productStatusHelper.GetNextMarketStatusChangeTime(response.Product);

                        LogDebug(string.Format("Refreshed timer for product {0}, next event due at: {1}.", response.Product.ProductId, productNextTime));

                        if (productNextTime < _nextTime)
                        {
                            LogDebug(string.Format("Next event for product {0} is earlier than existing timer, resetting timer to new time.", response.Product.ProductId));
                            StopMarketTimerIfRunning();
                            SetupTimer(productNextTime);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("ProcessProductResponse", e);
            }
        }

        private void OnTimerElapsed(object data, ElapsedEventArgs elapsedEventArgs)
        {
            LogDebug("Timer event triggered.");
            StopAndRebuildTimers();
        }

        private void StopAndRebuildTimers()
        {
            try
            {
                StopMarketTimerIfRunning();
                _nextTime = DateTime.MaxValue;

                // AOMK-27 Don't send a market open/close if it's a weekend
                bool isCurrentlyAWeekend = _dateTimeProvider.UtcNow.DayOfWeek == DayOfWeek.Saturday || _dateTimeProvider.UtcNow.DayOfWeek == DayOfWeek.Sunday;

                try
                {
                    var products = _productService.GetAllProducts();

                    foreach (var product in products)
                    {
                        if (product.IsInternal) continue;
                        var expectedStatus = _productStatusHelper.DetermineExpectedMarketStatus(product);

                        if ((expectedStatus != product.Status) && !isCurrentlyAWeekend)
                        {
                            if (product.IgnoreAutomaticMarketStatusTriggers)
                            {
                                Log.Warn("IgnoreAutomaticMarketStatusTriggers is set to true therefore ignoring automated market status update");
                            }
                            else
                            {
                                SendUpdateMarketStatusMessage(product.ProductId, expectedStatus == MarketStatus.Open ? MessageAction.Open : MessageAction.Close);
                            }
                        }

                        _nextTime = Min(_productStatusHelper.GetNextMarketStatusChangeTime(product), _nextTime);
                    }
                }
                finally
                {
                    SetupTimer(_nextTime);
                }
            }
            catch (Exception e)
            {
                Log.Error("StopAndRebuildTimers", e);
            }
        }

        private void SetupTimer(DateTime nextMarketEventTime)
        {
            try
            {
                bool limitToTwelveHoursFromNow = false;
                Log.DebugFormat("SetupTimer has been called.  The next time we need to run is {0}.  The Date Time now is: {1}", nextMarketEventTime, _dateTimeProvider.UtcNow);

                var dateTimeNowPlusTwelveHours = _dateTimeProvider.UtcNow.AddHours(12);

                if (nextMarketEventTime < _dateTimeProvider.UtcNow)
                {
                    nextMarketEventTime = _dateTimeProvider.UtcNow.AddHours(1);
                }

                if (nextMarketEventTime > dateTimeNowPlusTwelveHours)
                {
                    nextMarketEventTime = dateTimeNowPlusTwelveHours;
                    limitToTwelveHoursFromNow = true;
                }

                var timeSpan = nextMarketEventTime - _dateTimeProvider.UtcNow;
                var nextTimeToRunInMilliseconds = timeSpan.TotalMilliseconds;
                _nextTime = nextMarketEventTime;

                _timer = new Timer(nextTimeToRunInMilliseconds);
                _timer.Elapsed += OnTimerElapsed;
                _timer.Enabled = true;

                Log.DebugFormat("Timer started, next event due at: {0}. Next timer reset at: {1} in {2}msecs.  Limit next time to run to 12 hours: {3}",
                    nextMarketEventTime, nextMarketEventTime, nextTimeToRunInMilliseconds, limitToTwelveHoursFromNow);
            }
            catch (Exception ex)
            {
                Log.Error("Error Setting the next timer time that the job needs to run", ex);

                // Something has gone wrong with the above.  We need to set the next time to run in 1 minute.
                _timer = new Timer(60000);
                _timer.Elapsed += OnTimerElapsed;
                _timer.Enabled = true;

                Log.Debug("SetupTimer - An Error has occurred attempting to set the timer to run.  For now we are scheduling the next execution job to run in 1 minute.");
            }

        }

        public static DateTime Min(DateTime a, DateTime b)
        {
            return a < b ? a : b;
        }

        public void StopMarketTimerIfRunning()
        {
            LogDebug("StopMarketTimerIfRunning called.");
            lock (Padlock)
            {
                if (_timer != null)
                {
                    //LogDebug("Stopping timer.");
                    _timer.Elapsed -= OnTimerElapsed;
                    _timer.Stop();
                    _timer.Dispose();
                    _timer = null;
                }
            }
        }

        private void SendUpdateMarketStatusMessage(long productId, MessageAction action)
        {
            Log.Info(string.Format("Sending request to change status to '{1}' for product id {0}.", productId, action));

            var request = new AomMarketStatusChangeRequest
            {
                ClientSessionInfo = new ClientSessionInfo {UserId = -1, SessionId = ToString()},
                MessageAction = action,
                Product = _productService.GetProduct(productId)
            };

            _aomBus.Publish(request);
        }

        private void LogDebug(string msg)
        {
            Log.Debug(String.Format("{0}: {1}", GetType().Name, msg));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Log.InfoFormat("{0} Disposing", GetType().Name);
                StopMarketTimerIfRunning();
            }
        }

        public string SubscriptionId
        {
            get { return ""; }
        }

        public string SubscriptionUniqueToThisMachineAndConsumerId
        {
            get { return string.Format("{0}.{1}", GetType().Name, System.Environment.MachineName); }
        }

    }
}