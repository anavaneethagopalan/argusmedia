﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    internal class PurgeProductOrdersRequestConsumer : ProductConsumerBase
    {
        public PurgeProductOrdersRequestConsumer(
            IDbContextFactory dbFactory,
            IProductService productService,
            IBus aomBus,
            IErrorService errorService)
            : base(dbFactory, productService, aomBus, errorService)
        {
        }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomProductPurgeOrdersRequest>(
                SubscriptionId,
                ConsumeProductRequest<AomProductPurgeOrdersRequest, AomProductPurgeOrdersAuthenticationRequest>);
            _aomBus.Subscribe<AomProductPurgeOrdersAuthenticationResponse>(
                SubscriptionId,
                ConsumeProductResponse<AomProductPurgeOrdersAuthenticationResponse, AomProductResponse>);
        }

        internal override Product ConsumeProductResponseAdditionalProcessing<T>(IAomModel aomDb, T response)
        {
            switch (response.Message.MessageAction)
            {
                case MessageAction.Purge:
                    PublishProductPurgeOrders(response);
                    return response.Product;
            }
            return null;
        }

        private void PublishProductPurgeOrders(AomProductResponse response)
        {
            _aomBus.Publish(MakePurgeRequest(response.Message, response.Product));
        }

        internal AomProductPurgeOrdersResponse MakePurgeRequest(Message message, Product product)
        {
            return new AomProductPurgeOrdersResponse {Message = message, Product = product, UserId = -1};
        }

        internal override string SubscriptionId
        {
            get
            {
                return "PurgeProductOrdersRequestConsumer";
            }
        }
    }
}