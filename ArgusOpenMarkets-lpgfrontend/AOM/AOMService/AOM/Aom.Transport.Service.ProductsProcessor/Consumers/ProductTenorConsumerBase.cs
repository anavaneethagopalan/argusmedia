using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Events.Products;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    public abstract class ProductTenorConsumerBase : IConsumer
    {
        protected readonly IDbContextFactory DbFactory;
        protected readonly IProductService _productService;
        protected readonly IErrorService _errorService;
        protected IBus _aomBus;

        protected ProductTenorConsumerBase(IDbContextFactory dbFactory, IProductService productService, IBus aomBus, IErrorService errorService)
        {
            DbFactory = dbFactory;
            _productService = productService;
            _errorService = errorService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            SubscribeToEvents();
        }

        internal virtual void SubscribeToEvents()
        {
            throw new NotImplementedException("You must subscribe to the events to consume in this method");
        }

        internal void ConsumeProductTenorRequest<TReq, TFwd>(TReq request)
            where TReq : AomProductTenorRequest
            where TFwd : AomProductTenorAuthenticationRequest, new()
        {
            string requestType = typeof (TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd
                {
                    ClientSessionInfo = request.ClientSessionInfo,
                    Tenor = request.Tenor,
                    MessageAction = request.MessageAction
                };
                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                var errorMessage =
                    _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = String.Format("Authenticating {0}", requestType),
                        ErrorText = string.Empty,
                        Source = "ProductTenorConsumerBase"
                    });

                _aomBus.PublishErrorResponse<AomProductTenorResponse>(
                    MessageAction.Update,
                    request.ClientSessionInfo,
                    errorMessage);
            }
        }

        internal void ConsumeProductTenorResponse<TResp, TFwd>(TResp response)
            where TResp : AomProductTenorAuthenticationResponse
            where TFwd : AomProductTenorResponse, new()
        {
            string responseType = typeof (TResp).Name;

            Log.Debug(String.Format("{0} received", responseType));
            try
            {
                if (response.Message.MessageType == MessageType.Error)
                {
                    _aomBus.PublishErrorResponseAndLogError<AomProductTenorResponse>(
                        response.Message.MessageAction,
                        response.Message.ClientSessionInfo,
                        response.Message.MessageBody as string);
                }
                else
                {
                    using (var aomDb = DbFactory.CreateAomModel())
                    {
                        var result = ConsumeProductTenorResponseAdditionalProcessing(aomDb, response);
                        try
                        {
                            var forwardedResponse = new TFwd {Message = response.Message, ProductTenor = result};
                            _aomBus.Publish(forwardedResponse);
                        }
                        catch (Exception exception)
                        {
                            _errorService.LogUserError(new UserError
                            {
                                Error = exception,
                                AdditionalInformation = String.Format("{0} error", responseType),
                                ErrorText = string.Empty,
                                Source = "ProductTenorConsumerBase"
                            });
                        }
                    }
                }
            }
            catch (BusinessRuleException exception)
            {
                _aomBus.PublishErrorResponseAndLogInfo<AomProductTenorResponse>(
                    response.Message.MessageAction,
                    response.Message.ClientSessionInfo,
                    exception.Message);
            }
            catch (Exception exception)
            {
                var errMess = _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = String.Format("{0} error", responseType),
                        ErrorText = string.Empty,
                        Source = "ProductTenorConsumerBase"
                    });

                _aomBus.PublishErrorResponse<AomProductTenorResponse>(
                    response.Message.MessageAction,
                    response.Message.ClientSessionInfo,
                    errMess);
            }
        }

        internal virtual ProductTenor ConsumeProductTenorResponseAdditionalProcessing<T>(IAomModel aomDb, T response)
            where T : AomProductTenorResponse
        {
            throw new NotImplementedException(
                "You must add code in here if consuming responses. This is only called if the request being consumed is not indicating an error.");
        }

        internal virtual string SubscriptionId
        {
            get
            {
                throw new NotImplementedException(
                    "Add specific SubscriptionId to class in order for message to not be consumed by another consumer");
            }
        }
    }
}