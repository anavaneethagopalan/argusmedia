﻿using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    public abstract class ProductConsumerBase : IConsumer
    {
        protected readonly IDbContextFactory DbFactory;

        protected readonly IProductService ProductService;

        protected IBus _aomBus;

        private readonly IErrorService _errorService;

        protected ProductConsumerBase(
            IDbContextFactory dbFactory,
            IProductService productService,
            IBus aomBus,
            IErrorService errorService)
        {
            DbFactory = dbFactory;
            ProductService = productService;
            _aomBus = aomBus;
            _errorService = errorService;
        }

        internal virtual string SubscriptionId
        {
            get
            {
                throw new NotImplementedException(
                    "Add specific SubscriptionId to class in order for message to not be consumed by another consumer");
            }
        }

        public void Start()
        {
            SubscribeToEvents();
        }

        internal virtual void SubscribeToEvents()
        {
            throw new NotImplementedException("You must subscribe to the events to consume in this method");
        }

        internal void ConsumeProductRequest<TReq, TFwd>(TReq request) where TReq : AomProductRequest
            where TFwd : AomProductRequest, new()
        {
            string requestType = typeof (TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd
                {
                    ClientSessionInfo = request.ClientSessionInfo,
                    Product = request.Product,
                    MessageAction = request.MessageAction
                };
                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                var errMess =
                    _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = String.Format("Authenticating {0}", requestType),
                        ErrorText = string.Empty,
                        Source = "ProductConsumerBase"
                    });

                _aomBus.PublishErrorResponse<AomProductResponse>(
                    MessageAction.Update,
                    request.ClientSessionInfo,
                    errMess);
            }
        }

        internal void ConsumeProductResponse<TResp, TFwd>(TResp response) where TResp : AomProductResponse
            where TFwd : AomProductResponse, new()
        {
            string responseType = typeof (TResp).Name;

            Log.Debug(String.Format("{0} received", responseType));
            try
            {
                if (response.Message.MessageType == MessageType.Error)
                {
                    _aomBus.PublishErrorResponseAndLogError<AomProductResponse>(
                        response.Message.MessageAction,
                        response.Message.ClientSessionInfo,
                        response.Message.MessageBody as string);
                }
                else
                {
                    using (IAomModel aomDb = DbFactory.CreateAomModel())
                    {
                        Product result = ConsumeProductResponseAdditionalProcessing(aomDb, response);
                        try
                        {
                            var forwardedResponse = new TFwd {Message = response.Message, Product = result};
                            _aomBus.Publish(forwardedResponse);
                        }
                        catch (Exception exception)
                        {
                            _errorService.LogUserError(new UserError
                            {
                                Error = exception,
                                AdditionalInformation = String.Format("{0} error", responseType),
                                ErrorText = string.Empty,
                                Source = "ProductConsumerBase"
                            });
                        }
                    }
                }
            }
            catch (BusinessRuleException exception)
            {
                _aomBus.PublishErrorResponseAndLogInfo<AomProductResponse>(
                    response.Message.MessageAction,
                    response.Message.ClientSessionInfo,
                    exception.Message);
            }
            catch (Exception exception)
            {
                Log.Error("Inner Exception", exception);
                try
                {
                    var errMess = _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = String.Format("{0} error", responseType),
                        ErrorText = string.Empty,
                        Source = "ProductConsumerBase"
                    });

                    

                    _aomBus.PublishErrorResponse<AomProductResponse>(
                        response.Message.MessageAction,
                        response.Message.ClientSessionInfo,
                        errMess);

                }
                catch (Exception ex)
                {
                    Log.Error("ERROR SENDING MESSAGE", ex);
                }
            }
        }

        internal virtual Product ConsumeProductResponseAdditionalProcessing<T>(IAomModel aomDb, T response)
            where T : AomProductResponse
        {
            throw new NotImplementedException(
                "You must add code in here if consuming responses. This is only called if the request being consumed is not indicating an error.");
        }
    }
}