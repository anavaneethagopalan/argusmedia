﻿using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    using System;
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.Services.ProductService;
    using AOM.Transport.Events.Products;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public class ProductOpeningTimesConsumer : IConsumer
    {
        private readonly IBus _aomBus;
        private readonly IProductService _productService;

        public ProductOpeningTimesConsumer(
            IBus aomBus,
            IProductService productService)
        {
            _aomBus = aomBus;
            _productService = productService;
        }

        public void Start()
        {
            _aomBus.Respond<GetOpeningTimesRpc, IList<ProductOpeningTimes>>(OnMarketOpeningTimesRequest);
        }

        private IList<ProductOpeningTimes> OnMarketOpeningTimesRequest(GetOpeningTimesRpc getOpeningTimesRpc)
        {
            try
            {
                return _productService.GetAllProductOpeningTimes();
            }
            catch (Exception e)
            {
                Log.Error("OnMarketOpeningTimesRequest", e);
            }
            
            return new List<ProductOpeningTimes>();
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}