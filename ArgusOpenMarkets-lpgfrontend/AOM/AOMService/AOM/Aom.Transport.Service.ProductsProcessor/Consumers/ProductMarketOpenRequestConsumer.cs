﻿using System;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    internal class ProductMarketOpenRequestConsumer : IConsumer
    {
        private readonly IBus _aomBus;
        private readonly IProductService _productService;

        public ProductMarketOpenRequestConsumer(IBus aomBus, IProductService productService)
        {
            _aomBus = aomBus;
            _productService = productService;
        }

        public void Start()
        {
            _aomBus.Subscribe<ProductMarketOpenRequest>(SubscriptionId, Consume<ProductMarketOpenRequest, AomMarketStatusChangeRequest>);
        }

        private void Consume<TReq, TFwd>(TReq request) where TReq : ProductMarketOpenRequest where TFwd : AomMarketStatusChangeRequest, new()
        {
            string requestType = typeof(TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd
                {
                    ClientSessionInfo = new ClientSessionInfo { UserId = -1, SessionId = ToString() },
                    MessageAction = MessageAction.Open,
                    Product = _productService.GetProduct(request.ProductId)
                };

                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                Log.Warn(String.Format("Unable to publish Market Open Command to Control Client: {0}", exception.Message));
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}