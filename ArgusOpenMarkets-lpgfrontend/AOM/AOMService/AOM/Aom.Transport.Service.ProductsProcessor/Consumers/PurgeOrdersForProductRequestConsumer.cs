﻿using AOM.Transport.Events.Products;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using AOM.App.Domain.Dates;
using AOM.Repository.MySql;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    internal class PurgeOrdersForProductRequestConsumer : ScheduledTaskConsumerBase<PurgeOrdersForProductRequest>
    {
        public PurgeOrdersForProductRequestConsumer(IBus aomBus, IDateTimeProvider dateTimeProvider,
            IDbContextFactory dbContextFactory = null)
            : base(aomBus, dateTimeProvider, dbContextFactory)
        {
        }

        public override void Start()
        {
            AomBus.Subscribe<PurgeOrdersForProductRequest>(
                SubscriptionId,
                ConsumePurgeProductOrdersRequest<PurgeOrdersForProductRequest, PurgeOrdersForProductResponse>);
            base.Start();
        }

        private void ConsumePurgeProductOrdersRequest<TReq, TFwd>(TReq request)
            where TReq : PurgeOrdersForProductRequest
            where TFwd : PurgeOrdersForProductResponse, new()
        {
            string requestType = typeof(TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd { ProductId = request.ProductId };
                AomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                Log.Warn(
                    String.Format("Unable to publish Purge Orders For Product Command to Control Client: {0}", exception.Message));
            }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }
    }
}