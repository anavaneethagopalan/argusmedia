﻿using System.Linq;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;

    using System.Diagnostics;

    using AOM.Services.ProductService;
    using AOM.Transport.Events.Products;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using System;

    public class GetProductRequestConsumer : IConsumer
    {
        protected readonly IProductService _productService;

        private IBus _aomBus;

        public GetProductRequestConsumer(IProductService prodctService, IBus aomBus)
        {
            _productService = prodctService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<GetProductConfigRequest>(SubscriptionId, ConsumeGetProductConfigRequest);
            _aomBus.Subscribe<GetProductIdsRequest>(SubscriptionId, ConsumeGetProductIdsRequest);
            _aomBus.Respond<GetProductRequestRpc, AomProductResponse>(ConsumeGetProductRequest);
            _aomBus.Respond<GetAllActiveProductsRequestRpc, List<Product>>(ConsumeGetAllActiveProductsRequest);
            _aomBus.Respond<GetAllProductDefinitionsRequestRpc, List<ProductDefinitionItem>>(
                ConsumeGetAllProductDefinitionsRequest);
        }

        private AomProductResponse ConsumeGetProductRequest(GetProductRequestRpc requestRpc)
        {
            try
            {
                return new AomProductResponse { Product = _productService.GetProduct(requestRpc.Id) };
            }
            catch (Exception e)
            {
                Log.Error("ConsumeGetProductRequest", e);
            }

            return new AomProductResponse { Product = null };
        }

        private List<Product> ConsumeGetAllActiveProductsRequest(GetAllActiveProductsRequestRpc requestRpc)
        {
            try
            {
                return _productService.GetAllProducts();
            }
            catch (Exception ex)
            {
                Log.Error("ConsumeGetAllActiveProductsRequest", ex);
            }

            return new List<Product>();
        }

        private List<ProductDefinitionItem> ConsumeGetAllProductDefinitionsRequest(
            GetAllProductDefinitionsRequestRpc requestRpc)
        {
            try
            {
                return _productService.GetAllProductDefinitions();
            }
            catch (Exception e)
            {

                Log.Error("ConsumeGetAllProductDefinitionsRequest", e);
            }

            return new List<ProductDefinitionItem>();
        }

        /// <summary>
        /// Callback for when a GetProductConfigRequest message sent. Gets ProductDefinitions and
        /// publishes each individual ProductDto config as a separate response
        /// </summary>
        /// <param name="getProductConfigRequest"></param>
        internal void ConsumeGetProductConfigRequest(GetProductConfigRequest getProductConfigRequest)
        {
            if (getProductConfigRequest != null && getProductConfigRequest.ProductId.HasValue)
            {
                Log.InfoFormat("ConsumeGetProductConfigRequest received for a single product with the Id:{0}",
                    getProductConfigRequest.ProductId.Value);
                RefreshSingleProductDefinition(getProductConfigRequest.ProductId.Value);
            }
            else
            {
                Log.Info("Received GetProductConfig request - ALL Products; getting ProductDto definitions");
                RefreshAllProductDefinitionInDiffusion();
            }
        }

        private void RefreshSingleProductDefinition(long productId)
        {
            var productDefinition = _productService.GetProductDefinition(productId);
            PublishProductDefinitionRefresh(productDefinition);
        }

        private void RefreshAllProductDefinitionInDiffusion()
        {
            List<ProductDefinitionItem> productDefinitionItems = new List<ProductDefinitionItem>();
            try
            {
                productDefinitionItems = _productService.GetAllProductDefinitions();
            }
            catch (Exception ex)
            {
                Log.Error(
                    "GetProductRequestConsumer.ConsumeGetProductConfigRequest - call to GetAllProductDefinitions returned an error",
                    ex);
            }

            foreach (var productDefinition in productDefinitionItems)
            {
                PublishProductDefinitionRefresh(productDefinition);
            }
        }

        private void PublishProductDefinitionRefresh(ProductDefinitionItem productDefinition)
        {
            var response = new GetProductConfigResponse { ProductDefinition = productDefinition };
            response.ProductDefinition.ContractSpecification = productDefinition.ContractSpecification;

            Debug.WriteLine("Publishing ProductDefinition {0} to exchange", productDefinition.ProductId);

            _aomBus.Publish(response);
        }

        internal void ConsumeGetProductIdsRequest(GetProductIdsRequest request)
        {
            Log.Debug("Received GetProductIdsRequest request; getting ProductDto definitions");
            try
            {
                var productDefinitionItems = _productService.GetAllProductDefinitions();
                var productIds = productDefinitionItems.Select(pd => pd.ProductId).ToList();
                var response = new GetProductIdsResponse
                {
                    ProductIds = productIds
                };
                Log.Debug("Publishing GetProductIdsRequest to bus");
                _aomBus.Publish(response);
            }
            catch (Exception ex)
            {
                Log.Error(
                    "GetProductRequestConsumer.ConsumeGetProductIdsRequest error",
                    ex);
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}