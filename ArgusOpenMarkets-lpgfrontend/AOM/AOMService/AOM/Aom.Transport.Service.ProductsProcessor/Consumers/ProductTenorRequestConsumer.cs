﻿using AOM.Services.ErrorService;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    using System;
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Aom;
    using AOM.Services.ProductService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    internal class ProductTenorRequestConsumer : ProductTenorConsumerBase
    {
        public ProductTenorRequestConsumer(IDbContextFactory dbFactory, IProductService productService, IBus aomBus, IErrorService errorService)
            : base(dbFactory, productService, aomBus, errorService)
        {
        }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomProductTenorRequest>(SubscriptionId, ConsumeProductTenorRequest<AomProductTenorRequest, AomProductTenorAuthenticationRequest>);
            _aomBus.Subscribe<AomProductTenorAuthenticationResponse>(SubscriptionId,ConsumeProductTenorResponse<AomProductTenorAuthenticationResponse, AomProductTenorResponse>);
            _aomBus.Respond<GetAllProductTenorsRequestRpc, IEnumerable<ProductTenor>>(ConsumeGetAllProductTenorsRequest);
            _aomBus.Respond<GetProductTenorRequestRpc, AomProductTenorResponse>(ConsumeGetProductTenorRequest);
        }

        internal override ProductTenor ConsumeProductTenorResponseAdditionalProcessing<T>(IAomModel aomDb, T response)
        {
            switch (response.Message.MessageAction)
            {
                case MessageAction.Update:
                    return _productService.EditProductTenor(aomDb, response);
            }
            return null;
        }

        private AomProductTenorResponse ConsumeGetProductTenorRequest(GetProductTenorRequestRpc arg)
        {
            try
            {
                return new AomProductTenorResponse { ProductTenor = _productService.GetProductTenor(arg.Id) };
            }
            catch (Exception e)
            {
                Log.Error("ConsumeGetProductTenorRequest", e);
            }

            return new AomProductTenorResponse { ProductTenor = null };
        }

        private IEnumerable<ProductTenor> ConsumeGetAllProductTenorsRequest(GetAllProductTenorsRequestRpc arg)
        {
            try
            {
                return _productService.GetAllProductTenors();
            }
            catch (Exception e)
            {
                Log.Error("ConsumeGetProductTenorRequest", e);
            }

            return null;
        }

        internal override string SubscriptionId
        {
            get
            {
                return string.Empty;
            }
        }
    }
}