﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using System.Diagnostics;
using AOM.Services.ProductService;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using AOM.App.Domain;
using AOM.Services.ProductMetadataService;
using AOM.Transport.Events;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    public class ProductMetaDataRequestConsumer : IConsumer
    {
        private readonly IProductMetadataService _productService;
        private readonly IBus _aomBus;

        public ProductMetaDataRequestConsumer(IProductMetadataService productMetadataService, IBus aomBus)
        {
            _productService = productMetadataService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            //JsConfig<GetProductMetaDataResponse>.IncludeTypeInfo = true;
            //JsConfig<ProductMetaData>.IncludeTypeInfo = true;
            //JsConfig<ProductMetaDataItem>.IncludeTypeInfo = true;

            _aomBus.Subscribe<GetProductMetaDataRequest>(SubscriptionId, ConsumeGetProductMetaDataRequest);
            _aomBus.Respond<ProductMetadataItemRequest, OperationResultResponse>(ConsumeProductMetadataItemRequest);
            _aomBus.Respond<GetMetadataListsRequest, GetMetadataListsResponse>(ConsumeGetMetadataListsRequest);
            _aomBus.Respond<UpdateProductMetadataItemsDisplayOrderRequest, OperationResultResponse>(ConsumeUpdateMetadataItemsDisplayOrderRequest);
            _aomBus.Respond<UpdateMetadataListsRequest, OperationResultResponse>(ConsumeUpdateMetadataListsRequest);
        }

        private OperationResultResponse ConsumeUpdateMetadataItemsDisplayOrderRequest(UpdateProductMetadataItemsDisplayOrderRequest request)
        {
            try
            {
                Log.InfoFormat("UpdateProductMetadataItemsDisplayOrderRequest received.");
 
                _productService.UpdateProductMetadataItemsDisplayOrder(
                    request.ProductId,
                    request.MetadataItemIds,
                    request.ClientSessionInfo.UserId);

                return OperationResultResponse.CreateSuccessResponse();
            }
            catch (BusinessRuleException ex)
            {
                Log.Error(ex.Message, ex);
                return OperationResultResponse.CreateErroResponse(ex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return OperationResultResponse.CreateErroResponse("Internal error occured while processing the request.");
            }
        }

        private GetMetadataListsResponse ConsumeGetMetadataListsRequest(GetMetadataListsRequest arg)
        {
            var metadataLists = _productService.GetProductMetadataLists();

            return new GetMetadataListsResponse { MetaDataLists = metadataLists };
        }

        internal OperationResultResponse ConsumeProductMetadataItemRequest(ProductMetadataItemRequest request)
        {
            try
            {
                Log.InfoFormat("ProductMetadataItemRequest received with {0} message action.", request.MessageAction);

                var userId = request.ClientSessionInfo.UserId;

                switch (request.MessageAction)
                {
                    case MessageAction.Create:
                        _productService.AddProductMetadataItem(request.ProductMetaDataItem, userId);
                        break;
                    case MessageAction.Update:
                        _productService.UpdateProductMetadataItem(request.ProductMetaDataItem, userId);
                        break;
                    case MessageAction.Delete:
                        _productService.DeleteProductMetadataItem(request.ProductMetaDataItem.Id, userId);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                return OperationResultResponse.CreateSuccessResponse();
            }
            catch (BusinessRuleException ex)
            {
                Log.Error(ex.Message, ex);
                return OperationResultResponse.CreateErroResponse(ex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return OperationResultResponse.CreateErroResponse("Internal error occured while processing the request.");
            }
        }

        private OperationResultResponse ConsumeUpdateMetadataListsRequest(UpdateMetadataListsRequest request)
        {
            try
            {
                Log.InfoFormat("UpdateMetadataListsRequest received.");

                _productService.UpdateProductMetadataLists(
                    request.MetaDataLists, 
                    request.ClientSessionInfo.UserId);

                return OperationResultResponse.CreateSuccessResponse();
            }
            catch (BusinessRuleException ex)
            {
                Log.Error(ex.Message, ex);
                return OperationResultResponse.CreateErroResponse(ex.Message);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return OperationResultResponse.CreateErroResponse("Internal error occured while processing the request.");
            }
        }

        internal void ConsumeGetProductMetaDataRequest(GetProductMetaDataRequest request)
        {
            if (request != null && request.ProductId.HasValue)
            {
                Log.InfoFormat("ConsumeGetProductMetaDataRequest received for a single product with the Id:{0}", request.ProductId.Value);
                RefreshSingleProductMetaData(request.ProductId.Value);
            }
            else
            {
                Log.Info("Received ConsumeGetProductMetaDataRequest request - ALL Products; ");
                RefreshAllProductMetaData();
            }
        }

        private void RefreshSingleProductMetaData(long productId)
        {
            ProductMetaData productDefinition = _productService.GetProductMetaData(productId);
            if (productDefinition == null)
            {
                Log.InfoFormat("No product meta data defined for product={0}", productId);
                PublishProductMetaData(new ProductMetaData { ProductId = productId, Fields = new ProductMetaDataItem[0]});
            }
            else
            {
                PublishProductMetaData(productDefinition);
            }
        }

        private void RefreshAllProductMetaData()
        {
            var productDefinitionItems = new List<ProductMetaData>();
            try
            {
                productDefinitionItems = _productService.GetAllProductMetaData();
            }
            catch (Exception ex)
            {
                Log.Error("GetProductRequestConsumer.ConsumeGetProductConfigRequest - call to GetAllProductDefinitions returned an error", ex);
            }

            foreach (var productDefinition in productDefinitionItems)
            {
                PublishProductMetaData(productDefinition);
            }
        }

        private void PublishProductMetaData(ProductMetaData productDefinition)
        {
            var response = new GetProductMetaDataResponse {ProductMetaData = productDefinition};
            Debug.WriteLine("Publishing ProductMetaData {0} to exchange", productDefinition.ProductId);

            _aomBus.Publish(response);
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }
    }
}