﻿using System.Linq;
using AOM.App.Domain.Services;
using AOM.Services.ProductService;

namespace AOM.Transport.Service.ProductsProcessor.Consumers
{
    using AOM.Transport.Events.Products;

    using Processor.Common;

    using Argus.Transport.Infrastructure;


    public class GetProductSettingsConsumer : IConsumer
    {
        private IBus _aomBus;
        private readonly IProductService _productService;
        private readonly IUserService _userService;

        public GetProductSettingsConsumer(IBus aomBus, IProductService productService, IUserService userService)
        {
            _aomBus = aomBus;
            _productService = productService;
            _userService = userService;
        }

        public void Start()
        {
            _aomBus.Respond<GetProductSettingsRequestRpc, GetProductSettingsResponse>(OnGetProductSettings);
        }

        private GetProductSettingsResponse OnGetProductSettings(GetProductSettingsRequestRpc getProductSettingsRequest)
        {
            var user = _userService.GetUserWithPrivileges(getProductSettingsRequest.UserId);

            var productIds = user.ProductPrivileges.Select(pp => pp.ProductId).ToArray();
            var productSettings = _productService.GetProductSettings(productIds);

            var response = new GetProductSettingsResponse {ProductSettings = productSettings};
            return response;
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }
    }
}