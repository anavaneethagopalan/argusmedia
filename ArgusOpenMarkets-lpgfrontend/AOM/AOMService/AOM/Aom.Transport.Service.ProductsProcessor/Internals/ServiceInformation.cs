﻿namespace AOM.Transport.Service.ProductsProcessor.Internals
{
    using AOM.Transport.Service.Processor.Common;

    internal class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get
            {
                return DisplayName.Replace(" ", "");
            }
        }

        public string DisplayName
        {
            get
            {
                return "AOM Transport Service Products Processor";
            }
        }

        public string Description
        {
            get
            {
                return "AOM Transport Service Products Processor";
            }
        }
    }
}