﻿using AOM.Transport.Service.ProductsProcessor.Consumers;

namespace AOM.Transport.Service.ProductsProcessor
{
    using AOM.Transport.Service.Processor.Common;
    using AOM.Transport.Service.ProductsProcessor.Internals;    
    using Ninject;
    using Ninject.Extensions.Conventions;
    using Ninject.Modules;

    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<ISubscriptions>().To<Subscriptions>();
            kernel.Bind<IServiceManagement>().To<ServiceManagement>();
            kernel.Bind<IServiceInformation>().To<ServiceInformation>();
            kernel.Bind <IProductStatusHelper>().To<ProductStatusHelper>();
         
            kernel.Bind(
                x =>
                    x.FromThisAssembly()
                        .IncludingNonePublicTypes()
                        .SelectAllClasses()
                        .InheritedFrom<IConsumer>()
                        .BindAllInterfaces()
                        .Configure(b => b.InThreadScope())); // InThreadScope used as orginal code used 'ToConstant'   
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}
