﻿using AOM.Repository.MySql;
using AOM.Services;
using AOM.Transport.Service.Processor.Common;
using Ninject;

namespace AOM.Transport.Service.ProductsProcessor
{
    using System;
    using System.Diagnostics;
    using System.Linq;

    using Utils.Logging.Utils;

    internal class Program
    {
        private static void Main()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(new EFModelBootstrapper(), new ServiceBootstrap(), new LocalBootstrapper(), new ProcessorCommonBootstrap());

                WarmUp(kernel);
                ServiceHelper.BuildAndRunService<ProductsServiceProcessor>(kernel);
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (var dbAom = dbFactory.CreateAomModel())
                {
                    dbAom.Products.Any();
                }

                Log.Info(
                    string.Format(
                        "ProductsProcessor -Initial EF model build took {0} msecs.",
                        timer.ElapsedMilliseconds));

            }
            catch (Exception ex)
            {
                Log.Error("ProductsProcessor - WarmUp - Error - cannot connect to DB", ex);
                ServiceHelper.BuildAndRunService<ProductsServiceProcessor>(kernel);
            }
        }
    }
}