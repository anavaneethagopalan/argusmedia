﻿namespace AOM.Transport.Service.ProductsProcessor
{
    using System.Collections.Generic;

    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    public class ProductsServiceProcessor : ProcessorServiceBase
    {
        public ProductsServiceProcessor(
            IServiceManagement serviceManagement,
            IEnumerable<IConsumer> consumers,
            IBus aomBus)
            : base(consumers, serviceManagement, aomBus)
        {
        }

        public override string Name
        {
            get { return "ProductsServiceProcessor"; }
        }
    }
}