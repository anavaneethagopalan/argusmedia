﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOM.App.Domain.Services;
using AOM.Services.OrderService;
using AOM.Services.User;
using Ninject.Modules;

namespace DependencyResolver
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IOrderService>().To<OrderService>();
            Bind<IUserService>().To<UserService>();
        }
    }
}
