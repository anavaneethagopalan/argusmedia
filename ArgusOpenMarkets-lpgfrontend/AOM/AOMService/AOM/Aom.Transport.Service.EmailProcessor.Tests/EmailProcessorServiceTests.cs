﻿using System;
using System.Collections.Generic;
using AOM.Services.EmailService;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using Topshelf;

namespace AOM.Transport.Service.EmailProcessor.Tests
{
    [TestFixture]
    public class EmailProcessorServiceTests
    {    
        private Mock<IBus> _mockBus;
        private Mock<IServiceManagement> _mockServiceManagement;

        [TestFixtureSetUp]
        public void Setup()
        {
             _mockBus = new Mock<IBus>();
             _mockServiceManagement = new Mock<IServiceManagement>();
        }

        [Test]
        public void ProcessorStartsSingleConsumer()
        {
            //Arrange
            var mockConsumer = new Mock<IConsumer>();
            var consumers = new List<IConsumer> {mockConsumer.Object};
            var mockHostControl = new Mock<HostControl>();
            var mockEmailTimerService = new Mock<IEmailTimerService>();
            mockEmailTimerService.Setup(x => x.Start());

            //Act
            var emailProcessor = new EmailProcessorService(_mockBus.Object, consumers, _mockServiceManagement.Object, mockEmailTimerService.Object);
            emailProcessor.Start(mockHostControl.Object);

            //Assert
            mockConsumer.Verify(x => x.Start());
        }

        [Test]
        public void ProcessorStartsMultipleConsumers()
        {
            //Arrange
            var mockConsumer1 = new Mock<IConsumer>();
            var mockConsumer2 = new Mock<IConsumer>();
            var mockHostControl = new Mock<HostControl>();
            var consumers = new List<IConsumer> {mockConsumer1.Object, mockConsumer2.Object};
            var mockEmailTimerService = new Mock<IEmailTimerService>();
            mockEmailTimerService.Setup(x => x.Start());

            //Act
            var emailProcessor = new EmailProcessorService(_mockBus.Object, consumers, _mockServiceManagement.Object, mockEmailTimerService.Object);
            emailProcessor.Start(mockHostControl.Object);

            //Assert
            mockConsumer1.Verify(x => x.Start());
            mockConsumer2.Verify(x => x.Start());
        }

        [Test]
        public void ProcessorThrowsExceptionIfConsumerFailsToStart()
        {
            //Arrange
            var mockConsumer = new Mock<IConsumer>();
            mockConsumer.Setup(x => x.Start()).Throws(new Exception("Test"));
            var mockHostControl = new Mock<HostControl>();
            var consumers = new List<IConsumer> { mockConsumer.Object };

            var mockEmailTimerService = new Mock<IEmailTimerService>();
            mockEmailTimerService.Setup(x => x.Start());

            //Act
            var emailProcessor = new EmailProcessorService(_mockBus.Object, consumers, _mockServiceManagement.Object, mockEmailTimerService.Object);
           
            //Assert
            Assert.Throws<Exception>(() => emailProcessor.Start(mockHostControl.Object));
            mockConsumer.Verify(x => x.Start());        
        }
    }
}