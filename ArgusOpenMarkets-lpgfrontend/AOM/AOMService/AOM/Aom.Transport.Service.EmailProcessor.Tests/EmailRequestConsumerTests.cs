﻿using System;
using log4net.Core;
using Utils.Logging;

namespace AOM.Transport.Service.EmailProcessor.Tests
{
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Aom;
    using AOM.Services.EmailService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Emails;
    using AOM.Transport.Service.EmailProcessor.Consumers;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class EmailRequestConsumerTests
    {
        private Mock<IDbContextFactory> _mockDbFactory;

        private Mock<IEmailService> _mockEmailService;

        private EmailRequestConsumer _emailRequestConsumer;

        private Mock<ISmtpService> _mockSmtpService;

        private Mock<IBus> _mockBus;

        private Mock<IAomWebsiteDetails> _mockAomWebsiteDetails;

        [SetUp]
        public void Setup()
        {
            _mockDbFactory = new Mock<IDbContextFactory>();
            _mockEmailService = new Mock<IEmailService>();
            _mockSmtpService = new Mock<ISmtpService>();
            _mockBus = new Mock<IBus>();
            _mockAomWebsiteDetails = new Mock<IAomWebsiteDetails>();

            _emailRequestConsumer = new EmailRequestConsumer(
                _mockDbFactory.Object,
                _mockEmailService.Object,
                _mockSmtpService.Object,
                _mockBus.Object,
                _mockAomWebsiteDetails.Object);
        }

        [Test]
        public void ShouldNotCallEmailServiceToSendAnEmailIfTheActionIsVerify()
        {
            var emailRequest = new AomSendEmailRequest
                               {
                                   ClientSessionInfo = new ClientSessionInfo(),
                                   EmailId = 1,
                                   MessageAction = MessageAction.Verify
                               };

            _emailRequestConsumer.ConsumeAomSendEmailRequest(emailRequest);

            _mockEmailService.Verify(
                m =>
                    m.SendEmailNotification(It.IsAny<IAomModel>(),
                                            It.IsAny<ISmtpService>(),
                                            It.IsAny<long>(),
                                            It.IsAny<string>()),
                Times.Never);
        }

        [Test]
        public void ShouldCallEmailServiceToSendAnEmailIfTheActionIsSend()
        {
            var emailRequest = new AomSendEmailRequest
            {
                ClientSessionInfo = new ClientSessionInfo(),
                EmailId = 1,
                MessageAction = MessageAction.Send
            };

            _emailRequestConsumer.ConsumeAomSendEmailRequest(emailRequest);

            _mockEmailService.Verify(
                m =>
                    m.SendEmailNotification(It.IsAny<IAomModel>(),
                                            It.IsAny<ISmtpService>(),
                                            It.IsAny<long>(),
                                            It.IsAny<string>()),
                Times.Once);
        }


        [Test]
        public void ShouldCallSendOutstandingEmailsIfTheActionIsCheck()
        {
            var emailRequest = new AomSendEmailRequest
            {
                ClientSessionInfo = new ClientSessionInfo(),
                EmailId = 1,
                MessageAction = MessageAction.Check
            };

            _emailRequestConsumer.ConsumeAomSendEmailRequest(emailRequest);

            _mockEmailService.Verify(
                m =>
                    m.SendAnyOutstandingEmailNotifications(It.IsAny<IAomModel>(),
                                                           It.IsAny<ISmtpService>(),
                                                           It.IsAny<string>()),
                Times.Once);
        }

        [Test]
        public void ShouldReturnAnEmptySubscriptionId()
        {
            Assert.That(_emailRequestConsumer.SubscriptionId, Is.EqualTo("AOM"));
        }

        [Test]
        public void ShouldSupplyAomWebsiteUrlWhenSendingEmailNotifications()
        {
            // Arrange
            const string fakeUrl = "http://notarealwebsite.com";
            _mockAomWebsiteDetails.SetupGet(wd => wd.WebsiteUrl).Returns(fakeUrl);
            
            var emailRequest = new AomSendEmailRequest
            {
                ClientSessionInfo = new ClientSessionInfo(),
                EmailId = 1,
                MessageAction = MessageAction.Send
            };

            // Act
            _emailRequestConsumer.ConsumeAomSendEmailRequest(emailRequest);

            // Assert
            _mockEmailService.Verify(
                (m =>
                    m.SendEmailNotification(It.IsAny<IAomModel>(),
                                            It.IsAny<ISmtpService>(),
                                            It.Is<long>(id => id == emailRequest.EmailId),
                                            It.Is<string>(url => url == fakeUrl))));

        }

        [Test]
        public void ShouldSubscribeToSendEmailRequestsOnStart()
        {
            _emailRequestConsumer.Start();
            _mockBus.Verify(
                b => b.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomSendEmailRequest>>()));
        }

        [Test]
        public void ShouldLogErrorIfEmailSendFails()
        {
            // Arrange
            var fakeLog = new MemoryAppenderForTests();
            const string fakeExceptionMessage = "IAmAnException";
            _mockEmailService.Setup(
                s =>
                    s.SendEmailNotification(It.IsAny<IAomModel>(),
                                            It.IsAny<ISmtpService>(),
                                            It.IsAny<long>(),
                                            It.IsAny<string>()))
                             .Throws(new ApplicationException(fakeExceptionMessage));
            var emailRequest = new AomSendEmailRequest
            {
                ClientSessionInfo = new ClientSessionInfo(),
                EmailId = 1,
                MessageAction = MessageAction.Send
            };

            // Act
            _emailRequestConsumer.ConsumeAomSendEmailRequest(emailRequest);

            // Assert
            fakeLog.AssertALogMessageContains(Level.Error,
                                              "Failed to send Email Notification Id: " + emailRequest.EmailId);
            fakeLog.AssertALogMessageContains(Level.Error, fakeExceptionMessage);
        }
    }
}
