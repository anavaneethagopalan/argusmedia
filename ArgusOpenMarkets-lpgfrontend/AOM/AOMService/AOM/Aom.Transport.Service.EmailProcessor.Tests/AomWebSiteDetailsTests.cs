﻿using System.Configuration;
using AOM.Transport.Service.EmailProcessor.Internals;
using NUnit.Framework;

namespace AOM.Transport.Service.EmailProcessor.Tests
{
    [TestFixture]
    public class AomWebSiteDetailsTests
    {
        [Test]
        public void ShouldReturnWebSiteDetailsFromConfiguration()
        {
            ConfigurationManager.AppSettings["WebsiteURL"] = "http://aom";
            var aomWebSiteDetails = new AomWebsiteDetails();
            var websiteUrl = aomWebSiteDetails.WebsiteUrl;

            Assert.That(websiteUrl, Is.EqualTo("http://aom"));
        }
    }
}
