﻿namespace AOM.Transport.Service.OrganisationProcessor.Tests
{
    using System;

    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.Organisations;
    using AOM.Transport.Service.OrganisationProcessor.Consumers;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class OrganisationConsumerTest
    {
        [Test]
        public void SaveOrganisationShouldReturnOrganisationWithIdOfMinus1IfSavingTheOrganisationThrowsAndException()
        {
            var mockAomBus = new Mock<IBus>();
            var mockCrmAdministrationService = new Mock<ICrmAdministrationService>();

            var orgConsumer = new OrganisationConsumer(mockAomBus.Object, mockCrmAdministrationService.Object);

            Exception ex = new Exception();
            mockCrmAdministrationService.Setup(
                m => m.SaveOrganisation(It.IsAny<IOrganisation>(), It.IsAny<long>(), It.IsAny<IOrganisationService>()))
                                        .Throws(ex);

            var saveOrgReq = new SaveOrganisationRequestRpc { Organisation = null, UserId = 100000 };
            var response = orgConsumer.SaveOrganisation(saveOrgReq);

            Assert.That(response.OrganisationId, Is.EqualTo(-1));
        }
    }
}
