﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Services.AuthenticationService;
using AOM.Services.ProductService;
using AOM.Services.SystemNotificationService;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Service.OrganisationProcessor.Consumers;
using Argus.Transport.Infrastructure;
using AOM.Repository.MySql.Tests.Crm;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.OrganisationProcessor.Tests
{
    [TestFixture]
    public class OrganisationDetailsConsumerTests
    {
        [Test]
        public void ShouldReturnTheCorrectSubscriptionId()
        {
            var mockBus = new Mock<IBus>();
            var mockOrganisationService = new Mock<IOrganisationService>();
            var mockProductsService = new Mock<IProductService>();
            var mockDbContextFactory = new Mock<IDbContextFactory>();
            var mockSystemNotificationService = new Mock<ISystemNotificationService>();
            var mockCompanySettingsAuthenticationService = new Mock<ICompanySettingsAuthenticationService>();


            var orgDetailsConsumer = new OrganisationDetailsConsumer(
                mockOrganisationService.Object,
                mockBus.Object,
                mockDbContextFactory.Object,
                mockProductsService.Object,
                mockSystemNotificationService.Object,
                mockCompanySettingsAuthenticationService.Object);

            Assert.That(orgDetailsConsumer.SubscriptionId, Is.EqualTo("AOM"));
        }


        [Test]
        public void ShouldPublishCompanyDetailsRefreshResponse()
        {
            var mockBus = new Mock<IBus>();
            var mockOrganisationService = new Mock<IOrganisationService>();
            var mockProductsService = new Mock<IProductService>();

            var mockSystemNotificationService = new Mock<ISystemNotificationService>();

            var allProducts = new List<Product> {new Product {ProductId = 1, Name = "Prod1"}};
            mockProductsService.Setup(m => m.GetAllProducts()).Returns(allProducts);

            ICrmModel mockCrmModel = new MockCrmModel();

            OrganisationDto org;
            SystemEventNotificationDto notification;

            CrmDataBuilder.WithModel(mockCrmModel)
                .AddOrganisation("Org1", out org)
                .AddSystemEventNotification(
                    "charles@bonesteel.com",
                    1,
                    org,
                    SystemEventType.CounterPartyPermissionChanges,
                    out notification);

            mockOrganisationService.Setup(x => x.GetOrganisations())
                .Returns(
                    new List<Organisation>
                    {
                        new Organisation {Id = org.Id, Name = "TestingOrg", ShortCode = "XXX"}
                    });

            List<OrganisationDetails> organisationDetails = new List<OrganisationDetails>();
            mockOrganisationService.Setup(m => m.GetOrganisationDetails(It.IsAny<List<Product>>()))
                .Returns(organisationDetails);

            var mockCompanySettingsAuthenticationService = new Mock<ICompanySettingsAuthenticationService>();

            var orgDetailsConsumer = new OrganisationDetailsConsumer(
                mockOrganisationService.Object,
                mockBus.Object,
                new MockDbContextFactory(null, mockCrmModel),
                mockProductsService.Object,
                mockSystemNotificationService.Object,
                mockCompanySettingsAuthenticationService.Object);

            orgDetailsConsumer.ConsumeGetCompanyDetailRequest(
                new GetCompanyDetailsRefreshRequest());

            mockBus.Verify(x => x.Publish(It.IsAny<GetCompanyDetailsRefreshResponse>()));
        }
    }
}