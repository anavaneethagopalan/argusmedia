﻿using System.Linq;
using AOM.Repository.MySql;
using AOM.Services;
using AOM.Transport.Service.Processor.Common;
using AOM.Transport.Service.UserProcessor.Consumers;
using Ninject;
using NUnit.Framework;
using Utils.Javascript.Tests.Extension;

namespace AOM.Transport.Service.UserProcessor.Tests
{
    [TestFixture]
    public class BootstrapperTests
    {
        [Test]
        public void LoadsExpectedIConsumerListTest()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(new LocalBootstrapper(), new EFModelBootstrapper(), new ServiceBootstrap(),
                    new ProcessorCommonBootstrap());
                var bindings = kernel.GetAll(typeof(IConsumer)).ToList();

                AssertHelper.AssertListOnlyContains(bindings, x => x.GetType(),
                    typeof(ResetPasswordConsumer),
                    typeof(ForgotPasswordConsumer),
                    typeof(GetUserRequestConsumerRpc),
                    typeof(KillUsersOrderConsumer),
                    typeof(TerminateUserRequestConsumer),
                    typeof(UpdateUserDetailConsumer),
                    typeof(UserAuthenticationConsumer),
                    typeof(UserDisconnectConsumer),
                    typeof(ChangeUserPasswordConsumer),
                    typeof(GetUserRequestConsumer),
                    typeof(HealthCheckConsumer),
                    typeof(LogUserErrorConsumer),
                    typeof(ClientLogConsumer));
            }
        }
    }
}