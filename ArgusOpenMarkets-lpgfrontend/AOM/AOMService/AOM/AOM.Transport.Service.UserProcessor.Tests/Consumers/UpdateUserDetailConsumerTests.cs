﻿using AOM.App.Domain;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.UserProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.UserProcessor.Tests.Consumers
{
    [TestFixture]
    public class UpdateUserDetailConsumerTests
    {
        [Test]
        public void CapturesBusinessRulesExceptions()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<ICrmAdministrationService>();

            mockOrderService.Setup(x => x.BlockUser("userToBlock", -1))
                .Throws(new BusinessRuleException("Test Business Rule"));

            //Act
            var consumer = new UpdateUserDetailConsumer(mockOrderService.Object, mockBus.Object);

            //Assert
            var response =
                consumer.BlockUserRequest(new BlockUserRequestRpc {UserName = "userToBlock", RequestorUserId = -1});

            Assert.IsFalse(response.Success);
            Assert.AreEqual("userToBlock", response.UserName);
            Assert.AreEqual("Test Business Rule", response.MessageBody);
        }

        [Test]
        public void CreatesSuccessMessageCorrectly()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<ICrmAdministrationService>();

            mockOrderService.Setup(x => x.BlockUser("userToBlock", -1)).Returns(true);

            //Act
            var consumer = new UpdateUserDetailConsumer(mockOrderService.Object, mockBus.Object);

            //Assert
            var response =
                consumer.BlockUserRequest(new BlockUserRequestRpc {UserName = "userToBlock", RequestorUserId = -1});

            Assert.IsTrue(response.Success);
            Assert.AreEqual("userToBlock", response.UserName);
            Assert.AreEqual("Account blocked", response.MessageBody);
        }

        [Test]
        public void CreatesUnblockSuccessMessageCorrectly()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<ICrmAdministrationService>();

            mockOrderService.Setup(x => x.UnblockUser("userToUnblock", -1)).Returns(true);

            //Act
            var consumer = new UpdateUserDetailConsumer(mockOrderService.Object, mockBus.Object);

            //Assert
            var response =
                consumer.UnblockUserRequest(new UnblockUserRequestRpc {UserName = "userToUnblock", RequestorUserId = -1});

            Assert.IsTrue(response.Success);
            Assert.AreEqual("userToUnblock", response.UserName);
        }
    }
}
