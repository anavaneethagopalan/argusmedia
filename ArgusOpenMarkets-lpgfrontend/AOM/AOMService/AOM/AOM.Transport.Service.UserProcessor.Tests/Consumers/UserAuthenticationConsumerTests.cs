﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.TopicManager;
using AOM.Transport.Events.News;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.UserProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.UserProcessor.Tests.Consumers
{
    [TestFixture]
    public class UserAuthenticationConsumerTests
    {
        private Mock<IUserAuthenticationService> _mockUserAuthenticationService;
        private Mock<IUserService> _mockUserService;
        private Mock<IBus> _mockBus;
        private Mock<IAomTopicManager> _mockAomTopicManager;
        private UserAuthenticationConsumer _uac;

        [SetUp]
        public void Setup()
        {
            _mockUserAuthenticationService = new Mock<IUserAuthenticationService>();
            _mockBus = new Mock<IBus>();
            _mockUserService = new Mock<IUserService>();
            _mockAomTopicManager = new Mock<IAomTopicManager>();

            _uac = new UserAuthenticationConsumer(_mockUserAuthenticationService.Object, _mockBus.Object, _mockUserService.Object, _mockAomTopicManager.Object);
        }

        [Test]
        public void ShouldReturnSubscriptionIdOfAom()
        {
            Assert.That(_uac.SubscriptionId, Is.EqualTo("AOM"));
        }

        [Test]
        public void PublishingUserAuthenticatingPublishedShouldIfTokenAuthenticatedShouldPublishUserAuthenticated()
        {
            var subs = StartConsumer();

            var authenticationResult = new AuthenticationResult
            {
                FirstLogin = true,
                IsAuthenticated = true,
                Message = "",
                FailureReason = "",
                SessionToken = "token"
            };

            var onUserAuthenticatingPublished = subs.UserAuthenticatingPublished;
            _mockUserAuthenticationService.Setup(m => m.AuthenticateToken(It.IsAny<string>(), It.IsAny<string>())).Returns(authenticationResult);
            
            var user = MakeUser();
            var aomUserAuthenticatingPublished = new AomUserAuthenticatingPublished {User = user};

            // Act
            onUserAuthenticatingPublished(aomUserAuthenticatingPublished);

            // Assert
            _mockBus.Verify(b => b.Publish(It.IsAny<AomUserAuthenticatedPublished>()), Times.Once);
        }

        private User MakeUser()
        {
            return new User
            {
                Id = 1,
                Username = "fred",
                IsActive = true
            };
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();
            _mockBus.Setup(b => b.Subscribe("AOM", It.IsAny<Action<AomUserAuthenticatingPublished>>()))
                .Callback<string, Action<AomUserAuthenticatingPublished>>((s, a) => subs.UserAuthenticatingPublished = a);

            _uac.Start();
            Assert.That(subs.UserAuthenticatingPublished, Is.Not.Null);
            return subs;
        }

        private class SubscribedActions
        {
            public Action<AomUserAuthenticatingPublished> UserAuthenticatingPublished { get; set; }
        }
    }
}