﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Services.CrmService;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.UserProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;

namespace AOM.Transport.Service.UserProcessor.Tests.Consumers
{
    [TestFixture]
    public class ResetPasswordConsumerTests
    {
        private ResetPasswordConsumer _resetPasswordConsumer;
        private Mock<IEncryptionService> _mockEncryptionService;
        private Mock<IBus> _mockBus;
        private Mock<ICredentialsService> _mockCredentialService;

        [SetUp]
        public void Setup()
        {
            _mockEncryptionService = new Mock<IEncryptionService>();
            _mockBus = new Mock<IBus>();
            _mockCredentialService = new Mock<ICredentialsService>();

            _resetPasswordConsumer = new ResetPasswordConsumer(_mockEncryptionService.Object, _mockCredentialService.Object, _mockBus.Object);
        }

        [Test]
        public void NullRequest()
        {
            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(null));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);
        }

        [Test]
        public void InvalidUserName()
        {
            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc()));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);
        }

        [Test]
        public void UserNotExist()
        {
            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc()));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);
        }

        [Test]
        public void SystemUser()
        {
            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc()));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);
        }

        [Test]
        public void PasswordExpired()
        {
            SetupMockCredentialsServiceToReturn(null);

            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc { OldPassword = "3ENVQGvRX9i7cMUj8WPqTMikwBhonIoLE53TRXgvii8H@YZ" }));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockCredentialService.VerifyAll();

            SetupMockCredentialsServiceToReturn(new UserCredentials {Expiration = DateTime.UtcNow.AddHours(-1)});

            result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc { OldPassword = "3ENVQGvRX9i7cMUj8WPqTMikwBhonIoLE53TRXgvii8H@YZ" }));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockCredentialService.VerifyAll();
        }

        [Test]
        public void TempPasswordExpired()
        {
            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc { OldPassword = "3ENVQGvRX9i7cMUj8WPqTMikwBhonIoLE53TRXgvii8H@YZ" }));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockCredentialService.Setup(c => c.GetUserCredentials(It.IsAny<long>(), true, It.IsAny<bool>()))
                                  .Returns(new UserCredentials {Expiration = DateTime.UtcNow.AddHours(-1)});

            result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc { OldPassword = "3ENVQGvRX9i7cMUj8WPqTMikwBhonIoLE53TRXgvii8H@YZ" }));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockCredentialService.Verify(c => c.GetUserCredentials(It.IsAny<long>(), false, It.IsAny<bool>()),
                                          Times.Never);
            _mockCredentialService.VerifyAll();
        }

        [Test]
        public void InvalidRequest()
        {
            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc()));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockCredentialService.VerifyAll();
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ResetPasswordRequest(bool isSuccess)
        {
            SetupMockCredentialsServiceToReturn(new UserCredentials { Expiration = DateTime.UtcNow.AddMinutes(10), PasswordHashed = "hello" });
            _mockEncryptionService.Setup(c => c.Hash(It.IsAny<IUser>(), It.IsAny<string>())).Returns(() => "hello");
            _mockCredentialService.Setup(c => c.UpdateUserCredentials(It.IsAny<UserCredentials>())).Returns(() => isSuccess);

            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _resetPasswordConsumer.ResetPasswordRequest(new ResetPasswordRequestRpc { OldPassword = "3ENVQGvRX9i7cMUj8WPqTMikwBhonIoLE53TRXgvii8H@YZ" }));

            Assert.NotNull(result);
            Assert.AreEqual(result.Success, isSuccess);
            Assert.NotNull(result.MessageBody);

            _mockCredentialService.VerifyAll();
            _mockEncryptionService.VerifyAll();
        }

        private void SetupMockCredentialsServiceToReturn(UserCredentials credentials)
        {
            _mockCredentialService.Setup(c => c.GetUserCredentials(It.IsAny<long>(), It.IsAny<bool>(), It.IsAny<bool>()))
                                  .Returns(credentials);
        }
    }
}