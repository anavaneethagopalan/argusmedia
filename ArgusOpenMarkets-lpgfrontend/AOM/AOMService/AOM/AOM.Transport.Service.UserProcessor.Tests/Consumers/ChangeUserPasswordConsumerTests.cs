﻿using AOM.App.Domain.Services;
using AOM.Services.CrmService;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.UserProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.UserProcessor.Tests.Consumers
{
    [TestFixture]
    public class ChangeUserPasswordConsumerTests
    {
        [Test]
        public void CanNotUpdateSystemUsersPassword()
        {
            var mockBus = new Mock<IBus>();
            var mockCredentialService = new Mock<ICredentialsService>();
            var mockUserService = new Mock<IUserService>();
            var _changeUserPasswordConsumer = new ChangeUserPasswordConsumer(mockCredentialService.Object, mockBus.Object, mockUserService.Object);   


            mockUserService.Setup(x => x.GetUserId("sysUser")).Returns(-1);
            var changePasswordRequest = new ChangePasswordRequestRpc
            {
                AdminResetPassword = true,
                IsTemporary = true,
                NewPassword = "Test",
                OldPassword = "Test",
                CheckOldPassword = false,
                Username = "sysUser",
                RequestorUserId = 1
            };
            var result = _changeUserPasswordConsumer.ChangePasswordRequest(changePasswordRequest);
            Assert.That(result.MessageBody.Contains("can not change the password"));
        }
    }
}
