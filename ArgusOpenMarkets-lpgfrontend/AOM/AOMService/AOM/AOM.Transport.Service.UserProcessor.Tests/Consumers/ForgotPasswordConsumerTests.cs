﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm;
using AOM.Services.CrmService;
using AOM.Services.EmailService;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.UserProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.UserProcessor.Tests.Consumers
{
    [TestFixture]
    public class ForgotPasswordConsumerTests
    {
        private ForgotPasswordConsumer _forgotPasswordConsumer;
        private Mock<IDbContextFactory> _mockDbContextFactory;
        private Mock<IEncryptionService> _mockEncryptionService;
        private Mock<IBus> _mockBus;
        private Mock<ICredentialsService> _mockCredentialService;
        private Mock<IUserService> _mockUserService;
        private Mock<IEmailService> _mockEmailService;

        [SetUp]
        public void Setup()
        {
            _mockDbContextFactory = new Mock<IDbContextFactory>();
            _mockEncryptionService = new Mock<IEncryptionService>();
            _mockBus = new Mock<IBus>();
            _mockCredentialService = new Mock<ICredentialsService>();
            _mockUserService = new Mock<IUserService>();
            _mockEmailService = new Mock<IEmailService>();

            _forgotPasswordConsumer = new ForgotPasswordConsumer(
                _mockDbContextFactory.Object, _mockEncryptionService.Object, _mockCredentialService.Object,
                _mockBus.Object, _mockUserService.Object, _mockEmailService.Object);
        }

        [Test]
        public void NullRequest()
        {
            Assert.Throws<NullReferenceException>(() => _forgotPasswordConsumer.ForgotPasswordRequest(null));
        }

        [Test]
        public void InvalidUserName()
        {
            _mockUserService.Setup(c => c.GetUserInfoDto(It.IsAny<string>())).Returns(() => null);
            
            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _forgotPasswordConsumer.ForgotPasswordRequest(new ForgotPasswordRequestRpc()));
            
            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockUserService.VerifyAll();
        }

        [Test]
        public void UserNotExist()
        {
            _mockUserService.Setup(c => c.GetUserInfoDto(It.IsAny<string>())).Returns(() => new UserInfoDto {Id = -10});
            
            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _forgotPasswordConsumer.ForgotPasswordRequest(new ForgotPasswordRequestRpc()));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockUserService.VerifyAll();
        }

        [Test]
        public void SystemUser()
        {
            _mockUserService.Setup(c => c.GetUserInfoDto(It.IsAny<string>())).Returns(() => new UserInfoDto { Id = -1 });

            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _forgotPasswordConsumer.ForgotPasswordRequest(new ForgotPasswordRequestRpc()));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockUserService.VerifyAll();
        }

        [Test]
        public void CreateCredentialFailed()
        {
            _mockUserService.Setup(c => c.GetUserInfoDto(It.IsAny<string>())).Returns(() => new UserInfoDto());
            _mockEncryptionService.Setup(c => c.Hash(It.IsAny<IUser>(), It.IsAny<string>())).Returns(() => string.Empty);
            _mockCredentialService.Setup(c => c.CreateTempUserCredentials(It.IsAny<string>(), It.IsAny<string>())).Returns(() => false);

            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _forgotPasswordConsumer.ForgotPasswordRequest(new ForgotPasswordRequestRpc()));

            Assert.NotNull(result);
            Assert.IsFalse(result.Success);
            Assert.NotNull(result.MessageBody);

            _mockUserService.VerifyAll();
            _mockCredentialService.VerifyAll();
            _mockEncryptionService.VerifyAll();
        }

        [TestCase(1)]
        [TestCase(0)]
        [TestCase(-1)]
        public void ForgotPasswordRequest(long emailId)
        {
            _mockUserService.Setup(c => c.GetUserInfoDto(It.IsAny<string>())).Returns(() => new UserInfoDto());
            _mockEncryptionService.Setup(c => c.Hash(It.IsAny<IUser>(), It.IsAny<string>())).Returns(() => string.Empty);
            _mockCredentialService.Setup(c => c.CreateTempUserCredentials(It.IsAny<string>(), It.IsAny<string>())).Returns(() => true);
            _mockDbContextFactory.Setup(c => c.CreateAomModel());
            _mockEmailService.Setup(c => c.CreateEmail(It.IsAny<IAomModel>(), It.IsAny<ForgotPasswordEmailNotification>())).Returns(() => emailId);

            ChangePasswordResponse result = null;
            Assert.DoesNotThrow(() => result = _forgotPasswordConsumer.ForgotPasswordRequest(new ForgotPasswordRequestRpc()));

            Assert.NotNull(result);
            Assert.AreEqual(result.Success, emailId > 0);
            Assert.NotNull(result.MessageBody);

            _mockUserService.VerifyAll();
            _mockCredentialService.VerifyAll();
            _mockEncryptionService.VerifyAll();
            _mockDbContextFactory.VerifyAll();
            _mockEmailService.VerifyAll();
        }
    }
}
