﻿using System;
using System.Globalization;

namespace Utils.Validation
{
    public static class ArgumentValidationExtensions
    {
        /// <summary>
        /// Validates that a string value is not <c>null</c> or <see cref="string.Empty"/>.
        /// </summary>
        /// <param name="value">The string value to be validated</param>
        /// <param name="name">The name of the argument</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown when <paramref name="value"/> is <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Thrown when <paramref name="value"/> is <see cref="string.Empty"/>.
        /// </exception>
        public static string ArgumentStringNotNullOrEmpty(this string value, string name)
        {
            StringNotNullOrEmptyInternal(name, "name");
            StringNotNullOrEmptyInternal(value, name);

            return value;
        }

        /// <summary>
        /// Internal implementation of string validation that assumes its name argument is good.
        /// Validates that a string value is not <c>null</c> or <see cref="string.Empty"/>.
        /// </summary>
        /// <param name="value">The string value to be validated</param>
        /// <param name="name">The name of the argument</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown when <paramref name="value"/> is <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Thrown when <paramref name="value"/> is <see cref="string.Empty"/>.
        /// </exception>
// ReSharper disable UnusedParameter.Local
        private static void StringNotNullOrEmptyInternal(string value, string name)
// ReSharper restore UnusedParameter.Local
        {
            if (value == null)
            {
                throw new ArgumentNullException(name);
            }
            if (value.Length == 0)
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                    "Argument {0} must not be empty",
                    name));
            }
        }
    }
}