﻿using System.Text.RegularExpressions;

namespace Utils.Validation
{
    public static class PasswordValidator
    {
        static readonly Regex Regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d|.*[^\da-zA-Z]).{8,50}$");

        /// <summary>
        /// validates a password with the following conditions
        /// 1 or more upper case char
        /// 1 or more special char or number
        /// 1 or more lower case char
        /// 8 - 50 char length
        /// </summary>
        /// <param name="password"></param>
        /// <returns>bool</returns>
        public static bool ValidatePassword(string password)
        {
            return Regex.Match(password).Success;
        }
    }
}