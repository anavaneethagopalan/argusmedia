﻿using JetBrains.Annotations;
using log4net;
using log4net.Config;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using log4net.Core;

[assembly: XmlConfigurator(Watch = true)]
namespace Utils.Logging
{
    namespace Utils
	{
		/// <summary>
		/// Common module logging static class.
		/// </summary>
		public class Log
		{
			private static ILog log;

			static Log()
			{
				log = LogManager.GetLogger("AOM");
			}

			public static void Debug(string text)
			{
				if (log.IsDebugEnabled) log.Debug(text);
			}

            public static void Debug(string errorMessage, Exception ex)
            {
                if (!log.IsDebugEnabled) return;
                LogException(s => log.Debug(s), errorMessage, ex);
            }

            [StringFormatMethod("text")]
		    public static void DebugFormat(string text, params object[] args)
		    {
		        if (!log.IsDebugEnabled) return;

		        if (args.Length > 0)
		        {
		            log.DebugFormat(text, args);
		        }
		        else
		        {
		            log.Debug(text);
		        }
		    }
         
			public static void Warn(string text)
			{
				if (log.IsWarnEnabled) log.Warn(text);
			}

            public static void Warn(string errorMessage, Exception ex)
            {
                if (!log.IsWarnEnabled) return;
                LogException(s => log.Warn(s), errorMessage, ex);
            }

            [StringFormatMethod("text")]
            public static void WarnFormat(string text, params object[] args)
            {
                if (!log.IsWarnEnabled) return;

                if (args.Length > 0)
                {
                    log.WarnFormat(text, args);
                }
                else
                {
                    log.Warn(text);
                }
            }

		    public static void Info(string text)
			{
				if (log.IsInfoEnabled) log.Info(text);
			}

            public static void Info(string errorMessage, Exception ex)
            {
                if (!log.IsInfoEnabled) return;
                LogException(s => log.Info(s), errorMessage, ex);
            }

            [StringFormatMethod("text")]
		    public static void InfoFormat(string text, params object[] args)
		    {
		        if (log.IsInfoEnabled)
		        {
		            if (args.Length > 0)
		            {
		                log.InfoFormat(text, args);
		            }
		            else
		            {
                        log.Info(text);   
		            }
		        }
		    }

			public static void Error(string text)
			{
				if (log.IsErrorEnabled) log.Error(text);
			}

            [StringFormatMethod("text")]
            public static void ErrorFormat(string text, params object[] args)
            {
                if (log.IsErrorEnabled)
                {
                    if (args.Length > 0)
                    {
                        log.ErrorFormat(text, args);
                    }
                    else
                    {
                        log.Error(text);
                    }
                }
            }

            public static void Error(string errorMessage, Exception ex)
			{
			    if (!log.IsErrorEnabled) return;
                LogException(s => log.Error(s), errorMessage, ex);
			}

            public static void Fatal(String text)
            {
                log.Fatal(text);
            }

            [StringFormatMethod("text")]
		    public static void FatalFormat(string text, params object[] args)
		    {
		        if (log.IsFatalEnabled)
		        {
                    log.FatalFormat(text, args);
		        }
		    }

		    private static void LogException(Action<string> logAction, string message, Exception ex)
		    {
                logAction(ex != null ? CreateExceptionLogMessage(message, ex) : message);    

                var entiftyFrameworkException = ex as DbEntityValidationException;
                if (entiftyFrameworkException != null)
                {
                    foreach (var error in entiftyFrameworkException.EntityValidationErrors.SelectMany(e => e.ValidationErrors))
                    {
                        logAction("DbEntityValidationException::" + error.ErrorMessage);
                    }
                }

                var typeLoadException = ex as ReflectionTypeLoadException;
		        if (typeLoadException != null)
		        {
		            foreach (var error in typeLoadException.LoaderExceptions)
		            {
		                logAction("ReflectionTypeLoadException::" + CreateExceptionLogMessage(message, error));
		            }
		        }
		    }

		    private static string CreateExceptionLogMessage(string errorMessage, Exception ex)
		    {
		        var logMessage = string.Join("|",
		                                     errorMessage,
		                                     ex.GetType().Name,
		                                     ex.Message,
		                                     ex.InnerException,
		                                     ex.StackTrace);
		        return logMessage;
		    }

		    public static void Write(Level logLevel, string message)
		    {
		        WriteFormat(logLevel, message);
		    }

            public static void WriteFormat(Level logLevel, string message, params object[] args)
		    {
                // Can't use a switch here as the Levels are static class instances
		        if (logLevel == Level.Error)
		        {
		            ErrorFormat(message, args);
		        }
                else if (logLevel == Level.Warn)
                {
                    WarnFormat(message, args);
                }
                else if (logLevel == Level.Info)
                {
                    InfoFormat(message, args);
                }
                else if (logLevel == Level.Debug)
                {
                    DebugFormat(message, args);
                }
                else if (logLevel == Level.Fatal)
                {
                    FatalFormat(message, args);
                }
                else
                {
                    WarnFormat(
                        "Attempting to write to log at unexpected level [{0}], message: {1}", logLevel, message);
                }
		    }
		}
	}
}