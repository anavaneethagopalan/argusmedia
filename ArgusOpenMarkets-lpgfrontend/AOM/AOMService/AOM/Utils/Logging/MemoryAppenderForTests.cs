﻿using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Utils.Logging
{
    public class MemoryAppenderForTests : IDisposable
    {
        private bool _disposed = false;

        public enum MessageCheckType
        {
            Equals,
            Contains
        }

        public MemoryAppenderForTests()
        {
            Appender = CreateLog4NetMemoryAppender();
        }

        public MemoryAppender Appender { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool IsThereALogMessageThatContains(Level logLevel,
            string message,
            bool caseSensitive)
        {
            return IsLogMessagePresent(logLevel, message, MessageCheckType.Contains, caseSensitive);
        }

        public bool IsThereALogMessageThatIsEqualTo(Level logLevel,
            string message,
            bool caseSensitive)
        {
            return IsLogMessagePresent(logLevel, message, MessageCheckType.Equals, caseSensitive);
        }

        public void Clear()
        {
            Appender.Clear();
        }

        public LoggingEvent[] GetEvents()
        {
            return Appender.GetEvents();
        }

        public void AssertALogMessageContains(
            Level logLevel,
            string message,
            bool caseSensitive = false)
        {
            bool messageFound = IsThereALogMessageThatContains(logLevel, message, caseSensitive);
            Assert.IsTrue(messageFound,
                string.Format("No log message contains {0} at level {1}",
                    message,
                    logLevel));
        }

        public Level AssertALogMessageContains(IEnumerable<Level> logLevels, string message, bool caseSensitive = false)
        {
            var levels = logLevels.ToList();
            try
            {
                return levels.First(level => IsThereALogMessageThatContains(level, message, caseSensitive));
            }
            catch (Exception)
            {
                var failureMessage = string.Format("No log message contains {0} at levels [{1}]",
                    message,
                    string.Join(",", levels));
                Assert.Fail(failureMessage);
            }
            return null;
        }

        public void AssertThatThereAreNoLogErrors()
        {
            AssertNoLogMessagesAtLevel(Level.Error);
        }

        public void AssertNoLogMessagesAtLevel(params Level[] logLevel)
        {
            var logEvents = GetEvents();
            var messagesAtLevels = logLevel.SelectMany(l => logEvents.Where(e => e.Level == l)).ToList();
            if (messagesAtLevels.Count > 0)
            {
                Assert.Fail("Messages found at unexpected log levels: [{0}]",
                    string.Join(";",
                        messagesAtLevels.Select(
                            m => string.Format("{0}:{1}", m.Level, m.RenderedMessage))));
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                RemoveAppender();
            }
            _disposed = true;
        }

        private bool IsLogMessagePresent(Level logLevel,
            string message,
            MessageCheckType messageCheckType,
            bool caseSensitive)
        {
            var events = Appender.GetEvents();
            var eventsForLogLevel = events.Where(e => e.Level == logLevel);

            // http://stackoverflow.com/questions/11309557/assign-a-lambda-expression-using-the-conditional-ternary-operator
            var selectionPredicate =
                messageCheckType == MessageCheckType.Contains
                    ? (Func<string, string, bool>) ((s1, s2) => s1.Contains(s2))
                    : (Func<string, string, bool>) ((s1, s2) => s1 == s2);
            var stringMutate =
                caseSensitive
                    ? (Func<string, string>) (s => s)
                    : (Func<string, string>) (s => s.ToUpperInvariant());

            var matchingEvents =
                eventsForLogLevel.Where(e => selectionPredicate(stringMutate(e.RenderedMessage), stringMutate(message)));
            if (matchingEvents == null)
            {
                throw new ApplicationException("Dev Error: matchingEvents is null");
            }
            return matchingEvents.Any();
        }

        private static MemoryAppender CreateLog4NetMemoryAppender()
        {
            var memoryAppender = new MemoryAppender();
            BasicConfigurator.Configure(memoryAppender);
            return memoryAppender;
        }

        private void RemoveAppender()
        {
            var repository = LogManager.GetRepository() as log4net.Repository.Hierarchy.Hierarchy;
            if (repository == null)
            {
                throw new ApplicationException(
                    "Unable to get log4net repository to remove test MemoryAppender");
            }
            repository.Root.RemoveAppender(Appender);
        }
    }
}