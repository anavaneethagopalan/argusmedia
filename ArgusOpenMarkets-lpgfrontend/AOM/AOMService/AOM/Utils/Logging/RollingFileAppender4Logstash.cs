﻿using System.Configuration;
using System.IO;
using log4net.Appender;
using log4net.Core;

namespace Utils.Logging
{
    public class RollingFileAppender4Logstash : RollingFileAppender
    {
        public RollingFileAppender4Logstash()
        {
            // Use the entry assembly, unless it's not available (we are running under IISExpress).
            var entryAsssembly = System.Reflection.Assembly.GetEntryAssembly();
            if (null != entryAsssembly)
                log4net.GlobalContext.Properties["LogName"] = Path.GetFileNameWithoutExtension(entryAsssembly.Location);
            else
                log4net.GlobalContext.Properties["LogName"] = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
        }

        public override string File
        {
            get
            {
                return base.File;        
            }

            set
            {
                string productionLogsFolder = ConfigurationManager.AppSettings["ProductionLogsFolder"];
                if (Path.IsPathRooted(value) || !Directory.Exists(productionLogsFolder))
                {
                    base.File = value;
                }
                else
                {
                    base.File = Path.Combine(productionLogsFolder, value);
                }
            }
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            base.Append(CloneLoggingEvent(loggingEvent));
        }

        public static LoggingEvent CloneLoggingEvent(LoggingEvent loggingEvent)
        {
            const string newlineReplacement = " *** ";

            var eventData = new LoggingEventData
            {
                Domain = loggingEvent.Domain,
                ExceptionString = loggingEvent.GetExceptionString().Replace("\r\n", newlineReplacement).Replace("\n", newlineReplacement),
                Identity = loggingEvent.Identity,
                Level = loggingEvent.Level,
                LocationInfo = loggingEvent.LocationInformation,
                LoggerName = loggingEvent.LoggerName,
                Message = loggingEvent.RenderedMessage.Replace("\r\n", newlineReplacement).Replace("\n", newlineReplacement),
                Properties = loggingEvent.GetProperties(),
                ThreadName = loggingEvent.ThreadName,
                TimeStamp = loggingEvent.TimeStamp,
                UserName = loggingEvent.UserName
            };

            return new LoggingEvent(eventData);
        }
    }
}
