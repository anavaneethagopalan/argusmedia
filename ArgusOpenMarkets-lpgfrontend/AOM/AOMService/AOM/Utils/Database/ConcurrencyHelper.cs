﻿namespace Utils.Database
{
    using System;
    using System.Data.Entity.Infrastructure;
    using Utils.Logging.Utils;

    public class ConcurrencyHelper
    {
        public static T RetryOnConcurrencyException<T>(string description, int maxAttempts, Func<T> dbActionToRetry)
        {
            for (int attempts = maxAttempts; attempts > 0; attempts--)
            {
                try
                {
                    return dbActionToRetry.Invoke();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (attempts > 1)
                    {
                        var remainingAttempts = attempts - 1;
                        Log.Info(string.Format("{0} : Concurrency exception - trying execution again - {1} {2} left.", description, remainingAttempts, (remainingAttempts == 1 ? "attempt" : "attempts")));
                        continue;
                    }
                    Log.Error(string.Format("{0} - Exceeded retry count of {1}", description, maxAttempts), ex);
                    throw;
                }
            }

            throw new Exception("Internal Error : retry count unexpectedly exceeded");
        }
    }
}