﻿namespace AOMAdmin.Tests
{
    using System;

    using AOM.Transport.Events.Orders;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    class PurgeOrdersInDiffusionTests
    {
        [Test]
        public void ClickingOnPurgeOrdersShouldPlaceAPurgeOrdersInDiffusionRequestOnTheBus()
        {
            var mockBus = new Mock<IBus>();
            var purgeOrdersInDiffusion = new PurgeOrdersInDiffusion(mockBus.Object);

            purgeOrdersInDiffusion.lstProducts.Items.Add(new ComboBoxItem(1, "Product"), true);

            purgeOrdersInDiffusion.cmdPurgeOrdersInDiffusion_Click(new object(), new EventArgs());
            mockBus.Verify(m => m.Publish(It.IsAny<PurgeOrdersInDiffusionRequest>()), Times.Once);
        }
    }
}
