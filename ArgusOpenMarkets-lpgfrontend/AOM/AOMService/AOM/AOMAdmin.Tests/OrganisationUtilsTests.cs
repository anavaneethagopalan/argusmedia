﻿using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AOMAdmin.Tests
{
    [TestFixture]
    public class OrganisationUtilsTests
    {
        [Test]
        public void GetNonSystemOrganisationRolesShouldFilterOutProcessorUserRole()
        {
            var mockRole1 = new Mock<IOrganisationRole>();
            mockRole1.SetupGet(r => r.Id).Returns(1);
            mockRole1.SetupGet(r => r.Name).Returns("A mock role");
            
            var mockProcessorUserRole = new Mock<IOrganisationRole>();
            mockProcessorUserRole.SetupGet(r => r.Id).Returns(2);
            mockProcessorUserRole.SetupGet(r => r.Name).Returns("Super Dev/Test User");

            var mockOrganisationService = new Mock<IOrganisationService>();
            mockOrganisationService.Setup(os => os.GetOrganisationRoles(It.IsAny<long>()))
                                   .Returns(() => new List<IOrganisationRole>
                                   {
                                       mockRole1.Object,
                                       mockProcessorUserRole.Object
                                   });
            var nonSystemRoles = OrganisationUtils.GetNonSystemOrganisationRoles(1, mockOrganisationService.Object);
            Assert.That(nonSystemRoles.Count, Is.EqualTo(1));
            var nonSystemRole = nonSystemRoles.First();
            Assert.NotNull(nonSystemRole);
            Assert.That(nonSystemRole.Id, Is.EqualTo(mockRole1.Object.Id));
        }
    }
}
