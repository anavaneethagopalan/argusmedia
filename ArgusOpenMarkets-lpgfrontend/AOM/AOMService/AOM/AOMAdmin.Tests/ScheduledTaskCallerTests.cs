﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Transport.Events.ScheduledTasks;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOMAdmin.Tests
{
    [TestFixture]
    public class ScheduledTaskCallerTests
    {
        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
        }

        private class FakeRequestType
        {
        }

        [Test]
        public void CreateNewScheduledTaskAddsRequestContainingCorrectTask()
        {
            var task = new ScheduledTask
            {
                Id = 12345,
                TaskType = "fakeTask"
            };

            var stc = new ScheduledTaskCaller<FakeRequestType>(_mockBus.Object);
            stc.CreateNewScheduledTask(task);

            _mockBus.Verify(
                b =>
                    b.Request<CreateNewScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(
                        It.Is<CreateNewScheduledTaskRequestRpc>(rpc => rpc.Task.Id == task.Id)),
                Times.Once);
        }

        [Test]
        public void DeleteScheduledTaskAddsRequestWithCorrectId()
        {
            const long taskId = 123456;
            var stc = new ScheduledTaskCaller<FakeRequestType>(_mockBus.Object);
            stc.DeleteScheduledTask(taskId);
            _mockBus.Verify(
                b =>
                    b.Request<DeleteScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(
                        It.Is<DeleteScheduledTaskRequestRpc>(rpc => rpc.TaskId == taskId)));
        }

        [Test]
        public void GetAllTasksAddsRequestRpcToBus()
        {
            _mockBus.Setup(b =>
                    b.Request<GetAllScheduledTasksRequestRpc, GetAllScheduledTasksResponseRpc>(
                        It.IsAny<GetAllScheduledTasksRequestRpc>()))
                .Returns(new GetAllScheduledTasksResponseRpc());
            var stc = new ScheduledTaskCaller<FakeRequestType>(_mockBus.Object);

            stc.GetAllTasks();
            _mockBus.Verify(
                b =>
                    b.Request<GetAllScheduledTasksRequestRpc, GetAllScheduledTasksResponseRpc>(
                        It.Is<GetAllScheduledTasksRequestRpc>(rpc => rpc.TaskType == typeof(FakeRequestType).Name)));
        }

        [Test]
        public void TestGetTaskNameReturnsRequestTypeName()
        {
            var stc = new ScheduledTaskCaller<FakeRequestType>(_mockBus.Object);
            Assert.AreEqual(typeof(FakeRequestType).Name, stc.GetTaskName());
        }
    }
}