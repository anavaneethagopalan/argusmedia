﻿using AOM.App.Domain.Entities;

namespace AOMAdmin.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class ComboBoxItemTests
    {
        [Test]
        public void ShouldInitialiseComboBoxItemDescription()
        {
            var comboBoxItem = new ComboBoxItem(100, "Combo Special");

            Assert.That(comboBoxItem.Description, Is.EqualTo("Combo Special"));
        }

        [Test]
        public void ShouldInitialiseComboBoxItemId()
        {
            var comboBoxItem = new ComboBoxItem(100, "Combo Special");

            Assert.That(comboBoxItem.Id, Is.EqualTo(100));
        }

        [Test]
        public void ToStringShouldGiveDescription()
        {
            const string desc = "Combo Special";
            var comboBoxItem = new ComboBoxItem(100, desc);
            Assert.AreEqual(desc, comboBoxItem.ToString());
        }

        [Test]
        public void OrganisationComboBoxInitialisesOrgType()
        {
            const OrganisationType orgType = OrganisationType.Argus;
            var comboBoxItem = new OrganisationComboBox(100, "Combo Special", orgType);
            Assert.AreEqual(orgType, comboBoxItem.OrgType);
        }
    }
}
