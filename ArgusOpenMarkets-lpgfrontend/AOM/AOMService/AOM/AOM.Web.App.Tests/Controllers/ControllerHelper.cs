﻿using System.IO;
using System.Reflection;
using System.Web;
using System.Web.SessionState;

namespace AOM.Web.App.Tests.Controllers
{
    public static class ControllerHelper
    {
        public static void SetupHttpContext()
        {
            // Step 1: Setup the HTTP Request
            var httpRequest = new HttpRequest("", "http://localhost/", "");

            // Step 2: Setup the HTTP Response
            var httpResponse = new HttpResponse(new StringWriter());

            // Step 3: Setup the Http Context
            var httpContext = new HttpContext(httpRequest, httpResponse);
            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(), new HttpStaticObjectsCollection(), 
                10, true, HttpCookieMode.AutoDetect, SessionStateMode.InProc, false);

            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, CallingConventions.Standard,
                new[] { typeof(HttpSessionStateContainer) }, null).Invoke(new object[] { sessionContainer });
            
            // Step 4: Assign the Context
            HttpContext.Current = httpContext;
        }
    }
}
