﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.AuthenticationService;
using AOM.Services.CrmService;
using AOM.Services.CrmService.TopicReduction;
using AOM.Services.PermissionService;
using AOM.Transport.Events;
using AOM.Transport.Events.BrokerPermissions;
using AOM.Transport.Events.CounterpartyPermissions;
using AOM.Transport.Events.Organisations;
using AOM.Web.App.Controllers;
using AOM.Web.App.Security;

using Argus.Transport.Infrastructure;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AOM.Web.App.Tests.Controllers
{
    [TestFixture]
    public class PermissionControllerTests
    {
        private PermissionController _permissionController;
        private MockCrmModel _mockCrmModel;
        private MockAomModel _mockAomModel;
        private Mock<IBus> _mockBus;
        private Mock<IUserService> _mockUserService;
        private OrganisationService _mockOrganisationService;
        private TopicReductionService _mockTopicReductionService;
        private Mock<ICounterpartyPermissionAuthenticationService> _mockCounterpartyPermissionAuthenticationService;
        private Mock<IBrokerPermissionAuthenticationService> _mockBrokerPermissionAuthenticationService;
        private Mock<IBrokerPermissionService> _mockBrokerPermissionService;
        private Mock<ICounterpartyPermissionService> _mockCounterpartyPermissionService;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockCrmModel = new MockCrmModel();
            _mockAomModel = new MockAomModel();

            Organisation org1 = new Organisation() { Id = 1, Name = "TestOrg1", OrganisationType = OrganisationType.Trading, ShortCode = "ORG1", LegalName = "OrganisationOne" };
            Organisation org2 = new Organisation() { Id = 2, Name = "TestOrg2", OrganisationType = OrganisationType.Trading, ShortCode = "ORG2", LegalName = "OrganisationTwo" };
            Organisation brk1 = new Organisation() { Id = 3, Name = "TestBrk1", OrganisationType = OrganisationType.Brokerage, ShortCode = "BRK1", LegalName = "BrokerOne" };
            Organisation brk2 = new Organisation() { Id = 4, Name = "TestBrk2", OrganisationType = OrganisationType.Brokerage, ShortCode = "BRK2", LegalName = "BrokerTwo" };

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddDeliveryLocation("DeliveryLocation1")
                .AddProduct("Product1", 2, marketId:3)
                .AddProduct("Product2", 3, marketId:3) 
                .AddTenorCode("Tenor1")
                .AddProductTenor("Product1_Tenor1")
                .AddProductTenor("Product2_Tenor1")
                .DBContext.SaveChangesAndLog(-1);

            _mockCrmModel.Organisations.Add(org1.ToDto());
            _mockCrmModel.Organisations.Add(org2.ToDto());
            _mockCrmModel.Organisations.Add(brk1.ToDto());
            _mockCrmModel.Organisations.Add(brk2.ToDto());

            UserInfoDto usr;
            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddUser("Test User", false, out usr, org1.ToDto())
                .AddSubscribedProduct(org1.ToDto(), _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddSubscribedProduct(org1.ToDto(), _mockAomModel.Products.Last().Id, out subscribedProduct)
                .AddSubscribedProduct(org2.ToDto(), _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddSubscribedProduct(org2.ToDto(), _mockAomModel.Products.Last().Id, out subscribedProduct)
                .AddSubscribedProduct(brk1.ToDto(), _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddSubscribedProduct(brk1.ToDto(), _mockAomModel.Products.Last().Id, out subscribedProduct)
                .AddSubscribedProduct(brk2.ToDto(), _mockAomModel.Products.First().Id, out subscribedProduct)   
                .AddSubscribedProduct(brk2.ToDto(), _mockAomModel.Products.Last().Id, out subscribedProduct);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            _mockOrganisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());

            _mockTopicReductionService = new TopicReductionService();
            _mockUserService = new Mock<IUserService>();
            _mockCounterpartyPermissionAuthenticationService = new Mock<ICounterpartyPermissionAuthenticationService>();
            _mockBrokerPermissionAuthenticationService = new Mock<IBrokerPermissionAuthenticationService>();
            _mockBrokerPermissionService = new Mock<IBrokerPermissionService>();
            _mockCounterpartyPermissionService = new Mock<ICounterpartyPermissionService>();

            _mockBus.Setup(m => m.Request<GetCommonPermissionedOrganisationsRpc, GetCommonPermissionedOrganisationsResponse>(It.IsAny<GetCommonPermissionedOrganisationsRpc>())).Returns(
                new GetCommonPermissionedOrganisationsResponse
                {
                    CommonOrganisations = new List<Organisation>() { org1, org2, brk1, brk2 }
                });

            _permissionController = new PermissionController(_mockBus.Object, _mockUserService.Object, _mockOrganisationService, _mockTopicReductionService, _mockCounterpartyPermissionAuthenticationService.Object,
                _mockBrokerPermissionAuthenticationService.Object, _mockBrokerPermissionService.Object, _mockCounterpartyPermissionService.Object);

            _permissionController.Request = new HttpRequestMessage();
            ControllerHelper.SetupHttpContext();
        }

        [Test]
        public async Task GetCommonPermissionedOrganisations()
        {
            var response = _permissionController.GetCommonPermissionedOrganisations(1, 1, BuyOrSell.Buy);
            var content = await response.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<Organisation>>(content);
            
            Assert.That(resultList.Count == 4);
            Assert.That(resultList[0].ShortCode == "ORG1");
            Assert.That(resultList[1].ShortCode == "ORG2");
        }

        [Test]
        public async Task GetCommonPermissionedBrokers()
        {
            var response = _permissionController.GetCommonPermissionedOrganisations(1, 1, BuyOrSell.Buy);
            var content = await response.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<Organisation>>(content);
            
            Assert.That(resultList.Count(r => r.OrganisationType == OrganisationType.Brokerage) == 2);
            Assert.That(resultList[2].ShortCode == "BRK1");
        }

        [Test]
        public async Task GetCounterpartyPermissionsByMarket()
        {
            var response = _permissionController.GetCounterpartyPermissionsByMarket(3, 1);
            var content = await response.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<OrganisationPermissionList>>(content);

            Assert.That(resultList.Count == 2);
            Assert.That(resultList[0].ProductId == 2);
            Assert.That(resultList[1].ProductId == 3);
            Assert.That(resultList[0].Bps.Count == 1);
        }

        [Test]
        public async Task GetCounterpartyPermissionsByProduct()
        {
            var response = _permissionController.GetCounterpartyPermissionsByProduct(2, 2);
            var content = await response.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<OrganisationPermissionList>>(content);

            Assert.That(resultList[0].ProductId == 2);
            Assert.That(resultList[0].Bps.Count == 1);
        }

        [Test]
        public async Task GetBrokerPermissionsByMarket()
        {
            var response = _permissionController.GetBrokerPermissionsByMarket(3, 3);
            var content = await response.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<OrganisationPermissionList>>(content);

            Assert.That(resultList.Count == 2);
            Assert.That(resultList[0].ProductId == 2);
            Assert.That(resultList[1].ProductId == 3);
            Assert.That(resultList[0].Bps.Count == 2);
        }

        [Test]
        public async Task GetBrokerPermissionsByProduct()
        {
            var response = _permissionController.GetBrokerPermissionsByProduct(3, 3);
            var content = await response.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<OrganisationPermissionList>>(content);

            Assert.That(resultList.Count == 1);
            Assert.That(resultList[0].ProductId == 3);
            Assert.That(resultList[0].Bps.Count == 2);
        }
    }
}
