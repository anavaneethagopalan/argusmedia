﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.Services.ProductService;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Events.Products;
using AOM.Web.App.Controllers;

using Argus.Transport.Infrastructure;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AOM.Web.App.Tests.Controllers
{
    [TestFixture]
    public class DataControllerTests
    {
        private Mock<DataController> _mockDataController;
        private Mock<IBus> _mockBus;
        private Mock<IProductService> _mockProductService;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();

            _mockBus.Setup(m => m.Request<GetPrincipalsForBrokerRpc, GetPrincipalsForBrokerResponse>(It.IsAny<GetPrincipalsForBrokerRpc>())).Returns(
                new GetPrincipalsForBrokerResponse
                {
                    PrincipalsWithPermissions = new OrganisationsWithPermissions
                    {
                        BuyOrganisations = { new OrganisationPermissionSummary { CanTrade = true, Organisation = new OrganisationMinimumDetails { Id = 1, Name = "Tushara" } } }
                    }
                });

            _mockBus.Setup(m => m.Request<GetBrokersForPrincipalRpc, GetBrokersForPrincipalResponse>(It.IsAny<GetBrokersForPrincipalRpc>())).Returns(
                new GetBrokersForPrincipalResponse
                {
                    BrokersWithPermissions = new OrganisationsWithPermissions
                    {
                        BuyOrganisations = { new OrganisationPermissionSummary { CanTrade = true, Organisation = new OrganisationMinimumDetails { Id = 1, Name = "Bellamore" } } },
                        SellOrganisations = { new OrganisationPermissionSummary { CanTrade = true, Organisation = new OrganisationMinimumDetails { Id = 2, Name = "Kewish" } } }
                    }
                });

            _mockProductService = new Mock<IProductService>();
            List<MarketsProducts> marketsAndProducts = new List<MarketsProducts>()
            {
                new MarketsProducts() {MarketId = 1, MarketName = "TestMarket1", ProductId = 1, ProductName = "TestProduct1"},
                new MarketsProducts() {MarketId = 1, MarketName = "TestMarket1", ProductId = 2, ProductName = "TestProduct2"}
            };
            _mockProductService.Setup(g => g.GetAllMarketsAndProducts()).Returns(marketsAndProducts);

            _mockDataController = new Mock<DataController>(_mockBus.Object, _mockProductService.Object);
            _mockDataController.Object.Request = new HttpRequestMessage();

            ControllerHelper.SetupHttpContext();
        }

        [Test]
        public async Task DataControllerShouldGetAllPrincipals()
        {
            const long userId = 1;
            _mockDataController.Setup(m => m.GetUserId()).Returns(userId);
            var response = _mockDataController.Object.GetAllPrincipals(userId, 1);
            var content = await response.Content.ReadAsStringAsync();
            Assert.That(content.Contains("Tushara"));
        }

        [Test]
        public async Task DataControllerShouldGetAllMarketAndProducts()
        {
            var response = _mockDataController.Object.GetAllMarketAndProducts();
            var content = await response.Content.ReadAsStringAsync();
            var resultList = JsonConvert.DeserializeObject<List<MarketsProducts>>(content);

            Assert.That(resultList.Count == 2);
            Assert.That(resultList[0].ProductId == 1);
            Assert.That(resultList[1].ProductId == 2);
        }

        [Test]
        public async Task DataControllerShouldGetAllPrincipalsOnlyMinimumDetails()
        {
            const long userId = 1;
            _mockDataController.Setup(m => m.GetUserId()).Returns(userId);
            var response = _mockDataController.Object.GetAllPrincipals(userId, 1);
            var content = await response.Content.ReadAsStringAsync();
            Assert.That(!content.Contains("email"));
        }

        [Test]
        public async Task DataControllerShouldGetAllBrokers()
        {
            const long userId = 2;   
            _mockDataController.Setup(m => m.GetUserId()).Returns(userId);
            var response = _mockDataController.Object.GetAllBrokers(userId, 1);
            var content = await response.Content.ReadAsStringAsync();
            Assert.That(content.Contains("Kewish"));
        }

        [Test]
        public void GetProductPrivilegesShouldReturnAllProductPrivileges()
        {
            var productPrivileges = new List<ProductPrivilege>();
            productPrivileges.Add(new ProductPrivilege {Description = "Desc1", Id = 1});
            var busResponse = new GetProductSystemPrivilegesResponse
            {
                ProductPrivileges = productPrivileges,
                SystemPrivileges = new List<SystemPrivilege>()
            };

            _mockBus.Setup( m => m.Request<GetProductSystemPrivilegesRequestRpc, GetProductSystemPrivilegesResponse>(It.IsAny<GetProductSystemPrivilegesRequestRpc>())).Returns(busResponse);

            var response = _mockDataController.Object.GetProductSystemPrivileges();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            dynamic responseObject = JsonConvert.DeserializeObject(responseBody);

            string description = responseObject.productPrivileges[0].description;
            Assert.That(responseObject, Is.Not.Null);
            Assert.That(description, Is.EqualTo("Desc1"));
        }

        [Test]
        public void GetAllBrokersShouldReturnBadRequestIfInputUserIdDifferentToHeaderUserId()
        {
            const long inputUserId = 2;
            const long headerUserId = 42;
            _mockDataController.Setup(m => m.GetUserId()).Returns(headerUserId);
            var response = _mockDataController.Object.GetAllBrokers(inputUserId, 1);
            Assert.That(response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest));
        }

        [Test]
        public void GetAllPrinciplesShouldReturnBadRequestIfInputUserIdDifferentToHeaderUserId()
        {
            const long inputUserId = 2;
            const long headerUserId = 42;
            _mockDataController.Setup(m => m.GetUserId()).Returns(headerUserId);
            var response = _mockDataController.Object.GetAllPrincipals(inputUserId, 1);
            Assert.That(response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest));
        }
    }
}