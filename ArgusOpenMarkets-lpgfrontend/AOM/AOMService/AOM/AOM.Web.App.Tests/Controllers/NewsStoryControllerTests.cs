namespace AOM.Web.App.Tests.Controllers
{
    using System.Net.Http;
    using AOM.Transport.Events.News;
    using AOM.Web.App.Controllers;
    using Moq;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using Argus.Transport.Infrastructure;

    [TestFixture]
    public class NewsStoryControllerTests
    {
        private NewsStoryController _controller;

        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            var newsStoryResponse = new NewsStoryResponse {CmsId = "1", Story = "A Story"};
            _mockBus.Setup(m => m.Request<NewsStoryRequestRpc, NewsStoryResponse>(It.IsAny<NewsStoryRequestRpc>()))
                .Returns(newsStoryResponse);

            _controller = new NewsStoryController(_mockBus.Object) {Request = new HttpRequestMessage()};
            ControllerHelper.SetupHttpContext();
        }

        [Test]
        public void NewsStoryControllerShouldReturnAStoryForANewsItem()
        {
            var response = _controller.NewsStory(1, 1);

            string responseBody = response.Content.ReadAsStringAsync().Result;


            dynamic responseObject = JsonConvert.DeserializeObject(responseBody);
            string story = responseObject.story;

            Assert.That(story, Is.Not.Null);
            Assert.That(story, Is.EqualTo("A Story"));
        }

        [Test]
        public void NewsStoryControllerShouldReturnErrorForMissingNewsItem()
        {
            var newsStoryResponse = new NewsStoryResponse {CmsId = "1", Story = ""};
            _mockBus.Setup(m => m.Request<NewsStoryRequestRpc, NewsStoryResponse>(It.IsAny<NewsStoryRequestRpc>()))
                .Returns(newsStoryResponse);

            var response = _controller.NewsStory(1, 1);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            dynamic responseObject = JsonConvert.DeserializeObject(responseBody);
            string story = responseObject.story;
            bool error = responseObject.error;

            Assert.That(error, Is.EqualTo(true));
            Assert.That(story.Contains("Please contact Aom "));
        }
    }
}