using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using AOM.Web.App.Controllers;
using AOM.Web.App.Security;

using Argus.Transport.Infrastructure;

using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace AOM.Web.App.Tests.Controllers
{
    [TestFixture]
    public class LoginControllerTests
    {
        private LoginController _controller;
        private LoginController _controllerFailed;
        private Mock<IIpService> _mockIpService;
        private Mock<IUserService> _mockUserService;
        [SetUp]
        public void Setup()
        {
            var mockEncryptionService = new Mock<IEncryptionService>();
            _mockIpService = new Mock<IIpService>();
            var mockBus = new Mock<IBus>();
            _mockUserService = new Mock<IUserService>();
            var mockUserAuthenticationService = new Mock<IUserAuthenticationService>();
            var mockUserAuthenticationServiceFailed = new Mock<IUserAuthenticationService>();

            mockEncryptionService.Setup(m => m.Hash(It.IsAny<IUser>(), It.IsAny<string>())).Returns("hashpassword");
            var authenticationResultSucceed = new AuthenticationResult
            {
                IsAuthenticated = true,
                FirstLogin = false,
                Message = "Success",
                Token = "token",
                User = new User {Id = 1, Username = "nathan"}
            };
            var authenticationResultFailed = new AuthenticationResult
            {
                IsAuthenticated = false,
                FirstLogin = false,
                Message = "Error",
                Token = "token",
                User = new User { Id = 1, Username = "nathan" }
            };

            mockBus.Setup(m => m.Request<UserAuthenticationRequestRpc, AuthenticationResult>(It.IsAny<UserAuthenticationRequestRpc>())).Returns(authenticationResultSucceed);
            ChangePasswordResponse changePasswordResult = new ChangePasswordResponse {Success = true, UserName = "t1"};
            mockBus.Setup(m => m.Request<ChangePasswordRequestRpc, ChangePasswordResponse>(It.IsAny<ChangePasswordRequestRpc>())).Returns(changePasswordResult);

            var getUserIdResponse = new GetUserIdResponseRpc {UserId = 100};
            mockBus.Setup(m => m.Request<GetUserIdRequestRpc, GetUserIdResponseRpc>(It.IsAny<GetUserIdRequestRpc>())).Returns(getUserIdResponse);
            mockBus.Setup(m => m.Request<ForgotPasswordRequestRpc, ChangePasswordResponse>(It.IsAny<ForgotPasswordRequestRpc>())).Returns(
                new ChangePasswordResponse {Success = true, MessageBody = string.Empty, UserName = string.Empty,});

            mockBus.Setup(m => m.Request<ResetPasswordRequestRpc, ChangePasswordResponse>(It.IsAny<ResetPasswordRequestRpc>())).Returns(
                new ChangePasswordResponse {Success = true, MessageBody = string.Empty, UserName = string.Empty,});

            _mockIpService.Setup(m => m.GetIpAddressOfRemoteConnection()).Returns("1.1.1.1").Verifiable();

            List<UserLoginSource> loginSources = new List<UserLoginSource>() { new UserLoginSource() { Code = "aom" } };

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<string>()));
            _mockUserService.Setup(m => m.GetUserId(It.IsAny<string>())).Returns(1);

            _mockUserService.Setup(m => m.GetUserLoginSources()).Returns(loginSources);
            mockUserAuthenticationService.Setup(m => m.AuthenticateUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<long>(), It.IsAny<string>())).Returns(authenticationResultSucceed);

            _controller = new LoginController(mockBus.Object, mockEncryptionService.Object, _mockIpService.Object, _mockUserService.Object, mockUserAuthenticationService.Object)
            {
                Request = new HttpRequestMessage()
            };

            mockUserAuthenticationServiceFailed.Setup(m => m.AuthenticateUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<long>(), It.IsAny<string>())).Returns(authenticationResultFailed);
            _controllerFailed = new LoginController(mockBus.Object, mockEncryptionService.Object, _mockIpService.Object, _mockUserService.Object, mockUserAuthenticationServiceFailed.Object)
            {
                Request = new HttpRequestMessage()
            };

            ControllerHelper.SetupHttpContext();
        }

        [Test]
        public async Task LoginControllerShouldReturnIsAuthenticatedIsTrueAndSetTokenForValidUser()
        {
            var credentials = JObject.FromObject(new UserAuthenticationRequestRpc {Password = "test", Username = "Steve", LoginSource = "aom" });
            var response = await _controller.AuthenticateUserPost(credentials);

            string responseBody = response.Content.ReadAsStringAsync().Result;
            dynamic responseObject = JsonConvert.DeserializeObject(responseBody);
            bool isAuthenticated = responseObject.isAuthenticated;
            string token = responseObject.token;

            Assert.That(isAuthenticated, Is.True);
            Assert.That(token, Is.EqualTo("token"));
        }

        [Test]
        public async Task LoginControllerShouldReturnAUserWithProductsAndPrivilegesIfLoginCorrect()
        {
            var userWithPerms = MakeUserWithPerms();
            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(userWithPerms);
            var credentials = JObject.FromObject(new UserAuthenticationRequestRpc { Password = "test", Username = "Steve", LoginSource = "aom" });
            var response = await _controller.AuthenticateUserPost(credentials);

            string responseBody = response.Content.ReadAsStringAsync().Result;
            dynamic responseObject = JsonConvert.DeserializeObject(responseBody);
            bool isAuthenticated = responseObject.isAuthenticated;
            
            var user = responseObject.user;
            string token = responseObject.token;

            Assert.That(isAuthenticated, Is.True);
            Assert.That(token, Is.EqualTo("token"));
            var userWithPrivs = ((dynamic) ((JObject) (responseObject))).user;
            var username = ((dynamic) ((JObject) (userWithPrivs))).username;
            Assert.That(userWithPrivs, Is.Not.Null);
            Assert.That(((JValue)(username)).Value, Is.EqualTo("fred"));
        }

        private User MakeUserWithPerms()
        {
            var userWithPerms = new User();
            userWithPerms.Id = 1;
            userWithPerms.Username = "fred";
            userWithPerms.ProductPrivileges = new List<UserProductPrivilege>();
            userWithPerms.ProductPrivileges.Add(new UserProductPrivilege
            {
                DisplayOrder = 0,
                Name = "Priv1",
                Privileges = null,
                ProductId = 1
            });

            return userWithPerms;
        }

        [Test]
        public async Task LoginControllerShouldReturnIsNotAuthenticatedForInvalidLoginSource()
        {
            var credentials = JObject.FromObject(new UserAuthenticationRequestRpc { Password = "test", Username = "Steve", LoginSource = "xyz" });

            var response = await _controller.AuthenticateUserPost(credentials);
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var responseObject = JsonConvert.DeserializeObject<AuthenticationResult>(responseBody);

            Assert.That(responseObject.IsAuthenticated, Is.False);
            Assert.That(responseObject.FirstLogin, Is.False);
            Assert.That(responseObject.Message, Is.StringContaining("Invalid source information, please try again"));
        }
        
        [Test]
        public async Task LoginControllerShouldRecordIPAddressOfIncomingRequest()
        {
            var credentials = JObject.FromObject(new UserAuthenticationRequestRpc {Password = "test", Username = "steve", LoginSource = "aom"});

            var response = await _controller.AuthenticateUserPost(credentials);
            string responseBody = response.Content.ReadAsStringAsync().Result;
            JsonConvert.DeserializeObject(responseBody);

            _mockIpService.Verify(a => a.GetIpAddressOfRemoteConnection(), Times.Once);
        }

        [Test]
        public async Task LoginControllerShouldPause3SecondsBetweenFailedLoginAttempts()
        {
            var credentials = JObject.FromObject(new UserAuthenticationRequestRpc { Password = "test", Username = "steve", LoginSource = "aom"});

            var response = await _controllerFailed.AuthenticateUserPost(credentials);
            string responseBody = response.Content.ReadAsStringAsync().Result;
            dynamic responseObject = JsonConvert.DeserializeObject(responseBody);
            bool isAuthenticated = responseObject.isAuthenticated;
            string token = responseObject.token;
            DateTime firstAttempt = DateTime.Now;

            Assert.That(isAuthenticated, Is.False);
            Assert.That(token, Is.EqualTo("token"));

            response = await _controllerFailed.AuthenticateUserPost(credentials);
            responseBody = response.Content.ReadAsStringAsync().Result;
            responseObject = JsonConvert.DeserializeObject(responseBody);
            isAuthenticated = responseObject.isAuthenticated;
            DateTime secondAttempt = DateTime.Now;

            Assert.That(isAuthenticated, Is.False);
            Assert.Greater(secondAttempt, firstAttempt.AddSeconds(3));
        }

        [Test]
        public void LoginControllerShouldReturnErrorMessageWhenCredentialsAreNullWhenUpdatingDetails()
        {
            var response = _controller.UpdateDetails(null);
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var responseObject = JsonConvert.DeserializeObject<AuthenticationResult>(responseBody);
            Assert.That(responseObject.Message, Is.EqualTo("No credentials received, please try again."));
        }

        [Test]
        public void LoginControllerShouldReturnErrorMessageWhenOldPasswordDoesNotMatchNewPasswordWhenUpdatingDetails()
        {
            var credentials = new JObject();
            credentials["username"] = "nathan";
            credentials["oldpassword"] = "password";
            credentials["newpassword"] = "password";

            var response = _controller.UpdateDetails(credentials);
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JsonConvert.DeserializeObject<AuthenticationResult>(responseBody);
            Assert.That(responseObject.Message, Is.EqualTo("New and old passwords cannot be the same!"));
        }

        [Test]
        public void ForgotPasswordEmptyRequest()
        {
            var response = _controller.ForgotPassword(null);
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JsonConvert.DeserializeObject<AuthenticationResult>(responseBody);
            Assert.That(responseObject.Message, Is.EqualTo("No credentials received, please try again."));
        }

        [Test]
        public void ForgotPasswordSuccessful()
        {
            HttpResponseMessage response = null;
            Assert.DoesNotThrow(() => response = _controller.ForgotPassword(JObject.FromObject(new ForgotPasswordRequestRpc {Username = "test", Url = "http://localhost/#"})));
            Assert.NotNull(response);

            string responseBody = response.Content.ReadAsStringAsync().Result;
            var responseObject = JsonConvert.DeserializeObject<AuthenticationResult>(responseBody);

            Assert.NotNull(responseBody);
            Assert.IsTrue(responseObject.IsAuthenticated);
            Assert.AreEqual(responseObject.Message, string.Empty);
            Assert.AreEqual(responseObject.User, null);
        }

        [Test]
        public void ResetPasswordEmptyRequest()
        {
            var response = _controller.ResetPassword(null);
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JsonConvert.DeserializeObject<AuthenticationResult>(responseBody);
            Assert.That(responseObject.Message, Is.EqualTo("No credentials received, please try again."));
        }

        [Test]
        public void ResetPasswordSuccessful()
        {
            HttpResponseMessage response = null;

            Assert.DoesNotThrow(() => response = _controller.ResetPassword(JObject.FromObject(new ResetPasswordRequestRpc
            {
                NewPassword = "password",
                OldPassword = "3412asdf;kjadfasdf"
            })));

            Assert.NotNull(response);

            string responseBody = response.Content.ReadAsStringAsync().Result;
            var responseObject = JsonConvert.DeserializeObject<AuthenticationResult>(responseBody);

            Assert.NotNull(responseBody);
            Assert.IsTrue(responseObject.IsAuthenticated);
            Assert.AreEqual(responseObject.Message, string.Empty);
            Assert.AreEqual(responseObject.User, null);
        }
    }
}