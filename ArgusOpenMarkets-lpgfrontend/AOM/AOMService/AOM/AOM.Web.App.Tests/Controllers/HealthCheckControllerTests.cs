﻿using System.Threading.Tasks;

namespace AOM.Web.App.Tests.Controllers
{
    using AOM.Transport.Events.HealthCheck;
    using AOM.Web.App.Controllers;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    using System;
    using System.Collections.Generic;
    using System.Net.Http;

    [TestFixture]
    public class HealthCheckControllerTests
    {
        private HealthcheckController _controller;

        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();

            var processorHealthCheckStatusList = new List<ProcessorHealthCheckStatus>
            {
                new ProcessorHealthCheckStatus
                {
                    MachineName
                        =
                        "CZC",
                    Processor
                        =
                        "ARM Processor",
                    ProcessorFullName
                        =
                        "Archimedes Research Machine Processor",
                    ResponseTime
                        =
                        DateTime
                            .Now,
                    Status
                        =
                        true
                }
            };

            _mockBus.Setup(
                    m => m.Request<AomProcessorStatusRpc, AomProcessorStatusResult>(It.IsAny<AomProcessorStatusRpc>()))
                .Returns(new AomProcessorStatusResult {ListProcessorHealthChecks = processorHealthCheckStatusList});

            _controller = new HealthcheckController(_mockBus.Object) {Request = new HttpRequestMessage()};
            ControllerHelper.SetupHttpContext();
        }

        [Test]
        public async Task HealthCheckControllerShouldReturnAListOfProcessorHealthCheckStatus()
        {
            var response = _controller.AomProcessorStatus();
            var content = await response.Content.ReadAsStringAsync();
            Assert.That(content.Contains("Archimedes Research Machine Processor"));
        }
    }
}