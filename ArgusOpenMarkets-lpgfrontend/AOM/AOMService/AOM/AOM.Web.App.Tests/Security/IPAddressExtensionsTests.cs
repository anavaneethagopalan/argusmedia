﻿using System.Net;
using AOM.Web.App.Security;
using NUnit.Framework;

namespace AOM.Web.App.Tests.Security
{
    [TestFixture]
    public class IPAddressExtensionsTests
    {
        [Test]
        public void IsSameSubnetShouldReturnTrueWhenInSameSubnet()
        {
            IPAddress ipAddressOne = IPAddress.Parse("192.168.1.14");
            IPAddress ipAddressTwo = IPAddress.Parse("192.168.1.128");
            IPAddress subNet = IPAddress.Parse("255.255.255.0");

            var sameSubnet = IpAddressExtensions.IsInSameSubnet(ipAddressOne, ipAddressTwo, subNet);

            Assert.That(sameSubnet, Is.True);
        }

        [Test]
        public void IsSameSubnetShouldReturnFalseWhenInDifferentSubnet()
        {
            IPAddress ipAddressOne = IPAddress.Parse("192.168.1.14");
            IPAddress ipAddressTwo = IPAddress.Parse("192.168.1.130");
            IPAddress subNet = IPAddress.Parse("255.255.255.128");

            var sameSubnet = IpAddressExtensions.IsInSameSubnet(ipAddressOne, ipAddressTwo, subNet);

            Assert.That(sameSubnet, Is.False);
        }
    }
}
