﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Transport.Service.MarketTickerProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using AOM.Transport.Events.ScheduledTasks;
using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.MarketTickerProcessor.Tests.Consumers
{
    internal class MockScheduledTask
    {
        public string StringVar { get; set; }

    };

    [TestFixture]
    internal class MtScheduledTaskConsumerTests
    {
        private IAomModel _mockAomModel;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockAomModel = new MockAomModel();
            _mockBus = new Mock<IBus>();
        }

        public void SetupSingleSchedule()
        {
            _mockDateTimeProvider.SetupGet(x => x.UtcNow).Returns(new DateTime(2014, 1, 7));
            _mockAomModel.ScheduledTasks.Add(
                new ScheduledTaskDto()
                {
                    Arguments = "{\"StringVar\":\"TestString\"}",
                    DateCreated = new DateTime(2015, 1, 1),
                    Id = 1,
                    Interval = "D",
                    LastRun = null,
                    NextRun = new DateTime(1974, 1, 1, 0, 0, 0),
                    TaskName = "Test Market Ticker Purge",
                    TaskType = "PurgeMarketTickerRequest",
                    SkipIfMissed = false
                });
        }

        private CreateMTScheduledTaskRequestRpc GetScheduledTask()
        {
            return new CreateMTScheduledTaskRequestRpc
            {
                Task = new ScheduledTask()
                {
                    Arguments = new Dictionary<string, string>
                    {
                        {"StringVar", "TestString"}
                    },
                    //Arguments = "{\"StringVar\":\"TestString\"}",
                    DateCreated = new DateTime(2014, 1, 1),
                    Id = 1,
                    Interval = TaskRepeatInterval.Day,
                    LastRun = null,
                    NextRun = new DateTime(1974, 1, 1, 0, 0, 0),
                    TaskName = "Test Market Info Ticker Purge",
                    TaskType = "CreateMTScheduledTaskRequestRpc",
                    SkipIfMissed = false
                }
            };
        }

//        [Test]
//        public void ShouldAddNewScheduledTaskWhenConsumingCreateNewScheduledTaskRequestRpc()
//        {
//            SetupSingleSchedule();
//            Assert.That(_mockAomModel.ScheduledTasks.Count(), Is.EqualTo(1));
//
//            var consumer = new MtScheduledTaskConsumer(
//                _mockBus.Object,
//                _mockDateTimeProvider.Object,
//                new MockDbContextFactory(_mockAomModel, null));
//            var responseFn =
//                StartAndGetRpcResponseFunction<CreateMTScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(consumer, _mockBus);
//            var request = new CreateMTScheduledTaskRequestRpc
//            {
//                Task = new ScheduledTask
//                {
//                    Id = 1234
//                }
//            };
//            var response = responseFn(request);
//            StringAssert.Contains("Successfully", response.Message);
//            Assert.IsTrue(response.Success);
//            Assert.That(_mockAomModel.ScheduledTasks.Count(), Is.EqualTo(2));
//            var taskInDb = _mockAomModel.ScheduledTasks.Last();
//            Assert.That(taskInDb.Id, Is.EqualTo(request.Task.Id));
//        }

        [Test]
        public void ShouldDeleteScheduledTaskWhenConsumingDeleteScheduledTaskRequestRpc()
        {
            SetupSingleSchedule();
            Assert.That(_mockAomModel.ScheduledTasks.Count(), Is.EqualTo(1));

            var consumer = new MtScheduledTaskConsumer(
                _mockBus.Object,
                _mockDateTimeProvider.Object,
                new MockDbContextFactory(_mockAomModel, null));

            //var createRequestFn = 
            //    StartAndGetRpcResponseFunction<CreateMTScheduledTaskRequestRpc, string>(consumer, _mockBus);
            //var createRequest = new CreateMTScheduledTaskRequestRpc
            //{
            //    Task = new ScheduledTask
            //    {
            //        Id = 1234
            //    }
            //};
            //var createResponse = createRequestFn(createRequest);

            var responseFn =
                StartAndGetRpcResponseFunction<DeleteMTScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(consumer, _mockBus);
            var request = new DeleteMTScheduledTaskRequestRpc
            {
                TaskId = _mockAomModel.ScheduledTasks.First().Id
            };

            //consumer.Start();

            var response = responseFn(request);

            StringAssert.Contains("Successfully", response.Message);
            Assert.IsTrue(response.Success);
            Assert.That(_mockAomModel.ScheduledTasks.Count(t => t.Id == request.TaskId), Is.EqualTo(0));
        }


        [Test]
        public void ShouldFailToDeleteScheduledTaskWhenConsumingDeleteScheduledTaskRequestRpcWithInvalidTaskId()
        {
            SetupSingleSchedule();
            Assert.That(_mockAomModel.ScheduledTasks.Count(), Is.EqualTo(1));

            var consumer = new MtScheduledTaskConsumer(
                _mockBus.Object,
                _mockDateTimeProvider.Object,
                new MockDbContextFactory(_mockAomModel, null));

            var responseFn =
                StartAndGetRpcResponseFunction<DeleteMTScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(consumer, _mockBus);
            var request = new DeleteMTScheduledTaskRequestRpc
            {
                TaskId = _mockAomModel.ScheduledTasks.First().Id + 1
            };

            var tasksBeforeDeleteRequest = _mockAomModel.ScheduledTasks.Count();
            //consumer.Start();

            var response = responseFn(request);

            StringAssert.Contains("Failed", response.Message);
            Assert.IsFalse(response.Success);
            Assert.That(_mockAomModel.ScheduledTasks.Count(), Is.EqualTo(tasksBeforeDeleteRequest));
            Assert.That(_mockAomModel.ScheduledTasks.Count(t => t.Id == request.TaskId), Is.EqualTo(0));
        }

        private static Func<TRpcRequest, TResponse> StartAndGetRpcResponseFunction<TRpcRequest, TResponse>(
            IConsumer consumer,
            Mock<IBus> mockBus)
            where TRpcRequest : class
            where TResponse : class
        {
            Func<TRpcRequest, TResponse> responseFunc = null;
            mockBus.Setup(b => b.Respond(It.IsAny<Func<TRpcRequest, TResponse>>()))
                .Callback((Func<TRpcRequest, TResponse> fn) => responseFunc = fn);
            consumer.Start();
            Assert.IsNotNull(responseFunc);
            return responseFunc;
        }
    }
}