﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Service.MarketTickerProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.MarketTickerProcessor.Tests.Consumers
{
    [TestFixture]
    public class MarketTickerContinuousScrollRequestConsumerTests
    {
        private IAomModel _mockAomModel;
        private DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        [TestFixtureSetUp]
        public void Setup()
        {
            OrderDto testOrderDto1, testOrderDto2;
            DealDto testDealDto;

            MarketTickerItemDto firstMarketTickerItemDto,
                secondMarketTickerItemDto,
                thirdMarketTickerItemDto,
                fourthMarketTickerItemDto,
                fifthMarketTickerItemDto,
                sixthMarketTickerItemDto,
                seventhMarketTickerItemDto;

            _mockAomModel = new MockAomModel();

            //ARRANGE
            AomDataBuilder.WithModel(_mockAomModel)
                .AddDeliveryLocation("Farringdon")
                .AddProduct("Argus Naphtha CIF NWE - Outright")
                .AddProduct("Outright")
                .AddProduct("Naphtha Outright")
                .AddProduct("Argus CIF Outright")
                .AddTenorCode("Tenor1")
                .AddProductTenor("Product1_Tenor1")
                .AddOrder(111, "Product1_Tenor1", "A", out testOrderDto1)
                .AddOrder(111, "Product1_Tenor1", "A", out testOrderDto2)
                .AddDeal(111, testOrderDto1.Id, testOrderDto2.Id, out testDealDto, "P")
                .AddMarketTickerItem(
                    555,
                    1,
                    new DateTime(2014, 08, 10, 13, 00, 00),
                    new DateTime(2014, 08, 10, 14, 30, 00),
                    "Argus Naphtha CIF NWE - Outright",
                    out firstMarketTickerItemDto,
                    "A")
                .AddMarketTickerItem(
                    567,
                    2,
                    new DateTime(2014, 08, 10, 13, 00, 00),
                    new DateTime(2014, 08, 10, 14, 33, 00),
                    "Argus CIF Outright",
                    out secondMarketTickerItemDto)
                .AddMarketTickerItem(
                    444,
                    3,
                    new DateTime(2014, 08, 10, 13, 00, 00),
                    new DateTime(2014, 08, 10, 14, 31, 00),
                    "Naphtha Outright",
                    out thirdMarketTickerItemDto)
                .AddMarketTickerItem(
                    333,
                    4,
                    new DateTime(2014, 08, 10, 13, 00, 00),
                    new DateTime(2014, 08, 10, 14, 10, 00),
                    "Outright",
                    out fourthMarketTickerItemDto,
                    "A")
                .AddMarketTickerItem(
                    222,
                    5,
                    new DateTime(2014, 08, 10, 13, 00, 00),
                    new DateTime(2014, 08, 10, 14, 15, 00),
                    "Argus Naphtha CIF NWE - Outright",
                    out fifthMarketTickerItemDto)
                .AddMarketTickerItem(
                    111,
                    6,
                    new DateTime(2014, 08, 10, 13, 00, 00),
                    new DateTime(2014, 08, 10, 14, 29, 00),
                    "Argus Naphtha CIF NWE - Outright",
                    out sixthMarketTickerItemDto,
                    "A")
                .AddMarketTickerItem(
                    789,
                    7,
                    new DateTime(2014, 08, 10, 13, 00, 00),
                    new DateTime(2014, 08, 10, 14, 20, 00),
                    "Argus Naphtha CIF NWE - Outright",
                    out seventhMarketTickerItemDto,
                    "A")
                .DBContext.SaveChangesAndLog(-1);
        }

        [Test]
        public void ConsumesAomMarketTickerContinuousScrollRequestRpcReturnsResponse()
        {
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var mockAomDb = new Mock<IAomModel>();

            // Arrange

            var request = new AomMarketTickerContinuousScrollRequestRpc
            {
                ClientSessionInfo =
                    new ClientSessionInfo
                    {
                        SessionId =
                            "123",
                        UserId = 1,
                    },
                EarliestMarketTickerItemId = 3,
                NumberOfMarketTickerItemsRequired = 3
            };

            var mockDb = new MockDbContextFactory(_mockAomModel, null);
            var lastMarketTickerItemDateTime = new DateTime(2014, 08, 10, 12, 00, 00);
            var earliestTickerItemId = 3;
            var numberOfTickerItems = 3;

            mockMarketTickerService.Setup(
                    x =>
                        x.ReturnNextMarketTickerItemsFromDb(
                            It.IsAny<IAomModel>(),
                            It.IsAny<long>(),
                            It.IsAny<int>(),
                            It.IsAny<List<long>>(),
                            It.IsAny<bool>(),
                            It.IsAny<List<MarketTickerItemType>>()))
                .Returns(
                    new AomMarketTickerContinuousScrollResponseRpc
                    {
                        MarketTickerItems =
                        (from item in
                            _mockAomModel.MarketTickerItems
                            where
                            item.LastUpdated
                            > lastMarketTickerItemDateTime
                            orderby item.LastUpdated ascending
                            select item.ToEntity(mockAomDb.Object, null)).Take(numberOfTickerItems).ToList(),
                        EarliestTickerItemId = earliestTickerItemId
                    });

            //Act
            var scrollRequestController = new MarketTickerContinuousScrollRequestConsumer(
                mockDb,
                mockMarketTickerService.Object,
                mockBus.Object);

            //Assert
            var response = scrollRequestController.RetrieveNextMarketTickerItems(request);

            Assert.NotNull(response);
            //Assert.IsEmpty(response.ErrorMessage);
            Assert.True(response.MarketTickerItems.Count == numberOfTickerItems);

            // Verify
            mockMarketTickerService.Verify(
                x =>
                    x.ReturnNextMarketTickerItemsFromDb(
                        It.IsAny<IAomModel>(),
                        It.IsAny<long>(),
                        It.IsAny<int>(),
                        It.IsAny<List<long>>(),
                        It.IsAny<bool>(),
                        It.IsAny<List<MarketTickerItemType>>()),
                Times.Once());
        }

        [Test]
        public void ConsumesAomMarketTickerContinuousScrollRequestRpcReturnsResponseWithRealMarketTickerService()
        {
            var mockBus = new Mock<IBus>();
            var mockMarketTickerItemMessageGenerator = new Mock<IMarketTickerItemMessageGenerator>();

            var marketTickerService = new MarketTickerService(mockMarketTickerItemMessageGenerator.Object,
                _dateTimeProvider);

            // Arrange

            var request = new AomMarketTickerContinuousScrollRequestRpc
            {
                ClientSessionInfo =
                    new ClientSessionInfo
                    {
                        SessionId =
                            "123",
                        UserId = 1,
                    },
                EarliestMarketTickerItemId = 3,
                NumberOfMarketTickerItemsRequired = 2,
                ProductIdList = new long[] {-2},
                MarketTickerItemTypes =
                    new List<MarketTickerItemType>
                    {
                        MarketTickerItemType
                            .Info
                    }
            };

            var mockDb = new MockDbContextFactory(_mockAomModel, null);
            var numberOfTickerItems = 2;

            //Act
            var scrollRequestController = new MarketTickerContinuousScrollRequestConsumer(
                mockDb,
                marketTickerService,
                mockBus.Object);

            //Assert
            var response = scrollRequestController.RetrieveNextMarketTickerItems(request);

            Assert.NotNull(response);
            //Assert.IsEmpty(response.ErrorMessage);
            Assert.AreEqual(numberOfTickerItems, response.MarketTickerItems.Count);
            Assert.True(response.MarketTickerItems.All(x => x.ProductIds.Contains(-2)));
            Assert.True(
                response.MarketTickerItems.Count(x => x.MarketTickerItemType == MarketTickerItemType.Info)
                == numberOfTickerItems);
            Assert.True(
                response.MarketTickerItems.Count(x => x.MarketTickerItemStatus == MarketTickerItemStatus.Pending) == 0);
        }
    }
}