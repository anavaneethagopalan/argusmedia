﻿using AOM.App.Domain.Dates;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.ErrorService;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Service.MarketTickerProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;

namespace AOM.Transport.Service.MarketTickerProcessor.Tests.Consumers
{
    [TestFixture]
    public class MarketTickerRestartRecoveryTests
    {
        private IAomModel _mockAomModel;
        private DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        [TestFixtureSetUp]
        public void Setup()
        {
            OrderDto testOrderDto1, testOrderDto2;
            DealDto testDealDto;
            MarketTickerItemDto firstMarketTickerItemDto,
                secondMarketTickerItemDto,
                thirdMarketTickerItemDto,
                fourthMarketTickerItemDto,
                fifthMarketTickerItemDto,
                sixthMarketTickerItemDto,
                seventhMarketTickerItemDto;

            _mockAomModel = new MockAomModel();

            //ARRANGE
            AomDataBuilder.WithModel(_mockAomModel)
                .AddDeliveryLocation("Farringdon")
                .AddProduct("Argus Naphtha CIF NWE - Outright")
                .AddProduct("Outright")
                .AddProduct("Naphtha Outright")
                .AddProduct("Argus CIF Outright")
                .AddTenorCode("Tenor1")
                .AddProductTenor("Product1_Tenor1")
                .AddOrder(111, "Product1_Tenor1", "A", out testOrderDto1)
                .AddOrder(111, "Product1_Tenor1", "A", out testOrderDto2)
                .AddDeal(111, testOrderDto1.Id, testOrderDto2.Id, out testDealDto, "P")
                .AddMarketTickerItem(
                    555,
                    1,
                    _dateTimeProvider.UtcNow.AddDays(-1),
                    _dateTimeProvider.UtcNow.AddDays(-1),
                    "Argus Naphtha CIF NWE - Outright",
                    out firstMarketTickerItemDto,
                    "A")
                .AddMarketTickerItem(
                    567,
                    2,
                    _dateTimeProvider.UtcNow.AddDays(-2),
                    _dateTimeProvider.UtcNow.AddDays(-2),
                    "Argus CIF Outright",
                    out secondMarketTickerItemDto)
                .AddMarketTickerItem(
                    444,
                    3,
                    _dateTimeProvider.UtcNow.AddDays(-3),
                    _dateTimeProvider.UtcNow.AddDays(-3),
                    "Naphtha Outright",
                    out thirdMarketTickerItemDto)
                .AddMarketTickerItem(
                    333,
                    4,
                    _dateTimeProvider.UtcNow.AddDays(-4),
                    _dateTimeProvider.UtcNow.AddDays(-4),
                    "Outright",
                    out fourthMarketTickerItemDto,
                    "V")
                .AddMarketTickerItem(
                    222,
                    5,
                    _dateTimeProvider.UtcNow.AddDays(-4),
                    _dateTimeProvider.UtcNow.AddDays(-4),
                    "Argus Naphtha CIF NWE - Outright",
                    out fifthMarketTickerItemDto)
                .AddMarketTickerItem(
                    111,
                    6,
                    _dateTimeProvider.UtcNow.AddDays(-6),
                    _dateTimeProvider.UtcNow.AddDays(-6),
                    "Argus Naphtha CIF NWE - Outright",
                    out sixthMarketTickerItemDto)
                .AddMarketTickerItem(
                    789,
                    7,
                    _dateTimeProvider.UtcNow.AddDays(-7),
                    _dateTimeProvider.UtcNow.AddDays(-7),
                    "Argus Naphtha CIF NWE - Outright",
                    out seventhMarketTickerItemDto)
                .DBContext.SaveChangesAndLog(-1);
        }

        [Test]
        public void RecoveryPublishesAllItemsIntheDatabaseWithinTheSpecifiedPeriod()
        {
            var mockBus = new Mock<IBus>();

            // Arrange
            var mockDb = new MockDbContextFactory(_mockAomModel, null);
            var mockMarketTickerItemMessageGenerator = new Mock<IMarketTickerItemMessageGenerator>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var marketTickerRefreshConsumer = new MarketTickerRefreshConsumer(
                mockDb,
                new MarketTickerService(mockMarketTickerItemMessageGenerator.Object, _dateTimeProvider),
                mockBus.Object, mockErrorService.Object);
            marketTickerRefreshConsumer.Start();

            marketTickerRefreshConsumer.ConsumeAomRefreshMarketTickerRequest(
                new AomRefreshMarketTickerRequest
                {
                    RefreshFromTime = DateTime.Today.AddDays(-5),
                    RefreshToTime = DateTime.Today.AddDays(1)
                });

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Exactly(5));
        }
    }
}