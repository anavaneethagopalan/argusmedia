﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Events.Orders;
using AOM.Transport.Service.MarketTickerProcessor.Consumers;
using Argus.Transport.Infrastructure;
using log4net.Core;
using Moq;
using NUnit.Framework;
using Utils.Logging;

namespace AOM.Transport.Service.MarketTickerProcessor.Tests.Consumers
{
    [TestFixture]
    public class MarketTickerOrderResponseConsumerTests
    {        
        [Test]
        public void OnStartSubscribesProcessorToBusAndFiltersToJustTheMarketOpenTopic()
        {
            var configuredTopics = new List<string>();

            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IMarketTickerService>();

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, mockBus.Object);
            orderConsumer.Start();
            
            //Assert            
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomOrderResponse>>()
                , It.Is<Action<ISubscriptionConfiguration>>(p => CaptureTopicsCreated(p, configuredTopics))), Times.Once);

            Assert.IsNotNull(configuredTopics);
            
            CollectionAssert.Contains(configuredTopics, "Market.Open", "Subscription should be restricted to open market.");
            Assert.AreEqual(1, configuredTopics.Count, "only one topic subscription expected");
        }
       
        // Consume Messages put reponse on bus
        [Test]
        public void ConsumeKillOrderResponsePutsMarketTickResponseOnBus()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Kill, lastUpdatedTime);
            var killOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            killOrderResponse.Order = testOrder;
            killOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(killOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomNewMarketTickResponse>(r => (r.MarketTickerItem.OrderId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))), Times.Once);
        }

        [Test]
        public void ConsumeKillVirtualOrderResponseDoesNotPutMarketTickResponseOnBus()
        {
            using (var testLog = new MemoryAppenderForTests())
            {
                //Arrange
                var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
                var mockBus = new Mock<IBus>();
                var mockMarketTickerService = new Mock<IMarketTickerService>();
                var testMessage = CreateOrderTestMessage(MessageAction.Kill, lastUpdatedTime);
                var killOrderResponse = new AomOrderResponse();

                var testOrder = new Order
                {
                    Id = 1,
                    LastUpdated = lastUpdatedTime,
                    IsVirtual = true
                };

                killOrderResponse.Order = testOrder;
                killOrderResponse.Message = testMessage;

                mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

                //Act
                var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
                orderConsumer.ConsumeAomOrderResponse(killOrderResponse);

                //Assert publish is NEVER called
                mockBus.Verify(x => x.Publish(It.Is<AomNewMarketTickResponse>(r => (r.MarketTickerItem.OrderId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))), Times.Never);

                testLog.AssertThatThereAreNoLogErrors();
            }
        }

        [Test]
        public void ConsumeHoldOrderResponsePutsMarketTickResponseOnBus()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Hold, lastUpdatedTime);
            var holdOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            holdOrderResponse.Order = testOrder;
            holdOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(holdOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomNewMarketTickResponse>(r => (r.MarketTickerItem.OrderId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))), Times.Once);
        }

        [Test]
        public void ConsumeExecuteOrderResponsePutsMarketTickResponseOnBus()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Execute, lastUpdatedTime);
            var executeOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            executeOrderResponse.Order = testOrder;
            executeOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(
                    x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage))
                .Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(),
                mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(executeOrderResponse);
        }

        private Order MakeFakeOrder(DateTime? lastUpdatedTime)
        {
            var order = new Order
            {
                Id = 1,
            };

            if (lastUpdatedTime.HasValue)
            {
                order.LastUpdated = lastUpdatedTime.Value;
            }

            return order;
        }

        [Test]
        public void ConsumeEditOrderResponsePutsMarketTickResponseOnBus()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Update, lastUpdatedTime);
            var editOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            editOrderResponse.Order = testOrder;
            editOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(editOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomNewMarketTickResponse>(r => (r.MarketTickerItem.OrderId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))), Times.Once);
        }

        [Test]
        public void ConsumeReinstateOrderResponsePutsMarketTickResponseOnBus()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Reinstate, lastUpdatedTime);
            var reinstateOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            reinstateOrderResponse.Order = testOrder;
            reinstateOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(reinstateOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomNewMarketTickResponse>(r => (r.MarketTickerItem.OrderId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))), Times.Once);
        }

        [Test]
        public void ConsumeNewOrderResponsePutsMarketTickResponseOnBus()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Create, lastUpdatedTime);
            var newOrderResponse = new AomOrderResponse();
            var testOrder = MakeFakeOrder(lastUpdatedTime);

            newOrderResponse.Order = testOrder;
            newOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(newOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomNewMarketTickResponse>(r => (r.MarketTickerItem.OrderId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))), Times.Once);
        }

        // EmptyMessageBody does not get published

        [Test]
        public void ConsumeKillOrderResponseDoesNotPutsMarketTickResponseOnBusWhenMessageBodyIsMissing()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessageWithNullMessageBody(MessageAction.Kill);
            var killOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(null);

            killOrderResponse.Order = testOrder;
            killOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(killOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeHoldOrderResponseDoesNotPutsMarketTickResponseOnBusWhenMessageBodyIsMissing()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessageWithNullMessageBody(MessageAction.Kill);
            var holdOrderResponse = new AomOrderResponse();
            var testOrder = MakeFakeOrder(null);

            holdOrderResponse.Order = testOrder;
            holdOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(holdOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeExecuteOrderResponseDoesNotPutsMarketTickResponseOnBusWhenMessageBodyIsMissing()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessageWithNullMessageBody(MessageAction.Kill);
            var executeOrderResponse = new AomOrderResponse();
            var testOrder = MakeFakeOrder(null);

            executeOrderResponse.Order = testOrder;
            executeOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(executeOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeEditOrderResponseDoesNotPutsMarketTickResponseOnBusWhenMessageBodyIsMissing()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessageWithNullMessageBody(MessageAction.Kill);
            var editOrderResponse = new AomOrderResponse();
            var testOrder = MakeFakeOrder(null);

            editOrderResponse.Order = testOrder;
            editOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(editOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeReinstateOrderResponseDoesNotPutsMarketTickResponseOnBusWhenMessageBodyIsMissing()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessageWithNullMessageBody(MessageAction.Kill);
            var reinstateOrderResponse = new AomOrderResponse();
            var testOrder = MakeFakeOrder(null);

            reinstateOrderResponse.Order = testOrder;
            reinstateOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(reinstateOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeNewOrderResponseDoesNotPutsMarketTickResponseOnBusWhenMessageBodyIsMissing()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessageWithNullMessageBody(MessageAction.Kill);
            var newOrderResponse = new AomOrderResponse();
            var testOrder = MakeFakeOrder(null);

            newOrderResponse.Order = testOrder;
            newOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Returns(testMessage);

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(newOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        // No Market tick published on exception thrown

        [Test]
        public void ConsumeKillOrderResponseDoesNotPutsMarketTickResponseOnBusWhenExceptionOccures()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessageWithNullMessageBody(MessageAction.Kill);
            var killOrderResponse = new AomOrderResponse();
            var testOrder = MakeFakeOrder(null);

            killOrderResponse.Order = testOrder;
            killOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Throws(new Exception("Exception in service"));

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(killOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeHoldOrderResponseDoesNotPutMarketTickResponseOnBusWhenExceptionOccures()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Hold, lastUpdatedTime);
            var holdOrderResponse = new AomOrderResponse();
            var testOrder = MakeFakeOrder(lastUpdatedTime);

            holdOrderResponse.Order = testOrder;
            holdOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Throws(new Exception("Exception in service"));

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(holdOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeExecuteOrderResponseDoesNotPutMarketTickResponseOnBusWhenExceptionOccures()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Execute, lastUpdatedTime);
            var executeOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            executeOrderResponse.Order = testOrder;
            executeOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Throws(new Exception("Exception in service"));

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(executeOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeEditOrderResponseDoesNotPutMarketTickResponseOnBusWhenExceptionOccures()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Update, lastUpdatedTime);
            var editOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            editOrderResponse.Order = testOrder;
            editOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Throws(new Exception("Exception in service"));

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(editOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeReinstateOrderResponseDoesNotPutMarketTickResponseOnBusWhenExceptionOccures()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Reinstate, lastUpdatedTime);
            var reinstateOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            reinstateOrderResponse.Order = testOrder;
            reinstateOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Throws(new Exception("Exception in service"));

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(reinstateOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeNewOrderResponseDoesNotPutMarketTickResponseOnBusWhenExceptionOccures()
        {
            //Arrange
            var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = CreateOrderTestMessage(MessageAction.Create, lastUpdatedTime);
            var newOrderResponse = new AomOrderResponse();

            var testOrder = MakeFakeOrder(lastUpdatedTime);

            newOrderResponse.Order = testOrder;
            newOrderResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateOrderMarketTickerItemMessage(It.IsAny<IAomModel>(), testOrder, testMessage)).Throws(new Exception("Exception in service"));

            //Act
            var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            orderConsumer.ConsumeAomOrderResponse(newOrderResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeAomOrderResponseWithNullOrderLogsErrorAndDoesNotPublishResponse()
        {
            using (var testLog = new MemoryAppenderForTests())
            {
                //Arrange
                var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
                var mockBus = new Mock<IBus>();
                var mockMarketTickerService = new Mock<IMarketTickerService>();
                var testMessage = CreateOrderTestMessage(MessageAction.Kill, lastUpdatedTime);
                var killOrderResponse = new AomOrderResponse {Order = null, Message = testMessage};

                //Act
                var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
                orderConsumer.ConsumeAomOrderResponse(killOrderResponse);

                //Assert publish is NEVER called
                mockBus.Verify(x => x.Publish(It.Is<AomNewMarketTickResponse>(r => (r.MarketTickerItem.OrderId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))), Times.Never);

                testLog.AssertALogMessageContains(Level.Error, "Null order in orderResponse");
            }
        }

        [Test]
        public void ConsumeErrorAomOrderResponseWithNullOrderDoesNotPublishResponse()
        {
            using (var testLog = new MemoryAppenderForTests())
            {
                //Arrange
                var lastUpdatedTime = new DateTime(2014, 08, 14, 12, 10, 06);
                var mockBus = new Mock<IBus>();
                var mockMarketTickerService = new Mock<IMarketTickerService>();
                var testMessage = CreateOrderTestMessage(MessageAction.Kill, lastUpdatedTime);
                testMessage.MessageType = MessageType.Error;
                var killOrderResponse = new AomOrderResponse { Order = null, Message = testMessage };

                //Act
                var orderConsumer = new MarketTickerOrderResponseConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
                orderConsumer.ConsumeAomOrderResponse(killOrderResponse);

                //Assert publish is NEVER called
                mockBus.Verify(x => x.Publish(It.Is<AomNewMarketTickResponse>(r => (r.MarketTickerItem.OrderId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))), Times.Never);

                testLog.AssertThatThereAreNoLogErrors();
            }
        }

        // Helper Methods

        private static Message CreateOrderTestMessage(MessageAction action, DateTime lastUpdatedTime)
        {
            var testMessage = new Message
            {
                ClientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" },
                MessageAction = action,
                MessageType = MessageType.Order,
                MessageBody = new MarketTickerItem { LastUpdated = lastUpdatedTime, OrderId = 1 }
            };

            return testMessage;
        }

        private static Message CreateOrderTestMessageWithNullMessageBody(MessageAction action)
        {
            var testMessage = new Message
            {
                ClientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" },
                MessageAction = action,
                MessageType = MessageType.Order,
                MessageBody = null
            };

            return testMessage;
        }

        private bool CaptureTopicsCreated(Action<ISubscriptionConfiguration> p, List<String> configuredTopics)
        {
            var config = new SubscriptionConfiguration(0);
            p.Invoke(config);
            configuredTopics.AddRange(config.Topics);
            return true;
        }
    }
}
