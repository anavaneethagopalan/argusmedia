﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Service.MarketTickerProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.MarketTickerProcessor.Tests.Consumers
{
    [TestFixture]
    public class MarketTickerDealConsumerTests
    {
        DateTimeProvider _dateTimeProvider = new DateTimeProvider();
        [Test]
        public void OnStartSubscribesProcessorToBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IMarketTickerService>();

            //Act
            var dealConsumer = new MarketTickerDealConsumer(MockDbContextFactory.Stub(), mockOrderService.Object,
                mockBus.Object);
            dealConsumer.Start();

            //Assert
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomDealResponse>>()), Times.Once);
        }

        [Test]
        public void ConsumeDealResponsePutsMarketTickResponseOnBus()
        {
            //Arrange
            var lastUpdatedTime = _dateTimeProvider.UtcNow;
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var messageList = new List<Message>();
            var testMessage = MakeTestMessage(new MarketTickerItem { LastUpdated = lastUpdatedTime, DealId = 1 });

            messageList.Add(testMessage);
            var dealResponse = new AomDealResponse();

            var testDeal = new Deal
            {
                Id = 1,
                DealStatus = DealStatus.Pending,
                InitialOrderId = 1,
                MatchingOrderId = 2,
                LastUpdated = lastUpdatedTime,
            };

            dealResponse.Deal = testDeal;
            dealResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateDealMarketTick(It.IsAny<IAomModel>(), dealResponse))
                .Returns(messageList);

            //Act
            var dealConsumer = new MarketTickerDealConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object, mockBus.Object);
            dealConsumer.ConsumeAomDealResponse(dealResponse);

            //Assert
            mockBus.Verify(
                x =>
                    x.Publish(
                        It.Is<AomNewMarketTickResponse>(
                            r => (r.MarketTickerItem.DealId == 1 && r.MarketTickerItem.LastUpdated == lastUpdatedTime))),
                Times.Once);
        }

        [Test]
        public void ConsumeDealResponseDoesNotPutsMarketTickResponseOnBusWhenMessageBodyIsMissing()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var messageList = new List<Message>();
            var testMessage = MakeTestMessage(null);

            messageList.Add(testMessage);
            var dealResponse = new AomDealResponse();

            var lastUpdatedTime = _dateTimeProvider.UtcNow;
            var testDeal = new Deal
            {
                Id = 1,
                DealStatus = DealStatus.Pending,
                InitialOrderId = 1,
                MatchingOrderId = 2,
                LastUpdated = lastUpdatedTime,
            };

            dealResponse.Deal = testDeal;
            dealResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateDealMarketTick(It.IsAny<IAomModel>(), dealResponse))
                .Returns(messageList);

            //Act
            var dealConsumer = new MarketTickerDealConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object,
                mockBus.Object);
            dealConsumer.ConsumeAomDealResponse(dealResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        [Test]
        public void ConsumeDealResponseDoesNotPutsMarketTickResponseOnBusWhenExceptionOccures()
        {
            //Arrange
            var lastUpdatedTime = _dateTimeProvider.UtcNow;
            var mockBus = new Mock<IBus>();
            var mockMarketTickerService = new Mock<IMarketTickerService>();
            var testMessage = MakeTestMessage(new MarketTickerItem { LastUpdated = lastUpdatedTime, DealId = 1 });

            var dealResponse = new AomDealResponse();

            var testDeal = new Deal
            {
                Id = 1,
                DealStatus = DealStatus.Pending,
                InitialOrderId = 1,
                MatchingOrderId = 2,
                LastUpdated = lastUpdatedTime,
            };

            dealResponse.Deal = testDeal;
            dealResponse.Message = testMessage;

            mockMarketTickerService.Setup(x => x.CreateDealMarketTick(It.IsAny<IAomModel>(), dealResponse))
                .Throws(new Exception("Exception in service"));

            //Act
            var dealConsumer = new MarketTickerDealConsumer(MockDbContextFactory.Stub(), mockMarketTickerService.Object,
                mockBus.Object);
            dealConsumer.ConsumeAomDealResponse(dealResponse);

            //Assert
            mockBus.Verify(x => x.Publish(It.IsAny<AomNewMarketTickResponse>()), Times.Never);
        }

        private Message MakeTestMessage(object messageBody)
        {
            return new Message
            {
                ClientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" },
                MessageAction = MessageAction.Create,
                MessageType = MessageType.MarketTickerItem,
                MessageBody = messageBody
            };
        }
    }
}

