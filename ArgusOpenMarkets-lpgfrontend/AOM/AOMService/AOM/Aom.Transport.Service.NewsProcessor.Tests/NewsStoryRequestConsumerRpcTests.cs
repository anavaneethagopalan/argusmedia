﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.ErrorService;
using AOM.Transport.Events.News;
using AOM.Transport.Service.NewsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace Aom.Transport.Service.NewsProcessor.Tests
{
    [TestFixture]
    public class NewsStoryRequestConsumerRpcTests
    {
        [Test]
        public void GetNewsStoryRequestShouldReturnANewsStory()
        {
            var aomNews = new AomNews {Story = "story"};
            var mockBus = new Mock<IBus>();
            var mockNewsService = new Mock<IAomNewsService>();
            var mockErrorService = new Mock<IErrorService>();
            var newsStoryRequest = new NewsStoryRequestRpc {CmsId = "1", UserId = 1};

            mockNewsService.Setup(m => m.GetNewsStory(It.IsAny<string>())).Returns(aomNews.Story);
            var newsProcessor = new NewsStoryRequestConsumerRpc(mockNewsService.Object, mockBus.Object,
                mockErrorService.Object);
            var newsStory = newsProcessor.GetNewsStoryRequest(newsStoryRequest);

            Assert.That(newsStory, Is.Not.Null);
            Assert.That(newsStory.Story, Is.EqualTo(aomNews.Story));
        }
    }
}