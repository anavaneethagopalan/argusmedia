﻿namespace Aom.Transport.Service.NewsProcessor.Tests
{
    using AOM.App.Domain.Dates;
    using AOM.Transport.Events.News;
    using AOM.Transport.Service.NewsProcessor.Consumers;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class NewsPurgeConsumerTests
    {
        [Test]
        public void ShouldInheritFromScheduledTaskConsumerBase()
        {
            var mockBus = new Mock<IBus>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var newsPurgeConsumer = new NewsPurgeConsumer(mockBus.Object, mockDateTimeProvider.Object);
            var newsInterface = newsPurgeConsumer as ScheduledTaskConsumerBase<PurgeNewsRequest>;

            Assert.That(newsInterface, Is.Not.Null);
        }
    }
}
