﻿namespace Aom.Transport.Service.NewsProcessor.Tests
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.News;
    using AOM.Transport.Service.NewsProcessor.Consumers;

    using Argus.Transport.Infrastructure;
    using Argus.Transport.Messages.Dtos;
    using Argus.Transport.Messages.Events;

    using Moq;

    using NUnit.Framework;

    using System;

    [TestFixture]
    public class ArgusNewsPublishedConsumerTests
    {
        private Mock<IBus> _mockAomBus;

        private Mock<IBus> _mockArgusBus;

        private Mock<IAomNewsService> _mockNewsService;

        private ArgusNewsPublishedConsumer _argusNewsConsumer;

        [SetUp]
        public void Setup()
        {
            _mockAomBus = new Mock<IBus>();
            _mockArgusBus = new Mock<IBus>();
            _mockNewsService = new Mock<IAomNewsService>();
            _argusNewsConsumer = new ArgusNewsPublishedConsumer(
                _mockAomBus.Object,
                _mockArgusBus.Object,
                _mockNewsService.Object);
        }

        [Test]
        public void ShouldSubscribeToArgusBusAtStart()
        {
            // Act
            _argusNewsConsumer.Start();

            // Assert
            _mockArgusBus.Verify(
                b => b.Subscribe(_argusNewsConsumer.SubscriptionId, It.IsAny<Action<NewsPersisted>>()),
                Times.Once);
            _mockArgusBus.Verify(
                b => b.Subscribe(_argusNewsConsumer.SubscriptionId, It.IsAny<Action<NewsPersistedRefresh>>()),
                Times.Once);
        }

        [Test]
        public void ShoulSayThatItRequiresTheArgusBus()
        {
            Assert.That(_argusNewsConsumer.RequiresArgusBus, Is.True);
        }

        [Test]
        public void ShouldPublishNewsToAomBusIfAnAomNewsArticleIsReceived()
        {
            // Arrange
            var subs = StartConsumer();
            var onArgusNewsPublished = subs.Publish;
            _mockNewsService.Setup(s => s.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(true);
            var message = new NewsPersisted();

            // Act
            onArgusNewsPublished(message);

            // Assert
            _mockAomBus.Verify(b => b.Publish(It.IsAny<AomNewsPublished>()), Times.Once);
        }

        [Test]
        public void ShouldNotPublishNewsToAomBusIfANewsArticleNotForAomIsReceived()
        {
            // Arrange
            var subs = StartConsumer();
            var onArgusNewsPublished = subs.Publish;
            _mockNewsService.Setup(s => s.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(false);
            var message = new NewsPersisted();

            // Act
            onArgusNewsPublished(message);

            // Assert
            _mockAomBus.Verify(b => b.Publish(It.IsAny<AomNewsPublished>()), Times.Never);
        }

        [Test]
        public void ShouldPublishNewsToAomBusIfNewsIsReplayed()
        {
            // Arrange
            var subs = StartConsumer();
            var onArgusNewsReplayed = subs.Replay;
            var message = new NewsPersistedRefresh();

            // Act
            onArgusNewsReplayed(message);

            // Assert
            _mockAomBus.Verify(b => b.Publish(It.IsAny<AomNewsPublished>()), Times.Once);
        }

        [Test]
        public void ShouldNotPublishNewsToAomBusIfNullNewsRefreshIsReceived()
        {
            // Arrange
            var subs = StartConsumer();
            var onArgusNewsReplayed = subs.Replay;

            // Act
            onArgusNewsReplayed(null);

            // Assert
            _mockAomBus.Verify(b => b.Publish(It.IsAny<AomNewsPublished>()), Times.Never);
        }

        [Test]
        public void ShouldPublishCorrectTransformedNews()
        {
            const string fakeCmsId = "IAmACmsIdHonest";

            // Arrange
            var subs = StartConsumer();
            var onArgusNewsPublished = subs.Publish;
            _mockNewsService.Setup(s => s.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(true);
            _mockNewsService.Setup(s => s.TransformNews(It.IsAny<PersistedArticle>()))
                .Returns<PersistedArticle>(a => new AomNews { CmsId = fakeCmsId });
            var message = new NewsPersisted();

            // Act
            onArgusNewsPublished(message);

            // Assert
            _mockAomBus.Verify(b => b.Publish(It.Is<AomNewsPublished>(n => n.News.CmsId == fakeCmsId)));
        }


        [Test]
        public void
            ShouldPublishAomNewsDeletedEventWhenArgusPersistedNewsDeletedEventRaisedAndTheArgusNewsIsFreeAndHasNoAomContentStreams
            ()
        {
            // Arrange
            var subs = StartConsumer();
            var onArgusNewsDeleted = subs.NewsDeleted;
            _mockNewsService.Setup(s => s.GetArticleContentStreams(It.IsAny<PersistedMetaTypeDatum[]>()))
                .Returns<PersistedMetaTypeDatum[]>(null);

            _mockNewsService.Setup(s => s.TransformNews(It.IsAny<PersistedArticle>()))
                .Returns<PersistedArticle>(a => new AomNews { CmsId = "1" });

            var argusNewsDeleted = new PersistedArticle { DomainId = 1, ContentStreams = null, IsFree = true, CmsId = "1" };
            var message = new PersistedNewsDeleted { News = argusNewsDeleted };

            // Act
            onArgusNewsDeleted(message);

            // Assert
            _mockAomBus.Verify(d => d.Publish(It.Is<AomNewsDeleted>(n => n.CmsId == "1")));
        }

        [Test]
        public void ShouldPublishAomNewsDeletedEventWhenArgusPersistedNewsDeletedEventRaisedAndTheArgusNewsIsFree()
        {
            // Arrange
            var subs = StartConsumer();
            var onArgusNewsDeleted = subs.NewsDeleted;
            _mockNewsService.Setup(s => s.GetArticleContentStreams(It.IsAny<PersistedMetaTypeDatum[]>()))
                .Returns<long[]>(null);

            _mockNewsService.Setup(m => m.TransformNews(It.IsAny<PersistedArticle>())).Returns(new AomNews
            {
                CmsId = "1", 
                Headline = ""
            });

            var argusNewsDeleted = new PersistedArticle { DomainId = 1, ContentStreams = null, IsFree = true, CmsId = "1" };
            var message = new PersistedNewsDeleted { News = argusNewsDeleted };

            // Act
            onArgusNewsDeleted(message);

            // Assert
            _mockAomBus.Verify(d => d.Publish(It.Is<AomNewsDeleted>(n => n.CmsId == "1")));
        }

        [Test]
        public void
            ShouldPublishAomNewsDeletedEventWhenArgusPersistedNewsDeletedEventRaisedAndTheArgusNewsIsNotFreeButHasAomContentStreams
            ()
        {
            // Arrange
            var subs = StartConsumer();
            var onArgusNewsDeleted = subs.NewsDeleted;
            _mockNewsService.Setup(s => s.GetArticleContentStreams(It.IsAny<PersistedMetaTypeDatum[]>()))
                .Returns<long[]>(getCSResult);


            _mockNewsService.Setup(m => m.TransformNews(It.IsAny<PersistedArticle>())).Returns(new AomNews
            {
                CmsId = "1",
                Headline = ""
            });

            var argusNewsDeleted = new PersistedArticle { DomainId = 1, ContentStreams = null, IsFree = false, CmsId = "1" };
            var message = new PersistedNewsDeleted { News = argusNewsDeleted };

            // Act
            onArgusNewsDeleted(message);

            // Assert
            _mockAomBus.Verify(d => d.Publish(It.Is<AomNewsDeleted>(n => n.CmsId == "1")));
        }

        [Test]
        public void ShouldPublishAomNewsUpdatedEventWhenNewsUpdatePersistedEventIsRaisedFromArgus()
        {
            // Arrange
            var subs = StartConsumer();
            var onArgusNewsUpdated = subs.NewsUpdated;
            _mockNewsService.Setup(s => s.GetArticleContentStreams(It.IsAny<PersistedMetaTypeDatum[]>()))
                .Returns<long[]>(getCSResult);

            var newsUpdated = new PersistedArticle { DomainId = 1, ContentStreams = null, IsFree = true };
            var argusNewsUpdated = new NewsUpdatePersisted { News = newsUpdated, PreviousVersion = newsUpdated };

            var aomNews = new AomNews { CmsId = "1" };

            _mockNewsService.Setup(m => m.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(true);
            _mockNewsService.Setup(m => m.TransformNews(It.IsAny<PersistedArticle>())).Returns(aomNews);


            // Act
            onArgusNewsUpdated(argusNewsUpdated);

            // Assert
            _mockAomBus.Verify(d => d.Publish(It.IsAny<AomNewsUpdated>()), Times.Exactly(1));
        }

        private long[] getCSResult(long[] arg)
        {
            var ids = new long[] { 10 };
            return ids;
        }

        private class SubscribedActions
        {
            public Action<NewsPersisted> Publish { get; set; }

            public Action<NewsPersistedRefresh> Replay { get; set; }

            public Action<PersistedNewsDeleted> NewsDeleted { get; set; }

            public Action<NewsUpdatePersisted> NewsUpdated { get; set; }
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();
            _mockArgusBus.Setup(b => b.Subscribe(_argusNewsConsumer.SubscriptionId, It.IsAny<Action<NewsPersisted>>()))
                .Callback<string, Action<NewsPersisted>>((s, a) => subs.Publish = a);

            _mockArgusBus.Setup(
                b => b.Subscribe(_argusNewsConsumer.SubscriptionId, It.IsAny<Action<NewsPersistedRefresh>>()))
                .Callback<string, Action<NewsPersistedRefresh>>((s, a) => subs.Replay = a);

            _mockArgusBus.Setup(
                b => b.Subscribe(_argusNewsConsumer.SubscriptionId, It.IsAny<Action<PersistedNewsDeleted>>()))
                .Callback<string, Action<PersistedNewsDeleted>>((s, a) => subs.NewsDeleted = a);

            _mockArgusBus.Setup(
                b => b.Subscribe(_argusNewsConsumer.SubscriptionId, It.IsAny<Action<NewsUpdatePersisted>>()))
                .Callback<string, Action<NewsUpdatePersisted>>((s, a) => subs.NewsUpdated = a);

            _argusNewsConsumer.Start();
            Assert.That(subs.Publish, Is.Not.Null);
            Assert.That(subs.Replay, Is.Not.Null);
            return subs;
        }
    }
}