﻿using AOM.App.Domain.Entities;
using Argus.Transport.Messages.Dtos;
using Argus.Transport.Messages.Events;
using System;
using AOM.App.Domain.Services;
using AOM.Transport.Service.NewsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace Aom.Transport.Service.NewsProcessor.Tests.Consumers
{
    [TestFixture]
    public class ArgusNewsPublishedConsumerTests
    {
        private ArgusNewsPublishedConsumer _argusNewsPublishedConsumer;

        private Mock<IBus> _mockBus;

        private Mock<IBus> _mockArgusBus;

        private Mock<IAomNewsService> _mockAomNewsService;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockArgusBus = new Mock<IBus>();
            _mockAomNewsService = new Mock<IAomNewsService>();

            _argusNewsPublishedConsumer = new ArgusNewsPublishedConsumer(_mockBus.Object, _mockArgusBus.Object,
                _mockAomNewsService.Object);
        }

        [Test]
        public void ShouldReturnTrueForRequiresArgusBus()
        {
            Assert.That(_argusNewsPublishedConsumer.RequiresArgusBus, Is.True);
        }

        [Test]
        public void SubscriptionIdShouldBeSetToAom()
        {
            Assert.That(_argusNewsPublishedConsumer.SubscriptionId, Is.EqualTo("AOM"));
        }

        [Test]
        public void OnArgusNewsPersistedShouldCallArticleServiceToPersistNewsIntoAom()
        {
            var subs = StartConsumer();

            var newsPersisted = new NewsPersisted {News = MakePersistedArticle()};
            _mockAomNewsService.Setup(m => m.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(true);

            var aomNews = new AomNews
            {
                CmsId = "1",
                IsFree = true,
                Story = "A long time ago in a galaxy far far away..."
            };
            _mockAomNewsService.Setup(m => m.TransformNews(It.IsAny<PersistedArticle>())).Returns(aomNews);
            var onNewsPersisted = subs.NewsPersisted;
            onNewsPersisted(newsPersisted);

            _mockAomNewsService.Verify(m => m.CreateNews(aomNews), Times.Exactly(1));
        }

        [Test]
        public void OnArgusNewsPersistedRefreshShouldCallArticleServiceToPersistNewsIntoAom()
        {
            var subs = StartConsumer();

            var newsPersistedRefresh = new NewsPersistedRefresh {News = MakePersistedArticle()};

            _mockAomNewsService.Setup(m => m.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(true);

            var aomNews = new AomNews
            {
                CmsId = "1",
                IsFree = true,
                Story = "A long time ago in a galaxy far far away..."
            };
            _mockAomNewsService.Setup(m => m.TransformNews(It.IsAny<PersistedArticle>())).Returns(aomNews);
            var onNewsPersistedRefresh = subs.NewsPersistedRefresh;
            onNewsPersistedRefresh(newsPersistedRefresh);

            _mockAomNewsService.Verify(m => m.CreateNews(aomNews), Times.Exactly(1));
        }

        [Test]
        public void OnArgusNewsUpdatePersistedShouldCallArticleServiceToDeleteTheOldNews()
        {
            var subs = StartConsumer();

            var newsUpdatePersisted = new NewsUpdatePersisted
            {
                News = MakePersistedArticle(),
                PreviousVersion = MakePersistedArticle("2")
            };

            _mockAomNewsService.Setup(m => m.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(true);

            var aomNews = new AomNews
            {
                CmsId = "1",
                IsFree = true,
                Story = "A long time ago in a galaxy far far away..."
            };
            _mockAomNewsService.Setup(m => m.TransformNews(It.IsAny<PersistedArticle>())).Returns(aomNews);
            var onNewsUpdatePersisted = subs.NewsUpdatePersisted;
            onNewsUpdatePersisted(newsUpdatePersisted);

            _mockAomNewsService.Verify(m => m.UpdateNews(aomNews), Times.Exactly(0));
            _mockAomNewsService.Verify(m => m.DeleteNews(It.IsAny<string>()), Times.Exactly(1));
        }

        [Test]
        public void OnArgusNewsUpdatePersistedShouldCallArticleServiceToCreateTheNewNews()
        {
            var subs = StartConsumer();

            var newsUpdatePersisted = new NewsUpdatePersisted
            {
                News = MakePersistedArticle(),
                PreviousVersion = MakePersistedArticle("2")
            };

            _mockAomNewsService.Setup(m => m.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(true);

            var aomNews = new AomNews
            {
                CmsId = "1",
                IsFree = true,
                Story = "A long time ago in a galaxy far far away..."
            };
            _mockAomNewsService.Setup(m => m.TransformNews(It.IsAny<PersistedArticle>())).Returns(aomNews);
            var onNewsUpdatePersisted = subs.NewsUpdatePersisted;
            onNewsUpdatePersisted(newsUpdatePersisted);

            _mockAomNewsService.Verify(m => m.UpdateNews(aomNews), Times.Exactly(0));
            _mockAomNewsService.Verify(m => m.CreateNews(It.IsAny<AomNews>()), Times.Exactly(1));
        }

        [Test]
        public void OnArgusNewsPersistedDeletedShouldNotCallArticleServiceToDeleteNewsInAom()
        {
            var subs = StartConsumer();

            var persistedNewsDeleted = new PersistedNewsDeleted {News = MakePersistedArticle()};

            _mockAomNewsService.Setup(m => m.IsAomNewsArticle(It.IsAny<PersistedArticle>())).Returns(true);

            var aomNews = new AomNews
            {
                CmsId = "1",
                IsFree = true,
                Story = "A long time ago in a galaxy far far away..."
            };
            _mockAomNewsService.Setup(m => m.TransformNews(It.IsAny<PersistedArticle>())).Returns(aomNews);
            var onNewsDeleted = subs.PersistedNewsDeleted;
            onNewsDeleted(persistedNewsDeleted);

            _mockAomNewsService.Verify(m => m.DeleteNews(It.IsAny<string>()), Times.Exactly(0));
        }

        private PersistedArticle MakePersistedArticle(string cmsId = "1")
        {
            var persistedArticle = new PersistedArticle()
            {
                CmsId = cmsId,
                Story = "Once upon a time",
                Headline = "Headline",
                IsFree = true
            };

            return persistedArticle;
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _mockArgusBus.Setup(b => b.Subscribe("AOM", It.IsAny<Action<NewsPersisted>>()))
                .Callback<string, Action<NewsPersisted>>((s, a) => subs.NewsPersisted = a);

            _mockArgusBus.Setup(b => b.Subscribe("AOM", It.IsAny<Action<NewsPersistedRefresh>>()))
                .Callback<string, Action<NewsPersistedRefresh>>((s, a) => subs.NewsPersistedRefresh = a);

            _mockArgusBus.Setup(b => b.Subscribe("AOM", It.IsAny<Action<NewsUpdatePersisted>>()))
                .Callback<string, Action<NewsUpdatePersisted>>((s, a) => subs.NewsUpdatePersisted = a);

            _mockArgusBus.Setup(b => b.Subscribe("AOM", It.IsAny<Action<PersistedNewsDeleted>>()))
                .Callback<string, Action<PersistedNewsDeleted>>((s, a) => subs.PersistedNewsDeleted = a);

            _argusNewsPublishedConsumer.Start();
            return subs;
        }

        public class SubscribedActions
        {
            public Action<NewsPersisted> NewsPersisted { get; set; }
            public Action<NewsPersistedRefresh> NewsPersistedRefresh { get; set; }
            public Action<NewsUpdatePersisted> NewsUpdatePersisted { get; set; }
            public Action<PersistedNewsDeleted> PersistedNewsDeleted { get; set; }
        }
    }
}