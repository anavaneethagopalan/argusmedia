﻿using System;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Services;
using AOM.Transport.Events.News;
using AOM.Transport.Service.NewsProcessor;
using AOM.Transport.Service.NewsProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Argus.Transport.Messages.Events;
using Moq;
using NUnit.Framework;

namespace Aom.Transport.Service.NewsProcessor.Tests.Consumers
{
    [TestFixture]
    class ReplayArgusNewsConsumerTests
    {
        private Mock<IBus> _mockAomBus;

        private Mock<IBus> _mockArgusBus;

        private Mock<INewsConfig>_mockAomNewsConfig;

        private NewsRefreshRequested _newsRefreshRequested;

        private Mock<IDateTimeProvider> _mockDateTimeProvider;

        private ReplayArgusNewsConsumer _replayArgusNewsConsumer;

        private Mock<IAomNewsService> _mockAomNewsService;

        [SetUp]
        public void Setup()
        {
            _mockAomBus = new Mock<IBus>();
            _mockArgusBus = new Mock<IBus>();
            _mockAomNewsService = new Mock<IAomNewsService>();
            _mockAomNewsConfig = new Mock<INewsConfig>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();

            _replayArgusNewsConsumer = new ReplayArgusNewsConsumer(
                _mockAomBus.Object,
                _mockArgusBus.Object,
                _mockAomNewsService.Object,
                _mockAomNewsConfig.Object,
                _mockDateTimeProvider.Object);
        }

        [Test]
        public void OnReplayArgusNewsShouldPublishNewsRefreshRequestedOntoArgusBus()
        {
            var subs = StartConsumer();
            _replayArgusNewsConsumer.Start();

            var dateTimeNow = new DateTime(2000, 10, 20);
            _mockDateTimeProvider.SetupGet(p => p.Now).Returns(dateTimeNow);
            _mockAomNewsService.Setup(m => m.GetAomContentStreamIds()).Returns(new decimal[] { 1, 2, 3 });

            var onReplayArgusNews = subs.ReplayArgusNews;
            onReplayArgusNews(new ReplayArgusNews { DaysToReplay = 10 });

            _mockArgusBus.Verify(m => m.Publish(It.IsAny<NewsRefreshRequested>()), Times.Exactly(1));

        }

        [Test]
        public void OnReplayArgusNewsShouldPublishNewsRefreshRequestedOntoArgusBusWithHistoricalDaysSetInReplayArgusNewsObject()
        {
            var subs = StartConsumer();
            _replayArgusNewsConsumer.Start();

            var onReplayArgusNews = subs.ReplayArgusNews;
            _mockAomNewsConfig.SetupGet(p => p.HisoricalDaysGetNews).Returns(100);
            _mockAomNewsService.Setup(m => m.GetAomContentStreamIds()).Returns(new decimal[] { 1, 2, 3 });

            var dateTimeNow = new DateTime(2000, 10, 20);
            _mockDateTimeProvider.SetupGet(p => p.Now).Returns(dateTimeNow);

            onReplayArgusNews(new ReplayArgusNews { DaysToReplay = -15 });

            _mockArgusBus.Verify(m => m.Publish(It.Is<NewsRefreshRequested>(p => ValidateProperty(p))), Times.Exactly(1));
            Assert.That(_newsRefreshRequested.NewsRefreshParameters.StartDate, Is.EqualTo(dateTimeNow.AddDays(-15)));
        }

        [Test]
        public void OnReplayArgusNewsShouldPublishNewsRefreshRequestedOntoArgusBusWithHistoricalDaysSetFromConfig()
        {
            var subs = StartConsumer();
            _replayArgusNewsConsumer.Start();

            var onReplayArgusNews = subs.ReplayArgusNews;
            _mockAomNewsConfig.SetupGet(p => p.HisoricalDaysGetNews).Returns(-1);
            _mockAomNewsService.Setup(m => m.GetAomContentStreamIds()).Returns(new decimal[] { 1, 2, 3 });

            var dateTimeNow = new DateTime(2000, 10, 20);
            _mockDateTimeProvider.SetupGet(p => p.Now).Returns(dateTimeNow);

            onReplayArgusNews(new ReplayArgusNews());

            _mockArgusBus.Verify(m => m.Publish(It.Is<NewsRefreshRequested>(p => ValidateProperty(p))), Times.Exactly(1));
            Assert.That(_newsRefreshRequested.NewsRefreshParameters.StartDate, Is.EqualTo(dateTimeNow.AddDays(-1)));
        }

        [Test]
        public void OnReplayArgusNewsShouldPublishNewsRefreshRequestedOntoArgusBusWithAllAomContentStreamsIfNullStreamsPassedIn()
        {
            var subs = StartConsumer();
            _replayArgusNewsConsumer.Start();

            var onReplayArgusNews = subs.ReplayArgusNews;
            _mockAomNewsConfig.SetupGet(p => p.HisoricalDaysGetNews).Returns(-1);

            _mockAomNewsService.Setup(m => m.GetAomContentStreamIds()).Returns(new decimal[]{1,2,3});

            var dateTimeNow = new DateTime(2000, 10, 20);
            _mockDateTimeProvider.SetupGet(p => p.Now).Returns(dateTimeNow);

            onReplayArgusNews(new ReplayArgusNews{ ContentStreamsReplay = null});

            _mockArgusBus.Verify(m => m.Publish(It.Is<NewsRefreshRequested>(p => ValidateProperty(p))), Times.Exactly(1));
            Assert.That(_newsRefreshRequested.NewsRefreshParameters.ContentStreams, Is.EqualTo(new decimal[]{1,2,3}));
        }

        [Test]
        public void OnReplayArgusNewsShouldPublishNewsRefreshRequestedOntoArgusBusWithAllAomContentStreamsIfEmptyListStreamsPassedIn()
        {
            var subs = StartConsumer();
            _replayArgusNewsConsumer.Start();

            var onReplayArgusNews = subs.ReplayArgusNews;
            _mockAomNewsConfig.SetupGet(p => p.HisoricalDaysGetNews).Returns(-1);

            _mockAomNewsService.Setup(m => m.GetAomContentStreamIds()).Returns(new decimal[] { 1, 2, 3 });

            var dateTimeNow = new DateTime(2000, 10, 20);
            _mockDateTimeProvider.SetupGet(p => p.Now).Returns(dateTimeNow);

            onReplayArgusNews(new ReplayArgusNews { ContentStreamsReplay = new decimal[]{} });

            _mockArgusBus.Verify(m => m.Publish(It.Is<NewsRefreshRequested>(p => ValidateProperty(p))), Times.Exactly(1));
            Assert.That(_newsRefreshRequested.NewsRefreshParameters.ContentStreams, Is.EqualTo(new decimal[] { 1, 2, 3 }));
        }

        [Test]
        public void OnReplayArgusNewsShouldPublishNewsRefreshRequestedUsingPassedInContentStreams()
        {
            var subs = StartConsumer();
            _replayArgusNewsConsumer.Start();

            var onReplayArgusNews = subs.ReplayArgusNews;
            _mockAomNewsConfig.SetupGet(p => p.HisoricalDaysGetNews).Returns(-1);

            _mockAomNewsService.Setup(m => m.GetAomContentStreamIds()).Returns(new decimal[] { 1, 2, 3 });

            var dateTimeNow = new DateTime(2000, 10, 20);
            _mockDateTimeProvider.SetupGet(p => p.Now).Returns(dateTimeNow);

            onReplayArgusNews(new ReplayArgusNews {ContentStreamsReplay = new decimal[] {1, 2}});

            _mockArgusBus.Verify(m => m.Publish(It.Is<NewsRefreshRequested>(p => ValidateProperty(p))), Times.Exactly(1));
            Assert.That(_newsRefreshRequested.NewsRefreshParameters.ContentStreams, Is.EqualTo(new decimal[] { 1, 2 }));
        }

        [Test]
        public void OnReplayArgusNewsShouldNotPublishNewsRefreshRequestedIfCsPassedInDoNotMatchAomCs()
        {
            var subs = StartConsumer();
            _replayArgusNewsConsumer.Start();

            var onReplayArgusNews = subs.ReplayArgusNews;
            _mockAomNewsConfig.SetupGet(p => p.HisoricalDaysGetNews).Returns(-1);

            _mockAomNewsService.Setup(m => m.GetAomContentStreamIds()).Returns(new decimal[] { 1, 2, 3 });

            var dateTimeNow = new DateTime(2000, 10, 20);
            _mockDateTimeProvider.SetupGet(p => p.Now).Returns(dateTimeNow);

            onReplayArgusNews(new ReplayArgusNews { ContentStreamsReplay = new decimal[] { 6, 7 } });

            _mockArgusBus.Verify(m => m.Publish(It.IsAny<ReplayArgusNews>()), Times.Exactly(0));
        }

        private bool ValidateProperty(NewsRefreshRequested newsRefreshRequsted)
        {
            _newsRefreshRequested = newsRefreshRequsted;
            return true;
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _mockAomBus.Setup(b => b.Subscribe("AOM", It.IsAny<Action<ReplayArgusNews>>()))
                .Callback<string, Action<ReplayArgusNews>>((s, a) => subs.ReplayArgusNews = a);


            return subs;
        }

        public class SubscribedActions
        {
            public Action<ReplayArgusNews> ReplayArgusNews { get; set; }
        }
    }
}
