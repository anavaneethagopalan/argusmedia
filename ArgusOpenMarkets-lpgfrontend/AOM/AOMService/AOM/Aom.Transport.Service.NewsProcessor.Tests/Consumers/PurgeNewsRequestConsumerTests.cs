﻿namespace Aom.Transport.Service.NewsProcessor.Tests.Consumers
{
    using System;

    using AOM.Transport.Events.News;
    using AOM.Transport.Service.NewsProcessor.Consumers;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    class PurgeNewsRequestConsumerTests
    {
        private Mock<IBus> _mockBus;

        private PurgeNewsRequestConsumer _purgeNewsRequestConsumer;

        [Test]
        public void OnPurgeNewsRequestShouldPublishPurgeNewsResponse()
        {
            _mockBus = new Mock<IBus>();
            _purgeNewsRequestConsumer = new PurgeNewsRequestConsumer(_mockBus.Object);
           
            var subs = StartConsumer();
            _purgeNewsRequestConsumer.Start();

            var onPurgeNewsRequest = subs.PurgeNewsRequest;
            onPurgeNewsRequest(new PurgeNewsRequest { DaysToKeep = -2 });

            _mockBus.Verify(m => m.Publish(It.IsAny<PurgeNewsResponse>()), Times.Exactly(1));
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("AOM", It.IsAny<Action<PurgeNewsRequest>>()))
                .Callback<string, Action<PurgeNewsRequest>>((s, a) => subs.PurgeNewsRequest = a);

            
            return subs;
        }

        public class SubscribedActions
        {
            public Action<PurgeNewsRequest> PurgeNewsRequest { get; set; }
        }
    }
}
