﻿namespace Aom.Transport.Service.NewsProcessor.Tests
{
    using AOM.Transport.Service.NewsProcessor;

    using NUnit.Framework;

    [TestFixture]
    public class NewsConfigTests
    {
        [Test]
        public void NewsConfigShouldReturnMinusFourForTheHistoricalDaysToGetNews()
        {
            var newsConfig = new NewsConfig();

            Assert.That(newsConfig.HisoricalDaysGetNews, Is.EqualTo(-4));
        }
    }
}
