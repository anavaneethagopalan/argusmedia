using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.WebControls;
using AomWebAdmin.DynamicExtensions;
using EntityDataSource = Microsoft.AspNet.EntityDataSource.EntityDataSource;
using EntityDataSourceChangingEventArgs = Microsoft.AspNet.EntityDataSource.EntityDataSourceChangingEventArgs;

namespace AomWebAdmin
{
    public partial class ManyToMany_EditField : FieldTemplateUserControl
    {
        private ObjectContext _objectContext;
        private IDynamicPageInterceptor _dynamicPageInterceptor;

        public void Page_Load(object sender, EventArgs e)
        {
            EntityDataSource ds = (EntityDataSource)this.FindDataSourceControl();
            ds.ContextCreated += (_, ctxCreatedEnventArgs) => { _objectContext = ctxCreatedEnventArgs.Context; };
            ds.Updating += new EventHandler<EntityDataSourceChangingEventArgs>(DataSource_UpdatingOrInserting);
            ds.Inserting += new EventHandler<EntityDataSourceChangingEventArgs>(DataSource_UpdatingOrInserting);

            var eventDispatcherContainer = Page as IEventDispatcherContainer;
            if (eventDispatcherContainer != null)
            {
                eventDispatcherContainer.EventDispatcher.ForeignKeyChanged += OnForeignKeyChanged;
            }

            _dynamicPageInterceptor =
                DynamicPageInterceptorContainer.Instance.GetDynamicPageInterceptor(Table.EntityType);
        }

        public void Page_Unload(object sender, EventArgs e)
        {
            var eventDispatcherContainer = Page as IEventDispatcherContainer;
            if (eventDispatcherContainer != null)
            {
                eventDispatcherContainer.EventDispatcher.ForeignKeyChanged -= OnForeignKeyChanged;
            } 
        }

        private void OnForeignKeyChanged()
        {
            DataBind();
        }

        void DataSource_UpdatingOrInserting(object sender, EntityDataSourceChangingEventArgs e)
        {
            MetaTable childTable = ChildrenColumn.ChildTable;

            if (Mode == DataBoundControlMode.Edit)
            {
                _objectContext.LoadProperty(e.Entity, Column.Name);
            }

            dynamic entityCollection = Column.EntityTypeProperty.GetValue(e.Entity, null);

            foreach (dynamic childEntity in childTable.GetQuery(e.Context))
            {
                var isCurrentlyInList = ListContainsEntity(childTable, entityCollection, childEntity);

                string pkString = childTable.GetPrimaryKeyString(childEntity);
                ListItem listItem = CheckBoxList1.Items.FindByValue(pkString);

                if (listItem != null && listItem.Selected)
                {
                    if (!isCurrentlyInList)
                        entityCollection.Add(childEntity);
                }
                else
                {
                    if (isCurrentlyInList)
                        entityCollection.Remove(childEntity);
                }
            }
        }

        private static bool ListContainsEntity(MetaTable table, IEnumerable<object> list, object entity)
        {
            return list.Any(e => AreEntitiesEqual(table, e, entity));
        }

        private static bool AreEntitiesEqual(MetaTable table, object entity1, object entity2)
        {
            return Enumerable.SequenceEqual(table.GetPrimaryKeyValues(entity1), table.GetPrimaryKeyValues(entity2));
        }

        protected void CheckBoxList1_DataBound(object sender, EventArgs e)
        {
            MetaTable childTable = ChildrenColumn.ChildTable;
            MetaTable parentTable = Column.Table;

            object entity = null;
            IEnumerable<object> entityCollection = null;

            if (Mode == DataBoundControlMode.Edit)
            {
                if (!IsPostBack)
                {
                    ICustomTypeDescriptor rowDescriptor = Row as ICustomTypeDescriptor;
                    if (rowDescriptor != null)
                    {
                        entity = rowDescriptor.GetPropertyOwner(null);
                    }
                    else
                    {
                        entity = Row;
                    }

                    var keyDictionary = parentTable.GetPrimaryKeyDictionary(entity);
                    ViewState["entityKey"] = keyDictionary;
                }
                else
                {
                    if (_objectContext == null)
                        _objectContext = parentTable.CreateContext() as ObjectContext;

                    Dictionary<string, object> entityKeyFromViewState =
                        (Dictionary<string, object>) ViewState["entityKey"];

                    var entityKey = new EntityKey(
                        string.Join(".", _objectContext.DefaultContainerName, parentTable.DataContextPropertyName), 
                        entityKeyFromViewState);

                    entity = _objectContext.GetObjectByKey(entityKey);
                }
                
                entityCollection = (IEnumerable<object>)Column.EntityTypeProperty.GetValue(entity, null);
                var realEntityCollection = entityCollection as RelatedEnd;
                if (realEntityCollection != null && !realEntityCollection.IsLoaded)
                {
                    realEntityCollection.Load();
                }
            }

            CheckBoxList1.Items.Clear();

            foreach (object childEntity in childTable.GetQuery(_objectContext))
            {
                if (!_dynamicPageInterceptor.FilterManyToManyRelationship(entity, childEntity, Page))
                    continue;

                ListItem listItem = new ListItem(
                    childTable.GetDisplayString(childEntity),
                    childTable.GetPrimaryKeyString(childEntity));

                if (Mode == DataBoundControlMode.Edit)
                {
                    listItem.Selected = ListContainsEntity(childTable, entityCollection, childEntity);
                }
                CheckBoxList1.Items.Add(listItem);
            }
        }

        public override Control DataControl
        {
            get
            {
                return CheckBoxList1;
            }
        }
    }
}
