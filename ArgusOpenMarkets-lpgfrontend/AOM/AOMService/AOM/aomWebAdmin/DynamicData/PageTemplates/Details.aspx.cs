using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;

namespace AomWebAdmin
{
    public partial class Details : System.Web.UI.Page
    {
        protected MetaTable _table;

        protected void Page_Init(object sender, EventArgs e)
        {
            _table = DynamicDataRouteHandler.GetRequestMetaTable(Context);
            FormView1.SetMetaTable(_table);
            DetailsDataSource.EntityTypeFilter = _table.EntityType.Name;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Title = _table.DisplayName;
            DetailsDataSource.Include = _table.ForeignKeyColumnsNames;
        }

        protected void FormView1_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            if (e.Exception == null || e.ExceptionHandled)
            {
                Response.Redirect(_table.ListActionPath);
            }
        }

        protected void FormView1_OnItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == DataControlCommands.CancelCommandName)
            {
                Response.Redirect(_table.ListActionPath);
            }
        }
    }
}
