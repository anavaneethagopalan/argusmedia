<%@ Page Language="C#" MasterPageFile="~/Site.master" CodeBehind="List.aspx.cs" Inherits="AomWebAdmin.List" %>
<%@ Import Namespace="Microsoft.Owin.Security.Provider" %>
<%@ Import Namespace="AomWebAdmin.DynamicExtensions" %>

<%@ Register src="~/DynamicData/Content/GridViewPager.ascx" tagname="GridViewPager" tagprefix="asp" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DynamicDataManager ID="DynamicDataManager1" runat="server" AutoLoadForeignKeys="true">
        <DataControls>
            <asp:DataControlReference ControlID="GridView1" />
        </DataControls>
    </asp:DynamicDataManager>

    <h2 class="DDSubHeader"><%= _table.DisplayName%></h2>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="DD">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" HeaderText="List of validation errors" CssClass="DDValidator" />
                <asp:DynamicValidator runat="server" ID="GridViewValidator" ControlToValidate="GridView1" Display="None" CssClass="DDValidator" />
                    <div style="display: table">
                        <asp:QueryableFilterRepeater runat="server" ID="FilterRepeater">
                            <ItemTemplate>
                                <div style="display: table-row">
                                    <div style="display: table-cell">
                                        <asp:Label runat="server" Text='<%# Eval("DisplayName") %>' OnPreRender="Label_PreRender" CssClass="col-md-1" />
                                    </div>
                                    <div style="display: table-cell">
                                        <asp:DynamicFilter runat="server" ID="DynamicFilter" OnFilterChanged="DynamicFilter_FilterChanged" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:QueryableFilterRepeater>
                    </div>
                <br />
            </div>
            
            <% if (_table.IsInsertCommandEnabled()) { %>
                 <div class="DDBottomHyperLink">
                    <asp:DynamicHyperLink ID="InsertHyperLink" runat="server" Action="Insert"><img runat="server" src="~/DynamicData/Content/Images/plus.gif" alt="Insert new item" />Insert new item</asp:DynamicHyperLink>
                </div>
            <% } %>

            <br/>

            <asp:GridView ID="GridView1" runat="server" DataSourceID="GridDataSource" EnablePersistedSelection="true"
                AllowPaging="True" AllowSorting="True" CssClass="DDGridView" PageSize="20"
                RowStyle-CssClass="td" HeaderStyle-CssClass="th" CellPadding="6">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <% if (_table.IsEditCommandEnabled()) { %>
                                <asp:DynamicHyperLink runat="server" Action="Edit" Text="Edit"/>
                            <% } %>
                            <% if (_table.IsDeleteCommandEnabled()) { %>
                                <asp:LinkButton runat="server" CommandName="Delete" Text="Delete" OnClientClick='return confirm("Are you sure you want to delete this record?");' />
                            <% } %>
                            <asp:DynamicHyperLink runat="server" Text="Details"/>
                            <asp:Repeater runat="server" ID="CustomCommandsRepeater" OnDataBinding="CustomCommandsRepeater_OnDataBinding">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" 
                                        CommandName='<%# Eval("Name") %>' 
                                        Text='<%# Eval("DisplayName") %>'
                                        OnCommand="OnCustomCommand"
                                        OnDataBinding="OnCustomCommandDataBinding"/>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

                <PagerStyle CssClass="DDFooter"/>        
                <PagerTemplate>
                    <asp:GridViewPager runat="server" />
                </PagerTemplate>
                <EmptyDataTemplate>
                    There are currently no items in this table.
                </EmptyDataTemplate>
            </asp:GridView>

            <ef:EntityDataSource ID="GridDataSource" runat="server" EnableDelete="true" />
            
            <asp:QueryExtender TargetControlID="GridDataSource" ID="GridQueryExtender" runat="server">
                <asp:DynamicFilterExpression ControlID="FilterRepeater" />
            </asp:QueryExtender>

            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

