namespace AomWebAdmin
{
    public interface IEventDispatcherContainer
    {
        EventDispatcher EventDispatcher { get; }
    }
}