<%@ Page Language="C#" MasterPageFile="~/Site.master" CodeBehind="Edit.aspx.cs" Inherits="AomWebAdmin.Edit" %>
<%@ Import Namespace="AomWebAdmin.DynamicExtensions" %>


<asp:Content ID="headContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DynamicDataManager ID="DynamicDataManager1" runat="server" AutoLoadForeignKeys="true">
        <DataControls>
            <asp:DataControlReference ControlID="FormView1" />
        </DataControls>
    </asp:DynamicDataManager>

    <h2 class="DDSubHeader"><%= _table.DisplayName %></h2>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                HeaderText="List of validation errors" CssClass="DDValidator" />
            <asp:DynamicValidator runat="server" ID="DetailsViewValidator" ControlToValidate="FormView1" Display="None" CssClass="DDValidator" />

            <asp:FormView runat="server" ID="FormView1" DataSourceID="DetailsDataSource" DefaultMode="Edit"
                OnItemCommand="FormView1_ItemCommand" OnItemUpdated="FormView1_ItemUpdated" RenderOuterTable="false">
                <EditItemTemplate>
                    <table id="detailsTable" class="DDDetailsTable" cellpadding="6">
                        <asp:DynamicEntity runat="server" Mode="Edit" />
                        <tr class="td">
                            <td colspan="2">
                                <% if (_table.IsEditCommandEnabled()) { %>
                                    <asp:LinkButton runat="server" CommandName="Update" Text="Update" OnClientClick="return onUpdateButtonClick()" />
                                <% } %>
                                <asp:LinkButton runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </EditItemTemplate>
                <EmptyDataTemplate>
                    <div class="DDNoItem">No such item.</div>
                </EmptyDataTemplate>
            </asp:FormView>

            <ef:EntityDataSource ID="DetailsDataSource" runat="server" EnableUpdate="true" />

            <asp:QueryExtender TargetControlID="DetailsDataSource" ID="DetailsQueryExtender" runat="server">
                <asp:DynamicRouteExpression />
            </asp:QueryExtender>
            
            <asp:HiddenField runat="server" ID="_DEFAULTVALUES" />
            
            <script type="text/javascript">
                function onUpdateButtonClick() {
                    var defaultValues = getDefaultValues();
                    var currentValues = getFormCurrentValues(theForm);
                    var changedValues = getChangedValues(defaultValues, currentValues);

                    var confirmationMessage = "Are you sure you want to update the record?";
                    if (changedValues.length > 0) {
                        confirmationMessage += "\nFollowing properties were changed:";
                        for (var i = 0; i < changedValues.length; i++) {
                            confirmationMessage += "\n" + changedValues[i].description + ": " + changedValues[i].newValue;
                        }
                    }

                    return confirm(confirmationMessage);
                }

                function getChangedValues(originalValues, currentValues) {
                    var changedValues = [];

                    for (var key in currentValues) {
                        if (currentValues.hasOwnProperty(key)) {
                            var currentValue = currentValues[key];
                            if (originalValues.hasOwnProperty(key)) {
                                var defaultValue = originalValues[key];
                                if (currentValue.value !== defaultValue.value) {
                                    changedValues.push({ 
                                        description: key,
                                        originalValue: defaultValue.text,
                                        newValue: currentValue.text
                                    });
                                }
                            } else {
                                changedValues.push({
                                    description: key,
                                    originalValue: null,
                                    newValue: currentValue.text
                                });
                            }
                        }
                    }

                    return changedValues;
                }

                function getFormCurrentValues(form) {
                    var values = {};
                    
                    for (var i = 0; i < form.elements.length; i++) {
                        var element = form.elements[i];

                        var description = null;
                        if (element.labels && element.labels.length > 0) {
                            description = element.labels[0].textContent;
                        } else {
                            description = $('label[for="' + element.id + '"]').text();
                        }

                        if (element.type === "text") {
                            values[description] = { value: element.value, text: element.value };
                        } else if (element.type === "checkbox") {
                            values[description] = { value: element.checked, text: element.checked };
                        } else if (element.type === "select-one") {
                            var selectedOption = element.options[element.selectedIndex];
                            values[description] = { value: selectedOption.value, text: selectedOption.text };
                        }
                    }

                    return values;
                }

                function getDefaultValues() {
                    var defaultValuesInput = $("input[name*='_DEFAULTVALUES'");
                    var defaultValues = defaultValuesInput.val();
                    if (defaultValues !== null && defaultValues !== "") {
                        return jQuery.parseJSON(defaultValues);
                    }

                    return [];
                }

                function saveDefaultValues(defaultValues) {
                    var defaultValuesInput = $("input[name*='_DEFAULTVALUES'");
                    var defaultValuesText = defaultValuesInput.val();
                    if (defaultValuesText === null || defaultValuesText === "") {
                        defaultValuesText = JSON.stringify(defaultValues);
                        defaultValuesInput.val(defaultValuesText);
                    }
                }

                $(function() {
                    saveDefaultValues(getFormCurrentValues(theForm));
                });
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

