using System;

namespace AomWebAdmin
{
    public class EventDispatcher
    {
        public event Action ForeignKeyChanged;

        public void RaiseForeignKeyChanged()
        {
            if (ForeignKeyChanged != null)
                ForeignKeyChanged();
        }
    }
}