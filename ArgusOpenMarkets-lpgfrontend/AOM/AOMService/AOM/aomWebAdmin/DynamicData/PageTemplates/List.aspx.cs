using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.DynamicData;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using AomWebAdmin.DynamicExtensions;

namespace AomWebAdmin
{
    public partial class List : System.Web.UI.Page
    {
        protected MetaTable _table;
        protected Dictionary<string, ICustomCommand> _customNavigateCommands;

        protected void Page_Init(object sender, EventArgs e)
        {
            _table = DynamicDataRouteHandler.GetRequestMetaTable(Context);
            _customNavigateCommands = _table.GetCustomNavigateCommands().ToDictionary(x => x.Name, x => x);

            GridView1.SetMetaTable(_table, _table.GetColumnValuesFromRoute(Context));
            GridDataSource.EntityTypeFilter = _table.EntityType.Name;


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Title = _table.DisplayName;
            GridDataSource.Include = _table.ForeignKeyColumnsNames;

            // Disable various options if the table is readonly
            if (_table.IsReadOnly)
            {
                GridView1.Columns[0].Visible = false;
                InsertHyperLink.Visible = false;
                GridView1.EnablePersistedSelection = false;
            }
        }

        protected void Label_PreRender(object sender, EventArgs e)
        {
            Label label = (Label) sender;
            DynamicFilter dynamicFilter = (DynamicFilter) label.FindControl("DynamicFilter");
            QueryableFilterUserControl fuc = dynamicFilter.FilterTemplate as QueryableFilterUserControl;
            if (fuc != null && fuc.FilterControl != null)
            {
                label.AssociatedControlID = fuc.FilterControl.GetUniqueIDRelativeTo(label);
            }
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary(GridView1.GetDefaultValues());
            InsertHyperLink.NavigateUrl = _table.GetActionPath(PageAction.Insert, routeValues);
            base.OnPreRenderComplete(e);
        }

        protected void DynamicFilter_FilterChanged(object sender, EventArgs e)
        {
            GridView1.PageIndex = 0;
        }

        protected void CustomCommandsRepeater_OnDataBinding(object sender, EventArgs e)
        {
            var repeater = sender as Repeater;
            if (repeater != null)
                repeater.DataSource = _customNavigateCommands.Values;
        }

        protected void OnCustomCommand(object sender, CommandEventArgs e)
        {
            ICustomCommand customCommand;
            if (_customNavigateCommands.TryGetValue(e.CommandName, out customCommand))
            {
                customCommand.Execute(e, Page);
            }
        }

        protected void OnCustomCommandDataBinding(object sender, EventArgs e)
        {
            var linkButton = sender as LinkButton;
            if (linkButton != null)
            {
                var gridViewRow = linkButton.GetParentControl<GridViewRow>();
                object id = DataBinder.Eval(gridViewRow.DataItem, "Id");
                if (id != null)
                {
                    linkButton.CommandArgument = id.ToString();
                }
            }
        }
    }

    public static class ControlExtensions
    {
        public static TControl GetParentControl<TControl>(this Control control) where TControl : Control
        {
            while (!(control is TControl) && control != null)
            {
                control = control.Parent;
            }

            return (TControl)control;
        }
    }
}
