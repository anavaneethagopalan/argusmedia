using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;

namespace AomWebAdmin
{
    public partial class Insert : System.Web.UI.Page, IEventDispatcherContainer
    {
        protected MetaTable _table;

        public Insert()
        {
            EventDispatcher = new EventDispatcher();
        }

        public EventDispatcher EventDispatcher { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            _table = DynamicDataRouteHandler.GetRequestMetaTable(Context);
            FormView1.SetMetaTable(_table, _table.GetColumnValuesFromRoute(Context));
            DetailsDataSource.EntityTypeFilter = _table.EntityType.Name;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Title = _table.DisplayName;
        }

        protected void FormView1_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == DataControlCommands.CancelCommandName)
            {
                Response.Redirect(_table.ListActionPath);
            }
        }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null || e.ExceptionHandled)
            {
                Response.Redirect(_table.ListActionPath);
            }
        }
    }
}
