using System;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AomWebAdmin
{
    public partial class Edit : Page, IEventDispatcherContainer
    {
        protected MetaTable _table;

        public Edit()
        {
            EventDispatcher = new EventDispatcher();
        }

        public EventDispatcher EventDispatcher { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            _table = DynamicDataRouteHandler.GetRequestMetaTable(Context);
            FormView1.SetMetaTable(_table);
            DetailsDataSource.EntityTypeFilter = _table.EntityType.Name;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Title = _table.DisplayName;
            DetailsDataSource.Include = _table.ForeignKeyColumnsNames;
        }

        protected void FormView1_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == DataControlCommands.CancelCommandName)
            {
                Response.Redirect(_table.ListActionPath);
            }
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null || e.ExceptionHandled)
            {
                Response.Redirect(_table.ListActionPath);
            }
        }
    }
}
