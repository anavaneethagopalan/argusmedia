﻿using System;

namespace AomWebAdmin.DynamicExtensions
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DynamicCommandsAttribute : Attribute
    {
        public DynamicCommand[] DynamicCommands { get; set; }

        public DynamicCommandsAttribute(params DynamicCommand[] dynamicCommands)
        {
            DynamicCommands = dynamicCommands;
        }
    }
}