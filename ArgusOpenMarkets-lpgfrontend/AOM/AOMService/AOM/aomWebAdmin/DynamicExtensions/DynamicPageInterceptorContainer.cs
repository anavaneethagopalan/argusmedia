﻿using System;
using System.Collections.Generic;

namespace AomWebAdmin.DynamicExtensions
{
    public class DynamicPageInterceptorContainer : IDynamicPageInterceptorContainer
    {
        private readonly Dictionary<Type, IDynamicPageInterceptor> _pageInterceptors 
                = new Dictionary<Type, IDynamicPageInterceptor>();

        // Oh no! It's so bad to use singlton pattern! We can replace it future.
        public static IDynamicPageInterceptorContainer Instance { get; private set; }

        static DynamicPageInterceptorContainer()
        {
            Instance = new DynamicPageInterceptorContainer();
        }

        public void RegisterPageInterceptor<T>(IDynamicPageInterceptor dynamicPageInterceptor)
        {
            _pageInterceptors.Add(typeof(T), dynamicPageInterceptor);
        }

        public IDynamicPageInterceptor GetDynamicPageInterceptor(Type type)
        {
            IDynamicPageInterceptor dynamicPageInterceptor;
            if (_pageInterceptors.TryGetValue(type, out dynamicPageInterceptor))
                return dynamicPageInterceptor;

            return new DefaultPageInterceptor();
        }
    }
}