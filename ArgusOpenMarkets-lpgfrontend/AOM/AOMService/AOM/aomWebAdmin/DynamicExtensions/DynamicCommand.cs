﻿namespace AomWebAdmin.DynamicExtensions
{
    public enum DynamicCommand
    {
        Edit,
        Insert,
        Delete
    }
}