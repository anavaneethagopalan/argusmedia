using System.Web.UI;
using System.Web.UI.WebControls;

namespace AomWebAdmin.DynamicExtensions
{
    public class CustomNavigateCommand : ICustomCommand
    {
        public CustomNavigateCommand(string name, string displayName, string linkPattern)
        {
            Name = name;
            DisplayName = displayName;
            LinkPattern = linkPattern;
        }

        public string Name { get; private set; }

        public string DisplayName { get; private set; }

        public string LinkPattern { get; private set; }

        public void Execute(CommandEventArgs args, Page page)
        {
            var id = args.CommandArgument.ToString();
            var returnUrl = page.Request.RawUrl;
            var link = string.Format(LinkPattern, id, returnUrl);
            page.Response.Redirect(link);
        }
    }
}