﻿using System.Web.UI;
using AomWebAdmin.Repository.Crm;

namespace AomWebAdmin.DynamicExtensions
{
    public interface IDynamicPageInterceptor
    {
        bool FilterManyToManyRelationship(object parentEntity, object childEntity, Page page);

        void OnForeignKeyChanged(string selectedValue, Page page);
    }

    public class UserInfoPageInteceptor : IDynamicPageInterceptor
    {
        public bool FilterManyToManyRelationship(object parentEntity, object childEntity, Page page)
        {
            long selectedOrganisation = 0;

            if (page.Items.Contains("SelectedOrganisation"))
            {
                selectedOrganisation = long.Parse(page.Items["SelectedOrganisation"].ToString());
            }
            else
            {
                var userInfo = parentEntity as UserInfoDto;
                if (userInfo != null)
                    selectedOrganisation = userInfo.Organisation_Id_fk;
            }

            var organisationRole = childEntity as OrganisationRoleDto;
            if (organisationRole == null)
                return true;

            return selectedOrganisation == organisationRole.Organisation_Id_fk;
        }

        public void OnForeignKeyChanged(string selectedValue, Page page)
        {
            page.Items.Add("SelectedOrganisation", selectedValue);
        }
    }

    public class OrganisationRolePageInterceptor : IDynamicPageInterceptor
    {
        public bool FilterManyToManyRelationship(object parentEntity, object childEntity, Page page)
        {
            long selectedOrganisation = 0;

            if (page.Items.Contains("SelectedOrganisation"))
            {
                selectedOrganisation = long.Parse(page.Items["SelectedOrganisation"].ToString());
            }
            else
            {
                var role = parentEntity as OrganisationRoleDto;
                if (role != null)
                    selectedOrganisation = role.Organisation_Id_fk;
            }

            var userInfo = childEntity as UserInfoDto;
            if (userInfo == null)
                return true;

            return selectedOrganisation == userInfo.Organisation_Id_fk;
        }

        public void OnForeignKeyChanged(string selectedValue, Page page)
        {
            page.Items.Add("SelectedOrganisation", selectedValue);
        }
    }

    public class DefaultPageInterceptor : IDynamicPageInterceptor
    {
        public bool FilterManyToManyRelationship(object parentEntity, object childEntity, Page page)
        {
            return true;
        }

        public void OnForeignKeyChanged(string selectedValue, Page page)
        {
        }
    }
}
