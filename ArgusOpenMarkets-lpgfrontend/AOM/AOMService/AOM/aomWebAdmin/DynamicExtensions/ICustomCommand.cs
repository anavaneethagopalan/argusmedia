﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace AomWebAdmin.DynamicExtensions
{
    public interface ICustomCommand
    {
        string Name { get; }

        string DisplayName { get; }

        void Execute(CommandEventArgs args, Page page);
    }
}