using System.Linq;
using System.Reflection;
using System.Web.DynamicData;

namespace AomWebAdmin.DynamicExtensions
{
    public static class MetaTableExtensions
    {
        public static bool IsDynamicCommandEnabled(this MetaTable metaTable, DynamicCommand dynamicCommand)
        {
            var entityType = metaTable.EntityType;
            var attribute = entityType.GetCustomAttribute(typeof(DynamicCommandsAttribute)) as DynamicCommandsAttribute;
            if (attribute == null)
                return true;

            return attribute.DynamicCommands.Any(x => x == dynamicCommand);
        }

        public static bool IsEditCommandEnabled(this MetaTable metaTable)
        {
            return IsDynamicCommandEnabled(metaTable, DynamicCommand.Edit);
        }

        public static bool IsInsertCommandEnabled(this MetaTable metaTable)
        {
            return IsDynamicCommandEnabled(metaTable, DynamicCommand.Insert);
        }

        public static bool IsDeleteCommandEnabled(this MetaTable metaTable)
        {
            return IsDynamicCommandEnabled(metaTable, DynamicCommand.Delete);
        }

        public static ICustomCommand[] GetCustomNavigateCommands(this MetaTable metaTable)
        {
            return metaTable.EntityType
                .GetCustomAttributes(typeof(CustomNavigateCommandAttribute))
                .OfType<CustomNavigateCommandAttribute>()
                .Select(x => x.CreateCommand())
                .ToArray();
        }
    }
}