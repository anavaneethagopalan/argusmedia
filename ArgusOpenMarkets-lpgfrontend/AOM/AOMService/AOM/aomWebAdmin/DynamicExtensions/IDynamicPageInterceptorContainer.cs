using System;

namespace AomWebAdmin.DynamicExtensions
{
    public interface IDynamicPageInterceptorContainer
    {
        void RegisterPageInterceptor<T>(IDynamicPageInterceptor dynamicPageInterceptor);
        IDynamicPageInterceptor GetDynamicPageInterceptor(Type type);
    }
}