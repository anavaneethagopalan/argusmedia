﻿using System;

namespace AomWebAdmin.DynamicExtensions
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class CustomNavigateCommandAttribute : Attribute
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Link { get; set; }

        public ICustomCommand CreateCommand()
        {
            return new CustomNavigateCommand(Name, DisplayName, Link);
        }
    }
}