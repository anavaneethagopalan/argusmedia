﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;

namespace AomWebAdmin
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel _kernel;

        public NinjectControllerFactory(IKernel kernel)
        {
            _kernel = kernel;
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            IController controller = null;

            if (controllerType != null)
                controller = (IController)_kernel.Get(controllerType);

            return controller;
        }
    }
}