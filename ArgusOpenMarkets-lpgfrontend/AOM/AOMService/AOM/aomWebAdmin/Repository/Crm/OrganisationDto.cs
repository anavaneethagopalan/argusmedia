using System.ComponentModel;
using AomWebAdmin.DynamicExtensions;

namespace AomWebAdmin.Repository.Crm
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("crm.Organisation")]
    [ScaffoldTable(true)]
    [DisplayName("Organisation Management")]
    [DynamicCommands(DynamicCommand.Insert, DynamicCommand.Edit)]
    [DisplayColumn("Name")]
    public class OrganisationDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(5)]
        [DisplayName("Short code")]
        public string ShortCode { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        [DisplayName("Legal Name")]
        public string LegalName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        private DateTime? _DateCreated;

        [DisplayName("Creation Date")]
        public DateTime? DateCreated
        {
            get
            {
                return _DateCreated == null
                    ? _DateCreated
                    : DateTime.SpecifyKind((DateTime)_DateCreated, DateTimeKind.Utc);
            }
            set
            {
                _DateCreated = value;
            }
        }

        [Column(TypeName = "char")]
        [StringLength(1)]
        [DisplayName("Organisation Type")]
        public string OrganisationType { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [Column]
        [DisplayName("Deleted")]
        public bool IsDeleted { get; set; }

    }
}