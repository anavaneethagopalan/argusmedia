using System.Data.Entity;
using AOM.App.Domain.Entities;

//using AOM.Repository.MySql.Crm;

namespace AomWebAdmin.Repository.Crm
{
    public class CrmModel : DbContext
    {
        public CrmModel() : base("CrmModel")
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public virtual IDbSet<OrganisationDto> Organisations { get; set; }
        public virtual IDbSet<UserInfoDto> UserInfoes { get; set; } 
        public virtual IDbSet<OrganisationRoleDto> OrganisationRoles { get; set; }
        public virtual IDbSet<SystemPrivilegeDto> SystemPrivileges { get; set; }
        public virtual IDbSet<AuthenticationHistoryDto> AuthenticationHistories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserInfoDto>()
                .HasMany(x => x.OrganisationRoles)
                .WithMany(x => x.Users)
                .Map(mapper =>
                {
                    mapper.MapLeftKey("UserInfo_Id_fk");
                    mapper.MapRightKey("Role_Id_fk");
                    mapper.ToTable("UserInfoRole");
                });

            modelBuilder.Entity<OrganisationRoleDto>()
                .HasMany(x => x.OrganisationRoleSystemPrivileges)
                .WithMany(x => x.SystemPrivilegeOrganisationRoles)
                .Map(mapper =>
                {
                    mapper.MapLeftKey("Role_Id_fk");
                    mapper.MapRightKey("Privilege_Id_fk");
                    mapper.ToTable("SystemRolePrivilege");
                });

            /* AVOID ADDING MORE DEFINITIONS HERE
          Don't add foreign key back references here unliss your code explitly needs
          it. They create complex SQL queries.
          e.g a product has a location....a location does not have products. 
          If you need products at a location then this can be achieved through querying product.location
          */
        }
    }
}
