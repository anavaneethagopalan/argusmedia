using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using AomWebAdmin.DynamicExtensions;

namespace AomWebAdmin.Repository.Crm
{
    [Table("crm.OrganisationRole")]
    [DebuggerDisplay("{Id},{Name}")]
    [ScaffoldTable(true)]
    [DisplayName("Organisation Role Management")]
    [DisplayColumn("Name")]
    [DynamicCommands(DynamicCommand.Edit, DynamicCommand.Insert)]
    public class OrganisationRoleDto
    {
        public OrganisationRoleDto()
        {
            Users = new HashSet<UserInfoDto>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Order = 0)]
        public string Name { get; set; }

        [StringLength(200)]
        [Display(Order = 1)]
        public string Description { get; set; }

        [Required]
        public long Organisation_Id_fk { get; set; }

        [ForeignKey("Organisation_Id_fk")]
        [Display(Order = 2)]
        [DisplayName("Organisation")]
        public virtual OrganisationDto OrganisationDto { get; set; }

        [NotMapped]
        public virtual ICollection<UserInfoDto> Users { get; set; }

        [DisplayName("Organisation Role System Privileges"), Display(Order = 3)]
        public virtual ICollection<SystemPrivilegeDto> OrganisationRoleSystemPrivileges { get; set; }
                     
    }
}