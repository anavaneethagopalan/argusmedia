using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AomWebAdmin.DynamicExtensions;

namespace AomWebAdmin.Repository.Crm
{
    [Table("crm.UserInfo")]
    [DisplayName("User Management")]
    [ScaffoldTable(true)]
    [DynamicCommands(DynamicCommand.Insert, DynamicCommand.Edit)]
    [CustomNavigateCommand(Name = "ChangePassword", DisplayName = "Change Password", Link = "/Account/ChangeUserPassword?userId={0}&returnUrl={1}")]
    [DisplayColumn("Name")]
    public partial class UserInfoDto
    {
        public UserInfoDto()
        {
            OrganisationRoles = new HashSet<OrganisationRoleDto>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        
        [Required]
        [StringLength(100)]
        [Display(Order = 2)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Order = 0)]
        public string Name { get; set; }

        [StringLength(50)]
        [Display(Order = 1)]
        public string Title { get; set; }

        [StringLength(50)]
        [Display(Order = 3)]
        public string Telephone { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Order = 4)]
        [DisplayName("User Name")]
        public string Username { get; set; }

        [Column]
        [StringLength(100)]
        [Display(Order = 5)]
        [DisplayName("Argus CRM User Name")]
        public string ArgusCrmUsername { get; set; }

        public long Organisation_Id_fk { get; set; }

        [ForeignKey("Organisation_Id_fk")]
        [DisplayName("Organisation"), Display(Order = 6)]
        public virtual OrganisationDto OrganisationDto { get; set; }

        [DisplayName("Organisation Roles"), Display(Order = 7)]
        public virtual ICollection<OrganisationRoleDto> OrganisationRoles { get; set; }

        [DisplayName("Active")]
        [Display(Order = 8)]
        public bool IsActive { get; set; }

        [Column]
        [DisplayName("Blocked")]
        [Display(Order = 9)]
        public bool IsBlocked { get; set; }

        [Column]
        [DisplayName("Deleted")]
        [Display(Order = 10)]
        public bool IsDeleted { get; set; }
    }
}
