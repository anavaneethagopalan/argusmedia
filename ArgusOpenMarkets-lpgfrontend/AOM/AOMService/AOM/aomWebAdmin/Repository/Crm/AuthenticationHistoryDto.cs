using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AomWebAdmin.DynamicExtensions;

namespace AomWebAdmin.Repository.Crm
{
    [Table("crm.AuthenticationHistory")]
    [DisplayName("AuthenticationHistory")]
    [ScaffoldTable(true)]
    [DynamicCommands()]
    public partial class AuthenticationHistoryDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        
        [Required]
        [StringLength(100)]
        [Display(Order = 2)]
        public string IpAddress { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Order = 0)]
        public string Username { get; set; }

        [Display(Order = 1)]
        public bool SuccessfulLogin { get; set; }

        [Display(Order = 3)]
        [DisplayName("Date Created")]
        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(500)]
        [Display(Order = 0)]
        [DisplayName("Login Failure Reason")]
        public string LoginFailureReason { get; set; }
    }
}
