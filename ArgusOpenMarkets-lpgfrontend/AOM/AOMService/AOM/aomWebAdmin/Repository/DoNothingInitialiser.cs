﻿using System.Data.Entity;

namespace AomWebAdmin
{
    class DoNothingInitialiser<T> : IDatabaseInitializer<T> where T : DbContext
    {
        public void InitializeDatabase(T context)
        {
            // this provides no database creation/recreate/drop strategy on purpose
            // the liquidbase delta scripts has this responsilbility
        }
    }

}
