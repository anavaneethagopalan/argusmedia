﻿using System.Web.DynamicData;
using System.Web.Mvc;

namespace AomWebAdmin.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View(MetaModel.Default.VisibleTables.FindAll(x => x.Scaffold));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}