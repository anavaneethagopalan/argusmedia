﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

using AomWebAdmin.Models;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.CrmService;


namespace AomWebAdmin.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private const string ContactSupportMessage = "Please contact support aomdevteam@argusmedia.com if the problem persists.";

        private const string AdministratorPermissionName = "AOMAdminTool";

        private readonly IUserAuthenticationService _userAuthenticationService;
        private readonly IUserService _userService;
        private readonly ICredentialsService _credentialsService;
        private readonly IEncryptionService _encryptionService;

        public AccountController(
            IUserAuthenticationService userAuthenticationService, 
            IUserService userService, 
            ICredentialsService credentialsService, 
            IEncryptionService encryptionService)
        {
            _userAuthenticationService = userAuthenticationService;
            _userService = userService;
            _credentialsService = credentialsService;
            _encryptionService = encryptionService;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new User { Id = _userService.GetUserId(model.UserName) };
                    var password = _encryptionService.Hash(user, model.Password);
                    var userLoginSourceId = 1; //"AOM"

                    var authenticationResult = _userAuthenticationService.AuthenticateUser(model.UserName, password, userLoginSourceId);
                    if (authenticationResult.IsAuthenticated)
                    {
                        var privileges = _userService.GetSystemPrivileges(user.Id);
                        if (privileges.Privileges.Contains(AdministratorPermissionName))
                        {
                            await SignInAsync(new ApplicationUser { UserName = model.UserName }, model.RememberMe);
                            return RedirectToLocal(returnUrl);
                        }

                        ModelState.AddModelError(string.Empty, "User doesn't have sufficient privileges");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, authenticationResult.FailureReason);
                    }
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "An internal system error has occurred");
            }

            if (!ModelState.IsValid)
                ModelState.AddModelError(string.Empty, ContactSupportMessage);

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        //[AllowAnonymous]
        //public ActionResult Register()
        //{
        //    return View();
        //}

        //
        // POST: /Account/Register
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Register(RegisterViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new ApplicationUser() { UserName = model.UserName };
        //        var result = await UserManager.CreateAsync(user, model.Password);
        //        if (result.Succeeded)
        //        {
        //            await SignInAsync(user, isPersistent: false);
        //            return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {
        //            AddErrors(result);
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        //
        // GET: /Account/Manage
        //public ActionResult Manage(ManageMessageId? message)
        //{
        //    ViewBag.StatusMessage =
        //        message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
        //        : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
        //        : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
        //        : message == ManageMessageId.Error ? "An error has occurred."
        //        : "";
        //    ViewBag.HasLocalPassword = HasPassword();
        //    ViewBag.ReturnUrl = Url.Action("Manage");
        //    return View();
        //}

        //
        // POST: /Account/Manage
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Manage(ManageUserViewModel model)
        //{
        //    bool hasPassword = HasPassword();
        //    ViewBag.HasLocalPassword = hasPassword;
        //    ViewBag.ReturnUrl = Url.Action("Manage");
        //    if (hasPassword)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
        //            if (result.Succeeded)
        //            {
        //                return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
        //            }
        //            else
        //            {
        //                AddErrors(result);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        // User does not have a password so remove any validation errors caused by a missing OldPassword field
        //        ModelState state = ModelState["OldPassword"];
        //        if (state != null)
        //        {
        //            state.Errors.Clear();
        //        }

        //        if (ModelState.IsValid)
        //        {
        //            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
        //            if (result.Succeeded)
        //            {
        //                return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
        //            }
        //            else
        //            {
        //                AddErrors(result);
        //            }
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //[ChildActionOnly]
        //public ActionResult RemoveAccountList()
        //{
        //    var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
        //    ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
        //    return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        //}

        [HttpGet, Authorize]
        public ActionResult ChangeUserPassword(long userId, string returnUrl)
        {
            var user = _userService.GetContactDetails(userId);

            return View(new ChangeUserPasswordViewModel
            {
                UserId = userId, 
                ReturnUrl = returnUrl, 
                UserName = user.Name, 
                TemporaryPassword = true,
                TemporaryPasswordExpirationHours = 48
            });
        }
        
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeUserPassword(ChangeUserPasswordViewModel model)
        {
            try
            {
                var id = model.UserId;
                if (id < -1)
                {
                    throw new AuthenticationErrorException(
                        "Can not change password as user does not exist : " + model.UserId);
                }
                if (id == -1)
                {
                    throw new AuthenticationErrorException("You can not change the password of the system user");
                }

                model.NewPassword = _encryptionService.Hash(model.UserId, model.NewPassword);

                var success = _credentialsService.UpdateUserCredentials(
                        id,
                        null,
                        model.NewPassword,
                        false, 
                        true,
                        0,
                        model.TemporaryPassword, 
                        model.TemporaryPasswordExpirationHours);

                if (success)
                    return RedirectToLocal(model.ReturnUrl);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #region Helpers

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            var identity = new ClaimsIdentity(
                  new[] 
                  { 
                      // adding following 2 claim just for supporting default antiforgery provider
                      new Claim(ClaimTypes.NameIdentifier, user.UserName),
                      new Claim(
                          "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                          "ASP.NET Identity",
                          "http://www.w3.org/2001/XMLSchema#string"),
                      new Claim(ClaimTypes.Name, user.UserName)
                  },
                  DefaultAuthenticationTypes.ApplicationCookie);

            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
        }

        //private bool HasPassword()
        //{
        //    var user = UserManager.FindById(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        return user.PasswordHash != null;
        //    }
        //    return false;
        //}

//        public enum ManageMessageId
//        {
//            ChangePasswordSuccess,
//            SetPasswordSuccess,
//            RemoveLoginSuccess,
//            Error
//        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion
    }
}