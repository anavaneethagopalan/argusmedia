﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Web;
using System.Web.DynamicData;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.UI;
using MySql.Data.Entity;
using Ninject;
//using Utils.EncryptionService;

using AomWebAdmin.DynamicExtensions;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Services;
//using AOM.Repository.MySql.Crm;
using AOM.Services;
using AomWebAdmin.Repository.Crm;
using AOM.Services.EncryptionService;
using CrmModel = AomWebAdmin.Repository.Crm.CrmModel;

namespace AomWebAdmin
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            RegisterControllerFactory();
            RegisterDatabaseConfiguration();
            RegisterDynamicDataPages();
            RegisterScriptDefinitions();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private static void RegisterControllerFactory()
        {
            var kernel = new StandardKernel();

            kernel.Bind<IDateTimeProvider>().To<DateTimeProvider>();
            kernel.Bind<IEncryptionService>().To<EncryptionService>();
            kernel.Load(new AOM.Repository.MySql.EFModelBootstrapper(), new ServiceBootstrap());

            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory(kernel));
        }

        private static void RegisterDatabaseConfiguration()
        {
            DbConfiguration.SetConfiguration(new MySqlEFConfiguration());
        }

        private static void RegisterDynamicDataPages()
        {
            MetaModel model = new MetaModel();
            model.RegisterContext(
                new Microsoft.AspNet.DynamicData.ModelProviders.EFDataModelProvider(() => new CrmModel()),
                new ContextConfiguration {ScaffoldAllTables = true});

            RouteTable.Routes.Add(
                new DynamicDataRoute("{table}/{action}.aspx")
                {
                    Constraints = new RouteValueDictionary(
                        new {action = "List|Details|Edit|Insert"}),
                    Model = model
                });

            DynamicPageInterceptorContainer.Instance
                .RegisterPageInterceptor<UserInfoDto>(new UserInfoPageInteceptor());

            DynamicPageInterceptorContainer.Instance
                .RegisterPageInterceptor<OrganisationRoleDto>(new OrganisationRolePageInterceptor());
        }

        private static void RegisterScriptDefinitions()
        {
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/Scripts/jquery-1.7.1.min.js",
                DebugPath = "~/Scripts/jquery-1.7.1.js",
                CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js",
                CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.js",
                CdnSupportsSecureConnection = true,
                LoadSuccessExpression = "window.jQuery"
            });
        }
    }
}
