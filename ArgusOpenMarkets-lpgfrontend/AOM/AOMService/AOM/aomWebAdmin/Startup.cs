﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AomWebAdmin.Startup))]
namespace AomWebAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
