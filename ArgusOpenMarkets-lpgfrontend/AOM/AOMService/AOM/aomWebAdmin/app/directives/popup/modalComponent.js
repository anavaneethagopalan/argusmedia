System.register(["angular2/core", "angular2/common", "../../users/user.service"], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, user_service_1;
    var ModalComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            ;
            ModalComponent = (function () {
                function ModalComponent(userService) {
                    this.operation = "Unknown";
                    this.isOpen = false;
                    this.username = "Unknown";
                    this.userService = userService;
                }
                ModalComponent.prototype.open = function (user, operation) {
                    this.isOpen = true;
                    this.operation = operation;
                    if (user) {
                        this.user = user;
                        this.username = user.username;
                    }
                };
                ModalComponent.prototype.camelCase = function (title) {
                };
                ModalComponent.prototype.yesOperation = function () {
                    if (this.user) {
                        this.userService.userOperation(this.user, this.operation);
                    }
                    this.close();
                };
                ModalComponent.prototype.noOperation = function () {
                    if (this.user) {
                    }
                    this.close();
                };
                ModalComponent.prototype.close = function () {
                    this.isOpen = false;
                };
                ModalComponent = __decorate([
                    core_1.Component({
                        selector: 'modal',
                        template: "\n    <div class=\"modalPopup container\" *ngIf=\"isOpen\">\n        <div class=\"row\">\n        <span class=\"col-md-10\"><h1>{{operation}} user?</h1></span></div>\n      <ng-content></ng-content>\n\n      <div class=\"row\">\n      <p>Are you sure you want to {{operation}}  the user: {{username}}</p>\n      </div>\n      <div class=\"row\">\n      <button (click)=\"yesOperation()\" class=\"btn btn-primary col-xs-2\">Yes</button>\n      <button (click)=\"noOperation()\" class=\"btn btn-primary col-xs-2\">No</button>\n      </div>\n    </div>\n    <div class=\"overlay\" *ngIf=\"isOpen\" (click)=\"close()\"></div>\n  ",
                        styles: ["\n        .modalPopup {\n          background-color: #A9D5EF;\n          position: relative;\n          z-index: 1000;\n        }\n\n        h1{\n            text-transform:capitalize\n        }\n\n\n        .overlay {\n          top: 0;\n          right: 0;\n          bottom: 0;\n          left: 0;\n          position: fixed;\n          background: rgba(0, 0, 0, 0.5);\n          z-index: 1;\n        }\n    "],
                        directives: [common_1.NgIf]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService])
                ], ModalComponent);
                return ModalComponent;
            })();
            exports_1("ModalComponent", ModalComponent);
        }
    }
});

//# sourceMappingURL=modalComponent.js.map
