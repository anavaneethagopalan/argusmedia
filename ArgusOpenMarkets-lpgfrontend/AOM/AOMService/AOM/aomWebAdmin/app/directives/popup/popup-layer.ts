import {Component} from 'angular2/core';
import {View} from 'angular2/core';
import {NgIf} from 'angular2/common';

@Component({
    selector: 'modal-layer',
    template: '',
    styles: [`
        .overlay {
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          position: fixed;
          opacity:0.5 !important;
          z-index: 1;
        }
    `],
    directives: [NgIf]
})
//TODO
export class ModalLayer {

    constructor() {

    }

}