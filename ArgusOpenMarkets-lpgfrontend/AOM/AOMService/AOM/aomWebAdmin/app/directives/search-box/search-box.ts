import {Component} from "angular2/core";
import {Output} from "angular2/core";
import {EventEmitter} from "angular2/core";

@Component({
    selector: 'search-box',
    template: '<span><input #input placeholder="Filter (user/organisation)" type="text" (input)="update.emit(input.value)" /></span>'
})

export class SearchBox{
    @Output() update = new EventEmitter();

    ngOnInit(){
        this.update.emit('');
    }
}