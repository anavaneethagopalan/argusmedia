System.register(['./home/home.component', './users/users.component', './login/login.component', './organisations/organisations.component'], function(exports_1) {
    var home_component_1, users_component_1, login_component_1, organisations_component_1;
    var APP_ROUTES;
    return {
        setters:[
            function (home_component_1_1) {
                home_component_1 = home_component_1_1;
            },
            function (users_component_1_1) {
                users_component_1 = users_component_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (organisations_component_1_1) {
                organisations_component_1 = organisations_component_1_1;
            }],
        execute: function() {
            exports_1("APP_ROUTES", APP_ROUTES = [
                { path: '/login', name: 'Login', component: login_component_1.LoginComponent },
                { path: '/home', name: 'Home', component: home_component_1.HomeComponent, useAsDefault: true },
                // { path: '/simplebind', name: 'Simplebind', component: SimplebindComponent },
                // { path: '/todolist', name: 'Todolist', component: TodolistComponent },
                { path: '/organisations', name: 'Organisations', component: organisations_component_1.OrganisationsComponent },
                { path: '/users', name: 'Users', component: users_component_1.UserComponent }
            ]);
        }
    }
});

//# sourceMappingURL=app.routes.js.map
