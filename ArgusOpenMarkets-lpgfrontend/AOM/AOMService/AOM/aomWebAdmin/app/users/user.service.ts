import {User} from './user.model';
// import {Http, Response, HTTP_PROVIDERS} from "angular2/http";
// import 'rxjs/add/operator/map';
"use strict";

declare var jQuery:JQueryStatic;

export class UserService {
    // component state is now member variables, no more $scope
    users: Array<User>;
    result: Object;
    req: XMLHttpRequest;
    serviceEndPoint : String = 'http://192.168.56.1:8012/api';

    // constructors do dependency injection in Angular2
    constructor() {
    }

    public getUsers(cb, ctx){
        user: User;
        users: Array[User];

        var dataReq = jQuery.get(this.serviceEndPoint + '/users');
        dataReq.promise().then(function(data){
            if(data){
                this.users = new Array<User>();
                data = JSON.parse(data);


                for (var i = 0; i < data.length; i++){
                    let name = data[i].name + ".";
                    this.user = new User(data[i].id, name, data[i].isActive, data[i].isBlocked, data[i].organisation, data[i].username, data[i].lastSeen);
                    this.users.push(this.user);
                }

                if(cb) {
                    cb(this.users, ctx);
                }
            }
        });
    }

    public userOperation(user, operation){

        switch(operation){
            case "terminate":
                this.terminateUser(user);
                break;

            case "reset":
                this.resetPassword(user);
                break;

            case "blcok":
                this.blockUser(user);
                break;
        }
    }

    public resetPassword(user){

        var userAction = { 'action': 'reset', adminUser: user};

        if(user){
            jQuery.ajax({
                type: 'POST',
                url: this.serviceEndPoint + '/useraction',
                data: userAction
            });
        }
    }

    public terminateUser(user){

        if(user){

            var adminAction = { 'action': 'terminate', adminUser: user};

            jQuery.ajax({
                type: 'POST',
                url: this.serviceEndPoint + '/useraction',
                data: adminAction
            });
        }
    }

    public blockUser(user){

        if(user){

            var adminAction = { 'action': 'block', adminUser: user};

            jQuery.ajax({
                type: 'POST',
                url: this.serviceEndPoint + '/useraction',
                data: adminAction
            });
        }
    }
}