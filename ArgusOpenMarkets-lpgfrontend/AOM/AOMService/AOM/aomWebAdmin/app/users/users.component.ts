import {Component, View} from 'angular2/core';
import {CORE_DIRECTIVES} from 'angular2/common';
import {UserService} from './user.service';
import {User} from "./user.model";
import {SearchPipe} from '../pipes/search-pipe/search-pipe';
import {SearchBox} from '../directives/search-box/search-box';
import {ModalLayer} from '../directives/popup/popup-layer';
import {ModalComponent} from '../directives/popup/modalComponent';

@Component({
    pipes: [SearchPipe],
    selector: 'users',
    templateUrl: 'app/users/users.html',
    styleUrls: [
        'app/users/users.css'
    ],
    directives: [CORE_DIRECTIVES, SearchBox, ModalLayer, ModalComponent],
    providers: [UserService]
})
export class UserComponent {

    public listUsers: User[];
    public self: UserComponent;
    private userService: UserService;
    private modalComponent: ModalComponent;

    constructor(userService: UserService) {

        // this.modalComponent = modalComponent;
        this.userService = userService;
        this.listUsers = new Array<User>();
        this.self = this;

        this.userService.getUsers(this.usersLoaded, this);
    }

    usersLoaded(users, ctx){

        // this.listUsers = new Array<User>();
        ctx.listUsers = new Array<User>();

        for(var i = 0; i < users.length; i++)
        {
            ctx.listUsers.push(users[i]);
        }
    }

    resetPassword(user){

        // this.modalComponent.open();
        this.userService.resetPassword(user);
        this.refreshUsers();
    }

    terminateUser(user){

        this.userService.terminateUser(user);
        this.refreshUsers();
    }

    blockUser(user){

        this.userService.blockUser(user);
        this.refreshUsers();
    }

    refreshUsers(){
        this.userService.getUsers(this.usersLoaded, this);
    }

    disableTerminateUser(user){
        var disabled = true;
        if(user){
            if(user.lastSeen){
                disabled = false;
            }
        }

        return disabled;
    }

    disableBlockUser(user){
        var disabled = true;
        if(user){
            disabled = user.isBlocked;
        }

        return disabled;
    }

    disableResetPassword(user){
        var disabled = true;

        if(user){
            disabled = user.isBlocked;
        }

        return disabled;
    }
}