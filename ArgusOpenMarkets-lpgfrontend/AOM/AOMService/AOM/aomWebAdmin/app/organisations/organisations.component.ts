import {Component} from 'angular2/core';

@Component({
    selector: 'organisations',
    templateUrl: 'app/organisations/organisations.html'
})
export class OrganisationsComponent {
    private myname: string;

    constructor() {
        this.myname = 'Organisations';
    }
}
