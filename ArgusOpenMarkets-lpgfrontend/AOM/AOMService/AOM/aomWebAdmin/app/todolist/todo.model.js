System.register([], function(exports_1) {
    var Todo;
    return {
        setters:[],
        execute: function() {
            Todo = (function () {
                function Todo(name, done) {
                    this.name = name;
                    this.done = done;
                }
                Todo.create = function (todo) {
                    return new Todo(todo.name, todo.done);
                };
                Todo.prototype.check = function () {
                    this.done = !this.done;
                };
                Todo.prototype.clear = function () {
                    this.name = '';
                    this.done = false;
                };
                return Todo;
            })();
            exports_1("Todo", Todo);
        }
    }
});

//# sourceMappingURL=todo.model.js.map
