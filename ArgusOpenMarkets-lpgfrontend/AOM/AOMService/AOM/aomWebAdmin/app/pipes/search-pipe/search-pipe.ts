import {Pipe} from "angular2/core";

@Pipe({
    name: 'search'
})

export class SearchPipe{
    transform(value, [term]){
        return value.filter((item) => item.name.toLowerCase().startsWith(term.toLowerCase()) || item.organisation.toLowerCase().startsWith(term.toLowerCase()));
    }
}