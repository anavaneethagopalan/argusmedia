import {Component} from 'angular2/core';
import {RouterOutlet, RouteConfig} from 'angular2/router';
import {APP_ROUTES} from './app.routes';
import {NavbarComponent} from './navbar/navbar.component';
import {LoggerService} from './blocks/logger.service';
import {UserService} from './users/user.service';
import {ModalComponent} from './directives/popup/modalComponent';
import {LoginService} from './login/login.service';
import {OrganisationsComponent} from './organisations/organisations.component';
import {SecureRouter} from './directives/securerouter/securerouter';

@Component({
    selector: 'main-app',
    templateUrl: 'app/app.html',
    directives: [SecureRouter, NavbarComponent]
})
@RouteConfig(APP_ROUTES)
export class AppComponent {
    private logger: LoggerService;

    constructor(logger: LoggerService) {

        localStorage.clear();

        this.logger = logger;
        this.logger.log('Hello!');
    }
}
