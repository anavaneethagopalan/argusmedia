﻿namespace AOMAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    public partial class OrganisationSubscribedProductPrivileges : Form
    {
        private ICrmAdministrationService CrmAdministrationService;

        private IOrganisationService _organisationService;

        private readonly IBus _bus;

        private OrganisationComboBox _currentOrganisation;

        private ComboBoxItem _currentProduct;

        private bool _runItemCheckCode;

        private long? _orgId;

        public OrganisationSubscribedProductPrivileges(
            ICrmAdministrationService crmAdministrationService,
            IBus bus,
            IOrganisationService organisationService,
            long? organisationId = null)
        {
            CrmAdministrationService = crmAdministrationService;
            _bus = bus;
            _orgId = organisationId;
            _organisationService = organisationService;
            InitializeComponent();
        }

        private void SetOrganisationSubscribedProductPrivileges_Load(object sender, EventArgs e)
        {
            try
            {
                DisplayOrganisations();
                DisplayProducts();
                DisplayOrganisationSubscribedProductPrivileges();
            }
            catch (Exception)
            {
            }

            if (_orgId != null)
            {
                cboOrganisations.SelectedItem = _currentOrganisation;
                cboOrganisations.Enabled = false;
                Text = _currentOrganisation.Description + ": Subscribed Product Privileges";
            }
        }

        private void DisplayProducts()
        {
            var products =
                _bus.Request<GetAllProductDefinitionsRequestRpc, List<ProductDefinitionItem>>(
                    new GetAllProductDefinitionsRequestRpc());
            cboProducts.Items.Clear();
            foreach (var product in products)
            {
                cboProducts.Items.Add(new ComboBoxItem(product.ProductId, product.ProductName));
            }

            _currentProduct = new ComboBoxItem(-1, "");
        }

        private void DisplayOrganisations()
        {
            var organisations = _organisationService.GetOrganisations();
            _currentOrganisation = new OrganisationComboBox(-1, "", OrganisationType.Argus);

            foreach (var org in organisations)
            {
                var co = new OrganisationComboBox(org.Id, org.Name, org.OrganisationType);
                cboOrganisations.Items.Add(co);
                if (_orgId == org.Id)
                {
                    _currentOrganisation = co;
                }
            }
        }

        private void DisplayOrganisationSubscribedProductPrivileges()
        {
            if (_currentOrganisation.Id > 0 && _currentProduct.Id > 0)
            {
                try
                {
                    var productPrivileges = CrmAdministrationService.GetProductPrivileges();

                    var productPrivilegesFilteredByOrgType =
                        productPrivileges.Where(
                            pp =>
                                pp.OrganisationType == _currentOrganisation.OrgType
                                || pp.OrganisationType == OrganisationType.NotSpecified).ToList();

                    _runItemCheckCode = false;
                    lstOrganisationSubscribedPrivileges.Items.Clear();
                    foreach (var pp in productPrivilegesFilteredByOrgType)
                    {
                        bool orgHasPrivilege = DoesOrgHavePrivilege(_currentOrganisation.Id, _currentProduct.Id, pp);
                        lstOrganisationSubscribedPrivileges.Items.Add(new ComboBoxItem(pp.Id, pp.Name), orgHasPrivilege);
                    }
                    _runItemCheckCode = true;

                }
                catch (Exception ex)
                {
                    ExceptionReporter.ShowException(ex, "Error Loading Product Privileges");
                }
            }
        }

        private bool DoesOrgHavePrivilege(long organisationId, long productId, IProductPrivilege pp)
        {
            return
                CrmAdministrationService.GetOrganisationSubscribedProductPrivileges(organisationId)
                    .Any(
                        spp =>
                            spp.OrganisationId == organisationId && spp.ProductId == productId
                            && spp.ProductPrivilegeId == pp.Id);
        }

        private void cboOrganisations_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstOrganisationSubscribedPrivileges.Items.Clear();
            var currentOrg = cboOrganisations.SelectedItem as OrganisationComboBox;
            _currentOrganisation = currentOrg;

            cboProducts.SelectedItem = cboProducts.Items[0];
            DisplayOrganisationSubscribedProductPrivileges();
        }

        private void cboProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstOrganisationSubscribedPrivileges.Visible = false;
            lstOrganisationSubscribedPrivileges.Items.Clear();
            _currentProduct = cboProducts.SelectedItem as ComboBoxItem;

            DisplayOrganisationSubscribedProductPrivileges();
            lstOrganisationSubscribedPrivileges.Visible = true;
        }

        private void lstOrganisationSubscribedPrivileges_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void lstOrganisationsSubscribedProductPriveleges_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (_runItemCheckCode)
            {
                // The user has checked an item.
                if (e != null)
                {
                    var pp = lstOrganisationSubscribedPrivileges.Items[e.Index] as ComboBoxItem;

                    if (pp != null)
                    {
                        if (pp.Id > 0)
                        {
                            if (e.CurrentValue != e.NewValue)
                            {
                                if (e.NewValue == CheckState.Checked)
                                {
                                    AddProductPrivilegeToOrganisationSubscribedProductPrivileges(
                                        _currentOrganisation.Id,
                                        _currentProduct.Id,
                                        pp.Id);

                                    lblInfo.Text = "An error occurred adding the Product Privilege";
                                }
                                else
                                {
                                    // We are removing this role from this user.
                                    var result =
                                        RemoveProductPrivilegeForOrganisationSubscribedProductPrivileges(
                                            _currentOrganisation.Id,
                                            _currentProduct.Id,
                                            pp.Id);

                                    if (!result)
                                    {
                                        lblInfo.Text = "An error occurred removing the Product Privilege";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool RemoveProductPrivilegeForOrganisationSubscribedProductPrivileges(
            long organisationId,
            long productId,
            long productPrivilegeId)
        {
            return CrmAdministrationService.RemoveProductPrivilegeFromSubscribedProductPrivileges(
                organisationId,
                productId,
                productPrivilegeId,
                AdminSettings.LoggedOnUser.Id);
        }

        private void AddProductPrivilegeToOrganisationSubscribedProductPrivileges(
            long organisationId,
            long productId,
            long productPrivilegeId)
        {
            CrmAdministrationService.AddProductPrivilegeToSubscribedProductPrivileges(
                organisationId,
                productId,
                productPrivilegeId,
                AdminSettings.LoggedOnUser.Id);
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}