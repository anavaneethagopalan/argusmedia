﻿namespace AOMAdmin
{
    partial class DiffusionTopicMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lstDiffusionA = new System.Windows.Forms.ListBox();
            this.lstDiffusionB = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdConnectDiffusionA = new System.Windows.Forms.Button();
            this.cmdConnectDiffusionB = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtDiffusionA = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDiffusionB = new System.Windows.Forms.TextBox();
            this.lblA = new System.Windows.Forms.Label();
            this.lblB = new System.Windows.Forms.Label();
            this.cmdConnectToDiffAAndB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstDiffusionA
            // 
            this.lstDiffusionA.FormattingEnabled = true;
            this.lstDiffusionA.ItemHeight = 16;
            this.lstDiffusionA.Location = new System.Drawing.Point(7, 162);
            this.lstDiffusionA.Name = "lstDiffusionA";
            this.lstDiffusionA.Size = new System.Drawing.Size(541, 500);
            this.lstDiffusionA.TabIndex = 0;
            this.lstDiffusionA.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // lstDiffusionB
            // 
            this.lstDiffusionB.FormattingEnabled = true;
            this.lstDiffusionB.ItemHeight = 16;
            this.lstDiffusionB.Location = new System.Drawing.Point(554, 162);
            this.lstDiffusionB.Name = "lstDiffusionB";
            this.lstDiffusionB.Size = new System.Drawing.Size(541, 500);
            this.lstDiffusionB.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Diffusion A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(554, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Diffusion B";
            // 
            // cmdConnectDiffusionA
            // 
            this.cmdConnectDiffusionA.Location = new System.Drawing.Point(0, 0);
            this.cmdConnectDiffusionA.Name = "cmdConnectDiffusionA";
            this.cmdConnectDiffusionA.Size = new System.Drawing.Size(75, 23);
            this.cmdConnectDiffusionA.TabIndex = 14;
            // 
            // cmdConnectDiffusionB
            // 
            this.cmdConnectDiffusionB.Location = new System.Drawing.Point(0, 0);
            this.cmdConnectDiffusionB.Name = "cmdConnectDiffusionB";
            this.cmdConnectDiffusionB.Size = new System.Drawing.Size(75, 23);
            this.cmdConnectDiffusionB.TabIndex = 13;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtDiffusionA
            // 
            this.txtDiffusionA.Location = new System.Drawing.Point(126, 79);
            this.txtDiffusionA.Name = "txtDiffusionA";
            this.txtDiffusionA.Size = new System.Drawing.Size(257, 22);
            this.txtDiffusionA.TabIndex = 6;
            this.txtDiffusionA.Text = "dpt://10.31.4.6:8080";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Diffusion A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(554, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Diffusion B";
            // 
            // txtDiffusionB
            // 
            this.txtDiffusionB.Location = new System.Drawing.Point(673, 79);
            this.txtDiffusionB.Name = "txtDiffusionB";
            this.txtDiffusionB.Size = new System.Drawing.Size(257, 22);
            this.txtDiffusionB.TabIndex = 8;
            this.txtDiffusionB.Text = "dpt://10.31.5.6:8080";
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Location = new System.Drawing.Point(7, 669);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(49, 17);
            this.lblA.TabIndex = 10;
            this.lblA.Text = "Count:";
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Location = new System.Drawing.Point(554, 669);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(49, 17);
            this.lblB.TabIndex = 11;
            this.lblB.Text = "Count:";
            // 
            // cmdConnectToDiffAAndB
            // 
            this.cmdConnectToDiffAAndB.Location = new System.Drawing.Point(384, 29);
            this.cmdConnectToDiffAAndB.Name = "cmdConnectToDiffAAndB";
            this.cmdConnectToDiffAAndB.Size = new System.Drawing.Size(292, 23);
            this.cmdConnectToDiffAAndB.TabIndex = 12;
            this.cmdConnectToDiffAAndB.Text = "Connect to Diffusion A and B";
            this.cmdConnectToDiffAAndB.UseVisualStyleBackColor = true;
            this.cmdConnectToDiffAAndB.Click += new System.EventHandler(this.cmdConnectToDiffAAndB_Click);
            // 
            // DiffusionTopicMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1110, 693);
            this.Controls.Add(this.cmdConnectToDiffAAndB);
            this.Controls.Add(this.lblB);
            this.Controls.Add(this.lblA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDiffusionB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDiffusionA);
            this.Controls.Add(this.cmdConnectDiffusionB);
            this.Controls.Add(this.cmdConnectDiffusionA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstDiffusionB);
            this.Controls.Add(this.lstDiffusionA);
            this.Name = "DiffusionTopicMonitor";
            this.Text = "DiffusionTopicMonitor";
            this.Load += new System.EventHandler(this.DiffusionTopicMonitor_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstDiffusionA;
        private System.Windows.Forms.ListBox lstDiffusionB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdConnectDiffusionA;
        private System.Windows.Forms.Button cmdConnectDiffusionB;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox txtDiffusionA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDiffusionB;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.Button cmdConnectToDiffAAndB;
    }
}