﻿namespace AOMAdmin
{
    partial class OrganisationRoles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrganisationRoles));
            this.label1 = new System.Windows.Forms.Label();
            this.cboOrganisations = new System.Windows.Forms.ComboBox();
            this.lstOrganisationRoles = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRoleDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRoleName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmdCreateOrganisationRole = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdRolePrivs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Organisation";
            // 
            // cboOrganisations
            // 
            this.cboOrganisations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboOrganisations.FormattingEnabled = true;
            this.cboOrganisations.Location = new System.Drawing.Point(12, 30);
            this.cboOrganisations.Name = "cboOrganisations";
            this.cboOrganisations.Size = new System.Drawing.Size(224, 21);
            this.cboOrganisations.TabIndex = 0;
            this.cboOrganisations.SelectedIndexChanged += new System.EventHandler(this.cboOrganisations_SelectedIndexChanged);
            // 
            // lstOrganisationRoles
            // 
            this.lstOrganisationRoles.FormattingEnabled = true;
            this.lstOrganisationRoles.Location = new System.Drawing.Point(12, 86);
            this.lstOrganisationRoles.Name = "lstOrganisationRoles";
            this.lstOrganisationRoles.Size = new System.Drawing.Size(307, 109);
            this.lstOrganisationRoles.TabIndex = 1;
            this.lstOrganisationRoles.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstOrganisationRoles_ItemCheck);
            this.lstOrganisationRoles.SelectedIndexChanged += new System.EventHandler(this.lstOrganisationRoles_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Organisation Roles:";
            // 
            // txtRoleDescription
            // 
            this.txtRoleDescription.Location = new System.Drawing.Point(95, 279);
            this.txtRoleDescription.Name = "txtRoleDescription";
            this.txtRoleDescription.Size = new System.Drawing.Size(224, 20);
            this.txtRoleDescription.TabIndex = 4;
            this.txtRoleDescription.TextChanged += new System.EventHandler(this.txtRoleDescription_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 286);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Description:";
            // 
            // txtRoleName
            // 
            this.txtRoleName.Location = new System.Drawing.Point(95, 253);
            this.txtRoleName.Name = "txtRoleName";
            this.txtRoleName.Size = new System.Drawing.Size(224, 20);
            this.txtRoleName.TabIndex = 3;
            this.txtRoleName.TextChanged += new System.EventHandler(this.txtRoleName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 260);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Name:";
            // 
            // cmdCreateOrganisationRole
            // 
            this.cmdCreateOrganisationRole.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCreateOrganisationRole.Location = new System.Drawing.Point(95, 305);
            this.cmdCreateOrganisationRole.Name = "cmdCreateOrganisationRole";
            this.cmdCreateOrganisationRole.Size = new System.Drawing.Size(142, 27);
            this.cmdCreateOrganisationRole.TabIndex = 5;
            this.cmdCreateOrganisationRole.Text = "Create Organisation Role";
            this.cmdCreateOrganisationRole.UseVisualStyleBackColor = true;
            this.cmdCreateOrganisationRole.Click += new System.EventHandler(this.cmdCreateOrganisationRole_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(243, 305);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 27);
            this.cmdCancel.TabIndex = 6;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdRolePrivs
            // 
            this.cmdRolePrivs.Enabled = false;
            this.cmdRolePrivs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRolePrivs.Location = new System.Drawing.Point(12, 201);
            this.cmdRolePrivs.Name = "cmdRolePrivs";
            this.cmdRolePrivs.Size = new System.Drawing.Size(120, 23);
            this.cmdRolePrivs.TabIndex = 2;
            this.cmdRolePrivs.Text = "Privileges for Role ...";
            this.cmdRolePrivs.UseVisualStyleBackColor = true;
            this.cmdRolePrivs.Click += new System.EventHandler(this.cmdRolePrivs_Click);
            // 
            // OrganisationRoles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 345);
            this.Controls.Add(this.cmdRolePrivs);
            this.Controls.Add(this.cmdCreateOrganisationRole);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.txtRoleDescription);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtRoleName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstOrganisationRoles);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboOrganisations);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrganisationRoles";
            this.Text = "Manage Organisation Roles";
            this.Load += new System.EventHandler(this.ManageOrganisationRoles_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboOrganisations;
        private System.Windows.Forms.CheckedListBox lstOrganisationRoles;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRoleDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRoleName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button cmdCreateOrganisationRole;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdRolePrivs;
    }
}