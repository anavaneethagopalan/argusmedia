﻿using System.Windows.Forms;

namespace AOMAdmin
{
    partial class ProductMetadataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgMetadata = new System.Windows.Forms.DataGridView();
            this.clmDisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmFieldType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.closeButtonLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnClose = new System.Windows.Forms.Button();
            this.buttonsLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCreateMetadata = new System.Windows.Forms.Button();
            this.btnEditMetadata = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEditMetadataLists = new System.Windows.Forms.Button();
            this.btnRefreshDiffusionTopics = new System.Windows.Forms.Button();
            this.btnMoveUp = new System.Windows.Forms.Button();
            this.btnMoveDown = new System.Windows.Forms.Button();
            this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridPanel = new System.Windows.Forms.Panel();
            this.upDownButtonsLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonsPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadata)).BeginInit();
            this.closeButtonLayoutPanel.SuspendLayout();
            this.buttonsLayoutPanel.SuspendLayout();
            this.mainTableLayoutPanel.SuspendLayout();
            this.dataGridPanel.SuspendLayout();
            this.upDownButtonsLayoutPanel.SuspendLayout();
            this.buttonsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgMetadata
            // 
            this.dgMetadata.AllowUserToAddRows = false;
            this.dgMetadata.AllowUserToDeleteRows = false;
            this.dgMetadata.AllowUserToResizeRows = false;
            this.dgMetadata.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMetadata.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMetadata.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgMetadata.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMetadata.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmDisplayName,
            this.clmFieldType});
            this.dgMetadata.Location = new System.Drawing.Point(4, 0);
            this.dgMetadata.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgMetadata.Name = "dgMetadata";
            this.dgMetadata.ReadOnly = true;
            this.dgMetadata.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgMetadata.RowHeadersVisible = false;
            this.dgMetadata.RowHeadersWidth = 20;
            this.dgMetadata.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMetadata.Size = new System.Drawing.Size(941, 192);
            this.dgMetadata.TabIndex = 4;
            this.dgMetadata.DoubleClick += new System.EventHandler(this.EditMetadata_Click);
            // 
            // clmDisplayName
            // 
            this.clmDisplayName.DataPropertyName = "DisplayName";
            this.clmDisplayName.HeaderText = "Display Name";
            this.clmDisplayName.Name = "clmDisplayName";
            this.clmDisplayName.ReadOnly = true;
            // 
            // clmFieldType
            // 
            this.clmFieldType.DataPropertyName = "FieldType";
            this.clmFieldType.HeaderText = "Field Type";
            this.clmFieldType.Name = "clmFieldType";
            this.clmFieldType.ReadOnly = true;
            // 
            // closeButtonLayoutPanel
            // 
            this.closeButtonLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButtonLayoutPanel.AutoSize = true;
            this.closeButtonLayoutPanel.Controls.Add(this.btnClose);
            this.closeButtonLayoutPanel.Location = new System.Drawing.Point(888, 2);
            this.closeButtonLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.closeButtonLayoutPanel.Name = "closeButtonLayoutPanel";
            this.closeButtonLayoutPanel.Size = new System.Drawing.Size(108, 36);
            this.closeButtonLayoutPanel.TabIndex = 3;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 28);
            this.btnClose.TabIndex = 1;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // buttonsLayoutPanel
            // 
            this.buttonsLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonsLayoutPanel.AutoSize = true;
            this.buttonsLayoutPanel.Controls.Add(this.btnCreateMetadata);
            this.buttonsLayoutPanel.Controls.Add(this.btnEditMetadata);
            this.buttonsLayoutPanel.Controls.Add(this.btnDelete);
            this.buttonsLayoutPanel.Controls.Add(this.btnEditMetadataLists);
            this.buttonsLayoutPanel.Controls.Add(this.btnRefreshDiffusionTopics);
            this.buttonsLayoutPanel.Location = new System.Drawing.Point(4, 2);
            this.buttonsLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonsLayoutPanel.Name = "buttonsLayoutPanel";
            this.buttonsLayoutPanel.Size = new System.Drawing.Size(692, 36);
            this.buttonsLayoutPanel.TabIndex = 0;
            // 
            // btnCreateMetadata
            // 
            this.btnCreateMetadata.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateMetadata.Location = new System.Drawing.Point(4, 4);
            this.btnCreateMetadata.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCreateMetadata.Name = "btnCreateMetadata";
            this.btnCreateMetadata.Size = new System.Drawing.Size(100, 28);
            this.btnCreateMetadata.TabIndex = 0;
            this.btnCreateMetadata.TabStop = false;
            this.btnCreateMetadata.Text = "Create...";
            this.btnCreateMetadata.UseVisualStyleBackColor = true;
            this.btnCreateMetadata.Click += new System.EventHandler(this.btnCreateMetadata_Click);
            // 
            // btnEditMetadata
            // 
            this.btnEditMetadata.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditMetadata.Location = new System.Drawing.Point(112, 4);
            this.btnEditMetadata.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditMetadata.Name = "btnEditMetadata";
            this.btnEditMetadata.Size = new System.Drawing.Size(100, 28);
            this.btnEditMetadata.TabIndex = 1;
            this.btnEditMetadata.TabStop = false;
            this.btnEditMetadata.Text = "Edit...";
            this.btnEditMetadata.UseVisualStyleBackColor = true;
            this.btnEditMetadata.Click += new System.EventHandler(this.EditMetadata_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Location = new System.Drawing.Point(220, 4);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 28);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.TabStop = false;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEditMetadataLists
            // 
            this.btnEditMetadataLists.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditMetadataLists.Location = new System.Drawing.Point(328, 4);
            this.btnEditMetadataLists.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditMetadataLists.Name = "btnEditMetadataLists";
            this.btnEditMetadataLists.Size = new System.Drawing.Size(167, 28);
            this.btnEditMetadataLists.TabIndex = 4;
            this.btnEditMetadataLists.Text = "Edit Metadata Lists...";
            this.btnEditMetadataLists.UseVisualStyleBackColor = true;
            this.btnEditMetadataLists.Click += new System.EventHandler(this.btnEditMetadataLists_Click);
            // 
            // btnRefreshDiffusionTopics
            // 
            this.btnRefreshDiffusionTopics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshDiffusionTopics.Location = new System.Drawing.Point(503, 4);
            this.btnRefreshDiffusionTopics.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRefreshDiffusionTopics.Name = "btnRefreshDiffusionTopics";
            this.btnRefreshDiffusionTopics.Size = new System.Drawing.Size(185, 28);
            this.btnRefreshDiffusionTopics.TabIndex = 3;
            this.btnRefreshDiffusionTopics.TabStop = false;
            this.btnRefreshDiffusionTopics.Text = "Rebuild Metadata Topics";
            this.btnRefreshDiffusionTopics.UseVisualStyleBackColor = true;
            this.btnRefreshDiffusionTopics.Click += new System.EventHandler(this.btnRefreshDiffusionTopics_Click);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoveUp.Image = global::AOMAdmin.Properties.Resources.up;
            this.btnMoveUp.Location = new System.Drawing.Point(4, 4);
            this.btnMoveUp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(33, 28);
            this.btnMoveUp.TabIndex = 4;
            this.btnMoveUp.TabStop = false;
            this.btnMoveUp.UseVisualStyleBackColor = true;
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoveDown.Image = global::AOMAdmin.Properties.Resources.down;
            this.btnMoveDown.Location = new System.Drawing.Point(4, 40);
            this.btnMoveDown.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(33, 28);
            this.btnMoveDown.TabIndex = 5;
            this.btnMoveDown.TabStop = false;
            this.btnMoveDown.UseVisualStyleBackColor = true;
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // mainTableLayoutPanel
            // 
            this.mainTableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mainTableLayoutPanel.ColumnCount = 1;
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayoutPanel.Controls.Add(this.dataGridPanel, 0, 0);
            this.mainTableLayoutPanel.Controls.Add(this.buttonsPanel, 0, 1);
            this.mainTableLayoutPanel.Location = new System.Drawing.Point(13, 12);
            this.mainTableLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
            this.mainTableLayoutPanel.RowCount = 2;
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.mainTableLayoutPanel.Size = new System.Drawing.Size(1000, 254);
            this.mainTableLayoutPanel.TabIndex = 1;
            // 
            // dataGridPanel
            // 
            this.dataGridPanel.Controls.Add(this.upDownButtonsLayoutPanel);
            this.dataGridPanel.Controls.Add(this.dgMetadata);
            this.dataGridPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridPanel.Location = new System.Drawing.Point(4, 4);
            this.dataGridPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridPanel.Name = "dataGridPanel";
            this.dataGridPanel.Size = new System.Drawing.Size(992, 196);
            this.dataGridPanel.TabIndex = 2;
            // 
            // upDownButtonsLayoutPanel
            // 
            this.upDownButtonsLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.upDownButtonsLayoutPanel.AutoSize = true;
            this.upDownButtonsLayoutPanel.Controls.Add(this.btnMoveUp);
            this.upDownButtonsLayoutPanel.Controls.Add(this.btnMoveDown);
            this.upDownButtonsLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.upDownButtonsLayoutPanel.Location = new System.Drawing.Point(953, 4);
            this.upDownButtonsLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.upDownButtonsLayoutPanel.Name = "upDownButtonsLayoutPanel";
            this.upDownButtonsLayoutPanel.Size = new System.Drawing.Size(43, 72);
            this.upDownButtonsLayoutPanel.TabIndex = 0;
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonsPanel.Controls.Add(this.closeButtonLayoutPanel);
            this.buttonsPanel.Controls.Add(this.buttonsLayoutPanel);
            this.buttonsPanel.Location = new System.Drawing.Point(4, 208);
            this.buttonsPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(992, 42);
            this.buttonsPanel.TabIndex = 3;
            // 
            // ProductMetadataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(1032, 279);
            this.Controls.Add(this.mainTableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(794, 235);
            this.Name = "ProductMetadataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Product Metadata";
            this.Load += new System.EventHandler(this.ProductMetadataForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadata)).EndInit();
            this.closeButtonLayoutPanel.ResumeLayout(false);
            this.buttonsLayoutPanel.ResumeLayout(false);
            this.mainTableLayoutPanel.ResumeLayout(false);
            this.dataGridPanel.ResumeLayout(false);
            this.dataGridPanel.PerformLayout();
            this.upDownButtonsLayoutPanel.ResumeLayout(false);
            this.buttonsPanel.ResumeLayout(false);
            this.buttonsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel buttonsLayoutPanel;
        private System.Windows.Forms.Button btnCreateMetadata;
        private System.Windows.Forms.Button btnEditMetadata;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRefreshDiffusionTopics;
        private System.Windows.Forms.FlowLayoutPanel closeButtonLayoutPanel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgMetadata;
        private DataGridViewTextBoxColumn clmDisplayName;
        private DataGridViewTextBoxColumn clmFieldType;
        private Button btnMoveUp;
        private Button btnMoveDown;
        private TableLayoutPanel mainTableLayoutPanel;
        private FlowLayoutPanel upDownButtonsLayoutPanel;
        private Panel dataGridPanel;
        private Panel buttonsPanel;
        private Button btnEditMetadataLists;
    }
}