﻿namespace AOMAdmin
{
    using System;
    using System.Windows.Forms;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.Users;

    using Argus.Transport.Infrastructure;

    public partial class UserPasswordChange : Form
    {
        private readonly IBus _bus;
        private readonly IEncryptionService _encryptionService;
        private readonly ICrmAdministrationService CrmAdministrationService;
        private readonly IUserService _userService;
        private User _user;

        private void ChangeUserPassword_Load(object sender, EventArgs e)
        {
            lblMessage.Visible = false;

            if (_user != null)
            {
                txtUserName.Text = _user.Username;
                txtUserName.Enabled = false;
                Text = _user.Name + " (" + _user.Username + "): Change Password";
            }

            SetChangePasswordButtonStatus();
        }

        public UserPasswordChange(IBus bus,  IEncryptionService encryptionService, ICrmAdministrationService crmAdministrationService, IUserService userService, User user = null)
        {
            _bus = bus;
            _encryptionService = encryptionService;
            CrmAdministrationService = crmAdministrationService;
            _user = user;
            _userService = userService;
            InitializeComponent();
        }
        
        private void DisplayChangePasswordMessage(ChangePasswordResponse changePasswordResponse)
        {
            var message = string.Empty;
            if (changePasswordResponse.Success)
            {
                message += "Password has been succesfully changed";
            }
            else
            {
                message += "Error updating the user password";
                message += changePasswordResponse.MessageBody;
            }

            if (lblMessage.InvokeRequired)
            {
                lblMessage.BeginInvoke(
                    (MethodInvoker)delegate
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = message;
                    });
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = message;
            }
        }

        private void cmdChangeUserPassword_Click(object sender, EventArgs e)
        {
            cmdChangeUserPassword.Enabled = false;
            lblMessage.Text = "";
            var username = txtUserName.Text;
            var newPassword = txtNewPassword.Text;
            var tempExpiration = Convert.ToInt32(TempExpirHours.Value);
            var user = new User { Id = _userService.GetUserId(username) };
            var hashedPassword = _encryptionService.Hash(user, newPassword);
            var request = new ChangePasswordRequestRpc
            {
                Username = username,
                AdminResetPassword = true,
                OldPassword = hashedPassword,
                NewPassword = hashedPassword,
                CheckOldPassword = false,
                TempPassExpirInHours = tempExpiration,
                IsTemporary = TempPassCB.Checked,
                RequestorUserId = AdminSettings.LoggedOnUser.Id
            };

            var changePasswordResponse = _bus.Request<ChangePasswordRequestRpc, ChangePasswordResponse>(request);

            DisplayChangePasswordMessage(changePasswordResponse);

            cmdChangeUserPassword.Enabled = true;
        }

        private void SetChangePasswordButtonStatus()
        {
            var username = txtUserName.Text;
            var newPassword = txtNewPassword.Text;
            var newPasswordConfirm = txtNewPasswordConfirm.Text;
            var enableChangePasswordButton = !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(newPassword) && newPassword == newPasswordConfirm;
            cmdChangeUserPassword.Enabled = enableChangePasswordButton;
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            SetChangePasswordButtonStatus();
        }

        private void txtNewPassword_TextChanged(object sender, EventArgs e)
        {
            SetChangePasswordButtonStatus();
        }

        private void txtNewPasswordConfirm_TextChanged(object sender, EventArgs e)
        {
            SetChangePasswordButtonStatus();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}