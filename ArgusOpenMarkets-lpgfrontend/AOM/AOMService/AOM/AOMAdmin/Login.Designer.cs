﻿namespace AOMAdmin
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.cmdLogin = new System.Windows.Forms.Button();
            this.status = new System.Windows.Forms.Label();
            this.groupBoxConnStr = new System.Windows.Forms.GroupBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.groupBoxSetting = new System.Windows.Forms.GroupBox();
            this.buttonTestCrm = new System.Windows.Forms.Button();
            this.buttonTestQ = new System.Windows.Forms.Button();
            this.textBoxMQ = new System.Windows.Forms.TextBox();
            this.labelQ = new System.Windows.Forms.Label();
            this.textBoxCrm = new System.Windows.Forms.TextBox();
            this.labelCrm = new System.Windows.Forms.Label();
            this.groupBoxEnv = new System.Windows.Forms.GroupBox();
            this.comboBoxEnv = new System.Windows.Forms.ComboBox();
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.radioButtonManual = new System.Windows.Forms.RadioButton();
            this.radioButtonEnv = new System.Windows.Forms.RadioButton();
            this.buttonConfig = new System.Windows.Forms.Button();
            this.imageListConfig = new System.Windows.Forms.ImageList(this.components);
            this.toolTipLogin = new System.Windows.Forms.ToolTip(this.components);
            this.groupBoxConnStr.SuspendLayout();
            this.groupBoxSetting.SuspendLayout();
            this.groupBoxEnv.SuspendLayout();
            this.groupBoxOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(120, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(240, 20);
            this.txtUserName.TabIndex = 0;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(12, 9);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(58, 13);
            this.lblUsername.TabIndex = 3;
            this.lblUsername.Text = "Username:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Password:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(120, 46);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(240, 20);
            this.txtPassword.TabIndex = 1;
            // 
            // cmdLogin
            // 
            this.cmdLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLogin.Location = new System.Drawing.Point(285, 89);
            this.cmdLogin.Name = "cmdLogin";
            this.cmdLogin.Size = new System.Drawing.Size(75, 23);
            this.cmdLogin.TabIndex = 2;
            this.cmdLogin.Text = "Login";
            this.cmdLogin.UseVisualStyleBackColor = true;
            this.cmdLogin.Click += new System.EventHandler(this.cmdLogin_Click);
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.ForeColor = System.Drawing.Color.Red;
            this.status.Location = new System.Drawing.Point(36, 94);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(0, 13);
            this.status.TabIndex = 5;
            // 
            // groupBoxConnStr
            // 
            this.groupBoxConnStr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxConnStr.Controls.Add(this.buttonSave);
            this.groupBoxConnStr.Controls.Add(this.groupBoxSetting);
            this.groupBoxConnStr.Controls.Add(this.groupBoxEnv);
            this.groupBoxConnStr.Controls.Add(this.groupBoxOptions);
            this.groupBoxConnStr.Location = new System.Drawing.Point(12, 128);
            this.groupBoxConnStr.Name = "groupBoxConnStr";
            this.groupBoxConnStr.Size = new System.Drawing.Size(353, 181);
            this.groupBoxConnStr.TabIndex = 6;
            this.groupBoxConnStr.TabStop = false;
            this.groupBoxConnStr.Text = "Connection";
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.Location = new System.Drawing.Point(229, 152);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(118, 23);
            this.buttonSave.TabIndex = 6;
            this.buttonSave.Text = "Set as Preference";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // groupBoxSetting
            // 
            this.groupBoxSetting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSetting.Controls.Add(this.buttonTestCrm);
            this.groupBoxSetting.Controls.Add(this.buttonTestQ);
            this.groupBoxSetting.Controls.Add(this.textBoxMQ);
            this.groupBoxSetting.Controls.Add(this.labelQ);
            this.groupBoxSetting.Controls.Add(this.textBoxCrm);
            this.groupBoxSetting.Controls.Add(this.labelCrm);
            this.groupBoxSetting.Location = new System.Drawing.Point(7, 67);
            this.groupBoxSetting.Name = "groupBoxSetting";
            this.groupBoxSetting.Size = new System.Drawing.Size(340, 77);
            this.groupBoxSetting.TabIndex = 5;
            this.groupBoxSetting.TabStop = false;
            this.groupBoxSetting.Text = "Setting";
            // 
            // buttonTestCrm
            // 
            this.buttonTestCrm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTestCrm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTestCrm.Location = new System.Drawing.Point(294, 48);
            this.buttonTestCrm.Name = "buttonTestCrm";
            this.buttonTestCrm.Size = new System.Drawing.Size(40, 23);
            this.buttonTestCrm.TabIndex = 1;
            this.buttonTestCrm.Text = "Test";
            this.buttonTestCrm.UseVisualStyleBackColor = true;
            this.buttonTestCrm.Click += new System.EventHandler(this.buttonTestCrm_Click);
            // 
            // buttonTestQ
            // 
            this.buttonTestQ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTestQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTestQ.Location = new System.Drawing.Point(294, 19);
            this.buttonTestQ.Name = "buttonTestQ";
            this.buttonTestQ.Size = new System.Drawing.Size(40, 23);
            this.buttonTestQ.TabIndex = 0;
            this.buttonTestQ.Text = "Test";
            this.buttonTestQ.UseVisualStyleBackColor = true;
            this.buttonTestQ.Click += new System.EventHandler(this.buttonTestQ_Click);
            // 
            // textBoxMQ
            // 
            this.textBoxMQ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMQ.Location = new System.Drawing.Point(78, 20);
            this.textBoxMQ.Name = "textBoxMQ";
            this.textBoxMQ.ReadOnly = true;
            this.textBoxMQ.Size = new System.Drawing.Size(210, 20);
            this.textBoxMQ.TabIndex = 3;
            // 
            // labelQ
            // 
            this.labelQ.AutoSize = true;
            this.labelQ.Location = new System.Drawing.Point(6, 24);
            this.labelQ.Name = "labelQ";
            this.labelQ.Size = new System.Drawing.Size(58, 13);
            this.labelQ.TabIndex = 2;
            this.labelQ.Text = "RabbitMQ:";
            // 
            // textBoxCrm
            // 
            this.textBoxCrm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCrm.Location = new System.Drawing.Point(78, 49);
            this.textBoxCrm.Name = "textBoxCrm";
            this.textBoxCrm.ReadOnly = true;
            this.textBoxCrm.Size = new System.Drawing.Size(210, 20);
            this.textBoxCrm.TabIndex = 5;
            // 
            // labelCrm
            // 
            this.labelCrm.AutoSize = true;
            this.labelCrm.Location = new System.Drawing.Point(6, 53);
            this.labelCrm.Name = "labelCrm";
            this.labelCrm.Size = new System.Drawing.Size(66, 13);
            this.labelCrm.TabIndex = 4;
            this.labelCrm.Text = "CRM Model:";
            // 
            // groupBoxEnv
            // 
            this.groupBoxEnv.Controls.Add(this.comboBoxEnv);
            this.groupBoxEnv.Location = new System.Drawing.Point(229, 19);
            this.groupBoxEnv.Name = "groupBoxEnv";
            this.groupBoxEnv.Size = new System.Drawing.Size(118, 42);
            this.groupBoxEnv.TabIndex = 4;
            this.groupBoxEnv.TabStop = false;
            this.groupBoxEnv.Text = "Environment";
            // 
            // comboBoxEnv
            // 
            this.comboBoxEnv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEnv.FormattingEnabled = true;
            this.comboBoxEnv.Location = new System.Drawing.Point(14, 15);
            this.comboBoxEnv.Name = "comboBoxEnv";
            this.comboBoxEnv.Size = new System.Drawing.Size(91, 21);
            this.comboBoxEnv.TabIndex = 0;
            this.comboBoxEnv.SelectedIndexChanged += new System.EventHandler(this.comboBoxEnv_SelectedIndexChanged);
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.Controls.Add(this.radioButtonManual);
            this.groupBoxOptions.Controls.Add(this.radioButtonEnv);
            this.groupBoxOptions.Location = new System.Drawing.Point(7, 19);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(216, 42);
            this.groupBoxOptions.TabIndex = 3;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "Options";
            // 
            // radioButtonManual
            // 
            this.radioButtonManual.AutoSize = true;
            this.radioButtonManual.Location = new System.Drawing.Point(117, 19);
            this.radioButtonManual.Name = "radioButtonManual";
            this.radioButtonManual.Size = new System.Drawing.Size(93, 17);
            this.radioButtonManual.TabIndex = 1;
            this.radioButtonManual.Text = "Manual Config";
            this.radioButtonManual.UseVisualStyleBackColor = true;
            // 
            // radioButtonEnv
            // 
            this.radioButtonEnv.AutoSize = true;
            this.radioButtonEnv.Checked = true;
            this.radioButtonEnv.Location = new System.Drawing.Point(6, 19);
            this.radioButtonEnv.Name = "radioButtonEnv";
            this.radioButtonEnv.Size = new System.Drawing.Size(99, 17);
            this.radioButtonEnv.TabIndex = 0;
            this.radioButtonEnv.TabStop = true;
            this.radioButtonEnv.Text = "By Environment";
            this.radioButtonEnv.UseVisualStyleBackColor = true;
            this.radioButtonEnv.CheckedChanged += new System.EventHandler(this.radioButtonEnv_CheckedChanged);
            // 
            // buttonConfig
            // 
            this.buttonConfig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfig.ImageIndex = 0;
            this.buttonConfig.ImageList = this.imageListConfig;
            this.buttonConfig.Location = new System.Drawing.Point(255, 91);
            this.buttonConfig.Name = "buttonConfig";
            this.buttonConfig.Size = new System.Drawing.Size(24, 18);
            this.buttonConfig.TabIndex = 7;
            this.toolTipLogin.SetToolTip(this.buttonConfig, "Show/Hide Connection Configuration");
            this.buttonConfig.UseVisualStyleBackColor = true;
            this.buttonConfig.Click += new System.EventHandler(this.buttonConfig_Click);
            // 
            // imageListConfig
            // 
            this.imageListConfig.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListConfig.ImageStream")));
            this.imageListConfig.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListConfig.Images.SetKeyName(0, "down.png");
            this.imageListConfig.Images.SetKeyName(1, "up.png");
            // 
            // Login
            // 
            this.AcceptButton = this.cmdLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(377, 321);
            this.Controls.Add(this.buttonConfig);
            this.Controls.Add(this.groupBoxConnStr);
            this.Controls.Add(this.status);
            this.Controls.Add(this.cmdLogin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.txtUserName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(393, 164);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AOM Admin Login";
            this.groupBoxConnStr.ResumeLayout(false);
            this.groupBoxSetting.ResumeLayout(false);
            this.groupBoxSetting.PerformLayout();
            this.groupBoxEnv.ResumeLayout(false);
            this.groupBoxOptions.ResumeLayout(false);
            this.groupBoxOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button cmdLogin;
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.GroupBox groupBoxConnStr;
        private System.Windows.Forms.GroupBox groupBoxSetting;
        private System.Windows.Forms.Button buttonTestCrm;
        private System.Windows.Forms.Button buttonTestQ;
        private System.Windows.Forms.TextBox textBoxMQ;
        private System.Windows.Forms.Label labelQ;
        private System.Windows.Forms.TextBox textBoxCrm;
        private System.Windows.Forms.Label labelCrm;
        private System.Windows.Forms.GroupBox groupBoxEnv;
        private System.Windows.Forms.ComboBox comboBoxEnv;
        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.RadioButton radioButtonManual;
        private System.Windows.Forms.RadioButton radioButtonEnv;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonConfig;
        private System.Windows.Forms.ImageList imageListConfig;
        private System.Windows.Forms.ToolTip toolTipLogin;
    }
}

