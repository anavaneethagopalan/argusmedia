﻿namespace AOMAdmin
{
    using System;
    using System.Windows.Forms;

    using AOM.App.Domain.Dates;
    using AOM.Transport.Events.News;

    using Argus.Transport.Infrastructure;

    public partial class PurgeNews : Form
    {
        private readonly IBus _bus;

        private readonly IDateTimeProvider _dateTimeProvider;

        public PurgeNews(IBus bus, IDateTimeProvider dateTimeProvider)
        {
            _bus = bus;
            _dateTimeProvider = dateTimeProvider;
            InitializeComponent();
            dtpPurgeDate.MaxDate = _dateTimeProvider.Now;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            const string dialogText = "Are you sure you wish to purge news items?";
            const string dialogCaption = "Purge News";

            DialogResult dialogResult = MessageBox.Show(dialogText, dialogCaption, MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                var request = new PurgeNewsRequest
                {
                    // Since the replay says that it should include the selected date we add one
                    DaysToKeep = (_dateTimeProvider.Today - dtpPurgeDate.Value.Date).Days + 1
                };

                _bus.Publish(request);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmdReplayArgusNews_Click(object sender, EventArgs e)
        {
            string dtr = txtNumDaysToGoBack.Text;
            string cs = txtContentStreams.Text;

            int daysToReplay;
            Int32.TryParse(dtr, out daysToReplay);

            int csNum;
            Int32.TryParse(cs, out csNum);
            var csReplay = new decimal[1] { csNum };

            _bus.Publish(new ReplayArgusNews {DaysToReplay = daysToReplay, ContentStreamsReplay = csReplay});
        }

        private void PurgeNews_Load(object sender, EventArgs e)
        {

        }
    }
}