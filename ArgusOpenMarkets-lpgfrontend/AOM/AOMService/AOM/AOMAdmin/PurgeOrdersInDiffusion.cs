﻿using AOM.Transport.Events;

namespace AOMAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using AOM.App.Domain.Entities;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    public partial class PurgeOrdersInDiffusion : Form
    {
        private readonly IBus _bus;

        public PurgeOrdersInDiffusion(IBus bus)
        {
            _bus = bus;
            InitializeComponent();
        }

        internal void cmdPurgeOrdersInDiffusion_Click(object sender, EventArgs e)
        {
            var productsSelected = new List<long>();
            foreach (var p in lstProducts.CheckedItems)
            {
                var item = p as ComboBoxItem;
                if (item != null)
                {
                    productsSelected.Add(item.Id);
                }
            }

            if (productsSelected.Count <= 0)
            {
                MessageBox.Show(@"Cannot delete a blank topic", "Purge Orders", MessageBoxButtons.OK);
            }
            else
            {
                var purgeRequest = new PurgeOrdersInDiffusionRequest {ProductsToPurgeOrdersFor = productsSelected};
                _bus.Publish(purgeRequest);
                Close();
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PurgeOrdersInDiffusion_Load(object sender, EventArgs e)
        {

            var products =
                _bus.Request<GetAllProductDefinitionsRequestRpc, List<ProductDefinitionItem>>(
                    new GetAllProductDefinitionsRequestRpc());

            lstProducts.Items.Clear();

            foreach (var product in products)
            {
                lstProducts.Items.Add(new ComboBoxItem(product.ProductId, product.ProductName), false);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var productsSelected = new List<long>();
            foreach (var p in lstProducts.CheckedItems)
            {
                var item = p as ComboBoxItem;
                if (item != null)
                {
                    productsSelected.Add(item.Id);
                }
            }

            if (productsSelected.Count <= 0)
            {
                MessageBox.Show(@"Cannot delete a blank topic", "Purge Orders", MessageBoxButtons.OK);
            }
            else
            {
                foreach (var prodId in productsSelected)
                {
                    var request = new AomProductPurgeOrdersRequest
                    {
                        ClientSessionInfo =
                            new ClientSessionInfo
                            {
                                UserId = -1
                            },
                        MessageAction = MessageAction.Purge,
                        Product = new Product { ProductId = prodId }
                    };

                    _bus.Publish(request);
                }

            }
        }
    }
}