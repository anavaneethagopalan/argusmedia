﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class CreateEmails : Form
    {
        private readonly IBus _bus;
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly IEncryptionService _encryptionService;

        public CreateEmails(IBus bus, IUserService userService, IOrganisationService organisationService, IEncryptionService encryptionService)
        {
            _bus = bus;
            _userService = userService;
            _organisationService = organisationService;
            _encryptionService = encryptionService;
            InitializeComponent();
        }

        private void cmdDeleteDiffusionTopic_Click(object sender, EventArgs e)
        {
            var subject = txtSubject.Text;
            var body = txtBody.Text;
            var orgId = GetId(txtOrganisationId.Text);
            var userId = GetId(txtUserId.Text);

            IList<User> users = new List<User>();
            if (orgId > 0)
            {
                users = _userService.GetUsers(orgId);
            }
            else
            {
                var user = _userService.GetUserWithPrivileges(userId);
                users.Add(user as User);
            }

            foreach (var user in users)
            {
                var temporaryPassword = GenerateTemporaryPassword();
                var result = SetTemporaryPasswordForUser(user, temporaryPassword);
                if (result)
                {
                    OutlookAutomate.AutomateOutlook(subject, body, user, temporaryPassword);
                }
            }
        }

        private int GetId(string idValue)
        {
            var id = 0;
            int.TryParse(idValue, out id);

            return id;
        }

        private bool SetTemporaryPasswordForUser(User user, string temporaryPassword)
        {
            var hashedPassword = _encryptionService.Hash(user, temporaryPassword);

            var request = new ChangePasswordRequestRpc
            {
                Username = user.Username,
                AdminResetPassword = true,
                OldPassword = hashedPassword,
                NewPassword = hashedPassword,
                CheckOldPassword = false,
                TempPassExpirInHours = 48,
                IsTemporary = true,
                RequestorUserId = AdminSettings.LoggedOnUser.Id
            };

            var response = _bus.Request<ChangePasswordRequestRpc, ChangePasswordResponse>(request);
            return response.Success;
        }

        private string GenerateTemporaryPassword()
        {
            var tempPassword = "vJTu1W" + DateTime.Now.Second;
            return tempPassword;
        }

        private void txtBody_TextChanged(object sender, EventArgs e)
        {

        }
    }
}