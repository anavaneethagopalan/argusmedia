﻿using System.Configuration;

namespace AOMAdmin
{
    public class EnvConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true, IsKey = false, IsRequired = true)]
        public EnvConfigCollection Configs
        {
            get
            {
                return base[""] as EnvConfigCollection;
            }

            set
            {
                base[""] = value;
            }
        }
    }
}
