﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using AOM.App.Domain.Entities;
using AOMAdmin.CsvHelpers;
using AOMAdmin.Helpers;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class CsvImportForm : Form
    {
        private ICsvImporter _csvImporter;

        public CsvImportForm(ICsvImporter csvImporter)
        {
            _csvImporter = csvImporter;
            InitializeComponent();
        }

        private void CsvImportOrganisations_Load(object sender, EventArgs e)
        {
            EnableButton();
        }

        public string CsvHeaderText
        {
            get { return txtCsvHeader.Text; }
            set { txtCsvHeader.Text = value; }
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            string filename = "";

            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                filename = ofd.FileName;
            }

            if (!string.IsNullOrEmpty(filename))
            {
                txtCsvFile.Text = filename;
                LoadCsvFile(filename);
            }
        }

        private async void btnImport_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 1;
            progressBar1.Maximum = lstFileContent.Items.Count + 2;
            progressBar1.Step = 1;

            txtResults.Text = "";
            var progress = new Progress<ProgressResult>(result =>
            {
                progressBar1.Value = result.ProgressCount;

                if (result.ProgressMessage == null) return;
                txtResults.AppendText(result.ProgressMessage);
            });

            await Task.Run(() => SaveProgress(progress));
            progressBar1.Value = progressBar1.Maximum;

            txtResults.AppendText("Finished saving records...");
            lstFileContent.Items.Clear();
            EnableButton();
        }

        private async void LoadCsvFile(string filename)
        {
            progressBar1.Maximum = File.ReadAllLines(filename).Length;
            progressBar1.Step = 1;

            lstFileContent.Items.Clear();
            txtResults.Text = "";
            
            var progress = new Progress<ProgressResult>(result =>
            {
                progressBar1.Value = result.ProgressCount;

                if (result.ProgressMessage == null) return;
                
                if (result.ProgressSucceeded)
                    lstFileContent.Items.Add(result.ProgressMessage);
                else
                    txtResults.AppendText(result.ProgressMessage);
            });

            await Task.Run(() => LoadProgress(progress, filename));
            progressBar1.Value = progressBar1.Maximum;
            txtResults.AppendText("Finished loading file...");
            EnableButton();
        }

        private void LoadProgress(IProgress<ProgressResult> progress, string filename)
        {
            bool firstLine = true;
            int lineNo = 1;
            string[] file = File.ReadAllLines(filename);
            
            foreach (string line in file)
            {
                ProgressResult pr = new ProgressResult();
                pr.ProgressCount = lineNo++;
                
                if (!firstLine)
                {
                    ValidationResponse validationResponse = _csvImporter.ValidateCsvLine(line);

                    if (validationResponse.IsValid)
                    {
                        //lstFileContent.Items.Add(line);
                        pr.ProgressMessage = line;
                        pr.ProgressSucceeded = true;
                    }
                    else
                    {
                        // Add this line to the list of line errors.  
                        //txtResults.AppendText(string.Format("ERROR: {0} - {1}", validationResponse.ErrorMessage, line) + Environment.NewLine);
                        pr.ProgressMessage = string.Format("ERROR: {0} - {1}", validationResponse.ErrorMessage, line) + Environment.NewLine;
                        pr.ProgressSucceeded = false;
                    }
                }

                firstLine = false;

                if (progress != null)
                    progress.Report(pr);
            }
        }

        private void EnableButton()
        {
            btnImport.Enabled = lstFileContent.Items.Count > 0;
        }

        private void SaveProgress(IProgress<ProgressResult> progress)
        {
            int lineNo = 1;

            foreach (string line in lstFileContent.Items)
            {
                ProgressResult pr = new ProgressResult();
                pr.ProgressCount = lineNo++;

                pr.ProgressMessage = _csvImporter.ImportCsvLine(line);
                pr.ProgressMessage += Environment.NewLine;

                if (progress != null)
                    progress.Report(pr);
            }
        }
    }
}