﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using AOM.Services.CrmService;
using AOM.Services.EncryptionService;

namespace AOMAdmin
{
    public partial class OrganisationEditDetails : Form
    {
        private readonly IBus _bus;

        private OrganisationAdminEntities _entities;
        private readonly ICrmAdministrationService _crmAdministrationService;
        private readonly IOrganisationService _organisationService;
        private readonly IUserService _userService;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly ICredentialsService _credentialsService;

        private OrganisationComboBox _currentOrganisation;
        private Organisation _passedOrganisation;

        private IEnumerable<SubscribedProductPrivilege> _productPrivileges;

        private bool _shouldValidateOrganisation;

        public OrganisationEditDetails(IBus bus,
            ICrmAdministrationService crmAdministrationService,
            IOrganisationService organisationService,
            IUserService userService,
            IDateTimeProvider dateTimeProvider,
            ICredentialsService credentialsService,
            Organisation organisation = null)
        {
            _bus = bus;
            _crmAdministrationService = crmAdministrationService;
            _passedOrganisation = organisation;
            _userService = userService;
            _dateTimeProvider = dateTimeProvider;
            _organisationService = organisationService;
            _credentialsService = credentialsService;
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you wish to save organisation data now?\n" +
                                "Saving data during trading hours can have a serious performance impact!",
                "Save changes?",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) == DialogResult.OK)
            {
                lblMessage.Text = "";
                var organisation = CreateOrganisationFromFields();

                try
                {
                    _crmAdministrationService.SaveOrganisation(organisation,
                        AdminSettings.LoggedOnUser.Id,
                        _organisationService);

                }
                catch (DbUpdateException ex)
                {
                    MessageBox.Show(
                        "There was an error saving the Organisation: " + ex.Message, "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (SaveSubscribedProductPriviliges())
                {
                    MessageBox.Show(
                        "Organisation saved sucessfully!", "Great success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show(
                        "There was an error saving the Organisation Subscribed Product Privileges\n" +
                        "Subscriptions that are used in existing user roles cannot be removed.", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    RefreshProducts();
                    return;
                }

                PublishOrganisationUpdated(organisation);
                Close();

            }
        }


        private class SubscribedProductPrivilegeComparer : IEqualityComparer<SubscribedProductPrivilege>
        {

            public bool Equals(SubscribedProductPrivilege x, SubscribedProductPrivilege y)
            {
                return x.OrganisationId == y.OrganisationId
                       && x.ProductId == y.ProductId
                       && x.ProductPrivilegeId == y.ProductPrivilegeId;
            }

            public int GetHashCode(SubscribedProductPrivilege obj)
            {
                return obj.GetHashCode();
            }
        }

        private IEnumerable<SubscribedProductPrivilege> CalculateAddedSubscribedProductPrivileges(
            IEnumerable<SubscribedProductPrivilege> original,
            IEnumerable<SubscribedProductPrivilege> updated)
        {
            return updated.Where(priv => !original.Contains(priv,
                new SubscribedProductPrivilegeComparer()));
        }

        private IEnumerable<SubscribedProductPrivilege> CalculateRemovedSubscribedProductPrivileges(
            IEnumerable<SubscribedProductPrivilege> original,
            IEnumerable<SubscribedProductPrivilege> updated)
        {
            return original.Where(priv => !updated.Contains(priv,
                new SubscribedProductPrivilegeComparer()));
        }

        private bool SaveSubscribedProductPriviliges()
        {
            bool success = true;
            // Save SubscribedProduct and SubscribedProductPrivileges
            foreach (var priv in CalculateAddedSubscribedProductPrivileges(
                _productPrivileges,
                _entities.ProductsAndPrivileges.SelectMany(pp => pp.ProductPrivileges)))
            {
                success =
                    _crmAdministrationService.AddProductPrivilegeToSubscribedProductPrivileges(
                        _entities.Organisation.Id,
                        priv.ProductId, priv.ProductPrivilegeId, AdminSettings.LoggedOnUser.Id);
            }

            foreach (var priv in CalculateRemovedSubscribedProductPrivileges(
                _productPrivileges,
                _entities.ProductsAndPrivileges.SelectMany(pp => pp.ProductPrivileges)))
            {
                success =
                    _crmAdministrationService.RemoveProductPrivilegeFromSubscribedProductPrivileges(
                        _entities.Organisation.Id,
                        priv.ProductId, priv.ProductPrivilegeId, AdminSettings.LoggedOnUser.Id);
            }


            return success;
        }

        private void PublishOrganisationUpdated(Organisation organisation)
        {
            if (organisation != null)
            {
                var organisationUpdatedEvent = new OrganisationUpdatedEvent
                {
                    Organisation = organisation,
                    UserId = AdminSettings.LoggedOnUser.Id
                };

                _bus.Publish(organisationUpdatedEvent);
            }
        }

        private Organisation CreateOrganisationFromFields()
        {
            var selectedOrgType = cboOrganisationType.SelectedItem as OrganisationComboBox;
            var organisation = new Organisation
            {
                Address = txtAddress.Text,
                DateCreated = _dateTimeProvider.UtcNow,
                Description = txtDescription.Text,
                Id = -1,
                IsDeleted = chkIsDeleted.Checked,
                LegalName = txtLegalName.Text,
                ShortCode = txtShortCode.Text,
                Name = txtName.Text,
                Email = txtEmail.Text,
                OrganisationType = selectedOrgType.OrgType
            };

            var organisationId = txtId.Text;
            int orgId;
            Int32.TryParse(organisationId, out orgId);

            if (orgId > 0)
            {
                // We are updating an existing organisation.  
                organisation.Id = orgId;
            }
            return organisation;
        }

        private IOrganisation GetOrganisation(long id)
        {
            return _organisationService.GetOrganisationById(id);
        }

        private void DisplayOrganisations()
        {
            var organisations = _organisationService.GetOrganisations();
            foreach (var org in organisations)
            {
                var oc = new OrganisationComboBox(org.Id, org.Name, org.OrganisationType);
                cboOrganisations.Items.Add(oc);
                if (_passedOrganisation != null && _passedOrganisation.Id == org.Id)
                {
                    cboOrganisations.SelectedItem = oc;
                }
            }
            if (_passedOrganisation != null)
            {
                cboOrganisations.Enabled = false;
                cmdNewOrganisation.Visible = false;
            }
        }

        private void CreateOrganisation_Load(object sender, EventArgs e)
        {
            DisplayOrganisationTypes();
            DisplayOrganisations();
            RefreshProducts();
        }

        private void EditOrganisation_Shown(object sender, EventArgs e)
        {
            _shouldValidateOrganisation = true;
            SetSaveButtonEnabledState();
        }

        private void DisplayOrganisationTypes()
        {
            var orgComboItem = new OrganisationComboBox(1, "Trader", OrganisationType.Trading);
            cboOrganisationType.Items.Add(orgComboItem);

            orgComboItem = new OrganisationComboBox(2, "Argus", OrganisationType.Argus);
            cboOrganisationType.Items.Add(orgComboItem);

            orgComboItem = new OrganisationComboBox(3, "Broker", OrganisationType.Brokerage);
            cboOrganisationType.Items.Add(orgComboItem);

            orgComboItem = new OrganisationComboBox(4, "Not Specified", OrganisationType.NotSpecified);
            cboOrganisationType.Items.Add(orgComboItem);
        }

        private void cmdNewOrganisation_Click(object sender, EventArgs e)
        {
            DisplayOrganisation(null);

        }

        private void cboOrganisations_SelectedIndexChanged(object sender, EventArgs e)
        {
            _currentOrganisation = cboOrganisations.SelectedItem as OrganisationComboBox;

            if (_currentOrganisation != null)
            {
                // 
                var organisation = GetOrganisation(_currentOrganisation.Id);
                DisplayOrganisation(organisation);
                cmdSubscribedPrivs.Enabled = true;
                cmdRoles.Enabled = true;
                cmdUsers.Enabled = true;
            }

        }

        private void DisplayOrganisation(IOrganisation organisation)
        {
            if (organisation != null)
            {
                txtId.Text = organisation.Id.ToString(CultureInfo.InvariantCulture);
                txtAddress.Text = organisation.Address;
                txtDescription.Text = organisation.Description;
                chkIsDeleted.Checked = organisation.IsDeleted;
                txtShortCode.Text = organisation.ShortCode;
                txtLegalName.Text = organisation.LegalName;
                txtName.Text = organisation.Name;
                txtEmail.Text = organisation.Email;
                DisplayOrganisationType(organisation.OrganisationType);
                cmdSave.Text = "Update";
            }
            else
            {
                txtId.Text = "-1";
                cboOrganisations.SelectedIndex = -1;
                txtAddress.Text = "";
                txtDescription.Text = "";
                chkIsDeleted.Checked = false;
                txtShortCode.Text = "";
                txtLegalName.Text = "";
                txtEmail.Text = "";
                Name = txtName.Text = "";
                cmdSave.Text = "Create";
            }
        }

        private void DisplayOrganisationType(OrganisationType organisationType)
        {
            int index = 0;
            foreach (var orgType in cboOrganisationType.Items)
            {
                var orgCombo = orgType as OrganisationComboBox;
                if (orgCombo.OrgType == organisationType)
                {
                    cboOrganisationType.SelectedIndex = index;
                }

                index = index + 1;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var frm = new OrganisationRolePrivileges(
                _crmAdministrationService,
                _organisationService,
                _bus,
                null,
                _currentOrganisation.Id);

            // var frm = new OrganisationSubscribedProductPrivileges(CrmAdministrationService, _bus,_organisationService, _currentOrganisation.Id);

            try
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                ExceptionReporter.ShowException(ex, "Form Load Error");
            }

        }

        private void cmdRoles_Click(object sender, EventArgs e)
        {
            var frm = new OrganisationRoles(_crmAdministrationService, _bus, _organisationService,
                _currentOrganisation.Id) {StartPosition = FormStartPosition.CenterParent};
            frm.ShowDialog();
        }

        private void cmdUsers_Click(object sender, EventArgs e)
        {
            var frm = new UserMain(_crmAdministrationService, _bus, _userService, _organisationService,
                new EncryptionService(), _credentialsService, _organisationService.GetOrganisationById(_currentOrganisation.Id) as Organisation)
            {
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();

        }

        private void txtShortCode_TextChanged(object sender, EventArgs e)
        {
            SetSaveButtonEnabledState();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            SetSaveButtonEnabledState();
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            SetSaveButtonEnabledState();
        }

        private void SetSaveButtonEnabledState()
        {
            if (_shouldValidateOrganisation)
            {
                var organisation = CreateOrganisationFromFields();
                cmdSave.Enabled = _organisationService.IsOrganisationValid(organisation);
            }
        }

        private void RefreshProducts()
        {
            _productPrivileges = _crmAdministrationService.GetOrganisationSubscribedProductPrivileges(
                _passedOrganisation.Id);

            _entities = new OrganisationAdminEntities
            {
                Organisation = _passedOrganisation, //new Organisation(),
                ProductsAndPrivileges = new List<OrganisationProductAndPrivileges>()
            };

            var products =
                _bus.Request<GetAllActiveProductsRequestRpc, List<Product>>(new GetAllActiveProductsRequestRpc())
                    .ToList();
            foreach (var prod in products)
            {
                var pp = new OrganisationProductAndPrivileges
                {
                    Product = prod,
                    ProductPrivileges = _productPrivileges.Where(x => x.ProductId == prod.ProductId).ToList(),
                };

                _entities.ProductsAndPrivileges.Add(pp);
            }


            DisplayProductPrivilegeSubscriptionsInGrid(_entities);
        }

        private void DisplayProductPrivilegeSubscriptionsInGrid(OrganisationAdminEntities spp)
        {
            dgProducts.Rows.Clear();
            dgProducts.Refresh();

            int productPriviligeIndex = 0;
            foreach (var pp in spp.ProductsAndPrivileges)
            {
                var isProductSubscribed = !(pp.ProductPrivileges.Count < 1);
                dgProducts.Rows.Add(productPriviligeIndex++, pp.Product.Name, isProductSubscribed, "Configure...");
            }
            dgProducts.Refresh();

        }

        private void dgProducts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                var nullEntityIndex = dgProducts.Rows[e.RowIndex].Cells[0].Value as int?;
                var entityIndex = nullEntityIndex ?? 0;
                var prodAndPrivs = _entities.ProductsAndPrivileges[entityIndex];

                switch (e.ColumnIndex)
                {
                    case 3:
                        ShowProductPrivileges(prodAndPrivs);
                        break;
                    default:
                        MessageBox.Show("Bug in code - unexpected button index.",
                            "Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button1);
                        break;
                }
            }
        }

        private void ShowProductPrivileges(OrganisationProductAndPrivileges pp)
        {
            //MapOrganisationFields();
            var frm = new OrganisationSelectProductPrivileges(_crmAdministrationService, _organisationService, _bus,
                _entities.Organisation, pp, new DateTimeProvider())
            {
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();

            //_productPrivileges = _entities.ProductsAndPrivileges.SelectMany(x => x.ProductPrivileges).ToList();
            DisplayProductPrivilegeSubscriptionsInGrid(_entities);
        }
    }
}