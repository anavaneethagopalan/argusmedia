﻿namespace AOMAdmin
{
    partial class OrganisationSelectProductPrivileges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrganisationSelectProductPrivileges));
            this.lstOrganisationSubscribedPrivileges = new System.Windows.Forms.CheckedListBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cbSelectAll = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lstOrganisationSubscribedPrivileges
            // 
            this.lstOrganisationSubscribedPrivileges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstOrganisationSubscribedPrivileges.CheckOnClick = true;
            this.lstOrganisationSubscribedPrivileges.FormattingEnabled = true;
            this.lstOrganisationSubscribedPrivileges.Location = new System.Drawing.Point(12, 12);
            this.lstOrganisationSubscribedPrivileges.Name = "lstOrganisationSubscribedPrivileges";
            this.lstOrganisationSubscribedPrivileges.Size = new System.Drawing.Size(333, 259);
            this.lstOrganisationSubscribedPrivileges.TabIndex = 0;
            this.lstOrganisationSubscribedPrivileges.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstOrganisationsSubscribedProductPriveleges_ItemCheck);
            this.lstOrganisationSubscribedPrivileges.SelectedIndexChanged += new System.EventHandler(this.lstOrganisationSubscribedPrivileges_SelectedIndexChanged);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(251, 280);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 2;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // cbSelectAll
            // 
            this.cbSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbSelectAll.AutoSize = true;
            this.cbSelectAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSelectAll.Location = new System.Drawing.Point(12, 277);
            this.cbSelectAll.Name = "cbSelectAll";
            this.cbSelectAll.Size = new System.Drawing.Size(66, 17);
            this.cbSelectAll.TabIndex = 1;
            this.cbSelectAll.Text = "Select all";
            this.cbSelectAll.UseVisualStyleBackColor = true;
            this.cbSelectAll.CheckedChanged += new System.EventHandler(this.cbSelectAll_CheckedChanged);
            // 
            // OrganisationSelectProductPrivileges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 310);
            this.Controls.Add(this.cbSelectAll);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.lstOrganisationSubscribedPrivileges);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrganisationSelectProductPrivileges";
            this.Text = "Set Organisation Subscribed Product Privileges";
            this.Load += new System.EventHandler(this.SetOrganisationSubscribedProductPrivileges_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox lstOrganisationSubscribedPrivileges;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.CheckBox cbSelectAll;
    }
}