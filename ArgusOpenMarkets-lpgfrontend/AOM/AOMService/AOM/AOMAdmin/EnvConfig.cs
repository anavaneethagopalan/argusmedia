﻿using System.Configuration;

namespace AOMAdmin
{
    public class EnvConfig : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, DefaultValue = "Local")]
        public string Name { get { return (string) this["name"]; } }

        [ConfigurationProperty("rabbitMQ", IsRequired = true, DefaultValue = "host=localhost;username=guest;password=guest;virtualHost=aomtushara;")]
        public string RabbitMq { get { return (string) this["rabbitMQ"]; } }

        [ConfigurationProperty("crm", IsRequired = true, DefaultValue = "server=localhost;user id=root;password=password;persistsecurityinfo=True;database=crm")]
        public string Crm { get { return (string)this["crm"]; } }

        public override string ToString()
        {
            return Name;
        }
    }
}
