﻿namespace AOMAdmin
{
    partial class ProductMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductMain));
            this.cmdRefresh = new System.Windows.Forms.Button();
            this.dgProducts = new System.Windows.Forms.DataGridView();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.lblNotification = new System.Windows.Forms.Label();
            this.cmdCreateProduct = new System.Windows.Forms.Button();
            this.cmdRebuildDiffusionProductTopics = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.colIX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLastOpen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLastClose = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colCmdDetails = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdTimes = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdOrders = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdDeals = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cmdCoBrokering = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cmdMetaData = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgProducts)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRefresh.Location = new System.Drawing.Point(141, 160);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(99, 23);
            this.cmdRefresh.TabIndex = 1;
            this.cmdRefresh.Text = "Refresh Products";
            this.cmdRefresh.UseVisualStyleBackColor = true;
            this.cmdRefresh.Click += new System.EventHandler(this._cmdRefresh_Click);
            // 
            // dgProducts
            // 
            this.dgProducts.AllowUserToAddRows = false;
            this.dgProducts.AllowUserToDeleteRows = false;
            this.dgProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgProducts.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIX,
            this.colName,
            this.colStatus,
            this.colLastOpen,
            this.colLastClose,
            this.colDeleted,
            this.colCmdDetails,
            this.colCmdTimes,
            this.colCmdOrders,
            this.colCmdDeals,
            this.cmdCoBrokering,
            this.cmdMetaData});
            this.dgProducts.Location = new System.Drawing.Point(15, 13);
            this.dgProducts.Name = "dgProducts";
            this.dgProducts.ReadOnly = true;
            this.dgProducts.RowHeadersWidth = 20;
            this.dgProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProducts.Size = new System.Drawing.Size(1199, 135);
            this.dgProducts.TabIndex = 0;
            this.dgProducts.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgOrganisations_CellContentClick);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(1125, 160);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(77, 23);
            this.cmdCancel.TabIndex = 2;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // lblNotification
            // 
            this.lblNotification.AutoSize = true;
            this.lblNotification.ForeColor = System.Drawing.Color.Green;
            this.lblNotification.Location = new System.Drawing.Point(711, 170);
            this.lblNotification.Name = "lblNotification";
            this.lblNotification.Size = new System.Drawing.Size(408, 13);
            this.lblNotification.TabIndex = 14;
            this.lblNotification.Text = "Product changes detected. Close this dialog to propagate the changes to the syste" +
    "m.";
            // 
            // cmdCreateProduct
            // 
            this.cmdCreateProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdCreateProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCreateProduct.Location = new System.Drawing.Point(15, 160);
            this.cmdCreateProduct.Name = "cmdCreateProduct";
            this.cmdCreateProduct.Size = new System.Drawing.Size(120, 23);
            this.cmdCreateProduct.TabIndex = 15;
            this.cmdCreateProduct.Text = "Create Product";
            this.cmdCreateProduct.UseVisualStyleBackColor = true;
            this.cmdCreateProduct.Click += new System.EventHandler(this.cmdCreateProduct_Click);
            // 
            // cmdRebuildDiffusionProductTopics
            // 
            this.cmdRebuildDiffusionProductTopics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdRebuildDiffusionProductTopics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRebuildDiffusionProductTopics.Location = new System.Drawing.Point(246, 160);
            this.cmdRebuildDiffusionProductTopics.Name = "cmdRebuildDiffusionProductTopics";
            this.cmdRebuildDiffusionProductTopics.Size = new System.Drawing.Size(186, 23);
            this.cmdRebuildDiffusionProductTopics.TabIndex = 16;
            this.cmdRebuildDiffusionProductTopics.Text = "Rebuild Diffusion Product Topics";
            this.cmdRebuildDiffusionProductTopics.UseVisualStyleBackColor = true;
            this.cmdRebuildDiffusionProductTopics.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(438, 160);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(210, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Rebuild Market Open/Close Times";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // colIX
            // 
            this.colIX.HeaderText = "-";
            this.colIX.Name = "colIX";
            this.colIX.ReadOnly = true;
            this.colIX.Visible = false;
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colStatus
            // 
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            this.colStatus.Width = 50;
            // 
            // colLastOpen
            // 
            this.colLastOpen.HeaderText = "Last Open";
            this.colLastOpen.Name = "colLastOpen";
            this.colLastOpen.ReadOnly = true;
            this.colLastOpen.Width = 80;
            // 
            // colLastClose
            // 
            this.colLastClose.HeaderText = "Last Close";
            this.colLastClose.Name = "colLastClose";
            this.colLastClose.ReadOnly = true;
            this.colLastClose.Width = 80;
            // 
            // colDeleted
            // 
            this.colDeleted.HeaderText = "Deleted?";
            this.colDeleted.Name = "colDeleted";
            this.colDeleted.ReadOnly = true;
            this.colDeleted.Width = 60;
            // 
            // colCmdDetails
            // 
            this.colCmdDetails.HeaderText = "Tenors";
            this.colCmdDetails.Name = "colCmdDetails";
            this.colCmdDetails.ReadOnly = true;
            this.colCmdDetails.Width = 60;
            // 
            // colCmdTimes
            // 
            this.colCmdTimes.HeaderText = "Market Status";
            this.colCmdTimes.Name = "colCmdTimes";
            this.colCmdTimes.ReadOnly = true;
            this.colCmdTimes.Width = 80;
            // 
            // colCmdOrders
            // 
            this.colCmdOrders.HeaderText = "Orders";
            this.colCmdOrders.Name = "colCmdOrders";
            this.colCmdOrders.ReadOnly = true;
            this.colCmdOrders.Width = 60;
            // 
            // colCmdDeals
            // 
            this.colCmdDeals.HeaderText = "Deals";
            this.colCmdDeals.Name = "colCmdDeals";
            this.colCmdDeals.ReadOnly = true;
            this.colCmdDeals.Width = 60;
            // 
            // cmdCoBrokering
            // 
            this.cmdCoBrokering.HeaderText = "CoBrokering";
            this.cmdCoBrokering.Name = "cmdCoBrokering";
            this.cmdCoBrokering.ReadOnly = true;
            // 
            // cmdMetaData
            // 
            this.cmdMetaData.HeaderText = "MetaData";
            this.cmdMetaData.Name = "cmdMetaData";
            this.cmdMetaData.ReadOnly = true;
            this.cmdMetaData.Text = "";
            // 
            // ProductMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1226, 189);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmdRebuildDiffusionProductTopics);
            this.Controls.Add(this.cmdCreateProduct);
            this.Controls.Add(this.lblNotification);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.dgProducts);
            this.Controls.Add(this.cmdRefresh);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductMain";
            this.Text = "Manage Products";
            this.Load += new System.EventHandler(this.ManageProducts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgProducts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdRefresh;
        private System.Windows.Forms.DataGridView dgProducts;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Label lblNotification;
        private System.Windows.Forms.Button cmdCreateProduct;
        private System.Windows.Forms.Button cmdRebuildDiffusionProductTopics;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIX;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLastOpen;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLastClose;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colDeleted;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdDetails;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdTimes;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdOrders;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdDeals;
        private System.Windows.Forms.DataGridViewButtonColumn cmdCoBrokering;
        private System.Windows.Forms.DataGridViewButtonColumn cmdMetaData;
    }
}