﻿using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using System.Collections.Generic;
using System.Linq;

namespace AOMAdmin
{
    public static class OrganisationUtils
    {
        public static IList<IOrganisationRole> GetNonSystemOrganisationRoles(long organisationId,
                                                                             IOrganisationService organisationService)
        {
            return organisationService.GetOrganisationRoles(organisationId)
                                        .Where(or => !or.Name.ToUpperInvariant().Contains("SUPER DEV/TEST USER"))
                                        .OrderBy(or => or.Name)
                                        .ToList();
        }
    }
}
