﻿using System;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class ProductCoBrokering : Form
    {
        private readonly IBus _bus;
        private readonly Product _product;
        private bool _originalCoBrokeringStatus;

        public ProductCoBrokering(IBus bus, Product product = null)
        {
            _bus = bus;
            _product = product;
            InitializeComponent();
        }

        private void ProductCoBrokering_Load(object sender, EventArgs e)
        {
            PopulateProductDetails();
        }

        private void PopulateProductDetails()
        {
            if (_product != null)
            {
                _originalCoBrokeringStatus = _product.CoBrokering;
                txtProductName.Text = _product.Name;
                chkCoBrokering.Checked = _product.CoBrokering;
            }
            else
            {
                cmdSave.Enabled = false;
                chkCoBrokering.Enabled = false;
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            bool newCoBrokeringStatus = chkCoBrokering.Checked;
            if (_originalCoBrokeringStatus != newCoBrokeringStatus)
            {
                _product.CoBrokering = newCoBrokeringStatus;
                // User has made a change - publish a request.
                var request = new AomCoBrokeringStatusChangeRequest
                {
                    ClientSessionInfo =
                        new ClientSessionInfo
                        {
                            UserId =
                                AdminSettings
                                    .LoggedOnUser.Id
                        },
                    MessageAction = MessageAction.Update,

                    Product = _product,
                };

                _bus.Publish(request);
            }

            Close();
        }
    }
}