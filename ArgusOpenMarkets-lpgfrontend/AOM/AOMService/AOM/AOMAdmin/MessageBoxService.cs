using System.Windows.Forms;

namespace AOMAdmin
{
    public static class MessageBoxService
    {
        private const string Warning = "Warning";
        private const string Confirmation = "Confirmation";
        private const string Error = "Error";
        private const string Information = "Information";

        public static void ShowWarning(IWin32Window owner, string warningMessage, string caption = Warning)
        {
            MessageBox.Show(owner, warningMessage, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static bool ShowConfirmation(IWin32Window owner, string confirmationMessage, string caption = Confirmation)
        {
            var confirmationResult = MessageBox.Show(
                owner,
                confirmationMessage,
                caption,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Information);

            return confirmationResult == DialogResult.Yes;
        }

        public static void ShowError(IWin32Window owner, string errorMessage, string caption = Error)
        {
            MessageBox.Show(
                owner,
                errorMessage,
                caption,
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }

        public static void ShowInfo(IWin32Window owner, string infoMessage, string caption = Information)
        {
            MessageBox.Show(
                owner,
                infoMessage,
                caption,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }
    }
}