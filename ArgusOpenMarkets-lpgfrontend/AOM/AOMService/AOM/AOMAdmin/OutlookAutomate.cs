﻿#region Using directives
using System;
using System.Runtime.InteropServices;
using AOM.App.Domain.Interfaces;
using Outlook = Microsoft.Office.Interop.Outlook;
#endregion


namespace AOMAdmin
{
    public static class OutlookAutomate
    {
        
        public static void AutomateOutlook(string subject, string body, IUser user, string temporaryPassword)
        {
            object missing = Type.Missing;

            Outlook.Application oOutlook = null;
            Outlook.NameSpace oNS = null;
            Outlook.MailItem oMail = null;

            try
            {
                // Start Microsoft Outlook and log on with your profile. 

                // Create an Outlook application. 
                oOutlook = new Outlook.Application();
                Console.WriteLine("Outlook.Application is started");

                Console.WriteLine("User logs on ...");

                // Get the namespace. 
                oNS = oOutlook.GetNamespace("MAPI");

                // Log on by using a dialog box to choose the profile. 
                oNS.Logon(missing, missing, true, true);

                Console.WriteLine("Create and send a new mail item");

                oMail = (Outlook.MailItem)oOutlook.CreateItem(
                    Outlook.OlItemType.olMailItem);

                Microsoft.Office.Interop.Outlook.Accounts accounts = oMail.Session.Accounts;
                for (int i = 1; i <= accounts.Count; i++)
                {
                    if (accounts[i].DisplayName.ToLower() == "aomsupport@argusmedia.com")
                    {
                        oMail.SendUsingAccount = accounts[i];
                        break;
                    }
                }

                // Set the properties of the email. 
                oMail.Subject = subject;
                oMail.To = "nathanbellamore@hotmail.com";
                // oMail.SenderEmailAddress = "AOMSupport@argusmedia.com";
                // oMail.SenderName = "AOMSupport@argusmedia.com";

                body = body.Replace("<TEMPORARY_PASSWORD>", temporaryPassword);
                body = body.Replace("<USERNAME>", user.Username);
                oMail.HTMLBody = body;

                // Displays a new Inspector object for the item and allows users to  
                // click on the Send button to send the mail manually. 
                // Modal = true makes the Inspector window modal 
                oMail.Display(true);
                // [-or-] 
                // Automatically send the mail without a new Inspector window. 
                //((Outlook._MailItem)oMail).Send(); 
            }
            catch (Exception ex)
            {
                Console.WriteLine("AutomateOutlook throws the error: {0}", ex.Message);
            }
            finally
            {
                // Manually clean up the explicit unmanaged Outlook COM resources by   
                // calling Marshal.FinalReleaseComObject on all accessor objects.  
                // See http://support.microsoft.com/kb/317109. 
            }
        }
    }
}
