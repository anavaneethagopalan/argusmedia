﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AOM.App.Domain.Entities;

namespace AOMAdmin
{
    public partial class ProductMetadataListForm : Form
    {
        private long _selectedMetaDataListId;
        private List<MetaDataList> _metadataLists;
        private List<MetaDataListItem<long>> _metadataListItems;
        private BindingSource _metadataListsBindingSource;
        private BindingSource _metadataListItemsBindingSource;
        private readonly IProductMetadataConsumerClient _metadataConsumerClient;


        public ProductMetadataListForm(IProductMetadataConsumerClient metadataConsumerClient, long metaDataListId)
        {
            InitializeComponent();
            _selectedMetaDataListId = metaDataListId;

            dataGridMetadataLists.AutoGenerateColumns = true;
            dataGridMetadataListItems.AutoGenerateColumns = false;
            _metadataConsumerClient = metadataConsumerClient;
        }

        protected override void OnLoad(EventArgs e)
        {
            _metadataLists = _metadataConsumerClient.GetProductMetadataLists().ToList();
            _metadataListsBindingSource = new BindingSource(_metadataLists, null);
            _metadataListsBindingSource.CurrentChanged += OnSelectMetadataListChanged;
            dataGridMetadataLists.DataSource = _metadataListsBindingSource;
            dataGridMetadataLists.Columns[0].Visible = false;

            _metadataListsBindingSource.ResetBindings(false);

            if (_selectedMetaDataListId != -1)
                dataGridMetadataLists.Rows.OfType<DataGridViewRow>().Where(x => (long)x.Cells["Id"].Value == _selectedMetaDataListId).ToArray<DataGridViewRow>()[0].Selected = true;

            base.OnLoad(e);
        }

        private void OnSelectMetadataListChanged(object sender, EventArgs eventArgs)
        {
            var selectedMetadataList = GetSelectedMetadataList();
            if (selectedMetadataList == null)
                return;

            _metadataListItems = selectedMetadataList.FieldList.ToList();
            _metadataListItemsBindingSource = new BindingSource(_metadataListItems, null);
            dataGridMetadataListItems.DataSource = _metadataListItemsBindingSource;
        }

        private void btnAddMetadataList_Click(object sender, EventArgs e)
        {
            _metadataLists.Add(new MetaDataList { Description = string.Empty, FieldList = new MetaDataListItem<long>[0] });
            _metadataListsBindingSource.ResetBindings(false);
        }

        private void btnDeleteMetadataList_Click(object sender, EventArgs e)
        {
            var metadataList = GetSelectedMetadataList();
            if (metadataList != null)
            {
                if (MessageBoxService.ShowConfirmation(this, 
                    string.Format("Delete metadata list \"{0}\"?", metadataList.Description)))
                {
                    _metadataLists.Remove(metadataList);
                    _metadataListsBindingSource.ResetBindings(false);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateMetadataLists())
            {
                var result = _metadataConsumerClient.UpdateProductMetadataLists(_metadataLists);
                if (result.IsValid)
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    MessageBoxService.ShowWarning(this, result.ErrorMessage);
                }
            }
        }

        private void dataGridMetadataListItems_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (dataGridMetadataListItems.Columns[e.ColumnIndex].DataPropertyName == "ItemValue")
            {
                int value;
                if (!int.TryParse(e.FormattedValue.ToString(), out value))
                {
                    MessageBoxService.ShowWarning(this, "Item Value should have numeric value.");
                    e.Cancel = true;
                }
            }
        }

        private void btnMoveListItemUp_Click(object sender, EventArgs e)
        {
            MoveSelectedListItem(-1);
        }

        private void btnMoveListItemDown_Click(object sender, EventArgs e)
        {
            MoveSelectedListItem(1);
        }

        private void btnAddMetadataListItem_Click(object sender, EventArgs e)
        {
            var selectedList = GetSelectedMetadataList();
            if (selectedList == null)
                return;

            _metadataListItems.Add(new MetaDataListItem<long> { DisplayName = string.Empty, ItemValue = 0 });
            selectedList.FieldList = _metadataListItems.ToArray();
            _metadataListItemsBindingSource.ResetBindings(false);
        }

        private void btnDeleteMetadataListItem_Click(object sender, EventArgs e)
        {
            var selectedList = GetSelectedMetadataList();
            if (selectedList == null)
                return;

            var selectedListItem = GetSelectedMetadataListItem();
            if (selectedListItem == null)
                return;

            if (MessageBoxService.ShowConfirmation(this,
                string.Format("Delete metadata list item \"{0}\"?", selectedListItem.DisplayName)))
            {
                _metadataListItems.Remove(selectedListItem);
                selectedList.FieldList = _metadataListItems.ToArray();
                _metadataListItemsBindingSource.ResetBindings(false);
            }
        }

        private MetaDataList GetSelectedMetadataList()
        {
            return _metadataListsBindingSource.Current as MetaDataList;
        }

        private MetaDataListItem<long> GetSelectedMetadataListItem()
        {
            return _metadataListItemsBindingSource.Current as MetaDataListItem<long>;
        }

        private void MoveSelectedListItem(int step)
        {
            var selectedList = GetSelectedMetadataList();
            if (selectedList == null)
                return;

            var selectedListItem = GetSelectedMetadataListItem();
            if (selectedListItem == null)
                return;

            var currentIndex = _metadataListItems.IndexOf(selectedListItem);
            var targetIndex = currentIndex + step;
            if (targetIndex < 0 || targetIndex >= _metadataListItems.Count)
                return;

            _metadataListItems.Remove(selectedListItem);
            _metadataListItems.Insert(targetIndex, selectedListItem);

            selectedList.FieldList = _metadataListItems.ToArray();

            _metadataListItemsBindingSource.Position = targetIndex;
            _metadataListItemsBindingSource.ResetBindings(false);
        }

        private bool ValidateMetadataLists()
        {
            var metadataListWithEmptyDescription = _metadataLists.FirstOrDefault(x => string.IsNullOrEmpty(x.Description));
            if (metadataListWithEmptyDescription != null)
            {
                SetSelectedMetadataList(metadataListWithEmptyDescription);
                MessageBoxService.ShowWarning(this, "Metadata list should have not empty description.");
                return false;
            }

            var metadataListWithoutItems = _metadataLists.FirstOrDefault(x => x.FieldList == null || x.FieldList.Length == 0);
            if (metadataListWithoutItems != null)
            {
                SetSelectedMetadataList(metadataListWithoutItems);
                MessageBoxService.ShowWarning(this, "Metadata list should have at least one item.");
                return false;
            }

            MetaDataListItem<long> metadataListItemWithEmptyDisplayName = null;
            MetaDataList metadataListWithEmptyItemDisplayName = _metadataLists.FirstOrDefault(x =>
            {
                metadataListItemWithEmptyDisplayName = x.FieldList.FirstOrDefault(field => string.IsNullOrEmpty(field.DisplayName));
                return metadataListItemWithEmptyDisplayName != null;
            });

            if (metadataListWithEmptyItemDisplayName != null)
            {
                SetSelectedMetadataList(metadataListWithEmptyItemDisplayName);
                SetSelectedMetadataListItem(metadataListItemWithEmptyDisplayName);
                MessageBoxService.ShowWarning(this, "Metadata list item should have not empty display name.");
                return false;
            }

            return true;
        }

        private void SetSelectedMetadataList(MetaDataList metadataList)
        {
            var index = _metadataLists.IndexOf(metadataList);
            if (index >= 0)
                _metadataListsBindingSource.Position = index;
        }

        private void SetSelectedMetadataListItem(MetaDataListItem<long> metadataListItem)
        {
            var index = _metadataListItems.IndexOf(metadataListItem);
            if (index >= 0)
                _metadataListItemsBindingSource.Position = index;
        }
    }
}
