﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class ProductOrdersPurgeTimes : Form
    {
        class DataItem
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public DateTime? NextPurge { get; set; }
            public TimeSpan? TimeOfDay { get; set; }
            public int PurgeFrequency { get; set; }
            public bool IsDirty { get; set; }
            public string Kill { get; set; }
        }

        private readonly IBus _bus;
        private readonly Product _product;
        private Boolean _didUpdate;

        public ProductOrdersPurgeTimes(IBus bus, Product product = null)
        {
            _bus = bus;
            _product = product;
            InitializeComponent();

            _ConfigurationsDataGridView.AutoGenerateColumns = false;
        }

        private void PopulateProducts()
        {
            _ConfigurationsDataGridView.DataSource = null;

            var allProducts = _product == null
                ? _bus.Request<GetAllActiveProductsRequestRpc, List<Product>>(new GetAllActiveProductsRequestRpc())
                : new List<Product> {_product};

            _ConfigurationsDataGridView.DataSource = allProducts.Select(product => new DataItem
            {
                Id = product.ProductId,
                Name = product.Name,
                NextPurge = product.PurgeDate,
                TimeOfDay = product.PurgeTimeOfDay,
                PurgeFrequency = product.PurgeFrequency,
                Kill = "Kill"
            }).ToList();
            _ConfigurationsDataGridView.Rows[0].Selected = true;
        }

        private void _SaveChangesButton_Click(object sender, EventArgs e)
        {
            var products = (List<DataItem>) _ConfigurationsDataGridView.DataSource;

            foreach (var dataItem in products)
            {
                if (!dataItem.IsDirty) continue;

                var product =
                    _bus.Request<GetProductRequestRpc, AomProductResponse>(new GetProductRequestRpc {Id = dataItem.Id})
                        .Product;

                product.PurgeDate = DateTime.SpecifyKind(dataItem.NextPurge.Value.Date, DateTimeKind.Utc);
                product.PurgeFrequency = dataItem.PurgeFrequency;
                product.PurgeTimeOfDay = dataItem.TimeOfDay;

                var request = new AomProductConfigurePurgeRequest
                {
                    ClientSessionInfo = new ClientSessionInfo
                    {
                        UserId = AdminSettings.LoggedOnUser.Id
                    },
                    MessageAction = MessageAction.ConfigurePurge,
                    Product = product
                };

                _bus.Publish(request);

                dataItem.IsDirty = false;
                _didUpdate = true;
            }
        }

        private void ConfigurePurgeProductOrders_Load(object sender, EventArgs e)
        {
            if (_product != null)
            {
                Text = _product.Name + ": Manage Order Purging";
            }
            PopulateProducts();

            _bus.Subscribe<AomProductConfigurePurgeResponse>(
                string.Join("-", "AOMAdminTool", Environment.MachineName, Process.GetCurrentProcess().Id),
                response =>
                {
                    var product = response.Product;

                    if (InvokeRequired)
                    {
                        Invoke((Action) (() => UpdateProductRow(product)));
                    }
                    else
                    {
                        UpdateProductRow(product);
                    }

                });
            _didUpdate = false;
        }

        private void UpdateProductRow(Product product)
        {
            if (_ConfigurationsDataGridView.IsHandleCreated)
            {
                var products = (List<DataItem>) _ConfigurationsDataGridView.DataSource;

                foreach (var dataItem in products)
                {
                    if (dataItem.Id != product.ProductId) continue;
                    dataItem.NextPurge = product.PurgeDate;
                    dataItem.PurgeFrequency = product.PurgeFrequency;
                    dataItem.TimeOfDay = product.PurgeTimeOfDay;
                    dataItem.IsDirty = false;

                    Refresh();

                    break;
                }
            }
        }

        private void _refreshProductsButton_Click(object sender, EventArgs e)
        {
            PopulateProducts();
        }

        private void _ConfigurationsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                ((List<DataItem>) _ConfigurationsDataGridView.DataSource)[e.RowIndex].IsDirty = true;
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            DialogResult = _didUpdate ? DialogResult.Yes : DialogResult.No;
            Close();
        }

        private void _ConfigurationsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                var row = _ConfigurationsDataGridView.Rows[e.RowIndex];

                switch (e.ColumnIndex)
                {
                    case 5:
                        SendKillRequest(row);
                        break;
                    default:
                        MessageBox.Show("Bug in code - unexpected button index.", "Error", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        break;
                }
            }
        }

        private void SendKillRequest(DataGridViewRow row)
        {
            if (MessageBox.Show("Click OK to send a request to purge all orders for this product.",
                    "Confirmation Required",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2) == DialogResult.Cancel)
            {
                return;
            }

            var request = new AomProductPurgeOrdersRequest
            {
                ClientSessionInfo =
                    new ClientSessionInfo
                    {
                        UserId = AdminSettings.LoggedOnUser.Id
                    },
                MessageAction = MessageAction.Purge,
                Product =
                    _bus
                        .Request
                        <GetProductRequestRpc, AomProductResponse>(
                            new GetProductRequestRpc
                            {
                                Id = (long) row.Cells[0].Value
                            }).Product
            };

            _bus.Publish(request);
        }
    }
}