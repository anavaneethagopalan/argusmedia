﻿namespace AOMAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using AOM.App.Domain.Entities;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    public partial class ProductMarketStatusTimers : Form
    {
        private readonly IBus _bus;

        private readonly Product _product;

        private Boolean _didUpdate;

        public ProductMarketStatusTimers(IBus bus, Product product)
        {
            _bus = bus;
            _product = product;
            InitializeComponent();

            grid.AutoGenerateColumns = false;
        }

        private void ConfigureProductMarketStatusTimers_Load(object sender, EventArgs e)
        {
            Text = _product.Name + ": Manage Market Status Times";
            PopulateProducts();
            _didUpdate = false;
        }

        private void PopulateProducts()
        {
            grid.DataSource = new List<Product> {_product};

            grid.Rows[0].Cells[4].Value = "Open Market";
            grid.Rows[0].Cells[5].Value = "Close Market";

            grid.Refresh();
        }

        private void saveChanges_Click(object sender, EventArgs e)
        {
            //publish request
            var request = new AomMarketStatusChangeRequest
            {
                ClientSessionInfo =
                    new ClientSessionInfo
                    {
                        UserId =
                            AdminSettings
                                .LoggedOnUser.Id
                    },
                MessageAction = MessageAction.Update,
                Product = _product
            };

            _bus.Publish(request);

            _didUpdate = true;
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            PopulateProducts();
        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                switch (e.ColumnIndex)
                {
                    case 4:
                        OpenMarket();
                        break;
                    case 5:
                        CloseMarket();
                        break;
                    default:
                        MessageBox.Show(
                            @"Bug in code - unexpected button index.",
                            @"Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        break;
                }
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            DialogResult = _didUpdate ? DialogResult.Yes : DialogResult.No;
            Close();
        }

        private void OpenMarket()
        {
            SendRequest(_product.ProductId, MessageAction.Open);
        }

        private void CloseMarket()
        {
            SendRequest(_product.ProductId, MessageAction.Close);
        }

        private void SendRequest(long id, MessageAction action)
        {
            var request = new AomMarketStatusChangeRequest
            {
                ClientSessionInfo =
                    new ClientSessionInfo
                    {
                        UserId =
                            AdminSettings
                                .LoggedOnUser.Id
                    },
                MessageAction = action,
                Product =
                    _bus
                        .Request<GetProductRequestRpc, AomProductResponse>(
                            new GetProductRequestRpc {Id = id}).Product
            };

            _bus.Publish(request);
        }
    }
}