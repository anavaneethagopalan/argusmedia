﻿using AOM.App.Domain.Entities;

namespace AOMAdmin
{
    public class ComboBoxItem
    {
        public ComboBoxItem(string id, string description)
        {
            StringId = id;
            Description = description;
        }

        public ComboBoxItem(long id, string description)
        {
            Id = id;
            Description = description;
        }

        public long Id { get; set; }

        public string StringId { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }

    public class OrganisationComboBox : ComboBoxItem
    {
        public OrganisationType OrgType { get; set; }
        
        public OrganisationComboBox(long id, string description, OrganisationType organisationType) : base(id, description)
        {
            OrgType = organisationType;
        }
    }
}