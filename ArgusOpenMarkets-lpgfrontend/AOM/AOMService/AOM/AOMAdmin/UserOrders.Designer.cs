﻿namespace AOMAdmin
{
    partial class UserOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserOrders));
            this.label2 = new System.Windows.Forms.Label();
            this._UsernameTextbox = new System.Windows.Forms.TextBox();
            this._GetOrdersButton = new System.Windows.Forms.Button();
            this._SelectAllOrdersButton = new System.Windows.Forms.Button();
            this._DeleteSelectedOrdersButton = new System.Windows.Forms.Button();
            this._UsersOrdersCheckedListbox = new System.Windows.Forms.CheckedListBox();
            this.cmdFindUser = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._UserIdLabel = new System.Windows.Forms.Label();
            this._StatusLabel = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "Username:";
            // 
            // _UsernameTextbox
            // 
            this._UsernameTextbox.Location = new System.Drawing.Point(80, 8);
            this._UsernameTextbox.Name = "_UsernameTextbox";
            this._UsernameTextbox.Size = new System.Drawing.Size(234, 20);
            this._UsernameTextbox.TabIndex = 0;
            this._UsernameTextbox.TextChanged += new System.EventHandler(this._UsernameTextbox_TextChanged);
            // 
            // _GetOrdersButton
            // 
            this._GetOrdersButton.Enabled = false;
            this._GetOrdersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._GetOrdersButton.Location = new System.Drawing.Point(15, 321);
            this._GetOrdersButton.Name = "_GetOrdersButton";
            this._GetOrdersButton.Size = new System.Drawing.Size(94, 23);
            this._GetOrdersButton.TabIndex = 3;
            this._GetOrdersButton.Text = "Refresh Orders";
            this._GetOrdersButton.UseVisualStyleBackColor = true;
            this._GetOrdersButton.Click += new System.EventHandler(this._GetOrdersButton_Click);
            // 
            // _SelectAllOrdersButton
            // 
            this._SelectAllOrdersButton.Enabled = false;
            this._SelectAllOrdersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._SelectAllOrdersButton.Location = new System.Drawing.Point(115, 321);
            this._SelectAllOrdersButton.Name = "_SelectAllOrdersButton";
            this._SelectAllOrdersButton.Size = new System.Drawing.Size(94, 23);
            this._SelectAllOrdersButton.TabIndex = 4;
            this._SelectAllOrdersButton.Text = "Select All";
            this._SelectAllOrdersButton.UseVisualStyleBackColor = true;
            this._SelectAllOrdersButton.Click += new System.EventHandler(this._SelectAllOrdersButton_Click);
            // 
            // _DeleteSelectedOrdersButton
            // 
            this._DeleteSelectedOrdersButton.Enabled = false;
            this._DeleteSelectedOrdersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._DeleteSelectedOrdersButton.Location = new System.Drawing.Point(215, 321);
            this._DeleteSelectedOrdersButton.Name = "_DeleteSelectedOrdersButton";
            this._DeleteSelectedOrdersButton.Size = new System.Drawing.Size(123, 23);
            this._DeleteSelectedOrdersButton.TabIndex = 5;
            this._DeleteSelectedOrdersButton.Text = "Request Kill Selected";
            this._DeleteSelectedOrdersButton.UseVisualStyleBackColor = true;
            this._DeleteSelectedOrdersButton.Click += new System.EventHandler(this._DeleteSelectedOrdersButton_Click);
            // 
            // _UsersOrdersCheckedListbox
            // 
            this._UsersOrdersCheckedListbox.CheckOnClick = true;
            this._UsersOrdersCheckedListbox.FormattingEnabled = true;
            this._UsersOrdersCheckedListbox.Location = new System.Drawing.Point(15, 65);
            this._UsersOrdersCheckedListbox.Name = "_UsersOrdersCheckedListbox";
            this._UsersOrdersCheckedListbox.Size = new System.Drawing.Size(500, 244);
            this._UsersOrdersCheckedListbox.TabIndex = 2;
            this._UsersOrdersCheckedListbox.SelectedIndexChanged += new System.EventHandler(this._UsersOrdersCheckedListbox_SelectedIndexChanged);
            // 
            // cmdFindUser
            // 
            this.cmdFindUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindUser.Location = new System.Drawing.Point(326, 5);
            this.cmdFindUser.Name = "cmdFindUser";
            this.cmdFindUser.Size = new System.Drawing.Size(91, 23);
            this.cmdFindUser.TabIndex = 1;
            this.cmdFindUser.Text = "Find User";
            this.cmdFindUser.UseVisualStyleBackColor = true;
            this.cmdFindUser.Click += new System.EventHandler(this._FindUserButton_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 23);
            this.label1.TabIndex = 8;
            this.label1.Text = "Updating:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // _UserIdLabel
            // 
            this._UserIdLabel.Location = new System.Drawing.Point(73, 39);
            this._UserIdLabel.Name = "_UserIdLabel";
            this._UserIdLabel.Size = new System.Drawing.Size(62, 23);
            this._UserIdLabel.TabIndex = 9;
            this._UserIdLabel.Click += new System.EventHandler(this._UserIdLabel_Click);
            // 
            // _StatusLabel
            // 
            this._StatusLabel.Location = new System.Drawing.Point(12, 350);
            this._StatusLabel.Name = "_StatusLabel";
            this._StatusLabel.Size = new System.Drawing.Size(395, 23);
            this._StatusLabel.TabIndex = 10;
            // 
            // cmdCancel
            // 
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(421, 321);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 6;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // UserOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 354);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this._StatusLabel);
            this.Controls.Add(this._UserIdLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdFindUser);
            this.Controls.Add(this._UsersOrdersCheckedListbox);
            this.Controls.Add(this._DeleteSelectedOrdersButton);
            this.Controls.Add(this._SelectAllOrdersButton);
            this.Controls.Add(this._GetOrdersButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._UsernameTextbox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserOrders";
            this.Text = "ManageUsersOrders";
            this.Load += new System.EventHandler(this.ManageUsersOrders_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _UsernameTextbox;
        private System.Windows.Forms.Button _GetOrdersButton;
        private System.Windows.Forms.Button _SelectAllOrdersButton;
        private System.Windows.Forms.Button _DeleteSelectedOrdersButton;
        private System.Windows.Forms.CheckedListBox _UsersOrdersCheckedListbox;
        private System.Windows.Forms.Button cmdFindUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label _UserIdLabel;
        private System.Windows.Forms.Label _StatusLabel;
        private System.Windows.Forms.Button cmdCancel;
    }
}