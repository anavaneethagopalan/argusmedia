﻿using Utils.Logging.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Transport.Events.ScheduledTasks;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public interface IScheduledTaskCaller
    {
        List<ScheduledTask> GetAllTasks();

        string CreateNewScheduledTask(ScheduledTask task);

        string DeleteScheduledTask(long taskId);

        string GetTaskName();
    }

    internal class ScheduledTaskCaller<TRequestType> : IScheduledTaskCaller
        where TRequestType : class
    {
        private readonly IBus _bus;

        public ScheduledTaskCaller(IBus bus)
        {
            _bus = bus;
        }

        public List<ScheduledTask> GetAllTasks()
        {
            var response = _bus.Request<GetAllScheduledTasksRequestRpc, GetAllScheduledTasksResponseRpc>(
                new GetAllScheduledTasksRequestRpc {TaskType = typeof(TRequestType).Name});

            return response.ScheduledTasks;
        }

        public string CreateNewScheduledTask(ScheduledTask task)
        {
            ScheduledTaskResponseRpc response = null;

            try
            {
                switch (typeof(TRequestType).Name)
                {
                    case "PurgeNewsRequest":
                        response = _bus.Request<CreateNewsScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(
                            new CreateNewsScheduledTaskRequestRpc {Task = task});
                        break;
                    case "PurgeMarketTickerRequest":
                        response = _bus.Request<CreateMTScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(
                            new CreateMTScheduledTaskRequestRpc {Task = task});
                        break;
                    default:
                        response = _bus.Request<CreateNewScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(
                            new CreateNewScheduledTaskRequestRpc {Task = task});
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error("CreateNewScheduledTask", ex);
            }

            return response == null ? string.Empty : response.Message;
        }

        public string DeleteScheduledTask(long taskId)
        {
            ScheduledTaskResponseRpc response = null;

            try
            {
                switch (typeof(TRequestType).Name)
                {
                    case "PurgeNewsRequest":
                        response = _bus.Request<DeleteNewsScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(
                            new DeleteNewsScheduledTaskRequestRpc {TaskId = taskId});
                        break;

                    case "PurgeMarketTickerRequest":
                        response = _bus.Request<DeleteMTScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(
                            new DeleteMTScheduledTaskRequestRpc {TaskId = taskId});
                        break;
                    default:
                        response = _bus.Request<DeleteScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(
                            new DeleteScheduledTaskRequestRpc {TaskId = taskId});
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error("DeleteScheduledTask", ex);
            }

            return response == null ? string.Empty : response.Message;
        }

        public string GetTaskName()
        {
            return typeof(TRequestType).Name;
        }
    }
}