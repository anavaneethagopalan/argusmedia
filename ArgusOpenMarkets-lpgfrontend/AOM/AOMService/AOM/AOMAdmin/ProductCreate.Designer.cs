﻿namespace AOMAdmin
{
    partial class ProductCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboCommodity = new System.Windows.Forms.ComboBox();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkIsInternal = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboMarket = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboContractType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboLocation = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboVolumeUnitCode = new System.Windows.Forms.ComboBox();
            this.cboPriceUnitCode = new System.Windows.Forms.ComboBox();
            this.cboCurrencyCode = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOrderVolumeMinimum = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtOrderVolumeMaximum = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOrderVolumeIncrement = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtOrderVolumeDecimalPlaces = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtOrderVolumeSignificantFigures = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPriceSignificantFigures = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPriceDecimalPlaces = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPriceIncrement = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPriceMaximum = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPriceMinimum = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtBidAskStackNumberRows = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtPurgeFrequency = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.chkIgnoreMarketStatusTriggers = new System.Windows.Forms.CheckBox();
            this.chkAllowNegativePriceForExternalDeals = new System.Windows.Forms.CheckBox();
            this.chkDisplayOptionalPriceDetail = new System.Windows.Forms.CheckBox();
            this.txtOrderVolumeDefault = new System.Windows.Forms.TextBox();
            this.txtOpenTime = new System.Windows.Forms.DateTimePicker();
            this.txtCloseTime = new System.Windows.Forms.DateTimePicker();
            this.txtPurgeTimeOfDay = new System.Windows.Forms.DateTimePicker();
            this.txtProductTenorName = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtProductTenorDisplayName = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtProductTenorDeliveryDateEnd = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtProductTenorDeliveryDateStart = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtProductTenorRoleDateRule = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtProductTenorRollDate = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtProductTenorMinimumDeliveryRange = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.chkHasAssessment = new System.Windows.Forms.CheckBox();
            this.txtDeliveryPeriodLabel = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtLocationLabel = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.chkCoBrokering = new System.Windows.Forms.CheckBox();
            this.txtDisplayOrder = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtProductTenorDefaultEndDate = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtProductTenorDefaultStartDate = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmdCancel
            // 
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(656, 819);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(4);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(148, 30);
            this.cmdCancel.TabIndex = 38;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSave.Location = new System.Drawing.Point(503, 819);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(4);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(145, 30);
            this.cmdSave.TabIndex = 37;
            this.cmdSave.Text = "Save";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkIsDeleted.Location = new System.Drawing.Point(199, 50);
            this.chkIsDeleted.Margin = new System.Windows.Forms.Padding(4);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(89, 21);
            this.chkIsDeleted.TabIndex = 1;
            this.chkIsDeleted.Text = "Is Deleted";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.label5.Location = new System.Drawing.Point(16, 82);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 17);
            this.label5.TabIndex = 41;
            this.label5.Text = "Commodity";
            // 
            // cboCommodity
            // 
            this.cboCommodity.FormattingEnabled = true;
            this.cboCommodity.Location = new System.Drawing.Point(199, 79);
            this.cboCommodity.Margin = new System.Windows.Forms.Padding(4);
            this.cboCommodity.Name = "cboCommodity";
            this.cboCommodity.Size = new System.Drawing.Size(220, 24);
            this.cboCommodity.TabIndex = 3;
            // 
            // txtProductName
            // 
            this.txtProductName.Location = new System.Drawing.Point(199, 18);
            this.txtProductName.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(464, 22);
            this.txtProductName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 36;
            this.label1.Text = "Product Name:";
            // 
            // chkIsInternal
            // 
            this.chkIsInternal.AutoSize = true;
            this.chkIsInternal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkIsInternal.Location = new System.Drawing.Point(328, 50);
            this.chkIsInternal.Margin = new System.Windows.Forms.Padding(4);
            this.chkIsInternal.Name = "chkIsInternal";
            this.chkIsInternal.Size = new System.Drawing.Size(87, 21);
            this.chkIsInternal.TabIndex = 2;
            this.chkIsInternal.Text = "Is Internal";
            this.chkIsInternal.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.label2.Location = new System.Drawing.Point(16, 116);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 17);
            this.label2.TabIndex = 44;
            this.label2.Text = "Market:";
            // 
            // cboMarket
            // 
            this.cboMarket.FormattingEnabled = true;
            this.cboMarket.Location = new System.Drawing.Point(199, 112);
            this.cboMarket.Margin = new System.Windows.Forms.Padding(4);
            this.cboMarket.Name = "cboMarket";
            this.cboMarket.Size = new System.Drawing.Size(220, 24);
            this.cboMarket.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.label3.Location = new System.Drawing.Point(460, 82);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 46;
            this.label3.Text = "Contract Type:";
            // 
            // cboContractType
            // 
            this.cboContractType.FormattingEnabled = true;
            this.cboContractType.Location = new System.Drawing.Point(588, 79);
            this.cboContractType.Margin = new System.Windows.Forms.Padding(4);
            this.cboContractType.Name = "cboContractType";
            this.cboContractType.Size = new System.Drawing.Size(220, 24);
            this.cboContractType.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.label4.Location = new System.Drawing.Point(460, 116);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 48;
            this.label4.Text = "Location:";
            // 
            // cboLocation
            // 
            this.cboLocation.FormattingEnabled = true;
            this.cboLocation.Location = new System.Drawing.Point(588, 112);
            this.cboLocation.Margin = new System.Windows.Forms.Padding(4);
            this.cboLocation.Name = "cboLocation";
            this.cboLocation.Size = new System.Drawing.Size(220, 24);
            this.cboLocation.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 155);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 17);
            this.label6.TabIndex = 50;
            this.label6.Text = "Currency Code:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 187);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 17);
            this.label7.TabIndex = 52;
            this.label7.Text = "Volume Unit Code:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(460, 194);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 17);
            this.label8.TabIndex = 54;
            this.label8.Text = "Price Unit Code:";
            // 
            // cboVolumeUnitCode
            // 
            this.cboVolumeUnitCode.FormattingEnabled = true;
            this.cboVolumeUnitCode.Location = new System.Drawing.Point(199, 191);
            this.cboVolumeUnitCode.Margin = new System.Windows.Forms.Padding(4);
            this.cboVolumeUnitCode.Name = "cboVolumeUnitCode";
            this.cboVolumeUnitCode.Size = new System.Drawing.Size(171, 24);
            this.cboVolumeUnitCode.TabIndex = 8;
            // 
            // cboPriceUnitCode
            // 
            this.cboPriceUnitCode.FormattingEnabled = true;
            this.cboPriceUnitCode.Location = new System.Drawing.Point(633, 187);
            this.cboPriceUnitCode.Margin = new System.Windows.Forms.Padding(4);
            this.cboPriceUnitCode.Name = "cboPriceUnitCode";
            this.cboPriceUnitCode.Size = new System.Drawing.Size(175, 24);
            this.cboPriceUnitCode.TabIndex = 9;
            // 
            // cboCurrencyCode
            // 
            this.cboCurrencyCode.FormattingEnabled = true;
            this.cboCurrencyCode.Location = new System.Drawing.Point(199, 159);
            this.cboCurrencyCode.Margin = new System.Windows.Forms.Padding(4);
            this.cboCurrencyCode.Name = "cboCurrencyCode";
            this.cboCurrencyCode.Size = new System.Drawing.Size(171, 24);
            this.cboCurrencyCode.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 242);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 17);
            this.label9.TabIndex = 59;
            this.label9.Text = "Volume Default:";
            // 
            // txtOrderVolumeMinimum
            // 
            this.txtOrderVolumeMinimum.Location = new System.Drawing.Point(199, 271);
            this.txtOrderVolumeMinimum.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderVolumeMinimum.Name = "txtOrderVolumeMinimum";
            this.txtOrderVolumeMinimum.Size = new System.Drawing.Size(175, 22);
            this.txtOrderVolumeMinimum.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 274);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 17);
            this.label10.TabIndex = 61;
            this.label10.Text = "Volume Miniumum:";
            // 
            // txtOrderVolumeMaximum
            // 
            this.txtOrderVolumeMaximum.Location = new System.Drawing.Point(199, 303);
            this.txtOrderVolumeMaximum.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderVolumeMaximum.Name = "txtOrderVolumeMaximum";
            this.txtOrderVolumeMaximum.Size = new System.Drawing.Size(175, 22);
            this.txtOrderVolumeMaximum.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 306);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 17);
            this.label11.TabIndex = 63;
            this.label11.Text = "Volume Maximum:";
            // 
            // txtOrderVolumeIncrement
            // 
            this.txtOrderVolumeIncrement.Location = new System.Drawing.Point(199, 335);
            this.txtOrderVolumeIncrement.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderVolumeIncrement.Name = "txtOrderVolumeIncrement";
            this.txtOrderVolumeIncrement.Size = new System.Drawing.Size(175, 22);
            this.txtOrderVolumeIncrement.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 338);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 17);
            this.label12.TabIndex = 65;
            this.label12.Text = "Volume Increment:";
            // 
            // txtOrderVolumeDecimalPlaces
            // 
            this.txtOrderVolumeDecimalPlaces.Location = new System.Drawing.Point(199, 367);
            this.txtOrderVolumeDecimalPlaces.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderVolumeDecimalPlaces.Name = "txtOrderVolumeDecimalPlaces";
            this.txtOrderVolumeDecimalPlaces.Size = new System.Drawing.Size(175, 22);
            this.txtOrderVolumeDecimalPlaces.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 370);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(159, 17);
            this.label13.TabIndex = 67;
            this.label13.Text = "Volume Decimal Places:";
            // 
            // txtOrderVolumeSignificantFigures
            // 
            this.txtOrderVolumeSignificantFigures.Location = new System.Drawing.Point(199, 399);
            this.txtOrderVolumeSignificantFigures.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderVolumeSignificantFigures.Name = "txtOrderVolumeSignificantFigures";
            this.txtOrderVolumeSignificantFigures.Size = new System.Drawing.Size(175, 22);
            this.txtOrderVolumeSignificantFigures.TabIndex = 15;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 402);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(179, 17);
            this.label14.TabIndex = 69;
            this.label14.Text = "Volume Significant Figures:";
            // 
            // txtPriceSignificantFigures
            // 
            this.txtPriceSignificantFigures.Location = new System.Drawing.Point(633, 365);
            this.txtPriceSignificantFigures.Margin = new System.Windows.Forms.Padding(4);
            this.txtPriceSignificantFigures.Name = "txtPriceSignificantFigures";
            this.txtPriceSignificantFigures.Size = new System.Drawing.Size(175, 22);
            this.txtPriceSignificantFigures.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(460, 368);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(164, 17);
            this.label15.TabIndex = 79;
            this.label15.Text = "Price Significant Figures:";
            // 
            // txtPriceDecimalPlaces
            // 
            this.txtPriceDecimalPlaces.Location = new System.Drawing.Point(633, 333);
            this.txtPriceDecimalPlaces.Margin = new System.Windows.Forms.Padding(4);
            this.txtPriceDecimalPlaces.Name = "txtPriceDecimalPlaces";
            this.txtPriceDecimalPlaces.Size = new System.Drawing.Size(175, 22);
            this.txtPriceDecimalPlaces.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(460, 336);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(144, 17);
            this.label16.TabIndex = 77;
            this.label16.Text = "Price Decimal Places:";
            // 
            // txtPriceIncrement
            // 
            this.txtPriceIncrement.Location = new System.Drawing.Point(633, 301);
            this.txtPriceIncrement.Margin = new System.Windows.Forms.Padding(4);
            this.txtPriceIncrement.Name = "txtPriceIncrement";
            this.txtPriceIncrement.Size = new System.Drawing.Size(175, 22);
            this.txtPriceIncrement.TabIndex = 18;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(460, 304);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 17);
            this.label17.TabIndex = 75;
            this.label17.Text = "Price Increment:";
            // 
            // txtPriceMaximum
            // 
            this.txtPriceMaximum.Location = new System.Drawing.Point(633, 269);
            this.txtPriceMaximum.Margin = new System.Windows.Forms.Padding(4);
            this.txtPriceMaximum.Name = "txtPriceMaximum";
            this.txtPriceMaximum.Size = new System.Drawing.Size(175, 22);
            this.txtPriceMaximum.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(460, 272);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 17);
            this.label18.TabIndex = 73;
            this.label18.Text = "Price Maximum:";
            // 
            // txtPriceMinimum
            // 
            this.txtPriceMinimum.Location = new System.Drawing.Point(633, 237);
            this.txtPriceMinimum.Margin = new System.Windows.Forms.Padding(4);
            this.txtPriceMinimum.Name = "txtPriceMinimum";
            this.txtPriceMinimum.Size = new System.Drawing.Size(175, 22);
            this.txtPriceMinimum.TabIndex = 16;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(460, 240);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(111, 17);
            this.label19.TabIndex = 71;
            this.label19.Text = "Price Miniumum:";
            // 
            // txtBidAskStackNumberRows
            // 
            this.txtBidAskStackNumberRows.Location = new System.Drawing.Point(199, 444);
            this.txtBidAskStackNumberRows.Margin = new System.Windows.Forms.Padding(4);
            this.txtBidAskStackNumberRows.Name = "txtBidAskStackNumberRows";
            this.txtBidAskStackNumberRows.Size = new System.Drawing.Size(175, 22);
            this.txtBidAskStackNumberRows.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(16, 444);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(163, 17);
            this.label20.TabIndex = 81;
            this.label20.Text = "Bid Stack Number Rows:";
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(633, 410);
            this.txtStatus.Margin = new System.Windows.Forms.Padding(4);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(175, 22);
            this.txtStatus.TabIndex = 22;
            this.txtStatus.Text = "O";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(460, 414);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 17);
            this.label21.TabIndex = 83;
            this.label21.Text = "Status:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(20, 492);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 17);
            this.label22.TabIndex = 85;
            this.label22.Text = "Open Time:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(460, 458);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 17);
            this.label23.TabIndex = 87;
            this.label23.Text = "Close Time:";
            // 
            // txtPurgeFrequency
            // 
            this.txtPurgeFrequency.Location = new System.Drawing.Point(200, 520);
            this.txtPurgeFrequency.Margin = new System.Windows.Forms.Padding(4);
            this.txtPurgeFrequency.Name = "txtPurgeFrequency";
            this.txtPurgeFrequency.Size = new System.Drawing.Size(175, 22);
            this.txtPurgeFrequency.TabIndex = 25;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(21, 524);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(121, 17);
            this.label24.TabIndex = 89;
            this.label24.Text = "Purge Frequency:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(460, 503);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(133, 17);
            this.label25.TabIndex = 91;
            this.label25.Text = "Purge Time Of Day:";
            // 
            // chkIgnoreMarketStatusTriggers
            // 
            this.chkIgnoreMarketStatusTriggers.AutoSize = true;
            this.chkIgnoreMarketStatusTriggers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkIgnoreMarketStatusTriggers.Location = new System.Drawing.Point(18, 551);
            this.chkIgnoreMarketStatusTriggers.Margin = new System.Windows.Forms.Padding(4);
            this.chkIgnoreMarketStatusTriggers.Name = "chkIgnoreMarketStatusTriggers";
            this.chkIgnoreMarketStatusTriggers.Size = new System.Drawing.Size(280, 21);
            this.chkIgnoreMarketStatusTriggers.TabIndex = 27;
            this.chkIgnoreMarketStatusTriggers.Text = "Ignore Automatic Market Status Triggers";
            this.chkIgnoreMarketStatusTriggers.UseVisualStyleBackColor = true;
            // 
            // chkAllowNegativePriceForExternalDeals
            // 
            this.chkAllowNegativePriceForExternalDeals.AutoSize = true;
            this.chkAllowNegativePriceForExternalDeals.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkAllowNegativePriceForExternalDeals.Location = new System.Drawing.Point(18, 579);
            this.chkAllowNegativePriceForExternalDeals.Margin = new System.Windows.Forms.Padding(4);
            this.chkAllowNegativePriceForExternalDeals.Name = "chkAllowNegativePriceForExternalDeals";
            this.chkAllowNegativePriceForExternalDeals.Size = new System.Drawing.Size(274, 21);
            this.chkAllowNegativePriceForExternalDeals.TabIndex = 28;
            this.chkAllowNegativePriceForExternalDeals.Text = "Allow Negative Price For External Deals";
            this.chkAllowNegativePriceForExternalDeals.UseVisualStyleBackColor = true;
            // 
            // chkDisplayOptionalPriceDetail
            // 
            this.chkDisplayOptionalPriceDetail.AutoSize = true;
            this.chkDisplayOptionalPriceDetail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkDisplayOptionalPriceDetail.Location = new System.Drawing.Point(464, 551);
            this.chkDisplayOptionalPriceDetail.Margin = new System.Windows.Forms.Padding(4);
            this.chkDisplayOptionalPriceDetail.Name = "chkDisplayOptionalPriceDetail";
            this.chkDisplayOptionalPriceDetail.Size = new System.Drawing.Size(205, 21);
            this.chkDisplayOptionalPriceDetail.TabIndex = 29;
            this.chkDisplayOptionalPriceDetail.Text = "Display Optional Price Detail";
            this.chkDisplayOptionalPriceDetail.UseVisualStyleBackColor = true;
            // 
            // txtOrderVolumeDefault
            // 
            this.txtOrderVolumeDefault.Location = new System.Drawing.Point(199, 239);
            this.txtOrderVolumeDefault.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderVolumeDefault.Name = "txtOrderVolumeDefault";
            this.txtOrderVolumeDefault.Size = new System.Drawing.Size(175, 22);
            this.txtOrderVolumeDefault.TabIndex = 10;
            // 
            // txtOpenTime
            // 
            this.txtOpenTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.txtOpenTime.Location = new System.Drawing.Point(199, 484);
            this.txtOpenTime.Margin = new System.Windows.Forms.Padding(4);
            this.txtOpenTime.Name = "txtOpenTime";
            this.txtOpenTime.Size = new System.Drawing.Size(175, 22);
            this.txtOpenTime.TabIndex = 23;
            // 
            // txtCloseTime
            // 
            this.txtCloseTime.CustomFormat = "";
            this.txtCloseTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.txtCloseTime.Location = new System.Drawing.Point(633, 450);
            this.txtCloseTime.Margin = new System.Windows.Forms.Padding(4);
            this.txtCloseTime.Name = "txtCloseTime";
            this.txtCloseTime.Size = new System.Drawing.Size(175, 22);
            this.txtCloseTime.TabIndex = 24;
            // 
            // txtPurgeTimeOfDay
            // 
            this.txtPurgeTimeOfDay.CustomFormat = "";
            this.txtPurgeTimeOfDay.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.txtPurgeTimeOfDay.Location = new System.Drawing.Point(633, 499);
            this.txtPurgeTimeOfDay.Margin = new System.Windows.Forms.Padding(4);
            this.txtPurgeTimeOfDay.Name = "txtPurgeTimeOfDay";
            this.txtPurgeTimeOfDay.Size = new System.Drawing.Size(175, 22);
            this.txtPurgeTimeOfDay.TabIndex = 26;
            // 
            // txtProductTenorName
            // 
            this.txtProductTenorName.Location = new System.Drawing.Point(195, 618);
            this.txtProductTenorName.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorName.Name = "txtProductTenorName";
            this.txtProductTenorName.Size = new System.Drawing.Size(175, 22);
            this.txtProductTenorName.TabIndex = 30;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(16, 621);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(144, 17);
            this.label26.TabIndex = 100;
            this.label26.Text = "Product Tenor Name:";
            // 
            // txtProductTenorDisplayName
            // 
            this.txtProductTenorDisplayName.Location = new System.Drawing.Point(604, 618);
            this.txtProductTenorDisplayName.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorDisplayName.Name = "txtProductTenorDisplayName";
            this.txtProductTenorDisplayName.Size = new System.Drawing.Size(200, 22);
            this.txtProductTenorDisplayName.TabIndex = 31;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(401, 621);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(194, 17);
            this.label27.TabIndex = 102;
            this.label27.Text = "Product Tenor Display Name:";
            // 
            // txtProductTenorDeliveryDateEnd
            // 
            this.txtProductTenorDeliveryDateEnd.Location = new System.Drawing.Point(604, 648);
            this.txtProductTenorDeliveryDateEnd.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorDeliveryDateEnd.Name = "txtProductTenorDeliveryDateEnd";
            this.txtProductTenorDeliveryDateEnd.Size = new System.Drawing.Size(200, 22);
            this.txtProductTenorDeliveryDateEnd.TabIndex = 33;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(401, 652);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(126, 17);
            this.label28.TabIndex = 106;
            this.label28.Text = "Delivery Date End:";
            // 
            // txtProductTenorDeliveryDateStart
            // 
            this.txtProductTenorDeliveryDateStart.Location = new System.Drawing.Point(195, 648);
            this.txtProductTenorDeliveryDateStart.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorDeliveryDateStart.Name = "txtProductTenorDeliveryDateStart";
            this.txtProductTenorDeliveryDateStart.Size = new System.Drawing.Size(175, 22);
            this.txtProductTenorDeliveryDateStart.TabIndex = 32;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(16, 652);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(131, 17);
            this.label29.TabIndex = 104;
            this.label29.Text = "Delivery Date Start:";
            // 
            // txtProductTenorRoleDateRule
            // 
            this.txtProductTenorRoleDateRule.Location = new System.Drawing.Point(604, 708);
            this.txtProductTenorRoleDateRule.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorRoleDateRule.Name = "txtProductTenorRoleDateRule";
            this.txtProductTenorRoleDateRule.Size = new System.Drawing.Size(200, 22);
            this.txtProductTenorRoleDateRule.TabIndex = 35;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(401, 712);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(103, 17);
            this.label30.TabIndex = 110;
            this.label30.Text = "Roll Date Rule:";
            // 
            // txtProductTenorRollDate
            // 
            this.txtProductTenorRollDate.Location = new System.Drawing.Point(195, 708);
            this.txtProductTenorRollDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorRollDate.Name = "txtProductTenorRollDate";
            this.txtProductTenorRollDate.Size = new System.Drawing.Size(175, 22);
            this.txtProductTenorRollDate.TabIndex = 34;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 712);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(70, 17);
            this.label31.TabIndex = 108;
            this.label31.Text = "Roll Date:";
            // 
            // txtProductTenorMinimumDeliveryRange
            // 
            this.txtProductTenorMinimumDeliveryRange.Location = new System.Drawing.Point(195, 748);
            this.txtProductTenorMinimumDeliveryRange.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorMinimumDeliveryRange.Name = "txtProductTenorMinimumDeliveryRange";
            this.txtProductTenorMinimumDeliveryRange.Size = new System.Drawing.Size(175, 22);
            this.txtProductTenorMinimumDeliveryRange.TabIndex = 36;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(16, 752);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(168, 17);
            this.label32.TabIndex = 112;
            this.label32.Text = "Minimum Delivery Range:";
            // 
            // chkHasAssessment
            // 
            this.chkHasAssessment.AutoSize = true;
            this.chkHasAssessment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkHasAssessment.Location = new System.Drawing.Point(464, 50);
            this.chkHasAssessment.Margin = new System.Windows.Forms.Padding(4);
            this.chkHasAssessment.Name = "chkHasAssessment";
            this.chkHasAssessment.Size = new System.Drawing.Size(131, 21);
            this.chkHasAssessment.TabIndex = 113;
            this.chkHasAssessment.Text = "Has Assessment";
            this.chkHasAssessment.UseVisualStyleBackColor = true;
            // 
            // txtDeliveryPeriodLabel
            // 
            this.txtDeliveryPeriodLabel.Location = new System.Drawing.Point(604, 778);
            this.txtDeliveryPeriodLabel.Margin = new System.Windows.Forms.Padding(4);
            this.txtDeliveryPeriodLabel.Name = "txtDeliveryPeriodLabel";
            this.txtDeliveryPeriodLabel.Size = new System.Drawing.Size(200, 22);
            this.txtDeliveryPeriodLabel.TabIndex = 115;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(401, 782);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(147, 17);
            this.label33.TabIndex = 117;
            this.label33.Text = "Delivery Period Label:";
            // 
            // txtLocationLabel
            // 
            this.txtLocationLabel.Location = new System.Drawing.Point(195, 778);
            this.txtLocationLabel.Margin = new System.Windows.Forms.Padding(4);
            this.txtLocationLabel.Name = "txtLocationLabel";
            this.txtLocationLabel.Size = new System.Drawing.Size(175, 22);
            this.txtLocationLabel.TabIndex = 114;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(16, 782);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(105, 17);
            this.label34.TabIndex = 116;
            this.label34.Text = "Location Label:";
            // 
            // chkCoBrokering
            // 
            this.chkCoBrokering.AutoSize = true;
            this.chkCoBrokering.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkCoBrokering.Location = new System.Drawing.Point(464, 579);
            this.chkCoBrokering.Margin = new System.Windows.Forms.Padding(4);
            this.chkCoBrokering.Name = "chkCoBrokering";
            this.chkCoBrokering.Size = new System.Drawing.Size(220, 21);
            this.chkCoBrokering.TabIndex = 118;
            this.chkCoBrokering.Text = "Co-Brokering (Product Toggle)";
            this.chkCoBrokering.UseVisualStyleBackColor = true;
            // 
            // txtDisplayOrder
            // 
            this.txtDisplayOrder.Location = new System.Drawing.Point(633, 159);
            this.txtDisplayOrder.Margin = new System.Windows.Forms.Padding(4);
            this.txtDisplayOrder.Name = "txtDisplayOrder";
            this.txtDisplayOrder.Size = new System.Drawing.Size(175, 22);
            this.txtDisplayOrder.TabIndex = 124;
            this.txtDisplayOrder.TextChanged += new System.EventHandler(this.txtDisplayOrder_TextChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(460, 162);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(99, 17);
            this.label37.TabIndex = 125;
            this.label37.Text = "Display Order:";
            // 
            // txtProductTenorDefaultEndDate
            // 
            this.txtProductTenorDefaultEndDate.Location = new System.Drawing.Point(604, 678);
            this.txtProductTenorDefaultEndDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorDefaultEndDate.Name = "txtProductTenorDefaultEndDate";
            this.txtProductTenorDefaultEndDate.Size = new System.Drawing.Size(200, 22);
            this.txtProductTenorDefaultEndDate.TabIndex = 127;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(401, 681);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(120, 17);
            this.label35.TabIndex = 129;
            this.label35.Text = "Default End Date:";
            // 
            // txtProductTenorDefaultStartDate
            // 
            this.txtProductTenorDefaultStartDate.Location = new System.Drawing.Point(195, 678);
            this.txtProductTenorDefaultStartDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductTenorDefaultStartDate.Name = "txtProductTenorDefaultStartDate";
            this.txtProductTenorDefaultStartDate.Size = new System.Drawing.Size(175, 22);
            this.txtProductTenorDefaultStartDate.TabIndex = 126;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(16, 681);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(125, 17);
            this.label36.TabIndex = 128;
            this.label36.Text = "Default Start Date:";
            // 
            // ProductCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 867);
            this.Controls.Add(this.txtProductTenorDefaultEndDate);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.txtProductTenorDefaultStartDate);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.txtDisplayOrder);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.chkCoBrokering);
            this.Controls.Add(this.txtDeliveryPeriodLabel);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.txtLocationLabel);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.chkHasAssessment);
            this.Controls.Add(this.txtProductTenorMinimumDeliveryRange);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.txtProductTenorRoleDateRule);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.txtProductTenorRollDate);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.txtProductTenorDeliveryDateEnd);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.txtProductTenorDeliveryDateStart);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.txtProductTenorDisplayName);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.txtProductTenorName);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.txtPurgeTimeOfDay);
            this.Controls.Add(this.txtCloseTime);
            this.Controls.Add(this.txtOpenTime);
            this.Controls.Add(this.txtOrderVolumeDefault);
            this.Controls.Add(this.chkDisplayOptionalPriceDetail);
            this.Controls.Add(this.chkAllowNegativePriceForExternalDeals);
            this.Controls.Add(this.chkIgnoreMarketStatusTriggers);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtPurgeFrequency);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtBidAskStackNumberRows);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtPriceSignificantFigures);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtPriceDecimalPlaces);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtPriceIncrement);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtPriceMaximum);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtPriceMinimum);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtOrderVolumeSignificantFigures);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtOrderVolumeDecimalPlaces);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtOrderVolumeIncrement);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtOrderVolumeMaximum);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtOrderVolumeMinimum);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cboCurrencyCode);
            this.Controls.Add(this.cboPriceUnitCode);
            this.Controls.Add(this.cboVolumeUnitCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cboLocation);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboContractType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboMarket);
            this.Controls.Add(this.chkIsInternal);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.chkIsDeleted);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboCommodity);
            this.Controls.Add(this.txtProductName);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ProductCreate";
            this.Text = "Create Product";
            this.Load += new System.EventHandler(this.ProductCreate_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboCommodity;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkIsInternal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboMarket;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboContractType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboLocation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboVolumeUnitCode;
        private System.Windows.Forms.ComboBox cboPriceUnitCode;
        private System.Windows.Forms.ComboBox cboCurrencyCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOrderVolumeMinimum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtOrderVolumeMaximum;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtOrderVolumeIncrement;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtOrderVolumeDecimalPlaces;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtOrderVolumeSignificantFigures;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPriceSignificantFigures;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPriceDecimalPlaces;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPriceIncrement;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPriceMaximum;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPriceMinimum;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtBidAskStackNumberRows;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtPurgeFrequency;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox chkIgnoreMarketStatusTriggers;
        private System.Windows.Forms.CheckBox chkAllowNegativePriceForExternalDeals;
        private System.Windows.Forms.CheckBox chkDisplayOptionalPriceDetail;
        private System.Windows.Forms.TextBox txtOrderVolumeDefault;
        private System.Windows.Forms.DateTimePicker txtOpenTime;
        private System.Windows.Forms.DateTimePicker txtCloseTime;
        private System.Windows.Forms.DateTimePicker txtPurgeTimeOfDay;
        private System.Windows.Forms.TextBox txtProductTenorName;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtProductTenorDisplayName;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtProductTenorDeliveryDateEnd;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtProductTenorDeliveryDateStart;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtProductTenorRoleDateRule;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtProductTenorRollDate;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtProductTenorMinimumDeliveryRange;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox chkHasAssessment;
        private System.Windows.Forms.TextBox txtDeliveryPeriodLabel;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtLocationLabel;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.CheckBox chkCoBrokering;
        private System.Windows.Forms.TextBox txtDisplayOrder;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtProductTenorDefaultEndDate;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtProductTenorDefaultStartDate;
        private System.Windows.Forms.Label label36;
    }
}