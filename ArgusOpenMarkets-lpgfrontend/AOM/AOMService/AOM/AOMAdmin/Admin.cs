﻿using System.Data.Entity;
using AOM.Services.ProductService;
using AOM.Transport.Events.Products;
using AOMAdmin.CsvHelpers;
using AOM.Services.EncryptionService;
using System.Reflection;
using AOM.App.Domain.Dates;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Events.News;
using System;
using System.Configuration;
using System.Windows.Forms;
using AOM.Repository.MySql;
using AOM.Services.CrmService;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Services.PermissionService;
using Argus.Transport.Infrastructure;
using Ninject;

namespace AOMAdmin
{
    public partial class Admin : Form
    {
        private readonly IBus _bus;
        private readonly IKernel _kernel;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IProductService _productService;

        public Admin(IBus bus,
            IKernel kernel,
            IDateTimeProvider dateTimeProvider,
            IProductService productService)
        {
            _bus = bus;
            _kernel = kernel;
            _dateTimeProvider = dateTimeProvider;
            _productService = productService;
            InitializeComponent();
            // Force the MDI background colour to match what was set for this window.
            foreach (Control control in Controls)
            {
                MdiClient client = control as MdiClient;
                if (client != null)
                {
                    client.BackColor = BackColor;
                    MethodInfo method = (control).GetType()
                        .GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
                    method.Invoke(control, new Object[] {ControlStyles.OptimizedDoubleBuffer, true});
                    break;
                }
            }
            FormUtils.AppendVersionToTitle(this);
            string server = ConfigurationManager.ConnectionStrings["CrmModel"].ConnectionString.Split(';')[0].Split('=')[1];
            string database = ConfigurationManager.ConnectionStrings["CrmModel"].ConnectionString.Split(';')[4].Split('=')[1];
            Text = string.Format("{0} -  using: {1} / {2}", Text, server, database );
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            mnuMenu.Hide();

            if (AdminSettings.LoggedOn)
            {
                // We can assume we're logged in.
                mnuMenu.Show();
            }
            else
            {
                Close();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void manageUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var factory = new DbContextFactory();
            var crmService = new CrmAdministrationService(factory, _dateTimeProvider);
            var productService = new ProductService(factory, _dateTimeProvider);
            var frm = new UserMain(
                crmService,
                _bus,
                new UserService(factory, _dateTimeProvider, productService),
                new OrganisationService(new DbContextFactory(), new BilateralPermissionsBuilder()),
                new EncryptionService(),
                new CredentialsService(factory, _dateTimeProvider)) {StartPosition = FormStartPosition.CenterParent};

            frm.ShowDialog();
        }

        private void manageOrganisationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var factory = new DbContextFactory();
            var productService = new ProductService(factory, new DateTimeProvider());
            var frm = new OrganisationMain(
                _bus,
                new OrganisationService(new DbContextFactory(), new BilateralPermissionsBuilder()),
                new CrmAdministrationService(factory, _dateTimeProvider),
                new UserService(factory, _dateTimeProvider, productService),
                new DateTimeProvider(),
                new EncryptionService(),
                new CredentialsService(factory, _dateTimeProvider),
                productService);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

        }

        private void purgeNewsNowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new PurgeNews(_bus, _dateTimeProvider) {StartPosition = FormStartPosition.CenterParent};
            frm.ShowDialog();
        }

        private void manageProductsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var crmService = new CrmAdministrationService(new DbContextFactory(), _dateTimeProvider);
            var frm = new ProductMain(crmService, _bus, _productService)
            {
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void Admin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void configureAutoPurgeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var frm = new SchedulePurgeTask(new ScheduledTaskCaller<PurgeNewsRequest>(_bus), _dateTimeProvider)
            {
                Text = @"Purge News Ticker Schedule",
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void purgeOrdersInDiffusionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new PurgeOrdersInDiffusion(_bus)
            {
                Text = @"Purge Orders In Diffusion",
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void nowToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void configureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new SchedulePurgeProductOrdersTask(new ScheduledTaskCaller<PurgeOrdersForProductRequest>(_bus),
                _dateTimeProvider)
            {
                Text = @"Purge Orders For Product",
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void configureAutoOpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new SchedulePurgeProductOrdersTask(new ScheduledTaskCaller<ProductMarketOpenRequest>(_bus),
                _dateTimeProvider)
            {
                Text = @"Market Open",
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void configureAutoCloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new SchedulePurgeProductOrdersTask(new ScheduledTaskCaller<ProductMarketCloseRequest>(_bus),
                _dateTimeProvider)
            {
                Text = @"Market Close",
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void configureAutoPurgeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            var frm = new SchedulePurgeTask(new ScheduledTaskCaller<PurgeMarketTickerRequest>(_bus), _dateTimeProvider)
            {
                Text = @"Purge Market Ticker Schedule",
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void purgeMarketInfoNowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new PurgeMarketTicker(_bus, _dateTimeProvider) {StartPosition = FormStartPosition.CenterParent};
            frm.ShowDialog();
        }

        private void deleteDiffusionTopicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new DeleteDiffusionTopic(_bus) {StartPosition = FormStartPosition.CenterParent};
            frm.ShowDialog();
        }

        private void createMultipleOrganisationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new CreateMultipleOrganisations {StartPosition = FormStartPosition.CenterParent};
            frm.ShowDialog();
        }

        //private void aOMMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    var frm = new AOMMonitor(_bus);
        //    frm.StartPosition = FormStartPosition.CenterParent;
        //    frm.ShowDialog();
        //}

        private void importOrganisationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dbContextFactory = new DbContextFactory();
            var dateTimeProvider = new DateTimeProvider();
            var crmAdministrationService = new CrmAdministrationService(dbContextFactory, dateTimeProvider);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var organisationHelper = new Helpers.OrganisationHelper(crmAdministrationService, organisationService);
            var organisationCsvHelper = new OrganisationCsvImporter(organisationService, organisationHelper);

            var frm = new CsvImportForm(organisationCsvHelper)
            {
                Text = "CSV Import Organisations",
                CsvHeaderText =
                    "Short Code | Name | Legal Name | Address | Email | Organisation Type | Description | IsDeleted"
            };
            frm.ShowDialog();
        }

        private void importOrganisationProductsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dbContextFactory = new DbContextFactory();
            var dateTimeProvider = new DateTimeProvider();
            var crmAdministrationService = new CrmAdministrationService(dbContextFactory, dateTimeProvider);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var defaultOrganisationRoleHelper = new Helpers.DefaultOrganisationRoleHelper(crmAdministrationService, _productService);
            var organisationProductsCsvImporter = new OrganisationProductsCsvImporter(organisationService, _productService, defaultOrganisationRoleHelper);

            var frm = new CsvImportForm(organisationProductsCsvImporter)
            {
                Text = "Csv Import Organisation Products",
                CsvHeaderText = "Short Code | ProductIds"
            };
            frm.ShowDialog();
        }

        private void importUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var factory = new DbContextFactory();
            var productService = new ProductService(factory, _dateTimeProvider);
            var userService = new UserService(factory, new DateTimeProvider(), productService);
            var dbContextFactory = new DbContextFactory();
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var dateTimeProvider = new DateTimeProvider();

            var userHelper = new Helpers.UserHelper(
                new CrmAdministrationService(dbContextFactory, dateTimeProvider), new EncryptionService(), userService,
                new CredentialsService(dbContextFactory, dateTimeProvider));
            var userCsvImpoter = new UserCsvImporter(organisationService, userService, userHelper);


            var frm = new CsvImportForm(userCsvImpoter)
            {
                Text = "Csv Import Users",
                CsvHeaderText =
                    "<Organisation Id> OR <Organisation ShortCode> | Email | Name |Title | Telephone | Username | IsActive | IsDeleted | IsBlocked | ArgusCrmUsername | Password | Temporary Password | Number Hours Expire Password"
            };
            frm.ShowDialog();
        }

        private void importUserRolesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var factory = new DbContextFactory();
            var productService = new ProductService(factory, _dateTimeProvider);
            var userService = new UserService(factory, new DateTimeProvider(), productService);
            var dbContextFactory = new DbContextFactory();
            var dateTimeProvider = new DateTimeProvider();
            var crmAdministrationService = new CrmAdministrationService(dbContextFactory, dateTimeProvider);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            
            var userRoleCsvHelper = new UserRoleCsvImporter(crmAdministrationService, organisationService, userService);
            var userRoleHelper = new Helpers.UserRoleHelper(crmAdministrationService, organisationService);

            var frm = new CsvImportUserRoles(_bus, userRoleCsvHelper, userRoleHelper);
            frm.Text = "Csv Import User Roles";
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
        }

        private void importCounterpartyPermissionsToolStripMenuItem_Click(object sender, EventArgs e)
        {           
            var dbContextFactory = new DbContextFactory();
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());            
            var userService = new UserService(dbContextFactory, new DateTimeProvider(), _productService);
            var emailService = new EmailService(userService, organisationService, new DateTimeProvider(),
                _productService); 
            var errorService = new ErrorService(dbContextFactory, new DateTimeProvider());
            var counterpartyPermissionService = new CounterpartyPermissionService(_bus, dbContextFactory, organisationService, emailService, errorService);
            var counterpartyPermissionsCsvHelper = new CounterpartyPermissionsCsvImporter(organisationService, _productService, counterpartyPermissionService);

            var frm = new CsvImportForm(counterpartyPermissionsCsvHelper)
            {
                Text = "Csv Import Counterparty permissions",
                CsvHeaderText = "ProductId | OurOrganisationCode | BuyOrSell | TheirOrganisationCode | AllowOrDeny"
            };
            frm.ShowDialog();
        }

        private void importBrokerPermissionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dbContextFactory = new DbContextFactory();
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());            
            var userService = new UserService(dbContextFactory, new DateTimeProvider(), _productService);
            var emailService = new EmailService(userService, organisationService, new DateTimeProvider(),
                _productService);
            var errorService = new ErrorService(dbContextFactory, new DateTimeProvider());
            var brokerPermissionService = new BrokerPermissionService(_bus, dbContextFactory, organisationService, emailService, errorService);
            var brokerPermissionsCsvImporter = new BrokerPermissionsCsvImporter(organisationService, _productService, brokerPermissionService);

            var frm = new CsvImportForm(brokerPermissionsCsvImporter)
            {
                Text = "Csv Import Broker permissions",
                CsvHeaderText = "ProductId | OurOrganisationCode | BuyOrSell | TheirOrganisationCode | AllowOrDeny"
            };
            frm.ShowDialog();
        }

        private void changeAllUsersPasswordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var factory = new DbContextFactory();
            var productService = new ProductService(factory, _dateTimeProvider);
            var userService = new UserService(factory, new DateTimeProvider(), productService);
            var organisationService = new OrganisationService(new DbContextFactory(), new BilateralPermissionsBuilder());
            var enService = new EncryptionService();
            var frm = new UserChangePasswords(_bus, userService, organisationService, enService) { StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var factory = new DbContextFactory();
            var productService = new ProductService(factory, _dateTimeProvider);
            var userService = new UserService(factory, new DateTimeProvider(), productService);
            var organisationService = new OrganisationService(new DbContextFactory(), new BilateralPermissionsBuilder());
            var enService = new EncryptionService();
            var frm = new CreateEmails(_bus, userService, organisationService, enService);
            frm.ShowDialog();
        }

        private void monitorDiffusionTopicsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var smtpService = new SmtpService();
            var frm = new DiffusionTopicMonitor(_bus, smtpService) { StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
        }
    }
}