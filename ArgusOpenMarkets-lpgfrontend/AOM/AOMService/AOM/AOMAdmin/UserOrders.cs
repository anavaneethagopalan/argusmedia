﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using Argus.Transport.Infrastructure;
using Message = AOM.Transport.Events.Message;

namespace AOMAdmin
{
    using Utils.Logging.Utils;

    public partial class UserOrders : Form
    {
        private readonly IBus _bus;

        private readonly ICrmAdministrationService CrmAdministrationService;

        private readonly IUserService _userService;

        private readonly BackgroundWorker _fillOrdersBackgroundWorker;

        private IUser _user;

        public UserOrders(
            IBus bus,
            ICrmAdministrationService crmAdministrationService,
            IUserService userService,
            User user = null)
        {
            _bus = bus;
            CrmAdministrationService = crmAdministrationService;
            _user = user;
            _userService = userService;

            InitializeComponent();

            _fillOrdersBackgroundWorker = new BackgroundWorker();
            _fillOrdersBackgroundWorker.DoWork += _fillOrdersBackgroundWorker_DoWork;
            _fillOrdersBackgroundWorker.RunWorkerCompleted += _fillOrdersBackgroundWorker_RunWorkerCompleted;

            _UsersOrdersCheckedListbox.FormattingEnabled = true;
            _UsersOrdersCheckedListbox.Format += (sender, args) =>
            {
                var order = ((Order)args.ListItem);

                args.Value = string.Format(
                    "{0} - {1} - {2}@{3} - {4}",
                    order.Id,
                    order.ProductName,
                    order.Quantity,
                    order.Price,
                    order.LastUpdated.ToString(("dd/MM/yyyy HH:mm:ss")));
            };
        }

        private void LoadOrders()
        {
            if (_user == null)
            {
                MessageBox.Show("Must find a valid user first");
                return;
            }

            _GetOrdersButton.Enabled = false;
            _DeleteSelectedOrdersButton.Enabled = false;
            _SelectAllOrdersButton.Enabled = false;
            cmdFindUser.Enabled = false;

            _fillOrdersBackgroundWorker.RunWorkerAsync(_user.Id);
        }

        private void _GetOrdersButton_Click(object sender, EventArgs e)
        {
            LoadOrders();
        }

        private void _fillOrdersBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                IEnumerable<Order> ordersByUserId =
                    _bus.Request<UserOrderRequestRpc, List<Order>>(new UserOrderRequestRpc {UserId = (long) e.Argument});

                if (ordersByUserId == null)
                {
                    return;
                }

                Invoke((Action) (() => _UsersOrdersCheckedListbox.Items.Clear()));

                //fill in list box
                foreach (var order in ordersByUserId)
                {
                    var argument = order;

                    Invoke((Action) (() => _UsersOrdersCheckedListbox.Items.Add(argument, false)));
                }
            }
            catch (Exception ex)
            {
                Log.Error("Fill Orders Background Error", ex);
            }

            //test throwing error - caught in completed?
        }

        private void _fillOrdersBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _GetOrdersButton.Enabled = true;
            _DeleteSelectedOrdersButton.Enabled = true;
            _SelectAllOrdersButton.Enabled = true;
            cmdFindUser.Enabled = true;
        }

        private void ManageUsersOrders_Load(object sender, EventArgs e)
        {
//            _bus.Subscribe<AomOrderResponse>(
//                string.Join("-", "AOMAdminTool", Environment.MachineName, Process.GetCurrentProcess().Id),
//                onAomOrderResponse);

            if (_user != null)
            {
                cmdFindUser.Visible = false;
                _UsernameTextbox.Text = _user.Name + " (" + _user.Username + ")";
                _UsernameTextbox.Enabled = false;
                Text = _user.Name + ": Manage Orders";

                _GetOrdersButton.Enabled = true;
                _DeleteSelectedOrdersButton.Enabled = true;
                _SelectAllOrdersButton.Enabled = true;
                LoadOrders();

            }
        }

        private void _FindUserButton_Click(object sender, EventArgs e)
        {
            _UserIdLabel.Text = string.Empty;
            _GetOrdersButton.Enabled = true;
            _DeleteSelectedOrdersButton.Enabled = true;
            _SelectAllOrdersButton.Enabled = true;

            var username = _UsernameTextbox.Text;

            if (string.IsNullOrEmpty(username))
            {
                MessageBox.Show("Please enter a username");
            }
            else
            {
                var user = _userService.GetUserWithPrivileges(username);

                if (user == null)
                {
                    MessageBox.Show("Server response should not be null");
                }
                else
                {
                    _UserIdLabel.Text = user.Username;
                    _user = user;

                    _GetOrdersButton.Enabled = true;
                    _DeleteSelectedOrdersButton.Enabled = true;
                    _SelectAllOrdersButton.Enabled = true;

                    LoadOrders();
                }
            }
        }

        private void _UserIdLabel_Click(object sender, EventArgs e)
        {

        }

        private void _UsernameTextbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void _SelectAllOrdersButton_Click(object sender, EventArgs e)
        {
            for (var i = 0; i < _UsersOrdersCheckedListbox.Items.Count; i++)
            {
                _UsersOrdersCheckedListbox.SetItemChecked(i, true);
            }
        }

        private void _DeleteSelectedOrdersButton_Click(object sender, EventArgs e)
        {
            int lastIndex = _UsersOrdersCheckedListbox.Items.Count - 1;

            for (int i = lastIndex; i >= 0; i--)
            {
                if (_UsersOrdersCheckedListbox.GetItemCheckState(i) == CheckState.Checked)
                {
                    var item = _UsersOrdersCheckedListbox.Items[i];

                    var response = new AomOrderAuthenticationResponse
                    {
                        Order = (Order)item,
                        MarketMustBeOpen = false,
                        Message = new Message
                        {
                            MessageAction = MessageAction.Kill,
                            MessageType = MessageType.Order,
                            ClientSessionInfo = new ClientSessionInfo
                            {
                                UserId = AdminSettings.LoggedOnUser.Id
                            }
                        }
                    };

                    _bus.Publish(response);
                }
            }
        }

        private void _UsersOrdersCheckedListbox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
