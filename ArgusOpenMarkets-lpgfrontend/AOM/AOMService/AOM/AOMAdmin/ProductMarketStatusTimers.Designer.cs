﻿namespace AOMAdmin
{
    partial class ProductMarketStatusTimers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductMarketStatusTimers));
            this.grid = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OpenTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CloseTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCmdOpen = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdClose = new System.Windows.Forms.DataGridViewButtonColumn();
            this.IgnoreAutomaticMarketStatusTriggersColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.saveChanges = new System.Windows.Forms.Button();
            this.refresh = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.NameColumn,
            this.OpenTimeColumn,
            this.CloseTimeColumn,
            this.colCmdOpen,
            this.colCmdClose,
            this.IgnoreAutomaticMarketStatusTriggersColumn});
            this.grid.Location = new System.Drawing.Point(12, 12);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(696, 60);
            this.grid.TabIndex = 0;
            this.grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellContentClick);
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "Id";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            this.IdColumn.Width = 35;
            // 
            // NameColumn
            // 
            this.NameColumn.DataPropertyName = "Name";
            this.NameColumn.HeaderText = "Name";
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.ReadOnly = true;
            this.NameColumn.Width = 180;
            // 
            // OpenTimeColumn
            // 
            this.OpenTimeColumn.DataPropertyName = "OpenTime";
            this.OpenTimeColumn.HeaderText = "OpenTime";
            this.OpenTimeColumn.Name = "OpenTimeColumn";
            this.OpenTimeColumn.Width = 60;
            // 
            // CloseTimeColumn
            // 
            this.CloseTimeColumn.DataPropertyName = "CloseTime";
            this.CloseTimeColumn.HeaderText = "CloseTime";
            this.CloseTimeColumn.Name = "CloseTimeColumn";
            this.CloseTimeColumn.Width = 60;
            // 
            // colCmdOpen
            // 
            this.colCmdOpen.HeaderText = "Open Market";
            this.colCmdOpen.Name = "colCmdOpen";
            // 
            // colCmdClose
            // 
            this.colCmdClose.HeaderText = "Close Market";
            this.colCmdClose.Name = "colCmdClose";
            this.colCmdClose.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // IgnoreAutomaticMarketStatusTriggersColumn
            // 
            this.IgnoreAutomaticMarketStatusTriggersColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IgnoreAutomaticMarketStatusTriggersColumn.DataPropertyName = "IgnoreAutomaticMarketStatusTriggers";
            this.IgnoreAutomaticMarketStatusTriggersColumn.HeaderText = "Ignore Automatic Market Status Triggers";
            this.IgnoreAutomaticMarketStatusTriggersColumn.Name = "IgnoreAutomaticMarketStatusTriggersColumn";
            this.IgnoreAutomaticMarketStatusTriggersColumn.Width = 149;
            // 
            // saveChanges
            // 
            this.saveChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveChanges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveChanges.Location = new System.Drawing.Point(93, 78);
            this.saveChanges.Name = "saveChanges";
            this.saveChanges.Size = new System.Drawing.Size(75, 23);
            this.saveChanges.TabIndex = 2;
            this.saveChanges.Text = "Save Changes";
            this.saveChanges.UseVisualStyleBackColor = true;
            this.saveChanges.Click += new System.EventHandler(this.saveChanges_Click);
            // 
            // refresh
            // 
            this.refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refresh.Location = new System.Drawing.Point(12, 78);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(75, 23);
            this.refresh.TabIndex = 1;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(614, 78);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // ProductMarketStatusTimers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 106);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.saveChanges);
            this.Controls.Add(this.grid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductMarketStatusTimers";
            this.Text = "Configure Product Market Status Timers";
            this.Load += new System.EventHandler(this.ConfigureProductMarketStatusTimers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.Button saveChanges;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn OpenTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CloseTimeColumn;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdOpen;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdClose;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IgnoreAutomaticMarketStatusTriggersColumn;
    }
}