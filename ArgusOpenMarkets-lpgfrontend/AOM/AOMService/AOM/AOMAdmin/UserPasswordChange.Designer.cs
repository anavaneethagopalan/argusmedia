﻿namespace AOMAdmin
{
    using System;

    partial class UserPasswordChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserPasswordChange));
            this.cmdChangeUserPassword = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNewPasswordConfirm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.TempPassCB = new System.Windows.Forms.CheckBox();
            this.TempExpirHours = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TempExpirHours)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdChangeUserPassword
            // 
            this.cmdChangeUserPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdChangeUserPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdChangeUserPassword.Location = new System.Drawing.Point(173, 223);
            this.cmdChangeUserPassword.Name = "cmdChangeUserPassword";
            this.cmdChangeUserPassword.Size = new System.Drawing.Size(136, 23);
            this.cmdChangeUserPassword.TabIndex = 5;
            this.cmdChangeUserPassword.Text = "Change Password";
            this.cmdChangeUserPassword.UseVisualStyleBackColor = true;
            this.cmdChangeUserPassword.Click += new System.EventHandler(this.cmdChangeUserPassword_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "New Password:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(177, 12);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(218, 20);
            this.txtUserName.TabIndex = 0;
            this.txtUserName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(17, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "Username:";
            // 
            // txtNewPasswordConfirm
            // 
            this.txtNewPasswordConfirm.Location = new System.Drawing.Point(177, 92);
            this.txtNewPasswordConfirm.Name = "txtNewPasswordConfirm";
            this.txtNewPasswordConfirm.PasswordChar = '*';
            this.txtNewPasswordConfirm.Size = new System.Drawing.Size(218, 20);
            this.txtNewPasswordConfirm.TabIndex = 2;
            this.txtNewPasswordConfirm.TextChanged += new System.EventHandler(this.txtNewPasswordConfirm_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Confirm New Password:";
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Location = new System.Drawing.Point(177, 62);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '*';
            this.txtNewPassword.Size = new System.Drawing.Size(218, 20);
            this.txtNewPassword.TabIndex = 1;
            this.txtNewPassword.TextChanged += new System.EventHandler(this.txtNewPassword_TextChanged);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 188);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(46, 13);
            this.lblMessage.TabIndex = 11;
            this.lblMessage.Text = "ERROR";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(320, 223);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 6;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // TempPassCB
            // 
            this.TempPassCB.AutoSize = true;
            this.TempPassCB.Checked = true;
            this.TempPassCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TempPassCB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TempPassCB.Location = new System.Drawing.Point(177, 124);
            this.TempPassCB.Name = "TempPassCB";
            this.TempPassCB.Size = new System.Drawing.Size(223, 17);
            this.TempPassCB.TabIndex = 3;
            this.TempPassCB.Text = "Temporary (Prompt to change on 1st login)";
            this.TempPassCB.UseVisualStyleBackColor = true;
            // 
            // TempExpirHours
            // 
            this.TempExpirHours.Location = new System.Drawing.Point(177, 147);
            this.TempExpirHours.Name = "TempExpirHours";
            this.TempExpirHours.Size = new System.Drawing.Size(46, 20);
            this.TempExpirHours.TabIndex = 4;
            this.TempExpirHours.Value = new decimal(new int[] {
            48,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Temp Pass Expiration (Hours)";
            // 
            // UserPasswordChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 253);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TempExpirHours);
            this.Controls.Add(this.TempPassCB);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.txtNewPassword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNewPasswordConfirm);
            this.Controls.Add(this.cmdChangeUserPassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUserName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserPasswordChange";
            this.Text = "Change User Password";
            this.Load += new System.EventHandler(this.ChangeUserPassword_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TempExpirHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdChangeUserPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNewPasswordConfirm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.CheckBox TempPassCB;
        private System.Windows.Forms.NumericUpDown TempExpirHours;
        private System.Windows.Forms.Label label4;
    }
}