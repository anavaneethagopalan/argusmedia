﻿namespace AOMAdmin
{
    using System;
    using System.Windows.Forms;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.Users;

    using Argus.Transport.Infrastructure;

    public partial class UserPermissions : Form
    {
        private bool IgnoreRoleAssignCheck;

        private readonly IBus _bus;

        private readonly ICrmAdministrationService _crmAdministrationService;

        private readonly IUserService _userService;

        private readonly IOrganisationService _organisationService;

        private IUser _user;

        public UserPermissions(
            IBus bus,
            ICrmAdministrationService crmAdministrationService,
            IUserService userService,
            IOrganisationService organisationService,
            User user = null)
        {
            _bus = bus;
            _crmAdministrationService = crmAdministrationService;
            _user = user;
            _userService = userService;
            _organisationService = organisationService;
            InitializeComponent();
        }

        private void SetUserPermissions_Load(object sender, EventArgs e)
        {
            if (_user != null)
            {
                _user.UserOrganisation = _organisationService.GetOrganisationById(_user.OrganisationId) as Organisation;
                Text = string.Format("{0} ({1}): User Permissions", _user.Name, _user.Username);
                cmdFindUser.Visible = false;
                label2.Visible = false;
                txtUserToFind.Enabled = false;
                txtUserToFind.Text = _user.Username;
                DisplayUserDetails();
                DisplayUserOrganisationRoles();
            }
        }

        private void cmdFindUser_Click(object sender, EventArgs e)
        {
            cmdFindUser.Enabled = false;
            var userToFindUsername = txtUserToFind.Text;
            IUser userToFind = null;

            if (!string.IsNullOrEmpty(userToFindUsername))
            {
                var requestRpc = new GetUserRequestRpc { Username = userToFindUsername, UserId = 0 };

                userToFind = _bus.Request<GetUserRequestRpc, GetUserResponse>(requestRpc).User;
            }

            if (userToFind != null)
            {
                _user = userToFind;
                DisplayUserDetails();
                DisplayUserOrganisationRoles();
            }
            else
            {
                cmdFindUser.Enabled = true;
            }
        }

        private void DisplayUserDetails()
        {
            if (_user != null)
            {
                lblUserFound.Text = _user.Username;
                lblName.Text = _user.Name;
                lblEmail.Text = _user.Email;
                lblOrganisation.Text = _user.UserOrganisation.Name;
            }
            else
            {
                lblUserFound.Text = string.Empty;
                lblName.Text = string.Empty;
                lblEmail.Text = string.Empty;
                lblOrganisation.Text = string.Empty;
            }
        }


        private void DisplayUserOrganisationRoles()
        {
            var organisationId = _user.UserOrganisation.Id;
            var organisationRoles = 
                OrganisationUtils.GetNonSystemOrganisationRoles(organisationId, _organisationService);
            var userOrganisationRoles = _userService.GetUserOrganisationRoles(_user.Id);

            lstRoles.Items.Clear();
            IgnoreRoleAssignCheck = true;
            foreach (var or in organisationRoles)
            {
                var lri = new ListRoleItem(or.Id, or.Name, or.Description);
                lstRoles.Items.Add(lri, userOrganisationRoles.Contains(or.Id));
            }
            IgnoreRoleAssignCheck = false;
        }

        private void lstRoles_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!IgnoreRoleAssignCheck)
            {
                // The user has checked an item.
                if (e != null)
                {
                    var role = lstRoles.Items[e.Index] as ListRoleItem;

                    if (role != null)
                    {
                        if (role.RoleId > 0)
                        {
                            if (e.CurrentValue != e.NewValue)
                            {
                                if (e.NewValue == CheckState.Checked)
                                {
                                    // We are adding this role to this user.
                                    AddRoleToUser(_user, role.RoleId);
                                }
                                else
                                {
                                    // We are removing this role from this user.
                                    RemoveRoleForUser(_user, role.RoleId);
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool AddRoleToUser(IUser user, long roleId)
        {
            return _crmAdministrationService.AddRoleToUser(user.Id, roleId, AdminSettings.LoggedOnUser.Id);
        }

        public bool RemoveRoleForUser(IUser user, long roleId)
        {

            return _crmAdministrationService.RemoveRoleForUser(user.Id, roleId, AdminSettings.LoggedOnUser.Id);
        }

        private void txtUserToFind_TextChanged(object sender, EventArgs e)
        {
            // Set us up ready to find another user.
            if (txtUserToFind.Enabled)
            {
                cmdFindUser.Enabled = true;
                _user = null;
                DisplayUserDetails();
                lstRoles.Items.Clear();
            }
        }

        private void lstRoles_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }

    internal class ListRoleItem
    {
        public ListRoleItem(long roleId, string name, string description)
        {
            RoleId = roleId;
            Name = name;
            Description = description;
        }

        public string Name { get; set; }

        public long RoleId { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            return String.Format("{0}  ({1})", Name, Description);
        }
    }
}