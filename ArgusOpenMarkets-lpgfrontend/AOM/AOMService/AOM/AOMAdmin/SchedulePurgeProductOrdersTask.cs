﻿using Utils.Logging.Utils;

namespace AOMAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;


    public partial class SchedulePurgeProductOrdersTask : Form
    {
        private readonly IScheduledTaskCaller _taskCaller;

        private readonly IDateTimeProvider _dateTimeProvider;

        public SchedulePurgeProductOrdersTask(IScheduledTaskCaller taskCaller, IDateTimeProvider dateTimeProvider)
        {
            _taskCaller = taskCaller;
            _dateTimeProvider = dateTimeProvider;
            InitializeComponent();
            cmbTaskFrequency.DataSource = Enum.GetNames(typeof(TaskRepeatInterval));
            dtpFirstRun.MinDate = _dateTimeProvider.UtcNow;
            dtpFirstRunTime.MinDate = _dateTimeProvider.UtcNow;
            dgScheduledTasks.MultiSelect = false;
            RefreshScheduledTaskList();
        }


        private void RefreshScheduledTaskList()
        {
            try
            {

            var tasks = _taskCaller.GetAllTasks();
            dgScheduledTasks.DataSource = tasks;
            btnDeleteTask.Enabled = tasks.Count >= 1;

            }
            catch (Exception ex)
            {
                Log.Error("Admin Tool - Error", ex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btnOK.Enabled = false;
            TaskRepeatInterval interval;
            Enum.TryParse<TaskRepeatInterval>(cmbTaskFrequency.SelectedValue.ToString(), out interval);

            var bamp = new { ProductId = (int)numDaysToKeep.Value };

            var task = new ScheduledTask
                       {
                           TaskType = _taskCaller.GetTaskName(),
                           TaskName = txtTaskName.Text,
                           SkipIfMissed = cbSkipIfMissed.Checked,
                           DateCreated = _dateTimeProvider.UtcNow,
                           Arguments =
                               Newtonsoft.Json.JsonConvert
                               .DeserializeObject<Dictionary<string, string>>(
                                   Newtonsoft.Json.JsonConvert.SerializeObject(bamp)),
                           Interval = interval,
                           NextRun = dtpFirstRun.Value.Date.Add(dtpFirstRunTime.Value.TimeOfDay)
                       };

            _taskCaller.CreateNewScheduledTask(task);
            btnOK.Enabled = true;
            RefreshScheduledTaskList();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDeleteTask_Click(object sender, EventArgs e)
        {
            btnDeleteTask.Enabled = false;
            _taskCaller.DeleteScheduledTask(((ScheduledTask)dgScheduledTasks.SelectedRows[0].DataBoundItem).Id);
            RefreshScheduledTaskList();
            btnDeleteTask.Enabled = true;
        }

        private void SchedulePurgeProductOrdersTask_Load(object sender, EventArgs e)
        {

        }
    }
}