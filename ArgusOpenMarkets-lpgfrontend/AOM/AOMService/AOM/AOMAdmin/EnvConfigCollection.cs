﻿using System.Configuration;

namespace AOMAdmin
{
    public class EnvConfigCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new EnvConfig();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as EnvConfig).Name;
        }

        protected override string ElementName
        {
            get { return "envConfig"; }
        }

        protected override bool IsElementName(string elementName)
        {
            return !string.IsNullOrEmpty(elementName) && elementName == "envConfig";
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        public EnvConfig this[int index]
        {
            get
            {
                return BaseGet(index) as EnvConfig;
            }
        }

        public new EnvConfig this[string key]
        {
            get
            {
                return BaseGet(key) as EnvConfig;
            }
        }
    }
}
