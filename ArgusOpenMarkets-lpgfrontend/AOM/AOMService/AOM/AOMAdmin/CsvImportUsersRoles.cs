﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using AOM.App.Domain.Entities;
using AOMAdmin.CsvHelpers;
using AOMAdmin.Helpers;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class CsvImportUserRoles : Form
    {
        private IBus _bus;
        private UserRoleCsvImporter _userRoleCsvImporter;
        private UserRoleHelper _userRoleHelper;

        public CsvImportUserRoles(IBus bus, UserRoleCsvImporter userRoleCsvImporter, UserRoleHelper userRoleHelper)
        {
            _userRoleCsvImporter = userRoleCsvImporter;
            _userRoleHelper = userRoleHelper;
            _bus = bus;
            InitializeComponent();
        }

        private void CsvImportUserRoles_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filename = "";

            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                filename = ofd.FileName;
            }

            if (!string.IsNullOrEmpty(filename))
            {
                txtUserRolesCsvFile.Text = filename;
                LoadCsvFile(filename);
            }

            EnableButton();
        }

        private void EnableButton()
        {
            if (lstFileContent.Items.Count > 0)
            {
                cmdImportUserRoles.Enabled = true;
            }
            else
            {
                cmdImportUserRoles.Enabled = false;
            }
        }

        private void LoadCsvFile(string filename)
        {
            lstFileContent.Items.Clear();
            txtResults.Text = "";

            var firstLine = true;

            // TODO: We need to load the CSV file and place into list box.
            var file = File.ReadAllLines(filename);
            foreach (var line in file)
            {
                if (!firstLine)
                {
                    var validationResponse = _userRoleCsvImporter.ValidateCsvLine(line);

                    if (validationResponse.IsValid)
                    {
                        lstFileContent.Items.Add(line);
                    }
                    else
                    {
                        // Add this line to the list of line errors.  
                        txtResults.AppendText(string.Format("ERROR: {0} - {1}", validationResponse.ErrorMessage, line) + Environment.NewLine);
                    }
                }
                firstLine = false;
            }
        }


        private void cmdImportUsers_Click(object sender, EventArgs e)
        {
            List<string> messages = new List<string>();
            foreach (string userLine in this.lstFileContent.Items)
            {
                var csvUser = _userRoleCsvImporter.GetCsvUserRole(userLine);
                var message = CreateUserRole(csvUser);
                messages.Add(message);
            }

            txtResults.Text = "";
            foreach (var message in messages)
            {
                txtResults.AppendText(message + Environment.NewLine);
            }
        }

        private string CreateUserRole(CsvUserRole csvUserRole)
        {
            var userInfoRole = new UserInfoRole
            {
                UserId = csvUserRole.UserId,
                RoleId = csvUserRole.RoleId                
            };

            // TODO - call the User helper - to persist the user.
            return _userRoleHelper.CreateUserRole(userInfoRole);
        }
    }
}