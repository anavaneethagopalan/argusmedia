﻿namespace AOMAdmin
{
    partial class ProductMetadataListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.buttonsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.listTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridMetadataLists = new System.Windows.Forms.DataGridView();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridMetadataListItems = new System.Windows.Forms.DataGridView();
            this.DisplayNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemValueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.listsButtonsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnDeleteMetadataList = new System.Windows.Forms.Button();
            this.btnAddMetadataList = new System.Windows.Forms.Button();
            this.listItemsButtonsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnDeleteMetadataListItem = new System.Windows.Forms.Button();
            this.btnAddMetadataListItem = new System.Windows.Forms.Button();
            this.upDownButtonsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMoveListItemUp = new System.Windows.Forms.Button();
            this.btnMoveListItemDown = new System.Windows.Forms.Button();
            this.mainTableLayoutPanel.SuspendLayout();
            this.buttonsFlowLayoutPanel.SuspendLayout();
            this.listTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMetadataLists)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMetadataListItems)).BeginInit();
            this.listsButtonsPanel.SuspendLayout();
            this.listItemsButtonsPanel.SuspendLayout();
            this.upDownButtonsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTableLayoutPanel
            // 
            this.mainTableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTableLayoutPanel.ColumnCount = 1;
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayoutPanel.Controls.Add(this.buttonsFlowLayoutPanel, 0, 1);
            this.mainTableLayoutPanel.Controls.Add(this.listTableLayoutPanel, 0, 0);
            this.mainTableLayoutPanel.Location = new System.Drawing.Point(10, 10);
            this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
            this.mainTableLayoutPanel.RowCount = 2;
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.mainTableLayoutPanel.Size = new System.Drawing.Size(583, 351);
            this.mainTableLayoutPanel.TabIndex = 0;
            // 
            // buttonsFlowLayoutPanel
            // 
            this.buttonsFlowLayoutPanel.AutoSize = true;
            this.buttonsFlowLayoutPanel.Controls.Add(this.btnCancel);
            this.buttonsFlowLayoutPanel.Controls.Add(this.btnSave);
            this.buttonsFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonsFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.buttonsFlowLayoutPanel.Location = new System.Drawing.Point(418, 319);
            this.buttonsFlowLayoutPanel.Name = "buttonsFlowLayoutPanel";
            this.buttonsFlowLayoutPanel.Size = new System.Drawing.Size(162, 29);
            this.buttonsFlowLayoutPanel.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(84, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(3, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // listTableLayoutPanel
            // 
            this.listTableLayoutPanel.ColumnCount = 4;
            this.listTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.listTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.listTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.listTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.listTableLayoutPanel.Controls.Add(this.dataGridMetadataLists, 0, 1);
            this.listTableLayoutPanel.Controls.Add(this.dataGridMetadataListItems, 2, 1);
            this.listTableLayoutPanel.Controls.Add(this.listsButtonsPanel, 0, 0);
            this.listTableLayoutPanel.Controls.Add(this.listItemsButtonsPanel, 2, 0);
            this.listTableLayoutPanel.Controls.Add(this.upDownButtonsPanel, 3, 1);
            this.listTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.listTableLayoutPanel.Name = "listTableLayoutPanel";
            this.listTableLayoutPanel.RowCount = 2;
            this.listTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.listTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 239F));
            this.listTableLayoutPanel.Size = new System.Drawing.Size(577, 310);
            this.listTableLayoutPanel.TabIndex = 1;
            // 
            // dataGridMetadataLists
            // 
            this.dataGridMetadataLists.AllowUserToAddRows = false;
            this.dataGridMetadataLists.AllowUserToDeleteRows = false;
            this.dataGridMetadataLists.AllowUserToResizeColumns = false;
            this.dataGridMetadataLists.AllowUserToResizeRows = false;
            this.dataGridMetadataLists.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridMetadataLists.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMetadataLists.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DescriptionColumn});
            this.dataGridMetadataLists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridMetadataLists.Location = new System.Drawing.Point(3, 40);
            this.dataGridMetadataLists.MultiSelect = false;
            this.dataGridMetadataLists.Name = "dataGridMetadataLists";
            this.dataGridMetadataLists.RowHeadersVisible = false;
            this.dataGridMetadataLists.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridMetadataLists.Size = new System.Drawing.Size(254, 267);
            this.dataGridMetadataLists.TabIndex = 0;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.DataPropertyName = "Description";
            this.DescriptionColumn.HeaderText = "Description";
            this.DescriptionColumn.Name = "DescriptionColumn";
            // 
            // dataGridMetadataListItems
            // 
            this.dataGridMetadataListItems.AllowUserToAddRows = false;
            this.dataGridMetadataListItems.AllowUserToDeleteRows = false;
            this.dataGridMetadataListItems.AllowUserToResizeRows = false;
            this.dataGridMetadataListItems.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridMetadataListItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMetadataListItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DisplayNameColumn,
            this.ItemValueColumn});
            this.dataGridMetadataListItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridMetadataListItems.Location = new System.Drawing.Point(283, 40);
            this.dataGridMetadataListItems.Name = "dataGridMetadataListItems";
            this.dataGridMetadataListItems.RowHeadersVisible = false;
            this.dataGridMetadataListItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridMetadataListItems.Size = new System.Drawing.Size(254, 267);
            this.dataGridMetadataListItems.TabIndex = 1;
            this.dataGridMetadataListItems.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridMetadataListItems_CellValidating);
            // 
            // DisplayNameColumn
            // 
            this.DisplayNameColumn.DataPropertyName = "DisplayName";
            this.DisplayNameColumn.HeaderText = "Display Name";
            this.DisplayNameColumn.Name = "DisplayNameColumn";
            // 
            // ItemValueColumn
            // 
            this.ItemValueColumn.DataPropertyName = "ItemValue";
            this.ItemValueColumn.HeaderText = "Item Value";
            this.ItemValueColumn.Name = "ItemValueColumn";
            // 
            // listsButtonsPanel
            // 
            this.listsButtonsPanel.AutoSize = true;
            this.listsButtonsPanel.Controls.Add(this.btnDeleteMetadataList);
            this.listsButtonsPanel.Controls.Add(this.btnAddMetadataList);
            this.listsButtonsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.listsButtonsPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.listsButtonsPanel.Location = new System.Drawing.Point(195, 3);
            this.listsButtonsPanel.Name = "listsButtonsPanel";
            this.listsButtonsPanel.Size = new System.Drawing.Size(62, 31);
            this.listsButtonsPanel.TabIndex = 2;
            // 
            // btnDeleteMetadataList
            // 
            this.btnDeleteMetadataList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteMetadataList.Image = global::AOMAdmin.Properties.Resources.minus;
            this.btnDeleteMetadataList.Location = new System.Drawing.Point(34, 3);
            this.btnDeleteMetadataList.Name = "btnDeleteMetadataList";
            this.btnDeleteMetadataList.Size = new System.Drawing.Size(25, 25);
            this.btnDeleteMetadataList.TabIndex = 1;
            this.btnDeleteMetadataList.UseVisualStyleBackColor = true;
            this.btnDeleteMetadataList.Click += new System.EventHandler(this.btnDeleteMetadataList_Click);
            // 
            // btnAddMetadataList
            // 
            this.btnAddMetadataList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddMetadataList.Image = global::AOMAdmin.Properties.Resources.plus;
            this.btnAddMetadataList.Location = new System.Drawing.Point(3, 3);
            this.btnAddMetadataList.Name = "btnAddMetadataList";
            this.btnAddMetadataList.Size = new System.Drawing.Size(25, 25);
            this.btnAddMetadataList.TabIndex = 0;
            this.btnAddMetadataList.UseVisualStyleBackColor = true;
            this.btnAddMetadataList.Click += new System.EventHandler(this.btnAddMetadataList_Click);
            // 
            // listItemsButtonsPanel
            // 
            this.listItemsButtonsPanel.AutoSize = true;
            this.listItemsButtonsPanel.Controls.Add(this.btnDeleteMetadataListItem);
            this.listItemsButtonsPanel.Controls.Add(this.btnAddMetadataListItem);
            this.listItemsButtonsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.listItemsButtonsPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.listItemsButtonsPanel.Location = new System.Drawing.Point(475, 3);
            this.listItemsButtonsPanel.Name = "listItemsButtonsPanel";
            this.listItemsButtonsPanel.Size = new System.Drawing.Size(62, 31);
            this.listItemsButtonsPanel.TabIndex = 3;
            // 
            // btnDeleteMetadataListItem
            // 
            this.btnDeleteMetadataListItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteMetadataListItem.Image = global::AOMAdmin.Properties.Resources.minus;
            this.btnDeleteMetadataListItem.Location = new System.Drawing.Point(34, 3);
            this.btnDeleteMetadataListItem.Name = "btnDeleteMetadataListItem";
            this.btnDeleteMetadataListItem.Size = new System.Drawing.Size(25, 25);
            this.btnDeleteMetadataListItem.TabIndex = 0;
            this.btnDeleteMetadataListItem.UseVisualStyleBackColor = true;
            this.btnDeleteMetadataListItem.Click += new System.EventHandler(this.btnDeleteMetadataListItem_Click);
            // 
            // btnAddMetadataListItem
            // 
            this.btnAddMetadataListItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddMetadataListItem.Image = global::AOMAdmin.Properties.Resources.plus;
            this.btnAddMetadataListItem.Location = new System.Drawing.Point(3, 3);
            this.btnAddMetadataListItem.Name = "btnAddMetadataListItem";
            this.btnAddMetadataListItem.Size = new System.Drawing.Size(25, 25);
            this.btnAddMetadataListItem.TabIndex = 1;
            this.btnAddMetadataListItem.UseVisualStyleBackColor = true;
            this.btnAddMetadataListItem.Click += new System.EventHandler(this.btnAddMetadataListItem_Click);
            // 
            // upDownButtonsPanel
            // 
            this.upDownButtonsPanel.AutoSize = true;
            this.upDownButtonsPanel.Controls.Add(this.btnMoveListItemUp);
            this.upDownButtonsPanel.Controls.Add(this.btnMoveListItemDown);
            this.upDownButtonsPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.upDownButtonsPanel.Location = new System.Drawing.Point(543, 40);
            this.upDownButtonsPanel.Name = "upDownButtonsPanel";
            this.upDownButtonsPanel.Size = new System.Drawing.Size(31, 62);
            this.upDownButtonsPanel.TabIndex = 4;
            // 
            // btnMoveListItemUp
            // 
            this.btnMoveListItemUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoveListItemUp.Image = global::AOMAdmin.Properties.Resources.up;
            this.btnMoveListItemUp.Location = new System.Drawing.Point(3, 3);
            this.btnMoveListItemUp.Name = "btnMoveListItemUp";
            this.btnMoveListItemUp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMoveListItemUp.Size = new System.Drawing.Size(25, 25);
            this.btnMoveListItemUp.TabIndex = 0;
            this.btnMoveListItemUp.UseVisualStyleBackColor = true;
            this.btnMoveListItemUp.Click += new System.EventHandler(this.btnMoveListItemUp_Click);
            // 
            // btnMoveListItemDown
            // 
            this.btnMoveListItemDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoveListItemDown.Image = global::AOMAdmin.Properties.Resources.down;
            this.btnMoveListItemDown.Location = new System.Drawing.Point(3, 34);
            this.btnMoveListItemDown.Name = "btnMoveListItemDown";
            this.btnMoveListItemDown.Size = new System.Drawing.Size(25, 25);
            this.btnMoveListItemDown.TabIndex = 1;
            this.btnMoveListItemDown.UseVisualStyleBackColor = true;
            this.btnMoveListItemDown.Click += new System.EventHandler(this.btnMoveListItemDown_Click);
            // 
            // ProductMetadataListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(605, 373);
            this.Controls.Add(this.mainTableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ProductMetadataListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Product Metadata Lists";
            this.mainTableLayoutPanel.ResumeLayout(false);
            this.mainTableLayoutPanel.PerformLayout();
            this.buttonsFlowLayoutPanel.ResumeLayout(false);
            this.listTableLayoutPanel.ResumeLayout(false);
            this.listTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMetadataLists)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMetadataListItems)).EndInit();
            this.listsButtonsPanel.ResumeLayout(false);
            this.listItemsButtonsPanel.ResumeLayout(false);
            this.upDownButtonsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel buttonsFlowLayoutPanel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TableLayoutPanel listTableLayoutPanel;
        private System.Windows.Forms.DataGridView dataGridMetadataLists;
        private System.Windows.Forms.DataGridView dataGridMetadataListItems;
        private System.Windows.Forms.FlowLayoutPanel listsButtonsPanel;
        private System.Windows.Forms.Button btnDeleteMetadataList;
        private System.Windows.Forms.Button btnAddMetadataList;
        private System.Windows.Forms.FlowLayoutPanel listItemsButtonsPanel;
        private System.Windows.Forms.Button btnDeleteMetadataListItem;
        private System.Windows.Forms.Button btnAddMetadataListItem;
        private System.Windows.Forms.FlowLayoutPanel upDownButtonsPanel;
        private System.Windows.Forms.Button btnMoveListItemUp;
        private System.Windows.Forms.Button btnMoveListItemDown;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemValueColumn;
    }
}