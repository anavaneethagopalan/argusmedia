﻿namespace AOMAdmin
{
    partial class CreateEmails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateEmails));
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrganisationId = new System.Windows.Forms.TextBox();
            this.cmdCreateDraftEmails = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBody = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 16);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Organisation Id";
            // 
            // txtOrganisationId
            // 
            this.txtOrganisationId.Location = new System.Drawing.Point(140, 16);
            this.txtOrganisationId.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrganisationId.Name = "txtOrganisationId";
            this.txtOrganisationId.Size = new System.Drawing.Size(132, 22);
            this.txtOrganisationId.TabIndex = 16;
            // 
            // cmdCreateDraftEmails
            // 
            this.cmdCreateDraftEmails.Location = new System.Drawing.Point(511, 465);
            this.cmdCreateDraftEmails.Margin = new System.Windows.Forms.Padding(4);
            this.cmdCreateDraftEmails.Name = "cmdCreateDraftEmails";
            this.cmdCreateDraftEmails.Size = new System.Drawing.Size(228, 28);
            this.cmdCreateDraftEmails.TabIndex = 14;
            this.cmdCreateDraftEmails.Text = "Create Draft Emails";
            this.cmdCreateDraftEmails.UseVisualStyleBackColor = true;
            this.cmdCreateDraftEmails.Click += new System.EventHandler(this.cmdDeleteDiffusionTopic_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 65);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Subject";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(189, 65);
            this.txtSubject.Margin = new System.Windows.Forms.Padding(4);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(347, 22);
            this.txtSubject.TabIndex = 18;
            this.txtSubject.Text = "Welcome to Argus Open Markets";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 95);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 21;
            this.label2.Text = "Body";
            // 
            // txtBody
            // 
            this.txtBody.Location = new System.Drawing.Point(189, 95);
            this.txtBody.Margin = new System.Windows.Forms.Padding(4);
            this.txtBody.Multiline = true;
            this.txtBody.Name = "txtBody";
            this.txtBody.Size = new System.Drawing.Size(550, 362);
            this.txtBody.TabIndex = 20;
            this.txtBody.Text = resources.GetString("txtBody.Text");
            this.txtBody.TextChanged += new System.EventHandler(this.txtBody_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(281, 16);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 17);
            this.label4.TabIndex = 23;
            this.label4.Text = "User Id:";
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(346, 16);
            this.txtUserId.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(194, 22);
            this.txtUserId.TabIndex = 22;
            // 
            // CreateEmails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 506);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBody);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtOrganisationId);
            this.Controls.Add(this.cmdCreateDraftEmails);
            this.Name = "CreateEmails";
            this.Text = "CreateEmails";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOrganisationId;
        private System.Windows.Forms.Button cmdCreateDraftEmails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBody;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUserId;
    }
}