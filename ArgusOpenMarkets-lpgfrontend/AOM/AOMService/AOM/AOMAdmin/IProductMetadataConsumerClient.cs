using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Transport.Events.Products;

namespace AOMAdmin
{
    public interface IProductMetadataConsumerClient : IDisposable
    {
        event Action<GetProductMetaDataResponse> OnProductMetadataResponse;
        void PublishProductMetadataRequest(long productId);
        OperationResultResponse AddMetadataItem(ProductMetaDataItem productMetadataItem);
        OperationResultResponse UpdateMetadataItem(ProductMetaDataItem productMetadataItem);
        OperationResultResponse DeleteMetadataItem(ProductMetaDataItem productMetadataItem);
        OperationResultResponse UpdateMetadataItemsDisplayOrder(long productId, long[] metadataItemIds);

        IEnumerable<MetaDataList> GetProductMetadataLists();
        OperationResultResponse UpdateProductMetadataLists(List<MetaDataList> metadataLists);

        void RefreshDiffusionTopics(long productId);
    }
}