﻿using System;
using System.Linq;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class ProductMetadataForm : Form
    {
        private readonly Product _product;
        private readonly IProductMetadataConsumerClient _productMetadataConsumerClient;

        public ProductMetadataForm(Product product, IBus bus)
        {
            _product = product;
            _productMetadataConsumerClient = new ProductMetadataConsumerClient(bus);
            _productMetadataConsumerClient.OnProductMetadataResponse += OnProductMetaDataResponse;
            
            InitializeComponent();
            dgMetadata.AutoGenerateColumns = false;
        }

        protected override void OnLoad(EventArgs e)
        {
            PublishProductMetadataRequest();
        }

        protected override void OnClosed(EventArgs e)
        {
            _productMetadataConsumerClient.Dispose();
        }

        private void OnProductMetaDataResponse(GetProductMetaDataResponse productMetadataResponse)
        {
            dgMetadata.Invoke(new Action(() =>
            {
                dgMetadata.DataSource = new BindingSource(productMetadataResponse.ProductMetaData.Fields.ToList(), null);
                var selectedProductMetadataItem = GetSelectedProductMetadataItem();

                if (selectedProductMetadataItem != null)
                {
                    SetSelectedProductMetadataItem(selectedProductMetadataItem);
                }
            }));
        }

        private void btnCreateMetadata_Click(object sender, EventArgs e)
        {
            var productMetadataItem = new ProductMetaDataItemString
            {
                ProductId = _product.ProductId,
                ValueMinimum = 0,
                ValueMaximum = 100
            };

            var metadataDialog = new ProductMetadataItemForm(productMetadataItem, _productMetadataConsumerClient) { CreateNewProductMetadataItem = true };
            if (metadataDialog.ShowDialog() == DialogResult.OK)
            {
                PublishProductMetadataRequest();
            }
        }

        private void EditMetadata_Click(object sender, EventArgs e)
        {
            var productMetadataItem = GetSelectedProductMetadataItem();
            if (productMetadataItem != null)
            {
                var metadataDialog = new ProductMetadataItemForm(productMetadataItem, _productMetadataConsumerClient);
                if (metadataDialog.ShowDialog() == DialogResult.OK)
                {
                    PublishProductMetadataRequest();
                }
            }
        }

        private void btnRefreshDiffusionTopics_Click(object sender, EventArgs e)
        {
            _productMetadataConsumerClient.RefreshDiffusionTopics(_product.ProductId);
        }
        
        private void btnDelete_Click(object sender, EventArgs e)
        {
            var productMetadataItem = GetSelectedProductMetadataItem();
            if (productMetadataItem != null)
            {
                if (MessageBoxService.ShowConfirmation(
                    this,
                    string.Format("Delete metadata item \"{0}\"?", productMetadataItem.DisplayName)))
                {
                    var response = _productMetadataConsumerClient.DeleteMetadataItem(productMetadataItem);
                    if (response.IsValid)
                    {
                        PublishProductMetadataRequest();
                    }
                    else
                    {
                        MessageBoxService.ShowWarning(this, response.ErrorMessage);
                    }
                }
            }
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            if (MoveSelectedMetadataItem(-1))
            {
                SendUpdateMetadataItemsDisplayOrderRequest();
            }
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            if (MoveSelectedMetadataItem(1))
            {
                SendUpdateMetadataItemsDisplayOrderRequest();
            }
        }

        private ProductMetaDataItem GetSelectedProductMetadataItem()
        {
            var bindingSource = dgMetadata.DataSource as BindingSource;
            if (bindingSource != null)
                return bindingSource.Current as ProductMetaDataItem;

            return null;
        }

        private void SetSelectedProductMetadataItem(ProductMetaDataItem productMetadataItem)
        {
            var bindingSource = dgMetadata.DataSource as BindingSource;
            if (bindingSource != null)
            {
                foreach (var currentMetadataItem in bindingSource.OfType<ProductMetaDataItem>())
                {
                    if (currentMetadataItem.Id == productMetadataItem.Id)
                        bindingSource.Position = bindingSource.IndexOf(currentMetadataItem);
                }

                bindingSource.ResetBindings(false);
            }
        }

        private void PublishProductMetadataRequest()
        {
            _productMetadataConsumerClient.PublishProductMetadataRequest(_product.ProductId);
        }

        private bool MoveSelectedMetadataItem(int step)
        {
            var bindingSource = dgMetadata.DataSource as BindingSource;
            if (bindingSource == null)
                return false;

            var selectedMetadataItem = GetSelectedProductMetadataItem();
            if (selectedMetadataItem == null)
                return false;

            var currentIndex = bindingSource.IndexOf(selectedMetadataItem);
            var targetIndex = currentIndex + step;
            if (targetIndex < 0 || targetIndex >= bindingSource.Count)
                return false;

            bindingSource.Remove(selectedMetadataItem);
            bindingSource.Insert(targetIndex, selectedMetadataItem);
            bindingSource.Position = targetIndex;
            bindingSource.ResetBindings(false);

            return true;
        }

        private void SendUpdateMetadataItemsDisplayOrderRequest()
        {
            var bindingSource = dgMetadata.DataSource as BindingSource;
            if (bindingSource == null)
                return;

            var response = _productMetadataConsumerClient.UpdateMetadataItemsDisplayOrder(
                _product.ProductId,
                bindingSource.OfType<ProductMetaDataItem>().Select(x => x.Id).ToArray());

            if (!response.IsValid)
            {
                MessageBoxService.ShowWarning(this, response.ErrorMessage);
            }
        }

        private void btnEditMetadataLists_Click(object sender, EventArgs e)
        {
            var selectedMetadataItem = GetSelectedProductMetadataItem();

            long metaDataListId = -1;
            if (selectedMetadataItem.GetType() == typeof(ProductMetaDataItemEnum))
                metaDataListId = ((ProductMetaDataItemEnum) selectedMetadataItem).MetadataList.Id;

            var editMetadataListsDialog = new ProductMetadataListForm(_productMetadataConsumerClient, metaDataListId);

            editMetadataListsDialog.ShowDialog(this);
        }

        private void ProductMetadataForm_Load(object sender, EventArgs e)
        {

        }
    }
}
