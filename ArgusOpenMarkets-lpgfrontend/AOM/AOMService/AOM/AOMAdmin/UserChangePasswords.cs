﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{ 
    public partial class UserChangePasswords : Form
    {
        private IBus _bus;
        private IUserService _userService;
        private IOrganisationService _organisationService;
        private IEncryptionService _enService;

        public UserChangePasswords(IBus bus, IUserService userService, IOrganisationService organisationService, IEncryptionService enService)
        {
            _bus = bus;
            _userService = userService;
            _enService = enService;
            _organisationService = organisationService;
            InitializeComponent();
        }

        private void UserChangePasswords_Load(object sender, EventArgs e)
        {

        }

        private void cmdChangePasswords_Click(object sender, EventArgs e)
        {
            var newPassword = this.txtNewPassword.Text;
            var usersToExclude = this.txtUsersToExclude.Text;

            this.cmdChangePasswords.Enabled = false;
            var orgs = _organisationService.GetOrganisations();
            foreach (var org in orgs)
            {
                var users = _userService.GetUsers(org.Id);
                foreach (var user in users)
                {
                    if (!ExcludeUser(user.Username, usersToExclude))
                    {
                        ChangeUserPassword(newPassword, user);
                    }
                }
            }

            this.cmdChangePasswords.Enabled = true;
        }

        private void ChangeUserPassword(string newPassword, User user)
        {
            var username = user.Username;
            var tempExpiration = 0;
            var hashedPassword = _enService.Hash(user, newPassword);
            var request = new ChangePasswordRequestRpc
            {
                Username = username,
                AdminResetPassword = true,
                OldPassword = hashedPassword,
                NewPassword = hashedPassword,
                CheckOldPassword = false,
                TempPassExpirInHours = tempExpiration,
                IsTemporary = false,
                RequestorUserId = AdminSettings.LoggedOnUser.Id
            };

            _bus.Request<ChangePasswordRequestRpc, ChangePasswordResponse>(request);

        }

        private bool ExcludeUser(string username, string usersToExclude)
        {
            usersToExclude = usersToExclude.ToLower();
            username = username.ToLower();

            var index = usersToExclude.IndexOf(username);
            return (index > -1);
        }
    }
}

