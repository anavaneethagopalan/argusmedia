﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class ProductTenors : Form
    {
        class DataItem
        {
            public long Id { get; set; }
            public string TenorName { get; set; }
            public DateTime? DateCreated { get; set; }
            public string DisplayName { get; set; }
            public string DeliveryDateStart { get; set; }
            public string DeliveryDateEnd { get; set; }
            public string RollDate { get; set; }
            public string RollDateRule { get; set; }
            public long MinimumDeliveryRange { get; set; }
            public long ProductId { get; set; }
            public string ProductName { get; set; }
            public bool IsDirty { get; set; }
        }

        private readonly IBus _bus;
        private readonly Product _product;

        public ProductTenors(IBus bus, Product product = null)
        {
            _bus = bus;
            _product = product;
            InitializeComponent();

            grid.AutoGenerateColumns = false;
        }

        private void PopulateProductTenors()
        {
            grid.DataSource = null;

            var allProducts = _bus.Request<GetAllActiveProductsRequestRpc, List<Product>>(new GetAllActiveProductsRequestRpc()).ToList();
            var productTenors = _bus.Request<GetAllProductTenorsRequestRpc, List<ProductTenor>>(new GetAllProductTenorsRequestRpc());

            grid.DataSource = productTenors.Select(tenor =>
            {
                string productName = string.Empty;
                var product = allProducts.SingleOrDefault(p => p.ProductId == tenor.Id);
                if (product != null)
                {
                    productName = product.Name;
                }

                return new DataItem
                {
                    Id = tenor.ProductId,
                    TenorName = tenor.Name,
                    DateCreated = tenor.DateCreated,
                    DisplayName = tenor.DisplayName,
                    //DeliveryDateStart = tenor.DeliveryDateStart,
                    //DeliveryDateEnd = tenor.DeliveryDateEnd,
                    //RollDate = tenor.RollDate,
                    //RollDateRule = tenor.RollDateRule,
                    //MinimumDeliveryRange = tenor.MinimumDeliveryRange,
                    ProductId = tenor.ProductId,
                    ProductName = !string.IsNullOrEmpty(productName) ? productName : string.Empty
                };
            }).Where(pt => _product == null || pt.ProductId == _product.ProductId).ToList();
        }

        private void ManageProductTenor_Load(object sender, EventArgs e)
        {
            if (_product != null)
            {
                Text = _product.Name + ": Manage Tenors";
            }
            PopulateProductTenors();

            _bus.Subscribe<AomProductTenorResponse>(string.Join("-", "AOMAdminTool", Environment.MachineName, Process.GetCurrentProcess().Id), response =>
            {
                var tenor = response.ProductTenor;

                if (InvokeRequired)
                {
                    Invoke((Action) (() => UpdateProductRow(tenor)));
                }
                else
                {
                    if (response.Message.MessageType == MessageType.Error)
                    {
                        MessageBox.Show(string.Format("Error Occurred: {0}", response.Message.MessageBody));
                    }
                    else
                    {
                        if (response.ProductTenor != null)
                        {
                            UpdateProductRow(tenor);
                        }
                    }
                }
            });
        }

        private void UpdateProductRow(ProductTenor tenor)
        {
            var products = (List<DataItem>) grid.DataSource;

            foreach (var dataItem in products)
            {
                if (dataItem.Id != tenor.Id) continue;
                dataItem.TenorName = tenor.Name;
                dataItem.DateCreated = tenor.DateCreated;
                dataItem.DisplayName = tenor.DisplayName;
                //dataItem.DeliveryDateStart = tenor.DeliveryDateStart;
                //dataItem.DeliveryDateEnd = tenor.DeliveryDateEnd;
                //dataItem.RollDate = tenor.RollDate;
                //dataItem.RollDateRule = tenor.RollDateRule;
                //dataItem.MinimumDeliveryRange = tenor.MinimumDeliveryRange;
                dataItem.ProductId = tenor.ProductId;
                dataItem.IsDirty = false;

                Refresh();

                break;
            }
        }

        private void grid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                ((List<DataItem>) grid.DataSource)[e.RowIndex].IsDirty = true;
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            var products = (List<DataItem>) grid.DataSource;
            var didUpdate = false;

            foreach (var dataItem in products)
            {
                if (!dataItem.IsDirty) continue;

                var tenor =
                    _bus.Request<GetProductTenorRequestRpc, AomProductTenorResponse>(new GetProductTenorRequestRpc
                    {
                        Id = dataItem.Id
                    }).ProductTenor;

                if (dataItem.DateCreated != null)
                {
                    tenor.DateCreated = DateTime.SpecifyKind((DateTime) dataItem.DateCreated, DateTimeKind.Utc);
                }

                tenor.DisplayName = dataItem.DisplayName;
                //tenor.DeliveryDateStart = dataItem.DeliveryDateStart;
                //tenor.DeliveryDateEnd = dataItem.DeliveryDateEnd;
                //tenor.RollDate = dataItem.RollDate;
                //tenor.RollDateRule = dataItem.RollDateRule;
                //tenor.MinimumDeliveryRange = dataItem.MinimumDeliveryRange;
                tenor.ProductId = dataItem.ProductId;

                var request = new AomProductTenorRequest
                {
                    ClientSessionInfo = new ClientSessionInfo
                    {
                        UserId = AdminSettings.LoggedOnUser.Id
                    },
                    MessageAction = MessageAction.Update,
                    Tenor = tenor
                };

                didUpdate = true;
                _bus.Publish(request);

                dataItem.IsDirty = false;
            }

            DialogResult = didUpdate ? DialogResult.Yes : DialogResult.No;
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            PopulateProductTenors();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            Close();
        }
    }
}