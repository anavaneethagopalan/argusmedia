﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using AOM.Transport.Events.Users;

namespace AOMAdmin
{
    public class LoginWebService
    {
        public int UserId { get; private set; }
        public bool IsAuthenticated { get; private set; }

        public void Authorise(string loginUrl, string loginData)
        {
            try
            {
                var request = (HttpWebRequest)HttpWebRequest.Create(loginUrl);

                request.Method = "POST";
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";

                //request.Referer = "http://localhost:81";
                request.ContentLength = 41;
                request.KeepAlive = true;
                request.Headers.Add("Keep-Alive: 300");
                request.AllowAutoRedirect = true;
                request.UserAgent = "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";

                Stream requestStream = null;
                if (loginData != null)
                {
                    //var content = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("as", "Password12") });
                    var buffer = Encoding.UTF8.GetBytes(loginData);
                    request.ContentLength = buffer.Length;
                    requestStream = request.GetRequestStream();
                    requestStream.Write(buffer, 0, buffer.Length);
                }
                else
                {
                    requestStream = request.GetRequestStream();
                }

                requestStream.Close();
                string responseText = string.Empty;

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        responseText = streamReader.ReadToEnd();
                        //{
                        //  "isAuthenticated": true,
                        //  "message": "User authenticated successfully",
                        //  "token": "fac22f8a-e716-43bb-aa77-ebc5b6b0d74c",
                        //  "userId": 6530,
                        //  "firstLogin": false,
                        //  "sessionToken": "a9448184-4ac0-4f30-9a51-bc18755c4778"
                        //}

                        if (!string.IsNullOrEmpty(responseText))
                        {
                            //string userToken = null;
                            //string message = null;
                            dynamic userId = null;
                            dynamic isAuthenticated = null;

                            JavaScriptSerializer jss = new JavaScriptSerializer();
                            var jsonDict = jss.Deserialize<Dictionary<string, dynamic>>(responseText);
                            //jsonDict.TryGetValue("token", out userToken);
                            //jsonDict.TryGetValue("message", out message);
                            jsonDict.TryGetValue("userId", out userId);
                            jsonDict.TryGetValue("isAuthenticated", out isAuthenticated);
                            
                            UserId = userId;
                            IsAuthenticated = isAuthenticated;
                        }
                    }
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("WebService Login Error: {0}", ex.Message));
            }
        }
    }
}
