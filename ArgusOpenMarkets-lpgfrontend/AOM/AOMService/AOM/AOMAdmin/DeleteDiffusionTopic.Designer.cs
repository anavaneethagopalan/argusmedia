﻿namespace AOMAdmin
{
    partial class DeleteDiffusionTopic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDiffusionTopicToDelete = new System.Windows.Forms.TextBox();
            this.lblDiffusionTopicToDelete = new System.Windows.Forms.Label();
            this.cmdDeleteDiffusionTopic = new System.Windows.Forms.Button();
            this.lblWarning = new System.Windows.Forms.Label();
            this.cmdClose = new System.Windows.Forms.Button();
            this.txtWildcard1From = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtWildcard1To = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtWildcard2To = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtWildcard2From = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtDiffusionTopicToDelete
            // 
            this.txtDiffusionTopicToDelete.Location = new System.Drawing.Point(154, 50);
            this.txtDiffusionTopicToDelete.Name = "txtDiffusionTopicToDelete";
            this.txtDiffusionTopicToDelete.Size = new System.Drawing.Size(397, 20);
            this.txtDiffusionTopicToDelete.TabIndex = 0;
            // 
            // lblDiffusionTopicToDelete
            // 
            this.lblDiffusionTopicToDelete.AutoSize = true;
            this.lblDiffusionTopicToDelete.Location = new System.Drawing.Point(20, 57);
            this.lblDiffusionTopicToDelete.Name = "lblDiffusionTopicToDelete";
            this.lblDiffusionTopicToDelete.Size = new System.Drawing.Size(128, 13);
            this.lblDiffusionTopicToDelete.TabIndex = 1;
            this.lblDiffusionTopicToDelete.Text = "Diffusion Topic To Delete";
            // 
            // cmdDeleteDiffusionTopic
            // 
            this.cmdDeleteDiffusionTopic.Location = new System.Drawing.Point(287, 251);
            this.cmdDeleteDiffusionTopic.Name = "cmdDeleteDiffusionTopic";
            this.cmdDeleteDiffusionTopic.Size = new System.Drawing.Size(171, 23);
            this.cmdDeleteDiffusionTopic.TabIndex = 2;
            this.cmdDeleteDiffusionTopic.Text = "Delete Diffusion Topic";
            this.cmdDeleteDiffusionTopic.UseVisualStyleBackColor = true;
            this.cmdDeleteDiffusionTopic.Click += new System.EventHandler(this.cmdDeleteDiffusionTopic_Click);
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarning.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Location = new System.Drawing.Point(16, 9);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(545, 25);
            this.lblWarning.TabIndex = 3;
            this.lblWarning.Text = "NOTE: Only Deletes Topics - does NOT re-create them";
            this.lblWarning.UseWaitCursor = true;
            // 
            // cmdClose
            // 
            this.cmdClose.Location = new System.Drawing.Point(473, 251);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(75, 23);
            this.cmdClose.TabIndex = 4;
            this.cmdClose.Text = "Close";
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // txtWildcard1From
            // 
            this.txtWildcard1From.Location = new System.Drawing.Point(151, 185);
            this.txtWildcard1From.Name = "txtWildcard1From";
            this.txtWildcard1From.Size = new System.Drawing.Size(100, 20);
            this.txtWildcard1From.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Wildcard 1 from";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(316, 192);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Wildcard 1 to";
            // 
            // txtWildcard1To
            // 
            this.txtWildcard1To.Location = new System.Drawing.Point(448, 185);
            this.txtWildcard1To.Name = "txtWildcard1To";
            this.txtWildcard1To.Size = new System.Drawing.Size(100, 20);
            this.txtWildcard1To.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(316, 227);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Wildcard 2 to";
            // 
            // txtWildcard2To
            // 
            this.txtWildcard2To.Location = new System.Drawing.Point(448, 220);
            this.txtWildcard2To.Name = "txtWildcard2To";
            this.txtWildcard2To.Size = new System.Drawing.Size(100, 20);
            this.txtWildcard2To.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 227);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Wildcard 2 from";
            // 
            // txtWildcard2From
            // 
            this.txtWildcard2From.Location = new System.Drawing.Point(151, 220);
            this.txtWildcard2From.Name = "txtWildcard2From";
            this.txtWildcard2From.Size = new System.Drawing.Size(100, 20);
            this.txtWildcard2From.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(478, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Example #1 - No Wildcard:  AOM/MarketTicker/ViewableBy/All/Products/1/Ask" +
    "/107137";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(501, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Example #2 - Single Wildcard:  AOM/MarketTicker/ViewableBy/All/Products/{" +
    "0}/Ask/107137";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(484, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Example #3 - Double Wildcard:  AOM/MarketTicker/ViewableBy/All/Products/{" +
    "0}/Ask/{1}";
            // 
            // DeleteDiffusionTopic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 286);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtWildcard2To);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtWildcard2From);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtWildcard1To);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtWildcard1From);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.cmdDeleteDiffusionTopic);
            this.Controls.Add(this.lblDiffusionTopicToDelete);
            this.Controls.Add(this.txtDiffusionTopicToDelete);
            this.Name = "DeleteDiffusionTopic";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Delete Diffusion Topic";
            this.Load += new System.EventHandler(this.DeleteDiffusionTopic_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDiffusionTopicToDelete;
        private System.Windows.Forms.Label lblDiffusionTopicToDelete;
        private System.Windows.Forms.Button cmdDeleteDiffusionTopic;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Button cmdClose;
        private System.Windows.Forms.TextBox txtWildcard1From;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtWildcard1To;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtWildcard2To;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtWildcard2From;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}