﻿namespace AOMAdmin
{
    partial class PurgeMarketTicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDaysToPurge = new System.Windows.Forms.Label();
            this.dtpPurgeDate = new System.Windows.Forms.DateTimePicker();
            this.btnPurge = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblDaysToPurge
            // 
            this.lblDaysToPurge.AutoSize = true;
            this.lblDaysToPurge.Location = new System.Drawing.Point(4, 5);
            this.lblDaysToPurge.Name = "lblDaysToPurge";
            this.lblDaysToPurge.Size = new System.Drawing.Size(137, 13);
            this.lblDaysToPurge.TabIndex = 0;
            this.lblDaysToPurge.Text = "Refresh Since (Inclusive of)";
            // 
            // dtpPurgeDate
            // 
            this.dtpPurgeDate.Location = new System.Drawing.Point(140, 3);
            this.dtpPurgeDate.Name = "dtpPurgeDate";
            this.dtpPurgeDate.Size = new System.Drawing.Size(135, 20);
            this.dtpPurgeDate.TabIndex = 1;
            // 
            // btnPurge
            // 
            this.btnPurge.Location = new System.Drawing.Point(67, 27);
            this.btnPurge.Name = "btnPurge";
            this.btnPurge.Size = new System.Drawing.Size(118, 24);
            this.btnPurge.TabIndex = 2;
            this.btnPurge.Text = "Refresh Ticker";
            this.btnPurge.UseVisualStyleBackColor = true;
            this.btnPurge.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(200, 28);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // PurgeMarketTicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 53);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPurge);
            this.Controls.Add(this.dtpPurgeDate);
            this.Controls.Add(this.lblDaysToPurge);
            this.Name = "PurgeMarketTicker";
            this.Text = "Refresh Market Ticker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDaysToPurge;
        private System.Windows.Forms.DateTimePicker dtpPurgeDate;
        private System.Windows.Forms.Button btnPurge;
        private System.Windows.Forms.Button btnCancel;
    }
}