﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class TreeView : Form
    {
        private const string ProductsRootNode = "Products";
        private const string UsersRootNode = "Users";

        private readonly IBus _bus;
        private readonly IOrganisationService _organisationService;
        private readonly IUserService _userService;

        public TreeView(IBus bus,
            IOrganisationService organisationService,
            IUserService userService)
        {
            _bus = bus;
            _organisationService = organisationService;
            _userService = userService;
            InitializeComponent();
        }

        private void TreeView_Load(object sender, EventArgs e)
        {

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Organisations":
                    LoadOrganisations(e.Node);
                    break;
                case UsersRootNode:
                    LoadUsers(e.Node);
                    break;
                case ProductsRootNode:
                    LoadProducts(e.Node);
                    break;
            }

        }

        private void LoadProducts(TreeNode treeNode)
        {
            var products = GetProducts(string.Empty);
            PopulateProducts(treeNode, products);
        }

        private void PopulateProducts(TreeNode treeNode, List<Product> products)
        {
            foreach (var product in products)
            {
                treeNode.Nodes.Add(new TreeNode {Name = "P|" + product.ProductId, Text = product.Name});
            }

            treeNode.Expand();
        }

        private void LoadUsers(TreeNode treeNode)
        {
            var users = GetUsers("");
            PopulateUsers(treeNode, users);
        }

        private void PopulateUsers(TreeNode treeNode, List<User> users)
        {
            foreach (var user in users)
            {
                treeNode.Nodes.Add(new TreeNode
                {
                    Name = "U|" + user.Id,
                    Text = string.Format("{0} (email:{1})", user.Name, user.Email)
                });
            }
        }

        private List<Product> GetProducts(string filter)
        {
            var products =
                _bus.Request<GetAllActiveProductsRequestRpc, List<Product>>(new GetAllActiveProductsRequestRpc())
                    .Where(p => p.Name.StartsWith(filter))
                    .OrderBy(p => p.Name).ToList();

            return products;
        }

        private List<User> GetUsers(string filter)
        {
            var users = _userService.GetUsers(-1)
                .Where(u => u.IsBlocked == false && u.IsDeleted == false && u.Username.StartsWith(filter))
                .OrderBy(u => u.Username).ToList();

            return users;
        }

        private void LoadOrganisations(TreeNode treeNode)
        {
            var organisations = _organisationService.GetOrganisations();
            foreach (var org in organisations)
            {
                treeNode.Nodes.Add(new TreeNode {Name = "O|" + org.Id, Text = org.Name});
            }
        }

        private void txtProductFilter_TextChanged(object sender, EventArgs e)
        {
            ApplyFilter(ProductsRootNode, txtProductFilter.Text);
        }

        private void ApplyFilter(string nodeToFind, string filter)
        {
            TreeNode nodeToFilter = null;

            var rootNode = treeView1.Nodes[0];
            foreach (TreeNode node in rootNode.Nodes)
            {
                if (node.Text == nodeToFind)
                {
                    nodeToFilter = node;
                    break;
                }
            }

            switch (nodeToFind)
            {
                case ProductsRootNode:
                    RemoveChildNodes(nodeToFilter);
                    var products = GetProducts(filter);
                    PopulateProducts(nodeToFilter, products);
                    break;
                case UsersRootNode:
                    RemoveChildNodes(nodeToFilter);
                    var users = GetUsers(filter);
                    PopulateUsers(nodeToFilter, users);
                    break;
            }
        }

        private void RemoveChildNodes(TreeNode nodeToFilter)
        {
            for (var i = nodeToFilter.Nodes.Count - 1; i >= 0; i--)
            {
                nodeToFilter.Nodes[i].Remove();
            }
        }

        private void txtUserFilter_TextChanged(object sender, EventArgs e)
        {
            ApplyFilter(UsersRootNode, txtUserFilter.Text);
        }
    }
}