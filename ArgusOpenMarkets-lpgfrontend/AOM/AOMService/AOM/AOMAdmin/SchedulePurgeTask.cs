﻿namespace AOMAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;


    public partial class SchedulePurgeTask : Form
    {
        private readonly IScheduledTaskCaller _taskCaller;

        private readonly IDateTimeProvider _dateTimeProvider;

        public SchedulePurgeTask(IScheduledTaskCaller taskCaller, IDateTimeProvider dateTimeProvider)
        {
            _taskCaller = taskCaller;
            _dateTimeProvider = dateTimeProvider;
            InitializeComponent();
            cmbTaskFrequency.DataSource = Enum.GetNames(typeof(TaskRepeatInterval));
            dtpFirstRun.MinDate = _dateTimeProvider.UtcNow;
            dtpFirstRunTime.MinDate = _dateTimeProvider.UtcNow;
            dgScheduledTasks.MultiSelect = false;
        }


        private void RefreshScheduledTaskList()
        {
//            dgScheduledTasks.DataSource = _taskCaller.GetAllTasks();
//            btnDeleteTask.Enabled = dgScheduledTasks.RowCount >= 1;

            var tasks = _taskCaller.GetAllTasks();

            if (InvokeRequired)
            {
                Invoke((Action)(() =>
                {
                    dgScheduledTasks.DataSource = tasks;
                    btnDeleteTask.Enabled = tasks.Count >= 1;
                }));
            }
            else
            {
                dgScheduledTasks.DataSource = tasks;
                btnDeleteTask.Enabled = tasks.Count >= 1;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDeleteTask_Click(object sender, EventArgs e)
        {
            var result =
                _taskCaller.DeleteScheduledTask(((ScheduledTask)dgScheduledTasks.SelectedRows[0].DataBoundItem).Id);

            MessageBox.Show(result);
            RefreshScheduledTaskList();
        }

        private void cmdCreate_Click(object sender, EventArgs e)
        {
            TaskRepeatInterval interval;
            Enum.TryParse<TaskRepeatInterval>(cmbTaskFrequency.SelectedValue.ToString(), out interval);

            var bamp = new { DaysToKeep = (int)numDaysToKeep.Value };

            var task = new ScheduledTask
            {
                TaskType = _taskCaller.GetTaskName(),
                TaskName = txtTaskName.Text,
                SkipIfMissed = cbSkipIfMissed.Checked,
                DateCreated = _dateTimeProvider.UtcNow,
                Arguments =
                    Newtonsoft.Json.JsonConvert
                    .DeserializeObject<Dictionary<string, string>>(
                        Newtonsoft.Json.JsonConvert.SerializeObject(bamp)),
                Interval = interval,
                NextRun = dtpFirstRun.Value.Date.Add(dtpFirstRunTime.Value.TimeOfDay)
            };

            var result = _taskCaller.CreateNewScheduledTask(task);

            MessageBox.Show(result);
            RefreshScheduledTaskList();
        }

        private void SchedulePurgeTask_Load(object sender, EventArgs e)
        {

        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Name == "tabPage2")
            {

                RefreshScheduledTaskList();
            }
        }
    }
}