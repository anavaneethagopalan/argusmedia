﻿using AOM.App.Domain.Interfaces;

namespace AOMAdmin
{
    static class AdminSettings
    {

        public static bool LoggedOn { get; set; }
        public static IUser LoggedOnUser { get; set; }
    }
}
