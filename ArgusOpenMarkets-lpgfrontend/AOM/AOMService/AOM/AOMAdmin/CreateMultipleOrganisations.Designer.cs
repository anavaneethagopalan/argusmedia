﻿namespace AOMAdmin
{
    partial class CreateMultipleOrganisations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumberOrganisationsToCreate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOrganisationName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrganisationLegalName = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboOrganisationType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.txtShortCode = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ArgusCrmUsernameTB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chkCreateUsers = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Number Organisations To Create (Max: 99):";
            // 
            // txtNumberOrganisationsToCreate
            // 
            this.txtNumberOrganisationsToCreate.Location = new System.Drawing.Point(268, 12);
            this.txtNumberOrganisationsToCreate.Name = "txtNumberOrganisationsToCreate";
            this.txtNumberOrganisationsToCreate.Size = new System.Drawing.Size(100, 20);
            this.txtNumberOrganisationsToCreate.TabIndex = 8;
            this.txtNumberOrganisationsToCreate.Text = "10";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Organisation Name";
            // 
            // txtOrganisationName
            // 
            this.txtOrganisationName.Location = new System.Drawing.Point(268, 50);
            this.txtOrganisationName.Name = "txtOrganisationName";
            this.txtOrganisationName.Size = new System.Drawing.Size(256, 20);
            this.txtOrganisationName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Organisation Legal Name";
            // 
            // txtOrganisationLegalName
            // 
            this.txtOrganisationLegalName.Location = new System.Drawing.Point(268, 79);
            this.txtOrganisationLegalName.Name = "txtOrganisationLegalName";
            this.txtOrganisationLegalName.Size = new System.Drawing.Size(256, 20);
            this.txtOrganisationLegalName.TabIndex = 13;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(268, 154);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(256, 20);
            this.txtAddress.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Address:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(268, 180);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(256, 20);
            this.txtEmail.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Email:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Organisation Type:";
            // 
            // cboOrganisationType
            // 
            this.cboOrganisationType.AutoCompleteCustomSource.AddRange(new string[] {
            "A",
            "B",
            "T"});
            this.cboOrganisationType.FormattingEnabled = true;
            this.cboOrganisationType.Location = new System.Drawing.Point(268, 208);
            this.cboOrganisationType.Name = "cboOrganisationType";
            this.cboOrganisationType.Size = new System.Drawing.Size(256, 21);
            this.cboOrganisationType.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Descriptrion:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(268, 235);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(256, 20);
            this.txtDescription.TabIndex = 20;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(367, 551);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 24);
            this.cmdCancel.TabIndex = 22;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // cmdSave
            // 
            this.cmdSave.Location = new System.Drawing.Point(448, 551);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(75, 24);
            this.cmdSave.TabIndex = 23;
            this.cmdSave.Text = "Save";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // txtShortCode
            // 
            this.txtShortCode.Location = new System.Drawing.Point(268, 105);
            this.txtShortCode.MaxLength = 3;
            this.txtShortCode.Name = "txtShortCode";
            this.txtShortCode.Size = new System.Drawing.Size(100, 20);
            this.txtShortCode.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Short Code";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkCreateUsers);
            this.panel1.Controls.Add(this.ArgusCrmUsernameTB);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtTelephone);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtTitle);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtUsername);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Location = new System.Drawing.Point(16, 280);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(507, 255);
            this.panel1.TabIndex = 26;
            // 
            // ArgusCrmUsernameTB
            // 
            this.ArgusCrmUsernameTB.Location = new System.Drawing.Point(243, 71);
            this.ArgusCrmUsernameTB.Name = "ArgusCrmUsernameTB";
            this.ArgusCrmUsernameTB.Size = new System.Drawing.Size(222, 20);
            this.ArgusCrmUsernameTB.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(64, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 13);
            this.label10.TabIndex = 43;
            this.label10.Text = "Argus Crm username";
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(241, 216);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(224, 20);
            this.txtTelephone.TabIndex = 37;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(66, 223);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 40;
            this.label12.Text = "Telephone:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(243, 102);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(224, 20);
            this.txtPassword.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(64, 105);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "Password:";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(243, 133);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(130, 20);
            this.txtTitle.TabIndex = 33;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(66, 133);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 38;
            this.label14.Text = "Title:";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(243, 39);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(224, 20);
            this.txtUsername.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(64, 46);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "Username:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(243, 190);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(224, 20);
            this.textBox1.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(66, 197);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Email:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(243, 164);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(224, 20);
            this.txtName.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(66, 171);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "Name:";
            // 
            // chkCreateUsers
            // 
            this.chkCreateUsers.AutoSize = true;
            this.chkCreateUsers.Location = new System.Drawing.Point(11, 14);
            this.chkCreateUsers.Name = "chkCreateUsers";
            this.chkCreateUsers.Size = new System.Drawing.Size(308, 17);
            this.chkCreateUsers.TabIndex = 44;
            this.chkCreateUsers.Text = "Create User for each Organisation (Trader / Broker / Admin)";
            this.chkCreateUsers.UseVisualStyleBackColor = true;
            // 
            // CreateMultipleOrganisations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 587);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtShortCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.cboOrganisationType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtOrganisationLegalName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtOrganisationName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNumberOrganisationsToCreate);
            this.Name = "CreateMultipleOrganisations";
            this.Text = "CreateMultipleOrganisations";
            this.Load += new System.EventHandler(this.CreateMultipleOrganisations_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumberOrganisationsToCreate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOrganisationName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOrganisationLegalName;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboOrganisationType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.TextBox txtShortCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkCreateUsers;
        private System.Windows.Forms.TextBox ArgusCrmUsernameTB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label17;
    }
}