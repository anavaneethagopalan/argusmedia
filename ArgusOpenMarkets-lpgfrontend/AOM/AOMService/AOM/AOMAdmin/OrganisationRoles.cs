﻿using Argus.Transport.Infrastructure;
using System;
using System.Linq;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;

namespace AOMAdmin
{
    public partial class OrganisationRoles : Form
    {
        private readonly ICrmAdministrationService _crmAdministrationService;
        private readonly IOrganisationService _organisationService;
        private long? _orgId;
        private OrganisationComboBox _currentOrganisation;
        private IBus _bus;

        public OrganisationRoles(ICrmAdministrationService crmAdministrationService, IBus bus,
            IOrganisationService organisationService, long? organisationId = null)
        {
            _crmAdministrationService = crmAdministrationService;
            _bus = bus;
            _orgId = organisationId;
            _organisationService = organisationService;
            InitializeComponent();
        }

        private void ManageOrganisationRoles_Load(object sender, EventArgs e)
        {
            DisplayOrganisations();
            EnableCreateOrganisationRole();

            if (_orgId != null)
            {
                cboOrganisations.SelectedItem = _currentOrganisation;
                cboOrganisations.Enabled = false;
                Text = _currentOrganisation.Description + ": Organisation Roles";
            }
        }

        private void EnableCreateOrganisationRole()
        {
            bool enabled = !string.IsNullOrEmpty(txtRoleDescription.Text);

            if (string.IsNullOrEmpty(txtRoleName.Text))
            {
                enabled = false;
            }

            var selectedOrg = cboOrganisations.SelectedItem as OrganisationComboBox;
            if (selectedOrg == null)
            {
                enabled = false;
            }

            cmdCreateOrganisationRole.Enabled = enabled;
        }

        private void DisplayOrganisations()
        {
            var organisations = _organisationService.GetOrganisations();

            cboOrganisations.Items.Clear();
            foreach (var org in organisations)
            {
                var co = new OrganisationComboBox(org.Id, org.Name, org.OrganisationType);
                cboOrganisations.Items.Add(co);
                if (_orgId == org.Id)
                {
                    _currentOrganisation = co;
                }
            }
        }

        private void cboOrganisations_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayOrganisationRoles();
            EnableCreateOrganisationRole();
            _currentOrganisation = cboOrganisations.SelectedItem as OrganisationComboBox;
        }

        private void DisplayOrganisationRoles()
        {
            var selectedOrg = cboOrganisations.SelectedItem as OrganisationComboBox;
            if (selectedOrg != null)
            {
                var roles = _organisationService.GetOrganisationRoles(selectedOrg.Id).OrderBy(or => or.Name);

                lstOrganisationRoles.Items.Clear();
                foreach (var role in roles)
                {
                    lstOrganisationRoles.Items.Add(new ComboBoxItem(role.Id, role.Name), true);
                }
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmdCreateOrganisationRole_Click(object sender, EventArgs e)
        {
            var selectedOrg = cboOrganisations.SelectedItem as OrganisationComboBox;
            if (selectedOrg != null)
            {
                var roleName = txtRoleName.Text;
                var roleDescription = txtRoleDescription.Text;

                if (!DoesRoleExist(roleName))
                {
                    var role = new OrganisationRole
                    {
                        Description = roleDescription,
                        Name = roleName,
                        OrganisationId = selectedOrg.Id
                    };

                    _crmAdministrationService.SaveRole(role, AdminSettings.LoggedOnUser.Id);
                    DisplayOrganisationRoles();
                }
                else
                {
                    MessageBox.Show(@"Cannot delete a blank topic", "Duplicate Role", MessageBoxButtons.OK);
                    txtRoleName.Text = "";
                }
            }
        }


        private bool DoesRoleExist(string roleName)
        {
            var selectedOrg = cboOrganisations.SelectedItem as OrganisationComboBox;
            if (selectedOrg != null)
            {
                var roles = _organisationService.GetOrganisationRoles(selectedOrg.Id);
                foreach (var role in roles)
                {
                    if (role.Name.ToLower() == roleName.ToLower())
                    {
                        return true;
                    }
                }

            }

            return false;
        }

        private
            void txtRoleName_TextChanged(object sender, EventArgs e)
        {
            EnableCreateOrganisationRole();
        }

        private void txtRoleDescription_TextChanged(object sender, EventArgs e)
        {
            EnableCreateOrganisationRole();
        }

        private void lstOrganisationRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmdRolePrivs.Enabled = true;
        }

        private void lstOrganisationRoles_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //lblStatus.Text = "Status:";
            if (e.NewValue != e.CurrentValue)
            {
                if (e.NewValue == CheckState.Unchecked)
                {
                    var selectedRole = lstOrganisationRoles.SelectedItem as ComboBoxItem;

                    if (selectedRole.Id > 0)
                    {
                        var selectedOrg = cboOrganisations.SelectedItem as OrganisationComboBox;
                        if (selectedOrg != null)
                        {
                            if (
                                MessageBox.Show(
                                    String.Format("Please confirm deletion of the \"{0}\" role",
                                        selectedRole.Description), "CONFIRMATION REQUIRED",
                                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                                    MessageBoxDefaultButton.Button2) == DialogResult.OK)
                            {
                                var role = new OrganisationRole {Id = selectedRole.Id, OrganisationId = selectedOrg.Id};
                                var result = _crmAdministrationService.DeleteOrganisationRole(role,
                                    AdminSettings.LoggedOnUser.Id);

                                if (!result)
                                {
                                    MessageBox.Show(
                                        String.Format(
                                            "Unable to delete the \"{0}\" role.\n\nThere are probably users assigned this role.",
                                            selectedRole.Description),
                                        "ERROR",
                                        MessageBoxButtons.OK, MessageBoxIcon.Hand,
                                        MessageBoxDefaultButton.Button1);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void cmdRolePrivs_Click(object sender, EventArgs e)
        {
            var cbi = lstOrganisationRoles.SelectedItem as ComboBoxItem;
            long roleId = cbi.Id;
            var frm = new OrganisationRolePrivileges(_crmAdministrationService, _organisationService, _bus, roleId,
                _currentOrganisation.Id)
            {
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }
    }
}