using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public class ProductMetadataConsumerClient : IProductMetadataConsumerClient
    {
        private readonly long _adminId = AdminSettings.LoggedOnUser.Id;
        private readonly IBus _bus;
        private readonly IDisposable _metadataResponceSubscription;

        public ProductMetadataConsumerClient(IBus bus)
        {
            _bus = bus;
            _metadataResponceSubscription = _bus.Subscribe<GetProductMetaDataResponse>(string.Empty, ProductMetadataResponseHandler);
        }

        public event Action<GetProductMetaDataResponse> OnProductMetadataResponse;

        public void PublishProductMetadataRequest(long productId)
        {
            _bus.Publish(new GetProductMetaDataRequest
            {
                ProductId = productId, 
                MessageAction = MessageAction.Refresh, 
                ClientSessionInfo = new ClientSessionInfo { UserId = _adminId }
            });
        }

        public OperationResultResponse AddMetadataItem(ProductMetaDataItem productMetadataItem)
        {
            return ExecuteMetadataItemRequest(productMetadataItem, MessageAction.Create);
        }

        public OperationResultResponse UpdateMetadataItem(ProductMetaDataItem productMetadataItem)
        {
            return ExecuteMetadataItemRequest(productMetadataItem, MessageAction.Update);
        }

        public OperationResultResponse DeleteMetadataItem(ProductMetaDataItem productMetadataItem)
        {
            return ExecuteMetadataItemRequest(productMetadataItem, MessageAction.Delete);
        }

        public OperationResultResponse UpdateMetadataItemsDisplayOrder(long productId, long[] metadataItemIds)
        {
            var request = new UpdateProductMetadataItemsDisplayOrderRequest
            {
                ProductId = productId,
                MetadataItemIds = metadataItemIds,
                ClientSessionInfo = new ClientSessionInfo { UserId = _adminId }
            };

            return _bus.Request<UpdateProductMetadataItemsDisplayOrderRequest, OperationResultResponse>(request);
        }

        public void RefreshDiffusionTopics(long productId)
        {
            var request = new GetProductConfigRequest
            {
                ProductId = productId
            };

            _bus.Publish(request);
        }

        public IEnumerable<MetaDataList> GetProductMetadataLists()
        {
            var response = _bus.Request<GetMetadataListsRequest, GetMetadataListsResponse>(new GetMetadataListsRequest());
            if (response != null)
                return response.MetaDataLists;

            return null;
        }

        public OperationResultResponse UpdateProductMetadataLists(List<MetaDataList> metadataLists)
        {
            var request = new UpdateMetadataListsRequest
            {
                MetaDataLists = metadataLists,
                MessageAction = MessageAction.Update,
                ClientSessionInfo = new ClientSessionInfo {UserId = _adminId}
            };

            return _bus.Request<UpdateMetadataListsRequest, OperationResultResponse>(request);
        }

        private OperationResultResponse ExecuteMetadataItemRequest(ProductMetaDataItem productMetadataItem, MessageAction action)
        {
            var request = new ProductMetadataItemRequest(productMetadataItem)
            {
                MessageAction = action,
                ClientSessionInfo = new ClientSessionInfo { UserId = _adminId }
            };

            return _bus.Request<ProductMetadataItemRequest, OperationResultResponse>(request);
        }

        private void ProductMetadataResponseHandler(GetProductMetaDataResponse response)
        {
            if (OnProductMetadataResponse != null)
                OnProductMetadataResponse(response);
        }

        public void Dispose()
        {
            if (_metadataResponceSubscription != null)
                _metadataResponceSubscription.Dispose();
        }
    }
}