﻿namespace AOMAdmin
{
    partial class TreeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Organisations");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Users");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Products");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("AOM", new System.Windows.Forms.TreeNode[] {
            treeNode9,
            treeNode10,
            treeNode11});
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserFilter = new System.Windows.Forms.TextBox();
            this.txtOrganisationFilter = new System.Windows.Forms.TextBox();
            this.txtProductFilter = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.FullRowSelect = true;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            treeNode9.Name = "Organisations";
            treeNode9.Text = "Organisations";
            treeNode10.Name = "Users";
            treeNode10.Text = "Users";
            treeNode11.Name = "Products";
            treeNode11.Text = "Products";
            treeNode12.Name = "AOM";
            treeNode12.Text = "AOM";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode12});
            this.treeView1.Size = new System.Drawing.Size(775, 458);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtProductFilter);
            this.panel1.Controls.Add(this.txtOrganisationFilter);
            this.panel1.Controls.Add(this.txtUserFilter);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 358);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(775, 100);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Filter Username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Filter Organisation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Filter Product:";
            // 
            // txtUserFilter
            // 
            this.txtUserFilter.Location = new System.Drawing.Point(141, 8);
            this.txtUserFilter.Name = "txtUserFilter";
            this.txtUserFilter.Size = new System.Drawing.Size(100, 22);
            this.txtUserFilter.TabIndex = 3;
            this.txtUserFilter.TextChanged += new System.EventHandler(this.txtUserFilter_TextChanged);
            // 
            // txtOrganisationFilter
            // 
            this.txtOrganisationFilter.Location = new System.Drawing.Point(141, 36);
            this.txtOrganisationFilter.Name = "txtOrganisationFilter";
            this.txtOrganisationFilter.Size = new System.Drawing.Size(100, 22);
            this.txtOrganisationFilter.TabIndex = 4;
            // 
            // txtProductFilter
            // 
            this.txtProductFilter.Location = new System.Drawing.Point(141, 66);
            this.txtProductFilter.Name = "txtProductFilter";
            this.txtProductFilter.Size = new System.Drawing.Size(100, 22);
            this.txtProductFilter.TabIndex = 5;
            this.txtProductFilter.TextChanged += new System.EventHandler(this.txtProductFilter_TextChanged);
            // 
            // TreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 458);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.treeView1);
            this.Name = "TreeView";
            this.Text = "TreeView";
            this.Load += new System.EventHandler(this.TreeView_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtProductFilter;
        private System.Windows.Forms.TextBox txtOrganisationFilter;
        private System.Windows.Forms.TextBox txtUserFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}