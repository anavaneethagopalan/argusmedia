using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOMAdmin
{
    public struct OrganisationProductAndPrivileges
    {
        public Product Product;
        public List<SubscribedProductPrivilege> ProductPrivileges;
    }

    public class OrganisationAdminEntities
    {
        public Organisation Organisation { get; set; }
        public List<OrganisationProductAndPrivileges> ProductsAndPrivileges { get; set; }
    }
}