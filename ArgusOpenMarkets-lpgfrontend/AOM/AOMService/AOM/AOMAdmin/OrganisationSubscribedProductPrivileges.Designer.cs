﻿namespace AOMAdmin
{
    partial class OrganisationSubscribedProductPrivileges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrganisationSubscribedProductPrivileges));
            this.lstOrganisationSubscribedPrivileges = new System.Windows.Forms.CheckedListBox();
            this.cboOrganisations = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboProducts = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstOrganisationSubscribedPrivileges
            // 
            this.lstOrganisationSubscribedPrivileges.FormattingEnabled = true;
            this.lstOrganisationSubscribedPrivileges.Location = new System.Drawing.Point(16, 91);
            this.lstOrganisationSubscribedPrivileges.Name = "lstOrganisationSubscribedPrivileges";
            this.lstOrganisationSubscribedPrivileges.Size = new System.Drawing.Size(478, 259);
            this.lstOrganisationSubscribedPrivileges.TabIndex = 2;
            this.lstOrganisationSubscribedPrivileges.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstOrganisationsSubscribedProductPriveleges_ItemCheck);
            this.lstOrganisationSubscribedPrivileges.SelectedIndexChanged += new System.EventHandler(this.lstOrganisationSubscribedPrivileges_SelectedIndexChanged);
            // 
            // cboOrganisations
            // 
            this.cboOrganisations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboOrganisations.FormattingEnabled = true;
            this.cboOrganisations.Location = new System.Drawing.Point(85, 10);
            this.cboOrganisations.Name = "cboOrganisations";
            this.cboOrganisations.Size = new System.Drawing.Size(224, 21);
            this.cboOrganisations.TabIndex = 0;
            this.cboOrganisations.SelectedIndexChanged += new System.EventHandler(this.cboOrganisations_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Organisation";
            // 
            // cboProducts
            // 
            this.cboProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboProducts.FormattingEnabled = true;
            this.cboProducts.Location = new System.Drawing.Point(85, 37);
            this.cboProducts.Name = "cboProducts";
            this.cboProducts.Size = new System.Drawing.Size(224, 21);
            this.cboProducts.TabIndex = 1;
            this.cboProducts.SelectedIndexChanged += new System.EventHandler(this.cboProducts_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Product";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(13, 68);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(37, 13);
            this.lblInfo.TabIndex = 6;
            this.lblInfo.Text = "<Info>";
            // 
            // cmdCancel
            // 
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(400, 358);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // OrganisationSubscribedProductPrivileges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 389);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboProducts);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboOrganisations);
            this.Controls.Add(this.lstOrganisationSubscribedPrivileges);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrganisationSubscribedProductPrivileges";
            this.Text = "Set Organisation Subscribed Product Privileges";
            this.Load += new System.EventHandler(this.SetOrganisationSubscribedProductPrivileges_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox lstOrganisationSubscribedPrivileges;
        private System.Windows.Forms.ComboBox cboOrganisations;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboProducts;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Button cmdCancel;
    }
}