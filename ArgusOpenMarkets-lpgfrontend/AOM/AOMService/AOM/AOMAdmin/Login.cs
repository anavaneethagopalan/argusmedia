﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Web.Script.Serialization;
using System.Windows.Forms;

using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.CrmService;
using AOM.Services.ProductService;
using AOM.Transport.Events.Users;

using AOM.Services.EncryptionService;
using ConnectionConfiguration = Argus.Transport.Configuration.ConnectionConfiguration;
using Argus.Transport.Infrastructure;
using MySql.Data.Entity;
using MySql.Data.MySqlClient;

namespace AOMAdmin
{
    public partial class Login : Form
    {
        private IEncryptionService _encryptionService;
        private IUserService _userService;
        private IBus _bus;

        public Login()
        {
            InitializeComponent();
            Initialize();
            FormUtils.AppendVersionToTitle(this);
        }

        #region Event Handlers

        private void radioButtonEnv_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxEnv.Enabled = radioButtonEnv.Checked;
            textBoxMQ.ReadOnly = radioButtonEnv.Checked;
            textBoxCrm.ReadOnly = radioButtonEnv.Checked;
        }

        private void comboBoxEnv_SelectedIndexChanged(object sender, EventArgs e)
        {
            var envConfig = comboBoxEnv.SelectedItem as EnvConfig;
            textBoxMQ.Text = envConfig == null ? string.Empty : envConfig.RabbitMq;
            textBoxCrm.Text = envConfig == null ? string.Empty : envConfig.Crm;
        }

        private void buttonTestQ_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionString(textBoxMQ.Text.Trim());
            using (var bus = Argus.Transport.Transport.CreateBus(aomConnectionConfiguration))
            {
                try
                {
                    bus.Subscribe<string>(string.Empty, data => { });
                    Cursor = Cursors.Default;
                    MessageBoxService.ShowInfo(this, "RabbitMQ Connected Successfully!", "Login");
                }
                catch
                {
                    Cursor = Cursors.Default;
                    MessageBoxService.ShowError(
                        this, 
                        "Faild to connect RabbitMQ!\r\n\r\nPlease check connection string and Network connection!",
                        "Login");
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
        }

        private void buttonTestCrm_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            var factory = new MySqlConnectionFactory();
            using (var conn = factory.CreateConnection(textBoxCrm.Text.Trim()))
            {
                try
                {
                    conn.Open();
                    Cursor = Cursors.Default;
                    MessageBoxService.ShowInfo(this, "CRM DB Connected Successfully!", "Login");
                }
                catch
                {
                    Cursor = Cursors.Default;
                    MessageBoxService.ShowError(
                        this,
                        "Faild to connect CRM DB!\r\n\r\nPlease check connection string and Network connection!",
                        "Login");
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStringsSection = (ConnectionStringsSection) config.GetSection("connectionStrings");
            connectionStringsSection.ConnectionStrings["aom.transport.bus"].ConnectionString = textBoxMQ.Text.Trim();
            connectionStringsSection.ConnectionStrings["CrmModel"].ConnectionString = textBoxCrm.Text.Trim();
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        private void buttonConfig_Click(object sender, EventArgs e)
        {
            Height = buttonConfig.ImageIndex == 0 ? 359 : 164;
            buttonConfig.ImageIndex = buttonConfig.ImageIndex == 0 ? 1 : 0;
        }

        private void cmdLogin_Click(object sender, EventArgs e)
        {
            var factory = new DbContextFactory();
            var productService = new ProductService(factory, new DateTimeProvider());

            cmdLogin.Enabled = false;
            try
            {
                Cursor = Cursors.WaitCursor;
                status.Text = string.Empty;
                _userService = new UserService(new DbContextFactory(), new DateTimeProvider(), productService);
                _encryptionService = new EncryptionService();
                var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
                _bus = Argus.Transport.Transport.CreateBus(aomConnectionConfiguration);

                var username = txtUserName.Text;
                var password = txtPassword.Text;
                var result = ValidateUser(username, password);

                if (result)
                {
                    AdminSettings.LoggedOn = true;
                    AdminSettings.LoggedOnUser = _userService.GetUserWithPrivileges(username);
                    Close();
                }
                else
                {
                    status.Text = "Incorrect username or password";
                }
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                ExceptionReporter.ShowException(ex, "Failed to log in");
            }
            finally
            {
                Cursor = Cursors.Default;
                cmdLogin.Enabled = true;
            }
        }

        #endregion

        private bool ValidateUser(string username, string password)
        {
            var user = new User {Id = _userService.GetUserId(username)};
            //password = _encryptionService.Hash(user, password);
            //var authResult = _bus.Request<UserAuthenticationRequestRpc, AuthenticationResult>(new UserAuthenticationRequestRpc {IpAddress = GetIp(), Password = password, Username = username});
            
            string url = ConfigurationManager.AppSettings["WebServiceLoginUrl"];
            UserAuthenticationRequestRpc userData = new UserAuthenticationRequestRpc {IpAddress = GetIp(), Password = password, Username = username, LoginSource = "aom"};

            LoginWebService loginWebService = new LoginWebService();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            string loginData = jss.Serialize((UserAuthenticationRequestRpc)userData);
            loginWebService.Authorise(url, loginData);

            if (loginWebService.IsAuthenticated)
            {
                var getUserSysPrivsRequest = new SystemPrivilegeAuthenticationRequestRpc
                {
                    UserId = loginWebService.UserId,
                    PrivilegeName = "AOMAdminTool"
                };
                if (_bus.Request<SystemPrivilegeAuthenticationRequestRpc, AuthenticationResult>(getUserSysPrivsRequest).IsAuthenticated)
                {
                    return true;
                }
            }
            return false;
        }

        private string GetIp()
        {
            string localIp = "?";
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIp = ip.ToString();
                }
            }
            return localIp;
        }

        private void Initialize()
        {
            Height = 164;
            textBoxMQ.Text = ConfigurationManager.ConnectionStrings["aom.transport.bus"].ConnectionString;
            textBoxCrm.Text = ConfigurationManager.ConnectionStrings["CrmModel"].ConnectionString;
            var envConfigSection = ConfigurationManager.GetSection("envConfigSection") as EnvConfigSection;
            if (envConfigSection != null && envConfigSection.Configs.Count > 0)
            {
                foreach (EnvConfig config in envConfigSection.Configs)
                {
                    comboBoxEnv.Items.Add(config);
                }
            }
        }
    }
}