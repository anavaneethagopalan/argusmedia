﻿using System;
using System.Windows.Forms;
using Argus.Transport.Configuration;
using ExceptionReporting.Core;
using Utils.Logging.Utils;

namespace AOMAdmin
{
    static class ExceptionReporter
    {
        public static void ShowException(Exception e, string message = "")
        {
            Log.Error(message, e);
            var reporter = new ExceptionReporting.ExceptionReporter();
            reporter.Config.AppName = "AOM Admin Tool";
            reporter.Config.CompanyName = "Argus Media";
            reporter.Config.EmailReportAddress = "aomsupport@argusmedia.com";
            reporter.Config.TitleText = "AOM Admin Tool Error Report";
            reporter.Config.ShowFlatButtons = true;   // this particular config is code-only
            reporter.Config.TakeScreenshot = true;
            reporter.Config.MailMethod = ExceptionReportInfo.EmailMethod.SimpleMAPI;
            reporter.Config.UserExplanation = String.Format("Using bus connection: {0}\nLogged in as: {1}\n", 
                ConnectionConfiguration.FromConnectionStringName("aom.transport.bus").Host,
                AdminSettings.LoggedOnUser);
            reporter.Config.ShowConfigTab = false;

            if (message != "")
            {
                reporter.Config.TitleText = String.Format("{0}: {1}", reporter.Config.TitleText, message);
                reporter.Config.CustomMessage = String.Format("{0}: {1}", message, e.Message);
            }
            
            reporter.Show(e);
        }
    }
}
