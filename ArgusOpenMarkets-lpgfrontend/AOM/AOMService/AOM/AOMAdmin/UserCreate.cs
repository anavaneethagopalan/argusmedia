﻿using AOM.App.Domain.Dates;
using System;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.CrmService;

namespace AOMAdmin
{
    public partial class UserCreate : Form
    {
        private readonly ICrmAdministrationService CrmAdministrationService;

        private readonly IEncryptionService _encrpytionService;

        private readonly ICredentialsService _credentialsService;
        private readonly Organisation _organisation;

        private readonly IOrganisationService _organisationService;
        private readonly IUserService _userService;
        private readonly IDateTimeProvider _dateTimeProvider;

        public UserCreate(
            ICrmAdministrationService crmAdministrationService,
            IEncryptionService encryptionService,
            ICredentialsService credentialsService, IOrganisationService organisationService, IUserService userService,
            IDateTimeProvider dateTimeProvider, Organisation organisation = null)
        {
            CrmAdministrationService = crmAdministrationService;
            _encrpytionService = encryptionService;
            _credentialsService = credentialsService;
            _organisationService = organisationService;
            _userService = userService;
            _dateTimeProvider = dateTimeProvider;
            _organisation = organisation;

            InitializeComponent();
        }

        private void CreateUser_Load(object sender, EventArgs e)
        {
            DisplayOrganisations();
            EnableSaveButton();
            chkIsActive.Checked = true;
        }

        private void DisplayOrganisations()
        {
            var organisations = _organisationService.GetOrganisations();

            foreach (var org in organisations)
            {
                cboOrganisations.Items.Add(new OrganisationComboBox(org.Id, org.Name, org.OrganisationType));
                if (_organisation != null && org.Id == _organisation.Id)
                {
                    Text = String.Format("Create user for {0}", org.Name);
                    cboOrganisations.SelectedIndex = cboOrganisations.Items.Count - 1;
                    cboOrganisations.Enabled = false;
                }
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            Validateform();

            var password = txtPassword.Text;

            var selectedOrg = cboOrganisations.SelectedItem as OrganisationComboBox;
            var tempExpiration = Convert.ToInt32(TempExpirHours.Value);
            var permExpiration = Convert.ToInt32(PermExpirMonths.Value);

            if (selectedOrg != null)
            {
                var user = new User
                {
                    Email = txtEmail.Text,
                    IsActive = chkIsActive.Checked,
                    Name = txtName.Text,
                    Username = txtUsername.Text,
                    ArgusCrmUsername = ArgusCrmUsernameTB.Text,
                    OrganisationId = selectedOrg.Id,
                    Title = txtTitle.Text,
                    Telephone = txtTelephone.Text,
                    IsBlocked = false,
                    IsDeleted = false,
                };

                try
                {
                    // Create User First
                    var result = CrmAdministrationService.CreateUserWithCredentials(user, null, AdminSettings.LoggedOnUser.Id);
                    if (result)
                    {
                        user.Id = _userService.GetUserId(user.Username);

                        var userCredentials = new UserCredentials
                        {
                            DateCreated = _dateTimeProvider.UtcNow,
                            PasswordHashed = _encrpytionService.Hash(user, password),
                            DefaultExpirationInMonths = permExpiration,
                            Expiration = TempPassCB.Checked ? _dateTimeProvider.UtcNow.AddHours(tempExpiration) : _dateTimeProvider.UtcNow.AddMonths(permExpiration),
                            //FirstTimeLogin = TempPassCB.Checked,
                            CredentialType = TempPassCB.Checked ? CredentialType.Temporary : CredentialType.Permanent,
                            User = user,
                            UserId = user.Id
                        };

                        _credentialsService.CreateUserCredentials(userCredentials, AdminSettings.LoggedOnUser.Id);
                        ShowMessageBox("User added successfully ", "User added");
                    }
                    else
                    {
                        ShowMessageBox("Error while adding user", "User added");
                    }
                }
                catch (Exception ex)
                {
                    ExceptionReporter.ShowException(ex, "Error while adding user");
                }

                Close();
            }
        }

        private void ShowMessageBox(string msg, string title)
        {
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void Validateform()
        {
            if (TempPassCB.Checked && TempExpirHours.Value <= 0)
            {
                ShowMessageBox("Must specify an expiration for the temporary password ", "No expiration!");
            }
            else if (PermExpirMonths.Value <= 0)
            {
                ShowMessageBox("Must specify default expiration for the permanent password ", "No expiration!");
            }
        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void cboOrganisations_TabIndexChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void EnableSaveButton()
        {
            bool enabled = !string.IsNullOrEmpty(txtUsername.Text);

            if (string.IsNullOrEmpty(txtName.Text))
            {
                enabled = false;
            }

            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                enabled = false;
            }

            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                enabled = false;
            }

            var selectedOrg = cboOrganisations.SelectedItem as OrganisationComboBox;
            if (selectedOrg == null)
            {
                enabled = false;
            }

            cmdSave.Enabled = enabled;
        }

        private void TempExpirHours_ValueChanged(object sender, EventArgs e)
        {

        }

    }
}