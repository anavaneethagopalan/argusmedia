﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOMAdmin
{
    public partial class ProductMetadataItemForm : Form
    {
        private readonly ProductMetaDataItem _productMetaDataItem;
        private readonly IProductMetadataConsumerClient _productMetadataConsumerClient;

        public ProductMetadataItemForm(ProductMetaDataItem productMetaDataItem, IProductMetadataConsumerClient productMetadataConsumerClient)
        {
            _productMetaDataItem = productMetaDataItem;
            _productMetadataConsumerClient = productMetadataConsumerClient;

            InitializeComponent();
            PopulateMetadataTypes();
            SetProductMetadataItem(productMetaDataItem);
        }

        public bool CreateNewProductMetadataItem { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            var metadataLists = _productMetadataConsumerClient.GetProductMetadataLists();
            PopulateMetadataLists(metadataLists);
        }

        private void PopulateMetadataTypes()
        {
            cmbFieldType.DataSource = new BindingSource(new Dictionary<MetaDataTypes, string>
            {
                {MetaDataTypes.String, "String"},
                {MetaDataTypes.IntegerEnum, "Dropdown"}
            }, null);

            cmbFieldType.DisplayMember = "Value";
            cmbFieldType.ValueMember = "Key";
        }

        private void SetProductMetadataItem(ProductMetaDataItem productMetaDataItem)
        {
            tbDisplayName.Text = productMetaDataItem.DisplayName;

            cmbFieldType.SelectedValue = productMetaDataItem.FieldType;


            switch (productMetaDataItem.FieldType)
            {
                case MetaDataTypes.IntegerEnum:
                    var enumMetadataItem = _productMetaDataItem as ProductMetaDataItemEnum;
                    if (enumMetadataItem != null)
                    {
                    }
                    break;
                case MetaDataTypes.String:
                    var stringMetadataItem = _productMetaDataItem as ProductMetaDataItemString;
                    if (stringMetadataItem != null)
                    {
                        nmValueMinimum.Value = stringMetadataItem.ValueMinimum;
                        nmValueMaximum.Value = stringMetadataItem.ValueMaximum;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void PopulateMetadataLists(IEnumerable<MetaDataList> metadataLists)
        {
            cmbMetadataList.DataSource = new BindingSource(metadataLists, null);
            cmbMetadataList.DisplayMember = "Description";
            cmbMetadataList.ValueMember = "Id";

            switch (_productMetaDataItem.FieldType)
            {
                case MetaDataTypes.IntegerEnum:
                    cmbMetadataList.SelectedValue = ((ProductMetaDataItemEnum)_productMetaDataItem).MetadataList.Id;
                    break;
                case MetaDataTypes.String:
                    cmbMetadataList.SelectedItem = metadataLists.FirstOrDefault();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void cmbFieldType_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbFieldType.SelectedItem == null)
                return;

            var fieldType = ((KeyValuePair<MetaDataTypes, string>) cmbFieldType.SelectedItem).Key;

            switch (fieldType)
            {
                case MetaDataTypes.IntegerEnum:
                    tbStringMetadataLayout.Visible = false;
                    tbEnumMetadataLayout.Visible = true;
                    break;
                case MetaDataTypes.String:
                    tbEnumMetadataLayout.Visible = false;
                    tbStringMetadataLayout.Visible = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var productMetadataItem = GetProductMetadataItem();
            if (productMetadataItem == null)
                return;

            var response = CreateNewProductMetadataItem
                ? _productMetadataConsumerClient.AddMetadataItem(productMetadataItem)
                : _productMetadataConsumerClient.UpdateMetadataItem(productMetadataItem);

            if (response.IsValid)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MessageBoxService.ShowWarning(this, response.ErrorMessage);
            }
        }

        private ProductMetaDataItem GetProductMetadataItem()
        {
            if (cmbFieldType.SelectedItem == null)
                return null;

            switch ((MetaDataTypes)cmbFieldType.SelectedValue)
            {
                case MetaDataTypes.IntegerEnum:
                    return new ProductMetaDataItemEnum
                    {
                        DisplayName = tbDisplayName.Text, 
                        ProductId = _productMetaDataItem.ProductId, 
                        Id = _productMetaDataItem.Id, 
                        MetadataList = cmbMetadataList.SelectedItem as MetaDataList
                    };
                case MetaDataTypes.String:
                    return new ProductMetaDataItemString
                    {
                        DisplayName = tbDisplayName.Text,
                        ValueMinimum = (int) nmValueMinimum.Value,
                        ValueMaximum = (int) nmValueMaximum.Value,
                        ProductId = _productMetaDataItem.ProductId,
                        Id = _productMetaDataItem.Id
                    };
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
