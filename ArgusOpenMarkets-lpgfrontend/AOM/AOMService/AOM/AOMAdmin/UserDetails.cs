﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using System;
using System.Windows.Forms;

namespace AOMAdmin
{
    public partial class UserDetails : Form
    {
        private readonly ICrmAdministrationService _crmAdministrationService;
        private readonly User _user;
        private readonly string _organisationName;

        public UserDetails(ICrmAdministrationService crmAdministrationService,
                           User user,
                           IOrganisationMinimumDetails organisation)
        {
            _crmAdministrationService = crmAdministrationService;
            _user = user;
            _organisationName = organisation != null ? organisation.Name : string.Empty;
            InitializeComponent();
            PopulateDialog();
        }

        private void PopulateDialog()
        {
            txtUsername.Text = _user.Username;
            txtOrganisation.Text = _organisationName;
            txtArgusCrmUsername.Text = _user.ArgusCrmUsername;
            txtTitle.Text = _user.Title;
            txtName.Text = _user.Name;
            txtEmail.Text = _user.Email;
            txtTelephone.Text = _user.Telephone;
            IsActiveCB.Checked = _user.IsActive;
            IsDeletedCB.Checked = _user.IsDeleted;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            var user = new User
            {
                Email = txtEmail.Text,
                Name = txtName.Text,
                ArgusCrmUsername = txtArgusCrmUsername.Text,
                Title = txtTitle.Text,
                Telephone = txtTelephone.Text,
                IsActive = IsActiveCB.Checked,
                IsDeleted = IsDeletedCB.Checked
            };

            var result = _crmAdministrationService.UpdateUserDetails(_user.Id,
                                                                     user,
                                                                     AdminSettings.LoggedOnUser.Id);
            if (result)
            {
                MessageBox.Show("User details updated successfully",
                                "User Details Update",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }           
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
