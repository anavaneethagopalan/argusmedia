﻿namespace AOMAdmin
{
    partial class PurgeNews
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblDaysToPurge = new System.Windows.Forms.Label();
            this.dtpPurgeDate = new System.Windows.Forms.DateTimePicker();
            this.btnPurge = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cmdReplayArgusNews = new System.Windows.Forms.Button();
            this.txtNumDaysToGoBack = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtContentStreams = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblDaysToPurge
            // 
            this.lblDaysToPurge.AutoSize = true;
            this.lblDaysToPurge.Location = new System.Drawing.Point(7, 10);
            this.lblDaysToPurge.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDaysToPurge.Name = "lblDaysToPurge";
            this.lblDaysToPurge.Size = new System.Drawing.Size(176, 17);
            this.lblDaysToPurge.TabIndex = 0;
            this.lblDaysToPurge.Text = "Refresh Date (Inclusive of)";
            this.toolTip1.SetToolTip(this.lblDaysToPurge, "Will purge all news for and before the specified date!");
            this.lblDaysToPurge.Click += new System.EventHandler(this.label1_Click);
            // 
            // dtpPurgeDate
            // 
            this.dtpPurgeDate.Location = new System.Drawing.Point(360, 124);
            this.dtpPurgeDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpPurgeDate.Name = "dtpPurgeDate";
            this.dtpPurgeDate.Size = new System.Drawing.Size(203, 22);
            this.dtpPurgeDate.TabIndex = 1;
            // 
            // btnPurge
            // 
            this.btnPurge.Location = new System.Drawing.Point(360, 163);
            this.btnPurge.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPurge.Name = "btnPurge";
            this.btnPurge.Size = new System.Drawing.Size(100, 30);
            this.btnPurge.TabIndex = 2;
            this.btnPurge.Text = "Purge News";
            this.btnPurge.UseVisualStyleBackColor = true;
            this.btnPurge.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(463, 165);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmdReplayArgusNews
            // 
            this.cmdReplayArgusNews.Location = new System.Drawing.Point(23, 163);
            this.cmdReplayArgusNews.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmdReplayArgusNews.Name = "cmdReplayArgusNews";
            this.cmdReplayArgusNews.Size = new System.Drawing.Size(208, 30);
            this.cmdReplayArgusNews.TabIndex = 4;
            this.cmdReplayArgusNews.Text = "Replay Argus News";
            this.cmdReplayArgusNews.UseVisualStyleBackColor = true;
            this.cmdReplayArgusNews.Click += new System.EventHandler(this.cmdReplayArgusNews_Click);
            // 
            // txtNumDaysToGoBack
            // 
            this.txtNumDaysToGoBack.Location = new System.Drawing.Point(151, 60);
            this.txtNumDaysToGoBack.Name = "txtNumDaysToGoBack";
            this.txtNumDaysToGoBack.Size = new System.Drawing.Size(80, 22);
            this.txtNumDaysToGoBack.TabIndex = 5;
            this.txtNumDaysToGoBack.Text = "30";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Num days go back";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Content Streams";
            // 
            // txtContentStreams
            // 
            this.txtContentStreams.Location = new System.Drawing.Point(151, 90);
            this.txtContentStreams.Name = "txtContentStreams";
            this.txtContentStreams.Size = new System.Drawing.Size(80, 22);
            this.txtContentStreams.TabIndex = 7;
            this.txtContentStreams.Text = "95087";
            // 
            // PurgeNews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 206);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtContentStreams);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNumDaysToGoBack);
            this.Controls.Add(this.cmdReplayArgusNews);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPurge);
            this.Controls.Add(this.dtpPurgeDate);
            this.Controls.Add(this.lblDaysToPurge);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PurgeNews";
            this.Text = "Refresh News";
            this.Load += new System.EventHandler(this.PurgeNews_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDaysToPurge;
        private System.Windows.Forms.DateTimePicker dtpPurgeDate;
        private System.Windows.Forms.Button btnPurge;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button cmdReplayArgusNews;
        private System.Windows.Forms.TextBox txtNumDaysToGoBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtContentStreams;
    }
}