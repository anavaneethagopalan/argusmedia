﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOMAdmin.CsvHelpers
{
    public static class PermissionsCsvHelper
    {
        public static List<MatrixPermission> CreateMatrixPermissions(CsvMatrixPermission csvMatrixPermission, long ourOrganisationId, long theirOrganisationId)
        {
            return new List<MatrixPermission>
            {
                new MatrixPermission
                {
                    OurOrganisation = ourOrganisationId,
                    BuyOrSell = csvMatrixPermission.BuyOrSell,
                    TheirOrganisation = theirOrganisationId,
                    AllowOrDeny = csvMatrixPermission.AllowOrDeny,
                    ProductId = csvMatrixPermission.ProductId,
                    LastUpdated = csvMatrixPermission.LastUpdated,
                    LastUpdatedUserId = AdminSettings.LoggedOnUser.Id
                }
            };
        }
    }
}
