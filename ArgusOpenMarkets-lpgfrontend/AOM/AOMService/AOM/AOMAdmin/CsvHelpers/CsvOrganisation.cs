﻿using System;

namespace AOMAdmin.CsvHelpers
{
    public class CsvOrganisation
    {
        public string ShortCode { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string OrganisationType { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public long NumberAttributes { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}
