﻿using System.Collections.Generic;

namespace AOMAdmin.CsvHelpers
{
    public class CsvUser
    {
        private Dictionary<long, string> _organisationRoles;

        public Dictionary<long, string> OrganisationRoles
        {
            get
            {
                if (_organisationRoles == null)
                {
                    _organisationRoles = new Dictionary<long, string>();
                }

                return _organisationRoles;
            }

            set
            {
                _organisationRoles = value;
            }
        }

        public string OrganisationShortCode { get; set; }
        public long OrganisationId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Telephone { get; set; }
        public string Username { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsBlocked { get; set; }
        public string CrmUsername { get; set; }
        public long NumberAttributes { get; set; }
        public string Password { get; set; }
        public bool? TemporaryPassword { get; set; }
        public int? PasswordExpireHours { get; set; }
    }
}