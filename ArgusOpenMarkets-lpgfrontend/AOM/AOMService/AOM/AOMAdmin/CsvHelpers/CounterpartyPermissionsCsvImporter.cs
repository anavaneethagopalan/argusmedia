﻿using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.PermissionService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.CounterpartyPermissions;
using AOM.Transport.Events.Organisations;

namespace AOMAdmin.CsvHelpers
{
    public class CounterpartyPermissionsCsvImporter : CommonCsvImporter, ICsvImporter
    {
        private IOrganisationService _organisationService;
        private IProductService _productService;
        private CounterpartyPermissionService _counterpartyPermissionService;
        private const int _columnCount = 5;

        public CounterpartyPermissionsCsvImporter(IOrganisationService organisationService,
            IProductService productService, CounterpartyPermissionService counterpartyPermissionService)
        {
            _organisationService = organisationService;
            _productService = productService;
            _counterpartyPermissionService = counterpartyPermissionService;
        }

        public ValidationResponse ValidateCsvLine(string line)
        {
            var validationResponse = new ValidationResponse { IsValid = true };
            var csvMatrixPermission = CreateCsvMatrixPermission(line);

            if (csvMatrixPermission.NumberAttributes != _columnCount)
            {                
                validationResponse.ErrorMessage = "CSV Line does not contain the correct number of item(s). Should be " + _columnCount;
                validationResponse.IsValid = false;
            }

            if (validationResponse.IsValid)
                validationResponse = ValidateShortcode(validationResponse, csvMatrixPermission.OurOrganisationCode, "OurOrganisationCode", _organisationService);
            if (validationResponse.IsValid)
                validationResponse = ValidateShortcode(validationResponse, csvMatrixPermission.TheirOrganisationCode, "TheirOrganisationCode", _organisationService);

            if (validationResponse.IsValid)
            {
                var product = _productService.GetProduct(csvMatrixPermission.ProductId);
                if (product == null)
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "ProductId";
                    validationResponse.AttributePosition = 2;
                    validationResponse.ErrorMessage = "Not a valid Product Id: " + csvMatrixPermission.ProductId;
                }
            }

            return validationResponse;
        }

        public Organisation GetOrganisation(string shortCode)
        {
            return _organisationService.GetOrganisationByShortCode(shortCode);
        }

        public string ImportCsvLine(string line)
        {
            CsvMatrixPermission csvMatrixPermission = CreateCsvMatrixPermission(line);
            Organisation ourOrganisation = GetOrganisation(csvMatrixPermission.OurOrganisationCode);
            Organisation theirOrganisation = GetOrganisation(csvMatrixPermission.TheirOrganisationCode);

            var counterpartyPermissions = _organisationService.GetCounterpartyPermissions(ourOrganisation.Id);
            var currentPermission =
                counterpartyPermissions
                    .FirstOrDefault(c => c.OrgId == theirOrganisation.Id && c.ProductId == csvMatrixPermission.ProductId);
            if (currentPermission != null)
            {
                if (csvMatrixPermission.BuyOrSell == BuyOrSell.Buy)
                {
                    csvMatrixPermission.LastUpdated = currentPermission.BuySide.OurLastUpdated;
                }
                if (csvMatrixPermission.BuyOrSell == BuyOrSell.Sell)
                {
                    csvMatrixPermission.LastUpdated = currentPermission.SellSide.OurLastUpdated;
                }
            }

            var request = new AomCounterpartyPermissionAuthenticationResponse
            {
                Message = new Message
                {
                    ClientSessionInfo =
                        new ClientSessionInfo
                        {
                            UserId =
                                AdminSettings
                                    .LoggedOnUser.Id
                        },
                    MessageAction = MessageAction.Update,
                },
                CounterpartyPermissions = PermissionsCsvHelper.CreateMatrixPermissions(csvMatrixPermission, ourOrganisation.Id, theirOrganisation.Id)
            };

            AomCounterpartyPermissionResponse reponse =
                _counterpartyPermissionService.UpdatePermissions(request, ourOrganisation.Id);

            if (reponse.UpdateSuccessful)
                return "Successfully created permission: " + csvMatrixPermission;

            return "Failed to create permission: " + csvMatrixPermission;
        }
    }
}
