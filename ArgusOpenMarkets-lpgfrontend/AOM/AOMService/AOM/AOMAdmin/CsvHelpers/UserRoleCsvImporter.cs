﻿using System;
using System.Linq;

using AOM.App.Domain.Services;

namespace AOMAdmin.CsvHelpers
{
    public class UserRoleCsvImporter : CommonCsvImporter
    {
        private ICrmAdministrationService _crmAdministrationService;
        private IOrganisationService _organisationService;
        private IUserService _userService;
        private int _columnCount = 3;  //Convert to config property?

        public UserRoleCsvImporter(ICrmAdministrationService crmAdministrationService, IOrganisationService organisationService, IUserService userService)
        {
            _crmAdministrationService = crmAdministrationService;
            _organisationService = organisationService;
            _userService = userService;
        }

        public ValidationResponse ValidateCsvLine(string csvLine)
        {
            var validationResponse = new ValidationResponse {IsValid = true};
            var csvUserRole = new CsvUserRole();
            long validUserId = -1;
            long validOrganisationId = -1;

            try
            {
                csvUserRole = GetCsvUserRole(csvLine);
            }
            catch (Exception ex)
            {
                validationResponse.IsValid = false;
                validationResponse.ErrorMessage = string.Format("Could not parse line: {0} ", ex.Message);
            }

            if (validationResponse.IsValid)
            {
                if (csvUserRole.NumberAttributes < _columnCount)
                {
                    // Should be 3 elements in the csv line - pipe separated.
                    validationResponse.ErrorMessage = "CSV Line does not contain the correct number of item(s). Should be 3";
                    validationResponse.IsValid = false;
                }
            }

            // UserId or UserName
            if (csvUserRole.UserId > 0)
            {
                if (validationResponse.IsValid)
                {
                    var userContactDetails = _userService.GetContactDetails(csvUserRole.UserId);
                    if (userContactDetails.Id == csvUserRole.UserId)
                    {
                        validUserId = csvUserRole.UserId;
                    }
                    else
                    {
                        validationResponse.IsValid = false;
                        validationResponse.AttributeName = "UserId";
                        validationResponse.AttributePosition = 0;
                        validationResponse.ErrorMessage = string.Format("UserId: {0} does not exist", csvUserRole.UserId);
                    }   
                }
            }
            else
            {
                if (validationResponse.IsValid)
                {
                    validationResponse = CheckAttributeNotNull(csvUserRole.Username, 5, "Username", "Username cannot be null");
                }
                if (validationResponse.IsValid)
                {
                    validationResponse = CheckAttributeLength(csvUserRole.Username, 5, "Username", 100, "Username too long, needs to be <= 100 chars");
                }
                if (validationResponse.IsValid)
                {
                    long userId = _userService.GetUserId(csvUserRole.Username);
                    if (userId != -1024 && userId == csvUserRole.UserId)
                    {
                        validUserId = csvUserRole.UserId;
                    }
                    else
                    {
                        validationResponse.IsValid = false;
                        validationResponse.AttributeName = "Username";
                        validationResponse.AttributePosition = 0;
                        validationResponse.ErrorMessage = string.Format("Username: {0} does not exist", csvUserRole.Username);
                    }
                }
            }
            
            // OrganisationId or ShortCode
            if (validationResponse.IsValid)
            {
                if (csvUserRole.OrganisationId > 0)
                {
                    var organisation = _organisationService.GetOrganisationById(csvUserRole.OrganisationId);
                    if (organisation.Id == csvUserRole.OrganisationId)
                    {
                        validOrganisationId = csvUserRole.OrganisationId;
                    }
                    else
                    {
                        validationResponse.IsValid = false;
                        validationResponse.AttributeName = "OrganisationId";
                        validationResponse.AttributePosition = 1;
                        validationResponse.ErrorMessage = string.Format("Could not find an organisation based upon Id: {0} ", csvUserRole.OrganisationId);
                    }
                }
                else
                {
                    long organisationId = GetOrganisationByShortCode(csvUserRole.OrganisationShortCode);
                    if (organisationId != -1 && organisationId == csvUserRole.OrganisationId)
                    {
                        validOrganisationId = csvUserRole.OrganisationId;
                    }
                    else
                    {
                        validationResponse.IsValid = false;
                        validationResponse.AttributeName = "OrganisationName";
                        validationResponse.AttributePosition = 1;
                        validationResponse.ErrorMessage = string.Format("Could not find an organisation based upon ShortCode: {0}", csvUserRole.OrganisationShortCode);
                    }
                }
            }

            if (validationResponse.IsValid)
            {
                string result = DoesUserIdOnOrganisationMatch(validUserId, validOrganisationId);
                if (result != null)
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "OrganisationId";
                    validationResponse.AttributePosition = 1;
                    validationResponse.ErrorMessage = result;
                }
            }

            //RoleName
            if (validationResponse.IsValid)
            {
                long roleId = DoesRoleNameExist(validOrganisationId, csvUserRole.RoleName);
                if (roleId == -1)
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "RoleName";
                    validationResponse.AttributePosition = 2;
                    validationResponse.ErrorMessage = string.Format("Could not find an existing role with name: {0}", csvUserRole.RoleName);
                }
            }

            return validationResponse;
        }

        private long GetOrganisationByShortCode(string shortCode)
        {
            var organisation = _organisationService.GetOrganisationByShortCode(shortCode);
            if (organisation != null)
                return organisation.Id;
            else
                return -1;
        }

        private string DoesUserIdOnOrganisationMatch(long userId, long organisationId)
        {
            //check if organisationId provided in file matches organisation already setup in user table
            var userContactDetails = _userService.GetContactDetails(userId);
            var existingOrganisation = _organisationService.GetOrganisationById(userContactDetails.OrganisationId);
            var providedOrganisation = _organisationService.GetOrganisationById(organisationId);

            if (existingOrganisation.Id == providedOrganisation.Id)
                return null;
            else
                return string.Format("Provided organisation '{0}' does not match existing user organisation '{1}' ", providedOrganisation.ShortCode, existingOrganisation.ShortCode);
        }

        private long DoesRoleNameExist(long organisationId, string roleName)
        {
            var organisationRoles = _organisationService.GetOrganisationRoles(organisationId);
            var role = organisationRoles.FirstOrDefault(r => r.Name == roleName);
            if (role != null)
               return role.Id;
            else
               return -1;
        }

        public CsvUserRole GetCsvUserRole(string userRoleCsvLine)
        {
            var csvUserRole = new CsvUserRole();

            var userRoleItems = userRoleCsvLine.Split(CsvSeparator);
            csvUserRole.NumberAttributes = userRoleItems.Length;

            var userItem = userRoleItems[0].Trim();
            int userId;
            int.TryParse(userItem, out userId);
            if (userId > 0)
            {
                csvUserRole.UserId = userId;
            }
            else
            {
                csvUserRole.Username = userItem;
                csvUserRole.UserId = _userService.GetUserId(userItem);
            }

            var organisationItem = userRoleItems[1].Trim();
            int orgId;
            int.TryParse(organisationItem, out orgId);
            if (orgId > 0)
            {
                csvUserRole.OrganisationId = orgId;
            }
            else
            {
                csvUserRole.OrganisationShortCode = organisationItem;
                csvUserRole.OrganisationId = GetOrganisationByShortCode(organisationItem);
            }

            csvUserRole.RoleName = userRoleItems[2].Trim();
            csvUserRole.RoleId = DoesRoleNameExist(csvUserRole.OrganisationId, csvUserRole.RoleName);

            return csvUserRole;
        }

    }
}