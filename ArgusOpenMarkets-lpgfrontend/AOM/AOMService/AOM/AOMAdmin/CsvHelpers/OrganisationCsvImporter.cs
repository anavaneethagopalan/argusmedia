﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOMAdmin.Helpers;

namespace AOMAdmin.CsvHelpers
{
    public class OrganisationCsvImporter : CommonCsvImporter, ICsvImporter
    {
        private IOrganisationService _organisationService;
        private int _columnCount = 8;  //Convert to config property
        private OrganisationHelper _organisationHelper;

        public OrganisationCsvImporter(IOrganisationService organisationService, OrganisationHelper organisationHelper)
        {
            _organisationService = organisationService;
            _organisationHelper = organisationHelper;
        }

        public ValidationResponse ValidateCsvLine(string csvLine)
        {
            var validationResponse = new ValidationResponse { IsValid = true };
            var csvOrganisation = GetCsvOrganisation(csvLine);

            if (csvOrganisation.NumberAttributes != _columnCount)
            {
                // Should be 8 elements in the csv line - pipe separated.
                validationResponse.ErrorMessage = "CSV Line does not contain the correct number of item(s). Should be " + _columnCount;
                validationResponse.IsValid = false;
            }

            // Organisation header 
            // Short Code | Name | Legal Name | Address | Email | Organisation Type | Description | IsDeleted
            
            //ShortCode
            if (validationResponse.IsValid)
            {
                //*** TO CHECK - cannot be null if not admin company
                validationResponse = CheckAttributeNotNull(csvOrganisation.ShortCode, 0, "ShortCode", "ShortCode cannot be null");
            }
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeLength(csvOrganisation.ShortCode, 0, "ShortCode", 5, "ShortCode too long, needs to be <= 5 chars");
            }
            if (validationResponse.IsValid)
            {
                // check the shortcode is unique?
                if (_organisationService.GetOrganisationByShortCode(csvOrganisation.ShortCode) != null)
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "ShortCode";
                    validationResponse.AttributePosition = 0;
                    validationResponse.ErrorMessage = string.Format("Organisation shortcode: {0} is not unique", csvOrganisation.ShortCode);
                }
            }
            
            //Name
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeNotNull(csvOrganisation.Name, 1, "Name", "Name cannot be null");
            }
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeLength(csvOrganisation.Name, 0, "Name", 100, "Name too long, needs to be <= 100 chars");
            }
          

            //Legal Name
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeNotNull(csvOrganisation.LegalName, 1, "LegalName", "LegalName cannot be null");
            }
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeLength(csvOrganisation.LegalName, 0, "LegalName", 100, "Legal Name too long, needs to be <= 200 chars");
            }
           
            //Email
            if (validationResponse.IsValid)
            {
                if (!new EmailAddressAttribute().IsValid(csvOrganisation.Email))
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "Email";
                    validationResponse.AttributePosition = 4;
                    validationResponse.ErrorMessage = "Not a valid email address";
                }
            }

            //OrganisationType
            if (validationResponse.IsValid)
            {
                try
                {
                    OrganisationType orgType = DtoMappingAttribute.FindEnumWithValue<OrganisationType>(csvOrganisation.OrganisationType);
                }
                catch 
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "OrganisationType";
                    validationResponse.AttributePosition = 5;
                    validationResponse.ErrorMessage = "Not a valid Organisation Type: " + csvOrganisation.OrganisationType;
                }
            }
            
            return validationResponse;
        }

              
        public CsvOrganisation GetCsvOrganisation(string organisationCsvLine)
        {
            var csvOrganisation = new CsvOrganisation();
            var organisationItems = organisationCsvLine.Split(CsvSeparator);
            
            csvOrganisation.NumberAttributes = organisationItems.Length;
            csvOrganisation.ShortCode = SafeString(organisationItems[0]);
            csvOrganisation.Name = SafeString(organisationItems[1]);
            csvOrganisation.LegalName = SafeString(organisationItems[2]);
            csvOrganisation.Address = SafeString(organisationItems[3]);
            csvOrganisation.Email = SafeString(organisationItems[4]);
            csvOrganisation.OrganisationType = SafeString(organisationItems[5]);
            csvOrganisation.Description = SafeString(organisationItems[6]);
            csvOrganisation.IsDeleted = GetBool(organisationItems[7]);
            csvOrganisation.DateCreated = DateTime.Now;
            
            return csvOrganisation;
        }

        public string ImportCsvLine(string line)
        {
            CsvOrganisation csvOrganisation = GetCsvOrganisation(line);
            Dictionary<Organisation, string> result = CreateOrganisation(csvOrganisation);
            return result.First().Value;
        }

        private Dictionary<Organisation, string> CreateOrganisation(CsvOrganisation csvOrganisation)
        {
            Organisation organisation = new Organisation
            {
                ShortCode = csvOrganisation.ShortCode,
                Name = csvOrganisation.Name,
                LegalName = csvOrganisation.LegalName,
                Address = csvOrganisation.Address,
                Email = csvOrganisation.Email,
                OrganisationType = DtoMappingAttribute.FindEnumWithValue<OrganisationType>(csvOrganisation.OrganisationType),
                Description = csvOrganisation.Description,
                IsDeleted = csvOrganisation.IsDeleted,
                DateCreated = csvOrganisation.DateCreated
            };

            return _organisationHelper.CreateOrganisation(organisation);
        }
    }
}
