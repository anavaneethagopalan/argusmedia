﻿using System.Collections.Generic;
using System.Linq;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.ProductService;
using AOMAdmin.Helpers;

namespace AOMAdmin.CsvHelpers
{
    public class OrganisationProductsCsvImporter : CommonCsvImporter, ICsvImporter
    {
        private IOrganisationService _organisationService;
        private IProductService _productService;
        private int _columnCount = 2;  //Convert to config property?
        private DefaultOrganisationRoleHelper _defaultOrganisationRoleHelper;
        private Dictionary<int, List<OrganisationDetails>> _cachedOrganisationProductDetails;

        public OrganisationProductsCsvImporter(IOrganisationService organisationService, IProductService productService, DefaultOrganisationRoleHelper defaultOrganisationRoleHelper)
        {
            _organisationService = organisationService;
            _productService = productService;
            _defaultOrganisationRoleHelper = defaultOrganisationRoleHelper;
            _cachedOrganisationProductDetails = new Dictionary<int, List<OrganisationDetails>>();
        }

        public Organisation GetOrganisation(string shortCode)
        {
            return _organisationService.GetOrganisationByShortCode(shortCode);
        }

        public ValidationResponse ValidateCsvLine(string csvLine)
        {
            var validationResponse = new ValidationResponse {IsValid = true};
            var csvOrganisationProducts = GetCsvOrganisationProducts(csvLine);

            if (csvOrganisationProducts.NumberAttributes != _columnCount)
            {
                // Should be 2 elements in the csv line - pipe separated.
                validationResponse.ErrorMessage = "CSV Line does not contain the correct number of item(s). Should be " + _columnCount;
                validationResponse.IsValid = false;
            }

            // Organisation Products header 
            // Short Code | ProductIds

            //ShortCode
            if (validationResponse.IsValid)
            {
                //*** TO CHECK - cannot be null if not admin company
                validationResponse = CheckAttributeNotNull(csvOrganisationProducts.ShortCode, 0, "ShortCode", "ShortCode cannot be null");
            }
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeLength(csvOrganisationProducts.ShortCode, 0, "ShortCode", 5, "ShortCode too long, needs to be <= 5 chars");
            }
            if (validationResponse.IsValid)
            {
                // check the shortcode exists?
                if (_organisationService.GetOrganisationByShortCode(csvOrganisationProducts.ShortCode) == null)
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "ShortCode";
                    validationResponse.AttributePosition = 0;
                    validationResponse.ErrorMessage = string.Format("Organisation shortcode: {0} does not exist", csvOrganisationProducts.ShortCode);
                }
            }

            List<int> productIds = csvOrganisationProducts.ProductIds.Split(',').Select(i => int.Parse(i)).ToList();

            //ProductIds
            if (validationResponse.IsValid)
            {
                try
                {
                    foreach (int id in productIds)
                    {
                        //check if valid product id
                        var product = _productService.GetProduct(id);
                        if (product == null)
                        {
                            validationResponse.IsValid = false;
                            validationResponse.AttributeName = "ProductId";
                            validationResponse.AttributePosition = 2;
                            validationResponse.ErrorMessage = "Not a valid Product Id: " + id;
                        }
                        else
                        {
                            //Check if org has product assigned already                            
                            var organisationDetails = new List<OrganisationDetails>();
                            if (_cachedOrganisationProductDetails.ContainsKey(id))
                            {
                                organisationDetails = _cachedOrganisationProductDetails[id];
                            }
                            else
                            {
                                organisationDetails =
                                    _organisationService.GetOrganisationDetails(new List<Product> { product }).ToList();
                                _cachedOrganisationProductDetails.Add(id, organisationDetails);
                            }
                            OrganisationDetails orgDetails = organisationDetails.First(org => org.Organisation.ShortCode == csvOrganisationProducts.ShortCode);

                            if (orgDetails != null && orgDetails.SubscribedProducts.Count != 0)
                            {
                                validationResponse.IsValid = false;
                                validationResponse.AttributeName = "ProductId";
                                validationResponse.AttributePosition = 2;
                                validationResponse.ErrorMessage = "Organisation already assigne Product Id: " + id;
                            }
                        }
                    }
                }
                catch
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "ProductId";
                    validationResponse.AttributePosition = 2;
                    validationResponse.ErrorMessage = "Cannot parse productIds: " + csvOrganisationProducts.ProductIds;
                }
            }

            return validationResponse;
        }

        public CsvOrganisationProducts GetCsvOrganisationProducts(string organisationProductCsvLine)
        {
            var csvOrganisationProducts = new CsvOrganisationProducts();
            var organisationProductItems = organisationProductCsvLine.Split(CsvSeparator);
            
            csvOrganisationProducts.ShortCode = SafeString(organisationProductItems[0]);
            csvOrganisationProducts.ProductIds = SafeString(organisationProductItems[1]);
            csvOrganisationProducts.NumberAttributes = organisationProductItems.Length;

            return csvOrganisationProducts;
        }

        public string ImportCsvLine(string line)
        {
            CsvOrganisationProducts csvOrganisationProducts = GetCsvOrganisationProducts(line);
            Organisation org = GetOrganisation(csvOrganisationProducts.ShortCode);
            return _defaultOrganisationRoleHelper.AddDefaultOrganisationRoles(org, csvOrganisationProducts.ProductIds.Split(',').Select(i => int.Parse(i)).ToList());
        }
    }
}
