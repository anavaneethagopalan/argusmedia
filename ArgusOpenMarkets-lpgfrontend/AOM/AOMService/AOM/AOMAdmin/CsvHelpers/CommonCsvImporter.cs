﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;

namespace AOMAdmin.CsvHelpers
{
    public abstract class CommonCsvImporter
    {
        protected const char CsvSeparator = '|';

        public ValidationResponse CheckAttributeNotNull(string elementBeingCheckedValue, int positionInArray, string attributeName, string errorMessage)
        {
            var validationResponse = new ValidationResponse { IsValid = true };

            if (string.IsNullOrEmpty(elementBeingCheckedValue))
            {
                validationResponse.IsValid = false;
                validationResponse.AttributePosition = positionInArray;
                validationResponse.AttributeName = attributeName;
                validationResponse.ErrorMessage = errorMessage;
            }

            return validationResponse;
        }

        public ValidationResponse CheckAttributeLength(string elementBeingCheckedValue, int positionInArray, string attributeName, int charLength, string errorMessage)
        {
            var validationResponse = new ValidationResponse { IsValid = true };

            if (elementBeingCheckedValue.Length > charLength)
            {
                validationResponse.IsValid = false;
                validationResponse.AttributePosition = positionInArray;
                validationResponse.AttributeName = attributeName;
                validationResponse.ErrorMessage = errorMessage;
            }

            return validationResponse;
        }

        public ValidationResponse ValidateShortcode(ValidationResponse validationResponse, string shortCode, string attributeName, IOrganisationService organisationService)
        {
            if (validationResponse.IsValid)
            {
                //*** TO CHECK - cannot be null if not admin company
                validationResponse = CheckAttributeNotNull(shortCode, 0, attributeName, string.Format("{0} cannot be null", attributeName));
            }
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeLength(shortCode, 0, attributeName, 5, string.Format("{0} too long, needs to be <= 5 chars", attributeName));
            }
            if (validationResponse.IsValid)
            {
                // check the shortcode exists?
                if (organisationService.GetOrganisationByShortCode(shortCode) == null)
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = attributeName;
                    validationResponse.AttributePosition = 0;
                    validationResponse.ErrorMessage = string.Format("{0} shortcode: {1} does not exist", attributeName, shortCode);
                }
            }
            return validationResponse;
        }

        public CsvMatrixPermission CreateCsvMatrixPermission(string matrixPermissionCsvLine)
        {
            var csvMatrixPermission = new CsvMatrixPermission();
            var matrixPermissionItems = matrixPermissionCsvLine.Split(CsvSeparator);

            csvMatrixPermission.ProductId = GetLongValue(matrixPermissionItems[0]);
            csvMatrixPermission.OurOrganisationCode = SafeString(matrixPermissionItems[1]);
            string buyOrSellAttribute = matrixPermissionItems[2];
            if (buyOrSellAttribute == "B")
                csvMatrixPermission.BuyOrSell = BuyOrSell.Buy;
            else if (buyOrSellAttribute == "S")
                csvMatrixPermission.BuyOrSell = BuyOrSell.Sell;
            else
                throw new ArgumentOutOfRangeException("Invalid argument for field BuyOrSell");
            csvMatrixPermission.TheirOrganisationCode = SafeString(matrixPermissionItems[3]);
            string allowOrDenyAttribute = matrixPermissionItems[4];
            if (allowOrDenyAttribute == "A")
                csvMatrixPermission.AllowOrDeny = Permission.Allow;
            else if (allowOrDenyAttribute == "D")
                csvMatrixPermission.AllowOrDeny = Permission.Deny;
            else
                throw new ArgumentOutOfRangeException("Invalid argument for field AllowOrDeny");
            csvMatrixPermission.NumberAttributes = matrixPermissionItems.Length;

            return csvMatrixPermission;
        }

        public string SafeString(string value)
        {
            value = value.Replace("\"", "");
            value = value.Replace("'", "");

            return value.Trim();
        }

        public int GetIntValue(string val)
        {
            int numVal;
            int.TryParse(val, out numVal);

            return numVal;
        }

        public long GetLongValue(string val)
        {
            long numVal;
            long.TryParse(val, out numVal);

            return numVal;
        }

        public bool GetBool(string boolString)
        {
            bool result;
            if (boolString == "1")
                boolString = "true";
            else if (boolString == "0")
                boolString = "false";
            
            if (bool.TryParse(boolString, out result))
                return result;
            else
                throw new Exception("Boolean string is not a boolean character: " + boolString);
            
        }
    }
}
