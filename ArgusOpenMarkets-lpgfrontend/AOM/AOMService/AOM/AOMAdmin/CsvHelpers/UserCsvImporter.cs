﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using System.ComponentModel.DataAnnotations;
using AOMAdmin.Helpers;

namespace AOMAdmin.CsvHelpers
{
    public class UserCsvImporter : CommonCsvImporter, ICsvImporter
    {
        private IOrganisationService _organisationService;
        private IUserService _userService;
        private int _columnCount = 10;  //Convert to config property?
        private UserHelper _userHelper;

        public UserCsvImporter(IOrganisationService organisationService, IUserService userService, UserHelper userHelper)
        {
            _organisationService = organisationService;
            _userService = userService;
            _userHelper = userHelper;
        }

        public ValidationResponse ValidateCsvLine(string csvLine)
        {
            var validationResponse = new ValidationResponse { IsValid = true };
            var csvUser = new CsvUser();
            try
            {
                csvUser = GetCsvUser(csvLine);
            }
            catch (Exception ex)
            {
                validationResponse.IsValid = false;
                validationResponse.ErrorMessage = string.Format("Could not parse line: {0} ", ex.Message);
            }

            if (validationResponse.IsValid)
            {
                if (csvUser.NumberAttributes < _columnCount)
                {
                    // Should be 10+ elements in the csv line - pipe separated.
                    validationResponse.ErrorMessage = "CSV Line does not contain the correct number of item(s). Should be 10-13";
                    validationResponse.IsValid = false;
                }
            }

            // Organisation ShortCode or ID
            if (validationResponse.IsValid)
            {
                if (csvUser.OrganisationId > 0)
                {
                    var ogranisation = _organisationService.GetOrganisationById(csvUser.OrganisationId);
                    if (ogranisation != null)
                    {
                        csvUser.OrganisationShortCode = ogranisation.Name;
                    }
                    else
                    {
                        validationResponse.IsValid = false;
                        validationResponse.AttributeName = "OrganisationId";
                        validationResponse.AttributePosition = 0;
                        validationResponse.ErrorMessage = string.Format("Could not find an organisation based upon Id: {0} ", csvUser.OrganisationId);
                    }
                }
                else
                {
                    long organisationId = GetOrganisationByShortCode(csvUser.OrganisationShortCode);

                    if (organisationId != -1)
                    {
                        csvUser.OrganisationId = organisationId;
                    }
                    else
                    {
                        validationResponse.IsValid = false;
                        validationResponse.AttributeName = "OrganisationName";
                        validationResponse.AttributePosition = 0;
                        validationResponse.ErrorMessage = string.Format("Could not find an organisation based upon Name: {0}", csvUser.OrganisationShortCode);
                    }
                }
            }

            //Email
            if (validationResponse.IsValid)
            {
                if (!new EmailAddressAttribute().IsValid(csvUser.Email))
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "Email";
                    validationResponse.AttributePosition = 1;
                    validationResponse.ErrorMessage = "Not a valid email address";
                }
            }

            //Name
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeNotNull(csvUser.Name, 2, "Name", "Name cannot be null");
            }
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeLength(csvUser.Name, 2, "Name", 100, "Name too long, needs to be <= 100 chars");
            }

            //UserName
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeNotNull(csvUser.Username, 5, "Username", "Username cannot be null");
            }
            if (validationResponse.IsValid)
            {
                validationResponse = CheckAttributeLength(csvUser.Username, 5, "Username", 100, "Username too long, needs to be <= 100 chars");
            }
            if (validationResponse.IsValid)
            {
                // Ok - let's check the username is unique?
                if (DoesUserExist(csvUser.Username))
                {
                    validationResponse.IsValid = false;
                    validationResponse.AttributeName = "Username";
                    validationResponse.ErrorMessage = string.Format("Username: {0} is not unique", csvUser.Username);
                }
            }

            return validationResponse;
        }

        private long GetOrganisationByShortCode(string shortCode)
        {
            var organisation = _organisationService.GetOrganisationByShortCode(shortCode);
            if (organisation != null)
                return organisation.Id;
            else
                return -1;
        }

        private string MakePassword()
        {
            Random random = new Random();
            int randomNumber = random.Next(1, 100);

            return "Password" + randomNumber;
        }

        private bool DoesUserExist(string username)
        {
            long userId = _userService.GetUserId(username);

            if (userId > 0)
                return true;
            else
                return false;
        }

        public CsvUser GetCsvUser(string userCsvLine)
        {
            var csvUser = new CsvUser();

            var userItems = userCsvLine.Split(CsvSeparator);
            csvUser.NumberAttributes = userItems.Length;

            var organisationItem = userItems[0];
            int orgId;
            int.TryParse(organisationItem, out orgId);
            if (orgId > 0)
            {
                csvUser.OrganisationId = orgId;
            }
            else
            {
                csvUser.OrganisationShortCode = organisationItem;
                csvUser.OrganisationId = GetOrganisationByShortCode(organisationItem);
            }

            csvUser.Email = SafeString(userItems[1]);
            csvUser.Name = SafeString(userItems[2]);
            csvUser.Title = SafeString(userItems[3]);
            csvUser.Telephone = SafeString(userItems[4]);
            csvUser.Username = SafeString(userItems[5]);
            csvUser.IsActive = GetBool(userItems[6]);
            csvUser.IsDeleted = GetBool(userItems[7]);
            csvUser.IsBlocked = GetBool(userItems[8]);
            csvUser.CrmUsername = SafeString(userItems[9]);

            if (userItems.Length >= 11)
            {
                csvUser.Password = userItems[10];
            }
            else
            {
                csvUser.Password = MakePassword();
            }

            if (userItems.Length >= 12)
            {
                csvUser.TemporaryPassword = GetBool(userItems[11]);
            }
            else
            {
                csvUser.TemporaryPassword = true;
            }

            if (userItems.Length >= 13)
            {
                csvUser.PasswordExpireHours = GetIntValue(userItems[12]);
            }
            else
            {
                csvUser.PasswordExpireHours = 48;
            }

            return csvUser;
        }

        public string ImportCsvLine(string line)
        {
            CsvUser csvUser = GetCsvUser(line);
            return CreateUser(csvUser);
        }

        private string CreateUser(CsvUser csvUser)
        {
            var user = new User
            {
                Email = csvUser.Email,
                IsActive = csvUser.IsActive,
                Name = csvUser.Name,
                Username = csvUser.Username,
                ArgusCrmUsername = csvUser.CrmUsername,
                OrganisationId = csvUser.OrganisationId,
                Title = csvUser.Title,
                Telephone = csvUser.Telephone,
                IsBlocked = csvUser.IsBlocked,
                IsDeleted = csvUser.IsDeleted
            };

            var temporaryPassword = csvUser.TemporaryPassword.HasValue ? csvUser.TemporaryPassword.Value : false;

            var userCredentials = new UserCredentials
            {
                DateCreated = DateTime.UtcNow,
                DefaultExpirationInMonths = 12,
                Expiration = DateTime.UtcNow.AddYears(10),
                FirstTimeLogin = temporaryPassword,
                User = user,
                PasswordHashed = csvUser.Password,
                CredentialType = temporaryPassword ? CredentialType.Temporary : CredentialType.Permanent

            };

            // TODO - call the User helper - to persist the user.
            return _userHelper.CreateUser(userCredentials, csvUser.Password);
        }
    }
}