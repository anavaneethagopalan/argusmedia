﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOM.App.Domain.Entities;

namespace AOMAdmin.CsvHelpers
{
    public class CsvMatrixPermission
    {
        public Permission AllowOrDeny { get; set; }
        public BuyOrSell BuyOrSell { get; set; }
        public string OurOrganisationCode { get; set; }
        public string TheirOrganisationCode { get; set; }
        public long ProductId { get; set; }
        public DateTime? LastUpdated { get; set; }
        public int NumberAttributes { get; set; }

        public override string ToString()
        {
            return String.Format("{0} {1} {2} {3} for product {4}",
                AllowOrDeny, OurOrganisationCode,
                BuyOrSell == BuyOrSell.Buy ? "buy from" : "sell to",
                TheirOrganisationCode, ProductId);
        }
    }
}

