﻿namespace AOMAdmin.CsvHelpers
{
    public interface ICsvImporter
    {
        ValidationResponse ValidateCsvLine(string line);
        string ImportCsvLine(string line);
    }
}
