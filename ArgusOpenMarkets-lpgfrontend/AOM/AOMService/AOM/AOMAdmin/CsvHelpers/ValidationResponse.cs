﻿namespace AOMAdmin.CsvHelpers
{
    public class ValidationResponse
    {
        public int AttributePosition { get; set; }

        public string AttributeName { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsValid { get; set; }
    }
}
