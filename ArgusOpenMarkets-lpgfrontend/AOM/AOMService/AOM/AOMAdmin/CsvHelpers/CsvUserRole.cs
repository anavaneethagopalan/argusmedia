﻿namespace AOMAdmin.CsvHelpers
{
    public class CsvUserRole
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public long OrganisationId { get; set; }
        public string OrganisationShortCode { get; set; }
        public string RoleName { get; set; }
        public long RoleId { get; set; }
        public long NumberAttributes { get; set; }
    }
}
