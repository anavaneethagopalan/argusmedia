﻿namespace AOMAdmin.CsvHelpers
{
    public class CsvOrganisationProducts
    {
        public string ShortCode { get; set; }
        public string ProductIds { get; set; }
        public long NumberAttributes { get; set; }
    }
}
