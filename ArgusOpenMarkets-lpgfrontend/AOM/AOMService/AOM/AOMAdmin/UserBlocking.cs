﻿using AOM.App.Domain.Entities;
using System;
using System.Windows.Forms;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOMAdmin
{
    public partial class UserBlocking : Form
    {
        private readonly IBus _bus;
        private ICrmAdministrationService _crmAdminService;
        private readonly User _user;
        private UserCredentials _userCredentials;
        
        private const string invalidInputMessage = "Invalid input";

        public UserBlocking(IBus bus, ICrmAdministrationService crmAdminService, User user, UserCredentials userCredential)
        {
            _bus = bus;
            _user = user;
            _userCredentials = userCredential;
            _crmAdminService = crmAdminService;
            InitializeComponent();
            labelResult.Visible = false;
            if (userCredential == null)
            {
                dtpPasswordExpiration.Enabled = false;
                btnUpdatePasswordExpiration.Enabled = false;
                dtpPasswordExpiration.CustomFormat = " ";
            }
            else
            {
                dtpPasswordExpiration.CustomFormat = "dd/MM/yyyy HH:mm:ss";
                dtpPasswordExpiration.Value = userCredential.Expiration;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnBlock_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtUserName.Text))
            {
                labelResult.Text = invalidInputMessage;
                return;
            }

            var usr = txtUserName.Text;

            var request = new BlockUserRequestRpc {UserName = usr, RequestorUserId = AdminSettings.LoggedOnUser.Id};

            var blockUsrResponse = _bus.Request<BlockUserRequestRpc, BlockUserResponse>(request);

            labelResult.Visible = true;
            labelResult.Text = blockUsrResponse.MessageBody;

            if (blockUsrResponse.Success && chkTerminateUserSession.Checked)
            {
                RaiseTerminateUserRequest(usr);
            }

            Log.Info(
                String.Format(
                    "Block user request  processed for user {0}, result: {1}",
                    usr,
                    blockUsrResponse.MessageBody));
        }

        private void btnUnblock_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtUserName.Text))
            {
                labelResult.Text = invalidInputMessage;
                return;
            }

            var usr = txtUserName.Text;
            var request = new UnblockUserRequestRpc {UserName = usr, RequestorUserId = AdminSettings.LoggedOnUser.Id};

            //<UnblockUserRequestRpc, UnblockUserResponseRpc>(request);
            var unblockUserRequest = _bus.RequestAsync<UnblockUserRequestRpc, UnblockUserResponseRpc>(request);

            labelResult.Text = unblockUserRequest.Result.MessageBody;

            labelResult.Visible = true;

            Log.Info(
                String.Format(
                    "Unblock user request  processed for user {0}, result: {1}",
                    usr,
                    unblockUserRequest.Result.MessageBody));
        }

        private void RaiseTerminateUserRequest(string usr)
        {
            var terminateUserRequest = new TerminateUserRequest {UserName = usr};
            _bus.Publish(terminateUserRequest);
        }

        private void terminateSession_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtUserName.Text))
            {
                labelResult.Text = invalidInputMessage;
                return;
            }
            RaiseTerminateUserRequest(txtUserName.Text);
        }

        private void BlockUserForm_Load(object sender, EventArgs e)
        {
            if (_user != null)
            {
                txtUserName.Text = _user.Username;
                txtUserName.Enabled = false;
                Text = _user.Name + " (" + _user.Username + "): Block User";
            }
        }

        private void btnUpdatePasswordExpiration_Click(object sender, EventArgs e)
        {
            bool success = _crmAdminService.UpdateUserCredentialExpiration(_user.Id, dtpPasswordExpiration.Value,
                AdminSettings.LoggedOnUser.Id);
            if (success)
                MessageBox.Show(@"Password expiration updated successfully.", @"Update Password Expiration", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            else
                MessageBox.Show(@"Failed to update password expiration.", @"Update Password Expiration", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
    }
}