﻿using AOM.Transport.Events.System;
using Argus.Transport.Infrastructure;
using System;
using System.Windows.Forms;

namespace AOMAdmin
{
    public partial class DeleteDiffusionTopic : Form
    {
        private readonly IBus _bus;

        public DeleteDiffusionTopic(IBus bus)
        {
            _bus = bus;
            InitializeComponent();
        }

        private void DeleteDiffusionTopic_Load(object sender, EventArgs e)
        {
        }

        private void cmdDeleteDiffusionTopic_Click(object sender, EventArgs e)
        {
            var topicToDelete = txtDiffusionTopicToDelete.Text;
            var deleteTopic = true;

            if (string.IsNullOrEmpty(topicToDelete))
            {
                MessageBox.Show(@"Cannot delete a blank topic", @"Delete Topic", MessageBoxButtons.OK);
                deleteTopic = false;
            }

            if (topicToDelete.ToLower() == "aom")
            {
                MessageBox.Show(@"Cannot delete a blank topic", @"Delete Topic",
                    MessageBoxButtons.OK);
                deleteTopic = false;
            }

            if (deleteTopic)
            {
                DialogResult diagResult;
                var expectedResult = AnswerQuestionToDeleteTopic(out diagResult);

                if (diagResult == expectedResult)
                {
                    int wildcardsSet = 0;
                    if (topicToDelete.Contains("{0}"))
                    {
                        wildcardsSet++;
                    }

                    if (topicToDelete.Contains("{1}"))
                    {
                        wildcardsSet++;
                    }

                    WildcardRange outerWildcardRange = null;
                    WildcardRange innerWildcardRange = null;
                    if (wildcardsSet > 0)
                    {
                        outerWildcardRange = ValidateWildcardRange(txtWildcard1From.Text, txtWildcard1To.Text);
                    }

                    if (wildcardsSet > 1)
                    {
                        innerWildcardRange = ValidateWildcardRange(txtWildcard2From.Text, txtWildcard2To.Text);
                    }

                    long outerCounter = 0;

                    switch (wildcardsSet)
                    {
                        case 0:
                            PublishDeleteTopicMessage(topicToDelete);
                            break;

                        case 1:

                            if (outerWildcardRange.IsValid)
                            {
                                for (outerCounter = outerWildcardRange.StartIndex;
                                    outerCounter < outerWildcardRange.EndIndex;
                                    outerCounter++)
                                {
                                    var ttd = string.Format(topicToDelete, outerCounter);
                                    PublishDeleteTopicMessage(ttd);
                                }

                            }
                            break;

                        case 2:

                            if (outerWildcardRange.IsValid && innerWildcardRange.IsValid)
                            {
                                for (outerCounter = outerWildcardRange.StartIndex;
                                    outerCounter <= outerWildcardRange.EndIndex;
                                    outerCounter++)
                                {

                                    for (var innerCounter = innerWildcardRange.StartIndex;
                                        innerCounter <= innerWildcardRange.EndIndex;
                                        innerCounter++)
                                    {
                                        var ttd = string.Format(topicToDelete, outerCounter, innerCounter);
                                        PublishDeleteTopicMessage(ttd);
                                    }
                                }
                            }

                            break;
                    }

                    Close();
                }
            }
        }

        private void PublishDeleteTopicMessage(string topicToDelete)
        {
            _bus.Publish<DeleteDiffusionTopicRequest>(new DeleteDiffusionTopicRequest
            {
                TopicToDelete = topicToDelete,
                LoggedOnUserId = AdminSettings.LoggedOnUser.Id
            });
        }

        private WildcardRange ValidateWildcardRange(string startRange, string endRange)
        {
            var wildCardRange = new WildcardRange {IsValid = true};

            if (string.IsNullOrEmpty(startRange))
            {
                wildCardRange.IsValid = false;
            }

            if (string.IsNullOrEmpty(endRange))
            {
                wildCardRange.IsValid = false;
            }

            int sr;
            Int32.TryParse(startRange, out sr);

            int er;
            Int32.TryParse(endRange, out er);

            if (sr < 0)
            {
                wildCardRange.IsValid = false;
            }

            if (er < 0)
            {
                wildCardRange.IsValid = false;
            }

            if (er < sr)
            {
                wildCardRange.IsValid = false;
            }

            wildCardRange.StartIndex = sr;
            wildCardRange.EndIndex = er;

            return wildCardRange;
        }

        private static DialogResult AnswerQuestionToDeleteTopic(out DialogResult diagResult)
        {
            Random rnd = new Random();
            int random1 = rnd.Next(1, 10);
            int random2 = rnd.Next(1, 10);
            int random3 = rnd.Next(1, 3);

            DialogResult expectedResult;
            int answer;
            if (random1 <= 5)
            {
                answer = random1 + random2;
                expectedResult = DialogResult.Yes;
            }
            else
            {
                answer = random1 + random2 + random3;
                expectedResult = DialogResult.No;
            }

            var message =
                string.Format(
                    "Are you sure you wish to delete this topic?\n  The Topic will NOT be created.\n  Does {0} + {1} equal {2}",
                    random1,
                    random2,
                    answer);

            diagResult = MessageBox.Show(
                message,
                @"Delete Topic?",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question);

            return expectedResult;
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

    public class WildcardRange
    {
        public bool IsValid { get; set; }
        public long StartIndex { get; set; }
        public long EndIndex { get; set; }
    }
}