﻿using AOM.Services.CrmService;

namespace AOMAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    using System.Drawing;

    public partial class OrganisationRolePrivileges : Form
    {
        private readonly ICrmAdministrationService _crmAdministrationService;

        private readonly IOrganisationService _organisationService;

        private readonly IBus _bus;

        private IOrganisation _currentOrganisation;

        private IOrganisationRole _currentRole;

        private Color ColorPrivilegeSelected = Color.Black;
        private Color ColorPrivilegeNotSelected = Color.DarkRed;
        private Color ColorProduct = Color.DarkGreen;

        public OrganisationRolePrivileges(
            ICrmAdministrationService crmAdministrationService,
            IOrganisationService organisationService,
            IBus bus,
            long? roleId,
            long organisationId)
        {
            _crmAdministrationService = crmAdministrationService;
            _bus = bus;
            _organisationService = organisationService;
            InitializeComponent();
            _currentOrganisation = _organisationService.GetOrganisationById(organisationId);

            tvPrivileges.Nodes[0].Nodes.Clear();
            tvPrivileges.Nodes[1].Nodes.Clear();

            if (roleId != null)
            {
                RoleSelected((long) roleId);
                cboRoles.Visible = false;
                tvPrivileges.Enabled = true;
                tvPrivileges.Visible = true;
            }
            else
            {
                var roles = _organisationService.GetOrganisationRoles(_currentOrganisation.Id);
                cboRoles.Visible = true;
                foreach (var role in roles)
                {
                    cboRoles.Items.Add(new ComboBoxItem(role.Id, role.Name));
                }
                cboRoles.SelectedItem = 1;
            }

        }

        private void RoleSelected(long roleId)
        {
            _currentRole = _organisationService.GetOrganisationRoles(_currentOrganisation.Id)
                .Single(_ => _.Id == roleId);
            lblRoleName.Text = _currentRole.Name;
            tvPrivileges.Nodes[0].Nodes.Clear();
            tvPrivileges.Nodes[1].Nodes.Clear();
            DisplayProductRolePrivileges(_currentRole);
            DisplaySystemRolePrivileges(_currentRole, _currentOrganisation.OrganisationType);
        }

        private void ManageRolePrivileges_Load(object sender, EventArgs e)
        {
            var currentOrganisationName = string.Empty;
            var currentRoleName = string.Empty;

            if (_currentOrganisation != null)
            {
                currentOrganisationName = _currentOrganisation.Name;
            }
            lblOrganisationName.Text = currentOrganisationName;

            if (_currentRole != null)
            {
                currentRoleName = _currentRole.Name;
            }
            lblRoleName.Text = currentRoleName;

            Text = String.Format("{0}/{1}: Role Privileges", currentOrganisationName, currentRoleName);

            if (cboRoles.Visible == false)
            {
                RoleSelected(_currentRole.Id);
                tvPrivileges.Enabled = true;
                tvPrivileges.Visible = true;
            }
            else
            {
                tvPrivileges.Enabled = false;
                tvPrivileges.Visible = false;
            }
        }

        private void DisplaySystemRolePrivileges(IOrganisationRole organisationRole, OrganisationType organisationType)
        {
            if (organisationRole != null)
            {
                var systemPrivileges = _crmAdministrationService.GetSystemPrivileges(organisationType);
                var systemRolePrivileges = _crmAdministrationService.GetSystemRolePrivileges(_currentRole.Id);
                var noPrivs = 0;

                foreach (var sp in systemPrivileges)
                {
                    if (sp.OrganisationType == OrganisationType.NotSpecified
                        || sp.OrganisationType == _currentOrganisation.OrganisationType)
                    {
                        bool hasPrivilege = DoesRoleHaveSystemPrivilege(_currentRole.Id, sp.Id, systemRolePrivileges);

                        var node = tvPrivileges.Nodes[1].Nodes.Add(sp.Id.ToString(), sp.Description);
                        node.Checked = hasPrivilege;
                        if (hasPrivilege)
                        {
                            noPrivs++;
                            node.ForeColor = ColorPrivilegeSelected;
                        }
                        else
                        {
                            node.ForeColor = ColorPrivilegeNotSelected;
                        }
                    }
                }
                if (noPrivs > 0)
                {
                    tvPrivileges.Nodes[1].Expand();
                }

            }
        }

        private bool DoesRoleHaveSystemPrivilege(long roleId, long privilegeId,
            IEnumerable<ISystemRolePrivilege> systemRolePrivileges)
        {
            return systemRolePrivileges.Any(srp => srp.PrivilegeId == privilegeId && srp.RoleId == roleId);
        }

        private void DisplayProductRolePrivileges(IOrganisationRole organisationRole)
        {
            if (organisationRole != null)
            {
                var products =
                    _bus.Request<GetAllProductDefinitionsRequestRpc, List<ProductDefinitionItem>>(
                        new GetAllProductDefinitionsRequestRpc());
                var subscribedProductPrivileges =
                    _crmAdministrationService.GetOrganisationSubscribedProductPrivileges(_currentOrganisation.Id);
                var productRolePrivileges = _crmAdministrationService.GetProductRolePrivileges(_currentRole.Id);

                foreach (var product in products)
                {
                    var node = tvPrivileges.Nodes[0].Nodes.Add(product.ProductId.ToString(), product.ProductName);
                    node.ForeColor = ColorProduct;
                    var noPrivs = 0;

                    foreach (var pp in subscribedProductPrivileges.Where(_ => _.ProductId == product.ProductId))
                    {
                        var ppnode = node.Nodes.Add(pp.ProductPrivilege.Id.ToString(), pp.ProductPrivilege.Description);
                        bool hasPrivilege = DoesRoleHaveProductPrivilege(
                            _currentOrganisation.Id,
                            _currentRole.Id,
                            product.ProductId,
                            pp.ProductPrivilegeId,
                            productRolePrivileges);
                        ppnode.Checked = hasPrivilege;
                        if (hasPrivilege)
                        {
                            noPrivs++;
                            ppnode.ForeColor = ColorPrivilegeSelected;
                        }
                        else
                        {
                            ppnode.ForeColor = ColorPrivilegeNotSelected;
                        }

                    }
                    if (noPrivs > 0)
                    {
                        node.Expand();
                        if (noPrivs == subscribedProductPrivileges.Count(_ => _.ProductId == product.ProductId))
                        {
                            node.Checked = true;
                        }
                    }
                }
                tvPrivileges.Nodes[0].Expand();
            }
        }


        private bool DoesRoleHaveProductPrivilege(long organisationId, long roleId, long productId,
            long productPrivilegeId, IList<ProductRolePrivilege> productRolePrivileges)
        {

            return
                productRolePrivileges.Any(
                    prp =>
                        prp.OrganisationId == organisationId && prp.RoleId == roleId && prp.ProductId == productId
                        && prp.ProductPrivilegeId == productPrivilegeId);
        }

        private void RemoveProductPrivilegeFromRole(long organisationId, long roleId, long productId,
            long productPrivilegeId)
        {
            var prp = new ProductRolePrivilege
            {
                OrganisationId = organisationId,
                RoleId = roleId,
                ProductId = productId,
                ProductPrivilegeId = productPrivilegeId
            };

            _crmAdministrationService.RemoveProductRolePrivilege(prp, AdminSettings.LoggedOnUser.Id);
        }

        private void AddProductPrivilegeToRole(long organisationId, long roleId, long productId, long productPrivilegeId)
        {
            var prp = new ProductRolePrivilege
            {
                OrganisationId = organisationId,
                RoleId = roleId,
                ProductId = productId,
                ProductPrivilegeId = productPrivilegeId
            };

            _crmAdministrationService.AddProductRolePrivilege(prp, AdminSettings.LoggedOnUser.Id);
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void RemoveSystemPrivilegeFromRole(long systemPrivilegeId, long roleId)
        {
            var srp = new SystemRolePrivilege {PrivilegeId = systemPrivilegeId, RoleId = roleId};

            _crmAdministrationService.RemoveSystemPrivilegeFromRole(srp, AdminSettings.LoggedOnUser.Id);
        }

        private void AddSystemPrivilegeToRole(long systemPrivilegeId, long roleId)
        {
            var srp = new SystemRolePrivilege {PrivilegeId = systemPrivilegeId, RoleId = roleId};

            _crmAdministrationService.AddSystemPrivilegeToRole(srp, AdminSettings.LoggedOnUser.Id);
        }

        private void cboRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = cboRoles.SelectedItem as ComboBoxItem;
            RoleSelected((selectedItem).Id);

            if (selectedItem == null)
            {
                tvPrivileges.Enabled = false;
                tvPrivileges.Visible = false;
            }
            else
            {
                tvPrivileges.Enabled = true;
                tvPrivileges.Visible = true;
            }
        }

        // Updates all child tree nodes recursively. 
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                if (node.Checked != nodeChecked)
                {
                    node.Checked = nodeChecked;
                    if (node.Nodes.Count > 0)
                    {
                        // If the current node has child nodes, call the CheckAllChildsNodes method recursively. 
                        CheckAllChildNodes(node, nodeChecked);
                    }
                    else
                    {
                        UpdateNode(node);
                    }
                }
            }
        }

        // NOTE   This code can be added to the BeforeCheck event handler instead of the AfterCheck event. 
        // After a tree node's Checked property is changed, all its child nodes are updated to the same value. 
        private void tvPrivileges_AfterCheck(object sender, TreeViewEventArgs e)
        {
            try
            {
                // The code only executes if the user caused the checked state to change. 
                if (e.Action != TreeViewAction.Unknown)
                {
                    using (new StWaitCursor())
                    {

                        if (e.Node.Nodes.Count > 0)
                        {
                            /* Calls the CheckAllChildNodes method, passing in the current 
                    Checked value of the TreeNode whose checked state changed. */
                            CheckAllChildNodes(e.Node, e.Node.Checked);
                        }
                        else
                        {
                            UpdateNode(e.Node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionReporter.ShowException(ex);
            }
        }

        private void UpdateNode(TreeNode node)
        {
            if (node.Checked)
            {
                if (node.Level == 1) // system privilege
                {
                    AddSystemPrivilegeToRole(long.Parse(node.Name), _currentRole.Id);
                }
                else // product privilege
                {
                    AddProductPrivilegeToRole(_currentOrganisation.Id, _currentRole.Id, long.Parse(node.Parent.Name),
                        long.Parse(node.Name));
                }
                node.ForeColor = ColorPrivilegeSelected;
            }
            else
            {
                if (node.Level == 1) // system privilege
                {
                    RemoveSystemPrivilegeFromRole(long.Parse(node.Name), _currentRole.Id);
                }
                else // product privilege
                {
                    RemoveProductPrivilegeFromRole(_currentOrganisation.Id, _currentRole.Id,
                        long.Parse(node.Parent.Name), long.Parse(node.Name));
                }
                node.ForeColor = ColorPrivilegeNotSelected;
            }
        }

        public class StWaitCursor : IDisposable
        {
            public StWaitCursor()
            {
                Cursor.Current = Cursors.WaitCursor;
            }

            public void Dispose()
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void tvPrivileges_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }
    }
}