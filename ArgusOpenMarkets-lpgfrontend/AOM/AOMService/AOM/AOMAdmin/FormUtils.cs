﻿using System.Reflection;
using System.Windows.Forms;

namespace AOMAdmin
{
    public static class FormUtils
    {
        public static void AppendVersionToTitle(Form form)
        {
            form.Text += string.Format(" ({0})", GetAssemblyVersionAsString());
        }

        public static string GetAssemblyVersionAsString()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}
