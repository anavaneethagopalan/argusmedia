﻿namespace AOMAdmin
{
    partial class UserBlocking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserBlocking));
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.chkTerminateUserSession = new System.Windows.Forms.CheckBox();
            this.btnBlock = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.btnUnblock = new System.Windows.Forms.Button();
            this.terminateSession = new System.Windows.Forms.Button();
            this.btnUpdatePasswordExpiration = new System.Windows.Forms.Button();
            this.lblPasswordExpiration = new System.Windows.Forms.Label();
            this.dtpPasswordExpiration = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "Username:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(71, 5);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(252, 20);
            this.txtUserName.TabIndex = 0;
            // 
            // chkTerminateUserSession
            // 
            this.chkTerminateUserSession.AutoSize = true;
            this.chkTerminateUserSession.Checked = true;
            this.chkTerminateUserSession.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTerminateUserSession.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkTerminateUserSession.Location = new System.Drawing.Point(15, 32);
            this.chkTerminateUserSession.Name = "chkTerminateUserSession";
            this.chkTerminateUserSession.Size = new System.Drawing.Size(193, 17);
            this.chkTerminateUserSession.TabIndex = 6;
            this.chkTerminateUserSession.Text = "Terminate User Session Immediately";
            this.chkTerminateUserSession.UseVisualStyleBackColor = true;
            // 
            // btnBlock
            // 
            this.btnBlock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlock.Location = new System.Drawing.Point(107, 112);
            this.btnBlock.Name = "btnBlock";
            this.btnBlock.Size = new System.Drawing.Size(75, 23);
            this.btnBlock.TabIndex = 2;
            this.btnBlock.Text = "Block User";
            this.btnBlock.UseVisualStyleBackColor = true;
            this.btnBlock.Click += new System.EventHandler(this.btnBlock_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(453, 112);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(14, 87);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(59, 13);
            this.labelResult.TabIndex = 7;
            this.labelResult.Text = "labelResult";
            // 
            // btnUnblock
            // 
            this.btnUnblock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUnblock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnblock.Location = new System.Drawing.Point(15, 112);
            this.btnUnblock.Name = "btnUnblock";
            this.btnUnblock.Size = new System.Drawing.Size(86, 23);
            this.btnUnblock.TabIndex = 1;
            this.btnUnblock.Text = "Unblock User";
            this.btnUnblock.UseVisualStyleBackColor = true;
            this.btnUnblock.Click += new System.EventHandler(this.btnUnblock_Click);
            // 
            // terminateSession
            // 
            this.terminateSession.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.terminateSession.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.terminateSession.Location = new System.Drawing.Point(188, 112);
            this.terminateSession.Name = "terminateSession";
            this.terminateSession.Size = new System.Drawing.Size(120, 23);
            this.terminateSession.TabIndex = 3;
            this.terminateSession.Text = "Terminate Sessions";
            this.terminateSession.UseVisualStyleBackColor = true;
            this.terminateSession.Click += new System.EventHandler(this.terminateSession_Click);
            // 
            // btnUpdatePasswordExpiration
            // 
            this.btnUpdatePasswordExpiration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdatePasswordExpiration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdatePasswordExpiration.Location = new System.Drawing.Point(315, 112);
            this.btnUpdatePasswordExpiration.Name = "btnUpdatePasswordExpiration";
            this.btnUpdatePasswordExpiration.Size = new System.Drawing.Size(114, 23);
            this.btnUpdatePasswordExpiration.TabIndex = 8;
            this.btnUpdatePasswordExpiration.Text = "Update Exipration";
            this.btnUpdatePasswordExpiration.UseVisualStyleBackColor = true;
            this.btnUpdatePasswordExpiration.Click += new System.EventHandler(this.btnUpdatePasswordExpiration_Click);
            // 
            // lblPasswordExpiration
            // 
            this.lblPasswordExpiration.Location = new System.Drawing.Point(8, 59);
            this.lblPasswordExpiration.Name = "lblPasswordExpiration";
            this.lblPasswordExpiration.Size = new System.Drawing.Size(110, 23);
            this.lblPasswordExpiration.TabIndex = 9;
            this.lblPasswordExpiration.Text = "Password Expiration:";
            // 
            // dtpPasswordExpiration
            // 
            this.dtpPasswordExpiration.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPasswordExpiration.Location = new System.Drawing.Point(125, 59);
            this.dtpPasswordExpiration.Name = "dtpPasswordExpiration";
            this.dtpPasswordExpiration.Size = new System.Drawing.Size(198, 20);
            this.dtpPasswordExpiration.TabIndex = 10;
            // 
            // UserBlocking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 143);
            this.Controls.Add(this.dtpPasswordExpiration);
            this.Controls.Add(this.lblPasswordExpiration);
            this.Controls.Add(this.btnUpdatePasswordExpiration);
            this.Controls.Add(this.terminateSession);
            this.Controls.Add(this.btnUnblock);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBlock);
            this.Controls.Add(this.chkTerminateUserSession);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUserName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserBlocking";
            this.Text = "Block User";
            this.Load += new System.EventHandler(this.BlockUserForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.CheckBox chkTerminateUserSession;
        private System.Windows.Forms.Button btnBlock;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Button btnUnblock;
        private System.Windows.Forms.Button terminateSession;
        private System.Windows.Forms.Button btnUpdatePasswordExpiration;
        private System.Windows.Forms.Label lblPasswordExpiration;
        private System.Windows.Forms.DateTimePicker dtpPasswordExpiration;
    }
}