﻿namespace AOMAdmin
{
    partial class ProductMetadataItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.tbMetadataLayout = new System.Windows.Forms.TableLayoutPanel();
            this.lbDisplayName = new System.Windows.Forms.Label();
            this.lbFieldType = new System.Windows.Forms.Label();
            this.cmbFieldType = new System.Windows.Forms.ComboBox();
            this.tbDisplayName = new System.Windows.Forms.TextBox();
            this.tbEnumMetadataLayout = new System.Windows.Forms.TableLayoutPanel();
            this.lbMetadataList = new System.Windows.Forms.Label();
            this.cmbMetadataList = new System.Windows.Forms.ComboBox();
            this.tbStringMetadataLayout = new System.Windows.Forms.TableLayoutPanel();
            this.lbValueMinimum = new System.Windows.Forms.Label();
            this.lbValueMaximum = new System.Windows.Forms.Label();
            this.nmValueMaximum = new System.Windows.Forms.NumericUpDown();
            this.nmValueMinimum = new System.Windows.Forms.NumericUpDown();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbLayoutMain.SuspendLayout();
            this.tbMetadataLayout.SuspendLayout();
            this.tbEnumMetadataLayout.SuspendLayout();
            this.tbStringMetadataLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmValueMaximum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmValueMinimum)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbLayoutMain
            // 
            this.tbLayoutMain.ColumnCount = 1;
            this.tbLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbLayoutMain.Controls.Add(this.tbMetadataLayout, 0, 0);
            this.tbLayoutMain.Controls.Add(this.tbEnumMetadataLayout, 0, 2);
            this.tbLayoutMain.Controls.Add(this.tbStringMetadataLayout, 0, 1);
            this.tbLayoutMain.Controls.Add(this.flowLayoutPanel2, 0, 3);
            this.tbLayoutMain.Location = new System.Drawing.Point(12, 12);
            this.tbLayoutMain.Name = "tbLayoutMain";
            this.tbLayoutMain.RowCount = 4;
            this.tbLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tbLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tbLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tbLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbLayoutMain.Size = new System.Drawing.Size(290, 186);
            this.tbLayoutMain.TabIndex = 0;
            // 
            // tbMetadataLayout
            // 
            this.tbMetadataLayout.ColumnCount = 2;
            this.tbMetadataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbMetadataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tbMetadataLayout.Controls.Add(this.lbDisplayName, 0, 0);
            this.tbMetadataLayout.Controls.Add(this.lbFieldType, 0, 1);
            this.tbMetadataLayout.Controls.Add(this.cmbFieldType, 1, 1);
            this.tbMetadataLayout.Controls.Add(this.tbDisplayName, 1, 0);
            this.tbMetadataLayout.Location = new System.Drawing.Point(3, 3);
            this.tbMetadataLayout.Name = "tbMetadataLayout";
            this.tbMetadataLayout.RowCount = 2;
            this.tbMetadataLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tbMetadataLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tbMetadataLayout.Size = new System.Drawing.Size(284, 54);
            this.tbMetadataLayout.TabIndex = 1;
            // 
            // lbDisplayName
            // 
            this.lbDisplayName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDisplayName.AutoSize = true;
            this.lbDisplayName.Location = new System.Drawing.Point(3, 0);
            this.lbDisplayName.Name = "lbDisplayName";
            this.lbDisplayName.Size = new System.Drawing.Size(103, 27);
            this.lbDisplayName.TabIndex = 0;
            this.lbDisplayName.Text = "Display Name:";
            this.lbDisplayName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbFieldType
            // 
            this.lbFieldType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbFieldType.AutoSize = true;
            this.lbFieldType.Location = new System.Drawing.Point(3, 27);
            this.lbFieldType.Name = "lbFieldType";
            this.lbFieldType.Size = new System.Drawing.Size(103, 27);
            this.lbFieldType.TabIndex = 1;
            this.lbFieldType.Text = "Field Type:";
            this.lbFieldType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbFieldType
            // 
            this.cmbFieldType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbFieldType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFieldType.FormattingEnabled = true;
            this.cmbFieldType.Location = new System.Drawing.Point(112, 30);
            this.cmbFieldType.Name = "cmbFieldType";
            this.cmbFieldType.Size = new System.Drawing.Size(169, 21);
            this.cmbFieldType.TabIndex = 2;
            this.cmbFieldType.SelectedValueChanged += new System.EventHandler(this.cmbFieldType_SelectedValueChanged);
            // 
            // tbDisplayName
            // 
            this.tbDisplayName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tbDisplayName.Location = new System.Drawing.Point(112, 3);
            this.tbDisplayName.MaxLength = 50;
            this.tbDisplayName.Name = "tbDisplayName";
            this.tbDisplayName.Size = new System.Drawing.Size(169, 20);
            this.tbDisplayName.TabIndex = 3;
            // 
            // tbEnumMetadataLayout
            // 
            this.tbEnumMetadataLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEnumMetadataLayout.AutoSize = true;
            this.tbEnumMetadataLayout.ColumnCount = 2;
            this.tbEnumMetadataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbEnumMetadataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tbEnumMetadataLayout.Controls.Add(this.lbMetadataList, 0, 0);
            this.tbEnumMetadataLayout.Controls.Add(this.cmbMetadataList, 1, 0);
            this.tbEnumMetadataLayout.Location = new System.Drawing.Point(3, 121);
            this.tbEnumMetadataLayout.Name = "tbEnumMetadataLayout";
            this.tbEnumMetadataLayout.RowCount = 1;
            this.tbEnumMetadataLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbEnumMetadataLayout.Size = new System.Drawing.Size(284, 27);
            this.tbEnumMetadataLayout.TabIndex = 3;
            // 
            // lbMetadataList
            // 
            this.lbMetadataList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMetadataList.AutoSize = true;
            this.lbMetadataList.Location = new System.Drawing.Point(3, 0);
            this.lbMetadataList.Name = "lbMetadataList";
            this.lbMetadataList.Size = new System.Drawing.Size(103, 27);
            this.lbMetadataList.TabIndex = 0;
            this.lbMetadataList.Text = "Metadata List:";
            this.lbMetadataList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbMetadataList
            // 
            this.cmbMetadataList.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbMetadataList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMetadataList.FormattingEnabled = true;
            this.cmbMetadataList.Location = new System.Drawing.Point(112, 3);
            this.cmbMetadataList.Name = "cmbMetadataList";
            this.cmbMetadataList.Size = new System.Drawing.Size(169, 21);
            this.cmbMetadataList.TabIndex = 1;
            // 
            // tbStringMetadataLayout
            // 
            this.tbStringMetadataLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStringMetadataLayout.AutoSize = true;
            this.tbStringMetadataLayout.ColumnCount = 2;
            this.tbStringMetadataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbStringMetadataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tbStringMetadataLayout.Controls.Add(this.lbValueMinimum, 0, 0);
            this.tbStringMetadataLayout.Controls.Add(this.lbValueMaximum, 0, 1);
            this.tbStringMetadataLayout.Controls.Add(this.nmValueMaximum, 1, 1);
            this.tbStringMetadataLayout.Controls.Add(this.nmValueMinimum, 1, 0);
            this.tbStringMetadataLayout.Location = new System.Drawing.Point(3, 63);
            this.tbStringMetadataLayout.Name = "tbStringMetadataLayout";
            this.tbStringMetadataLayout.RowCount = 2;
            this.tbStringMetadataLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tbStringMetadataLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tbStringMetadataLayout.Size = new System.Drawing.Size(284, 52);
            this.tbStringMetadataLayout.TabIndex = 2;
            // 
            // lbValueMinimum
            // 
            this.lbValueMinimum.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbValueMinimum.AutoSize = true;
            this.lbValueMinimum.Location = new System.Drawing.Point(3, 0);
            this.lbValueMinimum.Name = "lbValueMinimum";
            this.lbValueMinimum.Size = new System.Drawing.Size(103, 26);
            this.lbValueMinimum.TabIndex = 0;
            this.lbValueMinimum.Text = "Value Minimum:";
            this.lbValueMinimum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbValueMaximum
            // 
            this.lbValueMaximum.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbValueMaximum.AutoSize = true;
            this.lbValueMaximum.Location = new System.Drawing.Point(3, 26);
            this.lbValueMaximum.Name = "lbValueMaximum";
            this.lbValueMaximum.Size = new System.Drawing.Size(103, 26);
            this.lbValueMaximum.TabIndex = 1;
            this.lbValueMaximum.Text = "Value Maximum:";
            this.lbValueMaximum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nmValueMaximum
            // 
            this.nmValueMaximum.Location = new System.Drawing.Point(112, 29);
            this.nmValueMaximum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmValueMaximum.Name = "nmValueMaximum";
            this.nmValueMaximum.Size = new System.Drawing.Size(169, 20);
            this.nmValueMaximum.TabIndex = 3;
            this.nmValueMaximum.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // nmValueMinimum
            // 
            this.nmValueMinimum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.nmValueMinimum.Location = new System.Drawing.Point(112, 3);
            this.nmValueMinimum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmValueMinimum.Name = "nmValueMinimum";
            this.nmValueMinimum.Size = new System.Drawing.Size(169, 20);
            this.nmValueMinimum.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this.btnSave);
            this.flowLayoutPanel2.Controls.Add(this.btnCancel);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(125, 154);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(162, 29);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(3, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(84, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // ProductMetadataItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(319, 210);
            this.Controls.Add(this.tbLayoutMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ProductMetadataItemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Product Metadata Item";
            this.tbLayoutMain.ResumeLayout(false);
            this.tbLayoutMain.PerformLayout();
            this.tbMetadataLayout.ResumeLayout(false);
            this.tbMetadataLayout.PerformLayout();
            this.tbEnumMetadataLayout.ResumeLayout(false);
            this.tbEnumMetadataLayout.PerformLayout();
            this.tbStringMetadataLayout.ResumeLayout(false);
            this.tbStringMetadataLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmValueMaximum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmValueMinimum)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tbLayoutMain;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TableLayoutPanel tbMetadataLayout;
        private System.Windows.Forms.Label lbDisplayName;
        private System.Windows.Forms.Label lbFieldType;
        private System.Windows.Forms.ComboBox cmbFieldType;
        private System.Windows.Forms.TextBox tbDisplayName;
        private System.Windows.Forms.TableLayoutPanel tbEnumMetadataLayout;
        private System.Windows.Forms.Label lbMetadataList;
        private System.Windows.Forms.ComboBox cmbMetadataList;
        private System.Windows.Forms.TableLayoutPanel tbStringMetadataLayout;
        private System.Windows.Forms.Label lbValueMinimum;
        private System.Windows.Forms.Label lbValueMaximum;
        private System.Windows.Forms.NumericUpDown nmValueMinimum;
        private System.Windows.Forms.NumericUpDown nmValueMaximum;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
    }
}