﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.CrmService;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using AOM.Transport.Events.Users;
using AOM.Services.EncryptionService;

namespace AOMAdmin
{
    public partial class UserMain : Form
    {
        private readonly IBus _bus;
        private readonly ICrmAdministrationService _crmAdministrationService;
        private readonly IOrganisationService _organisationService;
        private readonly IUserService _userService;
        private readonly ICredentialsService _credentialsService;

        private Organisation _organisation;
        private readonly IEncryptionService _encryptionService;

        private IList<User> _users;

        public UserMain(ICrmAdministrationService crmAdministrationService, IBus bus, IUserService userService, IOrganisationService organisationService, 
            IEncryptionService encryptionService, ICredentialsService credentialsService, Organisation organisation = null)
        {
            _bus = bus;
            _crmAdministrationService = crmAdministrationService;
            _organisation = organisation;
            _encryptionService = encryptionService;
            _organisationService = organisationService;
            _userService = userService;
            _credentialsService = credentialsService;

            InitializeComponent();
        }

        private void ManageUsersOrders_Load(object sender, EventArgs e)
        {
            Text = _organisation != null ? _organisation.Name + ": Manage Users" : "All Organisations: Manage Users";
            RefreshUsers(chkOnlyShowBlockedUsers.Checked, chkOnlyShowDeletedUsers.Checked);
        }

        private void RefreshUsers(bool onlyShowBlockedUsers, bool onlyShowDeletedUsers)
        {
            var users = _userService.GetUsers(_organisation != null ? _organisation.Id : -1).AsEnumerable();

            if (onlyShowBlockedUsers)
                users = users.Where(u => u.IsBlocked);
            if (onlyShowDeletedUsers)
                users = users.Where(u => u.IsDeleted);

            _users = users.OrderBy(u => u.Username).ToList();

            long orgId = (_organisation != null) ? _organisation.Id : -1;
            var userCredentials = _credentialsService.GetUsersCredentials(orgId).ToList();

            dgUsers.Rows.Clear();
            dgUsers.Refresh();
            for (var ix = 0; ix < _users.Count; ix++)
            {
                var user = _users[ix];
                var userCredential = userCredentials.Where(u => u.UserId == user.Id).ToList();
                var permCredential = userCredential.FirstOrDefault(u => u.CredentialType == CredentialType.Permanent);
                var tempCredential = userCredential.FirstOrDefault(u => u.CredentialType == CredentialType.Temporary);
                DateTime? expirationDate = null;
                string expirationString = "-";
                if (permCredential != null)
                {
                    expirationDate = permCredential.Expiration;
                    expirationString = permCredential.Expiration.ToString();
                }
                else if (tempCredential != null)
                {
                    expirationDate = tempCredential.Expiration;
                    expirationString = "(" + tempCredential.Expiration + ")";
                }

                dgUsers.Rows.Add(ix, user.Username, user.Name, user.Email, user.ArgusCrmUsername, expirationString, user.IsActive, user.IsBlocked, user.IsDeleted,
                    "Block...", "Password...", "Permissions...", "Orders...", "Details...");
                dgUsers.Rows[ix].Cells[5].Style.ForeColor = expirationDate < DateTime.UtcNow ? Color.Red : Color.Black;
                
            }
        }

        private void _cmdRefresh_Click(object sender, EventArgs e)
        {
            RefreshUsers(chkOnlyShowBlockedUsers.Checked, chkOnlyShowDeletedUsers.Checked);
        }

        private void dgUsers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var user = _users[dgUsers.Rows[e.RowIndex].Cells[0].Value as int? ?? 0];

                switch (e.ColumnIndex)
                {
                    case 9:
                        ShowBlock(user);
                        break;
                    case 10:
                        ShowPassword(user);
                        break;
                    case 11:
                        ShowPermissions(user);
                        break;
                    case 12:
                        ShowOrders(user);
                        break;
                    case 13:
                        ShowDetails(user);
                        break;
                    default:
                        MessageBox.Show("Bug in code - unexpected button index.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        break;
                }
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmdAddUser_Click(object sender, EventArgs e)
        {
            var encryptionService = new EncryptionService();
            var credentialsService = new CredentialsService(new DbContextFactory(), new DateTimeProvider());
            var frm = new UserCreate(_crmAdministrationService,  encryptionService, credentialsService, _organisationService, _userService, new DateTimeProvider(), _organisation);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
        }

        private void ShowPassword(User user)
        {
            // Change the Users OldPassword.
            //TODO: Use ninject to obtain services/bus
            var encryptionService = new EncryptionService();
            var frm = new UserPasswordChange(_bus, encryptionService, _crmAdministrationService, _userService, user);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
        }

        private void ShowBlock(User user)
        {
            //TODO: Use ninject to obtain services/bus
            var userCredential =  _credentialsService.GetUsersCredentialsByUsername(user.Username).FirstOrDefault(c => c.CredentialType == CredentialType.Permanent);
            var frm = new UserBlocking(_bus, _crmAdministrationService, user, userCredential) {StartPosition = FormStartPosition.CenterParent};
            frm.ShowDialog();
        }

        private void ShowPermissions(User user)
        {
            var frm = new UserPermissions(_bus, _crmAdministrationService, _userService, _organisationService, user)
            {
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void ShowOrders(User user)
        {
            var frm = new UserOrders(_bus, _crmAdministrationService, _userService, user)
            {
                StartPosition = FormStartPosition.CenterParent
            };
            frm.ShowDialog();
        }

        private void ShowDetails(User user)
        {
            var userOrganisation =
                _organisationService.GetOrganisationById(user.OrganisationId) as Organisation;
            var dlg = new UserDetails(_crmAdministrationService, user, userOrganisation)
            {
                StartPosition = FormStartPosition.CenterParent
            };
            dlg.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var newPassword = "Password%";

            // We are reseting ALL USERS Passwords.
            foreach (var user in _users)
            {
                if (user.Username.ToLower() != "pdsdbo")
                {
                    ResetUserPassword(user.Username, newPassword);
                }
            }
        }


        private void ResetUserPassword(string username, string newPassword)
        {
            var tempExpiration = 0;
            var user = new User {Id = _userService.GetUserId(username)};
            var hashedPassword = _encryptionService.Hash(user, newPassword);

            var request = new ChangePasswordRequestRpc
            {
                Username = username,
                AdminResetPassword = true,
                OldPassword = hashedPassword,
                NewPassword = hashedPassword,
                CheckOldPassword = false,
                TempPassExpirInHours = tempExpiration,
                IsTemporary = false,
                RequestorUserId = AdminSettings.LoggedOnUser.Id
            };

            _bus.Request<ChangePasswordRequestRpc, ChangePasswordResponse>(request);
        }

        private void chkOnlyShowBlockedUsers_CheckedChanged(object sender, EventArgs e)
        {
            RefreshUsers(chkOnlyShowBlockedUsers.Checked, chkOnlyShowDeletedUsers.Checked);
        }

        private void chkOnlyShowDeletedUsers_CheckedChanged(object sender, EventArgs e)
        {
            RefreshUsers(chkOnlyShowBlockedUsers.Checked, chkOnlyShowDeletedUsers.Checked);
        }
    }
}