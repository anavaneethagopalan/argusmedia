﻿using AOM.App.Domain.Services;
using AOM.Transport.Service.Processor.Common.ArgusBus;
using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;
using Ninject;
using Ninject.Modules;
using System;
using AOM.Services.EncryptionService;

namespace AOMAdmin
{
    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            try
            {
                var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
                var _bus = Argus.Transport.Transport.CreateBus(aomConnectionConfiguration, null, new BusLogger());
                kernel.Bind<Login>().ToSelf();
                kernel.Bind<IBus>().ToConstant(_bus);
                kernel.Bind<IEncryptionService>().To<EncryptionService>();
                kernel.Bind<UserCreate>().ToSelf();
            }
            catch (Exception ex)
            {
                ExceptionReporter.ShowException(ex, "Bootstrapper - Load - failed");

            }
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}