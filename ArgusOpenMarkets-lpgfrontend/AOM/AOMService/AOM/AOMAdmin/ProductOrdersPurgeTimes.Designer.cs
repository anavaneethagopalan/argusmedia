﻿namespace AOMAdmin
{
    partial class ProductOrdersPurgeTimes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductOrdersPurgeTimes));
            this._ConfigurationsDataGridView = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NextPurgeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeOfDayColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurgeFrequencyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCmdKill = new System.Windows.Forms.DataGridViewButtonColumn();
            this._SaveChangesButton = new System.Windows.Forms.Button();
            this._refreshProductsButton = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._ConfigurationsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _ConfigurationsDataGridView
            // 
            this._ConfigurationsDataGridView.AllowUserToAddRows = false;
            this._ConfigurationsDataGridView.AllowUserToDeleteRows = false;
            this._ConfigurationsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ConfigurationsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._ConfigurationsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._ConfigurationsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.ProductNameColumn,
            this.NextPurgeColumn,
            this.TimeOfDayColumn,
            this.PurgeFrequencyColumn,
            this.colCmdKill});
            this._ConfigurationsDataGridView.Location = new System.Drawing.Point(11, 14);
            this._ConfigurationsDataGridView.Name = "_ConfigurationsDataGridView";
            this._ConfigurationsDataGridView.Size = new System.Drawing.Size(603, 68);
            this._ConfigurationsDataGridView.TabIndex = 0;
            this._ConfigurationsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._ConfigurationsDataGridView_CellContentClick);
            this._ConfigurationsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this._ConfigurationsDataGridView_CellValueChanged);
            // 
            // IdColumn
            // 
            this.IdColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "Id";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            this.IdColumn.Width = 10;
            // 
            // ProductNameColumn
            // 
            this.ProductNameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProductNameColumn.DataPropertyName = "Name";
            this.ProductNameColumn.FillWeight = 186.8477F;
            this.ProductNameColumn.HeaderText = "Product";
            this.ProductNameColumn.Name = "ProductNameColumn";
            this.ProductNameColumn.ReadOnly = true;
            this.ProductNameColumn.Width = 140;
            // 
            // NextPurgeColumn
            // 
            this.NextPurgeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.NextPurgeColumn.DataPropertyName = "NextPurge";
            this.NextPurgeColumn.FillWeight = 25.3579F;
            this.NextPurgeColumn.HeaderText = "Next Purge Date Utc";
            this.NextPurgeColumn.Name = "NextPurgeColumn";
            // 
            // TimeOfDayColumn
            // 
            this.TimeOfDayColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TimeOfDayColumn.DataPropertyName = "TimeOfDay";
            this.TimeOfDayColumn.FillWeight = 25.3579F;
            this.TimeOfDayColumn.HeaderText = "Next Purge Time Utc";
            this.TimeOfDayColumn.Name = "TimeOfDayColumn";
            // 
            // PurgeFrequencyColumn
            // 
            this.PurgeFrequencyColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PurgeFrequencyColumn.DataPropertyName = "PurgeFrequency";
            this.PurgeFrequencyColumn.FillWeight = 162.4365F;
            this.PurgeFrequencyColumn.HeaderText = "Purge Frequency";
            this.PurgeFrequencyColumn.Name = "PurgeFrequencyColumn";
            // 
            // colCmdKill
            // 
            this.colCmdKill.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colCmdKill.DataPropertyName = "Kill";
            this.colCmdKill.HeaderText = "Send Kill Request";
            this.colCmdKill.Name = "colCmdKill";
            this.colCmdKill.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colCmdKill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _SaveChangesButton
            // 
            this._SaveChangesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._SaveChangesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._SaveChangesButton.Location = new System.Drawing.Point(93, 88);
            this._SaveChangesButton.Name = "_SaveChangesButton";
            this._SaveChangesButton.Size = new System.Drawing.Size(90, 23);
            this._SaveChangesButton.TabIndex = 2;
            this._SaveChangesButton.Text = "Save Changes";
            this._SaveChangesButton.UseVisualStyleBackColor = true;
            this._SaveChangesButton.Click += new System.EventHandler(this._SaveChangesButton_Click);
            // 
            // _refreshProductsButton
            // 
            this._refreshProductsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._refreshProductsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._refreshProductsButton.Location = new System.Drawing.Point(12, 88);
            this._refreshProductsButton.Name = "_refreshProductsButton";
            this._refreshProductsButton.Size = new System.Drawing.Size(75, 23);
            this._refreshProductsButton.TabIndex = 1;
            this._refreshProductsButton.Text = "Refresh";
            this._refreshProductsButton.UseVisualStyleBackColor = true;
            this._refreshProductsButton.Click += new System.EventHandler(this._refreshProductsButton_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(520, 88);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // ProductOrdersPurgeTimes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 123);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this._refreshProductsButton);
            this.Controls.Add(this._SaveChangesButton);
            this.Controls.Add(this._ConfigurationsDataGridView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductOrdersPurgeTimes";
            this.Text = "ConfigurePurgeProductOrders";
            this.Load += new System.EventHandler(this.ConfigurePurgeProductOrders_Load);
            ((System.ComponentModel.ISupportInitialize)(this._ConfigurationsDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView _ConfigurationsDataGridView;
        private System.Windows.Forms.Button _SaveChangesButton;
        private System.Windows.Forms.Button _refreshProductsButton;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NextPurgeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeOfDayColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurgeFrequencyColumn;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdKill;
    }
}