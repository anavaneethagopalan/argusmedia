﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class ProductDeals : Form
    {
        private readonly IBus _bus;
        private readonly Product _product;

        private Boolean _didUpdate;

        public ProductDeals(IBus bus, Product product = null)
        {
            _bus = bus;
            _product = product;
            InitializeComponent();

            _ConfigurationsDataGridView.AutoGenerateColumns = false;
        }

        private void PopulateProducts()
        {
            var product = _bus.Request<GetProductRequestRpc, AomProductResponse>(new GetProductRequestRpc { Id = _product.ProductId }).Product;

            _ConfigurationsDataGridView.DataSource = new List<Product> { product };            

            _ConfigurationsDataGridView.Refresh();
        }

        private void ProductDeals_Load(object sender, EventArgs e)
        {
            _didUpdate = false;

            PopulateProducts();
        }

        private void _RefreshButton_Click(object sender, EventArgs e)
        {
            PopulateProducts();
        }

        private void _CloseButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = _didUpdate ? DialogResult.Yes : DialogResult.No;
            Close();
        }

        private void _SaveButton_Click(object sender, EventArgs e)
        {
            var products = (List<Product>)_ConfigurationsDataGridView.DataSource;

            foreach (var product in products)
            {
                var request = new AomProductConfigurePurgeRequest
                {
                    ClientSessionInfo = new ClientSessionInfo
                    {
                        UserId = AdminSettings.LoggedOnUser.Id
                    },
                    MessageAction = MessageAction.ConfigurePurge,
                    Product = product
                };

                _bus.Publish(request);

                _didUpdate = true;
            }
        }
    }
}
