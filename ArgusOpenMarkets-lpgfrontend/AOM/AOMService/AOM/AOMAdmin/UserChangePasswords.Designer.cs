﻿namespace AOMAdmin
{
    partial class UserChangePasswords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtUsersToExclude = new System.Windows.Forms.TextBox();
            this.cmdChangePasswords = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(4, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 28);
            this.label2.TabIndex = 7;
            this.label2.Text = "Users to exclude";
            // 
            // txtUsersToExclude
            // 
            this.txtUsersToExclude.Location = new System.Drawing.Point(190, 13);
            this.txtUsersToExclude.Margin = new System.Windows.Forms.Padding(4);
            this.txtUsersToExclude.Name = "txtUsersToExclude";
            this.txtUsersToExclude.Size = new System.Drawing.Size(320, 22);
            this.txtUsersToExclude.TabIndex = 6;
            this.txtUsersToExclude.Text = "ae1,pdsdbo";
            // 
            // cmdChangePasswords
            // 
            this.cmdChangePasswords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdChangePasswords.Location = new System.Drawing.Point(350, 139);
            this.cmdChangePasswords.Margin = new System.Windows.Forms.Padding(4);
            this.cmdChangePasswords.Name = "cmdChangePasswords";
            this.cmdChangePasswords.Size = new System.Drawing.Size(160, 28);
            this.cmdChangePasswords.TabIndex = 8;
            this.cmdChangePasswords.Text = "Change Passwords";
            this.cmdChangePasswords.UseVisualStyleBackColor = true;
            this.cmdChangePasswords.Click += new System.EventHandler(this.cmdChangePasswords_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 62);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 28);
            this.label1.TabIndex = 10;
            this.label1.Text = "New Password:";
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Location = new System.Drawing.Point(190, 58);
            this.txtNewPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.Size = new System.Drawing.Size(320, 22);
            this.txtNewPassword.TabIndex = 9;
            this.txtNewPassword.Text = "password%";
            // 
            // UserChangePasswords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 178);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNewPassword);
            this.Controls.Add(this.cmdChangePasswords);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUsersToExclude);
            this.Name = "UserChangePasswords";
            this.Text = "UserChangePasswords";
            this.Load += new System.EventHandler(this.UserChangePasswords_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUsersToExclude;
        private System.Windows.Forms.Button cmdChangePasswords;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNewPassword;
    }
}