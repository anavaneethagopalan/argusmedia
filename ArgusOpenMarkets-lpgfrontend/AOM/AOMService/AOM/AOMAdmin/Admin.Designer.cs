﻿namespace AOMAdmin
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Admin));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aOMMonitorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUser = new System.Windows.Forms.ToolStripMenuItem();
            this.manageUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.organisationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageOrganisationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createMultipleOrganisationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageProductsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productMarketOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureAutoOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productMarketCloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureAutoCloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.argusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeNewsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeNewsNowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureAutoPurgeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeOrdersInDiffusionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeProductOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marketTIckerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeMarketInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeMarketInfoNowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureAutoPurgeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diffusionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteDiffusionTopicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.csvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importOrganisationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importOrganisationProductsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importUserRolesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importCounterpartyPermissionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importBrokerPermissionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importOrganisationProductsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monitorDiffusionTopicsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.mnuMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersToolStripMenuItem,
            this.disableAccountToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(193, 56);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
            this.usersToolStripMenuItem.Text = "Users";
            // 
            // disableAccountToolStripMenuItem
            // 
            this.disableAccountToolStripMenuItem.Name = "disableAccountToolStripMenuItem";
            this.disableAccountToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
            this.disableAccountToolStripMenuItem.Text = "Disable Account";
            // 
            // mnuMenu
            // 
            this.mnuMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnuMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.mnuUser,
            this.organisationToolStripMenuItem,
            this.productToolStripMenuItem,
            this.argusToolStripMenuItem,
            this.ordersToolStripMenuItem,
            this.marketTIckerToolStripMenuItem,
            this.diffusionToolStripMenuItem,
            this.csvToolStripMenuItem});
            this.mnuMenu.Location = new System.Drawing.Point(0, 0);
            this.mnuMenu.Name = "mnuMenu";
            this.mnuMenu.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.mnuMenu.Size = new System.Drawing.Size(1344, 28);
            this.mnuMenu.TabIndex = 2;
            this.mnuMenu.Text = "Users";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.aOMMonitorToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(108, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aOMMonitorToolStripMenuItem
            // 
            this.aOMMonitorToolStripMenuItem.Name = "aOMMonitorToolStripMenuItem";
            this.aOMMonitorToolStripMenuItem.Size = new System.Drawing.Size(108, 26);
            // 
            // mnuUser
            // 
            this.mnuUser.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageUsersToolStripMenuItem});
            this.mnuUser.Name = "mnuUser";
            this.mnuUser.Size = new System.Drawing.Size(50, 24);
            this.mnuUser.Text = "User";
            // 
            // manageUsersToolStripMenuItem
            // 
            this.manageUsersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("manageUsersToolStripMenuItem.Image")));
            this.manageUsersToolStripMenuItem.Name = "manageUsersToolStripMenuItem";
            this.manageUsersToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.manageUsersToolStripMenuItem.Text = "Manage Users";
            this.manageUsersToolStripMenuItem.Click += new System.EventHandler(this.manageUsersToolStripMenuItem_Click);
            // 
            // organisationToolStripMenuItem
            // 
            this.organisationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageOrganisationsToolStripMenuItem,
            this.createMultipleOrganisationsToolStripMenuItem});
            this.organisationToolStripMenuItem.Name = "organisationToolStripMenuItem";
            this.organisationToolStripMenuItem.Size = new System.Drawing.Size(106, 24);
            this.organisationToolStripMenuItem.Text = "Organisation";
            // 
            // manageOrganisationsToolStripMenuItem
            // 
            this.manageOrganisationsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("manageOrganisationsToolStripMenuItem.Image")));
            this.manageOrganisationsToolStripMenuItem.Name = "manageOrganisationsToolStripMenuItem";
            this.manageOrganisationsToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
            this.manageOrganisationsToolStripMenuItem.Text = "Manage Organisations";
            this.manageOrganisationsToolStripMenuItem.Click += new System.EventHandler(this.manageOrganisationsToolStripMenuItem_Click);
            // 
            // createMultipleOrganisationsToolStripMenuItem
            // 
            this.createMultipleOrganisationsToolStripMenuItem.Name = "createMultipleOrganisationsToolStripMenuItem";
            this.createMultipleOrganisationsToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
            this.createMultipleOrganisationsToolStripMenuItem.Text = "Create Multiple Organisations";
            this.createMultipleOrganisationsToolStripMenuItem.Click += new System.EventHandler(this.createMultipleOrganisationsToolStripMenuItem_Click);
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageProductsToolStripMenuItem,
            this.productMarketOpenToolStripMenuItem,
            this.productMarketCloseToolStripMenuItem});
            this.productToolStripMenuItem.Name = "productToolStripMenuItem";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.productToolStripMenuItem.Text = "Product";
            // 
            // manageProductsToolStripMenuItem
            // 
            this.manageProductsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("manageProductsToolStripMenuItem.Image")));
            this.manageProductsToolStripMenuItem.Name = "manageProductsToolStripMenuItem";
            this.manageProductsToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.manageProductsToolStripMenuItem.Text = "Manage Products";
            this.manageProductsToolStripMenuItem.Click += new System.EventHandler(this.manageProductsToolStripMenuItem_Click);
            // 
            // productMarketOpenToolStripMenuItem
            // 
            this.productMarketOpenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureAutoOpenToolStripMenuItem});
            this.productMarketOpenToolStripMenuItem.Enabled = false;
            this.productMarketOpenToolStripMenuItem.Name = "productMarketOpenToolStripMenuItem";
            this.productMarketOpenToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.productMarketOpenToolStripMenuItem.Text = "Product Market Open";
            // 
            // configureAutoOpenToolStripMenuItem
            // 
            this.configureAutoOpenToolStripMenuItem.Name = "configureAutoOpenToolStripMenuItem";
            this.configureAutoOpenToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.configureAutoOpenToolStripMenuItem.Text = "Configure Auto Open";
            this.configureAutoOpenToolStripMenuItem.Click += new System.EventHandler(this.configureAutoOpenToolStripMenuItem_Click);
            // 
            // productMarketCloseToolStripMenuItem
            // 
            this.productMarketCloseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureAutoCloseToolStripMenuItem});
            this.productMarketCloseToolStripMenuItem.Enabled = false;
            this.productMarketCloseToolStripMenuItem.Name = "productMarketCloseToolStripMenuItem";
            this.productMarketCloseToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.productMarketCloseToolStripMenuItem.Text = "Product Market Close";
            // 
            // configureAutoCloseToolStripMenuItem
            // 
            this.configureAutoCloseToolStripMenuItem.Name = "configureAutoCloseToolStripMenuItem";
            this.configureAutoCloseToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.configureAutoCloseToolStripMenuItem.Text = "Configure Auto Close";
            this.configureAutoCloseToolStripMenuItem.Click += new System.EventHandler(this.configureAutoCloseToolStripMenuItem_Click);
            // 
            // argusToolStripMenuItem
            // 
            this.argusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purgeNewsToolStripMenuItem});
            this.argusToolStripMenuItem.Name = "argusToolStripMenuItem";
            this.argusToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.argusToolStripMenuItem.Text = "News";
            // 
            // purgeNewsToolStripMenuItem
            // 
            this.purgeNewsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purgeNewsNowToolStripMenuItem,
            this.configureAutoPurgeToolStripMenuItem1});
            this.purgeNewsToolStripMenuItem.Name = "purgeNewsToolStripMenuItem";
            this.purgeNewsToolStripMenuItem.Size = new System.Drawing.Size(173, 26);
            this.purgeNewsToolStripMenuItem.Text = "Refresh News";
            // 
            // purgeNewsNowToolStripMenuItem
            // 
            this.purgeNewsNowToolStripMenuItem.Name = "purgeNewsNowToolStripMenuItem";
            this.purgeNewsNowToolStripMenuItem.Size = new System.Drawing.Size(238, 26);
            this.purgeNewsNowToolStripMenuItem.Text = "Refresh News Now";
            this.purgeNewsNowToolStripMenuItem.Click += new System.EventHandler(this.purgeNewsNowToolStripMenuItem_Click);
            // 
            // configureAutoPurgeToolStripMenuItem1
            // 
            this.configureAutoPurgeToolStripMenuItem1.Name = "configureAutoPurgeToolStripMenuItem1";
            this.configureAutoPurgeToolStripMenuItem1.Size = new System.Drawing.Size(238, 26);
            this.configureAutoPurgeToolStripMenuItem1.Text = "Configure Auto Refresh";
            this.configureAutoPurgeToolStripMenuItem1.Click += new System.EventHandler(this.configureAutoPurgeToolStripMenuItem1_Click);
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purgeOrdersInDiffusionToolStripMenuItem,
            this.purgeProductOrdersToolStripMenuItem});
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.ordersToolStripMenuItem.Text = "Orders";
            // 
            // purgeOrdersInDiffusionToolStripMenuItem
            // 
            this.purgeOrdersInDiffusionToolStripMenuItem.Name = "purgeOrdersInDiffusionToolStripMenuItem";
            this.purgeOrdersInDiffusionToolStripMenuItem.Size = new System.Drawing.Size(250, 26);
            this.purgeOrdersInDiffusionToolStripMenuItem.Text = "Purge Orders In Diffusion";
            this.purgeOrdersInDiffusionToolStripMenuItem.Click += new System.EventHandler(this.purgeOrdersInDiffusionToolStripMenuItem_Click);
            // 
            // purgeProductOrdersToolStripMenuItem
            // 
            this.purgeProductOrdersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureToolStripMenuItem,
            this.nowToolStripMenuItem});
            this.purgeProductOrdersToolStripMenuItem.Enabled = false;
            this.purgeProductOrdersToolStripMenuItem.Name = "purgeProductOrdersToolStripMenuItem";
            this.purgeProductOrdersToolStripMenuItem.Size = new System.Drawing.Size(250, 26);
            this.purgeProductOrdersToolStripMenuItem.Text = "Purge Product Orders";
            // 
            // configureToolStripMenuItem
            // 
            this.configureToolStripMenuItem.Name = "configureToolStripMenuItem";
            this.configureToolStripMenuItem.Size = new System.Drawing.Size(260, 26);
            this.configureToolStripMenuItem.Text = "Configure Auto Purge";
            this.configureToolStripMenuItem.Click += new System.EventHandler(this.configureToolStripMenuItem_Click);
            // 
            // nowToolStripMenuItem
            // 
            this.nowToolStripMenuItem.Name = "nowToolStripMenuItem";
            this.nowToolStripMenuItem.Size = new System.Drawing.Size(260, 26);
            this.nowToolStripMenuItem.Text = "Purge Product Orders Now";
            this.nowToolStripMenuItem.Click += new System.EventHandler(this.nowToolStripMenuItem_Click);
            // 
            // marketTIckerToolStripMenuItem
            // 
            this.marketTIckerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purgeMarketInfoToolStripMenuItem});
            this.marketTIckerToolStripMenuItem.Name = "marketTIckerToolStripMenuItem";
            this.marketTIckerToolStripMenuItem.Size = new System.Drawing.Size(110, 24);
            this.marketTIckerToolStripMenuItem.Text = "Market Ticker";
            // 
            // purgeMarketInfoToolStripMenuItem
            // 
            this.purgeMarketInfoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purgeMarketInfoNowToolStripMenuItem,
            this.configureAutoPurgeToolStripMenuItem});
            this.purgeMarketInfoToolStripMenuItem.Name = "purgeMarketInfoToolStripMenuItem";
            this.purgeMarketInfoToolStripMenuItem.Size = new System.Drawing.Size(215, 26);
            this.purgeMarketInfoToolStripMenuItem.Text = "Purge Market Ticker";
            // 
            // purgeMarketInfoNowToolStripMenuItem
            // 
            this.purgeMarketInfoNowToolStripMenuItem.Name = "purgeMarketInfoNowToolStripMenuItem";
            this.purgeMarketInfoNowToolStripMenuItem.Size = new System.Drawing.Size(261, 26);
            this.purgeMarketInfoNowToolStripMenuItem.Text = "Refresh Market Ticker Now";
            this.purgeMarketInfoNowToolStripMenuItem.Click += new System.EventHandler(this.purgeMarketInfoNowToolStripMenuItem_Click);
            // 
            // configureAutoPurgeToolStripMenuItem
            // 
            this.configureAutoPurgeToolStripMenuItem.Name = "configureAutoPurgeToolStripMenuItem";
            this.configureAutoPurgeToolStripMenuItem.Size = new System.Drawing.Size(261, 26);
            this.configureAutoPurgeToolStripMenuItem.Text = "Configure Auto Refresh";
            this.configureAutoPurgeToolStripMenuItem.Click += new System.EventHandler(this.configureAutoPurgeToolStripMenuItem_Click_1);
            // 
            // diffusionToolStripMenuItem
            // 
            this.diffusionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteDiffusionTopicToolStripMenuItem,
            this.monitorDiffusionTopicsToolStripMenuItem});
            this.diffusionToolStripMenuItem.Name = "diffusionToolStripMenuItem";
            this.diffusionToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.diffusionToolStripMenuItem.Text = "Diffusion";
            // 
            // deleteDiffusionTopicToolStripMenuItem
            // 
            this.deleteDiffusionTopicToolStripMenuItem.Name = "deleteDiffusionTopicToolStripMenuItem";
            this.deleteDiffusionTopicToolStripMenuItem.Size = new System.Drawing.Size(247, 26);
            this.deleteDiffusionTopicToolStripMenuItem.Text = "Delete Diffusion Topic";
            this.deleteDiffusionTopicToolStripMenuItem.Click += new System.EventHandler(this.deleteDiffusionTopicToolStripMenuItem_Click);
            // 
            // csvToolStripMenuItem
            // 
            this.csvToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importOrganisationsToolStripMenuItem,
            this.importOrganisationProductsToolStripMenuItem1,
            this.importUsersToolStripMenuItem,
            this.importUserRolesToolStripMenuItem,
            this.importCounterpartyPermissionsToolStripMenuItem,
            this.importBrokerPermissionsToolStripMenuItem});
            this.csvToolStripMenuItem.Name = "csvToolStripMenuItem";
            this.csvToolStripMenuItem.Size = new System.Drawing.Size(43, 24);
            this.csvToolStripMenuItem.Text = "Csv";
            // 
            // importOrganisationsToolStripMenuItem
            // 
            this.importOrganisationsToolStripMenuItem.Name = "importOrganisationsToolStripMenuItem";
            this.importOrganisationsToolStripMenuItem.Size = new System.Drawing.Size(299, 26);
            this.importOrganisationsToolStripMenuItem.Text = "Import Organisations";
            this.importOrganisationsToolStripMenuItem.Click += new System.EventHandler(this.importOrganisationsToolStripMenuItem_Click);
            // 
            // importOrganisationProductsToolStripMenuItem1
            // 
            this.importOrganisationProductsToolStripMenuItem1.Name = "importOrganisationProductsToolStripMenuItem1";
            this.importOrganisationProductsToolStripMenuItem1.Size = new System.Drawing.Size(299, 26);
            this.importOrganisationProductsToolStripMenuItem1.Text = "Import Organisation Products";
            this.importOrganisationProductsToolStripMenuItem1.Click += new System.EventHandler(this.importOrganisationProductsToolStripMenuItem_Click);
            // 
            // importUsersToolStripMenuItem
            // 
            this.importUsersToolStripMenuItem.Name = "importUsersToolStripMenuItem";
            this.importUsersToolStripMenuItem.Size = new System.Drawing.Size(299, 26);
            this.importUsersToolStripMenuItem.Text = "Import Users";
            this.importUsersToolStripMenuItem.Click += new System.EventHandler(this.importUsersToolStripMenuItem_Click);
            // 
            // importUserRolesToolStripMenuItem
            // 
            this.importUserRolesToolStripMenuItem.Name = "importUserRolesToolStripMenuItem";
            this.importUserRolesToolStripMenuItem.Size = new System.Drawing.Size(299, 26);
            this.importUserRolesToolStripMenuItem.Text = "Import UserRoles";
            this.importUserRolesToolStripMenuItem.Click += new System.EventHandler(this.importUserRolesToolStripMenuItem_Click);
            // 
            // importCounterpartyPermissionsToolStripMenuItem
            // 
            this.importCounterpartyPermissionsToolStripMenuItem.Name = "importCounterpartyPermissionsToolStripMenuItem";
            this.importCounterpartyPermissionsToolStripMenuItem.Size = new System.Drawing.Size(299, 26);
            this.importCounterpartyPermissionsToolStripMenuItem.Text = "Import Counterparty Permissions";
            this.importCounterpartyPermissionsToolStripMenuItem.Click += new System.EventHandler(this.importCounterpartyPermissionsToolStripMenuItem_Click);
            // 
            // importBrokerPermissionsToolStripMenuItem
            // 
            this.importBrokerPermissionsToolStripMenuItem.Name = "importBrokerPermissionsToolStripMenuItem";
            this.importBrokerPermissionsToolStripMenuItem.Size = new System.Drawing.Size(299, 26);
            this.importBrokerPermissionsToolStripMenuItem.Text = "Import Broker Permissions";
            this.importBrokerPermissionsToolStripMenuItem.Click += new System.EventHandler(this.importBrokerPermissionsToolStripMenuItem_Click);
            // 
            // importOrganisationProductsToolStripMenuItem
            // 
            this.importOrganisationProductsToolStripMenuItem.Name = "importOrganisationProductsToolStripMenuItem";
            this.importOrganisationProductsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.importOrganisationProductsToolStripMenuItem.Text = "Import Organisations";
            this.importOrganisationProductsToolStripMenuItem.Click += new System.EventHandler(this.importOrganisationProductsToolStripMenuItem_Click);
            // 
            // monitorDiffusionTopicsToolStripMenuItem
            // 
            this.monitorDiffusionTopicsToolStripMenuItem.Name = "monitorDiffusionTopicsToolStripMenuItem";
            this.monitorDiffusionTopicsToolStripMenuItem.Size = new System.Drawing.Size(247, 26);
            this.monitorDiffusionTopicsToolStripMenuItem.Text = "Monitor Diffusion Topics";
            this.monitorDiffusionTopicsToolStripMenuItem.Click += new System.EventHandler(this.monitorDiffusionTopicsToolStripMenuItem_Click);
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::AOMAdmin.Properties.Resources.login_background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1344, 898);
            this.Controls.Add(this.mnuMenu);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnuMenu;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(846, 579);
            this.Name = "Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AOM Admin";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Admin_FormClosed);
            this.Load += new System.EventHandler(this.Admin_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.mnuMenu.ResumeLayout(false);
            this.mnuMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableAccountToolStripMenuItem;
        private System.Windows.Forms.MenuStrip mnuMenu;
        protected System.Windows.Forms.ToolStripMenuItem mnuUser;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem organisationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem argusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageOrganisationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purgeNewsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purgeNewsNowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureAutoPurgeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem manageProductsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purgeOrdersInDiffusionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purgeProductOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productMarketOpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureAutoOpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productMarketCloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureAutoCloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marketTIckerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purgeMarketInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purgeMarketInfoNowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureAutoPurgeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diffusionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteDiffusionTopicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createMultipleOrganisationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aOMMonitorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem csvToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importOrganisationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importOrganisationProductsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importUserRolesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importOrganisationProductsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem importCounterpartyPermissionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importBrokerPermissionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monitorDiffusionTopicsToolStripMenuItem;
    }
}