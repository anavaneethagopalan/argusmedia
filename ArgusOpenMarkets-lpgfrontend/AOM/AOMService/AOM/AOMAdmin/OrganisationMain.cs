﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.CrmService;
using AOM.Services.EncryptionService;
using AOM.Services.ProductService;

using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class OrganisationMain : Form
    {
        private readonly IBus _bus;
        private readonly IOrganisationService _organisationService;
        private readonly IEncryptionService _encryptionService;
        private readonly ICrmAdministrationService _crmAdministrationService;
        private readonly IUserService _userService;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly ICredentialsService _credentialsService;
        private readonly IProductService _productService;
        private IList<Organisation> _organisations;

        public OrganisationMain(IBus bus, IOrganisationService organisationService, ICrmAdministrationService crmAdministrationService, IUserService userService,
            IDateTimeProvider dateTimeProvider, IEncryptionService encryptionService, ICredentialsService credentialsService, IProductService productService)
        {
            _bus = bus;
            _organisationService = organisationService;
            _crmAdministrationService = crmAdministrationService;
            _userService = userService;
            _dateTimeProvider = dateTimeProvider;
            _encryptionService = encryptionService;
            _credentialsService = credentialsService;
            _productService = productService;

            InitializeComponent();

        }

        private void ManageOrganisations_Load(object sender, EventArgs e)
        {
            RefreshOrganisations();
        }

        private void RefreshOrganisations(Organisation selectedOrganisation = null)
        {

            _organisations = _organisationService.GetOrganisations().OrderBy(o => o.Name).ToList();

            dgOrganisations.Rows.Clear();
            dgOrganisations.Refresh();
            for (var ix = 0; ix < _organisations.Count; ix++)
            {
                var org = _organisations[ix];
                dgOrganisations.Rows.Add(ix, org.ShortCode, org.Name, org.Email, org.OrganisationType.ToString(), org.IsDeleted, "Details...", "Users...", "Roles...");

                if (selectedOrganisation != null)
                {
                    if (org.Id == selectedOrganisation.Id)
                    {
                        dgOrganisations.Rows[ix].Selected = true;
                    }
                    else
                    {
                        dgOrganisations.Rows[ix].Selected = false;
                    }
                }
            }
        }

        private void _cmdRefresh_Click(object sender, EventArgs e)
        {
            var orgSelected = GetOrganisationSelected();

            RefreshOrganisations(orgSelected);
        }

        private void dgOrganisations_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var org = _organisations[dgOrganisations.Rows[e.RowIndex].Cells[0].Value as int? ?? 0];

                switch (e.ColumnIndex)
                {
                    case 6:
                        ShowDetails(org);
                        break;
                    case 7:
                        ShowUsers(org);
                        break;
                    case 8:
                        ShowRoles(org);
                        break;
                    default:
                        MessageBox.Show("Bug in code - unexpected button index.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        break;
                }
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmdAddOrganisation_Click(object sender, EventArgs e)
        {
            new OrganisationCreate(_bus,  _crmAdministrationService, _organisationService, _userService, _dateTimeProvider, new EncryptionService(), _credentialsService, _productService)
            {
                StartPosition = FormStartPosition.CenterParent
            }.ShowDialog();
        }

        private Organisation GetOrganisationSelected()
        {
            Organisation orgSelected = null;

            try
            {

                var senderGrid = (DataGridView) dgOrganisations;

                var rowIndex = senderGrid.SelectedRows[0];
                if (rowIndex != null)
                {
                    var index = rowIndex.Index;
                    orgSelected = _organisations[dgOrganisations.Rows[index].Cells[0].Value as int? ?? 0];
                }

            }
            catch (Exception)
            {

                throw;
            }

            return orgSelected;
        }

        private void ShowDetails(Organisation org)
        {
            new OrganisationEditDetails( _bus, _crmAdministrationService, _organisationService, _userService, _dateTimeProvider, _credentialsService, org)
            {
                StartPosition = FormStartPosition.CenterParent
            }.ShowDialog();
        }

        private void ShowUsers(Organisation org)
        {
            new UserMain(_crmAdministrationService, _bus, _userService, _organisationService, _encryptionService, _credentialsService, org)
            {
                StartPosition = FormStartPosition.CenterParent
            }.ShowDialog();
        }

        private void ShowRoles(Organisation org)
        {
            new OrganisationRoles(_crmAdministrationService, _bus, _organisationService, org.Id)
                {
                    StartPosition = FormStartPosition.CenterParent
                }.ShowDialog();
        }

    }
}