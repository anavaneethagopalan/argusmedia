﻿using System;
using System.Threading;
using System.Windows.Forms;
using AOM.App.Domain.Dates;
using AOM.Repository.MySql;
using AOM.Services;
using AOM.Services.ProductService;
using Argus.Transport.Infrastructure;
using Ninject;

namespace AOMAdmin
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new ThreadExceptionHandler().ApplicationThreadException;

            var login = new Login();
            login.ShowDialog();

            if (!AdminSettings.LoggedOn)
                Environment.Exit(0);

            using (var kernel = new StandardKernel())
            {
                kernel.Load(new EFModelBootstrapper(), new ServiceBootstrap(), new LocalBootstrapper());                

                var bus = kernel.Get<IBus>();
                var productService = kernel.Get<IProductService>();
                Application.Run(new Admin(bus, kernel, new DateTimeProvider(), productService));
            }
        }
    }
    internal class ThreadExceptionHandler
    {
        public void ApplicationThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ExceptionReporter.ShowException(e.Exception);
        }
    }
}
