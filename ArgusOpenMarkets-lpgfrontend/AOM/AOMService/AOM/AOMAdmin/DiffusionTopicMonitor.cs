﻿using System;
using System.Windows.Forms;
using System.Xml.Schema;
using AOM.App.Domain.Entities;
using AOM.RollProductDeliveryPeriods;
using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Services.EmailService;
using Argus.Transport.Infrastructure;
using PushTechnology.ClientInterface.Client.Features;
using Utils.Logging.Utils;

namespace AOMAdmin
{
    public partial class DiffusionTopicMonitor : Form
    {
        private readonly IBus _bus;
        private readonly ISmtpService _smtpService;

        private IDiffusionSessionConnection _diffusionAConnection;
        private IDiffusionSessionConnection _diffusionBConnection;

        private ITopicWatcher _fetchBCallback;
        private ITopicWatcher _fetchACallback;

        public DiffusionTopicMonitor(IBus bus, ISmtpService smtpService)
        {
            _bus = bus;
            _smtpService = smtpService;
            InitializeComponent();

            _diffusionAConnection = new DiffusionSessionConnection();
            _diffusionBConnection = new DiffusionSessionConnection();

            _fetchACallback = new FetchCallback("A", this.lstDiffusionA, this.lblA);
            _fetchBCallback = new FetchCallback("B", this.lstDiffusionB, this.lblB);
        }

        private void DiffusionTopicMonitor_Load(object sender, System.EventArgs e)
        {

        }

        private void cmdConnectToDiffAAndB_Click(object sender, EventArgs e)
        {
            var diffusionA = this.txtDiffusionA.Text;
            _diffusionAConnection.ConnectAndStartSession(diffusionA);
            var diffusionB = this.txtDiffusionB.Text;
            _diffusionBConnection.ConnectAndStartSession(diffusionB);

            this.timer1.Interval = 3000;
            this.timer1.Enabled = true;
            timer1.Start();

            SendEmailDiffusionTopicManagerStarted();
        }

        private void SendEmailDiffusionTopicManagerStarted()
        {
            var body =
                string.Format(
                    "The Diffusion Topic Manager has started.  We are monitoring Diffusion A:{0}  Diffusion B:{1}",
                    txtDiffusionA.Text, txtDiffusionB.Text);
            Email emailToSend = new Email
            {
                Body = body,
                Recipient = "aomdevteam@argusmedia.com",
                Subject = "Diffusion Topic Manager is running",
                Status = EmailStatus.Pending
            };

            _smtpService.SendEmail(emailToSend);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.lstDiffusionA.Items.Count != this.lstDiffusionA.Items.Count)
            {
                string body =
                    string.Format(
                        "The topic counts for orders in LIVE do not match.  Diffusion A Count: {0}   Diffusion B Count: {1}",
                        _fetchACallback.NumberTopics, _fetchBCallback.NumberTopics);
                ;
                ;

                Email emailToSend = new Email
                {
                    Body = body,
                    Recipient = "aomdevteam@argusmedia.com",
                    Subject = "LIVE Diffusion Topics counts for orders do not match",
                    Status = EmailStatus.Pending
                };

                _smtpService.SendEmail(emailToSend);
            }

            FetchDiffusionTopics();
        }

        private void FetchDiffusionTopics()
        {

            if (this.lstDiffusionA.InvokeRequired)
            {
                lstDiffusionA.BeginInvoke(
                    (MethodInvoker)delegate
                    {
                        lstDiffusionA.Items.Clear();
                    });
            }
            else
            {
                lstDiffusionA.Items.Clear();
            }
            _fetchACallback.NumberTopics = 0;

            if (_diffusionAConnection != null)
            {
                var session = _diffusionAConnection.Session;
                var topics = session.GetTopicsFeature();

                topics.Fetch("?AOM/Orders//", _fetchACallback);
            }


            if (this.lstDiffusionB.InvokeRequired)
            {
                lstDiffusionB.BeginInvoke(
                    (MethodInvoker)delegate
                    {
                        lstDiffusionB.Items.Clear();
                    });
            }
            else
            {
                lstDiffusionB.Items.Clear();
            }
            _fetchBCallback.NumberTopics = 0;

            if (_diffusionBConnection != null)
            {
                var session = _diffusionBConnection.Session;
                if (session != null)
                {
                    var topics = session.GetTopicsFeature();

                    topics.Fetch("?AOM/Orders//", _fetchBCallback);
                }
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }

    public class FetchCallback : ITopicWatcher
    {
        private string _diffusionServer;
        private ListBox _list;
        private Label _label;

        public long NumberTopics { get; set; }

        public FetchCallback(string diffusionServer, ListBox list, Label label)
        {
            _diffusionServer = diffusionServer;
            _list = list;
            _label = label;
        }

        public void OnFetchReply(string topicPath, PushTechnology.ClientInterface.Client.Content.IContent content)
        {
            NumberTopics++;

            var dataMessage = content.AsString();
            if (!string.IsNullOrEmpty(dataMessage))
            {
                string data = _diffusionServer + "-" + dataMessage;


                if (_list.InvokeRequired)
                {
                    _list.BeginInvoke(
                        (MethodInvoker) delegate
                        {
                            _list.Items.Add(data);
                            _list.Refresh();
                        });
                }
                else
                {
                    _list.Items.Add(data);
                    _list.Refresh();
                }

                if (_label.InvokeRequired)
                {
                    _label.BeginInvoke(
                        (MethodInvoker) delegate
                        {
                            _label.Text = "Count:" + NumberTopics;
                            _label.Refresh();
                        });
                }
                else
                {
                    _label.Text = "Count:" + NumberTopics;
                    _label.Refresh();
                }
            }
        }

        public void OnClose()
        {
        }

        public void OnDiscard()
        {
        }
    }

    public interface ITopicWatcher : IFetchStream
    {
        long NumberTopics { get; set; }
    }
}
