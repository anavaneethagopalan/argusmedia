﻿using System;
using System.Windows.Forms;
using PushTechnology.ClientInterface.Client.Session;

namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    public interface IDiffusionSessionConnection : IDisposable
    {
        bool ConnectAndStartSession(string diffusionEndPoint);

        void Disconnect();

        ISession Session { get; }
    }
}