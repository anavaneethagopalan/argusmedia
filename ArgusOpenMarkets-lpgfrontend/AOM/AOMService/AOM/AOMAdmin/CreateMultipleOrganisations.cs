﻿using AOM.Services.ProductService;
using Moq;

namespace AOMAdmin
{
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql;
    using AOM.Services.CrmService;
    using System;
    using System.Windows.Forms;
    using AOM.Services.EncryptionService;

    public partial class CreateMultipleOrganisations : Form
    {
        private ICrmAdministrationService _crmAdministrationService;

        private IEncryptionService _encrpytionService;
        private IUserService _userService;
        private ICredentialsService _credentialsService;

        public CreateMultipleOrganisations()
        {
            InitializeComponent();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            var dbContextFactory = new DbContextFactory();

            _crmAdministrationService = new CrmAdministrationService(dbContextFactory, new DateTimeProvider());
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());

            _userService = new UserService(dbContextFactory, new DateTimeProvider(), new Mock<IProductService>().Object);
            _encrpytionService = new EncryptionService();
            _credentialsService = new CredentialsService(dbContextFactory, new DateTimeProvider());

            long processorUserId = -1;
            var numberOrganisationToCreate = 10;
            var numOrgs = txtNumberOrganisationsToCreate.Text;
            int.TryParse(numOrgs, out numberOrganisationToCreate);

            var shortCode = txtShortCode.Text;
            if (shortCode.Length > 2)
            {
                shortCode = shortCode.Substring(0, 2);
            }

            for (int i = 0; i < numberOrganisationToCreate; i++)
            {

                OrganisationType orgType = OrganisationType.NotSpecified;
                switch (cboOrganisationType.SelectedItem.ToString())
                {
                    case "A":
                        orgType = OrganisationType.Argus;
                        break;
                    case "B":
                        orgType = OrganisationType.Brokerage;
                        break;
                    case "T":
                        orgType = OrganisationType.Trading;
                        break;

                }

                var organisation = new Organisation
                {
                    Address = MakeUniqueValue(txtAddress.Text, i),
                    DateCreated = DateTime.Now,
                    Description = MakeUniqueValue(txtDescription.Text, i),
                    Email = txtEmail.Text,
                    IsDeleted = false,
                    LegalName = MakeUniqueValue(txtOrganisationLegalName.Text, i),
                    Name = MakeUniqueValue(txtOrganisationName.Text, i),
                    OrganisationType = orgType,
                    ShortCode = MakeUniqueValue(shortCode, i)

                };

                long organisationId = _crmAdministrationService.SaveOrganisation(organisation, processorUserId,
                    organisationService);

                if (organisationId > 0)
                {
                    // We have managed to save the organisation.
                    if (chkCreateUsers.Checked)
                    {
                        // We need to save the user details against the newly created organisation.
                        CreateUsers(organisationId);
                    }
                }
            }
        }

        public bool CreateUsers(long organisationId)
        {
            if (organisationId > 0)
            {
                // We can create users against the newly created organisation.
                var password = txtPassword.Text;

                var user = new User
                {
                    Email = txtEmail.Text,
                    IsActive = true,
                    Name = txtName.Text,
                    Username = txtUsername.Text,
                    ArgusCrmUsername = ArgusCrmUsernameTB.Text,
                    OrganisationId = organisationId,
                    Title = txtTitle.Text,
                    Telephone = txtTelephone.Text,
                    IsBlocked = false,
                    IsDeleted = false,
                };

                var userCredentials = new UserCredentials
                {
                    DateCreated = DateTime.UtcNow,
                    PasswordHashed = password,
                    DefaultExpirationInMonths = 12,
                    Expiration = DateTime.UtcNow.AddMonths(100),
                    FirstTimeLogin = false,
                    User = user,
                };

                try
                {
                    var result = _crmAdministrationService.CreateUserWithCredentials(
                        user,
                        userCredentials,
                        AdminSettings.LoggedOnUser.Id);

                    if (result)
                    {
                        user.Id = _userService.GetUserId(user.Username);
                        password = _encrpytionService.Hash(user, password);

                        _credentialsService.UpdateUserCredentialsAdmin(user.Id, password, password, false, true, -1, false);
                    }
                }
                catch (Exception ex)
                {
                    ExceptionReporter.ShowException(ex, "Error while adding user");
                }


            }

            return true;
        }

        private string MakeUniqueValue(string value, int counter)
        {
            return value + counter.ToString();
        }

        private void CreateMultipleOrganisations_Load(object sender, EventArgs e)
        {
            cboOrganisationType.Items.Add("A");
            cboOrganisationType.Items.Add("B");
            cboOrganisationType.Items.Add("T");
            cboOrganisationType.SelectedIndex = 0;
        }
    }
}
