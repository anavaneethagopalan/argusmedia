﻿namespace AOMAdmin
{
    partial class ProductDeals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._ConfigurationsDataGridView = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AllowNegativePriceForExternalDealsColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DisplayOptionalPriceDetailColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._CloseButton = new System.Windows.Forms.Button();
            this._RefreshButton = new System.Windows.Forms.Button();
            this._SaveButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._ConfigurationsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _ConfigurationsDataGridView
            // 
            this._ConfigurationsDataGridView.AllowUserToAddRows = false;
            this._ConfigurationsDataGridView.AllowUserToDeleteRows = false;
            this._ConfigurationsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ConfigurationsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._ConfigurationsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.AllowNegativePriceForExternalDealsColumn,
            this.DisplayOptionalPriceDetailColumn});
            this._ConfigurationsDataGridView.Location = new System.Drawing.Point(12, 12);
            this._ConfigurationsDataGridView.Name = "_ConfigurationsDataGridView";
            this._ConfigurationsDataGridView.Size = new System.Drawing.Size(339, 60);
            this._ConfigurationsDataGridView.TabIndex = 1;
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "Id";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.ReadOnly = true;
            this.IdColumn.Visible = false;
            this.IdColumn.Width = 35;
            // 
            // AllowNegativePriceForExternalDealsColumn
            // 
            this.AllowNegativePriceForExternalDealsColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AllowNegativePriceForExternalDealsColumn.DataPropertyName = "AllowNegativePriceForExternalDeals";
            this.AllowNegativePriceForExternalDealsColumn.HeaderText = "Allow Negative Price For External Deals";
            this.AllowNegativePriceForExternalDealsColumn.Name = "AllowNegativePriceForExternalDealsColumn";
            // 
            // DisplayOptionalPriceDetailColumn
            // 
            this.DisplayOptionalPriceDetailColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DisplayOptionalPriceDetailColumn.DataPropertyName = "DisplayOptionalPriceDetail";
            this.DisplayOptionalPriceDetailColumn.HeaderText = "Display Optional Price Detail";
            this.DisplayOptionalPriceDetailColumn.Name = "DisplayOptionalPriceDetailColumn";
            // 
            // _CloseButton
            // 
            this._CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._CloseButton.Location = new System.Drawing.Point(257, 87);
            this._CloseButton.Name = "_CloseButton";
            this._CloseButton.Size = new System.Drawing.Size(94, 23);
            this._CloseButton.TabIndex = 3;
            this._CloseButton.Text = "&Close";
            this._CloseButton.UseVisualStyleBackColor = true;
            this._CloseButton.Click += new System.EventHandler(this._CloseButton_Click);
            // 
            // _RefreshButton
            // 
            this._RefreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._RefreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._RefreshButton.Location = new System.Drawing.Point(12, 87);
            this._RefreshButton.Name = "_RefreshButton";
            this._RefreshButton.Size = new System.Drawing.Size(75, 23);
            this._RefreshButton.TabIndex = 4;
            this._RefreshButton.Text = "Refresh";
            this._RefreshButton.UseVisualStyleBackColor = true;
            this._RefreshButton.Click += new System.EventHandler(this._RefreshButton_Click);
            // 
            // _SaveButton
            // 
            this._SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._SaveButton.Location = new System.Drawing.Point(93, 87);
            this._SaveButton.Name = "_SaveButton";
            this._SaveButton.Size = new System.Drawing.Size(75, 23);
            this._SaveButton.TabIndex = 5;
            this._SaveButton.Text = "Save Changes";
            this._SaveButton.UseVisualStyleBackColor = true;
            this._SaveButton.Click += new System.EventHandler(this._SaveButton_Click);
            // 
            // ter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 122);
            this.Controls.Add(this._RefreshButton);
            this.Controls.Add(this._SaveButton);
            this.Controls.Add(this._CloseButton);
            this.Controls.Add(this._ConfigurationsDataGridView);
            this.Name = "ter";
            this.Text = "Product Deal Configuration";
            this.Load += new System.EventHandler(this.ProductDeals_Load);
            ((System.ComponentModel.ISupportInitialize)(this._ConfigurationsDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView _ConfigurationsDataGridView;
        private System.Windows.Forms.Button _CloseButton;
        private System.Windows.Forms.Button _RefreshButton;
        private System.Windows.Forms.Button _SaveButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AllowNegativePriceForExternalDealsColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DisplayOptionalPriceDetailColumn;
    }
}