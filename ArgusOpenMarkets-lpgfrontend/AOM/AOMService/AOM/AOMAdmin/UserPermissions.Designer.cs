﻿namespace AOMAdmin
{
    partial class UserPermissions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserPermissions));
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserToFind = new System.Windows.Forms.TextBox();
            this.cmdFindUser = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserFound = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lstRoles = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOrganisation = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(11, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "User to find:";
            // 
            // txtUserToFind
            // 
            this.txtUserToFind.Location = new System.Drawing.Point(82, 9);
            this.txtUserToFind.Name = "txtUserToFind";
            this.txtUserToFind.Size = new System.Drawing.Size(240, 20);
            this.txtUserToFind.TabIndex = 1;
            this.txtUserToFind.TextChanged += new System.EventHandler(this.txtUserToFind_TextChanged);
            // 
            // cmdFindUser
            // 
            this.cmdFindUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindUser.Location = new System.Drawing.Point(328, 7);
            this.cmdFindUser.Name = "cmdFindUser";
            this.cmdFindUser.Size = new System.Drawing.Size(75, 23);
            this.cmdFindUser.TabIndex = 2;
            this.cmdFindUser.Text = "Find User";
            this.cmdFindUser.UseVisualStyleBackColor = true;
            this.cmdFindUser.Click += new System.EventHandler(this.cmdFindUser_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(11, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Username:";
            // 
            // lblUserFound
            // 
            this.lblUserFound.Location = new System.Drawing.Point(146, 68);
            this.lblUserFound.Name = "lblUserFound";
            this.lblUserFound.Size = new System.Drawing.Size(330, 18);
            this.lblUserFound.TabIndex = 8;
            this.lblUserFound.Text = "<User Found>";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(147, 85);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(329, 18);
            this.lblName.TabIndex = 10;
            this.lblName.Text = "<Name>";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Name:";
            // 
            // lblEmail
            // 
            this.lblEmail.Location = new System.Drawing.Point(147, 102);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(329, 18);
            this.lblEmail.TabIndex = 12;
            this.lblEmail.Text = "<Email>";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Email:";
            // 
            // lstRoles
            // 
            this.lstRoles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstRoles.FormattingEnabled = true;
            this.lstRoles.Location = new System.Drawing.Point(14, 159);
            this.lstRoles.Name = "lstRoles";
            this.lstRoles.Size = new System.Drawing.Size(466, 259);
            this.lstRoles.TabIndex = 3;
            this.lstRoles.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstRoles_ItemCheck);
            this.lstRoles.SelectedIndexChanged += new System.EventHandler(this.lstRoles_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Roles:";
            // 
            // lblOrganisation
            // 
            this.lblOrganisation.Location = new System.Drawing.Point(146, 50);
            this.lblOrganisation.Name = "lblOrganisation";
            this.lblOrganisation.Size = new System.Drawing.Size(330, 18);
            this.lblOrganisation.TabIndex = 6;
            this.lblOrganisation.Text = "<Organisation>";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "Organisation:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Location = new System.Drawing.Point(12, 427);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(297, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "NB: Any changes made to the roles are saved immediately";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(386, 424);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 4;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // UserPermissions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 451);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblOrganisation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lstRoles);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblUserFound);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdFindUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUserToFind);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserPermissions";
            this.Text = "SetUserPermissions";
            this.Load += new System.EventHandler(this.SetUserPermissions_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUserToFind;
        private System.Windows.Forms.Button cmdFindUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserFound;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckedListBox lstRoles;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblOrganisation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button cmdCancel;
    }
}