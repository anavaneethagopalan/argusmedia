﻿namespace AOMAdmin
{
    partial class OrganisationMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrganisationMain));
            this.cmdRefresh = new System.Windows.Forms.Button();
            this.dgOrganisations = new System.Windows.Forms.DataGridView();
            this.colIX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colCmdDetails = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdUsers = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdRoles = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cmdAddOrganisation = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgOrganisations)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRefresh.Location = new System.Drawing.Point(15, 321);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(126, 23);
            this.cmdRefresh.TabIndex = 1;
            this.cmdRefresh.Text = "Refresh Organisations";
            this.cmdRefresh.UseVisualStyleBackColor = true;
            this.cmdRefresh.Click += new System.EventHandler(this._cmdRefresh_Click);
            // 
            // dgOrganisations
            // 
            this.dgOrganisations.AllowUserToAddRows = false;
            this.dgOrganisations.AllowUserToDeleteRows = false;
            this.dgOrganisations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgOrganisations.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgOrganisations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOrganisations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIX,
            this.colCode,
            this.colName,
            this.colEmail,
            this.colType,
            this.colDeleted,
            this.colCmdDetails,
            this.colCmdUsers,
            this.colCmdRoles});
            this.dgOrganisations.Location = new System.Drawing.Point(15, 13);
            this.dgOrganisations.Name = "dgOrganisations";
            this.dgOrganisations.ReadOnly = true;
            this.dgOrganisations.RowHeadersWidth = 20;
            this.dgOrganisations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgOrganisations.Size = new System.Drawing.Size(818, 300);
            this.dgOrganisations.TabIndex = 0;
            this.dgOrganisations.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgOrganisations_CellContentClick);
            // 
            // colIX
            // 
            this.colIX.HeaderText = "-";
            this.colIX.Name = "colIX";
            this.colIX.ReadOnly = true;
            this.colIX.Visible = false;
            // 
            // colCode
            // 
            this.colCode.HeaderText = "Code";
            this.colCode.Name = "colCode";
            this.colCode.ReadOnly = true;
            this.colCode.Width = 50;
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colEmail
            // 
            this.colEmail.HeaderText = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.ReadOnly = true;
            this.colEmail.Width = 200;
            // 
            // colType
            // 
            this.colType.HeaderText = "Type";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Width = 80;
            // 
            // colDeleted
            // 
            this.colDeleted.HeaderText = "Deleted?";
            this.colDeleted.Name = "colDeleted";
            this.colDeleted.ReadOnly = true;
            this.colDeleted.Width = 60;
            // 
            // colCmdDetails
            // 
            this.colCmdDetails.HeaderText = "Details";
            this.colCmdDetails.Name = "colCmdDetails";
            this.colCmdDetails.ReadOnly = true;
            this.colCmdDetails.Width = 60;
            // 
            // colCmdUsers
            // 
            this.colCmdUsers.HeaderText = "Users";
            this.colCmdUsers.Name = "colCmdUsers";
            this.colCmdUsers.ReadOnly = true;
            this.colCmdUsers.Width = 60;
            // 
            // colCmdRoles
            // 
            this.colCmdRoles.HeaderText = "Roles";
            this.colCmdRoles.Name = "colCmdRoles";
            this.colCmdRoles.ReadOnly = true;
            this.colCmdRoles.Width = 60;
            // 
            // cmdAddOrganisation
            // 
            this.cmdAddOrganisation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdAddOrganisation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAddOrganisation.Location = new System.Drawing.Point(147, 321);
            this.cmdAddOrganisation.Name = "cmdAddOrganisation";
            this.cmdAddOrganisation.Size = new System.Drawing.Size(106, 23);
            this.cmdAddOrganisation.TabIndex = 2;
            this.cmdAddOrganisation.Text = "Add Organisation";
            this.cmdAddOrganisation.UseVisualStyleBackColor = true;
            this.cmdAddOrganisation.Click += new System.EventHandler(this.cmdAddOrganisation_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(739, 321);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // OrganisationMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 354);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdAddOrganisation);
            this.Controls.Add(this.dgOrganisations);
            this.Controls.Add(this.cmdRefresh);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrganisationMain";
            this.Text = "Manage Organisations";
            this.Load += new System.EventHandler(this.ManageOrganisations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgOrganisations)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmdRefresh;
        private System.Windows.Forms.DataGridView dgOrganisations;
        private System.Windows.Forms.Button cmdAddOrganisation;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIX;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colDeleted;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdDetails;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdUsers;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdRoles;
    }
}