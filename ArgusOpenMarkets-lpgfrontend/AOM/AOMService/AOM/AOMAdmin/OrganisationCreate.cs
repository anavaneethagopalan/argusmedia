﻿using AOM.Services.CrmService;
using AOM.Services.ProductService;
using Utils.Logging.Utils;

namespace AOMAdmin
{
    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.Organisations;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml;

    using SystemPrivileges = AOM.Services.AuthenticationService.SystemPrivileges;

    public partial class OrganisationCreate : Form
    {
        private readonly IBus _bus;

        private readonly ICrmAdministrationService _crmService;

        private readonly OrganisationAdminEntities _entities;

        private XmlDocument _defaultRoles;

        private readonly IOrganisationService _organisationService;

        private readonly IUserService _userService;

        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IEncryptionService _encryptionService;
        private readonly ICredentialsService _credentialsService;
        private readonly IProductService _productService;

        private IList<ISystemPrivilege> _systemPrivileges;

        public OrganisationCreate(
            IBus bus,
            ICrmAdministrationService crmService,
            IOrganisationService organisationService,
            IUserService userService,
            IDateTimeProvider dateTimeProvider,
            IEncryptionService encryptionService,
            ICredentialsService credentialsService, 
            IProductService productService)
        {
            _bus = bus;
            _crmService = crmService;
            _entities = new OrganisationAdminEntities
            {
                Organisation = new Organisation(),
                ProductsAndPrivileges =
                    new List<OrganisationProductAndPrivileges>()
            };
            _organisationService = organisationService;
            _userService = userService;
            _dateTimeProvider = dateTimeProvider;
            _encryptionService = encryptionService;
            _credentialsService = credentialsService;
            _productService = productService;

            InitializeComponent();
            cmdSave.Enabled = false;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CreateOrganisation_Load(object sender, EventArgs e)
        {
            _defaultRoles = new XmlDocument();
            _defaultRoles.Load("DefaultRoles.xml");
            DisplayOrganisationTypes();
            RefreshProducts();
        }

        private void RefreshProducts()
        {
            try
            {
                dgProducts.Rows.Clear();
                dgProducts.Refresh();

                List<Product> products;
                try
                {
                    products =
                        _bus.Request<GetAllActiveProductsRequestRpc, List<Product>>(new GetAllActiveProductsRequestRpc())
                            .ToList();
                }
                catch (Exception ex)
                {
                    Log.Error("Error getting all products", ex);
                    // Workaround for now - bypass the bus to obtain a list of all product(s).
                    products = _productService.GetAllProducts();
                }

                foreach (var prod in products)
                {
                    var pp = new OrganisationProductAndPrivileges
                    {
                        Product = prod,
                        ProductPrivileges =
                            new List<SubscribedProductPrivilege>(),
                    };

                    _entities.ProductsAndPrivileges.Add(pp);
                    dgProducts.Rows.Add(_entities.ProductsAndPrivileges.Count - 1, prod.Name, false, "Configure...");
                }
                dgProducts.Refresh();
            }
            catch (Exception ex)
            {
                Log.Error("Error refreshing organisations", ex);
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            MapOrganisationFields();
            bool success = false;

            List<SubscribedProduct> subscribedProducts = new List<SubscribedProduct>();

            try
            {
                _organisationService.ValidateOrganisation(_entities.Organisation);
            }
            catch (BusinessRuleException ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "There was a problem when creating the Organisation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            long createdCompanyId = -1;
            try
            {
                // Save Organisation
                createdCompanyId = _crmService.SaveOrganisation(
                    _entities.Organisation,
                    AdminSettings.LoggedOnUser.Id,
                    _organisationService);

            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error Saving Organsation", ex);
            }

            if (createdCompanyId > 0)
            {
                _entities.Organisation.Id = createdCompanyId;
                success = true;
                cmdSave.Enabled = false;
                lblMessage.Text = "Organisation successfully saved.";

                // Save SubscribedProduct and SubscribedProductPrivileges
                foreach (DataGridViewRow productRow in dgProducts.Rows)
                {
                    if ((Boolean) productRow.Cells[2].Value) // i.e. Subscribed is checked
                    {
                        var prodAndPrivs = _entities.ProductsAndPrivileges[productRow.Cells[0].Value as int? ?? 0];

                        subscribedProducts.Add(
                            new SubscribedProduct
                            {
                                IsInternal = prodAndPrivs.Product.IsInternal,
                                Name = prodAndPrivs.Product.Name,
                                ProductId = prodAndPrivs.Product.ProductId
                            });

                        foreach (var spp in prodAndPrivs.ProductPrivileges)
                        {
                            if (success)
                            {
                                success =
                                    _crmService.AddProductPrivilegeToSubscribedProductPrivileges(
                                        _entities.Organisation.Id,
                                        prodAndPrivs.Product.ProductId,
                                        spp.ProductPrivilegeId,
                                        AdminSettings.LoggedOnUser.Id);
                            }
                        }
                    }
                }
            }

            if (success && cbCreateDefaultRoles.Checked)
            {
                // Create and Save OrganisationRoles and RolePrivileges from defaults
                success = AddDefaultRoles();
            }


            if (success)
            {
                PublishCreateOrganisationTopic(_entities.Organisation, subscribedProducts);

                MessageBox.Show(
                    "Organisation successfully added with Id = " + createdCompanyId,
                    "Success",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                if (cbCreateUsers.Checked)
                {
                    var frm = new UserMain(
                        _crmService,
                        _bus,
                        _userService,
                        _organisationService,
                        _encryptionService, 
                        _credentialsService,
                        _entities.Organisation) {StartPosition = FormStartPosition.CenterParent};
                    frm.ShowDialog();
                }
                DisplayOrganisation(null);
                Close();
            }
            else
            {
                MessageBox.Show(
                    "Failed to add Organisation and privileges.  Check error log.",
                    "ERROR",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }

        private void PublishCreateOrganisationTopic(Organisation organisation,
            List<SubscribedProduct> subscribedProducts)
        {
            List<NotificationMapping> systemNotifications = new List<NotificationMapping>();

            // TODO - need to generate a list of public IList<BilateralMatrixPermissionPair> CounterpartyPermissions { get; set; }
            // var counterpartyPermissions = _organisationService.GetAllCounterpartyPermissions();

            var organisationDetails = new OrganisationDetails
            {
                Organisation = organisation,
                SubscribedProducts = subscribedProducts,
                SystemNotifications = systemNotifications,
            };

            var organisationCreatedEvent = new OrganisationCreatedEvent
            {
                OrganisationDetails = organisationDetails,
                UserId = AdminSettings.LoggedOnUser.Id
            };

            try
            {
                _bus.Publish(organisationCreatedEvent);
            }
            catch (Exception ex)
            {
                Log.Error("Eror publishing Organisation Created Event.", ex);
            }
        }

        private void MapOrganisationFields()
        {
            _entities.Organisation = new Organisation
            {
                Address = txtAddress.Text,
                DateCreated = _dateTimeProvider.UtcNow,
                Description = txtDescription.Text,
                Id = -1,
                IsDeleted = cbIsDeleted.Checked,
                LegalName = txtLegalName.Text,
                ShortCode = txtShortCode.Text,
                Name = txtName.Text,
                Email = txtEmail.Text,
                OrganisationType =
                    (cboOrganisationType.SelectedItem as OrganisationComboBox)
                        .OrgType
            };
        }

        private bool AddDefaultRoles()
        {
            //Add in default roles and privileges
            foreach (XmlNode roleNode in _defaultRoles.GetElementsByTagName("role"))
            {
                var orgType = roleNode.Attributes["organisationtype"] != null
                    ? roleNode.Attributes["organisationtype"].Value
                    : "Any";
                var perProduct = roleNode.Attributes["perproduct"].Value.ToLower() != "false";

                // only add default roles with matching org type if specified
                if (orgType == "Any" || orgType == _entities.Organisation.OrganisationType.ToString())
                {
                    if (perProduct)
                    {
                        // Add a role and appropriate privileges for each product.
                        foreach (DataGridViewRow productRow in dgProducts.Rows)
                        {
                            if ((Boolean) productRow.Cells[2].Value) // i.e. Subscribed is checked
                            {
                                var prodAndPrivs =
                                    _entities.ProductsAndPrivileges[productRow.Cells[0].Value as int? ?? 0];

                                var rolePrivileges = new List<ProductRolePrivilege>();

                                long roleId = AddOrganisationRole(
                                    roleNode,
                                    _entities.Organisation,
                                    prodAndPrivs.Product);

                                CreatRolePrivilegesForProduct(prodAndPrivs, roleNode, rolePrivileges, roleId);

                                _crmService.AddProductRolePrivileges(rolePrivileges, AdminSettings.LoggedOnUser.Id);

                                CreateSystemRolePrivileges(roleNode, roleId);
                            }
                        }
                    }
                    else
                    {
                        // Add a single role with appropriate privileges for every product.

                        long roleId = AddOrganisationRole(roleNode, _entities.Organisation, new Product());

                        var rolePrivileges = new List<ProductRolePrivilege>();

                        foreach (DataGridViewRow productRow in dgProducts.Rows)
                        {
                            if ((Boolean) productRow.Cells[2].Value) // i.e. Subscribed is checked
                            {
                                var pp = _entities.ProductsAndPrivileges[productRow.Cells[0].Value as int? ?? 0];
                                CreatRolePrivilegesForProduct(pp, roleNode, rolePrivileges, roleId);
                            }
                        }
                        _crmService.AddProductRolePrivileges(rolePrivileges, AdminSettings.LoggedOnUser.Id);

                        CreateSystemRolePrivileges(roleNode, roleId);
                    }
                }
            }
            return true;
        }

        private void CreatRolePrivilegesForProduct(
            OrganisationProductAndPrivileges pp,
            XmlNode roleNode,
            List<ProductRolePrivilege> rolePrivileges,
            long roleId)
        {
            foreach (var spp in pp.ProductPrivileges)
            {
                foreach (XmlNode rpNode in roleNode.SelectNodes("productprivileges/privilege"))
                {
                    if (String.Equals(
                        rpNode.InnerText,
                        spp.ProductPrivilege.Name,
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        rolePrivileges.Add(
                            new ProductRolePrivilege
                            {
                                Id = -1,
                                OrganisationId = _entities.Organisation.Id,
                                RoleId = roleId,
                                ProductId = spp.ProductId,
                                ProductPrivilegeId = spp.ProductPrivilegeId
                            });
                    }
                }
            }
        }

        private void CreateSystemRolePrivileges(XmlNode roleNode, long roleId)
        {
            foreach (var sp in _systemPrivileges)
            {
                foreach (XmlNode rpNode in roleNode.SelectNodes("systemprivileges/privilege"))
                {
                    if (String.Equals(rpNode.InnerText, sp.Name, StringComparison.CurrentCultureIgnoreCase))
                    {
                        _crmService.AddSystemPrivilegeToRole(
                            new SystemRolePrivilege {Id = -1, RoleId = roleId, PrivilegeId = sp.Id},
                            AdminSettings.LoggedOnUser.Id);
                    }
                }
            }
        }

        private long AddOrganisationRole(XmlNode roleNode, Organisation organisation, Product product)
        {
            var organisationRole = new OrganisationRole
            {
                Id = -1,
                OrganisationId = _entities.Organisation.Id,
                Name =
                    ReplaceRoleStrings(
                        roleNode.SelectSingleNode("name").InnerText,
                        organisation,
                        product),
                Description =
                    ReplaceRoleStrings(
                        roleNode.SelectSingleNode("description").InnerText,
                        organisation,
                        product)
            };
            return _crmService.SaveRole(organisationRole, AdminSettings.LoggedOnUser.Id); // Add the role
        }

        private string ReplaceRoleStrings(string roleString, Organisation org, Product product)
        {
            return roleString.Replace("{organisation}", org.Name).Replace("{product}", product.Name);
        }

        private void DisplayOrganisationTypes()
        {
            var orgComboItem = new OrganisationComboBox(1, "Trading", OrganisationType.Trading);
            cboOrganisationType.Items.Add(orgComboItem);

            orgComboItem = new OrganisationComboBox(2, "Argus", OrganisationType.Argus);
            cboOrganisationType.Items.Add(orgComboItem);

            orgComboItem = new OrganisationComboBox(3, "Brokerage", OrganisationType.Brokerage);
            cboOrganisationType.Items.Add(orgComboItem);

            orgComboItem = new OrganisationComboBox(4, "Not Specified", OrganisationType.NotSpecified);
            cboOrganisationType.Items.Add(orgComboItem);

            cboOrganisationType.SelectedIndex = 0;
        }

        private void DisplayOrganisation(IOrganisation organisation)
        {
            if (organisation != null)
            {
                txtId.Text = organisation.Id.ToString(CultureInfo.InvariantCulture);
                txtAddress.Text = organisation.Address;
                txtDescription.Text = organisation.Description;
                cbIsDeleted.Checked = organisation.IsDeleted;
                txtShortCode.Text = organisation.ShortCode;
                txtLegalName.Text = organisation.LegalName;
                txtName.Text = organisation.Name;
                txtEmail.Text = organisation.Email;
                DisplayOrganisationType(organisation.OrganisationType);
                cmdSave.Text = "Update";
            }
            else
            {
                txtId.Text = "-1";
                txtAddress.Text = "";
                txtDescription.Text = "";
                cbIsDeleted.Checked = false;
                txtShortCode.Text = "";
                txtLegalName.Text = "";
                txtEmail.Text = "";
                Name = txtName.Text = "";
                cmdSave.Text = "Create";
            }
        }

        private void DisplayOrganisationType(OrganisationType organisationType)
        {
            int index = 0;
            foreach (var orgType in cboOrganisationType.Items)
            {
                var orgCombo = orgType as OrganisationComboBox;
                if (orgCombo.OrgType == organisationType)
                {
                    cboOrganisationType.SelectedIndex = index;
                }

                index = index + 1;
            }
        }

        private void dgProducts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var prodAndPrivs =
                    _entities.ProductsAndPrivileges[dgProducts.Rows[e.RowIndex].Cells[0].Value as int? ?? 0];

                switch (e.ColumnIndex)
                {
                    case 3:
                        ShowProductPrivileges(prodAndPrivs);
                        break;
                    default:
                        MessageBox.Show(
                            "Bug in code - unexpected button index.",
                            "Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button1);
                        break;
                }
            }
        }

        private void ShowProductPrivileges(OrganisationProductAndPrivileges pp)
        {
            MapOrganisationFields();
            var frm = new OrganisationSelectProductPrivileges(
                _crmService,
                _organisationService,
                _bus,
                _entities.Organisation,
                pp,
                new DateTimeProvider()) {StartPosition = FormStartPosition.CenterParent};
            frm.ShowDialog();
        }

        private void txtShortCode_TextChanged(object sender, EventArgs e)
        {
            SetCreateButtonEnabledState();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            SetCreateButtonEnabledState();
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            SetCreateButtonEnabledState();
        }

        private void SetCreateButtonEnabledState()
        {
            MapOrganisationFields();
            cmdSave.Enabled = _organisationService.IsOrganisationValid(_entities.Organisation);
        }

        private void cboOrganisationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Ok - the user has changed the System Privileges - we need to re-display the system privileges
            var orgType = (cboOrganisationType.SelectedItem as OrganisationComboBox).OrgType;

            _systemPrivileges = _crmService.GetSystemPrivileges(orgType);

        }
    }
}