﻿namespace AOMAdmin
{
    partial class SchedulePurgeProductOrdersTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtTaskName = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtpFirstRunTime = new System.Windows.Forms.DateTimePicker();
            this.lblSkipIfMissed = new System.Windows.Forms.Label();
            this.cbSkipIfMissed = new System.Windows.Forms.CheckBox();
            this.lblDaysToKeep = new System.Windows.Forms.Label();
            this.numDaysToKeep = new System.Windows.Forms.NumericUpDown();
            this.lblFirstRun = new System.Windows.Forms.Label();
            this.dtpFirstRun = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTaskFrequency = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dgScheduledTasks = new System.Windows.Forms.DataGridView();
            this.btnDeleteTask = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDaysToKeep)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgScheduledTasks)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTaskName
            // 
            this.txtTaskName.Location = new System.Drawing.Point(191, 7);
            this.txtTaskName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTaskName.Name = "txtTaskName";
            this.txtTaskName.Size = new System.Drawing.Size(267, 22);
            this.txtTaskName.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(16, 15);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(477, 370);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtpFirstRunTime);
            this.tabPage1.Controls.Add(this.lblSkipIfMissed);
            this.tabPage1.Controls.Add(this.cbSkipIfMissed);
            this.tabPage1.Controls.Add(this.lblDaysToKeep);
            this.tabPage1.Controls.Add(this.numDaysToKeep);
            this.tabPage1.Controls.Add(this.lblFirstRun);
            this.tabPage1.Controls.Add(this.dtpFirstRun);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.cmbTaskFrequency);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.btnOK);
            this.tabPage1.Controls.Add(this.txtTaskName);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(469, 341);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "New Purge";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtpFirstRunTime
            // 
            this.dtpFirstRunTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFirstRunTime.Location = new System.Drawing.Point(359, 74);
            this.dtpFirstRunTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpFirstRunTime.Name = "dtpFirstRunTime";
            this.dtpFirstRunTime.ShowUpDown = true;
            this.dtpFirstRunTime.Size = new System.Drawing.Size(97, 22);
            this.dtpFirstRunTime.TabIndex = 14;
            this.dtpFirstRunTime.Value = new System.DateTime(2014, 11, 5, 0, 0, 0, 0);
            // 
            // lblSkipIfMissed
            // 
            this.lblSkipIfMissed.AutoSize = true;
            this.lblSkipIfMissed.Location = new System.Drawing.Point(37, 139);
            this.lblSkipIfMissed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSkipIfMissed.Name = "lblSkipIfMissed";
            this.lblSkipIfMissed.Size = new System.Drawing.Size(143, 17);
            this.lblSkipIfMissed.TabIndex = 13;
            this.lblSkipIfMissed.Text = "Skip if Task is Missed";
            // 
            // cbSkipIfMissed
            // 
            this.cbSkipIfMissed.AutoSize = true;
            this.cbSkipIfMissed.Location = new System.Drawing.Point(191, 139);
            this.cbSkipIfMissed.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbSkipIfMissed.Name = "cbSkipIfMissed";
            this.cbSkipIfMissed.Size = new System.Drawing.Size(18, 17);
            this.cbSkipIfMissed.TabIndex = 12;
            this.cbSkipIfMissed.UseVisualStyleBackColor = true;
            // 
            // lblDaysToKeep
            // 
            this.lblDaysToKeep.AutoSize = true;
            this.lblDaysToKeep.Location = new System.Drawing.Point(112, 108);
            this.lblDaysToKeep.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDaysToKeep.Name = "lblDaysToKeep";
            this.lblDaysToKeep.Size = new System.Drawing.Size(68, 17);
            this.lblDaysToKeep.TabIndex = 11;
            this.lblDaysToKeep.Text = "ProductId";
            // 
            // numDaysToKeep
            // 
            this.numDaysToKeep.Location = new System.Drawing.Point(191, 106);
            this.numDaysToKeep.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numDaysToKeep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDaysToKeep.Name = "numDaysToKeep";
            this.numDaysToKeep.Size = new System.Drawing.Size(267, 22);
            this.numDaysToKeep.TabIndex = 10;
            this.numDaysToKeep.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblFirstRun
            // 
            this.lblFirstRun.AutoSize = true;
            this.lblFirstRun.Location = new System.Drawing.Point(20, 76);
            this.lblFirstRun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFirstRun.Name = "lblFirstRun";
            this.lblFirstRun.Size = new System.Drawing.Size(162, 17);
            this.lblFirstRun.TabIndex = 9;
            this.lblFirstRun.Text = "First Run Date and Time";
            // 
            // dtpFirstRun
            // 
            this.dtpFirstRun.Location = new System.Drawing.Point(191, 73);
            this.dtpFirstRun.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpFirstRun.Name = "dtpFirstRun";
            this.dtpFirstRun.Size = new System.Drawing.Size(159, 22);
            this.dtpFirstRun.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Task Frequency";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Task Name";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cmbTaskFrequency
            // 
            this.cmbTaskFrequency.FormattingEnabled = true;
            this.cmbTaskFrequency.Location = new System.Drawing.Point(191, 39);
            this.cmbTaskFrequency.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbTaskFrequency.Name = "cmbTaskFrequency";
            this.cmbTaskFrequency.Size = new System.Drawing.Size(265, 24);
            this.cmbTaskFrequency.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(357, 306);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 4;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(249, 306);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "Create";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnCancel);
            this.tabPage2.Controls.Add(this.dgScheduledTasks);
            this.tabPage2.Controls.Add(this.btnDeleteTask);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Size = new System.Drawing.Size(469, 341);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Existing Purges";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(251, 303);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dgScheduledTasks
            // 
            this.dgScheduledTasks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Format = "G";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgScheduledTasks.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgScheduledTasks.Location = new System.Drawing.Point(8, 7);
            this.dgScheduledTasks.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgScheduledTasks.Name = "dgScheduledTasks";
            this.dgScheduledTasks.ReadOnly = true;
            this.dgScheduledTasks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgScheduledTasks.Size = new System.Drawing.Size(451, 288);
            this.dgScheduledTasks.TabIndex = 1;
            // 
            // btnDeleteTask
            // 
            this.btnDeleteTask.Location = new System.Drawing.Point(359, 303);
            this.btnDeleteTask.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteTask.Name = "btnDeleteTask";
            this.btnDeleteTask.Size = new System.Drawing.Size(100, 28);
            this.btnDeleteTask.TabIndex = 0;
            this.btnDeleteTask.Text = "Delete";
            this.btnDeleteTask.UseVisualStyleBackColor = true;
            this.btnDeleteTask.Click += new System.EventHandler(this.btnDeleteTask_Click);
            // 
            // SchedulePurgeProductOrdersTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 400);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SchedulePurgeProductOrdersTask";
            this.Text = "Schedule Product Orders Purge";
            this.Load += new System.EventHandler(this.SchedulePurgeProductOrdersTask_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDaysToKeep)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgScheduledTasks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtTaskName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTaskFrequency;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dgScheduledTasks;
        private System.Windows.Forms.Button btnDeleteTask;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numDaysToKeep;
        private System.Windows.Forms.Label lblFirstRun;
        private System.Windows.Forms.DateTimePicker dtpFirstRun;
        private System.Windows.Forms.Label lblSkipIfMissed;
        private System.Windows.Forms.CheckBox cbSkipIfMissed;
        private System.Windows.Forms.Label lblDaysToKeep;
        private System.Windows.Forms.DateTimePicker dtpFirstRunTime;
    }
}