﻿namespace AOMAdmin
{
    using System;
    using System.Windows.Forms;

    using AOM.App.Domain.Dates;
    using AOM.Transport.Events.MarketTickers;

    using Argus.Transport.Infrastructure;

    public partial class PurgeMarketTicker : Form
    {
        private readonly IBus _bus;

        private readonly IDateTimeProvider _dateTimeProvider;

        public PurgeMarketTicker(IBus bus, IDateTimeProvider dateTimeProvider)
        {
            _bus = bus;
            _dateTimeProvider = dateTimeProvider;
            InitializeComponent();
            dtpPurgeDate.MaxDate = _dateTimeProvider.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            const string dialogText = "Are you sure you wish to purge market ticker items?";
            const string dialogCaption = "Purge Market Ticker";

            DialogResult dialogResult = MessageBox.Show(dialogText, dialogCaption, MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                var request = new PurgeMarketTickerRequest
                              {
                                  DaysToKeep =
                                      ((dtpPurgeDate.Value.Date
                                        - _dateTimeProvider.Now.Date).Days)
                              };

                _bus.Publish(request);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}