﻿namespace AOMAdmin
{
    partial class PurgeOrdersInDiffusion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdPurgeOrdersInDiffusion = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lstProducts = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmdPurgeOrdersInDiffusion
            // 
            this.cmdPurgeOrdersInDiffusion.Location = new System.Drawing.Point(243, 318);
            this.cmdPurgeOrdersInDiffusion.Name = "cmdPurgeOrdersInDiffusion";
            this.cmdPurgeOrdersInDiffusion.Size = new System.Drawing.Size(201, 23);
            this.cmdPurgeOrdersInDiffusion.TabIndex = 0;
            this.cmdPurgeOrdersInDiffusion.Text = "Purge Orders In Diffusion And Rebuild";
            this.cmdPurgeOrdersInDiffusion.UseVisualStyleBackColor = true;
            this.cmdPurgeOrdersInDiffusion.Click += new System.EventHandler(this.cmdPurgeOrdersInDiffusion_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(450, 318);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 39);
            this.label1.TabIndex = 2;
            this.label1.Text = "This option allows you to send a message to the control client which will: ";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(244, 33);
            this.label2.TabIndex = 3;
            this.label2.Text = "1. Remove the View_BidAsk_Widget node from Diffusion - for the selected products." +
    "";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(244, 49);
            this.label3.TabIndex = 4;
            this.label3.Text = "2. Call the Refresh for Orders which will re-populate these nodes with the correc" +
    "t values from the DB.";
            // 
            // lstProducts
            // 
            this.lstProducts.FormattingEnabled = true;
            this.lstProducts.Location = new System.Drawing.Point(11, 203);
            this.lstProducts.Name = "lstProducts";
            this.lstProducts.Size = new System.Drawing.Size(514, 109);
            this.lstProducts.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(279, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Please select products you\'d like to remove the orders for:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(36, 318);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(201, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Purge Orders";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PurgeOrdersInDiffusion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 353);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lstProducts);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdPurgeOrdersInDiffusion);
            this.Name = "PurgeOrdersInDiffusion";
            this.Text = "Purge Orders In Diffusion";
            this.Load += new System.EventHandler(this.PurgeOrdersInDiffusion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdPurgeOrdersInDiffusion;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.CheckedListBox lstProducts;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
    }
}