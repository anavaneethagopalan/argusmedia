﻿using AOM.Services.ProductService;
using Utils.Logging.Utils;

namespace AOMAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    public partial class ProductMain : Form
    {
        private readonly IBus _bus;
        private readonly IProductService _productService;
        private readonly ICrmAdministrationService CrmAdministrationService;
        private IList<Product> _products;
        private Boolean _productConfigRefreshRequired;

        public ProductMain(ICrmAdministrationService crmAdministrationService, IBus bus, IProductService productService)
        {
            _bus = bus;
            _productService = productService;
            CrmAdministrationService = crmAdministrationService;

            InitializeComponent();
        }

        private void ManageProducts_Load(object sender, EventArgs e)
        {
            RefreshProducts();
            _productConfigRefreshRequired = false;
            lblNotification.Text = "";
        }

        private void RefreshProducts()
        {
            try
            {
                _products =
                    _bus.Request<GetAllActiveProductsRequestRpc, List<Product>>(new GetAllActiveProductsRequestRpc())
                        .OrderBy(p => p.Name).ToList();
            }
            catch (Exception ex)
            {
                Log.Error("Error Loading Products via the bus", ex);
                _products = _productService.GetAllProducts();
            }


            dgProducts.Rows.Clear();
            dgProducts.Refresh();
            for (var ix = 0; ix < _products.Count; ix++)
            {
                var prod = _products[ix];
                dgProducts.Rows.Add(
                    ix,
                    prod.Name,
                    prod.Status.ToString(),
                    prod.LastOpenDate,
                    prod.LastCloseDate,
                    prod.IsDeleted,
                    "Tenors...",
                    "Times...",
                    "Status...",
                    "Deals...",
                    "CoBrokering...",
                    "Metadata...");
            }
        }

        private void _cmdRefresh_Click(object sender, EventArgs e)
        {
            RefreshProducts();
        }

        private void dgOrganisations_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var product = _products[dgProducts.Rows[e.RowIndex].Cells[0].Value as int? ?? 0];

                switch (e.ColumnIndex)
                {
                    case 6:
                        ShowTenors(product);
                        break;
                    case 7:
                        ShowTimes(product);
                        break;
                    case 8:
                        ShowOrders(product);
                        break;
                    case 9:
                        ShowDealsConfiguration(product);
                        break;
                    case 10:
                        ShowCoBrokeringConfiguration(product);
                        break;
                    case 11:
                        ShowMetadataWindow(product);
                        break;
                    default:
                        MessageBox.Show(
                            "Bug in code - unexpected button index.",
                            "Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button1);
                        break;
                }
            }
        }

        private void ShowMetadataWindow(Product product)
        {
            var productMetadataDialog = new ProductMetadataForm(product, _bus);

            productMetadataDialog.ShowDialog();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            if (_productConfigRefreshRequired)
            {
                RequestProductConfigTopicTreeRefresh();
            }
            Close();
        }

        private void RequestProductConfigTopicTreeRefresh()
        {
            _bus.Publish(new GetProductConfigRequest());
        }

        private void ShowTenors(Product prod)
        {
            using (var frm = new ProductTenors(_bus, prod) { StartPosition = FormStartPosition.CenterParent })
            {
                CheckConfigurationRefreshRequired(frm.ShowDialog());
            }

            // RefreshProducts();
        }

        private void ShowTimes(Product prod)
        {
            using (
                var frm = new ProductMarketStatusTimers(_bus, prod) { StartPosition = FormStartPosition.CenterParent })
            {
                CheckConfigurationRefreshRequired(frm.ShowDialog());
            }

            RefreshProducts();
        }

        private void ShowOrders(Product prod)
        {
            using (var frm = new ProductOrdersPurgeTimes(_bus, prod) { StartPosition = FormStartPosition.CenterParent })
            {
                CheckConfigurationRefreshRequired(frm.ShowDialog());
            }

            // RefreshProducts();
        }

        private void ShowDealsConfiguration(Product product)
        {
            using (var frm = new ProductDeals(_bus, product) { StartPosition = FormStartPosition.CenterParent })
            {
                CheckConfigurationRefreshRequired(frm.ShowDialog());
            }

            RefreshProducts();
        }

        private void ShowCoBrokeringConfiguration(Product product)
        {
            using (var frm = new ProductCoBrokering(_bus, product) { StartPosition = FormStartPosition.CenterParent })
            {
                CheckConfigurationRefreshRequired(frm.ShowDialog());
            }

            // RefreshProducts();
        }

        private void CheckConfigurationRefreshRequired(DialogResult dr)
        {
            // DialogResult will be Yes if any updates were done in the child dialog
            _productConfigRefreshRequired = _productConfigRefreshRequired || dr == DialogResult.Yes;
            if (_productConfigRefreshRequired)
            {
                lblNotification.Text =
                    "Product changes detected. Close this dialog to propagate the changes to the system.";
            }
        }

        private void cmdCreateProduct_Click(object sender, EventArgs e)
        {
            using (var frm = new ProductCreate(_bus, _productService) { StartPosition = FormStartPosition.CenterParent })
            {
                CheckConfigurationRefreshRequired(frm.ShowDialog());
                RefreshProducts();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RequestProductConfigTopicTreeRefresh();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            _bus.Publish(new ResetMarketStatusTimers());
        }
    }
}