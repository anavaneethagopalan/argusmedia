﻿namespace AOMAdmin
{
    partial class UserDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDetails));
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.labelUsername = new System.Windows.Forms.Label();
            this.labelOrganisation = new System.Windows.Forms.Label();
            this.txtOrganisation = new System.Windows.Forms.TextBox();
            this.txtArgusCrmUsername = new System.Windows.Forms.TextBox();
            this.labelArgusCrmUsername = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.labelTelephone = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.labelTitle = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.IsActiveCB = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.IsDeletedCB = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtUsername.Location = new System.Drawing.Point(130, 8);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.ReadOnly = true;
            this.txtUsername.Size = new System.Drawing.Size(218, 20);
            this.txtUsername.TabIndex = 9;
            this.txtUsername.TabStop = false;
            // 
            // labelUsername
            // 
            this.labelUsername.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelUsername.AutoSize = true;
            this.labelUsername.Location = new System.Drawing.Point(3, 11);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelUsername.Size = new System.Drawing.Size(63, 13);
            this.labelUsername.TabIndex = 10;
            this.labelUsername.Text = "Username:";
            // 
            // labelOrganisation
            // 
            this.labelOrganisation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelOrganisation.AutoSize = true;
            this.labelOrganisation.Location = new System.Drawing.Point(3, 47);
            this.labelOrganisation.Name = "labelOrganisation";
            this.labelOrganisation.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelOrganisation.Size = new System.Drawing.Size(74, 13);
            this.labelOrganisation.TabIndex = 11;
            this.labelOrganisation.Text = "Organisation:";
            // 
            // txtOrganisation
            // 
            this.txtOrganisation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtOrganisation.Location = new System.Drawing.Point(130, 43);
            this.txtOrganisation.Name = "txtOrganisation";
            this.txtOrganisation.ReadOnly = true;
            this.txtOrganisation.Size = new System.Drawing.Size(218, 20);
            this.txtOrganisation.TabIndex = 12;
            this.txtOrganisation.TabStop = false;
            // 
            // txtArgusCrmUsername
            // 
            this.txtArgusCrmUsername.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtArgusCrmUsername.Location = new System.Drawing.Point(130, 80);
            this.txtArgusCrmUsername.Name = "txtArgusCrmUsername";
            this.txtArgusCrmUsername.Size = new System.Drawing.Size(218, 20);
            this.txtArgusCrmUsername.TabIndex = 1;
            // 
            // labelArgusCrmUsername
            // 
            this.labelArgusCrmUsername.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelArgusCrmUsername.AutoSize = true;
            this.labelArgusCrmUsername.Location = new System.Drawing.Point(3, 84);
            this.labelArgusCrmUsername.Name = "labelArgusCrmUsername";
            this.labelArgusCrmUsername.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelArgusCrmUsername.Size = new System.Drawing.Size(120, 13);
            this.labelArgusCrmUsername.TabIndex = 12;
            this.labelArgusCrmUsername.Text = "Argus CRM Username:";
            // 
            // txtTelephone
            // 
            this.txtTelephone.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTelephone.Location = new System.Drawing.Point(130, 231);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(218, 20);
            this.txtTelephone.TabIndex = 5;
            // 
            // labelTelephone
            // 
            this.labelTelephone.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTelephone.AutoSize = true;
            this.labelTelephone.Location = new System.Drawing.Point(3, 234);
            this.labelTelephone.Name = "labelTelephone";
            this.labelTelephone.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelTelephone.Size = new System.Drawing.Size(66, 13);
            this.labelTelephone.TabIndex = 16;
            this.labelTelephone.Text = "Telephone:";
            // 
            // txtTitle
            // 
            this.txtTitle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTitle.Location = new System.Drawing.Point(130, 118);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(124, 20);
            this.txtTitle.TabIndex = 2;
            // 
            // labelTitle
            // 
            this.labelTitle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(3, 121);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelTitle.Size = new System.Drawing.Size(35, 13);
            this.labelTitle.TabIndex = 13;
            this.labelTitle.Text = "Title:";
            // 
            // txtEmail
            // 
            this.txtEmail.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtEmail.Location = new System.Drawing.Point(130, 192);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(218, 20);
            this.txtEmail.TabIndex = 4;
            // 
            // labelEmail
            // 
            this.labelEmail.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(3, 195);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelEmail.Size = new System.Drawing.Size(40, 13);
            this.labelEmail.TabIndex = 15;
            this.labelEmail.Text = "Email:";
            // 
            // txtName
            // 
            this.txtName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtName.Location = new System.Drawing.Point(130, 154);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(218, 20);
            this.txtName.TabIndex = 3;
            // 
            // labelName
            // 
            this.labelName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(3, 158);
            this.labelName.Name = "labelName";
            this.labelName.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.labelName.Size = new System.Drawing.Size(43, 13);
            this.labelName.TabIndex = 14;
            this.labelName.Text = "Name:";
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmdUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdUpdate.Location = new System.Drawing.Point(136, 339);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(75, 23);
            this.cmdUpdate.TabIndex = 6;
            this.cmdUpdate.Text = "&Update";
            this.cmdUpdate.UseVisualStyleBackColor = true;
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(288, 339);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 7;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.26171F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.73829F));
            this.tableLayoutPanel1.Controls.Add(this.labelArgusCrmUsername, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtEmail, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelTelephone, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtName, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelUsername, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtOrganisation, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelTitle, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelEmail, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelOrganisation, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelName, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtUsername, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtTitle, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtTelephone, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtArgusCrmUsername, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.IsActiveCB, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.IsDeletedCB, 1, 8);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.80597F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.43284F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.92537F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.80597F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.1791F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.55224F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.29851F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(363, 331);
            this.tableLayoutPanel1.TabIndex = 35;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 272);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "IsActive";
            // 
            // IsActiveCB
            // 
            this.IsActiveCB.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.IsActiveCB.AutoSize = true;
            this.IsActiveCB.Location = new System.Drawing.Point(130, 271);
            this.IsActiveCB.Name = "IsActiveCB";
            this.IsActiveCB.Size = new System.Drawing.Size(15, 14);
            this.IsActiveCB.TabIndex = 18;
            this.IsActiveCB.UseVisualStyleBackColor = true;
            //this.IsActiveCB.CheckedChanged += new System.EventHandler(this.IsActiveCB_CheckedChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 307);
            this.label2.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "IsDeleted";
            // 
            // IsDeletedCB
            // 
            this.IsDeletedCB.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.IsDeletedCB.AutoSize = true;
            this.IsDeletedCB.Location = new System.Drawing.Point(130, 306);
            this.IsDeletedCB.Name = "IsDeletedCB";
            this.IsDeletedCB.Size = new System.Drawing.Size(15, 14);
            this.IsDeletedCB.TabIndex = 20;
            this.IsDeletedCB.UseVisualStyleBackColor = true;
            //this.IsDeletedCB.CheckedChanged += new System.EventHandler(this.IsDeletedCB_CheckedChanged);
            // 
            // UserDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 379);
            this.Controls.Add(this.cmdUpdate);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserDetails";
            this.Text = "User Details";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.Label labelOrganisation;
        private System.Windows.Forms.TextBox txtOrganisation;
        private System.Windows.Forms.TextBox txtArgusCrmUsername;
        private System.Windows.Forms.Label labelArgusCrmUsername;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.Label labelTelephone;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button cmdUpdate;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox IsActiveCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox IsDeletedCB;
    }
}