﻿using System.Collections.ObjectModel;
using AOM.Transport.Events.Products;
using System.Text;
using AOM.Repository.MySql.Aom;
using System;
using System.Windows.Forms;
using AOM.Services.ProductService;
using Argus.Transport.Infrastructure;

namespace AOMAdmin
{
    public partial class ProductCreate : Form
    {
        private readonly IBus _bus;
        private readonly IProductService _productService;

        public ProductCreate(IBus bus, IProductService productService)
        {
            _bus = bus;
            _productService = productService;
            InitializeComponent();
        }

        private void ProductCreate_Load(object sender, EventArgs e)
        {
            DisplayCommodities();
            DisplayContractTypes();
            DisplayMarkets();
            DisplayDeliveryLocations();
            DisplayCurrencies();
            DisplayUnits();

            PopulateProductDefaultValues();
            PopulateProductTenorDefaultValues();
        }

        private void PopulateProductTenorDefaultValues()
        {
            txtProductTenorName.Text = "";
            txtProductTenorDisplayName.Text = "";
            txtProductTenorDeliveryDateStart.Text = @"+1d";
            txtProductTenorDeliveryDateEnd.Text = @"+20d";
            txtProductTenorDefaultStartDate.Text = @"+1d";
            txtProductTenorDefaultEndDate.Text = @"+20d";
            txtProductTenorRollDate.Text = @"DAILY";
            txtProductTenorRoleDateRule.Text = @"ACTUAL";
            txtProductTenorMinimumDeliveryRange.Text = @"10";
        }

        private void PopulateProductDefaultValues()
        {
            txtOrderVolumeDecimalPlaces.Text = @"2";
            txtPriceDecimalPlaces.Text = @"2";
            txtPurgeFrequency.Text = @"7";
            txtProductName.Text = @"NEW PRODUCT";
            txtOpenTime.Value = new DateTime(2015, 1, 1, 9, 0, 0);
            txtCloseTime.Value = new DateTime(2015, 1, 1, 12, 0, 0);
            txtPurgeTimeOfDay.Value = new DateTime(2015, 1, 1, 23, 0, 0);
            txtPurgeFrequency.Text = @"7";

            txtOrderVolumeDefault.Text = @"500";
            txtOrderVolumeMinimum.Text = @"400";
            txtOrderVolumeMaximum.Text = @"20000";
            txtOrderVolumeIncrement.Text = @"5";
            txtOrderVolumeSignificantFigures.Text = @"10";
            txtOrderVolumeDecimalPlaces.Text = @"2";

            txtPriceMinimum.Text = @"100";
            txtPriceMaximum.Text = @"20000";
            txtPriceIncrement.Text = @"5";
            txtPriceSignificantFigures.Text = @"10";
        }

        private void DisplayUnits()
        {
            var units = _productService.GetUnits();
            var description = string.Empty;

            foreach (var unit in units)
            {
                description = string.Format("{0} [{1}]", unit.Description, unit.Name);
                var ci = new ComboBoxItem(unit.Code, description);
                cboVolumeUnitCode.Items.Add(ci);
                cboPriceUnitCode.Items.Add(ci);
            }
        }

        private void DisplayCurrencies()
        {
            var currencies = _productService.GetCurrencies();

            foreach (var currency in currencies)
            {
                var ci = new ComboBoxItem(currency.Code, currency.Name);
                cboCurrencyCode.Items.Add(ci);
            }
        }

        private void DisplayDeliveryLocations()
        {
            var deliveryLocations = _productService.GetDeliveryLocations();

            foreach (var deliveryLocation in deliveryLocations)
            {
                var co = new ComboBoxItem(deliveryLocation.Id, deliveryLocation.Name);
                cboLocation.Items.Add(co);
            }
        }

        private void DisplayMarkets()
        {
            var markets = _productService.GetMarkets();

            foreach (var market in markets)
            {
                var co = new ComboBoxItem(market.Id, market.Name);
                cboMarket.Items.Add(co);
            }
        }

        private void DisplayContractTypes()
        {
            var contractTypes = _productService.GetContractTypes();

            foreach (var contractType in contractTypes)
            {
                var co = new ComboBoxItem(contractType.Id, contractType.Name);
                cboContractType.Items.Add(co);
            }
        }

        private void DisplayCommodities()
        {
            var commodities = _productService.GetCommodities();

            foreach (var commodity in commodities)
            {
                var co = new ComboBoxItem(commodity.Id, commodity.Name);
                cboCommodity.Items.Add(co);
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                var dateTimeNow = DateTime.Now;

                var product = MapScreenToProduct();
                product.LastUpdatedUserId = AdminSettings.LoggedOnUser.Id;
                product.DateCreated = dateTimeNow;
                product.LastUpdated = dateTimeNow;

                var productTenor = MapScreenToProductTenor();

                //if (String.IsNullOrEmpty(productTenor.DefaultStartDate))
                //{
                //    productTenor.DefaultStartDate = productTenor.DeliveryDateStart;
                //}
                //if (string.IsNullOrEmpty(productTenor.DefaultEndDate))
                //{
                //    productTenor.DefaultEndDate = productTenor.DeliveryDateEnd;
                //}

                if (productTenor != null)
                {
                    productTenor.LastUpdatedUserId = AdminSettings.LoggedOnUser.Id;
                    productTenor.DateCreated = dateTimeNow;
                    product.ProductTenors = new Collection<ProductTenorDto> {productTenor};
                }

                var productError = ValidateProduct(product);
                var productTenorError = ValidateProductTenor(productTenor);

                if (!productError)
                {
                    if (!productTenorError)
                    {
                        var result = false;
                        try
                        {

                            result = _productService.SaveProduct(product, AdminSettings.LoggedOnUser.Id);
                            _bus.Publish(new ResetMarketStatusTimers());
                            _bus.Publish(new GetProductConfigRequest {ProductId = product.Id});

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error creating new product. " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        if (result)
                        {
                            MessageBox.Show("Product Successfully created", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Error creating new product.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error creating new product. " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private ProductTenorDto MapScreenToProductTenor()
        {
            var productTenor = new ProductTenorDto
            {
                Name = txtProductTenorName.Text,
                DisplayName = txtProductTenorDisplayName.Text,
                //DeliveryDateStart = txtProductTenorDeliveryDateStart.Text,
                //DeliveryDateEnd = txtProductTenorDeliveryDateEnd.Text,
                //RollDate = txtProductTenorRollDate.Text,
                //RollDateRule = txtProductTenorRollDate.Text,
                //MinimumDeliveryRange = GetNumber(txtProductTenorMinimumDeliveryRange, 3),
                //DefaultStartDate = txtProductTenorDefaultStartDate.Text,
                //DefaultEndDate = txtProductTenorDefaultEndDate.Text
            };

            return productTenor;
        }

        private bool ValidateProductTenor(ProductTenorDto productTenorDto)
        {
            StringBuilder errorMessage = new StringBuilder();
            bool error = false;

            if (productTenorDto == null)
            {
                errorMessage.Append("Product Tenor is null" + Environment.NewLine);
                error = true;
            }

            //if (GetIntValue(productTenorDto.DefaultStartDate) < GetIntValue(productTenorDto.DeliveryDateStart))
            //{
            //    error = true;
            //    errorMessage.Append("Default Start Date cannot be less tha Delivery Date Start" + Environment.NewLine);
            //}

            //if (GetIntValue(productTenorDto.DefaultEndDate) > GetIntValue(productTenorDto.DeliveryDateEnd))
            //{
            //    error = true;
            //    errorMessage.Append("Default End Date cannot be greater than the Delivery Date End" + Environment.NewLine);
            //}

            //if ((GetIntValue(productTenorDto.DefaultEndDate) - GetIntValue(productTenorDto.DefaultStartDate) < productTenorDto.MinimumDeliveryRange))
            //{
            //    error = true;
            //    errorMessage.Append("(Default End Date minus Default Start Date) cannot be less than the Minumum Delivery Range" + Environment.NewLine);
            //}

            if (error)
            {
                MessageBox.Show(errorMessage.ToString(), "Create Product - Failed - Product Tenor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return error;
        }

        private bool ValidateProduct(ProductDto product)
        {
            if (product == null)
            {
                MessageBox.Show("Product is null", "Create Product - Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }

            StringBuilder errorMessage = new StringBuilder();
            bool error = false;

            // Volume Checks
            if (product.OrderVolumeDefault < product.OrderVolumeMinimum)
            {
                error = true;
                errorMessage.Append("Order Volume Default cannot be less that Order Volume Minimum" + Environment.NewLine);
            }

            if (product.OrderVolumeDefault > product.OrderVolumeMaximum)
            {
                error = true;
                errorMessage.Append("Order Volume Default cannot be greater than Order Volume Maximum" + Environment.NewLine);
            }

            if (product.OrderVolumeDefault < product.OrderVolumeMinimum)
            {
                error = true;
                errorMessage.Append("Order Volume Default cannot be less than Order Volume Minimum" + Environment.NewLine);
            }

            if (product.OrderVolumeMaximum <= product.OrderVolumeMinimum)
            {
                error = true;
                errorMessage.Append("Order Volume Maximum cannot be less than or equal to Order Volume Minimum" + Environment.NewLine);
            }

            if (product.OrderPriceMinimum > product.OrderPriceMaximum)
            {
                error = true;
                errorMessage.Append("Price Maximum cannot be greater than Price Maximum" + Environment.NewLine);
            }


            // Product Required Fields
            if (product.CommodityId == 0)
            {
                error = true;
                errorMessage.Append("Please Select a Commodity" + Environment.NewLine);
            }

            if (product.MarketId == 0)
            {
                error = true;
                errorMessage.Append("Please Select a Commodity" + Environment.NewLine);
            }

            if (product.ContractTypeId == 0)
            {
                error = true;
                errorMessage.Append("Please Select a Contract Type" + Environment.NewLine);
            }

            if (product.DeliveryLocations == null)
            {
                error = true;
                errorMessage.Append("Please Select a Location" + Environment.NewLine);
            }


            // Check the string codes.
            //if (string.IsNullOrEmpty(product.Currency))
            //{
            //    error = true;
            //    errorMessage.Append("Please select a Currency Code" + Environment.NewLine);
            //}

            if (string.IsNullOrEmpty(product.VolumeUnit))
            {
                error = true;
                errorMessage.Append("Please select a Volume Unit Code" + Environment.NewLine);
            }

            //if (string.IsNullOrEmpty(product.PriceUnit))
            //{
            //    error = true;
            //    errorMessage.Append("Please select a Price Unit Code" + Environment.NewLine);
            //}

            if (error)
            {
                MessageBox.Show(errorMessage.ToString(), "Create Product - Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // TODO: Lots more validation.
            return error;
        }

        private ProductDto MapScreenToProduct()
        {
            var product = new ProductDto
            {
                Name = txtProductName.Text,
                IsDeleted = chkIsDeleted.Checked,
                IsInternal = chkIsInternal.Checked,
                DisplayOrder = GetIntValue(txtDisplayOrder.Text),
                CommodityId = GetSelectedComboBoxId(cboCommodity, "commodity"),
                ContractTypeId = GetSelectedComboBoxId(cboContractType, "contract type"),
                MarketId = GetSelectedComboBoxId(cboMarket, "market"),
                //LocationId = GetSelectedComboBoxId(cboLocation, "location"),
                //Currency = GetSelectedComboBoxStringId(cboCurrencyCode, "currency code"),
                VolumeUnit = GetSelectedComboBoxStringId(cboVolumeUnitCode, "volume unit code"),
                //PriceUnit = GetSelectedComboBoxStringId(cboPriceUnitCode, "price unit code"),
                OrderVolumeDefault = GetPrice(txtOrderVolumeDefault),
                OrderVolumeMinimum = GetPrice(txtOrderVolumeMinimum),
                OrderVolumeMaximum = GetPrice(txtOrderVolumeMaximum),
                OrderVolumeIncrement = GetPrice(txtOrderVolumeIncrement),
                OrderVolumeDecimalPlaces = GetPrice(txtOrderVolumeDecimalPlaces),
                OrderVolumeSignificantFigures = GetPrice(txtOrderVolumeSignificantFigures),
                OrderPriceMinimum = GetPrice(txtPriceMinimum),
                OrderPriceMaximum = GetPrice(txtPriceMaximum),
                OrderPriceIncrement = GetPrice(txtPriceIncrement),
                OrderPriceDecimalPlaces = GetPrice(txtPriceDecimalPlaces),
                OrderPriceSignificantFigures = GetPrice(txtPriceSignificantFigures),
                BidAskStackNumberRows = GetNumber(txtBidAskStackNumberRows),
                OpenTime = txtOpenTime.Value.TimeOfDay,
                CloseTime = txtCloseTime.Value.TimeOfDay,
                Status = txtStatus.Text,
                PurgeFrequency = GetNumber(txtPurgeFrequency),
                PurgeTimeOfDay = txtPurgeTimeOfDay.Value.TimeOfDay,
                IgnoreAutomaticMarketStatusTriggers = chkIgnoreMarketStatusTriggers.Checked,
                DisplayOptionalPriceDetail = chkDisplayOptionalPriceDetail.Checked,
                AllowNegativePriceForExternalDeals = chkAllowNegativePriceForExternalDeals.Checked,
                HasAssessment = chkHasAssessment.Checked,
                //LocationLabel = txtLocationLabel.Text,
                DeliveryPeriodLabel = txtDeliveryPeriodLabel.Text,
                CoBrokering = chkCoBrokering.Checked
            };

            return product;
        }

        private int GetIntValue(string period)
        {
            if (string.IsNullOrEmpty(period))
            {
                return 0;
            }

            period = period.Replace("+", "");
            period = period.Replace("d", "");
            int p = 0;
            int.TryParse(period, out p);
            return p;
        }

        private int GetNumber(TextBox textBox, int defaultValue = 0)
        {
            int textBoxValue = defaultValue;
            var value = textBox.Text;

            if (string.IsNullOrEmpty(value))
            {
                return textBoxValue;
            }

            int.TryParse(value, out textBoxValue);

            return textBoxValue;
        }


        private decimal GetPrice(TextBox textBox)
        {
            var textBoxValue = 0M;
            var value = textBox.Text;

            if (string.IsNullOrEmpty(value))
            {
                return textBoxValue;
            }

            decimal.TryParse(value, out textBoxValue);

            return textBoxValue;
        }

        private string GetSelectedComboBoxStringId(ComboBox comboBox, string label)
        {
            var item = comboBox.SelectedItem as ComboBoxItem;
            if (item == null)
            {
                throw new ApplicationException("You must select a " + label);
            }
            return item.StringId;
        }

        private long GetSelectedComboBoxId(ComboBox comboBox, string label)
        {
            var item = comboBox.SelectedItem as ComboBoxItem;
            if (item == null)
            {
                throw new ApplicationException(label + "was not selected");
            }
            return item.Id;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtDisplayOrder_TextChanged(object sender, EventArgs e)
        {

        }
    }
}