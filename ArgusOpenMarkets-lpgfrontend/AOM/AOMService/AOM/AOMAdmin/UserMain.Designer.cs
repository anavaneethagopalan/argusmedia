﻿namespace AOMAdmin
{
    partial class UserMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserMain));
            this.cmdRefresh = new System.Windows.Forms.Button();
            this.dgUsers = new System.Windows.Forms.DataGridView();
            this.cmdAddUser = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.chkOnlyShowBlockedUsers = new System.Windows.Forms.CheckBox();
            this.chkOnlyShowDeletedUsers = new System.Windows.Forms.CheckBox();
            this.colIX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCrmName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsActive = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colBlocked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colDeleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colCmdBlock = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdPassword = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdPermissions = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdOrders = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCmdDetails = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRefresh.Location = new System.Drawing.Point(15, 321);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(94, 23);
            this.cmdRefresh.TabIndex = 1;
            this.cmdRefresh.Text = "Refresh Users";
            this.cmdRefresh.UseVisualStyleBackColor = true;
            this.cmdRefresh.Click += new System.EventHandler(this._cmdRefresh_Click);
            // 
            // dgUsers
            // 
            this.dgUsers.AllowUserToAddRows = false;
            this.dgUsers.AllowUserToDeleteRows = false;
            this.dgUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgUsers.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIX,
            this.colUserName,
            this.colName,
            this.colEmail,
            this.colCrmName,
            this.colExpiryDate,
            this.colIsActive,
            this.colBlocked,
            this.colDeleted,
            this.colCmdBlock,
            this.colCmdPassword,
            this.colCmdPermissions,
            this.colCmdOrders,
            this.colCmdDetails});
            this.dgUsers.Location = new System.Drawing.Point(15, 13);
            this.dgUsers.Name = "dgUsers";
            this.dgUsers.ReadOnly = true;
            this.dgUsers.RowHeadersWidth = 20;
            this.dgUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUsers.Size = new System.Drawing.Size(1353, 300);
            this.dgUsers.TabIndex = 0;
            this.dgUsers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUsers_CellContentClick);
            // 
            // cmdAddUser
            // 
            this.cmdAddUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAddUser.Location = new System.Drawing.Point(115, 321);
            this.cmdAddUser.Name = "cmdAddUser";
            this.cmdAddUser.Size = new System.Drawing.Size(94, 23);
            this.cmdAddUser.TabIndex = 2;
            this.cmdAddUser.Text = "Add User";
            this.cmdAddUser.UseVisualStyleBackColor = true;
            this.cmdAddUser.Click += new System.EventHandler(this.cmdAddUser_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(1274, 321);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(214, 321);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(210, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Reset All Users Passwords";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkOnlyShowBlockedUsers
            // 
            this.chkOnlyShowBlockedUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkOnlyShowBlockedUsers.AutoSize = true;
            this.chkOnlyShowBlockedUsers.Location = new System.Drawing.Point(531, 323);
            this.chkOnlyShowBlockedUsers.Margin = new System.Windows.Forms.Padding(2);
            this.chkOnlyShowBlockedUsers.Name = "chkOnlyShowBlockedUsers";
            this.chkOnlyShowBlockedUsers.Size = new System.Drawing.Size(155, 17);
            this.chkOnlyShowBlockedUsers.TabIndex = 5;
            this.chkOnlyShowBlockedUsers.Text = "Only Show Blocked Users?";
            this.chkOnlyShowBlockedUsers.UseVisualStyleBackColor = true;
            this.chkOnlyShowBlockedUsers.CheckedChanged += new System.EventHandler(this.chkOnlyShowBlockedUsers_CheckedChanged);
            // 
            // chkOnlyShowDeletedUsers
            // 
            this.chkOnlyShowDeletedUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkOnlyShowDeletedUsers.AutoSize = true;
            this.chkOnlyShowDeletedUsers.Location = new System.Drawing.Point(689, 323);
            this.chkOnlyShowDeletedUsers.Margin = new System.Windows.Forms.Padding(2);
            this.chkOnlyShowDeletedUsers.Name = "chkOnlyShowDeletedUsers";
            this.chkOnlyShowDeletedUsers.Size = new System.Drawing.Size(153, 17);
            this.chkOnlyShowDeletedUsers.TabIndex = 6;
            this.chkOnlyShowDeletedUsers.Text = "Only Show Deleted Users?";
            this.chkOnlyShowDeletedUsers.UseVisualStyleBackColor = true;
            this.chkOnlyShowDeletedUsers.CheckedChanged += new System.EventHandler(this.chkOnlyShowDeletedUsers_CheckedChanged);
            // 
            // colIX
            // 
            this.colIX.HeaderText = "-";
            this.colIX.Name = "colIX";
            this.colIX.ReadOnly = true;
            this.colIX.Visible = false;
            // 
            // colUserName
            // 
            this.colUserName.HeaderText = "UserName";
            this.colUserName.Name = "colUserName";
            this.colUserName.ReadOnly = true;
            this.colUserName.Width = 80;
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colEmail
            // 
            this.colEmail.HeaderText = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.ReadOnly = true;
            this.colEmail.Width = 200;
            // 
            // colCrmName
            // 
            this.colCrmName.HeaderText = "Argus CRM Name";
            this.colCrmName.Name = "colCrmName";
            this.colCrmName.ReadOnly = true;
            this.colCrmName.Width = 200;
            // 
            // colExpiryDate
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;            
            this.colExpiryDate.DefaultCellStyle = dataGridViewCellStyle1;
            this.colExpiryDate.HeaderText = "Expiry Date";
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.ReadOnly = true;
            this.colExpiryDate.Width = 120;
            // 
            // colIsActive
            // 
            this.colIsActive.HeaderText = "Active?";
            this.colIsActive.Name = "colIsActive";
            this.colIsActive.ReadOnly = true;
            this.colIsActive.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIsActive.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIsActive.Width = 60;
            // 
            // colBlocked
            // 
            this.colBlocked.HeaderText = "Blocked?";
            this.colBlocked.Name = "colBlocked";
            this.colBlocked.ReadOnly = true;
            this.colBlocked.Width = 60;
            // 
            // colDeleted
            // 
            this.colDeleted.HeaderText = "Deleted?";
            this.colDeleted.Name = "colDeleted";
            this.colDeleted.ReadOnly = true;
            this.colDeleted.Width = 60;
            // 
            // colCmdBlock
            // 
            this.colCmdBlock.HeaderText = "Blocking";
            this.colCmdBlock.Name = "colCmdBlock";
            this.colCmdBlock.ReadOnly = true;
            this.colCmdBlock.Width = 60;
            // 
            // colCmdPassword
            // 
            this.colCmdPassword.HeaderText = "Password";
            this.colCmdPassword.Name = "colCmdPassword";
            this.colCmdPassword.ReadOnly = true;
            this.colCmdPassword.Width = 70;
            // 
            // colCmdPermissions
            // 
            this.colCmdPermissions.HeaderText = "Permissions";
            this.colCmdPermissions.Name = "colCmdPermissions";
            this.colCmdPermissions.ReadOnly = true;
            this.colCmdPermissions.Width = 80;
            // 
            // colCmdOrders
            // 
            this.colCmdOrders.HeaderText = "Orders";
            this.colCmdOrders.Name = "colCmdOrders";
            this.colCmdOrders.ReadOnly = true;
            this.colCmdOrders.Width = 60;
            // 
            // colCmdDetails
            // 
            this.colCmdDetails.HeaderText = "Details";
            this.colCmdDetails.Name = "colCmdDetails";
            this.colCmdDetails.ReadOnly = true;
            this.colCmdDetails.Width = 60;
            // 
            // UserMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 354);
            this.Controls.Add(this.chkOnlyShowDeletedUsers);
            this.Controls.Add(this.chkOnlyShowBlockedUsers);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdAddUser);
            this.Controls.Add(this.dgUsers);
            this.Controls.Add(this.cmdRefresh);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserMain";
            this.Text = "Manage Users";
            this.Load += new System.EventHandler(this.ManageUsersOrders_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgUsers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdRefresh;
        private System.Windows.Forms.DataGridView dgUsers;
        private System.Windows.Forms.Button cmdAddUser;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkOnlyShowBlockedUsers;
        private System.Windows.Forms.CheckBox chkOnlyShowDeletedUsers;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIX;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCrmName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExpiryDate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsActive;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colBlocked;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colDeleted;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdBlock;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdPassword;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdPermissions;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdOrders;
        private System.Windows.Forms.DataGridViewButtonColumn colCmdDetails;
    }
}