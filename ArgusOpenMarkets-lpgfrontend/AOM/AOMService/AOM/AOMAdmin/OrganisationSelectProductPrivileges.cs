﻿namespace AOMAdmin
{
    using AOM.App.Domain.Dates;

    using System;
    using System.Linq;
    using System.Windows.Forms;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public partial class OrganisationSelectProductPrivileges : Form
    {
        private readonly ICrmAdministrationService _crmService;

        private readonly IBus _bus;

        private readonly Organisation _organisation;

        private OrganisationProductAndPrivileges _productAndPrivileges;

        private readonly DateTimeProvider _dateTimeProvider;

        private readonly IOrganisationService _organisationService;

        public OrganisationSelectProductPrivileges(
            ICrmAdministrationService crmService,
            IOrganisationService organisationService,
            IBus bus,
            Organisation organisation,
            OrganisationProductAndPrivileges pp,
            DateTimeProvider dateTimeProvider)
        {
            _crmService = crmService;
            _organisationService = organisationService;
            _bus = bus;
            _organisation = organisation;
            _productAndPrivileges = pp;
            _dateTimeProvider = dateTimeProvider;
            InitializeComponent();
        }

        private void SetOrganisationSubscribedProductPrivileges_Load(object sender, EventArgs e)
        {
            DisplayOrganisationSubscribedProductPrivileges();

            Text = _organisation.Name + ": Subscribed Product Privileges";
        }

        private void DisplayOrganisationSubscribedProductPrivileges()
        {
            try
            {
                var productPrivileges = _crmService.GetProductPrivileges();

                var productPrivilegesFilteredByOrgType =
                    productPrivileges.Where(
                        pp =>
                            pp.OrganisationType == _organisation.OrganisationType
                            || pp.OrganisationType == OrganisationType.NotSpecified).ToList();

                lstOrganisationSubscribedPrivileges.Items.Clear();

                foreach (var pp in productPrivilegesFilteredByOrgType)
                {
                    var hasPrivilege = _productAndPrivileges.ProductPrivileges.Any(pap => pap.ProductPrivilegeId == pp.Id)
                        || (_productAndPrivileges.Product.IsInternal && pp.Name.Contains("Deal_Create_"));

                    lstOrganisationSubscribedPrivileges.Items.Add(new ComboBoxItem(pp.Id, pp.Name), hasPrivilege);
                }

            }
            catch (Exception ex)
            {
                ExceptionReporter.ShowException(ex, "Error Loading Product Privileges");
            }
        }

        private void lstOrganisationSubscribedPrivileges_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lstOrganisationsSubscribedProductPriveleges_ItemCheck(object sender, ItemCheckEventArgs e)
        {

        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            _productAndPrivileges.ProductPrivileges.Clear();

            foreach (ComboBoxItem li in lstOrganisationSubscribedPrivileges.CheckedItems)
            {
                _productAndPrivileges.ProductPrivileges.Add(
                    new SubscribedProductPrivilege
                    {
                        OrganisationId = _organisation.Id,
                        ProductId = _productAndPrivileges.Product.ProductId,
                        ProductPrivilegeId = li.Id,
                        CreatedBy = AdminSettings.LoggedOnUser.Id.ToString(),
                        DateCreated = _dateTimeProvider.UtcNow,
                        ProductPrivilege =
                            new ProductPrivilege { Id = li.Id, Name = li.Description }
                    });

            }
            Close();
        }

        private void cbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int x = 0; x < lstOrganisationSubscribedPrivileges.Items.Count; x++)
            {
                lstOrganisationSubscribedPrivileges.SetItemChecked(x, cbSelectAll.Checked);
            }
        }
    }
}