﻿namespace AOMAdmin
{
    partial class OrganisationRolePrivileges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Product-specific Privileges");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("System-wide Privileges");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrganisationRolePrivileges));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.tvPrivileges = new System.Windows.Forms.TreeView();
            this.lblOrganisationName = new System.Windows.Forms.Label();
            this.lblRoleName = new System.Windows.Forms.Label();
            this.cboRoles = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Organisation:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Role:";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(443, 538);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 4;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // tvPrivileges
            // 
            this.tvPrivileges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvPrivileges.CheckBoxes = true;
            this.tvPrivileges.Location = new System.Drawing.Point(12, 75);
            this.tvPrivileges.Name = "tvPrivileges";
            treeNode1.ForeColor = System.Drawing.Color.Blue;
            treeNode1.Name = "nodeProductPrivs";
            treeNode1.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            treeNode1.Text = "Product-specific Privileges";
            treeNode2.ForeColor = System.Drawing.Color.Blue;
            treeNode2.Name = "nodeSystemPrivs";
            treeNode2.Text = "System-wide Privileges";
            this.tvPrivileges.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.tvPrivileges.Size = new System.Drawing.Size(525, 457);
            this.tvPrivileges.TabIndex = 20;
            this.tvPrivileges.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvPrivileges_AfterCheck);
            this.tvPrivileges.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvPrivileges_AfterSelect);
            // 
            // lblOrganisationName
            // 
            this.lblOrganisationName.AutoSize = true;
            this.lblOrganisationName.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblOrganisationName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrganisationName.Location = new System.Drawing.Point(96, 20);
            this.lblOrganisationName.Name = "lblOrganisationName";
            this.lblOrganisationName.Size = new System.Drawing.Size(104, 13);
            this.lblOrganisationName.TabIndex = 21;
            this.lblOrganisationName.Text = "lblOrganisationName";
            // 
            // lblRoleName
            // 
            this.lblRoleName.AutoSize = true;
            this.lblRoleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoleName.Location = new System.Drawing.Point(96, 47);
            this.lblRoleName.Name = "lblRoleName";
            this.lblRoleName.Size = new System.Drawing.Size(65, 13);
            this.lblRoleName.TabIndex = 22;
            this.lblRoleName.Text = "lbRoleName";
            // 
            // cboRoles
            // 
            this.cboRoles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRoles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboRoles.FormattingEnabled = true;
            this.cboRoles.Location = new System.Drawing.Point(98, 44);
            this.cboRoles.Name = "cboRoles";
            this.cboRoles.Size = new System.Drawing.Size(438, 21);
            this.cboRoles.TabIndex = 23;
            this.cboRoles.SelectedIndexChanged += new System.EventHandler(this.cboRoles_SelectedIndexChanged);
            // 
            // OrganisationRolePrivileges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 568);
            this.Controls.Add(this.cboRoles);
            this.Controls.Add(this.lblRoleName);
            this.Controls.Add(this.lblOrganisationName);
            this.Controls.Add(this.tvPrivileges);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrganisationRolePrivileges";
            this.Text = "ManageRolePrivileges";
            this.Load += new System.EventHandler(this.ManageRolePrivileges_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.TreeView tvPrivileges;
        private System.Windows.Forms.Label lblOrganisationName;
        private System.Windows.Forms.Label lblRoleName;
        private System.Windows.Forms.ComboBox cboRoles;
    }
}