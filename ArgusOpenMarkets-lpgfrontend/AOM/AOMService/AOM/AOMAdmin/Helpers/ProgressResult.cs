﻿namespace AOMAdmin.Helpers
{
    public class ProgressResult
    {
        public int ProgressCount { get; set; }
        public string ProgressMessage { get; set; }
        public bool ProgressSucceeded { get; set; }
    }
}
