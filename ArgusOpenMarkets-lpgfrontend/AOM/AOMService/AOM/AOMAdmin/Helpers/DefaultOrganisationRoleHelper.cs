﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Services.ProductService;

namespace AOMAdmin.Helpers
{
    public class DefaultOrganisationRoleHelper
    {
        private ICrmAdministrationService _crmService;
        private IProductService _productService;
        private XmlDocument _defaultRoles;
        private XDocument _defaultRolesX;
        private IList<ISystemPrivilege> _systemPrivileges;

        public DefaultOrganisationRoleHelper(ICrmAdministrationService crmService, IProductService productService)
        {
            _crmService = crmService;
            _productService = productService;
            //_entities = new OrganisationAdminEntities
            //{
            //    Organisation = new Organisation(),
            //    ProductsAndPrivileges = new List<OrganisationProductAndPrivileges>()
            //};

            _defaultRoles = new XmlDocument();
            _defaultRoles.Load("DefaultRoles.xml");
            _defaultRolesX = XDocument.Load("DefaultRoles.xml");
        }

        public string AddDefaultOrganisationRoles(Organisation organisation, List<int> productIds)
        {
            StringBuilder resultMessage = new StringBuilder();
            resultMessage.Append("");

            _systemPrivileges = new List<ISystemPrivilege>();
            _systemPrivileges = _crmService.GetSystemPrivileges(organisation.OrganisationType);

            string newOrganisationType = string.Empty;
            switch (organisation.OrganisationType)
            {
                case OrganisationType.Brokerage:
                    newOrganisationType = "Brokerage";
                    break;
                case OrganisationType.Trading:
                    newOrganisationType = "Trading";
                    break;
                default:
                    //don't default roles if not Trading or Brokerage
                    //Error Message?
                    return null;
            }

            //get all role elements from DefaultRoles.xml where "organisationtype=all" or where equals organisationType of product
            foreach (XElement roleNode in _defaultRolesX.Elements("roles").Elements("role"))
            {
                var orgType = roleNode.Attribute("organisationtype") != null
                    ? roleNode.Attribute("organisationtype").Value
                    : "Any";

                // only add default roles with matching org type if specified
                if (orgType == "Any" || orgType == newOrganisationType)
                {
                    var perProduct = roleNode.Attribute("perproduct").Value.ToLower() != "false";
                    var adminFlag = roleNode.Element("name").Value.ToLower().Contains("administrator");

                    if (perProduct)
                    {
                        //loop through products
                        foreach (int productId in productIds)
                        {
                            var rolePrivileges = new List<ProductRolePrivilege>();
                            Product product = _productService.GetProduct(productId);

                            //Add new organisation role for product
                            OrganisationRole organisationRole = AddOrganisationRole(roleNode, organisation, product);

                            //get all possible product privileges for organisationType from DB
                            var productPrivileges = _crmService.GetProductPrivileges().Where(pp =>
                                pp.OrganisationType == organisation.OrganisationType ||
                                pp.OrganisationType == OrganisationType.NotSpecified).ToList();

                            //convert to correct object type
                            OrganisationProductAndPrivileges orgProductAndPrivileges = new OrganisationProductAndPrivileges
                            {
                                Product = product,
                                ProductPrivileges = new List<SubscribedProductPrivilege>(),
                            };

                            foreach (var pp in productPrivileges)
                            {
                                orgProductAndPrivileges.ProductPrivileges.Add(
                                    new SubscribedProductPrivilege
                                    {
                                        OrganisationId = organisation.Id,
                                        ProductId = productId,
                                        ProductPrivilegeId = pp.Id,
                                        CreatedBy = AdminSettings.LoggedOnUser.Id.ToString(),
                                        DateCreated = DateTime.UtcNow,
                                        ProductPrivilege = new ProductPrivilege {Id = pp.Id, Name = pp.Name}
                                    });
                            }

                            //get applicable privileges for this organisation by cross referencing all productprivileges and values stiplulated in DefaultRoles.xml
                            CreateRolePrivilegesForProduct(organisation, orgProductAndPrivileges, roleNode,
                                rolePrivileges, organisationRole.Id);

                            //check if organisation has any existing SubscribedProductPrivileges as DefaultRoles.xml has duplicates in "All" & "Product" sections e.g: View_BidAsk_Widget
                            var existingRolePrivileges =
                                _crmService.GetOrganisationSubscribedProductPrivilegesByProduct(organisation.Id,
                                    productId);
                            List<ProductRolePrivilege> newRolePrivileges = new List<ProductRolePrivilege>();
                            foreach (var rp in rolePrivileges)
                            {
                                //check if SubscribedProductPrivileges exist or not before adding
                                if (existingRolePrivileges.Count == 0 ||
                                    existingRolePrivileges.Select(erp => erp.OrganisationId == rp.OrganisationId
                                                                         && erp.ProductId == rp.ProductId &&
                                                                         erp.ProductPrivilegeId == rp.ProductPrivilegeId) ==
                                    null)
                                {
                                    _crmService.AddProductPrivilegeToSubscribedProductPrivileges(organisation.Id,
                                        productId, rp.ProductPrivilegeId, AdminSettings.LoggedOnUser.Id);
                                    newRolePrivileges.Add(rp);
                                }
                            }

                            //Add new ProductRolePrivileges
                            _crmService.AddProductRolePrivileges(newRolePrivileges, AdminSettings.LoggedOnUser.Id);

                            resultMessage.Append(string.Format("{0} - Role: {1} (added ProductPrivilege ids: {2}){3}",
                                organisation.ShortCode, organisationRole.Name,
                                string.Join(",", rolePrivileges.Select(rp => rp.ProductPrivilegeId)),
                                Environment.NewLine));
                        }
                    }
                    else if (adminFlag)
                    {

                        //Add new organisation role for system
                        OrganisationRole organisationRole = AddOrganisationRole(roleNode, organisation, null);

                        //Add new SystemPrivileges
                        if (organisationRole != null)
                        {
                            List<string> addedIds = CreateSystemRolePrivileges(roleNode, organisationRole.Id);
                            resultMessage.Append(string.Format("{0} - Role: {1} (added SystemPrivilege ids: {2}){3}",
                                organisation.ShortCode, organisationRole.Name,
                                string.Join(",", addedIds), Environment.NewLine));
                        }
                    }
                }
            }

            return resultMessage.ToString();
        }

        private void CreateRolePrivilegesForProduct(Organisation organisation, OrganisationProductAndPrivileges pp, XElement roleNode, List<ProductRolePrivilege> rolePrivileges, long roleID)
        {
            foreach (var spp in pp.ProductPrivileges)
            {
                foreach (XElement rpNode in roleNode.Elements("productprivileges").Elements("privilege"))
                //foreach (XElement rpNode in roleNode.SelectNodes("productprivileges/privilege"))
                {
                    if (String.Equals(rpNode.Value, spp.ProductPrivilege.Name, StringComparison.CurrentCultureIgnoreCase))
                    {
                        //if (existingRolePrivileges)
                        rolePrivileges.Add(new ProductRolePrivilege
                        {
                            Id = -1,
                            OrganisationId = organisation.Id,
                            RoleId = roleID,
                            ProductId = spp.ProductId,
                            ProductPrivilegeId = spp.ProductPrivilegeId
                        });
                    }
                }
            }
        }

        private List<string> CreateSystemRolePrivileges(XElement roleNode, long roleID)
        {
            //var systemRolePrivileges = _crmService.GetSystemRolePrivileges(roleID);

            List<string> addedIds = new List<string>();
            foreach (var sp in _systemPrivileges)
            {
                //foreach (XElement rpNode in roleNode.Elements("systemprivileges/privilege"))
                foreach (XElement rpNode in roleNode.Elements("systemprivileges").Elements("privilege"))
                {
                     //check if SystemRolePrivileges exist or not before adding
                    //if (systemRolePrivileges.Count == 0 || systemRolePrivileges.Select(srp => srp.RoleId == roleID) == null)
                    //{
                        if (String.Equals(rpNode.Value, sp.Name, StringComparison.CurrentCultureIgnoreCase))
                        {
                            _crmService.AddSystemPrivilegeToRole(new SystemRolePrivilege { Id = -1, RoleId = roleID, PrivilegeId = sp.Id }, AdminSettings.LoggedOnUser.Id);
                            addedIds.Add(sp.Id.ToString());
                        }
                    //}
                }
            }
            return addedIds;
        }

        private OrganisationRole AddOrganisationRole(XElement roleNode, Organisation organisation, Product product)
        {
            var organisationRole = new OrganisationRole
            {
                Id = -1,
                OrganisationId = organisation.Id,
                Name = ReplaceRoleStrings(roleNode.Elements("name").FirstOrDefault().Value, organisation, product),
                Description = ReplaceRoleStrings(roleNode.Elements("description").FirstOrDefault().Value, organisation, product)
            };

            if (_crmService.OrgansiationRoleExists(organisation.Id, organisationRole.Name)) return null;

            organisationRole.Id = _crmService.SaveRole(organisationRole, AdminSettings.LoggedOnUser.Id);
            return organisationRole;
        }

        private string ReplaceRoleStrings(string roleString, Organisation org, Product product)
        {
            if (product == null)
                return roleString.Replace("{organisation}", org.Name);
            else
                return roleString.Replace("{organisation}", org.Name).Replace("{product}", product.Name);
        }

    }
}
