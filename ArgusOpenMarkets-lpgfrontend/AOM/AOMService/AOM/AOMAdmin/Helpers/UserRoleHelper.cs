﻿using System;
using System.Collections.Generic;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;


namespace AOMAdmin.Helpers
{
    public class UserRoleHelper
    {
        private ICrmAdministrationService _crmAdministrationService;
        private IOrganisationService _organisationService;

        public UserRoleHelper(ICrmAdministrationService crmAdministrationService, IOrganisationService organisationService)
        {
            _crmAdministrationService = crmAdministrationService;
            _organisationService = organisationService;
        }

        public string CreateUserRole(UserInfoRole userInfoRole)
        {
            try
            {
                bool success = _crmAdministrationService.AddRoleToUser(userInfoRole.UserId, userInfoRole.RoleId, AdminSettings.LoggedOnUser.Id);
                if (success)
                {
                    return string.Format("RoleId: {0} created for user: {1}", userInfoRole.RoleId, userInfoRole.UserId);
                }
                else
                {
                    return string.Format("Failed to create role: {0} for user: {1}", userInfoRole.RoleId, userInfoRole.UserId);
                }
            }
            catch (Exception ex)
            {
                return string.Format("Error occurred creating role: {0} for user: {1} - ", userInfoRole.RoleId, userInfoRole.UserId, ex.Message);
            }


        }
    }
}
