﻿using System;
using System.Data.Entity.Validation;
using System.Text;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.CrmService;

namespace AOMAdmin.Helpers
{
    public class UserHelper
    {
        private ICrmAdministrationService _crmAdministrationService;
        private IEncryptionService _encryptionService;
        private IUserService _userService;
        private ICredentialsService _credentialsService;

        public UserHelper(ICrmAdministrationService crmAdministrationService, IEncryptionService encryptionService, IUserService userService, ICredentialsService credentialsService)
        {
            _crmAdministrationService = crmAdministrationService;
            _encryptionService = encryptionService;
            _credentialsService = credentialsService;
            _userService = userService;
        }

        public string CreateUser(UserCredentials userCredentials, string password)
        {
            try
            {
                var result = _crmAdministrationService.CreateUserWithCredentials(
                    userCredentials.User,
                    userCredentials,
                    AdminSettings.LoggedOnUser.Id);

                if (result)
                {
                    var userId = _userService.GetUserId(userCredentials.User.Username);
                    userCredentials.User.Id = userId; //set the new userid in userCredentials object
                    var hashedPassword = _encryptionService.Hash(userCredentials.User, password);

                    // NTB - JIRA- 723 - Segtting check old password to false.
                    _credentialsService.UpdateUserCredentials(
                        userId,
                        hashedPassword,
                        hashedPassword,
                        false,
                        true,
                        AdminSettings.LoggedOnUser.Id,
                        userCredentials.FirstTimeLogin);

                    return string.Format("User Created with id:{0}  The Password for the user is: {1}", userId, password);
                }
                else
                {
                    return string.Format("Error Occurred creating the user with username: {0}", userCredentials.User.Username);
                }
            }
            catch (DbEntityValidationException evex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in evex.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type {0} in state {1} has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("\t - Property: {0}, Error: {1}", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                return string.Format("Error Occurred creating the user with username: {0}  Exception: {1}", userCredentials.User.Username, ex.ToString());
            }
        }
    }
}