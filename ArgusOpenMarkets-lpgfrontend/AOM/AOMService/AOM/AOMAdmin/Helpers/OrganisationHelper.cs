﻿using System;
using System.Collections.Generic;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;

namespace AOMAdmin.Helpers
{
    public class OrganisationHelper
    {
        private ICrmAdministrationService _crmAdministrationService;
        private IOrganisationService _organisationService;

        public OrganisationHelper(ICrmAdministrationService crmAdministrationService, IOrganisationService organisationService)
        {
            _crmAdministrationService = crmAdministrationService;
            _organisationService = organisationService;
        }

        public Dictionary<Organisation, string> CreateOrganisation(Organisation newOrganisation)
        {
            try
            {
                long organisationId = _crmAdministrationService.SaveOrganisation(newOrganisation, AdminSettings.LoggedOnUser.Id, _organisationService);

                if (organisationId != -1)
                {
                    newOrganisation.Id = organisationId;
                    return new Dictionary<Organisation, string>() { { newOrganisation, string.Format("Organisation created with id:{0}", organisationId) } };
                }
                else
                {
                    return new Dictionary<Organisation, string>() { { null, string.Format("Error Occurred creating Organisation name:{0}", newOrganisation.Name) } };
                }
            }
            catch (Exception ex)
            {
                return new Dictionary<Organisation, string>() { { null, string.Format("Error Occurred creating Organisation name:{0}, Exception: {1}, StackTrace: {2}", newOrganisation.Name, ex.ToString(), ex.StackTrace) } };
            }
        }

    }
}
