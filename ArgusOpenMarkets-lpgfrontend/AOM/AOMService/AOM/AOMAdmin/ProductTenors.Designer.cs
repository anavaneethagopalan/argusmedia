﻿namespace AOMAdmin
{
    partial class ProductTenors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductTenors));
            this.grid = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCreated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryDateStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryDateEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RollDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoleDateRule = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinimumDeliveryRange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductDisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.save = new System.Windows.Forms.Button();
            this.refresh = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.TenorName,
            this.DateCreated,
            this.DisplayName,
            this.DeliveryDateStart,
            this.DeliveryDateEnd,
            this.RollDate,
            this.RoleDateRule,
            this.MinimumDeliveryRange,
            this.ProductId,
            this.ProductDisplayName});
            this.grid.Location = new System.Drawing.Point(12, 12);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(1030, 70);
            this.grid.TabIndex = 0;
            this.grid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellValueChanged);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 30;
            // 
            // TenorName
            // 
            this.TenorName.DataPropertyName = "TenorName";
            this.TenorName.HeaderText = "Name";
            this.TenorName.Name = "TenorName";
            this.TenorName.Width = 80;
            // 
            // DateCreated
            // 
            this.DateCreated.DataPropertyName = "DateCreated";
            this.DateCreated.HeaderText = "DateCreated";
            this.DateCreated.Name = "DateCreated";
            this.DateCreated.ReadOnly = true;
            this.DateCreated.Width = 80;
            // 
            // DisplayName
            // 
            this.DisplayName.DataPropertyName = "DisplayName";
            this.DisplayName.HeaderText = "DisplayName";
            this.DisplayName.Name = "DisplayName";
            this.DisplayName.Width = 80;
            // 
            // DeliveryDateStart
            // 
            this.DeliveryDateStart.DataPropertyName = "DeliveryDateStart";
            this.DeliveryDateStart.HeaderText = "DeliveryDateStart";
            this.DeliveryDateStart.Name = "DeliveryDateStart";
            // 
            // DeliveryDateEnd
            // 
            this.DeliveryDateEnd.DataPropertyName = "DeliveryDateEnd";
            this.DeliveryDateEnd.HeaderText = "DeliveryDateEnd";
            this.DeliveryDateEnd.Name = "DeliveryDateEnd";
            // 
            // RollDate
            // 
            this.RollDate.DataPropertyName = "RollDate";
            this.RollDate.HeaderText = "RollDate";
            this.RollDate.Name = "RollDate";
            this.RollDate.Width = 60;
            // 
            // RoleDateRule
            // 
            this.RoleDateRule.DataPropertyName = "RollDateRule";
            this.RoleDateRule.HeaderText = "RoleDateRule";
            this.RoleDateRule.Name = "RoleDateRule";
            this.RoleDateRule.Width = 80;
            // 
            // MinimumDeliveryRange
            // 
            this.MinimumDeliveryRange.DataPropertyName = "MinimumDeliveryRange";
            this.MinimumDeliveryRange.HeaderText = "Minimum Delivery Period";
            this.MinimumDeliveryRange.Name = "MinimumDeliveryRange";
            this.MinimumDeliveryRange.Width = 120;
            // 
            // ProductId
            // 
            this.ProductId.DataPropertyName = "ProductId";
            this.ProductId.HeaderText = "ProductId";
            this.ProductId.Name = "ProductId";
            this.ProductId.ReadOnly = true;
            this.ProductId.Width = 80;
            // 
            // ProductDisplayName
            // 
            this.ProductDisplayName.DataPropertyName = "ProductName";
            this.ProductDisplayName.HeaderText = "Product Name";
            this.ProductDisplayName.Name = "ProductDisplayName";
            this.ProductDisplayName.ReadOnly = true;
            this.ProductDisplayName.Width = 160;
            // 
            // save
            // 
            this.save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.save.Location = new System.Drawing.Point(139, 88);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(89, 23);
            this.save.TabIndex = 2;
            this.save.Text = "Save Changes";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // refresh
            // 
            this.refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refresh.Location = new System.Drawing.Point(12, 88);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(121, 23);
            this.refresh.TabIndex = 1;
            this.refresh.Text = "Refresh Tenors";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(948, 88);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 23);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // ProductTenors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 118);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.save);
            this.Controls.Add(this.grid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductTenors";
            this.Text = "Manage Product Tenors";
            this.Load += new System.EventHandler(this.ManageProductTenor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCreated;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryDateStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryDateEnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn RollDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoleDateRule;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinimumDeliveryRange;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductDisplayName;
        private System.Windows.Forms.Button cmdCancel;
    }
}