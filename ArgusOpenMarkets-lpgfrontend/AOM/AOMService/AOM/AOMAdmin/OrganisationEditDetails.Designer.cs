﻿using System.Security.AccessControl;

namespace AOMAdmin
{
    partial class OrganisationEditDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrganisationEditDetails));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmdSave = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cboOrganisations = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmdNewOrganisation = new System.Windows.Forms.Button();
            this.lblId = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtShortCode = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtLegalName = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.cboOrganisationType = new System.Windows.Forms.ComboBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cmdSubscribedPrivs = new System.Windows.Forms.Button();
            this.cmdRoles = new System.Windows.Forms.Button();
            this.cmdUsers = new System.Windows.Forms.Button();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgProducts = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSubscribe = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colConfigurePrivs = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProducts)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Short Code:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Legal Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Address:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Organisation Type:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Description:";
            // 
            // cmdSave
            // 
            this.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSave.Location = new System.Drawing.Point(310, 461);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(75, 23);
            this.cmdSave.TabIndex = 14;
            this.cmdSave.Text = "Save";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(400, 461);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 15;
            this.cmdCancel.Text = "&Close";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cboOrganisations
            // 
            this.cboOrganisations.FormattingEnabled = true;
            this.cboOrganisations.Location = new System.Drawing.Point(126, 8);
            this.cboOrganisations.Name = "cboOrganisations";
            this.cboOrganisations.Size = new System.Drawing.Size(190, 21);
            this.cboOrganisations.TabIndex = 0;
            this.cboOrganisations.SelectedIndexChanged += new System.EventHandler(this.cboOrganisations_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Existing Organisations:";
            // 
            // cmdNewOrganisation
            // 
            this.cmdNewOrganisation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdNewOrganisation.Location = new System.Drawing.Point(341, 8);
            this.cmdNewOrganisation.Name = "cmdNewOrganisation";
            this.cmdNewOrganisation.Size = new System.Drawing.Size(137, 23);
            this.cmdNewOrganisation.TabIndex = 1;
            this.cmdNewOrganisation.Text = "New Organisation";
            this.cmdNewOrganisation.UseVisualStyleBackColor = true;
            this.cmdNewOrganisation.Click += new System.EventHandler(this.cmdNewOrganisation_Click);
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(7, 56);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(19, 13);
            this.lblId.TabIndex = 17;
            this.lblId.Text = "Id:";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(7, 466);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(53, 13);
            this.lblMessage.TabIndex = 25;
            this.lblMessage.Text = "Message:";
            // 
            // txtShortCode
            // 
            this.txtShortCode.Location = new System.Drawing.Point(126, 79);
            this.txtShortCode.Name = "txtShortCode";
            this.txtShortCode.Size = new System.Drawing.Size(202, 20);
            this.txtShortCode.TabIndex = 3;
            this.txtShortCode.TextChanged += new System.EventHandler(this.txtShortCode_TextChanged);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(126, 105);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(202, 20);
            this.txtName.TabIndex = 4;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // txtLegalName
            // 
            this.txtLegalName.Location = new System.Drawing.Point(126, 131);
            this.txtLegalName.Name = "txtLegalName";
            this.txtLegalName.Size = new System.Drawing.Size(349, 20);
            this.txtLegalName.TabIndex = 5;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(126, 184);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(352, 20);
            this.txtAddress.TabIndex = 7;
            // 
            // cboOrganisationType
            // 
            this.cboOrganisationType.FormattingEnabled = true;
            this.cboOrganisationType.Location = new System.Drawing.Point(126, 210);
            this.cboOrganisationType.Name = "cboOrganisationType";
            this.cboOrganisationType.Size = new System.Drawing.Size(121, 21);
            this.cboOrganisationType.TabIndex = 8;
            this.cboOrganisationType.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(126, 237);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(352, 20);
            this.txtDescription.TabIndex = 9;
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkIsDeleted.Location = new System.Drawing.Point(126, 263);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(71, 17);
            this.chkIsDeleted.TabIndex = 10;
            this.chkIsDeleted.Text = "Is Deleted";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(125, 53);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(202, 20);
            this.txtId.TabIndex = 2;
            // 
            // cmdSubscribedPrivs
            // 
            this.cmdSubscribedPrivs.Enabled = false;
            this.cmdSubscribedPrivs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSubscribedPrivs.Location = new System.Drawing.Point(127, 427);
            this.cmdSubscribedPrivs.Name = "cmdSubscribedPrivs";
            this.cmdSubscribedPrivs.Size = new System.Drawing.Size(108, 23);
            this.cmdSubscribedPrivs.TabIndex = 11;
            this.cmdSubscribedPrivs.Text = "Subscribed Privs ...";
            this.cmdSubscribedPrivs.UseVisualStyleBackColor = true;
            this.cmdSubscribedPrivs.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmdRoles
            // 
            this.cmdRoles.Enabled = false;
            this.cmdRoles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRoles.Location = new System.Drawing.Point(245, 427);
            this.cmdRoles.Name = "cmdRoles";
            this.cmdRoles.Size = new System.Drawing.Size(109, 23);
            this.cmdRoles.TabIndex = 12;
            this.cmdRoles.Text = "Roles ...";
            this.cmdRoles.UseVisualStyleBackColor = true;
            this.cmdRoles.Click += new System.EventHandler(this.cmdRoles_Click);
            // 
            // cmdUsers
            // 
            this.cmdUsers.Enabled = false;
            this.cmdUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdUsers.Location = new System.Drawing.Point(365, 427);
            this.cmdUsers.Name = "cmdUsers";
            this.cmdUsers.Size = new System.Drawing.Size(111, 23);
            this.cmdUsers.TabIndex = 13;
            this.cmdUsers.Text = "Users ...";
            this.cmdUsers.UseVisualStyleBackColor = true;
            this.cmdUsers.Click += new System.EventHandler(this.cmdUsers_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(126, 157);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(202, 20);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 160);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Email:";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(12, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(463, 69);
            this.label9.TabIndex = 26;
            this.label9.Text = "Warning - creating a New Organisation will cause the Organisations topic tree" +
    " to be rebuilt.  DO NOT USE During Trading Hours.";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dgProducts);
            this.groupBox1.Location = new System.Drawing.Point(11, 287);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(464, 134);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Subscribed Products";
            // 
            // dgProducts
            // 
            this.dgProducts.AllowUserToAddRows = false;
            this.dgProducts.AllowUserToDeleteRows = false;
            this.dgProducts.AllowUserToResizeColumns = false;
            this.dgProducts.AllowUserToResizeRows = false;
            this.dgProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgProducts.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgProducts.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colName,
            this.colSubscribe,
            this.colConfigurePrivs});
            this.dgProducts.Location = new System.Drawing.Point(6, 19);
            this.dgProducts.Name = "dgProducts";
            this.dgProducts.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgProducts.RowHeadersWidth = 20;
            this.dgProducts.Size = new System.Drawing.Size(444, 109);
            this.dgProducts.TabIndex = 0;
            this.dgProducts.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProducts_CellContentClick);
            // 
            // colId
            // 
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.Visible = false;
            // 
            // colName
            // 
            this.colName.HeaderText = "Product";
            this.colName.Name = "colName";
            this.colName.Width = 200;
            // 
            // colSubscribe
            // 
            this.colSubscribe.HeaderText = "Subscribed?";
            this.colSubscribe.Name = "colSubscribe";
            this.colSubscribe.Width = 70;
            // 
            // colConfigurePrivs
            // 
            this.colConfigurePrivs.HeaderText = "Configure Privileges";
            this.colConfigurePrivs.Name = "colConfigurePrivs";
            this.colConfigurePrivs.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colConfigurePrivs.Width = 120;
            // 
            // OrganisationEditDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 496);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmdUsers);
            this.Controls.Add(this.cmdRoles);
            this.Controls.Add(this.cmdSubscribedPrivs);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.cmdNewOrganisation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cboOrganisations);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.chkIsDeleted);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboOrganisationType);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtLegalName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtShortCode);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "OrganisationEditDetails";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Organisation";
            this.Load += new System.EventHandler(this.CreateOrganisation_Load);
            this.Shown += new System.EventHandler(this.EditOrganisation_Shown);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProducts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.ComboBox cboOrganisations;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button cmdNewOrganisation;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TextBox txtShortCode;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtLegalName;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.ComboBox cboOrganisationType;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Button cmdSubscribedPrivs;
        private System.Windows.Forms.Button cmdRoles;
        private System.Windows.Forms.Button cmdUsers;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgProducts;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colSubscribe;
        private System.Windows.Forms.DataGridViewButtonColumn colConfigurePrivs;
    }
}