﻿using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using AOM.App.Domain.Interfaces;

namespace AOMAdminHelper
{
    public class RoleHelper
    {
        public bool DoesUserHaveRole(IList<long> userRoles, IOrganisationRole organisationRole)
        {
            if (userRoles == null)
            {
                return false;
            }

            foreach (var urId in userRoles)
            {
                if (urId == organisationRole.Id)
                {
                    return true;
                }
            }

            return false;
        }
        

    }
}
