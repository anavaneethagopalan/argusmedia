﻿namespace SystemLoadTester
{
    using AOM.Transport.Service.Processor.Common;

    using Ninject;
    using Ninject.Modules;

    using System;
    using System.Configuration;

    using SystemLoadTester.Consumers;
    using SystemLoadTester.Internals;

    using Utils.Logging.Utils;

    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<ISubscriptions>().To<Subscriptions>();
            kernel.Bind<IServiceManagement>().To<ServiceManagement>();
            kernel.Bind<IServiceInformation>().To<ServiceInformation>();

            kernel.Bind<IConsumer>().To<SystemLoadCreator>();

            switch (ConfigurationManager.AppSettings["RunMode"])
            {
                case "classic":
                {
                    kernel.Bind<IConsumer>().To<OrderConsumer>();
                    break;
                }
                case "kill_immediate":
                {
                    kernel.Bind<IConsumer>().To<KillAllOrdersImmediatelyConsumer>();
                    break;
                }
                default:
                {
                    Log.Error("Unrecognised RunMode setting. Exiting...");
                    throw new ApplicationException("Unrecognised RunMode setting.");
                }

            }
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}