﻿using System;
using System.Collections.Concurrent;

namespace SystemLoadTester.Internals
{
    static class OrderTracker 
    {
        static readonly ConcurrentDictionary<string, DateTime> OrdersCreated = new ConcurrentDictionary<string, DateTime>();
        public static ConcurrentDictionary<string, DateTime> TrackedOrders { get { return OrdersCreated; } }
    }
  
}
