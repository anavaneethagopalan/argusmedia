﻿using AOM.Transport.Service.Processor.Common;

namespace SystemLoadTester.Internals
{
    internal class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get
            {
                return DisplayName.Replace(" ", "");
            }
        }

        public string DisplayName
        {
            get
            {
                return "AOM Transport Service System Load Tester Processor";
            }
        }

        public string Description
        {
            get
            {
                return "AOM Transport Service System Load Tester Processor";
            }
        }
    }
}