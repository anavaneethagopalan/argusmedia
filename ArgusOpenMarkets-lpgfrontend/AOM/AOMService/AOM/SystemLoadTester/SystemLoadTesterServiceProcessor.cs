﻿namespace SystemLoadTester
{
    using System.Collections.Generic;

    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    public class SystemLoadTesterService : ProcessorServiceBase
    {
        public SystemLoadTesterService(
            IServiceManagement serviceManagement,
            IEnumerable<IConsumer> consumers,
            IBus aomBus)
            : base(consumers, serviceManagement, aomBus)
        {
        }

        public override string Name
        {
            get { return "SystemLoadTesterService"; }
        }
    }
}