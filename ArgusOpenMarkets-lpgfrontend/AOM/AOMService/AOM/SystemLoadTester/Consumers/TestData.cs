﻿namespace SystemLoadTester.Consumers
{
    using System.Configuration;

    static class TestData
    {
        public static int UserAccountForOrderCreation
        {
            get
            {
                var v=ConfigurationManager.AppSettings.Get("UserId");
                return int.Parse(  v ?? "-100");                
            }
        }

        public static int ProductForOrders
        {
            get
            {
                var v = ConfigurationManager.AppSettings.Get("ProductIdForOrders");
                return int.Parse(v ?? "1");
            }
        }

        public static long ProductTenorForOrders
        {
            get
            {
                var v = ConfigurationManager.AppSettings.Get("ProductTenorIdForOrders");
                return int.Parse(v ?? "1");
            }            
        }

        public static long UserOrganisationForOrderCreation
        {
            get
            {
                var v = ConfigurationManager.AppSettings.Get("UserOrganisationId");
                return int.Parse(v ?? "1");
            }
        }

        public static int MSecDelayBetweenOrderCreation
        {
            get
            {
                var v = ConfigurationManager.AppSettings.Get("DelayBetweenOrderCreationMsecs");
                return int.Parse(v ?? "100");
            }
        }

        public static int MaxCountOfOrdersToCreate
        {
            get
            {
                var v = ConfigurationManager.AppSettings.Get("MaxCountOfOrdersToCreate");
                return int.Parse(v ?? "100");
            }
        }

        public static string OrderType
        {
            get
            {
                var v = ConfigurationManager.AppSettings.Get("CreatedBidsOrAsks");
                return string.IsNullOrEmpty(v) ? "B" : v;
            }
        }

        public static decimal GetQuantity()
        {
            var v = ConfigurationManager.AppSettings.Get("Quantity");
            return decimal.Parse(v ?? "27000");
        }

        public static decimal GetPrice()
        {
            var v = ConfigurationManager.AppSettings.Get("Price");
            return decimal.Parse(v ?? "900");
        }

        public static int StartDateNumberDaysToAddToToday()
        {
            var v = ConfigurationManager.AppSettings.Get("StartDateNumberDaysToAddToTodaysDate");
            return int.Parse(v ?? "1");
        }

        public static int EndDateNumberDaysToAddToToday()
        {
            var v = ConfigurationManager.AppSettings.Get("EndDateNumberDaysToAddToTodaysDate");
            return int.Parse(v ?? "5");
        }
    }
}
