﻿namespace SystemLoadTester.Consumers
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading.Tasks;

    using AOM.App.Domain.Entities;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    internal class KillAllOrdersImmediatelyConsumer : IConsumer
    {
        protected IBus AomBus;

        private readonly ConcurrentStack<Order> _orders = new ConcurrentStack<Order>();

        private bool _ordersKilled;

        private readonly ClientSessionInfo _csi;

        public KillAllOrdersImmediatelyConsumer(IBus aombus)
        {
            AomBus = aombus;
            _csi = new ClientSessionInfo { UserId = TestData.UserAccountForOrderCreation, OrganisationId = TestData.UserOrganisationForOrderCreation};

        }

        public void Start()
        {
            AomBus.Subscribe<AomOrderResponse>(SubscriptionId, ConsumeOrderRequest);
        }

        private void ConsumeOrderRequest(AomOrderResponse response)
        {
            if (null == response.Order)
            {
                Log.Warn("Recieved null order in ConsumeOrderResponse! Ignoring..");
                return;
            }

            if (!_ordersKilled)
            {
                if (_orders.Count >= 20)
                {
                    Log.Info("25 Orders reached.  Executing Kill all.");
                    _ordersKilled = true;
                    Parallel.ForEach(_orders, (item, loopState) => KillOrder(item, AomBus, _csi));
                    _orders.Clear();

                }
                else
                {
                    Log.Info(String.Format("Order recieved.  There are now {0} orders", _orders.Count));
                    _orders.Push(response.Order);
                }
            }
            else
            {
                Log.Info("Kill all already activated. Killing immediate");
                KillOrder(response.Order, AomBus, _csi);
            }
        }

        private static void KillOrder(Order order, IBus bus, ClientSessionInfo csi)
        {
            order.Notes = order.Notes + "-Killed";
            PublishOrderRequest(MessageAction.Kill, order, csi, bus);
        }

        protected static void PublishOrderRequest(
            MessageAction messageAction,
            Order order,
            ClientSessionInfo csi,
            IBus bus)
        {
            var orderErrorResponse = new AomOrderRequest
                                     {
                                         Order = order,
                                         ClientSessionInfo = csi,
                                         MessageAction = messageAction
                                     };
            bus.Publish(orderErrorResponse);
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM_SystemLoadTester";
            }
        }
    }
}