﻿namespace SystemLoadTester.Consumers
{
    using System;
    using System.Threading;

    using Internals;

    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public class SystemLoadCreator : IConsumer, IDisposable
    {
        private static IDateTimeProvider _dateTimeProvider;

	    private const int MaxBacklog = 1000;

        private static int _orderCreateSoFar;

        private class Token
        {
            public IBus Bus { get; set; }

            public Token(IBus bus)
            {
                Bus = bus;
            }
        }

        private readonly IBus _aomBus;

        private Token _timerToken;

        private static Random _random = new Random();

        public SystemLoadCreator(IBus aomBus, IDateTimeProvider dateTimeProvider)
        {
            _aomBus = aomBus;
            _dateTimeProvider = dateTimeProvider;
        }

        public void Start()
        {
            int callFrequencyMSecs = TestData.MSecDelayBetweenOrderCreation;
            _timerToken = new Token(_aomBus);

	        for (int i = 0; i <= TestData.MaxCountOfOrdersToCreate; i++)
	        {
				string uniqueId = Guid.NewGuid().ToString().Replace('-', '*');

	            var orderType = OrderType.Bid;
	            if (TestData.OrderType == "B")
	            {
	                orderType = OrderType.Bid;
	            }
	            else
	            {
	                orderType = OrderType.Ask;
	            }

				var order = new Order
				{
					DeliveryStartDate = _dateTimeProvider.Now.AddDays(TestData.StartDateNumberDaysToAddToToday()),
					DeliveryEndDate = _dateTimeProvider.Now.AddDays(TestData.EndDateNumberDaysToAddToToday()),
					Notes = string.Format("SystemLoadTester-{0}-Active", uniqueId),
					Price = TestData.GetPrice(),
					ProductId = TestData.ProductForOrders,
                    OrderType = orderType,
					ProductTenor = new ProductTenor { Id = TestData.ProductTenorForOrders, },
					Quantity = TestData.GetQuantity(),
					PrincipalOrganisationId = TestData.UserOrganisationForOrderCreation,
					PrincipalUserId = TestData.UserAccountForOrderCreation,
					BrokerRestriction = BrokerRestriction.Any
				};

				var request = new AomOrderRequest
				{
					Order =
						order,
					ClientSessionInfo =
						new ClientSessionInfo
						{
							UserId =
								TestData
								.UserAccountForOrderCreation,
							OrganisationId = TestData.UserOrganisationForOrderCreation
						},
					MessageAction = MessageAction.Create
				};

				Log.Warn(string.Format("Creating Order Number #{0}", i));
				_timerToken.Bus.Publish(request);

				OrderTracker.TrackedOrders.GetOrAdd(uniqueId, _dateTimeProvider.Now);
				Log.Info(
					string.Format(
						"[Total so far={0}] Published new order : uniqueid {1} as userId {2}: In queue {3}",
						++_orderCreateSoFar,
						uniqueId,
						request.ClientSessionInfo.UserId,
						OrderTracker.TrackedOrders.Count));

                Thread.Sleep(callFrequencyMSecs);
            }
        }

        public void Dispose()
        {
        }
    }
}