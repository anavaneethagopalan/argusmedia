﻿namespace SystemLoadTester.Consumers
{
    using System;
    using Internals;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql;
    using AOM.Services.OrderService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public class OrderConsumer : IConsumer
    {
        protected readonly IDbContextFactory DbFactory;

        protected readonly IOrderService OrderService;

        protected IBus AomBus;
        private readonly IDateTimeProvider _dateTimeProvider;

        private readonly ClientSessionInfo _csi;

        public OrderConsumer(IDbContextFactory dbFactory, IOrderService orderService, IBus aomBus, IDateTimeProvider dateTimeProvider)
        {
            DbFactory = dbFactory;
            OrderService = orderService;
            AomBus = aomBus;
            _dateTimeProvider = dateTimeProvider;

            _csi = new ClientSessionInfo { UserId = TestData.UserAccountForOrderCreation, OrganisationId = TestData.UserOrganisationForOrderCreation };
        }

        private string ExtractIdFromNotes(string notes)
        {
            if (!notes.StartsWith("SystemLoadTester-")) return "";
            string id = notes.Substring(notes.IndexOf('-') + 1);
            return id.Substring(0, id.IndexOf('-'));
        }

        public void Start()
        {
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            AomBus.Subscribe<AomOrderResponse>(SubscriptionId, ConsumeOrderRequest);
        }

        private void ConsumeOrderRequest(AomOrderResponse response)
        {
	        try
	        {
		        if (response.Message.MessageType != MessageType.Error)
		        {
			        var notes = response.Order.Notes;
			        if (string.IsNullOrEmpty(notes) || !notes.StartsWith("SystemLoadTester-")) return;

			        var order = response.Order;

			        string uniqueId = ExtractIdFromNotes(notes);

			        if (notes.EndsWith("-Active"))
			        {
				        if (order.OrderStatus == OrderStatus.Active)
				        {
					        HoldOrder(order);
				        }
				        else
				        {
					        Log.Error(
						        string.Format(
							        "Is market closed? Order {0} has wrong status of {1}",
							        response.Order.Id,
							        response.Order.OrderStatus));
					        KillOrder(order);
				        }
			        }
			        else if (notes.EndsWith("-Held") && order.OrderStatus == OrderStatus.Held)
			        {
				        ReinstateOrder(order);
			        }
			        else if (notes.EndsWith("-Reinstated") && order.OrderStatus == OrderStatus.Active)
			        {
				        KillOrder(order);
			        }
			        else if (notes.EndsWith("-Killed"))
			        {
				        DateTime dtetimeCreatedByLoadTester;
				        OrderTracker.TrackedOrders.TryRemove(uniqueId, out dtetimeCreatedByLoadTester);

				        var totalMilliseconds = _dateTimeProvider.Now.Subtract(dtetimeCreatedByLoadTester).TotalMilliseconds;
				        Log.Info(
					        string.Format(
						        "Killing order uniqueid {0} : OrderId {1} : {2} msecs after it was created",
						        uniqueId,
						        order.Id,
						        totalMilliseconds));
			        }
			        else
			        {
				        Log.Error(
					        string.Format(
						        "Order {0} has unknown notes field {1} for order status of {2}",
						        response.Order.Id,
						        response.Order.Notes,
						        response.Order.OrderStatus));
			        }
		        }
		        else
		        {
			        Log.Error("Error repsponse message received");
			        Log.Error(string.Format("Error for order {0} ", response.Message.MessageBody));
		        }
	        }
	        catch (Exception ex)
	        {
		        Log.Error("Unknown Error", ex);
	        }
        }

        private void KillOrder(Order order)
        {
            order.Notes = order.Notes + "-Killed";
            PublishOrderRequest(MessageAction.Kill, order);
        }

        private void HoldOrder(Order order)
        {
            order.Notes = order.Notes + "-Held";
            PublishOrderRequest(MessageAction.Hold, order);
        }

        private void ReinstateOrder(Order order)
        {
            order.Notes = order.Notes + "-Reinstated";
            PublishOrderRequest(MessageAction.Reinstate, order);
        }

        protected void PublishOrderRequest(MessageAction messageAction, Order order)
        {
	        if (messageAction == MessageAction.Kill)
	        {
		        order.OrderStatus = OrderStatus.Killed;
	        }

	        try
	        {
            string uniqueId = ExtractIdFromNotes(order.Notes);
            Log.Debug(
                string.Format("Setting order uniqueid {0} : orderId {1} : {2}", uniqueId, order.Id, order.OrderStatus));

            var aomOrder = new AomOrderRequest
                                     {
                                         Order = order,
                                         ClientSessionInfo = _csi,
                                         MessageAction = messageAction
                                     };
            AomBus.Publish(aomOrder);
			}
			catch (Exception ex)
			{
				Log.Error("Publish Order", ex);
			}
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM_SystemLoadTester";
            }
        }
    }
}