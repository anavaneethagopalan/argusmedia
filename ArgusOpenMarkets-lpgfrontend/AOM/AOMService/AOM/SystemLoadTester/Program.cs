﻿namespace SystemLoadTester
{
    using AOM.Repository.MySql;
    using AOM.Services;
    using AOM.Transport.Service.Processor.Common;

    using Ninject;

    using System;

    using Utils.Logging.Utils;

    internal class Program
    {
        private static void Main()
        {

            using (var kernel = new StandardKernel())
            {
                try
                {
                    kernel.Load(
                        new EFModelBootstrapper(),
                        new ServiceBootstrap(),
                        new LocalBootstrapper(),
                        new ProcessorCommonBootstrap());
                    ServiceHelper.BuildAndRunService<SystemLoadTesterService>(kernel);
                }
                catch (ApplicationException e)
                {
                    Log.Error(String.Format("Application Exception occured: {0}", e.Message));
                }

            }

	        Log.Error("Application Shutting down");
        }
    }
}