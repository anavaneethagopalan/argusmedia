﻿
using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.OrganisationProcessor.Internals
{
    internal class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get
            {
                return DisplayName.Replace(" ", "");
            }
        }

        public string DisplayName
        {
            get
            {
                return "AOM Transport Service Organisation Processor";
            }
        }

        public string Description
        {
            get
            {
                return "AOM Transport Service Organisation Processor";
            }
        }
    }
}