﻿namespace AOM.Transport.Service.OrganisationProcessor
{
    using AOM.Repository.MySql;
    using AOM.Services;
    using AOM.Transport.Service.Processor.Common;

    using Ninject;

    using System;
    using System.Diagnostics;
    using System.Linq;

    using Utils.Logging.Utils;

    internal class Program
    {
        private static void Main()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(
                    new EFModelBootstrapper(),
                    new LocalBootstrapper(),
                    new ServiceBootstrap(),
                    new ProcessorCommonBootstrap());

                WarmUp(kernel);
                ServiceHelper.BuildAndRunService<OrganisationProcessorService>(kernel);
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (var dbCrm = dbFactory.CreateCrmModel())
                {
                    dbCrm.Organisations.Any();
                }

                Log.Info(
                    string.Format(
                        "OrganisationProcessor -Initial EF model build took {0} msecs.",
                        timer.ElapsedMilliseconds));

            }
            catch (Exception ex)
            {
                Log.Error("OrganisationProcessor - WarmUp - Error - cannot connect to DB", ex);
                ServiceHelper.BuildAndRunService<OrganisationProcessorService>(kernel);
            }
        }
    }
}