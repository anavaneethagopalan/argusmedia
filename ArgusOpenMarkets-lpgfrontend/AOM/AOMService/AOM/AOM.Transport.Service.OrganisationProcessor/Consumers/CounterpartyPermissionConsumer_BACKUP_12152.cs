using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using AOM.Repository.MySql.Crm;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.OrganisationProcessor.Consumers
{
    internal class CounterpartyPermissionConsumer : IConsumer
    {
        protected readonly IOrganisationService OrganisationService;

        private readonly IBus _aomBus;

        public CounterpartyPermissionConsumer(IOrganisationService organisationService, IBus aomBus)
        {
            OrganisationService = organisationService;
            _aomBus = aomBus;
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }

        public void Start()
        {
            _aomBus.Subscribe<GetCounterpartyPermissionsRefreshRequest>(
                SubscriptionId,
                ConsumeCounterpartyPermissionsRefreshRequest);
            _aomBus.Subscribe<GetBrokerPermissionsRefreshRequest>(
                SubscriptionId,
                ConsumeBrokerPermissionsRefreshRequest);
        }

        private void ConsumeBrokerPermissionsRefreshRequest(GetBrokerPermissionsRefreshRequest request)
        {
            try
            {
                IList<BrokerPermissionDto> brokerPermissions = OrganisationService.GetAllBrokerPermissionsReduced();
                var response = new GetBrokerPermissionsRefreshResponse
                {
                    BrokerPermissions = brokerPermissions, 
                    ControlClientActive = request.ControlClientActive
                };
                // _aomBus.Publish(response);
            }
            catch (Exception e)
            {
                Log.Error("GetBrokerPermissionsRefreshRequest", e);
            }
        }

        private void ConsumeCounterpartyPermissionsRefreshRequest(GetCounterpartyPermissionsRefreshRequest request)
        {
            try
            {
<<<<<<< HEAD
                // IList<BilateralMatrixPermissionPair> counterpartyPermissions = OrganisationService.GetAllCounterpartyPermissions();
                IList<CounterpartyPermissionDto> counterpartyPermissions = OrganisationService.GetAllCounterpartyPermissionsReduced();
=======
                IList<BilateralMatrixPermissionPair> counterpartyPermissions =
                    OrganisationService.GetAllCounterpartyPermissions();
>>>>>>> parent of 13cf4d6... changes

                counterpartyPermissions.GroupBy(p => p.ProductId).ToList().ForEach(
                    p =>
                    {
                        var response = new GetCounterpartyPermissionsRefreshResponse
                        {
<<<<<<< HEAD
                            AllCounterpartyPermissions = p.ToList(), 
                            ControlClientActive = request.ControlClientActive
=======
                            AllCounterpartyPermissions =
                                p.ToList()
>>>>>>> parent of 13cf4d6... changes
                        };
                        _aomBus.Publish(response);
                    });
            }
            catch (Exception e)
            {
                Log.Error("GetCounterpartyPermissionsRefreshRequest", e);
            }
        }
    }
}