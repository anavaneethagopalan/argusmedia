﻿namespace AOM.Transport.Service.OrganisationProcessor.Consumers
{
    using AOM.Services.ProductService;
    using App.Domain.Entities;
    using App.Domain.Services;
    using Argus.Transport.Infrastructure;
    using Events.Organisations;
    using Processor.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Utils.Logging.Utils;

    internal class OrganisationConsumer : IConsumerWithInitialise
    {
        protected readonly IUserService _userService;

        protected readonly IOrganisationService _organisationService;

        protected readonly ICrmAdministrationService _crmAdministrationService;

        protected readonly IProductService _productService;


        private IBus _aomBus;

        public OrganisationConsumer(
            IOrganisationService organisationService,
            IBus aomBus,
            ICrmAdministrationService crmAdministrationService,
            IUserService userService,
            IProductService productService)
        {
            _organisationService = organisationService;
            _aomBus = aomBus;
            _crmAdministrationService = crmAdministrationService;
            _userService = userService;
            _productService = productService;
        }

        public OrganisationConsumer(IBus aomBus, ICrmAdministrationService crmAdministrationService)
        {
            _aomBus = aomBus;
            _crmAdministrationService = crmAdministrationService;
        }

        public void Start()
        {
            _aomBus.Respond<SaveOrganisationRequestRpc, SaveOrganisationResponse>(SaveOrganisation);
            _aomBus.Respond<GetOrganisationsRequestRpc, GetOrganisationsResponse>(GetOrganisations);
            _aomBus.Respond<GetOrganisationRequestRpc, GetOrganisationResponse>(GetOrganisation);
            _aomBus.Respond<GetBrokersForPrincipalRpc, GetBrokersForPrincipalResponse>(GetBrokersForPrincipal);
            _aomBus.Respond<GetBrokersForPrincipalBuyOrSellRpc, GetBrokersForPrincipalBuyOrSellResponse>(GetBrokersForPrincipalBuyOrSell);
            _aomBus.Respond<GetPrincipalsForBrokerRpc, GetPrincipalsForBrokerResponse>(GetPrincipalsForBroker);
            _aomBus.Respond<GetPrincipalsForBrokerBuyOrSellRpc, GetPrincipalsForBrokerBuyOrSellResponse>(GetPrincipalsForBrokerBuyOrSell);
            _aomBus.Respond<GetCommonPermissionedOrganisationsRpc, GetCommonPermissionedOrganisationsResponse>(GetCommonPermissionedOrganisations);

        }

        private GetCommonPermissionedOrganisationsResponse GetCommonPermissionedOrganisations(GetCommonPermissionedOrganisationsRpc request)
        {
            try
            {
                var commonBrokers = _organisationService.GetCommonPermissionedOrganisations(request.RequestedByUserId, request.ProductId,
                    request.OrganisationId, request.OrganisationBrokerageId, request.BuyOrSell);

                return new GetCommonPermissionedOrganisationsResponse { CommonOrganisations = commonBrokers };
            }
            catch (Exception e)
            {
                Log.Error("GetCommonPermissionedBrokers", e);
                return new GetCommonPermissionedOrganisationsResponse { CommonOrganisations = new List<Organisation>() };
            }
        }

        private GetBrokersForPrincipalResponse GetBrokersForPrincipal(GetBrokersForPrincipalRpc request)
        {
            try
            {
                var brokersForPrincipal = _organisationService.GetPermissionedBrokersForUsersOrganisation(request.RequestedByUserId, request.ProductId);
                return new GetBrokersForPrincipalResponse { BrokersWithPermissions = brokersForPrincipal };
            }
            catch (Exception e)
            {
                Log.Error("GetBrokersForPrincipal", e);
                return new GetBrokersForPrincipalResponse { BrokersWithPermissions = new OrganisationsWithPermissions() };
            }
        }

        private GetBrokersForPrincipalBuyOrSellResponse GetBrokersForPrincipalBuyOrSell(GetBrokersForPrincipalBuyOrSellRpc req)
        {
            return new GetBrokersForPrincipalBuyOrSellResponse
            {
                BrokersWithPermissions = _organisationService.GetPermissionedBrokersForUsersOrganisationBuyOrSell(
                    req.RequestedByUserId,
                    req.ProductId, req.OrderType)
            };
        }

        private GetPrincipalsForBrokerResponse GetPrincipalsForBroker(GetPrincipalsForBrokerRpc request)
        {
            try
            {
                var principalsForBroker = _organisationService.GetPermissionedPrincipalsForUsersBrokerage(request.RequestedByUserId, request.ProductId);
                return new GetPrincipalsForBrokerResponse { PrincipalsWithPermissions = principalsForBroker };
            }
            catch (Exception e)
            {

                Log.Error("", e);
                return new GetPrincipalsForBrokerResponse { PrincipalsWithPermissions = new OrganisationsWithPermissions() };
            }
        }

        private GetPrincipalsForBrokerBuyOrSellResponse GetPrincipalsForBrokerBuyOrSell(GetPrincipalsForBrokerBuyOrSellRpc request)
        {
            try
            {
                var organisations = _organisationService.GetPermissionedPrincipalsForUsersBrokerageBuyOrSell(request.RequestedByUserId, request.ProductId, request.OrderType);
                return new GetPrincipalsForBrokerBuyOrSellResponse { Organisations = organisations };
            }
            catch (Exception e)
            {

                Log.Error("", e);
                return new GetPrincipalsForBrokerBuyOrSellResponse { Organisations = new List<OrganisationPermissionSummary>() };
            }
        }

        private GetOrganisationResponse GetOrganisation(GetOrganisationRequestRpc request)
        {
            try
            {
                var org = _organisationService.GetOrganisationById(request.Id);
                var response = new GetOrganisationResponse { Organisation = org as Organisation };
                return response;
            }
            catch (Exception e)
            {
                Log.Error("GetOrganisation", e);
                return new GetOrganisationResponse { Organisation = null };
            }

        }

        private GetOrganisationsResponse GetOrganisations(GetOrganisationsRequestRpc request)
        {
            try
            {
                return new GetOrganisationsResponse { Organisations = _organisationService.GetOrganisations() };
            }
            catch (Exception e)
            {

                Log.Error("GetOrganisations", e);
                return new GetOrganisationsResponse { Organisations = new List<Organisation>() };
            }
        }

        internal SaveOrganisationResponse SaveOrganisation(SaveOrganisationRequestRpc createOrgRequest)
        {
            try
            {
                long orgId = _crmAdministrationService.SaveOrganisation(createOrgRequest.Organisation, createOrgRequest.UserId, _organisationService);
                return new SaveOrganisationResponse { OrganisationId = orgId };

            }
            catch (Exception e)
            {
                Log.Error("SaveOrganisation", e);
                return new SaveOrganisationResponse { OrganisationId = -1 };
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }

        public bool RequiresArgusBus
        {
            get
            {
                return false;
            }
        }

        public void InitialiseAndWarmupConsumer()
        {
            var organisation = _organisationService.GetOrganisations().FirstOrDefault();
            if (organisation != null)
            {
                var users = _userService.GetUsers(organisation.Id);
                if (users != null && users.Count >= 0)
                {
                    var products = _productService.GetAllProducts();
                    if (products != null && products.Count >= 0)
                    {
                        var product = products[0];
                        var user = users[0];
                        _organisationService.GetAllBrokersForUsersOrganisation(user.Id, product.ProductId);
                    }
                }
            }
        }
    }
}