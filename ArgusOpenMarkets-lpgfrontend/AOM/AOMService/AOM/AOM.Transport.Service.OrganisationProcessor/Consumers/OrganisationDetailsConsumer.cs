﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.AuthenticationService;
using AOM.Services.ProductService;
using AOM.Services.SystemNotificationService;
using AOM.Transport.Events;
using AOM.Transport.Events.CompanySettings;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using System.Diagnostics;
using System.Linq;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.OrganisationProcessor.Consumers
{
    internal class OrganisationDetailsConsumer : IConsumer
    {
        private IBus _bus;

        protected readonly IDbContextFactory _dbContextFactory;

        protected readonly IOrganisationService OrganisationService;

        protected readonly IProductService _productService;

        protected readonly ISystemNotificationService _systemNotificationService;

        protected readonly ICompanySettingsAuthenticationService _companySettingsAuthenticationService;

        public OrganisationDetailsConsumer(
            IOrganisationService organisationService,
            IBus aomBus,
            IDbContextFactory dbContextFactory,
            IProductService productService,
            ISystemNotificationService systemNotificationService,
            ICompanySettingsAuthenticationService companySettingsAuthenticationService)
        {
            _dbContextFactory = dbContextFactory;
            OrganisationService = organisationService;
            _productService = productService;
            _systemNotificationService = systemNotificationService;
            _companySettingsAuthenticationService = companySettingsAuthenticationService;
            _bus = aomBus;
        }

        public void Start()
        {
            _bus.Subscribe<GetCompanyDetailsRefreshRequest>(SubscriptionId, ConsumeGetCompanyDetailRequest);

            _bus.Subscribe<CompanyDetailsSystemNotificationRequest>(SubscriptionId, CompanyDetailsSystemNotificationRequest);

            _bus.Respond<CompanyDetailsRpcRequest, CompanyDetailsRpcProcessedResponse>(ConsumeCompanyDetailsRequest);
        }

        private void CompanyDetailsSystemNotificationRequest(CompanyDetailsSystemNotificationRequest companyDetailsSystemNotificationRequest)
        {
            if (companyDetailsSystemNotificationRequest == null)
            {
                Log.ErrorFormat("CompanyDetailsSystemNotificationRequest - Was called with a NULL object.");
            }

            var authResult = _companySettingsAuthenticationService.AuthenticateCompanySettingsPermissionRequest(companyDetailsSystemNotificationRequest.ClientSessionInfo);

            if (authResult.Allow)
            {
                Log.Info("ConsumeCompanyDetailsRequest Called: " + companyDetailsSystemNotificationRequest);

                switch (companyDetailsSystemNotificationRequest.MessageAction)
                {
                    case MessageAction.Delete:

                        if (
                            _systemNotificationService.DeleteSystemNotification(companyDetailsSystemNotificationRequest.Id, companyDetailsSystemNotificationRequest.ClientSessionInfo.UserId))
                        {
                            // obj.ClientSessionInfo.OrganisationId
                            // NOTE: USED TO SET THE FOLLOWING: 
                            //                        response.EventTypeId = notification.SystemEventId;
                            //                        response.OrganisationId = notification.OrganisationId_fk;
                            //                        crmModel.SystemEventNotifications.Remove(notification);
                            PublishOrganisationSystemNotificationChanged(companyDetailsSystemNotificationRequest.Id,
                                string.Empty, companyDetailsSystemNotificationRequest.EventType, -1,
                                companyDetailsSystemNotificationRequest.ClientSessionInfo.OrganisationId,
                                companyDetailsSystemNotificationRequest.ClientSessionInfo.UserId, "DELETE");
                        }

                        break;

                    case MessageAction.Create:

                        var newNotificationId =
                            _systemNotificationService.CreateSystemNotification(companyDetailsSystemNotificationRequest.EmailAddress,
                                companyDetailsSystemNotificationRequest.EventType, companyDetailsSystemNotificationRequest.ProductId,
                                companyDetailsSystemNotificationRequest.ClientSessionInfo.OrganisationId, companyDetailsSystemNotificationRequest.ClientSessionInfo.UserId);

                        if (newNotificationId > 0)
                        {
                            PublishOrganisationSystemNotificationChanged(newNotificationId, companyDetailsSystemNotificationRequest.EmailAddress,
                                companyDetailsSystemNotificationRequest.EventType, companyDetailsSystemNotificationRequest.ProductId,
                                companyDetailsSystemNotificationRequest.ClientSessionInfo.OrganisationId, companyDetailsSystemNotificationRequest.ClientSessionInfo.UserId, "CREATE");
                        }

                        break;

                    default:
                        Log.Error("Unknown Message Action type for Company Details Request!");
                        break;
                }
            }
        }

        private void PublishOrganisationSystemNotificationChanged(long notificationId, string emailAddress,
            SystemEventType systemEventType, long productId, long organisationId, long userId, string operation)
        {
            if (operation == "CREATE")
            {
                _bus.Publish(new GetCompanyDetailsRefreshRequest());
            }
            else
            {
                var response = new CompanyDetailsSystemNotificationDeletedResponse
                {
                    Id = notificationId,
                    EmailAddress = emailAddress,
                    EventType = systemEventType,
                    ProductId = productId,
                    OrganisationId = organisationId,
                    UserId = userId
                };

                _bus.Publish(response);
            }
        }

        internal CompanyDetailsRpcProcessedResponse ConsumeCompanyDetailsRequest(CompanyDetailsRpcRequest obj)
        {

            Log.Info("ConsumeCompanyDetailsRequest Called: " + obj);
            var authRequest = new AomCompanySettingsAuthenticationRpcRequest()
            {
                ClientSessionInfo = obj.ClientSessionInfo,
                CompanyDetails = obj,
                MessageAction = obj.MessageAction
            };
            var authResponse =_bus.Request<AomCompanySettingsAuthenticationRpcRequest, AomCompanySettingsAuthenticationRpcResponse>(authRequest);

            switch (authResponse.CompanyDetails.MessageAction)
            {
                case MessageAction.Delete:
//                    return DeleteSystemNotification(
//                        authResponse.CompanyDetails.Id,
//                        authResponse.CompanyDetails.ClientSessionInfo.UserId);
                    if (_systemNotificationService.DeleteSystemNotification(authResponse.CompanyDetails.Id, obj.ClientSessionInfo.UserId))
                    {
                        // NOTE: USED TO SET THE FOLLOWING: 
//                        response.EventTypeId = notification.SystemEventId;
//                        response.OrganisationId = notification.OrganisationId_fk;
//                        crmModel.SystemEventNotifications.Remove(notification);

                        var response = new CompanyDetailsRpcProcessedResponse()
                        {
                            Message = new Message { MessageAction = MessageAction.Delete },
                            Id = authResponse.CompanyDetails.Id
                        };

                        return response;
                    }

                    break;

                case MessageAction.Create:

                    var result = _systemNotificationService.CreateSystemNotification(obj.EmailAddress, obj.EventType, obj.ProductId, 
                        obj.ClientSessionInfo.OrganisationId, obj.ClientSessionInfo.UserId);

                    if (result > 0)
                    {
                        return new CompanyDetailsRpcProcessedResponse()
                        {
                            Message = new Message {MessageAction = MessageAction.Create},
                            EventTypeId = (long) obj.EventType
                        };
                    }

                    break;

                default:
                    Log.Error("Unknown Message Action type for Company Details Request!" + authResponse);
                    return null;
            }

            return null;
        }

        internal void ConsumeGetCompanyDetailRequest(GetCompanyDetailsRefreshRequest request)
        {
            var sw = new Stopwatch();
            sw.Start();

            var products = _productService.GetAllProducts();
            var organisationDetails = OrganisationService.GetOrganisationDetails(products).ToList();

            sw.Stop();
            Log.InfoFormat("ConsumeGetCompanyDetailRequest Took : {0} ms", sw.ElapsedMilliseconds);

            _bus.Publish(new GetCompanyDetailsRefreshResponse { Details = organisationDetails});
        }

        public String SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}