using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.OrganisationProcessor.Consumers
{
    internal class CounterpartyPermissionConsumer : IConsumer
    {
        protected readonly IOrganisationService OrganisationService;

        private readonly IBus _aomBus;

        public CounterpartyPermissionConsumer(IOrganisationService organisationService, IBus aomBus)
        {
            OrganisationService = organisationService;
            _aomBus = aomBus;
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }

        public void Start()
        {
            _aomBus.Subscribe<GetCounterpartyPermissionsRefreshRequest>(
                SubscriptionId,
                ConsumeCounterpartyPermissionsRefreshRequest);
            _aomBus.Subscribe<GetBrokerPermissionsRefreshRequest>(
                SubscriptionId,
                ConsumeBrokerPermissionsRefreshRequest);
        }

        private void ConsumeBrokerPermissionsRefreshRequest(GetBrokerPermissionsRefreshRequest request)
        {
            try
            {
                IList<BilateralMatrixPermissionPair> brokerPermissions = OrganisationService.GetAllBrokerPermissions();
                var response = new GetBrokerPermissionsRefreshResponse {BrokerPermissions = brokerPermissions};
                _aomBus.Publish(response);
            }
            catch (Exception e)
            {
                Log.Error("GetBrokerPermissionsRefreshRequest", e);
            }
        }

        private void ConsumeCounterpartyPermissionsRefreshRequest(GetCounterpartyPermissionsRefreshRequest request)
        {
            try
            {
                IList<BilateralMatrixPermissionPair> counterpartyPermissions =
                    OrganisationService.GetAllCounterpartyPermissions();

                counterpartyPermissions.GroupBy(p => p.ProductId).ToList().ForEach(
                    p =>
                    {
                        var response = new GetCounterpartyPermissionsRefreshResponse
                        {
                            AllCounterpartyPermissions =
                                p.ToList()
                        };
                        _aomBus.Publish(response);
                    });
            }
            catch (Exception e)
            {
                Log.Error("GetCounterpartyPermissionsRefreshRequest", e);
            }
        }
    }
}