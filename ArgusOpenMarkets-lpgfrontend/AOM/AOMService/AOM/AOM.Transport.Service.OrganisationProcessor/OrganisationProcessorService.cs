﻿namespace AOM.Transport.Service.OrganisationProcessor
{
    using System.Collections.Generic;

    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    public class OrganisationProcessorService : ProcessorServiceBase
    {

        public OrganisationProcessorService(
            IBus aomBus,
            IEnumerable<IConsumer> consumers,
            IServiceManagement serviceManagement)
            : base(consumers, serviceManagement, aomBus)
        {

        }

        public override string Name
        {
            get { return "OrganisationProcessorService"; }
        }
    }
}