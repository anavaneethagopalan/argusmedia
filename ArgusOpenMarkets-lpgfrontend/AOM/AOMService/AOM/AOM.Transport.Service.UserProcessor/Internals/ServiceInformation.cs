﻿using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.UserProcessor.Internals
{
    internal class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get { return DisplayName.Replace(" ", ""); }
        }

        public string DisplayName
        {
            get { return "AOM Transport Service User Processor"; }
        }

        public string Description
        {
			get { return "AOM Transport Service User Processor"; }
        }
    }
}