﻿using System;
using System.Diagnostics;
using System.Linq;

using AOM.Repository.MySql;
using AOM.Services;
using AOM.Transport.Service.Processor.Common;

using Ninject;

using Utils.Logging.Utils;

namespace AOM.Transport.Service.UserProcessor
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(
                    new EFModelBootstrapper(), 
                    new ServiceBootstrap(),
                    new LocalBootstrapper(),
                    new ProcessorCommonBootstrap());

                WarmUp(kernel);
                ServiceHelper.BuildAndRunService<UserAuthenticationStartup>(kernel);
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (var dbCrm = dbFactory.CreateCrmModel())
                {
                    dbCrm.UserCredentials.Any();
                }

                Log.Info(string.Format("UserProcessor -Initial EF model build took {0} msecs.", timer.ElapsedMilliseconds));
            }
            catch (Exception ex)
            {
                Log.Error("UserProcessor - WarmUp - Error - cannot connect to DB", ex);
                ServiceHelper.BuildAndRunService<UserAuthenticationStartup>(kernel);
            }
        }
    }
}