﻿using AOM.App.Domain.Services;
using AOM.TopicManager;
using AOM.Transport.Service.Processor.Common;
using AOM.Transport.Service.UserProcessor.Internals;
//using Utils.EncryptionService;

using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Modules;

namespace AOM.Transport.Service.UserProcessor
{
    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<ISubscriptions>().To<Subscriptions>();
            kernel.Bind<IServiceManagement>().To<ServiceManagement>();
            kernel.Bind<IServiceInformation>().To<ServiceInformation>();
            //kernel.Bind<IEncryptionService>().To<EncryptionService>();
            kernel.Bind<IAomTopicManager>().To<AomTopicManager>();

            kernel.Bind(x => x.FromThisAssembly().IncludingNonePublicTypes()
                .SelectAllClasses()
                .InheritedFrom<IConsumer>()
                .BindAllInterfaces()
                .Configure(b => b.InThreadScope())); // InThreadScope used as orginal code used 'ToConstant'   
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}