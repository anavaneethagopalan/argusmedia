﻿using System.Collections.Generic;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.UserProcessor
{
    public class UserAuthenticationStartup : ProcessorServiceBase
    {
        public UserAuthenticationStartup(
            IServiceManagement serviceManagement,
            IEnumerable<IConsumer> consumers,
            IBus aomBus)
            : base(consumers, serviceManagement, aomBus)
        {
        }

        public override string Name
        {
            get { return "UserAuthenticationStartup"; }
        }
    }
}