﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.EmailService;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using IUserService = AOM.App.Domain.Services.IUserService;
using AOM.App.Domain;
using AOM.Services.CrmService;
using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    public class ForgotPasswordConsumer : IConsumer
    {
        private readonly ICredentialsService _userCreditalsService;

        private readonly IBus _aomBus;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly IEncryptionService _encryptionService;
        private readonly IDbContextFactory _dbFactory;

        public ForgotPasswordConsumer(IDbContextFactory dbFactory, IEncryptionService encryptionService,
            ICredentialsService crmService, IBus aomBus, IUserService userService, IEmailService emailService)
        {
            _userCreditalsService = crmService;
            _aomBus = aomBus;
            _userService = userService;
            _emailService = emailService;
            _encryptionService = encryptionService;
            _dbFactory = dbFactory;
        }

        public void Start()
        {
            _aomBus.Respond<ForgotPasswordRequestRpc, ChangePasswordResponse>(ForgotPasswordRequest);
        }

        internal ChangePasswordResponse ForgotPasswordRequest(ForgotPasswordRequestRpc request)
        {
            try
            {
                var userDto = _userService.GetUserInfoDto(request.Username);
                if (userDto == null || userDto.Id < 0)
                {
                    return FailResponse(string.Format("Invalid user name: {0}", request.Username), request.Username);
                }

                var user = userDto.ToEntity();
                user.Token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                var tempPassword = RemoveReservedChars(_encryptionService.Hash(user, user.Token));

                var success = _userCreditalsService.CreateTempUserCredentials(request.Username,
                    _encryptionService.Hash(user, tempPassword));

                if (!success)
                {
                    return FailResponse("Failed to create user credentials", request.Username);
                }

                var notification = new ForgotPasswordEmailNotification
                {
                    Url = request.Url,
                    Password = string.Format("{0}@{1}", tempPassword, _encryptionService.Encode(user.Id)),
                    Recipient = user.Email,
                };

                using (var aomModel = _dbFactory.CreateAomModel())
                {
                    success = _emailService.CreateEmail(aomModel, notification) > 0;
                }

                return new ChangePasswordResponse
                {
                    Success = success,
                    UserName = request.Username,
                    MessageBody = success ? "Success" : "Failure"
                };
            }
            catch (BusinessRuleException e)
            {
                return FailResponse(e.Message, request.Username);
            }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        public bool RequiresArgusBus
        {
            get { return false; }
        }

        private ChangePasswordResponse FailResponse(string reason, string userName)
        {
            return new ChangePasswordResponse
            {
                Success = false,
                UserName = userName,
                MessageBody = reason
            };
        }

        private string RemoveReservedChars(string input)
        {
            // http://tools.ietf.org/html/rfc3986#section-2.2
            return input.Replace(":", "0")
                .Replace("/", "1")
                .Replace("?", "2")
                .Replace("#", "3")
                .Replace("[", "4")
                .Replace("]", "5")
                .Replace("@", "6")
                .Replace("!", "7")
                .Replace("$", "8")
                .Replace("&", "9")
                .Replace("'", "A")
                .Replace("(", "B")
                .Replace(")", "C")
                .Replace("*", "D")
                .Replace("+", "E")
                .Replace(",", "F")
                .Replace(";", "G")
                .Replace("=", "H");
        }
    }
}