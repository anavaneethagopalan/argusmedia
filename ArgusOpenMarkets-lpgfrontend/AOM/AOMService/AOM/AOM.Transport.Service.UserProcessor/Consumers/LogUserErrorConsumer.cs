﻿using AOM.Services.ErrorService;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    public class LogUserErrorConsumer : IConsumer
    {
        private readonly IBus _bus;

        private readonly IErrorService _errorService;

        public LogUserErrorConsumer(IBus bus, IErrorService errorService)
        {
            _errorService = errorService;
            _bus = bus;
        }

        public void Start()
        {
            _bus.Respond<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(LogUserError);
        }

        private LogUserErrorResponseRpc LogUserError(LogUserErrorRequestRpc userError)
        {
            string errorMessage = string.Empty;

            if (userError.Error != null)
            {
                errorMessage = _errorService.LogException(userError.Error, userError.AdditionalInformation);
            }
            else
            {
                errorMessage = _errorService.LogMessage(userError.ErrorText, userError.Source);
            }

            return new LogUserErrorResponseRpc {UserErrorMessage = errorMessage};
        }

        private string Left(string s, int length)
        {
            return s.Substring(0, Math.Min(length, s.Length));

        }
    }
}