﻿using AOM.Services.ErrorService;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using AOM.App.Domain.Services;
using AOM.Transport.Events.System;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    public class ClientLogConsumer : IConsumer
    {
        private readonly IBus _bus;
        private readonly IUserService _userService;

        public ClientLogConsumer(IBus bus, IUserService userService)
        {
            _bus = bus;
            _userService = userService;
        }

        public void Start()
        {

            _bus.Subscribe<ClientLogEvent>(SubscriptionId, SaveClientLogEvent);

        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        private void SaveClientLogEvent(ClientLogEvent clientLogEvent)
        {
            if (clientLogEvent.ClientLog != null)
            {
                _userService.SaveClientLog(clientLogEvent.ClientLog);
            }
        }

    }
}
