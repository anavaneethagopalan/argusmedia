﻿using System;
using System.Diagnostics;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    internal class GetUserRequestConsumerRpc : IConsumerWithInitialise
    {
        protected readonly IUserService UserService;

        private readonly IBus _aomBus;

        public GetUserRequestConsumerRpc(IUserService userService, IBus aomBus)
        {
            UserService = userService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Respond<GetUserRequestRpc, GetUserResponse>(GetUserRpc);
            _aomBus.Respond<GetUserIdRequestRpc, GetUserIdResponseRpc>(GetUserIdRpc);
        }

        private GetUserIdResponseRpc GetUserIdRpc(GetUserIdRequestRpc arg)
        {
            try
            {

                var userId = UserService.GetUserId(arg.Username);
                return new GetUserIdResponseRpc {UserId = userId};

            }
            catch (Exception e)
            {
                Log.Error("GetUserIdResponseRpc", e);
            }

            return new GetUserIdResponseRpc {UserId = -1};
        }

        public void InitialiseAndWarmupConsumer()
        {
            try
            {
                UserService.GetUserWithPrivileges(-999);
            }
            catch (Exception)
            {
                // doesn't matter if user -999 doesn't exist
            }
        }

        internal GetUserResponse GetUserRpc(GetUserRequestRpc getUserRequestRpc)
        {
            try
            {
                var timer = Stopwatch.StartNew();
                var u = UserService.GetUserWithPrivileges(getUserRequestRpc.Username);
                var response = new GetUserResponse
                {
                    User = u as User
                };
                Log.Info(
                    string.Format(
                        "Took {0} msecs to get user {1} details",
                        timer.ElapsedMilliseconds,
                        getUserRequestRpc.Username));
                timer.Stop();
                return response;

            }
            catch (Exception ex)
            {
                Log.Error("GetUserRpc - Error", ex);
            }

            return new GetUserResponse {User = null};
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        public bool RequiresArgusBus
        {
            get { return false; }
        }
    }
}