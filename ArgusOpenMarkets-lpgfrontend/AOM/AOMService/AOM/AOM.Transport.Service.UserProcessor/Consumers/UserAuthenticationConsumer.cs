﻿using System.Collections.Generic;
using System.Diagnostics;
using AOM.TopicManager;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using System.Linq;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    internal class UserAuthenticationConsumer : IConsumer
    {
        private IBus _aomBus;

        private readonly IUserService UserService;
        private readonly IAomTopicManager _aomTopicManager;

        private IUserAuthenticationService _authenticationService;

        public UserAuthenticationConsumer(IUserAuthenticationService authenticationService, IBus bus, IUserService userService, IAomTopicManager aomTopicManager)
        {
            _authenticationService = authenticationService;
            _aomBus = bus;
            UserService = userService;
            _aomTopicManager = aomTopicManager;
        }

        public void Start()
        {
            _aomBus.Subscribe<AomUserAuthenticatingPublished>(SubscriptionId, ConsumeUserAuthenticatingPublished);
            _aomBus.Subscribe<TopicSubscribeRequest>(SubscriptionId, OnTopicSubscribeRequest);

            //_aomBus.Respond<UserAuthenticationRequestRpc, IAuthenticationResult>(AuthenticateUserRpc);
            _aomBus.Respond<AuthenticateUserTokenRequestRpc, IAuthenticationResult>(AuthenticateUserTokenRpc);
            _aomBus.Respond<SessionTokenAuthenticatonRequest, IAuthenticationResult>(AuthenticateSessionTokenRpc);
            _aomBus.Respond<UserMatchesIdAuthenticationRequestRpc, IAuthenticationResult>(AuthenticateUsernameMatchesId);
            _aomBus.Respond<UserPrivilageAuthenticationRequestRpc, AuthenticationResult>(AuthenticateUserAgainstProductPrivilage);
            _aomBus.Respond<UserContentStreamAuthenticationRequestRpc, AuthenticationResult>(AuthenticateUserAgainstContentStream);
            _aomBus.Respond<SystemPrivilegeAuthenticationRequestRpc, AuthenticationResult>(AuthenticateUserSystemPriv);
        }

        private void OnTopicSubscribeRequest(TopicSubscribeRequest topicSubscriptionRequest)
        {

            var topicsToSubscribeUserTo =
                _aomTopicManager.GetAllTopicsToSubscribeForUser(topicSubscriptionRequest.ClientSessionInfo.UserId);
            Log.InfoFormat("TopicSubscribeResponse.  We are subscribing the user to {0} Topic(s)",
                topicsToSubscribeUserTo.Count);

            _aomBus.Publish(new TopicSubscribeResponse
            {
                ClientSessionInfo = topicSubscriptionRequest.ClientSessionInfo,
                TopicsSubscribe = topicsToSubscribeUserTo
            });
        }

        private IAuthenticationResult AuthenticateUsernameMatchesId(UserMatchesIdAuthenticationRequestRpc request)
        {
            try
            {
                var userId = UserService.GetUserId(request.Username);
                return new AuthenticationResult {IsAuthenticated = userId == request.UserId};
            }
            catch (Exception e)
            {
                var logError = _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = e,
                    AdditionalInformation = string.Format("User={0}", request.UserId),
                    ErrorText = string.Empty,
                    Source = "UserAuthenticationConsumer"
                });

                return CreateErrorAuthenticationResult(logError.UserErrorMessage);
            }
        }

        private AuthenticationResult AuthenticateUserAgainstProductPrivilage(UserPrivilageAuthenticationRequestRpc userPrivilageAuthenticationRequestRpc)
        {
            try
            {
                var userHasPrivilege = UserService.CheckUserHasPrivilege(userPrivilageAuthenticationRequestRpc.Username, userPrivilageAuthenticationRequestRpc.OrganisationId,
                    userPrivilageAuthenticationRequestRpc.ProductId, userPrivilageAuthenticationRequestRpc.PrivilegeName);

                return new AuthenticationResult {IsAuthenticated = userHasPrivilege};
            }
            catch (Exception e)
            {
                var logError = _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = e,
                    AdditionalInformation = string.Format("User={0}", userPrivilageAuthenticationRequestRpc.Username),
                    ErrorText = string.Empty,
                    Source = ""
                });

                return CreateErrorAuthenticationResult(logError.UserErrorMessage);
            }
        }

        private AuthenticationResult AuthenticateUserTokenRpc(AuthenticateUserTokenRequestRpc authenticateUserToken)
        {
            try
            {
                var authenticateResult = _authenticationService.AuthenticateToken(authenticateUserToken.Username, authenticateUserToken.Token) as AuthenticationResult;
                return authenticateResult;
            }
            catch (Exception e)
            {
                var logError = _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = e,
                    AdditionalInformation = string.Format("User={0}", authenticateUserToken.Username),
                    ErrorText = string.Empty,
                    Source = "UserAuthenticationConsumer"
                });

                return CreateErrorAuthenticationResult(logError.UserErrorMessage);
            }
        }

        private AuthenticationResult AuthenticateUserSystemPriv(SystemPrivilegeAuthenticationRequestRpc request)
        {
            try
            {

                var privs = UserService.GetSystemPrivileges(request.UserId);
                return new AuthenticationResult {IsAuthenticated = privs.Privileges.Contains(request.PrivilegeName)};

            }
            catch (Exception e)
            {
                var logError = _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = e,
                    AdditionalInformation = string.Format("User={0}", request.UserId),
                    ErrorText = string.Empty,
                    Source = "UserAuthenticationConsumer"
                });

                return CreateErrorAuthenticationResult(logError.UserErrorMessage);
            }
        }

        private IAuthenticationResult AuthenticateSessionTokenRpc(SessionTokenAuthenticatonRequest sessionTokenAuthenticatioRequest)
        {
            try
            {
                var clock = Stopwatch.StartNew();

                var authResult = _authenticationService.AuthenticateSessionToken(sessionTokenAuthenticatioRequest.SessionTokenString, sessionTokenAuthenticatioRequest.UserId);
                Log.Info(String.Format("In UserAuthConsumer.AuthenticateSessionTokenRpc. session token {0} with userId: {1}; isAuthenticated: {2}, Took {3}msecs",
                    sessionTokenAuthenticatioRequest.SessionTokenString, sessionTokenAuthenticatioRequest.UserId, authResult.IsAuthenticated, clock.ElapsedMilliseconds));

                return authResult;
            }
            catch (Exception e)
            {
                var logError = _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = e,
                    AdditionalInformation = string.Empty,
                    ErrorText = string.Empty,
                    Source = "UserAuthenticationConsumer"
                });

                return CreateErrorAuthenticationResult(logError.UserErrorMessage);
            }

        }

        private AuthenticationResult AuthenticateUserAgainstContentStream(UserContentStreamAuthenticationRequestRpc request)
        {
            try
            {
                var result = UserService.CheckUserHasContentStream(request.Username, request.ContentStreamId);
                return new AuthenticationResult {IsAuthenticated = result};
            }
            catch (Exception e)
            {
                var logError = _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = e,
                    AdditionalInformation = string.Empty,
                    ErrorText = string.Empty,
                    Source = "UserAuthenticationConsumer"
                });

                return CreateErrorAuthenticationResult(logError.UserErrorMessage);
            }
        }

        private void ConsumeUserAuthenticatingPublished(AomUserAuthenticatingPublished publishedUserEvent)
        {
            Log.Info("UserAuthenticationConsumer called with event: " + publishedUserEvent.User.Username);
            var authResult = AuthenticateUserToken(publishedUserEvent);
            Log.Info(string.Format("User: {0} Authentication Result {1}", publishedUserEvent.User.Username, authResult.IsAuthenticated));

            var authPublishedEvent = new AomUserAuthenticatedPublished
            {
                IsAuthenticated = authResult.IsAuthenticated,
                User = authResult.User
            };
            _aomBus.Publish(authPublishedEvent);
        }

        public IBus AomBus
        {
            set { _aomBus = value; }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        private IAuthenticationResult AuthenticateUserToken(AomUserAuthenticatingPublished userAuthenticateEvent)
        {
            Log.Info("UserAuthenticationConsume.AuthenticateUserToken - Called");
            if (userAuthenticateEvent == null)
            {
                Log.Info("AomUserAuthenticatingPublished - null event");
                return new AuthenticationResult {IsAuthenticated = false, User = null};
            }

            if (userAuthenticateEvent.User == null)
            {
                Log.Info("AomUserAuthenticatingPublished - event - null User");
                return new AuthenticationResult {IsAuthenticated = false, User = null};
            }

            var userName = userAuthenticateEvent.User.Username;
            var token = userAuthenticateEvent.User.Token;
            //var sessionToken = userAuthenticateEvent.

            Log.Info("Calling Validate User");

            return _authenticationService.AuthenticateToken(userName, token);
        }

        private static AuthenticationResult CreateErrorAuthenticationResult(string errrorMessage)
        {
            return new AuthenticationResult
            {
                FirstLogin = false,
                IsAuthenticated = false,
                Message = errrorMessage,
                SessionToken = string.Empty,
                Token = string.Empty,
                User = null
            };
        }
    }
}