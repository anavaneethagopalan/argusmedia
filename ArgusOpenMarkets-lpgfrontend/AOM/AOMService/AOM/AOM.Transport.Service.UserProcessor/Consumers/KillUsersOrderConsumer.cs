﻿using System;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.UserProcessor.Consumers
{


    public class KillUsersOrderConsumer : IConsumer
    {
        private readonly IBus _bus;

        public KillUsersOrderConsumer(IBus bus)
        {
            _bus = bus;
        }

        public void Start()
        {
            _bus.Respond<KillUsersOrderRequestRpc, KillUsersOrderResponseRpc>(ConsumeKillUsersOrderRequest);
        }

        private KillUsersOrderResponseRpc ConsumeKillUsersOrderRequest(KillUsersOrderRequestRpc request)
        {
            try
            {
                var response = new KillUsersOrderResponseRpc();

                var message = new Message
                              {
                                  ClientSessionInfo =
                                      new ClientSessionInfo { UserId = request.RequestingUserId },
                                  MessageBody = request.Order,
                                  MessageAction = MessageAction.Kill,
                                  MessageType = MessageType.Order
                              };

                var authenticationResponse = new AomOrderAuthenticationResponse
                                             {
                                                 Order = request.Order,
                                                 MarketMustBeOpen = false,
                                                 Message = message
                                             };

                _bus.Publish(authenticationResponse);
                return response;

            }
            catch (Exception e)
            {
                Log.Error("KillUsersOrderResponseRpc", e);
            }

            return new KillUsersOrderResponseRpc { Success = false, MessageBody = string.Empty };
        }
    }
}