﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using AOM.Services.CrmService;
using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    public class ResetPasswordConsumer : IConsumer
    {
        private readonly ICredentialsService _userCreditalsService;

        private readonly IBus _aomBus;
        private readonly IEncryptionService _encryptionService;

        public ResetPasswordConsumer(IEncryptionService encryptionService, ICredentialsService crmService, IBus aomBus)
        {
            _userCreditalsService = crmService;
            _aomBus = aomBus;
            _encryptionService = encryptionService;
        }

        public void Start()
        {
            _aomBus.Respond<ResetPasswordRequestRpc, ChangePasswordResponse>(ResetPasswordRequest);
        }

        internal ChangePasswordResponse ResetPasswordRequest(ResetPasswordRequestRpc request)
        {
            try
            {
                string oldPassword;
                var userId = ParseRequest(request.OldPassword, out oldPassword);
                if (userId < 0) return FailResponse("Invalid Request!");

                var tempCredentials = _userCreditalsService.GetUserCredentials(userId, true);
                if (tempCredentials == null || tempCredentials.Expiration < DateTime.UtcNow)
                {
                    return FailResponse("Temporary password has expired!");
                }

                var user = new User {Id = userId};
                if (tempCredentials.PasswordHashed != _encryptionService.Hash(user, oldPassword))
                {
                    return FailResponse("Invalid Request!");
                }

                var newPassword = _encryptionService.Hash(user, request.NewPassword);
                var permCredentials = _userCreditalsService.GetUserCredentials(userId);
                if (permCredentials == null)
                {
                    return FailResponse("Invalid Request!");
                }
                permCredentials.PasswordHashed = newPassword;

                var success = _userCreditalsService.UpdateUserCredentials(permCredentials);

                return new ChangePasswordResponse
                {
                    Success = success,
                    UserName = null,
                    MessageBody = success ? "Success" : "Failure"
                };
            }
            catch (Exception e)
            {
                return FailResponse(e.Message);
            }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        public bool RequiresArgusBus
        {
            get { return false; }
        }

        private ChangePasswordResponse FailResponse(string reason, string userName = null)
        {
            return new ChangePasswordResponse
            {
                Success = false,
                UserName = userName,
                MessageBody = reason
            };
        }

        private long ParseRequest(string token, out string oldPassword)
        {
            if (string.IsNullOrEmpty(token))
            {
                oldPassword = string.Empty;
                return -1024;
            }

            var index = token.LastIndexOf('@');
            if (index < 0 || (index == token.Length - 1))
            {
                oldPassword = string.Empty;
                return -1024;
            }

            oldPassword = token.Substring(0, index);

            return _encryptionService.Decode(token.Substring(index + 1, token.Length - index - 1));
        }
    }
}