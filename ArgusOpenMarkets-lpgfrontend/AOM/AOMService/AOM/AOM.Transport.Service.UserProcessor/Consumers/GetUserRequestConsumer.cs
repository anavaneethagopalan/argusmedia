﻿using Utils.Logging.Utils;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    internal class GetUserRequestConsumer : IConsumer
    {
        protected readonly IUserService _userService;

        private IBus _aomBus;

        public GetUserRequestConsumer(IBus aomBus, IUserService userService)
        {
            _aomBus = aomBus;
            _userService = userService;
        }

        public GetUserRequestConsumer(IBus aomBus)
        {
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<GetUserRequest>(SubscriptionId, ConsumeGetUserRequest);
        }

        internal void ConsumeGetUserRequest(GetUserRequest getUserRequest)
        {
            Log.Info("GetUserRequestConsumer - called");
            User user;
            if (getUserRequest.UserId > 0)
            {
                Log.Info("GetUserRequestConsumer - called with User Id: " + getUserRequest.UserId);
                Log.Info("Calling _crmService.GetUser");
                user = _userService.GetUserWithPrivileges(getUserRequest.UserId) as User;
                if (user != null)
                {
                    // We have a user object returned.
                    Log.Info("_crmService.GetUser has returned as user.  User.Name:" + user.Name);

                    Log.Info(
                        string.Format(
                            "_crmService.GetUser - user has {0} Product Privileges",
                            user.ProductPrivileges.Count));
                }
                else
                {
                    Log.Info("_crmService.GetUser returned a null user");
                }
            }
            else
            {
                user = _userService.GetUserWithPrivileges(getUserRequest.Username) as User;
            }

            _aomBus.Publish<IGetUserResponse>(new GetUserResponse {User = user});
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        public bool RequiresArgusBus
        {
            get { return false; }
        }
    }
}