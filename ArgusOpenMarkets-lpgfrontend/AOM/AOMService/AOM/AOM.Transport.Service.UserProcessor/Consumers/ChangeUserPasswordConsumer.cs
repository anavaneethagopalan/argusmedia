﻿using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using IUserService = AOM.App.Domain.Services.IUserService;
using AOM.App.Domain;
using AOM.Services.CrmService;
using AOM.Transport.Service.Processor.Common;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    internal class ChangeUserPasswordConsumer : IConsumer
    {
        protected readonly ICredentialsService UserCreditalsService;

        private readonly IBus _aomBus;
        private readonly IUserService _userService;

        public ChangeUserPasswordConsumer(ICredentialsService crmService, IBus aomBus, IUserService userService)
        {
            UserCreditalsService = crmService;
            _aomBus = aomBus;
            _userService = userService;
        }

        public void Start()
        {
            _aomBus.Respond<ChangePasswordRequestRpc, ChangePasswordResponse>(ChangePasswordRequest);
        }

        internal ChangePasswordResponse ChangePasswordRequest(ChangePasswordRequestRpc changePasswordRequest)
        {
            try
            {
                var id = _userService.GetUserId(changePasswordRequest.Username);
                if (id < -1)
                {
                    throw new AuthenticationErrorException("Can not change password as user does not exist : " +
                                                           changePasswordRequest.Username);
                }
                if (id == -1)
                {
                    throw new AuthenticationErrorException("You can not change the password of the system user");
                }

                bool success = false;
                if (changePasswordRequest.AdminResetPassword)
                {
                    success = UserCreditalsService.UpdateUserCredentialsAdmin(id,
                        changePasswordRequest.OldPassword, changePasswordRequest.NewPassword,
                        changePasswordRequest.CheckOldPassword, changePasswordRequest.AdminResetPassword,
                        changePasswordRequest.RequestorUserId, changePasswordRequest.IsTemporary,
                        changePasswordRequest.TempPassExpirInHours);
                }
                else
                {
                    success = UserCreditalsService.UpdateUserCredentials(id,
                        changePasswordRequest.OldPassword, changePasswordRequest.NewPassword,
                        changePasswordRequest.CheckOldPassword, changePasswordRequest.AdminResetPassword,
                        changePasswordRequest.RequestorUserId, changePasswordRequest.IsTemporary,
                        changePasswordRequest.TempPassExpirInHours);
                }

                return new ChangePasswordResponse
                {
                    Success = success,
                    UserName = changePasswordRequest.Username,
                    MessageBody = success ? "Success" : "Failure"
                };
            }
            catch (BusinessRuleException e)
            {
                Log.Info("ChangePasswordRequest", e);
                return new ChangePasswordResponse
                {
                    Success = false,
                    UserName = changePasswordRequest.Username,
                    MessageBody = e.Message
                };
            }
            catch (AuthenticationErrorException e)
            {
                Log.Error("ChangePasswordRequest", e);
                return new ChangePasswordResponse
                {
                    Success = false,
                    UserName = changePasswordRequest.Username,
                    MessageBody = e.Message
                };
            }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        public bool RequiresArgusBus
        {
            get { return false; }
        }
    }
}