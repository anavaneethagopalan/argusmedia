﻿using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    internal class UserDisconnectConsumer : IConsumer
    {
        private readonly IBus _aomBus;
        private readonly IUserService _userService;

        public UserDisconnectConsumer(
            IBus bus, 
            IUserService userService)
        {
            _aomBus = bus;
            _userService = userService;
        }

        public void Start()
        {
            _aomBus.Subscribe<UserDisconnectedResponse>(SubscriptionId, ConsumeUserDisconnectedResponse);
        }

        private void ConsumeUserDisconnectedResponse(UserDisconnectedResponse userDisconnectResponse)
        {
            if (userDisconnectResponse != null)
            {
                _userService.UserDisconnect(userDisconnectResponse.UserId, userDisconnectResponse.DisconnectReason);
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}