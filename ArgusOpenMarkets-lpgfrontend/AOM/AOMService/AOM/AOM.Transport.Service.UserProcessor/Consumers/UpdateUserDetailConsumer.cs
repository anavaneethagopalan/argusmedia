﻿using AOM.App.Domain;
using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    internal class UpdateUserDetailConsumer : IConsumer
    {
        protected readonly ICrmAdministrationService CrmAdministrationService;

        private IBus _aomBus;

        public UpdateUserDetailConsumer(ICrmAdministrationService crmAdministrationService, IBus aomBus)
        {
            CrmAdministrationService = crmAdministrationService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Respond<BlockUserRequestRpc, BlockUserResponse>(BlockUserRequest);
            _aomBus.Respond<UnblockUserRequestRpc, UnblockUserResponseRpc>(UnblockUserRequest);
        }

        internal BlockUserResponse BlockUserRequest(BlockUserRequestRpc changePasswordRequestRpc)
        {
            try
            {
                bool success = CrmAdministrationService.BlockUser(changePasswordRequestRpc.UserName,
                    changePasswordRequestRpc.RequestorUserId);

                return success
                    ? new BlockUserResponse
                    {
                        Success = true,
                        UserName = changePasswordRequestRpc.UserName,
                        MessageBody = "Account blocked",
                        Message =
                            new Message
                            {
                                MessageType = MessageType.User,
                                MessageBody = "User blocked successfully"
                            }
                    }
                    : new BlockUserResponse
                    {
                        Success = false,
                        UserName = changePasswordRequestRpc.UserName,
                        MessageBody = "Account unblocked",
                        Message =
                            new Message
                            {
                                MessageType = MessageType.Error,
                                MessageBody = "Failed to block user!"
                            }
                    };

            }
            catch (BusinessRuleException e)
            {
                return new BlockUserResponse
                {
                    Success = false,
                    UserName = changePasswordRequestRpc.UserName,
                    MessageBody = e.Message
                };
            }
        }

        internal UnblockUserResponseRpc UnblockUserRequest(UnblockUserRequestRpc changePasswordRequestRpc)
        {
            try
            {
                bool success = CrmAdministrationService.UnblockUser(changePasswordRequestRpc.UserName,
                    changePasswordRequestRpc.RequestorUserId);

                return success
                    ? new UnblockUserResponseRpc
                    {
                        Success = true,
                        UserName = changePasswordRequestRpc.UserName,
                        MessageBody = "Account unblocked",
                        Message =
                            new Message
                            {
                                MessageType = MessageType.User,
                                MessageBody = "User unblocked successfully"
                            }
                    }
                    : new UnblockUserResponseRpc
                    {
                        Success = false,
                        UserName = changePasswordRequestRpc.UserName,
                        MessageBody = "Account blocked",
                        Message =
                            new Message
                            {
                                MessageType = MessageType.Error,
                                MessageBody = "Failed to unblock user!"
                            }
                    };
            }
            catch (BusinessRuleException e)
            {
                return new UnblockUserResponseRpc
                {
                    Success = false,
                    UserName = changePasswordRequestRpc.UserName,
                    MessageBody = e.Message
                };
            }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }

        public bool RequiresArgusBus
        {
            get { return false; }
        }
    }
}