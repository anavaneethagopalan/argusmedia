﻿using System.Linq;
using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.UserProcessor.Consumers
{
    public class TerminateUserRequestConsumer : IConsumer
    {
        protected readonly IUserService UserService;

        private IBus _aomBus;

        public TerminateUserRequestConsumer(IUserService _userService, IBus aomBus)
        {
            UserService = _userService;
            _aomBus = aomBus;
        }

        public TerminateUserRequestConsumer(IBus aomBus)
        {
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<TerminateUserRequest>(SubscriptionId, ConsumeTerminateUserRequest);
            _aomBus.Subscribe<TerminateOtherUserSessionsRequest>(SubscriptionId, ConsumeTerminateOtherUserSessions);
        }

        private void ConsumeTerminateOtherUserSessions(TerminateOtherUserSessionsRequest terminateRequest)
        {
            if (terminateRequest.UserId != 0)
            {
                //get count of external accounts, if any then total count of sessions user can have is 1 (for AOM) + count of external accounts
                var externalAccounts = UserService.GetUsersLoginExternalAccounts(terminateRequest.UserId).ToList();

                var response = new TerminateOtherUserSessionsResponse
                {
                    UserId = terminateRequest.UserId,
                    SessionId = terminateRequest.SessionId,
                    Reason = "You have been signed out by another session",
                    LoginSessionsAllowed = 1 + externalAccounts.Count(),
                    Message = new Message
                    {
                        MessageType = MessageType.User,
                        MessageBody = "Terminate user sessions"
                    }
                };
                _aomBus.Publish(response);
            }
        }

        internal void ConsumeTerminateUserRequest(TerminateUserRequest termiateUserRequest)
        {
            TerminateResponse response;
            if (string.IsNullOrEmpty(termiateUserRequest.UserName))
            {
                string errorText = "Received a TerminateUserRequest with null or empty user, aborting...";
                Log.Error(errorText);
                response = new TerminateResponse
                {
                    Message = new Message
                    {
                        MessageType = MessageType.Error,
                        MessageBody = errorText
                    },
                    Reason = termiateUserRequest.Reason
                };
                return;
            }

            var userDto = UserService.GetUserInfoDto(termiateUserRequest.UserName);

            if (userDto == null)
            {
                string errorText = string.Format("In ConsumeTerminateUserRequest. No user found with username: {0}", termiateUserRequest.UserName);
                Log.Error(errorText);
                response = new TerminateResponse
                {
                    Message = new Message
                    {
                        MessageType = MessageType.Error,
                        MessageBody = errorText
                    },
                    Reason = termiateUserRequest.Reason
                };
                return;
            }
            var userId = userDto.Id;

            response = new TerminateResponse
            {
                UserId = userId,
                UserName = termiateUserRequest.UserName,
                Reason = termiateUserRequest.Reason,
                Message = new Message
                {
                    MessageType = MessageType.User,
                    MessageBody = "Successfully retrieved user"
                }
            };

            _aomBus.Publish(response);
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }
    }
}