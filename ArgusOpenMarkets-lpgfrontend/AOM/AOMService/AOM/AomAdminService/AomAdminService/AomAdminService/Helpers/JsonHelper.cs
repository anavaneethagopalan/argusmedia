﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Reflection;

namespace AomAdminService
{
    public static class ExtensionMethods
    {
        public static string ToJsonCamelCase<T>(this T arg)
        {
            var messageType = typeof (T).Name;
            var result = JsonConvert.SerializeObject(arg, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ContractResolver = new JsonContractResolver()
                });

            return result;
        }
    }

    public class JsonContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            property.ShouldSerialize = instance => true;
            return property;
        }
    }
}