﻿using AomAdminService.Domain;
using AomAdminService.Helpers;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace AomAdminService
{
    public class UsersController : ApiController
    {
        // GET api/values 
        public HttpResponseMessage Get()
        {
            // TODO: We need to query DB / Rabbit to get a list of users.   
            var users = new List<User>();
            users.Add(new User{ Username = "AS", Name = "Alexander Smith", Organisation = "Argus Media Ltd"});
            users.Add(new User{ Username = "ZS", Name = "Zander Smith", Organisation = "Argus Media Ltd"});

            return HttpResponseHelper.JsonResponse(users.ToJsonCamelCase(), Configuration.Formatters.JsonFormatter);
        }

        // GET api/values/5 
        public string Get(int id)
        {
            // TODO: We need to return a list of users....
            return "value";
        }

        // POST api/values 
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5 
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5 
        public void Delete(int id)
        {
        }
    }
}