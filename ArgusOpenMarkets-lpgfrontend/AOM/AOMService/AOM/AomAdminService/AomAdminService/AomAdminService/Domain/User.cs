﻿namespace AomAdminService.Domain
{
    public class User
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Organisation { get; set; }
    }
}
