﻿using System;
using System.Diagnostics;
using AOMDiffusion.Common.Interfaces;
using Utils.Logging.Utils;

using PushTechnology.ClientInterface.Client.Features;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using PushTechnology.ClientInterface.Client.Session;
using PushTechnology.ClientInterface.Client.Topics;
using PushTechnology.ClientInterface.Data.JSON;
using ISession = PushTechnology.ClientInterface.Client.Session.ISession;

namespace AOMDiffusion.Common
{
    public abstract class AomDiffusionTopicManager : IAomDiffusionTopicManager
    {
        private const int WarningDiffusionMessageLength = 100000;
        private readonly string _topicRoot;

        public string TopicRoot
        {
            get { return _topicRoot; }
        }

        public ISession Session { get; private set; }
        private ITopics _topicService;
        private ISubscriptionControl _subscriptionControl;
        private ITopicControl _topicControlService;
        private ITopicUpdateControl _updateControl;
        private IMessagingControl _messageControl;
        private ISimpleTopicSourceHandler _topicSourceHandler;

        public event EventHandler Active;
        public event EventHandler StandBy;

        public bool Running { get; private set; }

        protected AomDiffusionTopicManager(string topicRoot)
        {
            _topicRoot = topicRoot;
            //_topicSourceHandler = new SimpleTopicSourceHandler();            
        }

        public void AttachToSessionAndRegisterTopicSource(ISession session)
        {
            Session = session;
            _topicService = Session.GetTopicsFeature();
            _subscriptionControl = Session.GetSubscriptionControlFeature();
            _topicControlService = Session.GetTopicControlFeature();
            _updateControl = Session.GetTopicUpdateControlFeature();
            _messageControl = Session.GetMessagingControlFeature();

            var topicControlAddCallback = new TopicControlAddCallback();
            var action = new Action(RegisterTopicSource);
            topicControlAddCallback.TopicAdded += (sender, s) => action();
            CreateTopic(_topicRoot, topicControlAddCallback, action);
        }

        public void GetTopicDetails(string topic, ITopicDetailsCallback topicDetailsCallback)
        {
            try
            {
                _topicService.GetTopicDetails(topic, TopicDetailsLevel.BASIC, topicDetailsCallback);
            }
            catch (Exception ex)
            {
                Log.Error("Error in GetTopicDetails for topic: " + topic, ex);
            }
        }

        public void SubscribeWithSelector(SessionId sessionId, string topicSelectorString, ISubscriptionCallback callback)
        {
            _subscriptionControl.Subscribe(Session.SessionId, topicSelectorString, callback);
        }

        public void SendDataToTopic(string topic, string message, ITopicUpdaterUpdateCallback callback = null)
        {
            if (message.Length > WarningDiffusionMessageLength)
            {
                Log.Error(String.Format("Message size of {0} characters is bigger than ideal and will impact performance/stability. Topic={1} Message={2}", message.Length, topic, message));
            }

            var topicDetailsCallback = new TopicDetailsCallback();

            IJSON jsonMessage = null;
            try
            {
                jsonMessage = PushTechnology.ClientInterface.Client.Factories.Diffusion.DataTypes.JSON.FromJSONString(message);
            }
            catch (Exception ex)
            {
                Log.Error("Error serializing to json", ex);
            }

            topicDetailsCallback.TopicDoesNotExist += (s, o) =>
            {
                // DIFFUSION 5.8  Changes...
                var addTopicCallback = new TopicControlAddCallback();
                addTopicCallback.TopicAdded += (sender, s1) =>
                {
                    if (callback == null)
                    {
                        callback = new TopicUpdateCallback();
                    }

                    GetUpdater().PushToTopic(topic, jsonMessage, false, callback);
                };

                CreateTopic(topic, addTopicCallback);
            };

            topicDetailsCallback.TopicDetails += (sender, s) =>
            {
                if (callback == null)
                {
                    callback = new TopicUpdateCallback();
                }

                GetUpdater().PushToTopic(topic, jsonMessage, false, callback);
            };
            GetTopicDetails(topic, topicDetailsCallback);
        }

        public void GetTopicData(string topicName, string context, IFetchContextStream<string> callback)
        {
            _topicService.Fetch(">" + topicName, context, callback);
        }

        public void AddTopicListener(string selector, ITopicStream topicListener)
        {
            _topicService.AddTopicStream(selector, topicListener);
        }

        public void AddMessageHandler(string topicPath, IMessageHandler messageHandler)
        {
            _messageControl.AddMessageHandler(topicPath, messageHandler);
        }

        public void DeleteTopic(string topicName, string pathselector, ITopicControlRemoveCallback topicControlRemoveCallback)
        {
            topicName = pathselector + topicName;
            _topicControlService.RemoveTopics(topicName, topicControlRemoveCallback);
        }

        public void CreateTopic(string topicToSetup, TopicControlAddCallback addCallback)
        {
            CreateTopic(topicToSetup, addCallback, null);
        }

        public void CreateTopic(string topicToSetup, TopicControlAddCallback addCallback, Action callBackIfTopicExists)
        {
            var topicDetailsCallback = new TopicDetailsCallback();

            addCallback.TopicAddFailed += (s, r) => LogTopicAddFailure(topicToSetup, r);

            topicDetailsCallback.TopicDoesNotExist += (e, i) => _topicControlService.AddTopic(topicToSetup, TopicType.JSON, addCallback);
            topicDetailsCallback.TopicDetails += (sender, s) =>
            {
                if (callBackIfTopicExists != null) callBackIfTopicExists();
            };

            GetTopicDetails(topicToSetup, topicDetailsCallback);
        }

        public virtual ISimpleTopicSourceHandler GetUpdater()
        {
            return _topicSourceHandler;
        }

        private void RegisterTopicSource()
        {
            _topicSourceHandler = new SimpleTopicSourceHandler();

            _topicSourceHandler.Active += (sender, s) =>
            {
                Log.InfoFormat("RUS - Active Event handler called");

                if (Active != null)
                {
                    Log.InfoFormat("RUS - Active Callback - Active on {0}", _topicRoot);

//                    SendDataToTopic(_topicRoot,
//                        string.Format("Session {0} went Active on {1} at {2}", Session.SessionId, _topicRoot,
//                            DateTime.UtcNow));

                    Active(this, EventArgs.Empty);
                }
                else
                {
                    Log.InfoFormat("RUS - Active Event handler called - Active is NULL");
                }

            };

            _topicSourceHandler.StandBy += (sender, s) =>
            {
                if (StandBy != null)
                {
                    Log.InfoFormat("RUS - StandBy on {0}", _topicRoot);

                    StandBy(this, EventArgs.Empty);
                }
            };

            Log.InfoFormat("RUS - Registering - Registering update source for topic root: {0}", _topicRoot);

            try
            {
                _updateControl.RegisterUpdateSource(_topicRoot, _topicSourceHandler);
            }
            catch (Exception ex)
            {
                Log.Error("RUS - Error - RegisterUpdateSource - Failed", ex);
            }

            Log.InfoFormat("RUS - Success -  Registered Update Source for root topic: {0}", _topicRoot);
        }

        public void Dispose()
        {
            if (Session != null)
            {
                Session.Close();
                Running = false;
                Debug.WriteLine("Closing session...");
            }
        }

        private static void LogTopicAddFailure(string topic, TopicAddFailReason reason)
        {
            if (reason != TopicAddFailReason.EXISTS)
            {
                Log.ErrorFormat("Failed to add Diffusion topic {0}: {1}", topic, reason);
            }
        }
    }
}