﻿namespace AOMDiffusion.Common
{
    using System;
    using System.Diagnostics;
    using PushTechnology.ClientInterface.Client.Content.Metadata;
    using PushTechnology.ClientInterface.Client.Content;
    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Factories;

    public class FetchCallback : IFetchContextStream<string>
    {
        public event EventHandler<string> TopicDataFetched;
        public event EventHandler<string> NoDataFetched;
        public event Action<string, string> TopicDataFetchedWithTopicPath; 

        public void OnDiscard(string context)
        {
            Debug.WriteLine("Fetch stream discarded");
        }

        public void OnClose(string context)
        {
            Debug.WriteLine("Fetch stream closed");
        }

        public void OnFetchReply(string context, string topicPath, IContent content)
        {
            // DIFFUSION 5.8 Upgrade
            var reader = Diffusion.Content.NewReader<IRecordContentReader>(content);

            if (TopicDataFetched != null && reader.HasMoreRecords())
            {
                TopicDataFetched(Diffusion.Content.NewReader<IRecordContentReader>(content).NextRecord(), "topic data fetched");
            }

            if (TopicDataFetchedWithTopicPath != null && reader.HasMoreRecords())
            {
                TopicDataFetchedWithTopicPath(topicPath, reader.NextRecord().ToString());
            }
            if (!reader.HasMoreRecords())
            {
                NoDataFetched(this, "no data returned");
            }
        }
    }
}