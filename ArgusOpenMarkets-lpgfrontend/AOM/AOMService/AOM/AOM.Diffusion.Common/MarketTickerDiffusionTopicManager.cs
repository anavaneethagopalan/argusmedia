﻿namespace AOMDiffusion.Common
{
    using AOMDiffusion.Common.Interfaces;

    public class MarketTickerDiffusionTopicManager : AomDiffusionTopicManager, IMarketTickerDiffusionTopicManager
    {
        public MarketTickerDiffusionTopicManager()
            : base("AOM/MarketTicker")
        {

        }
    }
}