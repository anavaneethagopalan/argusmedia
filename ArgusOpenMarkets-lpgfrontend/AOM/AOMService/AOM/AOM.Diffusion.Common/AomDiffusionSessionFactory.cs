﻿using AOMDiffusion.Common.Interfaces;
using PushTechnology.ClientInterface.Client.Session;
using DiffusionFactory = PushTechnology.ClientInterface.Client.Factories.Diffusion;

namespace AOMDiffusion.Common
{
    class AomDiffusionSessionFactory : IAomDiffusionSessionFactory
    {
        public ISessionFactory GetDiffusionSessionFactory(string username, string password)
        {
            ISessionFactory sessionFactory = DiffusionFactory.Sessions.Principal(username).Password(password);

            return sessionFactory;
        }
    }
}
