﻿using System;
using System.Diagnostics;
using System.Threading;
using AOMDiffusion.Common.Interfaces;
using Utils.Logging.Utils;

using PushTechnology.ClientInterface.Client.Session;

namespace AOMDiffusion.Common
{
    public class DiffusionSessionConnection : IDiffusionSessionConnection
    {
        private readonly IDiffusionConfig _diffusionConfig;
        private readonly IAomDiffusionSessionFactory _aomDiffusionSessionFactory;

        public bool Running { get; private set; }

        public bool StopRequested { get; set; }

        public ISession Session { get; private set; }

        public DiffusionSessionConnection(IDiffusionConfig diffusionConfig, IAomDiffusionSessionFactory aomDiffusionSessionFactory)
        {
            diffusionConfig.DiffusionEndPoint.ArgumentStringNotNullOrEmpty("diffusionConfig.DiffusionEndPoint");
            diffusionConfig.DiffusionUsername.ArgumentStringNotNullOrEmpty("diffusionConfig.DiffusionUsername");
            diffusionConfig.DiffusionPassword.ArgumentStringNotNullOrEmpty("diffusionConfig.DiffusionPassword");
            diffusionConfig.DiffusionHandlerName.ArgumentStringNotNullOrEmpty("diffusionConfig.DiffusionHandlerName");

            _diffusionConfig = diffusionConfig;
            _aomDiffusionSessionFactory = aomDiffusionSessionFactory;
        }

        public bool ConnectAndStartSession()
        {
            StopRequested = false;

            CreateSessionAndStart();

            //allow some time to connect before we give up
            if (Session == null || !Session.State.Connected)
            {
                if (!RetryConnect())
                {
                    Running = false;
                    Dispose();
                }
            }

            return Session.State.Connected;
        }

        public void Disconnect()
        {
            StopRequested = true;
            Dispose();
        }

        private void CreateSessionAndStart()
        {
            ISessionFactory sessionFactory = _aomDiffusionSessionFactory.GetDiffusionSessionFactory(_diffusionConfig.DiffusionUsername, _diffusionConfig.DiffusionPassword);
            try
            {
                Session = sessionFactory.Open(_diffusionConfig.DiffusionEndPoint);
            }
            catch (Exception exception)
            {
                Log.Warn(string.Format("Could not connect to diffusion -> {0}", exception));
            }
        }

        private bool RetryConnect()
        {
            int retry = 0;
            int maxRetry = 5;

            while (Session == null || (!Session.State.Connected && retry < maxRetry && !StopRequested))
            {
                retry++;
                Thread.Sleep(100);
                CreateSessionAndStart();
            }

            return (Session == null || Session.State.Connected);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Session != null)
                {
                    try
                    {
                        Session.Close();
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine(exception);
                        throw;
                    }
                }

                Running = false;
            }
        }
    }
}