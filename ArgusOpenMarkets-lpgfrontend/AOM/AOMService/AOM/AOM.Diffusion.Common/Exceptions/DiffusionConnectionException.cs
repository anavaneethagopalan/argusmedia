﻿using System;

namespace AOMDiffusionCommon.Exceptions
{
    [Serializable]
    public class DiffusionConnectionException : Exception
    {
        // The default constructor needs to be defined
        // explicitly now since it would be gone otherwise.

        public DiffusionConnectionException()
        {
        }

        public DiffusionConnectionException(string message)
            : base(message)
        {
        }
    }
}