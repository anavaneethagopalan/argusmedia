﻿using AOMDiffusion.Common.Interfaces;

namespace AOMDiffusion.Common
{    
    public class AuthenticationDiffusionTopicManager : AomDiffusionTopicManager, IAuthenticationDiffusionTopicManager
    {
        private const string AomRootTopic = "AOM/Users";       

        public AuthenticationDiffusionTopicManager() : base(AomRootTopic)
        {         
               
        }       
    }
}