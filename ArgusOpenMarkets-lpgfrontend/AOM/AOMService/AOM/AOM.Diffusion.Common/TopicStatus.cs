﻿namespace AOMDiffusion.Common
{
	public enum TopicStatus
	{
		Waiting,
		Unknown,
		Discard,
		Exists,
	}
}