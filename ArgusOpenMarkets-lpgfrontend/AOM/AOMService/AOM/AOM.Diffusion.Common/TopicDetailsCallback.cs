﻿using System.Diagnostics.CodeAnalysis;

namespace AOMDiffusion.Common
{
    using System;
    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Topics;
    using Utils.Logging.Utils;

    public class TopicDetailsCallback : ITopicDetailsCallback
    {
        public event EventHandler<string> TopicDetails;

        public event EventHandler<string> TopicDoesNotExist;

        public ITopicDetails Details { get; set; }

        [ExcludeFromCodeCoverage]
        public void OnDiscard()
        {
            Log.Warn("TopicDetailsCallback.OnDiscard");
        }

        public void OnTopicDetails(string topicPath, ITopicDetails details)
        {
            Details = details;
            if (TopicDetails != null)
            {
                TopicDetails(this, "GotDetails");
            }
        }

        public void OnTopicUnknown(string topicPath)
        {
            if (TopicDoesNotExist != null)
            {
                TopicDoesNotExist(this, "NoTopic");
            }
        }
    }
}