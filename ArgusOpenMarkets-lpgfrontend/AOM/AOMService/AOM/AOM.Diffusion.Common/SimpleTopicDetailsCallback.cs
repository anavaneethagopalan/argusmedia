﻿namespace AOMDiffusion.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Topics;
    using PushTechnology.DiffusionCore.Utilities;
    using Utils.Logging.Utils;

    public class SimpleTopicDetailsCallback : ITopicDetailsContextCallback<Nothing>
    {
        private TopicStatus _status = TopicStatus.Waiting;

        private Dictionary<String, ITopicDetails> topics = new Dictionary<String, ITopicDetails>();

        public void OnTopicDetails(Nothing context, string topicPath, ITopicDetails details)
        {
            Debug.WriteLine("onTopicDetails: Topic: '" + topicPath + "' has details: '" + details + "'");

            try
            {
                topics.Add(topicPath, details);
            }
            catch (Exception ex)
            {
                Log.Error("Error adding topic to diffusion", ex);
            }

            _status = TopicStatus.Exists;
        }

        public void OnTopicUnknown(Nothing context, string topicPath)
        {
            Debug.WriteLine("onTopicUnknown: Topic unknown: '" + topicPath + "'");
            if (topics.ContainsKey(topicPath))
            {
                topics.Remove(topicPath);
            }
            _status = TopicStatus.Unknown;
        }

        public void OnDiscard(Nothing context)
        {
            Debug.WriteLine("onDiscard: topic discarded ");
            _status = TopicStatus.Discard;
        }

        public bool TopicExists(String topic)
        {
            bool b = TopicData(topic) != null;
            Debug.WriteLine("Checking if topic '" + topic + "' exists returns : '" + b + "'");
            return b;
        }

        public ITopicDetails TopicData(String topic)
        {
            ITopicDetails details = (topics.ContainsKey(topic)) ? topics[topic] : null;
            Debug.WriteLine("Details: '" + ((details == null) ? "null" : details.ToString()) + "'");
            return details;
        }

        public TopicStatus Status()
        {
            return _status;
        }

        public void Status(TopicStatus status)
        {
            _status = status;
        }

        public void ClearStatus()
        {
            Status(TopicStatus.Waiting);
        }

        public bool HasResult()
        {
            switch (_status)
            {
                case TopicStatus.Waiting:
                    return false;
                default:
                    return true;
            }
        }
    }
}