﻿namespace AOMDiffusion.Common
{
    using PushTechnology.ClientInterface.Client.Content;
    using PushTechnology.ClientInterface.Client.Features;

    using System;
    using System.Diagnostics;
    using System.Threading;

    internal class SimpleFetchStream : IFetchStream
    {
        private string _val;

        private FetchState _state = FetchState.Waiting;

        public enum FetchState
        {
            Waiting,
            Replied,
            Closed,
            Discarded,
        }

        public void OnFetchReply(string topicPath, IContent content)
        {
            Debug.WriteLine("simpleFetchStream: onFetchReply...");
            _val = content.AsString();
            _state = FetchState.Replied;
        }

        public void OnClose()
        {
            Debug.WriteLine("simpleFetchStream: onClose...");
            _state = FetchState.Closed;
        }

        public void OnDiscard()
        {
            Debug.WriteLine("simpleFetchStream: onDiscard...");
            _state = FetchState.Discarded;
        }

        public String GetValue()
        {
            if (HasState())
            {
                return _val;
            }

            Thread.Sleep(10);
            return GetValue();
        }

        public FetchState State()
        {
            return _state;
        }

        public bool HasState()
        {
            switch (_state)
            {
                case FetchState.Waiting:
                    return false;
                default:
                    return true;
            }
        }
    }
}