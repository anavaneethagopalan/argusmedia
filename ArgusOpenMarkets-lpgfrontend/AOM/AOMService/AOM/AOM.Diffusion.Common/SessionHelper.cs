using AOM.App.Domain.Services;
using AOM.Repository.MySql.Crm;
using PushTechnology.ClientInterface.Client.Details;
using ServiceStack.Text;

namespace AOMDiffusion.Common
{
    public class SessionHelper : ISessionHelper
    {
        private readonly IUserService _userHelper;

        public SessionHelper(IUserService userHelper)
        {
            _userHelper = userHelper;
        }

        public UserInfoDto GetUserInfoDto(ISessionDetails sessionDetails)
        {
            var username = sessionDetails.Summary.Principal;

            if (username.StartsWithIgnoreCase("AOM-"))
            {
                username = username.Substring("AOM-".Length);
            }
            return _userHelper.GetUserInfoDto(username);
        }

        public bool IsAuthenticationUser(ISessionDetails sessionDetails)
        {
            var username = sessionDetails.Summary.Principal;
            return (username == "AuthenticationAdmin" || string.IsNullOrEmpty(username)) ;
        }
    }
}