﻿using AOMDiffusion.Common.Interfaces;

namespace AOMDiffusion.Common
{
    public class OrdersDiffusionTopicManager : AomDiffusionTopicManager, IOrdersDiffusionTopicManager
    {
        public OrdersDiffusionTopicManager()
            : base("AOM/Orders")
        {
            
        }
    }
}
