﻿using AOMDiffusion.Common;

namespace AOMDiffusion.Common
{
    using AOMDiffusion.Common.Interfaces;

    public class OrganisationDiffusionTopicManager : AomDiffusionTopicManager, IOrganistionDiffusionTopicManager
    {
        public OrganisationDiffusionTopicManager()
            : base("AOM/Organisations")
        {

        }
    }
}