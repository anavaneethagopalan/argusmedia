using AOM.Repository.MySql.Crm;
using PushTechnology.ClientInterface.Client.Details;

namespace AOMDiffusion.Common
{
    public interface ISessionHelper
    {
        UserInfoDto GetUserInfoDto(ISessionDetails sessionDetails);
        bool IsAuthenticationUser(ISessionDetails sessionDetails);
    }
}