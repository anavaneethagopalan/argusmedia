﻿using AOMDiffusion.Common.Interfaces;

namespace AOMDiffusion.Common
{
    public class AdminDiffusionTopicManager : AomDiffusionTopicManager, IAdminDiffusionTopicManager
    {
        public AdminDiffusionTopicManager() : base("AOM/Admin")
        { }
    }
}
