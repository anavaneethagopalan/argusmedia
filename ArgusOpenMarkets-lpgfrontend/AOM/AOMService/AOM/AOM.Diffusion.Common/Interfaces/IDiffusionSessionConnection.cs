﻿namespace AOMDiffusion.Common.Interfaces
{
    using System;

    using PushTechnology.ClientInterface.Client.Session;

    public interface IDiffusionSessionConnection : IDisposable
    {
        bool ConnectAndStartSession();

        void Disconnect();

        ISession Session { get; }
    }
}