﻿namespace AOMDiffusion.Common.Interfaces
{
    using System.Collections.Generic;

    using AOM.Transport.Events;

    using PushTechnology.ClientInterface.Client.Features.Control.Clients;
    using PushTechnology.ClientInterface.Client.Session;

    public interface IDiffusionSessionManager : ISessionDetailsListener
    {
        IDictionary<ClientSessionInfo, UserSessionInfo> SessionsDictionary { get; }

        ClientSessionInfo RegisterSession(long userId, long organisationId, SessionId sessionId);

        SessionId GetSession(ClientSessionInfo clientSessionInfo);

        IEnumerable<ClientSessionInfo> GetUserSessions(long userId);

        ClientSessionInfo GetUserSession(string currentSessionId);

        IEnumerable<ClientSessionInfo> GetUserOtherSessions(string currentSessionId, long userId);

        IEnumerable<UserSessionInfo> GetUserSessionsUserInfo(long userId);

        void RemoveUserSessions(long userId);

        void RemoveUserSessionId(SessionId sessionId, long userId);

        void RemoveUserOtherSessions(string currentSessionId, long userId);

        void AttachToSession(ISession session);
    }
}