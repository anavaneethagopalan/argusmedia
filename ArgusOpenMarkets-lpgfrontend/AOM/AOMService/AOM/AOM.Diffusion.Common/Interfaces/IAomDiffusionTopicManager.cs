﻿using System;
using PushTechnology.ClientInterface.Client.Features;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using PushTechnology.ClientInterface.Client.Session;

namespace AOMDiffusion.Common.Interfaces
{
    public interface IAomDiffusionTopicManager
    {
        void DeleteTopic(string topicName, string pathselector, ITopicControlRemoveCallback topicControlRemoveCallback);

        void CreateTopic(string topicToSetup, TopicControlAddCallback addCallback);

        void CreateTopic(string topicToSetup, TopicControlAddCallback addCallback, Action callBackIfTopicExists);

        ISession Session { get; }

        void AttachToSessionAndRegisterTopicSource(ISession session);

        void SendDataToTopic(string topic, string message, ITopicUpdaterUpdateCallback callback = null);

        //void CreateTopic(string topicToSetup, SimpleTopicSourceHandler topicSource);        
        void AddMessageHandler(string topicPath, IMessageHandler handler);

        ISimpleTopicSourceHandler GetUpdater();

        void GetTopicDetails(string topic, ITopicDetailsCallback topicDetailsCallback);

        event EventHandler Active;

        event EventHandler StandBy;

        void GetTopicData(string topicName, string context, IFetchContextStream<string> callback);
    }
}