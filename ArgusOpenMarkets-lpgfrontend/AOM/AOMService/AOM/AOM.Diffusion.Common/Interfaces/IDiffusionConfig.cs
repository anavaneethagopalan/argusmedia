﻿namespace AOMDiffusion.Common.Interfaces
{
    public interface IDiffusionConfig
    {
        string DiffusionEndPoint { get; }
        string DiffusionUsername { get; }
        string DiffusionPassword { get; }
        string DiffusionHandlerName { get; }
    }
}