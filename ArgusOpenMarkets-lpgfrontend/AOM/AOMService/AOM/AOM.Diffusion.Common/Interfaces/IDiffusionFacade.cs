﻿using PushTechnology.DiffusionCore.Utilities;

namespace AOMDiffusion.Common.Interfaces
{
    using AOM.Transport.Events;

    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using PushTechnology.ClientInterface.Client.Session;
    using PushTechnology.ClientInterface.Utils;

    using System;
    using System.Collections.Generic;

    public interface IDiffusionFacade
    {
        void DeleteTopic(string topicName, string pathselector, ITopicControlRemoveCallback topicControlRemoveCallback);

        void CreateTopic(string topicToSetup, TopicControlAddCallback addCallback);

        void CreateTopic(string topicToSetup, TopicControlAddCallback addCallback, Action callBackIfTopicExists);

        ISession Session { get; }

        void AttachToSession(ISession session);

        void SendDataToTopic(string topic, string message, TopicUpdateCallback callback = null);

        //void CreateTopic(string topicToSetup, SimpleTopicSourceHandler topicSource);
        void RegisterTopicSource();

        void AddMessageHandler(string topicPath, IMessageHandler handler);

        SimpleTopicSourceHandler GetUpdater();

        void GetTopicDetails(string topic, ITopicDetailsContextCallback<Nothing> topicDetailsCallback);

        ClientSessionInfo RegisterSession(long userId, long organisationId, SessionId sessionId);

        ClientSessionInfo GetUserSession(string currentSessionId);

        SessionId GetSession(ClientSessionInfo clientSessionInfo);

        IEnumerable<ClientSessionInfo> GetUserSessions(long userId);

        IEnumerable<ClientSessionInfo> GetUserOtherSessions(string currentSessionId, long userId);

        void SetOnActiveCallback(Action onActive);

        void SetOnStandbyCallback(Action onActive);

        void GetTopicData(string topicName, string context, IFetchContextStream<string> callback);

        void RemoveUserSessions(long userId);

        void RemoveUserOtherSessions(string currentSessionId, long userId);
    }
}