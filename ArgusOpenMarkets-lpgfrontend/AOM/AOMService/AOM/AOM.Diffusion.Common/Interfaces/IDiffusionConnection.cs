﻿namespace AOMDiffusion.Common.Interfaces
{
    public interface IPushGateway : IGateway
    {
        //void Connect(string url, string username, string password, string authType);
        void Connect();
        void Disconnect();
        void AddTopic(string topicName);
        void RemoveTopic(string topicName);
        void PushToTopic(string topicName, string message, bool deltaOnly);
        string GetMsgFromTopic(string topicName);
    }
}