﻿using PushTechnology.ClientInterface.Client.Session;
using DiffusionFactory = PushTechnology.ClientInterface.Client.Factories.Diffusion;

namespace AOMDiffusion.Common.Interfaces
{
    public interface IAomDiffusionSessionFactory
    {
        ISessionFactory GetDiffusionSessionFactory(string username, string password);
    }
}
