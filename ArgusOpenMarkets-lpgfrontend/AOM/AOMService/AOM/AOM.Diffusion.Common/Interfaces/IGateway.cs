﻿namespace AOMDiffusion.Common.Interfaces
{
    public interface IGateway
    {
        void Start();
        void Stop();
    }
}