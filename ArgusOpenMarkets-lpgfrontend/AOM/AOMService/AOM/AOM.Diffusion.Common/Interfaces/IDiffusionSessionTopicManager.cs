﻿namespace AOMDiffusion.Common.Interfaces
{
    using PushTechnology.ClientInterface.Client.Session;

    using System;

    public interface IDiffusionSessionTopicManager
    {
        void PushToTopic(string message, bool deltaOnly = true);

        ISession Session { get; }

        event EventHandler<string> TopicReadyForUpdates;
    }
}