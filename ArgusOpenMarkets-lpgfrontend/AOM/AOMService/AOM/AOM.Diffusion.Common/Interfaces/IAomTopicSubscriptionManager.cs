﻿namespace AOMDiffusion.Common.Interfaces
{
    internal interface IAomTopicSubscriptionManager
    {
        void SubscribeToSessionTopics();
    }
}