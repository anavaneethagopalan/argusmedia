﻿using System;
using PushTechnology.ClientInterface.Client.Callbacks;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using PushTechnology.ClientInterface.Data.JSON;

namespace AOMDiffusion.Common.Interfaces
{
    public interface ISimpleTopicSourceHandler : ITopicUpdateSource
    {
        event EventHandler<string> Active;

        event EventHandler<string> Closed;

        event EventHandler<string> StandBy;

        string OwnerName { get; set; }

        new void OnActive(string topicPath, ITopicUpdater updater);

        new void OnClose(string topicPath);

        new void OnError(string topicPath, ErrorReason errorReason);

        void SetOnActiveCallback(Action onActive);

        void SetOnStandbyCallback(Action onStandby);

        new void OnStandby(string topicPath);

        TopicSourceState State();

        bool HasState();

        bool IsActive();

        void PushToTopic(string topic, IJSON field, Boolean deltaOnly, ITopicUpdaterUpdateCallback callback = null);
    }
}
