﻿using AOMDiffusion.Common.Interfaces;

namespace AOMDiffusion.Common
{
    public class HealthCheckDiffusionTopicManager : AomDiffusionTopicManager, IHealthCheckDiffusionTopicManager
    {
        public HealthCheckDiffusionTopicManager()
            : base("AOM/HealthCheck")
        {
            
        }
    }
}