﻿using System.Diagnostics.CodeAnalysis;

namespace AOMDiffusion.Common
{
    using System;

    using PushTechnology.ClientInterface.Client.Features.Control.Topics;

    public class TopicControlDeleteCallback : ITopicControlRemoveCallback
    {
        private const string DefaultTopicName = "Topic Removed";
        public string TopicName { get; set; }
        public event EventHandler<string> TopicRemoved;

        public TopicControlDeleteCallback()
        {
            TopicName = DefaultTopicName;
        }

        public TopicControlDeleteCallback(string topicName)
        {
            TopicName = topicName;
        }

        [ExcludeFromCodeCoverage]
        public void OnDiscard()
        {
            Console.WriteLine("OnDiscard");
        }

        public void OnTopicsRemoved()
        {
            if (TopicRemoved != null)
            {
                TopicRemoved(this, TopicName);
            }
        }
    }
}