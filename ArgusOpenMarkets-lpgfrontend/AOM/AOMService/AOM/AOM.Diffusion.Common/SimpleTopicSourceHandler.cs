﻿using PushTechnology.ClientInterface.Client.Callbacks;
using PushTechnology.ClientInterface.Client.Content;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using AOMDiffusion.Common.Interfaces;
using PushTechnology.ClientInterface.Data.JSON;
using Utils.Logging.Utils;

namespace AOMDiffusion.Common
{
    public class SimpleTopicSourceHandler : ISimpleTopicSourceHandler
    {
        protected ITopicUpdater Updater;

        protected IRegistration Handler;

        private TopicSourceState _state = TopicSourceState.Waiting;

        public event EventHandler<string> Active;

        public event EventHandler<string> Closed;

        public event EventHandler<string> StandBy;

        public string OwnerName { get; set; }

        private Action _callbackOnActive;

        private Action _callbackOnStandby;

        public void OnActive(string topicPath, ITopicUpdater updater)
        {
            Log.Info("SimpleTopicSourceHandler:onActive: '" + topicPath + "'");
            Updater = updater;
            _state = TopicSourceState.Active;
            if (Active != null)
            {
                Active(this, "Active");
            }
            if (_callbackOnActive != null)
            {
                _callbackOnActive();
            }
        }

        public void OnClose(string topicPath)
        {
            Log.Info("OnClose topic updater");
            Log.Info("SimpleTopicSourceHandler:onClosed: '" + topicPath + "'");
            _state = TopicSourceState.Closed;
            if (Closed != null)
            {
                Closed(this, "Closed");
            }
        }

        [ExcludeFromCodeCoverage]
        public void OnError(string topicPath, ErrorReason errorReason)
        {
            Log.ErrorFormat("SimpleTopicSourceHandler OnError for {0}: {1}", topicPath, errorReason);
        }

        public void OnRegistered(string topicPath, IRegistration registration)
        {
            Log.InfoFormat("OnRegistererd topic updater for {0}", topicPath);

            Debug.WriteLine("OnRegistered: '" + topicPath + "'");
            Handler = registration;
        }

        public void SetOnActiveCallback(Action onActive)
        {
            _callbackOnActive = onActive;
        }

        public void SetOnStandbyCallback(Action onStandby)
        {
            _callbackOnStandby = onStandby;
        }

        public void OnStandby(string topicPath)
        {
            Log.Info("OnStandby topic updater");

            Log.Info("SimpleTopicSourceHandler:onStandBy: '" + topicPath + "'");
            _state = TopicSourceState.StandBy;
            if (StandBy != null)
            {
                StandBy(this, "StandBy");
            }

            if (_callbackOnStandby != null)
            {
                _callbackOnStandby();
            }
        }

        public TopicSourceState State()
        {
            return _state;
        }

        public bool HasState()
        {
            switch (_state)
            {
                case TopicSourceState.Waiting:
                    return false;
                default:
                    return true;
            }
        }

        public bool IsActive()
        {
            switch (_state)
            {
                case TopicSourceState.Active:
                    return true;
                default:
                    return false;
            }
        }

        public void PushToTopic(string topic, IJSON jsonField, Boolean deltaOnly, ITopicUpdaterUpdateCallback callback = null)
        {
            if (callback == null)
            {
                callback = new TopicUpdateCallback();
            }

            if (Updater == null)
            {
                Log.Info("SimpleTopicSourceHandler: Updater has not been initialised for topic: " + topic);
            }
            else
            {
                switch (_state)
                {
                    case TopicSourceState.Active:
                        // IContent msg = PushTechnology.ClientInterface.Client.Factories.Diffusion.Content.NewContent(field);
                        try
                        {
                            //Updater.Update(topic, jsonField, );
                            Updater.Update(topic, jsonField, callback);
                        }
                        catch (Exception ex)
                        {
                            Log.ErrorFormat("Error pushing to topic '{0}' --> {1}", topic, ex);
                        }
                        break;

                    default:
                        Log.Info("Push to Topic State is: '" + _state + "' - bypassing update");
                        break;
                }
            }
        }
    }
}