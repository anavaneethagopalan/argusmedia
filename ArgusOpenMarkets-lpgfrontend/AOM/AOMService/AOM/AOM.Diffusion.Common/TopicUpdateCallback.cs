﻿using System.Diagnostics.CodeAnalysis;

namespace AOMDiffusion.Common
{
    using System;

    using PushTechnology.ClientInterface.Client.Features.Control.Topics;

    using Utils.Logging.Utils;

    public class TopicUpdateCallback : ITopicUpdaterUpdateCallback
    {
        public event EventHandler<string> OnUpdate;
        public event EventHandler<string> OnTopicUpdateError;

        private string _topicName = "Unknown";
        private string _message = "Unknown";

        public TopicUpdateCallback()
        {
            
        }

        public TopicUpdateCallback(string topicName, string message)
        {
            _topicName = topicName;
            _message = message;
        }

        [ExcludeFromCodeCoverage]
        public void OnSuccess(string topicPath)
        {
            Log.Info("TopicUpdateCallback - OnSuccess.  TopicPath" + topicPath);
        }

        [ExcludeFromCodeCoverage]
        public void OnError(string topicPath, TopicSourceErrorDetails error)
        {
            Log.Error(topicPath + "TopicUpdateCallback.OnError - Topic Update Failed " + error.Name);
        }

        public void OnSuccess()
        {
            //Log.Info(string.Format("TopicUpdateCallback.OnSuccess: {0} - {1}", _topicName, _message));
            if (OnUpdate != null)
            {
                OnUpdate(this, string.Format("Updated.   Topic: {0}   Message: {1}", _topicName, _message));
            }
        }

        public void OnError(PushTechnology.ClientInterface.Client.Callbacks.ErrorReason errorReason)
        {
            Log.Error("TopicUpdateCallback.OnError - Update Failed " + errorReason);

            if (OnTopicUpdateError != null)
            {
                OnTopicUpdateError(this, string.Format("TopicUpdateCallback.OnError - Update Failed for topic:{0}  with message:{1}   Error Reason:{0}" + errorReason, _topicName, _message));
            }
        }
    }
}