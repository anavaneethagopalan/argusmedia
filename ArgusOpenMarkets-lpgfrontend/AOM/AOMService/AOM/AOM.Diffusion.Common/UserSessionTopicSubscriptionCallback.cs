﻿using System.Diagnostics.CodeAnalysis;

namespace AOMDiffusion.Common
{
    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using PushTechnology.ClientInterface.Client.Session;
    using Utils.Logging.Utils;

    internal class UserSessionTopicSubscriptionCallback : ISubscriptionCallback
    {
        [ExcludeFromCodeCoverage]
        public void OnDiscard()
        {
            Log.Debug("Session discarded");
        }

        [ExcludeFromCodeCoverage]
        public void OnComplete(SessionId client, string selector)
        {
            Log.Debug("AOM Client selected topics using " + selector);
        }
    }
}