﻿namespace AOMDiffusion.Common
{
    public enum TopicSourceState
    {
        Waiting,
        Active,
        StandBy,
        Closed,
    }
}