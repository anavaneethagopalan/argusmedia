﻿namespace AOMDiffusion.Common
{
    using System;
    using System.Globalization;

    public static class ArgumentValidationExtensions
    {
        public static TArgument ArgumentIsNotNull<TArgument>(this TArgument argument, string argumentName)
            where TArgument : class
        {
            if (argument == null)
            {
                throw new ArgumentNullException(argumentName);
            }

            return argument;
        }

        public static TArgument? ArgumentIsNotNull<TArgument>(this TArgument? argument, string argumentName)
            where TArgument : struct
        {
            if (argument.HasValue)
            {
                throw new ArgumentNullException(argumentName);
            }

            // TODO: ?? Always null
            return argument;
        }

        public static string ArgumentStringNotNullOrEmpty(this string value, string name)
        {
            StringNotNullOrEmptyInternal(name, "name");
            StringNotNullOrEmptyInternal(value, name);

            return value;
        }

        private static void StringNotNullOrEmptyInternal(string value, string name)
        {
            if (value == null)
            {
                throw new ArgumentException("value");
            }

            if (value.Length == 0)
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, name));
            }
        }
    }
}