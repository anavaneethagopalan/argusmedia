﻿using System.Diagnostics.CodeAnalysis;

namespace AOMDiffusion.Common
{
    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using PushTechnology.ClientInterface.Client.Session;

    using Utils.Logging.Utils;

    public class SubscribeTopicCallback : ISubscriptionCallback
    {
        [ExcludeFromCodeCoverage]
        public void OnComplete(SessionId client, string selector)
        {
            Log.Info("Successfully subscribed to topic");
        }

        [ExcludeFromCodeCoverage]
        public void OnDiscard()
        {
            Log.Info("Discard Subscription Topic");
        }
    }
}