﻿using AOM.App.Domain.Dates;
using AOM.Transport.Events;
using Argus.Transport.Infrastructure;
using PushTechnology.ClientInterface.Client.Details;
using PushTechnology.ClientInterface.Client.Features;
using PushTechnology.ClientInterface.Client.Session;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using AOM.Transport.Events.Users;
using AOMDiffusion.Common.Interfaces;
using Utils.Logging.Utils;

namespace AOMDiffusion.Common
{
    public class DiffusionSessionManager : IDiffusionSessionManager
    {
        public IDictionary<ClientSessionInfo, UserSessionInfo> SessionsDictionary { get; set; }

        private readonly ISessionHelper _sessionHelper;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IBus _aomBus;

        public DiffusionSessionManager(ISessionHelper sessionHelper, IDateTimeProvider dateTimeProvider, IBus aomBus)
        {
            _sessionHelper = sessionHelper;
            _dateTimeProvider = dateTimeProvider;
            SessionsDictionary = new ConcurrentDictionary<ClientSessionInfo, UserSessionInfo>();
            _aomBus = aomBus;
        }

        public void AttachToSession(ISession session)
        {
            var allDetails = DetailType.Values().ToArray();
            session.GetClientControlFeature().SetSessionDetailsListener(allDetails, this);
        }

        public void OnActive(IRegisteredHandler registeredHandler)
        {
            Log.Info("Session Active");
        }

        public void OnClose()
        {
            Log.Info("Session Closed");
        }

        public void OnSessionOpen(SessionId sessionId, ISessionDetails sessionDetails)
        {
            Log.Info(string.Format("Session.OnSessionOpen: {0} | {1}", sessionDetails.Summary.Principal, sessionId));

            if (_sessionHelper.IsAuthenticationUser(sessionDetails)) return;
            var userInfo = _sessionHelper.GetUserInfoDto(sessionDetails);
            
            if (userInfo != null)
            {
                RegisterSession(userInfo.Id, userInfo.Organisation_Id_fk, sessionId);
            }
        }

        public void OnSessionUpdate(SessionId sessionId, ISessionDetails sessionDetails)
        {
            Log.Info(string.Format("Session.OnSessionUpdate: {0} | {1}", sessionDetails.Summary.Principal, sessionId));

            if (_sessionHelper.IsAuthenticationUser(sessionDetails)) return;

            var userInfo = _sessionHelper.GetUserInfoDto(sessionDetails);
            if (userInfo != null)
            {
                RegisterSession(userInfo.Id, userInfo.Organisation_Id_fk, sessionId);
            }
            else
            {
                var username = sessionDetails.Summary.Principal;
                Log.InfoFormat("Could not GetUserInfo for the username: {0}", username);
            }
        }

        public void OnSessionClose(SessionId sessionId, ISessionDetails sessionDetails, CloseReason closeReason)
        {
            Log.Info(string.Format("Session.OnSessionClose: {0} | {1} REASON {2}", sessionDetails.Summary.Principal, sessionId, closeReason.Name));

            if (_sessionHelper.IsAuthenticationUser(sessionDetails)) return;

            var userInfo = _sessionHelper.GetUserInfoDto(sessionDetails);

            if (userInfo != null)
            {
                BlankUserTopic(userInfo.Id);

                var clientSessionInfo = new ClientSessionInfo
                {
                    UserId = userInfo.Id,
                    OrganisationId = userInfo.Organisation_Id_fk,
                    SessionId = sessionId.ToString()
                };

                if (SessionsDictionary.ContainsKey(clientSessionInfo))
                {
                    SessionsDictionary[clientSessionInfo].SessionTerminatedByAnotherLogin = true;
                    Log.Info("OnSessionClose - Removed user from dictionary: " +  clientSessionInfo.UserId);
                }

                _aomBus.Publish(new UserDisconnectedResponse
                {
                    UserId = userInfo.Id,
                    DisconnectReason = closeReason.Name,
                    Username = sessionDetails.Summary.Principal
                });
            }
            Console.WriteLine("Session Closed");
        }

        private void BlankUserTopic(long userId)
        {
            var clientSessions = GetUserSessions(userId);

            if (clientSessions.Count() <= 1)
            {
                // Only blank the users topic if the session count is dropping back to 1.
                var request = new BlankUserTopic();
                request.UserId = userId;
                _aomBus.Publish(request);
                foreach (var csi in clientSessions)
                {
                    SessionsDictionary.Remove(csi);
                }
            }
            else
            {
                foreach (var sess in clientSessions)
                {
                    if (SessionsDictionary[sess].SessionTerminatedByAnotherLogin)
                    {
                        // We can now remove this session.
                        SessionsDictionary.Remove(sess);
                    }
                }
            }
        }

        public ClientSessionInfo RegisterSession(long userId, long organisationId, SessionId sessionId)
        {
            var csi = new ClientSessionInfo
            {
                UserId = userId,
                OrganisationId = organisationId,
                SessionId = sessionId.ToString()
            };

            if (SessionsDictionary.ContainsKey(csi))
            {
                SessionsDictionary[csi].LastSeen = _dateTimeProvider.UtcNow;
                Log.Info("RegisterSession - Session alread exists - updated last seen time in dictionary for user: " + csi.UserId);
            }
            else
            {
                SessionsDictionary[csi] = new UserSessionInfo
                {
                    SessionId = sessionId,
                    LastSeen = _dateTimeProvider.UtcNow
                };


                // Publish the message to subscribe the user to ALL topic(s) that he/she is entitled.
                Log.InfoFormat("Requesting --> TopicSubscribeRequest");
                var topicSubscribeRequest = new TopicSubscribeRequest
                {
                    ClientSessionInfo = csi
                };
                _aomBus.Publish(topicSubscribeRequest);

                Log.Info("RegisterSession - added user to dictionary: " + csi.UserId);
            }

            return csi;
        }

        public SessionId GetSession(ClientSessionInfo csi)
        {
            UserSessionInfo sessionInfo;
            SessionsDictionary.TryGetValue(csi, out sessionInfo);
            return sessionInfo != null ? sessionInfo.SessionId : null;
        }

        public ClientSessionInfo GetUserSession(string currentSessionId)
        {
            //StackTrace st = new StackTrace();
            Log.DebugFormat("GetUserSession being called: we are searching for the session --> {0}"); //": {1}", currentSessionId, st.ToString());

            if (SessionsDictionary == null)
            {
                var message = string.Format("GetUserSession for session {0}: Session Dictionary is empty", currentSessionId);
                throw new ApplicationException(message);
            }
            Log.DebugFormat("Session Dictionary contains {0} items", SessionsDictionary.Count);

            ClientSessionInfo csi = SessionsDictionary.Keys.FirstOrDefault(c => c.SessionId == currentSessionId);

            if (csi == null)
            {
                throw new ApplicationException(string.Format("GetUserSession for session ID {0} failed, session ID not found in Session Dictionary", currentSessionId));
            }
            
            return csi;
        }

        public IEnumerable<ClientSessionInfo> GetUserSessions(long userId)
        {
            IEnumerable<ClientSessionInfo> csis = SessionsDictionary.Keys.Select(c => c).Where(c => c.UserId == userId);
            return csis;
        }

        public IEnumerable<ClientSessionInfo> GetUserOtherSessions(string currentSessionId, long userId)
        {
            return SessionsDictionary.Keys.Select(c => c).Where(c => c.UserId == userId && c.SessionId != currentSessionId);
        }

        public IEnumerable<UserSessionInfo> GetUserSessionsUserInfo(long userId)
        {
            return SessionsDictionary.Where(k => k.Key.UserId == userId).Select(v => v.Value);
        }

        public void RemoveUserSessions(long userId)
        {
            var userSessions = GetUserSessions(userId);
            foreach (var session in userSessions)
            {
                SessionsDictionary.Remove(session);
                Log.DebugFormat("RemoveUserSessions removing {0}", session.SessionId);
            }
        }

        public void RemoveUserSessionId(SessionId sessionId, long userId)
        {
            var userSessions = GetUserSessions(userId);
            var sessions = userSessions.Where(x => x.SessionId == sessionId.ToString()).ToList();

            foreach (var session in sessions)
            {
                SessionsDictionary[session].SessionTerminatedByAnotherLogin = true;
                Log.DebugFormat("RemoveUserSession removing {0}", session.SessionId);
            }
        }

        public void RemoveUserOtherSessions(string currentSessionId, long userId)
        {
            var userSessions = GetUserSessions(userId);

            if (!string.IsNullOrEmpty(currentSessionId))
            {
                var userSessionsLessCurrentSession = userSessions.Where(x => x.SessionId != currentSessionId).ToList();

                foreach (var session in userSessionsLessCurrentSession)
                {
                    SessionsDictionary[session].SessionTerminatedByAnotherLogin = true;
                }
            }
            else
            {
                foreach (var session in userSessions)
                {
                    SessionsDictionary[session].SessionTerminatedByAnotherLogin = true;
                }
            }
        }
    }
}