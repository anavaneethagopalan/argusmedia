﻿using AOMDiffusion.Common.Interfaces;

namespace AOMDiffusion.Common
{
    using System.Configuration;

    public class DiffusionConfig : IDiffusionConfig
    {
        public string DiffusionEndPoint { get; private set; }

        public string DiffusionUsername { get; private set; }

        public string DiffusionPassword { get; private set; }

        public string DiffusionHandlerName { get; private set; }

        public DiffusionConfig()
        {
            DiffusionEndPoint = ConfigurationManager.AppSettings["DiffusionEndPoint"];
            if (string.IsNullOrEmpty(DiffusionEndPoint))
            {
                throw new ConfigurationErrorsException("Missing 'DiffusionEndPoint' configuration setting");
            }
            DiffusionUsername = ConfigurationManager.AppSettings["DiffusionUsername"];
            if (string.IsNullOrEmpty(DiffusionUsername))
            {
                throw new ConfigurationErrorsException("Missing 'DiffusionUsername' configuration setting");
            }

            DiffusionPassword = ConfigurationManager.AppSettings["DiffusionPassword"];
            if (string.IsNullOrEmpty(DiffusionPassword))
            {
                throw new ConfigurationErrorsException("Missing 'DiffusionPassword' configuration setting");
            }
            DiffusionHandlerName = ConfigurationManager.AppSettings["DiffusionHandlerName"];
            if (string.IsNullOrEmpty(DiffusionHandlerName))
            {
                throw new ConfigurationErrorsException("Missing 'DiffusionHandlerName' configuration setting");
            }
        }
    }
}