﻿using AOMDiffusion.Common.Interfaces;

namespace AOMDiffusion.Common
{
    public class NewsDiffusionTopicManager : AomDiffusionTopicManager, INewsDiffusionTopicManager
    {
        public NewsDiffusionTopicManager()
            : base("AOM/News")
        {
            
        }
    }
}
