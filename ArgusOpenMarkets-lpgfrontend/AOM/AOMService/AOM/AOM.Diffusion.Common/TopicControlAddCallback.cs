﻿using System.Diagnostics.CodeAnalysis;

namespace AOMDiffusion.Common
{
    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using System;
    using Utils.Logging.Utils;

    public class TopicControlAddCallback : ITopicControlAddCallback
    {
        public event EventHandler<string> TopicAdded;

        public event EventHandler<TopicAddFailReason> TopicAddFailed;

        [ExcludeFromCodeCoverage]
        public void OnDiscard()
        {
            Log.Warn("TopicControlAddCallback.OnDiscard");
        }

        public void OnTopicAdded(string topicPath)
        {
            if (TopicAdded != null)
            {
                TopicAdded(this, topicPath);
            }
        }

        public void OnTopicAddFailed(string topicPath, TopicAddFailReason reason)
        {
            if (TopicAddFailed != null)
            {
                Log.DebugFormat("Topic Add Failed for Topic Path:{0}  For the reason: {1}", topicPath, reason.ToString());
                TopicAddFailed(this, reason);
            }
        }
    }
}