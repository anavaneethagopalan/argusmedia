﻿namespace AOM.Diffusion.Common.v1.Interfaces
{
    using System.Collections.Generic;

    using AOM.Transport.Events;

    using PushTechnology.ClientInterface.Client.Features.Control.Clients;
    using PushTechnology.ClientInterface.Client.Session;

    public interface IDiffusionSessionManager : ISessionDetailsListener
    {
        IDictionary<ClientSessionInfo, UserSessionInfo> SessionsDictionary { get; }

        ClientSessionInfo RegisterSession(long userId, long organisationId, SessionId sessionId);
    }
}