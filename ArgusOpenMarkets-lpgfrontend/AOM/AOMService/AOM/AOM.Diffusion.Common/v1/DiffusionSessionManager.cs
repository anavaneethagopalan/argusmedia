﻿using AOM.Diffusion.Common.v1.Interfaces;

namespace AOM.Diffusion.Common.v1
{
    using AOM.App.Domain.Dates;

    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;

    using AOM.App.Domain.Services;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;

    using PushTechnology.ClientInterface.Client.Details;
    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Session;

    using Utils.Logging.Utils;

    public class DiffusionSessionManager : IDiffusionSessionManager
    {
        private readonly IUserService _userService;

        private readonly IDateTimeProvider _dateTimeProvider;

        public DiffusionSessionManager(IUserService userService, IDateTimeProvider dateTimeProvider)
        {
            _userService = userService;
            _dateTimeProvider = dateTimeProvider;
            SessionsDictionary = new ConcurrentDictionary<ClientSessionInfo, UserSessionInfo>();
        }

        public void OnActive(IRegisteredHandler registeredHandler)
        {
            Log.Info("Session Active");
        }

        public void OnClose()
        {
            Log.Info("Session Closed");
        }

        public void OnSessionOpen(SessionId sessionId, ISessionDetails sessionDetails)
        {
            Log.Info(string.Format("Session.OnSessionOpen: {0} | {1}", sessionDetails.Summary.Principal, sessionId));

            var username = sessionDetails.Summary.Principal;

            if (username == "AuthenticationAdmin" || string.IsNullOrEmpty(username)) return;

            var userInfo = _userService.GetUserInfoDto(username);

            RegisterSession(userInfo.Id, userInfo.Organisation_Id_fk, sessionId);
        }

        public void OnSessionUpdate(SessionId sessionId, ISessionDetails sessionDetails)
        {
            Log.Info(string.Format("Session.OnSessionUpdate: {0} | {1}", sessionDetails.Summary.Principal, sessionId));

            var username = sessionDetails.Summary.Principal;

            if (username == "AuthenticationAdmin" || string.IsNullOrEmpty(username)) return;

            var userInfo = _userService.GetUserInfoDto(username);

            RegisterSession(userInfo.Id, userInfo.Organisation_Id_fk, sessionId);
        }

        public void OnSessionClose(SessionId sessionId, ISessionDetails sessionDetails, CloseReason closeReason)
        {
            Log.Info(string.Format("Session.OnSessionClose: {0} | {1} REASON: {2}", sessionDetails.Summary.Principal, sessionId, closeReason.Name));

            var username = sessionDetails.Summary.Principal;

            if (username == "AuthenticationAdmin" || string.IsNullOrEmpty(username)) return;

            var userInfo = _userService.GetUserInfoDto(username);

            var clientSessionInfo = new ClientSessionInfo
                                    {
                                        UserId = userInfo.Id,
                                        OrganisationId = userInfo.Organisation_Id_fk,
                                        SessionId = sessionId.ToString()
                                    };

            if (SessionsDictionary.ContainsKey(clientSessionInfo))
            {
                SessionsDictionary.Remove(clientSessionInfo);
            }

            Console.WriteLine("Session Closed");
        }

        public ClientSessionInfo RegisterSession(long userId, long organisationId, SessionId sessionId)
        {
            var csi = new ClientSessionInfo
                      {
                          UserId = userId,
                          OrganisationId = organisationId,
                          SessionId = sessionId.ToString()
                      };

            if (SessionsDictionary.ContainsKey(csi))
            {
                SessionsDictionary[csi].LastSeen = _dateTimeProvider.UtcNow;
            }
            else
            {
                SessionsDictionary[csi] = new UserSessionInfo
                                          {
                                              SessionId = sessionId,
                                              LastSeen = _dateTimeProvider.UtcNow
                                          };
            }

            return csi;
        }

        public IDictionary<ClientSessionInfo, UserSessionInfo> SessionsDictionary { get; private set; }

        public IUserService UserService
        {
            get
            {
                return _userService;
            }
        }
    }
}