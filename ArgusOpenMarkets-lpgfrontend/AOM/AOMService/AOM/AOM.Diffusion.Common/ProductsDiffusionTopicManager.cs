﻿using AOMDiffusion.Common.Interfaces;

namespace AOMDiffusion.Common
{

    public class ProductsDiffusionTopicManager : AomDiffusionTopicManager, IProductsDiffusionTopicManager
    {
        public ProductsDiffusionTopicManager()
            : base("AOM/Products")
        {
        }
    }
}