﻿using System;
using SystemLoadTester.Consumers;
using NUnit.Framework;

namespace SystemLoadTester.Tests
{
    [TestFixture]
    public class OrderConsumerTest
    {
        [Test]
        public void StartException()
        {
            var consumer = new OrderConsumer(null, null, null, null);
            Assert.Throws<NullReferenceException>(consumer.Start);
        }
    }
}
