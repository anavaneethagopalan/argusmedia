﻿using System;

namespace AOM.App.Domain.Dates
{
    public interface IDateTimeProvider
    {
        DateTime Now { get;}
        DateTime UtcNow { get; }
        DateTime Today { get; }
    }
}
