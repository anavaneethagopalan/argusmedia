﻿using System;

namespace AOM.App.Domain
{
    [Serializable]
    public class BusinessRuleException : Exception
    {
        public BusinessRuleException(string errorMessage) : base(errorMessage)
        {
        }
    }
}