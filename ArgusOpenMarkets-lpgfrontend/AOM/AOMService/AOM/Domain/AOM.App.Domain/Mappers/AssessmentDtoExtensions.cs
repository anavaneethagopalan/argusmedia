﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class AssessmentDtoExtensions
    {
        public static AssessmentDto ToDto(this Assessment entity, AssessmentDto passedDto = null)
        {
            var dto = passedDto ?? new AssessmentDto();
            dto.Id = entity.Id;
            dto.ProductId = entity.ProductId;
            dto.BusinessDate = entity.BusinessDate;
            dto.DateCreated = entity.DateCreated;
            dto.EnteredByUserId = entity.EnteredByUserId;
            dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
            dto.PriceHigh = entity.PriceHigh;
            dto.PriceLow = entity.PriceLow;
            dto.AssessmentStatus = DtoMappingAttribute.GetValueFromEnum(entity.AssessmentStatus);
            
            return dto;
        }

        public static Assessment ToEntity(this AssessmentDto passedDto)
        {
            var entity = new Assessment
            {
                Id = passedDto.Id,
                ProductId = passedDto.ProductId,
                BusinessDate = passedDto.BusinessDate,
                DateCreated = passedDto.DateCreated,
                EnteredByUserId = passedDto.EnteredByUserId,
                LastUpdated = passedDto.LastUpdated,
                LastUpdatedUserId = passedDto.LastUpdatedUserId,
                PriceHigh = passedDto.PriceHigh,
                PriceLow = passedDto.PriceLow,
                AssessmentStatus = DtoMappingAttribute.FindEnumWithValue<AssessmentStatus>(passedDto.AssessmentStatus)
            };

            return entity;
        }

        public static Assessment CopyAssessment(this Assessment passed)
        {
            return new Assessment
            {
                LastUpdated = passed.LastUpdated,
                LastUpdatedUserId = passed.LastUpdatedUserId,
                PriceHigh = passed.PriceHigh,
                PriceLow = passed.PriceLow,
                ProductId = passed.ProductId,
                Product = passed.Product,
                BusinessDate = passed.BusinessDate,
                DateCreated = passed.DateCreated,
                EnteredByUserId = passed.EnteredByUserId,
                AssessmentStatus = passed.AssessmentStatus
            };
        }
    }
}
