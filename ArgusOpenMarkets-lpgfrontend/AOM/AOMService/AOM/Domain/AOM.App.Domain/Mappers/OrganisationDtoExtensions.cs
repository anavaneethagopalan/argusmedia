﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;

namespace AOM.App.Domain.Mappers
{
    public static class OrganisationDtoExtensions
    {
        public static Organisation ToEntity(this OrganisationDto dto, Organisation organisation = null)
        {
            var entity = organisation ?? new Organisation();

            entity.Id = dto.Id;
            entity.Address = dto.Address;
            entity.Email = dto.Email;
            entity.DateCreated = dto.DateCreated;
            entity.Description = dto.Description;
            entity.IsDeleted = dto.IsDeleted;
            entity.LegalName = dto.LegalName;
            entity.Name = dto.Name;
            entity.OrganisationType = DtoMappingAttribute.FindEnumWithValue<OrganisationType>(dto.OrganisationType);
            entity.ShortCode = dto.ShortCode;
       
            return entity;
        }

        public static OrganisationDto ToDto(this Organisation entity, OrganisationDto passedDto = null)
        {
            var dto = passedDto ?? new OrganisationDto();

            dto.Id = entity.Id;
            dto.Address = entity.Address;
            dto.Email = entity.Email;
            dto.DateCreated = entity.DateCreated;
            dto.Description = entity.Description;
            dto.IsDeleted = entity.IsDeleted;
            dto.LegalName = entity.LegalName;
            dto.Name = entity.Name;
            dto.OrganisationType = DtoMappingAttribute.GetValueFromEnum(entity.OrganisationType);
            dto.ShortCode = entity.ShortCode;

            return dto;
        }

        public static bool IsBroker(this OrganisationDto dto)
        {            
            return dto.OrganisationType.Equals("B");
        }
    }
}
