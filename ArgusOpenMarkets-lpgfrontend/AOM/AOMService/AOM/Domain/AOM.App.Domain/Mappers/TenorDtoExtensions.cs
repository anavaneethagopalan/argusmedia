﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class TenorDtoExtensions
    {
        public static TenorCode ToEntity(this TenorCodeDto dto, TenorCode tenor = null)
        {
            var entity = tenor ?? new TenorCode();

            entity.Code = dto.Code;
            entity.Name = dto.Name;
            entity.DateCreated = dto.DateCreated;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            return entity;
        }

        public static TenorCodeDto ToDto(this TenorCode entity, TenorCodeDto passedDto = null)
        {
            var dto = passedDto ?? new TenorCodeDto();

            //dto.Id = entity.Id;
            dto.Code = entity.Code;
            dto.Name = entity.Name;
            //dto.DateCreated = entity.DateCreated;
            //dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
            return dto;
        }
    }
}