﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class EmailDtoExtensions
    {
        public static Email ToEntity(this EmailDto dto, Email email = null)
        {
            var entity = email ?? new Email();

            entity.Body = dto.Body;
            entity.DateCreated = dto.DateCreated;
            entity.DateSent = dto.DateSent;
            entity.Id = dto.Id;
            entity.Recipient = dto.Recipient;
            entity.Status = DtoMappingAttribute.FindEnumWithValue<EmailStatus>(dto.Status);
            entity.Subject = dto.Subject;

            return entity;
        }

        public static EmailDto ToDto(this Email entity, EmailDto passedDto = null)
        {
            var dto = passedDto ?? new EmailDto();

            dto.Body = entity.Body;
            dto.DateCreated = entity.DateCreated;
            dto.DateSent = entity.DateSent;
            dto.Id = entity.Id;
            dto.Recipient = entity.Recipient;
            dto.Status = DtoMappingAttribute.GetValueFromEnum(entity.Status);
            dto.Subject = entity.Subject;

            return dto;
        }
    }
}
