﻿namespace AOM.App.Domain.Mappers
{
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;

    using Newtonsoft.Json;

    public static class ScheduledTaskExtensions
    {
        public static ScheduledTask ToEntity(this ScheduledTaskDto passedDto, ScheduledTask task = null)
        {
            task = task ?? new ScheduledTask();
            task.Id = passedDto.Id;
            task.Interval = DtoMappingAttribute.FindEnumWithValue<TaskRepeatInterval>(passedDto.Interval);
            task.NextRun = passedDto.NextRun;
            task.LastRun = passedDto.LastRun;
            task.TaskName = passedDto.TaskName;
            task.TaskType = passedDto.TaskType;
            task.SkipIfMissed = passedDto.SkipIfMissed;
            task.IgnoreTask = passedDto.IgnoreTask;
            task.Arguments = JsonConvert.DeserializeObject<Dictionary<string, string>>(passedDto.Arguments);

            return task;
        }

        public static ScheduledTaskDto ToDto(this ScheduledTask passedEntity, ScheduledTaskDto task = null)
        {
            task = task ?? new ScheduledTaskDto();
            task.Id = passedEntity.Id;
            task.Interval = DtoMappingAttribute.GetValueFromEnum(passedEntity.Interval);
            task.NextRun = passedEntity.NextRun;
            task.DateCreated = passedEntity.DateCreated;
            task.LastRun = passedEntity.LastRun;
            task.TaskName = passedEntity.TaskName;
            task.TaskType = passedEntity.TaskType;
            task.SkipIfMissed = passedEntity.SkipIfMissed;
            task.IgnoreTask = passedEntity.IgnoreTask;
            task.Arguments = JsonConvert.SerializeObject(passedEntity.Arguments);

            return task;
        }

    }
}