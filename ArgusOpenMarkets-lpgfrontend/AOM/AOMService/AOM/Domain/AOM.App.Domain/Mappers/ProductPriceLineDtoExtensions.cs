﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class ProductPriceLineDtoExtensions
    {
        public static ProductPriceLine ToEntity(this ProductPriceLineDto dto, ProductPriceLine passedEntity = null)
        {
            var entity = passedEntity ?? new ProductPriceLine();

            entity.Id = dto.Id;
            entity.ProductTenorId = dto.ProductTenorId;
            entity.BasisProductId = dto.BasisProductId;
            entity.SequenceNo = dto.SequenceNo;
            entity.Description = dto.Description;
            entity.PercentageOptionList = dto.PercentageOptionList;
            entity.DefaultDisplay = dto.DefaultDisplay;

            entity.PriceBases = dto.ProductPriceLineBasesPeriods.Where(pplbp => pplbp.ProductPriceLineId == dto.Id).Select(pb => pb.PriceBasisDto.ToEntity(productPriceLineId: dto.Id)).ToList();

            return entity;
        }

        public static ProductPriceLineDto ToDto(this ProductPriceLine entity, ProductPriceLineDto passedDto = null)
        {
            var dto = passedDto ?? new ProductPriceLineDto();

            dto.Id = entity.Id;
            dto.ProductTenorId = entity.ProductTenorId;
            dto.BasisProductId = entity.BasisProductId;
            dto.SequenceNo = entity.SequenceNo;
            dto.Description = entity.Description;
            dto.PercentageOptionList = entity.PercentageOptionList;
            dto.DefaultDisplay = entity.DefaultDisplay;

            return dto;
        }
    }
}
