﻿using System;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class OrderPriceLineDtoExtensions
    {
        public static OrderPriceLine ToEntity(this OrderPriceLineDto dto, OrderPriceLine passedEntity = null) //, Type requestingParentType = null
        {
            var entity = passedEntity ?? new OrderPriceLine();

            entity.Id = dto.Id;
            entity.OrderId = dto.OrderId;
            entity.PriceLineId = dto.PriceLineId;
            entity.OrderStatusCode = dto.OrderStatusCode;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;

            //if (typeof(OrderDto).IsAssignableFrom(requestingParentType) && dto.OrderDto != null)
            //{
            //    entity.Order = dto.OrderDto.ToEntity(null, null, null, dto.GetType());
            //}

            if (dto.PriceLineDto != null) //typeof(PriceLineDto).IsAssignableFrom(requestingParentType) && 
            {
                entity.PriceLine = dto.PriceLineDto.ToEntity();
            }

            return entity;
        }

        ///// <param name="requestingParentType">To be used by other ToDto() methods, to avoid infinite loop. ToDto() will not be called on child entities of provided type.</param>
        /// <summary>
        /// Copies deserialised entity into a DTO
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="passedDto"></param>
        /// <returns></returns>
        public static OrderPriceLineDto ToDto(this OrderPriceLine entity, OrderPriceLineDto passedDto = null) 
        {
            var dto = passedDto ?? new OrderPriceLineDto();
            
            dto.Id = entity.Id;
            dto.OrderId = entity.OrderId;
            dto.PriceLineId = entity.PriceLineId;

            SetEditableFields(entity, dto);

            //if (requestingParentType != typeof(Order) && entity.Order != null)
            //{
            //    dto.OrderDto = entity.Order.ToDto(entity.GetType());
            //}

            if (entity.PriceLine != null) //requestingParentType != typeof(PriceLine) && 
            {
                dto.PriceLineDto = entity.PriceLine.ToDto();
            }
            
            return dto;
        }

        /// <summary>
        /// Updates DTO with new values from deserialised entity
        /// </summary>
        /// <param name="repositoryEntity"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static OrderPriceLineDto UpdateDto(this OrderPriceLineDto repositoryEntity, OrderPriceLine entity)
        {
            var dto = repositoryEntity;

            SetEditableFields(entity, dto);

            if (dto.PriceLineId == entity.PriceLineId)
            {
                dto.PriceLineDto = dto.PriceLineDto.UpdateDto(entity.PriceLine);
            }
            else
            {
                dto.PriceLineDto = entity.PriceLine.ToDto();
            }

            return dto;
        }

        private static void SetEditableFields(OrderPriceLine entity, OrderPriceLineDto dto)
        {
            dto.OrderStatusCode = entity.OrderStatusCode;

            dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
        }
    }
}

