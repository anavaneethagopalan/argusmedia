namespace AOM.App.Domain.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class MarketTickerItemExtensions
    {
        public static MarketTickerItem ToEntity(
            this MarketTickerItemDto repositoryMarketTickDto,
            IAomModel aomDb,
            MarketTickerItem marketTickeritem = null)
        {
            var ti = marketTickeritem ?? new MarketTickerItem();

            ti.Id = repositoryMarketTickDto.Id;
            ti.LastUpdated = repositoryMarketTickDto.LastUpdated;
            ti.LastUpdatedUserId = repositoryMarketTickDto.LastUpdatedUserId;
            ti.Message = repositoryMarketTickDto.Message;
            ti.CreatedDate = repositoryMarketTickDto.DateCreated;
            ti.ProductIds = NullableToList(repositoryMarketTickDto.ProductId);
            ti.CreatedUserId = repositoryMarketTickDto.EnteredByUserId;
            ti.DealId = repositoryMarketTickDto.DealId;
            ti.OrderId = repositoryMarketTickDto.OrderId;
            ti.ExternalDealId = repositoryMarketTickDto.ExternalDealId;
            ti.MarketInfoId = repositoryMarketTickDto.MarketInfoId;
            ti.MarketTickerItemStatus =
                DtoMappingAttribute.FindEnumWithValue<MarketTickerItemStatus>(
                    repositoryMarketTickDto.MarketTickerItemStatus);
            ti.MarketTickerItemType =
                DtoMappingAttribute.FindEnumWithValue<MarketTickerItemType>(
                    repositoryMarketTickDto.MarketTickerItemType);
            ti.Name = repositoryMarketTickDto.Name;
            ti.Notes = repositoryMarketTickDto.Notes;
            ti.CoBrokering = repositoryMarketTickDto.CoBrokering;
            ti.OwnerOrganisations = new List<long> {repositoryMarketTickDto.OwnerOrganisationId1 ?? 0};
            if (repositoryMarketTickDto.OwnerOrganisationId2 != null)
            {
                ti.OwnerOrganisations.Add(repositoryMarketTickDto.OwnerOrganisationId2 ?? 0);
            }
            if (repositoryMarketTickDto.OwnerOrganisationId3 != null)
            {
                ti.OwnerOrganisations.Add(repositoryMarketTickDto.OwnerOrganisationId3 ?? 0);
            }
            if (repositoryMarketTickDto.OwnerOrganisationId4 != null)
            {
                ti.OwnerOrganisations.Add(repositoryMarketTickDto.OwnerOrganisationId4 ?? 0);
            }

            if (String.IsNullOrEmpty(repositoryMarketTickDto.BrokerRestriction))
            {
                ti.BrokerRestriction = null;
            }
            else
            {
                ti.BrokerRestriction =
                    DtoMappingAttribute.FindEnumWithValue<BrokerRestriction>(
                        repositoryMarketTickDto.BrokerRestriction);
            }

            ti.MarketInfoType = null;
            if (ti.MarketInfoId.HasValue)
            {
                var marketInfo = aomDb.MarketInfoItems.SingleOrDefault(mi => mi.Id == ti.MarketInfoId.Value);
                if (marketInfo != null && !string.IsNullOrEmpty(marketInfo.MarketInfoType))
                {
                    ti.MarketInfoType =
                        DtoMappingAttribute.FindEnumWithValue<MarketInfoType>(marketInfo.MarketInfoType);
                }
            }    

            return ti;
        }

        private static List<T> NullableToList<T>(T? val) where T : struct
        {
            return val.HasValue ? new List<T> {val.Value} : new List<T>();
        }
    }
}