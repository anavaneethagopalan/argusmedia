﻿using System;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class PeriodDtoExtensions
    {
        public static Period ToEntity(this PeriodDto dto, Period priceBasis = null)
        {
            var entity = priceBasis ?? new Period();

            entity.Id = dto.Id;
            entity.TenorCode = dto.TenorCode;
            entity.PeriodActionCode = dto.PeriodActionCode;
            entity.Code = dto.Code;
            entity.Name = dto.Name;
            entity.RollDate = dto.RollDate;
            //entity.RollDateRule = dto.RollDateRule;
            entity.MinimumDeliveryRange = dto.MinimumDeliveryRange;
            entity.RollingPeriodFrom = dto.RollingPeriodFrom;
            entity.RollingPeriodTo = dto.RollingPeriodTo;
            entity.FixedDateFrom = dto.FixedDateFrom;
            entity.FixedDateTo = dto.FixedDateTo;
            entity.DateCreated = dto.DateCreated;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;

            entity.PeriodDataDisplay = PeriodCalculator.GetPeriodDisplayList(entity, DateTime.Today);

            return entity;
        }

        public static PeriodDto ToDto(this Period entity, PeriodDto passedDto = null)
        {
            var dto = passedDto ?? new PeriodDto();

            dto.Id = entity.Id;
            dto.TenorCode = entity.TenorCode;
            dto.PeriodActionCode = entity.PeriodActionCode;
            dto.Code = entity.Code;
            dto.Name = entity.Name;
            dto.RollDate = entity.RollDate;
            //dto.RollDateRule = entity.RollDateRule;
            dto.MinimumDeliveryRange = entity.MinimumDeliveryRange;
            dto.RollingPeriodFrom = entity.RollingPeriodFrom;
            dto.RollingPeriodTo = entity.RollingPeriodTo;
            dto.FixedDateFrom = entity.FixedDateFrom;
            dto.FixedDateTo = entity.FixedDateTo;
            //dto.DateCreated = entity.DateCreated;
            //dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;

            return dto;
        }
    }
}
