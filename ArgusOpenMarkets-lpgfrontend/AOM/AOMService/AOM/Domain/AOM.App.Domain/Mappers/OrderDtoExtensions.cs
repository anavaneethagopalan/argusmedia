﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AOM.App.Domain.Mappers
{
    public static class OrderDtoExtensions
    {
        public static Order ToEntity(this OrderDto repositoryOrderDto, IOrganisationService organisationService, IUserService userService, 
            Order order = null) //, Type requestingParentType = null
        { 
            var o = order ?? new Order();

            var principalOrganization = repositoryOrderDto.OrganisationId <= 0 
                ? //if we dont have broker org
                (repositoryOrderDto.UserId == null ? null : userService.GetUsersOrganisation((long) repositoryOrderDto.UserId))
                //we have broker org
                : organisationService.GetOrganisationById(repositoryOrderDto.OrganisationId);

            var brokerOrganization = repositoryOrderDto.BrokerOrganisationId == null
                ? //if we dont have broker org
                (repositoryOrderDto.BrokerId == null ? null : userService.GetUsersOrganisation((long) repositoryOrderDto.BrokerId))
                //we have broker org
                : organisationService.GetOrganisationById((long) repositoryOrderDto.BrokerOrganisationId);

            o.Id = repositoryOrderDto.Id;
            o.Price = repositoryOrderDto.Price;
            o.Quantity = repositoryOrderDto.Quantity;
            o.QuantityText = repositoryOrderDto.QuantityText;

            o.OrderType = DtoMappingAttribute.FindEnumWithValue<OrderType>(repositoryOrderDto.BidOrAsk);
            o.LastUpdated = repositoryOrderDto.LastUpdated;
            o.LastUpdatedUserId = repositoryOrderDto.LastUpdatedUserId;
            o.EnteredByUserId = repositoryOrderDto.EnteredByUserId;
            o.PrincipalUserId = repositoryOrderDto.UserId;
            o.DeliveryPeriod = repositoryOrderDto.DeliveryPeriod;
            o.DeliveryStartDate = repositoryOrderDto.DeliveryStartDate;
            o.DeliveryEndDate = repositoryOrderDto.DeliveryEndDate;

            o.OrderStatus = DtoMappingAttribute.FindEnumWithValue<OrderStatus>(repositoryOrderDto.OrderStatusCode);
            if (null != repositoryOrderDto.LastOrderStatusCode)
            {
                o.LastOrderStatus = DtoMappingAttribute.FindEnumWithValue<OrderStatus>(repositoryOrderDto.LastOrderStatusCode);
            }
            o.Notes = repositoryOrderDto.ProductTenorDto.ProductDto.OrderShowNotes ? repositoryOrderDto.Message : ""; // remove notes based on a bool in product table
            o.ExecutionMode = DtoMappingAttribute.FindEnumWithValue<ExecutionMode>(repositoryOrderDto.InternalOrExternal);
            o.ProductId = repositoryOrderDto.ProductTenorDto.ProductId;
            o.ProductName = repositoryOrderDto.ProductTenorDto.ProductDto.Name;
            o.IsVirtual = repositoryOrderDto.IsVirtual != 0;
            o.DateCreated = repositoryOrderDto.DateCreated;
            o.Notes = repositoryOrderDto.Message;
            o.BrokerId = repositoryOrderDto.BrokerId;

            if (brokerOrganization != null)
            {
                o.BrokerOrganisationId = brokerOrganization.Id;
                o.BrokerOrganisationName = brokerOrganization.Name;
                o.BrokerShortCode = brokerOrganization.ShortCode;
            }

            if (principalOrganization != null)
            {
                o.PrincipalOrganisationName = principalOrganization.Name;
                o.PrincipalOrganisationShortCode = principalOrganization.ShortCode;
            }

            o.PrincipalOrganisationId = repositoryOrderDto.OrganisationId;

            if (repositoryOrderDto.OrderPriceLines != null && repositoryOrderDto.OrderPriceLines.Count > 0) //typeof(OrderPriceLineDto) != requestingParentType && 
            {
                o.OrderPriceLines = repositoryOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList();
            }

            if (repositoryOrderDto.ProductTenorDto != null) //typeof(ProductTenorDto) != requestingParentType && 
            {
                //WILL TENOR COME THROUGH AS WELL...THE SUSPENSE!
                o.ProductTenor = repositoryOrderDto.ProductTenorDto.ToEntity(null, repositoryOrderDto.GetType());
            }

            //o.Tenor = new Tenor
            //{
                //DeliveryStartDate = repositoryOrderDto.ProductTenorDto.DeliveryDateStart,
                //DeliveryEndDate = repositoryOrderDto.ProductTenorDto.DeliveryDateEnd,
               // Id = repositoryOrderDto.ProductTenorDto.Id,
                //PeriodCode = repositoryOrderDto.ProductTenorDto.TenorDto.PeriodCode
                //DeliveryLocation = repositoryOrderDto.ProductTenorDto.ProductDto.DeliveryLocationDto == null ? null :
                //    new DeliveryLocation
                //    {
                //        Name = repositoryOrderDto.ProductTenorDto.ProductDto.DeliveryLocationDto.Name,
                //        Id = repositoryOrderDto.ProductTenorDto.ProductDto.DeliveryLocationDto.Id,
                //        DateCreated = repositoryOrderDto.ProductTenorDto.ProductDto.DeliveryLocationDto.DateCreated
                //    },
                //RollDate = repositoryOrderDto.ProductTenorDto.RollDate
            //};

            o.BrokerRestriction = DtoMappingAttribute.FindEnumWithValue<BrokerRestriction>(repositoryOrderDto.BrokerRestriction);
            o.CoBrokering = repositoryOrderDto.CoBrokering;
            o.MetaData = repositoryOrderDto.MetaData.ToEntity();

            return o;
        }
        
        ///// <param name="requestingParentType">To be used by other ToDto() methods, to avoid infinite loop. ToDto() will not be called on child entities of provided type.</param>
        /// <summary>
        /// Copies deserialised entity into a DTO
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static OrderDto ToDto(this Order entity) //, Type requestingParentType = null
        {
            var dto = new OrderDto();

            SetEditableFields(entity, dto);
            
            dto.BidOrAsk = DtoMappingAttribute.GetValueFromEnum(entity.OrderType);
            dto.EnteredByUserId = entity.EnteredByUserId;
            dto.UserId = entity.PrincipalUserId;
            dto.BrokerId = entity.BrokerId;
            dto.OrganisationId = entity.PrincipalOrganisationId;
            dto.LastOrderStatusCode = dto.OrderStatusCode;
            dto.InternalOrExternal = DtoMappingAttribute.GetValueFromEnum(entity.ExecutionMode);
            dto.IsVirtual = (sbyte) (entity.IsVirtual ? -1 : 0);
            //dto.DateCreated = order.DateCreated; //computed column
            dto.BrokerId = entity.BrokerId;
            dto.BrokerOrganisationId = entity.BrokerOrganisationId;
            dto.BrokerRestriction = DtoMappingAttribute.GetValueFromEnum(entity.BrokerRestriction);

            if (entity.OrderPriceLines != null && entity.OrderPriceLines.Count > 0) //requestingParentType != typeof(OrderPriceLine) && 
            {
                dto.OrderPriceLines = entity.OrderPriceLines.Select(pl => pl.ToDto()).ToList();
            }

            if (entity.ProductTenor != null)
            {
                dto.ProductTenorDto = entity.ProductTenor.ToDto();
                dto.ProductTenorId = entity.ProductTenor.Id;
            }

            return dto;
        }

        /// <summary>
        /// Updates DTO with new values from deserialised entity
        /// </summary>
        /// <param name="repositoryEntity"></param>
        /// <param name="entity"></param>
        /// <param name="requestorOrgId"></param>
        /// <returns></returns>
        public static OrderDto UpdateDto(this OrderDto repositoryEntity, Order entity, long requestorOrgId) //, Type requestorParentType = null
        {
            var dto = repositoryEntity;
            dto.LastOrderStatusCode = dto.OrderStatusCode;

            SetEditableFields(entity, dto);

            if (requestorOrgId == entity.PrincipalOrganisationId)
            {
                dto.BrokerOrganisationId = entity.BrokerOrganisationId;
                dto.BrokerRestriction = DtoMappingAttribute.GetValueFromEnum(entity.BrokerRestriction);
            }

            if (entity.OrderPriceLines != null && entity.OrderPriceLines.Count > 0)  //requestorParentType != typeof(OrderPriceLineDto) && 
            {
                var newOrderPriceLines = new List<OrderPriceLineDto>();
                foreach (var orderPriceLine in entity.OrderPriceLines)
                {
                    var orderPriceLineDto = dto.OrderPriceLines.SingleOrDefault(opl => opl.Id == orderPriceLine.Id);
                    if (orderPriceLineDto != null)
                    {
                        orderPriceLineDto.UpdateDto(orderPriceLine);
                    }
                    else
                    {
                        orderPriceLineDto = orderPriceLine.ToDto();
                    }
                    newOrderPriceLines.Add(orderPriceLineDto);
                }

                //mark no longer existing order price lines as killed
                dto.OrderPriceLines.Except(newOrderPriceLines).ToList().ForEach(opl => opl.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Killed));

                dto.OrderPriceLines = newOrderPriceLines;
            }

            return dto;
        }

        private static void SetEditableFields(Order entity, OrderDto dto)
        {
            dto.Id = entity.Id;
            dto.Price = entity.Price;
            dto.Quantity = entity.Quantity;
            dto.QuantityText = entity.QuantityText;
            dto.ProductTenorId = entity.ProductTenor.Id;
            dto.DeliveryPeriod = entity.DeliveryPeriod;
            dto.DeliveryStartDate = entity.DeliveryStartDate;
            dto.DeliveryEndDate = entity.DeliveryEndDate;
            dto.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(entity.OrderStatus);
            dto.Message = entity.Notes;
            dto.CoBrokering = entity.CoBrokering;
            dto.MetaData = entity.MetaData.ToDto();
            dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
        }
    }
}