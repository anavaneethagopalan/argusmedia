using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class OrderMetaDataExtensions
    {
        public static OrderMetaData[] ToEntity(this OrderMetaDataDto[] dto)
        {
            if (dto == null) return null;
            return dto.Select(m => m!=null ? m.ToEntity() : null).ToArray();
        }

        public static OrderMetaData ToEntity(this OrderMetaDataDto dto, OrderMetaData omd = null)
        {
            var o = omd ?? new OrderMetaData();

            o.ProductMetaDataId = dto.ProductMetaDataId;
            o.DisplayName = dto.DisplayName;
            o.DisplayValue = dto.DisplayValue;

            if (dto is OrderMetaDataIntegerEnumDto)
            {
                o.MetaDataListItemId = (dto as OrderMetaDataIntegerEnumDto).Id;
                o.ItemValue = (dto as OrderMetaDataIntegerEnumDto).ItemValue;
                o.ItemType = MetaDataTypes.IntegerEnum;
            }
            else
            {
                o.ItemType = MetaDataTypes.String;
            }

            return o;
        }

        public static OrderMetaDataDto[] ToDto(this OrderMetaData[] dto)
        {
            if (dto == null) return null;
            return dto.Select(m => m!=null ? m.ToDto() : null).ToArray();
        }

        public static OrderMetaDataDto ToDto(this OrderMetaData omd, OrderMetaDataDto dto = null)
        {
            OrderMetaDataDto orderMetaDataDto = null;

            switch (omd.ItemType)
            {
                case MetaDataTypes.IntegerEnum:
                    OrderMetaDataIntegerEnumDto dtoEnum = new OrderMetaDataIntegerEnumDto();
                    dtoEnum.Id = omd.MetaDataListItemId.Value;
                    dtoEnum.ItemValue = omd.ItemValue.Value;
                    orderMetaDataDto = dtoEnum;
                    break;
                    
                case MetaDataTypes.String:
                    OrderMetaDataStringDto dtoString = new OrderMetaDataStringDto();
                    orderMetaDataDto = dtoString;
                    break;

                default:
                    OrderMetaDataStringDto dtoDefault = new OrderMetaDataStringDto();
                    orderMetaDataDto = dtoDefault;
                    break;
            }

            orderMetaDataDto.ProductMetaDataId = omd.ProductMetaDataId;
            orderMetaDataDto.DisplayName = omd.DisplayName;
            orderMetaDataDto.DisplayValue = omd.DisplayValue;
            return orderMetaDataDto;
        }
    }
}