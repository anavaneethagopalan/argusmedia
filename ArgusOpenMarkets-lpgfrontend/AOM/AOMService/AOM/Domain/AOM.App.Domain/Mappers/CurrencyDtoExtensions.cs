﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class CurrencyDtoExtensions
    {
        public static Currency ToEntity(this CurrencyDto dto, Currency passedEntity = null)
        {
            var entity = passedEntity ?? new Currency();

            entity.Code = dto.Code;
            entity.DateCreated = dto.DateCreated;
            entity.DisplayName = dto.DisplayName;
            entity.Name = dto.Name;

            return entity;
        }

        static public CurrencyDto ToDto(this Currency entity, CurrencyDto passedDto = null)
        {
            var dto = passedDto ?? new CurrencyDto();

            dto.Code = entity.Code;
            dto.DateCreated = entity.DateCreated;
            dto.DisplayName = entity.DisplayName;
            dto.Name = entity.Name;

            return dto;
        }
    }
}