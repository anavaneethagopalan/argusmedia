using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class ExternalDealDtoExtensions
    {
        public static ExternalDeal ToEntity(this ExternalDealDto dto, ICrmAdministrationService userAdministrationService, ExternalDeal externalDeal = null)
        {
            var entity = externalDeal ?? new ExternalDeal();

            entity.Id = dto.Id;
            //entity.DateCreated = dto.DateCreated;
            entity.BrokerId = dto.BrokerId;
            entity.BrokerName = dto.BrokerName;
            entity.BuyerId = dto.BuyerId;
            entity.BuyerName = dto.BuyerName;
            entity.ContractInput = dto.ContractInput;
            entity.DealStatus = DtoMappingAttribute.FindEnumWithValue<DealStatus>(dto.DealStatus);
            entity.DeliveryEndDate = dto.DeliveryEndDate;
            entity.DeliveryLocation = dto.DeliveryLocation;
            entity.DeliveryStartDate = dto.DeliveryStartDate;
            entity.FreeFormDealMessage = dto.FreeFormDealMessage;
            entity.Price = dto.Price;
            entity.ProductId = dto.ProductId;
            entity.Quantity = dto.Quantity;
            entity.QuantityText = dto.QuantityText;
            entity.ReporterId = dto.ReporterId;
            entity.SellerId = dto.SellerId;
            entity.SellerName = dto.SellerName;
            entity.VoidReason = dto.VoidReason;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            entity.Notes = dto.Notes;
            entity.UseCustomFreeFormNotes = dto.UseCustomFreeFormNotes;
            entity.OptionalPriceDetail = dto.OptionalPriceDetail;
            entity.PreviousDealStatus = DtoMappingAttribute.FindEnumWithValue<DealStatus>(dto.PreviousDealStatus);
            entity.MetaData = dto.MetaData.ToEntity();
            entity.PriceLineId = dto.PriceLineId;

            if (dto.PriceLineDto != null)
            {
                entity.PriceLine = dto.PriceLineDto.ToEntity();
            }

            return entity;
        }

        public static ExternalDealDto ToDto(this ExternalDeal entity, ExternalDealDto passedDto = null)
        {
            var dto = passedDto ?? new ExternalDealDto();

            dto.Id = entity.Id;
            //dto.DateCreated = entity.DateCreated;
            dto.BrokerId = entity.BrokerId;
            dto.BrokerName = entity.BrokerName;
            dto.BuyerId = entity.BuyerId;
            dto.BuyerName = entity.BuyerName;
            dto.ContractInput = entity.ContractInput;
            dto.DealStatus = ToDtoDealStatus(entity.DealStatus);
            dto.DeliveryEndDate = entity.DeliveryEndDate;
            dto.DeliveryLocation = entity.DeliveryLocation;
            dto.DeliveryStartDate = entity.DeliveryStartDate;
            dto.FreeFormDealMessage = entity.FreeFormDealMessage;
            dto.Price = entity.Price;
            dto.ProductId = entity.ProductId;
            dto.Quantity = entity.Quantity;
            dto.QuantityText = entity.QuantityText;
            dto.ReporterId = entity.ReporterId;
            dto.SellerId = entity.SellerId;
            dto.SellerName = entity.SellerName;
            dto.VoidReason = entity.VoidReason;
            dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
            dto.Notes = entity.Notes;
            dto.UseCustomFreeFormNotes = entity.UseCustomFreeFormNotes;
            dto.OptionalPriceDetail = entity.OptionalPriceDetail;
            dto.PreviousDealStatus = ToDtoDealStatus(entity.PreviousDealStatus);
            dto.MetaData = entity.MetaData.ToDto();
            dto.PriceLineId = entity.PriceLineId;

            return dto;
        }

        private static string ToDtoDealStatus(DealStatus entityDealStatus)
        {
            return entityDealStatus == 0 ? "P" : DtoMappingAttribute.GetValueFromEnum(entityDealStatus);
        }
    }
}