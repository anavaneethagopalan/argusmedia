using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class ProductTenorDtoExtensions
    {
        public static ProductTenor ToEntity(this ProductTenorDto dto, ProductTenor tenor = null, Type requestingParentType = null)
        {
            var entity = tenor ?? new ProductTenor();

            entity.Id = dto.Id;
            entity.Name = dto.Name;
            entity.DateCreated = dto.DateCreated;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            entity.DisplayName = dto.DisplayName;
            //entity.DeliveryDateStart = dto.DeliveryDateStart;
            //entity.DeliveryDateEnd = dto.DeliveryDateEnd;
            //entity.RollDate = dto.RollDate;
            //entity.RollDateRule = dto.RollDateRule;
            //entity.MinimumDeliveryRange = dto.MinimumDeliveryRange;
            entity.ProductId = dto.ProductId;
            //entity.TenorId = dto.TenorId;

            // If Null Values for either DefaultStartDate or DefaultEndDate - should default to using the DeliveryDate Start and End.
            //entity.DefaultStartDate = string.IsNullOrEmpty(dto.DefaultStartDate) ? entity.DeliveryDateStart : dto.DefaultStartDate;
            //entity.DefaultEndDate = string.IsNullOrEmpty(dto.DefaultEndDate) ? entity.DeliveryDateEnd : dto.DefaultEndDate;
            
            return entity;
        }

        public static ProductTenorDto ToDto(this ProductTenor entity, ProductTenorDto passedDto = null, Type requestingParentType = null)
        {
            var dto = passedDto ?? new ProductTenorDto();

            dto.Id = entity.Id;
            dto.Name = entity.Name;
            //dto.DateCreated = entity.DateCreated;
            //dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
            dto.DisplayName = entity.DisplayName;
            //dto.DeliveryDateStart = entity.DeliveryDateStart;
            //dto.DeliveryDateEnd = entity.DeliveryDateEnd;
            //dto.RollDate = entity.RollDate;
            //dto.RollDateRule = entity.RollDateRule;
            //dto.MinimumDeliveryRange = entity.MinimumDeliveryRange;
            dto.ProductId = entity.ProductId;
            //dto.TenorId = entity.TenorId;

            //dto.DefaultStartDate = string.IsNullOrEmpty(entity.DefaultStartDate) ? entity.DeliveryDateStart : entity.DefaultStartDate;
            //dto.DefaultEndDate = string.IsNullOrEmpty(entity.DefaultEndDate) ? entity.DeliveryDateEnd : entity.DefaultEndDate;

            if (entity.Product != null)
            {
                dto.ProductDto = entity.Product.ToDto();
            }
            
            return dto;
        }
    }
}