namespace AOM.App.Domain.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.Repository.MySql.Crm;

    public static class UserDtoExtensions
    {
        public static UserInfoDto ToDto(this IUser entity, UserInfoDto passedDto = null)
        {
            var dto = passedDto ?? new UserInfoDto();

            dto.Id = entity.Id;
            dto.Email = entity.Email;
            dto.IsActive = entity.IsActive;
            dto.IsBlocked = entity.IsBlocked;
            dto.IsDeleted = entity.IsDeleted;
            dto.Name = entity.Name;
            dto.Organisation_Id_fk = entity.OrganisationId;
            dto.Telephone = entity.Telephone;
            dto.Title = entity.Title;
            dto.Username = entity.Username;
            dto.ArgusCrmUsername = entity.ArgusCrmUsername;

            return dto;
        }

        public static User ToEntity(this UserInfoDto dto, User passedEntity = null)
        {
            var entity = passedEntity ?? new User();

            entity.Id = dto.Id;
            entity.Email = dto.Email;
            entity.IsActive = dto.IsActive;
            entity.IsBlocked = dto.IsBlocked;
            entity.IsDeleted = dto.IsDeleted;
            entity.Name = dto.Name;
            entity.OrganisationId = dto.Organisation_Id_fk;
            entity.Telephone = dto.Telephone;
            entity.Title = dto.Title;
            entity.Username = dto.Username;
            entity.ArgusCrmUsername = dto.ArgusCrmUsername;
            return entity;
        }


        public static AuthenticationHistoryDto ToDto(this AuthenticationHistory entity, AuthenticationHistoryDto passedDto = null)
        {
            var dto = passedDto ?? new AuthenticationHistoryDto();

            dto.Id = entity.Id;
            dto.IpAddress = entity.IpAddress;
            dto.SuccessfulLogin = entity.SuccessfulLogin;
            dto.Username = entity.Username;
            dto.DateCreated = entity.DateCreated;
            dto.LoginFailureReason = entity.LoginFailureReason;
            dto.UserLoginSource_Id_fk = entity.UserLoginSourceId;
            return dto;
        }

        public static AuthenticationHistory ToEntity(this AuthenticationHistoryDto dto, AuthenticationHistory passedEntity = null)
        {
            var entity = passedEntity ?? new AuthenticationHistory();

            entity.Id = dto.Id;
            entity.IpAddress = dto.IpAddress;
            entity.SuccessfulLogin = dto.SuccessfulLogin;
            entity.Username = dto.Username;
            entity.DateCreated = dto.DateCreated;
            entity.LoginFailureReason = dto.LoginFailureReason;
            entity.UserLoginSourceId = dto.UserLoginSource_Id_fk;

            return entity;
        }

        public static DisconnectHistoryDto ToDto(this DisconnectHistory entity, DisconnectHistoryDto passedDto = null)
        {
            var dto = passedDto ?? new DisconnectHistoryDto();

            dto.Id = entity.Id;
            dto.UserId = entity.UserId;
            dto.DisconnectReason = entity.DisconnectReason;
            dto.DateCreated = entity.DateCreated;

            return dto;
        }

        public static DisconnectHistory ToEntity(this DisconnectHistoryDto dto, DisconnectHistory passedEntity = null)
        {
            var entity = passedEntity ?? new DisconnectHistory();

            entity.Id = dto.Id;
            entity.UserId = dto.UserId;
            entity.DisconnectReason = dto.DisconnectReason;
            entity.DateCreated = dto.DateCreated;

            return entity;
        }

        public static UserCredentialsDto ToDto(this UserCredentials entity, UserCredentialsDto passedDto = null)
        {
            var dto = passedDto ?? new UserCredentialsDto();

            dto.Id = entity.Id;
            dto.PasswordHashed = entity.PasswordHashed;
            dto.UserInfo_Id_fk = entity.UserId;
            dto.DateCreated = entity.DateCreated;
            dto.FirstTimeLogin = entity.FirstTimeLogin;
            dto.Expiration = entity.Expiration;
            dto.DefaultExpirationInMonths = entity.DefaultExpirationInMonths;
            dto.CredentialType = DtoMappingAttribute.GetValueFromEnum(entity.CredentialType);
            return dto;
        }
        
        public static UserCredentials ToEntity(this UserCredentialsDto dto, UserCredentials passedEntity = null)
        {
            var entity = passedEntity ?? new UserCredentials();

            entity.Id = dto.Id;
            entity.PasswordHashed = dto.PasswordHashed;
            entity.UserId = dto.UserInfo_Id_fk;
            entity.Expiration = dto.Expiration;
            entity.DateCreated = dto.DateCreated;
            entity.FirstTimeLogin = dto.FirstTimeLogin;
            entity.DefaultExpirationInMonths = dto.DefaultExpirationInMonths;
            entity.CredentialType = DtoMappingAttribute.FindEnumWithValue<CredentialType>(dto.CredentialType);

            if (dto.User != null)
            {
                entity.User = dto.User.ToEntity();
            }

            return entity;
        }

        public static UserLoginAttemptsDto ToDto(this UserLoginAttempts entity, UserLoginAttemptsDto passedDto = null)
        {
            var dto = passedDto ?? new UserLoginAttemptsDto();

            dto.UserId = entity.Id;
            dto.NumFailedLogins = entity.NumFamiledLogins;
            dto.LastLoginAttemptTime = entity.LastLoginAttemptTime;
            dto.UserLoginSource_Id_fk = entity.UserLoginSourceId;

            return dto;
        }

        public static UserLoginAttempts ToEntity(this UserLoginAttemptsDto dto, UserLoginAttempts passedEntity = null)
        {
            var entity = passedEntity ?? new UserLoginAttempts();

            entity.Id = dto.UserId;
            entity.NumFamiledLogins = dto.NumFailedLogins;
            entity.LastLoginAttemptTime = dto.LastLoginAttemptTime;
            entity.UserLoginSourceId = dto.UserLoginSource_Id_fk;

            return entity;
        }

        public static UserLoginSourceDto ToDto(this UserLoginSource entity, UserLoginSourceDto passedDto = null)
        {
            var dto = passedDto ?? new UserLoginSourceDto();

            dto.Id = entity.Id;
            dto.Code = entity.Code;
            dto.Name = entity.Name;
            dto.Organisation_Id_fk = entity.OrganisationId;

            return dto;
        }

        public static UserLoginSource ToEntity(this UserLoginSourceDto dto, UserLoginSource passedEntity = null)
        {
            var entity = passedEntity ?? new UserLoginSource();

            entity.Id = dto.Id;
            entity.Code = dto.Code;
            entity.Name = dto.Name;
            entity.OrganisationId = dto.Organisation_Id_fk;

            return entity;
        }

        public static UserLoginExternalAccountDto ToDto(this UserLoginExternalAccount entity, UserLoginExternalAccountDto passedDto = null)
        {
            var dto = passedDto ?? new UserLoginExternalAccountDto();

            dto.Id = entity.Id;
            dto.UserLoginSource_Id_fk = entity.UserLoginSourceId;
            dto.UserInfo_Id_fk = entity.UserInfoId;
            dto.ExternalUserCode = entity.ExternalUserCode;
            dto.ExternalUserName = entity.ExternalUserName;
            dto.ExternalUserEmail = entity.ExternalUserEmail;
            dto.SSOCredential = entity.SSOCredential;
            dto.DateCreated = entity.DateCreated;
            dto.IsDeleted = entity.IsDeleted;

            return dto;
        }

        public static UserLoginExternalAccount ToEntity(this UserLoginExternalAccountDto dto, UserLoginExternalAccount passedEntity = null)
        {
            var entity = passedEntity ?? new UserLoginExternalAccount();

            entity.Id = dto.Id;
            entity.UserLoginSourceId = dto.UserLoginSource_Id_fk;
            entity.UserInfoId = dto.UserInfo_Id_fk;
            entity.ExternalUserCode = dto.ExternalUserCode;
            entity.ExternalUserName = dto.ExternalUserName;
            entity.ExternalUserEmail = dto.ExternalUserEmail;
            entity.SSOCredential = dto.SSOCredential;
            entity.DateCreated = dto.DateCreated;
            entity.IsDeleted = dto.IsDeleted;

            return entity;
        }
    }
}