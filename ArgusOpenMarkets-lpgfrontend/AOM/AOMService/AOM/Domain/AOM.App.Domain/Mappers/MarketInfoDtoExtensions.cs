﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class MarketInfoDtoExtensions
    {
        public static MarketInfo ToEntity(this MarketInfoDto dto, MarketInfo marketInfo = null)
        {
            var entity = marketInfo ?? new MarketInfo();

            entity.Id = dto.Id;
            entity.Info = dto.Info;
            entity.MarketInfoStatus = DtoMappingAttribute.FindEnumWithValue<MarketInfoStatus>(dto.MarketInfoStatus);
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            entity.OwnerOrganisationId = dto.OwnerOrganisationId;
            entity.MarketInfoType = DtoMappingAttribute.FindEnumWithValue<MarketInfoType>(dto.MarketInfoType);

            return entity;
        }

        public static MarketInfoDto ToDto(this MarketInfo entity, MarketInfoDto passedDto = null)
        {
            var dto = passedDto ?? new MarketInfoDto();

            dto.Id = entity.Id;
            dto.Info = entity.Info;
            dto.MarketInfoStatus = DtoMappingAttribute.GetValueFromEnum(entity.MarketInfoStatus);
            dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
            dto.OwnerOrganisationId = entity.OwnerOrganisationId;
            dto.MarketInfoType = DtoMappingAttribute.GetValueFromEnum(entity.MarketInfoType);

            return dto;
        }
    }
}
