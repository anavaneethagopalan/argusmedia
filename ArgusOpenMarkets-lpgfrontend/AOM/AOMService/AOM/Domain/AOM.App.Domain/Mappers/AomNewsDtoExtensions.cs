﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using System.Globalization;

namespace AOM.App.Domain.Mappers
{
    public static class AomNewsDtoExtensions
    {
        public static AomNews ToEntity(this NewsDto repositoryNewsDto, AomNews aomNews = null)
        {
            var news = aomNews ?? new AomNews();
            news.Story = repositoryNewsDto.Story;
            news.CmsId = repositoryNewsDto.CmsId.ToString(CultureInfo.InvariantCulture);
            news.PublicationDate = repositoryNewsDto.PublicationDate;
            news.IsFree = repositoryNewsDto.IsFree;

            news.ContentStreams = repositoryNewsDto.ContentStreams.Select(cs => cs.ContentStreamId).ToArray();
            return news;
        }

        public static NewsDto ToDto(this AomNews aomNews)
        {
            var newsDto = new NewsDto
            {
                Story = aomNews.Story,
                CmsId = aomNews.CmsId,
                PublicationDate = aomNews.PublicationDate,
                IsFree = aomNews.IsFree,
                ContentStreams = new List<NewsContentStreamDto>()
            };


            if (aomNews.ContentStreams != null)
            {
                foreach (var cs in aomNews.ContentStreams)
                {
                    newsDto.ContentStreams.Add(new NewsContentStreamDto {ContentStreamId = cs});
                }
            }

            return newsDto;
        }
    }
}