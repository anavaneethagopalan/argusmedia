﻿namespace AOM.App.Domain.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;

    public static class CommodityExtensions
    {

        public static Commodity ToEntity(this CommodityDto dto, Commodity commodity = null)
        {
            var entity = commodity ?? new Commodity();

            entity.Id = dto.Id;
            entity.Name = dto.Name;
            entity.DateCreated = dto.DateCreated;
            entity.LastUpdated = dto.LastUpdated;
            entity.EnteredByUserId = (dto.EnteredByUserId.HasValue) ? dto.EnteredByUserId.Value : -1;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;

            return entity;
        }
    }
}