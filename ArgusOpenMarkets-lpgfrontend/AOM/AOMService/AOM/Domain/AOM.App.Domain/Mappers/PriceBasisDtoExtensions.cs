﻿using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class PriceBasisDtoExtensions
    {
        public static PriceBasis ToEntity(this PriceBasisDto dto, PriceBasis priceBasis = null, long productPriceLineId = -1)
        {
            var entity = priceBasis ?? new PriceBasis();

            entity.Id = dto.Id;
            entity.PriceTypeId = dto.PriceTypeId;
            entity.PriceTypeName = dto.PriceTypeDto != null ? dto.PriceTypeDto.Name : null;
            entity.PriceUnitCode = dto.PriceUnitCode;
            entity.PriceUnitDescription = dto.PriceUnitDto != null ? dto.PriceUnitDto.Description : null;
            entity.CurrencyCode = dto.CurrencyCode;
            entity.CurrencyDisplayName = dto.CurrencyDto != null ? dto.CurrencyDto.DisplayName : null;
            entity.ShortName = dto.ShortName;
            entity.Name = dto.Name;
            entity.PriceMinimum = dto.PriceMinimum;
            entity.PriceMaximum = dto.PriceMaximum;
            entity.PriceIncrement = dto.PriceIncrement;
            entity.PriceDecimalPlaces = dto.PriceDecimalPlaces;
            entity.PriceSignificantFigures = dto.PriceSignificantFigures;

            if (productPriceLineId != -1)
                entity.PriceBasisPeriods = dto.ProductPriceLineBasesPeriods.Where(pplbp => pplbp.ProductPriceLineId == productPriceLineId).Select(pb => pb.PeriodDto.ToEntity()).ToList();

            return entity;
        }

        public static PriceBasisDto ToDto(this PriceBasis entity, PriceBasisDto passedDto = null)
        {
            var dto = passedDto ?? new PriceBasisDto();
            dto.Id = entity.Id;
            dto.PriceTypeId = entity.PriceTypeId;
            dto.PriceUnitCode = entity.PriceUnitCode;
            dto.CurrencyCode = entity.CurrencyCode;
            dto.ShortName = entity.ShortName;
            dto.Name = entity.Name;
            dto.PriceMinimum = entity.PriceMinimum;
            dto.PriceMaximum = entity.PriceMaximum;
            dto.PriceIncrement = entity.PriceIncrement;
            dto.PriceDecimalPlaces = entity.PriceDecimalPlaces;
            dto.PriceSignificantFigures = entity.PriceSignificantFigures;

            return dto;
        }
    }
}

