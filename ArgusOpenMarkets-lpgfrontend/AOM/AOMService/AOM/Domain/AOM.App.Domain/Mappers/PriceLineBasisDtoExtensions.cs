﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using System.Linq;

namespace AOM.App.Domain.Mappers
{
    public static class PriceLineBasisDtoExtensions
    {
        public static PriceLineBasis ToEntity(this PriceLineBasisDto dto, PriceLineBasis passedEntity = null) //, Type requestingParentType = null
        {
            var entity = passedEntity ?? new PriceLineBasis();

            entity.Id = dto.Id;
            entity.DateCreated = dto.DateCreated;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            entity.PriceLineId = dto.PriceLineId;
            entity.PriceBasisId = dto.PriceBasisId;
            entity.ProductId = dto.ProductId;
            entity.TenorCode = dto.TenorCode;
            entity.Description = dto.Description;
            entity.SequenceNo = dto.SequenceNo;
            entity.PercentageSplit = dto.PercentageSplit;
            entity.PriceLineBasisValue = dto.PriceLineBasisValue;
            entity.PricingPeriod = dto.PricingPeriod;
            entity.PricingPeriodFrom = dto.PricingPeriodFrom;
            entity.PricingPeriodTo = dto.PricingPeriodTo;

            if (dto.PriceBasisDto != null) //typeof(PriceLineDto).IsAssignableFrom(requestingParentType) && 
            {
                entity.PriceBasis = dto.PriceBasisDto.ToEntity();
            }

            return entity;
        }

        /// <summary>
        /// Copies deserialised entity into a DTO
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="passedDto"></param>
        /// <returns></returns>
        public static PriceLineBasisDto ToDto(this PriceLineBasis entity, PriceLineBasisDto passedDto = null) //, Type requestingParentType = null
        {
            var dto = passedDto ?? new PriceLineBasisDto();

            SetEditableFields(entity, dto);

            dto.Id = entity.Id;
            //dto.DateCreated = entity.DateCreated; //computed column
            dto.PriceLineId = entity.PriceLineId;
            dto.PriceBasisId = entity.PriceBasisId;
            dto.ProductId = entity.ProductId;
            dto.TenorCode = entity.TenorCode;
            dto.SequenceNo = entity.SequenceNo;

            if (entity.PriceBasis != null)
            {
                dto.PriceBasisDto = entity.PriceBasis.ToDto();
            }

            return dto;
        }

        /// <summary>
        /// Updates DTO with new values from deserialised entity
        /// </summary>
        /// <param name="repositoryPriceLineBasis"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static PriceLineBasisDto UpdateDto(this PriceLineBasisDto repositoryPriceLineBasis, PriceLineBasis entity)
        {
            var dto = repositoryPriceLineBasis;

            SetEditableFields(entity, dto);

            return dto;
        }

        private static void SetEditableFields(PriceLineBasis entity, PriceLineBasisDto dto)
        {
            dto.Description = entity.Description;
            dto.PercentageSplit = entity.PercentageSplit;
            dto.PriceLineBasisValue = (dto.PriceLineDto == null || dto.PriceLineDto.OrderPriceLines == null || dto.PriceLineDto.OrderPriceLines.FirstOrDefault().OrderDto == null) ? entity.PriceLineBasisValue : dto.PriceLineDto.OrderPriceLines.FirstOrDefault().OrderDto.Price;
            dto.PricingPeriod = entity.PricingPeriod;
            dto.PricingPeriodFrom = entity.PricingPeriodFrom;
            dto.PricingPeriodTo = entity.PricingPeriodTo;
            //dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
        }
    }
}
