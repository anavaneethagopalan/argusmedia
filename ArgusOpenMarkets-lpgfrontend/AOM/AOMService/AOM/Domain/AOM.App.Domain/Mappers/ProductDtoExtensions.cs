﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using System.Collections.Generic;
using System.Linq;

namespace AOM.App.Domain.Mappers
{
    public static class ProductDtoExtensions
    {
        public static ProductSetting ToEntity(this ProductDto dto, ProductSetting productSetting = null)
        {
            if (productSetting == null)
            {
                productSetting = new ProductSetting();
            }

            var product = new Product();
            productSetting.Product = dto.ToEntity(product);

            productSetting.ProductTenors = new List<ProductTenor>();

            if (dto.ProductTenors != null)
            {
                foreach (var pt in dto.ProductTenors)
                {
                    var prodTenor = pt.ToEntity(new ProductTenor());
                    //prodTenor.ParentTenor = pt.TenorDto.ToEntity(new TenorCode());
                    productSetting.ProductTenors.Add(prodTenor);
                }
            }

            return productSetting;
        }

        public static Product ToEntity(this ProductDto dto, Product product = null)
        {
            var entity = product ?? new Product();

            entity.ProductId = dto.Id;
            entity.BidAskStackNumberRows = dto.BidAskStackNumberRows;
            entity.BidAskStackStyleCode = dto.BidAskStackStyleCode;
            entity.CloseTime = dto.CloseTime;
            entity.CommodityId = dto.CommodityId;
            entity.ContractTypeId = dto.ContractTypeId;
            //entity.CurrencyCode = dto.Currency;
            //entity.CurrencyDisplayName = dto.CurrencyDto == null ? string.Empty : dto.CurrencyDto.DisplayName;
            entity.DateCreated = dto.DateCreated;
            entity.IgnoreAutomaticMarketStatusTriggers = dto.IgnoreAutomaticMarketStatusTriggers;
            entity.AllowNegativePriceForExternalDeals = dto.AllowNegativePriceForExternalDeals;
            entity.DisplayOptionalPriceDetail = dto.DisplayOptionalPriceDetail;
            entity.IsDeleted = dto.IsDeleted;
            entity.IsInternal = dto.IsInternal;
            entity.LastCloseDate = dto.LastCloseDate;
            entity.LastOpenDate = dto.LastOpenDate;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            //entity.LocationId = dto.LocationId;
            entity.MarketId = dto.MarketId;
            entity.Name = dto.Name;
            entity.OpenTime = dto.OpenTime;
            entity.PurgeDate = dto.PurgeDate;
            entity.PurgeFrequency = dto.PurgeFrequency;
            entity.PurgeTimeOfDay = dto.PurgeTimeOfDay;
            //entity.PriceUnitCode = dto.PriceUnit;
            //entity.PriceUnitDisplayName = dto.PriceUnitDto == null ? string.Empty : dto.PriceUnitDto.Name;
            entity.Status = DtoMappingAttribute.FindEnumWithValue<MarketStatus>(dto.Status);
            entity.VolumeUnitCode = dto.VolumeUnit;
            entity.VolumeUnitDisplayName = dto.VolumeUnitDto == null ? string.Empty : dto.VolumeUnitDto.Name;
            entity.HasAssessment = dto.HasAssessment;
            //entity.LocationLabel = dto.LocationLabel;
            entity.DeliveryPeriodLabel = dto.DeliveryPeriodLabel;
            entity.DisplayOrder = dto.DisplayOrder;
            entity.CoBrokering = dto.CoBrokering;
            entity.OrderShowNotes = dto.OrderShowNotes;
            entity.ExternalDealShowNotes = dto.ExternalDealShowNotes;
            entity.DisplayContractName = dto.DisplayContractName;
            
            if (dto.DeliveryLocations != null)
            {
                entity.DeliveryLocations = dto.DeliveryLocations.Select(dl => dl.ToEntity()).ToList();
            }

            return entity;
        }

        public static ProductDto ToDto(this Product entity, ProductDto passedDto = null)
        {
            var dto = passedDto ?? new ProductDto();

            dto.Id = entity.ProductId;
            dto.BidAskStackNumberRows = entity.BidAskStackNumberRows;
            dto.BidAskStackStyleCode = entity.BidAskStackStyleCode;
            dto.CloseTime = entity.CloseTime;
            dto.CommodityId = entity.CommodityId;
            dto.ContractTypeId = entity.ContractTypeId;
            //dto.Currency = entity.CurrencyCode;
            dto.DateCreated = entity.DateCreated;
            //dto.ExchangeId = entity.ExchangeId;
            dto.IgnoreAutomaticMarketStatusTriggers = entity.IgnoreAutomaticMarketStatusTriggers;
            dto.AllowNegativePriceForExternalDeals = entity.AllowNegativePriceForExternalDeals;
            dto.DisplayOptionalPriceDetail = entity.DisplayOptionalPriceDetail;
            dto.IsDeleted = entity.IsDeleted;
            dto.IsInternal = entity.IsInternal;
            dto.LastCloseDate = entity.LastCloseDate;
            dto.LastOpenDate = entity.LastOpenDate;
            dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
            //dto.LocationId = entity.LocationId;
            dto.MarketId = entity.MarketId;
            dto.Name = entity.Name;
            dto.OpenTime = entity.OpenTime;
            //dto.PriceUnit = entity.PriceUnitCode;
            dto.PurgeDate = entity.PurgeDate;
            dto.PurgeFrequency = entity.PurgeFrequency;
            dto.PurgeTimeOfDay = entity.PurgeTimeOfDay;
            dto.Status = DtoMappingAttribute.GetValueFromEnum(entity.Status);
            dto.VolumeUnit = entity.VolumeUnitCode;
            dto.HasAssessment = entity.HasAssessment;
            //dto.LocationLabel = entity.LocationLabel;
            dto.DeliveryPeriodLabel = entity.DeliveryPeriodLabel;
            dto.DisplayOrder = entity.DisplayOrder;
            dto.CoBrokering = entity.CoBrokering;
            dto.OrderShowNotes = entity.OrderShowNotes;
            dto.ExternalDealShowNotes = entity.ExternalDealShowNotes;
            dto.DisplayContractName = entity.DisplayContractName;

            if (entity.DeliveryLocations != null)
            {
                dto.DeliveryLocations = entity.DeliveryLocations.Select(dl => dl.ToDto()).ToList();
            }
            return dto;
        }
    }
}