using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class MetaDataExtensions
    {
        public static MetaData[] ToEntity(this MetaDataDto[] dto)
        {
            if (dto == null) return null;
            return dto.Select(m => m != null ? m.ToEntity() : null).ToArray();
        }

        public static MetaData ToEntity(this MetaDataDto dto, MetaData omd = null)
        {
            var o = omd ?? new MetaData();

            o.ProductMetaDataId = dto.ProductMetaDataId;
            o.DisplayName = dto.DisplayName;
            o.DisplayValue = dto.DisplayValue;
            o.ItemType = dto.MetaDataType;

            if (dto is MetaDataIntegerEnumDto)
            {
                o.MetaDataListItemId = (dto as MetaDataIntegerEnumDto).Id;
                o.ItemValue = (dto as MetaDataIntegerEnumDto).ItemValue;                
            }
        
            return o;
        }

        public static MetaDataDto[] ToDto(this MetaData[] dto)
        {
            if (dto == null) return null;
            return dto.Select(m => m != null ? m.ToDto() : null).ToArray();
        }

        public static MetaDataDto ToDto(this MetaData omd, MetaDataDto dto = null)
        {
            MetaDataDto metaDataDto = null;

            switch (omd.ItemType)
            {
                case MetaDataTypes.IntegerEnum:
                    MetaDataIntegerEnumDto dtoEnum = new MetaDataIntegerEnumDto();
                    dtoEnum.Id = omd.MetaDataListItemId.Value;
                    dtoEnum.ItemValue = omd.ItemValue.Value;
                    metaDataDto = dtoEnum;
                    break;

                case MetaDataTypes.String:
                    MetaDataStringDto dtoString = new MetaDataStringDto();
                    metaDataDto = dtoString;
                    break;

                default:
                    MetaDataStringDto dtoDefault = new MetaDataStringDto();
                    metaDataDto = dtoDefault;
                    break;
            }

            metaDataDto.ProductMetaDataId = omd.ProductMetaDataId;
            metaDataDto.DisplayName = omd.DisplayName;
            metaDataDto.DisplayValue = omd.DisplayValue;
            return metaDataDto;
        }
    }
}