using System;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;

namespace AOM.App.Domain.Mappers
{
    public static class CounterpartyPermissionDtoExtensions
    {
        private static readonly DateTimeProvider DateTimeProvider;

        static CounterpartyPermissionDtoExtensions()
        {
            DateTimeProvider = new DateTimeProvider();
        }

        static public MatrixPermission ToEntity(this IIntraOrganisationPermission dto, MatrixPermission counterpartyPermission = null)
        {
            if (dto == null) return null;
            var entity = counterpartyPermission ?? new MatrixPermission();

            entity.Id = dto.Id;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            entity.ProductId = dto.ProductId;
            entity.OurOrganisation = dto.OurOrganisation;
            entity.TheirOrganisation = dto.TheirOrganisation;
            entity.BuyOrSell = DtoMappingAttribute.FindEnumWithValue<BuyOrSell>(dto.BuyOrSell);
            entity.AllowOrDeny = DtoMappingAttribute.FindEnumWithValue<Permission>(dto.AllowOrDeny);

            return entity;
        }

        static public CounterpartyPermissionDto ToDto(this MatrixPermission entity, CounterpartyPermissionDto passedDto = null)
        {
            var dto = passedDto ?? new CounterpartyPermissionDto();

            dto.Id = entity.Id;
            dto.LastUpdated = entity.LastUpdated ?? DateTime.MinValue;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId ?? -1;
            dto.ProductId = entity.ProductId;
            dto.OurOrganisation = entity.OurOrganisation;
            dto.TheirOrganisation = entity.TheirOrganisation;
            dto.BuyOrSell = DtoMappingAttribute.GetValueFromEnum(entity.BuyOrSell);
            dto.AllowOrDeny = DtoMappingAttribute.GetValueFromEnum(entity.AllowOrDeny);

            return dto;
        }
    }
}