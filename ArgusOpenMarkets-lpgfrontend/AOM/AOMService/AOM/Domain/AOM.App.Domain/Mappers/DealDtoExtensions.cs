using System;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class DealDtoExtensions
    {
        private static DateTimeProvider dateTimeProvider;

        static DealDtoExtensions()
        {
            dateTimeProvider = new DateTimeProvider();
        }

        static public Deal ToEntity(this DealDto dto,IOrganisationService organisationService, IUserService userService, Deal deal = null)
        {
            var entity = deal ?? new Deal();

            entity.Id = dto.Id;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            entity.Message = dto.Message;

            switch (dto.DealStatus)
            {
                case "V":
                    entity.DealStatus = DealStatus.Void;
                    break;
                case "E":
                    entity.DealStatus = DealStatus.Executed;
                    break;
                case "P":
                    entity.DealStatus = DealStatus.Pending;
                    break;
                default:
                    throw new Exception("GetDomainDealStatus called with invalid status: '" + dto.DealStatus + "'");
            }

            entity.InitialOrderId = dto.InitialOrderId;
            entity.MatchingOrderId = dto.MatchingOrderId;
            entity.EnteredByUserId = dto.EnteredByUserId;
            entity.CreatedDate = dto.DateCreated;
            entity.Initial = dto.InitialOrderDto == null ? null : dto.InitialOrderDto.ToEntity(organisationService,userService);
            entity.Matching = dto.MatchingOrderDto == null ? null : dto.MatchingOrderDto.ToEntity(organisationService,userService);

            return entity;
        }

        static public DealDto ToDto(this Deal entity, DealDto passedDto = null)
        {
            var dto = passedDto ?? new DealDto();

            dto.Id = entity.Id;
            dto.LastUpdated = entity.LastUpdated ?? dateTimeProvider.UtcNow;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
            dto.Message = entity.Message;
            
            switch (entity.DealStatus)
            {
                case DealStatus.Void:
                    dto.DealStatus = "V";
                    break;
                case DealStatus.Executed:
                    dto.DealStatus = "E";
                    break;
                case DealStatus.Pending:
                    dto.DealStatus = "P";
                    break;
                default:
                    throw new Exception("GetDealStatus called with invalid status: '" + entity.DealStatus + "'");
            }

            dto.InitialOrderId = entity.InitialOrderId;
            dto.MatchingOrderId = entity.MatchingOrderId;
            dto.EnteredByUserId = entity.EnteredByUserId;
            dto.DateCreated = entity.CreatedDate;

            return dto;
        }
    }
}