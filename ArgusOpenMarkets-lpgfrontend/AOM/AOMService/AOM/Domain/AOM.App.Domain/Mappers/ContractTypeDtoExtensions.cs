﻿namespace AOM.App.Domain.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;

    public static class ContractTypeDtoExtensions
    {

        public static ContractType ToEntity(this ContractTypeDto dto, ContractType contractType = null)
        {
            var entity = contractType ?? new ContractType();

            entity.Id = dto.Id;
            entity.Name = dto.Name;
            entity.DateCreated = dto.DateCreated;

            return entity;
        }
    }
}