﻿using System;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class ProductMetaDataDtoExtensions
    {
        public static ProductMetaDataItem ToEntity(this ProductMetaDataItemDto dto, ProductMetaDataItem md = null)
        {
            var entity = md ?? BuildEntity(dto);
            entity.Id = dto.Id;
            entity.ProductId = dto.ProductId;
            entity.DisplayName = dto.DisplayName;

            return entity;
        }

        public static ProductMetaDataItemDto ToDto(this ProductMetaDataItem productMetaDataItem)
        {
            var metadataItemDto = new ProductMetaDataItemDto();

            try
            {
                if (productMetaDataItem != null)
                {
                    metadataItemDto = new ProductMetaDataItemDto
                    {
                        Id = productMetaDataItem.Id,
                        ProductId = productMetaDataItem.ProductId,
                        DisplayOrder = 0,
                        DisplayName = productMetaDataItem.DisplayName,
                        MetaDataTypeId = productMetaDataItem.FieldType
                    };
                }
            }
            catch (Exception)
            {
                // Log.Error("ProductMetaDataItemDto.ToDto", ex);
            }


            ProductMetaDataItemString stringMetadataItem = null;
            try
            {
                stringMetadataItem = productMetaDataItem as ProductMetaDataItemString;
            }
            catch (Exception)
            {
                // Log.ErrorFormat("ProductMetaDataItemDto.ToStringDto", ex);
            }

            if (stringMetadataItem != null)
            {
                try
                {
                    metadataItemDto.ValueMinimum = stringMetadataItem.ValueMinimum;
                    metadataItemDto.ValueMaximum = stringMetadataItem.ValueMaximum;
                }
                catch (Exception)
                {
                    // Log.Error("Error Getting volume - min/max", ex);
                }
            }
            ProductMetaDataItemEnum enumMetadataItem = null;

            try
            {
                enumMetadataItem = productMetaDataItem as ProductMetaDataItemEnum;
            }
            catch (Exception)
            {
                // Log.Error("Error casting to ProductMetaDataItemEnum", ex);
            }

            if (enumMetadataItem != null)
            {
                metadataItemDto.MetaDataListId = enumMetadataItem.MetadataList.Id;
            }

            return metadataItemDto;
        }

        public class Log
        {
        }

        public static void Update(
            this ProductMetaDataItemDto sourceMetaDataItemDto,
            ProductMetaDataItem productMetaDataItem)
        {
            if (sourceMetaDataItemDto.Id != productMetaDataItem.Id)
                throw new Exception("Could not update metadata item with different id.");

            sourceMetaDataItemDto.DisplayName = productMetaDataItem.DisplayName;
            sourceMetaDataItemDto.ProductId = productMetaDataItem.ProductId;
            sourceMetaDataItemDto.MetaDataTypeId = productMetaDataItem.FieldType;
            
            var stringMetadataItem = productMetaDataItem as ProductMetaDataItemString;
            if (stringMetadataItem != null)
            {
                sourceMetaDataItemDto.ValueMinimum = stringMetadataItem.ValueMinimum;
                sourceMetaDataItemDto.ValueMaximum = stringMetadataItem.ValueMaximum;
            }
            else
            {
                sourceMetaDataItemDto.ValueMinimum = null;
                sourceMetaDataItemDto.ValueMaximum = null;
            }

            var enumMetadataItem = productMetaDataItem as ProductMetaDataItemEnum;
            if (enumMetadataItem != null)
            {
                sourceMetaDataItemDto.MetaDataListId = enumMetadataItem.MetadataList.Id;
            }
            else
            {
                sourceMetaDataItemDto.MetaDataListId = null;
            }
        }

        private static ProductMetaDataItem BuildEntity(ProductMetaDataItemDto dto)
        {            
            switch (dto.MetaDataTypeId)
            {
                case MetaDataTypes.IntegerEnum:
                    return new ProductMetaDataItemEnum
                    {
                        MetadataList = dto.MetaDataList != null ? dto.MetaDataList.ToEntity() : null
                    };
                case MetaDataTypes.String:
                    return new ProductMetaDataItemString
                    {
                        ValueMinimum = dto.ValueMinimum ?? 0,
                        ValueMaximum = dto.ValueMaximum ?? 0
                    };
            }

            throw new Exception(string.Format("MetaDataType [{0}] not supported", dto.MetaDataTypeId));
        }
    }
}
