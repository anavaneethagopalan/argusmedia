﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class DeliveryLocationDtoExtensions
    {
        public static DeliveryLocation ToEntity(this DeliveryLocationDto dto, DeliveryLocation passedEntity = null)
        {
            var entity = passedEntity ?? new DeliveryLocation();

            entity.Id = dto.Id;
            entity.DateCreated = dto.DateCreated;
            entity.Name = dto.Name;
            entity.LocationLabel = dto.LocationLabel;

            return entity;
        }

        public static DeliveryLocationDto ToDto(this DeliveryLocation entity, DeliveryLocationDto passedDto = null)
        {
            var dto = passedDto ?? new DeliveryLocationDto();

            dto.Id = entity.Id;
            dto.DateCreated = entity.DateCreated;
            dto.Name = entity.Name;
            dto.LocationLabel = entity.LocationLabel;

            return dto;
        }
    }
}