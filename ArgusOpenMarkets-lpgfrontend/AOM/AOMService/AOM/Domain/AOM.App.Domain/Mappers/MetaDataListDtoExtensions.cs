﻿using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Mappers
{
    public static class MetaDataListDtoExtensions
    {
        public static MetaDataList ToEntity(this MetaDataListDto dto)
        {
            return new MetaDataList
            {
                Id = dto.Id,
                Description = dto.Description,
                FieldList = dto.MetaDataListItems.Where(x => !x.IsDeleted)
                    .OrderBy(x => x.DisplayOrder)
                    .Select(x => new MetaDataListItem<long>(x.Id, x.ValueLong, x.ValueText)).ToArray()
            };
        }

        public static MetaDataListDto ToDto(this MetaDataList metadataList)
        {
            return new MetaDataListDto
            {
                Id = metadataList.Id,
                Description = metadataList.Description,
                MetaDataListItems = metadataList.FieldList.Select((x, index) => new MetaDataListItemDto
                {
                    Id = x.Id,
                    DisplayOrder = index + 1,
                    IsDeleted = false,
                    MetaDataListId = metadataList.Id,
                    ValueLong = x.ItemValue,
                    ValueText = x.DisplayName
                }).ToList()
            };
        }
    }
}