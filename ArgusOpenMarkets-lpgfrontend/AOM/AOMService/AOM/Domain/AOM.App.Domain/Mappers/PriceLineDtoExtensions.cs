﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using System.Linq;
using AOM.App.Domain.Dates;

namespace AOM.App.Domain.Mappers
{
    public static class PriceLineDtoExtensions
    {
        public static PriceLine ToEntity(this PriceLineDto dto, PriceLine passedEntity = null) //, Type requestingParentType = null
        {
            var entity = passedEntity ?? new PriceLine();

            entity.Id = dto.Id;
            entity.SequenceNo = dto.SequenceNo;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;

            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;

            if (dto.PriceLinesBases != null && dto.PriceLinesBases.Count > 0) //typeof(PriceLineBasisDto) != requestingParentType && 
            {
                entity.PriceLinesBases = dto.PriceLinesBases.Select(pl => pl.ToEntity()).ToList();
            }

            return entity;
        }

        ///// <param name="requestingParentType">To be used by other ToDto() methods, to avoid infinite loop. ToDto() will not be called on child entities of provided type.</param>
        /// <summary>
        /// Copies deserialised entity into a DTO
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="passedDto"></param>
        /// <returns></returns>
        public static PriceLineDto ToDto(this PriceLine entity, PriceLineDto passedDto = null) //, Type requestingParentType = null
        {
            var dto = passedDto ?? new PriceLineDto();

            dto.Id = entity.Id;
            dto.SequenceNo = entity.SequenceNo;

            SetEditableFields(entity, dto);

            if (entity.PriceLinesBases != null && entity.PriceLinesBases.Count > 0) //requestingParentType != typeof(PriceLineBasis) && 
            {
                dto.PriceLinesBases = entity.PriceLinesBases.Select(plb => plb.ToDto()).ToList();
            }

            return dto;
        }

        /// <summary>
        /// Updates DTO with new values from deserialised entity
        /// </summary>
        /// <param name="repositoryPriceLine"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static PriceLineDto UpdateDto(this PriceLineDto repositoryPriceLine, PriceLine entity)
        {
            var dto = repositoryPriceLine;

            SetEditableFields(entity, dto);

            if (entity.PriceLinesBases != null && entity.PriceLinesBases.Count > 0)
            {
                var newPriceLineBases = new List<PriceLineBasisDto>();
                foreach (var priceLineBasis in entity.PriceLinesBases)
                {
                    var priceLineBasisDto = dto.PriceLinesBases.SingleOrDefault(plb => plb.Id == priceLineBasis.Id);
                    if (priceLineBasisDto != null)
                    {
                        priceLineBasisDto.UpdateDto(priceLineBasis);
                    }
                    else
                    {
                        priceLineBasisDto = priceLineBasis.ToDto();
                    }
                    newPriceLineBases.Add(priceLineBasisDto);
                }
                
                var deletedEntities = dto.PriceLinesBases.Except(newPriceLineBases).ToList();
                if (deletedEntities.Count > 0)
                    throw new InvalidOperationException("PriceLineBases cannot be deleted.");

                dto.PriceLinesBases = newPriceLineBases;
            }

            return dto;
        }

        private static void SetEditableFields(PriceLine entity, PriceLineDto dto)
        {
            dto.LastUpdated = entity.LastUpdated;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId;
        }
    }
}