﻿namespace AOM.App.Domain.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;

    public static class UnitDtoExtensions
    {

        public static Unit ToEntity(this UnitDto dto, Unit unit = null)
        {
            var entity = unit ?? new Unit();

            entity.Code = dto.Code;
            entity.Name = dto.Name;
            entity.DateCreated = dto.DateCreated;
            entity.Description = dto.Description;

            return entity;
        }
    }
}