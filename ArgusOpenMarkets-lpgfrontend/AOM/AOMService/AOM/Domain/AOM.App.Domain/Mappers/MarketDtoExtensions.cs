﻿namespace AOM.App.Domain.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;

    public static class MarketDtoExtensions
    {

        public static Market ToEntity(this MarketDto dto, Market contractType = null)
        {
            var entity = contractType ?? new Market();

            entity.Id = dto.Id;
            entity.Name = dto.Name;
            entity.DateCreated = dto.DateCreated;

            return entity;
        }
    }
}