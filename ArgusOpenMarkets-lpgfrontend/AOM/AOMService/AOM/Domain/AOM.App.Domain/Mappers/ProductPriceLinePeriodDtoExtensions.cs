﻿//using AOM.App.Domain.Entities;
//using AOM.Repository.MySql.Aom;

//namespace AOM.App.Domain.Mappers
//{
//    public static class ProductPriceLinePeriodDtoExtensions
//    {
//        public static ProductPriceLinePeriod ToEntity(this ProductPriceLinePeriodDto dto, ProductPriceLinePeriod passedEntity = null)
//        {
//            var entity = passedEntity ?? new ProductPriceLinePeriod();

//            entity.PricePeriodId = dto.PricePeriodId;
//            entity.ProductPriceLineId = dto.ProductPriceLineId;

//            return entity;
//        }

//        public static ProductPriceLinePeriodDto ToDto(this ProductPriceLinePeriod entity, ProductPriceLinePeriodDto passedDto = null)
//        {
//            var dto = passedDto ?? new ProductPriceLinePeriodDto();

//            dto.PricePeriodId = entity.PricePeriodId;
//            dto.ProductPriceLineId = entity.ProductPriceLineId;

//            return dto;
//        }
//    }
//}
