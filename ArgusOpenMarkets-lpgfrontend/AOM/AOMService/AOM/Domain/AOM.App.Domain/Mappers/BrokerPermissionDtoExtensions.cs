using System;

namespace AOM.App.Domain.Mappers
{
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Crm;

    public static class BrokerPermissionDtoExtensions
    {
        private static readonly DateTimeProvider DateTimeProvider;

        static BrokerPermissionDtoExtensions()
        {
            DateTimeProvider = new DateTimeProvider();
        }

        public static MatrixPermission ToEntity(this BrokerPermissionDto dto, MatrixPermission brokerPermission = null)
        {
            if (dto == null) return null;
            var entity = brokerPermission ?? new MatrixPermission();

            entity.Id = dto.Id;
            entity.LastUpdated = dto.LastUpdated;
            entity.LastUpdatedUserId = dto.LastUpdatedUserId;
            entity.ProductId = dto.ProductId;
            entity.OurOrganisation = dto.OurOrganisation;
            entity.TheirOrganisation = dto.TheirOrganisation;
            entity.BuyOrSell = DtoMappingAttribute.FindEnumWithValue<BuyOrSell>(dto.BuyOrSell);
            entity.AllowOrDeny = DtoMappingAttribute.FindEnumWithValue<Permission>(dto.AllowOrDeny);

            return entity;
        }

        public static BrokerPermissionDto ToDto(this MatrixPermission entity, BrokerPermissionDto passedDto = null)
        {
            var dto = passedDto ?? new BrokerPermissionDto();

            dto.Id = entity.Id;
            dto.LastUpdated = entity.LastUpdated ?? DateTime.MinValue;
            dto.LastUpdatedUserId = entity.LastUpdatedUserId ?? -1;
            dto.ProductId = entity.ProductId;
            dto.OurOrganisation = entity.OurOrganisation;
            dto.TheirOrganisation = entity.TheirOrganisation;
            dto.BuyOrSell = DtoMappingAttribute.GetValueFromEnum(entity.BuyOrSell);
            dto.AllowOrDeny = DtoMappingAttribute.GetValueFromEnum(entity.AllowOrDeny);

            return dto;
        }
    }
}