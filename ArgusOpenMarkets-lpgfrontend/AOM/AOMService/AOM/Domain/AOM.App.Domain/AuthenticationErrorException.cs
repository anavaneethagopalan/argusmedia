﻿using System;
using System.Runtime.Serialization;

namespace AOM.App.Domain
{
    [Serializable]
    public class AuthenticationErrorException : Exception
    {
        public AuthenticationErrorException()
        {
        }

        public AuthenticationErrorException(string message)
            : base(message)
        {
        }

        public AuthenticationErrorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public AuthenticationErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}