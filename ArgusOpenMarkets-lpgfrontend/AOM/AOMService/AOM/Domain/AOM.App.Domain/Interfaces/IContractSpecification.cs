﻿using System.Collections.Generic;

namespace AOM.App.Domain.Interfaces
{
    public interface IContractSpecification
    {
        string ContractType { get; set; }

        IVolume Volume { get; set; }

        IPricing Pricing { get; set; }

        List<ITenor> Tenors { get; set; }
    }
}