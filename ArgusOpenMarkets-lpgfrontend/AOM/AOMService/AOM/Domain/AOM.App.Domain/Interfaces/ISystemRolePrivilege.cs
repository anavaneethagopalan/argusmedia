﻿namespace AOM.App.Domain.Interfaces
{
    public interface ISystemRolePrivilege
    {
        long Id { get; set; }

        long RoleId { get; set; }

        long PrivilegeId { get; set; }
    }
}
