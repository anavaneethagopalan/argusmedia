﻿namespace AOM.App.Domain.Interfaces
{
    using AOM.App.Domain.Entities;

    public interface ISystemPrivilege
    {
        long Id { get; set; }

        string Name { get; set; }

        string Description { get; set; }

        bool ArgusUseOnly { get; set; }

        string DefaultRoleGroup { get; set; }

        OrganisationType OrganisationType { get; set; }
    }
}