﻿namespace AOM.App.Domain.Interfaces
{
    public interface IVolume
    {
        string VolumeUnitCode { get; set; }

        decimal Minimum { get; set; }

        decimal Maximum { get; set; }

        decimal Increment { get; set; }

        decimal DecimalPlaces { get; set; }
    }
}