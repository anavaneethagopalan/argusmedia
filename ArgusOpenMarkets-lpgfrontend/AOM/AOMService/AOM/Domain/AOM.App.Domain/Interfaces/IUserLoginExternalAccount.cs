﻿using System;
using AOM.App.Domain.Entities;

namespace AOM.App.Domain.Interfaces
{
    interface IUserLoginExternalAccount
    {
        long Id { get; set; }
        long UserLoginSourceId { get; set; }
        long UserInfoId { get; set; }
        string ExternalUserCode { get; set; }
        string ExternalUserName { get; set; }
        string ExternalUserEmail { get; set; }
        string SSOCredential { get; set; }
        DateTime DateCreated { get; set; }
        bool IsDeleted { get; set; }

        User User { get; set; }
        UserLoginSource UserLoginSource { get; set; }
    }
}
