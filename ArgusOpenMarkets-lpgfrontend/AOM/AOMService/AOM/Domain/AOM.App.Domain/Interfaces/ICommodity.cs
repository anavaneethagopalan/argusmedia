﻿namespace AOM.App.Domain.Interfaces
{
    public interface ICommodity
    {
        long Id { get; set; }
        string Name { get; set; }
    }
}
