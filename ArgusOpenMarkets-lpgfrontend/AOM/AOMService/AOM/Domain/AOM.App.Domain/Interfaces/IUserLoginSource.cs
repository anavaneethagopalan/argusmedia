﻿using AOM.App.Domain.Entities;

namespace AOM.App.Domain.Interfaces
{
    public interface IUserLoginSource
    {
        long Id { get; set; }
        string Code { get; set; }
        string Name { get; set; }
        long OrganisationId { get; set; }

        Organisation ExternalSystemOrganisation { get; set; }
    }
}
