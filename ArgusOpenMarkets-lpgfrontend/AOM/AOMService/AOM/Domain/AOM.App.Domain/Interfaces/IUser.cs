﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.App.Domain.Interfaces
{
    public interface IUser
    {
        long Id { get; set; }

        string Username { get; set; }

        string Name { get; set; }
        
        bool IsActive { get; set; }

        bool IsDeleted { get; set; }

        bool IsBlocked { get; set; }

        string Token { get; set; }

        string Email { get; set; }

        long OrganisationId { get; set; }

        string Telephone { get; set; }

        string Title { get; set; }

        string ArgusCrmUsername { get; set; }

        Organisation UserOrganisation { get; set; }

        List<UserProductPrivilege> ProductPrivileges { get; set; }

        SystemPrivileges SystemPrivileges { get; set; }    
    }
}