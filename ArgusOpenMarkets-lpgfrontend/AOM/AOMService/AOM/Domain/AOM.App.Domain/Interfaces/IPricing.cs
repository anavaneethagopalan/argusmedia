﻿namespace AOM.App.Domain.Interfaces
{
    public interface IPricing
    {
        string PricingUnitCode { get; set; }

        string PricingCurrencyCode { get; set; }

        decimal Minimum { get; set; }

        decimal Maximum { get; set; }

        decimal Increment { get; set; }

        decimal DecimalPlaces { get; set; }
    }
}