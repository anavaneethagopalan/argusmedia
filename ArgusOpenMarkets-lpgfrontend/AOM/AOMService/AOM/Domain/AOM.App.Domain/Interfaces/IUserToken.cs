﻿using System;
using AOM.App.Domain.Entities;

namespace AOM.App.Domain.Interfaces
{
    public interface IUserToken
    {
        long Id { get; set; }

        string LoginTokenString { get; set; }

        long UserInfo_Id_fk { get; set; }

        string SessionId { get; set; }

        DateTime? TimeCreated { get; set; }

        string SessionTokenString { get; set; }

        string UserIpAddress { get; set; }

        User User { get; set; }
    }
}
