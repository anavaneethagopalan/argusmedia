﻿using System;

namespace AOM.App.Domain.Interfaces
{
    public interface IRunnable : IDisposable
    {
        void Run();

        void Disconnect();
    }
}