﻿using System;

namespace AOM.App.Domain.Interfaces
{
    public interface IProduct
    {
        long ProductId { get; set; }
        string Name { get; set; }
        DateTime DateCreated { get; set; }
        bool IsDeleted { get; set; }
        long CommodityId { get; set; }
        long MarketId { get; set; }
        long ContractTypeId { get; set; }
        //long LocationId { get; set; }
        //string CurrencyCode { get; set; }
        string VolumeUnitCode { get; set; }
        //string PriceUnitCode { get; set; }
        //long ExchangeId { get; set; }
        int BidAskStackNumberRows { get; set; }
    }
}
