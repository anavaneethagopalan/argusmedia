﻿namespace AOM.App.Domain.Interfaces
{
    internal interface IProductRolePrivilege
    {
        long Id { get; set; }

        long RoleId { get; set; }

        long OrganisationId { get; set; }

        long ProductId { get; set; }

        long ProductPrivilegeId { get; set; }
    }
}