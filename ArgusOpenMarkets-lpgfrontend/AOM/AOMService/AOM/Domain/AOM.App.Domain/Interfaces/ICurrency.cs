﻿using System;

namespace AOM.App.Domain.Interfaces
{
    public interface ICurrency
    {
        string Code { get; set; }

        string DisplayName { get; set; }

        string Name { get; set; }

        DateTime? DateCreated { get; set; }
    }
}