﻿using System;

namespace AOM.App.Domain.Interfaces
{
    public interface ISubscribedProductPrivilege
    {
        long ProductId { get; set; }

        long OrganisationId { get; set; }

        long ProductPrivilegeId { get; set; }

        DateTime DateCreated { get; set; }
        
        string CreatedBy { get; set; }
    }
}
