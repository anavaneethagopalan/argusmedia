﻿namespace AOM.App.Domain.Interfaces
{
    public interface IUserInfoRole
    {
        long UserId { get; set; }
        long RoleId { get; set; }
    }
}
