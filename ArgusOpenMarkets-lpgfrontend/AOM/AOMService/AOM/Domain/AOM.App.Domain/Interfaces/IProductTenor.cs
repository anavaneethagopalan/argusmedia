﻿using System;

namespace AOM.App.Domain.Interfaces
{
    public interface IProductTenor
    {
        long Id { get; set; }
        string Name { get; set; }
        string DisplayName { get; set; }
        long ProductId { get; set; }
        //long MinimumDeliveryRange { get; set; }
        //string DeliveryDateStart { get; set; }
        //string DeliveryDateEnd { get; set; }
        //string RollDate { get; set; }
        //string RollDateRule { get; set; }
        DateTime? DateCreated { get; set; }
    }
}