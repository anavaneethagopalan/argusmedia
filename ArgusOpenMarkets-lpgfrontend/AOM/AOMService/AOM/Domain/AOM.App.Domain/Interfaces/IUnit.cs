﻿using System;

namespace AOM.App.Domain.Interfaces
{
    public interface IUnit
    {
        string Code { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        DateTime? DateCreated { get; set; }
    }
}
