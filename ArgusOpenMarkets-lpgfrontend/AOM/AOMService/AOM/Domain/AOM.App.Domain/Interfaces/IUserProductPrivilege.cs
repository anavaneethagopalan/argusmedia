﻿using System;
using System.Collections.Generic;

namespace AOM.App.Domain.Interfaces
{
    public interface IUserProductPrivilege
    {
        long ProductId { get; set; }
        Dictionary<string,Boolean> Privileges { get; set; }
    }
}
