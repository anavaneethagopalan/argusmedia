﻿namespace AOM.App.Domain.Interfaces
{
    public interface IProductDefinitionItem
    {
        long ProductId { get; set; }
        string ProductName { get; set; }

        IContractSpecification ContractSpecification { get; set; }
    }
}
