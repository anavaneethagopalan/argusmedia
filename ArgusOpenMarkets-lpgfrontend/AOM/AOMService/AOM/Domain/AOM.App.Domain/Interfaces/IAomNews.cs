﻿using System;

namespace AOM.App.Domain.Interfaces
{
    public interface IAomNews
    {
        // For now - this is the Cms Id.
        string CmsId { get; set; }

        DateTime PublicationDate { get; set; }

        // TODO: Probably don't want web logic in our domain object.  
        string ItemUrl { get; set; }

        string Headline { get; set; }

        string Story { get; set; }

        bool IsFree { get; set; }

        string NewsType { get; set; }

        long[] ContentStreams { get; set; }

        ulong CommodityId { get; set; }
    }
}
