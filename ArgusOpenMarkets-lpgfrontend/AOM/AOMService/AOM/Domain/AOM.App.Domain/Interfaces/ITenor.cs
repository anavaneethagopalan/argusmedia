﻿namespace AOM.App.Domain.Interfaces
{
    public interface ITenor
    {
        IDeliveryLocation DeliveryLocation { get; set; }

        string DeliveryStartDate { get; set; }

        string DeliveryEndDate { get; set; }

        string RollDate { get; set; }

        int Id { get; set; }
    }
}