﻿using System;
using AOM.App.Domain.Entities;

namespace AOM.App.Domain.Interfaces
{

    public interface IOrganisationMinimumDetails
    {
        long Id { get; set; }
        string Name { get; set; }
        OrganisationType OrganisationType { get; set; }
        string ShortCode { get; set; }
        string LegalName { get; set; }
    }

    public class OrganisationMinimumDetails : IOrganisationMinimumDetails
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public OrganisationType OrganisationType { get; set; }
        public string ShortCode { get; set; }
        public string LegalName { get; set; }

        public BrokerRestriction BrokerRestriction { get; set; }
    }

    public interface IOrganisation : IOrganisationMinimumDetails
	{
        string Address { get; set; }
        string Email { get; set; }
        DateTime? DateCreated { get; set; }        
        string Description { get; set; }
		bool IsDeleted { get; set; }
	}
}
