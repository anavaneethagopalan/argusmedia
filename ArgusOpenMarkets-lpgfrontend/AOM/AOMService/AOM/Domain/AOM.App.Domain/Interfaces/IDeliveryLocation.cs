﻿using System;

namespace AOM.App.Domain.Interfaces
{
    public interface IDeliveryLocation
    {
        long Id { get; set; }

        string Name { get; set; }

        DateTime? DateCreated { get; set; }
    }
}