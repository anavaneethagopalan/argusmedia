﻿namespace AOM.App.Domain.Interfaces
{
    public interface IOrganisationRole
    {
        long Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        long OrganisationId { get; set; }
    }
}
