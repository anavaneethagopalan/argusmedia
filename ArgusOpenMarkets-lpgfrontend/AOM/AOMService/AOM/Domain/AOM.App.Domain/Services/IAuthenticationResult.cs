﻿using AOM.App.Domain.Entities;

namespace AOM.App.Domain.Services
{
    public interface IAuthenticationResult
	{
        long UserId { get; set; }

        bool IsAuthenticated { get; set; }
		User User { get; set; }
        string Message { get; set; }
        string Token { get; set; }
        string SessionToken { get; set; }
        bool FirstLogin { get; set; }
        string CredentialType { get; set; }
    }
}
