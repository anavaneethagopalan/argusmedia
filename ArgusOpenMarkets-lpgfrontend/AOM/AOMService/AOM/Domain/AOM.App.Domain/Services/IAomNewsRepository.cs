﻿namespace AOM.App.Domain.Services
{
    using AOM.App.Domain.Interfaces;
    using AOM.Repository.MySql.Aom;
    using System.Collections.Generic;

    public interface IAomNewsRepository
    {
        long CreateAomNews(IAomNews aomNews);

        bool CreateAomNewsContentStreams(List<NewsContentStreamDto> newsContentStreams);

        bool UpdateAomNews(IAomNews aomNews);

        bool DeleteAomNews(string cmsId);

        NewsDto GetAomNews(string cmsId);

        List<long> GetContentStreamsForArticle(long newsId);

        void PurgeNews(System.DateTime startDate);
    }
}