﻿using AOM.App.Domain.Entities;

namespace AOM.App.Domain.Services
{
    public interface IUserAuthenticationService
    {
        AuthenticationResult AuthenticateUser(string userName, string password, long userLoginSourceId, string ipAddress = "");

        IAuthenticationResult AuthenticateToken(string username, string token);

        IAuthenticationResult AuthenticateSessionToken(string token, string ipAddress);

        //bool RecordUsersIp(string ip, string username, bool successfulLogin, string loginFailureReason);
    }
}
