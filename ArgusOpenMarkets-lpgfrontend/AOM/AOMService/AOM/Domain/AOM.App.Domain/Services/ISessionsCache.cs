﻿using System.Runtime.Caching;

namespace AOM.App.Domain.Services
{
    public interface ISessionsCache<T>
        where T : class
    {
        T GetSessionForUser(string username);

        bool Add(string username, T session, CacheItemPolicy policy = null);

        T Remove(string username);

        bool Contains(string username);

        void Dispose();

        long GetCount();

        CacheItemPolicy GetCacheItemPolicy();
    }
}