﻿using AOM.App.Domain.Interfaces;

namespace AOM.App.Domain.Services
{
    public interface IUserTokenService
    {
        IUserToken GetTokenByUserId(long userId);

        IUserToken InsertNewUserToken(long userId, string username, string ipAddress = "");

        IUserToken GetLoginTokenByGuid(string tokenGuid);

        IUserToken GetSessionTokenByGuid(string tokenGuid);

        bool IsValidLoginTokenForUser(string token, long userId);
    }
}