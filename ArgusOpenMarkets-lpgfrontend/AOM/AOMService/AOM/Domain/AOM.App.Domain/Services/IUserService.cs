﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.Repository.MySql.Crm;

namespace AOM.App.Domain.Services
{
    public interface IUserService
    {
        IUser GetUserWithPrivileges(long userId);
        IUser GetUserWithPrivileges(string username);
        SystemPrivileges GetSystemPrivileges(long userId);
        long GetUserId(string username);
        UserInfoDto GetUserInfoDto(string username);
        List<long?> GetUserContentStreams(long userId);
        bool CheckUserHasContentStreams(long userId, List<long> contentStreamIds);
        bool CheckUserHasContentStreams(List<long?> userContentStreams, List<long> contentStreamsToCheck);
        bool CheckUserHasContentStream(string userName, long contentStreamId);
        IList<long> GetUserOrganisationRoles(long userId);
        IOrganisation GetUsersOrganisation(long userId);
        UserContactDetails GetContactDetails(long userId);
        List<UserLoginSource> GetUserLoginSources();
        List<UserLoginExternalAccount> GetUsersLoginExternalAccounts(long userId);
        bool RecordLoginIpForUser(string ip, string username, bool successfulLogin, string failureReason, long userLoginSourceId);
        IList<User> GetUsers(long organisationId);
        bool CheckUserHasPrivilege(string username, long organisationId, long productId, string priveledge);
        void UserDisconnect(long userId, string disconnectReason);
        bool SaveClientLog(ClientLogging clientLog);
        bool UserEntitledToProduct(long userId, long productId);
    }
}
