﻿namespace AOM.App.Domain.Services
{
    public interface INewsBodyRepository
    {
        bool SaveNewsBody(string cmsId, string newsBody);

        bool UpdateNewsBody(string cmsId, string newsBody);

        bool DeleteNewsBody(string cmsId);

        string GetNewsBody(string cmsId);
    }
}