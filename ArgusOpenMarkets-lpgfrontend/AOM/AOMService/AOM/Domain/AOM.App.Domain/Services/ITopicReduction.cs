﻿using AOM.App.Domain.Entities;
using System.Collections.Generic;

namespace AOM.App.Domain.Services
{
    public interface ITopicReduction
    {
        List<OrganisationPermissionList> ReduceOrganisationTopicCount(IList<BilateralMatrixPermissionPair> brokersOrCounterparties, bool brokerTopics);
    }
}
