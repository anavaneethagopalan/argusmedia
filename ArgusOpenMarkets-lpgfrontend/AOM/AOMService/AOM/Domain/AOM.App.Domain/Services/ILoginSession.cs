﻿using System;
using AOM.App.Domain.Interfaces;

namespace AOM.App.Domain.Services
{
    public interface ILoginSession
    {
        string Username { get; set; }
        int Password { get; set; }
        string IpAddress { get; set; }
        DateTime SessionStarted { get; set; }
        int NumberOfAttempts { get; set; }
        bool IsAlive { get; set; }
        IUser User { get; set; }
    }
}
