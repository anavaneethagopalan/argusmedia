﻿namespace AOM.App.Domain.Services
{
    using AOM.App.Domain.Interfaces;
    using Argus.Transport.Messages.Dtos;
    using System;

    public interface IAomNewsService
    {
        long CreateNews(IAomNews aomNews);

        bool UpdateNews(IAomNews aomNews);

        bool DeleteNews(string cmsId);

        IAomNews TransformNews(PersistedArticle argusNews);

        bool IsAomNewsArticle(PersistedArticle news);

        bool UserEntitledToStory(long newsId, long userId, bool freeYn);

        string GetNewsStory(string cmsId);

        string GetNewsStory(string cmsId, long userId);

        decimal[] GetAomContentStreamIds();

        string[] GetAomContentStreamDescriptions();

        long[] GetArticleContentStreams(PersistedMetaTypeDatum[] contentStreams);

        ulong GetCommodityIdFromContentStreamId(long[] contentStreams);

        string TruncateStory(string story);

        void PurgeNews(DateTime startDate);
    }
}