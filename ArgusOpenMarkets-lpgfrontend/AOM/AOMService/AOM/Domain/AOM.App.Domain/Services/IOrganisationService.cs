﻿namespace AOM.App.Domain.Services
{
    using System.Collections.Generic;

    using Entities;
    using Interfaces;

    public interface IOrganisationService
    {
        OrganisationsWithPermissions GetAllPrincipalsForUsersBrokerage(long userId, long productId);
        OrganisationsWithPermissions GetAllBrokersForUsersOrganisation(long userId, long productId);
        OrganisationsWithPermissions GetPermissionedPrincipalsForUsersBrokerage(long userId, long productId);

        List<OrganisationPermissionSummary> GetPermissionedPrincipalsForUsersBrokerageBuyOrSell(long userId, long productId, OrderType orderType);
        OrganisationsWithPermissions GetPermissionedBrokersForUsersOrganisation(long userId, long productId);

        List<OrganisationPermissionSummary> GetPermissionedBrokersForUsersOrganisationBuyOrSell(long userId, long productId, OrderType orderType);

        List<Organisation> GetCommonPermissionedOrganisations(long userId, long productId, long counterpartyOrgId, long? organisationBrokerageId, BuyOrSell buyOrSell);
        IOrganisation GetOrganisationById(long organistationId);
        IList<Organisation> GetOrganisations();
        IList<OrganisationDetails> GetOrganisationDetails(List<Product> products);
        Organisation GetOrganisationByShortCode(string shortCode);

        IList<IOrganisationRole> GetOrganisationRoles(long organisationId);
        IList<BilateralMatrixPermissionPair> GetAllCounterpartyPermissions();
        IList<BilateralMatrixPermissionPair> GetCounterpartyPermissions(long organisationId);
        IList<BilateralMatrixPermissionPair> GetAllBrokerPermissions();
        IList<BilateralMatrixPermissionPair> GetBrokerPermissions(long organisationId);
        List<MatrixPermission> EditCounterpartyPermission(List<MatrixPermission> newPermissions, long userId);
        //BilateralMatrixPermissionPair EditCounterpartyPermission(MatrixPermission permission);
        //BilateralMatrixPermissionPair EditBrokerPermission(MatrixPermission newPermission);
        List<MatrixPermission> EditBrokerPermission(List<MatrixPermission> newPermissions, long userId);
        List<string> GetEmailContactsForEvent(SystemEventType eventType, long productId, long organisationId);
        bool CanCounterpartiesTrade(long productId, BuyOrSell buyOrSell, long cp1OrganisationId, long cp2OrganisationId);
        bool CanCounterpartyTradeWithBroker(long productId, BuyOrSell buyOrSell, long cp1OrganisationId, long brokerOrganisationId);
        
        /// <summary>
        /// Checks that the supplied organisation's details are valid
        /// </summary>
        /// <param name="organisation">The organisation to validate</param>
        /// <exception cref="BusinessRuleException">If any of the details are invalid</exception>
        void ValidateOrganisation(IOrganisation organisation);

        /// <returns>Returns whether the organisation's details are valid</returns>
        bool IsOrganisationValid(IOrganisation organisation);

       
    }
}