﻿using AOM.App.Domain.Interfaces;

namespace AOM.App.Domain.Services
{
    public interface IEncryptionService
    {
        string Hash(IUser user, string passwordToHash);

        string Hash(long userId, string passwordToHash);

        string Encode(long input);

        long Decode(string input);
    }
}