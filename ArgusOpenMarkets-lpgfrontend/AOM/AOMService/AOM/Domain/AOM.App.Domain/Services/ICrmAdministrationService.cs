﻿using System;

namespace AOM.App.Domain.Services
{
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;

    public interface ICrmAdministrationService
    {
        bool BlockUser(string username, long requestedByUserId);
        bool UnblockUser(string username, long requestedByUserId);
        bool CreateUserWithCredentials(IUser user, UserCredentials userCredentials, long requestedByUserId);
        bool AddRoleToUser(long userId, long roleId, long requestedByUserId);
        bool RemoveRoleForUser(long userId, long roleId, long requestedByUserId);
        bool UpdateUserDetails(long userId, IUser user, long requestedByUserId);
        bool RemoveProductPrivilegeFromSubscribedProductPrivileges(long organisationId, long productId,  long productPrivilegeId, long requestedByUserId);
        bool AddProductPrivilegeToSubscribedProductPrivileges(long organisationId, long productId, long productPrivilegeId, long requestedByUserId);
        bool OrgansiationRoleExists(long organisationId, string name);
        long SaveRole(IOrganisationRole organisationRole, long requestedByUserId);
        bool DeleteOrganisationRole(IOrganisationRole role, long requestedByUserId);
        IList<SubscribedProductPrivilege> GetOrganisationSubscribedProductPrivileges(long organisationId);
        IList<SubscribedProductPrivilege> GetOrganisationSubscribedProductPrivilegesByProduct(long organisationId, long productId);
        IList<ProductRolePrivilege> GetProductRolePrivileges(long roleId);
        int AddProductRolePrivileges(List<ProductRolePrivilege> privileges, long requestedByUserId);
        bool AddProductRolePrivilege(ProductRolePrivilege privilege, long requestedByUserId);
        bool RemoveProductRolePrivilege(ProductRolePrivilege privilege, long requestedByUserId);
        IList<IProductPrivilege> GetProductPrivileges();
        IList<ISystemPrivilege> GetSystemPrivileges();
        IList<ISystemPrivilege> GetSystemPrivileges(OrganisationType organisationType);
        IList<ISystemRolePrivilege> GetSystemRolePrivileges(long roleId);
        bool AddSubscribedProduct(long organisationId, long productId, long requestedByUserId);
        bool RemoveSubscribedProduct(long organisationId, long productId, long requestedByUserId);
        long SaveOrganisation(IOrganisation organisation, long requestedByUserId, IOrganisationService organisationService);
        bool AddSystemPrivilegeToRole(SystemRolePrivilege systemRolePrivilege, long requestedByUserId);
        bool RemoveSystemPrivilegeFromRole(SystemRolePrivilege systemRolePrivilege, long requestedByUserId);
        bool UpdateUserCredentialExpiration(long userId, DateTime newExpirationDate, long requestedByUserId);
    }
}