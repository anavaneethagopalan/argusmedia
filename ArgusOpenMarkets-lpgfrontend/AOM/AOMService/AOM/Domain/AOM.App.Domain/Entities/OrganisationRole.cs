﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class OrganisationRole : IOrganisationRole
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long OrganisationId { get; set; }
    }
}