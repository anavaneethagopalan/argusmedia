﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class TenorCode
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public long LastUpdatedUserId { get; set; }
    }
}