﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class Period
    {
        public long Id { get; set; }
        public string TenorCode { get; set; }
        public string PeriodActionCode { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string RollDate { get; set; }
        //public string RollDateRule { get; set; }
        public long? MinimumDeliveryRange { get; set; }
        public int? RollingPeriodFrom { get; set; }
        public int? RollingPeriodTo { get; set; }
        public string FixedDateFrom { get; set; }
        public string FixedDateTo { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public long LastUpdatedUserId { get; set; }

        public List<PeriodData> PeriodDataDisplay { get; set; }
    }
}
