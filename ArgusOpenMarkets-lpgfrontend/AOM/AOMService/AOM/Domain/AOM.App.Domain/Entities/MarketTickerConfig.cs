namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class MarketTickerConfig
    {
        public long TopLimit { get; set; }

        public long BottomLimit { get; set; }

        public long InsertedOutOfSequence { get; set; }
    }
}