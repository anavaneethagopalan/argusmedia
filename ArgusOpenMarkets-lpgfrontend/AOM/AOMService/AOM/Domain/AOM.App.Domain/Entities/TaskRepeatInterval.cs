﻿namespace AOM.App.Domain.Entities
{
    public enum TaskRepeatInterval
    {
        [DtoMapping("T")] Test,

        [DtoMapping("H")] Hour,

        [DtoMapping("D")] Day,

        [DtoMapping("W")] Week,

        [DtoMapping("M")] Month,

        [DtoMapping("R")] WeekDay
    }
}