﻿namespace AOM.App.Domain.Entities
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class User : IUser
    {
        public long Id { get; set; }

        public string Username { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public long OrganisationId { get; set; }

        public bool IsBlocked { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public string Token { get; set; }

        public string Telephone { get; set; }

        public string Title { get; set; }

        public string ArgusCrmUsername { get; set; }

        public Organisation UserOrganisation { get; set; }

        public List<UserProductPrivilege> ProductPrivileges { get; set; }

        public SystemPrivileges SystemPrivileges { get; set; }
    }
}