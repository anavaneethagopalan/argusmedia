﻿namespace AOM.App.Domain.Entities
{
    using System.ComponentModel;

    /// <summary>
    /// A list of the different types of events that we wish to track in the system.
    /// Note: The Description attribute is important here.  It's what we use to display 
    /// contextual information to the user in the front end.
    /// </summary>
    public enum SystemEventType
    {
        [Description("Deal Confirmation")]
        DealConfirmation = 0,

        [Description("Counterparty Permission Changes")]
        CounterPartyPermissionChanges = 1,

        [Description("Broker Matrix Changes")]
        BrokerMatrixChanges = 2
    }
}