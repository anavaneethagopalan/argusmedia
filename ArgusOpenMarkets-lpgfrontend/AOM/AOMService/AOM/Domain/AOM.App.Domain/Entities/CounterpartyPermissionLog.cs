﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class CounterpartyPermissionLog
    {
        public long Id { get; set; }

        public DateTime LastUpdated { get; set; }

        public long LastUpdatedUserId { get; set; }

        public long OurOrganisation { get; set; }

        public long TheirOrganisation { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public BuyOrSell BuyOrSell { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Permission AllowOrDeny { get; set; }
    }
}