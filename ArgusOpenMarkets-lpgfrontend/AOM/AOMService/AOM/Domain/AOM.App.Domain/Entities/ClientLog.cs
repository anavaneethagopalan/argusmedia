﻿using System;

namespace AOM.App.Domain.Entities
{
    public class ClientLog
    {
        public long UserId { get; set; }

        public DateTime LoginPageLoadedClient { get; set; }
        public DateTime LoginButtonClickedClient { get; set; }
        public DateTime WebSvcAuthenticatedClient { get; set; }
        public DateTime DiffusionAuthenticatedClient { get; set; }
        public DateTime TransitionToDashboardClient { get; set; }
        public DateTime WebSktProdDefJsonReceivedClient { get; set; }
        public DateTime DashboardRenderedClient { get; set; }
    }
}