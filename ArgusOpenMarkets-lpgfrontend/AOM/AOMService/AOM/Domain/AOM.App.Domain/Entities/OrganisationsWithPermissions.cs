using System.Collections.Generic;

namespace AOM.App.Domain.Entities
{
    public class OrganisationsWithPermissions
    {
        public OrganisationsWithPermissions()
        {
            BuyOrganisations = new List<OrganisationPermissionSummary>();
            SellOrganisations = new List<OrganisationPermissionSummary>();
        }

        public List<OrganisationPermissionSummary> BuyOrganisations { get; set; }
        public List<OrganisationPermissionSummary> SellOrganisations { get; set; }
    }
}