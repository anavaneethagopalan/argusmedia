using System;

namespace AOM.App.Domain.Entities
{
    public static class BuyOrSellExtension
    {

        public static BuyOrSell ReverseValue(this BuyOrSell currentState)
        {
            if (currentState == BuyOrSell.None) throw new ArgumentNullException();
            return currentState == BuyOrSell.Buy ? BuyOrSell.Sell : BuyOrSell.Buy;
        }

        public static string ToDtoFormat(this BuyOrSell currentState)
        {
            return DtoMappingAttribute.GetValueFromEnum(currentState);
        }
    }
}