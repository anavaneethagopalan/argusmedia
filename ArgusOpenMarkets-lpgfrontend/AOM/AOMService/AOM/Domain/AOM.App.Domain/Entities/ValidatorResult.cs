namespace AOM.App.Domain.Entities
{
    public enum ValidatorResultState
    {
        Abstain = 0,

        Deny = 1,

        Allow = 2
    }

    public class ValidatorResult
    {
        private ValidatorResult()
        {

        }

        public ValidatorResultState Result { get; set; }

        public bool MarketMustBeOpen { get; set; }

        public string DenyReason { get; set; }

        public static ValidatorResult Allow(bool marketMustBeOpen = false)
        {
            return new ValidatorResult { MarketMustBeOpen = marketMustBeOpen, Result = ValidatorResultState.Allow };
        }

        public static ValidatorResult Deny(string denyReason)
        {
            return new ValidatorResult { Result = ValidatorResultState.Deny, DenyReason = denyReason };
        }

        public static ValidatorResult Abstain()
        {
            return new ValidatorResult { Result = ValidatorResultState.Abstain };
        }
    }
}