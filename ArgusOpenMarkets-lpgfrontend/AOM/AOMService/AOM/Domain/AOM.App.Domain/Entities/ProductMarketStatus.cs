﻿
namespace AOM.App.Domain.Entities
{
    public class ProductMarketStatus
    {
        public MarketStatus Status { get; set; }

        public string ProductName { get; set; }
    }
}
