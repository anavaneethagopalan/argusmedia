﻿using System;

namespace AOM.App.Domain.Entities
{
    public class AomNewsBody
    {
        public string CmsId { get; set; }

        public DateTime PublicationDate { get; set; }
    }
}
