﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using AOM.App.Domain.Interfaces;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class ProductTenor : IProductTenor
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public long LastUpdatedUserId { get; set; }
        //public string RollDate { get; set; }
        //public string RollDateRule { get; set; }
        //public long TenorId { get; set; }
        //public TenorCode ParentTenor { get; set; }
        //public string DeliveryDateStart { get; set; }
        //public string DeliveryDateEnd { get; set; }
        //public long MinimumDeliveryRange { get; set; }

        //private string _defaultStartDate;
        //public string DefaultStartDate
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_defaultStartDate))
        //        {
        //            // If no value then default to the Delivery Date Start.
        //            _defaultStartDate = DeliveryDateStart;
        //        }
        //        return _defaultStartDate;
        //    }

        //    set { _defaultStartDate = value; }
        //}

        //private string _defaultEndDate;
        //public string DefaultEndDate
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_defaultEndDate))
        //        {
        //            // If no value then default to the Delivery Date Start.
        //            _defaultEndDate = DeliveryDateEnd;
        //        }
        //        return _defaultEndDate;
        //    }
        //    set { _defaultEndDate = value; }
        //}

        public Product Product { get; set; }
        //public DeliveryLocation DeliveryLocation { get; set; }
        public List<ProductPriceLine> ProductPriceLines { get; set; }
        public List<ProductTenorPeriod> ProductTenorPeriods { get; set; }
    }
}