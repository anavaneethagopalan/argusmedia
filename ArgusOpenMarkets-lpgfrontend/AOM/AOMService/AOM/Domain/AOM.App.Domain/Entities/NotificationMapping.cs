﻿using System;

namespace AOM.App.Domain.Entities
{
    public class NotificationMapping
    {
        public long Id { get; set; }

        public SystemEventType EventType { get; set; }

        public Product Product { get; set; }

        public String EmailAddress { get; set; }
    }
}