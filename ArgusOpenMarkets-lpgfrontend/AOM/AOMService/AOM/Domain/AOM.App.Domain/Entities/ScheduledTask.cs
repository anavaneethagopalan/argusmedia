﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class ScheduledTask
    {
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string TaskName { get; set; }
        public string TaskType { get; set; }
        public TaskRepeatInterval Interval { get; set; }
        public DateTime NextRun { get; set; }
        public DateTime? LastRun { get; set; }
        public Boolean SkipIfMissed { get; set; }
        public Boolean IgnoreTask { get; set; }

        private Dictionary<string, string> _arguments;
        public T GetArgument<T>(string name)
        {
            return (T)Convert.ChangeType(_arguments[name], typeof(T));
        }

        public string GetArgument(string name)
        {
            return _arguments[name];
        }

        public Dictionary<string, string> Arguments
        {
            set {_arguments = value; }
            get { return _arguments; }
        }
    }
}