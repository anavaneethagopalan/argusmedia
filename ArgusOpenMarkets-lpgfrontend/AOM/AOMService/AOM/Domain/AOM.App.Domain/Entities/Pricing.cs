﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class Pricing
    {
        public decimal Minimum { get; set; }

        public decimal Maximum { get; set; }

        public decimal Increment { get; set; }

        public decimal DecimalPlaces { get; set; }

        public string PricingCurrencyCode { get; set; }

        public string PricingCurrencyDisplayName { get; set; }

        public string PricingUnitName { get; set; }

        public string PricingUnitDescription { get; set; }
    }
}