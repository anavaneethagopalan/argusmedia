﻿namespace AOM.App.Domain.Entities
{
    using System;

    public class Market
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public DateTime? DateCreated { get; set; }
    }
}