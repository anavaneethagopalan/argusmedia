﻿
namespace AOM.App.Domain.Entities
{
    public static class DealPresentation
    {
        public static string ToPresentationVoidReason(string dealVoidReason)
        {
            return string.IsNullOrWhiteSpace(dealVoidReason) ? "None specified" : dealVoidReason;
        }
    }
}