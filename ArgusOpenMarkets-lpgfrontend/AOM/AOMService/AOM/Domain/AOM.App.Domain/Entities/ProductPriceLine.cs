﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class ProductPriceLine
    {
        public long Id { get; set; }
        public long ProductTenorId { get; set; }
        public long? BasisProductId { get; set; }
        public int SequenceNo { get; set; }
        public string Description { get; set; }
        public string PercentageOptionList { get; set; }
        public bool DefaultDisplay { get; set; }

        public List<ProductPriceLineBasisPeriod> ProductPriceLineBasisPeriods { get; set; }
        public List<PriceBasis> PriceBases { get; set; }
    }
}
