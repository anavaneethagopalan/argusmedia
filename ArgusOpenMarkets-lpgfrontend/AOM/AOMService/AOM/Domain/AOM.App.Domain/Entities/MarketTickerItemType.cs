﻿namespace AOM.App.Domain.Entities
{
    public enum MarketTickerItemType
    {
        [DtoMapping("B")]
        Bid = 1,

        [DtoMapping("D")]
        Deal = 2,

        [DtoMapping("I")]
        Info = 3,

        [DtoMapping("A")]
        Ask = 4
    }
}