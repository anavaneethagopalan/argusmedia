﻿using System;

namespace AOM.App.Domain.Entities
{
    public class UserError
    {
        public string ErrorText { get; set; }

        public string Source { get; set; }

        public Exception Error { get; set; }

        public string AdditionalInformation { get; set; }
    }
}
