﻿namespace AOM.App.Domain.Entities
{
    public static class OrganisationTypeExtension
    {
        public static string ToDtoFormat(this OrganisationType currentState)
        {
            return DtoMappingAttribute.GetValueFromEnum(currentState);
        }
    }
}