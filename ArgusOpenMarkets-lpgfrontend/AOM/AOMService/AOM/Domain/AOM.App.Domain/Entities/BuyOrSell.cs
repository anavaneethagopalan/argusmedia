namespace AOM.App.Domain.Entities
{
    public enum BuyOrSell
    {
        [DtoMapping(null)]
        None,

        [DtoMapping("B")]
        Buy,

        [DtoMapping("S")]
        Sell
    }
}