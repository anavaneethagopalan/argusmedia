namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class ProductOpeningTimes
    {
        public string Name { get; set; }

        public long Id { get; set; }

        public TimeSpan? CloseTime { get; set; }

        public TimeSpan? OpenTime { get; set; }
    }
}