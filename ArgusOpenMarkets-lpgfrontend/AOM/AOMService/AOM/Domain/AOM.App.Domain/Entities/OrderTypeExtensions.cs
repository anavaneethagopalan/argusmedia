﻿namespace AOM.App.Domain.Entities
{
    public static class OrderTypeExtensions
    {
        public static OrderType SwapMarketSide(this OrderType t)
        {
            switch (t)
            {
                case OrderType.Bid:
                    return OrderType.Ask;
                case OrderType.Ask:
                    return OrderType.Bid;
                default:
                    return OrderType.None;
            }
        }
    }
}