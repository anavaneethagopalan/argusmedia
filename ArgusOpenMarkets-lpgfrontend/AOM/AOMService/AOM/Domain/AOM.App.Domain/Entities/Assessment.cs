﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class Assessment
    {
        public long Id { get; set; }

        public long EnteredByUserId { get; set; }

        public long LastUpdatedUserId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime LastUpdated { get; set; }

        public decimal? PriceHigh { get; set; }

        public decimal? PriceLow { get; set; }

        public long ProductId { get; set; }

        public Product Product { get; set; }

        public DateTime BusinessDate { get; set; }

        public string TrendFromPreviousAssesment { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AssessmentStatus AssessmentStatus { get; set; }
    }
}