﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class CombinedAssessment
    {
        public Assessment Today { get; set; }

        public Assessment Previous { get; set; }

        public long ProductId { get; set; }
    }
}