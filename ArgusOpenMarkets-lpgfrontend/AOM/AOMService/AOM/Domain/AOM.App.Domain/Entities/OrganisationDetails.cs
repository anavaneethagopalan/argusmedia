﻿namespace AOM.App.Domain.Entities
{
    using System.Collections.Generic;

    public class OrganisationDetails
    {
        public Organisation Organisation { get; set; }

        public List<NotificationMapping> SystemNotifications { get; set; }

        public List<SubscribedProduct> SubscribedProducts { get; set; }
    }
}
