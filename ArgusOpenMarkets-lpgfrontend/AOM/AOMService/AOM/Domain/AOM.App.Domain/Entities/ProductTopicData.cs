﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOM.App.Domain.Entities
{
    public class ProductTopicData
    {
        public long ProductId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ProductTopicType ProductTopicType { get; set; }

        public object Content { get; set; }
    }
}
