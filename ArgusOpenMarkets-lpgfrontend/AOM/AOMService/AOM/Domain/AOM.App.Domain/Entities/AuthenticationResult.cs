﻿using AOM.App.Domain.Interfaces;

namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    using Services;

    [ExcludeFromCodeCoverage]
    public class AuthenticationResult : IAuthenticationResult
    {
        public long UserId { get; set; }
        public bool IsAuthenticated { get; set; }

        public User User { get; set; }

        public string Message { get; set; }

        public string Token { get; set; }

        public string SessionToken { get; set; }

        public bool FirstLogin { get; set; }

        public string FailureReason { get; set; }

        public string CredentialType { get; set; }
    }
}