using AOM.Repository.MySql.Aom;

namespace AOM.App.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class Order
    {
        public long Id { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public string QuantityText { get; set; }
        public long ProductTenorId { get; set; }
        public ProductTenor ProductTenor { get; set; }
        [JsonConverter(typeof (StringEnumConverter))]
        public OrderType OrderType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public long LastUpdatedUserId { get; set; }
        public long? EnteredByUserId { get; set; }
        public string LastUpdatedUserName { get; set; }
        public long? PrincipalUserId { get; set; }
        public string DeliveryPeriod { get; set; }
        public DateTime DeliveryEndDate { get; set; }
        public DateTime DeliveryStartDate { get; set; }
        public long? BrokerId { get; set; }
        public long? BrokerOrganisationId { get; set; }
        public string BrokerOrganisationName { get; set; }
        private string _brokerShortCode;
        public string BrokerShortCode
        {
            get
            {
                if (BrokerRestriction == BrokerRestriction.Any)
                {
                    return "*";
                }

                if (BrokerRestriction == BrokerRestriction.None)
                {
                    return null;
                }

                return _brokerShortCode;
            }

            set
            {
                _brokerShortCode = value;
            }
        }
        public long PrincipalOrganisationId { get; set; }
        public string PrincipalOrganisationName { get; set; }
        public string PrincipalOrganisationShortCode { get; set; }
        [JsonConverter(typeof (StringEnumConverter))]
        public OrderStatus OrderStatus { get; set; }
        [JsonConverter(typeof (StringEnumConverter))]
        public OrderStatus LastOrderStatus { get; set; }
        public string Notes { get; set; }
        [JsonConverter(typeof (StringEnumConverter))]
        public ExecutionMode ExecutionMode { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public Boolean IsVirtual { get; set; }
        [JsonConverter(typeof (StringEnumConverter))]
        public BrokerRestriction BrokerRestriction { get; set; }
        public ExecutionInfo ExecutionInfo { get; set; } // Only ever passed in a client request, not stored.
        public bool CoBrokering { get; set; }
        public MetaData[] MetaData { get; set; }

        public List<OrderPriceLine> OrderPriceLines { get; set; }
    }
}