﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.Script.Serialization;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class OrderPriceLine
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long PriceLineId { get; set; }
        public string OrderStatusCode { get; set; }
        
        public virtual OrderStatus OrderStatus { get; set; }

        //[ScriptIgnore]
        //public virtual Order Order { get; set; }

        public virtual PriceLine PriceLine { get; set; }

        public long LastUpdatedUserId { get; set; }

        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get
            {
                return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc);
            }
            set { _lastUpdated = value; }
        }
    }
}
