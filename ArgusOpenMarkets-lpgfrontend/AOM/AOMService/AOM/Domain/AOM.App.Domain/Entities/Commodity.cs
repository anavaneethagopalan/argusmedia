﻿using System;

namespace AOM.App.Domain.Entities
{
    using AOM.App.Domain.Interfaces;

    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class Commodity : ICommodity
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long EnteredByUserId { get; set; }

        public long LastUpdatedUserId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime LastUpdated { get; set; }
    }
}