﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    using Interfaces;

    [ExcludeFromCodeCoverage]
    [DebuggerDisplay("ProductId = {ProductId} with {Privileges.Count} Privileges")]
    public class UserProductPrivilege : IUserProductPrivilege
    {
        public long ProductId { get; set; }

        public long DisplayOrder { get; set; }

        public string Name { get; set; }

        public bool HasAssessment { get; set; }

        public Dictionary<string, Boolean> Privileges { get; set; }
    }
}