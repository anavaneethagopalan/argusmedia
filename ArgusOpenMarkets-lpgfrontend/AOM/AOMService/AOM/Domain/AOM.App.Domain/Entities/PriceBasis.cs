﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class PriceBasis
    {
        public long Id { get; set; }
        public string ShortName { get; set; }
        public string Name { get; set; }
        public long PriceTypeId { get; set; }
        public string PriceTypeName { get; set; }
        public string PriceUnitCode { get; set; }
        public string PriceUnitDescription { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyDisplayName { get; set; }
        public decimal PriceMinimum { get; set; }
        public decimal PriceMaximum { get; set; }
        public decimal PriceIncrement { get; set; }
        public decimal PriceDecimalPlaces { get; set; }
        public decimal PriceSignificantFigures { get; set; }

        //public PriceType PriceType { get; set; }
        //public Unit PriceUnit { get; set; }
        //public Currency Currency { get; set; }

        public List<Period> PriceBasisPeriods { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class PriceType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
    
}