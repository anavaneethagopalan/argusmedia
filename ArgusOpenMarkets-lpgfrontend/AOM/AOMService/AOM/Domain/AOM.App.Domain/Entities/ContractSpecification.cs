﻿namespace AOM.App.Domain.Entities
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class ContractSpecification
    {
        public Volume Volume { get; set; }
        public List<ProductTenor> ProductTenors { get; set; }
        public string ContractType { get; set; }
    }
}