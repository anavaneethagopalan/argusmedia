﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class Deal
    {
        public long Id { get; set; }

        public long InitialOrderId { get; set; }

        public long MatchingOrderId { get; set; }

        public DateTime? LastUpdated { get; set; }

        public long LastUpdatedUserId { get; set; }

        public long EnteredByUserId { get; set; }

        public string Message { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public DealStatus DealStatus { get; set; }

        public DateTime CreatedDate { get; set; }

        public long CreatedCompanyId { get; set; }

        // We need these objects that comprise a deal.
        public Order Initial { get; set; }

        public Order Matching { get; set; }
    }
}