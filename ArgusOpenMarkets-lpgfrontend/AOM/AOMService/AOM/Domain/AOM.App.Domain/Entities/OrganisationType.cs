﻿namespace AOM.App.Domain.Entities
{
    public enum OrganisationType
    {
        [DtoMapping("")]
        NotSpecified = 1,

        [DtoMapping("A")]
        Argus = 2,

        [DtoMapping("T")]
        Trading = 3,

        [DtoMapping("B")]
        Brokerage = 4
    }
}