﻿using System;
using System.Diagnostics.CodeAnalysis;
using AOM.App.Domain.Interfaces;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class UserLoginExternalAccount : IUserLoginExternalAccount
    {
        public long Id { get; set; }
        public long UserLoginSourceId { get; set; }
        public long UserInfoId { get; set; }
        public string ExternalUserCode { get; set; }
        public string ExternalUserName { get; set; }
        public string ExternalUserEmail { get; set; }
        public string SSOCredential { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDeleted { get; set; }

        public User User { get; set; }
        public UserLoginSource UserLoginSource { get; set; }
    }
}
