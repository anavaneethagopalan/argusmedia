using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]

    [DebuggerDisplay(
        "Products={ProductId} [{BuySide.OurOrganisation},{BuySide.TheirOrganisation},{BuySide.BuyOrSell}-{BuySide.OurPermAllowOrDeny}-{BuySide.TheirPermAllowOrDeny}]" +
        " [{SellSide.OurOrganisation},{SellSide.TheirOrganisation},{SellSide.BuyOrSell}-{SellSide.OurPermAllowOrDeny}-{SellSide.TheirPermAllowOrDeny]")]
    public class BilateralMatrixPermissionPair
    {
        public long OrgId { get; set; }
        public string OrgShortCode { get; set; }
        public string OrgName { get; set; }
        public string OrgLegalName { get; set; }
        public long ProductId { get; set; }
        public long MarketId { get; set; }
        public BilateralMatrixPermission BuySide { get; set; }
        public BilateralMatrixPermission SellSide { get; set; }
        public bool? PermissionUpdated { get; set; }
    }

    public static class BilateralMatrixPairHelpers
    {
        public static bool CanTradeWith(this BilateralMatrixPermissionPair matrixPermissionPair)
        {
            if (matrixPermissionPair.BuySide == null || matrixPermissionPair.SellSide == null)
            {
                return false;
            }

            // TODO: NTB - How can we return CanTrade for an Assymetric Permission based on the logic below?
            // If Broker B can Sell For Organisation O, then surely he can trade  - he might not have permissions to Sell - but he can but for this organisation?  
            // I think this topic should contain: 
            // - Can Buy: True/False
            // - Can Sell: True/False.  
            // Leaving this for now - as we are going to Shim (Peter Taylor) the front end to NO LONGER SUPPORT Assymetric permissions.  

            // In computer programming, a shim is a small library that transparently intercepts API calls and changes the arguments passed, handles the operation itself, 
            // or redirects the operation elsewhere.[1][2] Shims typically come about when the behavior of an API changes, thereby causing compatibility issues for older 
            // applications which still rely on the older functionality. In such cases, the older API can still be supported by a thin compatibility layer on top of the 
            // newer code. Web polyfills are a related concept. Shims can also be used for running programs on different software platforms than they were developed for.

            return matrixPermissionPair.BuySide.OurPermAllowOrDeny.Equals(Permission.Allow) && matrixPermissionPair.SellSide.OurPermAllowOrDeny.Equals(Permission.Allow);
        }
    }
}