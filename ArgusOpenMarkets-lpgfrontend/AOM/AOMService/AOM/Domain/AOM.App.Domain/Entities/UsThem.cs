﻿namespace AOM.App.Domain.Entities
{
    public enum UsThem
    {
        Us = 0,

        Them = 1
    }
}