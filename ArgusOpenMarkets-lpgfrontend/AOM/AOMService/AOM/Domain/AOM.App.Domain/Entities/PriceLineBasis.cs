﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.Script.Serialization;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class PriceLineBasis
    {
        public long Id { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public long LastUpdatedUserId { get; set; }
        public long PriceLineId { get; set; }
        public long PriceBasisId { get; set; }
        public long ProductId { get; set; }
        public string TenorCode { get; set; }
        public string Description { get; set; }
        public int SequenceNo { get; set; }
        public int PercentageSplit { get; set; }
        public decimal PriceLineBasisValue { get; set; }
        public string PricingPeriod { get; set; }
        public DateTime PricingPeriodFrom { get; set; }
        public DateTime PricingPeriodTo { get; set; }

        public PriceBasis PriceBasis { get; set; }
    }
}
