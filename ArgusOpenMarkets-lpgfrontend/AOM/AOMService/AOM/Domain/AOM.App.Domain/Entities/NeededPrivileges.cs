using System.Collections.Generic;

namespace AOM.App.Domain.Entities
{
    public class NeededPrivileges
    {
        public List<string> ForPrincipal;

        public List<string> ForPrincipalOrg;

        public List<string> ForBroker;

        public List<string> ForBrokerOrg;

        public List<string> ForSuperUser;
    }
}