﻿namespace AOM.App.Domain.Entities
{
    public enum CredentialType
    {
        [DtoMapping(null)]
        NotSet,

        [DtoMapping("P")]
        Permanent,

        [DtoMapping("T")]
        Temporary
    }
}
