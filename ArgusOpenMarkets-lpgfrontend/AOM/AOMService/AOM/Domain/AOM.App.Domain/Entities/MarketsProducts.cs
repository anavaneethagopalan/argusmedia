﻿using System;

namespace AOM.App.Domain.Entities
{
    public class MarketsProducts
    {
        public long MarketId { get; set; }
        public string MarketName { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
    }
}
