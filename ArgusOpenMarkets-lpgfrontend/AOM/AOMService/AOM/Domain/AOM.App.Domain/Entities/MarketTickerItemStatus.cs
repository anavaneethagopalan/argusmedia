﻿namespace AOM.App.Domain.Entities
{
    public enum MarketTickerItemStatus
    {
        [DtoMapping("A")]
        Active = 1,

        [DtoMapping("H")]
        Held = 2,

        [DtoMapping("P")]
        Pending = 3,

        [DtoMapping("V")]
        Void = 4,

        [DtoMapping("U")]
        Updated = 5
    }
}