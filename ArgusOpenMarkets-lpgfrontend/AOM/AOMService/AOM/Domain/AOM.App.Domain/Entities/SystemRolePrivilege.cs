﻿namespace AOM.App.Domain.Entities
{
    using AOM.App.Domain.Interfaces;

    public class SystemRolePrivilege : ISystemRolePrivilege
    {
        public long Id { get; set; }

        public long RoleId { get; set; }

        public long PrivilegeId { get; set; }
    }
}