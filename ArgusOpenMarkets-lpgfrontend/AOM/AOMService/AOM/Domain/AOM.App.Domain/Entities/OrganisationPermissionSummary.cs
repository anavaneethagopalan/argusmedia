﻿using AOM.App.Domain.Interfaces;

namespace AOM.App.Domain.Entities
{
    public class OrganisationPermissionSummary {
        public OrganisationMinimumDetails Organisation { get; set; }
        public bool CanTrade { get; set; }
    }


}