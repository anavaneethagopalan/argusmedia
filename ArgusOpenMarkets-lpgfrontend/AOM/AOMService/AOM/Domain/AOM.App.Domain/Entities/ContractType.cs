﻿namespace AOM.App.Domain.Entities
{
    using System;

    using AOM.App.Domain.Interfaces;

    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class ContractType : IContractType
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public DateTime? DateCreated { get; set; }
    }
}