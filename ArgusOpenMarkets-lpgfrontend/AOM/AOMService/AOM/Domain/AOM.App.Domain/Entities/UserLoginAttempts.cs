﻿namespace AOM.App.Domain.Entities
{
    using System; 

    public class UserLoginAttempts
    {
        public long Id { get; set; }
        public long NumFamiledLogins { get; set; }
        public DateTime LastLoginAttemptTime { get; set; }
        public long UserLoginSourceId { get; set; }

        public UserLoginSource UserLoginSource { get; set; }
    }
}