﻿using System.Diagnostics.CodeAnalysis;
using AOM.App.Domain.Interfaces;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class UserLoginSource : IUserLoginSource
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long OrganisationId { get; set; }

        public Organisation ExternalSystemOrganisation { get; set; }
    }
}
