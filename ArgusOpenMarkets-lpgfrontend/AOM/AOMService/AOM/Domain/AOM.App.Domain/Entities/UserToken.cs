﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class UserToken : IUserToken
    {
        public long Id { get; set; }

        //this should probably change to user
        public string LoginTokenString { get; set; }

        public long UserInfo_Id_fk { get; set; }

        public string SessionId { get; set; }

        public DateTime? TimeCreated { get; set; }

        public string SessionTokenString { get; set; }

        public string UserIpAddress { get; set; }

        public User User { get; set; }
    }
}