﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using AOM.Repository.MySql.Aom;

using ServiceStack.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class ExternalDeal
    {
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string BuyerName { get; set; }
        public long? BuyerId { get; set; }
        public string SellerName { get; set; }
        public long? SellerId { get; set; }
        public string BrokerName { get; set; }
        public long? BrokerId { get; set; }
        public string DeliveryLocation { get; set; }
        public decimal? Price { get; set; }
        public decimal? Quantity { get; set; }
        public string QuantityText { get; set; }
        public long ProductId { get; set; }
        public string ContractInput { get; set; }
        public long ReporterId { get; set; }
        public DateTime DeliveryStartDate { get; set; }
        public DateTime DeliveryEndDate { get; set; }
        public string FreeFormDealMessage { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public DealStatus DealStatus { get; set; }
        public char VoidReason { get; set; }
        public DateTime LastUpdated { get; set; }
        public long LastUpdatedUserId { get; set; }
        public string Notes { get; set; }
        public bool UseCustomFreeFormNotes { get; set; }
        public bool AsVerified { get; set; }
        public string OptionalPriceDetail { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public DealStatus PreviousDealStatus { get; set; }
        public MetaData[] MetaData { get; set; }
        public long? PriceLineId { get; set; }

        public virtual PriceLine PriceLine { get; set; }
    }
}