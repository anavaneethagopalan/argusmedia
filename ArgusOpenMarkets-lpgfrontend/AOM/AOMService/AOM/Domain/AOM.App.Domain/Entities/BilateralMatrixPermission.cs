using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    [DebuggerDisplay("{OurOrganisation},{TheirOrganisation},{BuyOrSell}-{OurPermAllowOrDeny}-{TheirPermAllowOrDeny}")]
    public class BilateralMatrixPermission
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Permission OurPermAllowOrDeny { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Permission TheirPermAllowOrDeny { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public BuyOrSell BuyOrSell { get; set; }

        public long OurOrganisation { get; set; }
        public long TheirOrganisation { get; set; }
        public DateTime? OurLastUpdated { get; set; }
        public DateTime? TheirLastUpdated { get; set; }
        public string LastUpdatedByUser { get; set; }
    }
}