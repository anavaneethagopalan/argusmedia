﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class Volume
    {
        public decimal Default { get; set; }
        public decimal Minimum { get; set; }
        public decimal Maximum { get; set; }
        public decimal Increment { get; set; }
        public decimal DecimalPlaces { get; set; }
        public string VolumeUnitCode { get; set; }
        //public List<decimal> CommonQuantities { get; set; }
        public List<CommonQuantityValues> CommonQuantityValues { get; set; }
        public string VolumeUnitName { get; set; }
        public string VolumeUnitDescription { get; set; }
    }
}