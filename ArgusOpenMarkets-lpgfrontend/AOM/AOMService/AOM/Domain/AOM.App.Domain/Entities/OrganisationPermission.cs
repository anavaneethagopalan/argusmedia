﻿using System;

namespace AOM.App.Domain.Entities
{
    public class OrganisationPermission
    {
        public long OrgId { get; set; }                     //Their org Id
        public string BuyOd { get; set; }                   //Buy our decision
        public DateTime? BuyOdLastUpdated { get; set; }     //Buy our decision last updated date
        public string BuyUpdateUser { get; set; }           //Buy Update User 
        public string BuyTd { get; set; }                   //Buy their decision
        public DateTime? BuyTdLastUpdated { get; set; }     //Buy their decision last updated date
        public string SellOd { get; set; }                  //Sell our decision
        public DateTime? SellOdLastUpdated { get; set; }    //Sell our decision last updated date
        public string SellUpdateUser { get; set; }          //Sell Update User 
        public string SellTd { get; set; }                  //Sell their decision
        public DateTime? SellTdLastUpdated { get; set; }    //Sell their decision last updated date
    }
}
