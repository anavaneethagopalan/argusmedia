﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class ProductTenorPeriod
    {
        public long ProductTenorId { get; set; }
        public long DeliveryPeriodId { get; set; }
        public long DefaultPeriodId { get; set; }

        public Period DeliveryPeriod { get; set; }
        public Period DefaultPeriod { get; set; }
    }
}
