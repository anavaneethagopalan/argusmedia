﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class DealEmailNotification
    {
        public long Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateSent { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public EmailStatus Status { get; set; }

        public long DealId { get; set; }

        public DateTime DealDate { get; set; }

        public DateTime DealAuthorisationDate { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public DealStatus DealStatus { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public DateTime? DeliveryStartDate { get; set; }

        public DateTime? DeliveryEndDate { get; set; }

        public string BuyerCompanyName { get; set; }

        public string SellerCompanyName { get; set; }

        public string BuyerBrokerOrganisationName { get; set; }

        public string SellerBrokerOrganisationName { get; set; }

        public string ProductName { get; set; }

        public string Currency { get; set; }

        public string Units { get; set; }

        public string PriceUnits { get; set; }

        public string Recipient { get; set; }

        public string DealNotes { get; set; }

        public string DealVoidReason { get; set; }

        public MetaData[] OrderMetaData { get; set; }

        public string DeliveryPeriodLabel { get; set; }

        public string DeliveryLocationLabel { get; set; }
    }
}