﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class UserCredentials
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public string PasswordHashed { get; set; }

        public CredentialType CredentialType { get; set; }

        public DateTime Expiration { get; set; }

        public DateTime? DateCreated { get; set; }

        public bool FirstTimeLogin { get; set; }

        public int DefaultExpirationInMonths { get; set; }

        public User User { get; set; }
    }
}