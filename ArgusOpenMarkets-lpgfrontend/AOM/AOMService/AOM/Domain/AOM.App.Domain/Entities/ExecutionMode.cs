namespace AOM.App.Domain.Entities
{
    public enum ExecutionMode
    {
        [DtoMapping(null)]
        None,

        [DtoMapping("I")]
        Internal,

        [DtoMapping("E")]
        External
    }
}