﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    using Interfaces;

    [ExcludeFromCodeCoverage]
    [DebuggerDisplay("{ProductId},{Name}")]
    public class Product : IProduct
    {
        public long ProductId { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsInternal { get; set; }
        public DateTime LastUpdated { get; set; }
        public long LastUpdatedUserId { get; set; }
        public string LastUpdatedUserName { get; set; }
        public long CommodityId { get; set; }
        public long MarketId { get; set; }
        public long ContractTypeId { get; set; }
        //public long LocationId { get; set; }
        public List<DeliveryLocation> DeliveryLocations { get; set; }
        //public string CurrencyCode { get; set; }
        public string VolumeUnitCode { get; set; }
        //public string PriceUnitDisplayName { get; set; }
        //public string PriceUnitCode { get; set; }
        //public string CurrencyDisplayName { get; set; }
        public string VolumeUnitDisplayName { get; set; }
        public List<UserProductPrivilege> UserProductPrivileges { get; set; }
        public int BidAskStackNumberRows { get; set; }
        public string BidAskStackStyleCode { get; set; }
        public MarketStatus Status { get; set; }
        public bool IgnoreAutomaticMarketStatusTriggers { get; set; }
        public bool AllowNegativePriceForExternalDeals { get; set; }
        public bool DisplayOptionalPriceDetail { get; set; }
        public DateTime? LastOpenDate { get; set; }
        public DateTime? LastCloseDate { get; set; }
        public TimeSpan? OpenTime { get; set; }
        public TimeSpan? CloseTime { get; set; }
        public DateTime? PurgeDate { get; set; }
        public TimeSpan? PurgeTimeOfDay { get; set; }
        public int PurgeFrequency { get; set; }
        public bool HasAssessment { get; set; }
        //public string LocationLabel { get; set; }
        public string DeliveryPeriodLabel { get; set; }
        public int DisplayOrder { get; set; }
        public bool CoBrokering { get; set; }
        public bool OrderShowNotes { get; set; }
        public bool ExternalDealShowNotes { get; set; }
        public bool DisplayContractName { get; set; }
    }
}