﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class UserModule
    {
        public long Id { get; set; }

        public long UserName { get; set; }

        public long ModuleId { get; set; }

        public Module Module { get; set; }
    }
}