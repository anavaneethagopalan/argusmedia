﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class SubscribedProductPrivilege : ISubscribedProductPrivilege
    {
        public long ProductId { get; set; }

        public long OrganisationId { get; set; }

        public long ProductPrivilegeId { get; set; }

        public DateTime DateCreated { get; set; }

        public string CreatedBy { get; set; }

        public ProductPrivilege ProductPrivilege { get; set; }
    }
}