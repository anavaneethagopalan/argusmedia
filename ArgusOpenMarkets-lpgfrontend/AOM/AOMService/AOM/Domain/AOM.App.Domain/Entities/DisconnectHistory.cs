﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class DisconnectHistory
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public string DisconnectReason { get; set; }

        public DateTime DateCreated { get; set; }
    }
}