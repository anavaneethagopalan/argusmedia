﻿using System.Collections.Generic;

namespace AOM.App.Domain.Entities
{
    public class TradingMatrixUpdateNotification
    {
        public List<MatrixPermission> MatrixPermissions { get; set; }
        public SystemEventType BrokerOrCounterpartyMatrixUpdate { get; set; }
    }
}