﻿using System;

namespace AOM.App.Domain.Entities
{
    public static class PermissionExtension
    {

        public static Permission ReverseValue(this Permission currentState)
        {
            if (currentState == Permission.NotSet) throw new ArgumentNullException();
            return currentState == Permission.Allow ? Permission.Deny : Permission.Allow;
        }

        public static string ToDtoFormat(this Permission currentState)
        {
            return DtoMappingAttribute.GetValueFromEnum(currentState);
        }
    }
}