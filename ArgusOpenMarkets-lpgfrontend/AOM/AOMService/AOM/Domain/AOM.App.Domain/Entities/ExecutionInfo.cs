using System.Diagnostics;

namespace AOM.App.Domain.Entities
{
    [DebuggerDisplay("BrokerOrganisationId = {BrokerOrganisationId} , PrincipalOrganisationId = {PrincipalOrganisationId}")]
    public class ExecutionInfo
    {        
        /// <summary>
        /// The broker organisation executing the order (on behalf of the principal agressor)
        /// </summary>
        public long? BrokerOrganisationId { get; set; }

        /// <summary>
        /// The agressing principal organisation
        /// </summary>
        public long PrincipalOrganisationId { get; set; }
    }
}