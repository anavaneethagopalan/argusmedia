﻿using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class ProductPriceLineBasisPeriod
    {
        public long ProductPriceLineId { get; set; }
        public long PriceBasisId { get; set; }
        public long PricePeriodId { get; set; }
        public int DisplayOrder{ get; set; }

        public PriceBasis PriceBasis { get; set; }
        public Period Period { get; set; }
    }
}
