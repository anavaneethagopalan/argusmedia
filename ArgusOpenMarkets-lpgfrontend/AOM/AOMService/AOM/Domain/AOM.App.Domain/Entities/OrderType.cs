namespace AOM.App.Domain.Entities
{
    public enum OrderType
    {
        [DtoMapping(null)]
        None,

        [DtoMapping("B")]
        Bid,

        [DtoMapping("A")]
        Ask
    }
}