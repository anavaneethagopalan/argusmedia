﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class Currency : ICurrency
    {
        public string Code { get; set; }

        public string DisplayName { get; set; }

        public string Name { get; set; }

        public DateTime? DateCreated { get; set; }
    }
}