﻿namespace AOM.App.Domain.Entities
{
    public enum AssessmentStatus
    {
        [DtoMapping("n")]
        None = 0,

        [DtoMapping("C")]
        Corrected = 1,

        [DtoMapping("R")]
        RunningVwa = 2,

        [DtoMapping("F")]
        Final = 3
    }
}