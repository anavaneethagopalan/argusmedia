﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class DeliveryLocation : IDeliveryLocation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string LocationLabel { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}