namespace AOM.App.Domain.Entities
{
    public enum EmailStatus
    {
        [DtoMapping("P")]
        Pending = 1,

        [DtoMapping("S")]
        Sent = 2,
    }
}