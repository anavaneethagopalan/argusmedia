﻿namespace AOM.App.Domain.Entities
{
    using AOM.App.Domain.Interfaces;

    public static class OrganisationHelpers
    {
        public static OrganisationMinimumDetails MinimumDetails(this Organisation organisation)
        {
            return new OrganisationMinimumDetails
                   {
                       Id = organisation.Id,
                       LegalName = organisation.LegalName,
                       Name = organisation.Name,
                       OrganisationType = organisation.OrganisationType,
                       ShortCode = organisation.ShortCode,
                       BrokerRestriction = BrokerRestriction.Specific
                   };
        }
    }
}