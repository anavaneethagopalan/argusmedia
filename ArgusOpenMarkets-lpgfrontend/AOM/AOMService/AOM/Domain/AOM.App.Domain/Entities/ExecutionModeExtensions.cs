﻿using System;

namespace AOM.App.Domain.Entities
{
    public static class ExecutionModeExtensions
    {
        public static string RequiredPrivilege(this ExecutionMode executionMode)
        {
            switch (executionMode)
            {
                case ExecutionMode.External:
                    return "ExternalOrder_Amend";
                case ExecutionMode.Internal:
                    return "View_BidAsk_Widget";
                case ExecutionMode.None:
                    return string.Empty;
                default:
                    throw new ArgumentOutOfRangeException("executionMode",
                                                          executionMode,
                                                          "Invalid executionMode for order");
            }
        }
    }
}
