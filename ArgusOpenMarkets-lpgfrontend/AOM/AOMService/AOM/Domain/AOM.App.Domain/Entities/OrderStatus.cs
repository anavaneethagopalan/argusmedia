namespace AOM.App.Domain.Entities
{
    public enum OrderStatus
    {
        [DtoMapping(null)]
        None = 1,

        [DtoMapping("A")]
        Active = 2,

        [DtoMapping("H")]
        Held = 3,
        // Pending = 4,
        [DtoMapping("V")]
        VoidAfterExecuted = 5,

        [DtoMapping("E")]
        Executed = 6,

        [DtoMapping("K")]
        Killed = 7
    }
}