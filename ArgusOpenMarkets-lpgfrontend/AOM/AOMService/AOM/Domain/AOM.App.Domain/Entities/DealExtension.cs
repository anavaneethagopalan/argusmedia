﻿namespace AOM.App.Domain.Entities
{
    public static class DealExtension
    {
        public static string GetDealVoidReason(this Deal deal)
        {
            if (string.IsNullOrWhiteSpace(deal.Message))
            {
                return string.Empty;
            }
            var dealNotes = deal.Initial != null ? (deal.Initial.Notes ?? "") : string.Empty;
            return deal.Message.Substring(dealNotes.Length).TrimStart(' ', '\n', '.');
        }
    }
}