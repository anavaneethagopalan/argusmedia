﻿namespace AOM.App.Domain.Entities
{
    public enum MarketInfoStatus
    {
        [DtoMapping("A")]
        Active = 1,

        [DtoMapping("U")]
        Updated = 2,

        [DtoMapping("P")]
        Pending = 3,

        [DtoMapping("V")]
        Void = 4
    }
}