﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class AuthenticationHistory
    {
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string IpAddress { get; set; }
        public string Username { get; set; }
        public bool SuccessfulLogin { get; set; }
        public string LoginFailureReason { get; set; }
        public long UserLoginSourceId { get; set; }

        public UserLoginSource UserLoginSource { get; set; }
    }
}
