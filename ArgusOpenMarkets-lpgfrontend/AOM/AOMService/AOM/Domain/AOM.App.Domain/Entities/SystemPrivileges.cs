﻿namespace AOM.App.Domain.Entities
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class SystemPrivileges
    {
        public List<string> Privileges { get; set; }

        public string DefaultRoleGroup { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public OrganisationType OrganisationType { get; set; }
    }
}