﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class ClientTopicSubscribe
    {
        public string TopicSubscribe { get; set; }

        public string TopicUnsubscribe { get; set; }
    }
}