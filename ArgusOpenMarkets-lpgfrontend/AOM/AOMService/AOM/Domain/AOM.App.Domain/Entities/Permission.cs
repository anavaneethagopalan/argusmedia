﻿namespace AOM.App.Domain.Entities
{
    public enum Permission
    {
        [DtoMapping(null)]
        NotSet,

        [DtoMapping("A")]
        Allow,

        [DtoMapping("D")]
        Deny
    }
}