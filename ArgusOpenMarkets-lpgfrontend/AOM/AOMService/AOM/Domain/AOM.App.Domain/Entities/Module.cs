﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class Module
    {
        public long Id { get; set; }

        public string Description { get; set; }
    }
}