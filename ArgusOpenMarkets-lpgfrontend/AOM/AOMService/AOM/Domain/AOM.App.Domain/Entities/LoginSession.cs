﻿namespace AOM.App.Domain.Entities
{
    using System;

    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;

    public class LoginSession : ILoginSession
    {
        public string Username { get; set; }

        public int Password { get; set; }

        public string IpAddress { get; set; }

        public DateTime SessionStarted { get; set; }

        public int NumberOfAttempts { get; set; }

        public bool IsAlive { get; set; }

        public IUser User { get; set; }

        public override int GetHashCode()
        {
            int hash = 11;
            hash = hash * 17 + Username.GetHashCode();
            hash = hash * 17 + IpAddress.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj)
        {
            var session = obj as LoginSession;
            if (obj == null)
            {
                return false;
            }
            return Username == session.Username; // && IpAddress == session.IpAddress;
            //&& SessionStarted == session.SessionStarted;
        }
    }
}