﻿using System;
using System.Reflection;

namespace AOM.App.Domain.Entities
{
    public class DtoMappingAttribute : Attribute
    {
        private readonly string _databaseString;

        public DtoMappingAttribute(string databaseString)
        {
            _databaseString = databaseString;
        }

        public static string GetValueFromEnum(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi != null)
            {
                var attributes = (DtoMappingAttribute[])fi.GetCustomAttributes(typeof(DtoMappingAttribute), false);
                if (attributes.Length > 0) return attributes[0]._databaseString;
            }
            throw new Exception("Missing mapping for '" + value + "' within enum " + value.GetType().Name);
        }

        public static T FindEnumWithValue<T>(string stringValueToFind)
            where T : struct, IComparable, IFormattable, IConvertible
        {
            var values = Enum.GetValues(typeof(T));
            foreach (var value in values)
            {
                if (GetValueFromEnum((Enum)value) == stringValueToFind)
                {
                    return (T)value;
                }
            }

            throw new Exception("Missing mapping for '" + stringValueToFind + "' within enum " + typeof(T).Name);
        }
    }
}