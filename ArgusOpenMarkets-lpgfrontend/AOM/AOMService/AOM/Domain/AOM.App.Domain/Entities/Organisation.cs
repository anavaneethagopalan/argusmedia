﻿using System;
using System.Diagnostics.CodeAnalysis;
using AOM.App.Domain.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class Organisation : IOrganisation
    {
        public long Id { get; set; }

        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public OrganisationType OrganisationType { get; set; }

        public string ShortCode { get; set; }

        public string LegalName { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public DateTime? DateCreated { get; set; }

        public string Description { get; set; }

        public bool IsDeleted { get; set; }
    }
}