﻿using System.Collections;
using System.Collections.Generic;

namespace AOM.App.Domain.Entities
{
    public class UserUpdatedDetails
    {
        public string Id { get; set; }
        public string Username { get; set; }
    }

    public class PermittedTradingOrg
    {
        public long Id { get; set; }
        public string ShortCode { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }
    }

    public class OrganisationPermissionList
    {
        public long ProductId { get; set; }
        public long MarketId { get; set; }
        public List<OrganisationPermission> Bps { get; set; }
        public List<UserUpdatedDetails> UserDetails { get; set; }
        public List<PermittedTradingOrg> PermittedTradingOrgs { get; set; }

        public OrganisationPermissionList()
        {
            Bps = new List<OrganisationPermission>();
            UserDetails = new List<UserUpdatedDetails>();
            PermittedTradingOrgs = new List<PermittedTradingOrg>();
        }
    }
}