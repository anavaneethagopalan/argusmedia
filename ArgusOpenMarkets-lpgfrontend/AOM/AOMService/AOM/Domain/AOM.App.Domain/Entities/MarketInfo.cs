﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class MarketInfo
    {
        public long Id { get; set; }

        public string Info { get; set; }

        public DateTime LastUpdated { get; set; }

        public long LastUpdatedUserId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public MarketInfoStatus MarketInfoStatus { get; set; }

        public long OwnerOrganisationId { get; set; }

        public List<long> ProductIds { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public MarketInfoType MarketInfoType { get; set; }
    }
}