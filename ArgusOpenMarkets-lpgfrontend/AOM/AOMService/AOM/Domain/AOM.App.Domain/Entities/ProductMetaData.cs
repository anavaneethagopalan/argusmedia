﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using AOM.Repository.MySql.Aom;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ServiceStack.Text;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    [DebuggerDisplay("Product={ProductId},Fields={Fields.Length}")]
    public class ProductMetaData
    {
        public long ProductId { get; set; }
        /// <summary>
        /// Metadata fields in display order
        /// </summary>             
        public ProductMetaDataItem[] Fields { get; set; }
    }

    public class MetaDataList
    {
        public MetaDataList() { }

        public MetaDataList(long id, string description, MetaDataListItem<long>[] fields)
        {
            Id = id;
            Description = description;
            FieldList = fields;
        }

        public long Id { get; set; }
        public string Description { get; set; }
        public MetaDataListItem<long>[] FieldList { get; set; }
    }

    [ExcludeFromCodeCoverage]
    [DebuggerDisplay("{Id}:{DisplayName}={ItemValue}")]
    public class MetaDataListItem<T>
    {
        public MetaDataListItem() { }

        public MetaDataListItem(long id, T itemValue, string displayName)
        {
            Id = id;
            DisplayName = displayName;
            ItemValue = itemValue;
        }

        public long Id { get; set; }
        public string DisplayName { get; set; }
        public T ItemValue { get; set; }
    }

    [ExcludeFromCodeCoverage]
    [DebuggerDisplay("ProductId={ProductId},{DisplayName}")]
    public abstract class ProductMetaDataItem
    {
        protected ProductMetaDataItem(MetaDataTypes fieldType)
        {
            FieldType = fieldType;
        }

        public long Id { get; set; }
        public long ProductId { get; set; }
        public string DisplayName { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public MetaDataTypes FieldType { get; protected set; }
    }

    [ExcludeFromCodeCoverage]
    [DebuggerDisplay("ProductMetaDataItemEnum FieldCount={MetadataList.FieldList.Length}")]
    public class ProductMetaDataItemEnum : ProductMetaDataItem
    {
        static ProductMetaDataItemEnum()
        {
            // ReSharper disable once UnusedMember.Local
            JsConfig<ProductMetaDataItemEnum>.IncludeTypeInfo = true;
        }
        public ProductMetaDataItemEnum() : base(MetaDataTypes.IntegerEnum) { }

        public MetaDataList MetadataList { get; set; }
    }

    [ExcludeFromCodeCoverage]
    [DebuggerDisplay("ProductMetaDataItemString")]
    public class ProductMetaDataItemString : ProductMetaDataItem
    {
        static ProductMetaDataItemString()
        {
            // ReSharper disable once UnusedMember.Local
            JsConfig<ProductMetaDataItemString>.IncludeTypeInfo = true;
        }
        public ProductMetaDataItemString() : base(MetaDataTypes.String) { }

        public int ValueMinimum { get; set; }
        public int ValueMaximum { get; set; }
    }

}