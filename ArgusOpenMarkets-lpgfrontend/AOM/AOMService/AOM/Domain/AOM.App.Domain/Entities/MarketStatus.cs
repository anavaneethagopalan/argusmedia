﻿namespace AOM.App.Domain.Entities
{
    public enum MarketStatus
    {
        [DtoMapping("C")]
        Closed = 0,

        [DtoMapping("O")]
        Open = 1,
    }
}