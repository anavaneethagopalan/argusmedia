﻿namespace AOM.App.Domain.Entities
{
    using AOM.App.Domain.Interfaces;

    public class SystemPrivilege : ISystemPrivilege
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool ArgusUseOnly { get; set; }

        public string DefaultRoleGroup { get; set; }

        public OrganisationType OrganisationType { get; set; }
    }
}