﻿using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class CommonQuantityValues
    {
        public decimal Quantity { get; set; }
        public string QuantityText { get; set; }
        public int DisplayOrder { get; set; }
        public bool DisplayQuantityText { get; set; }
    }
}
