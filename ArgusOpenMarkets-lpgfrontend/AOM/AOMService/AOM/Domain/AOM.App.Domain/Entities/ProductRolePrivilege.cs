﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class ProductRolePrivilege : IProductRolePrivilege
    {
        public long Id { get; set; }

        public long RoleId { get; set; }

        public long OrganisationId { get; set; }

        public long ProductId { get; set; }

        public long ProductPrivilegeId { get; set; }
    }
}