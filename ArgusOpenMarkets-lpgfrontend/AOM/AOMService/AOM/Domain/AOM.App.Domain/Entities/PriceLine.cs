﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Web.Script.Serialization;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class PriceLine
    {
        public long Id { get; set; }
        public int SequenceNo { get; set; }
        //public DateTime? DateCreated { get; set; }

        public List<PriceLineBasis> PriceLinesBases { get; set; }

        //[ScriptIgnore]
        //public List<OrderPriceLine> OrderPriceLines { get; set; }

        public long LastUpdatedUserId { get; set; }

        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get
            {
                return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc);
            }
            set { _lastUpdated = value; }
        }
    }
}
