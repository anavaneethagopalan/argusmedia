﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class UserContactDetails
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public long OrganisationId { get; set; }
        public string OrganisationName { get; set; }
        public string Telephone { get; set; }
        public string Title { get; set; }
    }
}