﻿using System;

namespace AOM.App.Domain.Entities
{
    public class ErrorLog
    {
        public long Id { get; set; }

        public DateTime DateCreated { get; set; }

        public string Type { get; set; }

        public string Source { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string AdditionalInfo { get; set; }
    }
}