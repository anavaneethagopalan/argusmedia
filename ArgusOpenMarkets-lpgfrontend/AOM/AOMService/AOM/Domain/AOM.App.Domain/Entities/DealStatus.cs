﻿namespace AOM.App.Domain.Entities
{
    public enum DealStatus
    {
        [DtoMapping(null)]
        None = 1,

        [DtoMapping("E")]
        Executed = 2,

        [DtoMapping("P")]
        Pending = 4,

        [DtoMapping("V")]
        Void = 5
    }
}