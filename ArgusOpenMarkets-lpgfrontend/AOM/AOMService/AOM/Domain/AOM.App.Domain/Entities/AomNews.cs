﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class AomNews : IAomNews
    {
        public string CmsId { get; set; }

        public string Headline { get; set; }

        public DateTime PublicationDate { get; set; }

        public string ItemUrl { get; set; }

        public string Story { get; set; }

        public string NewsType { get; set; }

        public bool IsFree { get; set; }

        public long[] ContentStreams { get; set; }

        public ulong CommodityId { get; set; }
    }
}