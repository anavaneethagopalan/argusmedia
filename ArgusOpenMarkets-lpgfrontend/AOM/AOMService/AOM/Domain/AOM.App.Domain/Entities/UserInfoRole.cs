﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    [ExcludeFromCodeCoverage]
    public class UserInfoRole : IUserInfoRole
    {
        public long UserId { get; set; }

        public long RoleId { get; set; }
    }
}