﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using AOM.App.Domain.Interfaces;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class ProductPrivilege : IProductPrivilege
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string DefaultRoleGroup { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public OrganisationType OrganisationType { get; set; }

        public Boolean MarketMustBeOpen { get; set; }
    }
}