using System;
using System.Collections.Generic;

namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class ProductDefinitionItem
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductTitle { get; set; }
        public bool IsInternal { get; set; }
        public bool HasAssessment { get; set; }
        public MarketStatus Status { get; set; }
        public TimeSpan? OpenTime { get; set; }
        public TimeSpan? CloseTime { get; set; }
        public DateTime? LastOpenTime { get; set; }
        public DateTime? LastCloseTime { get; set; }
        public TimeSpan? PurgeTimeOfDay { get; set; }
        public int PurgeFrequency { get; set; }
        public Commodity Commodity { get; set; }
        public List<DeliveryLocation> DeliveryLocations { get; set; }
        public ContractSpecification ContractSpecification { get; set; }
        public int BidAskStackNumberRows { get; set; }
        public string BidAskStackStyle { get; set; }
        public bool AllowNegativePriceForExternalDeals { get; set; }
        public bool DisplayOptionalPriceDetail { get; set; }
        public long[] ContentStreamIds { get; set; }
        public string LocationLabel { get; set; }
        public string DeliveryPeriodLabel { get; set; }
        public int DisplayOrder { get; set; }
        public bool CoBrokering { get; set; }
    }
}