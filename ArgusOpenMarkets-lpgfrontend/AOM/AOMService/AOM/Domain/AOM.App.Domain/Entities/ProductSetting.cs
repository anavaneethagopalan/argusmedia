﻿namespace AOM.App.Domain.Entities
{
    using System.Collections.Generic;

    public class ProductSetting
    {
        public Product Product { get; set; }
        public List<ProductTenor> ProductTenors { get; set; }
    }
}