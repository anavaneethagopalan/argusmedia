﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AOM.App.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class ClientLogging
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long? RunJob { get; set; }
        public string Message { get; set; }

        public DateTime? LoginPageLoadedClient { get; set; }
        public DateTime? LoginButtonClickedClient { get; set; }

        public DateTime? WebSvcAuthenticatedClient { get; set; }

        public DateTime? DiffusionAuthenticatedClient { get; set; }
        public DateTime? WebSktUserJsonReceivedClient { get; set; }
        public DateTime? WebSktProdDefJsonReceivedClient { get; set; }
        public DateTime? WebSktAllJsonReceivedClient { get; set; }

        public DateTime? TransitionToDashboardClient { get; set; }
        public DateTime? DashboardRenderedClient { get; set; }
    }
}
