﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class MatrixPermission
    {
        public long Id { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Permission AllowOrDeny { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public BuyOrSell BuyOrSell { get; set; }

        public long OurOrganisation { get; set; }
        public long TheirOrganisation { get; set; }
        public long? LastUpdatedUserId { get; set; }
        public DateTime? LastUpdated { get; set; }
        public long ProductId { get; set; }
        public bool UpdateSuccessful { get; set; }
        public string UpdateError { get; set; }
    }
}