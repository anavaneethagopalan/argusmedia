﻿namespace AOM.App.Domain.Entities
{
    public enum ProductTopicType
    {
        Assessment, 
        ContentStream,
        Definition,
        MarketStatus,
        MetaData
    }
}
