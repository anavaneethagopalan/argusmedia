﻿namespace AOM.App.Domain.Entities
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class AomUserModuleViewEntity
    {
        public string UserName { get; set; }

        public long ModuleId { get; set; }

        public string Description { get; set; }

        public string Stream { get; set; }

        public long? StreamId { get; set; }

        public long? UserOracleCrmId { get; set; }

        public string TypeName { get; set; }
    }
}