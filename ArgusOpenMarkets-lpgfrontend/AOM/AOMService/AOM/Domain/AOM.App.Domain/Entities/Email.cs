namespace AOM.App.Domain.Entities
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class Email
    {
        public long Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateSent { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public EmailStatus Status { get; set; }

        public string Recipient { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}