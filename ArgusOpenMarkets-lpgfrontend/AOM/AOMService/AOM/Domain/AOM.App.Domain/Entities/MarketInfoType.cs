﻿namespace AOM.App.Domain.Entities
{
    public enum MarketInfoType
    {
        [DtoMapping(null)]
        Unspecified,

        [DtoMapping("I")]
        Info,

        [DtoMapping("B")]
        Bid,

        [DtoMapping("A")]
        Ask,

        [DtoMapping("D")]
        Deal
    }
}