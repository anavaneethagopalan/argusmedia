﻿namespace AOM.App.Domain.Entities
{
    using System;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class ForgotPasswordEmailNotification
    {
        private string _url;

        public long Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateSent { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public EmailStatus Status { get; set; }

        public string Url
        {
            get
            {
                var hashIndex = _url.IndexOf('#');
                return (hashIndex > 0) ? _url.Substring(0, hashIndex + 1) : string.Format("{0}/#", _url.TrimEnd('/'));
            }
            set
            {
                _url = value;
            }
        }

        public string Password { get; set; }

        public string Recipient { get; set; }
    }
}