﻿namespace AOM.App.Domain.Entities
{
    public class SubscribedProduct
    {
        public long ProductId { get; set; }

        public bool IsInternal { get; set; }

        public string Name { get; set; }
    }
}