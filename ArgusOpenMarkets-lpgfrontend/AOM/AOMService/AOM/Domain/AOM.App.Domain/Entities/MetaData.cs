﻿using AOM.Repository.MySql.Aom;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOM.App.Domain.Entities
{
    public class MetaData
    {
        public long ProductMetaDataId { get; set; }
        public string DisplayName { get; set; }
        public string DisplayValue { get; set; }
        public int? ItemValue { get; set; }
        public long? MetaDataListItemId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public MetaDataTypes ItemType { get; set; }
    }
}