﻿namespace AOM.App.Domain.Entities
{
    public enum BrokerRestriction
    {
        [DtoMapping("A")]
        Any = 1,

        [DtoMapping("N")]
        None,

        [DtoMapping("S")]
        Specific
    }
}