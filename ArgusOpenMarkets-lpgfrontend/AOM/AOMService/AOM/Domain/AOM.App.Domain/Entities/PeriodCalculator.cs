﻿using System;
using System.Collections.Generic;

//using Utils.Logging.Utils;

namespace AOM.App.Domain.Entities
{
    public class PeriodData
    {
        public string DisplayText { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }

    public static class PeriodCalculator
    {
        public static List<PeriodData> GetPeriodDisplayList(Period period, DateTime today)
        {
            TenorCodes periodTenorCode;
            if (!Enum.TryParse(period.TenorCode, out periodTenorCode))
                throw new Exception("Error unknown TenorCode encountered: " + period.TenorCode);

            PeriodActionCodes periodActionCode;
            if (!Enum.TryParse(period.PeriodActionCode, out periodActionCode))
                throw new Exception("Error unknown PeriodActionCode encountered: " + period.PeriodActionCode);
            
            List<PeriodData> periodData = new List<PeriodData>();

            int rollingPeriodFrom, rollingPeriodTo;
            rollingPeriodFrom = period.RollingPeriodFrom.GetValueOrDefault();
            rollingPeriodTo = period.RollingPeriodTo.GetValueOrDefault();

            //ADD LOGGING/CHECKS IF NULL
            //if (rollingPeriodFrom == 0)
            //if (rollingPeriodTo == 0)
            
            switch (periodTenorCode)
            {
                case TenorCodes.NA:
                    return null;

                case TenorCodes.H:
                    switch (periodActionCode)
                    {
                        case PeriodActionCodes.R:
                            periodData.Add(new PeriodData()
                            {
                                DateStart = today.AddHours(rollingPeriodFrom),
                                DateEnd = today.AddHours(rollingPeriodFrom)
                            });
                            break;
                    }
                    break;

                case TenorCodes.BD:
                    switch (periodActionCode)
                    {
                        case PeriodActionCodes.R:
                            periodData.Add(new PeriodData()
                            {
                                DateStart = AddBusinessDays(today, rollingPeriodFrom),
                                DateEnd = AddBusinessDays(today, rollingPeriodTo)
                            });
                            break;
                    }
                    break;

                case TenorCodes.D:
                    switch (periodActionCode)
                    {
                        case PeriodActionCodes.R:
                            periodData.Add(new PeriodData()
                            {
                                DateStart = today.AddDays(rollingPeriodFrom),
                                DateEnd = today.AddDays(rollingPeriodTo)
                            });
                            break;
                    }
                    break;
                case TenorCodes.WE:
                    break;

                case TenorCodes.WK:
                    break;

                case TenorCodes.BoW:
                    break;
                    
                case TenorCodes.HM:
                case TenorCodes.HM28:
                case TenorCodes.HM29:
                case TenorCodes.HM30:
                case TenorCodes.HM31:
                    //not sure different HM codes are needed??
                    switch (periodActionCode)
                    {
                        case PeriodActionCodes.RFL:
                            DateTime tempDate = today;
                            int fixedDateFrom, fixedDateTo, rollDate;
                            int.TryParse(period.FixedDateFrom, out fixedDateFrom);
                            int.TryParse(period.FixedDateTo, out fixedDateTo);
                            int.TryParse(period.RollDate, out rollDate);
                            //ADD CHECKS IF NULL

                            string tempCode = period.Code;

                            //If after rolling date then need to jump forward two half months - code stays the same
                            if (today.Day >= fixedDateFrom && today.Day > rollDate && today.Day <= fixedDateTo)
                            {
                                tempDate = tempDate.AddMonths(1);
                            }
                            else
                            {
                                //else just move forward one half month, incrementing actual month if moving from 2nd half to 1st half next month
                                tempCode = tempCode == "1-H" ? "2-H" : "1-H";
                                if (tempCode == "1-H")
                                    tempDate = tempDate.AddMonths(1);
                            }

                            periodData.AddRange(AddLpgHalfMonths(tempCode, tempDate, period));
                            
                            break;
                    }
                    break;
                    
                case TenorCodes.BoM:
                    periodData.Add(new PeriodData()
                    {
                        DateStart = today.AddDays(1),
                        DateEnd = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month))
                    });
                    break;

                case TenorCodes.BoM_1:
                    periodData.Add(new PeriodData()
                    {
                        DateStart = today,
                        DateEnd = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month))
                    });
                    break;

                case TenorCodes.M:
                    switch (periodActionCode)
                    {
                        case PeriodActionCodes.R:
                            periodData.Add(new PeriodData()
                            {
                                DateStart = today.AddMonths(rollingPeriodFrom),
                                DateEnd = today.AddMonths(rollingPeriodTo)
                            });
                            break;
                        case PeriodActionCodes.RL:
                            DateTime tempDate = new DateTime(today.Year, today.Month, 1);
                            int tempRollingStart = rollingPeriodFrom;
                            while (tempRollingStart <= rollingPeriodTo)
                            {
                                periodData.Add(new PeriodData()
                                {
                                    DateStart = tempDate,
                                    DateEnd = tempDate.AddMonths(1).AddDays(-1),
                                    DisplayText = tempDate.ToString("MMMM")
                                });
                                tempDate.AddMonths(1);
                                tempRollingStart++;
                            }
                            break;
                    }
                    break;

                case TenorCodes.M_BoM:
                    switch (periodActionCode)
                    {
                        case PeriodActionCodes.RL:
                            DateTime tempDate = today;
                            int tempRollingStart = rollingPeriodFrom;
                            while (tempRollingStart <= rollingPeriodTo)
                            {
                                periodData.Add(new PeriodData()
                                {
                                    DateStart = today.AddMonths(rollingPeriodFrom),
                                    DateEnd = today.AddMonths(rollingPeriodTo),
                                    DisplayText = today.AddMonths(tempRollingStart).ToString("MMMM")
                                });
                                tempRollingStart++;
                            }
                            periodData.Add(new PeriodData()
                            {
                                DateStart = today.AddDays(1),
                                DateEnd = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)),
                                DisplayText = "BoM"
                            });
                            break;
                    }
                    break;

                case TenorCodes.QTR:
                    break;

                case TenorCodes.SUM:
                    break;

                case TenorCodes.WIN:
                    break;

                case TenorCodes.Y:
                    switch (periodActionCode)
                    {
                        case PeriodActionCodes.R:
                            periodData.Add(new PeriodData()
                            {
                                DateStart = today.AddYears(rollingPeriodFrom),
                                DateEnd = today.AddYears(rollingPeriodTo)
                            });
                            break;
                    }
                    break;

                default:
                    throw new Exception("Unknown TenorCode: " + period.TenorCode);
            }

            if (periodActionCode == PeriodActionCodes.F)
            {
                periodData.Add(new PeriodData()
                {
                    //need to change if fixed date is not a single integer!
                    DateStart = new DateTime(today.Year, today.Month, int.Parse(period.FixedDateFrom)),
                    DateEnd = new DateTime(today.Year, today.Month, int.Parse(period.FixedDateTo))
                });
            }

            return periodData;
        }

        static DateTime AddBusinessDays(this DateTime dateStart, int businessDays)
        {
            DateTime tempDate = dateStart;
            while (businessDays > 0)
            {
                tempDate = tempDate.AddDays(1);
                if (tempDate.DayOfWeek < DayOfWeek.Saturday && tempDate.DayOfWeek > DayOfWeek.Sunday && !tempDate.IsHoliday())
                    businessDays--;
            }
            return tempDate;
        }

        static List<PeriodData> AddLpgHalfMonths(string code, DateTime date, Period period)
        {
            List<PeriodData> periodData = new List<PeriodData>();
            DateTime tempDate = date;
            int tempPeriodRollingFrom = period.RollingPeriodFrom.GetValueOrDefault();
            int tempPeriodRollingTo = period.RollingPeriodTo.GetValueOrDefault();

            while (tempPeriodRollingFrom <= tempPeriodRollingTo)
            {
                periodData.Add(new PeriodData()
                {
                    DateStart = code == "1-H" ? new DateTime(tempDate.Year, tempDate.Month, 1) : new DateTime(tempDate.Year, tempDate.Month, 16),
                    DateEnd = code == "1-H" ? new DateTime(tempDate.Year, tempDate.Month, 15) : new DateTime(tempDate.Year, tempDate.Month, DateTime.DaysInMonth(tempDate.Year, tempDate.Month)),
                    DisplayText = string.Format("{0} {1}", code, tempDate.ToString("MMM"))
                });
                
                code = code == "1-H" ? "2-H" : "1-H";

                //if changed code to 1-H increment month
                if (code.Contains("1-H"))
                    tempDate = tempDate.AddMonths(1);

                tempPeriodRollingFrom++;
            }
            return periodData;
        }

        static bool IsHoliday(this DateTime originalDate)
        {
            // Add holiday calendars
            return false;
        }
    }

    public enum PeriodActionCodes
    {
        F,
        R,
        RFL,
        RL
    }

    public enum TenorCodes
    {
        BD,
        BoM,
        BoM_1,
        BoW,
        D,
        H,
        HM,
        HM28,
        HM29,
        HM30,
        HM31,
        M,
        M_BoM,
        NA,
        P,
        PM,
        QTR,
        SUM,
        WE,
        WIN,
        WK,
        Y
    }
    
}
