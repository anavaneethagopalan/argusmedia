﻿namespace AOM.App.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [ExcludeFromCodeCoverage]
    public class MarketTickerItem
    {
        public long Id { get; set; }

        public DateTime LastUpdated { get; set; }

        public long LastUpdatedUserId { get; set; }

        public string LastUpdatedUser { get; set; }

        public long CreatedUserId { get; set; }

        public DateTime? CreatedDate { get; set; }

        public long CreatedCompanyId { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public MarketTickerItemStatus MarketTickerItemStatus { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public virtual MarketTickerItemType MarketTickerItemType { get; set; }

        public List<long> ProductIds { get; set; }

        public long? OrderId { get; set; }

        public long? DealId { get; set; }

        public long? ExternalDealId { get; set; }

        public long? MarketInfoId { get; set; }

        public BrokerRestriction? BrokerRestriction { get; set; }

        public List<long> OwnerOrganisations { get; set; }

        public string Notes { get; set; }

        // This field does not need to be persisted to the DB as the data is already in the MarketInfo table
        [JsonConverter(typeof(StringEnumConverter))]
        public MarketInfoType? MarketInfoType { get; set; }

        // This field does not need to be persisted to the DB as we only need it to decide who to send the message to
        [JsonConverter(typeof(StringEnumConverter))]
        public MarketTickerItemStatus? LastMarketTickerItemStatus { get; set; }

        public bool? CoBrokering { get; set; }
    }
}