﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using NUnit.Framework;
using System;

namespace AOM.App.Domain.Tests.Mappers
{
    [TestFixture]
    public class MarketTickerItemExtensionsTests
    {
        private MockAomModel _aomModel;

        [SetUp]
        public void Setup()
        {
            _aomModel = new MockAomModel();
        }

        [Test]
        public void MarketTickerMessageCreatedFromMarketInfoShouldHaveNullMarketInfoType()
        {
            var miDto = new MarketInfoDto
            {
                Id = 1234,
                MarketInfoStatus = "P",
                MarketInfoType = ""
            };
            _aomModel.MarketInfoItems.Add(miDto);
            
            var mtDto = new MarketTickerItemDto
            {
                MarketTickerItemType = "I",
                MarketTickerItemStatus = "P",
                MarketInfoId = miDto.Id
            };

            var mtEntity = mtDto.ToEntity(_aomModel);

            Assert.NotNull(mtEntity);
            Assert.That(mtEntity.MarketInfoType, Is.Null);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void MarketTickerMessageCreatedFromPlusInfoShouldHaveCorrectMarketInfoType(MarketInfoType miType)
        {
            var miDto = new MarketInfoDto
            {
                Id = 1234,
                MarketInfoStatus = "P",
                MarketInfoType = DtoMappingAttribute.GetValueFromEnum(miType)
            };
            _aomModel.MarketInfoItems.Add(miDto);

            var mtDto = new MarketTickerItemDto
            {
                MarketTickerItemType = "I",
                MarketTickerItemStatus = "P",
                MarketInfoId = miDto.Id
            };

            var mtEntity = mtDto.ToEntity(_aomModel);

            Assert.NotNull(mtEntity);
            Assert.NotNull(mtEntity.MarketInfoType);
            Assert.That(mtEntity.MarketInfoType.Value, Is.EqualTo(miType));
        }

        [TestCase]
        public void MarketTickerMessageCreatedFromOrderShouldHaveTheSameValues()
        {
            var mtDto = new MarketTickerItemDto
            {
                Id = 5,
                Name = "CreateOrderMarketTick",
                Message = "NEW:  Alpha Trading bids 1,000t Naphtha CIF NWE - Barges  14-Jun-2016 - 03-Jul-2016 at $25.00/t",
                MarketTickerItemType = "B",
                MarketTickerItemStatus = "A",
                OwnerOrganisationId1 = 1,
                OwnerOrganisationId2 = 2,
                OwnerOrganisationId3 = 3,
                OrderId = 5,
                ProductId = 7,
                Notes = "just a simple note",
                CoBrokering = true,
                BrokerRestriction = "S",
                LastUpdatedUserId = 5,
                DateCreated = new DateTime(2016, 6, 14),
                LastUpdated = new DateTime(2016, 6, 15)
            };

            var mtEntity = mtDto.ToEntity(_aomModel);

            Assert.NotNull(mtEntity);
            Assert.AreEqual(mtDto.Name, mtEntity.Name);
            Assert.AreEqual(mtDto.Message, mtEntity.Message);
            Assert.AreEqual(MarketTickerItemType.Bid, mtEntity.MarketTickerItemType);
            Assert.AreEqual(MarketTickerItemStatus.Active, mtEntity.MarketTickerItemStatus);
            CollectionAssert.AreEqual(new[] { 1, 2, 3 }, mtEntity.OwnerOrganisations);
            Assert.AreEqual(mtDto.OrderId, mtEntity.OrderId);
            CollectionAssert.AreEqual(new[] { mtDto.ProductId }, mtEntity.ProductIds);
            Assert.IsNull(mtEntity.DealId);
            Assert.IsNull(mtEntity.ExternalDealId);
            Assert.AreEqual(mtDto.Notes, mtEntity.Notes);
            Assert.AreEqual(mtDto.CoBrokering, mtEntity.CoBrokering);
            Assert.AreEqual(BrokerRestriction.Specific, mtEntity.BrokerRestriction);
            Assert.AreEqual(mtDto.LastUpdatedUserId, mtEntity.LastUpdatedUserId);
            Assert.AreEqual(mtDto.LastUpdated, mtEntity.LastUpdated);
            Assert.AreEqual(mtDto.DateCreated, mtEntity.CreatedDate);
        }
    }
}
