﻿namespace AOM.App.Domain.Tests.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.Repository.MySql.Aom;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    [TestFixture]
    internal class AomNewsDtoExtensionsTests
    {
        [Test]
        public void ShouldTransfomNewsDomainToDto()
        {
            var publicationDate = DateTime.Now;
            var isFree = true;
            var story = "A long time ago";
            var news = new AomNews
            {
                CmsId = "1",
                CommodityId = 1,
                ContentStreams = new long[] {10},
                Headline = "Headline",
                IsFree = isFree,
                ItemUrl = "http://news.news.com",
                NewsType = "N",
                PublicationDate = publicationDate,
                Story = story
            };

            var newsDto = news.ToDto();

            Assert.That(newsDto, Is.Not.Null);
            Assert.That(newsDto.CmsId, Is.EqualTo("1"));
            Assert.That(newsDto.IsFree, Is.EqualTo(isFree));
            Assert.That(newsDto.PublicationDate, Is.EqualTo(publicationDate));
            Assert.That(newsDto.Story, Is.EqualTo(story));

            Assert.That(newsDto.ContentStreams, Is.Not.Null);
            Assert.That(newsDto.ContentStreams.Count, Is.EqualTo(1));
            Assert.That(newsDto.ContentStreams[0].ContentStreamId, Is.EqualTo(10));
        }

        [Test]
        public void ShouldTransfomNewsdToDomain()
        {
            var pubDate = DateTime.Now;
            var isFree = false;
            var contentStreams = new List<NewsContentStreamDto>();
            contentStreams.Add(new NewsContentStreamDto {ContentStreamId = 100});
            var story = "How the west was won.";

            var newsDto = new NewsDto
            {
                CmsId = "1",
                ContentStreams = contentStreams,
                IsFree = isFree,
                PublicationDate = pubDate,
                Story = story
            };

            var news = newsDto.ToEntity();

            Assert.That(news, Is.Not.Null);
            Assert.That(news.CmsId, Is.EqualTo("1"));
            Assert.That(news.IsFree, Is.EqualTo(isFree));
            Assert.That(news.PublicationDate, Is.EqualTo(pubDate));
            Assert.That(newsDto.Story, Is.EqualTo(story));

            Assert.That(news.ContentStreams, Is.Not.Null);
            Assert.That(newsDto.ContentStreams.Count, Is.EqualTo(1));
            Assert.That(news.ContentStreams[0], Is.EqualTo(100));
        }
    }
}