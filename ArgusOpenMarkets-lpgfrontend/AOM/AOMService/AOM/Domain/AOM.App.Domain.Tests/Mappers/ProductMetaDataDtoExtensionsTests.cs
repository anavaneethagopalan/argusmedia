﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using NUnit.Framework;
using ServiceStack.Text;
using Utils.Javascript.Extension;

namespace AOM.App.Domain.Tests.Mappers
{
    [TestFixture]
    public class ProductMetaDataDtoExtensionsTests
    {
        [Test]
        [TestCase(MetaDataTypes.IntegerEnum, typeof(ProductMetaDataItemEnum))]
        [TestCase(MetaDataTypes.String, typeof(ProductMetaDataItemString))]
        public void ShouldReturnAProductMetaDataDomainFromADtoObject(MetaDataTypes metadataType, Type expectedDataItemType)
        {
            var dto = new ProductMetaDataItemDto
            {
                Id = -1,
                ProductId = 100,
                DisplayName = "fieldA",
                MetaDataTypeId = metadataType
            };

            var productMetaData = dto.ToEntity();

            Assert.That(productMetaData, Is.Not.Null);
            Assert.That(productMetaData.Id, Is.EqualTo(-1));
            Assert.That(productMetaData.ProductId, Is.EqualTo(100));
            Assert.That(productMetaData.DisplayName, Is.EqualTo("fieldA"));
            Assert.AreEqual(expectedDataItemType, productMetaData.GetType());
         }

        [Test]
        public void ProductMetaDataItemSerialisesToJsonRecognisingDerivedTypes()
        {                       
            var productMetaData = new ProductMetaData()
            {ProductId = 100, 
                Fields = new ProductMetaDataItem[2]
                {
                    new ProductMetaDataItemString() { DisplayName = "Qty", Id=20, ProductId = 100 }, 
                    new ProductMetaDataItemEnum() { DisplayName = "PortList", Id = 10, ProductId = 100, MetadataList = new MetaDataList(1, string.Empty, new[]
                    {   
                        new MetaDataListItem<long> { Id = 1, ItemValue = 101, DisplayName = "field1"}, 
                        new MetaDataListItem<long> { Id = 2, ItemValue = 102, DisplayName = "field2"}
                    })}, 
                }};

            JsConfig.ExcludeTypeInfo = true; //AOM Transport sets this by default - so we do 

            var json = productMetaData.ToJson();            
            var deserialisedObject = json.FromJson<ProductMetaData>();

            Assert.IsTrue(typeof(ProductMetaDataItem).IsAbstract, "Must be abstract to support serialisation with types");
            StringAssert.Contains(typeof(ProductMetaDataItemEnum).Name, json, "json to explicitly contain class names to support deserialisation");
            StringAssert.Contains(typeof(ProductMetaDataItemString).Name, json, "json to explicitly contain class names to support deserialisation");

            StringAssert.Contains("PortList", json, "expected properties from derived type to be serialised by json");
            StringAssert.Contains("field1", json, "expected properties from derived type to be serialised by json");
            StringAssert.Contains("101", json, "expected properties from derived type to be serialised by json");
            StringAssert.Contains("field2", json, "expected properties from derived type to be serialised by json");
            StringAssert.Contains("102", json, "expected properties from derived type to be serialised by json");

            StringAssert.Contains("Qty", json, "expected properties from derived type to be serialised by json");

            StringAssert.Contains("IntegerEnum", json, "expected properties from derived type to be serialised by json and enum to be serialised as text not number");

            //When deserialise is the object hierarchy rebuilt
            Assert.AreEqual(2, deserialisedObject.Fields.Length);
            Assert.AreEqual(MetaDataTypes.IntegerEnum, deserialisedObject.Fields[1].FieldType);
            Assert.AreEqual(2, ((ProductMetaDataItemEnum)deserialisedObject.Fields[1]).MetadataList.FieldList.Length);
        }

        [Test]
        public void ProductMetaDataItemSerialisesToJsonWithEnumsAsString()
        {
            var productMetaData = new ProductMetaData()
            {
                ProductId = 100,
                Fields = new ProductMetaDataItem[2]
                {
                    new ProductMetaDataItemString(){DisplayName = "Qty", Id=20, ProductId = 100}, 
                    new ProductMetaDataItemEnum(){DisplayName = "PortList", Id = 10, ProductId = 100, MetadataList = new MetaDataList(1, string.Empty, new[]
                    {   
                        new MetaDataListItem<long> { Id = 1, ItemValue = 101, DisplayName = "field1"}
                    })}, 
                }
            };

            //control client serialises with Newtonsoft
            StringAssert.Contains("IntegerEnum", productMetaData.ToJsonCamelCase(), "expected properties from derived type to be serialised by json and enum to be serialised as text not number");
        }

        [Test]
        public void OrdersFieldsForEnumByDisplayOrder()
        {
            var dto = new ProductMetaDataItemDto
            {
                Id = -1,
                ProductId = 100,
                DisplayName = "fieldA",
                MetaDataTypeId = MetaDataTypes.IntegerEnum,
                MetaDataList = new MetaDataListDto() { MetaDataListItems = new Collection<MetaDataListItemDto>() } 
            };

            dto.MetaDataList.MetaDataListItems.Add(new MetaDataListItemDto() { DisplayOrder = 3, ValueText = "BB" });
            dto.MetaDataList.MetaDataListItems.Add(new MetaDataListItemDto() { DisplayOrder = 4, ValueText = "AA" });
            dto.MetaDataList.MetaDataListItems.Add(new MetaDataListItemDto() { DisplayOrder = 1, ValueText = "CC" });

            var productMetaData = dto.ToEntity() as ProductMetaDataItemEnum;

            Assert.IsNotNull(productMetaData);
            Assert.AreEqual(3, productMetaData.MetadataList.FieldList.Length);

            Assert.AreEqual("CC", productMetaData.MetadataList.FieldList[0].DisplayName);
            Assert.AreEqual("BB", productMetaData.MetadataList.FieldList[1].DisplayName);
            Assert.AreEqual("AA", productMetaData.MetadataList.FieldList[2].DisplayName);
        }

        [Test]
        public void ThrowsExceptionForUnknownMetaDataType()
        {
            var dto = new ProductMetaDataItemDto
            {
                Id = -1,
                ProductId = 100,
                DisplayName = "fieldA",
                MetaDataTypeId = MetaDataTypes.Unknown
            };
            Assert.Throws<Exception>(() => dto.ToEntity());
        }

        [Test]
        public void ShouldReturnMetadataItemDtoFromMetadataEnumItem()
        {
            var metadataItemEntity = new ProductMetaDataItemEnum
            {
                DisplayName = "Delivery Port",
                Id = 1,
                ProductId = 2,
                MetadataList = new MetaDataList(3, string.Empty, new MetaDataListItem<long>[0])
            };

            var metadataItemDto = metadataItemEntity.ToDto();

            Assert.NotNull(metadataItemDto);
            Assert.AreEqual("Delivery Port", metadataItemDto.DisplayName);
            Assert.AreEqual(MetaDataTypes.IntegerEnum, metadataItemDto.MetaDataTypeId);
            Assert.AreEqual(1, metadataItemDto.Id);
            Assert.AreEqual(3, metadataItemDto.MetaDataListId);
            Assert.AreEqual(2, metadataItemDto.ProductId);
            Assert.IsNull(metadataItemDto.ValueMaximum);
            Assert.IsNull(metadataItemDto.ValueMinimum);
            Assert.IsNull(metadataItemDto.MetaDataList);
            Assert.AreEqual(0, metadataItemDto.DisplayOrder);
            Assert.AreEqual(new DateTime(), metadataItemDto.LastUpdated);
            Assert.IsFalse(metadataItemDto.IsDeleted);
        }

        [Test]
        public void ShouldReturnMetadataItemDtoFromMetadataStringItem()
        {
            var metadataItemEntity = new ProductMetaDataItemString
            {
                DisplayName = "Delivery Description",
                Id = 1,
                ProductId = 2,
                ValueMinimum = 1,
                ValueMaximum = 100
            };

            var metadataItemDto = metadataItemEntity.ToDto();

            Assert.NotNull(metadataItemDto);
            Assert.AreEqual("Delivery Description", metadataItemDto.DisplayName);
            Assert.AreEqual(MetaDataTypes.String, metadataItemDto.MetaDataTypeId);
            Assert.AreEqual(1, metadataItemDto.Id);
            Assert.AreEqual(2, metadataItemDto.ProductId);
            Assert.AreEqual(100, metadataItemDto.ValueMaximum);
            Assert.AreEqual(1, metadataItemDto.ValueMinimum);
            Assert.IsNull(metadataItemDto.MetaDataListId);
            Assert.IsNull(metadataItemDto.MetaDataList);
            Assert.AreEqual(0, metadataItemDto.DisplayOrder);
            Assert.AreEqual(new DateTime(), metadataItemDto.LastUpdated);
            Assert.IsFalse(metadataItemDto.IsDeleted);
        }
    }
}

