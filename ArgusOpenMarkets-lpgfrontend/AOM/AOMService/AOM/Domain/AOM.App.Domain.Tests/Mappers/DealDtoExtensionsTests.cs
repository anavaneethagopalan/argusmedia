﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using Moq;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Mappers
{
    [TestFixture]
    public class DealDtoExtensionsTests
    {
        Mock<IOrganisationService> _mockOrganisationService;
        private Mock<IUserService> _mockUserService;

        [SetUp]
        public void Setup()
        {
            _mockOrganisationService = new Mock<IOrganisationService>();
            _mockUserService = new Mock<IUserService>();
        }

        [Test]
        [TestCase("V", DealStatus.Void)]
        [TestCase("E", DealStatus.Executed)]
        [TestCase("P", DealStatus.Pending)]
        public void ShouldConvertDtoToEntitySettingTheStatusToVoid(string dtoDealStatus,
            DealStatus expectedEntityDealStatus)
        {
            var dealDto = new DealDto {DealStatus = dtoDealStatus};
            var entity = dealDto.ToEntity(_mockOrganisationService.Object, _mockUserService.Object, null);

            Assert.That(entity.DealStatus, Is.EqualTo(expectedEntityDealStatus));
        }

        [Test]
        public void ShouldThrowExceptionIfDealDtoHasInvalidDealStatus()
        {
            var dealDto = new DealDto {DealStatus = "Y"};
            var exceptionMessage = string.Empty;

            try
            {
                var entity = dealDto.ToEntity(_mockOrganisationService.Object, _mockUserService.Object, null);
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
            }

            Assert.That(exceptionMessage.StartsWith("GetDomainDealStatus called with invalid status"));
        }
    }
}