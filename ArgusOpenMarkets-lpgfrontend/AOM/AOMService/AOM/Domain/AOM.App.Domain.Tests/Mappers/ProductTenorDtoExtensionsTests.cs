﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Mappers
{
    [TestFixture]
    public class ProductTenorDtoExtensionsTests
    {
        private ProductTenorDto _productTenorDto;
        private ProductTenor _productTenor;

        [SetUp]
        public void Setup()
        {
            var dtNow = DateTime.UtcNow;

            _productTenorDto = new ProductTenorDto
            {
                DateCreated = dtNow,
                //DefaultEndDate = "",
                //DefaultStartDate = "",
                //DeliveryDateStart = "+1d",
                //DeliveryDateEnd = "+10d",
                DisplayName = "Tenor Display Name",
                Id = 1,
                LastUpdated = dtNow
            };

            _productTenor = new ProductTenor
            {
                DateCreated = dtNow,
                //DefaultEndDate = "",
                //DefaultStartDate = "",
                //DeliveryDateStart = "+1d",
                //DeliveryDateEnd = "+10d",
                DisplayName = "Tenor Display Name",
                Id = 1,
                LastUpdated = dtNow
            };
        }

        //[Test]
        //public void ShouldTransformProductTenorDtotoEntitySettingDefaultEndDateToDeliveryDateEndIfNoValue()
        //{
        //    var entity = _productTenorDto.ToEntity();
        //    Assert.That(entity.DefaultEndDate, Is.EqualTo(_productTenorDto.DeliveryDateEnd));
        //}

        //[Test]
        //public void ShouldTransformProductTenorDtotoEntitySettingDefaultStartDateToDeliveryDateStartIfNoValue()
        //{
        //    var entity = _productTenorDto.ToEntity();
        //    Assert.That(entity.DefaultStartDate, Is.EqualTo(_productTenorDto.DeliveryDateStart));
        //}

        //[Test]
        //public void ShouldTransformProductTenorEntityToDtoSettingDefaultStartDateToDeliveryDateStartIfNoValue()
        //{
        //    var dto = _productTenor.ToDto();
        //    Assert.That(dto.DefaultStartDate, Is.EqualTo(_productTenor.DeliveryDateStart));
        //}

        //[Test]
        //public void ShouldTransformProductTenorEntityToDtoSettingDefaultEndDateToDeliveryDateEndIfNoValue()
        //{
        //    var dto = _productTenor.ToDto();
        //    Assert.That(dto.DefaultEndDate, Is.EqualTo(_productTenor.DeliveryDateEnd));
        //}
    }
}
