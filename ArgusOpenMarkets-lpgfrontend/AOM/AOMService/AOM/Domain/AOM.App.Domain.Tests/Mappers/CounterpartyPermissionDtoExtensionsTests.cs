﻿namespace AOM.App.Domain.Tests.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.Repository.MySql.Aom;
    using AOM.Repository.MySql.Crm;

    using NUnit.Framework;

    [TestFixture]
    public class CounterpartyPermissionDtoExtensionsTests
    {
        [Test]
        public void TransformingAnEmptyCounterpartyPermissionDtoShouldReturnNotNullMatrixPermissionDomain()
        {
            var counterpartyPermissionDto = new CounterpartyPermissionDto();
            var matrixPermission = CounterpartyPermissionDtoExtensions.ToEntity(counterpartyPermissionDto);

            Assert.That(matrixPermission, Is.Not.Null);
        }

        [Test]
        public void TransformingMatrixPermissionDomainShouldReturnNotNullCounterPartyPermissionDto()
        {
            var matrixPermission = new MatrixPermission();
            var counterpartyPermissionDto = CounterpartyPermissionDtoExtensions.ToDto(matrixPermission);

            Assert.That(counterpartyPermissionDto, Is.Not.Null);
        }
    }
}