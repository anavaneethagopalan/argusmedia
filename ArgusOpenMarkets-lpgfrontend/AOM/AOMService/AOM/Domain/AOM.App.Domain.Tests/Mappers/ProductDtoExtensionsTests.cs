﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Mappers
{
    [TestFixture]
    public class ProductDtoExtensionsTests
    {
        [Test]
        public void ShouldReturnAProductDtoFromADomainObject()
        {
            const string locationLabel = "Geographic Location";
            const string deliveryPeriodLabel = "Period (Delivery)"; 
            var product = new Product
            {
                ProductId = 1,
                BidAskStackNumberRows = 1,
                DeliveryLocations = new List<DeliveryLocation> { new DeliveryLocation() {LocationLabel = locationLabel}},
                DeliveryPeriodLabel = "Period (Delivery)"
            };

            var productDto = ProductDtoExtensions.ToDto(product, null);

            Assert.That(productDto, Is.Not.Null);
            Assert.That(productDto.DeliveryLocations.FirstOrDefault().LocationLabel, Is.EqualTo(locationLabel));
            Assert.That(productDto.DeliveryPeriodLabel, Is.EqualTo(deliveryPeriodLabel));
            Assert.That(productDto.Id, Is.EqualTo(product.ProductId));
        }

        [Test]
        public void ShouldReturnAProductDomainFromADtoObject()
        {
            const string locationLabel = "Geographic Location";
            const string deliveryPeriodLabel = "Period (Delivery)";
            var productDto = new ProductDto
            {
                Id = 1,
                BidAskStackNumberRows = 1,
                DeliveryLocations = new List<DeliveryLocationDto> { new DeliveryLocationDto() { LocationLabel = locationLabel } },
                DeliveryPeriodLabel = deliveryPeriodLabel, 
                Status = "O"
            };

            Product p = null;
            Product product = productDto.ToEntity(p);

            Assert.That(product, Is.Not.Null);
            Assert.That(product.DeliveryLocations.FirstOrDefault().LocationLabel, Is.EqualTo(locationLabel));
            Assert.That(product.DeliveryPeriodLabel, Is.EqualTo(deliveryPeriodLabel)); 
            Assert.That(product.ProductId, Is.EqualTo(productDto.Id));
        }

        [Test]
        public void ShouldReturnAProductDtoFromADomainObjectContainingCoBrokering()
        {
            const string locationLabel = "Geographic Location";
            const string deliveryPeriodLabel = "Period (Delivery)";
            var product = new Product
            {
                ProductId = 1,
                BidAskStackNumberRows = 1,
                DeliveryLocations = new List<DeliveryLocation> { new DeliveryLocation() { LocationLabel = locationLabel } },
                DeliveryPeriodLabel = "Period (Delivery)", 
                CoBrokering = true
            };

            var productDto = ProductDtoExtensions.ToDto(product, null);

            Assert.That(productDto, Is.Not.Null);
            Assert.That(productDto.DeliveryLocations.FirstOrDefault().LocationLabel, Is.EqualTo(locationLabel));
            Assert.That(productDto.DeliveryPeriodLabel, Is.EqualTo(deliveryPeriodLabel));
            Assert.That(productDto.Id, Is.EqualTo(product.ProductId));
            Assert.That(productDto.CoBrokering, Is.True);
        }
    }
}