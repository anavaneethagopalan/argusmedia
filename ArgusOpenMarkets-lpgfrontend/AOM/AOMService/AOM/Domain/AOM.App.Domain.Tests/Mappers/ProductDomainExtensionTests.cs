﻿namespace AOM.App.Domain.Tests.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.Repository.MySql.Aom;

    using NUnit.Framework;

    [TestFixture]
    class ProductDomainExtensionsTests
    {
        [Test]
        public void ShouldReturnAProductDomainFromADtoObject()
        {
            var productDto = new ProductDto {CoBrokering = true, Id = 1, Status = "O"};

            Product p= null;
            var product = productDto.ToEntity(p);

            Assert.That(product, Is.Not.Null);
            Assert.That(product.CoBrokering, Is.True);
            Assert.That(product.ProductId, Is.EqualTo(1));
            Assert.That(product.Status, Is.EqualTo(MarketStatus.Open));
        }
    }
}