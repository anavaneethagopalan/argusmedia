﻿using System;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Mappers
{
    [TestFixture]
    public class UnitDtoExtensionTests
    {
        [Test]
        public void ShouldConvertDtoToEntity()
        {
            var dto = new UnitDto
            {
                Code = "C",
                DateCreated = null,
                Description = "Desc",
                LastUpdated = DateTime.Now,
                LastUpdatedUserId = 100,
                Name = "Unit1"
            };

            var entity = dto.ToEntity();

            Assert.That(entity.Code, Is.EqualTo("C"));
            Assert.That(entity.Name, Is.EqualTo("Unit1"));
        }
    }
}
