﻿

using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Crm;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Mappers
{
    [TestFixture]
    public class OrganisationDtoExtensionsTests
    {
        [Test]
        public void IsBrokerIsTrueForB()
        {
            Assert.IsTrue(new OrganisationDto { OrganisationType = "B" }.IsBroker());
        }

        [Test]
        public void IsBrokerIsNotTrueForNonB()
        {            
            Assert.IsFalse(new OrganisationDto {OrganisationType = "X"}.IsBroker());
            Assert.IsFalse(new OrganisationDto {OrganisationType = "b"}.IsBroker());
            Assert.IsFalse(new OrganisationDto {OrganisationType = "Y"}.IsBroker());
        }
    }
}

