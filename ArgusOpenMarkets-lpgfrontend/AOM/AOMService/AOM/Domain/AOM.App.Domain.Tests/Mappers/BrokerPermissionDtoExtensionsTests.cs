﻿namespace AOM.App.Domain.Tests.Mappers
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.Repository.MySql.Aom;

    using NUnit.Framework;

    [TestFixture]
    public class BrokerPermissionsDtoExtensionsTests
    {
        [Test]
        public void TransformingAnEmptyBrokerPermissionDtoShouldReturnNotNullMatrixPermissionDomain()
        {
            var matrixPermission = new MatrixPermission();
            var brokerPermissionDto = BrokerPermissionDtoExtensions.ToDto(matrixPermission);

            Assert.That(brokerPermissionDto, Is.Not.Null);
        }

        [Test]
        public void TransformingBrokerPermissionsDtoWithBuyOrSellShouldReturnNotNullMatrixPermissionDomainWithTheCorrectStatusAsAString()
        {
            var assessment = new Assessment();
            assessment.AssessmentStatus = AssessmentStatus.Corrected;
            var assessmentDomain = AssessmentDtoExtensions.ToDto(assessment);

            Assert.That(assessmentDomain, Is.Not.Null);
            Assert.That(assessmentDomain.AssessmentStatus, Is.EqualTo("C"));
        }

        [Test]
        public void TransformingAssessmentDomainWithAssessmentStatusShouldReturnNotNullAssessmentDtoWithTheCorrectStatusAsEnum()
        {
            var assessment = new AssessmentDto();
            assessment.AssessmentStatus = "C";
            var assessmentDto = AssessmentDtoExtensions.ToEntity(assessment);

            Assert.That(assessmentDto, Is.Not.Null);
            Assert.That(assessmentDto.AssessmentStatus, Is.EqualTo(AssessmentStatus.Corrected));
        }
    }
}