using System.Collections.Generic;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Mappers
{
    [TestFixture]
    public class MetaDataListDtoExtensionsTests
    {
        [TestCase]
        public void ShouldReturnMetaDataListFromMetaDataListDto()
        {
            var metaDataListDto = new MetaDataListDto
            {
                Id = 1,
                Description = "Bitumen Grade",
                MetaDataListItems = new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto {Id = 1, ValueText = "Port A", ValueLong = 100, DisplayOrder = 3, IsDeleted = true},
                    new MetaDataListItemDto {Id = 2, ValueText = "Port B", ValueLong = 101, DisplayOrder = 2},
                    new MetaDataListItemDto {Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1}
                }
            };

            var metadataList = metaDataListDto.ToEntity();

            Assert.NotNull(metadataList);
            Assert.That(metadataList.Id, Is.EqualTo(1));
            Assert.That(metadataList.Description, Is.EqualTo("Bitumen Grade"));
            Assert.That(metadataList.FieldList.Length, Is.EqualTo(2));
            Assert.That(metadataList.FieldList[0].Id, Is.EqualTo(3));
            Assert.That(metadataList.FieldList[0].DisplayName, Is.EqualTo("Port C"));
            Assert.That(metadataList.FieldList[0].ItemValue, Is.EqualTo(102));
            Assert.That(metadataList.FieldList[1].Id, Is.EqualTo(2));
            Assert.That(metadataList.FieldList[1].DisplayName, Is.EqualTo("Port B"));
            Assert.That(metadataList.FieldList[1].ItemValue, Is.EqualTo(101));
        }
    }
}