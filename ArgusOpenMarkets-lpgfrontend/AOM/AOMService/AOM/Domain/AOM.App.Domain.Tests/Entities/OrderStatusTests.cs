﻿using System;
using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class OrderStatusTests
    {
        [Test]
        public void EnsureAllEnumValuesHaveDtoMapping()
        {
            var enumType = typeof (OrderStatus);
            var values = Enum.GetValues(enumType);
            foreach (var value in values)
            {                
                if(value.ToString() == OrderStatus.None.ToString() ) continue;
                var mapping=DtoMappingAttribute.GetValueFromEnum((Enum) value);
                Assert.NotNull(mapping, "Expected '"+value+ "' enum within "+ enumType.Name+ " to have a mapping");
            }                  
        }         
    }
}

