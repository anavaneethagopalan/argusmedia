using System;
using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class PermissionExtensionTests
    {
        [Test]
        public void PermissionFlipsTest()
        {
            Assert.AreEqual(Permission.Allow, Permission.Deny.ReverseValue());
            Assert.AreEqual(Permission.Deny, Permission.Allow.ReverseValue());
        }

        [Test]
        public void PermissionFlipsThrowsExceptionWhenNotSetTest()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                Permission.NotSet.ReverseValue();
            });
        }

        [Test]
        public void PermissionToDtoTest()
        {
            Assert.AreEqual(Permission.Allow.ToDtoFormat(), "A");
            Assert.AreEqual(Permission.Deny.ToDtoFormat(), "D");
            Assert.AreEqual(Permission.NotSet.ToDtoFormat(), null);
        }

    }
}