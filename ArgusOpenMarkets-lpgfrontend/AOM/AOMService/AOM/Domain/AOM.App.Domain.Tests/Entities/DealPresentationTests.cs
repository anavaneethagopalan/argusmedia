﻿using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class DealPresentationTests
    {
        [Test]
        public void SpecifiedVoidReasonShouldBeLeftAsIsForPresentation()
        {
            const string fakeVoidReason = "He doesn't like you. I don't like you either.";
            var presentationReason = DealPresentation.ToPresentationVoidReason(fakeVoidReason);
            StringAssert.AreEqualIgnoringCase(fakeVoidReason, presentationReason);
        }

        [Test]
        public void EmptyVoidReasonStringShouldBeFilledInAsNoneForPresentation()
        {
            Assert.False(string.IsNullOrWhiteSpace(DealPresentation.ToPresentationVoidReason(null)));
            Assert.False(string.IsNullOrWhiteSpace(DealPresentation.ToPresentationVoidReason("")));
            Assert.False(string.IsNullOrWhiteSpace(DealPresentation.ToPresentationVoidReason(" ")));
        }
    }
}
