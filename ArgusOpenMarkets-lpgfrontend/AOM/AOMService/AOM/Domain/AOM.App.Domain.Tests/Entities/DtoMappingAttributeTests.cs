﻿using System.Diagnostics;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ServiceStack.Text;
using Utils.Logging.Utils;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class DtoMappingAttributeTests
    {
        enum TestingEnum
        {            
            [DtoMapping(null)]
            FieldWithMappingToNull,
            [DtoMapping("B")]
            FieldWithMappingToB,
            [DtoMapping("O")]
            FieldWithMappingToO,
            FieldMissingAMapping
        }         

        //[Test]
        //public void ThrowsExceptionIfMappingMissing()
        //{
        //    Assert.IsNull(DtoMappingAttribute.GetValueFromEnum(TestingEnum.FieldMissingAMapping));
        //}

        [Test]
        public void ReturnsDtoValueFromMappingAttribute()
        {
            Assert.AreEqual(null, DtoMappingAttribute.GetValueFromEnum(TestingEnum.FieldWithMappingToNull));
            Assert.AreEqual("B", DtoMappingAttribute.GetValueFromEnum(TestingEnum.FieldWithMappingToB));
            Assert.AreEqual("O", DtoMappingAttribute.GetValueFromEnum(TestingEnum.FieldWithMappingToO));
        }

        [Test]
        public void CanFindEnumByMapping()
        {
            Assert.AreEqual(TestingEnum.FieldWithMappingToB, DtoMappingAttribute.FindEnumWithValue<TestingEnum>("B"));
            Assert.AreEqual(TestingEnum.FieldWithMappingToO, DtoMappingAttribute.FindEnumWithValue<TestingEnum>("O"));            
        }

        [Test]
        public void CheckThatToDtoExtensionsAreMappingAllFields()
        {
            // TODO: Should they have DTO's ???  If they do can we add them - otherwise remove the comments below?
            // Commented out items are ONLY missing because they do not have ToDto methods.
            CheckForDefaultValues(SetNonDefaultValues(new Assessment()).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new Deal()).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new Email()).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new ExternalDeal()).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new MarketInfo()).ToDto());
            //CheckForDefaultValues(((MarketTickerItem)SetNonDefaultValues(new AuthenticationHistory())).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new Order()).ToDto(), ignoreAttributes: new List<string> { "ProductTenorId" }); // We could fix this but would need to recurse in SetNonDefaultValues.
            CheckForDefaultValues(SetNonDefaultValues(new Product()).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new ProductTenor()).ToDto());
            //CheckForDefaultValues(((Unit)SetNonDefaultValues(new AuthenticationHistory())).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new AuthenticationHistory()).ToDto());
            //CheckForDefaultValues(((CanTradeWith)SetNonDefaultValues(new CanTradeWith())).ToDto());
            //CheckForDefaultValues(((ContentStream)SetNonDefaultValues(new ContentStream())).ToDto());
            //CheckForDefaultValues(((MatrixPermission)SetNonDefaultValues(new MatrixPermission())).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new Organisation()).ToDto());
            //CheckForDefaultValues(((OrganisationRole)SetNonDefaultValues(new OrganisationRole())).ToDto());
            //CheckForDefaultValues(((ProductPrivilege)SetNonDefaultValues(new ProductPrivilege())).ToDto());
            //CheckForDefaultValues(((ProductRolePrivilege)SetNonDefaultValues(new ProductRolePrivilege())).ToDto());
            //CheckForDefaultValues(((SubscribedProduct)SetNonDefaultValues(new SubscribedProduct())).ToDto());
            //CheckForDefaultValues(((SubscribedProductPrivilege)SetNonDefaultValues(new SubscribedProductPrivilege())).ToDto());
            //CheckForDefaultValues(((SystemPrivilege)SetNonDefaultValues(new SystemPrivilege())).ToDto());
            //CheckForDefaultValues(((SystemRolePrivilege)SetNonDefaultValues(new SystemRolePrivilege())).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new UserCredentials()).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new User()).ToDto());
            //CheckForDefaultValues(((UserInfoRole)SetNonDefaultValues(new UserInfoRole())).ToDto());
            //CheckForDefaultValues(((UserModule)SetNonDefaultValues(new UserModule())).ToDto());
            CheckForDefaultValues(SetNonDefaultValues(new MetaData()).ToDto());
        }

        private void CheckForDefaultValues(object e, List<string> ignoreAttributes = null)
        {
            ignoreAttributes = ignoreAttributes ?? new List<string>();

            Type eType = e.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(eType.GetProperties());

            foreach (PropertyInfo prop in props.Where(p=>!ignoreAttributes.Contains(p.Name)))
            {
                Type pType = prop.PropertyType;
                
                if (pType.IsPrimitive)
                {
                    if (prop.GetValue(e).Equals(Activator.CreateInstance(pType))) // compare against default value
                        {
                            throw new AssertionException(String.Format("It looks like field '{0}' is missing from the ToDto() extension method for entity '{1}'.", prop.Name, eType.Name));
                        }
                }
            }
            
        }

        private T SetNonDefaultValues<T>(T e)
        {
            Type eType = e.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(eType.GetProperties());

            foreach (PropertyInfo prop in props)
            {
                var pType = prop.PropertyType;

                if (pType.IsEnum) // If it is an enum, select the last value (in the hope that this is not == the default value for the data type)
                {
                    Array values = Enum.GetValues(pType);
                    prop.SetValue(e, values.GetValue(values.Length-1), null);
                }
                else
                {


                    switch (pType.Name)
                    {
                        case "DateTime":
                            prop.SetValue(e, DateTime.UtcNow, null);
                            break;
                        case "Char":
                            prop.SetValue(e, 'C', null);
                            break;
                        case "String":
                            prop.SetValue(e, "S", null);
                            break;
                        case "Boolean":
                            prop.SetValue(e, true, null);
                            break;
                        case "Int64":
                        case "Int32":
                            prop.SetValue(e, 9, null);
                            break;
                        default:
                            if (prop.PropertyType.FullName.Contains("AOM.App.Domain.Entities") && !prop.PropertyType.IsGeneric())
                            {
                                try
                                {
                                    if (prop.PropertyType.IsArray)
                                    {
                                        prop.SetValue(e, Activator.CreateInstance(pType, 2), null);                                        
                                    }
                                    else if (prop.PropertyType.IsInterface)
                                    {
                                        
                                    }
                                    else
                                    {
                                        var newInstance = Activator.CreateInstance(pType);
                                        prop.SetValue(e, newInstance, null);

                                        SetNonDefaultValues(newInstance);
                                    }
                                }
                                catch (Exception)
                                {
                                    Trace.WriteLine("Error building " + prop.PropertyType.FullName);
                                    throw;
                                }
                                
                            }
                            else
                            {
                                Log.Debug(String.Format("Unsupported type in test will be ignored: {0}", pType.Name));
                            }
                            break;

                    }
                }
            }
            return e;
        }
    }
}

