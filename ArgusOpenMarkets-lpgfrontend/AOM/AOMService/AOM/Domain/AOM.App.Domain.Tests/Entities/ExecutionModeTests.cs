﻿using System;
using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class ExecutionModeTests
    {
        [Test]
        public void EnsureAllEnumValuesHaveDtoMapping()
        {
            var enumType = typeof(ExecutionMode);
            var values = Enum.GetValues(enumType);
            foreach (var value in values)
            {
                if (value.ToString() == ExecutionMode.None.ToString()) continue;
                var mapping = DtoMappingAttribute.GetValueFromEnum((Enum)value);
                Assert.NotNull(mapping, "Expected '" + value + "' enum within " + enumType.Name + " to have a mapping");
            }
        }

        [Test]
        public void InvalidExecutionModeThrowsInRequiredPrivilege()
        {
            ExecutionMode invalid = (ExecutionMode) 99999;
            Assert.Throws<ArgumentOutOfRangeException>(() => invalid.RequiredPrivilege());
        }

        [Test]
        [TestCase(ExecutionMode.Internal, "View_BidAsk_Widget")]
        [TestCase(ExecutionMode.External, "ExternalOrder_Amend")]
        [TestCase(ExecutionMode.None, "")]
        public void ShouldReturnExpectedRequiredPrivilege(ExecutionMode mode, string expectedPrivilege)
        {
            Assert.AreEqual(expectedPrivilege, mode.RequiredPrivilege());
        }
    }
}

