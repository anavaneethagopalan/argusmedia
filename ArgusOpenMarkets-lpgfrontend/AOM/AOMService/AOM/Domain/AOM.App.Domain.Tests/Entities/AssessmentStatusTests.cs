﻿using System;
using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class AssessmentStatusTests
    {
        [Test]
        public void EnsureAllEnumValuesHaveDtoMapping()
        {
            var enumType = typeof (AssessmentStatus);
            var values = Enum.GetValues(enumType);
            foreach (var value in values)
            {
                if (value.ToString() == AssessmentStatus.None.ToString()) continue;
                var mapping=DtoMappingAttribute.GetValueFromEnum((Enum) value);
                Assert.NotNull(mapping, "Expected '"+value+ "' enum within "+ enumType.Name+ " to have a mapping");
            }                  
        }         
    }
}

