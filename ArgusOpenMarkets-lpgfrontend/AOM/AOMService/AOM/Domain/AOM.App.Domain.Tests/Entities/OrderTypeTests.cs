﻿using System;
using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class OrderTypeTests
    {      
        [Test]
        public void EnsureAllEnumValuesHaveDtoMapping()
        {
            var enumType = typeof(OrderType);
            var values = Enum.GetValues(enumType);
            foreach (var value in values)
            {
                if (value.ToString() == OrderType.None.ToString()) continue;
                var mapping = DtoMappingAttribute.GetValueFromEnum((Enum)value);
                Assert.NotNull(mapping, "Expected '" + value + "' enum within " + enumType.Name + " to have a mapping");
            }
        }

        [Test]
        [TestCase(OrderType.Bid, OrderType.Ask)]
        [TestCase(OrderType.Ask, OrderType.Bid)]
        [TestCase(OrderType.None, OrderType.None)]
        public void TestSwapMarketSideReturnsOppositeOrderType(OrderType initial, OrderType expected)
        {
            Assert.That(initial.SwapMarketSide(), Is.EqualTo(expected));
        }
    }
}

