﻿using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class DealExtensionTests
    {
        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void DealWithNoDealMessageShouldReturnEmptyVoidReason(string emptyDealMessage)
        {
            var deal = new Deal
            {
                Message = emptyDealMessage
            };
            Assert.AreEqual(string.Empty, deal.GetDealVoidReason());
        }

        [Test]
        [TestCase("Test void reason", "Test void reason")]
        [TestCase(". \nTest void reason", "Test void reason")]
        [TestCase(".\nTest void reason", "Test void reason")]
        public void DealWithNoInitialOrderReturnsEntireMessageWithTrimmedStart(string message, string expectedVoidReason)
        {
            var deal = new Deal
            {
                Message = message,
                Initial = null
            };
            Assert.AreEqual(expectedVoidReason, deal.GetDealVoidReason());
        }

        [Test]
        [TestCase("Test void reason", "Test void reason")]
        [TestCase(". \nTest void reason", "Test void reason")]
        [TestCase(".\nTest void reason", "Test void reason")]
        public void DealWithNoNotesReturnsEntireMessageWithTrimmedStart(string message, string expectedVoidReason)
        {
            var deal = new Deal
            {
                Message = message,
                Initial = new Order
                {
                    Notes = null
                }
            };
            Assert.AreEqual(expectedVoidReason, deal.GetDealVoidReason());
        }

        [Test]
        [TestCase("", ". \nVOID reason was 'deal terms amended'", "VOID reason was 'deal terms amended'")]
        [TestCase(
            "Some notes",
            "Some notes. \nVOID reason was 'deal terms amended'",
            "VOID reason was 'deal terms amended'")]
        [TestCase(
            "Some notes",
            "Some notes. \nVOID reason was 'other: reason with \"both\" types of 'quotes''",
            "VOID reason was 'other: reason with \"both\" types of 'quotes''")]
        public void DealVoidReasonRemovesDealNotesFromMessage(string notes, string message, string expectedVoidReason)
        {
            var deal = new Deal
            {
                Message = message,
                Initial = new Order
                {
                    Notes = notes
                }
            };
            Assert.AreEqual(expectedVoidReason, deal.GetDealVoidReason());
        }
    }
}
