﻿namespace AOM.App.Domain.Tests.Entities
{
    using AOM.App.Domain.Entities;

    using NUnit.Framework;

    [TestFixture]
    public class OrderTests
    {
        [Test]
        public void ShouldSetBrokerShortCodeToAnAsterixIfBrokerRestrictionIsAny()
        {
            var order = new Order();
            order.BrokerRestriction = BrokerRestriction.Any;

            Assert.That(order.BrokerShortCode, Is.EqualTo("*"));
        }

        [Test]
        public void ShouldSetBrokerShortCodeToNullIfBrokerRestrictionIsNone()
        {
            var order = new Order();
            order.BrokerRestriction = BrokerRestriction.None;

            Assert.That(order.BrokerShortCode, Is.Null);
        }
    }
}
