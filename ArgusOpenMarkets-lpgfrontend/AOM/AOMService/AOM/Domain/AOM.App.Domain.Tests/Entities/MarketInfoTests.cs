﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Text;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class MarketInfoTests
    {
        [Test]
        [TestCase(MarketInfoType.Unspecified, null)]
        [TestCase(MarketInfoType.Ask, "A")]
        [TestCase(MarketInfoType.Bid, "B")]
        [TestCase(MarketInfoType.Deal, "D")]
        [TestCase(MarketInfoType.Info, "I")]
        public void EntityMarketInfoTypeShouldMapToCorrectStringInDto(MarketInfoType type, string expected)
        {
            var mi = new MarketInfo
            {
                MarketInfoType = type,
                MarketInfoStatus = MarketInfoStatus.Pending
            };
            var dto = mi.ToDto();
            Assert.NotNull(dto);
            Assert.That(dto.MarketInfoType, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(null, MarketInfoType.Unspecified)]
        [TestCase("A", MarketInfoType.Ask)]
        [TestCase("B", MarketInfoType.Bid)]
        [TestCase("D", MarketInfoType.Deal)]
        [TestCase("I", MarketInfoType.Info)]
        public void NonNullMarketInfoTypeStringInDtoShouldMapToCorrectTypeInEntity(string strType,
                                                                                   MarketInfoType expected)
        {
            var dto = new MarketInfoDto
            {
                MarketInfoType = strType,
                MarketInfoStatus = "P"
            };
            var mi = dto.ToEntity();
            Assert.NotNull(mi);
            Assert.That(mi.MarketInfoType, Is.EqualTo(expected));
        }

        [Test]
        public void ShouldDeserializeUndefinedMarketInfoTypeAsUnspecified()
        {
            const string json = "{\"marketInfoStatus\":\"Pending\",\"info\":\"test mi\",\"productIds\":[1]}";
            var mi = JsonConvert.DeserializeObject<MarketInfo>(json);
            Assert.NotNull(mi);
            Assert.That(mi.MarketInfoType, Is.EqualTo(MarketInfoType.Unspecified));
        }

        [Test]
        [TestCase("info", MarketInfoType.Info)]
        [TestCase("deal", MarketInfoType.Deal)]
        [TestCase("bid", MarketInfoType.Bid)]
        [TestCase("ask", MarketInfoType.Ask)]
        public void ShouldDeserializeDefinedMarketInfoTypeAsCorrectType(string jsonTypeString,
                                                                        MarketInfoType expectedType)
        {
            var json = CreateFakePlusInfoJson(jsonTypeString);
            var mi = JsonConvert.DeserializeObject<MarketInfo>(json);
            Assert.NotNull(mi);
            Assert.That(mi.MarketInfoType, Is.EqualTo(expectedType));
        }

        private static string CreateFakePlusInfoJson(string typeString)
        {
            var bld = new StringBuilder(50);
            bld.Append("{\"marketInfoStatus\":\"Pending\",\"marketInfoType\":\"");
            bld.Append(typeString);
            bld.Append("\",\"info\":\"test +info\",\"productIds\":[1]}");
            return bld.ToString();

        }
    }
}
