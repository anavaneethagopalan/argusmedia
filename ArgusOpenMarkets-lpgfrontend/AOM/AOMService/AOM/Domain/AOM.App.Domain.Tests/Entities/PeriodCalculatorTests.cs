﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class PeriodCalculatorTests
    {
        [Test]
        public void NAPeriodReturnsNull()
        {
            Period period = new Period()
            {
                TenorCode = "NA",
                PeriodActionCode = "F",
                Code = string.Empty,
                RollDate = string.Empty,
                RollingPeriodFrom = 5,
                RollingPeriodTo = 15,
                FixedDateFrom = null,
                FixedDateTo = null
            };

            List<PeriodData> periodsData = PeriodCalculator.GetPeriodDisplayList(period, DateTime.Today);

            Assert.IsNull(periodsData);
        }

        [Test]
        public void DailyRollingPeriodReturnsCorrectDates()
        {
            Period period = new Period()
            {
                TenorCode = "D",
                PeriodActionCode = "R",
                Code = string.Empty,
                RollDate = string.Empty,
                RollingPeriodFrom = 5,
                RollingPeriodTo = 15,
                FixedDateFrom = null,
                FixedDateTo = null
            };

            List<PeriodData> periodsData = PeriodCalculator.GetPeriodDisplayList(period, DateTime.Today);

            Assert.AreEqual(DateTime.Today.AddDays(5), periodsData[0].DateStart);
            Assert.AreEqual(DateTime.Today.AddDays(15), periodsData[0].DateEnd);
        }

        [Test]
        public void HalfMonthly1HBeforeRollDateFixedRollingListPeriodReturnsDateList()
        {
            Period period = new Period()
            {
                TenorCode = "HM28",
                PeriodActionCode = "RFL",
                Code = "1-H",
                RollDate = "4",
                RollingPeriodFrom = 1,
                RollingPeriodTo = 5,
                FixedDateFrom = "1",
                FixedDateTo = "15"
            };

            List<string> halfMonths = new List<string>();
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(0).ToString("MMM"));
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(1).ToString("MMM"));
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(1).ToString("MMM"));
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(2).ToString("MMM"));
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(2).ToString("MMM"));

            List<PeriodData> periodsData = PeriodCalculator.GetPeriodDisplayList(period, new DateTime(DateTime.Today.Year, DateTime.Today.Month, 2));

            Assert.AreEqual(periodsData.Count, 5);
            Assert.AreEqual(periodsData[0].DateStart.Day, 16);
            Assert.AreEqual(periodsData[0].DateEnd.Day, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));
            Assert.AreEqual(periodsData[1].DateStart.Day, 1);
            Assert.AreEqual(periodsData[1].DateEnd.Day, 15);
            CollectionAssert.AreEqual(halfMonths, periodsData.Select(pd => pd.DisplayText).ToList());
        }

        [Test]
        public void HalfMonthly1HAfterRollDateFixedRollingListPeriodReturnsDateList()
        {
            Period period = new Period()
            {
                TenorCode = "HM30",
                PeriodActionCode = "RFL",
                Code = "1-H",
                RollDate = "4",
                RollingPeriodFrom = 1,
                RollingPeriodTo = 5,
                FixedDateFrom = "1",
                FixedDateTo = "15"
            };

            List<string> halfMonths = new List<string>();
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(1).ToString("MMM"));
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(1).ToString("MMM"));
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(2).ToString("MMM"));
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(2).ToString("MMM"));
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(3).ToString("MMM"));

            List<PeriodData> periodsData = PeriodCalculator.GetPeriodDisplayList(period, new DateTime(DateTime.Today.Year, DateTime.Today.Month, 6));

            Assert.AreEqual(periodsData.Count, 5);
            Assert.AreEqual(periodsData[0].DateStart.Day, 1);
            Assert.AreEqual(periodsData[0].DateEnd.Day, 15);
            Assert.AreEqual(periodsData[1].DateStart.Day, 16);
            Assert.AreEqual(periodsData[1].DateEnd.Day, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(1).Month));
            CollectionAssert.AreEqual(halfMonths, periodsData.Select(pd => pd.DisplayText).ToList());
        }

        [Test]
        public void HalfMonthly2HBeforeRollDateFixedRollingListPeriodReturnsDateList()
        {
            Period period = new Period()
            {
                TenorCode = "HM31",
                PeriodActionCode = "RFL",
                Code = "2-H",
                RollDate = "19",
                RollingPeriodFrom = 1,
                RollingPeriodTo = 5,
                FixedDateFrom = "16",
                FixedDateTo = "28"
            };

            List<string> halfMonths = new List<string>();
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(1).ToString("MMM"));
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(1).ToString("MMM"));
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(2).ToString("MMM"));
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(2).ToString("MMM"));
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(3).ToString("MMM"));

            List<PeriodData> periodsData = PeriodCalculator.GetPeriodDisplayList(period, new DateTime(DateTime.Today.Year, DateTime.Today.Month, 17));
            Assert.AreEqual(periodsData.Count, 5);
            CollectionAssert.AreEqual(halfMonths, periodsData.Select(pd => pd.DisplayText).ToList());
        }

        [Test]
        public void HalfMonthly2HAfterRollDateFixedRollingListPeriodReturnsDateList()
        {
            Period period = new Period()
            {
                TenorCode = "HM29",
                PeriodActionCode = "RFL",
                Code = "2-H",
                RollDate = "19",
                RollingPeriodFrom = 1,
                RollingPeriodTo = 5,
                FixedDateFrom = "16",
                FixedDateTo = "28"
            };

            List<string> halfMonths = new List<string>();
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(1).ToString("MMM"));
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(2).ToString("MMM"));
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(2).ToString("MMM"));
            halfMonths.Add("1-H " + DateTime.Today.AddMonths(3).ToString("MMM"));
            halfMonths.Add("2-H " + DateTime.Today.AddMonths(3).ToString("MMM"));

            List<PeriodData> periodsData = PeriodCalculator.GetPeriodDisplayList(period, new DateTime(DateTime.Today.Year, DateTime.Today.Month, 22));
            Assert.AreEqual(periodsData.Count, 5);
            CollectionAssert.AreEqual(halfMonths, periodsData.Select(pd => pd.DisplayText).ToList());
        }

        [Test]
        public void MonthlyRollingListPeriodReturnsDateList()
        {
            Period period = new Period()
            {
                TenorCode = "M",
                PeriodActionCode = "RL",
                Code = string.Empty,
                RollDate = string.Empty,
                RollingPeriodFrom = 1,
                RollingPeriodTo = 3,
                FixedDateFrom = null,
                FixedDateTo = null
            };

            List<PeriodData> periodsData = PeriodCalculator.GetPeriodDisplayList(period, DateTime.Today);

            List<string> months = new List<string>();
            while (period.RollingPeriodFrom <= period.RollingPeriodTo)
            {
                months.Add(DateTime.Today.AddMonths(period.RollingPeriodFrom.GetValueOrDefault()).ToString("MMMM"));
                period.RollingPeriodFrom++;
            }

            Assert.AreEqual(periodsData.Count, 3);
            CollectionAssert.AreEqual(months, periodsData.Select(pd => pd.DisplayText).ToList());

            period.RollingPeriodFrom = 0;
            period.RollingPeriodTo = 4;
            periodsData = PeriodCalculator.GetPeriodDisplayList(period, DateTime.Today);

            months.Clear();
            while (period.RollingPeriodFrom <= period.RollingPeriodTo)
            {
                months.Add(DateTime.Today.AddMonths(period.RollingPeriodFrom.GetValueOrDefault()).ToString("MMMM"));
                period.RollingPeriodFrom++;
            }

            Assert.AreEqual(periodsData.Count, 5);
            CollectionAssert.AreEqual(months, periodsData.Select(pd => pd.DisplayText).ToList());
        }

        [Test]
        public void MonthlyBoMRollingListPeriodReturnsDateList()
        {
            Period period = new Period()
            {
                TenorCode = "M_BoM",
                PeriodActionCode = "RL",
                Code = string.Empty,
                RollDate = string.Empty,
                RollingPeriodFrom = 1,
                RollingPeriodTo = 3,
                FixedDateFrom = null,
                FixedDateTo = null
            };

            List<string> months = new List<string>();
            months.Add(DateTime.Today.AddMonths(1).ToString("MMMM"));
            months.Add(DateTime.Today.AddMonths(2).ToString("MMMM"));
            months.Add(DateTime.Today.AddMonths(3).ToString("MMMM"));
            months.Add("BoM");

            List<PeriodData> periodsData = PeriodCalculator.GetPeriodDisplayList(period, DateTime.Today);
            Assert.AreEqual(periodsData.Count, 4);
            Assert.AreEqual(periodsData[3].DisplayText, "BoM");
            CollectionAssert.AreEqual(months, periodsData.Select(pd => pd.DisplayText).ToList());
        }

        [Test]
        public void UnknownTenorCodeReturnsException()
        {
            Period period = new Period()
            {
                TenorCode = "ZZ",
                PeriodActionCode = "R",
                Code = string.Empty,
                RollDate = string.Empty,
                RollingPeriodFrom = 1,
                RollingPeriodTo = 3,
                FixedDateFrom = null,
                FixedDateTo = null
            };

            var e = Assert.Throws<Exception>(() => PeriodCalculator.GetPeriodDisplayList(period, DateTime.Today));
            Assert.That(e.Message.Contains("Error unknown TenorCode encountered"));
        }

        [Test]
        public void UnknownPeriodActionCodeReturnsException()
        {
            Period period = new Period()
            {
                TenorCode = "D",
                PeriodActionCode = "ZZ",
                Code = string.Empty,
                RollDate = string.Empty,
                RollingPeriodFrom = 1,
                RollingPeriodTo = 3,
                FixedDateFrom = null,
                FixedDateTo = null
            };

            var e = Assert.Throws<Exception>(() => PeriodCalculator.GetPeriodDisplayList(period, DateTime.Today));
            Assert.That(e.Message.Contains("Error unknown PeriodActionCode encountered"));
        }

    }
}
