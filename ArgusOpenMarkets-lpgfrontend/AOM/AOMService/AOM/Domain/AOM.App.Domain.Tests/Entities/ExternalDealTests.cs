﻿using System.Reflection;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using NUnit.Framework;
using System.Collections.Generic;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class ExternalDealTests
    {
        [Test]
        public void ShouldConvertExternalDealEntityToNotNullDto()
        {
            var externalDealMetaData = MakeExternalDealMetaData();

            var externalDeal = new ExternalDeal {Id = 1, MetaData = externalDealMetaData.ToArray()};
            var externalDealDto = externalDeal.ToDto();

            Assert.That(externalDealDto, Is.Not.Null);
        }

        [Test]
        public void ShouldPopulateMetaDataJsonOnDto()
        {
            var externalDealMetaData = MakeExternalDealMetaData();

            var externalDeal = new ExternalDeal { Id = 1, MetaData = externalDealMetaData.ToArray() };
            var externalDealDto = externalDeal.ToDto();

            var json = GetDtoJson(externalDealDto);
            Assert.That(json, Is.Not.Empty);
        }

        [Test]
        public void JsonInDtoShouldContainMetaDataValues()
        {
            var externalDealMetaData = MakeExternalDealMetaData();

            var externalDeal = new ExternalDeal { Id = 1, MetaData = externalDealMetaData.ToArray() };
            var externalDealDto = externalDeal.ToDto();

            var json = GetDtoJson(externalDealDto);
            Assert.That(json.Contains("DisplayName_01"));
            Assert.That(json.Contains("DisplayValue_01"));
        }

        private string GetDtoJson(ExternalDealDto dto)
        {
            var field = dto.GetType().GetProperty("MetaDataJson", BindingFlags.Instance | BindingFlags.GetField | BindingFlags.Public);

            Assert.NotNull(field, "didn't locate property MetaDataJson");
            var json = (string)field.GetValue(dto);

            return json;
        }

        private List<MetaData> MakeExternalDealMetaData()
        {
            var metaData = new List<MetaData>();
            metaData.Add(new MetaData{ DisplayName = "DisplayName_01", DisplayValue="DisplayValue_01", ItemType = MetaDataTypes.String, ItemValue = -1, MetaDataListItemId = 1, ProductMetaDataId = 1});

            return metaData;
        }
    }
}
