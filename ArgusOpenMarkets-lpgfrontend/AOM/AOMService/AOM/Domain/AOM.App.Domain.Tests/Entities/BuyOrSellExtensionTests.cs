﻿using System;
using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.App.Domain.Tests.Entities
{
    [TestFixture]
    public class BuyOrSellExtensionTests
    {
        [Test]
        public void BuySellFlipsTest()
        {
            Assert.AreEqual(BuyOrSell.Buy, BuyOrSell.Sell.ReverseValue());
            Assert.AreEqual(BuyOrSell.Sell, BuyOrSell.Buy.ReverseValue());
        }

        [Test]
        public void BuySellFlipsThrowsExceptionWhenNoneTest()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                BuyOrSell.None.ReverseValue();
            });
        }

        [Test]
        public void BuySellToDtoTest()
        {
            Assert.AreEqual(BuyOrSell.Buy.ToDtoFormat(), "B");
            Assert.AreEqual(BuyOrSell.Sell.ToDtoFormat(), "S");
            Assert.AreEqual(BuyOrSell.None.ToDtoFormat(), null);
        }
    }
}

