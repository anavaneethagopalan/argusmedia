﻿using System.Configuration;

namespace AOM.Service.Configs
{
    /// <summary>
    /// AOM service section
    /// </summary>
    public class AomServiceConfigSection : ConfigurationSection
    {
        // Create a "aomservice" element.
        [ConfigurationProperty("service")]
        public AomServiceElement Service
        {
            get
            {
                return (AomServiceElement)this["service"];
            }
            set
            { this["service"] = value; }
        }

        [ConfigurationProperty("isLongRunning",IsRequired = true,DefaultValue = false)]
        public bool IsLongRunning
        {
            get
            {
                return (bool)this["isLongRunning"];
            }
            set
            { this["isLongRunning"] = value; }
        }

    }
}