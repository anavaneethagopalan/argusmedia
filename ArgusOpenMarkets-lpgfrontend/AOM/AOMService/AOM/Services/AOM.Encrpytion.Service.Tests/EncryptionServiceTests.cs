﻿namespace AOM.Encrpytion.Service.Tests
{


    using AOM.Services.Encryption;
    using NUnit.Framework;

    [TestFixture]
    public class EncryptionServiceTests
    {
        [Test]
        public void HashingEmptyStringShouldReturnEmptyString()
        {
            var encryptionService = new EncryptionService();
            var hashedValue = encryptionService.Hash("");

            Assert.That(hashedValue, Is.EqualTo(string.Empty));
        }

        [Test]
        public void ShouldHashAStringValue()
        {
            var expectedHash = "7C6CF58961D0E9A540352B85456C56487CC43044";
            var encryptionService = new EncryptionService();
            var hashedValue = encryptionService.Hash("Nathan");

            Assert.That(hashedValue, Is.EqualTo(expectedHash));
        }

        [Test]
        public void HashingAHashedValueWillGenerateNewHash()
        {
            var expectedHash = "7C6CF58961D0E9A540352B85456C56487CC43044";
            var encryptionService = new EncryptionService();
            var hashedValue = encryptionService.Hash("Nathan");

            Assert.That(hashedValue, Is.EqualTo(expectedHash));

            hashedValue = encryptionService.Hash(hashedValue);
            Assert.That(hashedValue, Is.Not.EqualTo(expectedHash));
        }

        [Test]
        public void HashingTheSameValueMultipleTimesShouldReturnTheSameHashValue()
        {
            var password = "Very5ecureP4ssw0rd";
            var expectedHash = "A4F2F225E2D3AACBE67507613C54968F9793E7E6";
            var encryptionService = new EncryptionService();
            var hashedValue = encryptionService.Hash(password);
            Assert.That(hashedValue, Is.EqualTo(expectedHash));

            hashedValue = encryptionService.Hash(password);
            Assert.That(hashedValue, Is.EqualTo(expectedHash));

            hashedValue = encryptionService.Hash(password);
            Assert.That(hashedValue, Is.EqualTo(expectedHash));
        }
    }
}
