﻿namespace AOM.Services.Encryption
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    using AOM.App.Domain.Services;

    public class EncryptionService : IEncryptionService
    {
		public string Hash(string value)
		{
		    if (String.IsNullOrEmpty(value))
		    {
		        return String.Empty;
		    }

		    using (var sha1 = new SHA1Managed())
            {
                byte[] textData = Encoding.UTF8.GetBytes(value);

                byte[] hash = sha1.ComputeHash(textData);

                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
		}
	}
}
