﻿namespace AOM.Service.Contracts
{
    public interface IUserService
    {
        bool AuthenticateUserService(string username, string password);

        ServiceInfo GetServiceInfo();
    }
}
