﻿using System;
using System.Runtime.Serialization;

namespace AOM.Service.Contracts
{
    [DataContract]
    public class ServiceInfo
    {
        private Guid _serviceId;
        private string _serviceName;

        public ServiceInfo(Guid serviceId, string serviceName)
        {
            _serviceId = serviceId;
            _serviceName = serviceName;
        }

        [DataMember]
        public Guid ServiceId
        {
            get { return _serviceId; }
            set { _serviceId = value; }
        }

        [DataMember]
        public string ServiceName
        {
            get { return _serviceName; }
            set { _serviceName = value; }
        }
    }
}