﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Web.App.Entities
{
    public class UpdatePermissionsRequest
    {
        public IList<MatrixPermission> Permissions { get; set; }
        public long OrganisationId { get; set; }
    }
}