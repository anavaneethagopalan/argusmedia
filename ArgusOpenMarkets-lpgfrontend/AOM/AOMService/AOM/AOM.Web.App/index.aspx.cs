﻿namespace AOM.Web.App
{
    using System;
    using System.Reflection;

    public partial class index : System.Web.UI.Page
    {
        public string VersionInfo { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            VersionInfo = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}