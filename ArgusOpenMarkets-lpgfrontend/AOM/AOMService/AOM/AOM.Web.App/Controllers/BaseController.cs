﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace AOM.Web.App.Controllers
{
    public class BaseController : ApiController
    {
        internal HttpResponseMessage CreateResponseMessage(HttpStatusCode code, string json)
        {
            var response = Request.CreateResponse(code);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }

        public virtual long GetUserId()
        {
            return Convert.ToInt64(HttpContext.Current.Request.Headers["userid"]);
        }
    }
}
