﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using AOM.Services.ProductService;
using AOM.Web.App.Security;

using Argus.Transport.Infrastructure;
using Utils.Javascript.Extension;
using WebApi.OutputCache.V2;
using System;
using Utils.Logging.Utils;

namespace AOM.Web.App.Controllers
{
    // [CheckSessionToken]  Turned off as something has broken this TODO: Need to reestablish
    public class ProductController : BaseController
    {
        private readonly IBus _bus;
        private readonly IProductService _productService;
        private const int CacheTimeOutForDataSeconds = 3600;

        public ProductController(IBus bus, IProductService productService)
        {
            _bus = bus;
            _productService = productService;
        }

        [HttpGet]
        [Route("api/getProductDefinition")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage GetProductDefinition(long productId)
        {
            try
            {
                var productDefinition = _productService.GetProductDefinition(productId);
                if (productDefinition == null)
                {
                    return CreateResponseMessage(HttpStatusCode.NotFound, String.Empty);
                }
                return CreateResponseMessage(HttpStatusCode.OK, productDefinition.ToJsonCamelCase());
            }
            catch (Exception ex)
            {
                Log.Error("getProductDefinition failed for product" + productId, ex);
                return CreateResponseMessage(HttpStatusCode.InternalServerError, String.Empty);
            }
        }
    }
}