﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Services.AuthenticationService;
using AOM.Services.PermissionService;
using AOM.Transport.Events;
using AOM.Transport.Events.BrokerPermissions;
using AOM.Transport.Events.CounterpartyPermissions;
using AOM.Transport.Events.Organisations;
using AOM.Web.App.Entities;
using AOM.Web.App.Security;
using Argus.Transport.Infrastructure;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Web.App.Controllers
{
    [CheckSessionToken]
    public class PermissionController : BaseController
    {
        private readonly IBus _bus;
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly ITopicReduction _topicReductionService;
        private readonly ICounterpartyPermissionAuthenticationService _cptyAuthenticationService;
        private readonly IBrokerPermissionAuthenticationService _brokerAuthenticationService;
        private readonly IBrokerPermissionService _brokerPermissionService;
        private readonly ICounterpartyPermissionService _counterpartyPermissionService;

        //private const int CacheTimeOutForDataSeconds = 30;

        public PermissionController(IBus bus, IUserService userService, IOrganisationService organisationService, ITopicReduction topicReductionService,
            ICounterpartyPermissionAuthenticationService authenticationService, IBrokerPermissionAuthenticationService brokerAuthenticationService,
            IBrokerPermissionService brokerPermissionService, ICounterpartyPermissionService counterpartyPermissionService)
        {
            _bus = bus;
            _userService = userService;
            _organisationService = organisationService;
            _topicReductionService = topicReductionService;
            _cptyAuthenticationService = authenticationService;
            _brokerAuthenticationService = brokerAuthenticationService;
            _brokerPermissionService = brokerPermissionService;
            _counterpartyPermissionService = counterpartyPermissionService;
        }

        [HttpGet]
        [Route("api/getCommonPermissionedOrganisations")]
        public HttpResponseMessage GetCommonPermissionedOrganisations(long productId, long organisationId, long brokerId, BuyOrSell buyOrSell)
        {
            long userid = GetUserId();
            var brokersRequest = new GetCommonPermissionedOrganisationsRpc
            {
                ProductId = productId,
                RequestedByUserId = userid,
                OrganisationId = organisationId,
                OrganisationBrokerageId = brokerId,
                BuyOrSell = buyOrSell
            };

            string json = String.Empty;
            try
            {
                var brokers = _bus.Request<GetCommonPermissionedOrganisationsRpc, GetCommonPermissionedOrganisationsResponse>(brokersRequest).CommonOrganisations;
                json = brokers.Cast<IOrganisationMinimumDetails>().ToJsonCamelCase();
            }
            catch (Exception e)
            {
                Log.Error("Error retrieving data from bus for GetCommonPermissionedOrganisations (broker): ", e);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getCommonPermissionedOrganisations")]
        public HttpResponseMessage GetCommonPermissionedOrganisations(long productId, long organisationId, BuyOrSell buyOrSell)
        {
            long userid = GetUserId();
            var brokersRequest = new GetCommonPermissionedOrganisationsRpc
            {
                ProductId = productId,
                RequestedByUserId = userid,
                OrganisationId = organisationId,
                OrganisationBrokerageId = null,
                BuyOrSell = buyOrSell
            };

            string json = String.Empty;
            try
            {
                var brokers = _bus.Request<GetCommonPermissionedOrganisationsRpc, GetCommonPermissionedOrganisationsResponse>(brokersRequest).CommonOrganisations;
                json = brokers.Cast<IOrganisationMinimumDetails>().ToJsonCamelCase();
            }
            catch (Exception e)
            {
                Log.Error("Error in WebService - GetCommonPermissionedOrganisations: ", e);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getCounterpartyPermissionsByProduct")]
        public HttpResponseMessage GetCounterpartyPermissionsByProduct(long productId, long organisationId)
        {
            string json = string.Empty;
            try
            {
                IList<BilateralMatrixPermissionPair> permissions = _organisationService.GetCounterpartyPermissions(organisationId).Where(m => m.ProductId == productId).ToList();
                List<OrganisationPermissionList> reducedPermissionMatrix = _topicReductionService.ReduceOrganisationTopicCount(permissions, true);
                json = reducedPermissionMatrix.ToJsonCamelCase();
            }
            catch (Exception e)
            {
                Log.Error("Error in WebService - getCounterpartyPermissionsByProduct: ", e);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getCounterpartyPermissionsByMarket")]
        public HttpResponseMessage GetCounterpartyPermissionsByMarket(long marketId, long organisationId)
        {
            string json = string.Empty;
            try
            {
                IList<BilateralMatrixPermissionPair> permissions = _organisationService.GetCounterpartyPermissions(organisationId).Where(m => m.MarketId == marketId).ToList();
                List<OrganisationPermissionList> reducedPermissionMatrix = _topicReductionService.ReduceOrganisationTopicCount(permissions, true);
                json = reducedPermissionMatrix.ToJsonCamelCase();
            }
            catch (Exception e)
            {
                Log.Error("Error in WebService - getCounterpartyPermissionsByMarket: ", e);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getBrokerPermissionsByProduct")]
        public HttpResponseMessage GetBrokerPermissionsByProduct(long productId, long organisationId)
        {
            string json = string.Empty;
            try
            {
                IList<BilateralMatrixPermissionPair> permissions = _organisationService.GetBrokerPermissions(organisationId).Where(m => m.ProductId == productId).ToList();
                List<OrganisationPermissionList> reducedPermissionMatrix = _topicReductionService.ReduceOrganisationTopicCount(permissions, true);
                json = reducedPermissionMatrix.ToJsonCamelCase();
            }
            catch (Exception e)
            {
                Log.Error("Error in WebService - getBrokerPermissionsByProduct: ", e);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getBrokerPermissionsByMarket")]
        public HttpResponseMessage GetBrokerPermissionsByMarket(long marketId, long organisationId)
        {
            string json = string.Empty;
            try
            {
                IList<BilateralMatrixPermissionPair> permissions = _organisationService.GetBrokerPermissions(organisationId).Where(m => m.MarketId == marketId).ToList();
                List<OrganisationPermissionList> reducedPermissionMatrix = _topicReductionService.ReduceOrganisationTopicCount(permissions, true);
                json = reducedPermissionMatrix.ToJsonCamelCase();
            }
            catch (Exception e)
            {
                Log.Error("Error in WebService - getBrokerPermissionsByMarket: ", e);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpPost]
        [Route("api/updateCounterpartyPermissions")]
        public HttpResponseMessage UpdateCounterpartyPermissions(UpdatePermissionsRequest updatedPermissionsReq)
        {
            string json = string.Empty;
            try
            {
                long userId = GetUserId();
                long orgId = _userService.GetUsersOrganisation(userId).Id;

                AomCounterpartyPermissionAuthenticationRequest request = new AomCounterpartyPermissionAuthenticationRequest()
                {
                    CounterpartyPermissions = updatedPermissionsReq.Permissions.ToList(),
                    ClientSessionInfo = new ClientSessionInfo() {OrganisationId = orgId, UserId = userId},
                    MessageAction = MessageAction.Update
                };

                var authResult = _cptyAuthenticationService.AuthenticateCounterpartyPermissionRequest(request);

                if (authResult.Allow)
                {
                    AomCounterpartyPermissionAuthenticationResponse authenticatedPermission = new AomCounterpartyPermissionAuthenticationResponse()
                    {
                        Message = new Message
                        {
                            MessageAction = request.MessageAction,
                            ClientSessionInfo = request.ClientSessionInfo,
                            MessageType = MessageType.BrokerPermission
                        },
                        CounterpartyPermissions = updatedPermissionsReq.Permissions.ToList(),
                        MarketMustBeOpen = authResult.MarketMustBeOpen
                    };

                    AomCounterpartyPermissionResponse updatedPermissions = _counterpartyPermissionService.UpdatePermissions(authenticatedPermission, updatedPermissionsReq.OrganisationId);
                    updatedPermissions.Message.ClientSessionInfo = request.ClientSessionInfo;
                    json = updatedPermissions.ToJsonCamelCase();
                    //    response.MarketMustBeOpen = authResult.MarketMustBeOpen;
                }
                else
                {
                    json = String.Format("{0}: Authorisation was denied to user id {1} while attempting {2}/{3} [{4}]",
                        ToString(), userId, "UpdateBrokerPermissions", MessageAction.Update, authResult.DenyReason);
                    Log.Warn(json);
                }

            }
            catch (Exception e)
            {
                Log.Error("Error in WebService - UpdateCounterpartyPermissions: ", e);
                json = "Error updating CounterpartyPermissions. Update failed";
            }

            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpPost]
        [Route("api/updateBrokerPermissions")]
        public HttpResponseMessage UpdateBrokerPermissions(UpdatePermissionsRequest brokerPermissionsUpdate)
        {
            string json = string.Empty;

            try
            {
                long userId = GetUserId();
                Log.Info("Starting authentication of broker perms for org :" + brokerPermissionsUpdate.OrganisationId);
                AomBrokerPermissionAuthenticationRequest request = new AomBrokerPermissionAuthenticationRequest()
                {
                    BrokerPermissions = brokerPermissionsUpdate.Permissions.ToList(),
                    ClientSessionInfo = new ClientSessionInfo() {OrganisationId = brokerPermissionsUpdate.OrganisationId, UserId = userId},
                    MessageAction = MessageAction.Update
                };

                var authResult = _brokerAuthenticationService.AuthenticateBrokerPermissionRequest(request);
                Log.Info("Finished authentication of broker perms for org :" + brokerPermissionsUpdate.OrganisationId);

                if (authResult.Allow)
                {
                    AomBrokerPermissionAuthenticationResponse authenticatedPermission = new AomBrokerPermissionAuthenticationResponse()
                    {
                        Message = new Message
                        {
                            MessageAction = request.MessageAction,
                            ClientSessionInfo = request.ClientSessionInfo,
                            MessageType = MessageType.BrokerPermission
                        },
                        BrokerPermissions = brokerPermissionsUpdate.Permissions.ToList(),
                        MarketMustBeOpen = authResult.MarketMustBeOpen
                    };

                    AomBrokerPermissionResponse updatedPermissions = _brokerPermissionService.UpdatePermissions(authenticatedPermission, brokerPermissionsUpdate.OrganisationId);
                    updatedPermissions.Message.ClientSessionInfo = request.ClientSessionInfo;
                    json = updatedPermissions.ToJsonCamelCase();
                    //    response.MarketMustBeOpen = authResult.MarketMustBeOpen;
                }
                else
                {
                    json = String.Format("{0}: Authorisation was denied to user id {1} while attempting {2}/{3} [{4}]",
                        ToString(), userId, "UpdateBrokerPermissions", MessageAction.Update, authResult.DenyReason);
                    Log.Warn(json);
                }

            }
            catch (Exception e)
            {
                Log.Error("Error in WebService - updateBrokerPermissions: ", e);
                json = "Error updating BrokerPermissions. Update failed";
            }

            Log.Info("Finished Broker permissions update for org: " + brokerPermissionsUpdate.OrganisationId);
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }
    }
}