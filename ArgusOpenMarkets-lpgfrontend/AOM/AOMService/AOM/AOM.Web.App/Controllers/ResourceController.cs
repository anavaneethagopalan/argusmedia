﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

using AOM.App.Domain.Entities;
using AOM.Transport.Events.Users;

using Argus.Transport.Infrastructure;

namespace AOM.Web.App.Controllers
{
    //[CheckSessionToken(IgnoreCache = true)]
    public class ResourceController : BaseController
    {
        private readonly IBus _bus;

        public ResourceController(IBus iBus)
        {
            _bus = iBus;
        }

        [HttpGet]
        [Route("api/getHelpFile")]
        public HttpResponseMessage GetHelpFile(string token, string uid)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);

            long userId;

            if (!long.TryParse(uid, out userId))
            {
                response.Content = new StringContent("Invalid user id");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            }

            var authRequest = new SessionTokenAuthenticatonRequest{ UserId = uid, SessionTokenString = token };
            var authResult = _bus.Request<SessionTokenAuthenticatonRequest, AuthenticationResult>(authRequest);

            if (authResult.IsAuthenticated)
            {
                string fileLocation = HttpContext.Current.Server.MapPath("~/Resources/empty.pdf");
                byte[] fileBytes = File.ReadAllBytes(fileLocation);
                response.Content = new ByteArrayContent(fileBytes);
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "AOMHelp.pdf" };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            }
            else
            {
                response.Content = new StringContent("Invalid token");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            }

            return response;
        }
    }
}