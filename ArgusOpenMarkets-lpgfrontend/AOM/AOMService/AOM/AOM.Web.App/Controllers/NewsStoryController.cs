﻿using AOM.Transport.Events.News;
using AOM.Web.App.Security;
using Argus.Transport.Infrastructure;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;
using WebApi.OutputCache.V2;
using System.Globalization;

namespace AOM.Web.App.Controllers
{
    [CheckSessionToken]
    public class NewsStoryController : ApiController
    {
        private readonly IBus _aomBus;

        private const int CacheTimeOutForDataSeconds = 30;

        public NewsStoryController(IBus aomBus)
        {
            _aomBus = aomBus;
        }

        [HttpGet]
        [Route("api/newsStory")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage NewsStory(long? id, long userId)
        {
            Log.InfoFormat("NewsStoryController.NewsStory = GetNewsStory story id requested: {0} for the user id: {1} ", id, userId);

            var story = string.Empty;
            Boolean articleError = false;

            if (id.HasValue)
            {
                try
                {
                    var request = new NewsStoryRequestRpc
                    {
                        CmsId = id.Value.ToString(CultureInfo.InvariantCulture),
                        UserId = userId
                    };

                    var newsStoryResponse = _aomBus.Request<NewsStoryRequestRpc, NewsStoryResponse>(request);
                    if (newsStoryResponse != null && newsStoryResponse.Story != "")
                    {
                        story = newsStoryResponse.Story;
                    }
                    else
                    {
                        story = string.Format("The request for news article: {0} returned nothing.  Please contact Aom Support on aomsupport@argusmedia.com or call +44(0)2071999430 if the problem persists.", id);
                        articleError = true;
                    }

                }
                catch (Exception ex)
                {
                    story = "An internal system error has occurred.  Please contact support if the problem persists.";
                    // Done this way to avoid assembly having to reference IAomModel.
                    articleError = true;
                    Log.Error(ex.Message, ex);
                }
            }

            var jsonResponse = new { Story = story, Error = articleError }.ToJsonCamelCase();
            return CreateResponseMessage(HttpStatusCode.OK, jsonResponse);
        }

        private HttpResponseMessage CreateResponseMessage(HttpStatusCode code, string json)
        {
            var response = Request.CreateResponse(code);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }
    }
}