﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

using AOM.Transport.Events.HealthCheck;

using Argus.Transport.Infrastructure;

using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Web.App.Controllers
{
    public class HealthcheckController : BaseController
    {
        private readonly IBus _bus;

        public HealthcheckController(IBus bus)
        {
            _bus = bus;
        }

        [HttpGet]
        [Route("api/healthcheck")]
        public HttpStatusCode AuthenticateUserPost()
        {
            try
            {
                var aomStatus = _bus.Request<HealthCheckStatusRpc, HealthCheckStatusResult>(new HealthCheckStatusRpc());

                if (aomStatus.HealthCheckStatusOk)
                {
                    return HttpStatusCode.OK;
                }

                // 503 Error code - a down-stream service is unavailable.  
                return HttpStatusCode.ServiceUnavailable;
            }
            catch (Exception ex)
            {
                Log.Error("healthcheck errored", ex);
                return HttpStatusCode.ServiceUnavailable;
            }
        }


        [HttpGet]
        [Route("api/processorstatus")]
        public HttpResponseMessage AomProcessorStatus()
        {
            var aomProcessorStatus = _bus.Request<AomProcessorStatusRpc, AomProcessorStatusResult>(new AomProcessorStatusRpc());
            var json = aomProcessorStatus.ToJsonCamelCase();

            return CreateResponseMessage(HttpStatusCode.OK, json);
        }
    }
}