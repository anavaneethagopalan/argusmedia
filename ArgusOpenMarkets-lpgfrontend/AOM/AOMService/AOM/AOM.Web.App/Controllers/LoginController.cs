﻿using System;
using System.Linq;
using System.Runtime.Caching;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events.Users;
using AOM.Web.App.Security;

using Argus.Transport.Infrastructure;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;
using Utils.Validation;

namespace AOM.Web.App.Controllers
{
    public class LoginController : ApiController
    {
        private const string SystemFailure = "AOM is currently unavailable";
        private readonly IBus _bus;
        private readonly IEncryptionService _encryptionService;
        private readonly IIpService _ipService;
        private readonly IUserService _userService;
        private readonly IUserAuthenticationService _userAuthenticationService;

        public LoginController(IBus bus, IEncryptionService encryptionService, IIpService ipService, IUserService userService, IUserAuthenticationService userAuthenticationService)
        {
            _bus = bus;
            _encryptionService = encryptionService;
            _ipService = ipService;
            _userService = userService;
            _userAuthenticationService = userAuthenticationService;
        }

        [HttpPost]
        [Route("api/login")]
        public async Task<HttpResponseMessage> AuthenticateUserPost([FromBody] JObject incomingAuthenticationRequest)
        {
            string threadId = System.Threading.Thread.CurrentThread.ManagedThreadId.ToString();
            Log.Info(threadId + ": Starting AuthenticateUserPost");
            var parsedAuthenticationRequest = JsonConvert.DeserializeObject<UserAuthenticationRequestRpc>(incomingAuthenticationRequest.ToString());

            if (string.IsNullOrEmpty(parsedAuthenticationRequest.Username) ||
                string.IsNullOrEmpty(parsedAuthenticationRequest.Password) ||
                string.IsNullOrEmpty(parsedAuthenticationRequest.LoginSource))
            {
                var result = CreateErrorResult("Invalid login details, please try again.");
                return CreateResponseMessage(HttpStatusCode.OK, result.ToJsonCamelCase());
            }

            //BruteForce delay - adds 3 second delay to users login attempts if fail to authenticate correctly
            ObjectCache cache = MemoryCache.Default;
            string userContextId = HttpContext.Current.Request.UserHostAddress + "_" + parsedAuthenticationRequest.Username;
            if (cache.Get(userContextId) != null)
            {
                Log.Info(threadId + ": Pausing user login attempt by 3 secs");
                await Task.Delay((int) cache.Get(userContextId)*1000);
            }

            var user = new User();
            try
            {
                user.Id = _userService.GetUserId(parsedAuthenticationRequest.Username);
            }
            catch (Exception ex)
            {
                Log.Error("Error retrieving userId from userService for: " + parsedAuthenticationRequest.Username, ex);
            }

            UserLoginSource userLoginSource = null;
            try
            {
                if (!string.IsNullOrEmpty(parsedAuthenticationRequest.LoginSource))
                    userLoginSource = _userService.GetUserLoginSources().First(es => es.Code.ToUpper() == parsedAuthenticationRequest.LoginSource.ToUpper());

                if (userLoginSource == null)
                    throw new Exception();
            }
            catch (Exception)
            {
                Log.Info("Error retrieving database source information for: " + parsedAuthenticationRequest.LoginSource);
                var result = CreateErrorResult("Invalid source information, please try again.");
                return CreateResponseMessage(HttpStatusCode.Forbidden, result.ToJsonCamelCase());
            }

            try
            {
                Log.Info(threadId + ": Starting password hash");
                parsedAuthenticationRequest.Password = _encryptionService.Hash(user, parsedAuthenticationRequest.Password);
                Log.Info(threadId + ": Finished password hash");
            }
            catch (Exception ex)
            {
                Log.Error("EncryptionService. Password hash has failed.", ex);
            }

            try
            {
                parsedAuthenticationRequest.IpAddress = _ipService.GetIpAddressOfRemoteConnection();
                Log.Info(String.Format(threadId + ": User login attempt recorded for username: {0} from {1}", parsedAuthenticationRequest.Username, parsedAuthenticationRequest.IpAddress));
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Error while trying to get IP address for username: {0}: Message: {1}", parsedAuthenticationRequest.Username, ex));
            }

            AuthenticationResult authResult = AuthenticateUserLoginRequest(parsedAuthenticationRequest, user.Id, userLoginSource.Id);

            if (authResult.IsAuthenticated)
            {
                cache.Remove(userContextId);
            }
            else
            {
                //should probably make these config settings!
                CacheItemPolicy policy = new CacheItemPolicy {AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(60)};
                cache.Add(userContextId, 3, policy);
            }
            
            long userId = authResult.User != null && authResult.User.Id == user.Id ? authResult.User.Id : -1;
            var userWithPrivs = _userService.GetUserWithPrivileges(userId) as User;

            var jsonResult = new
            {
                authResult.IsAuthenticated,
                authResult.Message,
                authResult.Token,
                userId,
                sessionToken = authResult.SessionToken,
                credentialType = authResult.CredentialType,
                User = userWithPrivs
            }.ToJsonCamelCase();

            var response = CreateResponseMessage(HttpStatusCode.OK, jsonResult);
            Log.Info(threadId + ": Finished AuthenticateUserPost");
            return response;
        }

        private AuthenticationResult AuthenticateUserLoginRequest(UserAuthenticationRequestRpc parsedAuthenticationRequest, long userIdCheck, long userLoginSourceId)
        {
            //var authResult = _bus.Request<UserAuthenticationRequestRpc, AuthenticationResult>(parsedAuthenticationRequest);
            Log.Info("AuthenticateUserLoginRequest AuthenticateUser");
            var authResult = _userAuthenticationService.AuthenticateUser(parsedAuthenticationRequest.Username, parsedAuthenticationRequest.Password, userLoginSourceId, parsedAuthenticationRequest.IpAddress);
            Log.Info("AuthenticateUserLoginRequest Record User IP");
            _userService.RecordLoginIpForUser(parsedAuthenticationRequest.IpAddress, parsedAuthenticationRequest.Username, authResult.IsAuthenticated, authResult.FailureReason, userLoginSourceId);

            //check result is there and that id matches retrieved Id from earlier.

            if (authResult.User != null)
            {
                if (authResult.User.Id != userIdCheck)
                {
                    authResult.User.Id = -1;
                }
            }
            return authResult;
        }

        [HttpPost]
        [CheckSessionToken]
        [Route("api/updateDetails")]
        public HttpResponseMessage UpdateDetails([FromBody] JObject incomingChangePasswordRequest)
        {
            if (incomingChangePasswordRequest == null)
            {
                return CreateResponseMessage(HttpStatusCode.Forbidden, CreateErrorResult("No credentials received, please try again.").ToJsonCamelCase());
            }

            var parsedChangePasswordRequest = JsonConvert.DeserializeObject<ChangePasswordRequestRpc>(incomingChangePasswordRequest.ToString());

            //explicitly setting these just in case of unathorised request
            parsedChangePasswordRequest.IsTemporary = false;
            parsedChangePasswordRequest.AdminResetPassword = false;
            parsedChangePasswordRequest.TempPassExpirInHours = 48;

            if (parsedChangePasswordRequest.OldPassword == parsedChangePasswordRequest.NewPassword)
            {
                return CreateResponseMessage(HttpStatusCode.Forbidden, CreateErrorResult("New and old passwords cannot be the same!").ToJsonCamelCase());
            }

            if (!PasswordValidator.ValidatePassword(parsedChangePasswordRequest.NewPassword))
            {
                return CreateResponseMessage(HttpStatusCode.Forbidden, 
                    CreateErrorResult("Passwords must be 12-50 characters and contain at least one upper and lower case character one number and one special character").ToJsonCamelCase());
            }

            var userIdRequest = new GetUserIdRequestRpc { Username = parsedChangePasswordRequest.Username };
            var userIdResult = _bus.Request<GetUserIdRequestRpc, GetUserIdResponseRpc>(userIdRequest);
            var user = new User { Id = userIdResult.UserId };

            parsedChangePasswordRequest.OldPassword = _encryptionService.Hash(user, parsedChangePasswordRequest.OldPassword);
            parsedChangePasswordRequest.NewPassword = _encryptionService.Hash(user, parsedChangePasswordRequest.NewPassword);
            parsedChangePasswordRequest.CheckOldPassword = true;
            parsedChangePasswordRequest.RequestorUserId = user.Id;

            ChangePasswordResponse result;
            parsedChangePasswordRequest.IsTemporary = false;
            try
            {
                result = _bus.Request<ChangePasswordRequestRpc, ChangePasswordResponse>(parsedChangePasswordRequest);
            }
            catch (Exception ex)
            {
                Log.Error("Internal system problem ", ex);
                return CreateResponseMessage(HttpStatusCode.Forbidden, CreateErrorResult(SystemFailure).ToJsonCamelCase());
            }

            if (result.Success)
            {
                //user is OK, should let in at this point!
                var authRequest = new UserAuthenticationRequestRpc
                {
                    Username = parsedChangePasswordRequest.Username,
                    Password = parsedChangePasswordRequest.NewPassword,
                    IpAddress = _ipService.GetIpAddressOfRemoteConnection()
                };

                long userLoginSourceId = 1;
                if (_userService.GetUserLoginSources().First(ls => ls.Code.ToUpper() == "AOM") != null)
                    userLoginSourceId = _userService.GetUserLoginSources().First(ls => ls.Code.ToUpper() == "AOM").Id;

                AuthenticationResult authResult = AuthenticateUserLoginRequest(authRequest, user.Id, userLoginSourceId);
                var jsonResult = new { authResult.IsAuthenticated, authResult.Message, authResult.Token, authResult.User.Id, sessionToken = authResult.SessionToken }.ToJsonCamelCase();
                return CreateResponseMessage(HttpStatusCode.OK, jsonResult);
            }

            return CreateResponseMessage(HttpStatusCode.Forbidden, CreateErrorResult("Could not update user credentials!").ToJsonCamelCase());
        }

        [HttpPost]
        [Route("api/forgotPassword")]
        public HttpResponseMessage ForgotPassword([FromBody] JObject incomingRequest)
        {
            try
            {
                if (incomingRequest == null)
                {
                    return CreateResponseMessage(HttpStatusCode.OK, CreateErrorResult("No credentials received, please try again.").ToJsonCamelCase());
                }

                var request = JsonConvert.DeserializeObject<ForgotPasswordRequestRpc>(incomingRequest.ToString());
                var response = SendForgotPasswordRequest(request);

                return CreateResponseMessage(HttpStatusCode.OK, new { IsAuthenticated = response.Success, Message = response.MessageBody }.ToJsonCamelCase());
            }
            catch (Exception e)
            {
                return CreateResponseMessage(HttpStatusCode.OK, CreateErrorResult(e.Message).ToJsonCamelCase());
            }
        }

        [HttpPost]
        [Route("api/resetPassword")]
        public HttpResponseMessage ResetPassword([FromBody] JObject incomingRequest)
        {
            try
            {
                if (incomingRequest == null)
                {
                    return CreateResponseMessage(HttpStatusCode.Forbidden, CreateErrorResult("No credentials received, please try again.").ToJsonCamelCase());
                }

                var request = JsonConvert.DeserializeObject<ResetPasswordRequestRpc>(incomingRequest.ToString());
                var response = SendResetPasswordRequest(request);

                return CreateResponseMessage(HttpStatusCode.OK, new { IsAuthenticated = response.Success, Message = response.MessageBody }.ToJsonCamelCase());
            }
            catch (Exception e)
            {
                return CreateResponseMessage(HttpStatusCode.OK, CreateErrorResult(e.Message).ToJsonCamelCase());
            }
        }

        private HttpResponseMessage CreateResponseMessage(HttpStatusCode code, string json)
        {
            var response = Request.CreateResponse(code);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }

        private AuthenticationResult CreateErrorResult(string message)
        {
            return new AuthenticationResult
            {
                IsAuthenticated = false,
                Message = message,
                Token = string.Empty,
                User = null
            };
        }

        private ChangePasswordResponse SendForgotPasswordRequest(ForgotPasswordRequestRpc request)
        {
            try
            {
                return _bus.Request<ForgotPasswordRequestRpc, ChangePasswordResponse>(request);
            }
            catch
            {
                return new ChangePasswordResponse
                {
                    Success = false,
                    MessageBody = SystemFailure,
                    UserName = request.Username,
                };
            }
        }

        private ChangePasswordResponse SendResetPasswordRequest(ResetPasswordRequestRpc request)
        {
            try
            {
                return _bus.Request<ResetPasswordRequestRpc, ChangePasswordResponse>(request);
            }
            catch
            {
                return new ChangePasswordResponse { Success = false, MessageBody = SystemFailure, UserName = null, };
            }
        }
    }
}