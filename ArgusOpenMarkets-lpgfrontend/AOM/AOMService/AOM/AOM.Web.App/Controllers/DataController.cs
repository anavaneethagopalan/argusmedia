﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AOM.App.Domain.Entities;
using AOM.Services.ProductService;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Events.Products;
using AOM.Web.App.Security;

using Argus.Transport.Infrastructure;

using Utils.Javascript.Extension;
using Utils.Logging.Utils;

using WebApi.OutputCache.V2;

namespace AOM.Web.App.Controllers
{
    [CheckSessionToken]
    public class DataController : BaseController
    {
        private readonly IBus _bus;
        private readonly IProductService _productService;
        private const int CacheTimeOutForDataSeconds = 30;

        public DataController(IBus bus, IProductService productService)
        {
            _bus = bus;
            _productService = productService;
        }

        [HttpGet]
        [Route("api/getAllMarketAndProducts")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage GetAllMarketAndProducts()
        {
            string json = string.Empty;
            try
            {
                var marketsAndProducts = _productService.GetAllMarketsAndProducts();
                json = marketsAndProducts.ToJsonCamelCase();
            }
            catch (Exception ex)
            {
                Log.Error("Error in WebService - getAllMarkets", ex);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getAllPrincipals")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage GetAllPrincipals(long userId, long productId)
        {
            string json = string.Empty;
            try
            {
                if (!IsUserIdSameAsHeaderUserId(userId))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                var principalForBrokerRequest = new GetPrincipalsForBrokerRpc { ProductId = productId, RequestedByUserId = userId };
                var principalFromBroker = _bus.Request<GetPrincipalsForBrokerRpc, GetPrincipalsForBrokerResponse>(principalForBrokerRequest);
                var permissions = principalFromBroker.PrincipalsWithPermissions;
                json = permissions.ToJsonCamelCase();
            }
            catch (Exception ex)
            {
                Log.Error("Error in WebService - GetAllPrincipals", ex);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getAllPrincipalsBuyOrSell")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage GetAllPrincipalsBuyOrSell(long userId, long productId, string orderType)
        {
            string json = string.Empty;
            try
            {
                if (!IsUserIdSameAsHeaderUserId(userId))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }


                OrderType ot = GetOrderType(orderType);
                var principalForBrokerRequest = new GetPrincipalsForBrokerBuyOrSellRpc { ProductId = productId, RequestedByUserId = userId, OrderType = ot };
                var result = _bus.Request<GetPrincipalsForBrokerBuyOrSellRpc, GetPrincipalsForBrokerBuyOrSellResponse>(principalForBrokerRequest);
                json = result.Organisations.ToJsonCamelCase();
            }
            catch (Exception ex)
            {
                Log.Error("Error in WebService - GetAllPrincipals", ex);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        private OrderType GetOrderType(string orderType)
        {
            OrderType ot;

            if (orderType.ToUpper() == "BID")
            {
                orderType = "B";
            }
            if (orderType.ToUpper() == "ASK")
            {
                orderType = "A";
            }
            try
            {
                ot = DtoMappingAttribute.FindEnumWithValue<OrderType>(orderType);
            }
            catch (Exception ex)
            {
                Log.Error("Error casting Order Type to Enum.  Order Type string" + orderType, ex);
                ot = OrderType.None;
            }

            return ot;
        }

        [HttpGet]
        [Route("api/getAllBrokers")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage GetAllBrokers(long userId, long productId)
        {
            string json = string.Empty;
            try
            {
                if (!IsUserIdSameAsHeaderUserId(userId))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                var brokersForPrincipalRequest = new GetBrokersForPrincipalRpc { ProductId = productId, RequestedByUserId = userId };
                var brokersForPrincipal = _bus.Request<GetBrokersForPrincipalRpc, GetBrokersForPrincipalResponse>(brokersForPrincipalRequest).BrokersWithPermissions;
                json = brokersForPrincipal.ToJsonCamelCase();
            }
            catch (Exception ex)
            {
                Log.Error("Error in WebService - GetAllBrokers", ex);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getAllBrokersBuyOrSell")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage GetAllBrokers(long userId, long productId, string orderType)
        {
            string json = string.Empty;
            try
            {
                if (!IsUserIdSameAsHeaderUserId(userId))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                OrderType ot = GetOrderType(orderType);
                if (ot == OrderType.Ask || ot == OrderType.Bid)
                {
                    var brokersForPrincipalRequest = new GetBrokersForPrincipalBuyOrSellRpc
                    {
                        ProductId = productId,
                        RequestedByUserId = userId,
                        OrderType = ot
                    };


                    var brokersForPrincipal =
                        _bus.Request<GetBrokersForPrincipalBuyOrSellRpc, GetBrokersForPrincipalBuyOrSellResponse>(
                            brokersForPrincipalRequest).BrokersWithPermissions;
                    json = brokersForPrincipal.ToJsonCamelCase();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error in WebService - GetAllBrokers", ex);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        private bool IsUserIdSameAsHeaderUserId(long userId)
        {
            return userId == GetUserId();
        }

        [HttpGet]
        [Route("api/getProductSystemPrivileges")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage GetProductSystemPrivileges()
        {
            string json = string.Empty;
            try
            {
                var productPrivilegesResponse = _bus.Request<GetProductSystemPrivilegesRequestRpc, GetProductSystemPrivilegesResponse>(new GetProductSystemPrivilegesRequestRpc());
                json = productPrivilegesResponse.ToJsonCamelCase();
            }
            catch (Exception ex)
            {
                Log.Error("Error in WebService - GetProductSystemPrivileges", ex);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }

        [HttpGet]
        [Route("api/getProductSettings")]
        [CacheOutput(ClientTimeSpan = CacheTimeOutForDataSeconds, ServerTimeSpan = CacheTimeOutForDataSeconds)]
        public HttpResponseMessage GetProductSettings()
        {
            string json = string.Empty;
            try
            {
                var userId = GetUserId();
                var productSettingsResponse = _bus.Request<GetProductSettingsRequestRpc, GetProductSettingsResponse>(new GetProductSettingsRequestRpc { UserId = userId });
                json = productSettingsResponse.ProductSettings.ToJsonCamelCase();
            }
            catch (Exception ex)
            {
                Log.Error("Error in WebService - GetProductSettings", ex);
            }
            return CreateResponseMessage(HttpStatusCode.OK, json);
        }
        
    }
}