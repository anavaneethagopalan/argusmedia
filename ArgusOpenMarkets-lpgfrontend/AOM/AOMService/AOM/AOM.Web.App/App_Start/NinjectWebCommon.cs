using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
//using Ninject.Extensions.Conventions;
//using Ninject.Modules;

using WebActivatorEx;

using AOM.App.Domain.Services;
using AOM.App.Domain.Dates;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AOM.Services.AuthenticationService;
using AOM.Services.CrmService;
using AOM.Services.CrmService.TopicReduction;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Services.PermissionService;
using AOM.Transport.Service.Processor.Common.ArgusBus;
using AOM.Web.App;
using AOM.Web.App.Security;

using AOM.Services.EncryptionService;
using Utils.Logging.Utils;

using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]

namespace AOM.Web.App
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();
        public static IKernel Kernel { get; private set; }

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            
            Kernel = kernel;
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                kernel.Bind<IIpService>().To<IpService>();
                kernel.Bind<IEncryptionService>().To<EncryptionService>();
                kernel.Bind<IDbContextFactory>().To<DbContextFactory>();
                kernel.Bind<IDateTimeProvider>().To<DateTimeProvider>();
                kernel.Bind<IUserService>().To<UserService>();
                kernel.Bind<IUserLoginTrackingService>().To<UserLoginTrackingService>();
                kernel.Bind<IUserTokenService>().To<UserTokenService>();
                kernel.Bind<ICredentialsService>().To<CredentialsService>();
                kernel.Bind<ICrmAdministrationService>().To<CrmAdministrationService>();
                kernel.Bind<IUserAuthenticationService>().To<UserAuthenticationService>();
                kernel.Bind<IBilateralPermissionsBuilder>().To<BilateralPermissionsBuilder>();
                kernel.Bind<IOrganisationService>().To<OrganisationService>();
                kernel.Bind<ITopicReduction>().To<TopicReductionService>();
                kernel.Bind<IProductService>().To<ProductService>();
                kernel.Bind<ICounterpartyPermissionAuthenticationService>().To<CounterpartyPermissionAuthenticationService>();
                kernel.Bind<IBrokerPermissionAuthenticationService>().To<BrokerPermissionAuthenticationService>();
                kernel.Bind<IEmailService>().To<EmailService>();
                kernel.Bind<IErrorService>().To<ErrorService>();
                kernel.Bind<IBrokerPermissionService>().To<BrokerPermissionService>();
                kernel.Bind<ICounterpartyPermissionService>().To<CounterpartyPermissionService>();
                
                var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
                kernel.Bind<IBus>().ToMethod(context => Argus.Transport.Transport.CreateBus(aomConnectionConfiguration, null, new BusLogger()));
                kernel.Bind<ITokenInspector>().To<TokenInspector>();

                // Set Web API Resolver
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

                //ADD THIS?
                WarmUp(kernel);

                return kernel;
            }
            catch (Exception ex)
            {
                Log.Error("Error initialising ninject in Web.App - CreateKernel", ex);
                kernel.Dispose();
                throw;
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (var dbCrm = dbFactory.CreateUserModel())
                {
                    dbCrm.UserInfoes.Any();
                }

                Log.Info(string.Format("WebService - Initial EF user model build took {0} msecs.", timer.ElapsedMilliseconds));
            }
            catch (Exception ex)
            {
                Log.Error("WebService - WarmUp Error - cannot connect to DB", ex);
            }
        }
    }
}
