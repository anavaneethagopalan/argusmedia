﻿namespace AOM.Web.App
{
    using System.Web.Http;
    using System.Web.Http.Cors;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            //var cors = new EnableCorsAttribute("*", "*", "*");
            //config.EnableCors(cors);

            //var tokenInspector = new TokenInspector { InnerHandler = new HttpControllerDispatcher(config) };

            config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            config.Routes.MapHttpRoute("Authentication", "api/login", new { controller = "Login" });
            config.Routes.MapHttpRoute("ForgetPassword", "api/forgotPassword", new { controller = "Login" });
            config.Routes.MapHttpRoute("ResetPassword", "api/resetPassword", new { controller = "Login" });
            config.Routes.MapHttpRoute("PrincipalsForBroker", "api/getPrincipalsForBroker", new { controller = "Data" });
            config.Routes.MapHttpRoute("BrokersForPrincipal", "api/getBrokersForPrincipal", new { controller = "Data" });
        }
    }
}