﻿
namespace AOM.Web.App.Security
{
    public interface ITokenInspector
    {
        bool InspectUserToken(string clientIpOfRequest, string sessionTokenOfRequest, bool ignoreCache);
    }
}
