﻿namespace AOM.Web.App.Security
{
    using System;
    using System.Net;

    public static class IpAddressExtensions
    {
        private static IPAddress GetNetworkAddress(this IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length) throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
            }

            return new IPAddress(broadcastAddress);
        }
      
        public static bool IsInSameSubnet(this IPAddress address2, IPAddress address, IPAddress subnetMask)
        {
            try
            {
                IPAddress network1 = address.MapToIPv4().GetNetworkAddress(subnetMask);
                IPAddress network2 = address2.MapToIPv4().GetNetworkAddress(subnetMask);

                return network1.Equals(network2);
            }
            catch (ArgumentOutOfRangeException)
            {                
                //Work around for bug: http://stackoverflow.com/questions/23608829/why-does-ipaddress-maptoipv4-throw-argumentoutofrangeexception
                return false;
            }
        }
    }
}