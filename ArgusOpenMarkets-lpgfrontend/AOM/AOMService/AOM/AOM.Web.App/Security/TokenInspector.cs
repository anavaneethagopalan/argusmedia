﻿namespace AOM.Web.App.Security
{
    using System;
    using System.Runtime.Caching;
    using AOM.App.Domain.Entities;
    using AOM.Transport.Events.Users;
    using Argus.Transport.Infrastructure;
    using Utils.Logging.Utils;

    public class TokenInspector : ITokenInspector
    {
        private readonly IBus _bus;
        private static MemoryCache _memoryCache = MemoryCache.Default;
        private double _cacheExpirationMins = 30;

        private CacheItemPolicy CachePolicy
        {
            get
            {
                return new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTimeOffset.Now.LocalDateTime.AddMinutes(_cacheExpirationMins)
                };
            }
        }

        public TokenInspector(IBus bus)
        {
            _bus = bus;
        }

        public bool InspectUserToken(string userId, string sessionTokenOfRequest, bool ignoreCache)
        {
            try
            {
                if (String.IsNullOrEmpty(sessionTokenOfRequest))
                {
                    return false;
                }
                Log.Info(String.Format("Inspecting token {0} with userId {1}. ", sessionTokenOfRequest, userId));

                //check the cache - if the token is there, check the IP
                if (!ignoreCache && _memoryCache.Contains(sessionTokenOfRequest))
                {
                    if (_memoryCache.Get(sessionTokenOfRequest) as String == userId)
                    {
                        return true;
                    }

                    Log.Debug(String.Format("Denying request for token {0} with userId {1}. token is in cache but the username is different!", sessionTokenOfRequest, userId));
                    return false;
                }
                else //token not in cache, check the DB
                {
                    Log.Debug(String.Format(" token {0} with userId {1} is not in cache, going to db... ", sessionTokenOfRequest, userId));

                    //make a call to DB to get the usertoken
                    var sessionTokenAuthRequest = new SessionTokenAuthenticatonRequest
                    {
                        SessionTokenString = sessionTokenOfRequest,
                        UserId = userId
                    };

                    var authResult = _bus.Request<SessionTokenAuthenticatonRequest, AuthenticationResult>(sessionTokenAuthRequest);

                    if (authResult.IsAuthenticated)
                    {
                        //if it is in DB, then add it to the cache
                        _memoryCache.Add(sessionTokenOfRequest, userId, CachePolicy);
                        Log.Info(String.Format("Allowing request for token {0} with userId {1}. The token has been added to cache.", sessionTokenOfRequest, userId));
                        return true;
                    }
                }

                Log.Debug(String.Format("Denying request for token {0} with userId {1}. The token is not in cache or in DB", sessionTokenOfRequest, userId));
                return false;
            }

            catch (Exception ex)
            {
                Log.Error(String.Format("Exception within InspectUserToken for token {0} with userId {1}.", sessionTokenOfRequest, userId), ex);
                return false;
            }
        }
    }
}