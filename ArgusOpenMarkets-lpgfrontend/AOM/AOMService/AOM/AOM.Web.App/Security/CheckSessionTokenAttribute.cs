﻿namespace AOM.Web.App.Security
{
    using System.Net;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.Controllers;

    using Ninject;

    public class CheckSessionTokenAttribute : AuthorizeAttribute
    {
        private readonly ITokenInspector _tokenInspector;

        public bool IgnoreCache { get; set; }

        public CheckSessionTokenAttribute()
        {

            _tokenInspector = NinjectWebCommon.Kernel.Get<ITokenInspector>();
        }

        protected override bool IsAuthorized(HttpActionContext httpContext)
        {
            //var clientIpOfRequest = _ipService.GetIpAddressOfRemoteConnection();
            
            var sessionTokenOfRequest = HttpContext.Current.Request.Headers["token"];
            var userid = HttpContext.Current.Request.Headers["userid"];

            return _tokenInspector.InspectUserToken(userid, sessionTokenOfRequest, IgnoreCache);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            if (filterContext.Response == null)
            {
                filterContext.Response = new System.Net.Http.HttpResponseMessage();
            }
            filterContext.Response.StatusCode = HttpStatusCode.Forbidden;
        }
    }
}