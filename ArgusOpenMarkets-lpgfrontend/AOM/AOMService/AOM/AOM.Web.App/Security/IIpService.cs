﻿namespace AOM.Web.App.Security
{
    public interface IIpService
    {
        string GetIpAddressOfRemoteConnection();
    }
}
