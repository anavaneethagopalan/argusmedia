﻿using Utils.Logging.Utils;

namespace AOM.Web.App.Security
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.Controllers;

    public class RestrictToLocalHostAndSubnetFilterAttribute : AuthorizeAttribute
    {
        public static readonly IPAddress[] ServerIpAddressList = GetLocalServerIpList();
        
        protected override bool IsAuthorized(HttpActionContext httpContext)
        {
            try
            {
                var clientIpOfRequest = new IpService().GetIpAddressOfRemoteConnection();

                Log.Debug(string.Format("Authorisation Check :Client IP:{0}. Server IP:{1}", clientIpOfRequest, String.Join(", ",ServerIpAddressList.Select(i=>i.ToString()))) );

                return httpContext.Request.RequestUri.IsLoopback||
                        ServerIpAddressList.Any(ipAddress => IsIpInSameSubnet(ipAddress, clientIpOfRequest));
            }
            catch (Exception ex)
            {
                Log.Error("An authorisation error occured which may impact Diffusion.", ex);
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            filterContext.Response.StatusCode = HttpStatusCode.Forbidden;
        }

        protected static IPAddress[] GetLocalServerIpList()
        {
            IPHostEntry ipE = Dns.GetHostEntry(HttpContext.Current.Server.MachineName);
            return ipE.AddressList.ToArray();
        }

        private static bool IsIpInSameSubnet(IPAddress authorizedIp, string clientIp)
        {
            if (!String.IsNullOrEmpty(clientIp))
            {
                if (clientIp.Contains(':'))
                {
                    clientIp = clientIp.Split(':')[0];
                }
                IPAddress parsedClientIp;
                if (IPAddress.TryParse(clientIp, out parsedClientIp))
                {
                    return parsedClientIp.IsInSameSubnet(authorizedIp, SubnetMask.ClassB);
                }
            }
            return false;
        }
    }
}