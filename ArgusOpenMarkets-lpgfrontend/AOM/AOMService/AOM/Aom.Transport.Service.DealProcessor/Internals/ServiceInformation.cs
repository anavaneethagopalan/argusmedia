﻿using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.DealProcessor.Internals
{
    class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get { return DisplayName.Replace(" ", ""); }
        }

        public string DisplayName
        {
            get { return "AOM Transport Service Deal Processor"; }
        }

        public string Description
        {
            get { return "AOM Transport Service Deal Processor"; }
        }
    }
}
