﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Services.ExternalDealService;
using AOM.Transport.Events;
using AOM.Transport.Events.ExternalDeals;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.DealProcessor.Consumers
{
    internal class ExternalDealRequestConsumer : ExternalDealConsumerBase
    {
        public ExternalDealRequestConsumer(IDbContextFactory dbFactory, IExternalDealService externalDealService, IUserService userService, 
            IEmailService emailService, IBus aomBus, IErrorService errorService)
            : base(dbFactory, externalDealService, userService, emailService, aomBus, errorService)
        { }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomExternalDealRequest>(SubscriptionId, ConsumeExternalDealRequest<AomExternalDealRequest, AomExternalDealAuthenticationRequest>);
            _aomBus.Subscribe<AomExternalDealAuthenticationResponse>(SubscriptionId, ConsumeExternalDealResponse<AomExternalDealAuthenticationResponse, AomExternalDealResponse>);
        }

        internal override ExternalDeal ConsumeExternalDealResponseAdditionalProcessing<T>(IAomModel aomDb, T response, out UserContactDetails userContactDetails)
        {
            userContactDetails = null;

            switch (response.Message.MessageAction)
            {
                case MessageAction.Create:
                    return _externalDealService.CreateExternalDeal(aomDb, response);

                case MessageAction.Request:
                    var ed = _externalDealService.GetExternalDealById(aomDb, response);
                    userContactDetails = _userService.GetContactDetails(ed.LastUpdatedUserId);
                    return ed;

                case MessageAction.Void:
                    return _externalDealService.VoidExternalDeal(aomDb, response);

                case MessageAction.Update:
                    return _externalDealService.EditExternalDeal(aomDb, response);

                case MessageAction.Verify:
                    return _externalDealService.VerifyExternalDeal(aomDb, response);
            }
            return null;
        }
    }
}