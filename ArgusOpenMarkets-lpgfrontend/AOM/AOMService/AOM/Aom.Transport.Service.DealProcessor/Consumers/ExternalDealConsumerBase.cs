﻿using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Services.ExternalDealService;
using AOM.Transport.Events;
using AOM.Transport.Events.ExternalDeals;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.DealProcessor.Consumers
{
    public abstract class ExternalDealConsumerBase : IConsumer
    {
        protected readonly IDbContextFactory _dbFactory;
        protected readonly IExternalDealService _externalDealService;
        protected IBus _aomBus;
        protected readonly IUserService _userService;
        protected readonly IEmailService _emailService;
        private readonly IErrorService _errorService;

        protected ExternalDealConsumerBase(IDbContextFactory dbFactory, IExternalDealService externalDealService, IUserService userService,
            IEmailService emailService, IBus aomBus, IErrorService errorService)
        {
            _dbFactory = dbFactory;
            _externalDealService = externalDealService;
            _aomBus = aomBus;
            _errorService = errorService;
            _userService = userService;
            _emailService = emailService;
        }

        public void Start()
        {
            SubscribeToEvents();
        }

        internal virtual void SubscribeToEvents()
        {
            throw new NotImplementedException("You must subscribe to the events to consume in this method");
        }
        
        internal void ConsumeExternalDealRequest<TReq, TFwd>(TReq request) where TReq : AomExternalDealRequest where TFwd : AomExternalDealAuthenticationRequest, new()
        {
            string requestType = typeof (TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd
                {
                    MessageAction = request.MessageAction,
                    ClientSessionInfo = request.ClientSessionInfo,
                    ExternalDeal = request.ExternalDeal
                };
                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                var logError = _errorService.LogUserError(new UserError
                {
                    Error = exception,
                    AdditionalInformation = String.Format("Authenticating {0}", requestType),
                    ErrorText = string.Empty,
                    Source = "ExternalDealConsumerBase"
                });

                _aomBus.PublishErrorResponse<AomExternalDealResponse>(MessageAction.Update, request.ClientSessionInfo, logError);
            }
        }

        internal void ConsumeExternalDealResponse<TResp, TFwd>(TResp response) where TResp : AomExternalDealAuthenticationResponse where TFwd : AomExternalDealResponse, new()
        {
            string responseType = typeof (TResp).Name;

            Log.Debug(String.Format("{0} received", responseType));
            try
            {
                if (response.Message.MessageType == MessageType.Error)
                {
                    _aomBus.PublishErrorResponseAndLogError<AomExternalDealResponse>(response.Message.MessageAction, response.Message.ClientSessionInfo, response.Message.MessageBody as string);
                }
                else
                {
                    using (var aomDb = _dbFactory.CreateAomModel())
                    {
                        UserContactDetails returnedUserContactDetails;
                        var returnedExternalDeal = ConsumeExternalDealResponseAdditionalProcessing(aomDb, response, out returnedUserContactDetails);
                        try
                        {
                            var forwardedResponse = new TFwd
                            {
                                ExternalDeal = returnedExternalDeal,
                                UserContactDetails = returnedUserContactDetails,
                                Message = response.Message
                            };
                            _aomBus.Publish(forwardedResponse);
                        }
                        catch (Exception exception)
                        {
                            _errorService.LogUserError(new UserError
                            {
                                Error = exception,
                                AdditionalInformation = String.Format("{0} error", responseType),
                                ErrorText = string.Empty,
                                Source = "ExternalDealConsumerBase"
                            });
                        }
                    }
                }
            }
            catch (BusinessRuleException exception)
            {
                _aomBus.PublishErrorResponseAndLogInfo<AomExternalDealResponse>(response.Message.MessageAction, response.Message.ClientSessionInfo, exception.Message);
            }
            catch (Exception exception)
            {
                var logError = _errorService.LogUserError(new UserError
                {
                    Error = exception,
                    AdditionalInformation = String.Format("{0} error", responseType),
                    ErrorText = string.Empty,
                    Source = "ExternalDealConsumerBase"
                });

                _aomBus.PublishErrorResponse<AomExternalDealResponse>( response.Message.MessageAction, response.Message.ClientSessionInfo, logError);
            }
        }

        internal virtual ExternalDeal ConsumeExternalDealResponseAdditionalProcessing<T>(IAomModel aomDb, T response, out UserContactDetails userContactDetails) 
            where T : AomExternalDealAuthenticationResponse
        {
            throw new NotImplementedException("You must add code in here if consuming responses. This is only called if the request being consumed is not indicating an error.");
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}