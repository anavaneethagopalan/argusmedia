﻿using AOM.Services.ErrorService;

namespace AOM.Transport.Service.DealProcessor.Consumers
{
    using AOM.App.Domain;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Aom;
    using AOM.Services.DealService;
    using AOM.Services.EmailService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Deals;
    using AOM.Transport.Events.Emails;
    using AOM.Transport.Events.Users;
    using AOM.Transport.Service.Processor.Common;
    using Argus.Transport.Infrastructure;
    using System;
    using Utils.Logging.Utils;

    public abstract class InternalDealConsumerBase : IConsumer
    {
        protected readonly IDbContextFactory _dbFactory;

        protected readonly IDealService _dealService;

        protected IBus _aomBus;

        private readonly IErrorService _errorService;

        protected readonly IUserService _userService;

        protected readonly IEmailService _emailService;

        protected InternalDealConsumerBase(
            IDbContextFactory dbFactory,
            IDealService dealService,
            IUserService userService,
            IEmailService emailService,
            IBus aomBus, 
            IErrorService errorService)
        {
            _dbFactory = dbFactory;
            _dealService = dealService;
            _aomBus = aomBus;
            _errorService = errorService;
            _userService = userService;
            _emailService = emailService;
        }

        public void Start()
        {
            SubscribeToEvents();
        }

        internal virtual void SubscribeToEvents()
        {
            throw new NotImplementedException("You must subscribe to the events to consume in this method");
        }


        internal void ConsumeDealRequest<TReq, TFwd>(TReq request) where TReq : AomDealRequest
            where TFwd : AomDealAuthenticationRequest, new()
        {
            string requestType = typeof (TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd
                {
                    MessageAction = request.MessageAction,
                    ClientSessionInfo = request.ClientSessionInfo,
                    Deal = request.Deal
                };
                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                var logError =
                    _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = String.Format("Authenticating {0}", requestType),
                        ErrorText = string.Empty,
                        Source = "InternalDealRouterService"
                    });

                _aomBus.PublishErrorResponse<AomDealResponse>(
                    MessageAction.Update,
                    request.ClientSessionInfo,
                    logError);
            }
        }

        internal void ConsumeDealResponse<TResp, TFwd>(TResp response) where TResp : AomDealAuthenticationResponse
            where TFwd : AomDealResponse, new()
        {
            string responseType = typeof (TResp).Name;

            Log.Debug(String.Format("{0} received", responseType));
            try
            {
                if (response.Message.MessageType == MessageType.Error)
                {
                    _aomBus.PublishErrorResponseAndLogError<AomDealResponse>(
                        response.Message.MessageAction,
                        response.Message.ClientSessionInfo,
                        response.Message.MessageBody as string);
                }
                else
                {
                    using (var aomDb = _dbFactory.CreateAomModel())
                    {
                        UserContactDetails returnedUserContactDetails;
                        var returnedDeal = ConsumeDealResponseAdditionalProcessing(
                            aomDb,
                            response,
                            out returnedUserContactDetails);
                        try
                        {
                            var forwardedResponse = new TFwd
                            {
                                Deal = returnedDeal,
                                UserContactDetails = returnedUserContactDetails,
                                Message = response.Message
                            };
                            _aomBus.Publish(forwardedResponse);

                            if (response.Message.MessageAction == MessageAction.Verify
                                || response.Message.MessageAction == MessageAction.Void)
                            {
                                var notificationEmailIds = _emailService.CreateInternalDealEmails(aomDb, returnedDeal);

                                foreach (var emailId in notificationEmailIds)
                                {
                                    var sendEmailRequest = new AomSendEmailRequest
                                    {
                                        MessageAction = MessageAction.Send,
                                        EmailId = emailId
                                    };

                                    _aomBus.Publish(sendEmailRequest);
                                }
                            }

                        }
                        catch (Exception exception)
                        {
                            _errorService.LogUserError(new UserError
                            {
                                Error = exception,
                                AdditionalInformation = String.Format("{0} error", responseType),
                                ErrorText = string.Empty,
                                Source = "InternalDealRouterService"
                            });
                        }
                    }
                }
            }
            catch (BusinessRuleException exception)
            {
                _aomBus.PublishErrorResponseAndLogInfo<AomDealResponse>(
                    response.Message.MessageAction,
                    response.Message.ClientSessionInfo,
                    exception.Message);
            }
            catch (Exception exception)
            {
                var logError =
                    _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = String.Format("{0} error", responseType),
                        ErrorText = string.Empty,
                        Source = "InternalDealConsumerBase"
                    });

                _aomBus.PublishErrorResponse<AomDealResponse>(
                    response.Message.MessageAction,
                    response.Message.ClientSessionInfo,
                    logError);
            }
        }

        internal virtual Deal ConsumeDealResponseAdditionalProcessing<T>(
            IAomModel aomDb,
            T response,
            out UserContactDetails userContactDetails) where T : AomDealAuthenticationResponse
        {
            throw new NotImplementedException(
                "You must add code in here if consuming responses. This is only called if the request being consumed is not indicating an error.");
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}