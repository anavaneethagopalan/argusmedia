﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.DealService;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Services.OrderService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOM.Transport.Events.Orders;
using Argus.Transport.Infrastructure;
using System.Linq;

namespace AOM.Transport.Service.DealProcessor.Consumers
{
    internal class InternalDealRequestConsumer : InternalDealConsumerBase
    {
        private readonly IOrderService _orderService;

        private readonly IProductService _productService;

        public InternalDealRequestConsumer(
            IDbContextFactory dbFactory,
            IDealService dealService,
            IUserService userService,
            IEmailService emailService,
            IBus aomBus,
            IProductService productService,
            IOrderService orderService,
            IErrorService errorService)
            : base(dbFactory, dealService, userService, emailService, aomBus, errorService)
        {
            _orderService = orderService;
            _productService = productService;
        }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomDealRequest>(
                SubscriptionId,
                ConsumeDealRequest<AomDealRequest, AomDealAuthenticationRequest>);
            _aomBus.Subscribe<AomDealAuthenticationResponse>(
                SubscriptionId,
                ConsumeDealResponse<AomDealAuthenticationResponse, AomDealResponse>);
        }

        internal override Deal ConsumeDealResponseAdditionalProcessing<T>(
            IAomModel aomDb,
            T response,
            out UserContactDetails userContactDetails)
        {
            userContactDetails = null;

            switch (response.Message.MessageAction)
            {
                case MessageAction.Create:
                    return _dealService.CreateDeal(aomDb, response.Deal);

                case MessageAction.Request:
                    var ed = _dealService.GetDealById(aomDb, response);
                    userContactDetails = _userService.GetContactDetails(ed.LastUpdatedUserId);
                    return ed;

                case MessageAction.Void:
                    return VoidDeal(aomDb, response).MessageBody as Deal; // TODO: This needs cleaning up

                case MessageAction.Update:
                    return _dealService.EditDeal(aomDb, response.Deal).MessageBody as Deal;
                    // TODO: This needs cleaning up

                case MessageAction.Verify:
                    return _dealService.VerifyDeal(aomDb, response);
            }
            return null;
        }

        private Message VoidDeal(IAomModel aomDb, AomDealAuthenticationResponse response)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                bool dealWasExecuted = response.Deal.DealStatus == DealStatus.Executed;

                var voidDealResponse = _dealService.VoidDeal(aomDb, response.Deal);

                if (dealWasExecuted)
                {
                    VoidDealOrders(aomDb, response);
                }

                trans.Complete();

                return voidDealResponse;
            }
        }

        private void VoidDealOrders(IAomModel aomDb, AomDealAuthenticationResponse response)
        {
            var marketStatus = _productService.GetMarketStatus(response.Deal.Initial.ProductId);
            var dealOrders = new[] {response.Deal.Initial, response.Deal.Matching};
            var orderAuthResponses =
                dealOrders.Where(o => o != null)
                    .Select(
                        o =>
                            new AomOrderAuthenticationResponse
                            {
                                MarketMustBeOpen = false,
                                Order = o,
                                Message =
                                    new Message
                                    {
                                        MessageAction = MessageAction.Void,
                                        MessageBody = o,
                                        ClientSessionInfo =
                                            response.Message
                                                .ClientSessionInfo,
                                        MessageType = MessageType.Order
                                    }
                            });
            var voidedOrders = orderAuthResponses.Select(oar => _orderService.VoidOrder(aomDb, oar, marketStatus));

            var orderResponses =
                voidedOrders.Select(
                    vo =>
                        new AomOrderResponse
                        {
                            Order = vo,
                            Message =
                                new Message
                                {
                                    ClientSessionInfo = response.Message.ClientSessionInfo,
                                    MessageAction = MessageAction.Void,
                                    MessageType = MessageType.Order,
                                    MessageBody = null
                                }
                        });

            foreach (var aomOrderResponse in orderResponses)
            {
                _aomBus.Publish(aomOrderResponse);
            }
        }
    }
}