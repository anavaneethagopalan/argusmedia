﻿using System.Collections.Generic;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.DealProcessor
{
    class DealProcessorService : ProcessorServiceBase
    {
        public DealProcessorService(IBus aomBus, IEnumerable<IConsumer> consumers, IServiceManagement serviceManagement) :
            base(consumers, serviceManagement, aomBus)
        {

        }

        public override string Name
        {
            get { return "DealProcessorService"; }
        }
    }
}