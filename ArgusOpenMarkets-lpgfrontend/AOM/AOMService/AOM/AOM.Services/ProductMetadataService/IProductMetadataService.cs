﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Services.ProductMetadataService
{
    public interface IProductMetadataService
    {
        ProductMetaDataItem GetProductMetaDataById(long productMetaDataId);
        ProductMetaData GetProductMetaData(long productId);
        List<ProductMetaData> GetAllProductMetaData();
        void AddProductMetadataItem(ProductMetaDataItem productMetaDataItem, long userId);
        void UpdateProductMetadataItem(ProductMetaDataItem productMetaDataItem, long userId);
        void DeleteProductMetadataItem(long productMetadataItemId, long userId);
        List<MetaDataList> GetProductMetadataLists();
        void UpdateProductMetadataItemsDisplayOrder(long productId, long[] metadataItemIds, long userId);
        void UpdateProductMetadataLists(IEnumerable<MetaDataList> metadataLists, long userId);
    }
}