﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using Utils.Logging.Utils;

namespace AOM.Services.ProductMetadataService
{
    public class ProductMetadataService : IProductMetadataService
    {
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IDbContextFactory _dbContextFactory;

        public ProductMetadataService(IDbContextFactory dbContextFactory, IDateTimeProvider dateTimeProvider)
        {
            _dbContextFactory = dbContextFactory;
            _dateTimeProvider = dateTimeProvider;
        }

        public ProductMetaDataItem GetProductMetaDataById(long productMetaDataId)
        {
            ProductMetaData metaDataItem =
                GetAllProductMetaData().FirstOrDefault(p => p.Fields.Any(f => f.Id == productMetaDataId));
            return metaDataItem != null ? metaDataItem.Fields[0] : null;
        }

        public ProductMetaData GetProductMetaData(long productId)
        {
            ProductMetaData metaData = GetAllProductMetaData().FirstOrDefault(p => p.ProductId == productId);
            if (metaData != null)
                Log.Info(string.Format("Found 1 ProductMetaDataItem for product id: {0}", productId));
            return metaData;
        }

        public List<ProductMetaData> GetAllProductMetaData()
        {
            Log.Info("GetAllProductMetaData has been called");

            try
            {
                var productsMetaDataDefinitions = new List<ProductMetaData>();

                try
                {
                    using (var context = _dbContextFactory.CreateProductMetaDataModel())
                    {
                        var data = context.ProductMetaDataItems.AsNoTracking()
                            .Where(x => !x.IsDeleted)
                            .Include(p => p.MetaDataList.MetaDataListItems)
                            .ToList();

                        productsMetaDataDefinitions.AddRange(from md in data
                            group md by md.ProductId
                            into g
                            select new ProductMetaData()
                            {
                                ProductId = g.Key,
                                Fields = g.OrderBy(itm => itm.DisplayOrder).Select(itm => itm.ToEntity()).ToArray()
                            });
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("GetAllProductMetaData", ex);
                    throw;
                }

                return productsMetaDataDefinitions;
            }
            catch (Exception ex)
            {
                Log.Error("Error Getting Product Definitions", ex);
                Trace.WriteLine(ex);
            }

            return new List<ProductMetaData>();
        }

        public void AddProductMetadataItem(ProductMetaDataItem productMetaDataItem, long userId)
        {
            ProductMetaDataItemDto metadataItemDto = null;
            try
            {
                metadataItemDto = productMetaDataItem.ToDto();
            }
            catch (Exception ex)
            {
                Log.Error("AddProductMetaDataItem - Error", ex);
            }

            if (metadataItemDto != null)
            {
                try
                {

                    using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                    {
                        using (var metadataModel = _dbContextFactory.CreateProductMetaDataModel())
                        {
                            var existingProductMetadataItems = metadataModel.ProductMetaDataItems
                                .Where(x => x.ProductId == productMetaDataItem.ProductId && !x.IsDeleted)
                                .ToArray();

                            var lastDisplayOrder = existingProductMetadataItems.Length == 0
                                ? 0
                                : existingProductMetadataItems.Max(x => x.DisplayOrder);

                            metadataItemDto.DisplayOrder = lastDisplayOrder + 1;
                            metadataItemDto.LastUpdated = _dateTimeProvider.UtcNow;
                            ValidateProductMetadataItem(metadataItemDto);

                            metadataModel.ProductMetaDataItems.Add(metadataItemDto);
                            metadataModel.SaveChangesAndLog(userId);
                        }

                        transactionScope.Complete();
                    }

                }
                catch (BusinessRuleException businessEx)
                {
                    throw businessEx;
                }
                catch (Exception ex)
                {
                    Log.Error("Error Adding Product Meta Data", ex);
                }
            }
        }

        public void UpdateProductMetadataItem(ProductMetaDataItem productMetaDataItem, long userId)
        {
            using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                using (var metadataModel = _dbContextFactory.CreateProductMetaDataModel())
                {
                    var existingMetadataItemDto = metadataModel.ProductMetaDataItems.FirstOrDefault(x => x.Id == productMetaDataItem.Id);
                    if (existingMetadataItemDto == null)
                        throw new BusinessRuleException("Could not update unexisting metadata item.");

                    existingMetadataItemDto.Update(productMetaDataItem);
                    existingMetadataItemDto.LastUpdated = _dateTimeProvider.UtcNow;
                    ValidateProductMetadataItem(existingMetadataItemDto);
                    
                    metadataModel.SaveChangesAndLog(userId);
                }

                transactionScope.Complete();
            }
        }

        public void DeleteProductMetadataItem(long productMetadataItemId, long userId)
        {
            using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                using (var metadataModel = _dbContextFactory.CreateProductMetaDataModel())
                {
                    var metadataItemDto = metadataModel.ProductMetaDataItems.FirstOrDefault(x => x.Id == productMetadataItemId);
                    if (metadataItemDto == null)
                        throw new BusinessRuleException("Could not delete unexisting product metadata item.");

                    if (metadataItemDto.IsDeleted)
                        throw new BusinessRuleException("Product metadata item is already deleted.");

                    metadataItemDto.IsDeleted = true;
                    metadataItemDto.LastUpdated = _dateTimeProvider.UtcNow;
                    metadataModel.SaveChangesAndLog(userId);
                }

                transactionScope.Complete();
            }
        }

        private void ValidateProductMetadataItem(ProductMetaDataItemDto productMetaDataItem)
        {
            if (string.IsNullOrEmpty(productMetaDataItem.DisplayName))
                throw new BusinessRuleException("Product metadata item should have display name.");

            switch (productMetaDataItem.MetaDataTypeId)
            {
                case MetaDataTypes.IntegerEnum:
                    if (!productMetaDataItem.MetaDataListId.HasValue)
                        throw new BusinessRuleException("Product metadata item should have metadata list set.");
                    break;
                case MetaDataTypes.String:
                    if (!productMetaDataItem.ValueMaximum.HasValue ||
                        !productMetaDataItem.ValueMinimum.HasValue)
                        throw new BusinessRuleException("Product metadata item should have value minimum and value maximum set.");
                    if (productMetaDataItem.ValueMaximum <= productMetaDataItem.ValueMinimum)
                        throw new BusinessRuleException("Product metadata item should have value maximum greater than value minimum.");
                    break;
                default:
                    throw new BusinessRuleException("Unknown metadata item type.");
            }
        }

        public void UpdateProductMetadataItemsDisplayOrder(long productId, long[] metadataItemIds, long userId)
        {
            using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                using (var metadataModel = _dbContextFactory.CreateProductMetaDataModel())
                {
                    var metadataItems = metadataModel.ProductMetaDataItems.Where(x => x.ProductId == productId && !x.IsDeleted).ToArray();
                    if (metadataItems.Length != metadataItemIds.Length)
                        throw new BusinessRuleException("Length of metadata items ids should be equal to the metadata items count in database for the particular product.");

                    foreach (var metadataItem in metadataItems)
                    {
                        if (metadataItem.DisplayOrder != 0)
                            metadataItem.DisplayOrder = 0;
                    }

                    for (int i = 0; i < metadataItemIds.Length; i++)
                    {
                        var metadataItem = metadataItems.FirstOrDefault(x => x.Id == metadataItemIds[i]);
                        if (metadataItem == null)
                            throw new BusinessRuleException("Could not set display order for not existing or deleted metadata item.");

                        metadataItem.DisplayOrder = i + 1;
                        metadataItem.LastUpdated = _dateTimeProvider.UtcNow;
                    }

                    metadataModel.SaveChangesAndLog(userId);
                }

                transactionScope.Complete();
            }
        }

        public List<MetaDataList> GetProductMetadataLists()
        {
            using (var metadataModel = _dbContextFactory.CreateProductMetaDataModel())
            {
                return metadataModel.MetaDataLists
                    .AsNoTracking()
                    .Where(x => !x.IsDeleted)
                    .Include(x => x.MetaDataListItems)
                    .AsEnumerable()
                    .Select(x => x.ToEntity())
                    .ToList();
            }
        }

        public void UpdateProductMetadataLists(IEnumerable<MetaDataList> metadataLists, long userId)
        {
            using (var metadataModel = _dbContextFactory.CreateProductMetaDataModel())
            {
                var existingMetadataLists = metadataModel.MetaDataLists.Include(x => x.MetaDataListItems).Where(x => !x.IsDeleted).ToArray();

                foreach (var metadataList in existingMetadataLists)
                {
                    if (!metadataLists.Any(x => x.Id == metadataList.Id))
                    {
                        DeleteMetadataList(metadataModel, metadataList);
                    }
                }

                foreach (var metadataList in metadataLists)
                {
                    if (existingMetadataLists.Any(x => x.Id == metadataList.Id))
                    {
                        UpdateProductMetadataList(metadataModel, metadataList);
                    }
                    else
                    {
                        AddMetadataList(metadataModel, metadataList);
                    }
                }

                metadataModel.SaveChangesAndLog(userId);
            }
        }

        private void DeleteMetadataList(IProductMetaDataModel metadataModel, MetaDataListDto metadataList)
        {
            var referencingMetadataItem = metadataModel.ProductMetaDataItems.FirstOrDefault(x => !x.IsDeleted && x.MetaDataListId == metadataList.Id);
            if (referencingMetadataItem != null)
            {
                throw new BusinessRuleException(string.Format(
                    "Could not delete \"{0}\" metadata list as it is already referenced by \"{1}\" metadata item.",
                    metadataList.Description,
                    referencingMetadataItem.DisplayName));
            }

            metadataList.IsDeleted = true;

            foreach (var metaDataListItemDto in metadataList.MetaDataListItems)
            {
                metaDataListItemDto.IsDeleted = true;
            }
        }

        private void AddMetadataList(IProductMetaDataModel metadataModel, MetaDataList metadataList)
        {
            ValidateMetadataList(metadataList);
            metadataModel.MetaDataLists.Add(metadataList.ToDto());
        }

        private void UpdateProductMetadataList(IProductMetaDataModel metadataModel, MetaDataList metadataList)
        {
            ValidateMetadataList(metadataList);

            var existingMetadataList = metadataModel.MetaDataLists
                .Include(x => x.MetaDataListItems)
                .FirstOrDefault(x => x.Id == metadataList.Id && !x.IsDeleted);

            if (existingMetadataList == null)
                throw new BusinessRuleException("Could not update unexisting or deleted metadata list.");

            existingMetadataList.Description = metadataList.Description;

            var existingMetadataListItems = existingMetadataList.MetaDataListItems.ToArray();

            foreach (var listItem in existingMetadataListItems.Where(x => !x.IsDeleted))
            {
                var newListItem = metadataList.FieldList.FirstOrDefault(x => x.Id == listItem.Id);
                if (newListItem == null)
                {
                    listItem.IsDeleted = true;
                }
            }

            int currentDisplayOrder = 1;
            foreach (var metaDataListItem in metadataList.FieldList)
            {
                var existingMetadataListItem = existingMetadataListItems.FirstOrDefault(x => x.Id == metaDataListItem.Id && !x.IsDeleted);
                if (existingMetadataListItem == null)
                {
                    var sameMetadataListItem = existingMetadataListItems.FirstOrDefault(
                        x => x.MetaDataListId == metadataList.Id && 
                            x.ValueLong == metaDataListItem.ItemValue && 
                            x.ValueText == metaDataListItem.DisplayName && 
                            x.IsDeleted);

                    if (sameMetadataListItem != null)
                    {
                        sameMetadataListItem.IsDeleted = false;
                        existingMetadataListItem = sameMetadataListItem;
                    }
                }
                
                if (existingMetadataListItem == null)
                {
                    existingMetadataListItem = new MetaDataListItemDto { Id = metaDataListItem.Id };
                    existingMetadataList.MetaDataListItems.Add(existingMetadataListItem);
                }

                existingMetadataListItem.ValueText = metaDataListItem.DisplayName;
                existingMetadataListItem.ValueLong = metaDataListItem.ItemValue;
                existingMetadataListItem.DisplayOrder = currentDisplayOrder;
                currentDisplayOrder++;
            }
        }

        private void ValidateMetadataList(MetaDataList metadataList)
        {
            if (string.IsNullOrEmpty(metadataList.Description))
                throw new BusinessRuleException("Metadata list should have not empty description.");

            if (metadataList.FieldList == null || metadataList.FieldList.Length == 0)
                throw new BusinessRuleException("Metadata list should have at least one item.");

            foreach (var listItem in metadataList.FieldList)
            {
                if (string.IsNullOrEmpty(listItem.DisplayName))
                    throw new BusinessRuleException("Metadata list item should have not empty display name.");
            }
        }
    }
}
