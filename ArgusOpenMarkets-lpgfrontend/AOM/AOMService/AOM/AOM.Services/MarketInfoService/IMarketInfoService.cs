﻿namespace AOM.Services.MarketInfoService
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;
    using AOM.Transport.Events.MarketInfos;

    public interface IMarketInfoService
    {
        MarketInfo CreateMarketInfo(IAomModel aomDb, AomMarketInfoAuthenticationResponse request);
        MarketInfo GetMarketInfoItemById(IAomModel db, AomMarketInfoAuthenticationResponse request);
        MarketInfo VoidMarketInfo(IAomModel aomDb, AomMarketInfoAuthenticationResponse request);
        MarketInfo EditMarketInfo(IAomModel aomDb, AomMarketInfoAuthenticationResponse request);
        MarketInfo VerifyMarketInfo(IAomModel aomDb, AomMarketInfoAuthenticationResponse request);
    }
}