﻿using AOM.Repository.MySql;

namespace AOM.Services.MarketInfoService
{
    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.Repository.MySql.Aom;
    using AOM.Transport.Events.MarketInfos;
    using ServiceStack.Text;
    using System;
    using System.Linq;
    using System.Transactions;
    using Utils.Logging.Utils;

    public class MarketInfoService : IMarketInfoService
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        public MarketInfoService(IDateTimeProvider dateTimeProvider)
        {
            _dateTimeProvider = dateTimeProvider;
        }

        public MarketInfo CreateMarketInfo(IAomModel aomDb, AomMarketInfoAuthenticationResponse response)
        {
            long databaseAssignedId;
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {

                try
                {
                    var newMarketInfoDto = response.MarketInfo.ToDto();
                    newMarketInfoDto.LastUpdated = _dateTimeProvider.UtcNow;
                    newMarketInfoDto.LastUpdatedUserId = response.Message.ClientSessionInfo.UserId;
                    newMarketInfoDto.OwnerOrganisationId = response.Message.ClientSessionInfo.OrganisationId;
                    aomDb.MarketInfoItems.Add(newMarketInfoDto);
                    aomDb.SaveChangesAndLog(response.Message.ClientSessionInfo.UserId);

                    foreach (var id in response.MarketInfo.ProductIds)
                    {
                        aomDb.MarketInfoProductChannels.Add(
                            new MarketInfoProductChannelDto
                            {
                                MarketInfoItemId = newMarketInfoDto.Id,
                                ProductId = id
                            });
                    }

                    databaseAssignedId = newMarketInfoDto.Id;

                    aomDb.SaveChangesAndLog(response.Message.ClientSessionInfo.UserId);
                    trans.Complete();
                }
                catch (Exception ex)
                {
                    Log.Error("AOM.Services.MarketInfoService.CreateMarketInfo", ex);
                    return null;
                }

            }

            return GetMarketInfoItemById(aomDb, databaseAssignedId);
        }

        public MarketInfo GetMarketInfoItemById(IAomModel db, AomMarketInfoAuthenticationResponse response)
        {
            return GetMarketInfoItemById(db, response.MarketInfo.Id);
        }

        public MarketInfo GetMarketInfoItemById(IAomModel db, long marketInfoId)
        {
            try
            {
                var marketInfoById = _GetMarketInfoDtoById(db, marketInfoId).ToEntity();

                // Build the market info products list.
                marketInfoById.ProductIds =
                    db.MarketInfoProductChannels.Where(x => x.MarketInfoItemId == marketInfoById.Id)
                        .Select(x => x.ProductId)
                        .ToList();
                return marketInfoById;

            }
            catch (Exception ex)
            {
                Log.Error("GetMarketInfoItemById", ex);
                return null;
            }
        }

        private MarketInfoDto _GetMarketInfoDtoById(IAomModel db, long marketInfoId)
        {
            try
            {
                var repositoryMarketInfoItemDto =
                    (from d in db.MarketInfoItems where marketInfoId == d.Id select d).SingleOrDefault();

                if (repositoryMarketInfoItemDto == null)
                {
                    throw new BusinessRuleException(
                        String.Format("Error: MarketInfo {0} does not exist.", marketInfoId));
                }
                return repositoryMarketInfoItemDto;
            }
            catch (Exception ex)
            {
                Log.Error("_GetMarketInfoDtoById", ex);
                throw;
            }
        }

        public MarketInfo VoidMarketInfo(IAomModel aomDb, AomMarketInfoAuthenticationResponse response)
        {
            response.MarketInfo.MarketInfoStatus = MarketInfoStatus.Void;
            return _EditMarketInfo(aomDb, response);
        }

        public MarketInfo VerifyMarketInfo(IAomModel aomDb, AomMarketInfoAuthenticationResponse response)
        {
            response.MarketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            return _EditMarketInfo(aomDb, response);
        }

        public MarketInfo EditMarketInfo(IAomModel aomDb, AomMarketInfoAuthenticationResponse response)
        {
            // NB; we only want to mark this as updated if it's already active.  Pending edits should remain pending.
            if (response.MarketInfo.MarketInfoStatus == MarketInfoStatus.Active)
                response.MarketInfo.MarketInfoStatus = MarketInfoStatus.Updated;
            return _EditMarketInfo(aomDb, response);
        }

        private MarketInfo _EditMarketInfo(
            IAomModel aomDb,
            AomMarketInfoAuthenticationResponse response)
        {
            long newid = -1;
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                var currentMarketInfoDto = _GetMarketInfoDtoById(aomDb, response.MarketInfo.Id);

                MarketInfoDto updatedMarketInfoDto = response.MarketInfo.ToDto();

                if (ValidateMarketInfo(updatedMarketInfoDto, currentMarketInfoDto))
                {
                    response.MarketInfo.ToDto(currentMarketInfoDto);

                    currentMarketInfoDto.LastUpdated = _dateTimeProvider.UtcNow;
                    currentMarketInfoDto.LastUpdatedUserId = response.Message.ClientSessionInfo.UserId;

                    try
                    {
                        aomDb.SaveChangesAndLog(response.Message.ClientSessionInfo.UserId);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error Saving changes and logging for Market Info Service", ex);
                    }


                    var channelsToRemove =
                        aomDb.MarketInfoProductChannels.Where(x => x.MarketInfoItemId == response.MarketInfo.Id)
                            .ToList();
                    foreach (var c in channelsToRemove)
                    {
                        aomDb.MarketInfoProductChannels.Remove(c);
                    }

                    foreach (var id in response.MarketInfo.ProductIds)
                    {
                        aomDb.MarketInfoProductChannels.Add(
                            new MarketInfoProductChannelDto
                            {
                                MarketInfoItemId = currentMarketInfoDto.Id,
                                ProductId = id
                            });
                    }

                    newid = currentMarketInfoDto.Id;

                    try
                    {
                        aomDb.SaveChangesAndLog(response.Message.ClientSessionInfo.UserId);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error Saving changes and logging for Market Info Service", ex);
                    }
                }

                trans.Complete();
            }

            return GetMarketInfoItemById(aomDb, newid);
        }

        private Boolean ValidateMarketInfo(MarketInfoDto newState, MarketInfoDto previousState = null)
        {
            if (previousState != null)
            {
                if (previousState.LastUpdated.RoundToMs() != newState.LastUpdated.RoundToMs()) //TODO Precision Issue
                {
                    throw new BusinessRuleException("Market Info item has already been updated by another user.");
                }

                if (previousState.MarketInfoStatus == DtoMappingAttribute.GetValueFromEnum(MarketInfoStatus.Void))
                {
                    throw new BusinessRuleException("The Market Info item is void and cannot be amended.");
                }
            }

            return true;
        }
    }
}