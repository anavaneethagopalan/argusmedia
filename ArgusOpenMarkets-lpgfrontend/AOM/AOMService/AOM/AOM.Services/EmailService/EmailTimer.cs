﻿using System.Timers;

namespace AOM.Services.EmailService
{
    internal class EmailTimer : Timer, IEmailTimer
    {
        public EmailTimer(int timeoutMilliseconds)
            : base(timeoutMilliseconds)
        {
        }
    }
}
