namespace AOM.Services.EmailService
{
    public interface IEmailTimerService
    {
        void Start();
        void Stop();
    }
}