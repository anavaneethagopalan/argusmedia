﻿using System.Timers;
using AOM.Transport.Events;
using AOM.Transport.Events.Emails;
using Argus.Transport.Infrastructure;

namespace AOM.Services.EmailService
{
    public class EmailTimerService : IEmailTimerService
    {
        private IEmailTimer _timer;
        private readonly IBus _aomBus;
        private readonly ITimerFactory _timerFactory;

        public EmailTimerService(IBus aomBus, ITimerFactory timerFactory)
        {
            _aomBus = aomBus;
            _timerFactory = timerFactory;
        }

        public void Start()
        {
            PublishEmailNotificationCheck();

            if (_timer != null)
            {
                Stop();
            }

            // 10 minute timer
            _timer = _timerFactory.CreateTimer(60000); //ToDo: Parameterise this outside of the application
            _timer.Elapsed += timer_Elapsed;
            _timer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PublishEmailNotificationCheck();
        }

        private void PublishEmailNotificationCheck()
        {
            var request = new AomSendEmailRequest
            {
                MessageAction = MessageAction.Check,
            };

            _aomBus.Publish(request);
        }

        public void Stop()
        {
            if(_timer != null)
            {
                _timer.Stop();
                _timer.Dispose();
            }
        }
    }
}