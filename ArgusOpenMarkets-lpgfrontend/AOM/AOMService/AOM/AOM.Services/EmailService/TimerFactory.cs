﻿using System.Timers;

namespace AOM.Services.EmailService
{
    internal class TimerFactory : ITimerFactory
    {
        public IEmailTimer CreateTimer(int timeoutMilliseconds)
        {
            return new EmailTimer(timeoutMilliseconds);
        }
    }
}
