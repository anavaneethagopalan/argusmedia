using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.Services.EmailService
{
    public interface ISmtpService
    {
        void SendEmail(Email email);
    }
}