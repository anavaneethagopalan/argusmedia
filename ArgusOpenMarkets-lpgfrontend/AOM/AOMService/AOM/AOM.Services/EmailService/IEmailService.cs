﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.Services.EmailService
{
    public interface IEmailService
    {
        void SendAnyOutstandingEmailNotifications(IAomModel aomDb, ISmtpService smptService, string websiteUrl);
        long CreateEmail(IAomModel aomDb, DealEmailNotification notification);
        long CreateEmail(IAomModel aomDb, ForgotPasswordEmailNotification notification);
        IEnumerable<long> CreateEmail(IAomModel aomDb, TradingMatrixUpdateNotification notification);
        void SendEmailNotification(IAomModel aomDb, ISmtpService smptService, long emailId, string websiteUrl);
        IEnumerable<long> CreateInternalDealEmails(IAomModel aomDb, Deal deal);
    }
}
