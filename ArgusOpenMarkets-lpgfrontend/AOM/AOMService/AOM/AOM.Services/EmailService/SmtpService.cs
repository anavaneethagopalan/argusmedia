﻿using System;
using System.Net.Mail;
using System.Text;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using Utils.Logging.Utils;

namespace AOM.Services.EmailService
{
    public class SmtpService : ISmtpService, IDisposable
    {
        private SmtpClient _client;

        public void SendEmail(Email email)
        {
            try
            {

            _client = new SmtpClient();

            var from = new MailAddress("donotreply@argusmedia.com");
            var to = new MailAddress(email.Recipient);

            var message = new MailMessage(from, to)
            {
                Body = email.Body,
                BodyEncoding = Encoding.UTF8,
                Subject = email.Subject,
                SubjectEncoding = Encoding.UTF8
            };
            
            _client.Send(message);

            DisposeSmtpClient();

            }
            catch (Exception ex)
            {
                Log.Error("SendEmail - Exception sending the email id: " + email.Id, ex);
            }
        }

        private void DisposeSmtpClient()
        {
            if (_client != null)
            {
                _client.Dispose();
            }
        }

        public void Dispose()
        {
            DisposeSmtpClient();
        }
    }
}
