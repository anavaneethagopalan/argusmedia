﻿using System.Text;
using JetBrains.Annotations;

namespace AOM.Services.EmailService
{
    public static class StringBuilderExtensions
    {
        // Here we append a tab to each line to prevent Outlook from auto-cleaning line breaks
        // and messing up the format of our emails:
        // http://stackoverflow.com/questions/247546/outlook-autocleaning-my-line-breaks-and-screwing-up-my-email-format
        // http://stackoverflow.com/questions/136052/how-do-i-format-a-string-in-an-email-so-outlook-will-print-the-line-breaks/1638608#1638608
        public static StringBuilder AppendEmailLine(this StringBuilder builder, string s)
        {
            builder.AppendLine(string.Format("{0}\t", s));
            return builder;
        }

        [StringFormatMethod("fmt")]
        public static StringBuilder AppendEmailLine(this StringBuilder builder, string fmt, params object[] args)
        {
            return AppendEmailLine(builder, string.Format(fmt, args));
        }

        public static StringBuilder AppendBlankEmailLine(this StringBuilder builder)
        {
            builder.AppendLine(string.Format("\t"));
            return builder;
        }
    }
}