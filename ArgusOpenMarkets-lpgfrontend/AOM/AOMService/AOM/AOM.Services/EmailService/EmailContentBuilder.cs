﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.ProductService;
using Utils.Logging.Utils;

namespace AOM.Services.EmailService
{
    public static class EmailContentBuilder
    {
        public const string AomSupportDetails =
            "For AOM technical support issues please contact London +44(0)207 199 9430, Singapore +65 6496 9896 or email AOMsupport@argusmedia.com.";

        public const string ArgusAddress = "Argus Media Limited, Argus House, 175 St John Street, London EC1V 4LW";

        public const string CompanyRegistration = "Registered in England and Wales, Company Registration No: 1642534";

        public const string VatRegistration = "VAT Registration No: GB 229 7149 41";

        public const string LiabilityDisclaimer =
            "Argus is not liable for any errors or omissions contained in this email and recommends that you confirm all details with your counterparts.";

        public const string GeneralDisclaimer =
            "The information contained in this email and its attachments is confidential and may be the subject of " +
            "legal, professional or other privilege. It is intended only for the named addressees and may not be " +
            "disclosed to anyone else without consent from Argus Media. If you are not the named addressee you " +
            "must not use, disclose, distribute, copy, print or rely on the contents of this email and should " +
            "destroy it immediately. Whilst Argus Media takes care to protect its systems from electronic virus " +
            "attack or other harmful event, the firm gives no warranty that this email message (including any " +
            "attachments to it) is free of any virus or other harmful matter and accepts no responsibility for " +
            "any loss or damage resulting from the recipient receiving, opening or using it.";

        public static string CreateEmailBody(DealEmailNotification notification)
        {
            var body = new StringBuilder();

            body.AppendEmailLine("Deal Date: {0:dd-MMM-yyyy}", notification.DateCreated);

            if (notification.DealStatus == DealStatus.Executed)
            {
                body.AppendEmailLine("Deal Time: {0:HH:mm:ss} (UTC)", notification.DealDate);
            }

            body.AppendEmailLine("Product: {0}", notification.ProductName);
            body.AppendEmailLine("Price ({0}/{1}): {2:F}", notification.Currency, notification.PriceUnits, notification.Price);
            body.AppendEmailLine("Quantity ({0}): {1:##,###}", notification.Units, notification.Quantity);

            if (notification.DeliveryStartDate != null && notification.DeliveryEndDate != null)
            {
                var deliveryPeriod = GetDeliveryPeriod(notification.DeliveryPeriodLabel);
                body.AppendEmailLine("{2}: {0:dd-MMM-yyyy} to {1:dd-MMM-yyyy}", notification.DeliveryStartDate, notification.DeliveryEndDate, deliveryPeriod);
            }

            body.AppendEmailLine("Buyer Company: {0}", notification.BuyerCompanyName);

            if (!string.IsNullOrEmpty(notification.BuyerBrokerOrganisationName))
            {
                body.AppendEmailLine(!string.IsNullOrEmpty(notification.SellerBrokerOrganisationName) ? 
                    "Buyer Broker Company: {0}" : "Broker Company: {0}", notification.BuyerBrokerOrganisationName);
            }

            body.AppendEmailLine("Seller Company: {0}", notification.SellerCompanyName);
            if (!string.IsNullOrEmpty(notification.SellerBrokerOrganisationName))
            {
                body.AppendEmailLine(!string.IsNullOrEmpty(notification.BuyerBrokerOrganisationName) ?
                    "Seller Broker Company: {0}" : "Broker Company: {0}", notification.SellerBrokerOrganisationName);
            }

            if (notification.OrderMetaData != null)
            {
                foreach (var metaData in notification.OrderMetaData)
                {
                    body.AppendEmailLine("{0}: {1}", metaData.DisplayName, metaData.DisplayValue);
                }
            }

            if (!string.IsNullOrEmpty(notification.DealNotes))
            {
                body.AppendEmailLine(string.Format("Deal Notes: {0}", notification.DealNotes));
            }

            if (notification.DealStatus == DealStatus.Void)
            {
                body.AppendBlankEmailLine();
                var voidReason = DealPresentation.ToPresentationVoidReason(notification.DealVoidReason);
                body.AppendEmailLine(string.Format("Deal Voided: {0}", voidReason));
            }

            body.AppendBlankEmailLine();
            body.AppendEmailLine(LiabilityDisclaimer);
            body.AppendBlankEmailLine();

            return body.ToString();
        }

        private static string GetDeliveryPeriod(string deliveryPeriod)
        {
            if (!string.IsNullOrEmpty(deliveryPeriod))
            {
                return deliveryPeriod;
            }

            return "Delivery Period";
        }

        public static string CreateEmailSubject(DealEmailNotification notification)
        {
            switch (notification.DealStatus)
            {
                case DealStatus.Void:
                    return string.Format("AOM Deal Voided - Deal Id: {0}", notification.DealId);
                default:
                    return string.Format("AOM Deal - Deal Id: {0}", notification.DealId);
            }
        }

        public static string CreateEmailBody(ForgotPasswordEmailNotification notification)
        {
            var body = new StringBuilder(100);
            body.AppendEmailLine("Please click the following link to reset your password:");
            body.AppendBlankEmailLine();
            body.AppendEmailLine("{0}/ResetPassword?t={1}", notification.Url, notification.Password);
            body.AppendBlankEmailLine();
            return body.ToString();
        }

        public static string CreateEmailSubject(ForgotPasswordEmailNotification notification)
        {
            return "Reset Password - AOM";
        }

        public static string CreateEmailBody(TradingMatrixUpdateNotification notification, IOrganisationService organisationService, IProductService productService, long organisationId, bool filter)
        {
            var body = new StringBuilder();

            body.AppendEmailLine("The following trade permissions have been amended in AOM:");
            body.AppendBlankEmailLine();
            body.AppendEmailLine("Update Date/Time: {0}", notification.MatrixPermissions[0].LastUpdated);
            string ourOrgName = organisationService.GetOrganisationById(notification.MatrixPermissions[0].OurOrganisation).Name;
            body.AppendEmailLine("Organisation initiating permission amendment: {0}", ourOrgName);

            List<long> productIds = notification.MatrixPermissions.Select(p => p.ProductId).Distinct().ToList();

            foreach (long productId in productIds)
            {
                body.AppendEmailLine("Product: {0}", productService.GetProduct(productId).Name);
                bool addItem = true;
                foreach (var matrixPermission in notification.MatrixPermissions.Where(p => p.ProductId == productId))
                {

                    if (filter)
                    {
                        Log.Info("WE ARE FILTERING COUNTERPARTY PERMS Their Org Id:" + organisationId);
                        if (matrixPermission.TheirOrganisation != organisationId)
                        {
                            Log.Info("WE ARE FILTERING COUNTERPARTY FILTER IS TRUE");
                            addItem = false;

                        }
                        else
                        {
                            addItem = true;
                        }
                    }

                    if (addItem)
                    {
                        body.AppendEmailLine("\tPermission Amendment: {0} {1} {2} {3}", ourOrgName,
                            GetEmailTextAllowDeny(matrixPermission.AllowOrDeny.ToString()),
                            GetEmailTextBuyingSellingPermission(matrixPermission.BuyOrSell),
                            organisationService.GetOrganisationById(matrixPermission.TheirOrganisation).Name);
                    }
                }
            }
            body.AppendBlankEmailLine();
            body.AppendEmailLine(LiabilityDisclaimer);
            body.AppendBlankEmailLine();

            return body.ToString();
        }

        private static string GetEmailTextBuyingSellingPermission(BuyOrSell buyOrSell)
        {
            switch (buyOrSell)
            {
                case BuyOrSell.Buy:
                    return "Buying from";
                case BuyOrSell.Sell:
                    return "Selling to";
                default:
                    return "None";
            }
        }

        private static object GetEmailTextAllowDeny(string allowOrDeny)
        {
            switch (allowOrDeny.ToLower())
            {
                case "allow":
                    return "allows";
                default:
                    return "denies";
            }
        }

        public static string AppendStandardAomEmailFooter(string toAppendTo, string websiteUrl)
        {
            var bld = new StringBuilder(toAppendTo);
            bld.AppendEmailLine(GeneralDisclaimer);
            bld.AppendBlankEmailLine();
            bld.AppendEmailLine(ArgusAddress);
            bld.AppendEmailLine(CompanyRegistration);
            bld.AppendEmailLine(VatRegistration);
            bld.AppendBlankEmailLine();
            bld.AppendEmailLine("This email was generated by Argus Open Markets - " + websiteUrl);
            bld.AppendEmailLine("This is an automatically generated email. Please do not respond.");
            bld.AppendEmailLine(AomSupportDetails);
            bld.AppendBlankEmailLine();
            return bld.ToString();
        }

        public static string CreateEmailSubject(TradingMatrixUpdateNotification notification)
        {
            switch (notification.BrokerOrCounterpartyMatrixUpdate)
            {
                case SystemEventType.CounterPartyPermissionChanges:
                    return "Argus Media AOM : Counterparty Permission Update Notification";
                case SystemEventType.BrokerMatrixChanges:
                    return "Argus Media AOM : Broker Permission Update Notification";
                default:
                    throw new BusinessRuleException(String.Format("Unknown notification type: {0}", notification.BrokerOrCounterpartyMatrixUpdate));
            }
        }
    }
}