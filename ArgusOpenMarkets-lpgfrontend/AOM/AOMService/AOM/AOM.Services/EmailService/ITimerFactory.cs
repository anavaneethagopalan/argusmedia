﻿
namespace AOM.Services.EmailService
{
    public interface ITimerFactory
    {
        IEmailTimer CreateTimer(int timeoutMilliseconds);
    }
}
