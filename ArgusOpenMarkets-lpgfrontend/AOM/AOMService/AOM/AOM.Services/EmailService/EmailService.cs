﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Transactions;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.ProductService;

using Utils.Logging.Utils;

namespace AOM.Services.EmailService
{
    public class Recipient
    {
        public Recipient(string email, long organisationId)
        {

            Email = email;
            OrganisationId = organisationId;
        }

        public string Email { get; set; }
        public long OrganisationId { get; set; }
    }

    public class EmailService : IEmailService
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly IProductService _productService;
        private readonly IDateTimeProvider _dateTimeProvider;

        public EmailService(IUserService userService, IOrganisationService organisationService,IDateTimeProvider dateTimeProvider, IProductService productService)
        {
            _userService = userService;
            _organisationService = organisationService;
            _dateTimeProvider = dateTimeProvider;
            _productService = productService;
        }

        public void SendAnyOutstandingEmailNotifications(IAomModel aomDb, ISmtpService smptService, string websiteUrl)
        {
            var pendingStatus = DtoMappingAttribute.GetValueFromEnum(EmailStatus.Pending);
            var pendingEmailIds = aomDb.Emails.Where(en => en.Status == pendingStatus).Select(em => em.Id).ToList();

            foreach (var emailId in pendingEmailIds)
            {
                SendEmailNotification(aomDb, smptService, emailId, websiteUrl);
            }
        }

        public long CreateEmail(IAomModel aomDb, DealEmailNotification notification)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                var newEmailDto = new EmailDto
                {
                    DateCreated = _dateTimeProvider.UtcNow,
                    Status = DtoMappingAttribute.GetValueFromEnum(EmailStatus.Pending),
                    Recipient = notification.Recipient,
                    Subject = EmailContentBuilder.CreateEmailSubject(notification),
                    Body = EmailContentBuilder.CreateEmailBody(notification),
                };

                aomDb.Emails.Add(newEmailDto);
                aomDb.SaveChangesAndLog(-1);
                trans.Complete();
                return newEmailDto.Id;
            }
        }

        public long CreateEmail(IAomModel aomDb, ForgotPasswordEmailNotification notification)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                var newEmailDto = new EmailDto
                {
                    DateCreated = _dateTimeProvider.UtcNow,
                    Status = DtoMappingAttribute.GetValueFromEnum(EmailStatus.Pending),
                    Recipient = notification.Recipient,
                    Subject = EmailContentBuilder.CreateEmailSubject(notification),
                    Body = EmailContentBuilder.CreateEmailBody(notification),
                };

                aomDb.Emails.Add(newEmailDto);
                aomDb.SaveChangesAndLog(-1);
                trans.Complete();
                return newEmailDto.Id;
            }
        }

        public IEnumerable<long> CreateEmail(IAomModel aomDb, TradingMatrixUpdateNotification notification)
        {
            try
            {
                List<long> emailIdList = new List<long>();
                List<string> ourOrgContacts = new List<string>();
                List<string> theirOrgContacts = new List<string>();

                List<Recipient> thisSideRecipients = new List<Recipient>();
                List<Recipient> otherSideRecipients = new List<Recipient>();

                foreach (var matrixPermission in notification.MatrixPermissions)
                {
                    // List of our Recipients to email.   
                    ourOrgContacts.Clear();
                    ourOrgContacts = _organisationService.GetEmailContactsForEvent(notification.BrokerOrCounterpartyMatrixUpdate, matrixPermission.ProductId, matrixPermission.OurOrganisation);
                    foreach (var ourOrgContact in ourOrgContacts)
                    {
                        thisSideRecipients.Add(new Recipient(ourOrgContact, matrixPermission.OurOrganisation));
                    }

                    // List of their recipients to email
                    theirOrgContacts.Clear();
                    theirOrgContacts = _organisationService.GetEmailContactsForEvent(notification.BrokerOrCounterpartyMatrixUpdate, matrixPermission.ProductId, matrixPermission.TheirOrganisation);
                    foreach (var theirOrgContact in theirOrgContacts)
                    {
                        otherSideRecipients.Add(new Recipient(theirOrgContact, matrixPermission.TheirOrganisation));
                    }
                }

                // Send to our recipients - emails everyone.
                SendTradePermissionsChangeToRecipients(aomDb, notification, thisSideRecipients.GroupBy(r => r.Email).Select(r => r.First()).ToList(), emailIdList, false);
                SendTradePermissionsChangeToRecipients(aomDb, notification, otherSideRecipients.GroupBy(r => r.Email).Select(r => r.First()).ToList(), emailIdList, true);

                return emailIdList;
            }
            catch (Exception ex)
            {
                Log.Error("Error creating TradingMatrixUpdateNotification email: ", ex);
                return null;
            }
        }

        private void SendTradePermissionsChangeToRecipients(IAomModel aomDb, TradingMatrixUpdateNotification notification,
            List<Recipient> ourRecipients, List<long> emailIdList, bool filter)
        {
            foreach (var recipient in ourRecipients)
            {
                // Create email!
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {

                    var body = EmailContentBuilder.CreateEmailBody(notification, _organisationService, _productService, recipient.OrganisationId, filter);
                    var newEmailDto = new EmailDto
                    {
                        DateCreated = _dateTimeProvider.UtcNow,
                        Status = DtoMappingAttribute.GetValueFromEnum(EmailStatus.Pending),
                        Recipient = recipient.Email,
                        Subject = EmailContentBuilder.CreateEmailSubject(notification),
                        Body = body
                    };

                    aomDb.Emails.Add(newEmailDto);
                    aomDb.SaveChangesAndLog(-1);
                    trans.Complete();
                    emailIdList.Add(newEmailDto.Id);
                }
            }
        }

        public void SendEmailNotification(IAomModel aomDb, ISmtpService smptService, long emailId, string websiteUrl)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                var currentEmailDto = aomDb.Emails.Single(en => en.Id == emailId);
                currentEmailDto.Status = DtoMappingAttribute.GetValueFromEnum(EmailStatus.Sent);
                currentEmailDto.DateSent = _dateTimeProvider.UtcNow;
                aomDb.SaveChangesAndLog(-1);

                var email = currentEmailDto.ToEntity();
                email.Body = EmailContentBuilder.AppendStandardAomEmailFooter(email.Body, websiteUrl);
                smptService.SendEmail(email);

                trans.Complete();
            }
        }

        public IEnumerable<long> CreateInternalDealEmails(IAomModel aomDb, Deal deal)
        {
            var initialOrder = aomDb.Orders.SingleOrDefault(o => o.Id == deal.InitialOrderId).ToEntity(_organisationService, _userService);
            var matchingOrder = aomDb.Orders.SingleOrDefault(o => o.Id == deal.MatchingOrderId).ToEntity(_organisationService, _userService);
            var product = aomDb.Products.Single(p => p.Id == initialOrder.ProductId);

            var recipients = new List<string>();

            //get recipients for Principal
            recipients.AddRange(_organisationService.GetEmailContactsForEvent(SystemEventType.DealConfirmation, product.Id, initialOrder.PrincipalOrganisationId));
            recipients.AddRange(_organisationService.GetEmailContactsForEvent(SystemEventType.DealConfirmation, product.Id, matchingOrder.PrincipalOrganisationId));

            //get recipients for broker - initial order
            if(initialOrder.BrokerOrganisationId != null)
            {
                recipients.AddRange(_organisationService.GetEmailContactsForEvent(SystemEventType.DealConfirmation, product.Id, (long) initialOrder.BrokerOrganisationId));
            }

            if (matchingOrder.BrokerOrganisationId != null)
            {
                recipients.AddRange(_organisationService.GetEmailContactsForEvent( SystemEventType.DealConfirmation, product.Id, (long)matchingOrder.BrokerOrganisationId));
            }

            var dealEmail = new DealEmailNotification
            {
                DealId = deal.Id,
                DateCreated = _dateTimeProvider.UtcNow,
                DealDate = deal.CreatedDate,
                DealStatus = deal.DealStatus,
                Status = EmailStatus.Pending,
                Price = initialOrder.Price,
                Quantity = initialOrder.Quantity,
                PriceUnits = initialOrder.OrderPriceLines == null ? string.Empty : initialOrder.OrderPriceLines.First().PriceLine.PriceLinesBases.First().PriceBasis.PriceUnitCode, //TO CHANGE - WHICH PRICELINE TO USE
                Units = product.VolumeUnit,
                Currency = initialOrder.OrderPriceLines == null ? string.Empty : initialOrder.OrderPriceLines.First().PriceLine.PriceLinesBases.First().PriceBasis.CurrencyCode, //TO CHANGE - WHICH PRICELINE TO USE
                ProductName = initialOrder.ProductName,
                DeliveryStartDate = initialOrder.DeliveryStartDate,
                DeliveryEndDate = initialOrder.DeliveryEndDate,
                BuyerCompanyName = initialOrder.OrderType == OrderType.Bid ? initialOrder.PrincipalOrganisationName : matchingOrder.PrincipalOrganisationName,
                SellerCompanyName = initialOrder.OrderType == OrderType.Ask ? initialOrder.PrincipalOrganisationName : matchingOrder.PrincipalOrganisationName,
                //BuyerBrokerOrganisationName = (string.IsNullOrEmpty(initialOrder.BrokerOrganisationName)) ? string.Empty : initialOrder.BrokerOrganisationName,
                BuyerBrokerOrganisationName = initialOrder.OrderType == OrderType.Bid ? initialOrder.BrokerOrganisationName : matchingOrder.BrokerOrganisationName,
                //SellerBrokerOrganisationName = (string.IsNullOrEmpty(matchingOrder.BrokerOrganisationName)) ? string.Empty : matchingOrder.BrokerOrganisationName,
                SellerBrokerOrganisationName = initialOrder.OrderType == OrderType.Bid ? matchingOrder.BrokerOrganisationName : initialOrder.BrokerOrganisationName,
                DealAuthorisationDate = deal.DealStatus == DealStatus.Executed && deal.LastUpdated != null ? (DateTime)deal.LastUpdated : new DateTime(),
                DealNotes = initialOrder.Notes,
                DealVoidReason = deal.GetDealVoidReason(),
                OrderMetaData = initialOrder.MetaData,
                DeliveryPeriodLabel = product.DeliveryPeriodLabel,

                //TODO need to add the Location ID to Order as now can have multiple potential locations willl need to come from their not from Product.
                DeliveryLocationLabel = product.DeliveryLocations == null ? String.Empty : product.DeliveryLocations.First().LocationLabel
            };

            var emailIdList = new List<long>();

            foreach (var recipient in recipients)
            {
                var newEmail = dealEmail;
                newEmail.Recipient = recipient;
                var id = CreateEmail(aomDb, newEmail);
                emailIdList.Add(id);
            }

            return emailIdList;
        }
    }
}