﻿using System;
using System.Timers;

namespace AOM.Services.EmailService
{
    public interface IEmailTimer : IDisposable
    {
        void Start();
        void Stop();
        event ElapsedEventHandler Elapsed;
        bool Enabled { get; }
    }
}
