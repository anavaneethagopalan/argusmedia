﻿using AOM.App.Domain.Entities;
using System;

namespace AOM.Services.ErrorService
{
    public interface IErrorService
    {
        string LogException(Exception exception, string additionalInfo);

        string LogMessage(string errorText, string source);

        string LogUserError(UserError logUserErrorRequest);
    }
}