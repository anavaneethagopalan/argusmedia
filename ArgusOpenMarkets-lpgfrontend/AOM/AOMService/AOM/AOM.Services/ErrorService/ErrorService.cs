﻿using AOM.App.Domain.Entities;

namespace AOM.Services.ErrorService
{
    using AOM.App.Domain.Dates;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Aom;
    using System;
    using System.Transactions;

    public class ErrorService : IErrorService
    {
        private readonly IDbContextFactory _dbContextFactory;

        private readonly IDateTimeProvider _dateTimeProvider;

        private const int ProcessorUser = -1;

        public ErrorService(IDbContextFactory dbContextFactory, IDateTimeProvider dateTimeProvider)
        {
            _dbContextFactory = dbContextFactory;
            _dateTimeProvider = dateTimeProvider;
        }

        private long WriteErrorToTable(ErrorLogDto errorLogDto)
        {
            long errorId = -1;

            try
            {
                using (new TransactionScope(TransactionScopeOption.Suppress))
                {
                    using (IAomModel context = _dbContextFactory.CreateAomModel())
                    {
                        context.Errors.Add(errorLogDto);
                        context.SaveChangesAndLog(ProcessorUser);
                    }
                }

                errorId = errorLogDto.Id;
            }
            catch (Exception)
            {
                // ignored
            }

            return errorId;
        }

        public string LogException(Exception exception, string additionalInfo)
        {

            if (exception != null)
            {
                var errorLogDto = new ErrorLogDto
                {
                    DateCreated = _dateTimeProvider.UtcNow,
                    Message = Left(exception.Message, 500),
                    Source = Left(exception.Source, 200),
                    StackTrace = Left(exception.StackTrace, 4000),
                    Type = Left(exception.GetType().Name, 100),
                    AdditionalInfo = Left(additionalInfo, 200)
                };

                var errorId = WriteErrorToTable(errorLogDto);
                return string.Format("An internal system error has occurred.  Please contact Aom Support on aomsupport@argusmedia.com or call +44(0)2071999430.  The error reference code is #{0}.", errorId);
            }

            return string.Empty;
        }

        public string LogUserError(UserError userError)
        {
            string errorMessage = string.Empty;

            if (userError.Error != null)
            {
                errorMessage = LogException(userError.Error, userError.AdditionalInformation);
            }
            else
            {
                errorMessage = LogMessage(userError.ErrorText, userError.Source);
            }

            return errorMessage;
        }

        public string LogMessage(string errorText, string source)
        {
            var errorLogDto = new ErrorLogDto
            {
                DateCreated = _dateTimeProvider.UtcNow,
                Message = Left(errorText, 500),
                Source = Left(source, 200),
                StackTrace = "",
                Type = "Other"
            };

            try
            {
                var errorId = WriteErrorToTable(errorLogDto);
                return string.Format("An internal system error has occurred.  Please contact Aom Support on aomsupport@argusmedia.com or call +44(0)2071999430.  The error reference code is #{0}.", errorId);
            }
            catch (Exception)
            {
                return "An internal system error has occurred.  Please contact Aom Support on aomsupport@argusmedia.com or call +44(0)2071999430 if the problem persists.";
            }
        }

        private static string Left(string s, int length)
        {
            return string.IsNullOrEmpty(s) ? s : s.Substring(0, Math.Min(length, s.Length));
        }
    }
}