﻿namespace AOM.Services.DealService
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Deals;

    public interface IDealService
    {
        Deal CreateDeal(IAomModel aomDb, Deal deal);
        Message EditDeal(IAomModel aomDb, Deal deal);
        Message VoidDeal(IAomModel aomDb, Deal deal);
        Deal GetDealById(IAomModel db, AomDealAuthenticationResponse response);
        Deal VerifyDeal(IAomModel aomDb, AomDealAuthenticationResponse response);
    }
}