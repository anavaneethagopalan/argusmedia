﻿namespace AOM.Services.DealService
{
    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Deals;
    using ServiceStack.Text;
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Utils.Database;
    using AomRepository = Repository.MySql.Aom;

    public class DealService : IDealService
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly IDateTimeProvider _dateTimeProvider;

        public DealService(IUserService userService, IOrganisationService organisationService, IDateTimeProvider dateTimeProvider)
        {
            _userService = userService;
            _organisationService = organisationService;
            _dateTimeProvider = dateTimeProvider;
        }

        private static Boolean ValidateDeal(AomRepository.DealDto newState, AomRepository.DealDto previousState = null)
        {
            if (previousState != null)
            {
                if (previousState.LastUpdated.RoundToMs() != newState.LastUpdated.RoundToMs()) // TODO Precision Issue
                {
                    throw new BusinessRuleException("The deal has been changed by someone else since you started editing it.  Your request has not been successful.");
                }
                if (previousState.DealStatus == DtoMappingAttribute.GetValueFromEnum(DealStatus.Void))
                {
                    throw new BusinessRuleException("The deal is void and cannot be amended.");
                }
            }

            return true;
        }

        public Deal CreateDeal(AomRepository.IAomModel aomDb, Deal deal)
        {
            EnsureMarketIsOpen(aomDb, deal.Initial.ProductId);

            AomRepository.DealDto newDealDto = deal.ToDto();
            newDealDto.LastUpdated = _dateTimeProvider.UtcNow;
            newDealDto.EnteredByUserId = deal.LastUpdatedUserId;
            newDealDto.DateCreated = newDealDto.LastUpdated;

            if (ValidateDeal(newDealDto))
            {
                aomDb.Deals.Add(newDealDto);
                aomDb.SaveChangesAndLog(deal.LastUpdatedUserId);
                deal.Id = newDealDto.Id;
            }
            return newDealDto.ToEntity(_organisationService, _userService);
        }

        private Deal EditInternalDeal(AomRepository.IAomModel aomDb, Deal deal)
        {
            return ConcurrencyHelper.RetryOnConcurrencyException(string.Format("Attempting to update deal Id:{0}", deal.Id), 3, () =>
            {
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    AomRepository.DealDto currentDealDto = (from o in aomDb.Deals where deal.Id == o.Id select o).Single();

                    var updatedDealDto = deal.ToDto();

                    if (ValidateDeal(updatedDealDto, currentDealDto))
                    {
                        deal.LastUpdated = _dateTimeProvider.UtcNow;
                        deal.ToDto(currentDealDto);
                        aomDb.SaveChangesAndLog(deal.LastUpdatedUserId);
                        trans.Complete();
                    }
                    return currentDealDto.ToEntity(_organisationService, _userService);
                }
            });
        }

        public Message EditDeal(AomRepository.IAomModel aomDb, Deal deal)
        {
            EnsureMarketIsOpen(aomDb, deal.Initial.ProductId);
            var returnDeal = EditInternalDeal(aomDb, deal);

            return new Message
            {
                MessageAction = MessageAction.Update,
                MessageBody = returnDeal,
                MessageType = MessageType.Deal
            };
        }

        /// <summary>
        /// Attempts to set the specified deal into void state
        /// </summary>
        public Message VoidDeal(AomRepository.IAomModel aomDb, Deal deal)
        {
            deal.DealStatus = DealStatus.Void;
            EditInternalDeal(aomDb, deal);
            var returnDeal = GetInternalDealById(_organisationService, _userService, aomDb, deal.Id);

            return new Message
            {
                MessageAction = MessageAction.Void,
                MessageBody = returnDeal,
                MessageType = MessageType.Deal
            };
        }

        public Deal VerifyDeal(AomRepository.IAomModel aomDb, AomDealAuthenticationResponse response)
        {
            var existingDeal = GetInternalDealById(_organisationService, _userService, aomDb, response.Deal.Id);

            if (existingDeal.DealStatus == DealStatus.Executed)
            {
                throw new BusinessRuleException("Deal has already been verified");
            }

            response.Deal.DealStatus = DealStatus.Executed;
            return EditInternalDeal(aomDb, response.Deal);
        }

        public Deal GetDealById(AomRepository.IAomModel aomDb, AomDealAuthenticationResponse response)
        {
            return GetInternalDealById(_organisationService, _userService, aomDb, response.Deal.Id);
        }

        private static Deal GetInternalDealById(IOrganisationService organisationService, IUserService userService, AomRepository.IAomModel db, long id)
        {
            AomRepository.DealDto repositoryDealDto = db.Deals.Include(d => d.InitialOrderDto.ProductTenorDto.ProductDto.DeliveryLocations)
                .Include(d => d.MatchingOrderDto.ProductTenorDto.ProductDto.DeliveryLocations).First(d => d.Id == id);

            return repositoryDealDto.ToEntity(organisationService, userService);
        }

        private static void EnsureMarketIsOpen(AomRepository.IAomModel aomDb, long productId)
        {
            var product = aomDb.Products.SingleOrDefault(p => p.Id == productId);

            if (product == null || product.Status != DtoMappingAttribute.GetValueFromEnum(MarketStatus.Open))
            {
                throw new BusinessRuleException("Market is closed");
            }
        }
    }
}