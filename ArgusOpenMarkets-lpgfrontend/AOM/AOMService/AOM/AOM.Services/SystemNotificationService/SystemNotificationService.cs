﻿namespace AOM.Services.SystemNotificationService
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Crm;
    using System;
    using System.Linq;
    using System.Transactions;
    using Utils.Logging.Utils;

    public class SystemNotificationService : ISystemNotificationService
    {
        protected readonly IDbContextFactory _dbContextFactory;

        public SystemNotificationService(IDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public long CreateSystemNotification(string emailAddress, SystemEventType systemEventType,
            long productId, long organisationId, long userId)
        {
            long newNotificationId = -1;
            Log.Debug("SystemNotificationService.CreateSystemNotification");

            var existingNotification = GetSystemNotification(organisationId, productId, emailAddress, systemEventType);

            if (existingNotification == null)
            {
                using (var trans = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    try
                    {
                        using (var crmModel = _dbContextFactory.CreateCrmModel())
                        {
                            var notification = new SystemEventNotificationDto
                            {
                                NotificationEmailAddress = emailAddress,
                                SystemEventId = (int) systemEventType,
                                ProductId = productId,
                                OrganisationId_fk = organisationId
                            };

                            crmModel.SystemEventNotifications.Add(notification);
                            crmModel.SaveChangesAndLog(userId);

                            newNotificationId = notification.Id;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error - CreateSystemNotification", ex);
                    }
                }
            }

            return newNotificationId;
        }


        public bool DeleteSystemNotification(long systemEventNotificationId, long userId)
        {
            using (var crmModel = _dbContextFactory.CreateCrmModel())
            {
                var notification = GetSystemNotification(systemEventNotificationId);

                if (notification != null)
                {
                    crmModel.SystemEventNotifications.Attach(notification);
                    crmModel.SystemEventNotifications.Remove(notification);
                    crmModel.SaveChangesAndLog(userId);

                    // Refresh the company details in diffusion
                    return true;
                }
            }

            return false;
        }

        public SystemEventNotificationDto GetSystemNotification(long organisationId, long productId, string emailAddress,
            SystemEventType systemEventType)
        {
            SystemEventNotificationDto notification;
            using (var crmModel = _dbContextFactory.CreateCrmModel())
            {
                notification =
                    crmModel.SystemEventNotifications.FirstOrDefault(
                        n =>
                            n.OrganisationId_fk == organisationId && n.ProductId == productId &&
                            n.NotificationEmailAddress == emailAddress && n.SystemEventId == (int) systemEventType);
            }

            return notification;
        }

        public SystemEventNotificationDto GetSystemNotification(long systemEventNotificationId)
        {
            SystemEventNotificationDto notification;
            using (var crmModel = _dbContextFactory.CreateCrmModel())
            {
                notification = crmModel.SystemEventNotifications.FirstOrDefault(n => n.Id == systemEventNotificationId);
            }

            return notification;
        }
    }
}