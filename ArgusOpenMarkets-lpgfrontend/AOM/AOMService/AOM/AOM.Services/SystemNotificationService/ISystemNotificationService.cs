﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;

namespace AOM.Services.SystemNotificationService
{
    public interface ISystemNotificationService
    {
        long CreateSystemNotification(string emailAddress, App.Domain.Entities.SystemEventType systemEventType,
            long productId, long organisationId, long userId);

        bool DeleteSystemNotification(long systemEventNotificationId, long userId);

        SystemEventNotificationDto GetSystemNotification(long systemEventNotificationId);

        SystemEventNotificationDto GetSystemNotification(long organisationId, long productId, string emailAddress,
            SystemEventType systemEventType);
    }
}