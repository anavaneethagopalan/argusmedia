﻿using Utils.Logging.Utils;

namespace AOM.Services
{
    using System;
    using System.Reflection;

    using Ninject.Extensions.Conventions;
    using Ninject.Modules;

    public class ServiceBootstrap : NinjectModule
    {
        public override void Load()
        {
            try
            {
                Kernel.Bind(x => x.FromThisAssembly().IncludingNonePublicTypes().SelectAllClasses().BindAllInterfaces());
            }
            catch (Exception ex)
            {
                if (ex is ReflectionTypeLoadException)
                {
                    var typeLoadException = ex as ReflectionTypeLoadException;
                    var loaderExceptions = typeLoadException.LoaderExceptions;
                    foreach (var le in loaderExceptions)
                    {
                        Log.Error(string.Format("AOM.Services.ServiceBootstrap-loadException. Stack Trace:{0}", le.StackTrace), ex);
                    }
                }

                Log.Error("AOM.Services.ServiceBootstrap", ex);
            }
        }
    }
}