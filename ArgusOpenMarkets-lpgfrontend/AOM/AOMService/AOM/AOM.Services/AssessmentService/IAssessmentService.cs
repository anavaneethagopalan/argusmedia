﻿using AOM.App.Domain.Entities;

namespace AOM.Services.AssessmentService
{
        public interface IAssessmentService
        {
            CombinedAssessment GetCombinedAssessmentForProduct(long productId);
            bool UpdateCombinedAssessment(CombinedAssessment assessment);
            bool InsertSingleAssessment(Assessment assessment, bool skipValidation = false);
            CombinedAssessment ClearAssessment(long productId, string period, long userId);
        }
}