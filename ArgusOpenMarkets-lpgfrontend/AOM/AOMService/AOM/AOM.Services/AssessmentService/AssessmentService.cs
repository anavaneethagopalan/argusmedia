﻿namespace AOM.Services.AssessmentService
{
    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Aom;
    using System;
    using System.Linq;
    using System.Transactions;

    public class AssessmentService : IAssessmentService
    {
        private readonly IDbContextFactory _dbfactory;
        private readonly IDateTimeProvider _dateTimeProvider;

        public AssessmentService(IDbContextFactory dbfactory, IDateTimeProvider dateTimeProvider)
        {
            _dbfactory = dbfactory;
            _dateTimeProvider = dateTimeProvider;
        }

        public CombinedAssessment ClearAssessment(long productId, string period, long userId)
        {
            const AssessmentStatus assessmentStatusNone = AssessmentStatus.None;
            var clearAssessmentDate = getDateForAssessment(period);

            using (var context = _dbfactory.CreateAomModel())
            {
                var assessments = context.Assessments.Where(a =>a.ProductId.Equals(productId)).ToList();
                var assessmentsToClear = assessments.Where(x => DateTime.Compare(x.BusinessDate.Date, clearAssessmentDate.Date) == 0);

                if (assessmentsToClear != null)
                {
                    foreach (var ass in assessmentsToClear)
                    {
                        ass.PriceHigh = null;
                        ass.PriceLow = null;
                        ass.LastUpdated = DateTime.UtcNow;
                        ass.LastUpdatedUserId = userId;
                        ass.AssessmentStatus = DtoMappingAttribute.GetValueFromEnum(assessmentStatusNone);
                    }
                    context.SaveChangesAndLog(-1);
                }
            }

            return GetCombinedAssessmentForProduct(productId);
        }

        private DateTime getDateForAssessment(string period)
        {

            DateTime clearAssessmentDate = DateTime.UtcNow.Date;
            switch (period.ToLower())
            {
                case "previous":
                    clearAssessmentDate = DateTime.UtcNow.AddDays(-1).Date;
                    break;
            }

            return clearAssessmentDate;
        }

        public CombinedAssessment GetCombinedAssessmentForProduct(long productId)
        {
            using (var context = _dbfactory.CreateAomModel())
            {
                var assesments =context.Assessments.Where(a => a.ProductId.Equals(productId)).ToList()
                    .GroupBy(a => a.BusinessDate, (key, g) => new {Date = key, Assessments = g.OrderByDescending(z => z.Id).ToList()})
                    .Select(x => x.Assessments.First().ToEntity())
                    //.OrderByDescending(a => DateTime.Parse(a.BusinessDate, new CultureInfo("en-GB")))
                    .OrderByDescending(a => a.BusinessDate)
                    .Take(2)
                    .ToList();

                var today = assesments.FirstOrDefault(x => DateTime.Compare(x.BusinessDate.Date, DateTime.Today.Date) == 0);
                
                // TODO: This can break LIVE if you: 
                // a. Go into Assessments editor at the start of the day.
                // b. First Action - is to "CLEAR" Assessments.  
                // c. After Clear Assesmsnets - it causes a Refresh - comes in here - NO DATA FOR TODAY.
                // This causes a NULL value for today.  This then blows up the u/i.  
                // We have added a fix to the Front End to handle this.  We need to Tidy up this code in order to resolve.
                // NOT sure what the resolution is.  


                var yesterday = today == null ? assesments.FirstOrDefault() : assesments.Skip(1).FirstOrDefault();

                // First or default used to have the following code: .FirstOrDefault(x => x.PriceHigh != null); We are now potentially nulling this value - so removed.
                var combiAssessment = new CombinedAssessment
                {
                    ProductId = productId,
                    Today = CalculateTrendFromPreviousAssesment(today),
                    Previous = CalculateTrendFromPreviousAssesment(yesterday)
                };
                return combiAssessment;
            }
        }

        public bool UpdateCombinedAssessment(CombinedAssessment newAssessment)
        {
            if (newAssessment.Today == null)
            {
                return false;
            }
            return AddAssessmentToDb(newAssessment.Today, setAssessmentDateToParsedValue: false);
        }

        public bool InsertSingleAssessment(Assessment assessment, bool skipValidation = false)
        {
            if (assessment == null)
            {
                return false;
            }
            assessment.LastUpdated = _dateTimeProvider.UtcNow;
            return AddAssessmentToDb(assessment, true, skipValidation);
        }

        private bool AddAssessmentToDb(Assessment assessment, bool setAssessmentDateToParsedValue, bool skipValidation = false)
        {
            var parsedBusinessDate = assessment.BusinessDate;

            using (var context = _dbfactory.CreateAomModel())
            {
                // skip validation if the request is for inserting a default assessment
                if (!skipValidation)
                {
                    ValidateAssessmentPrices(context, assessment);
                }

                if (setAssessmentDateToParsedValue)
                {
                    assessment.BusinessDate = parsedBusinessDate;
                }
                using (var transaction = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    var dto = assessment.ToDto();
                    dto.BusinessDate = parsedBusinessDate;
                    context.Assessments.Add(dto);
                    var result = context.SaveChangesAndLog(-1);
                    transaction.Complete();
                    return result > 0;
                }
            }
        }

        private static void ValidateAssessmentPrices(IAomModel db, Assessment assessment)
        {
            if (assessment.PriceHigh.HasValue && assessment.PriceLow.HasValue && assessment.PriceLow.Value > assessment.PriceHigh.Value)
            {
                throw new BusinessRuleException(string.Format("Assement price high ({0}) cannot be lower than price low ({1})", assessment.PriceHigh, assessment.PriceLow));
            }
            var productSelected = GetProductAndTenorSelected(db, assessment.ProductId);
            if (productSelected == null)
            {
                throw new BusinessRuleException(string.Format("Product (ID: {0}) could not be found for Assessment {1}", assessment.ProductId, assessment.Id));
            }

            ValidateAssessmentPriceImpl(productSelected, assessment.PriceHigh, "high");
            ValidateAssessmentPriceImpl(productSelected, assessment.PriceLow, "low");
        }

        private static void ValidateAssessmentPriceImpl(ProductDto productSelected, decimal? price, string priceType)
        {
            if (!price.HasValue)
            {
                return;
            }
            var increment = productSelected.OrderPriceIncrement;
            var isTooLow = price.Value < productSelected.OrderPriceMinimum;
            var isTooHigh = price.Value > productSelected.OrderPriceMaximum;
            var isInvalidStepping = (increment > 0) && (price.Value%increment != 0);

            if (isTooLow || isTooHigh || isInvalidStepping)
            {
                throw new BusinessRuleException(string.Format("The {0} price for the assessment is invalid", priceType));
            }
        }

        private static ProductDto GetProductAndTenorSelected(IAomModel db, long productId)
        {
            return (from product in db.Products where product.Id == productId select product).FirstOrDefault();
        }

        private Assessment CalculateTrendFromPreviousAssesment(Assessment assessment)
        {
            using (var context = _dbfactory.CreateAomModel())
            {
                if (assessment != null)
                {
                    var dummyAssessmentStatus = DtoMappingAttribute.GetValueFromEnum(AssessmentStatus.None);
                    var previous = context.Assessments.Where(x =>
                        x.ProductId == assessment.ProductId &&
                        x.BusinessDate < assessment.BusinessDate &&
                        x.PriceHigh != null &&
                        x.AssessmentStatus != dummyAssessmentStatus)
                        .OrderByDescending(y => y.Id)
                        .FirstOrDefault();

                    if (previous != null)
                    {
                        var currentMean = (assessment.PriceHigh + assessment.PriceLow)/2;
                        var previousMean = (previous.PriceHigh + previous.PriceLow)/2;

                        if (currentMean > previousMean)
                        {
                            assessment.TrendFromPreviousAssesment = "Upward";
                        }
                        if (currentMean == previousMean)
                        {
                            assessment.TrendFromPreviousAssesment = "Equal";
                        }
                        if (currentMean < previousMean)
                        {
                            assessment.TrendFromPreviousAssesment = "Downward";
                        }
                    }
                }
                return assessment;
            }
        }
    }
}