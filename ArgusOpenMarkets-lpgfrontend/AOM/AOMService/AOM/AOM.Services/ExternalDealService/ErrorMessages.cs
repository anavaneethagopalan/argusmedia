﻿namespace AOM.Services.ExternalDealService
{
    public static class ErrorMessages
    {
        public static string OtherProductMustSpecifyContract =
            "The deal is for the 'Other' product but did not specify a contract";
    }
}
