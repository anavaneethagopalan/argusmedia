﻿namespace AOM.Services.ExternalDealService
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;
    using AOM.Transport.Events.ExternalDeals;

    public interface IExternalDealService
    {
        ExternalDeal CreateExternalDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response);
        ExternalDeal GetExternalDealById(IAomModel db, AomExternalDealAuthenticationResponse response);
        ExternalDeal GetExternalDealById(IAomModel aomDb, long id);
        ExternalDeal VoidExternalDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response);
        ExternalDeal VerifyExternalDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response);
        ExternalDeal EditExternalDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response);
    }
}