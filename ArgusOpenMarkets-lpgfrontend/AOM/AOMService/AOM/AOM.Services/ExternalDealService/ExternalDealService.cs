﻿using System.Collections.Generic;
using System.Data.Entity;
using AOM.Services.MetadataValidator;

namespace AOM.Services.ExternalDealService
{
    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Aom;
    using AOM.Transport.Events.ExternalDeals;
    using ServiceStack.Text;
    using System;
    using System.Linq;
    using System.Transactions;
    using Utils.Database;
    using Utils.Logging.Utils;

    public class ExternalDealService : IExternalDealService
    {
        private readonly ICrmAdministrationService _crmAdministrationService;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IMetadataValidator _metadataValidator;

        public ExternalDealService(ICrmAdministrationService crmAdministrationService,  IDateTimeProvider dateTimeProvider, IMetadataValidator metadataValidator)
        {
            _crmAdministrationService = crmAdministrationService;
            _dateTimeProvider = dateTimeProvider;
            _metadataValidator = metadataValidator;
        }

        public ExternalDeal CreateExternalDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                if (response.ExternalDeal.AsVerified)
                {
                    response.ExternalDeal.DealStatus = DealStatus.Executed;
                }
                var newExternalDealDto = response.ExternalDeal.ToDto();
                //newExternalDealDto.DateCreated = _dateTimeProvider.UtcNow;
                //newExternalDealDto.LastUpdated = _dateTimeProvider.UtcNow;
                newExternalDealDto.LastUpdatedUserId = response.Message.ClientSessionInfo.UserId;
                newExternalDealDto.ReporterId = response.Message.ClientSessionInfo.OrganisationId;

                #region TODO - change after R4.5 to pass in actual PriceLines/PriceBases from frontend rather than hardcode
                PriceLineDto priceLineDto = new PriceLineDto()
                {
                    SequenceNo = 1,
                    LastUpdatedUserId = response.Message.ClientSessionInfo.UserId
            };

                PriceLineBasisDto priceLineBasisDto = new PriceLineBasisDto()
                {
                    PriceLineDto = priceLineDto,
                    //PriceLineId = newExternalDealDto.PriceLineDto.Id,
                    SequenceNo = 1,
                    PriceBasisId = 1, //Hardcoded to FIXED
                    ProductId = newExternalDealDto.ProductId,
                    TenorCode = "P",
                    Description = newExternalDealDto.FreeFormDealMessage,
                    PercentageSplit = 100, //Hardcoded for now
                    PricingPeriodFrom = newExternalDealDto.DeliveryStartDate, //Hardcoded for now
                    PricingPeriodTo = newExternalDealDto.DeliveryEndDate, //Hardcoded for now
                    PriceLineBasisValue = newExternalDealDto.Price != null ? newExternalDealDto.Price.Value : 0, //This or QuantityText?
                    LastUpdated = newExternalDealDto.LastUpdated,
                    LastUpdatedUserId = newExternalDealDto.LastUpdatedUserId
                };

                priceLineDto.PriceLinesBases = new List<PriceLineBasisDto>() {priceLineBasisDto};
                newExternalDealDto.PriceLineDto = priceLineDto;
                
                #endregion

                if (ValidateExternalDeal(aomDb, newExternalDealDto))
                {
                    aomDb.ExternalDeals.Add(newExternalDealDto);
                    aomDb.SaveChangesAndLog(response.Message.ClientSessionInfo.UserId);
                }

                trans.Complete();

                return newExternalDealDto.ToEntity(_crmAdministrationService);
            }
        }

        public ExternalDeal GetExternalDealById(IAomModel db, AomExternalDealAuthenticationResponse response)
        {
            return GetExternalDealById(db, response.ExternalDeal.Id);
        }

        public ExternalDeal GetExternalDealById(IAomModel aomDb, long id)
        {
            return GetExternalDealDtoById(aomDb, id).ToEntity(_crmAdministrationService);
        }

        public ExternalDeal VoidExternalDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response)
        {
            response.ExternalDeal.DealStatus = DealStatus.Void;
            return EditDeal(aomDb, response);
        }

        public ExternalDeal EditExternalDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response)
        {
            return EditDeal(aomDb, response);
        }

        public ExternalDeal VerifyExternalDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response)
        {
            response.ExternalDeal.DealStatus = DealStatus.Executed;
            return EditDeal(aomDb, response);
        }

        private static ExternalDealDto GetExternalDealDtoById(IAomModel db, long id)
        {
            try
            {
                var repositoryExternalDealDto = (from d in db.ExternalDeals.Include(pl => pl.PriceLineDto.PriceLinesBases)
                                                 where id == d.Id select d).SingleOrDefault();

                return repositoryExternalDealDto;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("_GetExternalDealDtoById error.  Deal Id: {0}", id), ex);
            }

            return null;
        }

        private ExternalDeal EditDeal(IAomModel aomDb, AomExternalDealAuthenticationResponse response)
        {
            return ConcurrencyHelper.RetryOnConcurrencyException(string.Format("Attempting to update external deal Id:{0}", response.ExternalDeal.Id), 3, () =>
            {
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    var currentDto = GetExternalDealDtoById(aomDb, response.ExternalDeal.Id);
                    var updatedDto = response.ExternalDeal.ToDto();
                    PriceLineDto priceLineDto = null;

                    #region TODO - change after R4.5 to pass in actual PriceLines/PriceBases from frontend rather than hardcode
                    if (response.ExternalDeal.DealStatus != DealStatus.Void && response.ExternalDeal.DealStatus != DealStatus.Executed)
                    {
                        priceLineDto = new PriceLineDto()
                        {
                            SequenceNo = currentDto.PriceLineDto.SequenceNo,
                            LastUpdatedUserId = response.Message.ClientSessionInfo.UserId
                        };

                        priceLineDto.PriceLinesBases = new List<PriceLineBasisDto>();
                        foreach (PriceLineBasisDto priceLineBase in currentDto.PriceLineDto.PriceLinesBases)
                        {
                            priceLineBase.ProductId = updatedDto.ProductId;
                            priceLineBase.Description = updatedDto.FreeFormDealMessage;
                            priceLineBase.PricingPeriodFrom = updatedDto.DeliveryStartDate; //Hardcoded for now
                            priceLineBase.PricingPeriodTo = updatedDto.DeliveryEndDate; //Hardcoded for now
                            priceLineBase.PriceLineBasisValue = updatedDto.Price != null ? updatedDto.Price.Value : 0; //This or QuantityText?
                            priceLineBase.LastUpdatedUserId = response.Message.ClientSessionInfo.UserId;

                            priceLineDto.PriceLinesBases.Add(priceLineBase);
                        }
                    }
                    #endregion
                    
                    if (updatedDto.PriceLineDto == null)
                    {
                        updatedDto.PriceLineDto = priceLineDto ?? currentDto.PriceLineDto;
                    }
                    
                    var previousDealStatus = currentDto.DealStatus;

                    if (ValidateExternalDeal(aomDb, updatedDto, currentDto))
                    {
                        response.ExternalDeal.ToDto(currentDto);
                        //currentDto.LastUpdated = _dateTimeProvider.UtcNow;
                        currentDto.LastUpdatedUserId = response.Message.ClientSessionInfo.UserId;
                        currentDto.PreviousDealStatus = previousDealStatus;

                        aomDb.SaveChangesAndLog(currentDto.LastUpdatedUserId);
                    }
                    trans.Complete();
                    return currentDto.ToEntity(_crmAdministrationService);
                }
            });
        }

        private Boolean ValidateExternalDeal(IAomModel db, ExternalDealDto newState, ExternalDealDto previousState = null)
        {
            const string dealUpdatedByAnotherUser = "The deal has been updated by another user.";
            const string dealVerifiedByAnotherUser = "The deal has been verified by another user";
            const string dealVoidedByAnotherUser = "The deal has been voided by another user";

            if (previousState != null)
            {
                if (previousState.LastUpdated.RoundToMs() != newState.LastUpdated.RoundToMs())
                //if (previousState.LastUpdated.RoundToMs() != _dateTimeProvider.UtcNow.RoundToMs())
                {
                    var errorMessage = dealUpdatedByAnotherUser;
                    if (previousState.DealStatus.ToLower() == "e")
                    {
                        errorMessage = dealVerifiedByAnotherUser;
                    }

                    if (previousState.DealStatus.ToLower() == "v")
                    {
                        errorMessage = dealVoidedByAnotherUser;
                    }

                    throw new BusinessRuleException(errorMessage);
                }

                if (previousState.DealStatus == DtoMappingAttribute.GetValueFromEnum(DealStatus.Void))
                {
                    throw new BusinessRuleException("The deal is void and cannot be amended.");
                }
            }

            if (!DealSpecifiesProductOrContract(db, newState))
            {
                throw new BusinessRuleException(ErrorMessages.OtherProductMustSpecifyContract);
            }

            _metadataValidator.Validate(newState.MetaData, newState.ProductId);

            return true;
        }

        private static bool DealSpecifiesProductOrContract(IAomModel db, ExternalDealDto deal)
        {
            // If the product is 'Other' then the deal must specify the ContractInput field
            // N.B. Don't use p.Name.Equals with StringComparison.CurrentCultureIgnoreCase here as this can
            //      clash with the DB case sensitivity in EF.
            var otherProduct = db.Products.SingleOrDefault(p => "Other".Equals(p.Name, StringComparison.CurrentCultureIgnoreCase));
            if (otherProduct == null || deal.ProductId != otherProduct.Id)
            {
                return true;
            }
            return !String.IsNullOrWhiteSpace(deal.ContractInput);
        }
    }
}