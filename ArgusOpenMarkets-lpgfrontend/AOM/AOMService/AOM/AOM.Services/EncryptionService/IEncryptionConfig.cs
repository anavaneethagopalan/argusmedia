﻿namespace AOM.Services.EncryptionService
{
    public interface IEncryptionConfig
    {
        int SaltSize { get; }

        int Iteration { get; }

        int HashSize { get; }
    }
}
