﻿namespace AOM.Services.EncryptionService
{
    using System.Globalization;

    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;

    using System;
    using System.Security.Cryptography;
    using System.Text;

    public class EncryptionService : IEncryptionService
    {
        private IEncryptionConfig _config;

        public EncryptionService()
        {
            _config = new EncryptionConfig();
        }

        public EncryptionService(IEncryptionConfig encryptionConfig)
        {
            _config = encryptionConfig;
        }

        public string Hash(IUser user, string value)
        {
            return Hash(user.Id, value);
        }

        public string Hash(long userId, string value)
        {
            // Created an overload so we don't have to pass in a user object.
            var saltSeed = string.Format("{0}&{1}", userId, "31D6EDB5-CD3E-496D-907E-0B8393FCAE05");
            var hasher = new Rfc2898DeriveBytes(saltSeed, Encoding.UTF8.GetBytes(saltSeed), _config.Iteration);
            var salt = hasher.GetBytes(_config.SaltSize);
            hasher = new Rfc2898DeriveBytes(value, salt, _config.Iteration);

            return Convert.ToBase64String(hasher.GetBytes(_config.HashSize));
        }

        public string Encode(long input)
        {
            var items = input.ToString(CultureInfo.InvariantCulture).ToCharArray();
            var resultBuilder = new StringBuilder();
            foreach (var item in items)
            {
                resultBuilder.Append(Encode(item));
            }

            return resultBuilder.ToString();
        }

        public long Decode(string input)
        {
            var items = input.ToCharArray();
            var resultBuilder = new StringBuilder();
            foreach (var item in items)
            {
                resultBuilder.Append(Decode(item));
            }

            return Convert.ToInt64(resultBuilder.ToString());
        }

        private char Encode(char input)
        {
            switch (input)
            {
                case '0':
                    return 'Z';

                case '1':
                    return 'Y';

                case '2':
                    return 'X';

                case '3':
                    return 'W';

                case '4':
                    return 'V';

                case '5':
                    return 'e';

                case '6':
                    return 'd';

                case '7':
                    return 'c';

                case '8':
                    return 'b';

                default:
                    return 'a';
            }
        }

        private char Decode(char input)
        {
            switch (input)
            {
                case 'Z':
                    return '0';

                case 'Y':
                    return '1';

                case 'X':
                    return '2';

                case 'W':
                    return '3';

                case 'V':
                    return '4';

                case 'e':
                    return '5';

                case 'd':
                    return '6';

                case 'c':
                    return '7';

                case 'b':
                    return '8';

                default:
                    return '9';
            }
        }
    }
}