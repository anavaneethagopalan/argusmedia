﻿namespace AOM.Services.EncryptionService
{
    public class EncryptionConfig : IEncryptionConfig
    {
        public int SaltSize { get; private set; }

        public int Iteration { get; private set; }

        public int HashSize { get; private set; }

        public EncryptionConfig()
        {
            SaltSize = 32;
            Iteration = 10000;
            HashSize = 32;
        }
    }
}