﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Repository.Oracle;
using Utils.Logging.Utils;

namespace AOM.Services.CrmService
{
    public class ArgusCrmConsumerService : IArgusCrmConsumerService
    {
        private readonly IOracleReader _oracleReader;

        public ArgusCrmConsumerService(IOracleReader oracleReader)
        {
            _oracleReader = oracleReader;
        }

        public IEnumerable<AomUserModuleViewEntity> GetLatestData(string queryString)
        {
            var userModules = new List<AomUserModuleViewEntity>();

            using (var reader = _oracleReader.ExecuteSqlReader(queryString))
            {
                while (reader.Read())
                {
                    var username = reader.GetString(0);
                    var moduleId = reader.GetInt64(1);
                    var descript = reader.GetString(2);
                    var stream = reader.IsDBNull(3) ? null : reader.GetString(3);
                    long? streamId = reader.IsDBNull(4) ? null : (long?)reader.GetInt64(4);
                    long? userCrmId = reader.IsDBNull(5) ? null : (long?)reader.GetInt64(5);

                    userModules.Add(
                        new AomUserModuleViewEntity
                        {
                            UserName = username,
                            ModuleId = moduleId,
                            Description = descript,
                            Stream = stream,
                            StreamId = streamId,
                            UserOracleCrmId = userCrmId,
                            //TypeName = typeName
                        });

                    var userModuleline = String.Join(",", username, moduleId, descript, stream, streamId, userCrmId);
                    Log.Debug("Received new record from Argus Oracle: " + userModuleline);//debug to keep log file size down
                }
            }
            Log.InfoFormat("Read {0} records from Argus Oracle ", userModules.Count);
            return userModules;
        }

    }
}
