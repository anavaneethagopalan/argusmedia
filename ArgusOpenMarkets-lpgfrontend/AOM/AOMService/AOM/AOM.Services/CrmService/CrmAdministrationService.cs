﻿namespace AOM.Services.CrmService
{
    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Mappers;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Crm;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Transactions;

    using Utils.Logging.Utils;

    public class CrmAdministrationService : ICrmAdministrationService
    {
        private readonly IDbContextFactory _dbfactory;
        private readonly IDateTimeProvider _dateTimeProvider;

        public CrmAdministrationService(IDbContextFactory dbfactory, IDateTimeProvider dateTimeProvider)
        {
            _dbfactory = dbfactory;
            _dateTimeProvider = dateTimeProvider;
        }

        public bool BlockUser(string username, long requestedByUserId)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new BusinessRuleException("Username is required");
            }

            using (ICrmModel context = _dbfactory.CreateCrmModel())
            {
                UserInfoDto user = context.UserInfoes.FirstOrDefault(u => u.Username.Equals(username));
                if (user == null)
                {
                    throw new BusinessRuleException(string.Format("User: {0} does not exist", username));
                }

                if (!user.IsDeleted)
                {
                    user.IsBlocked = true;
                    context.SaveChangesAndLog(requestedByUserId);
                }
                else
                {
                    throw new BusinessRuleException("User " + username + " is inactive or deleted");
                }
            }

            return true;
        }

        public bool UnblockUser(string username, long requestedByUserId)
        {
            int result;
            using (ICrmModel context = _dbfactory.CreateCrmModel())
            {
                UserInfoDto user = context.UserInfoes.FirstOrDefault(u => u.Username.Equals(username) && !u.IsDeleted);
                if (user != null)
                {
                    user.IsBlocked = false;
                }
                result = context.SaveChangesAndLog(requestedByUserId);
            }
            return result > 0;
        }

        public bool CreateUserWithCredentials(IUser user, UserCredentials userCredentials, long requestedByUserId)
        {
            int result;

            using (var context = new CrmModel())
            {
                using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    context.UserInfoes.Add(user.ToDto());
                    if (userCredentials != null)
                    {
                        context.UserCredentials.Add(userCredentials.ToDto());
                    }
                    result = context.SaveChangesAndLog(requestedByUserId);

                    transactionScope.Complete();
                }
            }

            return result > 0;
        }

        public bool AddRoleToUser(long userId, long roleId, long requestedByUserId)
        {
            int result = 0;

            using (var context = new CrmModel())
            {
                List<UserInfoRoleDto> userRoles = context.UserInfoRoles.Where(uir => uir.UserInfo_Id_fk == userId).ToList();
                if (userRoles.Count(ur => ur.Role_Id_fk == roleId && ur.UserInfo_Id_fk == userId) != 0) return result > 0;
                var newRole = new UserInfoRoleDto {Role_Id_fk = roleId, UserInfo_Id_fk = userId};

                context.UserInfoRoles.Add(newRole);
                result = context.SaveChangesAndLog(requestedByUserId);
            }
            return result > 0;
        }

        public bool RemoveRoleForUser(long userId, long roleId, long requestedByUserId)
        {
            int result = 0;

            using (var context = new CrmModel())
            {
                List<UserInfoRoleDto> userRoles = context.UserInfoRoles.Where(uir => uir.UserInfo_Id_fk == userId).ToList();
                {
                    UserInfoRoleDto roleToRemove = userRoles.FirstOrDefault(ur => ur.Role_Id_fk == roleId && ur.UserInfo_Id_fk == userId);

                    if (roleToRemove != null)
                    {
                        context.UserInfoRoles.Remove(roleToRemove);
                        result = context.SaveChangesAndLog(requestedByUserId);
                    }
                }
            }
            return result > 0;
        }

        public bool UpdateUserDetails(long userId, IUser userDetails, long requestedByUserId)
        {
            int result;

            using (var context = _dbfactory.CreateCrmModel())
            {
                using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    UserInfoDto existingUserDto;
                    try
                    {
                        existingUserDto = context.UserInfoes.Single(ui => ui.Id == userId);
                    }
                    catch (InvalidOperationException ex)
                    {
                        throw new BusinessRuleException(string.Format("Unable to update details for user {0}, unique user not found [{1}]", userId, ex.Message));
                    }

                    existingUserDto.ArgusCrmUsername = userDetails.ArgusCrmUsername;
                    existingUserDto.Email = userDetails.Email;
                    existingUserDto.Telephone = userDetails.Telephone;
                    existingUserDto.Title = userDetails.Title;
                    existingUserDto.Name = userDetails.Name;
                    existingUserDto.IsActive = userDetails.IsActive;
                    existingUserDto.IsDeleted = userDetails.IsDeleted;
                    result = context.SaveChangesAndLog(requestedByUserId);
                    transactionScope.Complete();
                }
            }

            return result > 0;
        }

        public bool RemoveProductPrivilegeFromSubscribedProductPrivileges(long organisationId, long productId, long productPrivilegeId, long requestedByUserId)
        {
            int result = 0;

            using (var context = _dbfactory.CreateCrmModel())
            {
                SubscribedProductPrivilegeDto organisationSubscribedProductPrivilege = context.SubscribedProductPrivileges.FirstOrDefault(spp =>
                    spp.Organisation_Id_fk == organisationId && spp.Product_Id_fk == productId && spp.ProductPrivilege_Id_fk == productPrivilegeId);

                if (organisationSubscribedProductPrivilege != null)
                {
                    context.SubscribedProductPrivileges.Remove(organisationSubscribedProductPrivilege);

                    try
                    {
                        result = context.SaveChangesAndLog(requestedByUserId);
                    }
                    catch (DbUpdateException dbUpdateException)
                    {
                        Log.Error("RemoveProductPrivilegeFromSubscribedProductPrivileges - DBUpdateException", dbUpdateException);
                        return false;
                    }
                    catch (Exception ex)
                    {
                        Log.Error("RemoveProductPrivilegeFromSubscribedProductPrivileges", ex);
                        return false;
                    }
                }

                var subscribedProductPrivilegesForProduct = context.SubscribedProductPrivileges.Count(
                    spp => spp.Organisation_Id_fk == organisationId && spp.Product_Id_fk == productId);

                if (subscribedProductPrivilegesForProduct == 0)
                {
                    RemoveSubscribedProduct(organisationId, productId, requestedByUserId);
                }
            }

            return result > 0;
        }

        public bool AddProductPrivilegeToSubscribedProductPrivileges(long organisationId, long productId, long productPrivilegeId, long requestedByUserId)
        {
            long result;

            using (var context = new CrmModel())
            {
                IQueryable<SubscribedProductDto> subscribedProduct = context.SubscribedProducts.Where(
                    sp => sp.Product_Id_fk == productId && sp.Organisation_Id_fk == organisationId);

                if (!subscribedProduct.Any())
                {
                    AddSubscribedProduct(organisationId, productId, requestedByUserId);
                }

                var newSubscribedProductPrivilege = new SubscribedProductPrivilegeDto
                {
                    Organisation_Id_fk = organisationId,
                    ProductPrivilege_Id_fk = productPrivilegeId,
                    Product_Id_fk = productId,
                    DateCreated = _dateTimeProvider.Now
                };

                context.SubscribedProductPrivileges.Add(newSubscribedProductPrivilege);
                result = context.SaveChangesAndLog(requestedByUserId);
            }

            return result > 0;
        }

        public bool OrgansiationRoleExists(long organisationId, string name)
        {
            using (var context = new CrmModel())
            {
                var roles = context.OrganisationRoles.Where(or => or.Name == name && or.Organisation_Id_fk == organisationId).ToList();
                if (roles.Count > 0)
                    return true;
            }
            return false;
        }

        public long SaveRole(IOrganisationRole organisationRole, long requestedByUserId)
        {
            using (var context = new CrmModel())
            {
                var organisationRoleDto = new OrganisationRoleDto
                {
                    Name = organisationRole.Name,
                    Description = organisationRole.Description,
                    Organisation_Id_fk = organisationRole.OrganisationId
                };

                context.OrganisationRoles.Add(organisationRoleDto);
                var result = context.SaveChangesAndLog(requestedByUserId);

                return result > 1 ? organisationRoleDto.Id : -1;
            }
        }

        public bool DeleteOrganisationRole(IOrganisationRole role, long requestedByUserId)
        {
            int result = 0;

            using (var context = new CrmModel())
            {
                OrganisationRoleDto organisationRole = context.OrganisationRoles.FirstOrDefault(
                    or => or.Id == role.Id && or.Organisation_Id_fk == role.OrganisationId);

                if (organisationRole != null)
                {
                    context.OrganisationRoles.Remove(organisationRole);

                    try
                    {
                        result = context.SaveChangesAndLog(requestedByUserId);
                    }
                    catch (DbUpdateException dupEx)
                    {
                        Log.Error("DeleteOrganisationRole - DBUpdateException", dupEx);
                        return false;
                    }
                }
            }

            return result == 1;
        }

        public IList<SubscribedProductPrivilege> GetOrganisationSubscribedProductPrivileges(long organisationId)
        {
            using (var context = new CrmModel())
            {
                IQueryable<SubscribedProductPrivilegeDto> query =
                    from u in context.SubscribedProductPrivileges.Include(x => x.ProductPrivilegeDto)
                    where u.Organisation_Id_fk == organisationId
                    select u;

                List<SubscribedProductPrivilegeDto> subscribedPp = query.ToList();

                return subscribedPp.Select(spp => new SubscribedProductPrivilege
                    {
                        CreatedBy = spp.CreatedBy,
                        DateCreated = spp.DateCreated,
                        OrganisationId = spp.Organisation_Id_fk,
                        ProductId = spp.Product_Id_fk,
                        ProductPrivilegeId = spp.ProductPrivilege_Id_fk,
                        ProductPrivilege = new ProductPrivilege
                        {
                            Description = spp.ProductPrivilegeDto.Description,
                            Id = spp.ProductPrivilegeDto.Id,
                            Name = spp.ProductPrivilegeDto.Name,
                            MarketMustBeOpen = spp.ProductPrivilegeDto.MarketMustBeOpen
                        }
                    }).ToList();
            }
        }

        public IList<SubscribedProductPrivilege> GetOrganisationSubscribedProductPrivilegesByProduct(long organisationId, long productId)
        {
            using (var context = new CrmModel())
            {
                IQueryable<SubscribedProductPrivilegeDto> query =
                    from u in context.SubscribedProductPrivileges.Include(x => x.ProductPrivilegeDto)
                    where (u.Organisation_Id_fk == organisationId && u.Product_Id_fk == productId)
                    select u;

                List<SubscribedProductPrivilegeDto> subscribedPp = query.ToList();

                return subscribedPp.Select(spp => new SubscribedProductPrivilege
                {
                    CreatedBy = spp.CreatedBy,
                    DateCreated = spp.DateCreated,
                    OrganisationId = spp.Organisation_Id_fk,
                    ProductId = spp.Product_Id_fk,
                    ProductPrivilegeId = spp.ProductPrivilege_Id_fk,
                    ProductPrivilege = new ProductPrivilege
                    {
                        Description = spp.ProductPrivilegeDto.Description,
                        Id = spp.ProductPrivilegeDto.Id,
                        Name = spp.ProductPrivilegeDto.Name,
                        MarketMustBeOpen = spp.ProductPrivilegeDto.MarketMustBeOpen
                    }
                }).ToList();
            }
        }

        public IList<ProductRolePrivilege> GetProductRolePrivileges(long roleId)
        {
            using (var context = new CrmModel())
            {
                IQueryable<ProductRolePrivilegeDto> query =
                    from prp in context.ProductRolePrivileges.Include(x => x.OrganisationRole)
                    where prp.Role_Id_fk == roleId
                    select prp;

                return query.Select(prodRolePriv => new ProductRolePrivilege
                {
                    Id = prodRolePriv.Id,
                    OrganisationId = prodRolePriv.OrganisationRole.Organisation_Id_fk,
                    RoleId = prodRolePriv.Role_Id_fk,
                    ProductId = prodRolePriv.Product_Id_fk,
                    ProductPrivilegeId = prodRolePriv.ProductPrivilege_Id_fk
                }).ToList();
            }
        }

        public int AddProductRolePrivileges(List<ProductRolePrivilege> privileges, long requestedByUserId)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                var numberAdded = privileges.Count(privilege => AddProductRolePrivilege(privilege, requestedByUserId));
                trans.Complete();
                return numberAdded;
            }
        }

        public bool AddProductRolePrivilege(ProductRolePrivilege privilege, long requestedByUserId)
        {
            int result = 0;

            using (var context = _dbfactory.CreateCrmModel())
            {
                List<ProductRolePrivilegeDto> userRoles = context.ProductRolePrivileges.Where(prp =>
                    prp.Role_Id_fk == privilege.RoleId && prp.Organisation_Id_fk == privilege.OrganisationId
                    && prp.ProductPrivilege_Id_fk == privilege.ProductPrivilegeId
                    && prp.Product_Id_fk == privilege.ProductId).ToList();

                if (userRoles.Count == 0)
                {
                    OrganisationRoleDto organisationRoleDto = context.OrganisationRoles.FirstOrDefault(orgRole =>
                        orgRole.Organisation_Id_fk == privilege.OrganisationId && orgRole.Id == privilege.RoleId);

                    ProductPrivilegeDto productPrivilege = context.ProductPrivileges.FirstOrDefault(pp => pp.Id == privilege.ProductPrivilegeId);
                    
                    var newProductRolePrivilege = new ProductRolePrivilegeDto
                    {
                        Product_Id_fk = privilege.ProductId,
                        ProductPrivilege_Id_fk = privilege.ProductId,
                        Role_Id_fk = privilege.RoleId,
                        Organisation_Id_fk = privilege.OrganisationId,
                        OrganisationRole = organisationRoleDto,
                        ProductPrivilege = productPrivilege
                    };

                    context.ProductRolePrivileges.Add(newProductRolePrivilege);
                    result = context.SaveChangesAndLog(requestedByUserId);
                }
            }
            return result > 0;
        }

        public bool RemoveProductRolePrivilege(ProductRolePrivilege privilege, long requestedByUserId)
        {
            int result = 0;

            using (var context = new CrmModel())
            {
                ProductRolePrivilegeDto prpToDelete = context.ProductRolePrivileges.FirstOrDefault(prp =>
                    prp.Role_Id_fk == privilege.RoleId && prp.Organisation_Id_fk == privilege.OrganisationId
                    && prp.ProductPrivilege_Id_fk == privilege.ProductPrivilegeId
                    && prp.Product_Id_fk == privilege.ProductId);

                if (prpToDelete != null)
                {
                    prpToDelete.OrganisationRole = context.OrganisationRoles.FirstOrDefault(or => or.Id == privilege.RoleId);
                    prpToDelete.ProductPrivilege = context.ProductPrivileges.FirstOrDefault(pp => pp.Id == privilege.ProductPrivilegeId);

                    context.ProductRolePrivileges.Remove(prpToDelete);

                    try
                    {
                        result = context.SaveChangesAndLog(requestedByUserId);
                    }
                    catch (DbUpdateException dbUpdateException)
                    {
                        Log.Error("RemoveProductRolePrivilege - DBUpdateException", dbUpdateException);
                        return false;
                    }
                    catch (DbEntityValidationException e)
                    {
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                            }
                        }

                        Log.Error("RemoveProductRolePrivilege - " + err, e);
                        return false;
                    }
                    catch (Exception ex)
                    {
                        Log.Error("RemoveProductRolePrivilege", ex);
                        return false;
                    }
                }
            }

            return result > 0;
        }


        public IList<IProductPrivilege> GetProductPrivileges()
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                List<ProductPrivilegeDto> result = context.ProductPrivileges.OrderBy(x => x.Name).ToList();
                var productPrivileges = result.Select(pp =>
                    new ProductPrivilege
                    {
                        Id = pp.Id,
                        Name = pp.Name,
                        Description = pp.Description,
                        OrganisationType = DtoMappingAttribute.FindEnumWithValue<OrganisationType>(pp.OrganisationType),
                        MarketMustBeOpen = pp.MarketMustBeOpen
                    }).ToList<IProductPrivilege>();

                return productPrivileges;
            }
        }

        public IList<ISystemPrivilege> GetSystemPrivileges()
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                List<SystemPrivilegeDto> result = context.SystemPrivileges.OrderBy(x => x.Name).ToList();
                return result.Select(sp => new SystemPrivilege
                {
                    Id = sp.Id,
                    Name = sp.Name,
                    Description = sp.Description,
                    OrganisationType = DtoMappingAttribute.FindEnumWithValue<OrganisationType>(sp.OrganisationType),
                }).ToList<ISystemPrivilege>();
            }
        }
        
        public IList<ISystemPrivilege> GetSystemPrivileges(OrganisationType organisationType)
        {
            // TODO: Need to filter based upon the requesting Organisation Type.
            var privileges = GetSystemPrivileges();

            return privileges.Where(p => p.OrganisationType == organisationType || p.OrganisationType == OrganisationType.NotSpecified).ToList();
        }

        public IList<ISystemRolePrivilege> GetSystemRolePrivileges(long roleId)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                List<SystemRolePrivilegeDto> result = context.SystemRolePrivileges.Where(srp => srp.Role_Id_fk == roleId).ToList();
                return result.Select( sp => new SystemRolePrivilege
                {
                    Id = sp.System_Role_Privilege_Id,
                    PrivilegeId = sp.Privilege_Id_fk,
                    RoleId = sp.Role_Id_fk
                }).ToList<ISystemRolePrivilege>();
            }
        }

        public bool AddSystemPrivilegeToRole(SystemRolePrivilege systemRolePrivilege, long requestedByUserId)
        {
            int result = 0;

            if (systemRolePrivilege != null && systemRolePrivilege.RoleId > 0)
            {

                using (var context = _dbfactory.CreateCrmModel())
                {
                    bool exists = context.SystemRolePrivileges.Any(sp =>
                        sp.Role_Id_fk == systemRolePrivilege.RoleId
                        && sp.Privilege_Id_fk == systemRolePrivilege.PrivilegeId);

                    if (!exists)
                    {
                        var systemRolePrivilegeDto = new SystemRolePrivilegeDto
                        {
                            Role_Id_fk = systemRolePrivilege.RoleId,
                            Privilege_Id_fk = systemRolePrivilege.PrivilegeId
                        };

                        context.SystemRolePrivileges.Add(systemRolePrivilegeDto);
                        result = context.SaveChangesAndLog(requestedByUserId);
                    }
                }
            }

            return result > 0;
        }


        public bool RemoveSystemPrivilegeFromRole(SystemRolePrivilege systemRolePrivilege, long requestedByUserId)
        {
            int result = 0;

            using (var context = _dbfactory.CreateCrmModel())
            {
                var systemPrivilegeToRemove = context.SystemRolePrivileges.FirstOrDefault(srp =>
                    srp.Role_Id_fk == systemRolePrivilege.RoleId
                    && srp.Privilege_Id_fk == systemRolePrivilege.PrivilegeId);

                if (systemPrivilegeToRemove != null)
                {
                    context.SystemRolePrivileges.Remove(systemPrivilegeToRemove);
                    result = context.SaveChangesAndLog(requestedByUserId);
                }
            }
            return result > 0;
        }

        public bool RemoveSubscribedProduct(long organisationId, long productId, long requestedByUserId)
        {
            int result = 0;

            using (var context = _dbfactory.CreateCrmModel())
            {
                var subscribedProduct = context.SubscribedProducts.FirstOrDefault(sp => sp.Organisation_Id_fk == organisationId && sp.Product_Id_fk == productId);

                if (subscribedProduct != null)
                {
                    context.SubscribedProducts.Remove(subscribedProduct);

                    try
                    {
                        result = context.SaveChangesAndLog(requestedByUserId);
                    }
                    catch (DbUpdateException dupEx)
                    {
                        Log.Error("RemoveSubscribedProduct - DBUpdateException", dupEx);
                        return false;
                    }
                }
            }

            return result == 1;
        }

        public bool AddSubscribedProduct(long organisationId, long productId, long requestedByUserId)
        {
            long result;

            using (var context = new CrmModel())
            {
                var newSubscribedProduct = new SubscribedProductDto
                {
                    Organisation_Id_fk = organisationId,
                    Product_Id_fk = productId
                };

                context.SubscribedProducts.Add(newSubscribedProduct);
                result = context.SaveChangesAndLog(requestedByUserId);
            }

            return result > 0;
        }

        public long SaveOrganisation(IOrganisation organisation, long requestedByUserId, IOrganisationService organisationService)
        {
            long result;
            long orgId = 0;

            if (organisation == null || !IsOrganisationValid(organisation, organisationService))
            {
                return -1;
            }

            if (organisation.Id <= 0)
            {
                using (var context = new OrganisationModel())
                {
                    var organisationDto = new OrganisationDto
                    {
                        Address = organisation.Address,
                        DateCreated = organisation.DateCreated,
                        Description = organisation.Description,
                        IsDeleted = organisation.IsDeleted,
                        LegalName = organisation.LegalName,
                        Name = organisation.Name,
                        Email = organisation.Email,
                        ShortCode = organisation.ShortCode,
                        OrganisationType = DtoMappingAttribute.GetValueFromEnum(organisation.OrganisationType)
                    };

                    context.Organisations.Add(organisationDto);
                    result = context.SaveChangesAndLog(requestedByUserId);
                    orgId = organisationDto.Id;
                }
            }
            else
            {
                using (var context = new CrmModel())
                {
                    OrganisationDto org = context.Organisations.First(o => o.Id == organisation.Id);

                    if (org != null)
                    {
                        org.Address = organisation.Address;
                        org.Name = organisation.Name;
                        org.Email = organisation.Email;
                        org.IsDeleted = organisation.IsDeleted;
                        org.LegalName = organisation.LegalName;
                        org.ShortCode = organisation.ShortCode;
                        org.Description = organisation.Description;
                        org.OrganisationType = DtoMappingAttribute.GetValueFromEnum(organisation.OrganisationType);

                        orgId = org.Id;
                    }
                    result = context.SaveChangesAndLog(requestedByUserId);
                }
            }

            return (result > 0) ? orgId : -1;
        }

        private static bool IsOrganisationValid(IOrganisation organisation, IOrganisationService organisationService)
        {
            try
            {
                organisationService.ValidateOrganisation(organisation);
                return true;
            }
            catch (BusinessRuleException ex)
            {
                Log.Warn("Organisation save rejected: " + ex.Message);
                return false;
            }
        }

        public bool UpdateUserCredentialExpiration(long userId, DateTime newExpirationDate, long requestedByUserId)
        {
            int result;

            using (var context = _dbfactory.CreateCrmModel())
            {
                using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    string permCredentialType = DtoMappingAttribute.GetValueFromEnum(CredentialType.Permanent);
                    var credentials =
                        context.UserCredentials.Include(c => c.User)
                            .FirstOrDefault(
                                c =>
                                    c.User.Id == userId &&
                                    c.CredentialType == permCredentialType);

                    if (credentials == null)
                        throw new BusinessRuleException(string.Format("No Credentials of type {0} found for User {1}.",
                            permCredentialType, userId));

                    credentials.Expiration = newExpirationDate;
                    result = context.SaveChangesAndLog(requestedByUserId);
                    transactionScope.Complete();
                }
            }

            return result > 0;
        }
    }
}