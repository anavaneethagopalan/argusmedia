﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;

namespace AOM.Services.CrmService.TopicReduction
{
    public class TopicReductionService : ITopicReduction
    {
        public List<OrganisationPermissionList> ReduceOrganisationTopicCount(IList<BilateralMatrixPermissionPair> organisationPermissions, bool brokerTopics)
        {
            List<OrganisationPermissionList> reducedPermissions = new List<OrganisationPermissionList>();

            foreach (var orgPermission in organisationPermissions)
            {
                // NOTE: FOR BROKER PERMISSIONS - WE ONLY HAVE 1 PERMISSION THAT GRANTS ACCESS TO BUY OR SELL - SO ONLY ONE SIDE TO CHECK
                //var buyOrganisationId = orgPermission.BuySide.OurOrganisation;
                //var theirOrganisationId = orgPermission.BuySide.TheirOrganisation;

                Permission ourPermissionBuy = orgPermission.BuySide.OurPermAllowOrDeny;
                Permission ourPermissionSell = orgPermission.SellSide.OurPermAllowOrDeny;
                Permission theirPermissionSell = orgPermission.SellSide.TheirPermAllowOrDeny;
                Permission theirPermissionBuy = orgPermission.BuySide.TheirPermAllowOrDeny;

                //var productId = orgPermission.ProductId;

                //ourPermissionBuy = orgPermission.BuySide.OrganisationAllowOrDeny;
                //theirPermissionSell = orgPermission.SellSide.OrganisationAllowOrDeny;

                //BilateralMatrixPermissionPair reverseDecision = organisationPermissions.FirstOrDefault(b =>
                //    b.BuySide.OurOrganisation == orgPermission.BuySide.TheirOrganisation && 
                //    b.BuySide.TheirOrganisation == orgPermission.BuySide.OurOrganisation &&
                //    b.ProductId == orgPermission.ProductId);

                //if (reverseDecision != null)
                //{
                //    theirPermissionBuy = reverseDecision.BuySide.OurPermAllowOrDeny;
                //    ourPermissionSell = reverseDecision.SellSide.OurPermAllowOrDeny;
                //}

                AddToPermissionList(orgPermission, reducedPermissions, ourPermissionBuy, ourPermissionSell, theirPermissionBuy, theirPermissionSell);
            }

            return reducedPermissions;
        }

        private void AddToPermissionList(BilateralMatrixPermissionPair permissionPair, List<OrganisationPermissionList> reducedPermissions,
            Permission ourPermissionBuy, Permission ourPermissionSell, Permission theirPermissionBuy, Permission theirPermissionSell)
        {
            bool found = false;

            foreach (var orgPermission in reducedPermissions)
            {
                //if (orgPermission.OrgId == currentPermission.OrgId && orgPermission.ProductId == currentPermission.ProductId)
                if (orgPermission.ProductId == permissionPair.ProductId)
                {
                    found = true;
                }
            }

            if (!found)
            {
                reducedPermissions.Add(new OrganisationPermissionList
                {
                    ProductId = permissionPair.ProductId,
                    MarketId = permissionPair.MarketId
                });
            }

            // Find and populate details in permissionList
            foreach (var permission in reducedPermissions)
            {
                if (permission.PermittedTradingOrgs.Find(o => o.Id == permissionPair.OrgId) == null)
                {
                    permission.PermittedTradingOrgs.Add(new PermittedTradingOrg()
                    {
                        Id = permissionPair.OrgId,
                        ShortCode = permissionPair.OrgShortCode,
                        Name = permissionPair.OrgName,
                        LegalName = permissionPair.OrgLegalName
                    });
                }

                //if (broker.OrgId == theirOrganisationId && broker.ProductId == currentPermission.ProductId)
                if (permission.ProductId == permissionPair.ProductId)
                {
                    var permFound = false;

                    foreach (var currentPerm in permission.Bps)
                    {
                        if (currentPerm.OrgId == permissionPair.BuySide.TheirOrganisation)
                        {
                            permFound = true;
                            SetPermissionValues(reducedPermissions, permissionPair, currentPerm, ourPermissionBuy, ourPermissionSell, theirPermissionBuy, theirPermissionSell);

                            break;
                        }
                    }

                    if (!permFound)
                    {
                        var newPerm = new OrganisationPermission
                        {
                            OrgId = permissionPair.BuySide.TheirOrganisation
                        };

                        SetPermissionValues(reducedPermissions, permissionPair, newPerm, ourPermissionBuy, ourPermissionSell, theirPermissionBuy, theirPermissionSell);
                        permission.Bps.Add(newPerm);
                    }
                }
            }
        }

        private void SetPermissionValues(List<OrganisationPermissionList> brokerPermissions, BilateralMatrixPermissionPair permissionPair, OrganisationPermission perm,
            Permission ourPermissionBuy, Permission ourPermissionSell, Permission theirPermissionBuy, Permission theirPermissionSell)
        {
            perm.BuyOd = SmallPermission(ourPermissionBuy.ToString());
            perm.SellOd = SmallPermission(ourPermissionSell.ToString());
            perm.BuyTd = SmallPermission(theirPermissionBuy.ToString());
            perm.SellTd = SmallPermission(theirPermissionSell.ToString());

            perm.BuyUpdateUser = StoreLastUpdatedUserAndReturnKey(brokerPermissions, permissionPair.ProductId, permissionPair.BuySide.LastUpdatedByUser);
            perm.SellUpdateUser = StoreLastUpdatedUserAndReturnKey(brokerPermissions, permissionPair.ProductId, permissionPair.SellSide.LastUpdatedByUser);

            perm.BuyOdLastUpdated = permissionPair.BuySide.OurLastUpdated;
            perm.SellOdLastUpdated = permissionPair.SellSide.OurLastUpdated;
            perm.BuyTdLastUpdated = permissionPair.BuySide.TheirLastUpdated;
            perm.SellTdLastUpdated = permissionPair.SellSide.TheirLastUpdated;
        }

        private string SmallPermission(string originalPermission)
        {
            var smallPermission = string.Empty;
            switch (originalPermission.ToLower())
            {
                case "deny":
                    smallPermission = "d";
                    break;
                case "allow":
                    smallPermission = "a";
                    break;
                case "notset":
                    smallPermission = "u";
                    break;
                default:
                    smallPermission = originalPermission;
                    break;
            }
            return smallPermission;
        }

        private string StoreLastUpdatedUserAndReturnKey(List<OrganisationPermissionList> brokerPermissions, long productId, string lastUpdatedUsername)
        {
            if (string.IsNullOrEmpty(lastUpdatedUsername))
            {
                return lastUpdatedUsername;
            }

            foreach (var org in brokerPermissions)
            {
                //if (org.OrgId == ourOrganisationId && org.ProductId == productId)
                if (org.ProductId == productId)
                {
                    // Search for the existing username in the list.
                    foreach (var orgUser in org.UserDetails)
                    {
                        if (orgUser.Username == lastUpdatedUsername)
                        {
                            return orgUser.Id;
                        }
                    }

                    // Ok - we have NOT found the existing username in the list of users...
                    var newToken = string.Format("UT{0}", (org.UserDetails.Count + 1));
                    org.UserDetails.Add(new UserUpdatedDetails { Username = lastUpdatedUsername, Id = newToken });
                    return newToken;
                }
            }

            return lastUpdatedUsername;
        }
    }
}

