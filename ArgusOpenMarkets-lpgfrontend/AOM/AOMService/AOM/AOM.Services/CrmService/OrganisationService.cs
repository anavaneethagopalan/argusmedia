using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data.Entity;
using System.Linq;
using System.Transactions;

using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm;
using Utils.Logging.Utils;

using ServiceStack.Text;

namespace AOM.Services.CrmService
{
    public class OrganisationService : IOrganisationService
    {
        private readonly IDbContextFactory _dbContextFactory;
        private readonly IBilateralPermissionsBuilder _permissionsBuilder;

        public OrganisationService(IDbContextFactory dbContextFactory, IBilateralPermissionsBuilder permissionsBuilder)
        {
            _dbContextFactory = dbContextFactory;
            _permissionsBuilder = permissionsBuilder;
        }

        public virtual OrganisationsWithPermissions GetAllPrincipalsForUsersBrokerage(long requestedByUserId, long productId)
        {
            try
            {
                using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
                {
                    var userOrg = (from u in context.UserInfoes
                                   where u.Id == requestedByUserId
                                   select u.OrganisationDto).FirstOrDefault();

                    if (userOrg != null)
                    {
                        long orgId = userOrg.Id;
                        return GetPrincipalsOrBrokersInternalWithPermissions(context, productId, OrganisationType.Trading.ToDtoFormat(), orgId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("OrganisationService.GetAllBrokers  requestedByUserId:{0}   productId:{1}", requestedByUserId, productId), ex);
            }

            Log.ErrorFormat("OrganisationService.GetAllPrincipals  No Data Returned - requestedByUserId:{0}   productId:{1}", requestedByUserId, productId);

            return new OrganisationsWithPermissions();
        }

        public virtual OrganisationsWithPermissions GetAllBrokersForUsersOrganisation(long requestedByUserId, long productId)
        {
            try
            {
                using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
                {
                    var userOrg = (from u in context.UserInfoes
                                   where u.Id == requestedByUserId
                                   select u.OrganisationDto).FirstOrDefault();

                    if (userOrg != null)
                    {
                        long orgId = userOrg.Id;
                        return GetPrincipalsOrBrokersInternalWithPermissions(context, productId, OrganisationType.Brokerage.ToDtoFormat(), orgId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("OrganisationService.GetAllBrokers requestedByUserId:{0}, productId:{1}", requestedByUserId, productId), ex);
            }

            Log.ErrorFormat("OrganisationService.GetAllBrokers No Data Returned - requestedByUserId:{0}, productId:{1}", requestedByUserId, productId);

            return new OrganisationsWithPermissions();
        }

        public OrganisationsWithPermissions GetPermissionedPrincipalsForUsersBrokerage(long userId, long productId)
        {
            OrganisationsWithPermissions allPrincipals = GetAllPrincipalsForUsersBrokerage(userId , productId );
            allPrincipals.BuyOrganisations = allPrincipals.BuyOrganisations.Where(o => o.CanTrade).ToList();
            allPrincipals.SellOrganisations = allPrincipals.SellOrganisations.Where(o => o.CanTrade).ToList();
            return allPrincipals;
        }

        public List<OrganisationPermissionSummary> GetPermissionedPrincipalsForUsersBrokerageBuyOrSell(long userId,
            long productId, OrderType orderType)
        {
            OrganisationsWithPermissions allPrincipals = GetAllPrincipalsForUsersBrokerage(userId, productId);
            allPrincipals.BuyOrganisations = allPrincipals.BuyOrganisations.Where(o => o.CanTrade).ToList();
            allPrincipals.SellOrganisations = allPrincipals.SellOrganisations.Where(o => o.CanTrade).ToList();
            if (orderType == OrderType.Bid)
            {
                return allPrincipals.BuyOrganisations;
            }

            return allPrincipals.SellOrganisations;
        }

        public OrganisationsWithPermissions GetPermissionedBrokersForUsersOrganisation(long requestedByUserId, long productId)
        {
            OrganisationsWithPermissions allBrokers = GetAllBrokersForUsersOrganisation(requestedByUserId, productId);
            allBrokers.BuyOrganisations = allBrokers.BuyOrganisations.Where(o => o.CanTrade).ToList();
            allBrokers.SellOrganisations = allBrokers.SellOrganisations.Where(o => o.CanTrade).ToList();
            return allBrokers;
        }

        public List<OrganisationPermissionSummary> GetPermissionedBrokersForUsersOrganisationBuyOrSell(
            long reqRequestedByUserId, long productId,
            OrderType orderType)
        {
            OrganisationsWithPermissions
                allBrokers = GetAllBrokersForUsersOrganisation(reqRequestedByUserId, productId);

            if (orderType == OrderType.Bid)
            {
                // We are looking to buy
                return allBrokers.BuyOrganisations.Where(o => o.CanTrade).ToList();
            }

            // Must be looking to sell
            return allBrokers.SellOrganisations.Where(o => o.CanTrade).ToList();
        }

        public List<Organisation> GetCommonPermissionedOrganisations(long requestedByUserId, long productId, long counterpartyOrgId, long? organisationBrokerageId, BuyOrSell buyOrSell)
        {
            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                var requestedByUserOrganisation = context.UserInfoes.Include(x => x.OrganisationDto).First(x => x.Id == requestedByUserId).OrganisationDto;

                if (requestedByUserOrganisation.IsBroker())
                {
                    var organisations = GetCommonPermissionedCounterparties(requestedByUserId, productId, counterpartyOrgId, buyOrSell);

                    if (organisationBrokerageId.HasValue)
                    {
                        var principlesForBrokerage = GetAllCounterpartiesPermissionedToOrg(context, productId, organisationBrokerageId.Value, buyOrSell);
                        organisations = organisations.Where(o => principlesForBrokerage.Contains(o.Id)).ToList();
                    }
                    return organisations;
                }
                else
                {
                    return GetCommonPermissionedBrokers(requestedByUserId, productId, counterpartyOrgId, buyOrSell);
                }
            }
        }

        private List<Organisation> GetCommonPermissionedBrokers(long requestedByUserId, long productId, long counterpartyOrgId, BuyOrSell buyOrSell)
        {
            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                long orgId = context.UserInfoes.Include(x => x.OrganisationDto).First(x => x.Id == requestedByUserId).OrganisationDto.Id;

                var organisationBrokers = GetAllBrokersPermissionedToOrg(productId, orgId, buyOrSell);
                buyOrSell = buyOrSell.ReverseValue();
                var counterpartyBrokers = GetAllBrokersPermissionedToOrg(productId, counterpartyOrgId, buyOrSell);
                var commonBrokerIds = organisationBrokers.Intersect(counterpartyBrokers);

                var commonBrokers = (from o in context.Organisations
                                     where !o.IsDeleted && o.Id != orgId && o.OrganisationType == "B" && commonBrokerIds.Contains(o.Id)
                                     select o).ToList();

                return commonBrokers.Select(o => o.ToEntity()).ToList();
            }
        }

        private List<Organisation> GetCommonPermissionedCounterparties(long requestedByBrokerUserId, long productId, long principalOrgId, BuyOrSell buyOrSell)
        {
            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                long brokerId = context.UserInfoes.Include(x => x.OrganisationDto).First(x => x.Id == requestedByBrokerUserId).OrganisationDto.Id;

                var canBrokerTradeWithPrincipal = GetAllCounterpartiesPermissionedToOrg(context, productId, brokerId, buyOrSell.ReverseValue()).Contains(principalOrgId);

                if (!canBrokerTradeWithPrincipal)
                {
                    return new List<Organisation>();
                }

                var counterpartiesBrokerCanTradeWith = GetAllCounterpartiesPermissionedToOrg(context, productId, brokerId, buyOrSell);
                var counterpartiesPrincipalCanTradeWith = GetAllCounterpartiesPermissionedToOrg(context, productId, principalOrgId, buyOrSell);
                var orgIds = counterpartiesBrokerCanTradeWith.Intersect(counterpartiesPrincipalCanTradeWith).ToList();

                var commonCounteroarties = (from o in context.Organisations
                                            where !o.IsDeleted && o.OrganisationType == "T" && orgIds.Contains(o.Id) && o.Id != brokerId
                                            select o).ToList();

                return commonCounteroarties.Select(o => o.ToEntity()).ToList();
            }
        }


        private List<Organisation> GetPrincipalsOrBrokersInternal(long productId, string orgType)
        {
            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                List<OrganisationDto> organisations = null;
                try
                {
                    organisations = (from o in context.Organisations
                                     join sp in context.SubscribedProducts on o.Id equals sp.Organisation_Id_fk
                                     where sp.Product_Id_fk == productId && o.OrganisationType == orgType && o.IsDeleted == false
                                     select o).ToList();
                }
                catch (Exception ex)
                {
                    Log.Error("In CrmService.GetPrincipalsOrBrokersInternal, error while trying to get organisation! ", ex);
                }
                if (organisations != null)
                {
                    Log.Info("In CrmService.GetPrincipalsOrBrokersInternal, got organisations # " + organisations.Count);
                    return organisations.Select(o => o.ToEntity()).ToList();
                }
            }
            return null;
        }

        private OrganisationsWithPermissions GetPrincipalsOrBrokersInternalWithPermissions(IOrganisationModel context, long productId, string orgType, long orgId)
        {
            var bos = BuyOrSell.Buy;
            List<long> permissionedBuyOrganisationIds = orgType.Equals("B") ? 
                GetAllBrokersPermissionedToOrg(productId, orgId, bos) : GetAllCounterpartiesPermissionedToOrg(context, productId, orgId, bos);

            bos = BuyOrSell.Sell;
            List<long> permissionedSellOrganisationIds = orgType.Equals("B") ? 
                GetAllBrokersPermissionedToOrg(productId, orgId, bos) : GetAllCounterpartiesPermissionedToOrg(context, productId, orgId, bos);

            List<Organisation> allOrganisations = GetPrincipalsOrBrokersInternal(productId, orgType);
            var organisationsWithPermissions = new OrganisationsWithPermissions();

            foreach (Organisation orgainsation in allOrganisations.OrderBy(o => o.Name))
            {
                organisationsWithPermissions.BuyOrganisations.Add(
                    new OrganisationPermissionSummary
                    {
                        CanTrade = permissionedBuyOrganisationIds.Any(x => x == orgainsation.Id),
                        Organisation = orgainsation.MinimumDetails()
                    });

                organisationsWithPermissions.SellOrganisations.Add(
                    new OrganisationPermissionSummary
                    {
                        CanTrade = permissionedSellOrganisationIds.Any(x => x == orgainsation.Id),
                        Organisation = orgainsation.MinimumDetails()
                    });
            }

            if (orgType == "B")
            {
                var orgNone = CreateBrokerOrgNoneOrAny(BrokerRestriction.None, "Bilateral Only (No Broker)");
                var orgAny = CreateBrokerOrgNoneOrAny(BrokerRestriction.Any, "Any Broker");
                // We are Getting Brokers - add in the Any - Broker and None - Broker
                organisationsWithPermissions.BuyOrganisations.Insert(0, orgNone);
                organisationsWithPermissions.BuyOrganisations.Insert(0, orgAny);
                organisationsWithPermissions.SellOrganisations.Insert(0, orgNone);
                organisationsWithPermissions.SellOrganisations.Insert(0, orgAny);
            }

            return organisationsWithPermissions;
        }

        private OrganisationPermissionSummary CreateBrokerOrgNoneOrAny(BrokerRestriction brokerRestriction, string name)
        {
            return new OrganisationPermissionSummary
            {
                CanTrade = true,
                Organisation = new OrganisationMinimumDetails
                {
                    Id = -1,
                    BrokerRestriction = brokerRestriction,
                    LegalName = "",
                    Name = name,
                    OrganisationType = OrganisationType.Brokerage,
                    ShortCode = ""
                }
            };
        }

        private List<long> GetAllBrokersPermissionedToOrg(long productId, long organisationId, BuyOrSell buyOrSell)
        {
            string allow = Permission.Allow.ToDtoFormat();
            string dtoBuyOrSell = buyOrSell.ToDtoFormat();

            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                List<long> ourPerms = context.BrokerPermissions.Where(x => x.ProductId.Equals(productId) && x.AllowOrDeny.Equals(allow) &&
                    x.BuyOrSell.Equals(dtoBuyOrSell) && x.OurOrganisation.Equals(organisationId)).Select(x => x.TheirOrganisation).ToList();
                //var reversedBuyOrSell = buyOrSell.ReverseValue().ToDtoFormat();

                List<long> theirPerms = context.BrokerPermissions.Where(x => x.ProductId.Equals(productId) && x.AllowOrDeny.Equals(allow) &&
                    x.BuyOrSell.Equals(dtoBuyOrSell) && x.TheirOrganisation.Equals(organisationId)).Select(x => x.OurOrganisation).ToList();

                return ourPerms.Intersect(theirPerms).ToList();
            }
        }

        private List<long> GetAllCounterpartiesPermissionedToOrg(IOrganisationModel context, long productId, long organisationId, BuyOrSell buyOrSell)
        {
            string allow = Permission.Allow.ToDtoFormat();
            string dtoBuyOrSell = buyOrSell.ToDtoFormat();
            var reversedBuyOrSell = buyOrSell.ReverseValue().ToDtoFormat();

            List<long> ourPerms;
            List<long> theirPerms;

            var organisationDto = context.Organisations.FirstOrDefault(x => x.Id == organisationId);

            if (organisationDto == null)
            {
                throw new Exception(String.Format("Failed to load organisation with ID={0}", organisationId));
            }

            if (organisationDto.OrganisationType.Equals("B"))
            {
                ourPerms = context.BrokerPermissions.Where(x => x.ProductId.Equals(productId) && x.AllowOrDeny.Equals(allow) &&
                    x.BuyOrSell.Equals(reversedBuyOrSell) && x.OurOrganisation.Equals(organisationId)).Select(x => x.TheirOrganisation).ToList();

                theirPerms = context.BrokerPermissions.Where(x => x.ProductId.Equals(productId) && x.AllowOrDeny.Equals(allow) &&
                    x.BuyOrSell.Equals(dtoBuyOrSell) && x.TheirOrganisation.Equals(organisationId)).Select(x => x.OurOrganisation).ToList();

                return ourPerms.Intersect(theirPerms).ToList();
            }

            ourPerms = context.CounterpartyPermissions.Where(x => x.ProductId.Equals(productId) && x.AllowOrDeny.Equals(allow) &&
                x.BuyOrSell.Equals(reversedBuyOrSell) && x.OurOrganisation.Equals(organisationId)).Select(x => x.TheirOrganisation).ToList();

            theirPerms = context.CounterpartyPermissions.Where(x => x.ProductId.Equals(productId) && x.AllowOrDeny.Equals(allow) &&
                x.BuyOrSell.Equals(dtoBuyOrSell) && x.TheirOrganisation.Equals(organisationId)).Select(x => x.OurOrganisation).ToList();

            return ourPerms.Intersect(theirPerms).ToList();    
        }

        public IOrganisation GetOrganisationById(long organisationId)
        {
            using (new TransactionScope(TransactionScopeOption.Suppress))
            {
                using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
                {
                    OrganisationDto organisationDto = context.Organisations.AsNoTracking().FirstOrDefault(o => o.Id == organisationId);

                    if (organisationDto == null) return null;
                    return organisationDto.ToEntity();
                }
            }
        }

        public Organisation GetOrganisationByShortCode(string shortCode)
        {
            using (new TransactionScope(TransactionScopeOption.Suppress))
            {
                using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
                {
                    OrganisationDto organisationDto = context.Organisations.AsNoTracking().FirstOrDefault(o => o.ShortCode == shortCode);

                    if (organisationDto == null) return null;
                    return organisationDto.ToEntity();
                }
            }
        }


        public IList<Organisation> GetOrganisations()
        {
            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                List<OrganisationDto> organisations = context.Organisations.ToList();
                return organisations.Select(org => org.ToEntity()).ToList();
            }
        }

        public IList<OrganisationDetails> GetOrganisationDetails(List<Product> products)
        {
            var organisationDetails = new List<OrganisationDetails>();
            var organisations = GetOrganisations();

            foreach (var org in organisations)
            {
                var sw = new Stopwatch();
                sw.Start();

                organisationDetails.Add(new OrganisationDetails
                {
                    Organisation = org,
                    SubscribedProducts = GetSubscribedProducts(org.Id, products),
                    SystemNotifications = GetSystemNotifications(org.Id, products)
                });

                sw.Stop();
                Log.InfoFormat("GetOrganisationDetails for organisation id: {0} took: {1}ms", org.Id, sw.ElapsedMilliseconds);
            }

            return organisationDetails;
        }

        private List<NotificationMapping> GetSystemNotifications(long organisationId, IList<Product> products)
        {
            var sw = new Stopwatch();
            sw.Start();

            List<NotificationMapping> systemNotifications = new List<NotificationMapping>();

            try
            {
                using (var crmModel = _dbContextFactory.CreateCrmModel())
                {
                    systemNotifications = crmModel.SystemEventNotifications.Where(n => n.Organisation.Id == organisationId).AsEnumerable().Select(x =>
                        new NotificationMapping()
                        {
                            EmailAddress = x.NotificationEmailAddress,
                            EventType = (SystemEventType)x.SystemEventId,
                            Id = x.Id,
                            Product = products.Single(prod => prod.ProductId == x.ProductId)
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetSystemNotifications", ex);
            }

            sw.Stop();
            Log.InfoFormat("GetSystemNotifications for org id: {0} took: {1} returning {2} system notifications", organisationId, sw.ElapsedMilliseconds, systemNotifications.Count);

            return systemNotifications;
        }

        private List<SubscribedProduct> GetSubscribedProducts(long organisationId, List<Product> products)
        {
            var sw = new Stopwatch();
            sw.Start();

            // TODO: Implement get subscribed products...
            var subscribedProducts = new List<SubscribedProduct>();

            try
            {
                using (var crmModel = _dbContextFactory.CreateCrmModel())
                {
                    var sps = crmModel.SubscribedProducts.Where(sp => sp.Organisation_Id_fk == organisationId).ToList();

                    foreach (var sp in sps)
                    {
                        var product = products.FirstOrDefault(p => p.ProductId == sp.Product_Id_fk);
                        if (product != null)
                        {
                            if (!product.IsDeleted)
                            {
                                subscribedProducts.Add(new SubscribedProduct
                                {
                                    IsInternal = product.IsInternal,
                                    Name = product.Name,
                                    ProductId = product.ProductId
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetSubscribedProducts", ex);
            }

            sw.Stop();
            Log.InfoFormat("GetSubscribedProducts for org id: {0} took: {1} returning {2} subscribed products", organisationId, sw.ElapsedMilliseconds, subscribedProducts.Count);

            return subscribedProducts;
        }

        public IList<IOrganisationRole> GetOrganisationRoles(long organisationId)
        {
            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                List<OrganisationRoleDto> orgRoles = context.OrganisationRoles.Where(or => or.OrganisationDto.Id == organisationId).ToList();
                return orgRoles.Select(or => new OrganisationRole { Id = or.Id, Name = or.Name, Description = or.Description }).ToList<IOrganisationRole>();
            }
        }

        private Dictionary<long, long> GetMarketAndProductIds()
        {
            Dictionary<long, long> marketProductIds;
            using (IAomModel aomContext = _dbContextFactory.CreateAomModel())
            {
                marketProductIds = aomContext.Products.Where(x => !x.IsDeleted).Select(x => new {x.Id, x.MarketId}).ToDictionary(x => x.Id, x => x.MarketId);
            }
            return marketProductIds;
        }

        public IList<BilateralMatrixPermissionPair> GetAllCounterpartyPermissions()
        {
            return GetCounterpartyPermissions(-1);
        }

        public IList<BilateralMatrixPermissionPair> GetCounterpartyPermissions(long organisationId)
        {
            Dictionary<long, long> marketProductIds = GetMarketAndProductIds();

            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                try
                {
                    var counterpartyMatrix = new List<BilateralMatrixPermissionPair>();
                    IDictionary<long, string> userCache = context.UserInfoes.Select(u => new { u.Id, u.Name }).ToDictionary(k => k.Id, v => v.Name);

                    List<IIntraOrganisationPermission> counterpartyPermissions = organisationId == -1 ?
                        context.CounterpartyPermissions.AsNoTracking().ToList().Cast<IIntraOrganisationPermission>().ToList() :
                        context.CounterpartyPermissions.Where(cp => cp.OurOrganisation == organisationId || cp.TheirOrganisation == organisationId)
                        .AsNoTracking().ToList().Cast<IIntraOrganisationPermission>().ToList();

                    //counterpartyPermissions = counterpartyPermissions.Where(cp => cp.OurOrganisation == 2 || cp.TheirOrganisation == 2).ToList();
                    
                    List<OrganisationDto> cptyOrganisations = context.Organisations.Where(x => x.OrganisationType.Equals("T") && !x.IsDeleted).ToList();

                    foreach (KeyValuePair<long, long> ids in marketProductIds)
                    {
                        List<IIntraOrganisationPermission> counterpartyPermissionsForProduct = counterpartyPermissions.Where(x => x.ProductId == ids.Key).ToList();

                        List<long> orgSubscriptionList = context.SubscribedProducts.Where(x => x.Product_Id_fk.Equals(ids.Key))
                            .Select(x => x.Organisation_Id_fk).ToList()
                            .Intersect(cptyOrganisations.Select(co => co.Id)).ToList();

                        List<long> reverseOrgSubscriptionList = organisationId == -1 ? orgSubscriptionList : new List<long> { organisationId };

                        counterpartyMatrix.AddRange(_permissionsBuilder.GetIntraOrganisationPairs(userCache, counterpartyPermissionsForProduct, reverseOrgSubscriptionList,
                            orgSubscriptionList, ids.Key, ids.Value, cptyOrganisations));
                    }
                    return counterpartyMatrix;
                }
                catch (Exception ex)
                {
                    Log.Error("In OrganisationService.GetAllCounterpartyPermissions", ex);
                }
                return new List<BilateralMatrixPermissionPair>();
            }
        }

        public IList<BilateralMatrixPermissionPair> GetAllBrokerPermissions()
        {
            return GetBrokerPermissions(-1);
        }

        public IList<BilateralMatrixPermissionPair> GetBrokerPermissions(long organisationId)
        {
            Dictionary<long, long> marketProductIds = GetMarketAndProductIds();
            
            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                try
                {
                    var brokerMatrix = new List<BilateralMatrixPermissionPair>();
                    IDictionary<long, string> userCache = context.UserInfoes.Select(u => new { u.Id, u.Name }).ToDictionary(k => k.Id, v => v.Name);

                    List<IIntraOrganisationPermission> brokerPermissions = organisationId == -1 ?
                        context.BrokerPermissions.AsNoTracking().ToList().Cast<IIntraOrganisationPermission>().ToList() :
                        context.BrokerPermissions.Where(bp => bp.OurOrganisation == organisationId || bp.TheirOrganisation == organisationId)
                        .AsNoTracking().ToList().Cast<IIntraOrganisationPermission>().ToList();

                    OrganisationDto thisOrg = organisationId == -1 ? null : context.Organisations.First(org => org.Id == organisationId);

                    List<OrganisationDto> brokerOrganisations = thisOrg != null ? (thisOrg.OrganisationType.Equals("T") ?
                        context.Organisations.Where(x => !x.IsDeleted && (x.OrganisationType.Equals("B") || (x.OrganisationType.Equals("T") && x.Id == organisationId))).ToList() :
                        context.Organisations.Where(x => !x.IsDeleted && (x.OrganisationType.Equals("T") || (x.OrganisationType.Equals("B") && x.Id == organisationId))).ToList()) : 
                        context.Organisations.Where(x => !x.IsDeleted && (x.OrganisationType.Equals("B") || x.OrganisationType.Equals("T"))).ToList();

                    foreach (KeyValuePair<long, long> ids in marketProductIds)
                    {  
                        List<IIntraOrganisationPermission> brokerPermissionsForProduct = brokerPermissions.Where(x => x.ProductId == ids.Key).ToList();

                        List<long> orgSubscriptionList = context.SubscribedProducts.Where(x => x.Product_Id_fk.Equals(ids.Key))
                            .Select(x => x.Organisation_Id_fk).ToList()
                            .Intersect(brokerOrganisations.Select(bo => bo.Id)).ToList();

                        List<long> reverseOrgSubscriptionList = organisationId == -1 ? orgSubscriptionList : new List<long> { organisationId };

                        brokerMatrix.AddRange(_permissionsBuilder.GetIntraOrganisationPairs(userCache, brokerPermissionsForProduct, reverseOrgSubscriptionList,
                            orgSubscriptionList, ids.Key, ids.Value, brokerOrganisations));
                    }
                    return brokerMatrix;
                }
                catch (Exception ex)
                {
                    Log.Error("In OrganisationService.GetAllBrokerPermissions: ", ex);
                }
            }
            return new List<BilateralMatrixPermissionPair>();
        }


        public List<MatrixPermission> EditBrokerPermission(List<MatrixPermission> newPermissions, long userId)
        {
            using (IOrganisationModel model = _dbContextFactory.CreateOrganisationModel())
            {
                using (TransactionScope tran = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    foreach (MatrixPermission perm in newPermissions)
                    {
                        try
                        {
                            string dtoBuyOrSell = perm.BuyOrSell.ToDtoFormat();

                            //BrokerPermissionDto counterpartyPermission = model.BrokerPermissions.SingleOrDefault(x =>
                            //    x.OurOrganisation.Equals(perm.TheirOrganisation) && x.TheirOrganisation.Equals(perm.OurOrganisation) &&
                            //    x.ProductId.Equals(perm.ProductId) && x.BuyOrSell.Equals(dtoBuyOrSell));

                            //if (counterpartyPermission == null)
                            //{
                            //    counterpartyPermission = new BrokerPermissionDto
                            //    {
                            //        OurOrganisation = perm.TheirOrganisation,
                            //        TheirOrganisation = perm.OurOrganisation,
                            //        AllowOrDeny = Permission.NotSet.ToDtoFormat(),
                            //        ProductId = perm.ProductId,
                            //        BuyOrSell = dtoBuyOrSell
                            //    };
                            //}

                            BrokerPermissionDto organisationPermission = model.BrokerPermissions.SingleOrDefault(x =>
                                x.OurOrganisation.Equals(perm.OurOrganisation) && x.TheirOrganisation.Equals(perm.TheirOrganisation) &&
                                x.ProductId.Equals(perm.ProductId) && x.BuyOrSell.Equals(dtoBuyOrSell));

                            if (organisationPermission == null)
                            {
                                organisationPermission = new BrokerPermissionDto
                                {
                                    OurOrganisation = perm.OurOrganisation,
                                    TheirOrganisation = perm.TheirOrganisation,
                                    AllowOrDeny = perm.AllowOrDeny.ToDtoFormat(),
                                    ProductId = perm.ProductId,
                                    BuyOrSell = dtoBuyOrSell
                                };
                                model.BrokerPermissions.Add(organisationPermission);
                            }
                            else if (organisationPermission.LastUpdated.RoundToMs() != (perm.LastUpdated ?? DateTime.MinValue).RoundToMs())
                            {
                                throw new BusinessRuleException(String.Format(
                                    "The permission '{0}: {1} {2} {3}' has been changed by someone else since you viewed it.  Your request has not been successful. " +
                                    "Last Updated current: {4} != {5} new update",
                                    perm.ProductId, perm.AllowOrDeny, perm.BuyOrSell == BuyOrSell.Buy ? "buy from" : "sell to",
                                    GetOrganisationById(perm.TheirOrganisation).Name, organisationPermission.LastUpdated, perm.LastUpdated));
                            }

                            organisationPermission.LastUpdatedUserId = userId;
                            organisationPermission.LastUpdated = DateTime.Now;
                            organisationPermission.AllowOrDeny = perm.AllowOrDeny.ToDtoFormat();

                            Log.Info(string.Format("Broker permission update called: {0}-{1} for org: {2} against org: {3} by userId {4}",
                                perm.BuyOrSell, perm.AllowOrDeny, perm.OurOrganisation, perm.TheirOrganisation, userId));

                            perm.LastUpdatedUserId = userId;
                            perm.LastUpdated = organisationPermission.LastUpdated;
                            perm.UpdateSuccessful = true;
                            model.SaveChangesAndLog(userId);
                        }
                        catch (BusinessRuleException ex)
                        {
                            Log.Error("EditBrokerPermission - error: ", ex);
                            perm.UpdateSuccessful = false;
                            perm.UpdateError = ex.Message;
                        }
                    }

                    tran.Complete();
                    Log.Info(string.Format("EditBrokerPermission - DB transaction complete, {0} out of {1} saved successfully", 
                        newPermissions.Count(np => np.UpdateSuccessful), newPermissions.Count));

                    return newPermissions;
                }
            }
        }

        public List<MatrixPermission> EditCounterpartyPermission(List<MatrixPermission> newPermissions, long userId)
        {
            using (IOrganisationModel model = _dbContextFactory.CreateOrganisationModel())
            {
                using (TransactionScope tran = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    foreach (var perm in newPermissions)
                    {
                        try
                        {
                            string dtoBuyOrSell = perm.BuyOrSell.ToDtoFormat();
                            //string dtoSellOrBuy = perm.BuyOrSell.ReverseValue().ToDtoFormat();

                            //CounterpartyPermissionDto counterpartyPermission = model.CounterpartyPermissions.SingleOrDefault(x =>
                            //    x.OurOrganisation.Equals(perm.TheirOrganisation) && x.TheirOrganisation.Equals(perm.OurOrganisation) &&
                            //    x.ProductId.Equals(perm.ProductId) && x.BuyOrSell.Equals(dtoSellOrBuy));

                            //if (counterpartyPermission == null)
                            //{
                            //    counterpartyPermission = new CounterpartyPermissionDto
                            //    {
                            //        OurOrganisation = perm.TheirOrganisation,
                            //        TheirOrganisation = perm.OurOrganisation,
                            //        AllowOrDeny = Permission.NotSet.ToDtoFormat(),
                            //        ProductId = perm.ProductId,
                            //        BuyOrSell = dtoSellOrBuy
                            //    };
                            //}

                            CounterpartyPermissionDto organisationPermission = model.CounterpartyPermissions.SingleOrDefault(x =>
                                x.OurOrganisation.Equals(perm.OurOrganisation) && x.TheirOrganisation.Equals(perm.TheirOrganisation) &&
                                x.ProductId.Equals(perm.ProductId) && x.BuyOrSell.Equals(dtoBuyOrSell));

                            if (organisationPermission == null)
                            {
                                organisationPermission = new CounterpartyPermissionDto
                                {
                                    OurOrganisation = perm.OurOrganisation,
                                    TheirOrganisation = perm.TheirOrganisation,
                                    AllowOrDeny = perm.AllowOrDeny.ToDtoFormat(),
                                    ProductId = perm.ProductId,
                                    BuyOrSell = dtoBuyOrSell
                                };
                                model.CounterpartyPermissions.Add(organisationPermission);
                            }
                            else if (organisationPermission.LastUpdated.RoundToMs() != (perm.LastUpdated ?? DateTime.MinValue).RoundToMs())
                            {
                                throw new BusinessRuleException(String.Format(
                                    "The permission '{0}: {1} {2} {3}' has been changed by someone else since you viewed it.  Your request has not been successful." +
                                    "Last Updated current: {4} != {5} new update",
                                    perm.ProductId, perm.AllowOrDeny, perm.BuyOrSell == BuyOrSell.Buy ? "buy from" : "sell to",
                                    GetOrganisationById(perm.TheirOrganisation).Name, organisationPermission.LastUpdated, perm.LastUpdated));
                            }

                            organisationPermission.LastUpdatedUserId = userId;
                            organisationPermission.LastUpdated = DateTime.Now;
                            organisationPermission.AllowOrDeny = perm.AllowOrDeny.ToDtoFormat();
       
                            Log.Info(string.Format("Counterparty permission update called: {0}-{1} for org: {2} against org: {3} by userId {4}",
                                perm.BuyOrSell, perm.AllowOrDeny, perm.OurOrganisation, perm.TheirOrganisation, userId));

                            perm.LastUpdatedUserId = userId;
                            perm.UpdateSuccessful = true;
                            perm.LastUpdated = organisationPermission.LastUpdated;
                            model.SaveChangesAndLog(userId);
                        }
                        catch (BusinessRuleException ex)
                        {
                            Log.Error("EditCounterpartyPermission - error: ", ex);
                            perm.UpdateSuccessful = false;
                            perm.UpdateError = ex.Message;
                        }
                    }
                    
                    tran.Complete();
                    Log.Info(string.Format("EditCounterpartyPermission - DB transaction complete, {0} out of {1} saved successfully",
                        newPermissions.Count(np => np.UpdateSuccessful), newPermissions.Count));

                    return newPermissions;
                }
            }
        }
        
        public bool CanCounterpartiesTrade(long productId, BuyOrSell buyOrSell, long cp1OrganisationId, long cp2OrganisationId)
        {
            string dtoBuyOrSell = buyOrSell.ToDtoFormat();
            string dtoSellOrBuy = buyOrSell.ReverseValue().ToDtoFormat();
            string allow = Permission.Allow.ToDtoFormat();
            const string denied = "Missing";

            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                IQueryable<string> perms = context.CounterpartyPermissions.AsNoTracking().Where(_ =>
                    _.ProductId == productId && _.OurOrganisation == cp1OrganisationId && _.TheirOrganisation == cp2OrganisationId && _.BuyOrSell == dtoBuyOrSell)
                    .Select(_ => _.AllowOrDeny).DefaultIfEmpty(denied)
                    .Union(context.CounterpartyPermissions.AsNoTracking().Where(_ =>
                    _.ProductId == productId && _.OurOrganisation == cp2OrganisationId && _.TheirOrganisation == cp1OrganisationId && _.BuyOrSell == dtoSellOrBuy)
                    .Select(_ => _.AllowOrDeny).DefaultIfEmpty(denied));

                return perms.Any() && perms.All(_ => _ == allow);
            }
        }

        public bool CanCounterpartyTradeWithBroker(long productId, BuyOrSell buyOrSell, long cp1OrganisationId, long brokerOrganisationId)
        {
            string dtoBuyOrSell = buyOrSell.ToDtoFormat();
            //string dtoSellOrBuy = buyOrSell.ReverseValue().ToDtoFormat();
            string allow = Permission.Allow.ToDtoFormat();
            const string denied = "Missing";

            using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
            {
                IQueryable<string> perms = context.BrokerPermissions.AsNoTracking().Where(_ =>
                    _.ProductId == productId && _.OurOrganisation == cp1OrganisationId && _.TheirOrganisation == brokerOrganisationId && _.BuyOrSell == dtoBuyOrSell)
                    .Select(_ => _.AllowOrDeny).DefaultIfEmpty(denied)
                    .Union(context.BrokerPermissions.AsNoTracking().Where(_ =>
                    _.ProductId == productId && _.OurOrganisation == brokerOrganisationId && _.TheirOrganisation == cp1OrganisationId && _.BuyOrSell == dtoBuyOrSell)
                    .Select(_ => _.AllowOrDeny).DefaultIfEmpty(denied));

                return perms.Any() && perms.All(_ => _ == allow);
            }
        }

        public List<string> GetEmailContactsForEvent(SystemEventType eventType, long productId, long organisationId)
        {
            using (var trans = new TransactionScope(TransactionScopeOption.Suppress))
            {
                using (IOrganisationModel context = _dbContextFactory.CreateOrganisationModel())
                {
                    List<String> result = context.SystemEventNotifications.AsNoTracking().Where(notification =>
                        notification.SystemEventId == (int)eventType && notification.ProductId == productId &&
                        notification.OrganisationId_fk == organisationId)
                        .Select(notification => notification.NotificationEmailAddress).ToList();

                    if (!result.Any())
                    {
                        //Send to company default email
                        var organisationEmail = context.Organisations.AsNoTracking().Where(x => x.Id == organisationId).Select(x => x.Email).FirstOrDefault();
                        if (organisationEmail != null)
                        {
                            result.Add(organisationEmail);
                        }
                    }
                    trans.Complete();
                    return result;
                }
            }
        }

        public void ValidateOrganisation(IOrganisation organisation)
        {
            OrganisationValidator.ValidateOrganisation(organisation);
        }

        public bool IsOrganisationValid(IOrganisation organisation)
        {
            return OrganisationValidator.IsOrganisationValid(organisation);
        }
    }
}