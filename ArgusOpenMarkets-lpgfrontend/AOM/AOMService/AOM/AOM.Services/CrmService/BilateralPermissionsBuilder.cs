using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Crm;

namespace AOM.Services.CrmService
{
    public class BilateralPermissionsBuilder : IBilateralPermissionsBuilder
    {
        public List<BilateralMatrixPermissionPair> GetIntraOrganisationPairs(IDictionary<long, string> userCache, List<IIntraOrganisationPermission> orgPermissionsForProduct,
            IList<long> subscribedOrgsToProduct, IList<long> subscribedCptysToProduct, long productId, long marketId, List<OrganisationDto> organisationList)
        {
            var brokerMatrix = new List<BilateralMatrixPermissionPair>();

            IEnumerable<Tuple<long, long>> counterpartyPairs =
                from o in subscribedOrgsToProduct
                from o2 in subscribedCptysToProduct
                where o != o2
                select new Tuple<long, long>(o, o2);

            brokerMatrix.AddRange(
                from counterpartyPair in counterpartyPairs
                let ourPermToBuy = orgPermissionsForProduct.SingleOrDefault(
                    x => x.OurOrganisation == counterpartyPair.Item1 && x.TheirOrganisation == counterpartyPair.Item2 && x.BuyOrSell.Equals("B")).ToEntity()
                let ourPermToSell = orgPermissionsForProduct.SingleOrDefault(
                    x => x.OurOrganisation == counterpartyPair.Item1 && x.TheirOrganisation == counterpartyPair.Item2 && x.BuyOrSell.Equals("S")).ToEntity()
                let theirPermToBuy = orgPermissionsForProduct.SingleOrDefault(
                    x => x.OurOrganisation == counterpartyPair.Item2 && x.TheirOrganisation == counterpartyPair.Item1 && x.BuyOrSell.Equals("B")).ToEntity()
                let theirPermToSell = orgPermissionsForProduct.SingleOrDefault(
                    x => x.OurOrganisation == counterpartyPair.Item2 && x.TheirOrganisation == counterpartyPair.Item1 && x.BuyOrSell.Equals("S")).ToEntity()
                let selectedOrganisation = organisationList.First(o => o.OrganisationType == OrganisationType.Trading.ToString() ? o.Id == counterpartyPair.Item1 : o.Id == counterpartyPair.Item2)

                select new BilateralMatrixPermissionPair
                {
                    OrgId = selectedOrganisation.Id,
                    OrgShortCode = selectedOrganisation.ShortCode,
                    OrgName = selectedOrganisation.Name,
                    OrgLegalName = selectedOrganisation.LegalName,
                    ProductId = productId,
                    MarketId = marketId,
                    BuySide = CreateBilateralPermission(userCache, counterpartyPair.Item1, counterpartyPair.Item2, BuyOrSell.Buy, ourPermToBuy, theirPermToBuy),
                    SellSide = CreateBilateralPermission(userCache, counterpartyPair.Item1, counterpartyPair.Item2, BuyOrSell.Sell, ourPermToSell, theirPermToSell)
                });

            return brokerMatrix;
        }

        private BilateralMatrixPermission CreateBilateralPermission(IDictionary<long, string> userCache, long orgId, long counterpartyId, BuyOrSell buyOrsell,
            MatrixPermission ourPerm, MatrixPermission theirPerm)
        {
            var bilateralPermission = new BilateralMatrixPermission
            {
                OurOrganisation = orgId,
                TheirOrganisation = counterpartyId,
                BuyOrSell = buyOrsell
            };

            bilateralPermission.OurPermAllowOrDeny = ourPerm == null ? Permission.NotSet : ourPerm.AllowOrDeny;
            bilateralPermission.TheirPermAllowOrDeny = theirPerm == null ? Permission.NotSet : theirPerm.AllowOrDeny;
            bilateralPermission.LastUpdatedByUser = ourPerm == null ? string.Empty : GetUserName(userCache, ourPerm.LastUpdatedUserId);
            bilateralPermission.OurLastUpdated = ourPerm == null ? DateTime.MinValue : ourPerm.LastUpdated;
            bilateralPermission.TheirLastUpdated = theirPerm == null ? DateTime.MinValue : theirPerm.LastUpdated;

            return bilateralPermission;
        }

        private string GetUserName(IDictionary<long, string> userCache, long? userId)
        {
            return userId != null ? userCache.ContainsKey(userId.Value) ? userCache[userId.Value] : "NULL" : "NULL";
        }
    }
}