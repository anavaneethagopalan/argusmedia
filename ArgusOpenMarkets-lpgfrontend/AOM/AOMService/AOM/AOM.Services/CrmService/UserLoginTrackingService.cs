﻿using System;
using System.Configuration;
using System.Linq;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using Utils.Logging.Utils;

namespace AOM.Services.CrmService
{
    /// <summary>
    /// Track the number of login attempts by a user within a given timeout.
    /// </summary>
    public class UserLoginTrackingService : IUserLoginTrackingService
    {
        /// <summary>
        /// The TTL of existing login attempts in seconds.  After this timeout
        /// we reset the number of login attmempts to zero.
        /// </summary>
        private int SessionExprSeconds
        {
            get
            {
                int sessionExprSeconds;

                try
                {
                    sessionExprSeconds = Int32.Parse(ConfigurationManager.AppSettings["SessionExprSeconds"]);
                }
                catch (Exception)
                {
                    sessionExprSeconds = 120;
                }

                return sessionExprSeconds;
            }
        }

        private int MaxNumberLoginAttempts
        {
            get
            {
                int maxNumberLoginAttempts;

                try
                {
                    maxNumberLoginAttempts = Int32.Parse(ConfigurationManager.AppSettings["MaxNumberOfLoginAttempts"]);
                }
                catch (Exception)
                {
                    maxNumberLoginAttempts = 3;
                }

                return maxNumberLoginAttempts;
            }
        }

        private readonly IDbContextFactory _dbfactory;
        //private readonly IUserService _userService;

        public UserLoginTrackingService(IDbContextFactory dbfactory)
        {
            _dbfactory = dbfactory;
            //_userService = userService;
        }

        public void RegisterLoginAttempt(long userId, long userLoginSourceId)
        {
            //var userId = _userService.GetUserId(username);

            //using (var crmModel = _dbfactory.CreateCrmModel())
            using (var authModel = _dbfactory.CreateAuthenticationModel())
            {
                if (authModel.UserLoginAttempts.Count(x => x.UserId == userId && x.UserLoginSource_Id_fk == userLoginSourceId) == 0)
                {
                    //CreateNewUserLoginRecord(userId);
                    var userLoginCount = new UserLoginAttemptsDto()
                    {
                        UserId = userId,
                        LastLoginAttemptTime = DateTime.UtcNow,
                        NumFailedLogins = 0, // Init to zero
                        UserLoginSource_Id_fk = userLoginSourceId
                    };
                    authModel.UserLoginAttempts.Add(userLoginCount);
                    authModel.SaveChangesAndLog(-1);
                }

                var userLoginRecord = authModel.UserLoginAttempts.Single(x => x.UserId == userId && x.UserLoginSource_Id_fk == userLoginSourceId);

                // if last login time was less than SessionExprSeconds ago, increase login attempts
                if (DateTime.UtcNow <= userLoginRecord.LastLoginAttemptTime.AddSeconds(SessionExprSeconds))
                {
                    userLoginRecord.NumFailedLogins++;
                }
                else
                {
                    userLoginRecord.NumFailedLogins = 1;
                }

                userLoginRecord.LastLoginAttemptTime = DateTime.UtcNow;

                authModel.SaveChangesAndLog(-1);
            }
        }

        public bool IsUserAtLoginAttemptLimit(long userId, long userLoginSourceId)
        {
            //using (var crmModel = _dbfactory.CreateCrmModel())
            using (var authModel = _dbfactory.CreateAuthenticationModel())
            {
                //var userId = _userService.GetUserId(username);
                return authModel.UserLoginAttempts.Single(x => x.UserId == userId && x.UserLoginSource_Id_fk == userLoginSourceId).NumFailedLogins >= MaxNumberLoginAttempts;
            }
        }

        public void ResetUserAccount(long userId, long userLoginSourceId)
        {
            //var userId = _userService.GetUserId(username);
            //using (var crmModel = _dbfactory.CreateCrmModel())
            using (var authModel = _dbfactory.CreateAuthenticationModel())
            {
                Log.Info("ResetUserAccount Start");
                if (authModel.UserLoginAttempts.Count(x => x.UserId == userId && x.UserLoginSource_Id_fk == userLoginSourceId) != 0)
                {
                    authModel.UserLoginAttempts.Single(x => x.UserId == userId && x.UserLoginSource_Id_fk == userLoginSourceId).NumFailedLogins = 0;
                    authModel.SaveChangesAndLog(-1);
                    Log.Info("ResetUserAccount Finish");
                }
            }
        }
    }
}