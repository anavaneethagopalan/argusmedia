﻿namespace AOM.Services.CrmService
{
    public interface IUserLoginTrackingService
    {
        void RegisterLoginAttempt(long userId, long userLoginSourceId);
        bool IsUserAtLoginAttemptLimit(long userId, long userLoginSourceId);
        void ResetUserAccount(long userId, long userLoginSourceId);
    }
}