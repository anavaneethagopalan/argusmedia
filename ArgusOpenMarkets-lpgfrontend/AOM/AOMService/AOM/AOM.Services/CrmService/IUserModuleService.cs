﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Services.CrmService
{
    public interface IUserModuleService
    {
        int UpdateUserInfo(IEnumerable<AomUserModuleViewEntity> modules);
        int UpdateUserModules(IEnumerable<AomUserModuleViewEntity> modules);
        int UpdateModules(IEnumerable<AomUserModuleViewEntity> modules);
    }
}
