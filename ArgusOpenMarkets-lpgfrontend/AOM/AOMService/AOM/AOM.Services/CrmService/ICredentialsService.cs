﻿using System.Collections.Generic;

namespace AOM.Services.CrmService
{
    using AOM.App.Domain.Entities;

    public interface ICredentialsService
    {
        bool UpdateUserCredentials(long userId, string oldPasswordHashed, string newPasswordHashed, bool checkOldPassword, bool adminRequest, long requestedByUserId, 
            bool isTemporary = false, int tempExpirationInHours = 48);

        bool UpdateUserCredentialsAdmin(long userId, string oldPasswordHashed, string newPasswordHashed, bool checkOldPassword, bool adminRequest, long requestedByUserId,
            bool isTemporary = false, int tempExpirationInHours = 48);

        bool UpdateUserCredentials(UserCredentials credentials);

        UserCredentials GetUserCredentials(string userName, bool isTemp = false, bool returnDeleted = false);

        UserCredentials GetUserCredentials(long userId, bool isTemp = false, bool returnDeleted = false);

        bool CreateTempUserCredentials(string userName, string password);

        IList<UserCredentials> GetUsersCredentials(long organisationId = -1);
        IList<UserCredentials> GetUsersCredentialsByUsername(string username);
        bool CreateUserCredentials(UserCredentials userCredentials, long id);
    }
}