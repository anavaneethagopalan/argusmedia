using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Caching;
using System.Transactions;

using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AOM.Services.ProductService;
using Utils.Logging.Utils;

namespace AOM.Services.CrmService
{
    public class UserService : IUserService
    {
        private readonly IDbContextFactory _dbfactory;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IProductService _productService;

        public UserService(IDbContextFactory dbfactory, IDateTimeProvider dateTimeProvider,
            IProductService productService)
        {
            _dbfactory = dbfactory;
            _dateTimeProvider = dateTimeProvider;
            _productService = productService;
        }

        private int? _cacheTimeSeconds = null;

        private int CacheExpirationSeconds
        {
            get
            {
                if (!_cacheTimeSeconds.HasValue)
                {
                    int cachExpirySeconds = 60;
                    var cacheTimeSeconds = ConfigurationManager.AppSettings["GetUserWithPrivsSlidingCacheTimeSeconds"];
                    if (!string.IsNullOrEmpty(cacheTimeSeconds))
                    {
                        int ces = -1;
                        int.TryParse(cacheTimeSeconds, out ces);
                        if (ces > 0)
                        {
                            cachExpirySeconds = ces;
                        }
                    }

                    _cacheTimeSeconds = cachExpirySeconds;
                }

                Log.InfoFormat("Sliding Cache Expiration set to:{0}", _cacheTimeSeconds.Value);
                return _cacheTimeSeconds.Value;
            }
        }

        public IList<User> GetUsers(long organisationId)
        {
            using (IUserModel context = _dbfactory.CreateUserModel())
            {
                List<UserInfoDto> users = context.UserInfoes.ToList();
                return
                    users.Select(user => user.ToEntity())
                        .Where(u => organisationId == -1 || u.OrganisationId == organisationId)
                        .ToList();
            }
        }

        public UserInfoDto GetUserInfoDto(string username)
        {
            using (IUserModel context = _dbfactory.CreateUserModel())
            {
                var user =
                    context.UserInfoes.FirstOrDefault(u => u.Username.Equals(username) && u.IsActive && !u.IsDeleted);
                return user;
            }
        }

        public UserContactDetails GetContactDetails(long userId)
        {
            using (IUserModel context = _dbfactory.CreateUserModel())
            {
                UserInfoDto currentUser = context.UserInfoes.Include(u => u.OrganisationDto).First(u => u.Id == userId);

                return new UserContactDetails
                {
                    Id = currentUser.Id,
                    Title = currentUser.Title,
                    Name = currentUser.Name,
                    Email = currentUser.Email,
                    Telephone = currentUser.Telephone,
                    OrganisationName = currentUser.OrganisationDto.Name,
                    OrganisationId = currentUser.OrganisationDto.Id
                };
            }
        }

        public long GetUserId(string username)
        {
            UserInfoDto user = null;
            try
            {
                user = GetUserInfoDto(username);
            }
            catch (Exception ex)
            {
                Log.Error("CrmService.GetUserId for user:" + username, ex);
            }

            return user == null ? -1024 : user.Id;
        }


        public IOrganisation GetUsersOrganisation(long userId)
        {
            using (new TransactionScope(TransactionScopeOption.Suppress))
            {
                using (IUserModel context = _dbfactory.CreateUserModel())
                {
                    var org = context.UserInfoes.AsNoTracking()
                        .Where(u => u.Id == userId)
                        .Select(u => u.OrganisationDto)
                        .FirstOrDefault();

                    if (org == null) return null;

                    return org.ToEntity();
                }
            }
        }

        public List<UserLoginSource> GetUserLoginSources()
        {
            using (IUserModel context = _dbfactory.CreateUserModel())
            {
                context.AutoDetectChangesEnabled = false;
                List<UserLoginSourceDto> userLoginSources = context.UserLoginSources.AsNoTracking().ToList();
                return userLoginSources.Select(es => es.ToEntity()).ToList();
            }
        }

        public List<UserLoginExternalAccount> GetUsersLoginExternalAccounts(long userId)
        {
            using (IUserModel context = _dbfactory.CreateUserModel())
            {
                context.AutoDetectChangesEnabled = false;
                List<UserLoginExternalAccountDto> externalAccounts =
                    context.UserLoginExternalAccounts.AsNoTracking().Where(esa => esa.UserInfo_Id_fk == userId).ToList();
                return externalAccounts.Select(esa => esa.ToEntity()).ToList();
            }
        }

        public bool RecordLoginIpForUser(string ip, string username, bool successfulLogin, string loginFailureReason,
            long userLoginSourceId)
        {
            using (IAuthenticationModel context = _dbfactory.CreateAuthenticationModel())
            {
                var newAuthHistory = new AuthenticationHistory
                {
                    DateCreated = _dateTimeProvider.Now.ToUniversalTime(),
                    IpAddress = ip,
                    SuccessfulLogin = successfulLogin,
                    Username = username,
                    LoginFailureReason = successfulLogin ? "" : loginFailureReason,
                    UserLoginSourceId = userLoginSourceId
                };

                context.AuthenticationHistory.Add(newAuthHistory.ToDto());
                int result = context.SaveChangesAndLog(-1);
                return result > 0;
            }
        }

        public SystemPrivileges GetSystemPrivileges(long userId)
        {
            using (IPrivilegeModel context = _dbfactory.CreatePrivilegeModel())
            {
                context.AutoDetectChangesEnabled = false; //faster query

                IQueryable<SystemRolePrivilegeDto> systemprivroles = (from uir in context.UserInfoRoles
                    join spr in context.SystemRolePrivileges on uir.Role_Id_fk equals spr.Role_Id_fk
                    where uir.User.Id == userId
                    select spr);

                List<string> privs =
                    context.SystemPrivileges.Where(y => systemprivroles.Any(x => x.Privilege_Id_fk.Equals(y.Id)))
                        .Select(x => x.Name)
                        .ToList();

                return new SystemPrivileges {Privileges = privs};
            }
        }

        public bool CheckUserHasContentStream(string userName, long contentStreamId)
        {
            using (IArgusCrmFeedModel context = _dbfactory.CreateArgusCrmFeedModel())
            {
                string argusCrmUsername = context.UserInfoes.Where(u => u.Username == userName)
                                              .Select(u => u.ArgusCrmUsername).FirstOrDefault() ?? string.Empty;

                long?[] userContentStreams = context.UserModules.Where(um => um.ArgusCrmUsername == argusCrmUsername)
                    .Select(um => um.StreamId).ToArray();

                return userContentStreams.Contains(contentStreamId);
            }
        }

        public IList<long> GetUserOrganisationRoles(long userId)
        {
            using (var context = _dbfactory.CreatePrivilegeModel())
            {
                context.AutoDetectChangesEnabled = false;
                return
                    context.UserInfoRoles.Where(ui => ui.UserInfo_Id_fk == userId).Select(or => or.Role_Id_fk).ToList();
            }
        }

        public bool UserEntitledToProduct(long userId, long productId)
        {
            var user = GetUserWithPrivilegesFromDb(userId);
            var userEntitledToProduct = false;
            foreach (var pp in user.ProductPrivileges)
            {
                if (pp.ProductId == productId)
                {
                    userEntitledToProduct = true;
                    break;
                }
            }

            return userEntitledToProduct;
        }
        public bool CheckUserHasPrivilege(string username, long organisationId, long productId, string privilege)
        {
            //NOTE: We avoid using existing util methods that build and return a user object to improve performance - speed went from 400ms to 80ms.
            using (var db = _dbfactory.CreateCrmModel())
            {
                var userQry = db.UserInfoes.Where(s => s.Username == username);

                if (organisationId != -1)
                {
                    userQry = userQry.Where(s => s.Organisation_Id_fk == organisationId);
                }
                var userResult = userQry.Select(u => u.Id);

                if (productId == -1) return userResult.Any();

                var productPrivilegesDb =
                    from usr in userResult
                    from uir in db.UserInfoRoles
                    join prp in db.ProductRolePrivileges on uir.Role_Id_fk equals prp.Role_Id_fk
                    where uir.User.Id == usr && prp.Product_Id_fk == productId
                    select prp.ProductPrivilege.Name;

                if (!String.IsNullOrEmpty(privilege))
                {
                    productPrivilegesDb = productPrivilegesDb.Where(priv => priv == privilege);
                }

                return productPrivilegesDb.Any();
            }
        }

        public List<long?> GetUserContentStreams(long userId)
        {
            if (userId >= 0)
            {
                using (IArgusCrmFeedModel context = new ArgusCrmFeedModel())
                {
                    string argusCrmUsername =
                        context.UserInfoes.Where(u => u.Id == userId).Select(u => u.ArgusCrmUsername).FirstOrDefault() ??
                        string.Empty;
                    IQueryable<long?> userContentStreamIds =
                        context.UserModules.Where(um => um.ArgusCrmUsername == argusCrmUsername)
                            .Select(um => um.StreamId);
                    return userContentStreamIds.ToList();
                }
            }

            return new List<long?>();
        }

        public bool CheckUserHasContentStreams(long userId, List<long> articleContentStreamIds)
        {
            if (userId >= 0)
            {
                using (IArgusCrmFeedModel context = new ArgusCrmFeedModel())
                {
                    string argusCrmUsername =
                        context.UserInfoes.Where(u => u.Id == userId).Select(u => u.ArgusCrmUsername).FirstOrDefault() ??
                        string.Empty;
                    IQueryable<long?> userContentStreamIds =
                        context.UserModules.Where(um => um.ArgusCrmUsername == argusCrmUsername)
                            .Select(um => um.StreamId);
                    return CheckUserHasContentStreams(userContentStreamIds.ToList(), articleContentStreamIds);
                }
            }
            return false;

        }

        public bool CheckUserHasContentStreams(List<long?> userContentStreams, List<long> contentStreamsToCheck)
        {
            if (contentStreamsToCheck == null || contentStreamsToCheck.Count <= 0)
            {
                // Article has NO content streams - what should we do here - it's NOT free - but NO content stream.  
                Log.Info("CheckUserHasContentStreams has been called with NO content streams.");
                return false;
            }

            foreach (var uCs in userContentStreams)
            {
                if (uCs.HasValue)
                {
                    if (contentStreamsToCheck.Contains(uCs.Value))
                    {
                        Log.InfoFormat(
                            "CheckUserHasContentStreams - Returning true for Article Content Streams {0} checking against Article Content Streams {1}",
                            ToLogString(contentStreamsToCheck),
                            ToLogString(userContentStreams));
                        return true;
                    }
                }
            }

            return false;
        }

        private static string ToLogString<T>(List<T> xs)
        {
            return (xs != null && xs.Count > 0) ? string.Format("[{0}]", string.Join(",", xs)) : "[]";
        }

        public IUser GetUserWithPrivileges(string username)
        {
            UserInfoDto dto = GetUserInfoDto(username);
            return dto != null ? GetUserWithPrivileges(dto.Id) : null;
        }

        public IUser GetUserWithPrivileges(long userId)
        {
            var cacheKey = userId.ToString();
            IUser user = null;

            if (MemoryCache.Default.Contains(cacheKey))
            {
                Log.Debug(string.Format("UserService - get user privileges from CACHE for userId: {0}", cacheKey));
                user = MemoryCache.Default[cacheKey] as User;
            }
            else
            {
                Log.Debug(string.Format("UserService - get user privileges from DB for userId: {0}. Adding to cache",
                    cacheKey));
                user = GetUserWithPrivilegesFromDb(userId);
                MemoryCache.Default.Set(cacheKey, user,
                    new CacheItemPolicy() {SlidingExpiration = new TimeSpan(0, 0, 0, CacheExpirationSeconds)});
            }

            return user;
        }

        private class AllUsersProductPrivileges
        {
            public long UserId { get; set; }
            public long ProductId { get; set; }
            public List<ProductNameMarketOpen> Privileges { get; set; }
        }

        private class AllUsersSystemPrivileges
        {
            public long UserId { get; set; }
            public List<string> Privileges { get; set; }
        }

        private class ProductNameMarketOpen
        {
            public string Name { get; set; }
            public bool MarketOpen { get; set; }
        }

        public IUser GetUserWithPrivilegesFromDb(long userId)
        {
            var sw = new Stopwatch();
            sw.Start();
            User user = null;
            var prpCacheKey = "prp";
            // var prodDefCacheKey = "prodDef";
            var srpCacheKey = "srp";

//            if (!MemoryCache.Default.Contains(prpCacheKey))
//            {
//                Log.Debug("UserService - get product definitions from DB and adding to cache");
//                MemoryCache.Default.Set(prodDefCacheKey, _productService.GetAllProductDefinitions(),
//                    new CacheItemPolicy() {SlidingExpiration = new TimeSpan(0, 0, 0, CacheExpirationSeconds)});
//            }

            if (!MemoryCache.Default.Contains(srpCacheKey))
            {
                Log.Debug("UserService - get system role privileges from DB and adding to cache");
                using (IPrivilegeModel context = _dbfactory.CreatePrivilegeModel())
                {
                    context.AutoDetectChangesEnabled = false; //faster query

                    var allSystemPrivileges = (from uir in context.UserInfoRoles
                        join spr in context.SystemRolePrivileges on uir.Role_Id_fk equals spr.Role_Id_fk
                        //where uir.User.Id == userId
                        group spr by new {uir.UserInfo_Id_fk}
                        into s
                        select new AllUsersSystemPrivileges
                        {
                            UserId = s.Key.UserInfo_Id_fk,
                            Privileges = s.Select(p => p.SystemPrivilege.Name).ToList()
                        }).AsNoTracking().ToList();

                    MemoryCache.Default.Set(srpCacheKey, allSystemPrivileges,
                        new CacheItemPolicy() {SlidingExpiration = new TimeSpan(0, 0, 0, CacheExpirationSeconds)});
                }
            }

            if (!MemoryCache.Default.Contains(prpCacheKey))
            {
                Log.Debug(
                    string.Format("UserService - get product role privileges from DB for userId: {0}. Adding to cache",
                        userId));
                using (IPrivilegeModel context = _dbfactory.CreatePrivilegeModel())
                {
                    context.AutoDetectChangesEnabled = false; //faster queries

                    var allUserProductPrivileges = (from uir in context.UserInfoRoles
                        join prp in context.ProductRolePrivileges on uir.Role_Id_fk equals prp.Role_Id_fk
                        //where uir.User.Id == userId
                        group prp by new {prp.Product_Id_fk, uir.UserInfo_Id_fk}
                        into g
                        select new AllUsersProductPrivileges
                        {
                            UserId = g.Key.UserInfo_Id_fk,
                            ProductId = g.Key.Product_Id_fk,
                            Privileges = g.Select(p => new ProductNameMarketOpen
                            {
                                Name = p.ProductPrivilege.Name,
                                MarketOpen = p.ProductPrivilege.MarketMustBeOpen
                            }).ToList()
                        }).AsNoTracking().ToList();

                    MemoryCache.Default.Set(prpCacheKey, allUserProductPrivileges,
                        new CacheItemPolicy() {SlidingExpiration = new TimeSpan(0, 0, 0, CacheExpirationSeconds)});
                }
            }

            UserInfoDto currentUser = null;
            using (IPrivilegeModel context = _dbfactory.CreatePrivilegeModel())
            {
                currentUser = context.UserInfoes.Include(u => u.OrganisationDto).First(u => u.Id == userId);
            }

            Log.Debug(
                string.Format(
                    "UserService CACHE - retrieving product role privileges, system role privileges and product definitions from CACHE for userId: {0}",
                    userId));
            var productRolePrivileges = MemoryCache.Default[prpCacheKey] as List<AllUsersProductPrivileges>;
            // var allProductDefinitions = MemoryCache.Default[prodDefCacheKey] as List<ProductDefinitionItem>;
            var systemRolePrivileges = MemoryCache.Default[srpCacheKey] as List<AllUsersSystemPrivileges>;

            var productPrivileges = new List<UserProductPrivilege>();
            foreach (var p in productRolePrivileges.Where(prp => prp.UserId == userId))
            {
                var pDict = new Dictionary<string, Boolean>();
                foreach (var pp in p.Privileges)
                {
                    pDict[pp.Name] = pp.MarketOpen;
                }
                productPrivileges.Add(new UserProductPrivilege {ProductId = p.ProductId, Privileges = pDict});
            }

            // var systemPrivileges = GetSystemPrivileges(userId);
            var allProductDefinitions = _productService.GetAllProductDefinitions();
            foreach (var pp in productPrivileges)
            {
                var pdItem = GetProductDefinition(pp.ProductId, allProductDefinitions);
                pp.DisplayOrder = GetProductDisplayOrder(pdItem);
                pp.Name = GetProductName(pdItem);
                pp.HasAssessment = GetProductHasAssessment(pdItem);
            }

            var systemPrivileges =
                systemRolePrivileges.Where(srp => srp.UserId == userId).SelectMany(p => p.Privileges).ToList();

            user = new User
            {
                Email = currentUser.Email,
                Name = currentUser.Name,
                Id = currentUser.Id,
                Username = currentUser.Username,
                OrganisationId = currentUser.Organisation_Id_fk,
                IsBlocked = currentUser.IsBlocked,
                IsActive = currentUser.IsActive,
                UserOrganisation = currentUser.OrganisationDto.ToEntity(),
                ProductPrivileges = productPrivileges.OrderBy(pp => pp.DisplayOrder).ToList(),
                SystemPrivileges = new SystemPrivileges {Privileges = systemPrivileges},
                Telephone = currentUser.Telephone
                //ArgusCrmUsername = currentUser.ArgusCrmUsername
            };

            sw.Stop();
            Log.ErrorFormat("GetUserWithPrivileges took : {0}ms", sw.ElapsedMilliseconds);

            return user;
        }

        private bool GetProductHasAssessment(ProductDefinitionItem pdItem)
        {
            return (pdItem != null) ? pdItem.HasAssessment : false;
        }

        private ProductDefinitionItem GetProductDefinition(long productId, List<ProductDefinitionItem> allProductDefinitions)
        {
            ProductDefinitionItem pdItem = null;
            if (allProductDefinitions != null)
            {
                pdItem = allProductDefinitions.FirstOrDefault(p => p.ProductId == productId);
                
            }

            return pdItem;
        }

        private long GetProductDisplayOrder(ProductDefinitionItem pdItem)
        {
            return (pdItem != null) ? pdItem.DisplayOrder : 0;
        }

        private string GetProductName(ProductDefinitionItem pdItem)
        {
            return (pdItem != null) ? pdItem.ProductName : "";
        }

        public void UserDisconnect(long userId, string disconnectReason)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                var newDisconnectHistory = new DisconnectHistory
                {
                    UserId = userId,
                    DisconnectReason = disconnectReason,
                    DateCreated = _dateTimeProvider.Now.ToUniversalTime()
                };

                context.DisconnectHistory.Add(newDisconnectHistory.ToDto());
                context.SaveChangesAndLog(-1);
            }
        }

        public bool SaveClientLog(ClientLogging clientLog)
        {
            using (IClientLogModel context = _dbfactory.CreateClientLogModel())
            {
                //var clientLogRecord = context.ClientLogs.First(cl => cl.UserId == clientLog.UserId && cl.RunDate == clientLog.RunDate);
                //var clientLogRecord = context.ClientLogs;

                var clientLogDto = new ClientLoggingDto()
                {
                    UserId = clientLog.UserId,
                    RunJob = clientLog.RunJob == null ? new Random().Next(500000) : clientLog.RunJob,
                    Message = clientLog.Message,
                    LoginPageLoadedClient = clientLog.LoginPageLoadedClient,
                    LoginButtonClickedClient = clientLog.LoginButtonClickedClient,
                    WebSvcAuthenticatedClient = clientLog.WebSvcAuthenticatedClient,
                    DiffusionAuthenticatedClient = clientLog.DiffusionAuthenticatedClient,
                    WebSktUserJsonReceivedClient = clientLog.WebSktUserJsonReceivedClient,
                    WebSktProdDefJsonReceivedClient = clientLog.WebSktProdDefJsonReceivedClient,
                    WebSktAllJsonReceivedClient = clientLog.WebSktAllJsonReceivedClient,
                    TransitionToDashboardClient = clientLog.TransitionToDashboardClient,
                    DashboardRenderedClient = clientLog.DashboardRenderedClient
                };

                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    try
                    {
                        context.ClientLogs.Add(clientLogDto);
                        //context.ClientLogs.AddOrUpdate(cl => new { cl.UserId, cl.RunJob }, clientLogDto);
                        int result = context.SaveChangesAndLog(-1);
                        trans.Complete();
                        return result > 0;
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error saving ClientLog record: ", ex);
                        return false;
                    }
                }
            }
        }
    }
}