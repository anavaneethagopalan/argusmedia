﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Services.CrmService
{
    public interface IArgusCrmConsumerService
    {
        IEnumerable<AomUserModuleViewEntity> GetLatestData(string queryString);
    }
}