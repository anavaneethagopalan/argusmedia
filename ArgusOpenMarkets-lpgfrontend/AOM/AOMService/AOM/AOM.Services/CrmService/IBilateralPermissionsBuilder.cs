using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;

namespace AOM.Services.CrmService
{
    public interface IBilateralPermissionsBuilder
    {
        //BilateralMatrixPermissionPair BuildBilateralMatrixPermissionPair(MatrixPermission organisationPermission, IDictionary<long, string> userCache, MatrixPermission counterpartyPermission);

        List<BilateralMatrixPermissionPair> GetIntraOrganisationPairs(IDictionary<long, string> userCache, List<IIntraOrganisationPermission> fullSetOfIntraOrganisationPermissionsForProduct, 
            IList<long> subscribedTradingOrganisationsToProduct, IList<long> subscribedCounterpartiesToProduct, long productId, long marketId, List<OrganisationDto> organisationList);
    }
}