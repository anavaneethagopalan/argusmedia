﻿using AOM.App.Domain.Dates;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using Utils.Logging.Utils;

namespace AOM.Services.CrmService
{
    public class UserTokenService : IUserTokenService
    {
        private readonly IDbContextFactory _dbfactory;
        private readonly IDateTimeProvider _dateTimeProvider;

        public UserTokenService(IDbContextFactory dbfactory, IDateTimeProvider dateTimeProvider)
        {
            _dbfactory = dbfactory;
            _dateTimeProvider = dateTimeProvider;
        }

        public IUserToken GetTokenByUserId(long userId)
        {
            IUserToken token = null;
            using (var context = _dbfactory.CreateCrmModel())
            {
                var poco = context.UserTokens.FirstOrDefault(t => t.UserInfo_Id_fk == userId);
                if (poco != null)
                {
                    token = CreateTokenFromTokenDto(poco);
                }

                return token;
            }
        }

        private IUserToken CreateTokenFromTokenDto(UserTokenDto poco)
        {
            var token = new UserToken
            {
                Id = poco.Id,
                LoginTokenString = poco.LoginTokenString,
                SessionId = poco.SessionId,
                TimeCreated = poco.TimeCreated,
                UserInfo_Id_fk = poco.UserInfo_Id_fk,
                SessionTokenString = poco.SessionTokenString,
                UserIpAddress = poco.UserIpAddress,
                User = poco.UserInfoDto == null ? new User() : new User
                {
                    Id = poco.UserInfoDto.Id,
                    IsActive = poco.UserInfoDto.IsActive,
                    Email = poco.UserInfoDto.Email,
                    IsBlocked = poco.UserInfoDto.IsBlocked,
                    IsDeleted = poco.UserInfoDto.IsDeleted,
                    Name = poco.UserInfoDto.Name,
                    OrganisationId = poco.UserInfoDto.Organisation_Id_fk,
                    UserOrganisation = poco.UserInfoDto.OrganisationDto == null ? new Organisation() : new Organisation
                    {
                        Id = poco.UserInfoDto.OrganisationDto.Id,
                        Address = poco.UserInfoDto.OrganisationDto.Address,
                        DateCreated = poco.UserInfoDto.OrganisationDto.DateCreated,
                        Description = poco.UserInfoDto.OrganisationDto.Description,
                        OrganisationType = DtoMappingAttribute.FindEnumWithValue<OrganisationType>(poco.UserInfoDto.OrganisationDto.OrganisationType),
                        IsDeleted = poco.UserInfoDto.OrganisationDto.IsDeleted,
                        LegalName = poco.UserInfoDto.OrganisationDto.Name,
                        Name = poco.UserInfoDto.OrganisationDto.Name,
                        ShortCode = poco.UserInfoDto.OrganisationDto.ShortCode
                    },
                    Username = poco.UserInfoDto.Username
                }
            };
            return token;
        }

        public IUserToken GetLoginTokenByGuid(string tokenGuid)
        {
            IUserToken token = null;
            using (var context = _dbfactory.CreateCrmModel())
            {
                var poco = context.UserTokens.Include(x => x.UserInfoDto).FirstOrDefault(t => t.LoginTokenString == tokenGuid);
                if (poco != null)
                {
                    token = CreateTokenFromTokenDto(poco);
                }
            }

            return token;
        }

        public IUserToken GetSessionTokenByGuid(string tokenGuid)
        {
            IUserToken token = null;
            using (var context = _dbfactory.CreateCrmModel())
            {
                var poco = context.UserTokens.FirstOrDefault(t => t.SessionTokenString == tokenGuid);
                if (poco != null)
                {
                    token = CreateTokenFromTokenDto(poco);
                }
            }

            return token;
        }

        public bool IsValidLoginTokenForUser(string token, long userId)
        {
            bool isValid;

            using (var context = _dbfactory.CreateCrmModel())
            {
                isValid = context.UserTokens.Count(t => (t.SessionTokenString == token && t.UserInfo_Id_fk == userId)) == 1;
            }

            return isValid;
        }

        public IUserToken InsertNewUserToken(long userId, string userName, string ipAddress = "")
        {
            IUserToken resultToken = null;
            using (var context = _dbfactory.CreateCrmModel())
            {
                Log.Info("InsertNewUserToken Start");
                try
                {
                    //var token = context.UserTokens.Include(u => u.UserInfoDto).FirstOrDefault(t => t.UserInfo_Id_fk == userId);
                    //var userDto = context.UserInfoes.Include("UserTokens").FirstOrDefault(t => t.Id == userId);

                    var users = (from ui in context.UserInfoes
                        join ut in context.UserTokens on ui.Id equals ut.UserInfo_Id_fk into tokens
                        from token in tokens.DefaultIfEmpty()
                        where ui.Id == userId
                        select new {ui, token}).FirstOrDefault();

                    var userInfo = users.ui;
                    var userToken = users.token;

                    if (userInfo == null)
                    {
                        //no User found
                        Log.Error(String.Format("Trying to insert new user token: No user found with userid {0} or no db connection!", userId));
                        return null;
                    }

                    if (userToken == null)
                    {
                        // get the user 
                        //var userInfoPoco = context.UserInfoes.FirstOrDefault(u => u.Id == userId);

                        var tokenPoco = new UserTokenDto
                        {
                            TimeCreated = _dateTimeProvider.Now.ToUniversalTime(),
                            LoginTokenString = Guid.NewGuid().ToString(),
                            SessionTokenString = Guid.NewGuid().ToString(),
                            UserIpAddress = ipAddress,
                            UserInfo_Id_fk = userId,
                            UserInfoDto = userInfo
                        };

                        //add a new token for that user
                        context.UserTokens.Add(tokenPoco);
                        context.SaveChangesAndLog(-1);

                        resultToken = CreateTokenFromTokenDto(tokenPoco);
                        Log.Info(String.Format("InsertNewUserToken Finish - Created a new token for user: {0}", userName));
                    }
                    else
                    {
                        //add a new Guid for the token
                        userToken.LoginTokenString = Guid.NewGuid().ToString();
                        userToken.SessionTokenString = Guid.NewGuid().ToString();
                        userToken.TimeCreated = _dateTimeProvider.Now.ToUniversalTime();
                        userToken.UserIpAddress = ipAddress;

                        context.SaveChangesAndLog(-1);
                        resultToken = CreateTokenFromTokenDto(userToken);
                        Log.Info(String.Format("InsertNewUserToken Finish - Refreshed token for user: {0}", userName));
                    }
                }

                catch (Exception ex)
                {
                    Log.Error(String.Format("UserTokenService.InsertNewUserToken: Failed to insert login token for user: {0}. No changes were saved to DB.", userName), ex);
                }
            }

            return resultToken;
        }

        public void DeleteTokensForUser(long userId)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                var tokens = context.UserTokens.Where(t => t.UserInfo_Id_fk == userId).ToList();
                tokens.ForEach(a => context.UserTokens.Remove(a));
                context.SaveChangesAndLog(-1);
            }
        }
    }
}