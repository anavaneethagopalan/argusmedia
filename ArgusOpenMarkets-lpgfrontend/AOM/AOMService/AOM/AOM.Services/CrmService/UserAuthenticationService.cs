﻿using AOM.App.Domain.Dates;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using Utils.Logging.Utils;

namespace AOM.Services.CrmService
{
    public class UserAuthenticationService : IUserAuthenticationService
    {
        private const string AccountInaccessibleMessage = "This account is currently inaccessible. Please contact the Argus AOM administration team. Email: [[EMAILADDRESS]].  Tel +44 020 7199 9430";
        private const string UserBlockedMessage = "This account is currently blocked by AOM. Please contact the Argus AOM administration team. Email: [[EMAILADDRESS]].  Tel +44 020 7199 9430";
        private const string SystemError = "AOM is currently unavailable";

        private readonly IUserLoginTrackingService _userLoginTrackingService;
        private readonly IUserTokenService _userTokenService;
        private readonly IUserService _userService;
        private readonly ICredentialsService _credentialsService;
        private readonly ICrmAdministrationService _crmAdministrationService;
        private readonly IDateTimeProvider _dateTimeProvider;

        private string _unSuccessfullLoginMsg = "Invalid login details, please try again.";

        public UserAuthenticationService(IUserLoginTrackingService userLoginTackingService, IUserTokenService userTokenService, IUserService userService,
            ICredentialsService credentialsService, ICrmAdministrationService crmAdministrationService, IDateTimeProvider dateTimeProvider)
        {
            //_loginValidationService = loginValidationService;
            _userTokenService = userTokenService;
            _userService = userService;
            _credentialsService = credentialsService;
            _crmAdministrationService = crmAdministrationService;
            _dateTimeProvider = dateTimeProvider;
            _userLoginTrackingService = userLoginTackingService;
        }

        public AuthenticationResult AuthenticateUser(string userName, string password, long userLoginSourceId, string ipAddress = "")
        {
            string errorReason = "";
            try
            {
                Log.Info("AuthenticateUser Start for user: " + userName);
                IList<UserCredentials> userCredentials;
                User user;
                try
                {
                    //credentialswithUser = _credentialsService.GetUserCredentials(userName, returnDeleted: true);
                    userCredentials = _credentialsService.GetUsersCredentialsByUsername(userName);
                    user = userCredentials.Where(u => u.User != null).Select(u => u.User).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    errorReason = string.Format("In AuthenticateUser, Exception thrown while trying to get Credentials for user - {0}", userName);
                    Log.Error(errorReason, ex);
                    return CreateErrorAuthenticationResult(SystemError, errorReason);
                }
 
                if (user == null)
                {
                    errorReason = string.Format("In AuthenticateUser, No User found with username - {0} ", userName);
                    Log.Info(errorReason);
                    return CreateErrorAuthenticationResult(_unSuccessfullLoginMsg, errorReason);
                }

                if (user.IsBlocked)
                {
                    errorReason = string.Format("User {0} is blocked. Denying...", userName);
                    Log.Info(errorReason);
                    return CreateErrorAuthenticationResult(UserBlockedMessage, errorReason);
                }
                
                if (!user.IsActive || user.IsDeleted)
                {
                    errorReason = string.Format("User {0} is {1}. Denying...", userName, user.IsDeleted ? "deleted" : "not active");
                    Log.Info(errorReason);
                    return CreateErrorAuthenticationResult(UserBlockedMessage, errorReason);
                }               

                var userOrganisation = _userService.GetUsersOrganisation(user.Id);
                if (userOrganisation.IsDeleted == true)
                {
                    var msg = "Your organisation has been disabled. No users for this organisation will be able to login. Please contact AOM Support for more information";
                    errorReason = msg;
                    Log.Info(msg);
                    return CreateErrorAuthenticationResult(msg, errorReason);
                }
                
                //if password is OK and user is not blocked - allow
                var credentials = userCredentials.FirstOrDefault(c => c.PasswordHashed.Equals(password));
                if (credentials != null)
                {
                    if (credentials.Expiration < _dateTimeProvider.UtcNow)
                    {
                        errorReason = string.Format("Password has expired for user {0}. Please contact your administrator", credentials.User.Username);
                        Log.Info(errorReason);
                        return CreateErrorAuthenticationResult(errorReason, errorReason);
                    }
                    //check is user has any login session and if yes, remove them
                    //_loginValidationService.ResetUser(userName);
                    _userLoginTrackingService.ResetUserAccount(credentials.User.Id, userLoginSourceId);

                    var message = "User authenticated successfully";
                    Log.Info(String.Format("User: {0} is not blocked and password is OK, allowing to log in.", userName));
                    var authenticatedUser = new User
                    {
                        Id = credentials.User.Id,
                        Username = credentials.User.Username
                    };

                    var token = _userTokenService.InsertNewUserToken(credentials.User.Id, userName, ipAddress);

                    if (token != null)
                    {
                        Log.Info(String.Format("In AuthenticateUser, token for user added successfully: {0}", token != null));
                        Log.Info("AuthenticateUser Finish");
                        return CreateAuthenticationResult(true, authenticatedUser, message, token.LoginTokenString, token.SessionTokenString, 
                            DtoMappingAttribute.GetValueFromEnum(credentials.CredentialType), credentials.FirstTimeLogin);
                    }
                }
                else
                {
                    errorReason = string.Format("Password incorrect for user {0}. Denying ...", userName);
                    Log.Info(errorReason);
                }

                //var isAllowed = _loginValidationService.ValidateUser(userName);
                //if (!isAllowed)

                // Register that the user has attempted to login, and failed.
                _userLoginTrackingService.RegisterLoginAttempt(user.Id, userLoginSourceId);
                //test

                // Has the user reached the limit of login attempts? Block them if so.
                if (_userLoginTrackingService.IsUserAtLoginAttemptLimit(user.Id, userLoginSourceId))
                {
                    errorReason = string.Format("User {0} has been blocked! Too many unsuccessful attempts.", userName);
                    Log.Info(errorReason);
                    _crmAdministrationService.BlockUser(user.Username, -1);

                    return CreateErrorAuthenticationResult(AccountInaccessibleMessage, errorReason);
                }
                
                return CreateErrorAuthenticationResult(_unSuccessfullLoginMsg, errorReason);
            }
            catch (Exception ex)
            {
                errorReason = "UserAuthenticationService.AuthenticationUser - Error for user:" + userName;
                Log.Error(errorReason, ex);
                return CreateErrorAuthenticationResult("Something went wrong with our system. Please try again later.", errorReason);
            }
        }

        public IAuthenticationResult AuthenticateSessionToken(string token, string userid)
        {
            try
            {
                string message;
                var tokenFromDb = _userTokenService.GetSessionTokenByGuid(token);

                if (tokenFromDb == null || userid == null)
                {
                    message = String.Format("Could not get a token with guid: {0} and userid: {1}", token, userid);
                    Log.Info(message);
                    return CreateAuthenticationResult(false, null, "Something went wrong with our system. Please try again later.", string.Empty, string.Empty, string.Empty);
                }

                // UserId is different - deny
                if (tokenFromDb.UserInfo_Id_fk != long.Parse(userid))
                {
                    message = String.Format("Different username for token: {0}. User id of request: {1} ; user id of token from db: {2} ", token, userid, tokenFromDb.UserInfo_Id_fk);
                    Log.Warn(message);
                    return CreateAuthenticationResult(false, null, message, null, null, null);
                }
                Log.Info(String.Format("In UserAuthenticationService.AuthenticateSessionToken: Successfully authenticated token {0} and user id: {1}", token, userid));
                return CreateAuthenticationResult(true, null, null, null, null, null);
            }

            catch(Exception ex)
            {
                const string errorReason = "AuthenticateSessionToken error";
                Log.Error(errorReason, ex);
                return CreateErrorAuthenticationResult(errorReason);
            }
        }

        public IAuthenticationResult AuthenticateToken(string username, string token)
        {
            try
            {                
                string message;
                var tokenFromDb = _userTokenService.GetLoginTokenByGuid(token);

                if (tokenFromDb == null)
                {
                    message = String.Format("Could not get a token for username: {0}", username);
                    Log.Info(message);
                    return CreateAuthenticationResult(false, null, "Something went wrong with our system. Please try again later.", string.Empty, string.Empty, string.Empty);
                }

                // Username is different check - ADDED Case Insensitivity - should allow t1 to be the same as T1.
                if (tokenFromDb.User.Username.ToLower() != username.ToLower())
                {
                    message = String.Format("Incorrect username for that token: token's username: {0}, passed username: {1}.", tokenFromDb.User.Username, username);
                    Log.Info(message);
                    return CreateAuthenticationResult(false, tokenFromDb.User, message, string.Empty, string.Empty, string.Empty);
                }

                int ttl;
                try
                {
                    ttl = Int32.Parse(ConfigurationManager.AppSettings["tokenTtlSecs"]);
                }
                catch (Exception)
                {
                    ttl = 3700;
                }

                DateTime? tokenTimeCreated = tokenFromDb.TimeCreated;
                if (tokenTimeCreated.HasValue)
                {
                    var tokenExpiryTime = tokenTimeCreated.Value.AddSeconds(ttl);
                    if (tokenExpiryTime < _dateTimeProvider.UtcNow)
                    {
                        // the token has expired!
                        message = String.Format("Token expired at {0}. User will need to login again to create a new token.", tokenExpiryTime);
                        Log.Info(message);
                        return CreateAuthenticationResult(false, tokenFromDb.User, message, string.Empty, string.Empty, string.Empty);
                    }
                }

                message = String.Format("Token for username {0} has been validated: authentication successful!", username);

                return CreateAuthenticationResult(true, tokenFromDb.User, message, tokenFromDb.LoginTokenString, tokenFromDb.SessionTokenString, string.Empty);
            }
            catch (Exception ex)
            {
                var errorReason = "AuthenticateToken error for user:" + username;
                Log.Error(errorReason, ex);
                return CreateErrorAuthenticationResult(errorReason);
            }
        }

        //public bool RecordUsersIp(string ip, string username, bool successfulLogin, string loginFailureReason)
        //{
        //    return _userService.RecordLoginIpForUser(ip, username, successfulLogin, loginFailureReason);
        //}

        private static AuthenticationResult CreateErrorAuthenticationResult(string message, string failureReason = null)
        {
            const string defaultMessage = "Something went wrong with our system. Please try again later.";

            var resultMessage = string.IsNullOrEmpty(message) ? defaultMessage : message;
            return new AuthenticationResult
            {
                IsAuthenticated = false,
                User = null,
                Message = resultMessage,
                Token = string.Empty,
                FailureReason = failureReason ?? resultMessage
            };
        }

        private AuthenticationResult CreateAuthenticationResult(bool isAuthenticated, User user, string message, string tokenGuid, string sessionToken, string credentialType, bool firstLogin = false, string failureReason = null)
        {
            return new AuthenticationResult
            {
                IsAuthenticated = isAuthenticated,
                User = user,
                Message = message,
                Token = tokenGuid,
                SessionToken = sessionToken,
                FirstLogin = firstLogin,
                FailureReason = isAuthenticated ? "" : (failureReason ?? message),
                CredentialType = credentialType
            };
        }
    }
}