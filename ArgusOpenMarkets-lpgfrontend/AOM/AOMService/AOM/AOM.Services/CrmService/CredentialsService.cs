﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using Utils.Logging.Utils;

namespace AOM.Services.CrmService
{
    public class CredentialsService : ICredentialsService
    {
        private readonly IDbContextFactory _dbfactory;
        private readonly IDateTimeProvider _dateTimeProvider;

        public CredentialsService(IDbContextFactory dbfactory, IDateTimeProvider dateTimeProvider)
        {
            _dbfactory = dbfactory;
            _dateTimeProvider = dateTimeProvider;
        }

        public bool UpdateUserCredentials(long userId, string oldPasswordHashed, string newPasswordHashed, bool checkOldPassword, bool adminRequest, long requestedByUserId,
            bool isTemporary = false, int tempExpirationInHours = 48)
        {
            if (!adminRequest)
            {
                if (string.IsNullOrEmpty(oldPasswordHashed))
                {
                    throw new BusinessRuleException("Old password is required");
                }

                if (oldPasswordHashed == newPasswordHashed)
                {
                    throw new BusinessRuleException("New password must be different");
                }
            }

            if (string.IsNullOrEmpty(newPasswordHashed))
            {
                throw new BusinessRuleException("New password is required");
            }

            using (var context = _dbfactory.CreateCrmModel())
            {
                UserInfoDto user = context.UserInfoes.FirstOrDefault(u => u.Id.Equals(userId));
                if (user == null)
                    throw new AuthenticationErrorException("User does not exist");
                if (user.IsDeleted)
                    throw new AuthenticationErrorException(string.Format("User: {0} is deleted and password cannot be changed.", user.Username));

                return UpdatePermanentUserCredentials(context, userId, oldPasswordHashed, newPasswordHashed, requestedByUserId, checkOldPassword, adminRequest);
            }
        }

        public bool UpdateUserCredentialsAdmin(long userId, string oldPasswordHashed, string newPasswordHashed, bool checkOldPassword, bool adminRequest, long requestedByUserId, 
            bool isTemporary = false, int tempExpirationInHours = 48)
        {
            if (!adminRequest)
            {
                if (string.IsNullOrEmpty(oldPasswordHashed))
                {
                    throw new BusinessRuleException("Old password is required");
                }

                if (oldPasswordHashed == newPasswordHashed)
                {
                    throw new BusinessRuleException("New password must be different");
                }
            }

            if (string.IsNullOrEmpty(newPasswordHashed))
            {
                throw new BusinessRuleException("New password is required");
            }

            using (var context = _dbfactory.CreateCrmModel())
            {
                UserInfoDto user = context.UserInfoes.FirstOrDefault(u => u.Id.Equals(userId));
                if (user == null)
                    throw new AuthenticationErrorException("User does not exist");
                if (user.IsDeleted)
                    throw new AuthenticationErrorException(string.Format("User: {0} is deleted and password cannot be changed.", user.Username));

                if (isTemporary)
                {
                    return UpdateTempUserCredentials(context, userId, oldPasswordHashed, newPasswordHashed,
                        requestedByUserId, adminRequest, tempExpirationInHours);
                }
                
                return UpdatePermanentUserCredentials(context, userId, oldPasswordHashed, newPasswordHashed, requestedByUserId, checkOldPassword, adminRequest);              
            }
        }

        private bool UpdateTempUserCredentials(ICrmModel context, long userId, string oldPasswordHashed, string newPasswordHashed, long requestedByUserId, bool adminRequest, int tempExpirationInHours)
        {
            var tempUserCreds = GetUserCredentialsDto(context, userId, isTemp: true);

            if (tempUserCreds == null)
            {
                var newTempUserCreds = new UserCredentialsDto
                {
                    PasswordHashed = newPasswordHashed,
                    DateCreated = DateTime.UtcNow,
                    DefaultExpirationInMonths = 12,
                    Expiration = _dateTimeProvider.UtcNow.AddHours(tempExpirationInHours),
                    UserInfo_Id_fk = userId,
                    CredentialType = DtoMappingAttribute.GetValueFromEnum(CredentialType.Temporary)
                };
                bool result;
                using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    context.UserCredentials.Add(newTempUserCreds);
                    result = context.SaveChangesAndLog(requestedByUserId) > 0;
                    transactionScope.Complete();
                }
                return result;
            }

            if (!adminRequest)
            {
                Log.Info(string.Format("UpdateUserCredentials: Old pwd: {0} - New pwd: {1}", oldPasswordHashed,
                    tempUserCreds.PasswordHashed));
                if (oldPasswordHashed != tempUserCreds.PasswordHashed)
                    throw new AuthenticationErrorException("Old password does not match current stored password");
            }

            tempUserCreds.PasswordHashed = newPasswordHashed;
            //existUsrCreds.FirstTimeLogin = isTemporary;
            tempUserCreds.Expiration = _dateTimeProvider.UtcNow.AddHours(tempExpirationInHours);

            return context.SaveChangesAndLog(requestedByUserId) > 0;
        }

        private bool UpdatePermanentUserCredentials(ICrmModel context, long userId, string oldPasswordHashed, string newPasswordHashed, long requestedByUserId, bool checkOldPassword, bool adminRequest)
        {
            var tempUserCreds = GetUserCredentialsDto(context, userId, isTemp:true);
            var permUserCreds = GetUserCredentialsDto(context, userId);

            // no credentials for that user
            if (tempUserCreds == null && permUserCreds == null)
                throw new AuthenticationErrorException("UserCredentials do not exist");

            if (permUserCreds == null)
            {
                var newUserCredentialsDto = new UserCredentialsDto
                {
                    PasswordHashed = newPasswordHashed,
                    DateCreated = DateTime.UtcNow,
                    DefaultExpirationInMonths = tempUserCreds.DefaultExpirationInMonths,
                    Expiration = _dateTimeProvider.UtcNow.AddMonths(tempUserCreds.DefaultExpirationInMonths),
                    UserInfo_Id_fk = userId,
                    CredentialType = DtoMappingAttribute.GetValueFromEnum(CredentialType.Permanent)
                };
                bool result;
                using (var transactionScope = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    context.UserCredentials.Add(newUserCredentialsDto);
                    context.UserCredentials.Remove(tempUserCreds);

                    result = context.SaveChangesAndLog(requestedByUserId) > 0;
                    transactionScope.Complete();
                }
                return result;
            }

            if (!adminRequest)
            {
                if (permUserCreds.Expiration < DateTime.UtcNow)
                {
                    throw new AuthenticationErrorException(String.Format("Password has expired for user: {0}.",
                        permUserCreds.User.Username));
                }
            }
            if (checkOldPassword)
            {
                Log.Info(string.Format("UpdateUserCredentials: Old pwd: {0} - New pwd: {1}", oldPasswordHashed,
                    permUserCreds.PasswordHashed));
                if (!IsSamePassword(permUserCreds, oldPasswordHashed) && !IsSamePassword(tempUserCreds, oldPasswordHashed))
                {
                    throw new AuthenticationErrorException("Old password does not match current stored password");
                }
            }

            // all is well, can update credentials
            permUserCreds.PasswordHashed = newPasswordHashed;
            //existUsrCreds.FirstTimeLogin = isTemporary;
            if (tempUserCreds != null)
            {
                context.UserCredentials.Remove(tempUserCreds);
            }

            return context.SaveChangesAndLog(requestedByUserId) > 0;
        }

        private static bool IsSamePassword(UserCredentialsDto existingCredentials, string oldPassword)
        {
            if (existingCredentials == null) return false;
            return existingCredentials.PasswordHashed == oldPassword;
        }

        public bool UpdateUserCredentials(UserCredentials credentials)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                var existUsrCreds = GetUserCredentialsDto(context, credentials.UserId);

                existUsrCreds.PasswordHashed = credentials.PasswordHashed;

                var tempCredentials = GetUserCredentialsDto(context, credentials.UserId, isTemp: true);
                if (tempCredentials != null)
                {
                    context.UserCredentials.Remove(tempCredentials);
                }

                return context.SaveChangesAndLog(credentials.UserId) > 0;
            }
        }

        private static UserCredentialsDto GetUserCredentialsDto(ICrmModel context, long userId = 0, string userName = "", bool isTemp = false, bool returnDeleted = false)
        {
            var credentialType = isTemp ? "T" : "P";
            var credentials = context.UserCredentials.Include(a => a.User).FirstOrDefault(u =>
                (
                    (returnDeleted || !u.User.IsDeleted) &&
                    (u.User.Id.Equals(userId) || u.User.Username.ToLower().Equals(userName.ToLower()))
                ) && u.CredentialType.Equals(credentialType, StringComparison.OrdinalIgnoreCase));
            
            return credentials;
        }

        public UserCredentials GetUserCredentials(string userName, bool isTemp = false, bool returnDeleted = false)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                Log.Info("GetUserCredentials Start");
                var credentials = GetUserCredentialsDto(context, userName: userName, isTemp: isTemp, returnDeleted: returnDeleted);
                Log.Info("GetUserCredentials Finish");
                return credentials == null ? null : credentials.ToEntity();
            }
        }

        public UserCredentials GetUserCredentials(long userId, bool isTemp = false, bool returnDeleted = false)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                var credentials = GetUserCredentialsDto(context, userId, isTemp: isTemp, returnDeleted: returnDeleted);
                return credentials == null ? null : credentials.ToEntity();
            }
        }

        public bool CreateTempUserCredentials(string userName, string password)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                var credentials = GetUserCredentialsDto(context, userName: userName, isTemp: true);

                if (credentials == null)
                {
                    var permCredentials = GetUserCredentialsDto(context, userName: userName);
                    credentials = new UserCredentialsDto
                    {
                        PasswordHashed = password,
                        CredentialType = "T",
                        DateCreated = DateTime.UtcNow,
                        DefaultExpirationInMonths = 0,
                        Expiration = DateTime.UtcNow.AddHours(1),
                        User = permCredentials.User,
                        UserInfo_Id_fk = permCredentials.User.Id,
                        FirstTimeLogin = true,
                    };
                    context.UserCredentials.Add(credentials);
                }
                else
                {
                    credentials.PasswordHashed = password;
                    credentials.Expiration = DateTime.UtcNow.AddHours(1);
                }

                return context.SaveChangesAndLog(credentials.User.Id) > 0;
            }
        }

        public IList<UserCredentials> GetUsersCredentials(long organisationId = -1)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                return
                    context.UserCredentials.Include(c => c.User)
                        .Where(c => organisationId == -1 || organisationId == c.User.Organisation_Id_fk)
                        .ToList()
                        .Select(c => c.ToEntity())
                        .ToList();
            }
        }

        public IList<UserCredentials> GetUsersCredentialsByUsername(string username)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                return
                    context.UserCredentials.Include(c => c.User)
                        .Where(c => username == c.User.Username)
                        .ToList()
                        .Select(c => c.ToEntity())
                        .ToList();
            }
        }

        public bool CreateUserCredentials(UserCredentials userCredentials, long id)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                return CreateUserCredentials(context, userCredentials, id);
            }
        }

        private bool CreateUserCredentials(ICrmModel context, UserCredentials userCredentials, long id)
        {

            UserCredentialsDto userCredentialsDto = userCredentials.CredentialType == CredentialType.Temporary
                ? GetUserCredentialsDto(context, userCredentials.UserId, isTemp: true)
                : GetUserCredentialsDto(context, userCredentials.UserId);

            if (userCredentialsDto == null)
            {
                var newUserCredentialsDto = new UserCredentialsDto
                {
                    PasswordHashed = userCredentials.PasswordHashed,
                    DateCreated = DateTime.UtcNow,
                    DefaultExpirationInMonths = userCredentials.DefaultExpirationInMonths,
                    Expiration = userCredentials.Expiration,
                    UserInfo_Id_fk = userCredentials.UserId,
                    FirstTimeLogin = userCredentials.FirstTimeLogin,
                    CredentialType = DtoMappingAttribute.GetValueFromEnum(userCredentials.CredentialType)
                };
                context.UserCredentials.Add(newUserCredentialsDto);
            }
            else
            {
                userCredentialsDto.PasswordHashed = userCredentials.PasswordHashed;
                userCredentialsDto.DefaultExpirationInMonths = userCredentials.DefaultExpirationInMonths;
                userCredentialsDto.Expiration = userCredentials.Expiration;
                userCredentialsDto.FirstTimeLogin = userCredentials.FirstTimeLogin;
            }

            return context.SaveChangesAndLog(id) > 0;
        }
    }    
}
