﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AutoMapper;
using Utils.Logging.Utils;

namespace AOM.Services.CrmService
{
    /// <summary>
    /// Used by the ArgusCrmConsumer to update the UserInfo, Module in UserModule tables
    /// </summary>
    public class UserModuleService : IUserModuleService
    {
        private readonly IDbContextFactory _dbfactory;

        public UserModuleService(IDbContextFactory dbfactory)
        {
            _dbfactory = dbfactory;
        }

        /// <summary>
        /// Use to diactivate users in userInfo
        /// </summary>
        /// <param name="modules"></param>
        /// <returns>saved changes</returns>
        public int UpdateUserInfo(IEnumerable<AomUserModuleViewEntity> modules)
        {
            var userIds = modules.Select(a => a.UserName);
            using (var context = _dbfactory.CreateCrmModel())
            {
                var existingUsers =
                    context.UserInfoes.Select(a => a).Where(a => !string.IsNullOrEmpty(a.ArgusCrmUsername));

                var usersToDeactivate =
                    existingUsers.Where(u => !userIds.Contains(u.ArgusCrmUsername)).Where(u => u.IsActive);
                usersToDeactivate.Each(
                    u =>
                    {
                        u.IsActive = false;
                        Log.Info(String.Format("Deactivating user {0}", u.Name));
                    });

                var usersToActivate =
                    existingUsers.Where(u => userIds.Contains(u.ArgusCrmUsername)).Where(u => !u.IsActive);
                usersToActivate.Each(
                    u =>
                    {
                        u.IsActive = true;
                        Log.Info(String.Format("Activating user {0}", u.Name));
                    });

                return context.SaveChangesAndLog(-1);
            }
        }

        /// <summary>
        /// Updates the UserModule table
        /// </summary>
        /// <param name="modules"></param>
        /// <returns></returns>
        public int UpdateUserModules(IEnumerable<AomUserModuleViewEntity> modules)
        {
            int result = 0;
            var modulesList = modules.ToList();

            //select just the usernames passed from Oracle view
            var usersFromOracle = modulesList.Select(a => a.UserName);

            using (var context = _dbfactory.CreateCrmModel())
            {
                // get all existing modules
                var existingUserModules = context.UserModules.Include(m => m.Module).Select(a => a).ToList();

                // 1. Remove exising UserModules if user is not in the Oracle view - check by Username
                // if the Oracle modules don't have a specific user, delete it from our Usermodule

                var userModulesToRemove = (from ex in existingUserModules
                    where usersFromOracle.Contains(ex.ArgusCrmUsername) == false
                    select ex).ToList();
                userModulesToRemove.ForEach(u => context.UserModules.Remove(u));

                result += context.SaveChangesAndLog(-1);

                var newUsrModsFromOracle = modulesList.Select(m =>
                    new UserModuleDto
                    {
                        ArgusCrmUsername = m.UserName,
                        Module = context.Modules.FirstOrDefault(i => i.Id == m.ModuleId),
                        ModuleId_fk = m.ModuleId,
                        Stream = m.Stream,
                        StreamId = m.StreamId,
                        UserOracleCrmId = m.UserOracleCrmId,
                        TypeName = m.TypeName
                    }).ToList();

                // 2. Remove user modules
                // get all UMs that are in existing but not in the newly passed ones from Oracle view
                var modulesToRemove = existingUserModules.Where(a => !ContainsModule(newUsrModsFromOracle, a)).ToList();

                modulesToRemove.ForEach(m => context.UserModules.Remove(m));
                result += context.SaveChangesAndLog(-1);


                // 3. Add new modules for users 
                // add any new modules per user from the view query
                // these are the new USerModules passed in, create Dto from them
                // we assume we have all the modules added already!

                // then check if any are missing in our Db and add them
                var missingModules = newUsrModsFromOracle.Where(m => !ContainsModule(existingUserModules, m)).ToList();

                // add the missing modules to UserModules and save them
                missingModules.ForEach(m => context.UserModules.Add(m));
                result += context.SaveChangesAndLog(-1);


                return result;
            }
        }

        /// <summary>
        /// Updates the crm.Module table
        /// Check if any modules passed from Oracle view and if yes, add them to our crm.Module table
        /// </summary>
        /// <param name="modules"></param>
        /// <returns></returns>
        public int UpdateModules(IEnumerable<AomUserModuleViewEntity> modules)
        {
            using (var context = _dbfactory.CreateCrmModel())
            {
                var existingModules = context.Modules.Select(a => a).ToList();
                //filter by unique Id as the ModuleId is no longer unique in Argus CRM
                var modulesFromArgus = (from m in modules
                    group m by new {id = m.ModuleId, descript = m.Description}
                    into g
                    select new ModuleDto {Id = g.Key.id, Description = g.Key.descript}).ToList();

                //update the description of existing modules if it has changed
                existingModules.ForEach(em =>
                {
                    var am = modulesFromArgus.FirstOrDefault(myDto => myDto.Id == em.Id);
                    if (am != null && em.Description != am.Description)
                    {
                        em.Description = am.Description;
                    }
                });

                //save changes to existing modules
                var result = context.SaveChangesAndLog(-1);

                modulesFromArgus.Where(a => !existingModules.Any(b => a.Id == b.Id)).ToList().ForEach(a =>
                {
                    context.Modules.Add(a);
                });

                result += context.SaveChangesAndLog(-1);
                return result;
            }
        }

        private static bool ContainsModule(IEnumerable<UserModuleDto> allModules, UserModuleDto moduleToFind)
        {
            return
                allModules.Where(m => m.Module != null)
                    .Any(
                        m =>
                            m.ArgusCrmUsername == moduleToFind.ArgusCrmUsername &&
                            m.Module.Id == moduleToFind.ModuleId_fk &&
                            m.Module.Description == moduleToFind.Module.Description);
        }
    }
}