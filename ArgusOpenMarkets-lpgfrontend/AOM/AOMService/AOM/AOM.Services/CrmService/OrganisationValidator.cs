﻿using System;
using AOM.App.Domain;
using AOM.App.Domain.Interfaces;

namespace AOM.Services.CrmService
{
    public static class OrganisationValidator
    {
        public static void ValidateOrganisation(IOrganisation organisation)
        {
            var res = ValidateImpl(organisation);
            if (!res.Item1)
            {
                throw new BusinessRuleException(res.Item2);
            }
        }

        public static bool IsOrganisationValid(IOrganisation organisation)
        {
            return ValidateImpl(organisation).Item1;
        }

        private static Tuple<bool, string> ValidateImpl(IOrganisation organisation)
        {
            if (organisation == null) throw new ArgumentNullException("organisation");

            if (string.IsNullOrWhiteSpace(organisation.Name))
            {
                return Tuple.Create(false, "Organisation Name must not be empty");
            }
            // TODO: Better email checking? Email regex?
            // https://msdn.microsoft.com/en-us/library/01escwtf%28v=vs.110%29.aspx
            if (string.IsNullOrWhiteSpace(organisation.Email))
            {
                return Tuple.Create(false, "Organisation must have an email associated with it");
            }
            if (string.IsNullOrWhiteSpace(organisation.ShortCode))
            {
                return Tuple.Create(false, "Organisation ShortCode must not be empty");
            }
            return Tuple.Create(true, "");
        }
    }
}