﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Transactions;
using Utils.Logging.Utils;

namespace AOM.Services.NewsService
{
    public class AomNewsRepository : IAomNewsRepository
    {
        protected readonly IDbContextFactory _dbContextFactory;

        private const long ProcessorUserId = -1;

        public AomNewsRepository(IDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public bool CreateAomNewsContentStreams(List<NewsContentStreamDto> newsContentStreams)
        {
            try
            {
                using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
                {
                    foreach (var cs in newsContentStreams)
                    {
                        aomModel.NewsContentStream.Add(cs);
                        aomModel.SaveChangesAndLog(ProcessorUserId);
                    }
                }

                return true;
            }
            catch (DbUpdateException dbEx)
            {
                Log.Error("AomNewsRepository.CreateAomNews - DbUpdateException", dbEx);
            }
            catch (Exception ex)
            {
                Log.Error("AomNewRepository.CreateAomNews", ex);
            }

            return false;
        }

        public long CreateAomNews(IAomNews aomNews)
        {
            NewsDto existingNewsItem = null;
            try
            {
                existingNewsItem = GetAomNews(aomNews.CmsId);
            }
            catch (Exception ex)
            {
                Log.Error("CreateAomNews - could not load news id", ex);
            }

            if (existingNewsItem == null)
            {
                try
                {
                    var news = aomNews as AomNews;
                    NewsDto newsDto = news.ToDto();

                    using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
                    {

                        using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                        {
                            aomModel.News.Add(newsDto);
                            aomModel.SaveChangesAndLog(ProcessorUserId);

                            trans.Complete();

                            return newsDto.NewsId;
                        }
                    }
                }
                catch (DbUpdateException dbEx)
                {
                    Log.Error("AomNewsRepository.CreateAomNews - DbUpdateException - Unique Contraint CmsId", dbEx);
                }
                catch (Exception ex)
                {
                    Log.Error("AomNewRepository.CreateAomNews", ex);
                }
            }
            else
            {
                return existingNewsItem.NewsId;
            }

            return -1;
        }

        public bool DeleteAomNews(string cmsId)
        {
            Log.InfoFormat("News Repository - Deleting News for Cms Id :{0}", cmsId);

            try
            {
                using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
                {
                    var newsToDelete = aomModel.News.FirstOrDefault(n => n.CmsId == cmsId);
                    if (newsToDelete != null)
                    {
                        var newsContentStreams = aomModel.NewsContentStream.Where(ncs => ncs.NewsId == newsToDelete.NewsId).ToList();
                        if (newsContentStreams != null)
                        {
                            foreach (var newsCs in newsContentStreams)
                            {
                                aomModel.NewsContentStream.Remove(newsCs);
                            }
                            aomModel.SaveChangesAndLog(ProcessorUserId);
                        }
                        try
                        {
                            aomModel.News.Remove(newsToDelete);
                            aomModel.SaveChangesAndLog(ProcessorUserId);
                        }
                        catch (Exception ex)
                        {
                            Log.Error("AomNewsRepository.DeleteAomNews Exception-", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("DeleteAomNews", ex);
                return false;
            }

            return true;
        }

        public NewsDto GetAomNews(string cmsId)
        {
            NewsDto news = null;

            try
            {
                using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
                {
                    news = aomModel.News.FirstOrDefault(n => n.CmsId == cmsId);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("GetAomNews", ex);
            }

            return news;
        }

        public List<long> GetContentStreamsForArticle(long newsId)
        {
            List<long> newsContentStreams = new List<long>();
            using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
            {
                newsContentStreams =
                    aomModel.NewsContentStream.Where(ncs => ncs.NewsId == newsId)
                        .Select(x => x.ContentStreamId)
                        .ToList();
            }

            return newsContentStreams;
        }

        public bool UpdateAomNews(IAomNews aomNews)
        {
            try
            {
                using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
                {
                    var news = aomModel.News.Where(n => n.CmsId == aomNews.CmsId).FirstOrDefault();

                    if (news != null)
                    {
                        news.Story = aomNews.Story;
                        news.IsFree = aomNews.IsFree;
                        news.PublicationDate = aomNews.PublicationDate;
                        news.ContentStreams = new List<NewsContentStreamDto>();

                        if (aomNews.ContentStreams != null)
                        {
                            foreach (var cs in aomNews.ContentStreams)
                            {
                                news.ContentStreams.Add(new NewsContentStreamDto {ContentStreamId = cs});
                            }
                        }
                        try
                        {

                            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                            {
                                aomModel.SaveChangesAndLog(ProcessorUserId);
                                trans.Complete();
                            }

                        }
                        catch (Exception ex)
                        {
                            Log.Error("AomNewsRepository.UpdateAomNews", ex);
                        }

                        return true;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error("UpdateAomNews", ex);
            }

            return false;
        }

        public void PurgeNews(DateTime startDate)
        {
            using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
            {
                var newsItemsToPurge = aomModel.News.Where(n => n.PublicationDate < startDate).ToList();
                foreach (var newsItem in newsItemsToPurge)
                {
                    DeleteAomNews(newsItem.CmsId.ToString(CultureInfo.InvariantCulture));
                }
            }
        }
    }
}