﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AOM.Services.ProductService;
using Argus.Transport.Messages.Dtos;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Utils.Logging.Utils;

namespace AOM.Services.NewsService
{
    public class AomNewsService : IAomNewsService
    {
        private readonly IUserService _userService;

        private readonly IAomNewsRepository _aomNewsRepository;

        private readonly IDbContextFactory _dbContextFactory;

        private readonly IProductService _productService;

        private const string ArgusDirectArticleUrl = "https://direct.argusmedia.com/newsandanalysis/article/{0}";

        private const string Ellipsis = " ...";

        private const int AomStoryIntroDefaultLength = 300;

        public AomNewsService(
            IUserService userService,
            IAomNewsRepository aomNewsRepository,
            IDbContextFactory dbContextFactory,
            IProductService productService)
        {
            _userService = userService;
            _aomNewsRepository = aomNewsRepository;
            _dbContextFactory = dbContextFactory;
            _productService = productService;

        }

        public long CreateNews(IAomNews aomNews)
        {
            return _aomNewsRepository.CreateAomNews(aomNews);
        }

        public bool UpdateNews(IAomNews aomNews)
        {
            return _aomNewsRepository.UpdateAomNews(aomNews);
        }

        public bool DeleteNews(string cmsId)
        {
            return _aomNewsRepository.DeleteAomNews(cmsId);
        }

        public string GetNewsStory(string cmsId)
        {
            // Get the news story body without entitlement check(s) - the server is the only place we subscribe users 
            // to news - so  we don't need to double check the security of the user to see if they are entitled to see the news.
            var news = _aomNewsRepository.GetAomNews(cmsId);
            return TransformStoryBody(cmsId, news.Story, news.PublicationDate);
        }

        public string GetNewsStory(string cmsId, long userId)
        {
            var news = _aomNewsRepository.GetAomNews(cmsId);

            if (UserEntitledToStory(news.NewsId, userId, news.IsFree))
            {
                return TransformStoryBody(cmsId, news.Story, news.PublicationDate);
            }

            return string.Empty;
        }

        private string TransformStoryBody(string cmsId, string story, DateTime publicationDate)
        {
            if (!string.IsNullOrEmpty(story))
            {
                if (story.IndexOf("<html", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    Log.Info("In AomNewsService.GetNewsStory,  returning story without html tags");
                    story = ProcessHtml(cmsId, publicationDate, story);
                }
            }

            story = SetStoryTargetToBlank(story);

            Log.Info("In AomNewsService.GetNewsStory(),  returning default story: " + story);
            return story;
        }

        public IAomNews TransformNews(PersistedArticle argusNews)
        {
            try
            {
                if (argusNews != null)
                {
                    AomNews aomNews = new AomNews();

                    long[] contentStreams;
                    ulong commodityId;

                    contentStreams = GetArticleContentStreams(argusNews.ContentStreams);
                    commodityId = GetCommodityIdFromContentStreamId(contentStreams);

                    aomNews.CmsId = argusNews.CmsId.ToString(CultureInfo.InvariantCulture);
                    aomNews.ContentStreams = contentStreams;
                    aomNews.Headline = argusNews.Headline;
                    aomNews.IsFree = argusNews.IsFree;
                    aomNews.ItemUrl = String.Format(ArgusDirectArticleUrl, argusNews.DomainId);

                    if (argusNews.NewsType != null)
                    {
                        aomNews.NewsType = argusNews.NewsType.Name;
                    }
                    else
                    {
                        aomNews.NewsType = string.Empty;
                    }

                    aomNews.PublicationDate = argusNews.PublicationDate;
                    aomNews.CommodityId = commodityId;
                    aomNews.Story = argusNews.Story;

                    return aomNews;
                }
            }
            catch (Exception ex)
            {
                Log.Error("AOMNewsService - Error transforming PersistedArticle into AomNews", ex);
            }

            return null;
        }

        public ulong GetCommodityIdFromContentStreamId(long[] contentStreams)
        {
            try
            {
                if (contentStreams != null)
                {
                    var commodityContentStreams = _productService.GetCommoditiesForContentStreams(contentStreams);

                    if (commodityContentStreams != null &&
                        commodityContentStreams.Count > 0 &&
                        commodityContentStreams[0] != null)
                    {
                        return (ulong)commodityContentStreams[0].CommodityId;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetCommodityIdFromContentStreamId", ex);
            }

            return 0;
        }

        public decimal[] GetAomContentStreamIds()
        {
            return GetContentStreams().Select(cs => (decimal)cs.ContentStreamId).ToArray();
        }

        public string[] GetAomContentStreamDescriptions()
        {
            return GetContentStreams().Select(cs => cs.Description).ToArray();
        }

        private IEnumerable<ContentStreamDto> GetContentStreams()
        {
            using (ICrmModel context = _dbContextFactory.CreateCrmModel())
            {
                List<ContentStreamDto> contentStreams = context.ContentStreams.ToList();

                return contentStreams;
            }
        }

        public long[] GetArticleContentStreams(PersistedMetaTypeDatum[] contentStreams)
        {

            List<long> articleContentStreams = new List<long>();

            if (contentStreams != null)
            {
                var aomContentStreams = GetAomContentStreamIds();

                if (aomContentStreams != null)
                {
                    foreach (var cs in contentStreams)
                    {
                        if (aomContentStreams.Contains(cs.DomainId))
                        {
                            articleContentStreams.Add(Convert.ToInt64(cs.DomainId));
                        }
                    }
                }
            }

            return articleContentStreams.ToArray();
        }

        public bool IsAomNewsArticle(PersistedArticle news)
        {
            if (news != null)
            {
                if (news.ContentStreams != null)
                {
                    var aomNews = IsAomNewsArticle(news.ContentStreams);
                    if (aomNews) return true;
                }
            }

            return false;
        }

        public string TruncateStory(string story)
        {
            if (string.IsNullOrEmpty(story)) // empty
            {
                return string.Empty;
            }

            story = RemoveHtmlFromStory(story);

            if (story.Length < AomStoryIntroDefaultLength) // shorter than cutoff
            {
                return story;
            }
            // longer than cutoff, so truncate and add ellipsis.
            story = story.Substring(0, AomStoryIntroDefaultLength);

            var pos = story.LastIndexOf(' ');

            story = story.Substring(0, pos > -1 ? pos : AomStoryIntroDefaultLength);

            return story + Ellipsis;
        }

        private string RemoveHtmlFromStory(string story)
        {
            if (story.IndexOf("<html", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(story);
                story = htmlDoc.DocumentNode.ChildNodes["html"].ChildNodes["body"].InnerText;
            }

            return story;
        }

        private bool IsAomNewsArticle(PersistedMetaTypeDatum[] persistedContentStreams)
        {
            var aomNewsContentStreams = GetAomContentStreamIds();
            foreach (var cs in persistedContentStreams)
            {
                foreach (var aomcs in aomNewsContentStreams)
                {
                    if (cs.DomainId == aomcs)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        internal string SetStoryTargetToBlank(string story)
        {
            return story.Replace("_parent", "_blank");
        }

        public bool UserEntitledToStory(long newsId, long userId, bool freeYn)
        {
            if (freeYn)
            {
                // News Story is free - user is entitled.  
                return true;
            }

            var articleContentStreams = _aomNewsRepository.GetContentStreamsForArticle(newsId);
            if (articleContentStreams == null)
            {
                // No content streams nothing to check 
                return false;
            }

            if (articleContentStreams.Count == 0)
            {
                return false;
            }

            return _userService.CheckUserHasContentStreams(userId, articleContentStreams);
        }

        private string ProcessHtml(string cmsId, DateTime newsDate, string story)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(story);
            var imgs = htmlDoc.DocumentNode.SelectNodes("//img");
            var links = htmlDoc.DocumentNode.SelectNodes("//a");
            if (imgs != null)
            {
                foreach (var img in imgs)
                {
                    if (!img.Attributes["src"].Value.StartsWith("http://"))
                    {
                        img.Attributes["src"].Value = "http://www.argusmedia.com/NewsFiles/"
                                                      + string.Format("{0:yyyy-MM-dd}", newsDate) + "/" + cmsId + "/"
                                                      + img.Attributes["src"].Value;
                    }
                }
            }

            if (links != null && links.Any())
            {
                foreach (var link in links.Where(l => l.Attributes["href"] != null))
                {
                    var linkVal = link.Attributes["href"].Value;
                    if (linkVal.StartsWith("http://www.argusmedia.com/pages/NewsBody.aspx")
                        || linkVal.StartsWith("http://argusmedia.com/pages/NewsBody.aspx"))
                    {
                        var uri = new Uri(link.Attributes["href"].Value);
                        link.Attributes["href"].Value = "/NewsAndAnalysis/Article/"
                                                        + HttpUtility.ParseQueryString(uri.Query).Get("id");
                        continue;
                    }

                    if (linkVal.StartsWith("http://") || linkVal.StartsWith("mailto:"))
                    {
                        continue;
                    }

                    if (linkVal.StartsWith("NewsBody"))
                    {
                        var uri = new Uri("http://argusmedia.com/" + link.Attributes["href"].Value);
                        link.Attributes["href"].Value = "/NewsAndAnalysis/Article/"
                                                        + HttpUtility.ParseQueryString(uri.Query).Get("id");
                    }
                    else
                    {
                        link.Attributes["href"].Value = "http://www.argusmedia.com/NewsFiles/"
                                                        + string.Format("{0:yyyy-MM-dd}", newsDate) + "/" + cmsId + "/"
                                                        + link.Attributes["href"].Value;
                    }
                }
            }

            return htmlDoc.DocumentNode.ChildNodes["html"].ChildNodes["body"].InnerHtml;
        }

        public void PurgeNews(DateTime startDate)
        {
            Log.InfoFormat("AomNewsService.PurgeNews Called with startDate:{0}", startDate);
            // We have received a purge news request - which is going to remove old news stories from diffusion.
            // At this point we can purge the news from the AOM Repository.
            _aomNewsRepository.PurgeNews(startDate);
        }
    }
}