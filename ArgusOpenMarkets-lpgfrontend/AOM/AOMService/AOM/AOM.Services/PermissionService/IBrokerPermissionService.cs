﻿using System.Collections.Generic;

using AOM.Transport.Events.BrokerPermissions;

namespace AOM.Services.PermissionService 
{
    public interface IBrokerPermissionService
    {
        AomBrokerPermissionResponse UpdatePermissions(AomBrokerPermissionAuthenticationResponse authenticatedPermission, long organisationId);
    }
}
