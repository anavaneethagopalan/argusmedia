﻿using System.Collections.Generic;

using AOM.Transport.Events.CounterpartyPermissions;

namespace AOM.Services.PermissionService
{
    public interface ICounterpartyPermissionService
    {
        AomCounterpartyPermissionResponse UpdatePermissions(AomCounterpartyPermissionAuthenticationResponse authenticatedPermission, long organisationId);
    }
}
