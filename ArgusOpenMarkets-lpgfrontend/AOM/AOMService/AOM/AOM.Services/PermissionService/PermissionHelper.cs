﻿using System;
using System.Collections.Generic;
using System.Threading;

using AOM.App.Domain.Entities;

namespace AOM.Services.PermissionService
{
    public interface IPermissionTaskScheduler
    {
        void QueueBackgroundWorkItem(CancellationToken token, List<MatrixPermission> returnedPermissions);
    }

    internal static class PermissionsHelper
    {
        public static IList<long> BuildListOfOrganisationsAffected(List<MatrixPermission> permissionsChanged)
        {
            List<long> organisationIds = new List<long>();

            if (permissionsChanged == null)
            {
                return organisationIds;
            }

            foreach (var perm in permissionsChanged)
            {
                if (!organisationIds.Contains(perm.OurOrganisation))
                {
                    organisationIds.Add(perm.OurOrganisation);
                }

                if (!organisationIds.Contains(perm.TheirOrganisation))
                {
                    organisationIds.Add(perm.TheirOrganisation);
                }
            }
            return organisationIds;
        }

        public static IList<long> BuildListOfProductsAffected(List<MatrixPermission> permissionsChanged)
        {
            List<long> productIds = new List<long>();

            if (permissionsChanged == null)
            {
                return productIds;
            }

            foreach (var perm in permissionsChanged)
            {
                if (!productIds.Contains(perm.ProductId))
                {
                    productIds.Add(perm.ProductId);
                }
            }
            return productIds;
        }


    }
}