﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.BrokerPermissions;
using AOM.Transport.Events.Emails;
using AOM.Transport.Events.Organisations;

using Utils.Logging.Utils;
using Argus.Transport.Infrastructure;
using Argus.Transport.Messages.Events;
using JetBrains.Annotations;

namespace AOM.Services.PermissionService 
{
    public class BrokerPermissionService : IBrokerPermissionService, IPermissionTaskScheduler
    {
        private readonly IBus _aomBus;
        private readonly IDbContextFactory _dbFactory;
        private readonly IOrganisationService _organisationService;
        private readonly IEmailService _emailService;
        private readonly IErrorService _errorService;

        public BrokerPermissionService(IBus aomBus, IDbContextFactory dbFactory, IOrganisationService organisationService, IEmailService emailService, IErrorService errorService)
        {
            _aomBus = aomBus;
            _dbFactory = dbFactory;
            _organisationService = organisationService;
            _emailService = emailService;
            _errorService = errorService;
        }

        [CanBeNull]
        public AomBrokerPermissionResponse UpdatePermissions(AomBrokerPermissionAuthenticationResponse authenticatedPermission, long organisationId)
        {
            AomBrokerPermissionResponse permissionResponse = new AomBrokerPermissionResponse
            {
                Message = new Message()
                {
                    MessageAction = authenticatedPermission.Message.MessageAction,
                    ClientSessionInfo = authenticatedPermission.Message.ClientSessionInfo
                }
            };

            try
            {
                Log.Info("Calling Edit perms");
                List<MatrixPermission> returnedPermissions = EditBrokerPermissions(authenticatedPermission, organisationId);
                permissionResponse.BrokerPermissions = returnedPermissions;
                permissionResponse.Message = authenticatedPermission.Message;
                permissionResponse.UpdateSuccessful = !(returnedPermissions.Count(p => !p.UpdateSuccessful) > 0);

                Log.Info("Call to send broker permission change emails in background thread");
                CancellationTokenSource cts = new CancellationTokenSource(TimeSpan.FromSeconds(30));
                QueueBackgroundWorkItem(cts.Token, returnedPermissions);

                return permissionResponse;
            }
            catch (InvalidOperationException ioEx)
            {
                //operation error from test methods calling QueueBackgroundWorkItem - TODO - Write better tests!
                Log.Error("BrokerPermissionService.UpdatePermissions InvalidOperationException: ", ioEx);
                return permissionResponse;
            }
            catch (Exception ex)
            {
                var errorMessage = _errorService.LogUserError(new UserError
                {
                    Error = ex,
                    AdditionalInformation = String.Format("{0} error", authenticatedPermission.GetType()),
                    ErrorText = ex.Message,
                    Source = "BrokerPermissionService"
                });

                permissionResponse.Message.MessageBody = errorMessage;
                permissionResponse.UpdateSuccessful = false;
                return permissionResponse;
            }
        }

        public void QueueBackgroundWorkItem(CancellationToken token, List<MatrixPermission> returnedPermissions)
        {
            HostingEnvironment.QueueBackgroundWorkItem(qbwi =>
            {
                IEnumerable<long> emailIds = new List<long>();
                try
                {
                    token.ThrowIfCancellationRequested();
                    emailIds = _emailService.CreateEmail(_dbFactory.CreateAomModel(),
                        new TradingMatrixUpdateNotification()
                        {
                            MatrixPermissions = returnedPermissions.Where(p => p.UpdateSuccessful).ToList(),
                            BrokerOrCounterpartyMatrixUpdate = SystemEventType.BrokerMatrixChanges
                        });

                    foreach (var id in emailIds)
                    {
                        _aomBus.PublishAsync(new AomSendEmailRequest() { MessageAction = MessageAction.Send, EmailId = id });
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error sending emails for Broker UpdatePermissions change, email ids: {0} - ", string.Join(",", emailIds)), ex);
                }
            });
        }

        private List<MatrixPermission> EditBrokerPermissions(AomBrokerPermissionAuthenticationResponse response, long organisationId)
        {
            response.BrokerPermissions = _organisationService.EditBrokerPermission(response.BrokerPermissions, response.Message.ClientSessionInfo.UserId);
            SendRefreshForUpdate(response.BrokerPermissions, organisationId);   // Push permission updates to summary topics in diffusion
            return response.BrokerPermissions;
        }

        private void SendRefreshForUpdate(List<MatrixPermission> permissionsChanged, long organisationId)
        {
            var allBrokerPermissions = _organisationService.GetBrokerPermissions(organisationId);
            var orgsAffected = PermissionsHelper.BuildListOfOrganisationsAffected(permissionsChanged);
            var productsAffected = PermissionsHelper.BuildListOfProductsAffected(permissionsChanged);
            var relevantPermissions = allBrokerPermissions.Where(p => productsAffected.Contains(p.ProductId) &&
                orgsAffected.Contains(p.BuySide.OurOrganisation) && orgsAffected.Contains(p.BuySide.TheirOrganisation)).ToList();

            var refreshMessage = new GetBrokerPermissionsRefreshResponse
            {
                BrokerPermissions = relevantPermissions,
                OrganisationsAffected = orgsAffected
            };
            _aomBus.Publish(refreshMessage);
        }
    }
}