﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Hosting;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.CounterpartyPermissions;
using AOM.Transport.Events.Emails;
using AOM.Transport.Events.Organisations;

using Utils.Logging.Utils;
using Argus.Transport.Infrastructure;

namespace AOM.Services.PermissionService 
{
    public class CounterpartyPermissionService : ICounterpartyPermissionService, IPermissionTaskScheduler
    {
        private readonly IBus _aomBus;
        private readonly IDbContextFactory _dbFactory;
        private readonly IOrganisationService _organisationService;
        private readonly IEmailService _emailService;
        private readonly IErrorService _errorService;

        public CounterpartyPermissionService(IBus aomBus, IDbContextFactory dbFactory, IOrganisationService organisationService, IEmailService emailService, IErrorService errorService)
        {
            _aomBus = aomBus;
            _dbFactory = dbFactory;
            _organisationService = organisationService;
            _emailService = emailService;
            _errorService = errorService;
        }

        public AomCounterpartyPermissionResponse UpdatePermissions(AomCounterpartyPermissionAuthenticationResponse authenticatedPermission, long organisationId)
        {
            AomCounterpartyPermissionResponse permissionResponse = new AomCounterpartyPermissionResponse
            {
                Message = new Message()
                {
                    MessageAction = authenticatedPermission.Message.MessageAction,
                    ClientSessionInfo = authenticatedPermission.Message.ClientSessionInfo
                }
            };

            try
            {
                List<MatrixPermission> returnedPermissions = EditCounterpartyPermissions(authenticatedPermission, organisationId);
                permissionResponse.CounterpartyPermissions = returnedPermissions;
                permissionResponse.Message = authenticatedPermission.Message;
                permissionResponse.UpdateSuccessful = !(returnedPermissions.Count(p => !p.UpdateSuccessful) > 0);

                Log.Info("Call to send counterparty permission change emails in background thread");
                CancellationTokenSource cts = new CancellationTokenSource(TimeSpan.FromSeconds(30));
                QueueBackgroundWorkItem(cts.Token, returnedPermissions);
                
                return permissionResponse;
            }
            catch (InvalidOperationException ioEx)
            {
                //operation error from test methods calling QueueBackgroundWorkItem - TODO - Write better tests!
                Log.Error("CounterpartyPermissionService.UpdatePermissions InvalidOperationException: ", ioEx);
                return permissionResponse;
            }
            catch (Exception exception)
            {
                var errorMessage = _errorService.LogUserError(new UserError
                {
                    Error = exception,
                    AdditionalInformation = String.Format("{0} error", authenticatedPermission.GetType()),
                    ErrorText = string.Empty,
                    Source = "CounterpartyPermissionConsumerBase"
                });

                permissionResponse.Message.MessageBody = errorMessage;
                permissionResponse.UpdateSuccessful = false;
                return permissionResponse;
            }
        }

        public void QueueBackgroundWorkItem(CancellationToken token, List<MatrixPermission> returnedPermissions)
        {
            HostingEnvironment.QueueBackgroundWorkItem(qbwi =>
            {
                IEnumerable<long> emailIds = new List<long>();
                try
                {
                    token.ThrowIfCancellationRequested();
                    emailIds = _emailService.CreateEmail(_dbFactory.CreateAomModel(),
                           new TradingMatrixUpdateNotification()
                           {
                               MatrixPermissions = returnedPermissions.Where(p => p.UpdateSuccessful).ToList(),
                               BrokerOrCounterpartyMatrixUpdate = SystemEventType.CounterPartyPermissionChanges
                           });

                    foreach (var id in emailIds)
                    {
                        _aomBus.PublishAsync(new AomSendEmailRequest() { MessageAction = MessageAction.Send, EmailId = id });
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error sending emails for Counterparty UpdatePermissions change, email ids: {0} - ", string.Join(",", emailIds)), ex);
                }
            });
        }

        private List<MatrixPermission> EditCounterpartyPermissions(AomCounterpartyPermissionAuthenticationResponse response, long organisationId)
        {
            response.CounterpartyPermissions = _organisationService.EditCounterpartyPermission(response.CounterpartyPermissions, response.Message.ClientSessionInfo.UserId);
            SendRefreshForUpdate(response.CounterpartyPermissions, organisationId);   // Push permission updates to summary topics in diffusion
            return response.CounterpartyPermissions;
        }

        private void SendRefreshForUpdate(List<MatrixPermission> permissionsChanged, long organisationId)
        {
            var allCounterpartyPermissions = _organisationService.GetCounterpartyPermissions(organisationId);
            var orgsAffected = PermissionsHelper.BuildListOfOrganisationsAffected(permissionsChanged);
            var productsAffected = PermissionsHelper.BuildListOfProductsAffected(permissionsChanged);
            var relevantPermissions = allCounterpartyPermissions.Where(p => productsAffected.Contains(p.ProductId) &&
                orgsAffected.Contains(p.BuySide.OurOrganisation) && orgsAffected.Contains(p.BuySide.TheirOrganisation)).ToList();

            var refreshMessage = new GetCounterpartyPermissionsRefreshResponse
            {
                CounterpartyPermissions = relevantPermissions,
                OrganisationsAffected = orgsAffected
            };

            _aomBus.Publish(refreshMessage);
        }

    }
}