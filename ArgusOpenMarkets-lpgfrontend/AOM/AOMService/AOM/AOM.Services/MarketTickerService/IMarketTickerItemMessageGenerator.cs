﻿using AOM.App.Domain.Entities;
using AOM.Transport.Events;

namespace AOM.Services.MarketTickerService
{
    public interface IMarketTickerItemMessageGenerator
    {
        string GenerateMarketInfoMessage(MarketInfo info);

        string GenerateMarketItemMessage(Order order, MessageAction action);

        string GenerateMarketItemMessage(Deal deal, MessageAction action);

        string GenerateMarketItemMessage(Product product, MessageAction action);

        string GenerateMarketItemMessage(ExternalDeal externalDeal, MessageAction action);
    }
}