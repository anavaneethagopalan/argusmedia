﻿using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using System;
using Utils.Javascript.Extension;

namespace AOM.Services.MarketTickerService
{
    public class MarketTickerItemMessageGenerator : IMarketTickerItemMessageGenerator
    {
        private readonly IProductService _productService;

        public MarketTickerItemMessageGenerator(IProductService productService)
        {
            _productService = productService;
        }

        public string GenerateMarketInfoMessage(MarketInfo info)
        {
            if (info != null)
            {
                return GenerateInfoStatusText(info.MarketInfoStatus) + " " + info.Info;
            }
            return string.Empty;
        }

        public string GenerateMarketItemMessage(Order order, MessageAction action)
        {
            if (order == null) return string.Empty;

            string bidAsk = DtoMappingAttribute.GetValueFromEnum(order.OrderType);
            var bidAskString = bidAsk == "B" ? "bids" : "asks";
            decimal? price = order.Price;
            decimal? quantity = order.Quantity;

            DateTime? deliveryStartDate = order.DeliveryStartDate;
            DateTime? deliveryEndDate = order.DeliveryEndDate;

            string units = string.Empty;
            string productName = string.Empty;
            string priceUnits = string.Empty;
            string currency = string.Empty;
            string tenor = string.Empty;

            var productTenorDto = _productService.GetProductTenor(order.ProductTenorId);
            if (productTenorDto != null)
            {
                var product = _productService.GetProduct(productTenorDto.ProductId);
                
                if (product != null)
                {
                    //priceUnits = product.PriceUnitDisplayName;
                    priceUnits = order.OrderPriceLines.First().PriceLine.PriceLinesBases.First().PriceBasis.PriceUnitCode; //TO CHANGE - WHICH PRICELINE TO USE
                    units = product.VolumeUnitDisplayName;
                    //currency = product.CurrencyDisplayName;
                    currency = order.OrderPriceLines.First().PriceLine.PriceLinesBases.First().PriceBasis.CurrencyCode; //TO CHANGE - WHICH PRICELINE TO USE
                    productName = product.Name;
                }
            }

            var msg = string.Format("{0} {1} {2} {3:0,0}{4} {5} {6} {7} - {8} at {9}{10:F}/{11}",
                GenerateTickActionText(action),
                GenerateOrderOrganisationsString(order),
                bidAskString,
                quantity,
                units,
                productName,
                tenor,
                deliveryStartDate.ToArgusDateTimeFormat(),
                deliveryEndDate.ToArgusDateTimeFormat(),
                currency,
                price,
                priceUnits);

            msg = AppendMetaDataDetailToMessage(order.MetaData, msg);

            return msg;
        }

        private static string AppendMetaDataDetailToMessage(MetaData[] metaData, string msg)
        {
            if (metaData != null)
            {
                msg = metaData.Aggregate(msg, (current, item) => current + (", " + item.DisplayValue));
            }
            return msg;
        }

        private static string GenerateOrderOrganisationsString(Order order)
        {
            // External orders can be added with only a broker, in which case the principal org ID is 0
            if (order.ExecutionMode == ExecutionMode.External &&
                order.PrincipalOrganisationId == 0)
            {
                return order.BrokerOrganisationName;
            }
            return order.BrokerOrganisationId == null ? order.PrincipalOrganisationName : string.Format("{0}/{1}", order.PrincipalOrganisationName, order.BrokerOrganisationName);
        }

        public string GenerateMarketItemMessage(Deal deal, MessageAction action)
        {
            if (deal != null)
            {
                var initialOrder = deal.Initial;
                var matchingOrder = deal.Matching;
                var matchingOrderOrganisation = matchingOrder != null ? matchingOrder.PrincipalOrganisationName : string.Empty;

                string initialBrokerOrganisationName = initialOrder.BrokerOrganisationId != null ? String.Format("/{0}", initialOrder.BrokerOrganisationName) : string.Empty;
                string matchingBrokerOrganisationName = matchingOrder.BrokerOrganisationId != null ? String.Format("/{0}", matchingOrder.BrokerOrganisationName) : string.Empty;

                string organisationName = initialOrder.PrincipalOrganisationName;
                long? productId = initialOrder.ProductId;

                decimal? price = initialOrder.Price;
                decimal? quantity = initialOrder.Quantity;

                DateTime? deliveryStartDate = initialOrder.DeliveryStartDate;
                DateTime? deliveryEndDate = initialOrder.DeliveryEndDate;

                string units = string.Empty;
                string productName = initialOrder.ProductName;

                string priceUnits = string.Empty;
                string currency = string.Empty;
                string tenor = string.Empty;

                if (productId.HasValue)
                {
                    var productTenorDto = _productService.GetProductTenor(initialOrder.ProductTenor.Id);
                    if (productTenorDto != null)
                    {
                        //tenor = productTenorDto.DisplayName;
                        var product = _productService.GetProduct(productTenorDto.ProductId);

                        if (product != null)
                        {
                            //priceUnits = product.PriceUnitDisplayName;
                            priceUnits = initialOrder.OrderPriceLines.First().PriceLine.PriceLinesBases.First().PriceBasis.PriceUnitCode; //TO CHANGE
                            units = product.VolumeUnitDisplayName;
                            //currency = product.CurrencyDisplayName;
                            currency = initialOrder.OrderPriceLines.First().PriceLine.PriceLinesBases.First().PriceBasis.CurrencyCode; ///TO CHANGE
                            productName = product.Name;
                        }
                    }
                }

                switch (deal.DealStatus)
                {
                    case DealStatus.Executed:
                        //DEAL: {Initiator} sells {Quantity} {Units} {Product}{MetaData} to {Aggressor} {Tenor/Delivery} at {Price} {Currency}/{PriceUnits}
                        return string.Format("{11} {0}{14} {13} {1:0,0}{2} {3}{5}{16} {15} {4}{12} for {6} - {7} at {8}{9:F}/{10}",
                            matchingOrderOrganisation,
                            quantity,
                            units,
                            productName,
                            organisationName,
                            tenor,
                            deliveryStartDate.ToArgusDateTimeFormat(),
                            deliveryEndDate.ToArgusDateTimeFormat(),
                            currency,
                            price,
                            priceUnits,
                            GenerateDealStatusText(deal.DealStatus),
                            initialBrokerOrganisationName,
                            GenerateAggressorBuySellText(matchingOrder.OrderType),
                            matchingBrokerOrganisationName,
                            GetPreposition(matchingOrder.OrderType),
                            AppendMetaDataDetailToMessage(initialOrder.MetaData, ""));

                    case DealStatus.None:
                        return string.Format("DEAL MESSAGE NOT CONFIGURED FOR THIS STATE");

                    case DealStatus.Pending:
                        //PENDING: {Aggressor Principal} Buys {Quantity}{Units} {Product}{MetaData} from {Initiator Principal}/{Initiator Broker} {Tenor/Delivery} at {Currency}{Price}/{PriceUnits}
                        return string.Format("{0} {1}{14} {13} {2:0,0}{3} {4}{7}{16} {15} {5}{6} for {8} - {9} at {10}{11:F}/{12}",
                                GenerateDealStatusText(deal.DealStatus),
                                matchingOrderOrganisation,
                                quantity,
                                units,
                                productName,
                                organisationName,
                                initialBrokerOrganisationName,
                                tenor,
                                deliveryStartDate.ToArgusDateTimeFormat(),
                                deliveryEndDate.ToArgusDateTimeFormat(),
                                currency,
                                price,
                                priceUnits,
                                GenerateAggressorBuySellText(matchingOrder.OrderType),
                                matchingBrokerOrganisationName,
                                GetPreposition(matchingOrder.OrderType),
                                AppendMetaDataDetailToMessage(initialOrder.MetaData, ""));

                    case DealStatus.Void:
                        //VOIDED: {VoidReason}: {Aggressor} buys {Quantity}{Units} {Product}{MetaData} from {Initiator}/{Broker} {Tenor/Delivery} at {Currency}{Price}/{PriceUnits}
                        return
                            string.Format("{0} {16}: {1}{14} {13} {2:0,0}{3} {4}{7}{17} {15} {5}{6} for {8} - {9} at {10}{11:F}/{12}",
                                GenerateDealStatusText(deal.DealStatus),
                                matchingOrderOrganisation,
                                quantity,
                                units,
                                productName,
                                organisationName,
                                initialBrokerOrganisationName,
                                tenor,
                                deliveryStartDate.ToArgusDateTimeFormat(),
                                deliveryEndDate.ToArgusDateTimeFormat(),
                                currency,
                                price,
                                priceUnits,
                                GenerateAggressorBuySellText(matchingOrder.OrderType),
                                matchingBrokerOrganisationName,
                                GetPreposition(matchingOrder.OrderType),
                                DealPresentation.ToPresentationVoidReason(deal.GetDealVoidReason()),
                                AppendMetaDataDetailToMessage(initialOrder.MetaData, ""));
                }
            }

            return string.Empty;
        }

        public string GenerateMarketItemMessage(Product product, MessageAction action)
        {
            return string.Format("The {0} market is now {1}.", product.Name, GenerateTickActionText(action));
        }

        public string GenerateMarketItemMessage(ExternalDeal externalDeal, MessageAction action)
        {
            if (externalDeal != null)
            {
                switch (externalDeal.DealStatus)
                {
                    case DealStatus.None:
                        return string.Format("DEAL MESSAGE NOT CONFIGURED FOR THIS STATE");
                    case DealStatus.Pending:
                    case DealStatus.Void:
                    case DealStatus.Executed:
                        return string.Format("{0} {1}", GenerateExternalDealStatusText(externalDeal.DealStatus), externalDeal.FreeFormDealMessage);
                        //AppendMetaDataDetailToMessage(externalDeal.MetaData, string.Empty));
                }
            }
            return string.Empty;
        }

        private static string GenerateTickActionText(MessageAction action)
        {
            switch (action)
            {
                case MessageAction.Create:
                    return "NEW: ";
                case MessageAction.Execute:
                    return "EXECUTED: ";
                case MessageAction.Hold:
                    return "WITHDRAWN: ";
                case MessageAction.Kill:
                    return "WITHDRAWN: ";
                case MessageAction.Reinstate:
                    return "NEW: ";
                case MessageAction.Request:
                    return "REQUESTED: ";
                case MessageAction.Update:
                    return "UPDATED: ";
                case MessageAction.Void:
                    return "VOIDED: ";
                case MessageAction.Open:
                    return "open";
                case MessageAction.Close:
                    return "closed";
                default:
                    return string.Empty;
            }
        }

        private static string GenerateDealStatusText(DealStatus status)
        {
            switch (status)
            {
                case DealStatus.None:
                    return "";
                case DealStatus.Executed:
                    return "NEW: ";
                case DealStatus.Pending:
                    return "PENDING: ";
                case DealStatus.Void:
                    return "VOIDED: ";
                default:
                    return string.Empty;
            }
        }

        private static string GenerateExternalDealStatusText(DealStatus status)
        {
            // Do not use NEW for external deals.  AR-918.
            if (DealStatus.Executed == status) return "";

            return GenerateDealStatusText(status);
        }

        private static string GenerateInfoStatusText(MarketInfoStatus status)
        {
            switch (status)
            {
                case MarketInfoStatus.Active:
                    return "NEW: ";
                case MarketInfoStatus.Pending:
                    return "PENDING: ";
                case MarketInfoStatus.Void:
                    return "VOIDED: ";
                case MarketInfoStatus.Updated:
                    return "UPDATED: ";
                default:
                    return string.Empty;
            }
        }

        private static string GenerateAggressorBuySellText(OrderType type)
        {
            switch (type)
            {
                case OrderType.Ask:
                    return "sells";
                case OrderType.Bid:
                    return "buys";
                default:
                    return string.Empty;
            }
        }

        private static string GetPreposition(OrderType orderType)
        {
            switch (orderType)
            {
                case OrderType.Ask:
                    return "to";
                case OrderType.Bid:
                    return "from";
                default:
                    return string.Empty;
            }
        }
    }
}