﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOM.Transport.Events.ExternalDeals;
using AOM.Transport.Events.MarketInfos;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Events.Products;

namespace AOM.Services.MarketTickerService
{
    public interface IMarketTickerService
    {
        IList<Message> CreateDealMarketTick(IAomModel aomDb, AomDealResponse dealResponse);

        IList<Message> CreateExternalDealMarketTick(IAomModel aomDb, AomExternalDealResponse externalDealResponse);

        IList<Message> CreateMarketInfoMarketTick(IAomModel aomDb, AomMarketInfoResponse marketInfoResponse);

        Message CreateOrderMarketTickerItemMessage(IAomModel aomDb, Order order, Message message);

        AomMarketTickerContinuousScrollResponseRpc ReturnNextMarketTickerItemsFromDb(
            IAomModel aomDb,
            long earliestMarketTickerItemId,
            int numberOfTickerItems,
            IList<long> productIdsList,
            bool pending,
            IList<MarketTickerItemType> itemTypeList);

        IList<Message> GetItemsForRecovery(IAomModel aomDb, DateTime recoverFromTime, DateTime recoverToTime);

        Message CreateMarketStatusChangeMarketTick(IAomModel aomDb, AomMarketStatusChange marketChange);
    }
}