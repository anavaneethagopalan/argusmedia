using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;

using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOM.Transport.Events.ExternalDeals;
using AOM.Transport.Events.MarketInfos;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Events.Products;

using Utils.Logging.Utils;

namespace AOM.Services.MarketTickerService
{
    public class MarketTickerService : IMarketTickerService
    {
        private readonly IMarketTickerItemMessageGenerator _marketTickerItemMessageGenerator;

        private readonly IDateTimeProvider _dateTimeProvider;

        public MarketTickerService(IMarketTickerItemMessageGenerator marketTickerItemMessageGenerator, IDateTimeProvider dateTimeProvider)
        {
            _marketTickerItemMessageGenerator = marketTickerItemMessageGenerator;
            _dateTimeProvider = dateTimeProvider;
        }

        public AomMarketTickerContinuousScrollResponseRpc ReturnNextMarketTickerItemsFromDb(
            IAomModel aomDb,
            long earliestMarketTickerItemId,
            int numberOfTickerItems,
            IList<long> productIdsList,
            bool pending,
            IList<MarketTickerItemType> itemTypeList)
        {
            var dtoItemsFromDb =
                (from mtitem in aomDb.MarketTickerItems.AsNoTracking()
                 //faster wth AsNoTracking
                 where
                     mtitem.Id < earliestMarketTickerItemId && productIdsList.Contains((long) mtitem.ProductId)
                     && itemTypeList.Contains(
                         DtoMappingAttribute.FindEnumWithValue<MarketTickerItemType>(mtitem.MarketTickerItemType))
                 where pending ? mtitem.MarketTickerItemStatus == "P" : mtitem.MarketTickerItemStatus != "P"
                 select mtitem).ToList();

            var marketTickerItems = dtoItemsFromDb.OrderBy(x => x.Id)
                                                  .Select(x => x.ToEntity(aomDb, null))
                                                  .Take(numberOfTickerItems)
                                                  .ToList();

            var responseRpc = new AomMarketTickerContinuousScrollResponseRpc
                              {
                                  MarketTickerItems = marketTickerItems,
                                  EarliestTickerItemId =
                                      marketTickerItems.Count > 0
                                          ? marketTickerItems.Select(
                                              x => x.Id).Min()
                                          : -1
                              };

            return responseRpc;
        }

        // This method is pretty convoluted and should be refactored when we're not so close toa release - PT 21.05.2015
        public IList<Message> CreateMarketInfoMarketTick(IAomModel aomDb, AomMarketInfoResponse marketInfoResponse)
        {
            var messages = new List<Message>();
            MarketInfo marketInfo = marketInfoResponse.MarketInfo;

            var newMarketTickDto = new MarketTickerItemDto { LastUpdated = _dateTimeProvider.UtcNow };

            long? previousEditorId = null;
            bool transitioningFromPendingToVoid = false;

            try
            {
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    // update previous pending ticker items relating to this info item to remove them from view.
                    var pendingTickerItems =
                    (from item in aomDb.MarketTickerItems
                        where item.MarketInfoId == marketInfo.Id && item.MarketTickerItemStatus == "P"
                        select item).ToList();
                    foreach (var mt in pendingTickerItems)
                    {
                        mt.MarketTickerItemStatus = DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Void);
                        mt.LastUpdated = newMarketTickDto.LastUpdated;
                        mt.LastUpdatedUserId = marketInfoResponse.Message.ClientSessionInfo.UserId;
                        if (mt.OwnerOrganisationId2.HasValue)
                        {
                            previousEditorId = mt.OwnerOrganisationId2;
                        }
                        transitioningFromPendingToVoid = true;


                        var mte = mt.ToEntity(aomDb);
                        mte.LastMarketTickerItemStatus = MarketTickerItemStatus.Pending;
                        messages.Add(
                            new Message
                            {
                                MessageAction = MessageAction.Create,
                                MessageBody = mte,
                                MessageType = MessageType.MarketTickerItem
                            });
                    }


                    newMarketTickDto.DateCreated = newMarketTickDto.LastUpdated;
                    newMarketTickDto.LastUpdatedUserId = marketInfoResponse.Message.ClientSessionInfo.UserId;

                    newMarketTickDto.Name = "NewMarketInfoMarketTick";
                    newMarketTickDto.Message = _marketTickerItemMessageGenerator.GenerateMarketInfoMessage(marketInfo);
                    switch (marketInfoResponse.MarketInfo.MarketInfoStatus)
                    {
                        case MarketInfoStatus.Pending:
                            newMarketTickDto.MarketTickerItemStatus =
                                DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Pending);
                            break;
                        case MarketInfoStatus.Void:
                            newMarketTickDto.MarketTickerItemStatus =
                                DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Void);
                            break;
                        case MarketInfoStatus.Updated:
                            newMarketTickDto.MarketTickerItemStatus =
                                DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Updated);
                            break;
                        default:
                            newMarketTickDto.MarketTickerItemStatus =
                                DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Active);
                            break;
                    }
                    newMarketTickDto.MarketTickerItemType =
                        DtoMappingAttribute.GetValueFromEnum(MarketTickerItemType.Info);

                    newMarketTickDto.OwnerOrganisationId1 = marketInfoResponse.MarketInfo.OwnerOrganisationId;

                    // Don't set user company to be an owner if they are verifying or voiding the info 
                    // unless they have previously udpated the info (AR-1217)
                    if (marketInfoResponse.Message.MessageAction == MessageAction.Verify ||
                        marketInfoResponse.Message.MessageAction == MessageAction.Void)
                    {
                        newMarketTickDto.OwnerOrganisationId2 = previousEditorId;
                    }
                    else
                    {
                        newMarketTickDto.OwnerOrganisationId2 =
                            marketInfoResponse.Message.ClientSessionInfo.OrganisationId;
                    }

                    newMarketTickDto.MarketInfoId = marketInfo.Id;
                    aomDb.MarketTickerItems.Add(newMarketTickDto);
                    aomDb.SaveChangesAndLog(marketInfoResponse.Message.ClientSessionInfo.UserId);

                    foreach (var id in marketInfoResponse.MarketInfo.ProductIds)
                    {
                        var prodChannel = new MarketTickerItemProductChannelDto
                        {
                            MarketTickerItemItemId = newMarketTickDto.Id,
                            ProductId = id
                        };

                        aomDb.MarketTickerItemProductChannels.Add(prodChannel);

                    }
                    aomDb.SaveChangesAndLog(marketInfoResponse.Message.ClientSessionInfo.UserId);

                    trans.Complete();
                }
            }
            catch (DbException ex)
            {
                Log.Error("CreateMarketInfoMarketTick", ex);
                throw;
            }
            catch (Exception error)
            {
                Log.Error("Error creating market ticker item", error);
                return new List<Message>();
            }

            var messageBody = PopulateProductChannels(aomDb, newMarketTickDto.ToEntity(aomDb));
            if (transitioningFromPendingToVoid)
            {
                messageBody.LastMarketTickerItemStatus = MarketTickerItemStatus.Pending;
            }

            messages.Add(
                new Message
                {
                    MessageAction = MessageAction.Create,
                    MessageBody = messageBody,
                    MessageType = MessageType.MarketTickerItem
                });

            return messages;
        }

        public IList<Message> GetItemsForRecovery(IAomModel aomDb, DateTime recoverFromTime, DateTime recoverToTime)
        {
            var recoveryItems = new List<Message>();

            var marketTickerItems =
                (from mtitem in aomDb.MarketTickerItems.AsNoTracking()
                 //faster wth AsNoTracking
                 where mtitem.LastUpdated > recoverFromTime && mtitem.LastUpdated <= recoverToTime
                 select mtitem).ToList();

            foreach (var mti in marketTickerItems)
            {
                var mtiMessage = new Message
                                 {
                                     MessageAction = MessageAction.Create,
                                     MessageBody = mti.ToEntity(aomDb),
                                     MessageType = MessageType.MarketTickerItem
                                 };
                recoveryItems.Add(mtiMessage);
            }

            foreach (var message in recoveryItems)
            {
                message.MessageBody = PopulateProductChannels(aomDb, message.MessageBody as MarketTickerItem);
            }

            return recoveryItems;
        }

        private static MarketTickerItem PopulateProductChannels(IAomModel aomDb, MarketTickerItem mti)
        {
            if (mti.ProductIds == null || mti.ProductIds.Count == 0)
            {
                mti.ProductIds = aomDb.MarketTickerItemProductChannels
                                      .Where(x => x.MarketTickerItemItemId == mti.Id)
                                      .Select(x => x.ProductId).ToList();
            }
            return mti;
        }

        public Message CreateMarketStatusChangeMarketTick(IAomModel aomDb, AomMarketStatusChange marketChange)
        {
            var marketEventDateTime = marketChange.Product.LastUpdated; //it is important we report the time that the market open/closed and don't DateTime.Now

            var newMarketTickDto = new MarketTickerItemDto
            {
                DateCreated = marketEventDateTime,
                LastUpdated = marketEventDateTime,
                EnteredByUserId = marketChange.Message.ClientSessionInfo.UserId,
                ProductId = marketChange.Product.ProductId,
                Name = "MarketStatusChangeMarketTick",
                Message = _marketTickerItemMessageGenerator.GenerateMarketItemMessage(marketChange.Product, marketChange.Message.MessageAction),
                MarketTickerItemStatus = DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Active),
                MarketTickerItemType = DtoMappingAttribute.GetValueFromEnum(MarketTickerItemType.Info),
            };

            try
            {
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    aomDb.MarketTickerItems.Add(newMarketTickDto);
                    aomDb.SaveChangesAndLog(-1);
                    trans.Complete();
                }
            }
            catch (DbException exception)
            {
                // We have encountered a DB Exception - Re-throwing the original exception.  
                Log.Error("DB Exception - Error Creating MarketStatusChange Tick", exception);
                throw;
            }
            catch (Exception error)
            {
                Log.Error("Error creating market ticker item", error);
                return null;
            }

            //Assign a product to the products list for filtering.
            var marketTickerItem = newMarketTickDto.ToEntity(aomDb);
            marketTickerItem.ProductIds = new List<long> {marketChange.Product.ProductId};

            var message = new Message
            {
                MessageAction = MessageAction.Create,
                MessageBody = marketTickerItem,
                MessageType = MessageType.MarketTickerItem
            };

            return message;
        }

        public IList<Message> CreateDealMarketTick(IAomModel aomDb, AomDealResponse dealResponse)
        {
            Deal deal = dealResponse.Deal;
            Message message = dealResponse.Message;
            var messages = new List<Message>();

            var newMarketTickDto = new MarketTickerItemDto { LastUpdated = _dateTimeProvider.UtcNow };

            try
            {
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    foreach (var mt in from mtitem in aomDb.MarketTickerItems
                        where mtitem.DealId == deal.Id && mtitem.MarketTickerItemStatus == "P"
                        select mtitem)
                    {
                        mt.MarketTickerItemStatus = DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Void);
                        mt.LastUpdated = newMarketTickDto.LastUpdated;
                        mt.LastUpdatedUserId = dealResponse.Message.ClientSessionInfo.UserId;
                        messages.Add(
                            new Message
                            {
                                MessageAction = MessageAction.Create,
                                MessageBody = mt.ToEntity(aomDb),
                                MessageType = MessageType.MarketTickerItem
                            });
                    }
                    newMarketTickDto.DateCreated = newMarketTickDto.LastUpdated;
                    newMarketTickDto.LastUpdatedUserId = dealResponse.Message.ClientSessionInfo.UserId;
                    newMarketTickDto.Name = "NewDealMarketTick";
                    newMarketTickDto.Message = _marketTickerItemMessageGenerator.GenerateMarketItemMessage(deal, message.MessageAction);
                    newMarketTickDto.DealId = dealResponse.Deal.Id;
                    newMarketTickDto.MarketTickerItemStatus = DtoMappingAttribute.GetValueFromEnum(GetMarketTickerDealStatus(dealResponse.Deal.DealStatus));
                    newMarketTickDto.MarketTickerItemType = "D";
                    newMarketTickDto.ProductId = deal.Initial.ProductId;
                    newMarketTickDto.OrderId = deal.InitialOrderId;
                    newMarketTickDto.OwnerOrganisationId1 = deal.Initial.PrincipalOrganisationId;
                    newMarketTickDto.OwnerOrganisationId2 = deal.Initial.BrokerOrganisationId;
                    newMarketTickDto.OwnerOrganisationId3 = deal.Matching.PrincipalOrganisationId;
                    // AOMK- 348 2nd broker Market Ticker Item was NOT appearing as Orange - as he was NOT in the list of OwnerOrganisations.
                    newMarketTickDto.OwnerOrganisationId4 = deal.Matching.BrokerOrganisationId;
                    newMarketTickDto.Notes = deal.Initial.Notes;
                    newMarketTickDto.CoBrokering = deal.Matching.CoBrokering;

                    aomDb.MarketTickerItems.Add(newMarketTickDto);
                    aomDb.SaveChangesAndLog(dealResponse.Message.ClientSessionInfo.UserId);

                    trans.Complete();
                }
            }
            catch (DbException ex)
            {
                Log.Error("Create Market Tick from Deal - Db Error creating market ticker item", ex);
                throw;
            }
            catch (Exception error)
            {
                Log.Error("Error creating market ticker item", error);
                return null;
            }

            message = new Message
                      {
                          MessageAction = MessageAction.Create,
                          MessageBody = newMarketTickDto.ToEntity(aomDb),
                          MessageType = MessageType.MarketTickerItem
                      };

            messages.Add(message);

            return messages;
        }

        public IList<Message> CreateExternalDealMarketTick(IAomModel aomDb, AomExternalDealResponse externalDealResponse)
        {
            var messages = new List<Message>();
            ExternalDeal deal = externalDealResponse.ExternalDeal;
            Message message = externalDealResponse.Message;

            var newMarketTickDto = new MarketTickerItemDto { LastUpdated = _dateTimeProvider.UtcNow };

            try
            {
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    // update previous pending ticker items relating to this info item to remove them from view.
                    foreach (var mt in from mtitem in aomDb.MarketTickerItems
                        where mtitem.ExternalDealId == deal.Id && mtitem.MarketTickerItemStatus == "P"
                        select mtitem)
                    {
                        mt.MarketTickerItemStatus = DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Void);
                        mt.LastUpdated = newMarketTickDto.LastUpdated;
                        mt.LastUpdatedUserId = externalDealResponse.Message.ClientSessionInfo.UserId;
                        messages.Add(
                            new Message
                            {
                                MessageAction = MessageAction.Create,
                                MessageBody = mt.ToEntity(aomDb),
                                MessageType = MessageType.MarketTickerItem
                            });
                    }

                    newMarketTickDto.DateCreated = newMarketTickDto.LastUpdated;
                    newMarketTickDto.LastUpdatedUserId = externalDealResponse.Message.ClientSessionInfo.UserId;
                    newMarketTickDto.Name = "NewExternalDealMarketTick";
                    newMarketTickDto.Message = _marketTickerItemMessageGenerator.GenerateMarketItemMessage(deal, message.MessageAction);
                    newMarketTickDto.ExternalDealId = deal.Id;
                    newMarketTickDto.MarketTickerItemStatus = DtoMappingAttribute.GetValueFromEnum(GetMarketTickerDealStatus(deal.DealStatus));
                    newMarketTickDto.MarketTickerItemType = "D";
                    newMarketTickDto.Notes = deal.Notes;
                    newMarketTickDto.ProductId = deal.ProductId;
                    newMarketTickDto.OwnerOrganisationId1 = deal.BrokerId;
                    newMarketTickDto.OwnerOrganisationId2 = deal.BuyerId;
                    newMarketTickDto.OwnerOrganisationId3 = deal.SellerId;
                    newMarketTickDto.OwnerOrganisationId4 = deal.ReporterId;
                    aomDb.MarketTickerItems.Add(newMarketTickDto);
                    aomDb.SaveChangesAndLog(externalDealResponse.Message.ClientSessionInfo.UserId);

                    trans.Complete();
                }
            }
            catch (DbException ex)
            {
                Log.Error("CreateExternalDealMarketTick", ex);
                throw;
            }
            messages.Add(
                new Message
                {
                    MessageAction = MessageAction.Create,
                    MessageBody = newMarketTickDto.ToEntity(aomDb),
                    MessageType = MessageType.MarketTickerItem
                });

            return messages;
        }

        private MarketTickerItemStatus GetMarketTickerDealStatus(DealStatus dealStatus)
        {
            switch (dealStatus)
            {
                case DealStatus.Void:
                    return MarketTickerItemStatus.Void;
                case DealStatus.Pending:
                    return MarketTickerItemStatus.Pending;
                case DealStatus.Executed:
                    return MarketTickerItemStatus.Active;
            }
            return MarketTickerItemStatus.Active;
        }

        public Message CreateOrderMarketTickerItemMessage(IAomModel aomDb, Order order, Message message)
        {
            var marketTickMessage = new Message
                                    {
                                        MessageAction = MessageAction.Create,
                                        MessageBody = null,
                                        MessageType = MessageType.MarketTickerItem
                                    };

            var newMarketTickDto = new MarketTickerItemDto { LastUpdated = _dateTimeProvider.UtcNow };

            try
            {
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    newMarketTickDto.BrokerRestriction = DtoMappingAttribute.GetValueFromEnum(
                        order.BrokerRestriction);
                    newMarketTickDto.DateCreated = newMarketTickDto.LastUpdated;
                    newMarketTickDto.LastUpdatedUserId = message.ClientSessionInfo.UserId;
                    newMarketTickDto.Name = string.Format("{0}OrderMarketTick", message.MessageAction);

                    newMarketTickDto.Message = _marketTickerItemMessageGenerator.GenerateMarketItemMessage(
                        order,
                        message.MessageAction);
                    newMarketTickDto.MarketTickerItemType = DtoMappingAttribute.GetValueFromEnum(order.OrderType);

                    // AR-606.  We use LastOrderStatus as a placeholder until we have proper order history.
                    // This should probably get factored out in due course.  10/10/14 - PT
                    var orderIsInactive = order.OrderStatus != OrderStatus.Active;
                    var orderWasInActive = order.LastOrderStatus != OrderStatus.Active;

                    newMarketTickDto.MarketTickerItemStatus = orderIsInactive && orderWasInActive
                        ? DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Held)
                        : DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Active);
                    newMarketTickDto.OrderId = order.Id;
                    newMarketTickDto.ProductId = order.ProductId;
                    newMarketTickDto.OwnerOrganisationId1 = order.PrincipalOrganisationId;
                    newMarketTickDto.OwnerOrganisationId2 = order.BrokerOrganisationId;
                    newMarketTickDto.Notes = order.Notes;
                    newMarketTickDto.CoBrokering = order.CoBrokering;

                    aomDb.MarketTickerItems.Add(newMarketTickDto);
                    aomDb.SaveChangesAndLog(message.ClientSessionInfo.UserId);

                    trans.Complete();
                }
            }
            catch (DbException exception)
            {
                // We have encountered a DB Exception - Re-throwing the original exception.  
                Log.Error("DB Exception - Error Creating Market Tick for Order", exception);
                throw;
            }
            catch (Exception error)
            {
                Log.Error("Error creating market ticker item", error);
                return null;
            }

            marketTickMessage.MessageBody = newMarketTickDto.ToEntity(aomDb);

            return marketTickMessage;
        }

    }
}