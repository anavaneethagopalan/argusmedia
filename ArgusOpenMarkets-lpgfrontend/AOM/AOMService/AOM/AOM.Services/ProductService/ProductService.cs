﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;

using Utils.Database;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

using ServiceStack.Text;

namespace AOM.Services.ProductService
{
	public class ProductService : IProductService
	{
		private readonly IDateTimeProvider _dateTimeProvider;
		private readonly IDbContextFactory _dbContextFactory;

		public ProductService(IDbContextFactory dbContextFactory, IDateTimeProvider dateTimeProvider)
		{
			_dbContextFactory = dbContextFactory;
			_dateTimeProvider = dateTimeProvider;
		}

		public ProductService()
		{
			_dbContextFactory = new DbContextFactory();
			_dateTimeProvider = new DateTimeProvider();
		}

		public ProductDefinitionItem GetProductDefinition(long productId)
		{
			// return GetAllProductDefinitions().FirstOrDefault(p => p.ProductId == productId);
			return GetSingleProductDefinition(productId);
		}


		public ProductDefinitionItem GetSingleProductDefinition(long productId)
		{
			Log.Info("GetAllProductDefinitions has been called");

			List<ProductDefinitionItem> productDefinitions = new List<ProductDefinitionItem>();


			using (IAomModel context = _dbContextFactory.CreateAomModel())
			{
				var products = (from product in context.Products
								.Include(p => p.ContractTypeDto)
								//.Include(p => p.CurrencyDto)
								.Include(p => p.DeliveryLocations)
								.Include(p => p.VolumeUnitDto)
								.Include(p => p.CommodityDto)
								.Include(p => p.CommonQuantityValues)
								.Include(p => p.ProductTenors)
								where product.Id.Equals(productId)
								select product).ToList();

				//Seems like these subsequent calls should overwrite the products collection but it actually loads all the sub entities!
				//This splits up the sql statement that the MySql EF adapter creates and allows the correct population of the model rather than some shite nested sql statement that does not work.
				try
				{
					products = (from product in context.Products
								.Include(p => p.ProductTenors.Select(pt => pt.ProductTenorPeriods))
								.Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines))
								select product).ToList();
				}
				catch (Exception ex)
				{
					Log.Error("GetSingleProductDefinition - ProductTenorPeriods | ProductPriceLines", ex);
				}

				try
				{
					products = (from product in context.Products
								.Include(p => p.ProductTenors.Select(pt => pt.ProductTenorPeriods.Select(ptp => ptp.DeliveryPeriodDto)))
								.Include(p => p.ProductTenors.Select(pt => pt.ProductTenorPeriods.Select(ptp => ptp.DefaultPeriodDto)))
								select product).ToList();
				}
				catch (Exception ex)
				{
					Log.Error("GetSingleProductDefinition - ProductTenorPeriods | DeliveryPeriodDto", ex);
				}

				try
				{
					products = (from product in context.Products
								.Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines.Select(ppl => ppl.ProductPriceLineBasesPeriods)))
									//.Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines.Select(ppl => ppl.ProductPriceLineBasesPeriods.Select(pplbp => pplbp.PriceBasisDto))))
									//.Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines.Select(ppl => ppl.ProductPriceLineBasesPeriods.Select(pplbp => pplbp.PeriodDto))))
								select product).ToList();
				}
				catch (Exception ex)
				{
					Log.Error("GetSingleProductDefinition - ProductTenorPeriods | ProductPriceLines", ex);
				}

				List<PriceTypeDto> pts = new List<PriceTypeDto>();
				if (context.PriceTypes.Any())
				{
					pts = context.PriceTypes.ToList();
				}

				List<CurrencyDto> currs = new List<CurrencyDto>();
				if (context.Currencies.Any())
				{
					currs = context.Currencies.ToList();
				}

				List<PriceBasisDto> pbs = new List<PriceBasisDto>();
				if (context.PriceBases.Any())
				{
					pbs = context.PriceBases.ToList();
				}

				List<PeriodDto> pers = new List<PeriodDto>();
				if (context.Periods.Any())
				{
					pers = context.Periods.ToList();
				}

				productDefinitions.AddRange(from product in products
											let streams = context.CommodityContentStreams.Where(stream => stream.CommodityId == product.CommodityId).ToList()
											let priceTypes = pts
											let currencies = currs
											let priceBases = pbs
											let periods = pers
											select CreateProductDefinitionFromProduct(product, streams, priceBases, periods, priceTypes));
			}

			return productDefinitions.FirstOrDefault();
		}

        public List<ProductDefinitionItem> GetAllProductDefinitions()
        {
            List<ProductDefinitionItem> productDefinitionItems;

            var cacheKey = "PS_GET_ALL_PRODUCT_DEFINITIONS";
            if (MemoryCache.Default.Contains(cacheKey))
            {
                Log.Debug(string.Format("ProductService - get all product definitions - reading from cache: {0}", cacheKey));
                productDefinitionItems = MemoryCache.Default[cacheKey] as List<ProductDefinitionItem>;
            }
            else
            {
                Log.Debug(string.Format("ProductService - get all product definitions",
                    cacheKey));
                productDefinitionItems = GetAllProductDefinitionsFromDb();
                MemoryCache.Default.Set(cacheKey, productDefinitionItems,
                    new CacheItemPolicy() { SlidingExpiration = new TimeSpan(0, 0, 0, 60000) });
            }

            return productDefinitionItems;
        }

        public List<ProductDefinitionItem> GetAllProductDefinitionsFromDb()
        {
            Log.Info("GetAllProductDefinitions has been called");

            try
            {
                List<ProductDefinitionItem> productDefinitions = new List<ProductDefinitionItem>();

                try
                {
                    using (IAomModel context = _dbContextFactory.CreateAomModel())
                    {
                        var products = (from product in context.Products
                                        .Include(p => p.ContractTypeDto)
                                        //.Include(p => p.CurrencyDto)
                                        .Include(p => p.DeliveryLocations)
                                        .Include(p => p.VolumeUnitDto)
                                        .Include(p => p.CommodityDto)
                                        .Include(p => p.CommonQuantityValues)
                                        .Include(p => p.ProductTenors)
                                        select product).ToList();

                        //Seems like these subsequent calls should overwrite the products collection but it actually loads all the sub entities!
                        //This splits up the sql statement that the MySql EF adapter creates and allows the correct population of the model rather than some shite nested sql statement that does not work.
                        products = (from product in context.Products
                                    .Include(p => p.ProductTenors.Select(pt => pt.ProductTenorPeriods))
                                    .Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines))
                                    select product).ToList();

                        products = (from product in context.Products
                                    .Include(p => p.ProductTenors.Select(pt => pt.ProductTenorPeriods.Select(ptp => ptp.DeliveryPeriodDto)))
                                    .Include(p => p.ProductTenors.Select(pt => pt.ProductTenorPeriods.Select(ptp => ptp.DefaultPeriodDto)))
                                    select product).ToList();

                        products = (from product in context.Products
                                    .Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines.Select(ppl => ppl.ProductPriceLineBasesPeriods)))
                                    //.Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines.Select(ppl => ppl.ProductPriceLineBasesPeriods.Select(pplbp => pplbp.PriceBasisDto))))
                                    //.Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines.Select(ppl => ppl.ProductPriceLineBasesPeriods.Select(pplbp => pplbp.PeriodDto))))
                                    select product).ToList();


                        productDefinitions.AddRange(from product in products
                                                    let streams = context.CommodityContentStreams.Where(stream => stream.CommodityId == product.CommodityId).ToList()
                                                    let priceTypes = context.PriceTypes.ToList()
                                                    let currencies = context.Currencies.ToList()
                                                    let priceBases = context.PriceBases.ToList()
                                                    let periods = context.Periods.ToList()
                                                    select CreateProductDefinitionFromProduct(product, streams, priceBases, periods, priceTypes));
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Error in GetAllProductDefinitions: ", ex);
                }

                Log.Info(string.Format("Successfully returning: {0} product definitions", productDefinitions.Count));
                foreach (ProductDefinitionItem pd in productDefinitions)
                {
                    foreach (ProductTenor pt in pd.ContractSpecification.ProductTenors)
                    {
                        Log.Info(string.Format("Product Id:{0}  ProductName:{1}  ProductTenor:{2}  Status:{3}", pd.ProductId, pd.ProductName, pt.Name, pd.Status));
                    }
                }

                return productDefinitions;
            }
            catch (Exception ex)
            {
                Log.Error("Error Getting Product Definitions", ex);
            }

            return new List<ProductDefinitionItem>();
        }

        public List<Product> GetAllProducts()
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                List<ProductDto> products = context.Products.Where(p => p.IsDeleted == false).ToList();
                return products.Select(p => p.ToEntity(new Product())).ToList();
            }
        }

        public Product GetProduct(long productId)
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                return (from product in context.Products
                        //.Include(p => p.CurrencyDto)
                        .Include(p => p.VolumeUnitDto)
                        //.Include(p => p.PriceUnitDto)
                        select product).Single(p => p.Id == productId).ToEntity(new Product());
            }
        }

        public Product ChangeMarketStatus(IAomModel aomDb, AomProductResponse request, MarketStatus status)
        {
            return ConcurrencyHelper.RetryOnConcurrencyException("Attempting to update market status to " + status, 3, () =>
                {
                    //Due to multiple processor boxes requesting a single market change its status it is possible to get deadlocks
                    //the following blog post helps explain why we are using readcommited here...
                    //http://blogs.msdn.com/b/dbrowne/archive/2010/05/21/using-new-transactionscope-considered-harmful.aspx
                    //
                    using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                    {
                        var newStatus = DtoMappingAttribute.GetValueFromEnum(status);
                        var product = (from p in aomDb.Products where p.Id == request.Product.ProductId select p).FirstOrDefault();
                        if (product == null)
                        {
                            throw new Exception(string.Format("Cannot update market status for product that doesn't exist [{0}]", request.Product.ProductId));
                        }
                        if (product.Status == newStatus)
                        {
                            // One of the other processors must have picked this up - or someone has manually updated the Product Status.
                            Log.InfoFormat("Cannot update market status. Market is already {0} for product id {1}", newStatus, request.Product.ProductId);

                            // Note: May publish Market Status change multiple times in ProductsProcessor unless we return null here (AOMK-105)
                            return null;
                        }

                        product.Status = newStatus;
                        //product.LastUpdated = DateTime.UtcNow;  //NOW USES A COMPUTED DB COLUMN
                        product.LastUpdatedUserId = request.Product.LastUpdatedUserId;

                        switch (status)
                        {
                            case MarketStatus.Open:
                                product.LastOpenDate = product.LastUpdated;
                                break;
                            case MarketStatus.Closed:
                                product.LastCloseDate = product.LastUpdated;
                                break;
                        }
                        aomDb.SaveChangesAndLog(-1);
                        trans.Complete();
                        Log.Info(string.Format("Market status updated. Market is now {0} for product id {1}", newStatus, request.Product.ProductId));
                        return product.ToEntity(new Product());
                    }
                });
        }

        public Product EditProduct(IAomModel model, AomProductResponse request)
        {
            return EditProduct(model, request, MessageAction.Update);
        }

        public ProductTenor GetProductTenor(long productTenorId)
        {
            using (var context = new AomModel())
            {
                return context.ProductTenors.Single(t => t.Id == productTenorId).ToEntity();
            }
        }

        public IEnumerable<TenorCode> GetAllTenorCodes()
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                List<TenorCodeDto> tenorCodes = context.TenorCodes.Select(x => x).ToList();
                return tenorCodes.Select(pt => pt.ToEntity()).ToList();
            }
        }
        
        public IEnumerable<ProductTenor> GetAllProductTenors()
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                List<ProductTenorDto> productTenors = context.ProductTenors.Select(x => x).ToList();
                return productTenors.Select(pt => pt.ToEntity()).ToList();
            }
        }

        public IEnumerable<PriceBasis> GetAllPriceBases()
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                List<PriceBasisDto> priceBases = context.PriceBases.Include(p => p.PriceTypeDto).ToList();
                return priceBases.Select(pb => pb.ToEntity()).ToList();
            }
        }

        public IEnumerable<Period> GetAllPeriods()
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                List<PeriodDto> periods = context.Periods.ToList();
                return periods.Select(pp => pp.ToEntity()).ToList();
            }
        }

        public List<CommodityContentStreamDto> GetCommoditiesForContentStreams(long[] contentStreams)
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                var commodityContentStreams = context.CommodityContentStreams.Where(ccs => contentStreams.Contains(ccs.ContentStreamId)).ToList();
                return commodityContentStreams;
            }
        }

        public List<Commodity> GetCommodities()
        {
            Log.Info("GetCommodities has been called");

            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                var commodities = context.Commodities.ToList();
                return commodities.Select(c => c.ToEntity(new Commodity())).ToList();
            }
        }

        public List<ContractType> GetContractTypes()
        {
            Log.Info("GetContractTypes has been called");

            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                var contractTypes = context.ContractTypes.ToList();
                return contractTypes.Select(ct => ct.ToEntity(new ContractType())).ToList();
            }
        }

        public List<Market> GetMarkets()
        {
            Log.Info("GetMarkets has been called");

            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                var markets = context.Markets.ToList();
                return markets.Select(m => m.ToEntity(new Market())).ToList();
            }
        }

        public List<MarketsProducts> GetAllMarketsAndProducts()
        {
            Log.Info("GetAllMarketsAndProducts has been called");

            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                return context.Products.Select(p => new MarketsProducts() { MarketId = p.MarketId, MarketName = p.MarketDto.Name, ProductId = p.Id, ProductName = p.Name }).ToList();
            }
        }

        public List<DeliveryLocation> GetDeliveryLocations()
        {
            Log.Info("GetDeliveryLocations has been called");

            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                var deliveryLocations = context.DeliveryLocations.ToList();
                return deliveryLocations.Select(dl => dl.ToEntity(new DeliveryLocation())).ToList();
            }
        }

        public List<Currency> GetCurrencies()
        {
            Log.Info("GetCurrencies has been called");

            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                var currencies = context.Currencies.ToList();
                return currencies.Select(c => c.ToEntity(new Currency())).ToList();
            }
        }

        public List<Unit> GetUnits()
        {
            Log.Info("GetUnits has been called");

            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                var units = context.Units.ToList();
                return units.Select(u => u.ToEntity(new Unit())).ToList();
            }
        }

        public MarketStatus GetMarketStatus(long productId)
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                var statusdto = context.Products.Where(p => p.Id.Equals(productId)).Select(z => z.Status).First();
                return DtoMappingAttribute.FindEnumWithValue<MarketStatus>(statusdto);
            }
        }

        public List<ProductOpeningTimes> GetAllProductOpeningTimes()
        {
            using (IAomModel context = _dbContextFactory.CreateAomModel())
            {
                List<ProductOpeningTimes> openingTimes = (from product in context.Products
                                                          select new ProductOpeningTimes
                                                          {
                                                              Id = product.Id,
                                                              Name = product.Name,
                                                              CloseTime = product.CloseTime,
                                                              OpenTime = product.OpenTime,
                                                          }).ToList();
                return openingTimes;
            }
        }

        public ProductTenor EditProductTenor(IAomModel model, AomProductTenorResponse request)
        {
            return EditProductTenor(model, request, MessageAction.Update);
        }

        public bool SaveProduct(ProductDto product, long userId)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                var validProduct = ValidateProduct(product);
                if (validProduct == string.Empty)
                {
                    using (var context = _dbContextFactory.CreateAomModel())
                    {
                        context.Products.Add(product);
                        try
                        {
                            context.SaveChangesAndLog(userId);
                        }
                        catch (DbEntityValidationException dbEx)
                        {
                            foreach (DbEntityValidationResult validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (DbValidationError validationError in validationErrors.ValidationErrors)
                                {
                                    string message = string.Format("{0}:{1}", validationErrors.Entry.Entity, validationError.ErrorMessage);
                                    Log.Error(message);
                                }
                            }
                            throw;
                        }
                    }
                }
                else
                {
                    throw new BusinessRuleException("ProductValidation Error" + validProduct);
                }

                trans.Complete();
            }
            return true;
        }

        public string CalculateProductStatus(TimeSpan? openTime, TimeSpan? closeTime)
        {
            if (openTime.HasValue && closeTime.HasValue)
            {
                var now = _dateTimeProvider.UtcNow.TimeOfDay;
                if (openTime <= now && now <= closeTime)
                {
                    // Date Time UTC now - is between open / close time.  Market should be open.
                    return "O";
                }
            }

            return "C";
        }

        public bool CoBrokeringAllowed(long productId)
        {
            var coBrokeringAllowed = false;

            var productDefinition = GetProduct(productId);
            if (productDefinition != null)
            {
                coBrokeringAllowed = productDefinition.CoBrokering;
            }

            return coBrokeringAllowed;
        }

        private Product EditProduct(IAomModel aomDb, AomProductResponse productRequest, MessageAction msgAction)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                ProductDto currentProductDto = (from p in aomDb.Products where productRequest.Product.ProductId == p.Id select p).SingleOrDefault();

                if (currentProductDto == null)
                {
                    throw new BusinessRuleException(String.Format("Error: product {0} does not exist.", productRequest.Product.ProductId));
                }
                ProductDto updatedProductDto = productRequest.Product.ToDto();

                if (ValidateProduct(msgAction, updatedProductDto, currentProductDto))
                {
                    currentProductDto.LastUpdated = productRequest.Product.LastUpdated = _dateTimeProvider.UtcNow;
                    currentProductDto.LastUpdatedUserId = productRequest.Product.LastUpdatedUserId = productRequest.Message.ClientSessionInfo.UserId;
                    productRequest.Product.ToDto(currentProductDto);
                    aomDb.SaveChangesAndLog(productRequest.Message.ClientSessionInfo.UserId);
                }

                trans.Complete();
                return currentProductDto.ToEntity(new Product());
            }
        }

        private Boolean ValidateProduct(MessageAction action, ProductDto newState, ProductDto previousState = null)
        {
            if (previousState != null)
            {
                if (previousState.LastUpdated.RoundToMs() != newState.LastUpdated.RoundToMs()) //TODO Precision Issue
                {
                    throw new BusinessRuleException(ConstructAlreadyUpdatedMessage(action, newState, previousState));
                }
            }

            return true;
        }

        public Product GetProductById(IAomModel aomModel, long productId)
        {
            ProductDto repositoryProductDto = (from p in aomModel.Products where productId == p.Id select p).SingleOrDefault();

            if (repositoryProductDto == null)
            {
                return null;
            }
            return repositoryProductDto.ToEntity(new Product());
        }

        public List<ProductSetting> GetProductSettings(long[] productIds)
        {
            var productSettings = new List<ProductSetting>();

            using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
            {
                var products = aomModel.Products.Where(p => productIds.Contains(p.Id))
                    .Include(p => p.ContractTypeDto)
                    .Include(p => p.ProductTenors)
                    //.Include(p => p.ProductTenors.Select(pt => pt.TenorDto))
                    .Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines))
                    .Include(p => p.ProductTenors.Select(pt => pt.ProductPriceLines.Select(ppl => ppl.ProductPriceLineBasesPeriods)));
                    
                foreach (ProductDto productDto in products)
                {
                    productSettings.Add(productDto.ToEntity(new ProductSetting()));
                }
            }

            return productSettings;
        }

        private string ConstructAlreadyUpdatedMessage(MessageAction action, ProductDto newState, ProductDto previousState)
        {
            string myAction;
            switch (action)
            {
                case MessageAction.Create:
                    myAction = "created";
                    break;
                case MessageAction.Update:
                    myAction = "updated";
                    break;
                case MessageAction.Open:
                    myAction = "opened";
                    break;
                case MessageAction.Close:
                    myAction = "closed";
                    break;
                default:
                    myAction = "**UNKNOWN ACTION**";
                    break;
            }

            return "The product (Ref. #{product}: could not be {actioned} as this product was updated at {lastupdate}."
                .SafeReplace("{product}", () => newState.Id.ToString(CultureInfo.InvariantCulture))
                .SafeReplace("{actioned}", () => myAction)
                .SafeReplace("{lastupdate}", () => previousState.LastUpdated.ToString("HH:mm:ss"));
        }

        private ProductDefinitionItem CreateProductDefinitionFromProduct(ProductDto productDto, IEnumerable<CommodityContentStreamDto> contentStreams, 
            IEnumerable<PriceBasisDto> allPriceBases, IEnumerable<PeriodDto> allPeriods, IEnumerable<PriceTypeDto> allPriceTypes)
        {
            List<DeliveryLocation> delLocs = new List<DeliveryLocation>();

			if (productDto.DeliveryLocations != null)
			{
				foreach (DeliveryLocationDto delLoc in productDto.DeliveryLocations)
				{
					delLocs.Add(new DeliveryLocation()
					{
						Id = delLoc.Id,
						Name = delLoc.Name,
						LocationLabel = delLoc.LocationLabel,
						DateCreated = delLoc.DateCreated
					});
				}
			}

			ProductDefinitionItem productDefinition;

			try
			{
				List<CommonQuantityValues> commonQuants = new List<CommonQuantityValues>();
				if (productDto.CommonQuantityValues != null)
				{

					commonQuants = productDto.CommonQuantityValues.OrderBy(x => x.DisplayOrder).Select(
									cqv => new CommonQuantityValues()
									{
										Quantity = cqv.Quantity,
										QuantityText = cqv.QuantityText,
										DisplayQuantityText = cqv.DisplayQuantityText,
										DisplayOrder = cqv.DisplayOrder
									}).ToList();
				}

				long[] conStreams = new long[0];
				if (contentStreams != null)
				{
					conStreams = contentStreams.Select(x => x.ContentStreamId).ToArray();
				}

				List<ProductTenor> prodTenors = new List<ProductTenor>();
				if (productDto.ProductTenors != null)
				{
					try
					{
						// TODO: Mark this code below is breaking.
						prodTenors = productDto.ProductTenors.Select(pt =>
								new ProductTenor
								{
									Id = pt.Id,
									Name = pt.Name,
									DisplayName = pt.DisplayName,
									ProductPriceLines = pt.ProductPriceLines.Select(ppl => ppl.ToEntity()).ToList(),

									//new ProductPriceLine
									//{
									//    Id = ppl.Id,
									//    ProductTenorId = ppl.ProductTenorId,
									//    BasisProductId = ppl.BasisProductId,
									//    SequenceNo = ppl.SequenceNo,
									//    Description = ppl.Description,
									//    PercentageOptionList = ppl.PercentageOptionList,
									//DefaultDisplay = ppl.DefaultDisplay,
									//PriceBases = ppl.ProductPriceLineBasesPeriods.to
									//ProductPriceLineBasisPeriods = ppl.ProductPriceLineBasesPeriods.Select(pplbp =>
									//    new ProductPriceLineBasisPeriod
									//    {
									//        DisplayOrder = pplbp.DisplayOrder,
									//        PriceBasis = allPriceBases.FirstOrDefault(pb => pb.Id == pplbp.PriceBasisId).ToEntity(),
									//        Period = pplbp.PeriodId == -1 ? null : allPeriods.FirstOrDefault(p => p.Id == pplbp.PeriodId).ToEntity()
									//    }).ToList()
									//}).ToList(),
									ProductTenorPeriods = pt.ProductTenorPeriods.Select(ptp =>
												new ProductTenorPeriod
												{
													DeliveryPeriod = ptp.DeliveryPeriodDto == null ? null : ptp.DeliveryPeriodDto.ToEntity(),
													DefaultPeriod = ptp.DefaultPeriodDto == null ? null : ptp.DefaultPeriodDto.ToEntity()
												}).ToList()
								}).ToList();
					}
					catch (Exception ex)
					{
						Log.ErrorFormat("Error generating ProductTenor", ex);
					}
				}
			

				productDefinition = new ProductDefinitionItem
				{
					ProductName = productDto.Name,
					ProductTitle = productDto.Name,
					ProductId = productDto.Id,
					IsInternal = productDto.IsInternal,
					HasAssessment = productDto.HasAssessment,
					Status = DtoMappingAttribute.FindEnumWithValue<MarketStatus>(productDto.Status),
					OpenTime = productDto.OpenTime,
					CloseTime = productDto.CloseTime,
					LastOpenTime = productDto.LastOpenDate,
					LastCloseTime = productDto.LastCloseDate,
					PurgeTimeOfDay = productDto.PurgeTimeOfDay,
					PurgeFrequency = productDto.PurgeFrequency,
					BidAskStackNumberRows = productDto.BidAskStackNumberRows,
					BidAskStackStyle = productDto.BidAskStackStyleCode,
					AllowNegativePriceForExternalDeals = productDto.AllowNegativePriceForExternalDeals,
					DisplayOptionalPriceDetail = productDto.DisplayOptionalPriceDetail,
					//LocationLabel = productDto.LocationLabel,
					DeliveryPeriodLabel = productDto.DeliveryPeriodLabel,
					CoBrokering = productDto.CoBrokering,
					DisplayOrder = productDto.DisplayOrder,
					Commodity = new Commodity
					{
						Id = productDto.CommodityDto.Id,
						Name = productDto.CommodityDto.Name
					},
					ContentStreamIds = conStreams,
					DeliveryLocations = delLocs,
					ContractSpecification = new ContractSpecification
					{
						ContractType = productDto.ContractTypeDto.Name,
						//Pricing = new Pricing
						//{
						//    DecimalPlaces = productDto.OrderPriceDecimalPlaces,
						//    Increment = productDto.OrderPriceIncrement,
						//    Maximum = productDto.OrderPriceMaximum,
						//    Minimum = productDto.OrderPriceMinimum,
						//    PricingCurrencyCode = productDto.CurrencyDto.Code, //was: PricingCurrencyCode = productDto.CurrencyDto.DisplayName
						//    PricingCurrencyDisplayName = productDto.CurrencyDto.DisplayName,
						//    PricingUnitName = productDto.PriceUnitDto.Name,
						//    PricingUnitDescription = productDto.PriceUnitDto.Description, //was: PricingUnitCode = productDto.PriceUnitDto.Name
						//},
						Volume = new Volume
						{
							Default = productDto.OrderVolumeDefault,
							DecimalPlaces = productDto.OrderVolumeDecimalPlaces,
							Increment = productDto.OrderVolumeIncrement,
							Maximum = productDto.OrderVolumeMaximum,
							Minimum = productDto.OrderVolumeMinimum,
							VolumeUnitCode = productDto.VolumeUnitDto.Name,
							VolumeUnitName = productDto.VolumeUnitDto.Name, //was: VolumeUnitCode = productDto.VolumeUnitDto.Name,
							VolumeUnitDescription = productDto.VolumeUnitDto.Description,
							//CommonQuantities = productDto.CommonQuantityValues.OrderBy(x => x.Quantity).Select(x => x.Quantity).ToList(),
							CommonQuantityValues = commonQuants
						},
						ProductTenors = prodTenors
					}
				};
			}
			catch (Exception ex){
				Log.ErrorFormat("ProductService.XX", ex);
				return new ProductDefinitionItem();
			}

            return productDefinition;
        }

        private ProductTenor EditProductTenor(IAomModel aomDb, AomProductTenorResponse productTenorRequest, MessageAction msgAction)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                ProductTenorDto currentProductTenorDto = (from p in aomDb.ProductTenors where productTenorRequest.ProductTenor.Id == p.Id select p).SingleOrDefault();

                if (currentProductTenorDto == null)
                {
                    throw new BusinessRuleException(String.Format("Error: product tenor {0} does not exist.", productTenorRequest.ProductTenor.ProductId));
                }
                ProductTenorDto updatedProductTenorDto = productTenorRequest.ProductTenor.ToDto();

                if (ValidateProduct(msgAction, updatedProductTenorDto, currentProductTenorDto))
                {
                    currentProductTenorDto.LastUpdated = productTenorRequest.ProductTenor.LastUpdated = _dateTimeProvider.UtcNow;
                    currentProductTenorDto.LastUpdatedUserId = productTenorRequest.ProductTenor.LastUpdatedUserId = productTenorRequest.Message.ClientSessionInfo.UserId;
                    productTenorRequest.ProductTenor.ToDto(currentProductTenorDto);
                    aomDb.SaveChangesAndLog(productTenorRequest.Message.ClientSessionInfo.UserId);
                }
                trans.Complete();
                return currentProductTenorDto.ToEntity();
            }
        }

        private Boolean ValidateProduct(MessageAction action, ProductTenorDto newState, ProductTenorDto previousState = null)
        {
            if (previousState != null)
            {
                if (previousState.LastUpdated.RoundToMs() != newState.LastUpdated.RoundToMs()) //TODO Precision Issue
                {
                    throw new BusinessRuleException(ConstructAlreadyUpdatedMessage(action, newState, previousState));
                }
            }

            return true;
        }

        private string ConstructAlreadyUpdatedMessage(MessageAction action, ProductTenorDto newState, ProductTenorDto previousState)
        {
            string myAction;
            switch (action)
            {
                case MessageAction.Create:
                    myAction = "created";
                    break;
                case MessageAction.Update:
                    myAction = "updated";
                    break;
                default:
                    myAction = "**UNKNOWN ACTION**";
                    break;
            }

            return "The product tenor (Ref. #{product}: could not be {actioned} as this product tenor was updated at {lastupdate}."
                .SafeReplace("{product}", () => newState.Id.ToString(CultureInfo.InvariantCulture))
                .SafeReplace("{actioned}", () => myAction)
                .SafeReplace("{lastupdate}", () => previousState.LastUpdated.ToString("HH:mm:ss"));
        }
        
        private string ValidateProduct(ProductDto product)
        {
            StringBuilder errorMessage = new StringBuilder();
            if (product == null)
            {
                return "Product cannot be null";
            }

            // Volume Checks
            if (product.OrderVolumeDefault < product.OrderVolumeMinimum)
            {
                errorMessage.Append("Order Volume Default cannot be less that Order Volume Minimum" + Environment.NewLine);
            }

            if (product.OrderVolumeDefault > product.OrderVolumeMaximum)
            {
                errorMessage.Append("Order Volume Default cannot be greater than Order Volume Maximum" + Environment.NewLine);
            }

            if (product.OrderVolumeMaximum <= product.OrderVolumeMinimum)
            {
                errorMessage.Append("Order Volume Maximum cannot be less than or equal to Order Volume Minimum" + Environment.NewLine);
            }

            if (product.OrderPriceMinimum > product.OrderPriceMaximum)
            {
                errorMessage.Append("Order Price Minimum cannot be greater than Price Maximum" + Environment.NewLine);
            }

            // Product Required Fields
            if (product.CommodityId == 0)
            {
                errorMessage.Append("No Commodity specified" + Environment.NewLine);
            }

            if (product.MarketId == 0)
            {
                errorMessage.Append("No Market Specified" + Environment.NewLine);
            }

            if (product.ContractTypeId == 0)
            {
                errorMessage.Append("No Contract Type specified" + Environment.NewLine);
            }

            if (product.DeliveryLocations == null)
            {
                errorMessage.Append("No Location specified" + Environment.NewLine);
            }

            if (string.IsNullOrEmpty(product.VolumeUnit))
            {
                errorMessage.Append("No Volume Unit specified" + Environment.NewLine);
            }

            //TODO
            //if (string.IsNullOrEmpty(product.Currency))
            //{
            //    errorMessage.Append("No Currency Code specified" + Environment.NewLine);
            //}
            
            //if (string.IsNullOrEmpty(product.PriceUnit))
            //{
            //    errorMessage.Append("No Price Unit specified" + Environment.NewLine);
            //}

            if (product.ProductTenors != null)
            {
                foreach (var pt in product.ProductTenors)
                {
                    errorMessage.Append(ValidateProductTenor(product, pt));
                }
            }

            // Calculate the Product Status - dependant on the Market Open/Close times.
            product.Status = CalculateProductStatus(product.OpenTime, product.CloseTime);

            return errorMessage.ToString();
        }

        private string ValidateProductTenor(ProductDto product, ProductTenorDto pt)
        {
            string errorMessage = string.Empty;

            if (pt == null)
            {
                return string.Empty;
            }

            pt.LastUpdated = DateTime.Now;
            pt.LastUpdatedUserId = product.LastUpdatedUserId;

            if (pt.ProductId != product.Id)
            {
                errorMessage += "Product Tenor - Product Id does not match Product - Product Id";
            }

            return errorMessage;
        }
    }
}