﻿namespace AOM.Services.ProductService
{
    using App.Domain.Entities;
    using Repository.MySql.Aom;
    using System.Collections.Generic;
    using Transport.Events.Products;

    public interface IProductService
    {
        List<ProductOpeningTimes> GetAllProductOpeningTimes();
        ProductDefinitionItem GetProductDefinition(long productId);
        List<ProductDefinitionItem> GetAllProductDefinitions();

        Product GetProduct(long productId);
        List<Product> GetAllProducts();
        Product EditProduct(IAomModel model, AomProductResponse request);
        List<ProductSetting> GetProductSettings(long[] productIds);
        bool SaveProduct(ProductDto product, long userId);

        IEnumerable<PriceBasis> GetAllPriceBases();
        //TenorCode GetTenor(long tenorId);
        //IEnumerable<TenorCode> GetAllTenors();
        ProductTenor GetProductTenor(long productTenorId);
        ProductTenor EditProductTenor(IAomModel model, AomProductTenorResponse request);
        IEnumerable<ProductTenor> GetAllProductTenors();

        MarketStatus GetMarketStatus(long productId);
        Product ChangeMarketStatus(IAomModel model, AomProductResponse request, MarketStatus status);
        List<Commodity> GetCommodities();
        List<ContractType> GetContractTypes();
        List<Market> GetMarkets();
        List<MarketsProducts> GetAllMarketsAndProducts();
        List<DeliveryLocation> GetDeliveryLocations();
        List<Currency> GetCurrencies();
        List<Unit> GetUnits();
        
        List<CommodityContentStreamDto> GetCommoditiesForContentStreams(long[] contentStreams);
        bool CoBrokeringAllowed(long productId);
    }
}