﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.DealService;
using AOM.Services.ProductService;
using AOM.Services.MetadataValidator;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;

using ServiceStack.Text;
using Utils.Database;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Services.OrderService
{
    using AomRepository = Repository.MySql.Aom;

    public class OrderService : IOrderService
    {
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IDealService _dealService;
        private readonly IOrganisationService _organisationService;
        private readonly IUserService _userService;
        private readonly IProductService _productService;
        private readonly IMetadataValidator _metadataValidator;

        public OrderService(IOrganisationService organisationService, IDealService dealService, IUserService userService, IDateTimeProvider dateTimeProvider,
            IProductService productService, IMetadataValidator metadataValidator)
        {
            _dealService = dealService;
            _userService = userService;
            _dateTimeProvider = dateTimeProvider;
            _organisationService = organisationService;
            _productService = productService;
            _metadataValidator = metadataValidator;
        }

        public Order NewOrder(AomRepository.IAomModel aomModel,  AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus)
        {
            orderRequest.Order.OrderStatus = OrderStatus.Active;
            orderRequest.Order.IsVirtual = false;
            if (orderRequest.Order.ExecutionMode == ExecutionMode.None)
            {
                orderRequest.Order.ExecutionMode = ExecutionMode.Internal;
            }
            return NewInternalOrder(aomModel, orderRequest, MessageAction.Create, marketStatus);
        }

        public Order EditOrder(AomRepository.IAomModel aomDb, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus)
        {
            return EditInternalOrder(aomDb, orderRequest, MessageAction.Update, marketStatus);
        }

        /// <summary>
        ///     Attemps to Kill the specified order
        /// </summary>
        public Order KillOrder(AomRepository.IAomModel aomDb, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus)
        {
            orderRequest.Order.OrderStatus = OrderStatus.Killed;
            orderRequest.Order.OrderPriceLines.ForEach(opl => opl.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(orderRequest.Order.OrderStatus));
            return EditInternalOrder(aomDb, orderRequest, MessageAction.Kill, marketStatus);
        }

        /// <summary>
        ///     Attempts to set the specified order into Hold state
        /// </summary>
        public Order HoldOrder(AomRepository.IAomModel aomDb, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus)
        {
            orderRequest.Order.OrderStatus = OrderStatus.Held;
            orderRequest.Order.OrderPriceLines.ForEach(opl => opl.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(orderRequest.Order.OrderStatus));
            return EditInternalOrder(aomDb, orderRequest, MessageAction.Hold, marketStatus);
        }

        /// <summary>
        ///     Attempts to set the specified order into Active state
        /// </summary>
        public Order ReinstateOrder(AomRepository.IAomModel aomDb, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus)
        {
            orderRequest.Order.OrderStatus = OrderStatus.Active;
            orderRequest.Order.OrderPriceLines.ForEach(opl => opl.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(orderRequest.Order.OrderStatus));
            return EditInternalOrder(aomDb, orderRequest, MessageAction.Reinstate, marketStatus);
        }

        /// <summary>
        ///     Attempts to set the specified order into VoidAfterExecuted state if the order was previously Executed
        ///     (e.g. if the deal created from executing an order is itself voided)
        /// </summary>
        public Order VoidOrder(AomRepository.IAomModel aomDb, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus)
        {
            if ((orderRequest.Order.OrderStatus != OrderStatus.Executed) && (orderRequest.Order.OrderStatus != OrderStatus.VoidAfterExecuted))
            {
                throw new BusinessRuleException(string.Format( "Error: request to void order {0} however the order was not Executed (status is {1})",
                    orderRequest.Order.Id, DtoMappingAttribute.GetValueFromEnum(orderRequest.Order.OrderStatus)));
            }
            orderRequest.Order.OrderStatus = OrderStatus.VoidAfterExecuted;
            orderRequest.Order.OrderPriceLines.ForEach(opl => opl.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(orderRequest.Order.OrderStatus));
            return EditInternalOrder(aomDb, orderRequest, MessageAction.Void, marketStatus);
        }

        /// <summary>
        ///     Attempts to execute the specified order
        /// </summary>
        public Order ExecuteOrder(AomRepository.IAomModel aomDb, AomOrderAuthenticationResponse orderRequest,
            out long matchingOrderId, MarketStatus marketStatus)
        {
            Order existingOrder = GetOrderById(aomDb, orderRequest.Order.Id);
            matchingOrderId = -1;

            if (existingOrder == null)
            {
                throw new BusinessRuleException(String.Format("Error: order {0} does not exist.", orderRequest.Order.Id));
            }

            if (existingOrder.OrderStatus == OrderStatus.Executed)
            {
                throw new BusinessRuleException("Order has already been executed.");
            }

            if (orderRequest.Order.ExecutionInfo == null)
            {
                throw new BusinessRuleException("Order message was missing execution information.");
            }
            
            IOrganisation userOrganisation = _userService.GetUsersOrganisation(orderRequest.Message.ClientSessionInfo.UserId);
            
            bool coBrokeringAllowedForProduct = _productService.CoBrokeringAllowed(existingOrder.ProductId);
            if (userOrganisation.OrganisationType == OrganisationType.Brokerage)
            {
                // when executing as broker
                if (existingOrder.BrokerOrganisationId != null && existingOrder.BrokerOrganisationId != userOrganisation.Id)
                {
                    if (!coBrokeringAllowedForProduct || !existingOrder.CoBrokering)
                    {
                        throw new BusinessRuleException("Four-way deals are only supported if the order and product are co-brokerable");
                    }
                }
                if (existingOrder.PrincipalOrganisationId == orderRequest.Order.ExecutionInfo.PrincipalOrganisationId)
                {
                    throw new BusinessRuleException("You cannot execute an order with the same principal on both sides.");
                }
            }
            else
            {
                // when executing as principal
                if (existingOrder.PrincipalOrganisationId == userOrganisation.Id)
                {
                    throw new BusinessRuleException("You cannot execute your own order.");
                }
            }

            existingOrder.LastUpdated = orderRequest.Order.LastUpdated;
            existingOrder.OrderStatus = OrderStatus.Executed;
            existingOrder.OrderPriceLines.ForEach(opl => opl.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(existingOrder.OrderStatus));

            Order resultOfEdit = EditInternalOrder(aomDb, 
                new AomOrderAuthenticationResponse
                {
                    MarketMustBeOpen = orderRequest.MarketMustBeOpen,
                    Message = orderRequest.Message,
                    Order = existingOrder
                },
                MessageAction.Execute,
                marketStatus);

            // We now use existingOrder as the basis for the virtual matching order            
            var brokerageOrganisationOfAggressor = orderRequest.Order.ExecutionInfo.BrokerOrganisationId.HasValue
                ? _organisationService.GetOrganisationById(orderRequest.Order.ExecutionInfo.BrokerOrganisationId.Value) : null;

            var organisationOfAggressor = _organisationService.GetOrganisationById(orderRequest.Order.ExecutionInfo.PrincipalOrganisationId);

            if (brokerageOrganisationOfAggressor != null)
            {
                existingOrder.BrokerOrganisationId = brokerageOrganisationOfAggressor.Id;
                existingOrder.BrokerOrganisationName = brokerageOrganisationOfAggressor.Name;
            }
            else
            {
                existingOrder.CoBrokering = false;
                existingOrder.BrokerOrganisationId = null;
                existingOrder.BrokerOrganisationName = string.Empty;
            }

            existingOrder.PrincipalOrganisationId = organisationOfAggressor.Id;
            existingOrder.PrincipalOrganisationName = organisationOfAggressor.Name;

            if (userOrganisation.OrganisationType == OrganisationType.Brokerage)
            {
                existingOrder.BrokerId = orderRequest.Message.ClientSessionInfo.UserId;
                existingOrder.PrincipalUserId = null;
            }
            else
            {
                existingOrder.BrokerId = null;
                existingOrder.PrincipalUserId = orderRequest.Message.ClientSessionInfo.UserId;
            }

            existingOrder.OrderType = existingOrder.OrderType.SwapMarketSide();
            existingOrder.OrderStatus = OrderStatus.Executed;
            existingOrder.Notes = "Autogenerated virtual match for order: " + existingOrder.Id;
            existingOrder.Id = -1;
            existingOrder.IsVirtual = true;
            
            Order newOrder = NewInternalOrder(aomDb,
                new AomOrderAuthenticationResponse
                {
                    MarketMustBeOpen = orderRequest.MarketMustBeOpen,
                    Message = orderRequest.Message,
                    Order = existingOrder
                },
                MessageAction.Execute,
                marketStatus);

            matchingOrderId = newOrder.Id;
            return resultOfEdit;
        }

        public Deal CreateDealFromOrderExecute(AomRepository.IAomModel aomDb, Order existingOrder, long matchingOrderId)
        {
            var deal = new Deal
            {
                Id = -1,
                InitialOrderId = existingOrder.Id,
                MatchingOrderId = matchingOrderId,
                LastUpdatedUserId = existingOrder.LastUpdatedUserId,
                EnteredByUserId = existingOrder.LastUpdatedUserId,
                Message = existingOrder.Notes,
                DealStatus = DealStatus.Executed,
                Initial = existingOrder,
                Matching = aomDb.Orders.SingleOrDefault(o => o.Id == matchingOrderId).ToEntity(_organisationService, _userService)
            };

            return _dealService.CreateDeal(aomDb, deal);
        }

        public Order GetOrderById(AomRepository.IAomModel db, long orderId)
        {
            AomRepository.OrderDto repositoryOrderDto = (from o in db.Orders
                                                         .Include(x => x.ProductTenorDto)
                                                         .Include(x => x.ProductTenorDto.ProductDto)
                                                         .Include(x => x.ProductTenorDto.ProductDto.DeliveryLocations)
                                                         .Include(o => o.OrderPriceLines.Select(opl => opl.PriceLineDto.PriceLinesBases))
                                                         .Include(o => o.OrderPriceLines.Select(opl => opl.PriceLineDto.PriceLinesBases.Select(plb => plb.PriceBasisDto)))
                                                         //.Include(o => o.OrderPriceLines.Select(opl => opl.PriceLineDto.PriceLinesBases.Select(plb => plb.PriceBasisDto.CurrencyDto)))
                                                         //.Include(o => o.OrderPriceLines.Select(opl => opl.PriceLineDto.PriceLinesBases.Select(plb => plb.PriceBasisDto.PriceUnitDto)))
                                                         where orderId == o.Id
                                                         select o).SingleOrDefault();

            if (repositoryOrderDto == null) return null;

            return repositoryOrderDto.ToEntity(_organisationService, _userService);
        }

        public IEnumerable<Order> GetOrdersForLastUpdatedDateByProductId(AomRepository.IAomModel db, DateTime date, long productId)
        {
            IEnumerable<AomRepository.OrderDto> repositoryDtos = (from o in db.Orders.Include(x => x.ProductTenorDto)
                                                                  .Include(x => x.ProductTenorDto.ProductDto)
                                                                  .Include(x => x.ProductTenorDto.ProductDto.DeliveryLocations)
                                                                  where (o.LastUpdated.Year == date.Year && o.LastUpdated.Month == date.Month && o.LastUpdated.Day == date.Day) 
                                                                        && o.ProductTenorDto.ProductId == productId
                                                                  select o).AsEnumerable();

            return repositoryDtos.Select(x => x.ToEntity(_organisationService, _userService)).ToList();
        }

        public IEnumerable<Order> GetOrdersForTodayByProductId(AomRepository.IAomModel db, long productId)
        {
            DateTime dateFrom = DateTime.Today;
            DateTime dateTo = dateFrom.AddDays(1);

            IEnumerable<AomRepository.OrderDto> repositoryDtos = //(from o in db.Orders.AsNoTracking().Include(x => x.ProductTenorDto) //faster wth AsNoTracking
            (from o in db.Orders.AsNoTracking().Include(x => x.ProductTenorDto) //faster wth AsNoTracking
             .Include(x => x.ProductTenorDto.ProductDto).Include(x => x.ProductTenorDto.ProductDto.DeliveryLocations)
                    .Include(x => x.OrderPriceLines.Select(opl => opl.PriceLineDto.PriceLinesBases))
             where (o.LastUpdated >= dateFrom && o.LastUpdated < dateTo) && o.ProductTenorDto.ProductId == productId
             select o).AsEnumerable();

            return repositoryDtos.Select(x => x.ToEntity(_organisationService, _userService)).ToList();
        }

        public IEnumerable<AomRepository.OrderDto> GetOpenOrderDtosForProductId(AomRepository.IAomModel db, long productId)
        {
            string activeStatus = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Active);

            IEnumerable<AomRepository.OrderDto> repositoryDtos = (from o in db.Orders.Include(x => x.ProductTenorDto)
                                                                  .Include(x => x.ProductTenorDto.ProductDto)
                                                                  .Include(x => x.ProductTenorDto.ProductDto.DeliveryLocations)
                                                                  where (o.OrderStatusCode == activeStatus) && o.ProductTenorDto.ProductId == productId
                                                                  select o).AsEnumerable();

            return repositoryDtos.ToList();
        }

        public IEnumerable<AomRepository.OrderDto> GetOpenAndHeldOrderDtosForProductId(AomRepository.IAomModel db, long productId)
        {
            string activeStatus = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Active);
            string heldStatus = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held);

            IEnumerable<AomRepository.OrderDto> repositoryDtos = (from o in db.Orders.Include(x => x.ProductTenorDto)
                                                                  .Include(x => x.ProductTenorDto.ProductDto)
                                                                  .Include(x => x.ProductTenorDto.ProductDto.DeliveryLocations)
                                                                  where (o.OrderStatusCode == activeStatus || o.OrderStatusCode == heldStatus) && o.ProductTenorDto.ProductId == productId
                                                                  select o).AsEnumerable();

            return repositoryDtos.ToList();
        }

        public IEnumerable<Order> GetOpenOrdersInitiatedByUserId(AomRepository.IAomModel db, long userId)
        {
            string activeStatus = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Active);
            string heldStatus = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held);

            IEnumerable<AomRepository.OrderDto> repositoryDtos = (from o in db.Orders.Include(x => x.ProductTenorDto)
                                                                  .Include(x => x.ProductTenorDto.ProductDto)
                                                                  .Include(x => x.ProductTenorDto.ProductDto.DeliveryLocations)
                                                                  where (o.OrderStatusCode == activeStatus || o.OrderStatusCode == heldStatus) && o.EnteredByUserId == userId
                                                                  select o).AsEnumerable();

            return repositoryDtos.Select(x => x.ToEntity(_organisationService, _userService)).ToList();
        }

        public Order CloseMarketHoldOrder(AomRepository.IAomModel aomDb, long userId, AomRepository.OrderDto orderDto)
        {
            orderDto.LastUpdated = DateTime.UtcNow;
            orderDto.LastUpdatedUserId = userId;
            orderDto.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held);

            return orderDto.ToEntity(_organisationService, _userService);
        }

        public Order PurgeOrder(AomRepository.IAomModel aomDb, long userId, AomRepository.OrderDto orderDto)
        {
            orderDto.LastUpdated = DateTime.UtcNow;
            orderDto.LastUpdatedUserId = userId;
            orderDto.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Killed);

            return orderDto.ToEntity(_organisationService, _userService);
        }

        private Boolean ValidateOrder(AomRepository.IAomModel db, MessageAction action, bool marketMustBeOpen, AomRepository.OrderDto newState, MarketStatus marketStatus,
            long productIdForOrder, AomRepository.OrderDto previousState = null)
        {
            if (previousState != null)
            {
                if (previousState.LastUpdated.RoundToMs() != newState.LastUpdated.RoundToMs()) //TODO Precision Issue
                {
                    throw new BusinessRuleException(ConstructAlreadyUpdatedMessage(action, newState, previousState));
                }

                if (previousState.BidOrAsk != newState.BidOrAsk)
                {
                    throw new BusinessRuleException("An order cannot be changed from a Bid to an Ask or vice-versa. You must cancel it and re-enter.");
                }
                if (previousState.OrderStatusCode == DtoMappingAttribute.GetValueFromEnum(OrderStatus.Killed))
                {
                    throw new BusinessRuleException("The order is killed and cannot be amended.");
                }
                if (action == MessageAction.Execute && previousState.OrderStatusCode == DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held))
                {
                    throw new BusinessRuleException("A held order cannot be executed");
                }
            }

            if (newState.CoBrokering && !newState.BrokerOrganisationId.HasValue)
            {
                throw new BusinessRuleException("A co-brokerable order must have a brokerage assigned");
            }

            AomRepository.ProductTenorDto productTenorSelected = GetProductAndTenorSelected(db, newState.ProductTenorId);

            if (productTenorSelected == null)
            {
                throw new BusinessRuleException("The order is linked to an invalid Product Tenor  (Id: " + newState.ProductTenorId + ")");
            }

            if (newState.OrderStatusCode != DtoMappingAttribute.GetValueFromEnum(OrderStatus.Killed))
            {
                // Allow orders with invalid values to be killed
                CheckPrice(productTenorSelected.ProductDto, newState);
                CheckQuantity(productTenorSelected.ProductDto, newState.Quantity);
                CheckDeliveryPeriod(productTenorSelected, newState);
            }

            _metadataValidator.Validate(newState.MetaData, productIdForOrder);
            
            if (marketStatus.Equals(MarketStatus.Closed) && marketMustBeOpen)
            {
                // TODO: This logic should really be using the marketMustBeOpen flag.
                // The idea is that the business logic at a higher level should decide
                // whether the action can still be carried out when the market is closed.
                if ((action != MessageAction.Update && action != MessageAction.Create) || newState.OrderStatusCode != DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held))
                {
                    throw new BusinessRuleException(string.Format("The {0} market is closed", productTenorSelected.ProductDto.Name));
                }
            }

            #region LPG validations
            if (newState.OrderPriceLines == null || !(bool) newState.OrderPriceLines.Any())
            {
                throw new InvalidOperationException("Order must have associated OrderPriceLines populated");
            }
            if (newState.OrderPriceLines.Any(opl => opl.PriceLineDto == null))
            {
                throw new InvalidOperationException("OrderPriceLines must have associated PriceLines populated");
            }
            if (!newState.OrderPriceLines.All(opl => opl.PriceLineDto.PriceLinesBases != null && opl.PriceLineDto.PriceLinesBases.Any()))
            {
                throw new InvalidOperationException("PriceLines must have associated PriceLinesBases populated");
            }

            //TODO: TEmporary validation. Should be removed once we remove obsolete fields from Order table
            if (newState.OrderPriceLines.Any(opl => opl.PriceLineDto.PriceLinesBases.Any(plb => plb.PriceLineBasisValue != newState.Price)))
            {
                throw new BusinessRuleException("PriceLineBasisValue must match price on the order");
            }

            if (newState.OrderStatusCode == DtoMappingAttribute.GetValueFromEnum(OrderStatus.Active) || 
                newState.OrderStatusCode == DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held) || 
                newState.OrderStatusCode == DtoMappingAttribute.GetValueFromEnum(OrderStatus.Killed)
            )
            {
                if (newState.OrderPriceLines.Any(opl => opl.OrderStatusCode != newState.OrderStatusCode))
                {
                    throw new BusinessRuleException(string.Format("When order status is {0}, {1} and {2}, then status on order price lines must match",
                        OrderStatus.Active.ToString(), OrderStatus.Held.ToString(), OrderStatus.Killed.ToString()));
                }
            }
            else if(newState.OrderStatusCode == DtoMappingAttribute.GetValueFromEnum(OrderStatus.Executed) || 
                newState.OrderStatusCode == DtoMappingAttribute.GetValueFromEnum(OrderStatus.VoidAfterExecuted))
            {
                //TODO: which status should "not chosen" order price line have?
                if (newState.OrderPriceLines.Count(opl => opl.OrderStatusCode == newState.OrderStatusCode) > 1)
                {
                    throw new BusinessRuleException("After the order was executed, only one order price line may have a status matching order status");
                }
            }
            else
            {
                throw new NotImplementedException("New Order status is not handled");
            }

            #endregion

            return true;
        }

        private static AomRepository.ProductTenorDto GetProductAndTenorSelected(AomRepository.IAomModel db, long productTenorId)
        {
            return (from productTenor in db.ProductTenors
                    where productTenor.Id == productTenorId
                    select productTenor).Include(pt => pt.ProductDto).FirstOrDefault();
        }

        public string ConstructAlreadyUpdatedMessage(MessageAction action, AomRepository.OrderDto newState, AomRepository.OrderDto previousState)
        {
            string myAction;
            switch (action)
            {
                case MessageAction.Create:
                    myAction = "created";
                    break;
                case MessageAction.Execute:
                    myAction = previousState.BidOrAsk == "B" ? "hit" : "lifted";
                    break;
                case MessageAction.Hold:
                    myAction = "held";
                    break;
                case MessageAction.Kill:
                    myAction = "killed";
                    break;
                case MessageAction.Reinstate:
                    myAction = "reinstated";
                    break;
                case MessageAction.Update:
                    myAction = "amended";
                    break;
                case MessageAction.Void:
                    myAction = "voided";
                    break;
                default:
                    myAction = "**UNKNOWN ACTION**";
                    break;
            }
            string theirAction;
            var actualState = DtoMappingAttribute.FindEnumWithValue<OrderStatus>(previousState.OrderStatusCode);
            //var requestedState = DtoMappingAttribute.FindEnumWithValue<OrderStatus>(newState.OrderStatusCode);
            switch (actualState)
            {
                case OrderStatus.Active:
                    theirAction = "updated";
                    if (DtoMappingAttribute.FindEnumWithValue<OrderStatus>(previousState.LastOrderStatusCode) == OrderStatus.Held)
                    {
                        theirAction = "reinstated";
                    }
                    break;
                case OrderStatus.Executed:
                    theirAction = previousState.BidOrAsk == "B" ? "hit" : "lifted";
                    break;
                case OrderStatus.Held:
                    theirAction = action == MessageAction.Reinstate ? "updated" : "withdrew";
                    break;
                case OrderStatus.Killed:
                    theirAction = "withdrew";
                    break;
                case OrderStatus.VoidAfterExecuted:
                    theirAction = "voided after execution";
                    break;
                default:
                    theirAction = "**UNKNOWN ACTION**";
                    break;
            }

            AomRepository.PriceLineBasisDto priceLineBasis = previousState.OrderPriceLines.First().PriceLineDto.PriceLinesBases.First();

            return "The order (Ref. #{order}: {bidask} {quantity} at {price}) could not be {actioned} as {name} {otheraction} this order at {lastupdate} (UTC)."
                .SafeReplace("{order}", () => newState.Id.ToString(CultureInfo.InvariantCulture))
                //.SafeReplace("{quantity}", () => String.Format("{0:#,##0} {1}", previousState.Quantity, previousState.ProductTenorDto.ProductDto.PriceUnit))
                .SafeReplace("{quantity}", () => String.Format("{0:#,##0} {1}", previousState.Quantity, priceLineBasis.PriceBasisDto.PriceUnitCode))
                //.SafeReplace("{price}", () => String.Format("{0:#,##0.00##} {1}/{2}", previousState.Price, previousState.ProductTenorDto.ProductDto.CurrencyDto.DisplayName, previousState.ProductTenorDto.ProductDto.PriceUnitDto.Name))
                .SafeReplace("{price}", () => String.Format("{0:#,##0.00##} {1}/{2}", previousState.Price, priceLineBasis.PriceBasisDto.CurrencyDto.DisplayName, priceLineBasis.PriceBasisDto.PriceUnitDto.Name))
                .SafeReplace("{actioned}", () => myAction).SafeReplace("{name}", () => _userService.GetUsersOrganisation(previousState.LastUpdatedUserId).Name)
                .SafeReplace("{otheraction}", () => theirAction).SafeReplace("{bidask}", () => previousState.BidOrAsk == "B" ? "Bid" : "Ask")
                .SafeReplace("{lastupdate}", () => previousState.LastUpdated.ToString("HH:mm:ss"));
        }

        private static void CheckPrice(AomRepository.ProductDto product, AomRepository.OrderDto order)
        {
            foreach (AomRepository.OrderPriceLineDto opl in order.OrderPriceLines)
            {
                foreach (AomRepository.PriceLineBasisDto plb in opl.PriceLineDto.PriceLinesBases)
                {
                    if (plb.PriceBasisDto.PriceMaximum < plb.PriceLineBasisValue || plb.PriceBasisDto.PriceMinimum > plb.PriceLineBasisValue || ViolatesSteppingIncrement(plb.PriceBasisDto.PriceIncrement, plb.PriceLineBasisValue))
                        throw new BusinessRuleException("The price is invalid.");
                }
            }
            //if (product.OrderPriceMaximum < price || product.OrderPriceMinimum > price || ViolatesSteppingIncrement(product.OrderPriceIncrement, price))
            //{
                //throw new BusinessRuleException("The price is invalid.");
            //}
        }

        private static void CheckQuantity(AomRepository.ProductDto product, decimal quantity)
        {
            if (product.OrderVolumeMaximum < quantity || quantity < product.OrderVolumeMinimum || ViolatesSteppingIncrement(product.OrderVolumeIncrement, quantity))
            {
                throw new BusinessRuleException("The quantity is invalid.");
            }
        }

        private static bool ViolatesSteppingIncrement(decimal increment, decimal val)
        {
            return (increment > 0) && (val % increment != 0);
        }

        //TODO
        private void CheckDeliveryPeriod(AomRepository.ProductTenorDto productTenor, AomRepository.OrderDto order)
        {
            //// These are local times since .Today is local however these should just be whole days
            //DateTime firstValidDate = OffsetDateByIntervalString(productTenor..DeliveryDateStart, _dateTimeProvider.Today);
            //DateTime lastValidDate = OffsetDateByIntervalString(productTenor.DeliveryDateEnd, _dateTimeProvider.Today);

            //// These dates are UTC since the dates for the DTO are set to UTC 
            //// (the dates sent by the client for the Order are also UTC).
            //TimeSpan deliveryRange = order.DeliveryEndDate.AddDays(1) - order.DeliveryStartDate;

            //// Deal with a range that crosses a daylight savings boundary by rounding the number of days
            //// (e.g. across a DST boundary a 3 day range ends up only being 71 hours ~ 2.96 days)
            //var deliveryRangeDays = Math.Round(deliveryRange.TotalDays, MidpointRounding.AwayFromZero);
            //var orderDeliveryEndDate = new DateTime(order.DeliveryEndDate.Year, order.DeliveryEndDate.Month, order.DeliveryEndDate.Day, 0, 0, 0);

            //// NOTE: LINE BELOW USED TO USE : order.DeliveryEndDate  Now Using - the calculated data - line above.
            //// THIS Just takes the date part from the information - stripping out the time.  
            //if (order.DeliveryStartDate < firstValidDate || orderDeliveryEndDate > lastValidDate || deliveryRangeDays < productTenor.MinimumDeliveryRange)
            //{
            //    throw new BusinessRuleException(String.Format(
            //        "The delivery period is invalid. For this market, it must be between {0:dd-MMM-yy} and {1:dd-MMM-yy} inclusive, with a minumum range of {2} days.",
            //        firstValidDate, lastValidDate, productTenor.MinimumDeliveryRange));
            //}
        }

        private static DateTime OffsetDateByIntervalString(string intervalString, DateTime dateToOffset)
        {
            var t = new TimeSpan();

            var regex = new Regex(@"([+-])(\d+)([Dd])");
            Match match = regex.Match(intervalString);
            if (match.Success)
            {
                int sign = match.Groups[1].Value == "-" ? -1 : 1;
                var size = match.Groups[2].Value.To<int>();
                string unit = match.Groups[3].Value.ToUpper();
                switch (unit)
                {
                    case "D":
                        t = TimeSpan.FromDays(sign * size);
                        break;
                    default:
                        throw new BusinessRuleException(String.Format("ProductTenor delivery start or end interval string is invalid: \"{0}\"", intervalString ?? "null"));
                }
            }
            else
            {
                throw new BusinessRuleException(String.Format("ProductTenor delivery start or end interval string is invalid: \"{0}\"", intervalString ?? "null"));
            }

            return dateToOffset.Add(t);
        }

        private Order NewInternalOrder(AomRepository.IAomModel aomDb, AomOrderAuthenticationResponse orderRequest, MessageAction msgAction, MarketStatus marketStatus)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                //TODO: should arrive populated in JSON
                orderRequest.Order.LastUpdatedUserId = orderRequest.Message.ClientSessionInfo.UserId;

                #region TODO - change after R4.5 to pull in actual PriceBases rather than hardcode

                if (orderRequest.Order.OrderPriceLines == null)
                {
                    var priceLine = new PriceLine()
                    {
                        SequenceNo = 1,
                        LastUpdatedUserId = orderRequest.Order.LastUpdatedUserId,
                    };

                    var priceLineBasis = new PriceLineBasis()
                    {
                        SequenceNo = 1,
                        PriceBasisId = 1,
                        ProductId = GetProductAndTenorSelected(aomDb, orderRequest.Order.ProductTenor.Id).ProductId,
                        TenorCode = "P",
                        Description = orderRequest.Order.Notes,
                        PercentageSplit = 100,
                        PricingPeriodFrom = orderRequest.Order.DeliveryStartDate,
                        PricingPeriodTo = orderRequest.Order.DeliveryEndDate,
                        PriceLineBasisValue = orderRequest.Order.Price,
                        LastUpdatedUserId = orderRequest.Order.LastUpdatedUserId,
                    };

                    priceLine.PriceLinesBases = new List<PriceLineBasis>() { priceLineBasis };

                    var orderPriceLine = new OrderPriceLine()
                    {
                        PriceLine = priceLine,
                        OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(orderRequest.Order.OrderStatus),
                        LastUpdatedUserId = orderRequest.Order.LastUpdatedUserId,
                    };

                    var orderPriceLines = new List<OrderPriceLine>() { orderPriceLine };
                    orderRequest.Order.OrderPriceLines = orderPriceLines;
                }

                #endregion

                AomRepository.OrderDto newOrderDto = orderRequest.Order.ToDto();

                if (marketStatus.Equals(MarketStatus.Closed) && orderRequest.MarketMustBeOpen)
                {
                    newOrderDto.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held);
                    foreach (AomRepository.OrderPriceLineDto orderPriceLineDto in newOrderDto.OrderPriceLines)
                    {
                        orderPriceLineDto.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held);
                    }
                }

                if (ValidateOrder(aomDb, msgAction, orderRequest.MarketMustBeOpen, newOrderDto, marketStatus, orderRequest.Order.ProductId))
                {
                    aomDb.Orders.Add(newOrderDto);
                    //foreach (var orderPriceLine2Insert in newOrderDto.OrderPriceLines)
                    //{
                    //    aomDb.OrderPriceLines.Add(orderPriceLine2Insert); 
                    //}
                    //foreach (var priceLineBasis2Insert in newOrderDto.OrderPriceLines.SelectMany(opl => opl.PriceLineDto.PriceLinesBases))
                    //{
                    //    aomDb.PriceLineBases.Add(priceLineBasis2Insert);
                    //}

                    try
                    {
                        aomDb.SaveChangesAndLog(orderRequest.Message.ClientSessionInfo.UserId);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (DbEntityValidationResult validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (DbValidationError validationError in validationErrors.ValidationErrors)
                            {
                                string message = string.Format("{0}:{1}", validationErrors.Entry.Entity, validationError.ErrorMessage);
                                Log.Error(message);
                            }
                        }
                        throw;
                    }
                    aomDb.SaveChangesAndLog(orderRequest.Message.ClientSessionInfo.UserId);
                    orderRequest.Order.Id = newOrderDto.Id;
                }
                Order order = GetOrderById(aomDb, newOrderDto.Id);
                trans.Complete();
                return order;
            }
        }

        private Order EditInternalOrder(AomRepository.IAomModel aomDb, AomOrderAuthenticationResponse orderRequest, MessageAction msgAction, MarketStatus marketStatus)
        {
            return ConcurrencyHelper.RetryOnConcurrencyException(string.Format("Attempting to update order Id:{0}", orderRequest.Order.Id), 3, () =>
            {
                using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                {
                    AomRepository.OrderDto currentOrderDto = (from o in aomDb.Orders
                                                              .Include(o => o.ProductTenorDto)
                                                              .Include(x => x.ProductTenorDto.ProductDto)
                                                              .Include(x => x.ProductTenorDto.ProductDto.DeliveryLocations)
                                                              .Include(o => o.OrderPriceLines.Select(pl => pl.PriceLineDto.PriceLinesBases))
                                                              //.Include(o => o.OrderPriceLines.Select(opl => opl.OrderStatusDto))
                                                              .Include(o => o.OrderPriceLines.Select(opl => opl.PriceLineDto.PriceLinesBases.Select(plb => plb.PriceBasisDto)))
                                                              //.Include(o => o.OrderPriceLines.Select(opl => opl.PriceLineDto.PriceLinesBases.Select(plb => plb.PriceBasisDto.CurrencyDto)))
                                                              //.Include(o => o.OrderPriceLines.Select(opl => opl.PriceLineDto.PriceLinesBases.Select(plb => plb.PriceBasisDto.PriceUnitDto)))
                                                              where  o.Id == orderRequest.Order.Id
                                                              select o).SingleOrDefault();

                    if (currentOrderDto == null)
                    {
                        throw new BusinessRuleException(String.Format("Error: order {0} does not exist.", orderRequest.Order.Id));
                    }

                    //TODO: remove after R4.5, as child entities will arrive filled from frontend
                    if (orderRequest.Order.OrderPriceLines == null)
                    {
                        orderRequest.Order.OrderPriceLines = currentOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList();
                    }

                    if (orderRequest.Order.OrderPriceLines[0].PriceLine != null && orderRequest.Order.OrderPriceLines[0].PriceLine.PriceLinesBases != null)
                        orderRequest.Order.OrderPriceLines.ForEach(opl => opl.PriceLine.PriceLinesBases.ForEach(plb => plb.PriceLineBasisValue = orderRequest.Order.Price));

                    var updatedOrderDto = orderRequest.Order.ToDto();

                    if (ValidateOrder(aomDb, msgAction, orderRequest.MarketMustBeOpen, updatedOrderDto, marketStatus, orderRequest.Order.ProductId, currentOrderDto))
                    {
                        currentOrderDto.UpdateDto(orderRequest.Order, orderRequest.Message.ClientSessionInfo.OrganisationId);
                        aomDb.SaveChangesAndLog(orderRequest.Message.ClientSessionInfo.UserId);
                    }

                    Order updateResult = GetOrderById(aomDb, updatedOrderDto.Id);
                    trans.Complete();
                    return updateResult;
                }
            });
        }
    }
}