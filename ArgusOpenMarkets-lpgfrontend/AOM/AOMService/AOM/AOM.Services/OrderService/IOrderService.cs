﻿namespace AOM.Services.OrderService
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;
    using AOM.Transport.Events.Orders;
    using System;
    using System.Collections.Generic;

    public interface IOrderService
    {
        Order NewOrder(IAomModel model, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus);

        Order EditOrder(IAomModel model, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus);

        Order HoldOrder(IAomModel model, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus);

        Order KillOrder(IAomModel model, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus);

        Order ExecuteOrder(
            IAomModel model,
            AomOrderAuthenticationResponse orderRequest,
            out long matchingOrderId,
            MarketStatus marketStatus);

        Order ReinstateOrder(IAomModel model, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus);

        Order VoidOrder(IAomModel model, AomOrderAuthenticationResponse orderRequest, MarketStatus marketStatus);

        Order GetOrderById(IAomModel model, long orderId);

        Deal CreateDealFromOrderExecute(IAomModel aomDb, Order existingOrder, long matchingOrderId);

        Order CloseMarketHoldOrder(IAomModel aomDb, long userId, OrderDto orderDto);

        Order PurgeOrder(IAomModel aomDb, long userId, OrderDto orderDto);

        IEnumerable<Order> GetOrdersForLastUpdatedDateByProductId(IAomModel db, DateTime date, long productId);

        IEnumerable<Order> GetOrdersForTodayByProductId(IAomModel db, long productId);

        IEnumerable<OrderDto> GetOpenOrderDtosForProductId(IAomModel db, long productId);

        IEnumerable<OrderDto> GetOpenAndHeldOrderDtosForProductId(IAomModel db, long productId);

        IEnumerable<Order> GetOpenOrdersInitiatedByUserId(IAomModel db, long userId);
    }
}