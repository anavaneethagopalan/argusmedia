﻿namespace AOM.Services.AuthenticationService
{
    public static class ProductPrivileges
    {
        public static class Orders
        {
            public static class Amend
            {
                public const string Own = "Order_Amend_Own";
                public const string OwnOrg = "Order_Amend_OwnOrg";
                public const string Any = "Order_Amend_Any";
                public const string KillOwn = "Order_Amend_Kill";
            }

            public static class Create
            {
                public const string AsPrincipal = "Order_Create_Principal";
                public const string AsBroker = "Order_Create_Broker";
                public const string AsAnyone = "Order_Create_Any";
            }

            public static class Aggress
            {
                public const string AsPrincipal = "Order_Aggress_Principal";
                public const string AsBroker = "Order_Aggress_Broker";

                public static class OwnOrg
                {
                    public const string Principal = "Order_Aggress_Principal_OwnOrg";
                    public const string Broker = "Order_Aggress_Broker_OwnOrg";
                }

                public const string Any = "Order_Aggress_Any";
            }            
        }

        // AOMK-120 
        // Orders being added via the +Bid/+Ask buttons in "Structured AOM Lite" are marked as External
        // by having ExecutionMode.External. Currently the permissioning for external orders is a 
        // little more basic then Internal (regular) orders: there is no differentiation between
        // amending one's own orders and that of someone else in the same organsiation.
        // If this changes then the permissions here should be changed to be more like those of
        // internal orders, although there is no need for Aggress if External Orders remain untradable.
        public static class ExternalOrders
        {
            private const string ExternalOrderPrivilegePrincipal = "ExternalOrder_Create_Principal";
            private const string ExternalOrderPrivilegeBroker = "ExternalOrder_Create_Broker";
            private const string ExternalOrderAmend = "ExternalOrder_Amend";

            public static class Create
            {
                public const string AsPrincipal = ExternalOrderPrivilegePrincipal;
                public const string AsBroker = ExternalOrderPrivilegeBroker;
                // No specific Any privilege, calling code should add principal and broker to list
            }

            public static class Amend
            {
                public const string Own = ExternalOrderAmend;
                public const string OwnOrg = ExternalOrderAmend;
                public const string Any = ExternalOrderAmend;
                public const string KillOwn = ExternalOrderAmend;
            }
        }

        public static class MarketTicker
        {
            public const string Create = "MarketTicker_Create";
            public const string Authenticate = "MarketTicker_Authenticate";
        }

        public static class MarketTickerPlusInfo
        {
            public const string Create = "MarketTicker_PlusInfo_Create";
        }

        public static class ExternalDeal
        {
            public const string Authenticate = "Deal_Authenticate";

            public static class Amend
            {
                public const string Any = "Deal_Amend_All";
            }

            public static class Create
            {
                public const string AsPrincipal = "Deal_Create_Principal";
                public const string AsBroker = "Deal_Create_Broker";
                public const string AsSuperUser = "Deal_Create_All";
            }

            public static class Void
            {
                public const string Any = "Deal_Amend_All";
            }
        }

        public static class Deal
        {
            public const string Authenticate = "Deal_Authenticate";

            public static class Amend
            {
                public const string Any = "Deal_Amend_All";
            }

            public static class Create
            {
                public const string AsPrincipal = "Deal_Create_Principal";
                public const string AsBroker = "Deal_Create_Broker";
                public const string AsSuperUser = "Deal_Create_All";
            }

            public static class Void
            {
                public const string Any = "Deal_Amend_All";
            }
        }

        public static class Product
        {
            public const string ChangeMarketStatus = "Product_Change_Market_Status";
            public const string StackPurge = "Stack_Purge";
            public const string ProductConfigurePurge = "Product_Configure_Purge";
        }

        public static class Assessment
        {
            public const string Create = "Assessment_Create";
        }
    }
}
