﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;

namespace AOM.Services.AuthenticationService
{
    public class MarketInfoAuthenticationService : IMarketInfoAuthenticationService
    {
        private struct NeededPrivileges
        {
            public List<string> ForBroker;
            public List<string> ForPrincipal;
            public List<string> ForSuperUser;
        }

        private readonly IUserService _userService;

        public MarketInfoAuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        public AuthResult AuthenticateMarketInfoRequest<T>(T request) where T : AomMarketInfoAuthenticationRequest
        {
            switch (request.MessageAction)
            {
                case MessageAction.Create:
                    return AuthenticateNewMarketInfoRequest(request.MarketInfo, request.ClientSessionInfo);
                case MessageAction.Request:
                    return AuthenticateGetMarketInfoRequest(request.MarketInfo, request.ClientSessionInfo);
                case MessageAction.Update:
                    return AuthenticateEditMarketInfoRequest(request.MarketInfo, request.ClientSessionInfo);
                case MessageAction.Verify:
                    return AuthenticateVerifyMarketInfoRequest(request.MarketInfo, request.ClientSessionInfo);
                case MessageAction.Void:
                    return AuthenticateVoidMarketInfoRequest(request.MarketInfo, request.ClientSessionInfo);
                default:
                    return AccessDenied("Unknown Request received: " + request.EventName + "/" + request.MessageAction);
            }
        }

        public AuthResult AuthenticateNewMarketInfoRequest(MarketInfo marketInfo, ClientSessionInfo csi)
        {
            var neededPrivileges = GetNeededPrivilegesForNewOrEdit(marketInfo);
            return AuthenticateCommonItems(csi, neededPrivileges);
        }

        public AuthResult AuthenticateGetMarketInfoRequest(MarketInfo marketInfo, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForBroker = new List<string>(),
                ForPrincipal = new List<string>(),
                ForSuperUser = new List<string> { ProductPrivileges.MarketTicker.Authenticate },
            };
                
            return AuthenticateCommonItems(csi, neededPrivileges);
        }

        public AuthResult AuthenticateEditMarketInfoRequest(MarketInfo marketInfo, ClientSessionInfo csi)
        {
            var neededPrivileges = GetNeededPrivilegesForNewOrEdit(marketInfo);
            return AuthenticateCommonItems(csi, neededPrivileges);
        }

        private static NeededPrivileges GetNeededPrivilegesForNewOrEdit(MarketInfo marketInfo)
        {
            var isPlusInfoMessage = marketInfo.MarketInfoType != MarketInfoType.Unspecified;
            if (marketInfo.MarketInfoStatus == MarketInfoStatus.Pending)
            {
                return isPlusInfoMessage ? PrivilegesForPendingPlusInfo : PrivilegesForPendingMarketInfo;
            }
            return PrivilegesToAuthenticate;
        }

        private static readonly NeededPrivileges PrivilegesForPendingMarketInfo = new NeededPrivileges
        {
            ForBroker = new List<string> {ProductPrivileges.MarketTicker.Create},
            ForPrincipal = new List<string> {ProductPrivileges.MarketTicker.Create},
            ForSuperUser = new List<string> {ProductPrivileges.MarketTicker.Authenticate}
        };

        // Both +Info and regular MarketInfo messages share the same Authenticate product privilege
        private static readonly NeededPrivileges PrivilegesForPendingPlusInfo = new NeededPrivileges
        {
            ForBroker = new List<string> { ProductPrivileges.MarketTickerPlusInfo.Create },
            ForPrincipal = new List<string> { ProductPrivileges.MarketTickerPlusInfo.Create },
            ForSuperUser = new List<string> { ProductPrivileges.MarketTicker.Authenticate }
        };

        private static readonly NeededPrivileges PrivilegesToAuthenticate = new NeededPrivileges
        {
            ForBroker = new List<string> { ProductPrivileges.MarketTicker.Authenticate },
            ForPrincipal = new List<string> { ProductPrivileges.MarketTicker.Authenticate },
            ForSuperUser = new List<string> { ProductPrivileges.MarketTicker.Authenticate }
        };

        public AuthResult AuthenticateVerifyMarketInfoRequest(MarketInfo marketInfo, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForBroker = new List<string>(),
                ForPrincipal = new List<string>(),
                ForSuperUser = new List<string> { ProductPrivileges.MarketTicker.Authenticate },
            };

            return AuthenticateCommonItems(csi, neededPrivileges);
        }

        public AuthResult AuthenticateVoidMarketInfoRequest(MarketInfo marketInfo, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForBroker = new List<string>(),
                ForPrincipal = new List<string>(),
                ForSuperUser = new List<string> { ProductPrivileges.MarketTicker.Authenticate },
            };

            return AuthenticateCommonItems(csi, neededPrivileges);
        }

        private AuthResult AuthenticateCommonItems(ClientSessionInfo csi, NeededPrivileges neededPrivileges)
        {
            var currentUser = _userService.GetUserWithPrivileges(csi.UserId);

            if (!currentUser.IsActive)
            {
                return AccessDenied("Current user is not active");
            }

            var productPrivileges = currentUser.ProductPrivileges.FirstOrDefault();

            if (productPrivileges == null)
            {
                return AccessDenied("You are not permissioned to modify market info items");
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser).Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser)
                            .All(p => productPrivileges.Privileges[p])
                };
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal).Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal)
                            .All(p => productPrivileges.Privileges[p])
                };
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker).Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker)
                            .All(p => productPrivileges.Privileges[p])
                };
            }
            return AccessDenied("You are not permissioned to modify market info items");
        }
        private static AuthResult AccessDenied(string errorMessage)
        {
            return new AuthResult
            {
                MarketMustBeOpen = false,
                Allow = false,
                DenyReason = errorMessage
            };
        }

    }
}
