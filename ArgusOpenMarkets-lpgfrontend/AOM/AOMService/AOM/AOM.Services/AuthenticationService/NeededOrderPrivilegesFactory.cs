using System;
using AOM.App.Domain.Entities;

namespace AOM.Services.AuthenticationService
{
    internal class NeededOrderPrivilegesFactory : INeededOrderPrivilegesFactory
    {
        public INeededOrderPrivileges CreatePrivileges(ExecutionMode executionMode)
        {
            switch (executionMode)
            {
                case ExecutionMode.None:
                case ExecutionMode.Internal:
                    return new InternalOrderNeededPrivileges();
                case ExecutionMode.External:
                    return new ExternalOrderNeededPrivileges();
                default:
                    throw new ArgumentOutOfRangeException("executionMode", executionMode, null);
            }
        }
    }
}