﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.Transport.Events;

namespace AOM.Services.AuthenticationService
{
    public interface IValidator<in T> where T: AomRequest
    {
        ValidatorResult IsValid(T request, AuthenticationDataTypes.Role role, IUser currentUserFromCsi);
    }
}



