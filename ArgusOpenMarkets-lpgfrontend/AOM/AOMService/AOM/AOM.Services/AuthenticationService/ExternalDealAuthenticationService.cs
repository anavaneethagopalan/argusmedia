﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.ExternalDeals;

namespace AOM.Services.AuthenticationService
{
    public class ExternalDealAuthenticationService : IExternalDealAuthenticationService
    {
        private struct NeededPrivileges
        {
            public List<string> ForPrincipal;
            public List<string> ForBroker;
            public List<string> ForSuperUser;
        }

        private readonly IUserService _userService;

        public ExternalDealAuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        public AuthResult AuthenticateExternalDealRequest(AomExternalDealAuthenticationRequest request)
        {
            switch (request.MessageAction)
            {
                case MessageAction.Create:
                    return AuthenticateNewExternalDealRequest(request.ExternalDeal, request.ClientSessionInfo);
                case MessageAction.Request:
                    return AuthenticateGetExternalDealRequest(request.ExternalDeal, request.ClientSessionInfo);
                case MessageAction.Void:
                    return AuthenticateVoidExternalDealRequest(request.ExternalDeal, request.ClientSessionInfo);
                case MessageAction.Verify:
                    return AuthenticateVerifyExternalDealRequest(request.ExternalDeal, request.ClientSessionInfo);
                case MessageAction.Update:
                    return AuthenticateEditExternalDealRequest(request.ExternalDeal, request.ClientSessionInfo);
                default:
                    return AccessDenied("Unknown Request received: " + request.EventName + "/" + request.MessageAction);
            }
        }

        public AuthResult AuthenticateNewExternalDealRequest(ExternalDeal externalDeal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForPrincipal = new List<string> {ProductPrivileges.ExternalDeal.Create.AsPrincipal},
                ForBroker = new List<string> {ProductPrivileges.ExternalDeal.Create.AsBroker},
                ForSuperUser = new List<string> {ProductPrivileges.ExternalDeal.Create.AsSuperUser}
            };
            return externalDeal.AsVerified
                ? AuthenticateGetExternalDealRequest(externalDeal, csi)
                : AuthenticateCommonItems(externalDeal.ProductId, csi, neededPrivileges);
        }

        public AuthResult AuthenticateGetExternalDealRequest(ExternalDeal externalDeal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForPrincipal = new List<string>(),
                ForBroker = new List<string>(),
                ForSuperUser =
                    new List<string>
                    {
                        ProductPrivileges.ExternalDeal.Authenticate,
                        ProductPrivileges.ExternalDeal.Amend.Any
                    }
            };

            return AuthenticateCommonItems(externalDeal.ProductId, csi, neededPrivileges);
        }

        public AuthResult AuthenticateEditExternalDealRequest(ExternalDeal externalDeal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForPrincipal = new List<string>(),
                ForBroker = new List<string>(),
                ForSuperUser =
                    new List<string>
                    {
                        ProductPrivileges.ExternalDeal.Authenticate,
                        ProductPrivileges.ExternalDeal.Amend.Any
                    }
            };

            return AuthenticateCommonItems(externalDeal.ProductId, csi, neededPrivileges);
        }

        public AuthResult AuthenticateVerifyExternalDealRequest(ExternalDeal externalDeal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForPrincipal = new List<string>(),
                ForBroker = new List<string>(),
                ForSuperUser = new List<string> {ProductPrivileges.ExternalDeal.Authenticate}
            };

            return AuthenticateCommonItems(externalDeal.ProductId, csi, neededPrivileges);
        }

        public AuthResult AuthenticateVoidExternalDealRequest(ExternalDeal externalDeal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForPrincipal = new List<string>(),
                ForBroker = new List<string>(),
                ForSuperUser = new List<string> {ProductPrivileges.ExternalDeal.Authenticate}
            };

            return AuthenticateCommonItems(externalDeal.ProductId, csi, neededPrivileges);
        }

        private AuthResult AuthenticateCommonItems(long productId, ClientSessionInfo csi,
            NeededPrivileges neededPrivileges)
        {
            var currentUser = _userService.GetUserWithPrivileges(csi.UserId);

            if (!currentUser.IsActive)
            {
                return AccessDenied("Current user is not active");
            }

            var productPrivileges = currentUser.ProductPrivileges.FirstOrDefault(x => x.ProductId == productId);

            if (productPrivileges == null)
            {
                return AccessDenied("You are not permissioned to edit this external deal");
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser).Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser)
                            .All(p => productPrivileges.Privileges[p])
                };
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal).Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal)
                            .All(p => productPrivileges.Privileges[p])
                };
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker).Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker)
                            .All(p => productPrivileges.Privileges[p])
                };
            }
            return AccessDenied("You are not permissioned to edit this external deal");
        }

        private static AuthResult AccessDenied(string errorMessage)
        {
            return new AuthResult
            {
                MarketMustBeOpen = false,
                Allow = false,
                DenyReason = errorMessage
            };
        }

    }
}