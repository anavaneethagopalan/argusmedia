﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.Transport.Events;

namespace AOM.Services.AuthenticationService
{
    public static class ValidatorExtention
    {
        public static IEnumerable<ValidatorResult> IsValidAction<T>(this IEnumerable<IValidator<T>> validators, T request, AuthenticationDataTypes.Role role, IUser currentUser) where T : AomRequest
        {
            var validationResults = validators.Select(validator => validator.IsValid(request, role, currentUser));
            return validationResults;
        }

        public static string GetValidityError(this IEnumerable<ValidatorResult> validatorResults)
        {
            var denyResult = validatorResults.FirstOrDefault(x => x.DenyReason != null);
            return denyResult != null ? denyResult.DenyReason : null;
        }

        public static bool RequiresMarketToBeOpen(this IEnumerable<ValidatorResult> validatorResults)
        {
            return validatorResults.Any(x => x.MarketMustBeOpen);
        }

        public static bool Result(this IEnumerable<ValidatorResult> validatorResults)
        {
            return validatorResults.Any(x => x.Result == ValidatorResultState.Deny);
        }
    }
}