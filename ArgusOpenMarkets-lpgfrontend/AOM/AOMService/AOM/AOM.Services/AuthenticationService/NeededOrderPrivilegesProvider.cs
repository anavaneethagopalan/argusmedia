using AOM.App.Domain.Entities;
using AOM.Transport.Events;

namespace AOM.Services.AuthenticationService
{
    internal class NeededOrderPrivilegesProvider : INeededOrderPrivilegesProvider
    {
        private readonly INeededOrderPrivilegesFactory _neededOrderPrivilegesFactory;

        public NeededOrderPrivilegesProvider(INeededOrderPrivilegesFactory neededOrderPrivilegesFactory)
        {
            _neededOrderPrivilegesFactory = neededOrderPrivilegesFactory;
        }

        public NeededPrivileges GetRequiredPrivilegesForAction(MessageAction action, ExecutionMode executionMode)
        {
            var privs = _neededOrderPrivilegesFactory.CreatePrivileges(executionMode);
            if (privs == null)
            {
                return null;
            }
            switch (action)
            {
                case MessageAction.Create:
                    return privs.ForCreate;
                case MessageAction.Update:
                    return privs.ForUpdate;
                case MessageAction.Hold:
                    return privs.ForHold;
                case MessageAction.Reinstate:
                    return privs.ForReinstate;
                case MessageAction.Kill:
                    return privs.ForKill;
                case MessageAction.Execute:
                    return privs.ForExecute;
                default:
                    return null;
            }
        }
    }
}