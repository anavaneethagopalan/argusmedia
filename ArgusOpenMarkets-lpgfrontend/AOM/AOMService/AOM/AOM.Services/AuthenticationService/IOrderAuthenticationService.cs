﻿using AOM.Transport.Events.Orders;

namespace AOM.Services.AuthenticationService
{
    public interface IOrderAuthenticationService
    {
        AuthResult AuthenticateOrderRequest<T>(T authenticationRequest) where T : AomOrderAuthenticationRequest;
    }
}
