﻿using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.BrokerPermissions;
using System.Linq;

namespace AOM.Services.AuthenticationService
{
    public class BrokerPermissionAuthenticationService : IBrokerPermissionAuthenticationService
    {
        private readonly IUserService _userService;
        private static readonly string[] RequiredUpdatePrivileges = { SystemPrivileges.BrokerPermissions.Amend.Own };
        private const string UserNotSpecifiedMessage = "No user was specified for the connection";
        private const string UserNotActiveMessage = "Current user is not active";
        private const string UserNotPermissionedMessage = "You are not permissioned to edit BrokerPermissions";

        public BrokerPermissionAuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        public AuthResult AuthenticateBrokerPermissionRequest(AomBrokerPermissionAuthenticationRequest request)
        {
            AuthResult authResult = null;

            foreach (var bpReq in request.BrokerPermissions)
            {
                if (request.ClientSessionInfo.OrganisationId != bpReq.OurOrganisation)
                {
                    return RequestDenied("Unknown Request received: " + request.EventName + "/" + request.MessageAction);
                }

                
                switch (request.MessageAction)
                {
                    case MessageAction.Update:
                        authResult = AuthenticateUpdateBrokerPermissionRequest(request.ClientSessionInfo);
                        if (!authResult.Allow)
                        {
                            return authResult;
                        }
                        break;

                    case MessageAction.Refresh:
                        authResult = AuthenticateRefreshBrokerPermissionRequest(request.ClientSessionInfo);
                        if (!authResult.Allow)
                        {
                            return authResult;
                        }
                        break;

                    default:
                        // Uknown request?
                        return RequestDenied("Unknown Request received: " + request.EventName + "/" + request.MessageAction);
                }
            }

            return authResult;
        }

        private AuthResult AuthenticateUpdateBrokerPermissionRequest(IClientSessionInfo csi)
        {
            return AuthoriseCommon(csi, RequiredUpdatePrivileges);
        }

        private AuthResult AuthenticateRefreshBrokerPermissionRequest(IClientSessionInfo csi)
        {
            return AuthoriseCommon(csi, RequiredUpdatePrivileges);
        }

        private AuthResult AuthoriseCommon(IClientSessionInfo csi, string[] requiredPrivileges)
        {
            if (csi == null)
            {
                return RequestDenied(UserNotSpecifiedMessage);
            }

            var currentUser = _userService.GetUserWithPrivileges(csi.UserId);
            if (!currentUser.IsActive)
            {
                return RequestDenied(UserNotActiveMessage);
            }

            var systemPrivileges = currentUser.SystemPrivileges;
            if (systemPrivileges == null)
            {
                return RequestDenied(UserNotPermissionedMessage);
            }

            var privilegesIntersect = systemPrivileges.Privileges.Intersect(requiredPrivileges).ToList();
            if (privilegesIntersect.Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen = false
                };
            }

            return RequestDenied(UserNotPermissionedMessage);
        }

        private static AuthResult RequestDenied(string errorMessage)
        {
            return new AuthResult
            {
                MarketMustBeOpen = false,
                Allow = false,
                DenyReason = errorMessage
            };
        }

    }
}
