﻿using AOM.Transport.Events.CounterpartyPermissions;

namespace AOM.Services.AuthenticationService
{
    public interface ICounterpartyPermissionAuthenticationService
    {
        AuthResult AuthenticateCounterpartyPermissionRequest(AomCounterpartyPermissionAuthenticationRequest request);
    }
}
