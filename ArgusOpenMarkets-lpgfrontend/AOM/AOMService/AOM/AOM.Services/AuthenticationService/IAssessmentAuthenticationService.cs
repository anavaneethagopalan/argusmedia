﻿using AOM.Transport.Events.Assessments;

namespace AOM.Services.AuthenticationService
{
    public interface IAssessmentAuthenticationService
    {
        AuthResult AuthenticateAssessmentRequest(AomAssessmentAuthenticationRequest request);
    }
}
