﻿using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using System.Collections.Generic;
using System.Linq;

namespace AOM.Services.AuthenticationService
{
    public class OrderAuthenticationService : IOrderAuthenticationService
    {
        private readonly IUserService _userService;
        private readonly INeededOrderPrivilegesProvider _neededOrderPrivilegesProvider;
        private readonly IEnumerable<IValidator<AomOrderAuthenticationRequest>> _validators;

        public OrderAuthenticationService(IUserService userService,
            INeededOrderPrivilegesProvider neededOrderPrivilegesProvider,
            IEnumerable<IValidator<AomOrderAuthenticationRequest>> validators)
        {
            _userService = userService;
            _neededOrderPrivilegesProvider = neededOrderPrivilegesProvider;
            _validators = validators.ToList();
        }

        public AuthResult AuthenticateOrderRequest<T>(T request) where T : AomOrderAuthenticationRequest
        {
            var currentUser = _userService.GetUserWithPrivileges(request.ClientSessionInfo.UserId);
            var role = DetermineRole(request, currentUser);
            var results = _validators.IsValidAction(request, role, currentUser).ToList();
            return results.Result()
                ? AccessDenied(results.GetValidityError())
                : AccessAllowed(results.RequiresMarketToBeOpen());
        }

        internal AuthenticationDataTypes.Role DetermineRole(
            AomOrderAuthenticationRequest request,
            IUser currentUser)
        {
            if (currentUser == null)
            {
                return AuthenticationDataTypes.Role.None;
            }
            if (!currentUser.IsActive)
            {
                return AuthenticationDataTypes.Role.Inactive;
            }

            var order = request.Order;
            var productPrivileges = currentUser.ProductPrivileges.FirstOrDefault(x => x.ProductId == order.ProductId);

            var neededPrivileges = _neededOrderPrivilegesProvider.GetRequiredPrivilegesForAction(request.MessageAction,
                order.ExecutionMode);
            if (productPrivileges == null || neededPrivileges == null)
            {
                return AuthenticationDataTypes.Role.None;
            }

            var role = AuthenticationDataTypes.Role.None;

            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser).Any())
            {
                role = AuthenticationDataTypes.Role.Super;
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal).Any()
                &&
                (order.PrincipalUserId == currentUser.Id &&
                 order.PrincipalOrganisationId == currentUser.UserOrganisation.Id))
            {
                role = AuthenticationDataTypes.Role.Principal;
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipalOrg).Any()
                && order.PrincipalOrganisationId == currentUser.UserOrganisation.Id)
            {
                role = AuthenticationDataTypes.Role.Principal;
            }

            //we are in the principal organisation named as executing/aggressing the order
            if (order.ExecutionInfo != null &&
                productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal).Any())
            {
                if (order.ExecutionInfo.PrincipalOrganisationId == currentUser.OrganisationId)
                {
                    role = AuthenticationDataTypes.Role.Principal;
                }
            }

            //we are broker executing/aggressing an order
            if (request.MessageAction == MessageAction.Execute
                && productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker).Any())
            {
                role = AuthenticationDataTypes.Role.Broker;
            }

            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker).Any()
                && order.BrokerId == currentUser.Id && order.BrokerOrganisationId == currentUser.UserOrganisation.Id)
            {
                role = AuthenticationDataTypes.Role.Broker;
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBrokerOrg).Any()
                && order.BrokerOrganisationId == currentUser.UserOrganisation.Id)
            {
                role = AuthenticationDataTypes.Role.Broker;
            }
            return role;
        }

        private static AuthResult AccessAllowed(bool marketMustBeOpen)
        {
            return new AuthResult {Allow = true, MarketMustBeOpen = marketMustBeOpen};
        }

        private static AuthResult AccessDenied(string errorMessage)
        {
            return new AuthResult {MarketMustBeOpen = false, Allow = false, DenyReason = errorMessage};
        }
    }
}