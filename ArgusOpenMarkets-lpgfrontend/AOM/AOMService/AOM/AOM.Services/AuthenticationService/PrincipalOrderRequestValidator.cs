﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.Repository.MySql;
using AOM.Services.OrderService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Services.AuthenticationService
{
    public class PrincipalOrderRequestValidator : IValidator<AomOrderAuthenticationRequest>
    {
        private readonly IOrderService _orderService;
        private readonly IDbContextFactory _context;

        public PrincipalOrderRequestValidator(IOrderService orderService, IDbContextFactory context)
        {
            _orderService = orderService;
            _context = context;
        }

        public ValidatorResult IsValid(AomOrderAuthenticationRequest request, AuthenticationDataTypes.Role role, IUser currentUserFromCsi)
        {
            if (role != AuthenticationDataTypes.Role.Principal)
            {
                return ValidatorResult.Abstain();
            }
         
            if (request.MessageAction == MessageAction.Execute)
            {               
                if (request.Order.ExecutionInfo.PrincipalOrganisationId != request.ClientSessionInfo.OrganisationId)
                {
                    Log.Warn("Order recieved with security error: CSI ORG DOES NOT MATCH EXECUTOR PRINCIPAL ORG " + request.ToJsonCamelCase());
                    return DenyAction(request);
                }             
            }
            else
            {      
                if (request.Order.PrincipalOrganisationId != request.ClientSessionInfo.OrganisationId)
                {
                    Log.Warn("Order received with security error: CSI ORG DOES NOT MATCH PRINCIPAL ORG " + request.ToJsonCamelCase());
                    return DenyAction(request);
                }  
                              
                if (request.MessageAction != MessageAction.Execute && request.MessageAction != MessageAction.Create)
                {   
                    //exists to ensure we use the organisation in the database and dont' trust the message
                    var principalOrgOnOrder = _orderService.GetOrderById(_context.CreateAomModel(), request.Order.Id).PrincipalOrganisationId;
                    var requestorsOrgSameAsOrderOrg = request.ClientSessionInfo.OrganisationId == principalOrgOnOrder;
                    if (!requestorsOrgSameAsOrderOrg)
                    {
                        Log.Warn("Order received with security error: CSI USER IS NOT IN PRINCIPAL ORG (db)" + request.ToJsonCamelCase());
                        return DenyAction(request);
                    }
                }                 
            }
            return ValidatorResult.Allow();
        }

        private static ValidatorResult DenyAction(AomRequest request)
        {
            return ValidatorResult.Deny(string.Format("You are not authorised to {0} this order", request.MessageAction));
        }

    }
}