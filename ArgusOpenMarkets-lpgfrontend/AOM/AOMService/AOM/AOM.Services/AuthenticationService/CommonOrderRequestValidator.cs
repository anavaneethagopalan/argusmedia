using System;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Services.AuthenticationService
{
    public class CommonOrderRequestValidator : IValidator<AomOrderAuthenticationRequest>
    {
        private readonly IOrganisationService _organisationService;
        private readonly INeededOrderPrivilegesProvider _neededOrderPrivilegesProvider;

        public CommonOrderRequestValidator(IOrganisationService organisationService,
            INeededOrderPrivilegesProvider neededOrderPrivilegesProvider)
        {
            _organisationService = organisationService;
            _neededOrderPrivilegesProvider = neededOrderPrivilegesProvider;
        }

        private string GetOrganisationName(long organisationId)
        {
            var org = _organisationService.GetOrganisationById(organisationId);
            if (org == null)
            {
                throw new Exception("Error: Unknown organisation id of " + organisationId);
            }

            return org.Name;
        }

        public ValidatorResult IsValid(AomOrderAuthenticationRequest orderRequest, AuthenticationDataTypes.Role role,
            IUser currentUserFromCsi)
        {
            var order = orderRequest.Order;
            var productPrivileges =
                currentUserFromCsi.ProductPrivileges.FirstOrDefault(x => x.ProductId == order.ProductId);
            var messageAction = orderRequest.MessageAction;
            var neededPrivileges = _neededOrderPrivilegesProvider.GetRequiredPrivilegesForAction(messageAction,
                order.ExecutionMode);

            if (currentUserFromCsi.OrganisationId != orderRequest.ClientSessionInfo.OrganisationId)
            {
                Log.ErrorFormat("The CSI organisation [{0}] does not match the users actual organisation [{1}]",
                    currentUserFromCsi.OrganisationId, orderRequest.ClientSessionInfo.OrganisationId);
                return
                    ValidatorResult.Deny(
                        string.Format("You are not authorised to {0} this order due to organisation change",
                            messageAction));
            }

            if (productPrivileges == null || neededPrivileges == null)
            {
                Log.WarnFormat(
                    "Null privileges for AomOrderAuthenticationRequest validation: pp null? {0}, np null? {1}, {2}",
                    productPrivileges == null,
                    neededPrivileges == null,
                    orderRequest);
                return ValidatorResult.Deny(string.Format("You are not authorised to {0} this order", messageAction));
            }

            if (orderRequest.MessageAction == MessageAction.Execute && orderRequest.Order.ExecutionInfo == null)
            {
                Log.WarnFormat("Null ExecutionInfo for AomOrderAuthenticationRequest validation: {0}",
                    orderRequest.ToJsonCamelCase());
                return ValidatorResult.Deny(string.Format("You are not authorised to {0} this order", messageAction));
            }

            bool marketMustBeOpen;

            switch (role)
            {
                case AuthenticationDataTypes.Role.Inactive:
                    return ValidatorResult.Deny("Current user is not active");

                case AuthenticationDataTypes.Role.None:
                    return ValidatorResult.Deny(String.Format("You are not authorised to {0} this order", messageAction));

                case AuthenticationDataTypes.Role.Super:
                    marketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser)
                            .All(p => productPrivileges.Privileges[p]);
                    break;

                case AuthenticationDataTypes.Role.Principal:
                    marketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal)
                            .All(p => productPrivileges.Privileges[p]);
                    break;

                case AuthenticationDataTypes.Role.Broker:
                    marketMustBeOpen =
                        productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker)
                            .All(p => productPrivileges.Privileges[p]);
                    break;

                default:
                    return ValidatorResult.Deny(String.Format("You are not authorised to {0} this order", messageAction));
            }

            return AuthenticateOrderPermissions(
                order,
                marketMustBeOpen,
                messageAction);
        }

        public bool CheckCounterpartyAndBrokerPermissions(
            Order order,
            out string errorReason)
        {


            errorReason = "";
            long? agressingOrganisationId = order.ExecutionInfo != null
                ? order.ExecutionInfo.PrincipalOrganisationId
                : (long?) null;
            long? agressingBrokerId = order.ExecutionInfo != null
                ? order.ExecutionInfo.BrokerOrganisationId
                : (long?) null;

            long productId = order.ProductId;
            BuyOrSell buyOrSell = order.OrderType == OrderType.Ask ? BuyOrSell.Sell : BuyOrSell.Buy;
            long cp1OrganisationId = order.PrincipalOrganisationId;
            long? brokerOrganisationId = order.BrokerOrganisationId;

            if (agressingOrganisationId.HasValue)
            {
                // check CP1 <==> agressingCP permissions
                if (
                    !CounterPartiesCanTradeProducts(productId, cp1OrganisationId, buyOrSell,
                        agressingOrganisationId.Value, out errorReason)) return false;
            }

            if (agressingBrokerId.HasValue)
            {
                // check CP1 <==> agressingBROKER permissions
                if (
                    !BrokerCanTradeProductForCountperparty(productId, cp1OrganisationId, buyOrSell,
                        agressingBrokerId.Value, ref errorReason)) return false;
            }

            if (brokerOrganisationId.HasValue)
            {
                // check CP1 <==> BROKER permissions
                if (
                    !BrokerCanTradeProductForCountperparty(productId, cp1OrganisationId, buyOrSell,
                        brokerOrganisationId.Value, ref errorReason)) return false;
            }

            if (agressingOrganisationId.HasValue && agressingBrokerId.HasValue)
            {
                //check agressingCP <==> aggressingBROKER
                var reverseBuyOrSell = buyOrSell.ReverseValue();
                if (
                    !BrokerCanTradeProductForCountperparty(productId, agressingBrokerId.Value, reverseBuyOrSell,
                        agressingOrganisationId.Value, ref errorReason)) return false;
            }

            if (brokerOrganisationId.HasValue && agressingOrganisationId.HasValue)
            {
                // check BROKER <==> agressingCP permissions
                var reverseBuyOrSell = buyOrSell.ReverseValue();
                if (
                    !BrokerCanTradeProductForCountperparty(productId, agressingOrganisationId.Value, reverseBuyOrSell,
                        brokerOrganisationId.Value, ref errorReason)) return false;
            }

            return true;
        }

        private bool BrokerCanTradeProductForCountperparty(long productId, long cp1OrganisationId, BuyOrSell buyOrSell,
            long brokerOrganisationId, ref string errorReason)
        {
            if (
                !_organisationService.CanCounterpartyTradeWithBroker(
                    productId,
                    buyOrSell,
                    cp1OrganisationId,
                    brokerOrganisationId))
            {
                errorReason =
                    String.Format(
                        "There is no bilateral agreement in place for {0} to {1} {2} for this product",
                        GetOrganisationName(cp1OrganisationId),
                        buyOrSell == BuyOrSell.Buy ? "buy through" : "sell through",
                        GetOrganisationName(brokerOrganisationId));
                return false;
            }
            return true;
        }


        private bool CounterPartiesCanTradeProducts(long productId, long cp1OrganisationId, BuyOrSell buyOrSell,
            long cp2OrganisationId, out string errorReason)
        {
            errorReason = null;
            if (
                !_organisationService.CanCounterpartiesTrade(
                    productId,
                    buyOrSell,
                    cp1OrganisationId,
                    cp2OrganisationId))
            {
                errorReason =
                    String.Format(
                        "There is no bilateral agreement in place for {0} to {1} {2} for this product",
                        GetOrganisationName(cp1OrganisationId),
                        buyOrSell == BuyOrSell.Buy ? "buy from" : "sell to",
                        GetOrganisationName(cp2OrganisationId));
                return false;
            }

            return true;
        }

        public ValidatorResult AuthenticateOrderPermissions(
            Order order,
            bool marketMustBeOpen,
            MessageAction messageAction)
        {
            if (order.ExecutionMode == ExecutionMode.Internal && messageAction != MessageAction.Kill)
            {
                string errorReason;
                if (
                    !CheckCounterpartyAndBrokerPermissions(
                        order,
                        out errorReason))
                {
                    return ValidatorResult.Deny(errorReason);
                }
            }
            return ValidatorResult.Allow(marketMustBeOpen);
        }
    }
}