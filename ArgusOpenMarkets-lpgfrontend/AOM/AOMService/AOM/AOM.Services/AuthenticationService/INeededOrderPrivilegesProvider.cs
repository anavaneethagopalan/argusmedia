﻿using AOM.App.Domain.Entities;
using AOM.Transport.Events;

namespace AOM.Services.AuthenticationService
{
    public interface INeededOrderPrivilegesProvider
    {
        NeededPrivileges GetRequiredPrivilegesForAction(MessageAction action, ExecutionMode executionMode);
    }
}
