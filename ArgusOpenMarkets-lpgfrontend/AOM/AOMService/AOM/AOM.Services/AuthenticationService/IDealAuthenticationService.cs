﻿using AOM.Transport.Events.Deals;

namespace AOM.Services.AuthenticationService
{
    public interface IDealAuthenticationService
    {
        AuthResult AuthenticateDealRequest(AomDealAuthenticationRequest authenticationRequest);
    }
}