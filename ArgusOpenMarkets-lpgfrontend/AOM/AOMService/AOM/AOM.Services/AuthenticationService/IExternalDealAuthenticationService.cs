﻿namespace AOM.Services.AuthenticationService
{
    using AOM.Transport.Events.ExternalDeals;

    public interface IExternalDealAuthenticationService
    {
        AuthResult AuthenticateExternalDealRequest(AomExternalDealAuthenticationRequest authenticationRequest);
    }
}