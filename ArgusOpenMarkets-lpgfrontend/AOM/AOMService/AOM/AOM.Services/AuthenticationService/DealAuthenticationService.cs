﻿namespace AOM.Services.AuthenticationService
{
    using System.Collections.Generic;
    using System.Linq;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Deals;

    public class DealAuthenticationService : IDealAuthenticationService
    {
        private struct NeededPrivileges
        {
            public List<string> ForPrincipal;

            public List<string> ForBroker;

            public List<string> ForSuperUser;
        }

        private readonly IUserService _userService;

        public DealAuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        public AuthResult AuthenticateDealRequest(AomDealAuthenticationRequest request)
        {
            switch (request.MessageAction)
            {
                case MessageAction.Create:
                    return AuthenticateNewDealRequest(request.Deal, request.ClientSessionInfo);
                case MessageAction.Request:
                    return AuthenticateGetDealRequest(request.Deal, request.ClientSessionInfo);
                case MessageAction.Void:
                    return AuthenticateVoidDealRequest(request.Deal, request.ClientSessionInfo);
                case MessageAction.Verify:
                    return AuthenticateVerifyDealRequest(request.Deal, request.ClientSessionInfo);
                case MessageAction.Update:
                    return AuthenticateEditDealRequest(request.Deal, request.ClientSessionInfo);
                default:
                    return AccessDenied("Unknown Request received: " + request.EventName + "/" + request.MessageAction);
            }
        }

        public AuthResult AuthenticateNewDealRequest(Deal deal, ClientSessionInfo csi)
        {
            var authenticateDealPrivileges = new NeededPrivileges
                                   {
                                       ForPrincipal =
                                           new List<string>
                                           {
                                               ProductPrivileges.Deal.Create
                                                   .AsPrincipal
                                           },
                                       ForBroker =
                                           new List<string>
                                           {
                                               ProductPrivileges.Deal.Create
                                                   .AsBroker
                                           },
                                       ForSuperUser =
                                           new List<string>
                                           {
                                               ProductPrivileges.Deal.Create
                                                   .AsSuperUser
                                           }
                                   };

            long productId = 0;
            if (deal != null)
            {
                if (deal.Initial != null)
                {
                    productId = deal.Initial.ProductId;
                }
            }

            return AuthenticateCommonItems(productId, csi, authenticateDealPrivileges);
        }

        public AuthResult AuthenticateGetDealRequest(Deal deal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
                                   {
                                       ForPrincipal = new List<string>(),
                                       ForBroker = new List<string>(),
                                       ForSuperUser =
                                           new List<string>
                                           {
                                               ProductPrivileges.Deal.Authenticate,
                                               ProductPrivileges.Deal.Amend.Any
                                           }
                                   };

            return AuthenticateCommonItems(deal.Initial.ProductId, csi, neededPrivileges);
        }

        public AuthResult AuthenticateEditDealRequest(Deal deal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
                                   {
                                       ForPrincipal = new List<string>(),
                                       ForBroker = new List<string>(),
                                       ForSuperUser =
                                           new List<string>
                                           {
                                               ProductPrivileges.Deal.Authenticate,
                                               ProductPrivileges.Deal.Amend.Any
                                           }
                                   };

            return AuthenticateCommonItems(deal.Initial.ProductId, csi, neededPrivileges);
        }

        public AuthResult AuthenticateVerifyDealRequest(Deal deal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
                                   {
                                       ForPrincipal = new List<string>(),
                                       ForBroker = new List<string>(),
                                       ForSuperUser =
                                           new List<string> { ProductPrivileges.Deal.Authenticate }
                                   };

            return AuthenticateCommonItems(deal.Initial.ProductId, csi, neededPrivileges);
        }

        public AuthResult AuthenticateVoidDealRequest(Deal deal, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
                                   {
                                       ForPrincipal = new List<string>(),
                                       ForBroker = new List<string>(),
                                       ForSuperUser =
                                           new List<string> { ProductPrivileges.Deal.Authenticate }
                                   };

            return AuthenticateCommonItems(deal.Initial.ProductId, csi, neededPrivileges);
        }

        private AuthResult AuthenticateCommonItems(
            long productId,
            ClientSessionInfo csi,
            NeededPrivileges neededPrivileges)
        {
            var currentUser = _userService.GetUserWithPrivileges(csi.UserId);

            if (!currentUser.IsActive)
            {
                return AccessDenied("Current user is not active");
            }

            var productPrivileges = currentUser.ProductPrivileges.FirstOrDefault(x => x.ProductId == productId);

            if (productPrivileges == null)
            {
                return AccessDenied("You are not permissioned to edit this deal");
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser).Any())
            {
                return new AuthResult
                       {
                           Allow = true,
                           MarketMustBeOpen =
                               productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser)
                               .All(p => productPrivileges.Privileges[p])
                       };
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal).Any())
            {
                return new AuthResult
                       {
                           Allow = true,
                           MarketMustBeOpen =
                               productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForPrincipal)
                               .All(p => productPrivileges.Privileges[p])
                       };
            }
            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker).Any())
            {
                return new AuthResult
                       {
                           Allow = true,
                           MarketMustBeOpen =
                               productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForBroker)
                               .All(p => productPrivileges.Privileges[p])
                       };
            }
            return AccessDenied("You are not permissioned to edit this deal");
        }

        private static AuthResult AccessDenied(string errorMessage)
        {
            return new AuthResult { MarketMustBeOpen = false, Allow = false, DenyReason = errorMessage };
        }

    }
}