using AOM.App.Domain.Entities;

namespace AOM.Services.AuthenticationService
{
    public interface INeededOrderPrivilegesFactory
    {
        INeededOrderPrivileges CreatePrivileges(ExecutionMode executionMode);
    }
}