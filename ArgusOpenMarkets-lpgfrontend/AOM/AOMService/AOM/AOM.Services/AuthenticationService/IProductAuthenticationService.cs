﻿using AOM.Transport.Events;
using AOM.Transport.Events.Products;

namespace AOM.Services.AuthenticationService
{
    public interface IProductAuthenticationService
    {
        Message AuthenticateProductRequest<T>(T authenticationRequest) where T : AomProductRequest;
        Message AuthenticateProductTenorRequest<T>(T authenticationRequest) where T : AomProductTenorRequest;
    }
}