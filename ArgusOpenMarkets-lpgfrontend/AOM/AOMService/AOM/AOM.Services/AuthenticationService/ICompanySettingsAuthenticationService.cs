﻿using AOM.Transport.Events;
using AOM.Transport.Events.CompanySettings;

namespace AOM.Services.AuthenticationService
{
    public interface ICompanySettingsAuthenticationService
    {
        AuthResult AuthenticateCompanySettingsPermissionRequest(AomCompanySettingsAuthenticationRpcRequest request);

        AuthResult AuthenticateCompanySettingsPermissionRequest(IClientSessionInfo clientSessionInfo);

        string[] PermissionsToUpdateCompanySettings { get; }
    }
}
