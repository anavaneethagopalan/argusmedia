﻿using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.CompanySettings;
using System.Linq;

namespace AOM.Services.AuthenticationService
{
    public class CompanySettingsAuthenticationService : ICompanySettingsAuthenticationService
    {
        private static readonly string[] RequiredUpdatePrivileges = { SystemPrivileges.CompanySettings.Amend.Own };

        private const string UserNotSpecifiedMessage = "No user was specified for the connection";

        private const string UserNotActiveMessage = "Current user is not active";

        private const string UserNotPermissionedMessage = "You are not permissioned to edit Company Settings";

        private readonly IUserService _userService;

        public CompanySettingsAuthenticationService(IUserService userService)
        {
            _userService = userService;
        }


        public AuthResult AuthenticateCompanySettingsPermissionRequest(IClientSessionInfo clientSessionInfo)
        {
            return AuthoriseCommon(clientSessionInfo, RequiredUpdatePrivileges);
        }

        public AuthResult AuthenticateCompanySettingsPermissionRequest(AomCompanySettingsAuthenticationRpcRequest request)
        {
            return AuthoriseCommon(request.ClientSessionInfo, RequiredUpdatePrivileges);
        }

        private AuthResult AuthoriseCommon(IClientSessionInfo csi,
                                           string[] requiredPrivileges)
        {
            if (csi == null)
            {
                return RequestDenied(UserNotSpecifiedMessage);
            }

            var currentUser = _userService.GetUserWithPrivileges(csi.UserId);
            if (!currentUser.IsActive)
            {
                return RequestDenied(UserNotActiveMessage);
            }

            var systemPrivileges = currentUser.SystemPrivileges;
            if (systemPrivileges == null)
            {
                return RequestDenied(UserNotPermissionedMessage);
            }

            var privilegesIntersect = systemPrivileges.Privileges.Intersect(requiredPrivileges).ToList();
            if (privilegesIntersect.Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen = false
                };
            }

            return RequestDenied(UserNotPermissionedMessage);
        }

        private static AuthResult RequestDenied(string errorMessage)
        {
            return new AuthResult
            {
                MarketMustBeOpen = false,
                Allow = false,
                DenyReason = errorMessage
            };
        }


        public string[] PermissionsToUpdateCompanySettings
        {
            get { return RequiredUpdatePrivileges; }
        }
    }
}
