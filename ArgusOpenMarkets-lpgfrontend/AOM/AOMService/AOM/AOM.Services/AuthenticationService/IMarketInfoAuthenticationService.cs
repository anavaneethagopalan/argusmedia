﻿using AOM.Transport.Events.MarketInfos;

namespace AOM.Services.AuthenticationService
{
    public interface IMarketInfoAuthenticationService
    {
        AuthResult AuthenticateMarketInfoRequest<T>(T authenticationRequest) where T : AomMarketInfoAuthenticationRequest;
    }
}