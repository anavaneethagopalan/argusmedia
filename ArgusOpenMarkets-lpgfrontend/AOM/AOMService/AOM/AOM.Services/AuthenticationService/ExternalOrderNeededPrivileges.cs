using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Services.AuthenticationService
{
    internal class ExternalOrderNeededPrivileges : INeededOrderPrivileges
    {
        private static readonly NeededPrivileges CreateRequest = new NeededPrivileges
        {
            ForPrincipal = new List<string> {ProductPrivileges.ExternalOrders.Create.AsPrincipal},
            ForPrincipalOrg = new List<string>(),
            ForBroker = new List<string> {ProductPrivileges.ExternalOrders.Create.AsBroker},
            ForBrokerOrg = new List<string>(),
            ForSuperUser =
                new List<string>
                {
                    ProductPrivileges.ExternalOrders.Create.AsPrincipal,
                    ProductPrivileges.ExternalOrders.Create.AsBroker
                },
        };

        private static readonly NeededPrivileges EditRequest = new NeededPrivileges
        {
            ForPrincipal = new List<string> {ProductPrivileges.ExternalOrders.Amend.Own},
            ForPrincipalOrg = new List<string> {ProductPrivileges.ExternalOrders.Amend.OwnOrg},
            ForBroker = new List<string> {ProductPrivileges.ExternalOrders.Amend.Own},
            ForBrokerOrg = new List<string> {ProductPrivileges.ExternalOrders.Amend.OwnOrg},
            ForSuperUser = new List<string> {ProductPrivileges.ExternalOrders.Amend.Any}
        };

        public NeededPrivileges ForCreate
        {
            get { return CreateRequest; }
        }

        public NeededPrivileges ForUpdate
        {
            get { return EditRequest; }
        }

        public NeededPrivileges ForHold
        {
            get { return EditRequest; }
        }

        public NeededPrivileges ForReinstate
        {
            get { return EditRequest; }
        }

        public NeededPrivileges ForKill
        {
            get { return EditRequest; }
        }

        public NeededPrivileges ForExecute
        {
            // As it stands External orders are not tradable so return null
            get { return null; }
        }
    }
}