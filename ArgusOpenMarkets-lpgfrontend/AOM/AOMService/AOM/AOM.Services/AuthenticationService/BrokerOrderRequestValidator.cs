﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.OrderService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Services.AuthenticationService
{
    public class BrokerOrderRequestValidator : IValidator<AomOrderAuthenticationRequest>
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly IOrderService _orderService;
        private readonly IDbContextFactory _context;

        public BrokerOrderRequestValidator(IUserService userService, IOrganisationService organisationService, IOrderService orderService, IDbContextFactory context)
        {
            _userService = userService;
            _organisationService = organisationService;
            _orderService = orderService;
            _context = context;
        }

        public ValidatorResult IsValid(AomOrderAuthenticationRequest request, AuthenticationDataTypes.Role role, IUser currentUser)
        {
            if (role != AuthenticationDataTypes.Role.Broker)
            {
                return ValidatorResult.Abstain();
            }


            if (request.MessageAction == MessageAction.Create)
            {
                if (request.Order.BrokerId != request.ClientSessionInfo.UserId)
                {
                    Log.Warn("Order received with security error: CSI USER DOES NOT MATCH Broker UserID " + request.ToJsonCamelCase());
                    return DenyAction(request);
                }

                if (!request.Order.BrokerOrganisationId.HasValue)
                {
                    Log.Warn("Order received with security error: CSI USER DOES NOT CONTAIN BROKER ID " + request.ToJsonCamelCase());
                    return DenyAction(request);
                }                               
            }
            
            if (request.Order.BrokerOrganisationId != request.ClientSessionInfo.OrganisationId || request.Order.BrokerOrganisationId != currentUser.OrganisationId)
            {
                if (request.MessageAction != MessageAction.Execute)
                {
                    Log.Warn("Order received with security error: CSI USER DOES NOT MATCH BROKER ORG ID " +
                             request.ToJsonCamelCase());
                    return DenyAction(request);
                }
            }

            return request.Order.ExecutionMode == ExecutionMode.External
                ? IsValidForExternalOrderUpdate(request)
                : IsValidForInternalOrderUpdate(request);
        }

        private ValidatorResult IsValidForInternalOrderUpdate(AomOrderAuthenticationRequest request)
        {
            if (request.MessageAction != MessageAction.Execute && request.MessageAction != MessageAction.Create)
            {
                var requestorsOrg = _userService.GetUsersOrganisation(request.ClientSessionInfo.UserId).Id;
                var order = request.Order;
                var principalOrgOnOrder = _orderService.GetOrderById(_context.CreateAomModel(), request.Order.Id).PrincipalOrganisationId;
                BuyOrSell buyOrSell = order.OrderType == OrderType.Ask ? BuyOrSell.Sell : BuyOrSell.Buy;
                var canActOnBehalfOf = _organisationService.CanCounterpartyTradeWithBroker(order.ProductId, buyOrSell, principalOrgOnOrder, requestorsOrg);
                if (!canActOnBehalfOf)
                {
                    Log.Warn("Order received with security error: CSI USERS ORG CAN NOT ACT ON BEHALF OF THIS PRINCIPAL" + request.ToJsonCamelCase());
                    return DenyAction(request);
                }
            }

            return ValidatorResult.Allow();
        }

        private ValidatorResult IsValidForExternalOrderUpdate(AomOrderAuthenticationRequest request)
        {
            if (request.MessageAction == MessageAction.Execute)
            {
                Log.Info("Attempt to execute external order received: " + request.ToJsonCamelCase());
                return DenyAction(request);
            }
            return ValidatorResult.Allow();
        }

        private static ValidatorResult DenyAction(AomRequest request)
        {
            return ValidatorResult.Deny(string.Format("You are not authorised to {0} this order", request.MessageAction));
        }
    }
}

