﻿namespace AOM.Services.AuthenticationService
{
    public static class AuthenticationDataTypes
    {
        public enum Role
        {
            Super,
            Principal,
            Broker,
            None,
            Inactive
        }

        public enum BrokerOrPrincipal
        {
            B,
            P
        }
    }
}
