﻿using System;

namespace AOM.Services.AuthenticationService
{
    public class AuthResult
    {
        public Boolean Allow { get; set; }
        public Boolean MarketMustBeOpen { get; set; }
        public string DenyReason { get; set; }
    }
}