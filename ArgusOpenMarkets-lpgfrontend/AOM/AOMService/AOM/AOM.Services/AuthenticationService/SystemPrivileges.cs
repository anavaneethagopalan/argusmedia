
namespace AOM.Services.AuthenticationService
{
    public static class SystemPrivileges
    {
        public const string AdminToolUser = "AOMAdminTool";

        public static class BrokerPermissions
        {
            public static class Amend
            {
                public const string Own = "BPMatrix_Update";
            }
            public static class View
            {
                public const string Own = "BPMatrix_View";
            }
        }
        public static class CounterpartyPermissions
        {
            public static class Amend
            {
                public const string Own = "CPMatrix_Update";
            }
            public static class View
            {
                public const string Own = "CPMatrix_View";
            }
        }

        public static class CompanySettings
        {
            public static class Amend
            {
                public const string Own = "CompanySettings_Amend";
            }

            public static class View
            {
                public const string Own = "CompanySettings_View";
            }
        }
    }
}