using AOM.App.Domain.Entities;

namespace AOM.Services.AuthenticationService
{
    public interface INeededOrderPrivileges
    {
        NeededPrivileges ForCreate { get; }
        NeededPrivileges ForUpdate { get; }
        NeededPrivileges ForHold { get; }
        NeededPrivileges ForReinstate { get; }
        NeededPrivileges ForKill { get; }
        NeededPrivileges ForExecute { get; }
    }
}