﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using System.Collections.Generic;
using System.Linq;
using Utils.Logging.Utils;

namespace AOM.Services.AuthenticationService
{
    internal class ProductAuthenticationService : IProductAuthenticationService
    {
        private const int ProcessorUserId = -1;

        private struct NeededPrivileges
        {
            public List<string> ForSuperUser;
        }

        private readonly IUserService _userService;

        private readonly IErrorService _errorService;

        public ProductAuthenticationService(IUserService userService, IErrorService errorService)
        {
            _userService = userService;
            _errorService = errorService;
        }

        public Message AuthenticateProductRequest<T>(T authenticationRequest) where T : AomProductRequest
        {
            switch (authenticationRequest.MessageAction)
            {
                case MessageAction.Open:
                case MessageAction.Close:
                    return AuthenticateMarketStatusRequest(
                        authenticationRequest.Product,
                        authenticationRequest.MessageAction,
                        authenticationRequest.ClientSessionInfo);
                case MessageAction.Update:
                    return AuthenticateProductUpdateRequest(
                        authenticationRequest.Product,
                        authenticationRequest.MessageAction,
                        authenticationRequest.ClientSessionInfo);
                case MessageAction.Purge:
                    return AuthenticatePurgeProductOrdersRequest(
                        authenticationRequest.Product,
                        authenticationRequest.MessageAction,
                        authenticationRequest.ClientSessionInfo);
                case MessageAction.ConfigurePurge:
                    return AuthenticateProductConfigurePurgeDateTimeRequest(
                        authenticationRequest.Product,
                        authenticationRequest.MessageAction,
                        authenticationRequest.ClientSessionInfo);
                default:
                    return ProductErrorMessage(
                        MessageAction.Update,
                        authenticationRequest.ClientSessionInfo,
                        "Unknown Request received: " + authenticationRequest.EventName);
            }
        }

        private Message AuthenticateProductUpdateRequest(
            Product product,
            MessageAction messageAction,
            ClientSessionInfo clientSessionInfo)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForSuperUser =
                    new List<string>
                    {
                        ProductPrivileges.Product
                            .ChangeMarketStatus
                    }
            };

            return AuthenticateCommonItems(product, clientSessionInfo, messageAction, neededPrivileges);
        }

        private Message AuthenticatePurgeProductOrdersRequest(
            Product product,
            MessageAction messageAction,
            ClientSessionInfo clientSessionInfo)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForSuperUser =
                    new List<string>
                    {
                        ProductPrivileges.Product.StackPurge
                    }
            };

            return AuthenticateCommonItems(product, clientSessionInfo, messageAction, neededPrivileges);
        }

        private Message AuthenticateProductConfigurePurgeDateTimeRequest(
            Product product,
            MessageAction messageAction,
            ClientSessionInfo clientSessionInfo)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForSuperUser =
                    new List<string>
                    {
                        ProductPrivileges.Product
                            .ProductConfigurePurge
                    }
            };

            return AuthenticateCommonItems(product, clientSessionInfo, messageAction, neededPrivileges);
        }

        private Message AuthenticateMarketStatusRequest(Product product, MessageAction action, ClientSessionInfo csi)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForSuperUser =
                    new List<string>
                    {
                        ProductPrivileges.Product
                            .ChangeMarketStatus
                    }
            };

            return AuthenticateCommonItems(product, csi, action, neededPrivileges);
        }

        private Message AuthenticateCommonItems(
            Product product,
            ClientSessionInfo csi,
            MessageAction messageAction,
            NeededPrivileges neededPrivileges)
        {
            if (csi == null)
            {
                string msg =
                    string.Format(
                        "Call to AuthenticateCommonItems is missing ClientSessionInfo. Product={0} MessageAction={1}",
                        product.ProductId,
                        messageAction);
                Log.Error(msg);

                var errorMessage = _errorService.LogUserError(new UserError
                {
                    ErrorText = msg,
                    Source = "ProductAuthenticationService.AuthenticateCommonItems"
                });

                return ProductErrorMessage(messageAction, null, errorMessage);
            }

            // Allow the Processor User Carte Blance with Product Operations.
            if (csi.UserId == ProcessorUserId)
            {
                return ProductSuccessMessage(messageAction, MessageType.Product, product, csi);
            }

            var currentUser = _userService.GetUserWithPrivileges(csi.UserId);
            if (currentUser == null)
            {
                string msg =
                    string.Format(
                        "Call to AuthenticateCommonItems has invalid user. Product={0} MessageAction={1} UserId={2}",
                        product.ProductId,
                        messageAction,
                        csi.UserId);
                Log.Error(msg);

                var errorMessage = _errorService.LogUserError(new UserError
                {
                    ErrorText = msg,
                    Source = "ProductAuthenticationService.AuthenticateCommonItems"
                });

                return ProductErrorMessage(messageAction, csi, errorMessage);
            }

            var role = DetermineRole(product, currentUser, neededPrivileges);

            switch (role)
            {
                case AuthenticationDataTypes.Role.Inactive:
                    return ProductErrorMessage(messageAction, csi, "Current user is not active");

                case AuthenticationDataTypes.Role.Super:
                    return ProductSuccessMessage(messageAction, MessageType.Product, product, csi);

                default:
                    Log.Debug(string.Format("Super user check failed for {0} of product {1} by user ID {2}",
                        messageAction,
                        product.ProductId,
                        csi.UserId));
                    return ProductErrorMessage(messageAction, csi,
                        string.Format("You are not a super user whilst attempting: {0} against the Product: {1}",
                            messageAction, product.ProductId));
            }
        }

        private static AuthenticationDataTypes.Role DetermineRole(
            Product product,
            IUser currentUser,
            NeededPrivileges neededPrivileges)
        {
            if (!currentUser.IsActive)
            {
                return AuthenticationDataTypes.Role.Inactive;
            }

            UserProductPrivilege productPrivileges =
                currentUser.ProductPrivileges.FirstOrDefault(x => x.ProductId == product.ProductId);
            if (productPrivileges == null)
            {
                return AuthenticationDataTypes.Role.None;
            }

            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser).Any())
            {
                return AuthenticationDataTypes.Role.Super;
            }

            return AuthenticationDataTypes.Role.None;
        }

        private Message ProductErrorMessage(MessageAction action, ClientSessionInfo csi, string errorMessage)
        {
            return new Message
            {
                MessageAction = action,
                MessageType = MessageType.Error,
                MessageBody = errorMessage,
                ClientSessionInfo = csi
            };
        }

        private Message ProductSuccessMessage(
            MessageAction action,
            MessageType messageType,
            Product product,
            ClientSessionInfo csi)
        {
            return new Message
            {
                MessageAction = action,
                MessageType = messageType,
                MessageBody = product,
                ClientSessionInfo = csi
            };
        }

        // Product Tenor Authentication

        public Message AuthenticateProductTenorRequest<T>(T authenticationRequest) where T : AomProductTenorRequest
        {
            switch (authenticationRequest.MessageAction)
            {
                case MessageAction.Update:
                    return AuthenticateProductConfigurePurgeDateTimeRequest(
                        authenticationRequest.Tenor,
                        authenticationRequest.MessageAction,
                        authenticationRequest.ClientSessionInfo);
                default:
                    return ProductErrorMessage(
                        MessageAction.Update,
                        authenticationRequest.ClientSessionInfo,
                        "Unknown Request received: " + authenticationRequest.EventName);
            }
        }

        private Message AuthenticateProductConfigurePurgeDateTimeRequest(
            ProductTenor tenor,
            MessageAction messageAction,
            ClientSessionInfo clientSessionInfo)
        {
            var neededPrivileges = new NeededPrivileges
            {
                ForSuperUser =
                    new List<string>
                    {
                        ProductPrivileges.Product
                            .ChangeMarketStatus
                    }
            };

            return AuthenticateCommonItems(tenor, clientSessionInfo, messageAction, neededPrivileges);
        }

        private Message AuthenticateCommonItems(
            ProductTenor tenor,
            ClientSessionInfo csi,
            MessageAction messageAction,
            NeededPrivileges neededPrivileges)
        {
            if (csi.UserId == -1)
            {
                // User is super user.
                return ProductTenorSuccessMessage(messageAction, MessageType.Product, tenor, csi);
            }
            else
            {
                var currentUser = _userService.GetUserWithPrivileges(csi.UserId);
                var role = DetermineRole(tenor, currentUser, neededPrivileges);

                switch (role)
                {
                    case AuthenticationDataTypes.Role.Inactive:
                        return ProductErrorMessage(messageAction, csi, "Current user is not active");

                    case AuthenticationDataTypes.Role.Super:
                        return ProductTenorSuccessMessage(messageAction, MessageType.Product, tenor, csi);

                    default:
                        return ProductErrorMessage(messageAction, csi, "You are not a super user");
                }
            }
        }

        private static AuthenticationDataTypes.Role DetermineRole(
            ProductTenor tenor,
            IUser currentUser,
            NeededPrivileges neededPrivileges)
        {
            if (!currentUser.IsActive)
            {
                return AuthenticationDataTypes.Role.Inactive;
            }

            UserProductPrivilege productPrivileges =
                currentUser.ProductPrivileges.FirstOrDefault(x => x.ProductId == tenor.ProductId);
            if (productPrivileges == null)
            {
                return AuthenticationDataTypes.Role.None;
            }

            if (productPrivileges.Privileges.Keys.Intersect(neededPrivileges.ForSuperUser).Any())
            {
                return AuthenticationDataTypes.Role.Super;
            }

            return AuthenticationDataTypes.Role.None;
        }

        private Message ProductTenorSuccessMessage(
            MessageAction action,
            MessageType messageType,
            ProductTenor tenor,
            ClientSessionInfo csi)
        {
            return new Message
            {
                MessageAction = action,
                MessageType = messageType,
                MessageBody = tenor,
                ClientSessionInfo = csi
            };
        }
    }
}