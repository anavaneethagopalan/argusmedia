﻿using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.CounterpartyPermissions;
using System.Linq;

namespace AOM.Services.AuthenticationService
{
    public class CounterpartyPermissionAuthenticationService : ICounterpartyPermissionAuthenticationService
    {
        private readonly IUserService _userService;
        private static readonly string[] RequiredUpdatePrivileges = { SystemPrivileges.CounterpartyPermissions.Amend.Own };
        private const string UserNotSpecifiedMessage = "No user was specified for the connection";
        private const string UserNotActiveMessage = "Current user is not active";
        private const string UserNotPermissionedMessage = "You are not permissioned to edit CounterpartyPermissions";

        public CounterpartyPermissionAuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        public AuthResult AuthenticateCounterpartyPermissionRequest(AomCounterpartyPermissionAuthenticationRequest request)
        {
            AuthResult authenticationResult = null;

            foreach (var cpChange in request.CounterpartyPermissions)
            {
                if (request.ClientSessionInfo.OrganisationId != cpChange.OurOrganisation)
                {
                    return RequestDenied("Unknown Request received: " + request.EventName + "/" + request.MessageAction);
                }

                switch (request.MessageAction)
                {
                    case MessageAction.Update:
                        authenticationResult = AuthenticateUpdateCounterpartyPermissionRequest(request.ClientSessionInfo);
                        break;

                    case MessageAction.Refresh:
                        authenticationResult = AuthenticateRefreshCounterpartyPermissionRequest(request.ClientSessionInfo);
                        break;

                    default:
                        return RequestDenied("Unknown Request received: " + request.EventName + "/" + request.MessageAction);
                }

                if (!authenticationResult.Allow)
                {
                    return authenticationResult;
                }
            }

            return authenticationResult;
        }

        private AuthResult AuthenticateUpdateCounterpartyPermissionRequest(IClientSessionInfo csi)
        {
            return AuthoriseCommon(csi, RequiredUpdatePrivileges);
        }

        private AuthResult AuthenticateRefreshCounterpartyPermissionRequest(IClientSessionInfo csi)
        {
            return AuthoriseCommon(csi, RequiredUpdatePrivileges);
        }

        private AuthResult AuthoriseCommon(IClientSessionInfo csi, string[] requiredPrivileges)
        {
            if (csi == null)
            {
                return RequestDenied(UserNotSpecifiedMessage);
            }

            var currentUser = _userService.GetUserWithPrivileges(csi.UserId);
            if (!currentUser.IsActive)
            {
                return RequestDenied(UserNotActiveMessage);
            }

            var systemPrivileges = currentUser.SystemPrivileges;
            if (systemPrivileges == null)
            {
                return RequestDenied(UserNotPermissionedMessage);
            }

            var privilegesIntersect = systemPrivileges.Privileges.Intersect(requiredPrivileges).ToList();
            if (privilegesIntersect.Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen = false
                };
            }

            return RequestDenied(UserNotPermissionedMessage);
        }

        private static AuthResult RequestDenied(string errorMessage)
        {
            return new AuthResult
            {
                MarketMustBeOpen = false,
                Allow = false,
                DenyReason = errorMessage
            };
        }

    }
}
