﻿using AOM.Transport.Events.BrokerPermissions;

namespace AOM.Services.AuthenticationService
{
    public interface IBrokerPermissionAuthenticationService
    {
        AuthResult AuthenticateBrokerPermissionRequest(AomBrokerPermissionAuthenticationRequest request);
    }
}
