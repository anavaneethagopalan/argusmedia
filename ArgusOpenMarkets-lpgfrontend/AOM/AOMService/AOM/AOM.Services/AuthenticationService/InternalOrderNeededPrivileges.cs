using System.Collections.Generic;
using AOM.App.Domain.Entities;

namespace AOM.Services.AuthenticationService
{
    internal class InternalOrderNeededPrivileges : INeededOrderPrivileges
    {
        private static readonly NeededPrivileges NewOrderRequest = new NeededPrivileges
        {
            ForPrincipal = new List<string> {ProductPrivileges.Orders.Create.AsPrincipal},
            ForPrincipalOrg = new List<string>(),
            ForBroker = new List<string> {ProductPrivileges.Orders.Create.AsBroker},
            ForBrokerOrg = new List<string>(),
            ForSuperUser = new List<string> {ProductPrivileges.Orders.Create.AsAnyone}
        };

        private static readonly NeededPrivileges EditOrderRequest = new NeededPrivileges
        {
            ForPrincipal = new List<string> {ProductPrivileges.Orders.Amend.Own},
            ForPrincipalOrg = new List<string> {ProductPrivileges.Orders.Amend.OwnOrg},
            ForBroker = new List<string> {ProductPrivileges.Orders.Amend.Own},
            ForBrokerOrg = new List<string> {ProductPrivileges.Orders.Amend.OwnOrg},
            ForSuperUser = new List<string> {ProductPrivileges.Orders.Amend.Any}
        };

        private static readonly NeededPrivileges KillOrderRequest = new NeededPrivileges
        {
            ForPrincipal = new List<string>
            {
                ProductPrivileges.Orders.Amend.Own,
                ProductPrivileges.Orders.Amend.KillOwn
            },
            ForPrincipalOrg = new List<string>
            {
                ProductPrivileges.Orders.Amend.OwnOrg
            },
            ForBroker = new List<string>
            {
                ProductPrivileges.Orders.Amend.Own,
                ProductPrivileges.Orders.Amend.KillOwn
            },
            ForBrokerOrg = new List<string>
            {
                ProductPrivileges.Orders.Amend.OwnOrg
            },
            ForSuperUser = new List<string>
            {
                ProductPrivileges.Orders.Amend.Any
            }
        };

        private static readonly NeededPrivileges ExecuteOrderRequest = new NeededPrivileges
        {
            ForPrincipal = new List<string> {ProductPrivileges.Orders.Aggress.AsPrincipal},
            ForPrincipalOrg = new List<string> {ProductPrivileges.Orders.Aggress.OwnOrg.Principal},
            ForBroker = new List<string> {ProductPrivileges.Orders.Aggress.AsBroker},
            ForBrokerOrg = new List<string> {ProductPrivileges.Orders.Aggress.OwnOrg.Broker},
            ForSuperUser = new List<string> {ProductPrivileges.Orders.Aggress.Any}
        };

        public NeededPrivileges ForCreate
        {
            get { return NewOrderRequest; }
        }

        public NeededPrivileges ForUpdate
        {
            get { return EditOrderRequest; }
        }

        public NeededPrivileges ForHold
        {
            get { return EditOrderRequest; }
        }

        public NeededPrivileges ForReinstate
        {
            get { return EditOrderRequest; }
        }

        public NeededPrivileges ForKill
        {
            get { return KillOrderRequest; }
        }

        public NeededPrivileges ForExecute
        {
            get { return ExecuteOrderRequest; }
        }
    }
}