﻿using AOM.App.Domain.Services;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using System.Linq;
using Utils.Logging.Utils;

namespace AOM.Services.AuthenticationService
{
    public class AssessmentAuthenticationService : IAssessmentAuthenticationService
    {
        private const int ProcessorUserId = -1;
        private readonly IUserService _userService;

        public AssessmentAuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        public AuthResult AuthenticateAssessmentRequest(AomAssessmentAuthenticationRequest request)
        {
            if (request.ClientSessionInfo != null)
            {
                if (request.ClientSessionInfo.UserId == ProcessorUserId)
                {
                    Log.InfoFormat("Authentication Assessment Request - Authorized for ProductId: {0}  UserId:{1}", request.ProductId, request.ClientSessionInfo.UserId);
                    return new AuthResult {Allow = true, DenyReason = string.Empty, MarketMustBeOpen = false};
                }
            }

            switch (request.MessageAction)
            {
                case MessageAction.Create:
                    return AuthenticateCreateAssessmentRequest(request.ProductId, request.ClientSessionInfo);
                case MessageAction.Refresh:
                    return AuthenticateRefreshAssessmentRequest(request.ProductId, request.ClientSessionInfo);
                default:
                    return RequestDenied("Unknown Request received: " + request.EventName + "/" + request.MessageAction); 
            }
        }

        private AuthResult AuthenticateCreateAssessmentRequest(long productId, IClientSessionInfo csi)
        {
            return AuthoriseCommon(productId, csi, RequiredCreatePrivileges);
        }

        private AuthResult AuthenticateRefreshAssessmentRequest(long productId, IClientSessionInfo csi)
        {
            return AuthoriseCommon(productId, csi, RequiredRefreshPrivileges);
        }

        private AuthResult AuthoriseCommon(long productId,
                                           IClientSessionInfo csi,
                                           string[] requiredPrivileges)
        {
            if (csi == null)
            {
                Log.InfoFormat("AuthoriseCommon Csi object is null attempting to authorise for productId: {0}", productId);
                return RequestDenied(UserNotSpecifiedMessage);
            }

            Log.InfoFormat("AssessmentAuthenticationService.AuthoriseCommon.  Product Id: {0}  User Id: {1} ", productId, csi.UserId);

            if (csi.UserId == ProcessorUserId)
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen = false,
                    DenyReason =  string.Empty
                };
            }

            var currentUser = _userService.GetUserWithPrivileges(csi.UserId);
            if (!currentUser.IsActive)
            {
                return RequestDenied(UserNotActiveMessage);
            }

            var productPrivileges = currentUser.ProductPrivileges.FirstOrDefault(x => x.ProductId == productId);
            if (productPrivileges == null)
            {
                return RequestDenied(UserNotPermissionedMessage);
            }

            var privilegesIntersect = productPrivileges.Privileges.Keys.Intersect(requiredPrivileges).ToList();
            if (privilegesIntersect.Any())
            {
                return new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen = privilegesIntersect.All(p => productPrivileges.Privileges[p])
                };
            }

            return RequestDenied(UserNotPermissionedMessage);
        }

        private static AuthResult RequestDenied(string errorMessage)
        {
            return new AuthResult
            {
                MarketMustBeOpen = false,
                Allow = false,
                DenyReason = errorMessage
            };
        }

        private static readonly string[] RequiredCreatePrivileges = {ProductPrivileges.Assessment.Create};

        private static readonly string[] RequiredRefreshPrivileges = {ProductPrivileges.Assessment.Create};

        private const string UserNotSpecifiedMessage = "No user was specified for the connection";

        private const string UserNotActiveMessage = "Current user is not active";

        private const string UserNotPermissionedMessage =
            "You are not permissioned to edit Assessments for this product";
    }
}
