using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.Services.MetadataValidator
{
    public interface IMetadataItemValidator
    {
        bool Validates(MetaDataDto orderMetadataItem);
        void Validate(MetaDataDto orderMetadataItem, ProductMetaDataItem associatedProductMetaDataItem);
    }
}