using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.Services.MetadataValidator
{
    public class MetadataCommonValidator : IMetadataItemValidator
    {
        public bool Validates(MetaDataDto orderMetadataItem)
        {
            return true;
        }

        public void Validate(MetaDataDto orderMetadataItem, ProductMetaDataItem associatedProductMetaDataItem)
        {
            if (orderMetadataItem.DisplayName != associatedProductMetaDataItem.DisplayName)
            {
                throw new BusinessRuleException(
                    string.Format("The Display Name on the MetaData \"{0}\" - does not match the Display Name on the MetaDataItem from the DB: {1}",
                        orderMetadataItem.DisplayName, associatedProductMetaDataItem.DisplayName));
            }
        }
    }
}