using System.Linq;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.Services.MetadataValidator
{
    public class MetadataItemEnumValidator : IMetadataItemValidator
    {
        public bool Validates(MetaDataDto orderMetadataItem)
        {
            return (orderMetadataItem.MetaDataType == MetaDataTypes.IntegerEnum);
        }

        public void Validate(MetaDataDto orderMetadataItem, ProductMetaDataItem associatedProductMetaDataItem)
        {
            //check metadatalistitem exists
            var orderMetaDataInteger = (MetaDataIntegerEnumDto)orderMetadataItem;
            var itemEnum = ((ProductMetaDataItemEnum)associatedProductMetaDataItem).MetadataList.FieldList.FirstOrDefault(
                listItem => listItem.Id == orderMetaDataInteger.Id
                            && listItem.ItemValue == orderMetaDataInteger.ItemValue);
            if (itemEnum == null)
            {
                throw new BusinessRuleException(
                    string.Format("There is no valid MetaData list item for Id: {0} and item value: {1}",
                        orderMetaDataInteger.Id, orderMetaDataInteger.ItemValue));
            }
            
            if (orderMetadataItem.DisplayValue != itemEnum.DisplayName)
            {
                throw new BusinessRuleException(
                    string.Format("The metadata display value \"{0}\" does not match displayName on the MetaDataItem from the DB: \"{1}\"",
                        orderMetadataItem.DisplayValue, itemEnum.DisplayName));
            }
        }
    }
}