using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.Services.MetadataValidator
{
    public class MetadataStringValidator : IMetadataItemValidator
    {
        public bool Validates(MetaDataDto orderMetadataItem)
        {
            return (orderMetadataItem.MetaDataType == MetaDataTypes.String);
        }

        public void Validate(MetaDataDto orderMetadataItem, ProductMetaDataItem associatedProductMetaDataItem)
        {
            var itemString = ((ProductMetaDataItemString)associatedProductMetaDataItem);
            var orderMetaDataString = (MetaDataStringDto)orderMetadataItem;

            if (orderMetaDataString.DisplayValue.Length < itemString.ValueMinimum)
            {
                throw new BusinessRuleException(
                    string.Format(
                        "The Product MetaData item length is less than required minimum char length of {0}. Entered value was {1}",
                        itemString.ValueMinimum, orderMetaDataString.DisplayValue));
            }

            if (orderMetaDataString.DisplayValue.Length > itemString.ValueMaximum)
            {
                throw new BusinessRuleException(
                    string.Format(
                        "The Product MetaData item length is greater than required maximum char length of {0}. Entered value was {1}",
                        itemString.ValueMaximum, orderMetaDataString.DisplayValue));
            }
        }
    }
}