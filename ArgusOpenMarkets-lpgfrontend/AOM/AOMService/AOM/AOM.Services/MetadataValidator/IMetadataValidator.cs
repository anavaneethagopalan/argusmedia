using AOM.Repository.MySql.Aom;

namespace AOM.Services.MetadataValidator
{
    public interface IMetadataValidator
    {        
        void Validate(MetaDataDto[] metadata, long productId);
    }
}