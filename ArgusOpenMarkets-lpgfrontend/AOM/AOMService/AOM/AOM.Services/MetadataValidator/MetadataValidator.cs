using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Services.ProductMetadataService;

namespace AOM.Services.MetadataValidator
{
    public class MetadataValidator : IMetadataValidator
    {
        private readonly IProductMetadataService _productMetadataService;
        private readonly IList<IMetadataItemValidator> _validators;

        public MetadataValidator(IProductMetadataService productMetadataService, IList<IMetadataItemValidator> validators)
        {
            _productMetadataService = productMetadataService;
            _validators = validators;
        }

        public void Validate(MetaDataDto[] metadata, long productId)
        {
            if (metadata != null && metadata.Length > 0)
            {
                var allProductMetaDataItems = _productMetadataService.GetAllProductMetaData();// cache for use below

                foreach (MetaDataDto metadataItem in metadata)
                {
                    // Check order product is the same as that on the productmetadataitem
                    var productMetaDataItem = LookupProductMetaDataDefinitionForIdAndProduct(allProductMetaDataItems, metadataItem.ProductMetaDataId, productId);

                    foreach (var validator in _validators)
                    {
                        if (validator.Validates(metadataItem))
                        {
                            validator.Validate(metadataItem, productMetaDataItem);
                        }
                    }                    
                }
            }
        }

        private ProductMetaDataItem LookupProductMetaDataDefinitionForIdAndProduct(IList<ProductMetaData> allProductMetaDataItems, long productMetaDataId, long productId)
        {
            // check metadata item exists for product id
            ProductMetaData metaDataItem = allProductMetaDataItems.FirstOrDefault(p => p.Fields.Any(f => f.Id == productMetaDataId && f.ProductId==productId));

            ProductMetaDataItem productMetaDataItem = null;

            if (metaDataItem != null)
                productMetaDataItem = metaDataItem.Fields.FirstOrDefault(f => f.Id == productMetaDataId);

            if (productMetaDataItem == null)
            {
                throw new BusinessRuleException(string.Format("There is no valid Product MetaData for the item Id: {0}", productMetaDataId));
            }

            return productMetaDataItem;
        }
        
    }
}