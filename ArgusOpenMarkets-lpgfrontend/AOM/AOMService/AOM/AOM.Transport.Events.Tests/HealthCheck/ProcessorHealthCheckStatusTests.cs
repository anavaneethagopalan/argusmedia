﻿namespace AOM.Transport.Events.Tests.HealthCheck
{
    using AOM.Transport.Events.HealthCheck;

    using global::System;

    using NUnit.Framework;

    [TestFixture]
    class ProcessorHealthCheckStatusTests
    {
        [Test]
        public void ShouldReturnGreenForTheProcessorStatusWhenTheResponseTimeIsLessThanTwoSeconds()
        {
            var processorHealthCheckStatus = new ProcessorHealthCheckStatus
            {
                ResponseTime =
                    DateTime.UtcNow.AddMilliseconds(-500)
            };

            Assert.That(processorHealthCheckStatus.ProcessorStatus, Is.EqualTo(ProcessorStatusEnum.Green));
        }

        [Test]
        public void ShouldReturnAmberForTheProcessorStatusWhenTheResponseTimeIsThreeSeconds()
        {
            var processorHealthCheckStatus = new ProcessorHealthCheckStatus
            {

                ResponseTime =
                    DateTime.UtcNow.AddMilliseconds(-40000)
            };

            Assert.That(processorHealthCheckStatus.ProcessorStatus, Is.EqualTo(ProcessorStatusEnum.Amber));
        }

        [Test]
        public void ShouldReturnRedForTheProcessorStatusWhenTheResponseTimeIsEightSeconds()
        {
            var processorHealthCheckStatus = new ProcessorHealthCheckStatus
            {
                ResponseTime =
                    DateTime.UtcNow.AddMilliseconds(-75000)
            };

            Assert.That(processorHealthCheckStatus.ProcessorStatus, Is.EqualTo(ProcessorStatusEnum.Red));
        }
    }
}