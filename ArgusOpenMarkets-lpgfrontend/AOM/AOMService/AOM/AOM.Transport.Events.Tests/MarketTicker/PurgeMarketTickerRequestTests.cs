﻿namespace AOM.Transport.Events.Tests.MarketTicker
{
    using AOM.Transport.Events.MarketTickers;

    using NUnit.Framework;

    [TestFixture]
    public class PurgeMarketTickerRequestTests
    {
        [Test]
        public void ShouldContainAnEventNameOfPurgeMarketTickerRequest()
        {
            var purgeMarketTickerRequest = new PurgeMarketTickerRequest();

            Assert.That(purgeMarketTickerRequest.EventName, Is.EqualTo("PurgeMarketTickerRequest"));
        }
    }
}
