﻿
namespace AOM.Transport.Events.Tests.News
{
    using AOM.Transport.Events.News;

    using NUnit.Framework;

    [TestFixture]
    public class PurgeNewsRequestTests
    {
        [Test]
        public void ShouldReturnEventName()
        {
            var purgeNewsRequest = new PurgeNewsRequest();
            Assert.That(purgeNewsRequest.EventName, Is.EqualTo("PurgeArgusNewsRequest"));
        }
    }
}
