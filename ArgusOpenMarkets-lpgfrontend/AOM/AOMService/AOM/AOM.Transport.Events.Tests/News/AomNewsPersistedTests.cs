﻿namespace AOM.Transport.Events.Tests.MarketTicker.News
{
    using AOM.Transport.Events.News;

    using NUnit.Framework;

    [TestFixture]
    public class AomNewsPersistedTests
    {
        [Test]
        public void ShouldReturnEventName()
        {
            var aomNewsPersisted = new AomNewsPersisted();
            Assert.That(aomNewsPersisted.EventName, Is.EqualTo("AomNewsPersisted"));
        }
    }
}
