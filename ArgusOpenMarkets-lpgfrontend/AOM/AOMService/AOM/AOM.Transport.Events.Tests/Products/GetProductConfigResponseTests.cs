﻿namespace AOM.Transport.Events.Tests.Products
{
    using AOM.Transport.Events.Products;

    using NUnit.Framework;

    [TestFixture]
    class GetProductConfigResponseTests
    {
        [Test]
        public void ShouldReturnEventName()
        {
            var getProductConfigResponse = new GetProductConfigResponse();
            Assert.That(getProductConfigResponse.EventName, Is.EqualTo("GetProductConfigResponse"));
        }
    }
}
