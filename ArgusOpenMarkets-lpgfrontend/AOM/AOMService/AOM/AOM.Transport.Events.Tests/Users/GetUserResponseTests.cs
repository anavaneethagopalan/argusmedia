﻿using AOM.Transport.Events.Users;
using NUnit.Framework;

namespace AOM.Transport.Events.Tests.Users
{
    [TestFixture]
    public class GetUserResponseTests
    {
        [Test]
        public void ShouldReturnEventName()
        {
            var x = new GetUserResponse();
            Assert.That(x.EventName, Is.EqualTo("GetUserResponse"));
        }
    }
}
