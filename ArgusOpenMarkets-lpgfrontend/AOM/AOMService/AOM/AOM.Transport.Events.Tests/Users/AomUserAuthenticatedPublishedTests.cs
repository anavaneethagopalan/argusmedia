﻿using AOM.Transport.Events.Users;
using NUnit.Framework;

namespace AOM.Transport.Events.Tests.Users
{
    [TestFixture]
    public class AomUserAuthenticatedPublishedTests
    {
        [Test]
        public void ShouldSetEventName()
        {
            var x = new AomUserAuthenticatedPublished();
            Assert.That(x.EventName, Is.EqualTo("AomUserAuthenticatedPublished"));
        }
    }
}
