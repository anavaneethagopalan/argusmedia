﻿using AOM.Transport.Events.Users;
using NUnit.Framework;

namespace AOM.Transport.Events.Tests.Users
{
    [TestFixture]
    public class AomUserAuthenticatingPublishedTests
    {
        [Test]
        public void ShouldSetEventName()
        {
            var x = new AomUserAuthenticatingPublished();
            Assert.That(x.EventName, Is.EqualTo("AomUserAuthenticatingPublished"));
        }
    }
}
