﻿using AOM.Transport.Events.Users;
using NUnit.Framework;

namespace AOM.Transport.Events.Tests.Users
{
    [TestFixture]
    public class GetUserRequestTests
    {
        [Test]
        public void ShouldReturnEventName()
        {
            var x = new GetUserRequest();
            Assert.That(x.EventName, Is.EqualTo("GetUserRequest"));
        }
    }
}
