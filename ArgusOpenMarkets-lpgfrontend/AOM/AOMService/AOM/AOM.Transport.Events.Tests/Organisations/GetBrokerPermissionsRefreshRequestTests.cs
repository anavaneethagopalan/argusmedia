﻿namespace AOM.Transport.Events.Tests.Organisations
{
    using AOM.Transport.Events.Organisations;

    using NUnit.Framework;

    [TestFixture]
    public class GetBrokerPermissionsRefreshRequestTests
    {
        [Test]
        public void ShouldReturnAValidEventName()
        {
            var getBrokerPermissionsRefreshRequest = new GetBrokerPermissionsRefreshRequest();
            Assert.That(getBrokerPermissionsRefreshRequest.EventName, Is.EqualTo("GetBrokerPermissionsRefreshRequest"));
        }
    }
}
