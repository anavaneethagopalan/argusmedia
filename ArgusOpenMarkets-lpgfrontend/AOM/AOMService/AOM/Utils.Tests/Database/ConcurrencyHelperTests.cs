﻿using System.Data.Entity.Infrastructure;
using System.IO;
using NUnit.Framework;
using Utils.Database;

namespace Utils.Tests.Database
{
    [TestFixture]
    public class ConcurrencyHelperTests
    {
        [Test]
        public void ConcurrencyHelperWillRetryDbUpdateConcurrencyExceptionTests()
        {
            int cnt = 0;
            ConcurrencyHelper.RetryOnConcurrencyException("Testing", 2, () =>
            {
                cnt=cnt+1;
                if (cnt < 2) throw new DbUpdateConcurrencyException(string.Format("throw for retry attempt {0}", cnt));
                return true;
            } );
        }

        [Test]
        public void ConcurrencyHelperWillRethrowDbUpdateConcurrencyExceptionWhenRetryCountExceeded()
        {
            Assert.Throws<DbUpdateConcurrencyException>(() =>
            {
                ConcurrencyHelper.RetryOnConcurrencyException<bool>("Testing", 10, () =>
                {
                    throw new DbUpdateConcurrencyException("throw for retry attempt");
                });
            });
        }

        [Test]
        public void ConcurrencyHelperWillRethrowAnExceptionThatIsntDbUpdateConcurrencyException()
        {
            Assert.Throws<InvalidDataException>(() =>
            {
                ConcurrencyHelper.RetryOnConcurrencyException<bool>("Testing", 30, () =>
                {
                    throw new InvalidDataException("this exception should be rethrown");
                });
            });
        }
        
    }
}

