﻿using log4net.Core;
using NUnit.Framework;
using System.Linq;
using Utils.Logging;
using Utils.Logging.Utils;

namespace Utils.Tests
{
    [TestFixture]
    public class TestsForMemoryAppenderForTests
    {
        private const string TestText = "Log message for unit tests";

        [Test]
        public void ShouldRecordMessagesWrittenToTheLog()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                const string testText2 = TestText + " 2";
                LogMessage(Level.Debug, TestText);
                Assert.That(testAppender.GetEvents().Length, Is.EqualTo(1));
                LogMessage(Level.Debug, testText2);
                Assert.That(testAppender.GetEvents().Length, Is.EqualTo(2));
                Assert.That(
                    testAppender.GetEvents().Count(e => e.Level == Level.Debug && e.RenderedMessage == TestText),
                    Is.EqualTo(1));
                Assert.That(
                    testAppender.GetEvents().Count(e => e.Level == Level.Debug && e.RenderedMessage == testText2),
                    Is.EqualTo(1));
            }            
        }

        [Test]
        public void ShouldFindMessageWithEqualTextIfItExists()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(
                    testAppender.IsThereALogMessageThatIsEqualTo(Level.Debug, TestText, caseSensitive: false),
                    Is.True);
            }
        }

        [Test]
        public void ShouldFindMessageContainingTextIfItExists()
        {
            const string subText = "message for unit";
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(
                    testAppender.IsThereALogMessageThatContains(Level.Debug, subText, caseSensitive: false),
                    Is.True);
            }
        }

        [Test]
        public void ShouldObeyRequestedCaseSensitivityWhenLookingForMatchingMessages()
        {
            const string toSearch = "log message for unit tests";
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(
                    testAppender.IsThereALogMessageThatIsEqualTo(Level.Debug, toSearch, caseSensitive: false),
                    Is.True);
                Assert.That(
                    testAppender.IsThereALogMessageThatIsEqualTo(Level.Debug, toSearch, caseSensitive: true),
                    Is.False);
            }
        }

        [Test]
        public void ShouldObeyRequestedCaseSensitivityWhenLookingForMessagesContainingText()
        {
            const string toSearch = "log message";
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(
                    testAppender.IsThereALogMessageThatContains(Level.Debug, toSearch, caseSensitive: false),
                    Is.True);
                Assert.That(
                    testAppender.IsThereALogMessageThatContains(Level.Debug, toSearch, caseSensitive: true),
                    Is.False);
            }
        }


        [Test]
        public void ShouldNotFindMessageWithEqualTextIfItDoesNotExist()
        {
            const string toSearch = "Dave's not here";
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(
                    testAppender.IsThereALogMessageThatIsEqualTo(Level.Debug, toSearch, caseSensitive: false),
                    Is.False);
            }
        }

        [Test]
        public void ShouldNotFindMessageContainingTextIfItDoesNotExist()
        {
            const string toSearch = "Dave's not here";
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(
                    testAppender.IsThereALogMessageThatContains(Level.Debug, toSearch, caseSensitive: false),
                    Is.False);
            }
        }

        [Test]
        public void ShouldNotFindMessageIfTextMatchesButLevelIsDifferent()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(
                    testAppender.IsThereALogMessageThatIsEqualTo(Level.Error, TestText, caseSensitive: false),
                    Is.False);
            }
        }

        [Test]
        public void ShouldNotFindMessageIfContainsTextButLevelIsDifferent()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(
                    testAppender.IsThereALogMessageThatIsEqualTo(Level.Error, TestText, caseSensitive: false),
                    Is.False);
            }
        }

        [Test]
        public void ShouldClearLogMessagesWhenAskedTo()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                Assert.That(testAppender.GetEvents().Length, Is.EqualTo(1));
                testAppender.Clear();
                Assert.That(testAppender.GetEvents().Length, Is.EqualTo(0));
            }
        }

        [Test]
        public void ShouldAssertThereAreNoErrorMessagesIfThereAreNone()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                LogMessage(Level.Info, TestText);
                testAppender.AssertThatThereAreNoLogErrors();
            }
        }

        [Test]
        public void ShouldAssertThatALogMessageExistsThatContainsCertainText()
        {
            const string subText = "message for unit";
            using (var testAppender = new MemoryAppenderForTests())
            {
                LogMessage(Level.Debug, TestText);
                testAppender.AssertALogMessageContains(Level.Debug, subText, caseSensitive: false);
            }            
        }

        private static void LogMessage(Level level, string message)
        {
            if (level == Level.Debug)
            {
                Log.Debug(message);
            }
            else if (level == Level.Error)
            {
                Log.Error(message);
            }
            else if (level == Level.Info)
            {
                Log.Info(message);
            }
            else if (level == Level.Warn)
            {
                Log.Warn(message);
            }
            else if (level == Level.Fatal)
            {
                Log.Fatal(message);
            }
            else
            {
                Assert.Fail("Log Level {0} not implemented for these tests", level);
            }
        }
    }
}
