﻿

using NUnit.Framework;
using Utils.Logging;
using log4net.Core;

namespace Utils.Tests.Logging
{
    [TestFixture]
    public class RollingFileAppender4LogstashTests
    {
        [Test]
        public void HandlesSingleNewLineTest()
        {
            var eventData = new LoggingEventData
            {               
                Message = "sss\ntttt"
            };

            var originalEvent = new LoggingEvent(eventData);
            var singleLineClone = RollingFileAppender4Logstash.CloneLoggingEvent(originalEvent);

            Assert.IsTrue(originalEvent.RenderedMessage.Contains("\n"));
            Assert.IsFalse(singleLineClone.RenderedMessage.Contains("\n"));

            Assert.AreEqual("sss *** tttt", singleLineClone.RenderedMessage);
        }

        [Test]
        public void HandlesNewLineAndCarraigeReturnTest()
        {
            var eventData = new LoggingEventData
            {
                Message = "sss\r\ntttt"
            };

            var originalEvent = new LoggingEvent(eventData);
            var singleLineClone = RollingFileAppender4Logstash.CloneLoggingEvent(originalEvent);

            Assert.IsTrue(originalEvent.RenderedMessage.Contains("\n"));
            Assert.IsFalse(singleLineClone.RenderedMessage.Contains("\n"));

            Assert.AreEqual("sss *** tttt", singleLineClone.RenderedMessage);
        }        
    }
}

