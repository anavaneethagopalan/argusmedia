﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOM.Transport.Events;
using NUnit.Framework;
using Utils.Javascript.Extension;

namespace Utils.Tests
{
    [TestFixture]
    public class JsonSerializerTests
    {
        [Test]
        public void JsonSerializerShouldStripCSi()
        {
            var csiName = typeof (ClientSessionInfo).Name;
            var message = new Message()
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = 11,
                    SessionId = "sessionId",
                    UserId = 1
                }
            };
            var serialized = message.ToJsonCamelCase();

            Assert.That(serialized.IndexOf(csiName, StringComparison.OrdinalIgnoreCase) == -1, "message should not contain csi");
        }
            

    }
}
