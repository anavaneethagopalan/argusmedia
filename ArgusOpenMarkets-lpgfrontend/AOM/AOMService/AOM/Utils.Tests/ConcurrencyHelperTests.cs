﻿using System.Linq;
using log4net.Core;
using NUnit.Framework;
using System;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using Utils.Database;
using Utils.Logging;

namespace Utils.Tests
{
    [TestFixture]
    public class ConcurrencyHelperTests
    {
        [Test]
        public void TestSingleAttemptCallsAction()
        {
            var f = CreateFakeAction(0);
            var result = ConcurrencyHelper.RetryOnConcurrencyException("blah", 1, f);
            Assert.AreEqual(TestReturnValue, result);
        }

        [Test]
        public void TestThatASingleFailureWithNoRetryAttemptsThrows()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                const string description = "Doing a database thing";
                var f = CreateFakeAction(1);
                var e = Assert.Throws<DbUpdateConcurrencyException>(
                    () => ConcurrencyHelper.RetryOnConcurrencyException(description, 1, f));
                StringAssert.Contains("test exception", e.Message);
                testAppender.AssertALogMessageContains(Level.Error, description);
                testAppender.AssertALogMessageContains(Level.Error, "Exceeded retry count");                
            }
        }

        [Test]
        public void TestThatASingleFailureIsRetriedSuccessfully()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                var f = CreateFakeAction(1);
                var result = ConcurrencyHelper.RetryOnConcurrencyException("blah", 2, f);
                Assert.AreEqual(TestReturnValue, result);
                testAppender.AssertALogMessageContains(Level.Info, "trying execution again");
                testAppender.AssertALogMessageContains(Level.Info, "1 attempt left");
            }
        }

        [Test]
        public void TestThatMultipleFailuresLessThanTheRetryLimitStillSucceeds()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                const int retryLimit = 10;
                var f = CreateFakeAction(retryLimit - 1);
                var result = ConcurrencyHelper.RetryOnConcurrencyException("blah", retryLimit, f);
                Assert.AreEqual(TestReturnValue, result);
                testAppender.AssertThatThereAreNoLogErrors();
                for (int i = retryLimit - 1; i > 0; --i)
                {
                    testAppender.AssertALogMessageContains(
                        Level.Info,
                        string.Format("{0} {1} left", i, (i == 1 ? "attempt" : "attempts")));
                }
            }

        }

        [Test]
        public void TestThatMultipleFailuresGreaterThanTheRetyLimitThrows()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                const string description = "Doing a database thing";
                const int retryLimit = 10;
                var f = CreateFakeAction(11);
                var e = Assert.Throws<DbUpdateConcurrencyException>(
                    () => ConcurrencyHelper.RetryOnConcurrencyException(description, retryLimit, f));
                StringAssert.Contains("test exception", e.Message);
                StringAssert.Contains(retryLimit.ToString(CultureInfo.InvariantCulture), e.Message);
                testAppender.AssertALogMessageContains(Level.Error, description);
                testAppender.AssertALogMessageContains(
                    Level.Error, string.Format("Exceeded retry count of {0}", retryLimit));
            }
        }

        private const int TestReturnValue = 123456;

        private static Func<int> CreateFakeAction(int numberOfThrows)
        {
            int attempt = 0;
            return () =>
            {
                ++attempt;
                if (attempt <= numberOfThrows)
                {
                    throw new DbUpdateConcurrencyException(string.Format("test exception {0}", attempt));
                }
                return TestReturnValue;
            };
        }
    }
}
