﻿using System;
using NUnit.Framework;
using Utils.Validation;

namespace Utils.Tests
{
    [TestFixture]
    public class ArgumentValidationExtensionTests
    {
        private const string FakeArgName = "AF60D6FF-1AD2-4D21-ACD3-AAF9E305648F";

        [Test]
        public void ArgumentStringNotNullOrEmptyShouldNotThrowForValidString()
        {
            Assert.DoesNotThrow(() => "valid".ArgumentStringNotNullOrEmpty(FakeArgName));
        }

        [Test]
        public void ArgumentStringNotNullOrEmptyShouldThrowForEmptyString()
        {
            var e = Assert.Throws<ArgumentException>(() => string.Empty.ArgumentStringNotNullOrEmpty(FakeArgName));
            StringAssert.Contains(FakeArgName, e.Message);
        }

        [Test]
        public void ArgumentStringNotNullOrEmptyShouldThrowForNullString()
        {
            string s = null;
// ReSharper disable ExpressionIsAlwaysNull
            var e = Assert.Throws<ArgumentNullException>(() => s.ArgumentStringNotNullOrEmpty(FakeArgName));
// ReSharper restore ExpressionIsAlwaysNull
            StringAssert.Contains(FakeArgName, e.Message);
        }
    }
}
