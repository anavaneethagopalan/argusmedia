﻿using System.Linq;
using NUnit.Framework;
using Utils.Validation;

namespace Utils.Tests
{
    [TestFixture]
    public class PasswordValidationTests
    {
        [Test]

        //8 char upper lower number/ special char 
        public void PasswordMustPassValidationRules()
        {
            Assert.IsTrue(PasswordValidator.ValidatePassword("Tusharaabc1£"));
            Assert.IsTrue(PasswordValidator.ValidatePassword("Tu££££araabc1£"));
            Assert.IsTrue(PasswordValidator.ValidatePassword("TusTTTTTTTara)bc1£"));
            Assert.IsTrue(PasswordValidator.ValidatePassword("TusTTTT333ra)bc1£"));
            Assert.IsTrue(PasswordValidator.ValidatePassword("Tushar%1"));
            Assert.IsTrue(PasswordValidator.ValidatePassword("Tushara1"));
        }

        [Test]
        public void PasswordMustContainOneUpperCaseChar()
        {
            Assert.IsFalse(PasswordValidator.ValidatePassword("tusharaabc1£"));
        }

        [Test]
        public void PasswordMustContainOneLowerCaseChar()
        {
            Assert.IsFalse(PasswordValidator.ValidatePassword("TUSHARAABC1£"));
        }

        [Test]
        public void PasswordMustContainOneSpecialCharOrNumber()
        {
            Assert.IsFalse(PasswordValidator.ValidatePassword("TUSHARAABCe"));
        }

        [Test]
        public void PasswordMustBeMoreThan8Chars()
        {
            Assert.IsFalse(PasswordValidator.ValidatePassword("TUSRAA1"));
        }

        [Test]
        public void PasswordMustBeLessThan50Chars()
        {
            Assert.IsFalse(PasswordValidator.ValidatePassword(string.Concat(Enumerable.Range(1, 40)) + "Tusharaabc1£"));
        }
    }
}
