﻿using System;
using log4net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace AOM
{
    /// <summary>
    /// Common module logging static class.
    /// </summary>
    public static class Log
    {
        private static ILog log;

        static Log()
        {
            log = LogManager.GetLogger("AOM_Common");
        }

        public static void Debug(String text)
        {
            if (log.IsDebugEnabled)
                log.Debug(text);
        }

        public static void Warn(String text)
        {
            if (log.IsWarnEnabled)
                log.Warn(text);
        }

        public static void Info(String text)
        {
            if (log.IsInfoEnabled)
                log.Info(text);
        }

        public static void Error(String text)
        {
            if (log.IsErrorEnabled)
                log.Error(text);
        }

        public static void Fatal(String text)
        {
            log.Fatal(text);
        }
    }
}
