﻿using System;
using System.ServiceModel;

namespace AOM
{
    public class SvcHost : IDisposable
    {
        // Flag: Has Dispose already been called? 
        bool _disposed = false;

        private ServiceHost _serviceHost;

        /// <summary>
        /// Create WCF service host acting as proxy for the true type
        /// </summary>
        /// <param name="path">Path to an assembly file containing the true type.</param>
        /// <param name="classname"></param>
        /// <returns>Success indicator.</returns>
        public bool CreateServiceHost(string path, string classname)
        {
            // Generate type for WCF service. This type serves as proxy of a true type.
            var serviceType = ServiceTypeGenerator.GetLoadedType(path,classname);
            if (null == serviceType) return false;
            _serviceHost = OpenServiceHost(serviceType);
            if (null == _serviceHost) return false;
            foreach (var sep in _serviceHost.Description.Endpoints)
                Console.WriteLine(sep.Address.ToString());
            return true;
        }

        /// <summary>
        /// Actually create and open service host.
        /// </summary>
        /// <param name="serviceType">WCF service type.</param>
        /// <returns>Service host object.</returns>
        internal static ServiceHost OpenServiceHost(Type serviceType)
        {
            ServiceHost serviceHost;
            if (null == serviceType) return null;
            try
            {
                serviceHost = new ServiceHost(serviceType);
                serviceHost.Open();
            }
            catch (Exception e)
            {
                serviceHost = null;
                Console.WriteLine("ERROR! {0}", e.Message);
            }
            return serviceHost;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                 _serviceHost.Close();
                _serviceHost = null;
            }
            
            _disposed = true;
        }

    }
}
