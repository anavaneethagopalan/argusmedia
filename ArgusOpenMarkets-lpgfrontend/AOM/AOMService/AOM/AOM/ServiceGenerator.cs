﻿using System;
using System.Linq;
using System.Reflection;

namespace AOM
{
    /// <summary>
    /// Static methods that returns a Type
    /// </summary>
    public static class ServiceTypeGenerator
    {
        /// <summary>
        /// Return a single object type a passed dll
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static public Type GetLoadedType(string path)
        {
            var assm = Assembly.LoadFrom(path);
            return assm.GetType();
        }

        /// <summary>
        /// Returns a specific object in a dll
        /// </summary>
        /// <param name="path"></param>
        /// <param name="classname"></param>
        /// <returns></returns>
        static public Type GetLoadedType(string path, string classname)
        {
            var assm = Assembly.LoadFrom(path);
            return assm.GetTypes().FirstOrDefault(t => t.Name.Equals(classname));
        }
    }
}