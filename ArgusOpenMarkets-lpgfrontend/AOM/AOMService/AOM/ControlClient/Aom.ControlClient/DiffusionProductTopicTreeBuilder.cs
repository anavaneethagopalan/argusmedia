﻿using System;
using System.Collections.Generic;
using System.Linq;

using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.TopicManager;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using AOM.Transport.Events.Products;
using AOM.Transport.Events.Users;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using PushTechnology.ClientInterface.Client.Factories;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

using PushTechnology.ClientInterface.Data.JSON;

namespace AOM.ControlClient
{
    internal class DiffusionProductTopicTreeBuilder : IDiffusionProductTopicTreeBuilder
    {
        private const string MetadataTopicName = "MetaData";
        private readonly IProductsDiffusionTopicManager _productsDiffusionTopicManager;
        private bool _topicManagerStandby = false;


        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private readonly IBus _aomBus;

        private bool disposed;

        public DiffusionProductTopicTreeBuilder(IProductsDiffusionTopicManager productsDiffusionTopicManager,
            IBus aomBus)
        {
            _productsDiffusionTopicManager = productsDiffusionTopicManager;
            _aomBus = aomBus;

            _productsDiffusionTopicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("DiffusionProductTopicTreeBuilder Active");
                _subscriptionHandlers.AddRange(RunAndSubscribe());
                if (_topicManagerStandby == false)
                {
                    _aomBus.Publish(new GetProductConfigRequest());
                }
            };
            _productsDiffusionTopicManager.StandBy += (sender, args) =>
            {
                _topicManagerStandby = true;
                DisposeSubscriptions();
            };
        }

        internal int SubscriberCount
        {
            get { return _subscriptionHandlers.Count; }
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<GetProductConfigResponse>(SubscriptionId, ConsumeGetProductConfigResponse);
            yield return
                _aomBus.Subscribe<GetProductMetaDataResponse>(SubscriptionId, ConsumeGetProductMetaDataConfigResponse);
        }

        private void ConsumeGetProductMetaDataConfigResponse(GetProductMetaDataResponse productMetaData)
        {
            Log.Debug("DiffusionProductTopicTreeBuilder GetProductMetaDataResponse message received");

            var metaData = productMetaData.ProductMetaData;

            if (metaData != null)
            {
                if (metaData.Fields != null)
                {
                    if (metaData.Fields.Any(f => f == null))
                    {
                        Log.ErrorFormat("Invalid product metadata received for product {0}, possibly null values",
                            metaData.ProductId);
                    }
                    else
                    {
                        //                        var json = metaData.ToJsonCamelCase();
                        //                        Log.Debug(
                        //                            "DiffusionProductTopicTreeBuilder ConsumeGetProductMetaDataConfigResponse - json returned" +
                        //                            json);
                        //                        Log.Debug("DiffusionProductTopicTreeBuilder Product MetaData: " + metaData);
                        var productTopicData = new ProductTopicData
                        {
                            ProductId = productMetaData.ProductMetaData.ProductId,
                            ProductTopicType = ProductTopicType.MetaData,
                            Content = metaData
                        };
                        var json = productTopicData.ToJsonCamelCase();
                        BuildProductTopicStructure(metaData, json);
                    }
                }
            }
            else
            {
                Log.Warn(
                    "DiffusionProductTopicTreeBuilder GetProductConfigResponse.ProductDefinition is null, ignoring message");
            }
        }

        private void ConsumeGetProductConfigResponse(GetProductConfigResponse productConfigResponse)
        {
            Log.Debug("DiffusionProductTopicTreeBuilder GetProductConfigResponse message received");

            var productDefinition = productConfigResponse.ProductDefinition;

            if (productDefinition != null)
            {
                Log.Debug("DiffusionProductTopicTreeBuilder Product Definition: " + productDefinition);
                BuildProductTopicStructure(productDefinition);
            }
            else
            {
                Log.Warn(
                    "DiffusionProductTopicTreeBuilder GetProductConfigResponse.ProductDefinition is null, ignoring message");
            }
        }

        private ClientSessionInfo BuildSystemUserSessionInfo()
        {
            return new ClientSessionInfo {UserId = -1, SessionId = ToString()};
        }

        private string BuildProductTopicTree(long productId)
        {
            return ProductTopicManager.ProductTopicPath(productId);
        }

        private void BuildProductTopicStructure(ProductMetaData productMetaData, string json)
        {
            var productTopicPath = BuildProductTopicTree(productMetaData.ProductId);

            if (_productsDiffusionTopicManager != null)
            {
                try
                {
                    string metaDataTopicName = string.Join("/", productTopicPath, MetadataTopicName);
                    Log.Info("BuildProductTopicStructure - Product MetaData Topic Path: " + metaDataTopicName);
                    BuildDiffusionTopicAndPopulateData(metaDataTopicName, json);
                }
                catch (Exception exception)
                {
                    _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                    {
                        Error = exception,
                        AdditionalInformation = "Error communicating with AomTopicManager for Definition topic",
                        ErrorText = string.Empty,
                        Source = "DiffusionProductTopicTreeBuilder"
                    });
                }
            }
            else
            {
                Log.Info("_aomTopicManager is null - this means were not pushing to topic");
            }
        }

        private void BuildProductTopicStructure(ProductDefinitionItem product)
        {
            var productTopicPath = BuildProductTopicTree(product.ProductId);
            Log.Info("BuildProductTopicStructure - Product Topic Path: " + productTopicPath);
            if (_productsDiffusionTopicManager != null)
            {
                try
                {
                    var productStatusTopicName = string.Format("{0}/MarketStatus", productTopicPath);
                    var productMarketStatusJson = new ProductTopicData
                    {
                        ProductId = product.ProductId,
                        ProductTopicType = ProductTopicType.MarketStatus,
                        Content = new {product.Status, product.ProductName}
                    }.ToJsonCamelCase();
                    BuildDiffusionTopicAndPopulateData(productStatusTopicName, productMarketStatusJson);

                    string productDefinitionTopicName = string.Format("{0}/Definition", productTopicPath);
                    var productDefinitionJson = new ProductTopicData
                    {
                        ProductId = product.ProductId,
                        ProductTopicType = ProductTopicType.Definition,
                        Content = product
                    }.ToJsonCamelCase();

                    BuildDiffusionTopicAndPopulateData(productDefinitionTopicName, productDefinitionJson);

                    var productContentStreamsTopicName = ContentStreamTopicManager.ContentStreamTopicPath(product.ProductId);

                    var productContentJson = new ProductTopicData
                    {
                        ProductId = product.ProductId,
                        ProductTopicType = ProductTopicType.ContentStream,
                        Content = product.ContentStreamIds
                    }.ToJsonCamelCase();

                    BuildDiffusionTopicAndPopulateData(productContentStreamsTopicName, productContentJson);
                }
                catch (Exception exception)
                {
                    _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                    {
                        Error = exception,
                        AdditionalInformation = "Error communicating with AomTopicManager for Definition topic",
                        ErrorText = string.Empty,
                        Source = "DiffusionProductTopicTreeBuilder"
                    });
                }
            }
            else
            {
                Log.Info("_aomTopicManager is null - this means were not pushing to topic");
            }

            var systemUserSessionInfo = BuildSystemUserSessionInfo();

            _aomBus.Publish(new AomRefreshAssessmentRequest
            {
                ProductId = product.ProductId,
                ClientSessionInfo = systemUserSessionInfo
            });

            _aomBus.Publish(new GetProductMetaDataRequest()
            {
                ClientSessionInfo = systemUserSessionInfo,
                MessageAction = MessageAction.Refresh,
                ProductId = product.ProductId
            });
        }

        private void BuildDiffusionTopicAndPopulateData(string topicName, string data)
        {
            var handler = new TopicControlAddCallback();
            handler.TopicAdded += (o, s) =>
            {
                IJSON jsonMessage = null;
                try
                {
                    jsonMessage = Diffusion.DataTypes.JSON.FromJSONString(data);
                }
                catch (Exception ex)
                {
                    Log.Error("Error serializing to json", ex);
                }

                try
                {
                    Log.Info(string.Format("Actually Pushing to topic:{0} data:{1}", topicName, data));
                    _productsDiffusionTopicManager.GetUpdater().PushToTopic(topicName, jsonMessage, true);
                }
                catch (Exception ex)
                {
                    Log.Error(
                        string.Format(
                            "Error attempting to build diffusion topic and populate.  Topic Name{0}  Data:{1}",
                            topicName,
                            data),
                        ex);
                }

            };

            _productsDiffusionTopicManager.CreateTopic(topicName, handler, () => TopicAlreadyExists(topicName, data));
        }

        private void TopicAlreadyExists(string topicName, string data)
        {
            Log.InfoFormat(
                "DiffusionProductTopicTreeBuilder.TopicAlreadyExists. The topic: {0} already exists. We were attempting to push the following data: {1}",
                topicName,
                data);

            if (topicName.ToLower().EndsWith("definition")
                || topicName.ToLower().EndsWith("marketstatus")
                || topicName.ToLower().EndsWith("assessment")
                || topicName.ToLower().EndsWith(MetadataTopicName.ToLower()))
            {
                Log.Info(string.Format("DiffusionProductTopicTreeBuilder.TopicAlreadyExists - The topic:{0}, already exists we are attempting to push the data down it.  Data:{1}", topicName, data));
                var jsonData = Diffusion.DataTypes.JSON.FromJSONString(data);
                _productsDiffusionTopicManager.GetUpdater().PushToTopic(topicName, jsonData, true, null);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    DisposeSubscriptions();
                }
            }
            disposed = true;
        }

        private void DisposeSubscriptions()
        {
            foreach (var subscriptionHandler in _subscriptionHandlers)
            {
                if (subscriptionHandler != null)
                {
                    subscriptionHandler.Dispose();
                }
            }
            _subscriptionHandlers.Clear();
        }

        public void Disconnect()
        {
            DisposeSubscriptions();
        }

        protected virtual string SubscriptionId
        {
            get { return "DiffusionProductTopicTreeBuilder"; }
        }
    }
}