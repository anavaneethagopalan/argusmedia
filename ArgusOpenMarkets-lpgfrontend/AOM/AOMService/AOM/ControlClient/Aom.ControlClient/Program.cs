﻿namespace AOM.ControlClient
{
    using System;
    using System.Diagnostics;
    using System.Linq;

    using AOM.Repository.MySql;
    using AOM.Services;
    using AOM.Transport.Service.Processor.Common;
    
    using Utils.Logging.Utils;
    using Ninject;
    
    internal class Program
    {
        private static void Main()
        {
            Log.Info("Starting AOM.ControlClient Version 2");            

            using (var kernel = new StandardKernel())
            {
                kernel.Load(
                    new EFModelBootstrapper(),
                    new ServiceBootstrap(),
                    new LocalBootstrapper(),
                    new ProcessorCommonBootstrap());

                WarmUp(kernel);
                ServiceHelper.BuildAndRunService<ControlClientController>(kernel);
                Log.Info("Finished Kernel load, warmup and build-run of services");
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                bool any;
                using (var db = dbFactory.CreateCrmModel())
                {
                    any = db.UserInfoes.Any();
                }
                Log.Info(string.Format("Initial EF model build took {0} msecs. Do we have any results: {1}", timer.ElapsedMilliseconds, any));

            }
            catch (Exception ex)
            {
                Log.Error("AOMControlClient - WarmUp - Error - cannot connect to DB", ex);
                ServiceHelper.BuildAndRunService<ControlClientController>(kernel);
            }
        }
    }
}