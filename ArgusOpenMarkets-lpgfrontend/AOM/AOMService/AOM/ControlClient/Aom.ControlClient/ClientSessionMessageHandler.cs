﻿using AOM.TopicManager;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient
{
    using AOM.ControlClient.Interfaces;
    using AOM.Transport.Events;
    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using PushTechnology.ClientInterface.Client.Session;
    using System;
    using System.Diagnostics;
    using System.Linq;
    using Utils.Logging.Utils;

    public class ClientSessionMessageHandler : IClientSessionMessageHandler
    {
        private IMessagingControl _messageControl;
        private readonly IDiffusionSessionManager _diffusionSessionManager;

        public ClientSessionMessageHandler(IDiffusionSessionManager diffusionSessionManager)
        {
            _diffusionSessionManager = diffusionSessionManager;
        }

        public void AttachToSession(ISession session)
        {
            _messageControl = session.GetMessagingControlFeature();
        }

        public void SendMessage(ClientSessionInfo csi, string content)
        {
            try
            {
                if (csi == null)
                {
                    throw new ArgumentNullException("csi");
                }

                var topic = UserTopicManager.UserTopicPath(csi.UserId);
                var sessionId = _diffusionSessionManager.GetSession(csi);
                var msg = PushTechnology.ClientInterface.Client.Factories.Diffusion.Content.NewContent(content);

                if (sessionId != null)
                {
                    _messageControl.Send(sessionId, topic, msg, this);
                    Debug.Print("\r===========MESSAGE TO CLIENT============\r\r" + content
                        + "\r\r==========================================\r");
                }
            }
            catch (Exception ex)
            {
                Log.Error("ClientSessionMessageHandler.SendMessage", ex);
            }
        }

        public void SendMessageToAllUserSessions(long userId, string content)
        {
            try
            {
                var csis = _diffusionSessionManager.GetUserSessions(userId);

                foreach (var csi in csis)
                {
                    SendMessage(csi, content);
                }
                Log.Info(string.Format("Successfully sent notification: [{0}]/n message to user with Id: {1}", content, userId));
            }
            catch (Exception ex)
            {
                Log.Error("ClientSessionMessageHandler.SendMessageToAllUserSessions", ex);
            }
        }

        public void SendMessageThisSessionOnly(string currentSessionId, long userId, string content)
        {
            try
            {
                var clientSessionInfo = _diffusionSessionManager.GetUserSessions(userId).FirstOrDefault(s => s.SessionId == currentSessionId);

                if (clientSessionInfo != null)
                {
                    SendMessage(clientSessionInfo, content);
                }
            }
            catch (Exception ex)
            {
                Log.Error("ClientSessionMessageHandler.SendMessageThisSessionOnly", ex);
            }
        }

        public void SendMessageToUserOtherSessions(string userSessionToIgnore, long userId, string content)
        {
            try
            {
                var csis = _diffusionSessionManager.GetUserOtherSessions(userSessionToIgnore, userId);

                foreach (var csi in csis)
                {
                    SendMessage(csi, content);
                }

                Log.Info(string.Format("Successfully sent notification: [{0}]/n message to user with Id: {1}", content, userId));
            }
            catch (Exception ex)
            {
                Log.Error("ClientSessionMessageHandler.SendMessageToUserOtherSessions", ex);
            }
        }

        public void OnComplete()
        {
            Log.Info(string.Format("Sent message to client"));
        }

        public void OnDiscard()
        {
            Log.Error(string.Format("ERROR SENDING MESSAGE TO CLIENT: OnDiscard"));
        }

    }
}