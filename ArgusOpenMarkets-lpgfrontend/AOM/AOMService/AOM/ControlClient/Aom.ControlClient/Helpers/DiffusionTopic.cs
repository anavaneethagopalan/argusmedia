﻿using System.Text.RegularExpressions;

namespace AOM.ControlClient.Helpers
{
    internal static class DiffusionTopic
    {
        public static long? GetProductIdFromTopicDeleted(string topicDeleted)
        {
            long productId;
            var numbers = Regex.Match(topicDeleted, "[0-9]+");
            var parsed = long.TryParse(numbers.Value, out productId);
            return parsed ? productId : (long?) null;
        }
    }
}
