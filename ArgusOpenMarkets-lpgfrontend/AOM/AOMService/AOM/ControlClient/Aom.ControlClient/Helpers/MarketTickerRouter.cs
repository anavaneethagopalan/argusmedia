﻿using System;
using AOM.App.Domain.Entities;
using AOM.Transport.Events.MarketTickers;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Helpers
{
    public static class MarketTickerRouter
    {
        public static bool ShouldSendNonPendingOnlyToOwnerOrganisations(
            this MarketTickerItem mti,
            AdditionalMarketTickerData additionalData)
        {
            if (mti == null) throw new ArgumentNullException("mti");

            bool isVoid = mti.MarketTickerItemStatus.Equals(MarketTickerItemStatus.Void);
            bool isInfo = mti.MarketTickerItemType.Equals(MarketTickerItemType.Info);
            bool isFromPending = mti.LastMarketTickerItemStatus != null &&
                                 mti.LastMarketTickerItemStatus == MarketTickerItemStatus.Pending;
            if (isInfo && isVoid && isFromPending)
            {
                return true;
            }
            else if (isInfo && isVoid)
            {
                return false;
            }

            bool isExternalDeal =
                mti.MarketTickerItemType.Equals(MarketTickerItemType.Deal) &&
                mti.ExternalDealId.HasValue;
            if (isVoid && isExternalDeal)
            {
                if (additionalData == null)
                {
                    Log.Error(string.Format(
                            "Request to void external deal {0} has no AdditionalData",
                            mti.ExternalDealId.Value));    
                    return false;
                }
                if (!additionalData.PreviousDealStatus.HasValue)
                {
                    Log.Error(string.Format(
                            "Request to void external deal {0} has no PreviousDealStatus",
                            mti.ExternalDealId.Value));
                    return false;
                }
                return additionalData.PreviousDealStatus == DealStatus.Pending;
            }

            return false;
        }
    }
}
