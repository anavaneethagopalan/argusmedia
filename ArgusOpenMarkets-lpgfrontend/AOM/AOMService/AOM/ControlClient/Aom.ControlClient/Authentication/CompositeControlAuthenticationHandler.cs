﻿namespace AOM.ControlClient.Authentication
{
    using System.Collections.Generic;
    using System.Linq;

    using PushTechnology.ClientInterface.Client.Details;
    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Features.Control.Clients;
    using PushTechnology.ClientInterface.Client.Security.Authentication;
    using PushTechnology.DiffusionCore.Client.Types;

    internal class CompositeControlAuthenticationHandler<T> : IControlAuthenticationHandler
        where T : IControlAuthenticationHandler
    {
        private List<T> handlers;

        public CompositeControlAuthenticationHandler(params T[] handlers)
        {
            this.handlers = handlers.OfType<T>().ToList();
        }

        public void Authenticate(
            string principal,
            ICredentials credentials,
            ISessionDetails sessionDetails,
            IAuthenticationHandlerCallback callback)
        {
            foreach (T handler in handlers)
            {
                handler.Authenticate(principal, credentials, sessionDetails, callback);
            }
        }

        public void OnActive(IRegisteredHandler registeredHandler)
        {
            foreach (T handler in handlers)
            {
                handler.OnActive(registeredHandler);
            }
        }

        public void OnClose()
        {
            foreach (T handler in handlers)
            {
                handler.OnClose();
            }
        }
    }
}