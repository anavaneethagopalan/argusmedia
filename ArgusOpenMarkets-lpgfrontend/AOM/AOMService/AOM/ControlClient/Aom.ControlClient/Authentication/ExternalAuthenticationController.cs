﻿using AOM.App.Domain.Services;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using PushTechnology.ClientInterface.Client.Features.Control.Clients;

namespace AOM.ControlClient.Authentication
{
    internal class ExternalAuthenticationController : CompositeControlAuthenticationHandler
    {
        private static ExternalCredentialsAuthentication _externalCredentialsAuthentication;

        public bool Registered
        {
            get { return _externalCredentialsAuthentication.Registered; }
        }
        
        public ExternalAuthenticationController(IAuthenticationDiffusionTopicManager aomTopicManager, IUserAuthenticationService authService,
            IBus bus, IDiffusionSessionManager diffusionSessionManager) : 
            base(_externalCredentialsAuthentication = new ExternalCredentialsAuthentication(aomTopicManager, authService, bus, diffusionSessionManager), new AuthenticationForConsole())
        { }
    }
}