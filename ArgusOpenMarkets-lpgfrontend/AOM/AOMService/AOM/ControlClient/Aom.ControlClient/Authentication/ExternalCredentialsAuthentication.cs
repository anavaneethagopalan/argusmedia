﻿using System;
using System.Text;
using AOM.App.Domain.Services;
using AOM.TopicManager;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;
using Argus.Transport.Infrastructure;

using PushTechnology.ClientInterface.Client.Details;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using PushTechnology.ClientInterface.Client.Security.Authentication;
using PushTechnology.ClientInterface.Data.JSON;
using PushTechnology.DiffusionCore.Client.Types;

namespace AOM.ControlClient.Authentication
{
    public class ExternalCredentialsAuthentication : ExternalAbstractAuthentication
    {
        private const string AomPrefix = "AOM-";
        private readonly IBus _aomBus;
        private readonly IDiffusionSessionManager _diffusionSessionManager;
        private readonly IAuthenticationDiffusionTopicManager _aomTopicManager;
        private readonly IUserAuthenticationService _userAuthenticationService;
        private string _environmentMachineName = string.Empty;


        public ExternalCredentialsAuthentication(IAuthenticationDiffusionTopicManager aomTopicManager, IUserAuthenticationService userAuthenticationService, IBus bus, 
            IDiffusionSessionManager diffusionSessionManager)
        {
            _userAuthenticationService = userAuthenticationService;
            _aomBus = bus;
            _diffusionSessionManager = diffusionSessionManager;
            _aomTopicManager = aomTopicManager;

            // NTB - NO LONGER PUBLISHING MESSAGES ON THE BUS TO SETUP THE USERS TOPIC
            // _aomBus.Subscribe<SetupUserTopicResponse>(SubscriptionId, OnSetupUserTopic);
        }

        public string SubscriptionId
        {
            get { return "AOM.ExternalCredentialsAuthentication." + EnvironmentMachineName; }
        }

        public string EnvironmentMachineName
        {
            get
            {
                if (string.IsNullOrEmpty(_environmentMachineName))
                    _environmentMachineName = Environment.MachineName;

                return _environmentMachineName;
            }
        }

        public override void Auth(string principal, ICredentials credentials, ISessionDetails sessionDetails, IAuthenticationHandlerCallback callback)
        {
            Log.Debug("CLIENT_AUTH: External Credentials Authentication has been called - User: " + principal);
            
            if (!principal.StartsWithIgnoreCase(AomPrefix))
            {
                Log.Debug("CLIENT_AUTH: AOM Authentication Handler is abstaining from Authenticating User - " + principal);
                callback.Abstain();
                return;
            }
            
            var token = Encoding.UTF8.GetString(credentials.ToBytes());
            var username = StripOutAomPrefix(principal);

            if (credentials.Type == CredentialsType.NONE)
            {
                callback.Abstain();
            }
            else
            {
                Log.Debug("CLIENT_AUTH: Checking Token For User : " + username);
                var result = _userAuthenticationService.AuthenticateToken(username, token);

                if (result.IsAuthenticated)
                {
                    Log.DebugFormat("CLIENT_AUTH: Checking Token For User : {0}   User IS Authenticated.  Publishing setup user topic", username);
                    callback.Allow(PushTechnology.ClientInterface.Client.Factories.Diffusion.AuthenticationResult.WithRoles());

                    // NTB - CHANGE WE DON'T WANT TO PUSH THE USERS DETAILS DOWN THE DIFFUSION TOPIC.  
                    // _aomBus.Publish(new SetupUserTopicRequest {UserId = result.User.Id, Principal = username});
                    // NOTE: I will setup a new topic for the user - as subsequernt code pushes messages up/down 
                    // the user topic so it's probably easiest to leave the topic there for now.  
                    SetupUserTopic(new UserDetails { UserId = result.UserId, Principal = principal });
                }
                else
                {
                    callback.Abstain();
                    Log.Warn(string.Format("CLIENT_AUTH: NOT AUTHENTICATED : Abstained connection for {0}.", principal));
                }
            }
        }

        private static string StripOutAomPrefix(string principal)
        {
            if (principal.StartsWithIgnoreCase(AomPrefix))
                return principal.Substring(AomPrefix.Length);
            return principal;
        }

        private void SetupUserTopic(UserDetails userDetails)
        {
            Log.Debug("CLIENT_AUTH: OnSetupUserTopicResponse");
            var principal = string.Empty;

            try
            {
                var userTopicSetupCallback = new TopicUpdateCallback(principal, string.Empty);
                userTopicSetupCallback.OnUpdate += (sender, s) => { Log.DebugFormat("CLIENT_AUTH: Updated User Topic. Principal: {0} - {1}", principal, s); };
                userTopicSetupCallback.OnTopicUpdateError += (sender, s) => { Log.DebugFormat("CLIENT_AUTH: User Topic Setup - ERROR.  S: {0}", s); };

                SetupUserTopic(userDetails, userTopicSetupCallback);
            }
            catch (Exception ex)
            {
                Log.Error("OnSetupUserTopic", ex);
            }
        }


        private void SetupUserTopic(UserDetails userDetails, ITopicUpdaterUpdateCallback topicUpdateCallback)
        {
            Log.DebugFormat("CLIENT_AUTH: SetupUserTopic for the user: {0}", userDetails.UserId);
            var topicName = UserTopicManager.UserTopicPath(userDetails.UserId);
            Log.DebugFormat("Topic Name:{0}", topicName);

            try
            {
                var addTopicCallback = new TopicControlAddCallback();
                addTopicCallback.TopicAdded += (sender, s) =>
                {
                    Log.DebugFormat("CLIENT_AUTH: SetupUserTopic.AddTopicCallback.PushToUserTopic  User Id: {0}   Username:{1}   Topic Name: {2}",
                        userDetails.UserId, userDetails.Principal, topicName);
                    PushToUserTopic(userDetails, topicUpdateCallback, topicName);
                };

                addTopicCallback.TopicAddFailed += (sender, s) =>
                {
                    Log.DebugFormat("CLIENT_AUTH: Topic Add Failed - Hooking into the Event in the User Topic Setup - Pushing User To Topic anyway.  Topic:{0}", s);
                    // Topic creation failed - ignore and attempt to push the user does the topic.  
                    PushToUserTopic(userDetails, topicUpdateCallback, topicName);
                };

                _aomTopicManager.CreateTopic(topicName, addTopicCallback, () =>
                {
                    if (userDetails != null)
                    {
                        Log.DebugFormat("SetupUserTopic.TopicAlreadyExists.PushUserToTopic.  User Id:{0}  Username: {1}  Topic Name:{2}", userDetails.UserId, userDetails.Principal, topicName);
                        PushToUserTopic(userDetails, topicUpdateCallback, topicName);
                    }
                    else
                    {
                        if (userDetails != null)
                        {
                            Log.DebugFormat("CLIENT_AUTH: SetupUserTopic.TopicAlreadyExists.PushUserToTopic.  User Id:{0}  Username: {1}  Topic Name:{2}",
                                userDetails.UserId, userDetails.Principal, topicName);
                            PushToUserTopic(userDetails, topicUpdateCallback, topicName);
                        }
                        else
                        {
                            Log.DebugFormat("CLIENT_AUTH: SetupUserTopic.USER IS NULL  TopicName: {0}", topicName);
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Log.Error("ExternalCredentialsAuthentication-SetupTopicAndGetUserDetails", ex);
                Log.Error("ExternalCredentialsAuthentication-SetupTopicAndGetUserDetails", ex);
            }
        }

        private void PushToUserTopic(UserDetails user, ITopicUpdaterUpdateCallback topicUpdateCallback, string topicName)
        {
            Log.DebugFormat("CLIENT_AUTH: WE ARE PUSHING THE USER DOWN THE TOPIC  User Id: {0}", user.UserId);
            var message = user.ToJsonCamelCase();

            var topicUpdater = _aomTopicManager.GetUpdater();

            if (topicUpdater != null)
            {

                IJSON jsonMessage = null;
                try
                {
                    jsonMessage = PushTechnology.ClientInterface.Client.Factories.Diffusion.DataTypes.JSON.FromJSONString(message);
                }
                catch (Exception ex)
                {
                    Log.Error("Error serializing to json", ex);
                }
                Log.InfoFormat("CLIENT_AUTH: PushUserToTopic - Is Topic Updater is Active - Yes - Actually Pushing the user down with Id: {0}", user.UserId);
                topicUpdater.PushToTopic(topicName, jsonMessage, true, topicUpdateCallback);
            }
            else
            {
                Log.Error("ExternalCredentialsAuthentication - GetUpdater() returned null");
            }
        }
    }

    public class UserDetails
    {
        public long UserId { get; set; }
        public string Principal { get; set; }
    }
}