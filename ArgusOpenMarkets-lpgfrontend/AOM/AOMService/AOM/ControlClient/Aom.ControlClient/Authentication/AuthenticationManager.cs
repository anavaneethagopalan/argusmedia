﻿using System;
using AOM.App.Domain.Services;
using AOM.ControlClient.Interfaces;
using Argus.Transport.Infrastructure;
using PushTechnology.ClientInterface.Client.Details;
using PushTechnology.ClientInterface.Client.Features.Control.Clients;
using System.Configuration;
using System.Linq;
using AOMDiffusion.Common.Interfaces;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Authentication
{
    public class AuthenticationManager : IAuthenticationManager
    {
        private readonly string _authHandlerName = ConfigurationManager.AppSettings["DiffusionHandlerName"];
        private readonly IAuthenticationDiffusionTopicManager _aomTopicManager;
        private readonly IUserAuthenticationService _authService;
        private IAuthenticationControl _authControl;
        private readonly IBus _bus;
        private readonly IDiffusionSessionManager _diffusionSessionManager;

        private ExternalAuthenticationController _authenticationHandler;

        public bool AuthHandlerRegistered
        {
            get { return _authenticationHandler.Registered; }
        }

        public AuthenticationManager(IAuthenticationDiffusionTopicManager aomTopicManager, IUserAuthenticationService authService, IBus bus, IDiffusionSessionManager diffusionSessionManager)
        {
            _aomTopicManager = aomTopicManager;
            _authService = authService;
            _bus = bus;
            _diffusionSessionManager = diffusionSessionManager;

            _authenticationHandler = new ExternalAuthenticationController(_aomTopicManager , _authService , _bus , _diffusionSessionManager );

            _aomTopicManager.Active += (sender, args) =>
            {
                if (!AuthHandlerRegistered)
                {
                    Log.InfoFormat("Registering Authentication Handler");
                    RegisterAuthenticationHandler("ACTIVE");
                }
            };

            _aomTopicManager.StandBy += (sender, args) =>
            {
                Log.InfoFormat("We are going into Standby Mode - Authentication Capabilities shutting down");
                Log.Error("Authentication Manager on standby");
                if (!AuthHandlerRegistered)
                {
                    RegisterAuthenticationHandler("STANDBY");
                }
            };
        }

        private void RegisterAuthenticationHandler(string mode)
        {
            Log.InfoFormat("RAH - STARTING *** AUTHENTICATION {0}. Registering as an authentication handler... With handler name: {1}***", mode, _authHandlerName);

            try
            {
                _authControl = _aomTopicManager.Session.GetAuthenticationControlFeature();
                _authControl.SetAuthenticationHandler(_authHandlerName, DetailType.Values().ToList(), _authenticationHandler);
                Log.InfoFormat("RAH - SUCCESS *** AUTHENTICATION {0}. Registered as an authentication handler... With handler name: {1}***", mode, _authHandlerName);
            }
            catch (Exception ex)
            {
                Log.InfoFormat("RAH - ERROR *** AUTHENTICATION {0}. Failed to Register an authentication handler... With handler name: {1}***", mode, _authHandlerName);
                Log.Error("RegisterAuthenticationHandler - An error has occurred", ex);
            }
        }

        public void Run()
        {

        }

        public void Dispose()
        {
            //throw new System.NotImplementedException();
        }

        public void Disconnect()
        {

        }
    }
}