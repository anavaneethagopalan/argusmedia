﻿namespace AOM.ControlClient.Authentication
{
    using AOM.ControlClient.Interfaces;

    using PushTechnology.ClientInterface.Client.Security.Authentication;

    public class AuthenticationCallback : IAuthenticationCallback
    {
        public string Username { get; set; }

        public IAuthenticationHandlerCallback Callback { get; set; }
    }
}