﻿namespace AOM.ControlClient.Authentication
{
    using PushTechnology.ClientInterface.Client.Details;
    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Features.Control.Clients;
    using PushTechnology.ClientInterface.Client.Security.Authentication;
    using PushTechnology.DiffusionCore.Client.Types;

    using Utils.Logging.Utils;

    public abstract class ExternalAbstractAuthentication : IControlAuthenticationHandler
    {
        public bool Registered;

        public abstract void Auth(
            string principal,
            ICredentials credentials,
            ISessionDetails sessionDetails,
            IAuthenticationHandlerCallback callback);

        public void Authenticate(
            string principal,
            ICredentials credentials,
            ISessionDetails sessionDetails,
            IAuthenticationHandlerCallback callback)
        {
            // NOTE: Only ever called if we are ACTIVE.
            Auth(principal, credentials, sessionDetails, callback);

        }

        public void OnActive(IRegisteredHandler registeredHandler)
        {
            Log.Info("Authentication Is Now Active");
            Registered = true;
        }

        public void OnClose()
        {
            Log.Info("ExternalAuthenticationController::OnClose called...");
            Registered = false;
        }

        protected bool IsActive()
        {
            return Registered;
        }
    }
}