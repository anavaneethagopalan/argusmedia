﻿namespace AOM.ControlClient.Authentication
{
    using System.Collections.Generic;
    using System.Linq;

    using Interfaces;

    using PushTechnology.ClientInterface.Client.Security.Authentication;

    using Utils.Logging.Utils;

    public class AuthenticationCallbackHandlerService : IAuthenticationCallbackService
    {
        private List<AuthenticationCallback> _authenticationCallbacks = new List<AuthenticationCallback>();

        public void RegisterUserCallback(string username, IAuthenticationHandlerCallback callback)
        {
            Log.Info("Attempting To Register Authentication Callback For User: " + username);
            var existingCallback = GetUserAuthenticationCallback(username);
            if (existingCallback != null)
            {
                Log.Info("User Authentication Callback For User: " + username + " Already Exists");
                existingCallback.Callback = callback;
            }
            else
            {
                Log.Info("Added User Authentication Callback For User: " + username);
                _authenticationCallbacks.Add(new AuthenticationCallback { Callback = callback, Username = username });
            }
        }

        public AuthenticationCallback GetUserAuthenticationCallback(string username)
        {
            Log.Info("Checking For User Authentication Callback For User: " + username);
            AuthenticationCallback callback = _authenticationCallbacks.FirstOrDefault(cb => cb.Username == username);
            return callback;
        }


        public void Flush()
        {
            Log.Info("Flushing Authentication Callbacks");
            _authenticationCallbacks = new List<AuthenticationCallback>();
        }

        public long NumberCallbacks()
        {
            return _authenticationCallbacks.Count();
        }
    }
}