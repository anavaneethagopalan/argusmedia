﻿using PushTechnology.DiffusionCore.Client.Types;

namespace AOM.ControlClient.Authentication
{
    using PushTechnology.ClientInterface.Client.Details;
    using PushTechnology.ClientInterface.Client.Security.Authentication;

    using Utils.Logging.Utils;

    public class AuthenticationForConsole : ExternalAbstractAuthentication
    {
        public override void Auth(
            string principal,
            ICredentials credentials,
            ISessionDetails sessionDetails,
            IAuthenticationHandlerCallback callback)
        {
            if (credentials.Type == CredentialsType.NONE)
            {
                /*NOTE
                    the java code permits Loopback calls 
                    so the console can be opened on the machine diffusion is installed 
                    so we would not expect a call here for AddressType.LOCAL
                 */

                if (sessionDetails.Location.AddressType == AddressType.LOCAL
                    || sessionDetails.Location.AddressType == AddressType.LOOPBACK)
                {
                    Log.Info(
                        string.Format(
                            "Allowed {0} connection without credentials.",
                            sessionDetails.Location.AddressType));
                    callback.Allow();
                }
                else
                {
                    Log.Warn(
                        string.Format(
                            "NOT AUTHENTICATED : Denied {0} connection without credentials.",
                            sessionDetails.Location.AddressType));
                    callback.Deny();
                }
            }
            else
            {
                callback.Abstain();
            }

        }
    }
}