﻿using AOM.ControlClient.Interfaces;
using System;
using System.Collections.Generic;
using AOM.App.Domain.Dates;
using AOM.ControlClient.Helpers;
using AOM.TopicManager;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Events.Products;
using AOM.Transport.Events.Users;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.ControlClient
{
    internal sealed class DiffusionOrderTopicTreeBuilder : IDiffusionOrderTopicTreeBuilder
    {
        private bool _topicManagerStandby;

        private readonly IOrdersDiffusionTopicManager _diffusionTopicManager;
        private readonly IBus _aomBus;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        public DiffusionOrderTopicTreeBuilder(IOrdersDiffusionTopicManager diffusionTopicManager,
            IBus aomBus,
            IDateTimeProvider dateTimeProvider)
        {
            _diffusionTopicManager = diffusionTopicManager;
            _aomBus = aomBus;
            _dateTimeProvider = dateTimeProvider;

            _diffusionTopicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("DiffusionOrderTopicTreeBuilder Active");
                _subscriptionHandlers.AddRange(RunAndSubscribe());

                if (_topicManagerStandby == false)
                {
                    _aomBus.Publish(new GetProductIdsRequest());
                }
            };

            _diffusionTopicManager.StandBy += (sender, args) =>
            {
                _topicManagerStandby = true;
                ClearSubscriptions();
            };
        }

        private void ClearSubscriptions()
        {
            if (_subscriptionHandlers == null) return;
            _subscriptionHandlers.ForEach(s =>
            {
                if (s != null) s.Dispose();
            });
            _subscriptionHandlers.Clear();
        }

        internal long SubscriberCount
        {
            get { return _subscriptionHandlers.Count; }
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<GetProductIdsResponse>(SubscriptionId, ConsumeGetProductConfigResponse);
        }

        private void ConsumeGetProductConfigResponse(GetProductIdsResponse response)
        {
            Log.Debug("DiffusionOrderTopicTreeBuilder GetProductIdsResponse message received");
            if (response.ProductIds == null)
            {
                Log.Warn("DiffusionOrderTopicTreeBuilder GetProductIdsResponse.ProductIds is null, ignoring message");
                return;
            }
            response.ProductIds.ForEach(productId =>
            {
                Log.Debug("DiffusionOrderTopicTreeBuilder building orders tree for product " + productId);
                BuildOrdersTree(productId);
            });
        }

        public void BuildOrdersTree(long productId)
        {
            BuildAllOrdersTopic(productId);

            var systemUserSessionInfo = new ClientSessionInfo {UserId = -1, SessionId = ToString()};
            _aomBus.Publish(
                new AomOrderRefreshRequest
                {
                    BusinessDay = _dateTimeProvider.Today,
                    ProductId = productId,
                    ClientSessionInfo = systemUserSessionInfo
                });
        }

        private void BuildAllOrdersTopic(long productId)
        {
            var allOrdersPath = OrderTopicManager.CreateAllOrdersRootTopicPath(productId);
            BuildTopic(allOrdersPath);
        }

        private void BuildTopic(string topicName)
        {
            if (_diffusionTopicManager == null)
            {
                Log.Debug("DiffusionOrderTopicTreeBuilder._diffusionTopicManager is null");
                return;
            }

            try
            {
                var handler = new TopicControlAddCallback();
                _diffusionTopicManager.CreateTopic(topicName, handler);
            }
            catch (Exception ex)
            {
                _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = ex,
                    AdditionalInformation = "Error communicating with DiffusionTopicManager for Orders topic",
                    ErrorText = string.Empty,
                    Source = "DiffusionOrderTopicTreeBuilder"
                });
            }
        }

        public void PurgeOrdersForProduct(long productId)
        {
            DeleteTopicAndRebuild(productId);
        }

        private void DeleteTopicAndRebuild(long productId)
        {
            var topicPath = OrderTopicManager.CreateProductTopicPath(productId);
            var deleteCallback = new TopicControlDeleteCallback(topicPath);
            deleteCallback.TopicRemoved += (sender, s) =>
            {
                Log.InfoFormat("PurgeOrdersForProduct -  Topic {0} has been deleted.", s);
                long? idOfProductDeleted = DiffusionTopic.GetProductIdFromTopicDeleted(s);
                if (idOfProductDeleted.HasValue)
                {
                    BuildOrdersTree(idOfProductDeleted.Value);
                }
                else
                {
                    Log.WarnFormat("Purge orders has deleted a topic that cannot be linked to a product ID: {0}", s);
                }

            };
            _diffusionTopicManager.DeleteTopic(topicPath, ">", deleteCallback);
        }

        private const string SubscriptionId = "DiffusionOrderTopicTreeBuilder";
    }
}
