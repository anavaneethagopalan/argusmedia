﻿using AOM.ControlClient.Interfaces;
using AOM.ControlClient.Routers;
using AOM.TopicManager;
using AOM.Transport.Events;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using AssessmentRouterService = AOM.ControlClient.Routers.AssessmentRouterService;
using MarketTickerRouterService = AOM.ControlClient.Routers.MarketTickerRouterService;
using NewsRouterService = AOM.ControlClient.Routers.NewsRouterService;
using OrganisationRouterService = AOM.ControlClient.Routers.OrganisationRouterService;
using ProductsRouterService = AOM.ControlClient.Routers.ProductsRouterService;

using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Modules;

namespace AOM.ControlClient
{
    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind(x => x.FromThisAssembly().IncludingNonePublicTypes().SelectAllClasses().BindAllInterfaces());

            kernel.Bind(x => x.FromAssemblyContaining<IAomDiffusionTopicManager>().IncludingNonePublicTypes().SelectAllClasses().BindAllInterfaces());
            kernel.Unbind<IAomDiffusionTopicManager>();
            kernel.Rebind<IAuthenticationDiffusionTopicManager>().To<AuthenticationDiffusionTopicManager>().InSingletonScope();
            kernel.Rebind<IProductsDiffusionTopicManager>().To<ProductsDiffusionTopicManager>().InSingletonScope();
            kernel.Rebind<IOrdersDiffusionTopicManager>().To<OrdersDiffusionTopicManager>().InSingletonScope();
            kernel.Rebind<IOrganistionDiffusionTopicManager>().To<OrganisationDiffusionTopicManager>().InSingletonScope();
            kernel.Rebind<IMarketTickerDiffusionTopicManager>().To<MarketTickerDiffusionTopicManager>().InSingletonScope();
            kernel.Rebind<INewsDiffusionTopicManager>().To<NewsDiffusionTopicManager>().InSingletonScope();
            kernel.Rebind<IHealthCheckDiffusionTopicManager>().To<HealthCheckDiffusionTopicManager>().InSingletonScope();
            kernel.Rebind<IAdminDiffusionTopicManager>().To<AdminDiffusionTopicManager>().InSingletonScope();
            kernel.Bind<IAomTopicManager>().To<AomTopicManager>().InSingletonScope();
            kernel.Rebind<IDiffusionSessionManager>().To<DiffusionSessionManager>().InSingletonScope();
            kernel.Rebind<IMessageDistributionService>().To<MessageDistributionService>();
            kernel.Rebind<IDiffusionProductTopicTreeBuilder>().To<DiffusionProductTopicTreeBuilder>().InSingletonScope();
            kernel.Rebind<IDiffusionOrderTopicTreeBuilder>().To<DiffusionOrderTopicTreeBuilder>().InSingletonScope();
            kernel.Rebind<IUserDistributionService>().To<UserDistributionService>();
            kernel.Rebind<IOrderRouterService>().To<OrderRouterService>();
            kernel.Rebind<IMarketTickerRouterService>().To<MarketTickerRouterService>();
            kernel.Rebind<IAssessmentRouterService>().To<AssessmentRouterService>();
            kernel.Rebind<INewsRouterService>().To<NewsRouterService>();
            kernel.Rebind<IProductsRouterService>().To<ProductsRouterService>();
            kernel.Rebind<IHealthCheckService>().To<HealthCheckRouterService>();
            kernel.Rebind<IAdminRouterService>().To<AdminRouterService>();
            kernel.Rebind<IOrganisationRouterService>().To<OrganisationRouterService>();
            kernel.Rebind<IExternalDealRouterService>().To<ExternalDealRouterService>();
            kernel.Rebind<IInternalDealRouterService>().To<InternalDealRouterService>();
            kernel.Rebind<IMarketInfoRouterService>().To<MarketInfoRouterService>();
            kernel.Bind<IClientSessionInfo>().To<ClientSessionInfo>();
            kernel.Rebind<IClientSessionMessageHandler>().To<ClientSessionMessageHandler>().InSingletonScope();
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}