﻿namespace AOM.ControlClient.Internals
{
    using AOM.Transport.Service.Processor.Common;

    internal class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get
            {
                return "AOMControlClient";
            }
        }

        public string DisplayName
        {
            get
            {
                return "AOM Diffusion Control Client";
            }
        }

        public string Description
        {
            get
            {
                return "AOM Diffusion Control Client";
            }
        }
    }
}