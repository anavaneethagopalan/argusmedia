﻿using Argus.Transport.Infrastructure;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;

namespace AOM.ControlClient.Interfaces
{
    public interface IAomClientMessageHandler : IMessageHandler
    {
        string TopicToListenOn { get; }

        IBus AomBus { set; }
    }
}