﻿namespace AOM.ControlClient.Interfaces
{
    using AOM.Transport.Events;

    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Session;

    public interface IClientSessionMessageHandler : ISendCallback
    {
        void AttachToSession(ISession session);

        void SendMessageThisSessionOnly(string currentSessionId, long userId, string content);

        void SendMessage(ClientSessionInfo csi, string content);

        void SendMessageToAllUserSessions(long userId, string content);

        void SendMessageToUserOtherSessions(string currentSessionId, long userId, string content);
    }
}