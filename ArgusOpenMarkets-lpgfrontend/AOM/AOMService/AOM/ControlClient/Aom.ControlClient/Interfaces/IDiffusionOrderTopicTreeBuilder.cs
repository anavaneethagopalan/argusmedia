﻿
namespace AOM.ControlClient.Interfaces
{
    public interface IDiffusionOrderTopicTreeBuilder
    {
        void BuildOrdersTree(long productId);
        void PurgeOrdersForProduct(long productId);
    }
}
