﻿using AOM.ControlClient.Authentication;
using PushTechnology.ClientInterface.Client.Security.Authentication;

namespace AOM.ControlClient.Interfaces
{
    public interface IAuthenticationCallbackService
    {
        void RegisterUserCallback(string username, IAuthenticationHandlerCallback callback);

        AuthenticationCallback GetUserAuthenticationCallback(string username);

        void Flush();
    }
}