﻿using AOM.App.Domain.Interfaces;
using AOM.ControlClient.Interfaces;
using PushTechnology.ClientInterface.Client.Session;

namespace AOM.ControlClient.Interfaces
{
    using System;

    using PushTechnology.ClientInterface.Client.Features.Control.Topics;

    public interface IMessageDistributionService : IMessageHandler, IRunnable
    {
        void AttachToSession(ISession session);

        event Action<object, string> MessageHandlerActive;

        event Action<object, string> MessageHandlerClosed;
    }
}