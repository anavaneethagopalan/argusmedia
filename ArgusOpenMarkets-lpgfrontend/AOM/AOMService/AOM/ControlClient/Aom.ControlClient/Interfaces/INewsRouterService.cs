﻿using AOM.App.Domain.Interfaces;

namespace AOM.ControlClient.Interfaces
{
    public interface INewsRouterService : IRouterServiceBase, IRunnable
    {

    }
}