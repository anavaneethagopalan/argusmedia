using AOM.Transport.Events;

namespace AOM.ControlClient.Interfaces
{
    public interface IRouterServiceBase
    {
        // TODO marksh: Why is this commented out? 
        // What is the point of the interface without it!?
        //
        // void RouteMessage(Message message);

        int SubscriberCount { get; }
    }
}