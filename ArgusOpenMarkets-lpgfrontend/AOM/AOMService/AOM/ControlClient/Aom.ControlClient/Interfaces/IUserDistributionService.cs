﻿using AOM.App.Domain.Interfaces;

namespace AOM.ControlClient.Interfaces
{
    public interface IUserDistributionService : IRunnable
    {
        void SendTerminateMessageToUser(long userId, string message);
        void SendTerminateMessageToUserSession(string currentSessionId, long userId, string message);
    }
}
