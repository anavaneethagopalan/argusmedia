﻿using AOM.App.Domain.Interfaces;

namespace AOM.ControlClient.Interfaces
{
    using AOM.Transport.Events;

    public interface IOrderRouterService : IRouterServiceBase, IRunnable
    {
        void RouteMessage(Message message);
    }
}