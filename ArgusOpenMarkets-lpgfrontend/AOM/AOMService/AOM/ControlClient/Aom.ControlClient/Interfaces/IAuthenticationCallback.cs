﻿using PushTechnology.ClientInterface.Client.Security.Authentication;

namespace AOM.ControlClient.Interfaces
{
    public interface IAuthenticationCallback
    {
        string Username { get; set; }

        IAuthenticationHandlerCallback Callback { get; set; }
    }
}