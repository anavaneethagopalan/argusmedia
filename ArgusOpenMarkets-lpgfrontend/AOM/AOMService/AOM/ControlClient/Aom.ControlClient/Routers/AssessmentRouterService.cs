﻿using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;
using AOM.TopicManager;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Routers
{
    public class AssessmentRouterService : RouterServiceBase, IAssessmentRouterService
    {
        public AssessmentRouterService(
            IBus aomBus,
            IProductsDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientMessageHandler)
            : base(aomBus, topicManager, clientMessageHandler)
        {
            _topicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("AssessmentRouterService Active");
                AddSubscribers(RunAndSubscribe());
            };
            _topicManager.StandBy += (sender, args) => DisposeSubscriptions();
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomNewAssessmentResponse>(SubscriptionId, ForwardAssessmentResponse);
            yield return _aomBus.Subscribe<AomAssessmentMessageResponse>(SubscriptionId, RouteMessage);
        }

        private void RouteMessage(AomAssessmentMessageResponse assessmentMessageResponse)
        {
            switch (assessmentMessageResponse.Message.MessageAction)
            {
                case MessageAction.Create:
                    CombinedAssessment assessmentCreate =
                        JsonConvert.DeserializeObject<CombinedAssessment>(assessmentMessageResponse.ObjectJson);
                    ForwardNewAssessmentRequest(assessmentMessageResponse.Message, assessmentCreate,
                        new AomNewAssessmentRequest());
                    break;
                case MessageAction.Update:
                    CombinedAssessment assessmentUpdate =
                        JsonConvert.DeserializeObject<CombinedAssessment>(assessmentMessageResponse.ObjectJson);
                    ForwardNewAssessmentRequest(assessmentMessageResponse.Message, assessmentUpdate,
                        new AomNewAssessmentRequest());
                    break;
                case MessageAction.Refresh:
                    var aomRefreshAssessmentRequest =
                        JsonConvert.DeserializeObject<AomRefreshAssessmentRequest>(assessmentMessageResponse.ObjectJson);
                    ForwardAssessmentRefreshRequest(assessmentMessageResponse.Message, aomRefreshAssessmentRequest);
                    break;
                case MessageAction.Clear:
                    var aomClearAssessmentRequest =
                        JsonConvert.DeserializeObject<AomClearAssessmentRequest>(assessmentMessageResponse.ObjectJson);
                    ForwardClearAssessmentRequest(assessmentMessageResponse.Message, aomClearAssessmentRequest);
                    break;
                default:
                    _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                    {
                        ErrorText = "Unknown Action Assessment",
                        Source = "RouteMessage"
                    }).ContinueWith(
                        t => SendErrorToClient(assessmentMessageResponse.Message, t.Result.UserErrorMessage));
                    break;
            }
        }

        private void ForwardClearAssessmentRequest(Message receivedMessage,
            AomClearAssessmentRequest aomClearAssessmentRequest)
        {
            Log.Debug(
                receivedMessage.MessageAction + " Assessment Clear request received " + receivedMessage.MessageBody);
            if (receivedMessage.MessageType != MessageType.Assessment)
            {
                var logUserError =
                    _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                    {
                        ErrorText = "Message type is not Assessment!",
                        Source = "AomClearAssessmentRequest"
                    });

                SendErrorToClient(
                    receivedMessage,
                    logUserError.UserErrorMessage);
            }
            try
            {
                _aomBus.Publish(aomClearAssessmentRequest);
            }
            catch (Exception error)
            {
                _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = error,
                    AdditionalInformation = receivedMessage.MessageAction + " assessment",
                    ErrorText = string.Empty,
                    Source = "AssessmentRouterService"
                });
            }
        }


        private void ForwardAssessmentRefreshRequest(Message receivedMsg,
            AomRefreshAssessmentRequest aomRefreshAssessmentRequest)
        {
            Log.Debug(receivedMsg.MessageAction + " Assessment refresh request received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Assessment)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    ErrorText = "Message type is not Assessment",
                    Source = "AomNewAssessmentRefresh"
                }).ContinueWith(t => SendErrorToClient(
                    receivedMsg,
                    t.Result.UserErrorMessage));
            }
            try
            {
                if (aomRefreshAssessmentRequest.ClientSessionInfo == null)
                {
                    aomRefreshAssessmentRequest.ClientSessionInfo = new ClientSessionInfo
                    {
                        UserId = aomRefreshAssessmentRequest.LastUpdatedUserId
                    };

                }
                _aomBus.Publish(aomRefreshAssessmentRequest);
            }
            catch (Exception error)
            {
                var logError = _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = error,
                    AdditionalInformation = receivedMsg.MessageAction + " assessment",
                    ErrorText = string.Empty,
                    Source = "AssessmentRouterService"
                });

                SendErrorToClient(receivedMsg, logError.UserErrorMessage);
            }
        }

        private void ForwardNewAssessmentRequest<T>(Message receivedMsg, CombinedAssessment combinedAssessment,
            T requestToSend)
            where T : AomNewAssessmentRequest
        {
            Log.Debug(receivedMsg.MessageAction + " Assessment info received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Assessment)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    ErrorText = "Message type is not Assessment!",
                    Source = "AomNewAssessmentInfoRequest"
                }).ContinueWith(t => SendErrorToClient(
                    receivedMsg,
                    t.Result.UserErrorMessage));
            }
            try
            {

                requestToSend.MessageAction = receivedMsg.MessageAction;
                if (receivedMsg.MessageAction == MessageAction.Create)
                {
                    requestToSend.Assessment = combinedAssessment;
                    requestToSend.ProductId = combinedAssessment.ProductId;
                    requestToSend.Assessment.Today.BusinessDate =
                        DateTime.SpecifyKind(requestToSend.Assessment.Today.BusinessDate, DateTimeKind.Utc);

                }
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = error,
                    AdditionalInformation = receivedMsg.MessageAction + " assessment",
                    ErrorText = string.Empty,
                    Source = "AssessmentRouterService"
                }).ContinueWith(t => SendErrorToClient(receivedMsg, t.Result.UserErrorMessage));
            }
        }

        private void ForwardAssessmentResponse<T>(T response) where T : AomNewAssessmentResponse
        {
            if (response.Message.MessageType == MessageType.Assessment)
            {
                try
                {
                    var assessmentTopicPath = AssessmentTopicManager.AssessmentTopicPath(response.ProductId);

                    var msg =
                        new ProductTopicData
                        {
                            ProductId = response.ProductId,
                            ProductTopicType = ProductTopicType.Assessment,
                            Content = response.Assessment
                        }.ToJsonCamelCase();

                    _topicManager.SendDataToTopic(string.Format(assessmentTopicPath), msg);
                }
                catch (Exception exception)
                {
                    _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                    {
                        Error = exception,
                        AdditionalInformation = "ERROR  ITEM TO CLIENT",
                        ErrorText = string.Empty,
                        Source = "AssessmentRouterService"
                    });
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
        }

        protected override string SubscriptionId
        {
            get
            {
                return "AssessmentRouterService";
            }
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("AssessmentRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        public void Disconnect()
        {
            DisposeSubscriptions();
        }
    }
}