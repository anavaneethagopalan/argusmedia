﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using AOM.App.Domain.Dates;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;
using AOM.Transport.Events.HealthCheck;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    public class HealthCheckRouterService : RouterServiceBase, IHealthCheckService
    {
        private Timer _healthCheckTimer;

        private readonly IDateTimeProvider _dateTimeProvider;

        private string _environmentMachineName;

        private const string ProcessorName = "Aom.ControlClient";

        private const int MaximumNumberSecondsToReturnSuccessfulHeartbeatPing = 120;

        private readonly Dictionary<string, ProcessorHealthCheckStatus> _listProcessorPingResults =
            new Dictionary<string, ProcessorHealthCheckStatus>();

        private bool _topicManagerActive = false;

        public HealthCheckRouterService(
            IBus aomBus,
            IHealthCheckDiffusionTopicManager topicManager,
            IDateTimeProvider dateTimeProvider)
            : base(aomBus, topicManager, null)

        {
            _dateTimeProvider = dateTimeProvider;

            _topicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("HealthCheckService Active");
                AddSubscribers(RunAndSubscribe());
                _topicManagerActive = true;
            };
            _topicManager.StandBy += (sender, args) => DisposeSubscriptions();
        }

        public string EnvironmentMachineName
        {
            get
            {
                return
                    _environmentMachineName =
                        String.IsNullOrEmpty(_environmentMachineName)
                            ? Environment.MachineName
                            : _environmentMachineName;
            }
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            _healthCheckTimer = new Timer(HealthCheckTimerTick, new object(), 0, 30000);
            // THis is subscribing to the response of the Processors and the Control client - so it can push data down a topic."
            yield return _aomBus.Subscribe<ProcessorHealthCheckStatus>(SubscriptionId, OnHealthCheckStatusHeartbeat);

            // This is subscribing to the request for each processor (+ Control Client) to respond to a health check. 
            // Note as only one control client is every connected to rabbit we don't use machinename for subscription id
            yield return _aomBus.Subscribe<ProcessorHealthCheck>(SubscriptionId, OnProcessorHealthCheck);
            // This is the response (RPC) to the web-api calling us for an immediate yes/no answer for the health status.
            _aomBus.Respond<HealthCheckStatusRpc, HealthCheckStatusResult>(WebApiHealthCheckStatusPing);

            _aomBus.Respond<AomProcessorStatusRpc, AomProcessorStatusResult>(GetAomProcessorStatus);
        }

        private AomProcessorStatusResult GetAomProcessorStatus(AomProcessorStatusRpc arg)
        {
            var processorStatusList = _listProcessorPingResults.Select(lpp => lpp.Value).ToList();
            return new AomProcessorStatusResult {ListProcessorHealthChecks = processorStatusList};
        }

        private void HealthCheckTimerTick(object data)
        {
            try
            {
                var newProcessorHealthCheckPing = new ProcessorHealthCheck();
                _aomBus.Publish(newProcessorHealthCheckPing);
            }
            catch (Exception ex)
            {
                Log.Error("Error Publishing a new processor health check onto the bus.", ex);
            }
        }

        private void OnProcessorHealthCheck(ProcessorHealthCheck obj)
        {
            _aomBus.Publish(
                new ProcessorHealthCheckStatus
                {
                    Processor = ProcessorName,
                    ProcessorFullName = GetProcessorFullName(),
                    Status = true,
                    MachineName = EnvironmentMachineName,
                    ResponseTime = DateTime.UtcNow,
                    ControlClientActive = _topicManagerActive
                });
        }

        private string GetProcessorFullName()
        {
            var assemblyName = string.Empty;
            try
            {
                assemblyName = Assembly.GetEntryAssembly().FullName;
            }
            catch (Exception)
            {
                assemblyName = "HealthCheckRouterService";
            }

            return assemblyName;
        }

        private void OnHealthCheckStatusHeartbeat(ProcessorHealthCheckStatus processorHealthCheck)
        {
            try
            {
                // Push this down the correct topic.
                if (processorHealthCheck != null)
                {
                    processorHealthCheck.ResponseTime = _dateTimeProvider.Now;
                    SaveProcessorPingResponse(processorHealthCheck);
                }
            }
            catch (Exception ex)
            {
                Log.Error("OnHealthCheckStatusHeartbeat", ex);
            }
        }

        private void SaveProcessorPingResponse(ProcessorHealthCheckStatus processorHealthCheckStatus)
        {
            if (_listProcessorPingResults.ContainsKey(processorHealthCheckStatus.Processor))
            {
                _listProcessorPingResults[processorHealthCheckStatus.Processor] = processorHealthCheckStatus;
            }
            else
            {
                _listProcessorPingResults.Add(processorHealthCheckStatus.Processor, processorHealthCheckStatus);
            }
        }

        private HealthCheckStatusResult WebApiHealthCheckStatusPing(HealthCheckStatusRpc arg)
        {
            try
            {

                bool pingTimesOk = true;

                foreach (var processor in _listProcessorPingResults)
                {
                    int numSeconds = (_dateTimeProvider.Now - processor.Value.ResponseTime).Seconds;

                    if (numSeconds > MaximumNumberSecondsToReturnSuccessfulHeartbeatPing)
                    {
                        Log.WarnFormat(
                            "Processor: {0} is unresponsive.  Number seconds since pinged:{1}",
                            processor,
                            numSeconds);

                        pingTimesOk = false;
                        break;
                    }
                }

                Log.InfoFormat(
                    "WebApiHealthCheckStatusPing.  Ping Ok: {0}   Number Processors Checked:{1}",
                    pingTimesOk,
                    _listProcessorPingResults.Count);

                return new HealthCheckStatusResult {HealthCheckStatusOk = pingTimesOk};

            }
            catch (Exception e)
            {
                Log.Error("WebApiHealthCheckStatusPing", e);
            }

            return new HealthCheckStatusResult {HealthCheckStatusOk = false};
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("HealthCheckRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        protected override string SubscriptionId
        {
            get { return "HealthCheckRouterService"; }
        }

        public void RouteMessage(Message message)
        {
        }

        public void Disconnect()
        {
            DisposeSubscriptions();
        }
    }
}