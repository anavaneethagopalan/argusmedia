﻿using AOM.TopicManager;
using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using PushTechnology.ClientInterface.Client.Factories;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    internal class OrderRouterService : RouterServiceBase, IOrderRouterService
    {
        private string _environmentMachineName;

        private readonly IDiffusionOrderTopicTreeBuilder _diffusionTopicTreeBuilder;

        private bool _topicManagerActive;

        public OrderRouterService(
            IBus aomBus,
            IOrdersDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientSessionHandler,
            IDiffusionOrderTopicTreeBuilder diffusionTopicTreeBuilder)
            : base(aomBus, topicManager, clientSessionHandler)
        {
            _diffusionTopicTreeBuilder = diffusionTopicTreeBuilder;

            _topicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("ORDER ROUTER SERVICE - TOPIC MANAGER HAS PUT US ACTIVE");
                AddSubscribers(RunAndSubscribe());
                _topicManagerActive = true;
            };
            _topicManager.StandBy += (sender, args) =>
            {
                Log.InfoFormat("ORDER ROUTER SERVICE - TOPIC MANAGER HAS PUT US INTO STANDBY MODE");
                DisposeSubscriptions();
                _topicManagerActive = false;
            };
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            Log.Debug("OrderRouterService - RunAndSubscribe");
            yield return _aomBus.Subscribe<AomOrderResponse>(SubscriptionId, ForwardOrderResponse);
            yield return _aomBus.Subscribe<AomOrderRefreshResponse>(SubscriptionId, ForwardOrderResponse);
            yield return _aomBus.Subscribe<PurgeOrdersInDiffusionRequest>(SubscriptionId, PurgeDiffusionOrders);
            yield return _aomBus.Subscribe<AomOrderMessageResponse>(SubscriptionId, RouteMessage);
        }

        private void RouteMessage(AomOrderMessageResponse aomOrderMessageResponse)
        {
            var message = aomOrderMessageResponse.Message;
            message.MessageBody = aomOrderMessageResponse.ObjectJson;
            ForwardOrderRequest(message, new AomOrderRequest());
        }

        public void RouteMessage(Message message)
        {
            ForwardOrderRequest(message, new AomOrderRequest());
        }

        private void PurgeDiffusionOrders(PurgeOrdersInDiffusionRequest purgeOrdersInDiffusionRequest)
        {
            if (_topicManagerActive)
            {
                Log.InfoFormat("TopicManagerActive : {0}", _topicManagerActive);
                try
                {
                    foreach (var productId in purgeOrdersInDiffusionRequest.ProductsToPurgeOrdersFor)
                    {
                        _diffusionTopicTreeBuilder.PurgeOrdersForProduct(productId);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Unknown Error", ex);
                }
            }
            else
            {
                Log.Info("OrderRouterService.PurgeDiffsionOrder - Topic Manager is NOT Active.");
            }
        }

        private void ForwardOrderRequest<T>(Message receivedMsg, T requestToSend) where T : AomOrderRequest
        {
            Log.Debug(receivedMsg.MessageAction + " Order Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Order)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                    {
                        ErrorText = "Message type is not Order!",
                        Source = "ForwardOrderRequest"
                    }).ContinueWith(t => SendErrorToClient(
                    receivedMsg,
                    t.Result.UserErrorMessage));

                return;
            }

            try
            {
                var order = JsonConvert.DeserializeObject<Order>(receivedMsg.MessageBody.ToString());
                requestToSend.Order = order;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);
            }
            catch (Exception error)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = error,
                    AdditionalInformation = receivedMsg.MessageAction + " order",
                    ErrorText = string.Empty,
                    Source = "OrderRouterService"
                }).ContinueWith(t => SendErrorToClient(receivedMsg, t.Result.UserErrorMessage));
            }
        }

        private void ForwardOrderResponse<T>(T response) where T : AomOrderResponse
        {
            if (!_topicManagerActive)
            {
                Log.Info("OrderRouterService - TopicManager is NOT ACTIVE.");
                return;
            }

            if (response == null || response.Message == null)
            {
                Log.ErrorFormat(
                    "Invalid response in OrderRouterService.ForwardOrderResponse, cannot send error to client");
                return;
            }

            if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
                return;
            }

            if (response.Order == null || response.Message.MessageType != MessageType.Order)
            {
                SendUnexpectedResponseErrorToClient(response);
                return;
            }

            Log.InfoFormat(
                "ForwardOrderResponse we are pushing the order down the topic.  Are we Active:{0}",
                _topicManagerActive);

            var order = response.Order;


            var notHeldTopicPath = OrderTopicManager.CreateAllOrdersTopicPathForOrder(order.ProductId, order.Id);
            var heldTopicPaths = OrderTopicManager.CreateHeldOrderTopicPaths(order.ProductId, order.ExecutionMode, order.Id, order.PrincipalOrganisationId, order.BrokerOrganisationId);

            switch (response.Order.OrderStatus)
            {
                case OrderStatus.Killed:
                case OrderStatus.Executed:
                    foreach (var htp in heldTopicPaths)
                    {
                        DeleteOrderTopics(htp, notHeldTopicPath);
                        
                    }
                    break;
                case OrderStatus.Held:
                    foreach (var htp in heldTopicPaths)
                    {
                        DeleteOrderTopics(htp, notHeldTopicPath);
                    }
                    CreateHeldOrder(response, heldTopicPaths);
                    break;
                case OrderStatus.Active:
                    foreach (var htp in heldTopicPaths)
                    {
                        DeleteOrderTopics(htp);
                    }
                    CreateNotHeldOrder(response, notHeldTopicPath);
                    break;
                default:
                    Log.DebugFormat("Ignoring OrderResponse with order status of {0}", response.Order.OrderStatus);
                    break;
            }
        }

        private void CreateNotHeldOrder(AomOrderResponse response, string notHeldTopicPath)
        {
            CreateOrderTopic(response, notHeldTopicPath);
        }

        private void CreateHeldOrder(AomOrderResponse response, List<string> heldOrderTopicPaths)
        {
            foreach (var topicPath in heldOrderTopicPaths)
            {
                CreateOrderTopic(response, topicPath);
            }
        }

        private void DeleteOrderTopics(params string[] paths)
        {
            foreach (var path in paths)
            {
                RemoveTopicAndSubTopics(path);
            }
        }

        private void CreateOrderTopic(AomResponse response, string topicPath)
        {
            var topicDetailsCallback = new TopicDetailsCallback();
            topicDetailsCallback.TopicDoesNotExist += (s, o) =>
            {
                var addTopicCallback = new TopicControlAddCallback();
                addTopicCallback.TopicAdded += (sender, s1) =>
                {
                    var callback = new TopicUpdateCallback(topicPath,
                                                           response.ToJsonCamelCase());
                    callback.OnUpdate +=
                        (sender1, s2) => Log.DebugFormat("TopicUpdateCallback.OnUpdate, parameter: " + s2);

                    callback.OnTopicUpdateError +=
                        (sender1, s2) => Log.InfoFormat("TopicUpdateCallback.OnTopicUpdateError, parameter: " + s2);

                    var jsonData = Diffusion.DataTypes.JSON.FromJSONString(response.ToJsonCamelCase());

                    _topicManager.GetUpdater()
                                 .PushToTopic(topicPath, jsonData, true, callback);
                };

                _topicManager.CreateTopic(topicPath, addTopicCallback);
            };

            topicDetailsCallback.TopicDetails +=
                (sender, s) =>
                {
                    var jsonData = Diffusion.DataTypes.JSON.FromJSONString(response.ToJsonCamelCase());

                    _topicManager.GetUpdater()
                        .PushToTopic(topicPath, jsonData, true, null);
                };

            _topicManager.GetTopicDetails(topicPath, topicDetailsCallback);
        }

        private void RemoveTopicAndSubTopics(string topicPath)
        {
            if (!string.IsNullOrEmpty(topicPath))
            {
                // We want to delete the sub-trees below the topics as well so append "/"
                var path = topicPath.EndsWith("/") ? topicPath : topicPath + "/";
                _topicManager.DeleteTopic(path, ">", new TopicControlDeleteCallback());
            }
        }

        private void SendUnexpectedResponseErrorToClient(AomResponse response)
        {
            _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
            {
                ErrorText = string.Format("Unexpected response from service --> {0}", response),
                Source = "OrderRouterSerice.ForwardOrderResponse"
            }).ContinueWith(t => SendErrorToClient(
                response.Message,
                t.Result.UserErrorMessage));
        }

        private new void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            Log.ErrorFormat("ERROR OrderRouterService.SendErrorToClient - sending a message to the user/session: {0} / {1} - Message: {2} - {3}", 
                receivedMessage.ClientSessionInfo.UserId, receivedMessage.ClientSessionInfo.SessionId, errorMessageText, receivedMessage.MessageBody);

            var errorResponse = new AomOrderResponse
            {
                Order = null,
                Message =
                    new Message
                    {
                        ClientSessionInfo = receivedMessage.ClientSessionInfo,
                        MessageAction = receivedMessage.MessageAction,
                        MessageType = MessageType.Error,
                        MessageBody = errorMessageText
                    }
            };

            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("OrderRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        public string EnvironmentMachineName
        {
            get
            {
                if (string.IsNullOrEmpty(_environmentMachineName))
                {
                    _environmentMachineName = Environment.MachineName;
                }

                return _environmentMachineName;
            }
        }

        protected override string SubscriptionId
        {
            get
            {
                return "OrderRouterService";
            }
        }

        public virtual string ForwardOrderSubscriptionId
        {
            get
            {
                return "OrderRouterService";
            }
        }


        public void Disconnect()
        {
            this.DisposeSubscriptions();
        }
    }
}