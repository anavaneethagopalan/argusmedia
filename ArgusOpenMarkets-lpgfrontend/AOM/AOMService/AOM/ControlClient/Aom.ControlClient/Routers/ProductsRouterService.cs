﻿using AOM.ControlClient.Interfaces;
using AOM.TopicManager;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Routers
{
    using AOM.Transport.Events.Products;
    using Argus.Transport.Infrastructure;
    using System;
    using System.Collections.Generic;
    using Utils.Javascript.Extension;
    using Utils.Logging.Utils;

    public class ProductsRouterService : RouterServiceBase, IProductsRouterService
    {
        public ProductsRouterService(
            IBus aomBus,
            IProductsDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientSessionMessageHandler)
            : base(aomBus, topicManager, clientSessionMessageHandler)
        {
            _topicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("ProductsRouterService Active");
                AddSubscribers(RunAndSubscribe());
            };
            _topicManager.StandBy += (sender, args) => DisposeSubscriptions();
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomMarketStatusChange>(SubscriptionId, ConsumeMarketStatusChange);
        }

        private void ConsumeMarketStatusChange(AomMarketStatusChange marketStatusChange)
        {
            var topic = ProductTopicManager.MarketStatusTopicPath(marketStatusChange.Product.ProductId);
            var message = new { MarketStatus = marketStatusChange.Product.Status, ProductName = marketStatusChange.Product.Name }.ToJsonCamelCase();

            _topicManager.SendDataToTopic(topic, message);
        }

        protected override string SubscriptionId
        {
            get
            {
                return "ProductRouterService";
            }
        }

        internal void DisposeSubscriptions()
        {
            Log.InfoFormat("ProductsRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        public void Disconnect()
        {
            this.DisposeSubscriptions();
        }
    }
}