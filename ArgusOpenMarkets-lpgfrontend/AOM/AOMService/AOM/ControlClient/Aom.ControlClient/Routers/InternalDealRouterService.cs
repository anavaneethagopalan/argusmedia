using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using AOMDiffusion.Common.Interfaces;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    public class InternalDealRouterService : RouterServiceBase, IInternalDealRouterService
    {
        public InternalDealRouterService(IBus aomBus,
            IMarketTickerDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientSessionMessageHandler)
            : base(aomBus, topicManager, clientSessionMessageHandler)
        {
            topicManager.Active += (sender, args) =>
            {
                Log.Info("InternalDealRouterService Active");
                AddSubscribers(RunAndSubscribe());
            };
            topicManager.StandBy += (sender, args) => DisposeSubscriptions();
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomDealResponse>(SubscriptionId, ForwardInternalDealResponse);
            yield return _aomBus.Subscribe<AomInternalDealMessageResponse>(SubscriptionId, RouteMessage);
        }

        private void RouteMessage(AomInternalDealMessageResponse aomInternalDealMessageResponse)
        {
            ForwardInternalDealRequest(aomInternalDealMessageResponse.Message, aomInternalDealMessageResponse.ObjectJson,
                new AomDealRequest());
        }

        private void ForwardInternalDealRequest<T>(Message receivedMsg, string dealJson, T requestToSend)
            where T : AomDealRequest
        {
            Log.Debug(receivedMsg.MessageAction + " Internal deal Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Deal)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    ErrorText = "Message type is not Deal!",
                    Source = "ForwardInternalDealRequest"
                }).ContinueWith(t =>
                    SendErrorToClient(
                        receivedMsg,
                        t.Result.UserErrorMessage));
            }
            try
            {
                var deal = JsonConvert.DeserializeObject<Deal>(dealJson);
                requestToSend.Deal = deal;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);
            }
            catch (Exception error)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = error,
                    AdditionalInformation = receivedMsg.MessageAction + " Internal deal",
                    ErrorText = string.Empty,
                    Source = "InternalDealRouterService"
                }).ContinueWith(t =>
                        SendErrorToClient(receivedMsg, t.Result.UserErrorMessage));
            }
        }

        private void ForwardInternalDealResponse<T>(T response) where T : AomDealResponse
        {
            if (response.Message.MessageType == MessageType.Deal)
            {
                try
                {
                    _clientMessageHandler.SendMessage(response.Message.ClientSessionInfo, response.ToJsonCamelCase());
                }
                catch (Exception exception)
                {
                    _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                    {
                        Error = exception,
                        AdditionalInformation = "ERROR SENDING AomDealResponse TO CLIENT",
                        ErrorText = string.Empty,
                        Source = "InternalDealRouterService"
                    });
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
            else
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    ErrorText = string.Format("Unexpected response from service --> {0}", response),
                    Source = "ForwardInternalDealResponse"
                }).ContinueWith(t => SendErrorToClient(
                    response.Message,
                    t.Result.UserErrorMessage));
            }
        }

        private new void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomDealResponse
            {
                Deal = null,
                Message =
                    new Message
                    {
                        ClientSessionInfo =
                            receivedMessage.ClientSessionInfo,
                        MessageAction = receivedMessage.MessageAction,
                        MessageType = MessageType.Error,
                        MessageBody = errorMessageText
                    }
            };
            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("InternalDealRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        protected override string SubscriptionId
        {
            get { return "InternalDealRouterService"; }
        }

        public void Disconnect()
        {
            DisposeSubscriptions();
        }
    }
}