﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Events.Users;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

using Argus.Transport.Infrastructure;
using Newtonsoft.Json;

namespace AOM.ControlClient.Routers
{
    public class OrganisationRouterService : RouterServiceBase, IOrganisationRouterService
    {
        private bool TopicManagerStandby = false;
        private const string PermissionsPath = "AOM/Organisations";

        public OrganisationRouterService(IBus aomBus, 
            IOrganistionDiffusionTopicManager topicManager, 
            IClientSessionMessageHandler clientMessageHandler)
            : base(aomBus, topicManager, clientMessageHandler)
        {
            _topicManager.Active += (sender, args) =>
            {
                if (TopicManagerStandby)
                {
                    // We have been in standby mode - then we've gone active.   Don't need to build the Sec Organisation.
                }
                else
                {
                    Log.Info(
                        "OrganisationRouterService - Starting in Ative mode - we need to build the sec organisation - passing True for Processor starting up.");
                    BuildSecOrganisations(true);
                }

            };
            _topicManager.StandBy += (sender, args) =>
            {
                TopicManagerStandby = true;
                DisposeSubscriptions();
            };
        }

        private void BuildSecOrganisations(bool processorStartingUp)
        {
            Log.InfoFormat("OrganisationRouterService Active");
            AddSubscribers(RunAndSubscribe());
            _aomBus.Publish(new GetCounterpartyPermissionsRefreshRequest { ProcessorStartingUp = processorStartingUp });
            _aomBus.Publish(new GetBrokerPermissionsRefreshRequest { ProcessorStartingUp = processorStartingUp });
            _aomBus.Publish(new GetCompanyDetailsRefreshRequest());
        }

        public void Run() { }

        public IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<GetCounterpartyPermissionsRefreshResponse>(SubscriptionId + "Summary", PushSummaryCounterpartyPermissions);
            yield return _aomBus.Subscribe<GetBrokerPermissionsRefreshResponse>(SubscriptionId + "Summary", PushSummaryBrokerPermissions);
            yield return _aomBus.Subscribe<OrganisationCreatedEvent>(SubscriptionId, OnOrganisationCreated);
            yield return _aomBus.Subscribe<OrganisationUpdatedEvent>(SubscriptionId, OnOrganisationUpdated);
            yield return _aomBus.Subscribe<CompanyDetailsSystemNotificationDeletedResponse>(SubscriptionId, OnSystemNotificationChanged);
            yield return _aomBus.Subscribe<GetCompanyDetailsRefreshResponse>(SubscriptionId, PushFullCompanyDetails);
            yield return _aomBus.Subscribe<AomOrganisationMessageResponse>(SubscriptionId, RouteMessage);
        }

        private void OnSystemNotificationChanged(CompanyDetailsSystemNotificationDeletedResponse systemNotificationDeleted)
        {
            var eventTypeId = (int) systemNotificationDeleted.EventType;

            var notificationTopic = string.Join("/", PermissionsPath, systemNotificationDeleted.OrganisationId, "SystemNotifications", eventTypeId, systemNotificationDeleted.Id);
            _topicManager.DeleteTopic(notificationTopic, ">", new TopicControlDeleteCallback());
        }

        private void OnOrganisationCreated(OrganisationCreatedEvent org)
        {
            BuildSecOrganisations(false);
        }

        private void OnOrganisationUpdated(OrganisationUpdatedEvent obj)
        {
            BuildSecOrganisations(false);
        }

        private static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[]) fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        private void PublishEventNotificationTypesToTopic(string topic)
        {
            foreach (var notificationType in (SystemEventType[]) Enum.GetValues(typeof(SystemEventType)))
            {
                _topicManager.SendDataToTopic(string.Join("/", topic, (int) notificationType),
                    new {Id = (int) notificationType, Description = GetEnumDescription(notificationType)}.ToJsonCamelCase());
            }
        }

        private void PublishSubscribedProductsToTopic(string topic, IEnumerable<SubscribedProduct> subscribedProducts)
        {
            if (subscribedProducts != null)
            {
                foreach (var sp in subscribedProducts)
                {
                    _topicManager.SendDataToTopic(string.Join("/", topic, sp.ProductId), sp.ToJsonCamelCase());
                }
            }
        }

        private void PushFullCompanyDetails(GetCompanyDetailsRefreshResponse obj)
        {
            foreach (var organisationDetails in obj.Details)
            {
                PublishEventNotificationTypesToTopic(string.Join("/", PermissionsPath, organisationDetails.Organisation.Id, "SystemNotificationTypes"));
                PublishSubscribedProductsToTopic(string.Join("/", PermissionsPath, organisationDetails.Organisation.Id, "SubscribedProducts"), organisationDetails.SubscribedProducts);
                PublishSystemNotificationToTopic(string.Join("/", PermissionsPath, organisationDetails.Organisation.Id, "SystemNotifications"), organisationDetails.SystemNotifications);
            }
        }

        private void PublishSystemNotificationToTopic(string path, IEnumerable<NotificationMapping> systemNotifications)
        {
            foreach (var sn in systemNotifications)
            {
                _topicManager.SendDataToTopic(string.Join("/", path, (int) sn.EventType, sn.Id),
                    new {sn.Id, sn.Product.Name, sn.EmailAddress, sn.EventType}.ToJsonCamelCase());
            }
        }

        internal string PermissionTopicPath(bool counterParty, long buySideOrgId, long productId, BuyOrSell buyOrSell, long sellSideOrgId)
        {
            return string.Format(counterParty ? "AOM/Organisations/{0}/PermissionsMatrix/Summary/Products/{1}/Principals/{2}/{3}" : 
                "AOM/Organisations/{0}/PermissionsMatrix/Summary/Products/{1}/Brokers/{2}/{3}", buySideOrgId, productId, buyOrSell, sellSideOrgId);
        }

        private void SendDataToTopic(string type, string topicPath, PermissionSummary permissionSummary, long orgId)
        {
            Log.InfoFormat("CREATE {0} SUMMARY TOPIC: {1}", type, topicPath);
            permissionSummary.OrganisationId = orgId;
            _topicManager.SendDataToTopic(topicPath, permissionSummary.ToJsonCamelCase());
        }

        private void DeleteTopic(string type, string topicPath)
        {
            Log.InfoFormat("DELETE {0} SUMMARY TOPIC: {1}", type, topicPath);
            _topicManager.DeleteTopic(topicPath, "*", new TopicControlDeleteCallback());
        }

        private void PushSummaryCounterpartyPermissions(GetCounterpartyPermissionsRefreshResponse response)
        {
            int createCounter = 0, deleteCounter = 0;

            bool permissionUpdated = false;
            foreach (var cpPerm in response.CounterpartyPermissions)
            {
                permissionUpdated = !cpPerm.PermissionUpdated.HasValue || cpPerm.PermissionUpdated.Value;

                if (permissionUpdated)
                {
                    var permissionSummary = new PermissionSummary()
                    {
                        Message = new Message { MessageType = MessageType.CounterpartyPermission },
                        CanTrade = true,
                        OrganisationId = cpPerm.BuySide.TheirOrganisation
                    };

                    var ourBuyPath = PermissionTopicPath(true, cpPerm.BuySide.OurOrganisation, cpPerm.ProductId, BuyOrSell.Buy, cpPerm.BuySide.TheirOrganisation);
                    var theirSellPath = PermissionTopicPath(true, cpPerm.SellSide.TheirOrganisation, cpPerm.ProductId, BuyOrSell.Sell, cpPerm.SellSide.OurOrganisation);
                    
                    if (cpPerm.BuySide.OurPermAllowOrDeny == Permission.Allow && cpPerm.SellSide.TheirPermAllowOrDeny == Permission.Allow)
                    {
                        SendDataToTopic("COUNTERPARTY", ourBuyPath, permissionSummary, cpPerm.BuySide.TheirOrganisation);
                        createCounter++;
                        SendDataToTopic("COUNTERPARTY", theirSellPath, permissionSummary, cpPerm.SellSide.OurOrganisation);
                        createCounter++;
                    }
                    if (!response.ProcessorStartingUp)
                    {
                        if (cpPerm.BuySide.OurPermAllowOrDeny == Permission.Deny ||
                            cpPerm.BuySide.OurPermAllowOrDeny == Permission.NotSet ||
                            cpPerm.SellSide.TheirPermAllowOrDeny == Permission.Deny ||
                            cpPerm.SellSide.TheirPermAllowOrDeny == Permission.NotSet)
                        {
                            DeleteTopic("COUNTERPARTY", ourBuyPath);
                            deleteCounter++;
                            DeleteTopic("COUNTERPARTY", theirSellPath);
                            deleteCounter++;
                        }
                    }


                    var ourSellPath = PermissionTopicPath(true, cpPerm.SellSide.OurOrganisation, cpPerm.ProductId, BuyOrSell.Sell, cpPerm.SellSide.TheirOrganisation);
                    var theirBuyPath = PermissionTopicPath(true, cpPerm.BuySide.TheirOrganisation, cpPerm.ProductId, BuyOrSell.Buy, cpPerm.BuySide.OurOrganisation);

                    if (cpPerm.SellSide.OurPermAllowOrDeny == Permission.Allow && cpPerm.BuySide.TheirPermAllowOrDeny == Permission.Allow)
                    {
                        SendDataToTopic("COUNTERPARTY", ourSellPath, permissionSummary, cpPerm.SellSide.TheirOrganisation);
                        createCounter++;
                        SendDataToTopic("COUNTERPARTY", theirBuyPath, permissionSummary, cpPerm.BuySide.OurOrganisation);
                        createCounter++;
                    }

                    if (!response.ProcessorStartingUp)
                    {
                        if (cpPerm.SellSide.OurPermAllowOrDeny == Permission.Deny ||
                            cpPerm.SellSide.OurPermAllowOrDeny == Permission.NotSet ||
                            cpPerm.BuySide.TheirPermAllowOrDeny == Permission.Deny ||
                            cpPerm.BuySide.TheirPermAllowOrDeny == Permission.NotSet)
                        {
                            DeleteTopic("COUNTERPARTY", ourSellPath);
                            deleteCounter++;
                            DeleteTopic("COUNTERPARTY", theirBuyPath);
                            deleteCounter++;
                        }
                    }
                }
            }

            Log.InfoFormat("OrganisationRouterService - COUNTERPARTY PERMISSION MATRIX TOPICS CREATED: {0} & DELETED: {1}", createCounter, deleteCounter);
        }

        private void PushSummaryBrokerPermissions(GetBrokerPermissionsRefreshResponse response)
        {
            int createCounter = 0, deleteCounter = 0;

            foreach (var brokPerm in response.BrokerPermissions)
            {
                var permissionSummary = new PermissionSummary()
                {
                    Message = new Message { MessageType = MessageType.BrokerPermission },
                    CanTrade = true,
                    OrganisationId = brokPerm.BuySide.TheirOrganisation
                };

                var ourBuyPath = PermissionTopicPath(false, brokPerm.BuySide.OurOrganisation, brokPerm.ProductId, BuyOrSell.Buy, brokPerm.BuySide.TheirOrganisation);
                var theirSellPath = PermissionTopicPath(false, brokPerm.SellSide.TheirOrganisation, brokPerm.ProductId, BuyOrSell.Sell, brokPerm.SellSide.OurOrganisation);

                if (brokPerm.BuySide.OurPermAllowOrDeny == Permission.Allow && brokPerm.SellSide.TheirPermAllowOrDeny == Permission.Allow)
                {
                    SendDataToTopic("BROKER", ourBuyPath, permissionSummary, brokPerm.BuySide.TheirOrganisation);
                    createCounter++;
                    SendDataToTopic("BROKER", theirSellPath, permissionSummary, brokPerm.SellSide.OurOrganisation);
                    createCounter++;
                }


                if (!response.ProcessorStartingUp && (brokPerm.BuySide.OurPermAllowOrDeny == Permission.Deny 
                    || brokPerm.SellSide.TheirPermAllowOrDeny == Permission.Deny 
                    || brokPerm.BuySide.OurPermAllowOrDeny == Permission.NotSet 
                    || brokPerm.SellSide.TheirPermAllowOrDeny == Permission.NotSet))
                {
                    DeleteTopic("BROKER", ourBuyPath);
                    deleteCounter++;
                    DeleteTopic("BROKER", theirSellPath);
                    deleteCounter++;
                }

                var ourSellPath = PermissionTopicPath(false, brokPerm.SellSide.OurOrganisation, brokPerm.ProductId, BuyOrSell.Sell, brokPerm.SellSide.TheirOrganisation);
                var theirBuyPath = PermissionTopicPath(false, brokPerm.BuySide.TheirOrganisation, brokPerm.ProductId, BuyOrSell.Buy, brokPerm.BuySide.OurOrganisation);

                if (brokPerm.SellSide.OurPermAllowOrDeny == Permission.Allow && brokPerm.BuySide.TheirPermAllowOrDeny == Permission.Allow)
                {
                    SendDataToTopic("BROKER", ourSellPath, permissionSummary, brokPerm.SellSide.TheirOrganisation);
                    createCounter++;
                    SendDataToTopic("BROKER", theirBuyPath, permissionSummary, brokPerm.SellSide.OurOrganisation);
                    createCounter++;
                }

                if (!response.ProcessorStartingUp)
                {
                    if (brokPerm.SellSide.OurPermAllowOrDeny == Permission.Deny ||
                        brokPerm.BuySide.TheirPermAllowOrDeny == Permission.Deny ||
                        brokPerm.SellSide.OurPermAllowOrDeny == Permission.NotSet || 
                        brokPerm.SellSide.TheirPermAllowOrDeny == Permission.NotSet)
                    {
                        DeleteTopic("BROKER", ourSellPath);
                        deleteCounter++;
                        DeleteTopic("BROKER", theirBuyPath);
                        deleteCounter++;
                    }
                }
            }
            Log.InfoFormat("OrganisationRouterService - BROKER PERMISSION MATRIX TOPICS CREATED: {0} & DELETED: {1}", createCounter, deleteCounter);
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("OrganisationRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        protected override string SubscriptionId
        {
            get { return "OrganisationRouterService"; }
        }

        private void RouteMessage(AomOrganisationMessageResponse organisationMessageResponse)
        {
            switch (organisationMessageResponse.Message.MessageType)
            {
                //case MessageType.CounterpartyPermission:
                //    ForwardCounterpartyPermissionRequest(organisationMessageResponse.Message, organisationMessageResponse.ObjectJson, new AomCounterpartyPermissionRequest());
                //    break;
                //case MessageType.BrokerPermission:
                //    ForwardBrokerPermissionRequest(organisationMessageResponse.Message, organisationMessageResponse.ObjectJson, new AomBrokerPermissionRequest());
                //    break;
                case MessageType.OrganisationNotificationDetail:
                    ForwardOrganisationNotificationRequest(organisationMessageResponse.Message, organisationMessageResponse.ObjectJson, new CompanyDetailsRpcRequest());
                    break;
            }
        }

        private void ForwardOrganisationNotificationRequest<T>(Message message, string companyJson, T requestToSend) where T : CompanyDetailsRpcRequest
        {
            Log.Debug(message.MessageAction + " CompanyDetailsRequest Received " + message.MessageBody);

            if (message.MessageType != MessageType.OrganisationNotificationDetail)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(
                    new LogUserErrorRequestRpc
                    {
                        ErrorText = "Message type is not OrganisationNotificationDetail!",
                        Source = "ForwardOrganisationNotificationRequest"
                    }).ContinueWith(t => SendErrorToClient(message, t.Result.UserErrorMessage));
            }
            try
            {
                requestToSend.ClientSessionInfo = message.ClientSessionInfo;
                requestToSend.MessageAction = message.MessageAction;
                var companyDetailsRpcRequest = JsonConvert.DeserializeObject<CompanyDetailsRpcRequest>(companyJson);

                var companyDetailsSystemNotification = new CompanyDetailsSystemNotificationRequest
                {
                    ClientSessionInfo = message.ClientSessionInfo,
                    MessageAction = message.MessageAction
                };

                companyDetailsSystemNotification.Id = companyDetailsRpcRequest.Id;
                companyDetailsSystemNotification.ProductId = companyDetailsRpcRequest.ProductId;
                companyDetailsSystemNotification.EmailAddress = companyDetailsRpcRequest.EmailAddress;
                companyDetailsSystemNotification.EventType = companyDetailsRpcRequest.EventType;

                _aomBus.Publish(companyDetailsSystemNotification);
            }
            catch (Exception error)
            {
                _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = error,
                    AdditionalInformation = message.MessageAction + " CounterpartyPermission",
                    ErrorText = string.Empty,
                    Source = "OrganisationRouterService"
                });
            }
        }

        public void Disconnect()
        {
            DisposeSubscriptions();
        }

    }
}