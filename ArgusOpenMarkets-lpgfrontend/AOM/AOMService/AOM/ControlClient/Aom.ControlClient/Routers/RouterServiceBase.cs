﻿using System;
using System.Collections.Generic;
using AOMDiffusion.Common.Interfaces;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    using AOM.ControlClient.Interfaces;
    using AOM.Transport.Events;

    using Argus.Transport.Infrastructure;

    using Utils.Javascript.Extension;

    public abstract class RouterServiceBase : IRouterServiceBase
    {
        protected readonly IBus _aomBus;
        protected readonly IAomDiffusionTopicManager _topicManager;
        protected readonly IClientSessionMessageHandler _clientMessageHandler;
        protected abstract string SubscriptionId { get; }

        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();
        private bool _disposed;

        protected RouterServiceBase(IBus aomBus, IAomDiffusionTopicManager topicManager, IClientSessionMessageHandler clientMessageHandler)
        {
            _aomBus = aomBus;
            _topicManager = topicManager;
            _clientMessageHandler = clientMessageHandler;
        }

        protected void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomResponse()
            {
                Message = new Message
                {
                    ClientSessionInfo = receivedMessage.ClientSessionInfo,
                    MessageAction = receivedMessage.MessageAction,
                    MessageType = MessageType.Error,
                    MessageBody = errorMessageText
                }
            };

            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        protected void AddSubscribers(IEnumerable<IDisposable> subs)
        {
            _subscriptionHandlers.AddRange(subs);
        }

        protected void DisposedAndClearSubscribers()
        {
            foreach (var subscriptionHandler in _subscriptionHandlers)
            {
                if (subscriptionHandler != null)
                {
                    try
                    {
                        subscriptionHandler.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("{0} - Error Disposing Subscription : {1}.  Error:{2}", SubscriptionId, subscriptionHandler, ex);
                    }                    
                }
            }
            _subscriptionHandlers.Clear();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DisposedAndClearSubscribers();
                }
            }
            _disposed = true;
        }

        
        public int SubscriberCount
        {
            get
            {
                return _subscriptionHandlers.Count;
            }
        }
    }
}