﻿using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient
{
    using AOM.ControlClient.Interfaces;
    using AOM.ControlClient.Routers;
    using AOM.Transport.Events.System;

    using Argus.Transport.Infrastructure;

    using System;
    using System.Collections.Generic;

    using Utils.Logging.Utils;

    public class AdminRouterService : RouterServiceBase, IAdminRouterService
    {
        public AdminRouterService(
            IBus aomBus,
            IAdminDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientMessageHandler)
            : base(aomBus, topicManager, clientMessageHandler)
        {
            _topicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("AdminRouterService - Active");
                AddSubscribers(RunAndSubscribe());
            };
            _topicManager.StandBy += (sender, args) => DisposeSubscriptions();
        }

        public void Run()
        {

        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<DeleteDiffusionTopicRequest>(SubscriptionId, OnDeleteDiffusionTopicRequest);
        }

        private void OnDeleteDiffusionTopicRequest(DeleteDiffusionTopicRequest diffusionTopicDeleteRequest)
        {
            var topicToDelete = diffusionTopicDeleteRequest.TopicToDelete;
            if (DeleteTopic(diffusionTopicDeleteRequest.TopicToDelete, diffusionTopicDeleteRequest.LoggedOnUserId))
            {
                var deleteCallback = new TopicControlDeleteCallback(topicToDelete);
                deleteCallback.TopicRemoved += (sender, s) =>
                {
                    Log.InfoFormat("OnDeleteDiffusionTopicRequest - Topic {0} has been deleted.", s);
                };

                Log.InfoFormat("OnDeleteDiffusionTopicRequest - Deleting Topic: {0}", topicToDelete);
                _topicManager.DeleteTopic(topicToDelete, ">", deleteCallback);
            }
        }

        private bool DeleteTopic(string topicToDelete, long loggedOnUserId)
        {
            bool deleteTopic = !string.IsNullOrEmpty(topicToDelete);

            if (topicToDelete.ToLower() == "aom")
            {
                Log.ErrorFormat(
                    "User Id:{0} attempted to delete the Root AOM topic.  This is NOT allowed.  Permissions should be revoked for this user.",
                    loggedOnUserId);

                deleteTopic = false;
            }

            return deleteTopic;
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("AdminRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        protected override string SubscriptionId
        {
            get
            {
                return "AdminRouterService";
            }
        }

        public void Disconnect()
        {
            DisposeSubscriptions();
        }
    }
}