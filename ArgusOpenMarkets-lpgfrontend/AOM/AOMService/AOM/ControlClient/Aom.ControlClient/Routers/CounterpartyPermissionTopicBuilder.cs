﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.Diffusion.Common;
using AOM.Diffusion.Common.Interfaces;
using AOM.Repository.MySql.Crm;
using AOM.TopicManager;
using AOM.Transport.Events;
using AOM.Transport.Events.Organisations;
using Utils.Extension;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    public interface ICounterpartyPermissionTopicBuilder
    {
        void PushFullCounterpartyPermissionsDownDetailTopics(GetCounterpartyPermissionsRefreshResponse counterpartyPermissionsRefreshResponse, bool controlClientActive);

        long CreateConflatedBrokerCounterpartyDetailTopics(IList<CounterpartyPermissionDto> brokersOrCounterparties,
            IList<long> organisationsAffected, bool brokerTopics, bool controlClientActive);
    }

    public class CounterpartyPermissionTopicBuilder : ICounterpartyPermissionTopicBuilder
    {
        private readonly IOrganistionDiffusionTopicManager _topicManager;
        private readonly ITopicReduction _topicReduction;

        public CounterpartyPermissionTopicBuilder(IOrganistionDiffusionTopicManager topicManager, 
            ITopicReduction topicReduction)
        {
            _topicManager = topicManager;
            _topicReduction = topicReduction;
        }

        public  void PushFullCounterpartyPermissionsDownDetailTopics(
            GetCounterpartyPermissionsRefreshResponse counterpartyPermissionsRefreshResponse, bool controlClientActive)
        {
            Log.InfoFormat("X-PERMS PFCPDD for Prod Id:{0} Starting", counterpartyPermissionsRefreshResponse.AllCounterpartyPermissions[0].ProductId);
            var sw = new Stopwatch();
            sw.Start();

            // NOTE: WE ARE ALWAYS PUSHING CONFLATED DETAIL TOPICS DOWN THE TREE.
            var topicsCreatedInDiffusion =
                CreateConflatedBrokerCounterpartyDetailTopics(
                    counterpartyPermissionsRefreshResponse.AllCounterpartyPermissions, new List<long>(), false,
                    controlClientActive);
            sw.Stop();

            Log.InfoFormat(
                "X-PERMS PFCPDD for Prod Id:{0}  took: {1} milliseconds for {2} counterpartyPermissions topics creaqted: {3}",
                counterpartyPermissionsRefreshResponse.AllCounterpartyPermissions[0].ProductId, sw.ElapsedMilliseconds, counterpartyPermissionsRefreshResponse.AllCounterpartyPermissions.Count, topicsCreatedInDiffusion);
        }

        public long CreateConflatedBrokerCounterpartyDetailTopics(
            IList<CounterpartyPermissionDto> brokersOrCounterparties,
            IList<long> organisationsAffected,
            bool brokerTopics, 
            bool controlClientActive)
        {
            long topicsCreated = 0;
            topicsCreated = _topicReduction.ReduceBrokerCounterpartyTopics(brokersOrCounterparties, brokerTopics, organisationsAffected, controlClientActive);

            // OK - SO HERE'S THE COMPARISON IN THE TOPICS WERE CREATING
            Log.InfoFormat("FOR DETAIL TOPICS - OLD METHOD NUMBER TOPICS: {0}   NEW METHOD NUMBER TOPICS: {1}",
                brokersOrCounterparties.Count, topicsCreated);
            return topicsCreated;
        }

        private bool HasOrganisationBeenAffected(long orgId, IList<long> organisationsAffected)
        {
            if (organisationsAffected == null)
            {
                return true;
            }

            if (organisationsAffected.Count == 0)
            {
                return true;
            }

            return organisationsAffected.Contains(orgId);
        }
    }
}
