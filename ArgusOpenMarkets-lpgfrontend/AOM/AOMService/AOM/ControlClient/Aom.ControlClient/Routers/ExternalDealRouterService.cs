using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;
using AOM.Transport.Events.ExternalDeals;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using AOMDiffusion.Common.Interfaces;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    public class ExternalDealRouterService : RouterServiceBase, IExternalDealRouterService
    {

        public ExternalDealRouterService(IBus aomBus,
            IMarketTickerDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientSessionMessageHandler)
            : base(aomBus, topicManager, clientSessionMessageHandler)
        {
            topicManager.Active += (sender, args) =>
            {
                Log.Info("ExternalDealRouterService Active");
                AddSubscribers(RunAndSubscribe());
            };
            topicManager.StandBy += (sender, args) => DisposeSubscriptions();
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomExternalDealResponse>(SubscriptionId, ForwardExternalDealResponse);
            yield return _aomBus.Subscribe<AomExternalDealMessageResponse>(SubscriptionId, RouteMessage);
        }

        private void RouteMessage(AomExternalDealMessageResponse externalDealMessageResponse)
        {
            ForwardExternalDealRequest(externalDealMessageResponse.Message, externalDealMessageResponse.ExternalDeal,
                new AomExternalDealRequest());
        }

        private void ForwardExternalDealRequest<T>(Message receivedMsg, ExternalDeal externalDeal, T requestToSend)
            where T : AomExternalDealRequest
        {
            Log.Debug(receivedMsg.MessageAction + " External deal Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.ExternalDeal)
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    ErrorText = "Message type is not ExternalDeal!",
                    Source = "ForwardExternalDealRequest"
                }).ContinueWith(t => SendErrorToClient(
                    receivedMsg,
                    t.Result.UserErrorMessage));
            }
            try
            {
                // var externalDeal = JsonConvert.DeserializeObject<ExternalDeal>(externalDealJson);
                requestToSend.ExternalDeal = externalDeal;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);
            }
            catch (Exception error)
            {
                _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = error,
                    AdditionalInformation = receivedMsg.MessageAction + " external deal",
                    ErrorText = string.Empty,
                    Source = "InternalDealRouterService"
                });
            }
        }

        private void ForwardExternalDealResponse<T>(T response) where T : AomExternalDealResponse
        {
            if (response.Message.MessageType == MessageType.ExternalDeal)
            {
                try
                {
                    _clientMessageHandler.SendMessage(response.Message.ClientSessionInfo, response.ToJsonCamelCase());
                }
                catch (Exception exception)
                {
                    _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                    {
                        Error = exception,
                        AdditionalInformation = "ERROR SENDING AomExternalDealResponse TO CLIENT",
                        ErrorText = string.Empty,
                        Source = "ExternalDealRouterService"
                    });
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
            else
            {
                _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    ErrorText = string.Format("Unexpected response from service --> {0}", response),
                    Source = "ForwardExternalDealResponse"
                }).ContinueWith(t => SendErrorToClient(
                    response.Message,
                    t.Result.UserErrorMessage));
            }
        }

        private void SendErrorToClient(Message receivedMessage, LogUserErrorResponseRpc logUserErrorResponse)
        {

            var errorMessageText = string.Empty;
            if (logUserErrorResponse != null)
            {
                errorMessageText = logUserErrorResponse.UserErrorMessage;
            }

            var errorResponse = new AomExternalDealResponse
            {
                ExternalDeal = null,
                Message =
                    new Message
                    {
                        ClientSessionInfo =
                            receivedMessage.ClientSessionInfo,
                        MessageAction =
                            receivedMessage.MessageAction,
                        MessageType = MessageType.Error,
                        MessageBody = errorMessageText
                    }
            };
            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        protected override string SubscriptionId
        {
            get { return "ExternalDealRouterService"; }
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("ExternalDealRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        public void Disconnect()
        {
            DisposeSubscriptions();
        }
    }
}