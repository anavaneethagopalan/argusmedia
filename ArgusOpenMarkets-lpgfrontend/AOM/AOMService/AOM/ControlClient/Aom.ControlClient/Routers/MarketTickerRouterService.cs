﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.ControlClient.Helpers;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using AOM.TopicManager;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    public class MarketTickerRouterService : RouterServiceBase, IMarketTickerRouterService
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        private bool _marketTickerStandby;

        public MarketTickerRouterService(
            IBus aomBus,
            IMarketTickerDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientSessionMessageHandler,
            IDateTimeProvider dateTimeProvider)
            : base(aomBus, topicManager, clientSessionMessageHandler)
        {
            _dateTimeProvider = dateTimeProvider;
            _topicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("MarketTickerRouterService Active");
                AddSubscribers(RunAndSubscribe());

                if (_marketTickerStandby == false)
                {
                    // Only rebuild the market ticker content if we've NOT been in Standby mode.
                    RebuildMarketTickerContent(1);
                }
            };
            _topicManager.StandBy += (sender, args) =>
            {
                _marketTickerStandby = true;
                DisposeSubscriptions();
            };
        }

        public void Run()
        {
        }

        private void RebuildMarketTickerContent(int daysToPopulate)
        {
            Log.InfoFormat("Rebuilding MarketTicker Content for {0} days", daysToPopulate);

            try
            {
                var refreshMessage =
                    new AomRefreshMarketTickerRequest
                    {
                        RefreshFromTime = _dateTimeProvider.Today.AddWeekDays(-daysToPopulate),
                        RefreshToTime = _dateTimeProvider.Today.AddWeekDays(1)
                    };

                _aomBus.Publish(refreshMessage);
            }
            catch (Exception ex)
            {
                Log.Error("ControlClientControlle.RebuildMarketTickerContent", ex);
            }
        }

        public IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomNewMarketTickResponse>(SubscriptionId, ForwardMarketTickerResponse);
            yield return _aomBus.Subscribe<PurgeMarketTickerResponse>(SubscriptionId, ForwardPurgeMarketTickerResponse);
            yield return _aomBus.Subscribe<AomMarketTickerItemMessageResponse>(SubscriptionId, RouteMessage);

            var fetchCallback = new FetchCallback();
            fetchCallback.TopicDataFetched += (o, s1) => _aomBus.Subscribe<AomNewMarketTickResponse>(SubscriptionId, ForwardMarketTickerResponse);

            fetchCallback.NoDataFetched += (sender, s) =>
            {
                _aomBus.Subscribe<AomNewMarketTickResponse>(SubscriptionId, ForwardMarketTickerResponse);
            };

        }

        private void ForwardPurgeMarketTickerResponse(PurgeMarketTickerResponse response)
        {
            var topicDeleteCallback = new TopicControlDeleteCallback();
            topicDeleteCallback.TopicRemoved += (o, s) =>
            {
                Log.Debug(string.Format("Removed Market Ticker Topic: {0}:{1}", s, o.ToString()));
            };

            _topicManager.DeleteTopic(MarketTickerTopicManager.GetRootMarketTickerTopic(), ">", topicDeleteCallback);

            var daysToKeep = (response.DaysToKeep > 0) ? (response.DaysToKeep*-1) : response.DaysToKeep;
            DateTime fromDateTime = _dateTimeProvider.Today.AddDays(daysToKeep);


            var refreshMessage = new AomRefreshMarketTickerRequest
            {
                RefreshFromTime = fromDateTime,
                RefreshToTime = _dateTimeProvider.Today.AddDays(1)
            };

            _aomBus.Publish(refreshMessage);
        }

        private void RouteMessage(AomMarketTickerItemMessageResponse marketTickerItemMessageResponse)
        {
            switch (marketTickerItemMessageResponse.Message.MessageAction)
            {
                default:
                    _aomBus.RequestAsync<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                        {
                            ErrorText = "Unknown Action On MarketTicker",
                            Source = "RouteMessage"
                        })
                        .ContinueWith(
                            t => SendErrorToClient(marketTickerItemMessageResponse.Message, t.Result.UserErrorMessage));
                    break;
            }
        }

        private void ForwardMarketTickerResponse<T>(T response) where T : AomNewMarketTickResponse
        {
            try
            {
                MarketTickerItemStatus marketTickerStatus = response.MarketTickerItem.MarketTickerItemStatus;
                if (response.MarketTickerItem.MarketTickerItemType == MarketTickerItemType.Deal
                    || response.MarketTickerItem.MarketTickerItemType == MarketTickerItemType.Info)
                {
                    DeleteOwnOrgMarketTickerItem(response);
                }
                if (response.MarketTickerItem.Message.StartsWith("PENDING")
                    && marketTickerStatus != MarketTickerItemStatus.Pending)
                {
                    return;
                }
                if (marketTickerStatus.Equals(MarketTickerItemStatus.Pending)
                    || marketTickerStatus.Equals(MarketTickerItemStatus.Held)
                    || response.MarketTickerItem.ShouldSendNonPendingOnlyToOwnerOrganisations(response.AdditionalData))
                {
                    PushOwnOrgMarketTickerItem(response);
                }
                else
                {
                    PushAllOrgMarketTickerItem(response);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("ForwardMarketTickerResponse - Error: {0}", ex);
            }
        }

        private void DeleteOwnOrgMarketTickerItem(AomNewMarketTickResponse newMarketTickerResponse)
        {
            var ownOrgTopicsToDelete =
                MarketTickerTopicManager.GetOwnOrganisationTopicsToDelete(newMarketTickerResponse.MarketTickerItem.Id, newMarketTickerResponse.MarketTickerItem.ProductIds, newMarketTickerResponse.MarketTickerItem.OwnerOrganisations);

            foreach (var ownOrgTopicToDelete in ownOrgTopicsToDelete)
            {
                _topicManager.DeleteTopic(ownOrgTopicToDelete,
                    "*",
                    new TopicControlDeleteCallback());
            }
        }



        private void PushAllOrgMarketTickerItem<T>(T response) where T : AomNewMarketTickResponse
        {
            var topicsToSendDataTo = new List<string>();
            TopicUpdateCallback callback = new TopicUpdateCallback();
            try
            {
                topicsToSendDataTo =
                    MarketTickerTopicManager.CreateTopicsForAllOrgMarketTickerItem(response.MarketTickerItem.Id,
                        response.MarketTickerItem.ProductIds, response.MarketTickerItem.MarketTickerItemType).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(
                    string.Format(
                        "Unable to PushNonPendingMarketTickerItem for {0} [id={1}]",
                        response.MarketTickerItem.MarketTickerItemType,
                        response.MarketTickerItem.Id),
                    ex);
            }

            foreach (var topic in topicsToSendDataTo)
            {
                _topicManager.SendDataToTopic(topic, response.ToJsonCamelCase(), callback);
            }
        }

        //This should be called for
        //void external deal that was pending
        //void info / pending info / held order / pending deal
        private void PushOwnOrgMarketTickerItem<T>(T response) where T : AomNewMarketTickResponse
        {
            var callback = new TopicUpdateCallback();
            MarketTickerItem marketTickerItem = response.MarketTickerItem;

            var topics = MarketTickerTopicManager.CreateOwnOrganisationMarketTickerTopics(marketTickerItem);
            foreach (var topic in topics)
            {
                _topicManager.SendDataToTopic(topic, response.ToJsonCamelCase(), callback);
            }
        }

        private new void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomNewMarketTickResponse
            {
                MarketTickerItem = null,
                Message =
                    new Message
                    {
                        ClientSessionInfo =
                            receivedMessage.ClientSessionInfo,
                        MessageAction =
                            receivedMessage.MessageAction,
                        MessageType = MessageType.Error,
                        MessageBody = errorMessageText
                    }
            };
            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("MarketTickerRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }


        protected override string SubscriptionId
        {
            get { return "MarketTickerRouterService"; }
        }

        public void Disconnect()
        {
            this.DisposeSubscriptions();
        }
    }
}