﻿using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;
using Argus.Transport.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using AOMDiffusion.Common.Interfaces;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    public class MarketInfoRouterService : RouterServiceBase, IMarketInfoRouterService
    {
        private readonly IErrorService _errorService;

        public MarketInfoRouterService(IBus aomBus,
            IMarketTickerDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientSessionMessageHandler,
            IErrorService errorService)
            : base(aomBus, topicManager, clientSessionMessageHandler)
        {
            _errorService = errorService;
            topicManager.Active += (sender, args) =>
            {
                Log.Info("MarketInfoRouterService Active");
                AddSubscribers(RunAndSubscribe());
            };
            topicManager.StandBy += (sender, args) => DisposeSubscriptions();
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomMarketInfoResponse>(SubscriptionId, ForwardMarketInfoResponse);
            yield return _aomBus.Subscribe<AomMarketInfoMessageResponse>(SubscriptionId, RouteMessage);
        }

        private void RouteMessage(AomMarketInfoMessageResponse marketInfoMessageResponse)
        {
            ForwardMarketInfoRequest(marketInfoMessageResponse.Message, marketInfoMessageResponse.ObjectJson,
                new AomMarketInfoRequest());
        }

        private void ForwardMarketInfoRequest<T>(Message receivedMsg, string marketInfolJson, T requestToSend)
            where T : AomMarketInfoRequest
        {
            Log.Debug(receivedMsg.MessageAction + " Market Info Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Info)
            {
                var logUserError =
                    _errorService.LogUserError(new UserError
                    {
                        ErrorText = "Message type is not Info!",
                        Source = "ForwardNewMarketInfoRequest"
                    });

                SendErrorToClient(
                    receivedMsg,
                    logUserError);
            }
            try
            {
                var marketInfo = JsonConvert.DeserializeObject<MarketInfo>(marketInfolJson);
                requestToSend.MarketInfo = marketInfo;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                var logError = _errorService.LogUserError(new UserError
                {
                    Error = error,
                    AdditionalInformation = receivedMsg.MessageAction + " market info",
                    ErrorText = string.Empty,
                    Source = "InternalDealRouterService"
                });

                SendErrorToClient(receivedMsg, logError);
            }
        }

        private void ForwardMarketInfoResponse<T>(T response) where T : AomMarketInfoResponse
        {
            if (response.Message.MessageType == MessageType.Info)
            {
                try
                {
                    _clientMessageHandler.SendMessage(response.Message.ClientSessionInfo, response.ToJsonCamelCase());
                }
                catch (Exception exception)
                {
                    _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = "ERROR SENDING AomMarketInfoResponse TO CLIENT",
                        ErrorText = string.Empty,
                        Source = "MarketInfoRouterService"
                    });
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
            else
            {
                var logUserError =
                    _errorService.LogUserError(new UserError
                    {
                        ErrorText = string.Format("Unexpected response from service --> {0}", response),
                        Source = "ForwardMarketInfoResponse"
                    });

                SendErrorToClient(
                    response.Message,
                    logUserError);
            }
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("MarketInfoRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        protected override string SubscriptionId
        {
            get
            {
                return "MarketInfoRouterService";
            }
        }

        public void Disconnect()
        {
            this.DisposeSubscriptions();
        }
    }
}