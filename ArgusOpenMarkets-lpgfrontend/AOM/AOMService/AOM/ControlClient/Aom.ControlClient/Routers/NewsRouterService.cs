﻿using AOM.App.Domain.Services;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events.News;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using AOM.TopicManager;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.ControlClient.Routers
{
    public class NewsRouterService : RouterServiceBase, INewsRouterService
    {
        private readonly IAomNewsService _aomNewsService;

        private bool _newRouterStandby;

        public NewsRouterService(
            IBus aomBus,
            INewsDiffusionTopicManager topicManager,
            IClientSessionMessageHandler clientMessageHandler,
            IAomNewsService aomNewsService)
            : base(aomBus, topicManager, clientMessageHandler)
        {
            _aomNewsService = aomNewsService;
            _topicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("NewsRouterService Active");
                AddSubscribers(RunAndSubscribe(_newRouterStandby));
            };
            _topicManager.StandBy += (sender, args) =>
            {
                _newRouterStandby = true;
                DisposeSubscriptions();
            };
        }


        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe(bool standbyMode)
        {
            yield return _aomBus.Subscribe<AomNewsPublished>(SubscriptionId, OnAomNewsPublished);
            yield return _aomBus.Subscribe<PurgeNewsResponse>(SubscriptionId, OnAomNewsPurgeResponse);
            yield return _aomBus.Subscribe<AomNewsDeleted>(SubscriptionId, OnAomNewsDeleted);
            yield return _aomBus.Subscribe<AomNewsUpdated>(SubscriptionId, OnAomNewsUpdated);

            if (standbyMode == false)
            {
                PublishReplayArgusNews();
            }
        }

        private void OnAomNewsUpdated(AomNewsUpdated newsUpdated)
        {
            // NOTE: Could be some additional logic to determine if we need to Delete and Add - or just Update in situ?
            if (newsUpdated.PreviousNews != null)
            {
                {
                    // Ok - let's delete the News Story from any existing Topics.
                    DeleteAomNewsTopicForNewsStory(
                        newsUpdated.PreviousNews.CmsId,
                        newsUpdated.PreviousNews.CommodityId,
                        newsUpdated.PreviousNews.ContentStreams,
                        newsUpdated.PreviousNews.IsFree);
                }
            }

            var aomNewsPublishedEvent = new AomNewsPublished
            {
                Message = newsUpdated.Message,
                News = newsUpdated.UpdatedNews
            };

            PublishNewsToTopics(aomNewsPublishedEvent);
        }

        private void OnAomNewsDeleted(AomNewsDeleted aomNewsDeleted)
        {
            if (aomNewsDeleted != null)
            {
                Log.InfoFormat(
                    "We have received an AOM News Deleted Event for news Id: {0}   News Is Free: {1}",
                    aomNewsDeleted.CmsId,
                    aomNewsDeleted.IsFree);

                DeleteAomNewsTopicForNewsStory(
                    aomNewsDeleted.CmsId,
                    aomNewsDeleted.CommodityId,
                    aomNewsDeleted.ContentStreams,
                    aomNewsDeleted.IsFree);

                // Actually removing from our DB here - just in case a lag in the Rabbit messages being fired out 
                // And the removal from the Topic Tree.
                Log.InfoFormat("Control Client - News Router - Calling News Service to Delete News for CmsId:{0}",
                    aomNewsDeleted.CmsId);
                _aomNewsService.DeleteNews(aomNewsDeleted.CmsId);
            }
            else
            {
                Log.InfoFormat("OnAomNewsDeleted - null news object passed in");
            }
        }

        private void DeleteAomNewsTopicForNewsStory(string cmsId, ulong commodityId, long[] contentStreams, bool isFree)
        {
            var newsTopicsToDelete = GetNewsTopicsToDelete(cmsId, commodityId, contentStreams, isFree);

            foreach (var topicToDelete in newsTopicsToDelete)
            {
                var deleteCallback = new TopicControlDeleteCallback(topicToDelete);
                deleteCallback.TopicRemoved += (sender, s) =>
                {
                    Log.InfoFormat("OnAomNewsDeleted - Topic {0} has been deleted.", s);
                };
                Log.InfoFormat("OnAomNewsDeleted - Deleting Topic: {0}", topicToDelete);
                _topicManager.DeleteTopic(topicToDelete, ">", deleteCallback);
            }
        }

        private string[] GetNewsTopicsToDelete(string cmsId, ulong commodityId, long[] contentStreams, bool isFree)
        {
            List<string> topicsToDelete = new List<string>();
            if (isFree)
            {
                topicsToDelete.Add(NewsTopicManager.NewsTopicPath(commodityId, cmsId, isFree, 0));
            }
            else
            {
                if (contentStreams != null)
                {
                    // Loop through all Content Streams the article is linked to and delete these topics.
                    foreach (var cs in contentStreams)
                    {
                        topicsToDelete.Add(NewsTopicManager.NewsTopicPath(commodityId, cmsId, isFree, cs));
                    }
                }
            }

            return topicsToDelete.ToArray();
        }

        private void OnAomNewsPurgeResponse(PurgeNewsResponse purgeNewsResponse)
        {
            var topicPath = "AOM/News/";
            _topicManager.DeleteTopic(topicPath, ">", new TopicControlDeleteCallback());

            var replayNewsRequest = new ReplayArgusNews {DaysToReplay = purgeNewsResponse.DaysToKeep, ContentStreamsReplay = null};
            _aomBus.Publish(replayNewsRequest);
        }

        private void PublishReplayArgusNews()
        {
            _aomBus.Publish(new ReplayArgusNews());
        }

        private void OnAomNewsPublished(AomNewsPublished aomNewsPublishedEvent)
        {
            PublishNewsToTopics(aomNewsPublishedEvent);
        }

        private void PublishNewsToTopics(AomNewsPublished aomNewsPublishedEvent)
        {
            if (aomNewsPublishedEvent.News != null)
            {
                if (aomNewsPublishedEvent.News.IsFree)
                {
                    // If the news is free - we just publish the news to a single content stream. 
                    SendFreeNews(aomNewsPublishedEvent);
                }
                else if (aomNewsPublishedEvent.News.ContentStreams != null)
                {
                    SendNewsToMultipleTopics(aomNewsPublishedEvent);
                }
                else
                {
                    Log.Error(
                        "News Router Service - news item is not free and has no content streams, so cannot publish.  Article: "
                        + aomNewsPublishedEvent.ToJsonCamelCase());
                }
            }
        }

        private void SendNewsToMultipleTopics(AomNewsPublished aomNewsPublishedEvent)
        {
            if (aomNewsPublishedEvent.News != null)
            {
                foreach (var cs in aomNewsPublishedEvent.News.ContentStreams)
                {
                    var topicPath = NewsTopicManager.NewsTopicPath(
                        aomNewsPublishedEvent.News.CommodityId,
                        aomNewsPublishedEvent.News.CmsId,
                        false,
                        cs);

                    PushNewsDownTopic(topicPath, aomNewsPublishedEvent);
                }
            }
        }

        private void PushNewsDownTopic(string topicPath, AomNewsPublished aomNewsPublishedEvent)
        {
            // Truncating the news story here - should be transform to a view model?
            aomNewsPublishedEvent.News.Story = _aomNewsService.TruncateStory(aomNewsPublishedEvent.News.Story);
            _topicManager.SendDataToTopic(topicPath, aomNewsPublishedEvent.ToJsonCamelCase());
        }

        private void SendFreeNews(AomNewsPublished aomNewsPublishedEvent)
        {

            var topicPath = NewsTopicManager.NewsTopicPath(
                aomNewsPublishedEvent.News.CommodityId,
                aomNewsPublishedEvent.News.CmsId,
                true,
                0);

            PushNewsDownTopic(topicPath, aomNewsPublishedEvent);
        }

        protected override string SubscriptionId
        {
            get { return "NewsRouterService"; }
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("NewsRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        public void Disconnect()
        {
            DisposeSubscriptions();
        }
    }
}