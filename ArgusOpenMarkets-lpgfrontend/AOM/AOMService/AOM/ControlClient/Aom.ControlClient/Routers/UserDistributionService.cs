﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.ControlClient.Interfaces;
using AOM.TopicManager;
using AOM.Transport.Events;
using AOM.Transport.Events.Users;
using AOMDiffusion.Common.Interfaces;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

using Argus.Transport.Infrastructure;

namespace AOM.ControlClient.Routers
{
    public class UserDistributionService : RouterServiceBase, IUserDistributionService
    {
        private const string TerminateUserMessage = "You have been logged off, please contact Aom Support on aomsupport@argusmedia.com or call +44(0)2071999430";

        private readonly IDiffusionSessionManager _diffusionSessionManager;

        public UserDistributionService(IBus aomBus, IAuthenticationDiffusionTopicManager topicManager, IDiffusionSessionManager diffusionSessionManager,
            IClientSessionMessageHandler clientMessageHandler) : base(aomBus, topicManager, clientMessageHandler)
        {
            _diffusionSessionManager = diffusionSessionManager;

            _topicManager.Active += (sender, args) =>
            {
                Log.InfoFormat("UserDistributionService Active");
                AddSubscribers(RunAndSubscribe());
            };
            _topicManager.StandBy += (sender, args) => DisposeSubscriptions();
        }

        public void Run()
        {
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            _aomBus.Respond<GetLoggedOnUsersRequest, GetLoggedOnUsersResponse>(GetLoggedOnUsers);
            yield return _aomBus.Subscribe<TerminateResponse>(SubscriptionId, OnTerminateUser);
            yield return _aomBus.Subscribe<TerminateOtherUserSessionsResponse>(SubscriptionId + Environment.MachineName, OnTerminateUserOtherSessions);
            yield return _aomBus.Subscribe<BlankUserTopic>(SubscriptionId, OnBlankUserTopic);
        }

        private void OnBlankUserTopic(BlankUserTopic blankUserTopicRequest)
        {
            var userTopic = UserTopicManager.UserTopicPath(blankUserTopicRequest.UserId);
            _topicManager.SendDataToTopic(userTopic, string.Empty);
            Log.InfoFormat("OnBlankUserTopic - Topic {0} has been blanked.", userTopic);
        }

        public GetLoggedOnUsersResponse GetLoggedOnUsers(GetLoggedOnUsersRequest obj)
        {
            List<LoggedOnUserInfo> usersLoggedOn = new List<LoggedOnUserInfo>();

            if (_diffusionSessionManager != null)
            {
                if (_diffusionSessionManager.SessionsDictionary != null)
                {
                    foreach (var item in _diffusionSessionManager.SessionsDictionary)
                    {
                        var loggedOnUser = new LoggedOnUserInfo
                        {
                            LastSeen = item.Value.LastSeen,
                            UserId = item.Key.UserId
                        };
                        usersLoggedOn.Add(loggedOnUser);
                    }
                }
            }

            var getLoggedOnUsersResponse = new GetLoggedOnUsersResponse {LoggedOnUsers = usersLoggedOn};
            return getLoggedOnUsersResponse;
        }

        private void OnTerminateUserOtherSessions(TerminateOtherUserSessionsResponse terminateUserOtherSessionsResponse)
        {
            if (terminateUserOtherSessionsResponse.Message.MessageType != MessageType.Error && terminateUserOtherSessionsResponse.UserId > 0)
            {
                var allUserSessions = _diffusionSessionManager.GetUserSessionsUserInfo(terminateUserOtherSessionsResponse.UserId);
                int sessionCount = allUserSessions.Count() - terminateUserOtherSessionsResponse.LoginSessionsAllowed;

                if (sessionCount > 0)
                {
                    var sessionsToTerminate = allUserSessions.OrderBy(ui => ui.LastSeen).Take(sessionCount);
                    foreach (UserSessionInfo session in sessionsToTerminate)
                    {
                        SendTerminateMessageToUserSession(session.SessionId.ToString(), terminateUserOtherSessionsResponse.UserId,
                            !string.IsNullOrEmpty(terminateUserOtherSessionsResponse.Reason) ? terminateUserOtherSessionsResponse.Reason : TerminateUserMessage);
                        
                        _diffusionSessionManager.RemoveUserSessionId(session.SessionId, terminateUserOtherSessionsResponse.UserId);
                    }
                    SendMessageToUserSessionOnly(terminateUserOtherSessionsResponse.SessionId, terminateUserOtherSessionsResponse.UserId, "Your other open sessions have been logged off");
                }
            }
        }

        public void OnTerminateUser(TerminateResponse terminateUserReposnse)
        {
            if (terminateUserReposnse.Message.MessageType != MessageType.Error && terminateUserReposnse.UserName != null)
            {
                SendTerminateMessageToUser(terminateUserReposnse.UserId,
                    !string.IsNullOrEmpty(terminateUserReposnse.Reason) ? terminateUserReposnse.Reason : TerminateUserMessage);

                _diffusionSessionManager.RemoveUserSessions(terminateUserReposnse.UserId);
            }
        }

        public void SendTerminateMessageToUser(long userId, string message)
        {
            var csi = new ClientSessionInfo { UserId = userId };

            var terminateResponse = new TerminateResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = csi,
                    MessageAction = MessageAction.Void,
                    MessageType = MessageType.Logoff,
                    MessageBody = message
                },
                UserId = userId,
            };

            _clientMessageHandler.SendMessageToAllUserSessions(userId, terminateResponse.ToJsonCamelCase());
        }

        public void SendTerminateMessageToUserSession(string currentSessionId, long userId, string message)
        {
            var csi = new ClientSessionInfo { UserId = userId };

            var terminateResponse = new TerminateResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = csi,
                    MessageAction = MessageAction.Void,
                    MessageType = MessageType.Logoff,
                    MessageBody = message
                },
                UserId = userId,
            };

            _clientMessageHandler.SendMessageThisSessionOnly(currentSessionId, userId, terminateResponse.ToJsonCamelCase());
        }

        public void SendMessageToUserSessionOnly(string currentSessionId, long userId, string message)
        {
            var csi = new ClientSessionInfo { UserId = userId };

            var terminateResponse = new TerminateResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = csi,
                    MessageAction = MessageAction.Void,
                    MessageType = MessageType.UserMessage,
                    MessageBody = message
                },
                UserId = userId,
            };

            _clientMessageHandler.SendMessageThisSessionOnly(currentSessionId, userId, terminateResponse.ToJsonCamelCase());
        }

        public void SendMessageToUserOtherSessions(string currentSessionId, long userId, string message)
        {
            var csi = new ClientSessionInfo { UserId = userId };

            var terminateResponse = new TerminateResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = csi,
                    MessageAction = MessageAction.Void,
                    MessageType = MessageType.Logoff,
                    MessageBody = message
                },
                UserId = userId,
            };

            _clientMessageHandler.SendMessageToUserOtherSessions(currentSessionId, userId, terminateResponse.ToJsonCamelCase());
        }

        private void DisposeSubscriptions()
        {
            Log.InfoFormat("UserRouterService-DisposeSubscription - for {0} subscriptions", SubscriberCount);
            DisposedAndClearSubscribers();
        }

        protected override string SubscriptionId
        {
            get
            {
                return "UserDistributionService";
            }
        }

        public void Disconnect()
        {
            this.DisposeSubscriptions();
        }
    }
}