﻿namespace AOM.ControlClient.v1
{
    using System;
    using System.Linq;
    using System.Diagnostics;

    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;

    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using PushTechnology.ClientInterface.Client.Session;

    using Utils.Logging.Utils;

    public class ClientSessionMessageHandler : IClientSessionMessageHandler
    {
        private IMessagingControl _messageControl;

        private readonly IDiffusionFacade _diffusionFacade;

        public ClientSessionMessageHandler(IDiffusionFacade diffusionFacade)
        {
            _diffusionFacade = diffusionFacade;
        }

        public void AttachToSession(ISession session)
        {
            _messageControl = session.GetMessagingControlFeature();
        }

        public void SendMessage(ClientSessionInfo csi, string content)
        {
            try
            {
                if (csi == null) throw new ArgumentNullException("csi");

                var topic = string.Format("AOM/SEC-Users/{0}", csi.UserId);

                var sessionId = _diffusionFacade.GetSession(csi);
                var msg = PushTechnology.ClientInterface.Client.Factories.Diffusion.Content.NewContent(content);

                if (sessionId != null)
                {
                    _messageControl.Send(sessionId, topic, msg, this);
                    Debug.Print(
                        "\r===========MESSAGE TO CLIENT============\r\r" + content
                        + "\r\r==========================================\r");
                }

            }
            catch (Exception ex)
            {
                Log.Error("ClientSessionMessageHandler.SendMessage", ex);
            }
        }

        public void SendMessageToAllUserSessions(long userId, string content)
        {
            try
            {

                var csis = _diffusionFacade.GetUserSessions(userId);

                foreach (var csi in csis)
                {
                    SendMessage(csi, content);
                }

                Log.Info(
                    string.Format(
                        "Successfully sent notification: [{0}]/n message to user with Id: {1}",
                        content,
                        userId));

            }
            catch (Exception ex)
            {
                Log.Error("ClientSessionMessageHandler.SendMessageToAllUserSessions", ex);
            }
        }

        public void SendMessageThisSessionOnly(string currentSessionId, long userId, string content)
        {
            try
            {

            var clientSessionInfo =
                _diffusionFacade.GetUserSessions(userId).FirstOrDefault(s => s.SessionId == currentSessionId);

            if (clientSessionInfo != null)
            {
                SendMessage(clientSessionInfo, content);
            }

            }
            catch (Exception ex)
            {
                Log.Error("ClientSessionMessageHandler.SendMessageThisSessionOnly", ex);
            }
        }

        public void SendMessageToUserOtherSessions(string userSessionToIgnore, long userId, string content)
        {
            try
            {
                var csis = _diffusionFacade.GetUserOtherSessions(userSessionToIgnore, userId);

                foreach (var csi in csis)
                {
                    SendMessage(csi, content);
                }

                Log.Info(
                    string.Format(
                        "Successfully sent notification: [{0}]/n message to user with Id: {1}",
                        content,
                        userId));
            }
            catch (Exception ex)
            {
                Log.Error("ClientSessionMessageHandler.SendMessageToUserOtherSessions", ex);
            }
        }

        public void OnComplete()
        {
            Log.Info(string.Format("Sent message to client"));
        }

        public void OnDiscard()
        {
            Log.Error(string.Format("ERROR SENDING MESSAGE TO CLIENT: OnDiscard"));
        }

    }
}