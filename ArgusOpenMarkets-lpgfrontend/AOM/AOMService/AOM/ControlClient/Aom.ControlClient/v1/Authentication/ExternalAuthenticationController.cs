﻿using AOM.ControlClient.Authentication;

namespace AOM.ControlClient.v1.Authentication
{
    using AOM.App.Domain.Services;
    using AOM.Diffusion.Common.Interfaces;

    using Argus.Transport.Infrastructure;

    using PushTechnology.ClientInterface.Client.Features.Control.Clients;

    internal class ExternalAuthenticationController : CompositeControlAuthenticationHandler
    {
        public ExternalAuthenticationController(
            IDiffusionFacade aomTopicManager,
            IUserAuthenticationService authService,
            IBus bus)
            : base(
                new ExternalCredentialsAuthentication(aomTopicManager, authService, bus),
                new AuthenticationForConsole())
        {
        }
    }
}