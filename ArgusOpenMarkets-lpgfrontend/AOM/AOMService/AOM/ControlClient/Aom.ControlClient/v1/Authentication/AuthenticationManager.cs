﻿using AOM.ControlClient.v1.Interfaces;

namespace AOM.ControlClient.v1.Authentication
{
    using System.Configuration;
    using System.Linq;

    using AOM.App.Domain.Services;
    using AOM.ControlClient.v1.Interfaces;
    using AOM.Diffusion.Common.Interfaces;

    using Argus.Transport.Infrastructure;

    using PushTechnology.ClientInterface.Client.Details;
    using PushTechnology.ClientInterface.Client.Features.Control.Clients;

    using Utils.Logging.Utils;

    public class AuthenticationManager : IAuthenticationManager
    {
        #region Properties

        private readonly string _authHandlerName = ConfigurationManager.AppSettings["DiffusionHandlerName"];

        private readonly IDiffusionFacade _aomTopicManager;

        private readonly IUserAuthenticationService _authService;

        private IAuthenticationControl _authControl;

        private readonly IBus _bus;

        #endregion

        #region Construstor

        public AuthenticationManager(IDiffusionFacade aomTopicManager, IUserAuthenticationService authService, IBus bus)
        {
            _aomTopicManager = aomTopicManager;
            _authService = authService;
            _bus = bus;
        }

        #endregion

        #region IAuthenticationManager interfaces

        public void Start()
        {
            Log.Info("Registering as an authentication handler...");

            _authControl = _aomTopicManager.Session.GetAuthenticationControlFeature();

            _authControl.SetAuthenticationHandler(
                _authHandlerName,
                DetailType.Values().ToList(),
                new ExternalAuthenticationController(_aomTopicManager, _authService, _bus));

            Log.Info("Authentication handler registered, waiting for clients...");
        }

        public void Stop()
        {
            Log.Info("Authentication handler stopped");
        }

        #endregion
    }
}