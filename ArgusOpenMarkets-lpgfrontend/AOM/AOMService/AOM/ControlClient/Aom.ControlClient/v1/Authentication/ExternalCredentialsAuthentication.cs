﻿using AOM.ControlClient.Authentication;

namespace AOM.ControlClient.v1.Authentication
{
    using System;
    using System.Text;

    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Diffusion.Common;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events.Users;

    using Argus.Transport.Infrastructure;

    using PushTechnology.ClientInterface.Client.Details;
    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using PushTechnology.ClientInterface.Client.Security.Authentication;
    using PushTechnology.ClientInterface.Client.Types;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class ExternalCredentialsAuthentication : ExternalAbstractAuthentication
    {       
        private readonly IBus _aomBus;

        private readonly IDiffusionFacade _aomTopicManager;

        private readonly IUserAuthenticationService _userAuthenticationService;        

        public ExternalCredentialsAuthentication(
            IDiffusionFacade aomTopicManager,
            IUserAuthenticationService userAuthenticationService,
            IBus bus)
        {
            _userAuthenticationService = userAuthenticationService;
            _aomBus = bus;
            _aomTopicManager = aomTopicManager;
        }

        public override void Auth(
            string principal,
            ICredentials credentials,
            ISessionDetails sessionDetails,
            IAuthenticationHandlerCallback callback)
        {            
            Log.Debug("External Credentials Authentication has been called - User: " + principal);

            var token = Encoding.UTF8.GetString(credentials.ToBytes());
            var username = principal;

            if (credentials.Type == CredentialsType.NONE)
            {
                callback.Abstain();
            }
            else
            {
                Log.Debug("Checking Token For User : " + username);
                var result = _userAuthenticationService.AuthenticateToken(username, token);

                if (result.IsAuthenticated)
                {
                    Log.Debug("User is authenticated : " + username);
                    var fullUser = RequestFullUser(result);
                    if (fullUser != null)
                    {
                        callback.Allow();
                        Log.Info("Authentication - Allow()");

                        var userTopicSetupCallback = new TopicUpdateCallback();
                        userTopicSetupCallback.OnUpdate += (sender, s) =>
                        {
                            Log.Debug("Updated User Topic. Principal: " + principal);
                        };

                        SetupUserTopic(fullUser, userTopicSetupCallback);                
                    }
                    else
                    {
                        Log.Info("Authentication - Null user returned");
                    }
                }
                else
                {
                    callback.Deny();
                    Log.Warn(string.Format("NOT AUTHENTICATED : Denied connection for {0}.", principal));
                }
            }
        }

        private IUser RequestFullUser(App.Domain.Services.IAuthenticationResult authResult)
        {
            if (authResult == null) return null;
            if (authResult.User == null) return null;
            var userId = authResult.User.Id;
            if (userId > 0)
            {
                try
                {
                    var getUserRequest = new GetUserRequestRpc
                    {
                        UserId = authResult.User.Id,
                        Username = authResult.User.Username
                    };
                    var getUserResponse = _aomBus.Request<GetUserRequestRpc, GetUserResponse>(getUserRequest);
                    return getUserResponse.User;
                }
                catch (Exception ex)
                {
                    Log.Error("RequestFullUser errored", ex);
                }
            }
            Log.Debug("Did not receive a valid user id.");
            return null;
        }

        private void SetupUserTopic(IUser user, ITopicUpdaterUpdateCallback topicUpdateCallback)
        {
            var topicName = GetUserTopicName(user);
            var topicNotificationName = GetUserNotificationTopicName(user);
            try
            {
                var addTopicCallback = new TopicControlAddCallback();
                addTopicCallback.TopicAdded += (sender, s) => PushToUserTopic(user, topicUpdateCallback, topicName);
                _aomTopicManager.CreateTopic(topicName, addTopicCallback, () => PushToUserTopic(user, topicUpdateCallback, topicName));

                _aomTopicManager.CreateTopic(topicNotificationName, new TopicControlAddCallback());
            }
            catch (Exception ex)
            {
                Log.Error("ExternalCredentialsAuthentication-SetupTopicAndGetUserDetails", ex);
            }
        }

        private void PushToUserTopic(IUser user, ITopicUpdaterUpdateCallback topicUpdateCallback, string topicName)
        {
            var message = user.ToJsonCamelCase();
            var topicUpdater = _aomTopicManager.GetUpdater();
            if (topicUpdater != null)
            {
                topicUpdater.PushToTopic(topicName, message, true, topicUpdateCallback);
            }
            else
            {
                Log.Error("ExternalCredentialsAuthentication - GetUpdater() returned null");
            }

            Log.Debug(
                string.Format(
                    "AOMControlClient - Pushing User Details down topic: {0}  With Message:{1}",
                    topicName,
                    message));
        }

        private string GetUserTopicName(IUser user)
        {
            return user != null ? string.Format("AOM/SEC-Users/{0}", user.Id) : string.Empty;
        }

        private string GetUserNotificationTopicName(IUser user)
        {
            return string.Format("AOM/SEC-Users/{0}", user.Id);
        }
    }
}