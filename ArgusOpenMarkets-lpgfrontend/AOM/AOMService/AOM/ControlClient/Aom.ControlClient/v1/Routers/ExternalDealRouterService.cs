namespace AOM.ControlClient.v1.Routers
{
    using System;
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.ExternalDeals;

    using Argus.Transport.Infrastructure;

    using Newtonsoft.Json;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class ExternalDealRouterService : IExternalDealRouterService
    {
        private readonly IBus _aomBus;

        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private readonly IClientSessionMessageHandler _clientMessageHandler;

        private bool disposed;

        public ExternalDealRouterService(IBus aomBus, IClientSessionMessageHandler clientSessionMessageHandler)
        {
            _aomBus = aomBus;

            // error responses can be this type.
            _clientMessageHandler = clientSessionMessageHandler;
        }


        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomExternalDealResponse>(SubscriptionId, ForwardExternalDealResponse);
        }

        public void RouteMessage(Message message)
        {
            ForwardExternalDealRequest(message, new AomExternalDealRequest());
        }

        private void ForwardExternalDealRequest<T>(Message receivedMsg, T requestToSend)
            where T : AomExternalDealRequest
        {
            Log.Debug(receivedMsg.MessageAction + " External deal Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.ExternalDeal)
            {
                SendErrorToClient(
                    receivedMsg,
                    LogException.LogString("Message type is not ExternalDeal!", "ForwardExternalDealRequest"));
            }
            try
            {
                var deal = JsonConvert.DeserializeObject<ExternalDeal>(receivedMsg.MessageBody.ToString());
                requestToSend.ExternalDeal = deal;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                SendErrorToClient(receivedMsg, LogException.Log(error, receivedMsg.MessageAction + " external deal"));
            }
        }

        private void ForwardExternalDealResponse<T>(T response) where T : AomExternalDealResponse
        {
            if (response.Message.MessageType == MessageType.ExternalDeal)
            {
                try
                {
                    _clientMessageHandler.SendMessage(response.Message.ClientSessionInfo, response.ToJsonCamelCase());
                }
                catch (Exception exception)
                {
                    LogException.Log(exception, "ERROR SENDING AomExternalDealResponse TO CLIENT");
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
            else
            {
                SendErrorToClient(
                    response.Message,
                    LogException.LogString(
                        string.Format("Unexpected response from service --> {0}", response),
                        "ForwardExternalDealResponse"));
            }
        }

        private void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomExternalDealResponse
                                {
                                    ExternalDeal = null,
                                    Message =
                                        new Message
                                        {
                                            ClientSessionInfo =
                                                receivedMessage.ClientSessionInfo,
                                            MessageAction =
                                                receivedMessage.MessageAction,
                                            MessageType = MessageType.Error,
                                            MessageBody = errorMessageText
                                        }
                                };
            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "ExternalDealRouterService";
            }
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }

    }
}