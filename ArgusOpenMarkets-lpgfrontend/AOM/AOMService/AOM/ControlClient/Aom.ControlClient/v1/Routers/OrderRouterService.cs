﻿namespace AOM.ControlClient.v1.Routers
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;

    using Argus.Transport.Infrastructure;

    using Newtonsoft.Json;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class OrderRouterService : IOrderRouterService
    {
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private readonly IBus _aomBus;

        private readonly IDiffusionFacade _topicManager;

        private readonly IClientSessionMessageHandler _clientMessageHandler;

        private readonly IDiffusionProductTopicTreeBuilder _diffusionProductTopicTreeBuilder;

        private bool disposed;

        public OrderRouterService(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IClientSessionMessageHandler clientSessionHandler, 
            IDiffusionProductTopicTreeBuilder diffusionProductTopicTreeBuilder)
        {
            _aomBus = aomBus;
            _topicManager = topicManager;
            _clientMessageHandler = clientSessionHandler;
            _diffusionProductTopicTreeBuilder = diffusionProductTopicTreeBuilder;
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomOrderResponse>(SubscriptionId, ForwardOrderResponse, "Market.*");
            yield return _aomBus.Subscribe<AomOrderRefreshResponse>(SubscriptionId, ForwardOrderResponse);
            yield return _aomBus.Subscribe<PurgeOrdersInDiffusionRequest>(SubscriptionId, PurgeDiffusionOrders);
        }

        public void RouteMessage(Message message)
        {
            ForwardOrderRequest(message, new AomOrderRequest());
        }

        private void PurgeDiffusionOrders(PurgeOrdersInDiffusionRequest purgeOrdersInDiffusionRequest)
        {
            try
            {

                foreach (var productId in purgeOrdersInDiffusionRequest.ProductsToPurgeOrdersFor)
                {
                    var bidAskTopicNode = string.Join(
                        "/",
                        "AOM",
                        "SEC-Products",
                        productId,
                        "SEC-Privileges",
                        "View_BidAsk_Widget");

                    var deleteCallback = new TopicControlDeleteCallback(bidAskTopicNode);
                    deleteCallback.TopicRemoved += (sender, s) =>
                    {
                        Log.InfoFormat("PurgeDiffusionOrder -  Topic {0} has been deleted.", s);
                        var idOfProductDeleted = GetProductIdFromTopicDeleted(s);
                        _diffusionProductTopicTreeBuilder.BuildBidAskStackTree(idOfProductDeleted);
                    };
                    _topicManager.DeleteTopic(bidAskTopicNode, ">", deleteCallback);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unknown Error", ex);
            }
        }

        public long GetProductIdFromTopicDeleted(string topicDeleted)
        {
            long productId = 0;
            var numbers = Regex.Match(topicDeleted, "[0-9]+");

            long.TryParse(numbers.Value, out productId);

            return productId;
        }

        private void ForwardOrderRequest<T>(Message receivedMsg, T requestToSend) where T : AomOrderRequest
        {
            Log.Debug(receivedMsg.MessageAction + " Order Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Order)
            {
                SendErrorToClient(
                    receivedMsg,
                    LogException.LogString("Message type is not Order!", "ForwardOrderRequest"));
            }
            try
            {
                var order = JsonConvert.DeserializeObject<Order>(receivedMsg.MessageBody.ToString());
                requestToSend.Order = order;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                SendErrorToClient(receivedMsg, LogException.Log(error, receivedMsg.MessageAction + " order"));
            }
        }

        private void ForwardOrderResponse<T>(T response) where T : AomOrderResponse
        {
            if (response.Message.MessageType == MessageType.Order)
            {
                try
                {
                    bool isHeld = response.Order.OrderStatus == OrderStatus.Held;

                    var parentTopicPath = string.Join(
                        "/",
                        "AOM",
                        "SEC-Products",
                        response.Order.ProductId,
                        "SEC-Privileges",
                        "View_BidAsk_Widget");

                    var notHeldTopicPath = string.Join("/", parentTopicPath, "NotHeld", "Orders", response.Order.Id);

                    var heldTopicPath = string.Join("/", parentTopicPath, "Held", "Orders", response.Order.Id);

                    var principalHeldOrderTopicPath = string.Join(
                        "/",
                        heldTopicPath,
                        "SEC-Organisations",
                        response.Order.PrincipalOrganisationId,
                        response.Order.Id);

                    var brokerHeldOrderTopicPath = string.Join(
                        "/",
                        heldTopicPath,
                        "SEC-Organisations",
                        response.Order.BrokerOrganisationId,
                        response.Order.Id);

                    if (isHeld)
                    {
                        //try delete Not Held order topic                     
                        var notHeldDetailsCallback = new TopicDetailsCallback();
                        notHeldDetailsCallback.TopicDetails +=
                            (sender, s) =>
                                _topicManager.DeleteTopic(notHeldTopicPath, ">", new TopicControlDeleteCallback());
                        _topicManager.DeleteTopic(heldTopicPath, ">", new TopicControlDeleteCallback());
                        _topicManager.GetTopicDetails(notHeldTopicPath, notHeldDetailsCallback);
                        _topicManager.DeleteTopic(heldTopicPath, ">", new TopicControlDeleteCallback());
                        //send order for principal org                        
                        var principalDetailsCallback = new TopicDetailsCallback();
                        principalDetailsCallback.TopicDoesNotExist += (s, o) =>
                        {
                            var addTopicCallback = new TopicControlAddCallback();
                            addTopicCallback.TopicAdded +=
                                (sender, s1) =>
                                    _topicManager.GetUpdater()
                                        .PushToTopic(principalHeldOrderTopicPath, response.ToJsonCamelCase(), true);
                            _topicManager.CreateTopic(principalHeldOrderTopicPath, addTopicCallback);
                        };
                        principalDetailsCallback.TopicDetails +=
                            (sender, s) =>
                                _topicManager.GetUpdater()
                                    .PushToTopic(principalHeldOrderTopicPath, response.ToJsonCamelCase(), true);

                        _topicManager.GetTopicDetails(principalHeldOrderTopicPath, principalDetailsCallback);

                        //also send order for broker organisation if order has one

                        if (response.Order.BrokerOrganisationId.HasValue)
                        {
                            var brokerDetailsCallback = new TopicDetailsCallback();
                            brokerDetailsCallback.TopicDoesNotExist += (s, o) =>
                            {
                                var addTopicCallback = new TopicControlAddCallback();
                                addTopicCallback.TopicAdded +=
                                    (sender, s1) =>
                                        _topicManager.GetUpdater()
                                            .PushToTopic(brokerHeldOrderTopicPath, response.ToJsonCamelCase(), true);
                                _topicManager.CreateTopic(brokerHeldOrderTopicPath, addTopicCallback);
                            };
                            brokerDetailsCallback.TopicDetails +=
                                (sender, s) =>
                                    _topicManager.GetUpdater()
                                        .PushToTopic(brokerHeldOrderTopicPath, response.ToJsonCamelCase(), true);

                            _topicManager.GetTopicDetails(brokerHeldOrderTopicPath, brokerDetailsCallback);
                        }
                    }
                    else
                    {
                        //if this was a held order delete the topic under Held tree
                        var heldDetailsCallback = new TopicDetailsCallback();
                        heldDetailsCallback.TopicDetails +=
                            (sender, s) =>
                                _topicManager.DeleteTopic(heldTopicPath, ">", new TopicControlDeleteCallback());
                        _topicManager.GetTopicDetails(heldTopicPath, heldDetailsCallback);


                        //create NotHeld order
                        if (response.Order.OrderStatus == OrderStatus.Active)
                        {
                            var notHeldOrderDetailsCallback = new TopicDetailsCallback();
                            notHeldOrderDetailsCallback.TopicDoesNotExist += (s, o) =>
                            {
                                var addTopicCallback = new TopicControlAddCallback();
                                addTopicCallback.TopicAdded +=
                                    (sender, s1) =>
                                        _topicManager.GetUpdater()
                                            .PushToTopic(notHeldTopicPath, response.ToJsonCamelCase(), true);
                                _topicManager.CreateTopic(notHeldTopicPath, addTopicCallback);
                            };
                            notHeldOrderDetailsCallback.TopicDetails +=
                                (sender, s) =>
                                    _topicManager.GetUpdater()
                                        .PushToTopic(notHeldTopicPath, response.ToJsonCamelCase(), true);

                            _topicManager.GetTopicDetails(notHeldTopicPath, notHeldOrderDetailsCallback);
                        }
                        else
                        {
                            var notheldDetailsCallback = new TopicDetailsCallback();
                            notheldDetailsCallback.TopicDetails +=
                                (sender, s) =>
                                    _topicManager.DeleteTopic(notHeldTopicPath, ">", new TopicControlDeleteCallback());
                            _topicManager.GetTopicDetails(notHeldTopicPath, notheldDetailsCallback);
                        }
                    }
                }
                catch (Exception exception)
                {
                    LogException.Log(exception, "ERROR SENDING ORDER TO CLIENT");
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
            else
            {
                SendErrorToClient(
                    response.Message,
                    LogException.LogString(
                        string.Format("Unexpected response from service --> {0}", response),
                        "OrderRouterSerice.ForwardOrderResponse"));
            }
        }

        private void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomOrderResponse
                                {
                                    Order = null,
                                    Message =
                                        new Message
                                        {
                                            ClientSessionInfo =
                                                receivedMessage.ClientSessionInfo,
                                            MessageAction = receivedMessage.MessageAction,
                                            MessageType = MessageType.Error,
                                            MessageBody = errorMessageText
                                        }
                                };
            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "OrderRouterService";
            }
        }
    }
}