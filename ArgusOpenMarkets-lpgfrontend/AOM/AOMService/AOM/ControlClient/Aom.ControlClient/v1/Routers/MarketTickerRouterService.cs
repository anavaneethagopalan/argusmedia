﻿using AOM.Repository.MySql;
using AOM.Services.ExternalDealService;

namespace AOM.ControlClient.v1.Routers
{
    using System;
    using System.Collections.Generic;

    using Utils.Logging.Utils;

    using System.Linq;

    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.MarketTickers;

    using Argus.Transport.Infrastructure;

    using Utils.Extension;

    public class MarketTickerRouterService : IMarketTickerRouterService
    {
        private const string MarketTickerTopic = "AOM/MarketTicker/";
        private const string MarketTickerConfigTopic = MarketTickerTopic + "Config";
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private readonly IClientSessionMessageHandler _clientMessageHandler;
        private readonly IDiffusionFacade _topicManager;

        private readonly IBus _aomBus;

        private long _currentBottomLimit = long.MaxValue;

        private long _currentTopLimit = -1;

        private long _insertedOutOfSequence = -1;

        private bool disposed;

        public MarketTickerRouterService(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IClientSessionMessageHandler clientSessionMessageHandler)
        {
            _aomBus = aomBus;
            _topicManager = topicManager;
            _clientMessageHandler = clientSessionMessageHandler;
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        public IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomNewMarketTickResponse>(SubscriptionId, ForwardMarketTickerResponse);
            yield return _aomBus.Subscribe<PurgeMarketTickerResponse>(SubscriptionId, ForwardPurgeMarketTickerResponse);

            var topicDetailsCallback = new TopicDetailsCallback();
            var fetchCallback = new FetchCallback();
            fetchCallback.TopicDataFetched +=
                (o, s1) => _aomBus.Subscribe<AomNewMarketTickResponse>(SubscriptionId, ForwardMarketTickerResponse);

            fetchCallback.NoDataFetched += (sender, s) =>
            {
                PushMarketTickerConfig();
                _aomBus.Subscribe<AomNewMarketTickResponse>(SubscriptionId, ForwardMarketTickerResponse);

            };

            topicDetailsCallback.TopicDetails +=
                (sender, s) => _topicManager.GetTopicData(MarketTickerConfigTopic, string.Empty, fetchCallback);

            topicDetailsCallback.TopicDoesNotExist += (sender, s) =>
                {
                    var topicAddedCallback = new TopicControlAddCallback();
                    topicAddedCallback.TopicAdded += (o, s1) => PushMarketTickerConfig();
                    _topicManager.CreateTopic(MarketTickerConfigTopic, topicAddedCallback);
                };
            _topicManager.GetTopicDetails(MarketTickerConfigTopic, topicDetailsCallback);
        }

        private void PushMarketTickerConfig()
        {
            var callback = new TopicUpdateCallback();
            callback.OnUpdate +=
                (sender1, s2) =>
                    _aomBus.Subscribe<AomNewMarketTickResponse>(SubscriptionId, ForwardMarketTickerResponse);
            var config = new AomNewMarketConfigResponse
            {
                MarketTickerConfig =
                    new MarketTickerConfig
                    {
                        TopLimit = _currentTopLimit,
                        BottomLimit = _currentBottomLimit,
                        InsertedOutOfSequence = _insertedOutOfSequence
                    },
                Message =
                    new Message
                    {
                        MessageType = MessageType.MarketTickerItem,
                        MessageAction = MessageAction.ConfigurationChange
                    }
            };

            _topicManager.GetUpdater().PushToTopic(MarketTickerConfigTopic, config.ToJsonCamelCase(), true, callback);
        }

        private void ForwardPurgeMarketTickerResponse(PurgeMarketTickerResponse response)
        {
            var topicDeleteCallback = new TopicControlDeleteCallback();
            topicDeleteCallback.TopicRemoved += (o, s) =>
            {
                Log.Debug(string.Format("Removed Market Ticker Topic: {0}:{1}", s, o.ToString()));

                _currentBottomLimit = long.MaxValue;
                _currentTopLimit = -1;

                _topicManager.CreateTopic(MarketTickerConfigTopic, new TopicControlAddCallback());
            };

            _topicManager.DeleteTopic(
               MarketTickerTopic,
               ">",
               topicDeleteCallback);

            var refreshMessage = new AomRefreshMarketTickerRequest
            {
                RefreshFromTime =
                    DateTime.Today.AddDays(response.DaysToKeep),
                RefreshToTime = DateTime.Today.AddDays(1)
            };

            _aomBus.Publish(refreshMessage);
        }

        public void RouteMessage(Message message)
        {
            switch (message.MessageAction)
            {
                default:
                    SendErrorToClient(message, LogException.LogString("Unknown Action On MarketTicker", "RouteMessage"));
                    break;
            }
        }

        private void ForwardMarketTickerResponse<T>(T response) where T : AomNewMarketTickResponse
        {
            MarketTickerItemStatus marketTickerStatus = response.MarketTickerItem.MarketTickerItemStatus;
            if (marketTickerStatus.Equals(MarketTickerItemStatus.Pending))
            {
                if (response.MarketTickerItem.MarketTickerItemType == MarketTickerItemType.Info)
                    PushPendingMarketInfoTickerItem(response);
                else
                    PushPendingMarketTickerItem(response);
            }
            else if (marketTickerStatus.Equals(MarketTickerItemStatus.Held))
            {
                PushHeldMarketTickerItem(response);
            }
            else
            {
                if (response.MarketTickerItem.MarketTickerItemType == MarketTickerItemType.Deal
                    || response.MarketTickerItem.MarketTickerItemType == MarketTickerItemType.Info)
                {
                    DeletePendingMarketTickerItem(response);
                }
                //PushNonPendingMarketTickerItem(response);

                if (response.MarketTickerItem.MarketTickerItemType == MarketTickerItemType.Info)
                    PushNonPendingMarketInfoTickerItem(response);
                else
                    PushNonPendingMarketTickerItem(response);

            }
        }

        private void PushNonPendingMarketInfoTickerItem(AomNewMarketTickResponse response)
        {
            var callback = new TopicUpdateCallback();
            callback.OnUpdate += (sender, s) => PushChunkSizeAndId(response.MarketTickerItem.Id);
            MarketTickerItem marketTickerItem = response.MarketTickerItem;
            bool isInfo = marketTickerItem.MarketTickerItemType.Equals(MarketTickerItemType.Info);
            bool isVoid = marketTickerItem.MarketTickerItemStatus.Equals(MarketTickerItemStatus.Void);

            var productIds = marketTickerItem.ProductIds ?? new List<long>();
            marketTickerItem.ProductIds = productIds;

            if (isInfo && isVoid)
            {
                var topics = marketTickerItem.OwnerOrganisations.Distinct().Select(
                    orgId => string.Join(
                        "/",
                        "AOM/MarketTicker/SEC-ViewableBy",
                        orgId,
                        marketTickerItem.MarketTickerItemType,
                        marketTickerItem.Id)).ToList();
                topics.AddRange(
                    Helpers.MarketTickerRouter.CreateEditorsTopicStringsForTickerItem(marketTickerItem));

                var responseJson = response.ToJsonCamelCase();
                foreach (var topic in topics)
                {
                    _topicManager.SendDataToTopic(topic, responseJson, callback);
                }
            }
            else
            {
                if (productIds.Count > 0)
                {
                    var responseJson = response.ToJsonCamelCase();
                    foreach (var productId in productIds)
                    {
                        string messageTopic =
                            string.Join(
                                "/",
                                "AOM/MarketTicker/SEC-ViewableBy/All",
                                Helpers.MarketTickerRouter.MarketTickerProductsTopic,
                                productId,
                                marketTickerItem.MarketTickerItemType,
                                marketTickerItem.Id);
                        _topicManager.SendDataToTopic(messageTopic, responseJson, callback);
                    }
                }
            }
        }

        private void PushPendingMarketInfoTickerItem(AomNewMarketTickResponse response)
        {
            var marketTickerItem = response.MarketTickerItem;
            var callback = new TopicUpdateCallback();

            var productIds = response.MarketTickerItem.ProductIds ?? new List<long>();
            response.MarketTickerItem.ProductIds = productIds;

            callback.OnUpdate += (sender, s) => PushChunkSizeAndId(response.MarketTickerItem.Id);
            foreach (long organisationId in marketTickerItem.OwnerOrganisations.Distinct())
            {
                if (productIds.Count > 0)
                {
                    foreach (var productId in productIds)
                    {
                        string messageTopic =
                            string.Join(
                                "/",
                                "AOM/MarketTicker/SEC-ViewableBy",
                                organisationId,
                                Helpers.MarketTickerRouter.MarketTickerProductsTopic,
                                productId,
                                marketTickerItem.MarketTickerItemType,
                                marketTickerItem.Id);

                        _topicManager.SendDataToTopic(messageTopic, response.ToJsonCamelCase(), callback);
                    }
                }
                else
                {
                    string messageTopic =
                        string.Join(
                            "/",
                            "AOM/MarketTicker/SEC-ViewableBy",
                            organisationId,
                            marketTickerItem.MarketTickerItemType,
                            marketTickerItem.Id);
                    _topicManager.SendDataToTopic(messageTopic, response.ToJsonCamelCase(), callback);
                }
            }

            if (productIds.Count > 0)
            {
                foreach (var productId in productIds)
                {
                    string argusEditorTopic =
                        string.Join(
                            "/",
                            "AOM/MarketTicker/SEC-ViewableBy/Editors",
                            Helpers.MarketTickerRouter.MarketTickerProductsTopic,
                            productId,
                            marketTickerItem.MarketTickerItemType,
                            marketTickerItem.Id);

                    _topicManager.SendDataToTopic(argusEditorTopic, response.ToJsonCamelCase(), callback);
                }
            }

            else
            {
                string argusEditorTopic = string.Join(
                    "/",
                    "AOM/MarketTicker/SEC-ViewableBy/Editors",
                    marketTickerItem.MarketTickerItemType,
                    marketTickerItem.Id);
                _topicManager.SendDataToTopic(argusEditorTopic, response.ToJsonCamelCase(), callback);
            }
        }

        private void PushHeldMarketTickerItem(AomNewMarketTickResponse response)
        {
            var callback = new TopicUpdateCallback();
            callback.OnUpdate += (sender, s) => PushChunkSizeAndId(response.MarketTickerItem.Id);
            MarketTickerItem marketTickerItem = response.MarketTickerItem;

            foreach (long organisationId in response.MarketTickerItem.OwnerOrganisations.Distinct())
            {
                string messageTopic = response.MarketTickerItem.MarketTickerItemType.Equals(MarketTickerItemType.Info)
                    ? string.Join(
                        "/",
                        "AOM/MarketTicker/SEC-ViewableBy",
                        organisationId,
                        marketTickerItem.MarketTickerItemType,
                        marketTickerItem.Id)
                    : string.Join(
                        "/",
                        "AOM/MarketTicker/SEC-ViewableBy",
                        organisationId,
                        Helpers.MarketTickerRouter.MarketTickerProductsTopic,
                        marketTickerItem.ProductIds.Single(),
                        marketTickerItem.MarketTickerItemType,
                        marketTickerItem.Id);
                _topicManager.SendDataToTopic(messageTopic, response.ToJsonCamelCase(), callback);
            }
        }

        private void DeletePendingMarketTickerItem(AomNewMarketTickResponse response)
        {
            _topicManager.DeleteTopic(
                "AOM/MarketTicker/.*" + response.MarketTickerItem.Id,
                "*",
                new TopicControlDeleteCallback());

            if (response.MarketTickerItem.ProductIds != null)
            {
                foreach (var productId in response.MarketTickerItem.ProductIds)
                {
                    string messageTopic =
                    string.Join(
                                "/",
                                "AOM/MarketTicker/SEC-ViewableBy",
                                ".*",
                                Helpers.MarketTickerRouter.MarketTickerProductsTopic,
                                productId,
                                response.MarketTickerItem.MarketTickerItemType,
                                response.MarketTickerItem.Id);

                    _topicManager.DeleteTopic(
                    messageTopic,
                    "*",
                    new TopicControlDeleteCallback());
                }
            }
        }

        private void PushChunkSizeAndId(long id)
        {
            var topOrBottomLimitSet = false;

            if (id > _currentTopLimit)
            {
                _currentTopLimit = id;
                topOrBottomLimitSet = true;
            }
            if (_currentBottomLimit > id)
            {
                _currentBottomLimit = id;
                topOrBottomLimitSet = true;
            }

            if (!topOrBottomLimitSet)
            {
                _insertedOutOfSequence = id;
            }
            else
            {
                _insertedOutOfSequence = -1;
            }

            PushMarketTickerConfig();
        }


        private void PushNonPendingMarketTickerItem<T>(T response) where T : AomNewMarketTickResponse
        {
            var callback = new TopicUpdateCallback();
            callback.OnUpdate += (sender, s) => PushChunkSizeAndId(response.MarketTickerItem.Id);

            var topicsToSendDataTo =
                Helpers.MarketTickerRouter.CreateTopicsForNonPendingMarketTickerItem(response.MarketTickerItem,
                                                                                     response.AdditionalData);

            foreach (var topic in topicsToSendDataTo)
            {
                _topicManager.SendDataToTopic(topic, response.ToJsonCamelCase(), callback);
            }
        }

        private void PushPendingMarketTickerItem<T>(T response) where T : AomNewMarketTickResponse
        {
            var callback = new TopicUpdateCallback();
            callback.OnUpdate += (sender, s) => PushChunkSizeAndId(response.MarketTickerItem.Id);
            MarketTickerItem marketTickerItem = response.MarketTickerItem;
            if (marketTickerItem.ExternalDealId != null || marketTickerItem.MarketInfoId != null)
            {
                foreach (long organisationId in response.MarketTickerItem.OwnerOrganisations.Distinct())
                {
                    string messageTopic =
                        response.MarketTickerItem.MarketTickerItemType.Equals(MarketTickerItemType.Info)
                            ? string.Join(
                                "/",
                                "AOM/MarketTicker/SEC-ViewableBy",
                                organisationId,
                                marketTickerItem.MarketTickerItemType,
                                marketTickerItem.Id)
                            : string.Join(
                                "/",
                                "AOM/MarketTicker/SEC-ViewableBy",
                                organisationId,
                                Helpers.MarketTickerRouter.MarketTickerProductsTopic,
                                marketTickerItem.ProductIds.Single(),
                                marketTickerItem.MarketTickerItemType,
                                marketTickerItem.Id);
                    _topicManager.SendDataToTopic(messageTopic, response.ToJsonCamelCase(), callback);
                }
            }
            else
            {
                string messageTopic = string.Join(
                    "/",
                    "AOM/MarketTicker/SEC-ViewableBy/All",
                    Helpers.MarketTickerRouter.MarketTickerProductsTopic,
                    marketTickerItem.ProductIds.Single(),
                    marketTickerItem.MarketTickerItemType,
                    marketTickerItem.Id);
                _topicManager.SendDataToTopic(messageTopic, response.ToJsonCamelCase(), callback);
            }

            var editorsTopics = Helpers.MarketTickerRouter.CreateEditorsTopicStringsForTickerItem(marketTickerItem);
            foreach (var topic in editorsTopics)
            {
                _topicManager.SendDataToTopic(topic, response.ToJsonCamelCase(), callback);
            }
        }


        private void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomNewMarketTickResponse
                                {
                                    MarketTickerItem = null,
                                    Message =
                                        new Message
                                        {
                                            ClientSessionInfo =
                                                receivedMessage.ClientSessionInfo,
                                            MessageAction =
                                                receivedMessage.MessageAction,
                                            MessageType = MessageType.Error,
                                            MessageBody = errorMessageText
                                        }
                                };
            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }



        protected virtual string SubscriptionId
        {
            get
            {
                return "MarketTickerRouterService";
            }
        }
    }
}