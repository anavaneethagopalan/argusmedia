﻿using System.Collections.Generic;

namespace AOM.ControlClient.v1.Routers
{
    using System;

    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Assessments;

    using Argus.Transport.Infrastructure;

    using Newtonsoft.Json;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class AssessmentRouterService : RouterServiceBase, IAssessmentRouterService
    {
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();
        private bool disposed;

        public AssessmentRouterService(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IClientSessionMessageHandler clientMessageHandler)
            : base(aomBus, topicManager, clientMessageHandler)
        {
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomNewAssessmentResponse>(SubscriptionId, ForwardAssessmentResponse);
        }

        public override void RouteMessage(Message message)
        {
            switch (message.MessageAction)
            {
                case MessageAction.Create:
                    ForwardNewAssessmentRequest(message, new AomNewAssessmentRequest());
                    break;
                case MessageAction.Update:
                    ForwardNewAssessmentRequest(message, new AomNewAssessmentRequest());
                    break;
                case MessageAction.Refresh:
                    ForwardAssessmentRefreshRequest(message, new AomRefreshAssessmentRequest());
                    break;
                default:
                    SendErrorToClient(message, LogException.LogString("Unknown Action Assessment", "RouteMessage"));
                    break;
            }
        }

        private void ForwardAssessmentRefreshRequest(Message receivedMsg, AomRefreshAssessmentRequest requestToSend)
        {
            Log.Debug(receivedMsg.MessageAction + " Assessment refresh request received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Assessment)
            {
                SendErrorToClient(
                    receivedMsg,
                    LogException.LogString("Message type is not Assessment!", "AomNewAssessmentRefresh"));
            }
            try
            {
                var info = JsonConvert.DeserializeObject<AomRefreshAssessmentRequest>(receivedMsg.MessageBody.ToString());
                requestToSend.MessageAction = receivedMsg.MessageAction;
                if (receivedMsg.MessageAction == MessageAction.Refresh)
                {
                    requestToSend.ProductId = info.ProductId;
                }
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                SendErrorToClient(receivedMsg, LogException.Log(error, receivedMsg.MessageAction + " assessment"));
            }
        }

        private void ForwardNewAssessmentRequest<T>(Message receivedMsg, T requestToSend)
            where T : AomNewAssessmentRequest
        {
            Log.Debug(receivedMsg.MessageAction + " Assessment info received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Assessment)
            {
                SendErrorToClient(
                    receivedMsg,
                    LogException.LogString("Message type is not Assessment!", "AomNewAssessmentInfoRequest"));
            }
            try
            {
                var info = JsonConvert.DeserializeObject<CombinedAssessment>(receivedMsg.MessageBody.ToString());
                requestToSend.MessageAction = receivedMsg.MessageAction;
                if (receivedMsg.MessageAction == MessageAction.Create)
                {
                    requestToSend.Assessment = info;
                    requestToSend.ProductId = info.ProductId;

                }
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                SendErrorToClient(receivedMsg, LogException.Log(error, receivedMsg.MessageAction + " assessment"));
            }
        }

        private void ForwardAssessmentResponse<T>(T response) where T : AomNewAssessmentResponse
        {
            if (response.Message.MessageType == MessageType.Assessment)
            {
                try
                {
                    var assessmentTopicPath = string.Concat("AOM/SEC-Products/", response.ProductId, "/Assessment");

                    var msg = response.ToJsonCamelCase();
                    _topicManager.SendDataToTopic(string.Format(assessmentTopicPath), msg);
                }
                catch (Exception exception)
                {
                    LogException.Log(exception, "ERROR  ITEM TO CLIENT");
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "AssessmentRouterService";
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }
    }
}