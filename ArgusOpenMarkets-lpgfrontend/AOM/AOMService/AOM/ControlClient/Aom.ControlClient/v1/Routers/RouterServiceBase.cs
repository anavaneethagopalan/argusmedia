﻿namespace AOM.ControlClient.v1.Routers
{
    using System;

    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.MarketTickers;

    using Argus.Transport.Infrastructure;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public abstract class RouterServiceBase : IRouterServiceBase
    {
        protected IBus _aomBus;

        protected readonly IDiffusionFacade _topicManager;

        protected IClientSessionMessageHandler _clientMessageHandler;

        public RouterServiceBase(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IClientSessionMessageHandler clientMessageHandler)
        {
            _aomBus = aomBus;
            _topicManager = topicManager;
            _clientMessageHandler = clientMessageHandler;
        }

        public abstract void RouteMessage(Message message);


        protected void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomResponse()
                                {
                                    Message =
                                        new Message
                                        {
                                            ClientSessionInfo =
                                                receivedMessage.ClientSessionInfo,
                                            MessageAction =
                                                receivedMessage.MessageAction,
                                            MessageType = MessageType.Error,
                                            MessageBody = errorMessageText
                                        }
                                };

            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }
    }
}