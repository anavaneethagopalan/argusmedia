﻿namespace AOM.ControlClient.v1.Routers
{
    using System.Collections.Generic;
    using System.Linq;

    using AOM.ControlClient.Interfaces;
    using AOM.ControlClient.Routers;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Users;

    using Argus.Transport.Infrastructure;

    using System;

    using Utils.Extension;

    public class UserDistributionService : RouterServiceBase, IUserDistributionService
    {
        private const string TerminateUserMessage =
            "You have been logged off, please contact Aom Support on aomsupport@argusmedia.com or call +44(0)2071999430";

        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private bool disposed;

        public UserDistributionService(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IClientSessionMessageHandler clientMessageHandler)
            : base(aomBus, topicManager, clientMessageHandler)
        {
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<TerminateResponse>(Environment.MachineName, OnTerminateUser);
            yield return
                _aomBus.Subscribe<TerminateOtherUserSessionsResponse>(
                    Environment.MachineName,
                    OnTerminateUserOtherSessions);
        }

        private void OnTerminateUserOtherSessions(TerminateOtherUserSessionsResponse terminateUserOtherSessionsResponse)
        {
            if (terminateUserOtherSessionsResponse.Message.MessageType != MessageType.Error
                && terminateUserOtherSessionsResponse.UserId > 0)
            {
                SendMessageToUserOtherSessions(
                    terminateUserOtherSessionsResponse.SessionId,
                    terminateUserOtherSessionsResponse.UserId,
                    !string.IsNullOrEmpty(terminateUserOtherSessionsResponse.Reason)
                        ? terminateUserOtherSessionsResponse.Reason
                        : TerminateUserMessage);

                var otherSessions = _topicManager.GetUserOtherSessions(
                    terminateUserOtherSessionsResponse.SessionId,
                    terminateUserOtherSessionsResponse.UserId);

                if (otherSessions.Any())
                {
                    SendMessageToUserSessionOnly(
                        terminateUserOtherSessionsResponse.SessionId,
                        terminateUserOtherSessionsResponse.UserId,
                        "Your other open sessions have been logged off");
                }

                _topicManager.RemoveUserOtherSessions(
                    terminateUserOtherSessionsResponse.SessionId,
                    terminateUserOtherSessionsResponse.UserId);
            }
        }

        public void OnTerminateUser(TerminateResponse terminateUserReposnse)
        {
            if (terminateUserReposnse.Message.MessageType != MessageType.Error && terminateUserReposnse.UserName != null)
            {
                SendTerminateMessageToUser(
                    terminateUserReposnse.UserId,
                    !string.IsNullOrEmpty(terminateUserReposnse.Reason)
                        ? terminateUserReposnse.Reason
                        : TerminateUserMessage);

                _topicManager.RemoveUserSessions(terminateUserReposnse.UserId);
            }
        }

        public void SendTerminateMessageToUser(long userId, string message)
        {
            var csi = new ClientSessionInfo { UserId = userId };

            var terminateResponse = new TerminateResponse
                                    {
                                        Message =
                                            new Message
                                            {
                                                ClientSessionInfo = csi,
                                                MessageAction = MessageAction.Void,
                                                MessageType = MessageType.Logoff,
                                                MessageBody = message
                                            },
                                        UserId = userId,
                                    };

            _clientMessageHandler.SendMessageToAllUserSessions(userId, terminateResponse.ToJsonCamelCase());
        }

        public void SendMessageToUserSessionOnly(string currentSessionId, long userId, string message)
        {
            var csi = new ClientSessionInfo { UserId = userId };

            var terminateResponse = new TerminateResponse
                                    {
                                        Message =
                                            new Message
                                            {
                                                ClientSessionInfo = csi,
                                                MessageAction = MessageAction.Void,
                                                MessageType = MessageType.UserMessage,
                                                MessageBody = message
                                            },
                                        UserId = userId,
                                    };

            _clientMessageHandler.SendMessageThisSessionOnly(
                currentSessionId,
                userId,
                terminateResponse.ToJsonCamelCase());
        }

        public void SendMessageToUserOtherSessions(string currentSessionId, long userId, string message)
        {
            var csi = new ClientSessionInfo { UserId = userId };

            var terminateResponse = new TerminateResponse
                                    {
                                        Message =
                                            new Message
                                            {
                                                ClientSessionInfo = csi,
                                                MessageAction = MessageAction.Void,
                                                MessageType = MessageType.Logoff,
                                                MessageBody = message
                                            },
                                        UserId = userId,
                                    };

            _clientMessageHandler.SendMessageToUserOtherSessions(
                currentSessionId,
                userId,
                terminateResponse.ToJsonCamelCase());
        }

        public override void RouteMessage(Message message)
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }
    }
}