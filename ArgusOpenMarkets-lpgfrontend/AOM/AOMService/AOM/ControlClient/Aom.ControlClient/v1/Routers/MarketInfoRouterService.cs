﻿using System.Collections.Generic;

namespace AOM.ControlClient.v1.Routers
{
    using System;

    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.MarketInfos;

    using Argus.Transport.Infrastructure;

    using Newtonsoft.Json;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class MarketInfoRouterService : IMarketInfoRouterService
    {
        private readonly IBus _aomBus;
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private readonly IClientSessionMessageHandler _clientMessageHandler;
        private bool disposed;

        public MarketInfoRouterService(
            IBus aomBus,
            IClientSessionMessageHandler clientSessionMessageHandler)
        {
            _aomBus = aomBus;
            _clientMessageHandler = clientSessionMessageHandler;
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }


        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomMarketInfoResponse>(SubscriptionId, ForwardMarketInfoResponse);
        }

        public void RouteMessage(Message message)
        {
            ForwardMarketInfoRequest(message, new AomMarketInfoRequest());
        }

        private void ForwardMarketInfoRequest<T>(Message receivedMsg, T requestToSend) where T : AomMarketInfoRequest
        {
            Log.Debug(receivedMsg.MessageAction + " Market Info Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Info)
            {
                SendErrorToClient(
                    receivedMsg,
                    LogException.LogString("Message type is not Info!", "ForwardNewMarketInfoRequest"));
            }
            try
            {
                var info = JsonConvert.DeserializeObject<MarketInfo>(receivedMsg.MessageBody.ToString());
                requestToSend.MarketInfo = info;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                SendErrorToClient(receivedMsg, LogException.Log(error, receivedMsg.MessageAction + " market info"));
            }
        }

        private void ForwardMarketInfoResponse<T>(T response) where T : AomMarketInfoResponse
        {
            if (response.Message.MessageType == MessageType.Info)
            {
                try
                {
                    _clientMessageHandler.SendMessage(response.Message.ClientSessionInfo, response.ToJsonCamelCase());
                }
                catch (Exception exception)
                {
                    LogException.Log(exception, "ERROR SENDING AomMarketInfoResponse TO CLIENT");
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
            else
            {
                SendErrorToClient(
                    response.Message,
                    LogException.LogString(
                        string.Format("Unexpected response from service --> {0}", response),
                        "ForwardMarketInfoResponse"));
            }
        }

        private void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomMarketInfoResponse
                                {
                                    MarketInfo = null,
                                    Message =
                                        new Message
                                        {
                                            ClientSessionInfo =
                                                receivedMessage.ClientSessionInfo,
                                            MessageAction =
                                                receivedMessage.MessageAction,
                                            MessageType = MessageType.Error,
                                            MessageBody = errorMessageText
                                        }
                                };
            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "MarketInfoRouterService";
            }
        }
    }
}