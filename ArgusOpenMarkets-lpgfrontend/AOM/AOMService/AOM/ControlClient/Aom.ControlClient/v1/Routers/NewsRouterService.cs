﻿namespace AOM.ControlClient.v1.Routers
{
    using System;
    using System.Collections.Generic;

    using AOM.Diffusion.Common;
    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.News;

    using Argus.Transport.Infrastructure;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class NewsRouterService : RouterServiceBase, INewsRouterService
    {
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private bool disposed;

        public NewsRouterService(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IClientSessionMessageHandler clientMessageHandler)
            : base(aomBus, topicManager, clientMessageHandler)
        {

        }


        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomNewsPublished>(SubscriptionId, OnAomNewsPublished);
            yield return _aomBus.Subscribe<PurgeNewsResponse>(SubscriptionId, OnAomNewsPurgeResponse);
            PublishReplayArgusNews();
        }

        private void OnAomNewsPurgeResponse(PurgeNewsResponse obj)
        {
            var topicPath = "AOM/News/";
            _topicManager.DeleteTopic(topicPath, ">", new TopicControlDeleteCallback());

            var replayNewsRequest = new ReplayArgusNews();
            replayNewsRequest.DaysToReplay = obj.DaysToKeep;
            _aomBus.Publish(replayNewsRequest);

        }

        private void PublishReplayArgusNews()
        {
            _aomBus.Publish(new ReplayArgusNews());
        }

        private void OnAomNewsPublished(AomNewsPublished aomNewsPublishedEvent)
        {
            if (aomNewsPublishedEvent.News != null)
            {
                if (aomNewsPublishedEvent.News.IsFree)
                {
                    // If the news is free - we just publish the news to a single content stream.  
                    SendFreeNews(aomNewsPublishedEvent);
                }
                else if (aomNewsPublishedEvent.News.ContentStreams != null)
                {
                    SendNewsToMultipleTopics(aomNewsPublishedEvent);
                }
                else
                {
                    Log.Error(
                        "News Router Service - news item is not free and has no content streams, so cannot publish.  Article: "
                        + aomNewsPublishedEvent.ToJsonCamelCase());
                }
            }
        }

        private void SendNewsToMultipleTopics(AomNewsPublished aomNewsPublishedEvent)
        {
            if (aomNewsPublishedEvent.News != null)
            {
                foreach (var cs in aomNewsPublishedEvent.News.ContentStreams)
                {
                    var topicPath = GenerateNewsTopicPath(aomNewsPublishedEvent.News, false, cs);
                    _topicManager.SendDataToTopic(topicPath, aomNewsPublishedEvent.ToJsonCamelCase());
                }
            }
        }

        private void SendFreeNews(AomNewsPublished aomNewsPublishedEvent)
        {
            var topicPath = GenerateNewsTopicPath(aomNewsPublishedEvent.News, true, 0);
            _topicManager.SendDataToTopic(topicPath, aomNewsPublishedEvent.ToJsonCamelCase());
        }

        private string GenerateNewsTopicPath(AomNews news, bool isFree, ulong contentStreamId)
        {
            var topicPath = "AOM/News/";

            if (isFree)
            {
                topicPath += "Free/" + news.CmsId;
            }
            else
            {
                topicPath += "SEC-ContentStreams/" + contentStreamId + "/" + news.CmsId;
            }

            return topicPath;
        }

        public override void RouteMessage(Message message)
        {
            // We should never have any News Messages from the client.
            Log.Error(
                "News Router Service - Route Message called.  Should not receive these events from client.  Message: "
                + message);
        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "NewsRouterService";
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }
    }
}