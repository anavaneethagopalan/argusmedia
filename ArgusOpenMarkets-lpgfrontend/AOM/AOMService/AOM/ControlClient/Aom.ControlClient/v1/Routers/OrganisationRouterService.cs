﻿using System.ComponentModel;
﻿using System.Linq;
﻿using System.Reflection;
﻿using AOM.Diffusion.Common;
﻿using AOM.Transport.Events.BrokerPermissions;
using AOM.Transport.Events.CounterpartyPermissions;
using Newtonsoft.Json;
﻿using Utils.Logging.Utils;

namespace AOM.ControlClient.v1.Routers
{
    using System;
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Organisations;

    using Argus.Transport.Infrastructure;

    using Utils.Extension;

    public class OrganisationRouterService : RouterServiceBase, IOrganisationRouterService
    {
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private const string PermissionsPath = "AOM/SEC-Organisations";

        private bool disposed;

        public OrganisationRouterService(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IClientSessionMessageHandler clientMessageHandler)
            : base(aomBus, topicManager, clientMessageHandler)
        {
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        public IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return
                _aomBus.Subscribe<GetCounterpartyPermissionsRefreshResponse>(
                    SubscriptionId,
                    PushFullCounterpartyPermissions);
            yield return
                _aomBus.Subscribe<GetBrokerPermissionsRefreshResponse>(SubscriptionId, PushFullBrokerPermissions);
            yield return
                _aomBus.Subscribe<GetCounterpartyPermissionsRefreshResponse>(
                    SubscriptionId + "Summary",
                    PushSummaryCounterpartyPermissions);
            yield return
                _aomBus.Subscribe<GetBrokerPermissionsRefreshResponse>(
                    SubscriptionId + "Summary",
                    PushSummaryBrokerPermissions);
             yield return 
                 _aomBus.Subscribe<GetCompanyDetailsRefreshResponse>(
                    SubscriptionId, 
                    PushFullCompanyDetails);
             yield return 
                 _aomBus.Subscribe<GetCounterpartyPermissionsRefreshResponse>(
                    SubscriptionId, 
                    PushFullCounterpartyPermissions);
             yield return 
                 _aomBus.Subscribe<GetCounterpartyPermissionsRefreshResponse>(
                    SubscriptionId + "Summary", 
                    PushSummaryCounterpartyPermissions);
             yield return 
                 _aomBus.Subscribe<AomCounterpartyPermissionResponse>(
                    SubscriptionId, 
                    ForwardCounterpartyPermissionResponse);
             yield return 
                 _aomBus.Subscribe<AomBrokerPermissionResponse>(
                    SubscriptionId, 
                    ForwardBrokerPermissionResponse);
        }

        private void ConsumeCompanyDetailsRequestProcessedResponse(CompanyDetailsRpcProcessedResponse obj)
        {
            if (obj.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(obj.Message,obj.Message.MessageBody.ToString());
            }
            switch (obj.Message.MessageAction)
            {
                case MessageAction.Delete:
                    var notificationTopic = string.Join("/", PermissionsPath, obj.OrganisationId, "SystemNotifications", obj.EventTypeId, obj.Id);
                    _topicManager.DeleteTopic(
                        notificationTopic,
                        ">",
                        new TopicControlDeleteCallback());

                    break;
                case MessageAction.Create:
                    _aomBus.Publish(new GetCompanyDetailsRefreshRequest());
                    break;
            }
            
        }

        private static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        private void PublishEventNotificationTypesToTopic(string topic)
        {
            foreach (var notificationType in (SystemEventType[]) Enum.GetValues(typeof (SystemEventType)))
            {
                _topicManager.SendDataToTopic(string.Join("/", topic, (int) notificationType),
                    new
                    {
                        Id = (int) notificationType,
                        Description = GetEnumDescription(notificationType)
                    }.ToJsonCamelCase());
            }
        }

        private void PublishSubscribedProductsToTopic(string topic, IEnumerable<SubscribedProduct> subscribedProducts)
        {
            if (subscribedProducts != null)
            {
                foreach (var sp in subscribedProducts)
                {
                    _topicManager.SendDataToTopic(string.Join("/", topic, sp.ProductId), sp.ToJsonCamelCase());
                }
            }
        }

        private void PushFullCompanyDetails(GetCompanyDetailsRefreshResponse obj)
        {
            foreach (var organisationDetails in obj.Details)
            {
                PublishEventNotificationTypesToTopic(string.Join("/", PermissionsPath,
                    organisationDetails.Organisation.Id, "SystemNotificationTypes"));

                PublishSubscribedProductsToTopic(string.Join("/", PermissionsPath,
                    organisationDetails.Organisation.Id, "SubscribedProducts"), organisationDetails.SubscribedProducts);

                PublishSystemNotificationToTopic(string.Join("/", PermissionsPath,
                    organisationDetails.Organisation.Id, "SystemNotifications"), organisationDetails.SystemNotifications);

             }
        }

        private void PublishSystemNotificationToTopic(string path, IEnumerable<NotificationMapping> systemNotifications)
        {
            foreach (var sn in systemNotifications)
            {
                _topicManager.SendDataToTopic(string.Join("/", path, (int) sn.EventType, sn.Id),
                    new
                    {
                        sn.Id,
                        sn.Product.Name,
                        sn.EmailAddress,
                        sn.EventType
                    }.ToJsonCamelCase());
            }
        }

        private void PushFullBrokerPermissions(GetBrokerPermissionsRefreshResponse response)
        {
            foreach (var brokerPermission in response.BrokerPermissions)
            {
                var permissionDetail = new PermissionDetail
                {
                    Message = new Message {MessageType = MessageType.BrokerPermissionDetail},
                    UsThem = UsThem.Us,
                    Permission = brokerPermission.BuySide,
                };

                _topicManager.SendDataToTopic(permissionDetail.GetDetailPath(BrokerOrPrincipalPermission.Broker), permissionDetail.ToJsonCamelCase());
                permissionDetail.UsThem = UsThem.Them;
                _topicManager.SendDataToTopic(permissionDetail.GetDetailPath(BrokerOrPrincipalPermission.Broker), permissionDetail.ToJsonCamelCase());
                permissionDetail.Permission = brokerPermission.SellSide;
                permissionDetail.UsThem = UsThem.Us;
                _topicManager.SendDataToTopic(permissionDetail.GetDetailPath(BrokerOrPrincipalPermission.Broker), permissionDetail.ToJsonCamelCase());
                permissionDetail.UsThem = UsThem.Them;
                _topicManager.SendDataToTopic(permissionDetail.GetDetailPath(BrokerOrPrincipalPermission.Broker), permissionDetail.ToJsonCamelCase());
            }
        }

        private void PushFullCounterpartyPermissions(GetCounterpartyPermissionsRefreshResponse response)
        {
            foreach (var counterpartyPermissionPair in response.CounterpartyPermissions)
            {
                var permissionDetail = new PermissionDetail
                {
                    Message = new Message {MessageType = MessageType.CounterpartyPermissionDetail},
                    Permission = counterpartyPermissionPair.BuySide,
                    UsThem = UsThem.Us,
                };
                _topicManager.SendDataToTopic(permissionDetail.GetDetailPath(BrokerOrPrincipalPermission.Principal), permissionDetail.ToJsonCamelCase());
                permissionDetail.UsThem = UsThem.Them;
                _topicManager.SendDataToTopic(permissionDetail.GetDetailPath(BrokerOrPrincipalPermission.Principal), permissionDetail.ToJsonCamelCase());
                permissionDetail.Permission = counterpartyPermissionPair.SellSide;
                permissionDetail.UsThem = UsThem.Us;
                _topicManager.SendDataToTopic(permissionDetail.GetDetailPath(BrokerOrPrincipalPermission.Principal), permissionDetail.ToJsonCamelCase());
                permissionDetail.UsThem = UsThem.Them;
                _topicManager.SendDataToTopic(permissionDetail.GetDetailPath(BrokerOrPrincipalPermission.Principal), permissionDetail.ToJsonCamelCase());
            }
        }

        private void PushSummaryCounterpartyPermissions(GetCounterpartyPermissionsRefreshResponse response)
        {
            foreach (var counterpartyPermissionPair in response.CounterpartyPermissions)
            {
                var permissionSummary = new PermissionSummary()
                {
                    Message = new Message { MessageType = MessageType.CounterpartyPermission },
                    CanTrade = counterpartyPermissionPair.CanTradeWith(),
                    OrganisationId = counterpartyPermissionPair.BuySide.TheirOrganisation
                };
                var buyPath = string.Join(
                    "/",
                    PermissionsPath,
                    counterpartyPermissionPair.BuySide.OurOrganisation,
                    "PermissionsMatrix",
                    "Summary",
                    "SEC-Products",
                    counterpartyPermissionPair.ProductId,
                    "Principals",
                    BuyOrSell.Buy,
                    counterpartyPermissionPair.BuySide.TheirOrganisation);
                if (permissionSummary.CanTrade)
                {
                    _topicManager.SendDataToTopic(buyPath, permissionSummary.ToJsonCamelCase());
                }
                else
                {
                    _topicManager.DeleteTopic(buyPath, "*", new TopicControlDeleteCallback());
                }


                permissionSummary.OrganisationId = counterpartyPermissionPair.SellSide.TheirOrganisation;

                var sellPath = string.Join(
                    "/",
                    PermissionsPath,
                    counterpartyPermissionPair.SellSide.OurOrganisation,
                    "PermissionsMatrix",
                    "Summary",
                    "SEC-Products",
                    counterpartyPermissionPair.ProductId,
                    "Principals",
                    BuyOrSell.Sell,
                    counterpartyPermissionPair.SellSide.TheirOrganisation);

                if (permissionSummary.CanTrade)
                {
                    _topicManager.SendDataToTopic(sellPath, permissionSummary.ToJsonCamelCase());
                }
                else
                {
                    _topicManager.DeleteTopic(sellPath, "*", new TopicControlDeleteCallback());
                }
            }
        }

        private void PushSummaryBrokerPermissions(GetBrokerPermissionsRefreshResponse response)
        {
            foreach (var brokerPermission in response.BrokerPermissions)
            {
                var permissionSummary = new PermissionSummary()
                {
                    Message = new Message { MessageType = MessageType.BrokerPermission },
                    CanTrade = brokerPermission.CanTradeWith(),
                    OrganisationId = brokerPermission.BuySide.TheirOrganisation
                };
                var ownOrgPath = string.Join(
                    "/",
                    PermissionsPath,
                    brokerPermission.BuySide.OurOrganisation,
                    "PermissionsMatrix",
                    "Summary",
                    "SEC-Products",
                    brokerPermission.ProductId,
                    "Brokers",
                    BuyOrSell.Buy,
                    brokerPermission.BuySide.TheirOrganisation);

                if (permissionSummary.CanTrade)
                {
                    _topicManager.SendDataToTopic(ownOrgPath, permissionSummary.ToJsonCamelCase());
                }
                else
                {
                    _topicManager.DeleteTopic(ownOrgPath, "*", new TopicControlDeleteCallback());
                }
                permissionSummary.OrganisationId = brokerPermission.SellSide.TheirOrganisation;

                var broPath = string.Join(
                    "/",
                    PermissionsPath,
                    brokerPermission.SellSide.OurOrganisation,
                    "PermissionsMatrix",
                    "Summary",
                    "SEC-Products",
                    brokerPermission.ProductId,
                    "Brokers",
                    BuyOrSell.Sell,
                    brokerPermission.SellSide.TheirOrganisation);

                if (permissionSummary.CanTrade)
                {
                    _topicManager.SendDataToTopic(broPath, permissionSummary.ToJsonCamelCase());
                }
                else
                {
                    _topicManager.DeleteTopic(broPath, "*", new TopicControlDeleteCallback());
                }
            }
        }



        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (IDisposable subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }


        protected virtual string SubscriptionId
        {
            get { return "OrganisationRouterService"; }
        }

        public override void RouteMessage(Message message)
        {
            switch (message.MessageType)
            {
                case MessageType.CounterpartyPermission:
                    ForwardCounterpartyPermissionRequest(message, new AomCounterpartyPermissionRequest());
                    break;
                case MessageType.BrokerPermission:
                    ForwardBrokerPermissionRequest(message, new AomBrokerPermissionRequest());
                    break;
                case MessageType.OrganisationNotificationDetail:
                    ForwardOrganisationNotificationRequest(message, new CompanyDetailsRpcRequest());
                    break;

            }
        }

        private void ForwardOrganisationNotificationRequest<T>(Message message, T requestToSend) where T : CompanyDetailsRpcRequest
        {
            Log.Debug(message.MessageAction + " CompanyDetailsRequest Received " + message.MessageBody);
            if (message.MessageType != MessageType.OrganisationNotificationDetail)
            {
                SendErrorToClient(
                    message,
                    LogException.LogString("Message type is not OrganisationNotificationDetail!", "ForwardOrganisationNotificationRequest"));
            }
            try
            {
                var deserialisedObj = JsonConvert.DeserializeObject<CompanyDetailsRpcRequest>(message.MessageBody.ToString());

                requestToSend.ClientSessionInfo = message.ClientSessionInfo;
                requestToSend.MessageAction = message.MessageAction;

                switch (message.MessageAction)
                {
                    case MessageAction.Delete:
                        requestToSend.Id = deserialisedObj.Id;
                        break;
                    case MessageAction.Create:
                        requestToSend.ProductId = deserialisedObj.ProductId;
                        requestToSend.EmailAddress = deserialisedObj.EmailAddress;
                        requestToSend.EventType = deserialisedObj.EventType;
                        break;
                }

                var result = _aomBus.Request<CompanyDetailsRpcRequest,CompanyDetailsRpcProcessedResponse>(requestToSend);
                result.Message.ClientSessionInfo = message.ClientSessionInfo;
                ConsumeCompanyDetailsRequestProcessedResponse(result);
            }
            catch (Exception error)
            {
                SendErrorToClient(message, LogException.Log(error, message.MessageAction + " CounterpartyPermission"));
            }
        }

        private void ForwardCounterpartyPermissionRequest<T>(Message receivedMsg, T requestToSend) where T : AomCounterpartyPermissionRequest
        {
            Log.Debug(receivedMsg.MessageAction + " CounterpartyPermission Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.CounterpartyPermission)
            {
                SendErrorToClient(
                    receivedMsg,
                    LogException.LogString("Message type is not CounterpartyPermission!", "ForwardCounterpartyPermissionRequest"));
            }
            try
            {
                var permission = JsonConvert.DeserializeObject<MatrixPermission>(receivedMsg.MessageBody.ToString());
                requestToSend.CounterpartyPermission = permission;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                SendErrorToClient(receivedMsg, LogException.Log(error, receivedMsg.MessageAction + " CounterpartyPermission"));
            }
        }

        private void ForwardBrokerPermissionRequest<T>(Message receivedMsg, T requestToSend) where T : AomBrokerPermissionRequest
        {
            Log.Debug(receivedMsg.MessageAction + " BrokerPermission Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.BrokerPermission)
            {
                SendErrorToClient(
                    receivedMsg,
                    LogException.LogString("Message type is not BrokerPermission!", "ForwardBrokerPermissionRequest"));
            }
            try
            {
                var permission = JsonConvert.DeserializeObject<MatrixPermission>(receivedMsg.MessageBody.ToString());
                requestToSend.BrokerPermission = permission;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                SendErrorToClient(receivedMsg, LogException.Log(error, receivedMsg.MessageAction + " BrokerPermission"));
            }
        }

        private void ForwardCounterpartyPermissionResponse<T>(T response) where T : AomCounterpartyPermissionResponse
        {
            switch (response.Message.MessageType)
            {
                case MessageType.CounterpartyPermission:
                    // no action
                    break;
                case MessageType.Error:
                    SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
                    break;
                default:
                    SendErrorToClient(response.Message, LogException.LogString(string.Format("Unexpected response from service --> {0}", response),
                        "OganisationRouterSerice.ForwardCounterpartyPermissionResponse"));
                    break;
            }
        }

        private void ForwardBrokerPermissionResponse<T>(T response) where T : AomBrokerPermissionResponse
        {
            switch (response.Message.MessageType)
            {
                case MessageType.BrokerPermission:
                    // no action
                    break;
                case MessageType.Error:
                    SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
                    break;
                default:
                    SendErrorToClient(response.Message, LogException.LogString(string.Format("Unexpected response from service --> {0}", response),
                        "OganisationRouterSerice.ForwardBrokerPermissionResponse"));
                    break;
            }
        }
    }
}