namespace AOM.ControlClient.v1.Routers
{
    using System;
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Deals;

    using Argus.Transport.Infrastructure;

    using Newtonsoft.Json;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class InternalDealRouterService : IInternalDealRouterService
    {
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private readonly IBus _aomBus;

        private readonly IClientSessionMessageHandler _clientMessageHandler;

        private bool disposed;

        public InternalDealRouterService(IBus aomBus, 
            IClientSessionMessageHandler clientSessionMessageHandler)
        {
            _aomBus = aomBus;
            _clientMessageHandler = clientSessionMessageHandler;
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomDealResponse>(SubscriptionId, ForwardInternalDealResponse);
        }

        public void RouteMessage(Message message)
        {
            ForwardInternalDealRequest(message, new AomDealRequest());
        }

        private void ForwardInternalDealRequest<T>(Message receivedMsg, T requestToSend) where T : AomDealRequest
        {
            Log.Debug(receivedMsg.MessageAction + " Internal deal Received " + receivedMsg.MessageBody);
            if (receivedMsg.MessageType != MessageType.Deal)
            {
                SendErrorToClient(
                    receivedMsg,
                    LogException.LogString("Message type is not Deal!", "ForwardInternalDealRequest"));
            }
            try
            {
                var deal = JsonConvert.DeserializeObject<Deal>(receivedMsg.MessageBody.ToString());
                requestToSend.Deal = deal;
                requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
                requestToSend.MessageAction = receivedMsg.MessageAction;

                _aomBus.Publish(requestToSend);

            }
            catch (Exception error)
            {
                SendErrorToClient(receivedMsg, LogException.Log(error, receivedMsg.MessageAction + " Internal deal"));
            }
        }

        private void ForwardInternalDealResponse<T>(T response) where T : AomDealResponse
        {
            if (response.Message.MessageType == MessageType.Deal)
            {
                try
                {
                    _clientMessageHandler.SendMessage(response.Message.ClientSessionInfo, response.ToJsonCamelCase());
                }
                catch (Exception exception)
                {
                    LogException.Log(exception, "ERROR SENDING AomDealResponse TO CLIENT");
                }
            }
            else if (response.Message.MessageType == MessageType.Error)
            {
                SendErrorToClient(response.Message, response.Message.MessageBody.ToString());
            }
            else
            {
                SendErrorToClient(
                    response.Message,
                    LogException.LogString(
                        string.Format("Unexpected response from service --> {0}", response),
                        "ForwardInternalDealResponse"));
            }
        }

        private void SendErrorToClient(Message receivedMessage, string errorMessageText)
        {
            var errorResponse = new AomDealResponse
                                {
                                    Deal = null,
                                    Message =
                                        new Message
                                        {
                                            ClientSessionInfo =
                                                receivedMessage.ClientSessionInfo,
                                            MessageAction = receivedMessage.MessageAction,
                                            MessageType = MessageType.Error,
                                            MessageBody = errorMessageText
                                        }
                                };
            _clientMessageHandler.SendMessage(receivedMessage.ClientSessionInfo, errorResponse.ToJsonCamelCase());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "InternalDealRouterService";
            }
        }
    }
}