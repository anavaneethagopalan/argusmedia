﻿namespace AOM.ControlClient.v1.Routers
{
    using System;
    using System.Collections.Generic;

    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    using Utils.Extension;

    internal class ProductsRouterService : RouterServiceBase, IProductsRouterService
    {
        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private bool disposed;

        public ProductsRouterService(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IClientSessionMessageHandler clientSessionMessageHandler)
            : base(aomBus, topicManager, clientSessionMessageHandler)
        {
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            yield return _aomBus.Subscribe<AomMarketStatusChange>(SubscriptionId, ConsumeMarketStatusChange);
        }

        private void ConsumeMarketStatusChange(AomMarketStatusChange marketStatusChange)
        {
            var topic = string.Join("/", "AOM", "SEC-Products", marketStatusChange.Product.ProductId, "MarketStatus");
            var message =
                new { MarketStatus = marketStatusChange.Product.Status, ProductName = marketStatusChange.Product.Name }
                    .ToJsonCamelCase();

            _topicManager.SendDataToTopic(topic, message);
        }

        public override void RouteMessage(Transport.Events.Message message)
        {

        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "ProductRouterService";
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }
    }
}