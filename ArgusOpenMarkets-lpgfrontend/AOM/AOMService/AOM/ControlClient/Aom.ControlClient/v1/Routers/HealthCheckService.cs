﻿namespace AOM.ControlClient.v1.Routers
{
    using AOM.App.Domain.Dates;

    using System.Collections.Generic;
    using System.Reflection;
    using System.Threading;

    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Users;

    using Argus.Transport.Infrastructure;

    using System;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class HealthCheckService : IHealthCheckService
    {
        private Timer _healthCheckTimer;

        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private readonly IDiffusionFacade _topicManager;

        private readonly IDateTimeProvider _dateTimeProvider;

        private readonly IBus _aomBus;

        private const string ProcessorName = "Aom.ControlClient";

        private const int MaximumNumberSecondsToReturnSuccessfulHeartbeatPing = 120;

        private Dictionary<string, ProcessorHealthCheckInternalList> ListProcessorPingResults =
            new Dictionary<string, ProcessorHealthCheckInternalList>();

        private bool disposed;

        public HealthCheckService(IBus aomBus, IDiffusionFacade topicManager, IDateTimeProvider dateTimeProvider)
        {
            _aomBus = aomBus;
            _topicManager = topicManager;
            _dateTimeProvider = dateTimeProvider;
        }

        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            _healthCheckTimer = new Timer(HealthCheckTimerTick, new object(), 0, 30000);
            // THis is subscribing to the response of the Processors and the Control client - so it can push data down a topic."
            yield return _aomBus.Subscribe<ProcessorHealthCheckStatus>(SubscriptionId, OnHealthCheckStatusHeartbeat);

            // This is subscribing to the request for each processor (+ Control Client) to respond to a health check. 
            // Note as only one control client is every connected to rabbit we don't use machinename for subscription id
            yield return _aomBus.Subscribe<ProcessorHealthCheck>(SubscriptionId, OnProcessorHealthCheck);
            // This is the response (RPC) to the web-api calling us for an immediate yes/no answer for the health status.
            _aomBus.Respond<HealthCheckStatusRpc, HealthCheckStatusResult>(WebApiHealthCheckStatusPing);
        }

        private void HealthCheckTimerTick(object data)
        {
            _aomBus.Publish(new ProcessorHealthCheck());
        }


        private void OnProcessorHealthCheck(ProcessorHealthCheck obj)
        {
            _aomBus.Publish(
                new ProcessorHealthCheckStatus
                {
                    Processor = ProcessorName,
                    ProcessorFullName = GetProcessorFullName(),
                    Status = true,
                    MachineName = Environment.MachineName
                });
        }

        private string GetProcessorFullName()
        {
            return Assembly.GetEntryAssembly().FullName;
        }

        private void OnHealthCheckStatusHeartbeat(ProcessorHealthCheckStatus processorHealthCheck)
        {
            // Push this down the correct topic.
            if (processorHealthCheck != null)
            {
                var processor = processorHealthCheck.Processor.Replace(".", "");
                var topic = string.Join("/", "AOM", "SEC-HealthCheck", processor);
                var message = _dateTimeProvider.Now;

                processorHealthCheck.ResponseTime = _dateTimeProvider.Now;
                SaveProcessorPingResponse(processor, message);
                SendDataToTopic(topic, processorHealthCheck.ToJsonCamelCase());
            }
        }

        private void SaveProcessorPingResponse(string processor, DateTime pingTime)
        {
            if (ListProcessorPingResults.ContainsKey(processor))
            {
                ListProcessorPingResults[processor].PingTime = pingTime;
            }
            else
            {
                ListProcessorPingResults.Add(
                    processor,
                    new ProcessorHealthCheckInternalList { PingTime = pingTime, ProcessorName = processor });
            }
        }

        private HealthCheckStatusResult WebApiHealthCheckStatusPing(HealthCheckStatusRpc arg)
        {
            try
            {

                bool pingTimesOk = true;

                foreach (var processor in ListProcessorPingResults)
                {
                    int numSeconds = (_dateTimeProvider.Now - processor.Value.PingTime).Seconds;

                    if (numSeconds > MaximumNumberSecondsToReturnSuccessfulHeartbeatPing)
                    {
                        Log.Info(
                            string.Format(
                                "Processor: {0} is unresponsive.  Number seconds since pinged:{1}",
                                processor,
                                numSeconds));

                        pingTimesOk = false;
                        break;
                    }
                }
                Log.Info(
                    string.Format(
                        "WebApiHealthCheckStatusPing.  Ping Ok: {0}   Number Processors Checked{1}",
                        pingTimesOk,
                        ListProcessorPingResults.Count));

                return new HealthCheckStatusResult { HealthCheckStatusOk = pingTimesOk };

            }
            catch (Exception e)
            {
                Log.Error("WebApiHealthCheckStatusPing", e);
            }

            return new HealthCheckStatusResult { HealthCheckStatusOk = false };
        }

        private void SendDataToTopic(string topic, string message, TopicUpdateCallback callback = null)
        {
            var topicDetailsCallback = new TopicDetailsCallback();

            topicDetailsCallback.TopicDoesNotExist += (s, o) =>
            {
                var addTopicCallback = new TopicControlAddCallback();
                addTopicCallback.TopicAdded +=
                    (sender, s1) => { _topicManager.GetUpdater().PushToTopic(topic, message, true, callback); };
                _topicManager.CreateTopic(topic, addTopicCallback);
            };
            topicDetailsCallback.TopicDetails +=
                (sender, s) => { _topicManager.GetUpdater().PushToTopic(topic, message, true, callback); };
            _topicManager.GetTopicDetails(topic, topicDetailsCallback);
        }

        public void RouteMessage(Message message)
        {

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var subscriptionHandler in _subscriptionHandlers)
                    {
                        subscriptionHandler.Dispose();
                    }
                    _subscriptionHandlers.Clear();
                }
            }
            disposed = true;
        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "HealthCheckRouterService";
            }
        }
    }

    public class ProcessorHealthCheckInternalList
    {
        public string ProcessorName { get; set; }

        public DateTime PingTime { get; set; }
    }
}