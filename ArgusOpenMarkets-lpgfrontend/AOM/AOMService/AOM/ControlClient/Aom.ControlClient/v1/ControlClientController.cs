﻿using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;

namespace AOM.ControlClient.v1
{
    using System;
    using System.Threading;

    using AOM.Transport.Events.Organisations;
    using AOM.ControlClient.v1.Interfaces;
    using AOM.Diffusion.Common;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events.MarketTickers;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Topshelf;

    using PushTechnology.ClientInterface.Client.Session;
    using PushTechnology.DiffusionCore.Exceptions;

    using Utils.Extension;
    using Utils.Logging.Utils;

    public class ControlClientController : IServiceBase
    {
        private readonly IAuthenticationManager _authenticationManager;

        private readonly IDiffusionFacade _diffusionFacade;

        private readonly IBus _aomBus;

        private readonly IDiffusionProductTopicTreeBuilder _topicTreeBuilder;

        private readonly IMessageDistributionService _messageDistributionService;

        private HostControl _hostControl;

        private readonly IDiffusionSessionConnection _diffusionSessionConnection;

        private readonly IClientSessionMessageHandler _clientSessionMessageHandler;

        private bool controlClientStandby;

        public ControlClientController(
            IBus aomBus,
            IDiffusionFacade diffusionFacade,
            IAuthenticationManager authenticationManager,
            IDiffusionProductTopicTreeBuilder topicTreeBuilder,
            IMessageDistributionService messageDistributionService,
            IDiffusionSessionConnection diffusionSessionConnection,
            IClientSessionMessageHandler clientSessionMessageHandler)
        {
            _aomBus = aomBus;
            _diffusionFacade = diffusionFacade;
            _authenticationManager = authenticationManager;
            _topicTreeBuilder = topicTreeBuilder;
            _messageDistributionService = messageDistributionService;
            _diffusionSessionConnection = diffusionSessionConnection;
            _clientSessionMessageHandler = clientSessionMessageHandler;
        }

        private void Session_ErrorNotified(object sender, SessionErrorHandlerEventArgs e)
        {
            Log.Error("ControlClientController.Session.Error: " + e.Error.Message);

            throw new DiffusionException("Diffusion Session Error occured");
        }

        private void Session_StateChanged(object sender, SessionListenerEventArgs e)
        {
            Log.Info("ControlClientController.Session.StateChanged: " + e.NewState);

            if (!controlClientStandby && e.NewState == SessionState.CLOSED_BY_SERVER)
            {
                Log.Error("CONNECTION CLOSED BY SERVER - attempting connection to failover and stopping service");
                this.Stop(_hostControl);
            }

            if (controlClientStandby && e.NewState == SessionState.CLOSED_BY_SERVER)
            {
                ConnectToDiffusion();
            }
        }

        public bool Start(HostControl hostControl)
        {
            _hostControl = hostControl;
            Log.Info("ControlClientController.Start");
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            ConnectToDiffusion();
            return true;
        }

        private void ConnectToDiffusion()
        {
            // Loop until we are connected.
            Log.Info("CONNECT TO DIFFUSION - ControlClientController.ConnectToDiffusion");

            var isConnected = false;
            var retryCount = 0;


            while (!isConnected)
            {
                if (_diffusionSessionConnection.ConnectAndStartSession())
                {
                    Log.Debug("SUCCESSFULLY CONNECTED TO DIFFUSION");

                    _diffusionSessionConnection.Session.StateChanged += Session_StateChanged;
                    _diffusionSessionConnection.Session.ErrorNotified += Session_ErrorNotified;

                    _diffusionFacade.AttachToSession(_diffusionSessionConnection.Session);
                    _clientSessionMessageHandler.AttachToSession(_diffusionSessionConnection.Session);

                    RegisterToBeTopicSourceUpdater();

                    isConnected = true;
                }
                else
                {
                    Log.Warn(string.Format("RECONNECT TO DIFFUSION FAILED - Retry count:{0}", retryCount));
                    retryCount++;
                    Thread.Sleep(500);
                }
            }
        }

        private void RegisterToBeTopicSourceUpdater()
        {
            _diffusionFacade.RegisterTopicSource();
            _diffusionFacade.SetOnStandbyCallback(OnStandby);
            _diffusionFacade.SetOnActiveCallback(OnActive);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Error("Unhandled exception within control client", e.ExceptionObject as Exception);
        }

        public bool Stop(HostControl hostControl)
        {
            _aomBus.Dispose();
            Log.Info("ControlClientController.Stop");

            //try
            //{
            //    // Environment.Exit(1);
            //}
            //catch (Exception ex)
            //{
            //    Log.Error("Error shutting down control client");
            //}

            return true;
        }

        public void OnStandby()
        {
            Log.Info("ControlClientController.OnStandby");

            //try create the topic
            Log.Info("Attempting to create AOM topic incase doesnt exist");
            var topicControlAddCallback = new TopicControlAddCallback();
            controlClientStandby = true;
            topicControlAddCallback.TopicAddFailed += (o, s1) =>
            {
                Log.InfoFormat("STANDYBY - ATTTEMPT TO CREATE AOM TOPIC FAILED - Could not create AOM Topic: {0}", s1);
            };
            topicControlAddCallback.TopicAdded += (o, s1) =>
            {
                // Log.Info("AOM Topic created, registering as TopicSource updater");
                // RegisterToBeTopicSourceUpdater();
            };
            _diffusionFacade.CreateTopic("AOM", topicControlAddCallback);

            // Create the Diffusion Admin Authnentication Process.  
        }

        public void OnActive()
        {
            // START THE AOM Authentication manager - can only run when Active...  
            _authenticationManager.Start();
            Log.Info("Authentication Manager has started");

            Log.Info("ControlClientController.OnActive");
            controlClientStandby = false;
            //only the active control client can update the topic tree
            _diffusionFacade.AddMessageHandler("AOM/SEC-Users", _messageDistributionService);

            _messageDistributionService.MessageHandlerActive += (o, s) =>
            {
                Log.InfoFormat("Message Handler for topic {0} has become active.", s);
                _messageDistributionService.Run();
                Log.Info("ControlClientController Rebuilding content");

                _topicTreeBuilder.Run();
                RebuildMarketTickerContent(1);
                RebuildCounterpartyMatrix();
                RebuildBrokerMatrix();
                RebuildCompanyDetails();
            };

            _messageDistributionService.MessageHandlerClosed += (o, s) => Log.Error("Message Handler Closed");
        }

        private void RebuildCounterpartyMatrix()
        {
            _aomBus.Publish(new GetCounterpartyPermissionsRefreshRequest());
        }

        private void RebuildBrokerMatrix()
        {
            _aomBus.Publish(new GetBrokerPermissionsRefreshRequest());
        }

        private void RebuildCompanyDetails()
        {
            _aomBus.Publish(new GetCompanyDetailsRefreshRequest());
        }

        private void RebuildMarketTickerContent(int daysToPopulate)
        {
            try
            {
                var refreshMessage = new AomRefreshMarketTickerRequest
                                     {
                                         RefreshFromTime =
                                             DateTime.Today.AddWeekDays(
                                                 -daysToPopulate),
                                         RefreshToTime = DateTime.Today.AddWeekDays(1)
                                     };

                _aomBus.Publish(refreshMessage);

            }
            catch (Exception ex)
            {
                Log.Error("ControlClientControlle.RebuildMarketTickerContent", ex);
            }
        }
    }
}