﻿using AOM.Transport.Events;

namespace AOM.ControlClient.v1
{
    using System.Collections.Generic;
    using System;

    using AOM.App.Domain.Entities;
    using AOM.ControlClient.Interfaces;
    using AOM.Diffusion.Common;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events.Assessments;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    using Utils.Extension;
    using Utils.Logging.Utils;


    public class DiffusionProductTopicTreeBuilder : IDiffusionProductTopicTreeBuilder
    {
        private readonly IDiffusionFacade _aomTopicManager;

        private readonly List<IDisposable> _subscriptionHandlers = new List<IDisposable>();

        private readonly IBus _bus;

        public DiffusionProductTopicTreeBuilder(IDiffusionFacade aomTopicManager, IBus aomBus)
        {
            _aomTopicManager = aomTopicManager;
            _bus = aomBus;
        }


        public void Run()
        {
            _subscriptionHandlers.AddRange(RunAndSubscribe());
        }

        private IEnumerable<IDisposable> RunAndSubscribe()
        {
            var subscriptions = new List<IDisposable>();
            try
            {
                subscriptions.Add(
                    _bus.Subscribe<GetProductConfigResponse>(string.Empty, ConsumeGetProductConfigResponse));
            }
            catch (Exception exception)
            {
                LogException.Log(exception, "Error subscribing to GetProductConfigResponse on AomBus");
            }

            try
            {
                _bus.Publish(new GetProductConfigRequest());
            }
            catch (Exception exception)
            {
                LogException.Log(exception, "Error publishing to GetProductConfigRequest on AomBus");
            }
            return subscriptions;
        }

        private void ConsumeGetProductConfigResponse(GetProductConfigResponse productConfigResponse)
        {
            Log.Info("GetProductConfigResponse message received");

            var productDefinition = productConfigResponse.ProductDefinition;

            if (productDefinition != null)
            {
                var json = productDefinition.ToJsonCamelCase();
                Log.Info("ConsumeGetProductConfigResponse - json returned" + json);
                Log.Info("Product Definition: " + productDefinition);
                BuildProductTopicStructure(productDefinition, json);
            }
            else
            {
                Log.Warn("GetProductConfigResponse.ProductDefinition is null, ignoring message");
            }
        }

        private ClientSessionInfo BuildSystemUserSessionInfo()
        {
            return new ClientSessionInfo
            {
                UserId = -1,
                SessionId = ToString()
            };
        }

        private string BuildProductTopicTree(long productId)
        {
            return string.Join("/", "AOM", "SEC-Products", productId);
        }

        private void BuildProductTopicStructure(ProductDefinitionItem product, string productDefinitionJson)
        {
            
            var productTopicPath = BuildProductTopicTree(product.ProductId);
            Log.Info("BuildProductTopicStructure - Product Topic Path: " + productTopicPath);
            if (_aomTopicManager != null)
            {
                try
                {
                    var productStatusTopicName = string.Format("{0}/MarketStatus", productTopicPath);
                    var productStatus = new { MarketStatus = product.Status, product.ProductName }.ToJsonCamelCase();
                    BuildDiffusionTopicAndPopulateData(productStatusTopicName, productStatus);

                    string productDefinitionTopicName = string.Format("{0}/Definition", productTopicPath);
                    BuildDiffusionTopicAndPopulateData(productDefinitionTopicName, productDefinitionJson);

                    string productContentStreamsTopicName = string.Format("{0}/ContentStreams", productTopicPath);
                    BuildDiffusionTopicAndPopulateData(productContentStreamsTopicName, product.ContentStreamIds.ToJsonCamelCase());

                }
                catch (Exception exception)
                {
                    LogException.Log(exception, "Error communicating with AomTopicManager for Definition topic");
                }
            }
            else
            {
                Log.Info("_aomTopicManager is null - this means were not pushing to topic");
            }

            BuildBidAskStackTree(product.ProductId);

            var systemUserSessionInfo = BuildSystemUserSessionInfo();

            _bus.Publish(new AomRefreshAssessmentRequest
            {
                ProductId = product.ProductId,
                ClientSessionInfo = systemUserSessionInfo
            });
        }

        public void BuildBidAskStackTree(long productId)
        {
            const string OrderTopicSubTree = "/SEC-Privileges/View_BidAsk_Widget/";

            var productTopicPath = BuildProductTopicTree(productId);

            if (_aomTopicManager != null)
            {
                try
                {
                    string topicName = productTopicPath + OrderTopicSubTree + "NotHeld/Orders";

                    Log.Info(string.Format("Building topic: {0}", topicName));

                    var handler = new TopicControlAddCallback();
                    _aomTopicManager.CreateTopic(topicName, handler);
                }
                catch (Exception exception)
                {
                    LogException.Log(exception, "Error communicating with AomTopicManager for Definition topic");
                }
            }
            else
            {
                Log.Info("_aomTopicManager is null");
            }

            if (_aomTopicManager != null)
            {
                try
                {
                    string topicName = productTopicPath + OrderTopicSubTree + "Held/Orders";

                    Log.Info(string.Format("Building topic: {0}", topicName));

                    var handler = new TopicControlAddCallback();
                    _aomTopicManager.CreateTopic(topicName, handler);
                }
                catch (Exception exception)
                {
                    LogException.Log(exception, "Error communicating with AomTopicManager for Definition topic");
                }
            }

            var systemUserSessionInfo = BuildSystemUserSessionInfo();

            _bus.Publish(new AomOrderRefreshRequest
            {
                BusinessDay = DateTime.Today,
                ProductId = productId,
                ClientSessionInfo = systemUserSessionInfo
            });
        }

        private void BuildDiffusionTopicAndPopulateData(string topicName, string data)
        {
            Log.Info(string.Format("Building topic: {0}", topicName));

            var handler = new TopicControlAddCallback();
            handler.TopicAdded += (o, s) =>
            {
                try
                {
                    _aomTopicManager.GetUpdater().PushToTopic(topicName, data, true);
                }
                catch (Exception ex)
                {
                    Log.Error(
                        string.Format(
                            "Error attempting to build diffusion topic and populate.  Topic Name{0}  Data:{1}",
                            topicName,
                            data),
                        ex);
                }

            };

            _aomTopicManager.CreateTopic(topicName, handler, () => { });
        }

        public void Dispose()
        {
            foreach (var subscriptionHandler in _subscriptionHandlers)
            {
                subscriptionHandler.Dispose();
            }
        }
    }
}