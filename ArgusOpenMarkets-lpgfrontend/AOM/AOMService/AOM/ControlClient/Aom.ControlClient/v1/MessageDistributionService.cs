﻿using AOM.ControlClient.Interfaces;
using Utils.Extension;

namespace AOM.ControlClient.v1
{
    using System.Collections.Generic;
    using System;
    using System.Configuration;

    using AOM.ControlClient.v1.Interfaces;
    using AOM.Diffusion.Common.Interfaces;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Users;

    using Argus.Transport.Infrastructure;

    using Newtonsoft.Json;

    using Utils.Logging.Utils;

    using PushTechnology.ClientInterface.Client.Content;
    using PushTechnology.ClientInterface.Client.Features;
    using PushTechnology.ClientInterface.Client.Session;
    using PushTechnology.ClientInterface.Client.Types;

    public class MessageDistributionService : IMessageDistributionService
    {
        private readonly IBus _aomBus;

        private readonly IDiffusionFacade _topicManager;

        private readonly IOrderRouterService _orderRouterService;

        private readonly IMarketTickerRouterService _marketTickerRouterService;

        private readonly IExternalDealRouterService _externalDealRouterService;

        private readonly IInternalDealRouterService _internalDealRouterService;

        private readonly IMarketInfoRouterService _marketInfoRouterService;

        private readonly IAssessmentRouterService _assessmentRouterService;

        private readonly INewsRouterService _newsRouterService;

        private readonly IProductsRouterService _productRouterService;

        private readonly IUserDistributionService _userDistributionService;

        private readonly IHealthCheckService _healthCheckService;

        private readonly List<IRunnable> _serviceHandlers = new List<IRunnable>();

        public event Action<object, string> MessageHandlerActive;

        public event Action<object, string> MessageHandlerClosed;

        private readonly bool _singleSessionPerUser;

        private bool disposed;
        private IOrganisationRouterService _organisationRouterService;



        public MessageDistributionService(
            IBus aomBus,
            IDiffusionFacade topicManager,
            IUserDistributionService userDistributionService,
            IExternalDealRouterService externalDealRouterService,
            IInternalDealRouterService internalDealRouterService,
            IOrderRouterService orderRouterService,
            IMarketTickerRouterService marketTickerRouterService,
            IMarketInfoRouterService marketInfoRouterService,
            IAssessmentRouterService assessmentRouterService,
            INewsRouterService newsRouterService,
            IProductsRouterService productRouterService,
            IHealthCheckService healthCheckService,
            IOrganisationRouterService organisationRouterService)
        {
            _aomBus = aomBus;
            _topicManager = topicManager;

            _userDistributionService = userDistributionService;
            _externalDealRouterService = externalDealRouterService;
            _internalDealRouterService = internalDealRouterService;
            _orderRouterService = orderRouterService;
            _marketTickerRouterService = marketTickerRouterService;
            _marketInfoRouterService = marketInfoRouterService;
            _assessmentRouterService = assessmentRouterService;
            _newsRouterService = newsRouterService;
            _productRouterService = productRouterService;
            _healthCheckService = healthCheckService;
            _organisationRouterService = organisationRouterService;
            _serviceHandlers.AddRange(
                new List<IRunnable>
                {
                    _userDistributionService,
                    _externalDealRouterService,
                    _internalDealRouterService,
                    _orderRouterService,
                    _marketTickerRouterService,
                    _marketInfoRouterService,
                    _assessmentRouterService,
                    _newsRouterService,
                    _productRouterService,
                    _healthCheckService,
                    _organisationRouterService
                });



            _singleSessionPerUser = bool.Parse(ConfigurationManager.AppSettings["SingleSessionPerUser"]);
        }

        public void Run()
        {
            try
            {

                Log.Info("MessageDistributionService.Run");
                foreach (var serviceHandler in _serviceHandlers)
                {
                    serviceHandler.Run();
                }
                Log.Info("MessageDistributionService.Run - completed");

            }
            catch (Exception ex)
            {
                Log.Error("MessageDistributionService.Run", ex);
            }
        }

        public void OnMessage(SessionId sessionId, string topicPath, IContent content, IReceiveContext context)
        {
            try
            {
                Log.Info("MessageDistributionService.OnMessage");

                var parsedMessage = JsonConvert.DeserializeObject<Message>(content.AsString());
                parsedMessage.ClientSessionInfo = _topicManager.GetUserSession(sessionId.ToString());
                if (parsedMessage.ClientSessionInfo == null)
                {
                    Log.Error("Unknown message originator session: " + sessionId + " " + parsedMessage.ToJsonCamelCase()); 
                }
                RouteMessage(parsedMessage);
            }
            catch (Exception ex)
            {
                Log.Error("MessageDistrubtionService.OnMessage", ex);
                SendRoutingFailedMessage(LogException.Log(ex, "routing"), null);
            }
        }

        private void RouteMessage(Message parsedMessage)
        {
            try
            {

                Log.InfoFormat("MessageDistributionService.RouteMessage: {0}", parsedMessage.MessageType);

                switch (parsedMessage.MessageType)
                {
                    case MessageType.Order:
                        _orderRouterService.RouteMessage(parsedMessage);
                        break;
                    case MessageType.Deal:
                        _internalDealRouterService.RouteMessage(parsedMessage);
                        break;
                    case MessageType.ExternalDeal:
                        _externalDealRouterService.RouteMessage(parsedMessage);
                        break;
                    case MessageType.MarketTickerItem:
                        _marketTickerRouterService.RouteMessage(parsedMessage);
                        break;
                    case MessageType.User:
                        Log.Debug(
                            "Request to register a new user with userid: " + parsedMessage.ClientSessionInfo.UserId);
                        Log.Debug(string.Format("Single session per UserId: {0}", _singleSessionPerUser));
                        if (_singleSessionPerUser && parsedMessage.MessageAction == MessageAction.Register)
                        {
                            Log.Debug(
                                string.Format(
                                    "Terminating other user sessions for UserId: {0}",
                                    parsedMessage.ClientSessionInfo.UserId));
                            var terminateOtherUserSessions = new TerminateOtherUserSessionsRequest
                                                             {
                                                                 UserId =
                                                                     parsedMessage
                                                                     .ClientSessionInfo
                                                                     .UserId,
                                                                 SessionId =
                                                                     parsedMessage
                                                                     .ClientSessionInfo
                                                                     .SessionId,
                                                             };
                            _aomBus.Publish(terminateOtherUserSessions);
                        }
                        break;
                    case MessageType.Info:
                        _marketInfoRouterService.RouteMessage(parsedMessage);
                        break;
                    case MessageType.Assessment:
                        _assessmentRouterService.RouteMessage(parsedMessage);
                        break;
                    case MessageType.CounterpartyPermission:
                    case MessageType.BrokerPermission:
                    case MessageType.OrganisationNotificationDetail:
                        _organisationRouterService.RouteMessage(parsedMessage);
                        break;
                    default:
                        SendRoutingFailedMessage("Problem routing message", parsedMessage);
                        break;
                }

            }
            catch (Exception ex)
            {
                Log.Error("MessageDistributionService.RouteMessage", ex);
            }
        }

        private void SendRoutingFailedMessage(string error, Message message)
        {
            try
            {
                Log.Error(string.Format("{0}: {1}",error,message));
            }
            catch (Exception e)
            {
                LogException.Log(e);
            }
        }

        public void OnActive(string topicPath, IRegisteredHandler registeredHandler)
        {
            Log.Debug("MessageDistributionService Activated on topic path " + topicPath);
            if (MessageHandlerActive != null)
            {
                MessageHandlerActive(this, topicPath);
            }
        }

        public void OnClose(string topicPath)
        {
            Log.Debug("MessageDistributionService Closed on topic path " + topicPath);
            if (MessageHandlerClosed != null)
            {
                MessageHandlerClosed(this, topicPath);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var serviceHandler in _serviceHandlers)
                    {
                        serviceHandler.Dispose();
                    }
                    _serviceHandlers.Clear();
                }
            }
            disposed = true;
        }
    }
}