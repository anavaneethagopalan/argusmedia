﻿namespace AOM.ControlClient.v1.Interfaces
{
    public interface IAuthenticationManager
    {
        void Start();

        void Stop();
    }
}