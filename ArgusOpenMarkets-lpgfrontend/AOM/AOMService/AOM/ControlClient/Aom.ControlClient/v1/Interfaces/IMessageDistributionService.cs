﻿using AOM.ControlClient.Interfaces;

namespace AOM.ControlClient.v1.Interfaces
{
    using System;

    using PushTechnology.ClientInterface.Client.Features.Control.Topics;

    public interface IMessageDistributionService : IMessageHandler, IRunnable
    {
        event Action<object, string> MessageHandlerActive;

        event Action<object, string> MessageHandlerClosed;
    }
}