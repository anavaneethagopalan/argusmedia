﻿using AOM.Diffusion.Common;
using AOM.Diffusion.Common.Interfaces;
using AOM.Transport.Events;
using Ninject;
using Ninject.Modules;
using Ninject.Extensions.Conventions;

namespace AOM.ControlClient.v1
{
    using AOM.ControlClient.Interfaces;

    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind(x => x.FromThisAssembly().IncludingNonePublicTypes().SelectAllClasses()
                .NotInNamespaces("AOM.ControlClient.v2", "AOM.Diffusion.Common.v2")                
                .BindAllInterfaces());

            kernel.Bind<IDiffusionConfig>().To<DiffusionConfig>();
            kernel.Bind<IDiffusionSessionConnection>().To<DiffusionSessionConnection>();
            kernel.Bind<IClientSessionInfo>().To<ClientSessionInfo>();
            kernel.Bind<IDiffusionSessionManager>().To<DiffusionSessionManager>();
            kernel.Rebind<IDiffusionFacade>().To<DiffusionFacade>().InSingletonScope();
            kernel.Rebind<IClientSessionMessageHandler>().To<ClientSessionMessageHandler>().InSingletonScope();
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}