﻿namespace AOM.ControlClient.v1
{
    using System;
    using System.Diagnostics;
    using System.Linq;

    using AOM.Repository.MySql;
    using AOM.Services;
    using AOM.Transport.Service.Processor.Common;

    using Ninject;

    using Utils.Logging.Utils;

    internal class Program
    {
        private static void Main()
        {
            Log.Info("Starting AOM.ControlClient");

            using (var kernel = new StandardKernel())
            {
                kernel.Load(
                    new EFModelBootstrapper(),
                    new ServiceBootstrap(),
                    new LocalBootstrapper(),
                    new ProcessorCommonBootstrap());

                WarmUp(kernel);
                ServiceHelper.BuildAndRunService<ControlClientController>(kernel);
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (var db = dbFactory.CreateCrmModel())
                {
                    db.UserInfoes.Any();
                }
                Log.Info(string.Format("Initial EF model build took {0} msecs.", timer.ElapsedMilliseconds));

            }
            catch (Exception ex)
            {
                Log.Error("AOMControlClient - WarmUp - Error - cannot connect to DB", ex);
                ServiceHelper.BuildAndRunService<ControlClientController>(kernel);
            }
        }
    }
}