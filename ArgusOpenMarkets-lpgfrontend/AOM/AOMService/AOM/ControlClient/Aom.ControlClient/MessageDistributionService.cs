﻿using System;
using System.Collections.Generic;

using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using AOM.Transport.Events.Deals;
using AOM.Transport.Events.ExternalDeals;
using AOM.Transport.Events.MarketInfos;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Events.Users;
using AOM.Transport.Events.System;
using AOM.Transport.Events.Orders;
using Utils.Logging.Utils;

using Argus.Transport.Infrastructure;
using Newtonsoft.Json;
using PushTechnology.ClientInterface.Client.Content;
using PushTechnology.ClientInterface.Client.Features;
using PushTechnology.ClientInterface.Client.Session;
using PushTechnology.ClientInterface.Client.Types;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient
{
    public class MessageDistributionService : IMessageDistributionService
    {
        private readonly IBus _aomBus;
        private readonly IDiffusionSessionManager _diffusionSessionManager;
        private readonly IOrderRouterService _orderRouterService;
        private readonly IMarketTickerRouterService _marketTickerRouterService;
        private readonly IExternalDealRouterService _externalDealRouterService;
        private readonly IInternalDealRouterService _internalDealRouterService;
        private readonly IMarketInfoRouterService _marketInfoRouterService;
        private readonly IAssessmentRouterService _assessmentRouterService;
        private readonly INewsRouterService _newsRouterService;
        private readonly IProductsRouterService _productRouterService;
        private readonly IUserDistributionService _userDistributionService;
        private readonly IHealthCheckService _healthCheckService;
        private readonly IOrganisationRouterService _organisationRouterService;
        private readonly Interfaces.IAuthenticationManager _authenticationManager;
        private readonly IDiffusionProductTopicTreeBuilder _diffusionProductTopicTreeBuilder;
        private readonly IAdminRouterService _adminRouterService;
        private readonly List<IRunnable> _serviceHandlers = new List<IRunnable>();

        public event Action<object, string> MessageHandlerActive;
        public event Action<object, string> MessageHandlerClosed;

        //private readonly bool _singleSessionPerUser;
        private bool disposed;

        public MessageDistributionService(
            IBus aomBus,
            IDiffusionSessionManager diffusionSessionManagerv2,
            IUserDistributionService userDistributionService,
            IExternalDealRouterService externalDealRouterService,
            IInternalDealRouterService internalDealRouterService,
            IOrderRouterService orderRouterService,
            IMarketTickerRouterService marketTickerRouterService,
            IMarketInfoRouterService marketInfoRouterService,
            IAssessmentRouterService assessmentRouterService,
            INewsRouterService newsRouterService,
            IProductsRouterService productRouterService,
            IHealthCheckService healthCheckService,
            IOrganisationRouterService organisationRouterService,
            IAuthenticationManager authenticationManager,
            IDiffusionProductTopicTreeBuilder diffusionProductTopicTreeBuilder,
            IAdminRouterService adminRouterService)
        {
            _aomBus = aomBus;
            _diffusionSessionManager = diffusionSessionManagerv2;

            _userDistributionService = userDistributionService;
            _externalDealRouterService = externalDealRouterService;
            _internalDealRouterService = internalDealRouterService;
            _orderRouterService = orderRouterService;
            _marketTickerRouterService = marketTickerRouterService;
            _marketInfoRouterService = marketInfoRouterService;
            _assessmentRouterService = assessmentRouterService;
            _newsRouterService = newsRouterService;
            _productRouterService = productRouterService;
            _healthCheckService = healthCheckService;
            _organisationRouterService = organisationRouterService;
            _authenticationManager = authenticationManager;
            _diffusionProductTopicTreeBuilder = diffusionProductTopicTreeBuilder;
            _adminRouterService = adminRouterService;

            _serviceHandlers.AddRange(
                new List<IRunnable>
                {
                    _userDistributionService,
                    _externalDealRouterService,
                    _internalDealRouterService,
                    _orderRouterService,
                    _marketTickerRouterService,
                    _marketInfoRouterService,
                    _assessmentRouterService,
                    _newsRouterService,
                    _productRouterService,
                    _healthCheckService,
                    _organisationRouterService,
                    _authenticationManager,
                    _diffusionProductTopicTreeBuilder,
                    _adminRouterService
                });

            //_singleSessionPerUser = bool.Parse(ConfigurationManager.AppSettings["SingleSessionPerUser"]);
        }

        public void AttachToSession(ISession session)
        {
            session.GetMessagingControlFeature().AddMessageHandler("AOM/Users", this);
        }

        public void Run()
        {
            try
            {
                Log.Info("MessageDistributionService.Run");
                foreach (var serviceHandler in _serviceHandlers)
                {
                    Log.DebugFormat("Service Handler - {0} Calling Run", serviceHandler.ToString());
                    serviceHandler.Run();
                }
                Log.Info("MessageDistributionService.Run - completed");

            }
            catch (Exception ex)
            {
                Log.Error("MessageDistributionService.Run", ex);
            }
        }

        public void OnActive(string topicPath, IRegisteredHandler registeredHandler)
        {
            Log.Debug("MessageDistributionService Activated on topic path " + topicPath);
            if (MessageHandlerActive != null)
            {
                MessageHandlerActive(this, topicPath);
            }
        }

        public void OnClose(string topicPath)
        {
            Log.Debug("MessageDistributionService Closed on topic path " + topicPath);
            if (MessageHandlerClosed != null)
            {
                MessageHandlerClosed(this, topicPath);
            }

            foreach (var serviceHandler in _serviceHandlers)
            {
                serviceHandler.Disconnect();
            }
        }


        public void OnMessage(SessionId sessionId, string topicPath, IContent content, IReceiveContext context)
        {
            try
            {
                //StackTrace st = new StackTrace();
                Log.Info("MessageDistributionService.OnMessage called: " + topicPath);   //" from: " + st.ToString());

                var parsedMessage = JsonConvert.DeserializeObject<Message>(content.AsString());

                parsedMessage.ClientSessionInfo = _diffusionSessionManager.GetUserSession(sessionId.ToString());
                RouteMessage(parsedMessage);
            }
            catch (Exception ex)
            {
                Log.Error("MessageDistrubtionService.OnMessage", ex);

                var logError = _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = ex,
                    AdditionalInformation = "routing",
                    ErrorText = string.Empty,
                    Source = "MessageDistributionService"
                });

                SendRoutingFailedMessage(logError.UserErrorMessage, null);
            }
        }

        private void RouteMessage(Message parsedMessage)
        {
            try
            {
                Log.InfoFormat("MessageDistributionService.RouteMessage: {0}", parsedMessage.MessageType);

                switch (parsedMessage.MessageType)
                {
                    case MessageType.ClientLog:
                        if (string.IsNullOrEmpty(parsedMessage.MessageBody.ToString()))
                        {
                            Log.Info("ClientLog message had invalid construct for deserialization: " + parsedMessage.MessageBody);
                            break;
                        }
                        var clientLog = JsonConvert.DeserializeObject<ClientLogging>(parsedMessage.MessageBody.ToString());
                    
                        var clientLogEvent = new ClientLogEvent()
                        {
                            ClientLog = clientLog
                        };
                        _aomBus.Publish(clientLogEvent);

                    break;

                    case MessageType.Order:
                        // var order = JsonConvert.DeserializeObject<Order>(parsedMessage.MessageBody.ToString());
                        var orderMessageResponse = new AomOrderMessageResponse
                        {
                            Message = parsedMessage,
                            ObjectJson = parsedMessage.MessageBody.ToString()
                        };
                        _aomBus.Publish(orderMessageResponse);
                        break;

                    case MessageType.Deal:
                        var deal = JsonConvert.DeserializeObject<Deal>(parsedMessage.MessageBody.ToString());
                        var internalDealMessageResponse = new AomInternalDealMessageResponse
                        {
                            Message = parsedMessage,
                            Deal = deal,
                            ObjectJson = parsedMessage.MessageBody.ToString()
                        };
                        _aomBus.Publish(internalDealMessageResponse);
                        break;

                    case MessageType.ExternalDeal:
                        var externalDeal = JsonConvert.DeserializeObject<ExternalDeal>(parsedMessage.MessageBody.ToString());

                        var externalDealMessageResponse = new AomExternalDealMessageResponse
                        {
                            Message = parsedMessage,
                            ExternalDeal = externalDeal,
                            ObjectJson = parsedMessage.MessageBody.ToString()
                        };
                        _aomBus.Publish(externalDealMessageResponse);
                        break;

                    case MessageType.MarketTickerItem:
                        var marketTickerItemMessageResponse = new AomMarketTickerItemMessageResponse
                        {
                            Message = parsedMessage
                        };
                        _aomBus.Publish(marketTickerItemMessageResponse);
                        break;

                    case MessageType.User:
                        Log.Debug("Request to register a user with userid: " + parsedMessage.ClientSessionInfo.UserId);
                        //Log.Debug(string.Format("Single session per UserId: {0}", _singleSessionPerUser));

                        //if (_singleSessionPerUser && parsedMessage.MessageAction == MessageAction.Register)
                        if (parsedMessage.MessageAction == MessageAction.Register)
                        {
                            Log.Debug(string.Format("Terminating other user sessions for UserId: {0}", parsedMessage.ClientSessionInfo.UserId));
                            var terminateOtherUserSessions = new TerminateOtherUserSessionsRequest
                            {
                                UserId = parsedMessage.ClientSessionInfo.UserId,
                                SessionId = parsedMessage.ClientSessionInfo.SessionId,
                            };
                            _aomBus.Publish(terminateOtherUserSessions);
                        }
                        break;

                    case MessageType.Info:
                        var marketInfo = JsonConvert.DeserializeObject<MarketInfo>(parsedMessage.MessageBody.ToString());
                        var marketInfoMessageResponse = new AomMarketInfoMessageResponse
                        {
                            Message = parsedMessage,
                            MarketInfo = marketInfo,
                            ObjectJson = parsedMessage.MessageBody.ToString()
                        };
                        _aomBus.Publish(marketInfoMessageResponse);
                        break;

                    case MessageType.Assessment:

                        CombinedAssessment combinedAssessment = null;
                        AomRefreshAssessmentRequest aomRefreshAssessmentRequest = null;
                        AomClearAssessmentRequest aomClearAssessmentRequest = null;

                        string objectJson = string.Empty;
                        switch (parsedMessage.MessageAction)
                        {
                            case MessageAction.Create:
                                objectJson = parsedMessage.MessageBody.ToString();
                                combinedAssessment = JsonConvert.DeserializeObject<CombinedAssessment>(parsedMessage.MessageBody.ToString());
                                break;
                            case MessageAction.Update:
                                objectJson = parsedMessage.MessageBody.ToString();
                                combinedAssessment = JsonConvert.DeserializeObject<CombinedAssessment>(parsedMessage.MessageBody.ToString());
                                break;
                            case MessageAction.Refresh:
                                objectJson = parsedMessage.MessageBody.ToString();
                                aomRefreshAssessmentRequest = JsonConvert.DeserializeObject<AomRefreshAssessmentRequest>(parsedMessage.MessageBody.ToString());
                                break;
                            case MessageAction.Clear:
                                objectJson = parsedMessage.MessageBody.ToString();
                                aomClearAssessmentRequest = JsonConvert.DeserializeObject<AomClearAssessmentRequest>(parsedMessage.MessageBody.ToString());
                                break;
                        }

                        var assessmentMessageResponse = new AomAssessmentMessageResponse
                        {
                            Message = parsedMessage,
                            CombinedAssessment = combinedAssessment,
                            AomRefreshAssessmentRequest = aomRefreshAssessmentRequest,
                            AomClearAssessmentRequest = aomClearAssessmentRequest,
                            ObjectJson = objectJson
                        };

                        if (assessmentMessageResponse.AomClearAssessmentRequest != null)
                        {
                            if (assessmentMessageResponse.Message != null)
                            {
                                assessmentMessageResponse.AomClearAssessmentRequest.ClientSessionInfo = assessmentMessageResponse.Message.ClientSessionInfo;
                            }
                        }
                        _aomBus.Publish(assessmentMessageResponse);
                        break;

                    case MessageType.CounterpartyPermission:
                    case MessageType.BrokerPermission:
                    case MessageType.OrganisationNotificationDetail:

                        var organisationMessageResponse = new AomOrganisationMessageResponse
                        {
                            Message = parsedMessage,
                            ObjectJson = parsedMessage.MessageBody.ToString()
                        };
                        _aomBus.Publish(organisationMessageResponse);
                        // _organisationRouterService.RouteMessage(parsedMessage);
                        break;

                    default:
                        SendRoutingFailedMessage("Problem routing message", parsedMessage);
                        break;
                }

            }
            catch (Exception ex)
            {
                Log.Error("MessageDistributionService.RouteMessage", ex);
            }
        }

        private void SendRoutingFailedMessage(string error, Message message)
        {
            try
            {
                Log.Error(string.Format("SendRouting Failed Message -{0}: {1}", error, message));
            }
            catch (Exception e)
            {
                _aomBus.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(new LogUserErrorRequestRpc
                {
                    Error = e,
                    AdditionalInformation = string.Empty,
                    ErrorText = string.Empty,
                    Source = "MessageDistributionService"
                });
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (var serviceHandler in _serviceHandlers)
                    {
                        serviceHandler.Dispose();
                    }
                    _serviceHandlers.Clear();
                }
            }
            disposed = true;
        }

        public void Disconnect()
        {

        }
    }
}