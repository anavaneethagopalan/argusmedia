﻿using AOM.TopicManager;
using AOM.Transport.Events.Users;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using AOM.ControlClient.Interfaces;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using PushTechnology.ClientInterface.Client.Session;
using PushTechnology.DiffusionCore.Exceptions;
using System;
using System.Threading;
using System.Threading.Tasks;
using AOMDiffusion.Common.Interfaces;
using PushTechnology.ClientInterface.Client.Topics;
using Topshelf;
using Utils.Logging.Utils;

namespace AOM.ControlClient
{
    public class ControlClientController : IServiceBase
    {
        private readonly IBus _aomBus;
        private string _environmentMachineName { get; set; }
        private readonly IMessageDistributionService _messageDistributionService;
        private readonly IDiffusionSessionConnection _diffusionSessionConnection;
        private readonly IClientSessionMessageHandler _clientSessionMessageHandler;
        private readonly IDiffusionSessionManager _diffusionSessionManager;
        private readonly IAuthenticationDiffusionTopicManager _authenticationDiffusionTopicManager;
        private readonly IProductsDiffusionTopicManager _productsDiffusionTopicManager;
        private readonly IOrganistionDiffusionTopicManager _organsOrganistionDiffusionTopicManager;
        private readonly IHealthCheckDiffusionTopicManager _healthCheckTickerDiffusionTopicManager;
        private readonly IMarketTickerDiffusionTopicManager _marketTickerDiffusionTopicManager;
        private readonly INewsDiffusionTopicManager _newsDiffusionTopicManager;
        private readonly IAdminDiffusionTopicManager _adminDiffusionTopicManager;
        private readonly IOrdersDiffusionTopicManager _ordersDiffusionTopicManager;
        private readonly IAomTopicManager _aomTopicManager;
        private readonly CancellationTokenSource _taskCancellationSource;

        public ControlClientController(
            IBus aomBus,
            IMessageDistributionService messageDistributionServicev2,
            IDiffusionSessionConnection diffusionSessionConnection,
            IClientSessionMessageHandler clientSessionMessageHandler,
            IDiffusionSessionManager diffusionSessionManager,
            IAuthenticationDiffusionTopicManager authenticationDiffusionTopicManager,
            IProductsDiffusionTopicManager productsDiffusionTopicManager,
            IOrganistionDiffusionTopicManager organsOrganistionDiffusionTopicManager,
            IHealthCheckDiffusionTopicManager healthCheckTickerDiffusionTopicManager,
            IMarketTickerDiffusionTopicManager marketTickerDiffusionTopicManager,
            INewsDiffusionTopicManager newsDiffusionTopicManager,
            IAdminDiffusionTopicManager adminDiffusionTopicManager,
            IOrdersDiffusionTopicManager ordersDiffusionTopicManager,
            IAomTopicManager aomTopicManager)
        {
            _aomBus = aomBus;
            _messageDistributionService = messageDistributionServicev2;
            _diffusionSessionConnection = diffusionSessionConnection;
            _clientSessionMessageHandler = clientSessionMessageHandler;
            _diffusionSessionManager = diffusionSessionManager;

            _authenticationDiffusionTopicManager = authenticationDiffusionTopicManager;
            _productsDiffusionTopicManager = productsDiffusionTopicManager;
            _organsOrganistionDiffusionTopicManager = organsOrganistionDiffusionTopicManager;
            _healthCheckTickerDiffusionTopicManager = healthCheckTickerDiffusionTopicManager;
            _marketTickerDiffusionTopicManager = marketTickerDiffusionTopicManager;
            _newsDiffusionTopicManager = newsDiffusionTopicManager;
            _adminDiffusionTopicManager = adminDiffusionTopicManager;
            _ordersDiffusionTopicManager = ordersDiffusionTopicManager;
            _aomTopicManager = aomTopicManager;

            _taskCancellationSource = new CancellationTokenSource();
        }

        public bool Start(HostControl hostControl)
        {
            Log.Info("ControlClientController.Start v2");
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            _messageDistributionService.MessageHandlerActive += (o, s) =>
            {
                Log.Info("Message Distribution service active, running routers");
                _messageDistributionService.Run();
            };

            _messageDistributionService.MessageHandlerClosed += (o, s) =>
            {
                Log.Info("Control Client - MessageHandlerClosed");
            };

            try
            {
                // Task.Factory.StartNew(ConnectToDiffusion);
                ConnectToDiffusion();
            }
            catch (OperationCanceledException)
            {
                Log.Error("Initial connection to Diffusion in Start threw OperationCanceledException");
                return false;
            }

            _aomBus.Subscribe<TopicSubscribeResponse>(EnvironmentMachineName, OnTopicSubscribeResponse);
            Log.Info("CC: Subscribed to topic subscription...listening for topic subscription messages");
            return true;
        }

        public string EnvironmentMachineName
        {
            get
            {
                if (string.IsNullOrEmpty(_environmentMachineName))
                    _environmentMachineName = "TopicSubscription_" + Environment.MachineName;

                return _environmentMachineName;
            }
        }


        private void OnTopicSubscribeResponse(TopicSubscribeResponse topicSubscriptionResponse)
        {
            Log.InfoFormat("We are subscribing the user to topics: " );
            var csi = topicSubscriptionResponse.ClientSessionInfo;
            if (_diffusionSessionManager.GetSession(csi) != null)
            {
                // WE ARE SUBSCRIBING TO TOPIC(S)....
                if (topicSubscriptionResponse.TopicsSubscribe.Count > 0)
                {
                    Log.Info("OnTopicSubscribeResponse TopicSubscribe count: " + topicSubscriptionResponse.TopicsSubscribe.Count);
                    var clientSession = _diffusionSessionConnection.Session.GetClientControlFeature().SessionIdFromString(topicSubscriptionResponse.ClientSessionInfo.SessionId);

                    foreach (var topicSubscribe in topicSubscriptionResponse.TopicsSubscribe)
                    {
                        try
                        {
                            ITopicSelector topicSelector = ParseDiffusionTopic(topicSubscribe);

                            if (topicSelector != null)
                            {
                                // WE ARE SUBSCRIBING THE USER TO A TOPIC....
                                _diffusionSessionConnection.Session.GetSubscriptionControlFeature().Subscribe(clientSession, topicSubscribe, new SubscriptionCallbackDefault());
                                Log.Info(string.Format("CCC - Subscribed user: {0} - {1} to topic: {2}", csi.UserId, csi.SessionId,  topicSubscribe));
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error("Subscribe Client to topic. Error", ex);
                        }
                    }
                }

                //if (topicSubscriptionResponse.TopicsUnSubscribe.Count > 0)
                //{
                //    //Log.Info("OnTopicSubscribeResponse TopicUnSubscribe count: " + topicSubscriptionResponse.TopicsUnSubscribe.Count);
                //    var clientSession = _diffusionSessionConnection.Session.GetClientControlFeature().SessionIdFromString(topicSubscriptionResponse.ClientSessionInfo.SessionId);

                //    foreach (var topicSubscribe in topicSubscriptionResponse.TopicsUnSubscribe)
                //    {
                //        try
                //        {
                //            // WE ARE SUBSCRIBING THE USER TO A TOPIC....
                //            var topicSelector = ParseDiffusionTopic(topicSubscribe);
                //            if (topicSelector != null)
                //            {
                //                _diffusionSessionConnection.Session.GetSubscriptionControlFeature().Unsubscribe(clientSession, topicSubscribe, new SubscriptionCallbackDefault());
                //                Log.Info(string.Format("CCC - UNSubscribed user: {0} - {1} to topic: {2}", csi.UserId, csi.SessionId, topicSubscribe));
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            Log.Error("Unsubscribe user from topic.  Error:", ex);
                //        }
                //    }
                //}
            }
        }

        public ITopicSelector ParseDiffusionTopic(string topicSubscribe)
        {
            var topicSelectors = PushTechnology.ClientInterface.Client.Factories.Diffusion.TopicSelectors;
            ITopicSelector topicSelector = null;
            try
            {
                topicSelector = topicSelectors.Parse(topicSubscribe);
            }
            catch (Exception ex)
            {
                Log.Error("TopicSelectors.Parse threw an exception for the topic: " + topicSubscribe, ex);
            }

            return topicSelector;
        }

        private void Session_ErrorNotified(object sender, SessionErrorHandlerEventArgs e)
        {
            Log.Error("ControlClientController.Session.Error: " + e.Error.Message);
            throw new DiffusionException("Diffusion Session Error occured");
        }

        private void Session_StateChanged(object sender, SessionListenerEventArgs e)
        {
            Log.Info("ControlClientController.Session.StateChanged: " + e.NewState);

            if (e.NewState == SessionState.CLOSED_BY_SERVER)
            {
                Log.Error("---------------------------- CONNECTION CLOSED BY SERVER - attempting connection to failover and stopping service");
                Task.Factory.StartNew(ConnectToDiffusion, _taskCancellationSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
            }
        }

        private void ConnectToDiffusion()
        {
            // Loop until we are connected.
            Log.Info("CONNECT TO DIFFUSION - ControlClientController.ConnectToDiffusion");

            var isConnected = false;
            var retryCount = 0;

            try
            {
                while (!isConnected)
                {
                    _taskCancellationSource.Token.ThrowIfCancellationRequested();

                    if (_diffusionSessionConnection.ConnectAndStartSession())
                    {
                        Log.Info("SUCCESSFULLY CONNECTED TO DIFFUSION");

                        _diffusionSessionConnection.Session.StateChanged += Session_StateChanged;
                        _diffusionSessionConnection.Session.ErrorNotified += Session_ErrorNotified;
                        _diffusionSessionManager.AttachToSession(_diffusionSessionConnection.Session);
                        _clientSessionMessageHandler.AttachToSession(_diffusionSessionConnection.Session);
                        _messageDistributionService.AttachToSession(_diffusionSessionConnection.Session);
                        _authenticationDiffusionTopicManager.AttachToSessionAndRegisterTopicSource(_diffusionSessionConnection.Session);
                        _productsDiffusionTopicManager.AttachToSessionAndRegisterTopicSource(_diffusionSessionConnection.Session);
                        _organsOrganistionDiffusionTopicManager.AttachToSessionAndRegisterTopicSource(_diffusionSessionConnection.Session);
                        _healthCheckTickerDiffusionTopicManager.AttachToSessionAndRegisterTopicSource(_diffusionSessionConnection.Session);
                        _marketTickerDiffusionTopicManager.AttachToSessionAndRegisterTopicSource(_diffusionSessionConnection.Session);
                        _newsDiffusionTopicManager.AttachToSessionAndRegisterTopicSource(_diffusionSessionConnection.Session);
                        _adminDiffusionTopicManager.AttachToSessionAndRegisterTopicSource(_diffusionSessionConnection.Session);
                        _ordersDiffusionTopicManager.AttachToSessionAndRegisterTopicSource(_diffusionSessionConnection.Session);
                        isConnected = true;
                    }
                    else
                    {
                        Log.Warn(string.Format("RECONNECT TO DIFFUSION FAILED - Retry count:{0}", retryCount));
                        retryCount++;
                        Thread.Sleep(500);
                    }
                }
            }
            catch (OperationCanceledException)
            {
                Log.Info("ControlClientController-ConnectToDiffusion-OperationCanceledException");
                throw; // This exception transitions the calling Task to the Canceled state
            }
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Error("Unhandled exception within control client", e.ExceptionObject as Exception);
        }

        public bool Stop(HostControl hostControl)
        {
            Log.Info("Stopping ControlClientController");

            _taskCancellationSource.Cancel();

            var t = Task.Run(() => _aomBus.Dispose());
            if (!t.Wait(TimeSpan.FromSeconds(10)))
            {
                Log.Error("ControlClientController timed out when disposing AOM Bus");
            }
            Log.Info("ControlClientController stopped");

            return true;
        }

    }
}