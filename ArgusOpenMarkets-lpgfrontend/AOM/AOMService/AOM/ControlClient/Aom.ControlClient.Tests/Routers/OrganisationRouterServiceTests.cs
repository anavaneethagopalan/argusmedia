﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.ControlClient.Routers;
using AOM.Transport.Events;
using AOM.Transport.Events.Organisations;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using Utils.Javascript.Tests.Extension;

namespace AOM.ControlClient.Tests.Routers
{
    [TestFixture]
    public class OrganisationRouterServiceTests
    {
        private Mock<IBus> _mockAomBus;
        private Mock<IOrganistionDiffusionTopicManager> _mockOrganisationDiffusionTopicManager;
        private readonly Mock<IClientSessionMessageHandler> _mockClientSessionHandler = new Mock<IClientSessionMessageHandler>();
        private OrganisationRouterService _organisationRouterService;

        [SetUp]
        public void Setup()
        {
            _mockAomBus = new Mock<IBus>();
            _mockOrganisationDiffusionTopicManager = new Mock<IOrganistionDiffusionTopicManager>();
            _organisationRouterService = new OrganisationRouterService(_mockAomBus.Object, _mockOrganisationDiffusionTopicManager.Object, _mockClientSessionHandler.Object);
        }

        [Test]
        public void SubscribesToPermissionMatrix()
        {
            _mockOrganisationDiffusionTopicManager.GoActive();
            _mockAomBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<GetCounterpartyPermissionsRefreshResponse>>()), Times.Exactly(1));
            _mockAomBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<GetBrokerPermissionsRefreshResponse>>()), Times.Exactly(1));
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_organisationRouterService);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockOrganisationDiffusionTopicManager, _organisationRouterService);
        }

        [Test]
        public void ShouldDisposeSubscriptionsWhenTopicManagerGoesFromActiveToStandby()
        {
            TestHelpers.ShouldClearSubscribersWhenTopicManagerGoesActiveToStandby(_mockOrganisationDiffusionTopicManager, _organisationRouterService);
        }

        [Test]
        public void PushFullCompanyDetailsArePushedToDiffusion()
        {
            var response = new GetCompanyDetailsRefreshResponse
            {
                Details = new List<OrganisationDetails>
                {
                    new OrganisationDetails
                    {
                        Organisation = new Organisation { Id = 1 },
                        SystemNotifications = new List<NotificationMapping> 
                        {
                            new NotificationMapping 
                            {
                                EmailAddress = "travis@goldhammer.com",
                                EventType = SystemEventType.CounterPartyPermissionChanges,
                                Id = 1,
                                Product = new Product { Name = "Test" }
                            }
                        }
                    }
                }
            };

            _mockAomBus.SpyOnSubscribe<GetCompanyDetailsRefreshResponse>();
            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }
            _mockOrganisationDiffusionTopicManager.Verify(x => x.SendDataToTopic("AOM/Organisations/1/SystemNotifications/1/1", It.IsAny<string>(), null), Times.Once);
        }

        [Test]
        public void RouteOrganisationNotificationDetailShouldPublishAGetCompanyDetailsRefreshRequestOntoTheBusIfWeAreCreatingAnOrganisation()
        {
            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());

            var companyDetails = new CompanyDetailsRpcRequest
            {
                ClientSessionInfo = new ClientSessionInfo(),
                Id = 1,
                ProductId = 1,
                EmailAddress = "help@aom.com",
                EventType = SystemEventType.DealConfirmation
            };

            var objectJson = string.Empty;

            objectJson = JsonConvert.SerializeObject(companyDetails);

            var aomOrganisationMessageResponse = new AomOrganisationMessageResponse
            {
                Message = new Message {MessageType = MessageType.OrganisationNotificationDetail},
                ObjectJson = objectJson
            };

            _mockAomBus.Setup(m => m.Request<CompanyDetailsRpcRequest, CompanyDetailsRpcProcessedResponse>(It.IsAny<CompanyDetailsRpcRequest>()))
                .Returns(new CompanyDetailsRpcProcessedResponse
                {
                    Message = new Message {MessageAction = MessageAction.Create}
                });

            var subs = StartConsumer(_mockOrganisationDiffusionTopicManager);
            var onAomOrganisationMessageResponse = subs.AomOrganisationMessageResponse;
            onAomOrganisationMessageResponse(aomOrganisationMessageResponse);

            _mockAomBus.Verify(m => m.Publish(It.IsAny<GetCompanyDetailsRefreshRequest>()), Times.AtLeast(1));
        }

        [Test]
        public void AllowCounterpartyPermissionsArePushedToDiffusion()
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = DateTime.Now.AddDays(-1);

            var response = GetCounterpartyPermissionsRefreshResponse(now, yesterday, Permission.Allow, Permission.Allow, Permission.Allow, Permission.Allow);
            _mockAomBus.SpyOnSubscribe<GetCounterpartyPermissionsRefreshResponse>();

            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }

            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Principals/Buy/2", It.IsAny<string>(), null), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Principals/Sell/2", It.IsAny<string>(), null), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Principals/Buy/1", It.IsAny<string>(), null), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Principals/Sell/1", It.IsAny<string>(), null), Times.Exactly(1));
        }

        [Test]
        [TestCase(Permission.Deny, Permission.Deny, Permission.Deny, Permission.Deny, 0, 0,0,0)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, Permission.Allow, 1, 1, 1, 1)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Deny, Permission.Deny, 1, 1, 0, 0)]
        [TestCase(Permission.Deny, Permission.Deny, Permission.Allow, Permission.Allow, 0, 0, 1, 1)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.Allow, Permission.Allow, 0, 0, 1, 1)]
        [TestCase(Permission.Deny, Permission.NotSet, Permission.Allow, Permission.Allow, 0, 0, 1, 1)]
        [TestCase(Permission.NotSet, Permission.Deny, Permission.Allow, Permission.Allow, 0, 0, 1, 1)]
        [TestCase(Permission.Allow, Permission.Deny, Permission.Allow, Permission.Allow, 0, 0, 1, 1)]
        [TestCase(Permission.Deny, Permission.Allow, Permission.Allow, Permission.Allow, 0, 0, 1, 1)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.NotSet, Permission.NotSet, 1, 1, 0, 0)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Deny, Permission.NotSet, 1, 1, 0, 0)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.NotSet, Permission.Deny, 1, 1, 0, 0)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, Permission.Deny, 1, 1, 0, 0)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Deny, Permission.Allow, 1, 1, 0, 0)]
        [TestCase(Permission.Allow, Permission.Deny, Permission.Deny, Permission.Allow, 0, 0, 0, 0)]
        [TestCase(Permission.Deny, Permission.Allow, Permission.Allow, Permission.Deny, 0, 0, 0, 0)]
        public void AllowCounterpartyPermissionsForBuyArePushedButSellIsNotSentIfPermissionDeny(Permission buyOurPermission, Permission sellTheirPermission,
            Permission sellOurPermission, Permission buyTheirPermission, 
            int ourTopicBuyCount, int theirTopicBuyCount, int ourTopicSellCount, int theirTopicSellCount)
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = DateTime.Now.AddDays(-1);

            var response = GetCounterpartyPermissionsRefreshResponse(now, yesterday, buyOurPermission, sellTheirPermission, sellOurPermission, buyTheirPermission);
            _mockAomBus.SpyOnSubscribe<GetCounterpartyPermissionsRefreshResponse>();

            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }

            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Principals/Buy/2", It.IsAny<string>(), null), Times.Exactly(ourTopicBuyCount));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Principals/Sell/2", It.IsAny<string>(), null), Times.Exactly(ourTopicSellCount));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Principals/Sell/1", It.IsAny<string>(), null), Times.Exactly(theirTopicBuyCount));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Principals/Buy/1", It.IsAny<string>(), null), Times.Exactly(theirTopicSellCount));
        }

        [Test]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, Permission.Allow, 0)]
        [TestCase(Permission.Allow, Permission.Deny, Permission.Allow, Permission.Allow, 2)]
        [TestCase(Permission.Deny, Permission.Allow, Permission.Allow, Permission.Allow, 2)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, Permission.Deny, 2)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Deny, Permission.Allow, 2)]
        [TestCase(Permission.Deny, Permission.Deny, Permission.Deny, Permission.Deny, 4)]
        [TestCase(Permission.NotSet, Permission.Allow, Permission.Allow, Permission.Allow, 2)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.NotSet, Permission.Allow, 2)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.Deny, Permission.Deny, 4)]
        [TestCase(Permission.Deny, Permission.Deny, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.Deny, Permission.NotSet, Permission.Deny, 4)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.Deny, 4)]
        [TestCase(Permission.NotSet, Permission.Deny, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.Allow, Permission.Deny, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.Allow, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.Allow, Permission.NotSet, Permission.Allow, 4)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.Deny, Permission.Deny, Permission.Deny, Permission.Deny, 4)]
        public void BrokerPermissionsForDeleteTopic(Permission buyOurPermission,
            Permission sellTheirPermission,
            Permission sellOurPermission,
            Permission buyTheirPermission,
            int deleteTopicCount)
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = DateTime.Now.AddDays(-1);

            var response = GetBrokerPermissionsRefreshResponse(now, yesterday, buyOurPermission, sellTheirPermission, sellOurPermission, buyTheirPermission);
            _mockAomBus.SpyOnSubscribe<GetBrokerPermissionsRefreshResponse>();

            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }

            _mockOrganisationDiffusionTopicManager.Verify(m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(deleteTopicCount));
        }

        [Test]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, Permission.Allow, 0)]
        [TestCase(Permission.Allow, Permission.Deny, Permission.Allow, Permission.Allow, 2)]
        [TestCase(Permission.Deny, Permission.Allow, Permission.Allow, Permission.Allow, 2)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, Permission.Deny, 2)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Deny, Permission.Allow, 2)]
        [TestCase(Permission.Deny, Permission.Deny, Permission.Deny, Permission.Deny, 4)]
        [TestCase(Permission.NotSet, Permission.Allow, Permission.Allow, Permission.Allow, 2)]
        [TestCase(Permission.Allow, Permission.NotSet, Permission.Allow, Permission.Allow, 2)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.NotSet, Permission.Allow, 2)]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, Permission.NotSet, 2)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.Deny, Permission.Deny, 4)]
        [TestCase(Permission.Deny, Permission.Deny, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.Deny, Permission.NotSet, Permission.Deny, 4)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.Deny, 4)]
        [TestCase(Permission.NotSet, Permission.Deny, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.Allow, Permission.Deny, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.Allow, Permission.NotSet, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.Deny, Permission.Allow, Permission.NotSet, 4)]
        [TestCase(Permission.NotSet, Permission.Allow, Permission.NotSet, Permission.Allow, 4)]
        public void CounterpartyPermissionsForDeleteTopic(Permission buyOurPermission, 
            Permission sellTheirPermission, 
            Permission sellOurPermission, 
            Permission buyTheirPermission,
            int deleteTopicCount)
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = DateTime.Now.AddDays(-1);

            var response = GetCounterpartyPermissionsRefreshResponse(now, yesterday, buyOurPermission, sellTheirPermission, sellOurPermission, buyTheirPermission);
            _mockAomBus.SpyOnSubscribe<GetCounterpartyPermissionsRefreshResponse>();

            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }

            _mockOrganisationDiffusionTopicManager.Verify(m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(deleteTopicCount));
        }

        [Test]
        [TestCase(true, 0)]
        [TestCase(false, 1)]
        public void WhenProcessorIsStartingUpItShouldNotDeleteAnyPermissionTopicsAsTheyWontHaveBeenCreated(bool processorStartingUp, int deleteTopicCount)
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = DateTime.Now.AddDays(-1);

            var response = GetCounterpartyPermissionsRefreshResponse(now, yesterday, Permission.Deny, Permission.Deny, Permission.Deny, Permission.Deny);
            response.ProcessorStartingUp = processorStartingUp;
            _mockAomBus.SpyOnSubscribe<GetCounterpartyPermissionsRefreshResponse>();

            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }

            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.DeleteTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Principals/Buy/2", "*", It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(deleteTopicCount));

            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.DeleteTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Principals/Sell/1", "*", It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(deleteTopicCount));
        }

        private static GetCounterpartyPermissionsRefreshResponse GetCounterpartyPermissionsRefreshResponse(DateTime now,
            DateTime yesterday, Permission ourBuyPermission, Permission theirSellPermission, Permission ourSellPermission, Permission theirBuyPermission)
        {
            var response = new GetCounterpartyPermissionsRefreshResponse()
            {
                CounterpartyPermissions = new List<BilateralMatrixPermissionPair>
                {
                    new BilateralMatrixPermissionPair
                    {
                        BuySide = new BilateralMatrixPermission
                        {
                            BuyOrSell = BuyOrSell.Buy,
                            OurOrganisation = 1,
                            OurPermAllowOrDeny = ourBuyPermission,
                            OurLastUpdated = now,
                            LastUpdatedByUser = "OurUser",
                            TheirOrganisation = 2,
                            TheirLastUpdated = yesterday,
                            TheirPermAllowOrDeny = theirBuyPermission
                        },
                        SellSide = new BilateralMatrixPermission
                        {
                            BuyOrSell = BuyOrSell.Sell,
                            OurOrganisation = 1,
                            OurPermAllowOrDeny = ourSellPermission,
                            OurLastUpdated = now,
                            LastUpdatedByUser = "OurUser",
                            TheirOrganisation = 2,
                            TheirLastUpdated = yesterday,
                            TheirPermAllowOrDeny = theirSellPermission
                        },
                        ProductId = 2,
                        MarketId = 1,
                        OrgId = 1,
                        OrgShortCode = "ORG1",
                        OrgName = "Organisation1",
                        OrgLegalName = "LegalOrganisation1"
                    }
                }
            };
            return response;
        }

        private static GetBrokerPermissionsRefreshResponse GetBrokerPermissionsRefreshResponse(DateTime now,
            DateTime yesterday, Permission ourBuyPermission, Permission theirSellPermission, Permission ourSellPermission, Permission theirBuyPermission)
        {
            var response = new GetBrokerPermissionsRefreshResponse()
            {
                BrokerPermissions = new List<BilateralMatrixPermissionPair>
                {
                    new BilateralMatrixPermissionPair
                    {
                        BuySide = new BilateralMatrixPermission
                        {
                            BuyOrSell = BuyOrSell.Buy,
                            OurOrganisation = 1,
                            OurPermAllowOrDeny = ourBuyPermission,
                            OurLastUpdated = now,
                            LastUpdatedByUser = "OurUser",
                            TheirOrganisation = 2,
                            TheirLastUpdated = yesterday,
                            TheirPermAllowOrDeny = theirBuyPermission
                        },
                        SellSide = new BilateralMatrixPermission
                        {
                            BuyOrSell = BuyOrSell.Sell,
                            OurOrganisation = 1,
                            OurPermAllowOrDeny = ourSellPermission,
                            OurLastUpdated = now,
                            LastUpdatedByUser = "OurUser",
                            TheirOrganisation = 2,
                            TheirLastUpdated = yesterday,
                            TheirPermAllowOrDeny = theirSellPermission
                        },
                        ProductId = 2,
                        MarketId = 1,
                        OrgId = 1,
                        OrgShortCode = "ORG1",
                        OrgName = "Organisation1",
                        OrgLegalName = "LegalOrganisation1"
                    }
                }
            };
            return response;
        }

        [Test]
        public void DenyCounterpartyPermissionsAreNotPushedToDiffusion()
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = DateTime.Now.AddDays(-1);
            var response = new GetCounterpartyPermissionsRefreshResponse
            {
                CounterpartyPermissions = new List<BilateralMatrixPermissionPair>
                {
                    new BilateralMatrixPermissionPair 
                    {
                        BuySide = new BilateralMatrixPermission { 
                            BuyOrSell = BuyOrSell.Buy, OurOrganisation = 1, OurPermAllowOrDeny = Permission.Deny, OurLastUpdated = now, LastUpdatedByUser = "OurUser",
                            TheirOrganisation = 2, TheirLastUpdated = yesterday, TheirPermAllowOrDeny = Permission.Deny},
                        SellSide = new BilateralMatrixPermission { 
                            BuyOrSell = BuyOrSell.Sell, OurOrganisation = 2, OurPermAllowOrDeny = Permission.Allow, OurLastUpdated = yesterday, LastUpdatedByUser = "TheirUser",
                            TheirOrganisation = 1, TheirLastUpdated = now, TheirPermAllowOrDeny = Permission.Allow},
                        ProductId = 2,
                        MarketId = 1,
                        OrgId = 1,
                        OrgShortCode = "ORG1",
                        OrgName = "Organisation1",
                        OrgLegalName = "LegalOrganisation1"
                    }
                }
            };
            _mockAomBus.SpyOnSubscribe<GetCounterpartyPermissionsRefreshResponse>();

            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }

            //Only two deletes as counterparty checks both sides of deal (buy & sell) whilst broker defaults sell permission to buy permission (only one tick on gui)
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.DeleteTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Principals/Buy/2", It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.DeleteTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Principals/Sell/1", It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(1));
        }

        [Test]
        public void AllowBrokerPermissionsArePushedToDiffusion()
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = DateTime.Now.AddDays(-1);
            var response = new GetBrokerPermissionsRefreshResponse
            {
                BrokerPermissions = new List<BilateralMatrixPermissionPair>
                {
                    new BilateralMatrixPermissionPair 
                    {
                        BuySide = new BilateralMatrixPermission { 
                            BuyOrSell = BuyOrSell.Buy, OurOrganisation = 1, OurPermAllowOrDeny = Permission.Allow, OurLastUpdated = now, LastUpdatedByUser = "OurUser",
                            TheirOrganisation = 2, TheirLastUpdated = yesterday, TheirPermAllowOrDeny = Permission.Allow},
                        SellSide = new BilateralMatrixPermission { 
                            BuyOrSell = BuyOrSell.Sell, OurOrganisation = 2, OurPermAllowOrDeny = Permission.Allow, OurLastUpdated = now, LastUpdatedByUser = "OurUser",
                            TheirOrganisation = 1, TheirLastUpdated = yesterday, TheirPermAllowOrDeny = Permission.Allow},
                        ProductId = 2,
                        MarketId = 1,
                        OrgId = 1,
                        OrgShortCode = "BRK1",
                        OrgName = "Broker1",
                        OrgLegalName = "LegalBroker1"
                    }
                }
            };
            _mockAomBus.SpyOnSubscribe<GetBrokerPermissionsRefreshResponse>();
            
            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }

            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Brokers/Buy/2", It.IsAny<string>(), null), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Brokers/Sell/2", It.IsAny<string>(), null), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Brokers/Buy/1", It.IsAny<string>(), null), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.SendDataToTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Brokers/Sell/1", It.IsAny<string>(), null), Times.Exactly(1));
        }

        [Test]
        public void DenyBrokerPermissionsAreNotPushedToDiffusion()
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = DateTime.Now.AddDays(-1);
            var response = new GetBrokerPermissionsRefreshResponse
            {
                BrokerPermissions = new List<BilateralMatrixPermissionPair>
                {
                    new BilateralMatrixPermissionPair 
                    {
                        BuySide = new BilateralMatrixPermission { 
                            BuyOrSell = BuyOrSell.Buy, OurOrganisation = 1, OurPermAllowOrDeny = Permission.Deny, OurLastUpdated = now, LastUpdatedByUser = "OurUser",
                            TheirOrganisation = 2, TheirLastUpdated = yesterday, TheirPermAllowOrDeny = Permission.Deny},
                        SellSide = new BilateralMatrixPermission { 
                            BuyOrSell = BuyOrSell.Sell, OurOrganisation = 1, OurPermAllowOrDeny = Permission.Allow, OurLastUpdated = yesterday, LastUpdatedByUser = "TheirUser",
                            TheirOrganisation = 2, TheirLastUpdated = now, TheirPermAllowOrDeny = Permission.Allow},
                        ProductId = 2,
                        MarketId = 1,
                        OrgId = 1,
                        OrgShortCode = "BRK1",
                        OrgName = "Broker1",
                        OrgLegalName = "LegalBroker1"
                    }
                }
            };
            _mockAomBus.SpyOnSubscribe<GetBrokerPermissionsRefreshResponse>();

            _mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            dynamic actions = _mockAomBus.GetSubscriptionActions();
            foreach (dynamic action in actions)
            {
                action(response);
            }

            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.DeleteTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Brokers/Buy/2", It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.DeleteTopic("AOM/Organisations/1/PermissionsMatrix/Summary/Products/2/Brokers/Sell/2", It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.DeleteTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Brokers/Buy/1", It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(1));
            _mockOrganisationDiffusionTopicManager.Verify(x =>
                x.DeleteTopic("AOM/Organisations/2/PermissionsMatrix/Summary/Products/2/Brokers/Sell/1", It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()), Times.Exactly(1));
        }

        [Test]
        public void ShouldReturnCounterpartyPermissionBuyDetailPath()
        {
            var path = _organisationRouterService.PermissionTopicPath(true, 100, 20, BuyOrSell.Buy, 200);
            Assert.That(path, Is.EqualTo("AOM/Organisations/100/PermissionsMatrix/Summary/Products/20/Principals/Buy/200"));
        }

        [Test]
        public void ShouldReturnCounterpartyPermissionSellDetailPath()
        {
            var path = _organisationRouterService.PermissionTopicPath(true, 100, 20, BuyOrSell.Sell, 200);
            Assert.That(path, Is.EqualTo("AOM/Organisations/100/PermissionsMatrix/Summary/Products/20/Principals/Sell/200"));
        }

        [Test]
        public void ShouldReturnBrokerPermissionSellDetailPath()
        {
            var path = _organisationRouterService.PermissionTopicPath(false, 100, 20, BuyOrSell.Sell, 200);
            Assert.That(path, Is.EqualTo("AOM/Organisations/100/PermissionsMatrix/Summary/Products/20/Brokers/Sell/200"));
        }

        [Test]
        public void ShouldReturnBrokerPermissionBuyDetailPath()
        {
            var path = _organisationRouterService.PermissionTopicPath(false, 100, 20, BuyOrSell.Buy, 200);
            Assert.That(path, Is.EqualTo("AOM/Organisations/100/PermissionsMatrix/Summary/Products/20/Brokers/Buy/200"));
        }

        private SubscribedActions StartConsumer(Mock<IOrganistionDiffusionTopicManager> mockOrganisationDiffusionTopicManager)
        {
            var subs = new SubscribedActions();

            _mockAomBus.Setup(b => b.Subscribe("OrganisationRouterService", It.IsAny<Action<AomOrganisationMessageResponse>>()))
                .Callback<string, Action<AomOrganisationMessageResponse>>((s, a) => subs.AomOrganisationMessageResponse = a);

            mockOrganisationDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            return subs;
        }

        public class SubscribedActions
        {
            public Action<AomOrganisationMessageResponse> AomOrganisationMessageResponse { get; set; }
        }
    }
}