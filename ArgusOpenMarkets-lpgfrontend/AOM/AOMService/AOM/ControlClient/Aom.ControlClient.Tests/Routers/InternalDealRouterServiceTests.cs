﻿using System;
using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.ControlClient.Routers;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.ControlClient.Tests.Routers
{
    [TestFixture]
    public class InternalDealRouterServiceTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IMarketTickerDiffusionTopicManager> _mockTopicManager;
        private Mock<IClientSessionMessageHandler> _mockMessageHandler;
        private InternalDealRouterService _service;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockTopicManager = new Mock<IMarketTickerDiffusionTopicManager>();
            _mockMessageHandler = new Mock<IClientSessionMessageHandler>();
            _service = new InternalDealRouterService(_mockBus.Object, _mockTopicManager.Object,
                _mockMessageHandler.Object);
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_service);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockTopicManager, _service);
        }

        [Test]
        public void InternalDealRouterServiceGoActiveThenDisconnectingShouldDisposeSubscriptions()
        {
            _mockTopicManager.GoActive();
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _service.Disconnect();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void InternalDealRouterServiceGoActiveThenStandbyShouldDisposeSubscriptions()
        {
            _mockTopicManager.GoActive();
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _mockTopicManager.GoStandby();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void OnAomDealResponseWhenTheResponseIsADealShouldPublishAAomDealRequestOnTheBus()
        {
            var subs = StartConsumer();
            _mockTopicManager.GoActive();

            var onAomDealResponse = subs.AomDealResponse;

            var message = new Message {MessageAction = MessageAction.Reinstate, MessageType = MessageType.Deal};
            var deal = new Deal {Id = 1};
            var userContactDetails = new UserContactDetails {Id = 100};
            onAomDealResponse(
                new AomDealResponse {Deal = deal, Message = message, UserContactDetails = userContactDetails});

            _mockMessageHandler.Verify(
                m => m.SendMessage(It.IsAny<ClientSessionInfo>(), It.IsAny<string>()),
                Times.Exactly(1));
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("InternalDealRouterService", It.IsAny<Action<AomDealResponse>>()))
                .Callback<string, Action<AomDealResponse>>((s, a) => subs.AomDealResponse = a);

            return subs;
        }

        public class SubscribedActions
        {
            public Action<AomDealResponse> AomDealResponse { get; set; }
        }
    }
}