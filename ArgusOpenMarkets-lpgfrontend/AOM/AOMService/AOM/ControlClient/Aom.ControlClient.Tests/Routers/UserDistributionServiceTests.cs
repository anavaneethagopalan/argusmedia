﻿using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests.Routers
{
    using AOM.ControlClient.Interfaces;
    using AOM.ControlClient.Routers;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Users;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    using System;

    [TestFixture]
    public class UserDistributionTests
    {
        private UserDistributionService _userDistributionService;

        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;

        private Mock<IAuthenticationDiffusionTopicManager> _mockAuthenticationDiffusionTopicManager;

        private Mock<IDiffusionSessionManager> _mockDiffusionSessionManager;

        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();
            _mockAuthenticationDiffusionTopicManager = new Mock<IAuthenticationDiffusionTopicManager>();
            _mockDiffusionSessionManager = new Mock<IDiffusionSessionManager>();
            _userDistributionService = new UserDistributionService(
                _mockBus.Object,
                _mockAuthenticationDiffusionTopicManager.Object,
                _mockDiffusionSessionManager.Object,
                _mockClientSessionMessageHandler.Object);
        }

        [Test]
        public void AuthenticationDiffusionTopicManagerGoingActiveShouldSubscribeToTerminateResponse()
        {
            StartConsumer(_mockAuthenticationDiffusionTopicManager);

            _mockBus.Verify(
                m => m.Subscribe("UserDistributionService", It.IsAny<Action<TerminateResponse>>()),
                Times.Exactly(1));
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_userDistributionService);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockAuthenticationDiffusionTopicManager,
                                                                  _userDistributionService);
        }

        [Test]
        public void AuthenticationDiffusionTopicManagerGoingActiveThenDisconnectingShouldDisposeSubscriptions()
        {
            StartConsumer(_mockAuthenticationDiffusionTopicManager);

            Assert.That(_userDistributionService.SubscriberCount, Is.GreaterThan(0));
            _userDistributionService.Disconnect();
            Assert.That(_userDistributionService.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void AuthenticationDiffusionTopicManagerGoingActiveThenStandbyShouldDisposeSubscriptions()
        {
            StartConsumer(_mockAuthenticationDiffusionTopicManager);
            Assert.That(_userDistributionService.SubscriberCount, Is.GreaterThan(0));
            _mockAuthenticationDiffusionTopicManager.GoStandby();
            Assert.That(_userDistributionService.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void OnTerminateUserShouldRemoveTheUserFromDiffusionSessions()
        {
            var subs = StartConsumer(_mockAuthenticationDiffusionTopicManager);

            var onTerminateUser = subs.TerminateResponse;
            onTerminateUser(
                new TerminateResponse
                {
                    Message = new Message(),
                    Reason = "USER LOGGED IN",
                    UserId = 1,
                    UserName = "Fred"
                });

            _mockDiffusionSessionManager.Verify(m => m.RemoveUserSessions(1), Times.Exactly(1));
        }

        [Test]
        public void CallingGetLoggedOnUsersWhenNooneHasLoggedInShouldReturnAnEmptyList()
        {
            var req = new GetLoggedOnUsersRequest();
            var loggedOnUsers = _userDistributionService.GetLoggedOnUsers(req);

            Assert.That(loggedOnUsers.LoggedOnUsers, Is.Not.Null);
            Assert.That(loggedOnUsers.LoggedOnUsers.Count, Is.EqualTo(0));
        }

        private SubscribedActions StartConsumer(
            Mock<IAuthenticationDiffusionTopicManager> mockAuthenticationDiffusionTopicManager)
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("UserDistributionService", It.IsAny<Action<TerminateResponse>>()))
                .Callback<string, Action<TerminateResponse>>((s, a) => subs.TerminateResponse = a);

            mockAuthenticationDiffusionTopicManager.GoActive();
            return subs;
        }

        public class SubscribedActions
        {
            public Action<TerminateResponse> TerminateResponse { get; set; }
        }
    }
}