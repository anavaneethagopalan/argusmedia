﻿using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests.Routers
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.ControlClient.Interfaces;
    using AOM.ControlClient.Routers;
    using AOM.Transport.Events.News;
    using Argus.Transport.Infrastructure;
    using Moq;
    using NUnit.Framework;
    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using System;
    using System.Collections.Generic;
    using Utils.Logging;

    [TestFixture]
    internal class NewsRouterServiceTests
    {
        private Mock<IBus> _mockBus;

        private NewsRouterService _service;

        private Mock<INewsDiffusionTopicManager> _mockNewsTopicManager;

        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;

        private Mock<IAomNewsService> _mockAomNewsService;

        private List<Tuple<string, string>> _publishedMessages;

        [SetUp]
        public void Setup()
        {
            _publishedMessages = new List<Tuple<string, string>>();
            _mockBus = new Mock<IBus>();
            _mockNewsTopicManager = new Mock<INewsDiffusionTopicManager>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();
            _mockAomNewsService = new Mock<IAomNewsService>();

            _service = new NewsRouterService(
                _mockBus.Object,
                _mockNewsTopicManager.Object,
                _mockClientSessionMessageHandler.Object,
                _mockAomNewsService.Object);

            _mockNewsTopicManager.Setup(
                m => m.SendDataToTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicUpdaterUpdateCallback>()))
                .Callback<string, string, ITopicUpdaterUpdateCallback>(
                    (topic, content, callback) => _publishedMessages.Add(Tuple.Create(topic, content)));
        }

        [Test]
        public void PublishAomNewsDeletedEventForPaidNewsWithNoContentStreamsShouldSendNoDeleteNewsTopicEvent()
        {
            var subs = StartConsumer();
            var onArgusNewsDeleted = subs.AomNewsDeleted;

            var message = new AomNewsDeleted { CmsId = "1", CommodityId = 1, ContentStreams = null, IsFree = false };

            onArgusNewsDeleted(message);

            _mockNewsTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(0));
        }

        [Test]
        public void PublishAomNewsDeletedEventForFreeNewsShouldSendOneDeleteTopicEventToTheNewsTopicManager()
        {
            var subs = StartConsumer();
            var onArgusNewsDeleted = subs.AomNewsDeleted;

            var message = new AomNewsDeleted { CmsId = "1", CommodityId = 1, ContentStreams = null, IsFree = true };

            onArgusNewsDeleted(message);

            _mockNewsTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(1));
        }

        [Test]
        public void PublishAomNewsDeletedEventForFreeNewsShouldSendOneDeleteTopicContainingFreeAndCmsIdAndCommodityId()
        {
            var subs = StartConsumer();
            var onArgusNewsDeleted = subs.AomNewsDeleted;

            var message = new AomNewsDeleted { CmsId = "1", CommodityId = 1, ContentStreams = null, IsFree = true };

            onArgusNewsDeleted(message);

            _mockNewsTopicManager.Verify(
                m =>
                    m.DeleteTopic(
                        It.Is<string>(s => s == "AOM/News/Free/1/1"),
                        It.IsAny<string>(),
                        It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(1));
        }

        [Test]
        public void PublishAomNewsDeletedEventForPaidForNewsWithOneContentStreamShouldSendOneDeleteTopicEvent()
        {
            var subs = StartConsumer();
            var onArgusNewsDeleted = subs.AomNewsDeleted;

            var contentStreams = new long[] { 1 };
            var message = new AomNewsDeleted
                          {
                              CmsId = "1",
                              CommodityId = 1,
                              ContentStreams = contentStreams,
                              IsFree = false
                          };

            onArgusNewsDeleted(message);

            _mockNewsTopicManager.Verify(
                m =>
                    m.DeleteTopic(
                        It.Is<string>(s => s == "AOM/News/ContentStreams/1/1"),
                        It.IsAny<string>(),
                        It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(1));
        }

        [Test]
        public void PublishAomNewsDeletedEventForPaidForNewsWithTwoContentStreamShouldSendTwoDeleteTopicEvents()
        {
            var subs = StartConsumer();
            var onArgusNewsDeleted = subs.AomNewsDeleted;

            var contentStreams = new long[] { 1, 2 };
            var message = new AomNewsDeleted
                          {
                              CmsId = "1",
                              CommodityId = 1,
                              ContentStreams = contentStreams,
                              IsFree = false
                          };

            onArgusNewsDeleted(message);

            _mockNewsTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(2));
        }

        [Test]
        public void
            PublishAomNewsUpdatedWhenThePublishedDateHasChangedShouldDeleteThePreviousNewsStoryAndAddTheUpdatedNewsStory
            ()
        {
            var subs = StartConsumer();
            var onArgusNewsUpdated = subs.AomNewsUpdated;

            var contentStreams = new long[] { 1 };
            var pubDate = new DateTime(2015, 1, 1);
            var newPubDate = new DateTime(2015, 2, 2);

            var prevNews = new AomNews
                           {
                               CmsId = "1",
                               CommodityId = 1,
                               Headline = "OldNews",
                               ContentStreams = contentStreams,
                               IsFree = true,
                               PublicationDate = pubDate
                           };

            var updatedNews = new AomNews
                              {
                                  CmsId = "1",
                                  CommodityId = 1,
                                  ContentStreams = contentStreams,
                                  Headline = "NewNews",
                                  IsFree = true,
                                  PublicationDate = newPubDate
                              };

            var message = new AomNewsUpdated { PreviousNews = prevNews, UpdatedNews = updatedNews };

            onArgusNewsUpdated(message);

            _mockNewsTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(1));

            _mockNewsTopicManager.Verify(
                m => m.SendDataToTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TopicUpdateCallback>()),
                Times.Exactly(1));
        }

        [Test]
        public void
            PublishAomNewsUpdatedWhenThePublishedDateHasChangedShouldDeleteThePreviousNewsStoryAndNotAddTheUpdatedStoryIfItIsNotForAom
            ()
        {
            var subs = StartConsumer();
            var onArgusNewsUpdated = subs.AomNewsUpdated;

            var contentStreams = new long[] { 1 };
            var pubDate = new DateTime(2015, 1, 1);
            var newPubDate = new DateTime(2015, 2, 2);

            var prevNews = new AomNews
                           {
                               CmsId = "1",
                               CommodityId = 1,
                               Headline = "OldNews",
                               ContentStreams = contentStreams,
                               IsFree = true,
                               PublicationDate = pubDate
                           };

            var updatedNews = new AomNews
                              {
                                  CmsId = "1",
                                  CommodityId = 1,
                                  ContentStreams = null,
                                  Headline = "NewNews",
                                  IsFree = false,
                                  PublicationDate = newPubDate
                              };

            var message = new AomNewsUpdated { PreviousNews = prevNews, UpdatedNews = updatedNews };

            onArgusNewsUpdated(message);

            _mockNewsTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(1));

            _mockNewsTopicManager.Verify(
                m => m.SendDataToTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TopicUpdateCallback>()),
                Times.Exactly(0));
        }

        [Test]
        public void
            PublishAomNewsUpdatedWhenThePublishedDateHasChangedShouldDeleteThePreviousNewsStoryAndAddTheUpdatedStoryOnAllAomContentStreams
            ()
        {
            var subs = StartConsumer();
            var onArgusNewsUpdated = subs.AomNewsUpdated;

            var contentStreams = new long[] { 1, 2 };

            var pubDate = new DateTime(2015, 1, 1);
            var newPubDate = new DateTime(2015, 2, 2);

            var prevNews = new AomNews
                           {
                               CmsId = "1",
                               CommodityId = 1,
                               Headline = "OldNews",
                               ContentStreams = contentStreams,
                               IsFree = true,
                               PublicationDate = pubDate
                           };

            var updatedNews = new AomNews
                              {
                                  CmsId = "1",
                                  CommodityId = 1,
                                  ContentStreams = contentStreams,
                                  Headline = "NewNews",
                                  IsFree = false,
                                  PublicationDate = newPubDate
                              };

            var message = new AomNewsUpdated { PreviousNews = prevNews, UpdatedNews = updatedNews };

            onArgusNewsUpdated(message);

            _mockNewsTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(1));

            _mockNewsTopicManager.Verify(
                m => m.SendDataToTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TopicUpdateCallback>()),
                Times.Exactly(2));
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_service);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockNewsTopicManager, _service);
        }

        [Test]
        public void StartingTheNewsRouterServiceThenCallingDisconnectShouldRemoveAllSubscribers()
        {
            StartConsumer();

            Assert.That(_service.SubscriberCount, Is.EqualTo(4));
            _service.Disconnect();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void ShouldDisposeSubscribersWhenTopicManagerGoesFromActiveToStandby()
        {
            StartConsumer();
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _mockNewsTopicManager.GoStandby();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void ShouldPublishReplayArgusNewsWhenPurgeNewsResponseReceived()
        {
            var subs = StartConsumer();
            var purgeNewsResponse = new PurgeNewsResponse
            {
                DaysToKeep = 123
            };
            using (var appender = new MemoryAppenderForTests())
            {
                subs.PurgeNewsResponseHandler(purgeNewsResponse);
                _mockBus.Verify(
                    b => b.Publish(It.Is<ReplayArgusNews>(ran => ran.DaysToReplay == purgeNewsResponse.DaysToKeep)),
                    Times.Once);
            }
        }

        [Test]
        public void ShouldDeleteNewsTopicWhenPurgeNewsResponseReceived()
        {
            var subs = StartConsumer();
            var purgeNewsResponse = new PurgeNewsResponse
            {
                DaysToKeep = 123
            };
            subs.PurgeNewsResponseHandler(purgeNewsResponse);
            _mockNewsTopicManager.Verify(m => m.DeleteTopic("AOM/News/", ">", It.IsAny<ITopicControlRemoveCallback>()),
                                         Times.Once);
        }

        private class SubscribedActions
        {
            public Action<AomNewsDeleted> AomNewsDeleted { get; set; }

            public Action<AomNewsUpdated> AomNewsUpdated { get; set; }

            public Action<PurgeNewsResponse> PurgeNewsResponseHandler { get; set; }
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();
            _mockBus.Setup(b => b.Subscribe("NewsRouterService", It.IsAny<Action<AomNewsDeleted>>()))
                .Callback<string, Action<AomNewsDeleted>>((s, a) => subs.AomNewsDeleted = a);

            _mockBus.Setup(b => b.Subscribe("NewsRouterService", It.IsAny<Action<AomNewsUpdated>>()))
                .Callback<string, Action<AomNewsUpdated>>((s, a) => subs.AomNewsUpdated = a);

            _mockBus.Setup(b => b.Subscribe("NewsRouterService", It.IsAny<Action<PurgeNewsResponse>>()))
                    .Callback<string, Action<PurgeNewsResponse>>((s, a) => subs.PurgeNewsResponseHandler = a);

            _service.Run();
            _mockNewsTopicManager.GoActive();
            return subs;
        }
    }
}