﻿using System;
using AOM.ControlClient.Interfaces;
using AOM.ControlClient.Routers;
using AOM.Transport.Events.ExternalDeals;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.ControlClient.Tests.Routers
{
    [TestFixture]
    public class ExternalDealRouterServiceTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IMarketTickerDiffusionTopicManager> _mockTopicManager;
        private Mock<IClientSessionMessageHandler> _mockMessageHandler;
        private ExternalDealRouterService _service;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockTopicManager = new Mock<IMarketTickerDiffusionTopicManager>();
            _mockMessageHandler = new Mock<IClientSessionMessageHandler>();
            _service = new ExternalDealRouterService(_mockBus.Object, _mockTopicManager.Object,
                _mockMessageHandler.Object);
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_service);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockTopicManager, _service);
        }

        [Test]
        public void ExternalDealRouterServiceGoActiveThenDisconnectingShouldDisposeSubscriptions()
        {
            _mockTopicManager.GoActive();
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _service.Disconnect();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void ExternalDealRouterServiceGoActiveThenStandbyShouldDisposeSubscriptions()
        {
            _mockTopicManager.GoActive();
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _mockTopicManager.GoStandby();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        public class SubscribedActions
        {
            public Action<AomExternalDealResponse> AomExternalDealResponse { get; set; }
        }
    }
}