﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.ControlClient.Routers;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketTickers;
using AOM.App.Domain.Dates;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using log4net.Core;
using Moq;
using NUnit.Framework;
using Utils.Logging;

using PushTechnology.ClientInterface.Client.Features.Control.Topics;

namespace AOM.ControlClient.Tests.Routers
{
    [TestFixture]
    internal class MarketTickerRouterServiceTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IMarketTickerDiffusionTopicManager> _mockTopicManager;
        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;

        // TODO Use a Mock here and inject test times for the tests
        private IDateTimeProvider _dateTimeProvider;
        private MarketTickerRouterService _service;
        private List<Tuple<string, string>> _publishedMessages;

        [SetUp]
        public void Setup()
        {
            _publishedMessages = new List<Tuple<string, string>>();
            _mockBus = new Mock<IBus>();
            _mockTopicManager = new Mock<IMarketTickerDiffusionTopicManager>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();
            _dateTimeProvider = new DateTimeProvider();

            _service = new MarketTickerRouterService(
                _mockBus.Object,
                _mockTopicManager.Object,
                _mockClientSessionMessageHandler.Object,
                _dateTimeProvider);

            _mockTopicManager.Setup(m =>
                    m.SendDataToTopic(It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<ITopicUpdaterUpdateCallback>()))
                .Callback<string, string, ITopicUpdaterUpdateCallback>(
                    (topic, content, callback) => _publishedMessages.Add(Tuple.Create(topic, content)));
        }

        #region Diffusion topic generation tests

        [Test]
        public void TestTopicGenerationForPendingMarketInfo()
        {
            var organisationIds = new long[] {2, 4};
            var productIds = new long[] {7, 9};
            const long fakeItemId = 123456;

            var response = CreateMarketTickResponse(MarketTickerItemType.Info, MarketTickerItemStatus.Pending,
                fakeItemId, organisationIds, productIds);
            List<string> expectedTopics = new List<string>();
            //CreateExpectedTopicsForPendingItem( organisationIds, productIds, response.MarketTickerItem);
            foreach (var prodId in productIds)
            {
                foreach (var orgId in organisationIds)
                {
                    expectedTopics.Add(CreateOrganisationOrdersTopicPathForOrder(prodId, orgId, fakeItemId));
                }
            }
            expectedTopics.Add("AOM/MarketTicker/Products/7/Editor/123456");
            expectedTopics.Add("AOM/MarketTicker/Products/9/Editor/123456");
            AssertPublishedTopicsAreAsExpected(response, expectedTopics);
        }

        [Test]
        public void TestTopicGenerationForPendingExternalDeal()
        {
            // TODO: Currently the MarketTickerRouterService can only deal with ExternalDeals for 1 product
            var organisationIds = new long[] {1, 2, 4};
            var productIds = new long[] {5};
            const long fakeItemId = 123456;

            var response = CreateMarketTickResponse(MarketTickerItemType.Deal, MarketTickerItemStatus.Pending,
                fakeItemId, organisationIds, productIds, isExternalDeal: true);

            List<string> expectedTopics = new List<string>();
            foreach (var orgId in organisationIds)
            {
                expectedTopics.Add(CreateOrganisationOrdersTopicPathForOrder(productIds[0], orgId, fakeItemId));
            }
            expectedTopics.Add("AOM/MarketTicker/Products/5/Editor/123456");
            AssertPublishedTopicsAreAsExpected(response, expectedTopics);
        }

        [Test]
        [TestCase(MarketTickerItemType.Ask)]
        [TestCase(MarketTickerItemType.Bid)]
        public void TestTopicGenerationForHeldOrderInfo(MarketTickerItemType itemType)
        {
            // A market ticker item can only have a status of 'Held' when an already Held order is updated
            var organisationIds = new long[] {1, 2, 4};
            var productIds = new long[] {5};
            const long fakeItemId = 123456;

            var response = CreateMarketTickResponse(itemType, MarketTickerItemStatus.Held, fakeItemId, organisationIds,
                productIds, isExternalDeal: true);

            List<string> expectedTopics = new List<string>();
            foreach (var orgId in organisationIds)
            {
                foreach (var productId in productIds)
                {
                    expectedTopics.Add(CreateOrganisationOrdersTopicPathForOrder(productId, orgId, fakeItemId));
                }
            }

            AssertPublishedTopicsAreAsExpected(response, expectedTopics);
        }

        [Test]
        public void TestTopicGenerationForVoidMarketInfo()
        {
            var organisationIds = new long[] {1, 2};
            var productIds = new long[] {5, 7};
            const long fakeItemId = 123456;

            var response = CreateMarketTickResponse(MarketTickerItemType.Info, MarketTickerItemStatus.Void, fakeItemId,
                organisationIds, productIds);

            var expectedTopics = new List<string>
            {
                "AOM/MarketTicker/Products/5/All/123456",
                "AOM/MarketTicker/Products/7/All/123456"
            };

            AssertPublishedTopicsAreAsExpected(response, expectedTopics);
        }

        [Test]
        [TestCase(MarketTickerItemType.Deal, true, DealStatus.Pending)]
        public void TestTopicGenerationForVoidOtherItems(MarketTickerItemType itemType, bool isExternalDeal,
            DealStatus previousDealStatus)
        {
            // The topics published to for voided external deals depends on whether that deal was previously
            // pending or executed (see AR-1271).
            var organisationIds = new long[] {1, 2};
            var productIds = new long[] {5, 7};
            const long fakeItemId = 123456;

            var additionalData = new AdditionalMarketTickerData();
            if (previousDealStatus == DealStatus.None)
            {
                additionalData.PreviousDealStatus = previousDealStatus;
            }

            var response = CreateMarketTickResponse(itemType, MarketTickerItemStatus.Void, fakeItemId, organisationIds,
                productIds, isExternalDeal: isExternalDeal, data: additionalData);

            var expectedTopics = new List<string>
            {
                "AOM/MarketTicker/Products/5/All/123456",
                "AOM/MarketTicker/Products/7/All/123456"
            };

            AssertPublishedTopicsAreAsExpected(response, expectedTopics);
        }

        [Test]
        public void TestTopicGenerationForActiveMarketInfo()
        {
            var organisationIds = new long[] {1, 2, 4};
            var productIds = new long[] {5, 7, 9};
            const long fakeItemId = 123456;
            var response = CreateMarketTickResponse(MarketTickerItemType.Info, MarketTickerItemStatus.Active, fakeItemId,
                organisationIds, productIds);

            List<string> expectedTopics = new List<string>
            {
                "AOM/MarketTicker/Products/5/All/123456",
                "AOM/MarketTicker/Products/7/All/123456",
                "AOM/MarketTicker/Products/9/All/123456",
            };

            AssertPublishedTopicsAreAsExpected(response, expectedTopics);
        }

        [Test]
        [TestCase(MarketTickerItemType.Ask, false)]
        [TestCase(MarketTickerItemType.Bid, false)]
        [TestCase(MarketTickerItemType.Deal, false)]
        [TestCase(MarketTickerItemType.Deal, true)]
        public void TestTopicGenerationForActiveOtherItems(MarketTickerItemType itemType, bool isExternalDeal = false)
        {
            var organisationIds = new long[] {1, 2};
            var productIds = new long[] {5, 7};
            const long fakeItemId = 123456;

            var response = CreateMarketTickResponse(itemType, MarketTickerItemStatus.Active, fakeItemId, organisationIds,
                productIds, isExternalDeal: isExternalDeal);

            var expectedTopics = new List<string>
            {
                "AOM/MarketTicker/Products/5/All/123456",
                "AOM/MarketTicker/Products/7/All/123456"
            };
            AssertPublishedTopicsAreAsExpected(response, expectedTopics);
        }

        [Test]
        public void ShouldLogErrorIfAskedToPushVoidedExternalDealWithNoPreviousDealStatus()
        {
            using (var testLog = new MemoryAppenderForTests())
            {
                var productIds = new long[] {5, 7, 9};
                var organisationIds = new long[] {1, 2, 4};
                const long fakeItemId = 123456;

                var response = CreateMarketTickResponse(MarketTickerItemType.Deal, MarketTickerItemStatus.Void,
                    fakeItemId, organisationIds, productIds, isExternalDeal: true,
                    data: new AdditionalMarketTickerData());
                var processorFn = StartServiceAndGetSubscriberFunction<AomNewMarketTickResponse>();
                Assert.DoesNotThrow(() => processorFn(response));

                testLog.AssertALogMessageContains(Level.Error, "has no PreviousDealStatus");
            }
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_service);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockTopicManager, _service);
        }

        [Test]
        public void ShouldClearSubscribersWhenTopicManagerGoesActiveToStandby()
        {
            TestHelpers.ShouldClearSubscribersWhenTopicManagerGoesActiveToStandby(_mockTopicManager, _service);
        }

        [Test]
        public void ShouldPublishRefreshMarketTickerRequestWhenTopicManagerActiveEventRaised()
        {
            _mockTopicManager.Raise(m => m.Active += null, EventArgs.Empty);
            _mockBus.Verify(b => b.Publish(It.IsAny<AomRefreshMarketTickerRequest>()), Times.Once);
        }

        [Test]
        public void ShouldSubscribeToTickerResponseMessagesWhenTopicManagerActiveEventRaised()
        {
            _mockTopicManager.Raise(m => m.Active += null, EventArgs.Empty);
            _mockBus.Verify(b => b.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomNewMarketTickResponse>>()),
                Times.Once);
            _mockBus.Verify(b => b.Subscribe(It.IsAny<string>(), It.IsAny<Action<PurgeMarketTickerResponse>>()),
                Times.Once);
        }

        [Test]
        public void ForwardPurgeMarketTickerResponseShouldEnsureWePublishAnAomRefreshMarketTickerRequest()
        {
            var subs = StartConsumer(_mockTopicManager);
            var onPurgeMarketTickerResponse = subs.PurgeMarketTickerResponse;
            var message = new PurgeMarketTickerResponse {DaysToKeep = 31};

            onPurgeMarketTickerResponse(message);
            _mockBus.Verify(m => m.Publish(It.IsAny<AomRefreshMarketTickerRequest>()), Times.AtLeastOnce);
        }

        [Test]
        public void
            ForwardPurgeMarketTickerResponseShouldPublishRefreshMarketTickerRequestWithStartDateInThePastWhenDaysToKeepIsPositive
            ()
        {
            var subs = StartConsumer(_mockTopicManager);
            var onPurgeMarketTickerResponse = subs.PurgeMarketTickerResponse;
            var message = new PurgeMarketTickerResponse {DaysToKeep = 10};

            onPurgeMarketTickerResponse(message);
            _mockBus.Verify(
                m => m.Publish(It.Is<AomRefreshMarketTickerRequest>(r => r.RefreshFromTime < DateTime.Today)),
                Times.AtLeast(2));
        }

        [Test]
        public void
            ForwardPurgeMarketTickerResponseShouldPublishRefreshMarketTickerRequestWithStartDateInThePastWhenDaysToKeepIsNegative
            ()
        {
            var subs = StartConsumer(_mockTopicManager);
            var onPurgeMarketTickerResponse = subs.PurgeMarketTickerResponse;
            var message = new PurgeMarketTickerResponse {DaysToKeep = -10};

            onPurgeMarketTickerResponse(message);
            _mockBus.Verify(
                m => m.Publish(It.Is<AomRefreshMarketTickerRequest>(r => r.RefreshFromTime < DateTime.Today)),
                Times.AtLeast(2));
        }

        [Test]
        public void MarketTickerDiffusionTopicManagerShouldDisconnectAllSubscribersWhenDisconnectIsCalled()
        {
            StartConsumer(_mockTopicManager);

            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _service.Disconnect();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void ShouldDisposeSubscribersWhenTopicManagerGoesFromActiveToStandby()
        {
            StartConsumer(_mockTopicManager);
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _mockTopicManager.GoStandby();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        private SubscribedActions StartConsumer(
            Mock<IMarketTickerDiffusionTopicManager> mockMarketTickerDiffusionTopicManager)
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("MarketTickerRouterService", It.IsAny<Action<PurgeMarketTickerResponse>>()))
                .Callback<string, Action<PurgeMarketTickerResponse>>((s, a) => subs.PurgeMarketTickerResponse = a);

            mockMarketTickerDiffusionTopicManager.GoActive();
            return subs;
        }

        public class SubscribedActions
        {
            public Action<PurgeMarketTickerResponse> PurgeMarketTickerResponse { get; set; }
        }


        private static AomNewMarketTickResponse CreateMarketTickResponse(MarketTickerItemType itemType,
            MarketTickerItemStatus itemStatus, long itemId, IEnumerable<long> ownerOrganisations = null,
            IEnumerable<long> productIds = null, string message = "", long domainItemId = 12345,
            bool isExternalDeal = false, AdditionalMarketTickerData data = null)
        {
            var r = new AomNewMarketTickResponse
            {
                MarketTickerItem = new MarketTickerItem
                {
                    Id = itemId,
                    MarketTickerItemType = itemType,
                    MarketTickerItemStatus = itemStatus,
                    Message = message,
                    OwnerOrganisations = ToList(ownerOrganisations),
                    ProductIds = ToList(productIds)
                },
                AdditionalData = data ?? new AdditionalMarketTickerData(),
                Message = new Message()
            };

            switch (itemType)
            {
                case MarketTickerItemType.Info:
                    r.MarketTickerItem.MarketInfoId = domainItemId;
                    break;
                case MarketTickerItemType.Ask:
                case MarketTickerItemType.Bid:
                    r.MarketTickerItem.OrderId = domainItemId;
                    break;
                case MarketTickerItemType.Deal:
                    if (isExternalDeal)
                    {
                        r.MarketTickerItem.ExternalDealId = domainItemId;
                    }
                    else
                    {
                        r.MarketTickerItem.DealId = domainItemId;
                    }
                    break;
            }
            return r;
        }

        private static List<T> ToList<T>(IEnumerable<T> ie)
        {
            return ie != null ? ie.ToList() : new List<T>();
        }

        private void AssertPublishedTopicsAreAsExpected(AomNewMarketTickResponse response,
            IEnumerable<string> expectedTopics)
        {
            var processorFn = StartServiceAndGetSubscriberFunction<AomNewMarketTickResponse>();
            Assert.DoesNotThrow(() => processorFn(response));
            var publishedTopics = _publishedMessages.Select(topicMessage => topicMessage.Item1);
            Assert.That(publishedTopics, Is.EquivalentTo(expectedTopics));
            //Assert.That(publishedTopics.Count(), Is.EqualTo(3));
        }

        private Action<T> StartServiceAndGetSubscriberFunction<T>() where T : class
        {
            Action<T> subsFn = null;
            _mockBus.Setup(b => b.Subscribe(It.IsAny<string>(), It.IsAny<Action<T>>()))
                .Callback((string s, Action<T> fn) => subsFn = fn);
            _service.RunAndSubscribe().ToList(); // Force evaluation to pickup subs fns
            Assert.IsNotNull(subsFn, "Could not get Subscribed function when starting service");
            return subsFn;
        }

        private static string CreateProductTopicPath(long productId)
        {
            var createProductPath = string.Join("/", "AOM", "MarketTicker", "Products", productId);
            return createProductPath;
        }

        private static string CreateOrganisationOrdersTopicPathForOrder(long productId, long organisationId,
            long orderId)
        {
            var allOrdersTopicPathForSpecificOrder = string.Join("/", CreateProductTopicPath(productId), organisationId,
                orderId);
            return allOrdersTopicPathForSpecificOrder;
        }

        #endregion
    }
}