﻿using System;
using System.Collections.Generic;

using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.ControlClient.Routers;
using AOM.Transport.Events;
using AOM.Transport.Events.Users;
using AOM.Transport.Events.Orders;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Utils.Logging;
using Utils.Javascript.Extension;
using Argus.Transport.Infrastructure;

using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using log4net.Core;
using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Data.JSON;

namespace AOM.ControlClient.Tests.Routers
{
    [TestFixture]
    public class OrderRouterServiceTests
    {
        private OrderRouterService _orderRouterService;
        private Mock<IOrdersDiffusionTopicManager> _mockOrdersDiffusionTopicManager;
        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;
        private Mock<IDiffusionOrderTopicTreeBuilder> _mockDiffusionOrderTopicTreeBuilder;
        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockOrdersDiffusionTopicManager = new Mock<IOrdersDiffusionTopicManager>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();
            _mockDiffusionOrderTopicTreeBuilder = new Mock<IDiffusionOrderTopicTreeBuilder>();

            _mockBus.Setup(b => b.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(It.IsAny<LogUserErrorRequestRpc>()))
                .Returns(new LogUserErrorResponseRpc {UserErrorMessage = "Fake User Error Message"});

            _orderRouterService = new OrderRouterService(
                _mockBus.Object,
                _mockOrdersDiffusionTopicManager.Object,
                _mockClientSessionMessageHandler.Object,
                _mockDiffusionOrderTopicTreeBuilder.Object);
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_orderRouterService);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockOrdersDiffusionTopicManager, _orderRouterService);
        }

        [Test]
        public void ProductsDiffusionTopicManagerGoingActiveThenDisconnectingShouldDisposeSubscriptions()
        {
            StartConsumer();
            Assert.That(_orderRouterService.SubscriberCount, Is.GreaterThan(0));
            _orderRouterService.Disconnect();
            Assert.That(_orderRouterService.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void ProductsDiffusionTopicManagerGoingActiveThenStandbyShouldDisposeSubscriptions()
        {
            StartConsumer();
            Assert.That(_orderRouterService.SubscriberCount, Is.GreaterThan(0));
            _mockOrdersDiffusionTopicManager.GoStandby();
            Assert.That(_orderRouterService.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void PurgeOrdersInDiffusionShouldTellTopicManagerToPurgeOrdersForEachRequestedProduct()
        {
            var subs = StartConsumer();

            var onPurgeOrdersInDiffusion = subs.PurgeOrdersInDiffusion;
            var productIds = new List<long> {1, 3};
            onPurgeOrdersInDiffusion(new PurgeOrdersInDiffusionRequest {ProductsToPurgeOrdersFor = productIds});

            _mockDiffusionOrderTopicTreeBuilder.Verify(b => b.PurgeOrdersForProduct(It.IsAny<long>()),
                Times.Exactly(productIds.Count));
            productIds.ForEach(
                id => _mockDiffusionOrderTopicTreeBuilder.Verify(b => b.PurgeOrdersForProduct(id), Times.Once));
        }

        [Test]
        public void ShouldPublishAomOrderRequestWithCorrectDetailsOnAomOrderMessageResponse()
        {
            var fakeOrder = new Order
            {
                Id = 123
            };
            var response = new AomOrderMessageResponse
            {
                Message = new Message
                {
                    MessageType = MessageType.Order,
                    MessageAction = MessageAction.Hold,
                    ClientSessionInfo = new ClientSessionInfo {SessionId = "fakeSessionId"}
                },
                ObjectJson = fakeOrder.ToJsonCamelCase()
            };
            var subs = StartConsumer();
            subs.AomOrderMessageResponseAction(response);
            _mockBus.Verify(b =>
                b.Publish(It.Is<AomOrderRequest>(r =>
                    r.Order.Id == fakeOrder.Id &&
                    r.MessageAction == response.Message.MessageAction &&
                    r.ClientSessionInfo.SessionId == response.Message.ClientSessionInfo.SessionId)), Times.Once);
        }

        // Note that the AomOrderRefreshResponse and AomOrderResponse handlers both call the same
        // method ForwardOrderResponse so all test here just use AomOrderResponse messages.

        [Test]
        [TestCase(ExecutionMode.Internal)]
        [TestCase(ExecutionMode.External)]
        public void ShouldDeleteTopicsForOrderWhenReceivingADeleteOrderResponse(ExecutionMode orderType)
        {
            var subs = StartConsumer();
            var response = CreateOrderResponse(OrderStatus.Killed, orderType: orderType);
            AssertOrderTopicsDeleted(subs, response);
        }

        [Test]
        [TestCase(ExecutionMode.Internal)]
        [TestCase(ExecutionMode.External)]
        public void ShouldDeleteTopicsForOrderWhenReceivingAnExecuteOrderResponse(ExecutionMode orderType)
        {
            var subs = StartConsumer();
            var response = CreateOrderResponse(OrderStatus.Executed, orderType: orderType);
            AssertOrderTopicsDeleted(subs, response);
        }

        [Test]
        [TestCase(ExecutionMode.Internal)]
        [TestCase(ExecutionMode.External)]
        public void ShouldDeleteTopicsForOrderWhenReceivingAHeldOrderResponse(ExecutionMode orderType)
        {
            var subs = StartConsumer();
            var response = CreateOrderResponse(OrderStatus.Held, orderType: orderType);
            AssertOrderTopicsDeleted(subs, response);
        }

        [Test]
        [TestCase(ExecutionMode.Internal, true, Description = "Internal order with broker")]
        [TestCase(ExecutionMode.External, true, Description = "External order with broker")]
        [TestCase(ExecutionMode.Internal, false, Description = "Internal order without broker")]
        [TestCase(ExecutionMode.External, false, Description = "External order without broker")]
        public void ShouldCreateHeldTopicWhenReceivingAHeldOrderResponse(ExecutionMode orderType, bool hasBroker)
        {
            var subs = StartConsumer();
            var response = CreateOrderResponse(OrderStatus.Held, orderType: orderType,
                brokerOrgId: hasBroker ? 789 : (long?) null);


            if (hasBroker)
            {
                Assert.True(response.Order.BrokerOrganisationId.HasValue);
            }
        }

        [Test]
        [TestCase(ExecutionMode.Internal)]
        [TestCase(ExecutionMode.External)]
        public void ShouldDeleteHeldOrderTopicWhenReceivingAnActiveOrderResponse(ExecutionMode orderType)
        {
            var subs = StartConsumer();
            var response = CreateOrderResponse(OrderStatus.Active, orderType: orderType);
            AssertOrderTopicsDeleted(subs, response, checkNotHeldTopic: false);
        }

        [Test]
        [TestCase(ExecutionMode.Internal, true, Description = "Internal order with broker")]
        [TestCase(ExecutionMode.External, true, Description = "External order with broker")]
        [TestCase(ExecutionMode.Internal, false, Description = "Internal order without broker")]
        [TestCase(ExecutionMode.External, false, Description = "External order without broker")]
        public void ShouldCreateNotHeldTopicWhenReceivingAnActiveOrderResponse(ExecutionMode orderType, bool hasBroker)
        {
            // The not-held order topic is created the same irrespective of whether there's a
            // broker on the order or not, so our expected path is not changed by the hasBroker flag
            var subs = StartConsumer();
            var response = CreateOrderResponse(OrderStatus.Active, orderType: orderType,
                brokerOrgId: hasBroker ? 789 : (long?) null);

            List<string> expectedPaths = new List<string>();
            AssertOrderTopicsCreated(subs, response, expectedPaths);
        }

        private void AssertOrderTopicsCreated(SubscribedActions subs, AomOrderResponse response,
            List<string> expectedPaths)
        {
            var detailsCallbacks = new Dictionary<string, TopicDetailsCallback>();
            var addCallbacks = new Dictionary<string, TopicControlAddCallback>();

            var mockTopicUpdater = new Mock<ISimpleTopicSourceHandler>();
            _mockOrdersDiffusionTopicManager.Setup(m => m.GetUpdater()).Returns(mockTopicUpdater.Object);

            _mockOrdersDiffusionTopicManager.Setup(m =>
                        m.CreateTopic(It.IsAny<string>(), It.IsAny<TopicControlAddCallback>()))
                .Callback((string p, TopicControlAddCallback c) => addCallbacks[p] = c);

            subs.AomOrderResponseAction(response);

            Assert.That(detailsCallbacks.Count, Is.EqualTo(expectedPaths.Count));
            Assert.That(addCallbacks.Count, Is.EqualTo(0));

            // Now the TopicDoesNotExist events have been triggered check that the topics were created
            _mockOrdersDiffusionTopicManager.Verify(m =>
                        m.CreateTopic(It.IsAny<string>(), It.IsAny<TopicControlAddCallback>()),
                Times.Exactly(expectedPaths.Count));

            foreach (var path in expectedPaths)
            {
                var pathCopy = path;
                _mockOrdersDiffusionTopicManager.Verify(m =>
                        m.CreateTopic(pathCopy, It.IsAny<TopicControlAddCallback>()), Times.Once);
            }

            // This should also have registered the addCallbacks
            Assert.That(addCallbacks.Count, Is.EqualTo(expectedPaths.Count));

            // Trigger the TopicAdded callbacks, this should result in the order response JSON being pushed to the topic
            foreach (var valuePair in addCallbacks)
            {
                valuePair.Value.OnTopicAdded(valuePair.Key);
            }

            var responseJson = response.ToJsonCamelCase();
            foreach (var path in expectedPaths)
            {
                var pathCopy = path;
                mockTopicUpdater.Verify(u =>
                        u.PushToTopic(pathCopy, It.IsAny<IJSON>(), true, It.IsAny<ITopicUpdaterUpdateCallback>()), Times.Once);
            }
        }

        private void AssertOrderTopicsDeleted(SubscribedActions subs, AomOrderResponse response,
            bool checkHeldTopic = true, bool checkNotHeldTopic = true)
        {
        }

        private void AssertTopicsDeleted(IEnumerable<string> paths)
        {
            foreach (var path in paths)
            {
                var topicPathToDelete = path + "/";
                _mockOrdersDiffusionTopicManager.Verify(
                    m => m.DeleteTopic(topicPathToDelete, ">", It.IsAny<ITopicControlRemoveCallback>()),
                    Times.Once);
            }
        }

        // Edge case handling tests
        [Test]
        public void ShouldSendErrorOnAomOrderResponseWithMessageTypeError()
        {
            var response = new AomOrderResponse
            {
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo() { UserId = 99, SessionId = "1234"},
                    MessageType = MessageType.Error,
                    MessageBody = "fake"
                }
            };
            var subs = StartConsumer();
            subs.AomOrderResponseAction(response);
            _mockClientSessionMessageHandler.Verify(h =>
                h.SendMessage(It.IsAny<ClientSessionInfo>(),
                    It.Is<string>(m => m.Contains(MessageType.Error.ToString()))));
        }

        private void AssertLogUserErrorRequestWasPublished(string errorTextContains)
        {
            _mockBus.Verify(b =>
                b.Request<LogUserErrorRequestRpc, LogUserErrorResponseRpc>(
                    It.Is<LogUserErrorRequestRpc>(req => req.ErrorText.Contains(errorTextContains))));
        }

        [Test]
        public void ShouldLogMessageAndNotPublishToTopicManagerIfNotActive()
        {
            var subs = StartConsumer();
            _mockOrdersDiffusionTopicManager.GoStandby();
            var response = CreateOrderResponse();

            using (var appender = new MemoryAppenderForTests())
            {
                subs.AomOrderResponseAction(response);
                appender.AssertALogMessageContains(new[] {Level.Debug, Level.Info}, "TopicManager is not active");
                _mockOrdersDiffusionTopicManager.Verify(
                    m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                    Times.Never);
                _mockOrdersDiffusionTopicManager.Verify(
                    m => m.CreateTopic(It.IsAny<string>(), It.IsAny<TopicControlAddCallback>()), Times.Never);
            }
        }

        [Test]
        public void ShouldLogAnErrorIfTheOrderResponseIsNull()
        {
            var subs = StartConsumer();
            using (var appender = new MemoryAppenderForTests())
            {
                subs.AomOrderResponseAction(null);
                appender.AssertALogMessageContains(Level.Error,
                    "Invalid response in OrderRouterService.ForwardOrderResponse");
            }
        }

        [Test]
        public void ShouldLogAnErrorIfTheOrderResponseMessageIsNull()
        {
            var subs = StartConsumer();
            var response = new AomOrderResponse
            {
                Message = null
            };
            using (var appender = new MemoryAppenderForTests())
            {
                subs.AomOrderResponseAction(response);
                appender.AssertALogMessageContains(Level.Error,
                    "Invalid response in OrderRouterService.ForwardOrderResponse");
            }
        }

        [Test]
        [TestCase(OrderStatus.None)]
        [TestCase(OrderStatus.VoidAfterExecuted)]
        public void ShouldIgnoreOrdersWithAStatusThatDoNotRequireProcessing(OrderStatus status)
        {
            var subs = StartConsumer();
            var response = CreateOrderResponse(status);
            using (var appender = new MemoryAppenderForTests())
            {
                subs.AomOrderResponseAction(response);
                appender.AssertALogMessageContains(Level.Debug, "Ignoring OrderResponse with order status of " + status);
            }
        }


        private static AomOrderResponse CreateOrderResponse(OrderStatus status = OrderStatus.Active, long id = 123,
            long productId = 234, long principalOrgId = 345,
            long? brokerOrgId = null, ExecutionMode orderType = ExecutionMode.Internal,
            MessageAction action = MessageAction.Create)
        {
            return new AomOrderResponse
            {
                Order = new Order
                {
                    Id = id,
                    ProductId = productId,
                    PrincipalOrganisationId = principalOrgId,
                    BrokerOrganisationId = brokerOrgId,
                    ExecutionMode = orderType,
                    OrderStatus = status
                },
                Message = new Message
                {
                    MessageType = MessageType.Order,
                    MessageAction = action,
                    MessageBody = "fake body"
                }
            };
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            SetupGetSubscribedAction<PurgeOrdersInDiffusionRequest>(a => subs.PurgeOrdersInDiffusion = a);
            SetupGetSubscribedAction<AomOrderResponse>(a => subs.AomOrderResponseAction = a);
            SetupGetSubscribedAction<AomOrderRefreshResponse>(a => subs.AomOrderRefreshResponseAction = a);
            SetupGetSubscribedAction<AomOrderMessageResponse>(a => subs.AomOrderMessageResponseAction = a);

            _mockOrdersDiffusionTopicManager.GoActive();
            return subs;
        }

        private void SetupGetSubscribedAction<T>(Action<Action<T>> onActionReceived) where T : class
        {
            _mockBus.Setup(b => b.Subscribe("OrderRouterService", It.IsAny<Action<T>>()))
                .Callback((string s, Action<T> subsAction) => onActionReceived(subsAction));
        }

        public class SubscribedActions
        {
            public Action<PurgeOrdersInDiffusionRequest> PurgeOrdersInDiffusion { get; set; }
            public Action<AomOrderResponse> AomOrderResponseAction { get; set; }
            public Action<AomOrderRefreshResponse> AomOrderRefreshResponseAction { get; set; }
            public Action<AomOrderMessageResponse> AomOrderMessageResponseAction { get; set; }
        }
    }
}