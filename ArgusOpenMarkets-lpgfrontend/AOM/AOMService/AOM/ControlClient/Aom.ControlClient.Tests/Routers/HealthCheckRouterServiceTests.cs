﻿using AOM.ControlClient.Routers;
using AOM.App.Domain.Dates;
using AOM.Transport.Events.HealthCheck;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests.Routers
{


    [TestFixture]
    public class HealthCheckRouterServiceTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IHealthCheckDiffusionTopicManager> _mockHealthCheckDiffusionTopicManager;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private HealthCheckRouterService _service;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockHealthCheckDiffusionTopicManager = new Mock<IHealthCheckDiffusionTopicManager>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();

            _service = new HealthCheckRouterService(
                _mockBus.Object,
                _mockHealthCheckDiffusionTopicManager.Object,
                _mockDateTimeProvider.Object);
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_service);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockHealthCheckDiffusionTopicManager, _service);
        }

        [Test]
        public void HealthCheckDiffusionTopicManagerGoingActiveThenDisconnectingShouldDisposeSubscriptions()
        {
            StartConsumer(_mockHealthCheckDiffusionTopicManager);

            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _service.Disconnect();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void OnProcessorHealthCheckShouldPublishAProcessorHealthCheckStatusOntoTheBus()
        {
            var subs = StartConsumer(_mockHealthCheckDiffusionTopicManager);

            var onProcessorHealthCheck = subs.ProcessorHealthCheck;
            onProcessorHealthCheck(new ProcessorHealthCheck());

            _mockBus.Verify(b => b.Publish(It.IsAny<ProcessorHealthCheckStatus>()), Times.Once);
        }

        [Test]
        public void ShouldDisposeSubscribersWhenTopicManagerGoesFromActiveToStandby()
        {
            StartConsumer(_mockHealthCheckDiffusionTopicManager);
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _mockHealthCheckDiffusionTopicManager.GoStandby();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        private SubscribedActions StartConsumer(Mock<IHealthCheckDiffusionTopicManager> healthCheckDiffusionTopicManager)
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("HealthCheckRouterService", It.IsAny<Action<ProcessorHealthCheckStatus>>()))
                .Callback<string, Action<ProcessorHealthCheckStatus>>((s, a) => subs.ProcessorHealthCheckStatus = a);

            _mockBus.Setup(b => b.Subscribe("HealthCheckRouterService", It.IsAny<Action<ProcessorHealthCheck>>()))
                .Callback<string, Action<ProcessorHealthCheck>>((s, a) => subs.ProcessorHealthCheck = a);

            healthCheckDiffusionTopicManager.GoActive();

            return subs;
        }

        public class SubscribedActions
        {
            public Action<ProcessorHealthCheckStatus> ProcessorHealthCheckStatus { get; set; }
            public Action<ProcessorHealthCheck> ProcessorHealthCheck { get; set; }
        }
    }
}