﻿using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.ControlClient.Routers;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests.Routers
{
    [TestFixture]
    public class MarketInfoRouterServiceTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IMarketTickerDiffusionTopicManager> _mockTopicManager;
        private Mock<IClientSessionMessageHandler> _mockMessageHandler;
        private MarketInfoRouterService _service;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockTopicManager = new Mock<IMarketTickerDiffusionTopicManager>();
            _mockMessageHandler = new Mock<IClientSessionMessageHandler>();
            var mockErrorService = new Mock<IErrorService>();
            _service = new MarketInfoRouterService(_mockBus.Object, _mockTopicManager.Object, _mockMessageHandler.Object,
                mockErrorService.Object);
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_service);
        }

//        [Test]
//        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
//        {
//            // See AOMK-99
//            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockTopicManager, _service);
//        }

        [Test]
        public void AuthenticationDiffusionTopicManagerGoingActiveThenDisconnectingShouldDisposeSubscriptions()
        {
            _mockTopicManager.GoActive();
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _service.Disconnect();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void AuthenticationDiffusionTopicManagerGoingActiveThenStandbyShouldDisposeSubscriptions()
        {
            _mockTopicManager.GoActive();
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _mockTopicManager.GoStandby();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void OnAomMarketInfoResponseMessageReceivedThatIsNotInfoShouldSendAnErrorToTheClient()
        {
            var subs = StartConsumer();
            _mockTopicManager.GoActive();

            var onAomMarketInfoResponse = subs.AomMarketInfoResponse;

            var message = new Message {MessageAction = MessageAction.Reinstate};
            var marketInfo = new MarketInfo {Id = 1};
            var userContactDetails = new UserContactDetails {Id = 100};
            onAomMarketInfoResponse(
                new AomMarketInfoResponse
                {
                    MarketInfo = marketInfo,
                    Message = message,
                    UserContactDetails = userContactDetails
                });

            _mockMessageHandler.Verify(
                m => m.SendMessage(It.IsAny<ClientSessionInfo>(), It.IsAny<string>()),
                Times.Exactly(1));
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("MarketInfoRouterService", It.IsAny<Action<AomMarketInfoResponse>>()))
                .Callback<string, Action<AomMarketInfoResponse>>((s, a) => subs.AomMarketInfoResponse = a);

            return subs;
        }

        public class SubscribedActions
        {
            public Action<AomMarketInfoResponse> AomMarketInfoResponse { get; set; }
        }
    }
}