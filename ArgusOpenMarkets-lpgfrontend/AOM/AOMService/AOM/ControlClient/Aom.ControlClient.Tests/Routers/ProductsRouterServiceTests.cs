﻿using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests.Routers
{
    using AOM.ControlClient.Interfaces;
    using AOM.ControlClient.Routers;
    using AOM.Transport.Events.Products;
    using Argus.Transport.Infrastructure;
    using Moq;
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class ProductsRouterServiceTests
    {
        private ProductsRouterService _productsRouterService;

        private Mock<IProductsDiffusionTopicManager> _mockProductsDiffusionTopicManager;

        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;

        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockProductsDiffusionTopicManager = new Mock<IProductsDiffusionTopicManager>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();

            _productsRouterService = new ProductsRouterService(
                _mockBus.Object,
                _mockProductsDiffusionTopicManager.Object,
                _mockClientSessionMessageHandler.Object);
        }

        [Test]
        public void ShouldNotSubscribeToBusOnRun()
        {
            // See AOMK-99
            TestHelpers.ShouldNotSubscribeToBusOnRun(_productsRouterService);
        }

        [Test]
        public void ShouldSubscribeToAomMarketInfoResponseWhenTopicManagerGoesActive()
        {
            // See AOMK-99
            TestHelpers.ShouldSubscribeWhenTopicManagerGoesActive(_mockProductsDiffusionTopicManager,
                                                                  _productsRouterService);
        }

        [Test]
        public void ProductsRouterServiceTopicManagerGoingActiveShouldSubscribeToAomMarketStatusChange()
        {
            StartConsumer(_mockProductsDiffusionTopicManager);
            _mockBus.Verify(m => m.Subscribe("ProductRouterService", It.IsAny<Action<AomMarketStatusChange>>()), Times.Exactly(1));
        }

        [Test]
        public void ProductsRouterServiceTopicManagerGoingActiveThenDisconnectingShouldDisposeSubscriptions()
        {
            StartConsumer(_mockProductsDiffusionTopicManager);

            _mockBus.Verify(m => m.Subscribe("ProductRouterService", It.IsAny<Action<AomMarketStatusChange>>()), Times.Exactly(1));

            Assert.That(_productsRouterService.SubscriberCount, Is.EqualTo(1));
            _productsRouterService.Disconnect();
            Assert.That(_productsRouterService.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void ProductsRouterServiceTopicManagerGoingActiveThenStandbyShouldDisposeSubscriptions()
        {
            StartConsumer(_mockProductsDiffusionTopicManager);
            Assert.That(_productsRouterService.SubscriberCount, Is.EqualTo(1));
            _mockProductsDiffusionTopicManager.GoStandby();
            Assert.That(_productsRouterService.SubscriberCount, Is.EqualTo(0));
        }

//        [Test]
//        public void ProductsRouterServiceReceivingAomMarketStatucChangeShouldSendDataToTheRelevantProductTopic()
//        {
//            _mockProductsDiffusionTopicManager = new Mock<IProductsDiffusionTopicManager>();
//
//            var subs = StartConsumer(_mockProductsDiffusionTopicManager);
//            _mockProductsDiffusionTopicManager.Raise(e => e.Active += (sender, args) =>
//            {
//                
//            });
//
//            var aomMarketStatusChangeEvent = new AomMarketStatusChange
//                                             {
//                                                 Message = new Message(),
//                                                 Product =
//                                                     new Product
//                                                     {
//                                                         ProductId = 1,
//                                                         Name = "PROD",
//                                                         Status = MarketStatus.Open
//                                                     }
//                                             };
//
//            var onAomMarketStatusChange = subs.AomMarketStatusChange;
//            onAomMarketStatusChange(aomMarketStatusChangeEvent);
//
//            _mockProductsDiffusionTopicManager.Verify(
//                m => m.SendDataToTopic("AOM/Products/1/MarketStatus", It.IsAny<string>(), null));
//        }

        private SubscribedActions StartConsumer(
            Mock<IProductsDiffusionTopicManager> mockProductsDiffusionTopicManager)
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("ProductRouterService", It.IsAny<Action<AomMarketStatusChange>>()))
                .Callback<string, Action<AomMarketStatusChange>>((s, a) => subs.AomMarketStatusChange = a);

            mockProductsDiffusionTopicManager.GoActive();
            return subs;
        }

        public class SubscribedActions
        {
            public Action<AomMarketStatusChange> AomMarketStatusChange { get; set; }
        }
    }
}