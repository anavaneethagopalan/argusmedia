﻿using AOM.ControlClient.Interfaces;
using AOM.Transport.Events.System;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using System;
using System.Collections.Generic;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests.Routers
{


    [TestFixture]
    internal class AdminRouterServiceTests
    {
        private Mock<IBus> _mockBus;

        private AdminRouterService _service;

        private Mock<IAdminDiffusionTopicManager> _mockAdminTopicManager;

        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;

        private List<Tuple<string, string>> _publishedMessages;

        [SetUp]
        public void Setup()
        {
            _publishedMessages = new List<Tuple<string, string>>();
            _mockBus = new Mock<IBus>();
            _mockAdminTopicManager = new Mock<IAdminDiffusionTopicManager>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();

            _service = new AdminRouterService(
                _mockBus.Object, 
                _mockAdminTopicManager.Object,
                _mockClientSessionMessageHandler.Object);

            _mockAdminTopicManager.Setup(
                m => m.SendDataToTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicUpdaterUpdateCallback>()))
                .Callback<string, string, ITopicUpdaterUpdateCallback>(
                    (topic, content, callback) => _publishedMessages.Add(Tuple.Create(topic, content)));
        }

        [Test]
        public void PublishDeleteDiffusionTopicWithTheTopicJustAomShouldNotDeleteTheTopicFromDiffusion()
        {
            var subs = StartConsumer();
            var onDeleteDiffusionTopicRequest = subs.DeleteDiffusionTopicRequest;

            var message = new DeleteDiffusionTopicRequest { LoggedOnUserId = 1, TopicToDelete = "AOM" };

            onDeleteDiffusionTopicRequest(message);

            _mockAdminTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(0));
        }

        [Test]
        public void PublishDeleteDiffusionTopicWithTheTopicBlankShouldNotDeleteTheTopicFromDiffusion()
        {
            var subs = StartConsumer();
            var onDeleteDiffusionTopicRequest = subs.DeleteDiffusionTopicRequest;

            var message = new DeleteDiffusionTopicRequest { LoggedOnUserId = 1, TopicToDelete = "" };

            onDeleteDiffusionTopicRequest(message);

            _mockAdminTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(0));
        }

        [Test]
        public void PublishDeleteDiffusionTopicWithValidTopicShouldNotDeleteTheTopicFromDiffusion()
        {
            var subs = StartConsumer();
            var onDeleteDiffusionTopicRequest = subs.DeleteDiffusionTopicRequest;

            var message = new DeleteDiffusionTopicRequest { LoggedOnUserId = 1, TopicToDelete = "Aom/News/ContentStreams/1/232435" };

            onDeleteDiffusionTopicRequest(message);

            _mockAdminTopicManager.Verify(
                m => m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()),
                Times.Exactly(1));
        }

        [Test]
        public void AdminDiffusionTopicManagerGoingActiveThenCallingDisconnectShouldRemoveAllSubscribers()
        {
            StartConsumer();

            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _service.Disconnect();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void ShouldDisposeConsumersWhenTopicManagerGoesFromActiveToStandby()
        {
            StartConsumer();
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _mockAdminTopicManager.GoStandby();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        private class SubscribedActions
        {
            public Action<DeleteDiffusionTopicRequest> DeleteDiffusionTopicRequest { get; set; }
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("AdminRouterService", It.IsAny<Action<DeleteDiffusionTopicRequest>>()))
                .Callback<string, Action<DeleteDiffusionTopicRequest>>((s, a) => subs.DeleteDiffusionTopicRequest = a);

            _mockAdminTopicManager.GoActive();
            return subs;
        }
    }
}