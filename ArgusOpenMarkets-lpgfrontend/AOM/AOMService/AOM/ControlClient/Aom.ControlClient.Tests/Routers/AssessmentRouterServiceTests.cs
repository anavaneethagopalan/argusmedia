﻿using AOM.App.Domain.Entities;
using AOM.ControlClient.Interfaces;
using AOM.ControlClient.Routers;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;

using Argus.Transport.Infrastructure;

using Moq;

using NUnit.Framework;

using System;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests.Routers
{
    [TestFixture]
    public class AssessmentRouterServiceTests
    {
        private Mock<IBus> _mockBus;

        private Mock<IProductsDiffusionTopicManager> _mockProductsDiffusionTopicManager;

        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;

        private AssessmentRouterService _service;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockProductsDiffusionTopicManager = new Mock<IProductsDiffusionTopicManager>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();

            _service = new AssessmentRouterService(
                _mockBus.Object,
                _mockProductsDiffusionTopicManager.Object,
                _mockClientSessionMessageHandler.Object);
        }


        [Test]
        public void ProductsDiffusionTopicManagerGoingActiveThenDisconnectingShouldDisposeSubscriptions()
        {
            StartConsumer(_mockProductsDiffusionTopicManager);
            Assert.That(_service.SubscriberCount, Is.GreaterThan(0));
            _service.Disconnect();
            Assert.That(_service.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        public void AomNewAssessmentResponseShouldSendDataToTheRelevantTopic()
        {
            var subs = StartConsumer(_mockProductsDiffusionTopicManager);

            var onNewAomAssessmesntResponse = subs.AomNewAssessmentResponse;
            onNewAomAssessmesntResponse(
                new AomNewAssessmentResponse
                {
                    Assessment = new CombinedAssessment(),
                    Message = new Message {MessageType = MessageType.Assessment},
                    ProductId = 1
                });

            _mockProductsDiffusionTopicManager.Verify(
                m => m.SendDataToTopic("AOM/Products/1/Assessment", It.IsAny<string>(), null));
        }

        private SubscribedActions StartConsumer(Mock<IProductsDiffusionTopicManager> topicManager)
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("AssessmentRouterService", It.IsAny<Action<AomNewAssessmentResponse>>()))
                .Callback<string, Action<AomNewAssessmentResponse>>((s, a) => subs.AomNewAssessmentResponse = a);

            topicManager.Raise(x => x.Active += null, new EventArgs());

            return subs;
        }

        public class SubscribedActions
        {
            public Action<AomNewAssessmentResponse> AomNewAssessmentResponse { get; set; }
        }
    }
}