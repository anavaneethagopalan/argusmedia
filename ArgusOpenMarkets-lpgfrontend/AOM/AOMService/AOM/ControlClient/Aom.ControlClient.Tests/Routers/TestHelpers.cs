﻿using AOM.ControlClient.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using AOM.App.Domain.Interfaces;
using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests.Routers
{
    public static class TestHelpers
    {
        public static void ShouldNotSubscribeToBusOnRun<TService>(TService service)
            where TService : IRunnable, IRouterServiceBase
        {
            service.Run();
            Assert.That(service.SubscriberCount, Is.EqualTo(0));
        }

        public static void ShouldSubscribeWhenTopicManagerGoesActive<TTopicManager, TService>(
            Mock<TTopicManager> mockTopicManager,
            TService service) 
            where TTopicManager : class, IAomDiffusionTopicManager
            where TService : IRunnable, IRouterServiceBase
        {
            Assert.That(service.SubscriberCount, Is.EqualTo(0));
            mockTopicManager.GoActive();
            Assert.That(service.SubscriberCount, Is.GreaterThan(0));
        }

        public static void ShouldClearSubscribersWhenTopicManagerGoesActiveToStandby<TTopicManager, TService>(
            Mock<TTopicManager> mockTopicManager,
            TService service) 
            where TTopicManager : class, IAomDiffusionTopicManager
            where TService : IRunnable, IRouterServiceBase
        {
            ShouldSubscribeWhenTopicManagerGoesActive(mockTopicManager, service);
            mockTopicManager.GoStandby();
            Assert.That(service.SubscriberCount, Is.EqualTo(0));
        }
    }

    public static class MockTopicManagerExtensions
    {
        public static Mock<TTopicManager> GoActive<TTopicManager>(this Mock<TTopicManager> mockTopicManager)
            where TTopicManager : class, IAomDiffusionTopicManager
        {
            mockTopicManager.Raise(t => t.Active += null, EventArgs.Empty);
            return mockTopicManager;
        }

        public static Mock<TTopicManager> GoStandby<TTopicManager>(this Mock<TTopicManager> mockTopicManager)
            where TTopicManager : class, IAomDiffusionTopicManager
        {
            mockTopicManager.Raise(t => t.StandBy += null, EventArgs.Empty);
            return mockTopicManager;
        }        
    }
}
