﻿using AOM.ControlClient.Interfaces;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Session;
using System.Collections.Generic;
using AOM.TopicManager;
using AOMDiffusion.Common.Interfaces;
using Topshelf;

namespace AOM.ControlClient.Tests.v2
{
    [TestFixture]
    internal class ControlClientControllerTests
    {
        private ControlClientController _controlClientController;

        private Mock<IBus> _mockBus;

        private Mock<IMessageDistributionService> _mockMessageDistributionService;

        private Mock<IDiffusionSessionConnection> _mockDiffusionSessionConnection;

        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;

        private Mock<IDiffusionSessionManager> _mockDiffusionSessionManager;

        private Mock<IAuthenticationDiffusionTopicManager> _mockAuthenticationDiffusionTopicManager;

        private Mock<IProductsDiffusionTopicManager> _mockProductDiffusionTopicManager;

        private Mock<IOrganistionDiffusionTopicManager> _mockOrganisationDiffusionTopicManager;

        private Mock<IHealthCheckDiffusionTopicManager> _mockHealthCheckDiffusionTopicManager;

        private Mock<IMarketTickerDiffusionTopicManager> _mockMarketTickerDiffusionTopicManager;

        private Mock<INewsDiffusionTopicManager> _mockNewsDiffusionTopicManager;

        private Mock<HostControl> _mockHostControl;

        private Mock<IAomTopicManager> _mockAomTopicManager;

        private Mock<IAdminDiffusionTopicManager> _mockAdminDiffusionTopicManager;

        private Mock<IOrdersDiffusionTopicManager> _mockOrdersDiffusionTopicManager;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockMessageDistributionService = new Mock<IMessageDistributionService>();
            _mockDiffusionSessionConnection = new Mock<IDiffusionSessionConnection>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();
            _mockDiffusionSessionManager = new Mock<IDiffusionSessionManager>();
            _mockAuthenticationDiffusionTopicManager = new Mock<IAuthenticationDiffusionTopicManager>();
            _mockProductDiffusionTopicManager = new Mock<IProductsDiffusionTopicManager>();
            _mockOrganisationDiffusionTopicManager = new Mock<IOrganistionDiffusionTopicManager>();
            _mockHealthCheckDiffusionTopicManager = new Mock<IHealthCheckDiffusionTopicManager>();
            _mockMarketTickerDiffusionTopicManager = new Mock<IMarketTickerDiffusionTopicManager>();
            _mockNewsDiffusionTopicManager = new Mock<INewsDiffusionTopicManager>();
            _mockHostControl = new Mock<HostControl>();
            _mockAdminDiffusionTopicManager = new Mock<IAdminDiffusionTopicManager>();
            _mockOrdersDiffusionTopicManager = new Mock<IOrdersDiffusionTopicManager>();
            _mockAomTopicManager = new Mock<IAomTopicManager>();

            _controlClientController = new ControlClientController(
                _mockBus.Object,
                _mockMessageDistributionService.Object,
                _mockDiffusionSessionConnection.Object,
                _mockClientSessionMessageHandler.Object,
                _mockDiffusionSessionManager.Object,
                _mockAuthenticationDiffusionTopicManager.Object,
                _mockProductDiffusionTopicManager.Object,
                _mockOrganisationDiffusionTopicManager.Object,
                _mockHealthCheckDiffusionTopicManager.Object,
                _mockMarketTickerDiffusionTopicManager.Object,
                _mockNewsDiffusionTopicManager.Object,
                _mockAdminDiffusionTopicManager.Object,
                _mockOrdersDiffusionTopicManager.Object,
                _mockAomTopicManager.Object);
        }

        [Test]
        public void CallingStartShouldStartDiffusionSessionConnectionAndReturnTrue()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockDiffusionSessionConnection.Verify(m => m.ConnectAndStartSession(), Times.Once());
        }

        [Test]
        public void CallingStartShouldRetryToConnectToDiffusionIfFirstConnectFails()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession())
                .Returns(new Queue<bool>(new[] { false, true }).Dequeue);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockDiffusionSessionConnection.Verify(m => m.ConnectAndStartSession(), Times.Exactly(2));
        }

        [Test]
        public void CallingStopShouldDisposeTheBus()
        {
            _controlClientController.Stop(_mockHostControl.Object);

            _mockBus.Verify(m => m.Dispose(), Times.Once);
        }

        [Test]
        public void CallingStartWhenSuccessfullyConnectingToDiffusionShouldRegisterAuthenticationTopicSourceManager()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockAuthenticationDiffusionTopicManager.Verify(
                m => m.AttachToSessionAndRegisterTopicSource(It.IsAny<ISession>()),
                Times.Once);
        }

        [Test]
        public void CallingStartWhenSuccessfullyConnectingToDiffusionShouldRegisterProductsTopicSourceManager()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockProductDiffusionTopicManager.Verify(
                m => m.AttachToSessionAndRegisterTopicSource(It.IsAny<ISession>()),
                Times.Once);
        }

        [Test]
        public void CallingStartWhenSuccessfullyConnectingToDiffusionShouldRegisterOrganisationsTopicSourceManager()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockOrganisationDiffusionTopicManager.Verify(
                m => m.AttachToSessionAndRegisterTopicSource(It.IsAny<ISession>()),
                Times.Once);
        }

        [Test]
        public void CallingStartWhenSuccessfullyConnectingToDiffusionShouldRegisterHealthChecksTopicSourceManager()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockHealthCheckDiffusionTopicManager.Verify(
                m => m.AttachToSessionAndRegisterTopicSource(It.IsAny<ISession>()),
                Times.Once);
        }

        [Test]
        public void CallingStartWhenSuccessfullyConnectingToDiffusionShouldRegisterMarketTickerTopicSourceManager()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockMarketTickerDiffusionTopicManager.Verify(
                m => m.AttachToSessionAndRegisterTopicSource(It.IsAny<ISession>()),
                Times.Once);
        }

        [Test]
        public void CallingStartWhenSuccessfullyConnectingToDiffusionShouldRegisteNewsTopicSourceManager()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockNewsDiffusionTopicManager.Verify(
                m => m.AttachToSessionAndRegisterTopicSource(It.IsAny<ISession>()),
                Times.Once);
        }

        [Test]
        public void AttemptingToStartAfterStopCalledShouldReturnFalse()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);
            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);
            _controlClientController.Stop(null);
            var startResult = _controlClientController.Start(null);
            Assert.IsFalse(startResult);
        }

        [Test]
        public void ShouldTryToReconnectIfSessionClosedByServer()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);
            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);

            _controlClientController.Start(null);
            _mockDiffusionSessionConnection.Verify(c => c.ConnectAndStartSession(), Times.Once);

            bool secondCall = false;
            _mockDiffusionSessionConnection.Setup(c => c.ConnectAndStartSession()).Callback(() => secondCall = true);

            var fakeArgs = new SessionListenerEventArgs(mockSession.Object,
                                            SessionState.CONNECTED_ACTIVE,
                                            SessionState.CLOSED_BY_SERVER);
            var fakeSender = new object();
            mockSession.Raise(s => s.StateChanged += null, fakeSender, fakeArgs);

            // TODO marksh Sleeping in a unit test is not a good idea ...
            // Unfortunately we currently need to with the test like this as the event
            // handler reconnects to Diffusion in a new Task (thread). We give the handler
            // thread up to 100ms to fire: long enough for even a pretty crappy machine (hopefully)
            // but not so long that the test becomes a PITA to run.
            const int maxRetries = 10;
            for (int retry = 0; retry < maxRetries && !secondCall; ++retry)
            {
                System.Threading.Thread.Sleep(10);
            }
            _mockDiffusionSessionConnection.Verify(c => c.ConnectAndStartSession(), Times.Exactly(2));
        }        
    }
}