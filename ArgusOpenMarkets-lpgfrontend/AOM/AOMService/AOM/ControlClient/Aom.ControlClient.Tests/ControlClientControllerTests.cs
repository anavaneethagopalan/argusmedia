﻿using System;
using AOM.ControlClient.Interfaces;
using AOM.TopicManager;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Session;
using Topshelf;

namespace AOM.ControlClient.Tests
{
    [TestFixture]
    public class ControlClientControllerTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IMessageDistributionService> _mockMessageDistributionService;

        private Mock<IDiffusionSessionConnection> _mockDiffusionSessionConnection;

        private Mock<IClientSessionMessageHandler> _mockClientSessionMessageHandler;

        private Mock<IDiffusionSessionManager> _mockDiffusionSessionManager;

        private Mock<IAuthenticationDiffusionTopicManager> _mockAuthenticationDiffusionTopicManager;
        private Mock<IProductsDiffusionTopicManager> _mockProductsDiffusionTopicManager;
        private Mock<IOrganistionDiffusionTopicManager> _mockOrgansOrganistionDiffusionTopicManager;
        private Mock<IHealthCheckDiffusionTopicManager> _mockHealthCheckTickerDiffusionTopicManager;
        private Mock<IMarketTickerDiffusionTopicManager> _mockMarketTickerDiffusionTopicManager;
        private Mock<INewsDiffusionTopicManager> _mockNewsDiffusionTopicManager;
        private Mock<IAdminDiffusionTopicManager> _mockAdminDiffusionTopicManager;
        private Mock<IOrdersDiffusionTopicManager> _mockOrdersDiffusionTopicManager;
        private Mock<IAomTopicManager> _mockAomTopicManager;
        private ControlClientController _controlClientController;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockMessageDistributionService = new Mock<IMessageDistributionService>();
            _mockDiffusionSessionConnection = new Mock<IDiffusionSessionConnection>();
            _mockClientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();
            _mockDiffusionSessionManager = new Mock<IDiffusionSessionManager>();
            _mockAuthenticationDiffusionTopicManager = new Mock<IAuthenticationDiffusionTopicManager>();
            _mockProductsDiffusionTopicManager = new Mock<IProductsDiffusionTopicManager>();
            _mockOrgansOrganistionDiffusionTopicManager = new Mock<IOrganistionDiffusionTopicManager>();
            _mockHealthCheckTickerDiffusionTopicManager = new Mock<IHealthCheckDiffusionTopicManager>();
            _mockMarketTickerDiffusionTopicManager = new Mock<IMarketTickerDiffusionTopicManager>();
            _mockNewsDiffusionTopicManager = new Mock<INewsDiffusionTopicManager>();
            _mockAdminDiffusionTopicManager = new Mock<IAdminDiffusionTopicManager>();
            _mockOrdersDiffusionTopicManager = new Mock<IOrdersDiffusionTopicManager>();
            _mockAomTopicManager = new Mock<IAomTopicManager>();

            _controlClientController = new ControlClientController(
                _mockBus.Object,
                _mockMessageDistributionService.Object,
                _mockDiffusionSessionConnection.Object,
                _mockClientSessionMessageHandler.Object,
                _mockDiffusionSessionManager.Object,
                _mockAuthenticationDiffusionTopicManager.Object,
                _mockProductsDiffusionTopicManager.Object,
                _mockOrgansOrganistionDiffusionTopicManager.Object,
                _mockHealthCheckTickerDiffusionTopicManager.Object,
                _mockMarketTickerDiffusionTopicManager.Object,
                _mockNewsDiffusionTopicManager.Object,
                _mockAdminDiffusionTopicManager.Object,
                _mockOrdersDiffusionTopicManager.Object,
                _mockAomTopicManager.Object);
        }

        [Test]
        public void CallingStartShouldStartDiffusionSessionConnectionAndReturnTrue()
        {
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            var startResult = _controlClientController.Start(null);

            Assert.That(startResult, Is.True);
            _mockDiffusionSessionConnection.Verify(m => m.ConnectAndStartSession(), Times.Once());
        }

        [Test]
        public void CallingStopShouldDisposeAomBusAndReturnTrue()
        {
            _mockBus.Setup(m => m.Dispose()).Verifiable();
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);
            var mockHostControl = new Mock<HostControl>();
            _controlClientController.Stop(mockHostControl.Object);

            var stopResult = _controlClientController.Stop(null);

            Assert.That(stopResult, Is.True);
            _mockBus.Verify(m => m.Dispose(), Times.AtLeastOnce());
        }

        [Test]
        public void ControlClientControllerWhenMessageDistributionServiceGoesActiveShouldCallRun()
        {
            _mockBus.Setup(m => m.Dispose()).Verifiable();
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            _controlClientController.Start(null);

            _mockMessageDistributionService.Raise(x => x.MessageHandlerActive += null, "", null);
            _mockMessageDistributionService.Verify(m => m.Run(), Times.Once);
        }

        [Test]
        public void
            ControlClientControllerConnectedWhenDiffusionSessionRaisesAnErrorNotificationShouldThrowDiffusionException()
        {
            _mockBus.Setup(m => m.Dispose()).Verifiable();
            var mockSession = new Mock<ISession>();
            _mockDiffusionSessionConnection.Setup(m => m.ConnectAndStartSession()).Returns(true);
            _mockDiffusionSessionConnection.SetupGet(e => e.Session).Returns(mockSession.Object);

            var sessionError = new Mock<ISessionError>();
            var expError = new SessionErrorHandlerEventArgs(sessionError.Object);

            _controlClientController.Start(null);
            _mockMessageDistributionService.Raise(x => x.MessageHandlerActive += null, "", null);

            string exceptionError = string.Empty;
            // Ok - we're now active - let's raise ErrorNotified
            try
            {
                mockSession.Raise(x => x.ErrorNotified += null, null, expError);
            }
            catch (Exception exception)
            {
                exceptionError = exception.Message;
            }

            Assert.That(exceptionError, Is.EqualTo("Diffusion Session Error occured"));
        }
    }
}