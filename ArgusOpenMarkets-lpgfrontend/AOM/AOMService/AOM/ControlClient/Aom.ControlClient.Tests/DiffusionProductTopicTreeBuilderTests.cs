using System;

using AOM.App.Domain.Entities;
using AOM.Transport.Events.Products;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;

using Moq;
using NUnit.Framework;

namespace AOM.ControlClient.Tests
{
    [TestFixture]
    public class DiffusionProductTopicTreeBuilderTests
    {
        private Mock<IProductsDiffusionTopicManager> _mockProductsDiffusionTopicManager;
        private Mock<IBus> _mockBus = new Mock<IBus>();
        private DiffusionProductTopicTreeBuilder _diffusionProductTopicTreeBuilder;

        [SetUp]
        public void Setup()
        {
            _mockProductsDiffusionTopicManager = new Mock<IProductsDiffusionTopicManager>();
            _mockBus = new Mock<IBus>();

            _diffusionProductTopicTreeBuilder = new DiffusionProductTopicTreeBuilder(_mockProductsDiffusionTopicManager.Object, _mockBus.Object);
        }

        [Test]
        public void ProductsDiffusionTopicManagerGoingActiveShouldPublishGetProductConfigRequest()
        {
            _mockProductsDiffusionTopicManager.Raise(m => m.Active += null, null, null);
            _mockBus.Verify(m => m.Publish(It.IsAny<GetProductConfigRequest>()), Times.Exactly(1));
        }

        [Test]
        public void ProductsDiffusionTopicManagerGoingActiveRunAndSubscribeToAllEventsOnTheBus()
        {
            _mockProductsDiffusionTopicManager.Raise(m => m.Active += null, null, null);
            Assert.That(_diffusionProductTopicTreeBuilder.SubscriberCount, Is.GreaterThan(0));
        }

        [Test]
        public void ProductsDiffusionTopicManagerGoingActiveAndThenDisconnectingShouldHaveNoSubscribers()
        {
            _mockProductsDiffusionTopicManager.Raise(m => m.Active += null, null, null);
            Assert.That(_diffusionProductTopicTreeBuilder.SubscriberCount, Is.GreaterThan(0));
            _diffusionProductTopicTreeBuilder.Disconnect();
            Assert.That(_diffusionProductTopicTreeBuilder.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        [TestCase("AOM/Products/100/MarketStatus")]
        [TestCase("AOM/Products/100/Definition")]
        [TestCase("AOM/Products/100/ContentStreams")]
        public void ProductsDiffusionTopicManagerOnGetProductConfigResponseShouldCreateTopicForProductDefinitions(
            string topicToCreate)
        {
            var subs = StartConsumer(_mockProductsDiffusionTopicManager);

            var onGetProductConfigResponse = subs.GetProductConfigResponse;
            var mockSimpleTopicSourceHandler = new Mock<SimpleTopicSourceHandler>();

            _mockProductsDiffusionTopicManager.Setup(p => p.GetUpdater()).Returns(mockSimpleTopicSourceHandler.Object);

            var prodDefinitionItem = new ProductDefinitionItem {ProductId = 100};
            var response = new GetProductConfigResponse {ProductDefinition = prodDefinitionItem};
            onGetProductConfigResponse(response);

            _mockProductsDiffusionTopicManager.Verify(m => m.CreateTopic(topicToCreate, It.IsAny<TopicControlAddCallback>(), It.IsAny<Action>()), Times.Exactly(1));
        }

        private TopicControlAddCallback _topicCreatedCallback;

        private bool StoreCallback(TopicControlAddCallback callback)
        {
            _topicCreatedCallback = callback;
            return true;
        }

        private SubscribedActions StartConsumer(Mock<IProductsDiffusionTopicManager> mockProductsDiffusionTopicManager)
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("DiffusionProductTopicTreeBuilder", It.IsAny<Action<GetProductConfigResponse>>()))
                .Callback<string, Action<GetProductConfigResponse>>((s, a) => subs.GetProductConfigResponse = a);

            mockProductsDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            return subs;
        }

        public class SubscribedActions
        {
            public Action<GetProductConfigResponse> GetProductConfigResponse { get; set; }
        }
    }
}
