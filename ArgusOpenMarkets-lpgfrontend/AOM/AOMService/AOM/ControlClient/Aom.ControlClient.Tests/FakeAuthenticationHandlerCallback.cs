﻿namespace AOM.ControlClient.Tests
{
    using PushTechnology.ClientInterface.Client.Security.Authentication;

    public class FakeAuthenticationHandlerCallback : IAuthenticationHandlerCallback
    {
        public bool AllowCalled { get; set; }

        public bool AbstainCalled { get; set; }

        public bool DenyCalled { get; set; }

        public void Allow(IAuthenticationResult result)
        {
            AllowCalled = true;
        }

        public void Abstain()
        {
            AbstainCalled = true;
        }

        public void Allow()
        {
            AllowCalled = true;
        }

        public void Deny()
        {
            DenyCalled = true;
        }
    }
}