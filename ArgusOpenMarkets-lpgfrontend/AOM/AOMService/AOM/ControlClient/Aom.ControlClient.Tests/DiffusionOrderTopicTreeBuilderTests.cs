﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Transport.Events.Products;
using AOMDiffusion.Common;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;

namespace AOM.ControlClient.Tests
{
    [TestFixture]
    public class DiffusionOrderTopicTreeBuilderTests
    {
        private DiffusionOrderTopicTreeBuilder _topicTreeBuilder;
        private Mock<IOrdersDiffusionTopicManager> _mockOrdersDiffusionTopicManager;
        private Mock<IBus> _mockBus;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockOrdersDiffusionTopicManager = new Mock<IOrdersDiffusionTopicManager>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _topicTreeBuilder = new DiffusionOrderTopicTreeBuilder(_mockOrdersDiffusionTopicManager.Object, _mockBus.Object, _mockDateTimeProvider.Object);
        }

        [Test]
        public void ShouldSubscribeToGetProductIdsResponseOnStart()
        {
            var subs = StartConsumer();
            Assert.NotNull(subs);
            Assert.NotNull(subs.GetProductIdsResponse);
            Assert.That(_topicTreeBuilder.SubscriberCount, Is.EqualTo(1));
        }

        [Test]
        public void ShouldPublishGetProductIdsRequestOnGoingActive()
        {
            StartConsumer();
            _mockBus.Verify(b => b.Publish(It.IsAny<GetProductIdsRequest>()), Times.Once);
        }

        [Test]
        public void ShouldClearSubscribersWhenGoingIntoStandby()
        {
            StartConsumer();
            Assert.That(_topicTreeBuilder.SubscriberCount, Is.GreaterThan(0));
            _mockOrdersDiffusionTopicManager.Raise(x => x.StandBy += null, new EventArgs());
            Assert.That(_topicTreeBuilder.SubscriberCount, Is.EqualTo(0));
        }

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void BuildBidAskStackTreeForAProductShouldCallCreateTopicExpectedNumberOfTimesPerProduct(int products)
        {
            // Currently 4 times per product: [Held,Not-Held] * [Internal,External]
            var fakeIds = Enumerable.Range(1, products).ToList();
            fakeIds.ForEach(id => _topicTreeBuilder.BuildOrdersTree(id));

            _mockOrdersDiffusionTopicManager.Verify(m => 
                m.CreateTopic(It.IsAny<string>(), It.IsAny<TopicControlAddCallback>()), Times.Exactly(products * 1));
        }

        [Test]
        [TestCase("AOM/Orders/Products/1/All", ExecutionMode.Internal, Description = "Held Internal")]
        [TestCase("AOM/Orders/Products/1/All", ExecutionMode.Internal, Description = "Not-Held Internal")]
        [TestCase("AOM/Orders/Products/1/All", ExecutionMode.External, Description = "Held External")]
        [TestCase("AOM/Orders/Products/1/All", ExecutionMode.External, Description = "Not-Held External")]
        public void BuildOrdersTreeForAProductShouldCreateTopicForExpectedPaths(string expectedPathFormat, ExecutionMode orderType)
        {
            var expectedPath = string.Format(expectedPathFormat, orderType.RequiredPrivilege());
            _topicTreeBuilder.BuildOrdersTree(1);
            _mockOrdersDiffusionTopicManager.Verify(m => m.CreateTopic(expectedPath, It.IsAny<TopicControlAddCallback>()), Times.Once);
        }

        [Test]
        public void BuildOrdersTreeShouldPublishAomOrderRefreshRequestForProduct()
        {
            
        }

        [Test]
        [TestCase("AOM/Orders/Products/100/All", ExecutionMode.Internal, Description = "Held Internal")]
        [TestCase("AOM/Orders/Products/100/All", ExecutionMode.Internal, Description = "Not-Held Internal")]
        [TestCase("AOM/Orders/Products/100/All", ExecutionMode.External, Description = "Held External")]
        [TestCase("AOM/Orders/Products/100/All", ExecutionMode.External, Description = "Not-Held External")]
        public void OrdersDiffusionTopicManagerOnGetProductConfigResponseShouldCreateTopicForHeldAndNotHeldOrders(string expectedPathFormat, ExecutionMode orderType)
        {
            var expectedPath = string.Format(expectedPathFormat, orderType.RequiredPrivilege());
            var subs = StartConsumer();

            var mockSimpleTopicSourceHandler = new Mock<SimpleTopicSourceHandler>();
            _mockOrdersDiffusionTopicManager.Setup(p => p.GetUpdater()).Returns(mockSimpleTopicSourceHandler.Object);

            var productIds = new List<long> {100};
            var response = new GetProductIdsResponse {ProductIds = productIds};
            subs.GetProductIdsResponse(response);
            _mockOrdersDiffusionTopicManager.Verify(
                m => m.CreateTopic(expectedPath, It.IsAny<TopicControlAddCallback>()), Times.Once);
        }

        [Test]
        [TestCase(ExecutionMode.Internal)]
        [TestCase(ExecutionMode.External)]
        public void PurgeOrdersShouldRebuildTopicsWhenTopicRemoved(ExecutionMode orderType)
        {
            var removedTopic = string.Format("AOM/Orders/SEC-Products/100/SEC-Privileges/{0}", orderType.RequiredPrivilege());

            ITopicControlRemoveCallback callback = null;
            _mockOrdersDiffusionTopicManager.Setup(m => 
                m.DeleteTopic(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ITopicControlRemoveCallback>()))
                .Callback((string t, string s, ITopicControlRemoveCallback c) => callback = c);
            
            _topicTreeBuilder.PurgeOrdersForProduct(100);

            _mockOrdersDiffusionTopicManager.Verify(m => 
                m.CreateTopic(It.IsAny<string>(), It.IsAny<TopicControlAddCallback>()), Times.Never);
            
            Assert.NotNull(callback);

            Assert.IsInstanceOf<TopicControlDeleteCallback>(callback);
            var concreteCallback = (TopicControlDeleteCallback)callback;
            concreteCallback.TopicName = removedTopic;
            concreteCallback.OnTopicsRemoved();

            var expectedPath = "AOM/Orders/Products/100/All";
            _mockOrdersDiffusionTopicManager.Verify(m => m.CreateTopic(expectedPath, It.IsAny<TopicControlAddCallback>()), Times.Once);

        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();

            _mockBus.Setup(b => b.Subscribe("DiffusionOrderTopicTreeBuilder", It.IsAny<Action<GetProductIdsResponse>>()))
                .Callback<string, Action<GetProductIdsResponse>>((s, a) => subs.GetProductIdsResponse = a);

            _mockOrdersDiffusionTopicManager.Raise(x => x.Active += null, new EventArgs());
            return subs;
        }

        private class SubscribedActions
        {
            public Action<GetProductIdsResponse> GetProductIdsResponse { get; set; }
        }
    }
}
