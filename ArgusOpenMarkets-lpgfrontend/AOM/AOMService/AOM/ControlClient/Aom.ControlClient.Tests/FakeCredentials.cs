﻿using System.Text;
using PushTechnology.DiffusionCore.Client.Types;

namespace AOM.ControlClient.Tests
{
    public class FakeCredentials : ICredentials
    {
        private CredentialsType _credentialsType = CredentialsType.NONE;

        public void SetCredentialsType(CredentialsType ct)
        {
            _credentialsType = ct;
        }


        private string _password = "";

        public string Password
        {
            get
            {
                if (string.IsNullOrEmpty(_password))
                {
                    _password = "password";
                }

                return _password;
            }
            set
            {
                _password = value;
            }
        }

        public byte[] ToBytes()
        {
            return Encoding.UTF8.GetBytes(Password);
        }

        public CredentialsType Type
        {
            get
            {
                return _credentialsType;
            }
        }
    }
}