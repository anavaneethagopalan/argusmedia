﻿using AOMDiffusion.Common.Interfaces;

namespace AOM.ControlClient.Tests
{
    using System.Collections.Generic;
    using System.Globalization;
    using AOM.Transport.Events;

    using Moq;

    using NUnit.Framework;

    using PushTechnology.ClientInterface.Client.Content;
    using PushTechnology.ClientInterface.Client.Features.Control.Topics;
    using PushTechnology.ClientInterface.Client.Session;

    [TestFixture]
    public class ClientSessionMessageHandlerTests
    {
        private ClientSessionMessageHandler _clientSessionMessageHandler;

        private Mock<IDiffusionSessionManager> _mockDiffusionFacade;

        [SetUp]
        public void Setup()
        {
            _mockDiffusionFacade = new Mock<IDiffusionSessionManager>();
            _clientSessionMessageHandler = new ClientSessionMessageHandler(_mockDiffusionFacade.Object);
        }

        [Test]
        public void ShouldSendAMessageToTheUser()
        {
            var message = "Message";

            var mockMessageControl = new Mock<IMessagingControl>();

            var mockSession = new Mock<ISession>();
            mockSession.Setup(m => m.GetMessagingControlFeature()).Returns(mockMessageControl.Object);

            _clientSessionMessageHandler.AttachToSession(mockSession.Object);

            var sesionId = new SessionId();
            _mockDiffusionFacade.Setup(m => m.GetSession(It.IsAny<ClientSessionInfo>())).Returns(sesionId);

            var csi = new ClientSessionInfo { OrganisationId = 1, UserId = 1, SessionId = "1" };
            _clientSessionMessageHandler.SendMessage(csi, message);

            var expectedMessageContent =
                PushTechnology.ClientInterface.Client.Factories.Diffusion.Content.NewContent(message);

            mockMessageControl.Verify(m => m.Send(It.IsAny<SessionId>(),
                It.Is<string>(topic => topic.Equals("AOM/Users/1")), 
                It.Is<IContent>(content => content.Equals(expectedMessageContent)), 
                It.IsAny<ClientSessionMessageHandler>()
                ), Times.Once);
        }

        [Test]
        public void ShouldNotSendAMessageToTheClientIfItCannotObtainASessionIdForTheUser()
        {
            var message = "Message";
            var mockMessageControl = new Mock<IMessagingControl>();

            var mockSession = new Mock<ISession>();
            mockSession.Setup(m => m.GetMessagingControlFeature()).Returns(mockMessageControl.Object);

            _clientSessionMessageHandler.AttachToSession(mockSession.Object);

            var csi = new ClientSessionInfo { OrganisationId = 1, UserId = 1, SessionId = "1" };
            _clientSessionMessageHandler.SendMessage(csi, message);

            mockMessageControl.Verify(m => m.Send(It.IsAny<SessionId>(),
                It.IsAny<string>(),
                It.IsAny<IContent>(),
                It.IsAny<ClientSessionMessageHandler>()
                ), Times.Never);
        }

        [Test]
        public void ShouldSendAMessageToAllUsersSessions()
        {
            var message = "Message";
            var mockMessageControl = new Mock<IMessagingControl>();

            var mockSession = new Mock<ISession>();
            mockSession.Setup(m => m.GetMessagingControlFeature()).Returns(mockMessageControl.Object);

            var multipleUserSessions = GenerateUserSessions(2);
            _mockDiffusionFacade.Setup(m => m.GetUserSessions(It.IsAny<long>())).Returns(multipleUserSessions);

            _clientSessionMessageHandler.AttachToSession(mockSession.Object);

            var sesionId = new SessionId();
            _mockDiffusionFacade.Setup(m => m.GetSession(It.IsAny<ClientSessionInfo>())).Returns(sesionId);

            _clientSessionMessageHandler.SendMessageToAllUserSessions(1, message);

            mockMessageControl.Verify(m => m.Send(It.IsAny<SessionId>(),
                It.IsAny<string>(),
                It.IsAny<IContent>(),
                It.IsAny<ClientSessionMessageHandler>()
                ), Times.Exactly(multipleUserSessions.Count));
        }

        private List<ClientSessionInfo> GenerateUserSessions(int numberSessionsToGenerate)
        {
            List<ClientSessionInfo> userSessions = new List<ClientSessionInfo>();
            for (int i = 1; i <= numberSessionsToGenerate; i++)
            {
                userSessions.Add(new ClientSessionInfo { OrganisationId = 1, SessionId = i.ToString(CultureInfo.InvariantCulture), UserId = i });
            }

            return userSessions;
        }
    }
}
