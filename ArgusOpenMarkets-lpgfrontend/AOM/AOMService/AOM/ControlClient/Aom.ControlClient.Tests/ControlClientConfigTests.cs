﻿using AOM.ControlClient.Interfaces;
using AOM.TopicManager;
using AOMDiffusion.Common.Interfaces;
using Argus.Transport.Infrastructure;
using Moq;

namespace AOM.ControlClient.Tests.Authentication
{
    using System.IO;
    using System.Linq;

    using NUnit.Framework;

    [TestFixture]
    public class ControlClientConfigTests
    {
        private ControlClientController _controlClientController;

        [SetUp]
        public void Setup()
        {
            Mock<IBus> aomBus = new Mock<IBus>();
            Mock<IMessageDistributionService> messageDistributionServicev2 = new Mock<IMessageDistributionService>();
            Mock<IDiffusionSessionConnection> diffusionSessionConnection = new Mock<IDiffusionSessionConnection>();
            Mock<IClientSessionMessageHandler> clientSessionMessageHandler = new Mock<IClientSessionMessageHandler>();
            Mock<IDiffusionSessionManager> diffusionSessionManager = new Mock<IDiffusionSessionManager>();
            Mock<IAuthenticationDiffusionTopicManager> authenticationDiffusionTopicManager = new Mock<IAuthenticationDiffusionTopicManager>();
            Mock<IProductsDiffusionTopicManager> productsDiffusionTopicManager = new Mock<IProductsDiffusionTopicManager>();
            Mock<IOrganistionDiffusionTopicManager> organsOrganistionDiffusionTopicManager = new Mock<IOrganistionDiffusionTopicManager>();
            Mock<IHealthCheckDiffusionTopicManager> healthCheckTickerDiffusionTopicManager = new Mock<IHealthCheckDiffusionTopicManager>();
            Mock<IMarketTickerDiffusionTopicManager> marketTickerDiffusionTopicManager = new Mock<IMarketTickerDiffusionTopicManager>();
            Mock<INewsDiffusionTopicManager> newsDiffusionTopicManager = new Mock<INewsDiffusionTopicManager>();
            Mock<IAdminDiffusionTopicManager > adminDiffusionTopicManager = new Mock<IAdminDiffusionTopicManager>();

            Mock<IOrdersDiffusionTopicManager> ordersDiffusionTopicManager = new Mock<IOrdersDiffusionTopicManager>();
            Mock<IAomTopicManager> aomTopicManager = new Mock<IAomTopicManager>();

            _controlClientController = new ControlClientController(aomBus.Object, messageDistributionServicev2.Object, diffusionSessionConnection.Object, clientSessionMessageHandler.Object, 
                diffusionSessionManager.Object, authenticationDiffusionTopicManager.Object, productsDiffusionTopicManager.Object,
                organsOrganistionDiffusionTopicManager.Object, healthCheckTickerDiffusionTopicManager.Object, marketTickerDiffusionTopicManager.Object,
                newsDiffusionTopicManager.Object, adminDiffusionTopicManager.Object, ordersDiffusionTopicManager.Object,
                aomTopicManager.Object);
        }

        [Test]
        public void ParseDiffusionTopicShouldReturnTopicSelectorForValidTopic()
        {
            var topicSelector = _controlClientController.ParseDiffusionTopic(TopicHelper.MakeWildCardTopic("AOM/Organisations/525/BrokerPerms/"));
            Assert.That(topicSelector, Is.Not.Null);
            Assert.That(topicSelector.PathPrefix, Is.EqualTo(@"AOM/Organisations/525/BrokerPerms"));
        }

        [Test]
        public void ParseDiffusionTopicShouldReturnNullTopicSelectorForInValidTopic()
        {
            var topicSelector = _controlClientController.ParseDiffusionTopic(TopicHelper.MakeWildCardTopic("AOM/Organisations/*/BrokerPerms/"));
            Assert.That(topicSelector, Is.Null);
        }

        [Test]
        public void DiffusionEndPointShouldBeSetToLocalhost()
        {
            //EscapedCodeBase "file:///C:/Projects/ArgusOpenMarkets/AOM/AOMService/AOM/ControlClient/Aom.ControlClient.Tests/bin/x64/Debug/AOM.ControlClient.Tests.DLL"   string

            const string diffusionEndPoint = "dpt://localhost:8080";
            string filePath = Directory.GetParent(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath).Parent.Parent.Parent.FullName;
            string[] configFiles = Directory.GetFiles(filePath, "*.config", SearchOption.AllDirectories)
                             .Where(x => x.EndsWith("App.config")).ToArray();

            foreach (var configFile in configFiles)
            {
                if (configFile.ToLower().Contains("\\aom.controlclient\\app.config"))
                {
                    string contents = File.ReadAllText(configFile);
                    Assert.True(contents.Contains(diffusionEndPoint), configFile + "Diffusion Config Set Incorrectly");
                }
            }
        }

        [Test]
        public void RabbitBusShouldBeSetToLocahost()
        {
            const string diffusionEndPoint = "dpt://localhost:8080";
            string filePath = Directory.GetParent(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath).Parent.Parent.Parent.FullName;
            string[] configFiles = Directory.GetFiles(filePath, "*.config", SearchOption.AllDirectories).Where(x => x.EndsWith("App.config")).ToArray();

            foreach (var configFile in configFiles)
            {
                if (configFile.ToLower().Contains("\\aom.controlclient\\app.config"))
                {
                    string contents = File.ReadAllText(configFile);
                    Assert.True(contents.Contains(diffusionEndPoint), configFile + "Diffusion Config Set Incorrectly");
                }
            }
        }
        
    }
}
