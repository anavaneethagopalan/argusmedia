﻿using AOMDiffusion.Common.Interfaces;
using PushTechnology.DiffusionCore.Client.Types;

namespace AOM.ControlClient.Tests.Authentication
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.ControlClient.Authentication;
    using Argus.Transport.Infrastructure;
    using Moq;
    using NUnit.Framework;
    using PushTechnology.ClientInterface.Client.Details;

    [TestFixture]
    public class ExternalCredentialsAuthenticationTests
    {
        [Test]
        public void ShouldAbstainTheConnectionIfCredentialsTypeIsNone()
        {
            var mockTopicManager = new Mock<IAuthenticationDiffusionTopicManager>();
            var mockUserAuthenticationService = new Mock<IUserAuthenticationService>();
            var mockDiffusionSessionManager = new Mock<IDiffusionSessionManager>();
            var mockBus = new Mock<IBus>();

            //            IAuthenticationDiffusionTopicManager aomTopicManager,
            //            IUserAuthenticationService userAuthenticationService,
            //            IBus bus, 
            //            IDiffusionSessionManager diffusionSessionManager)

            var externalCredentialsAuthentication = new ExternalCredentialsAuthentication(mockTopicManager.Object,
            mockUserAuthenticationService.Object,
            mockBus.Object,
            mockDiffusionSessionManager.Object)
            ;

            var fakeAuthenticationHandlerCallback = new FakeAuthenticationHandlerCallback();
            var fakeCredentials = new FakeCredentials();
            var fakeSessionDetails = new FakeSessionDetails(AddressType.GLOBAL);

            externalCredentialsAuthentication.Auth(
                "nathan",
                fakeCredentials,
                fakeSessionDetails,
                fakeAuthenticationHandlerCallback);

            Assert.That(fakeAuthenticationHandlerCallback.AllowCalled, Is.False);
            Assert.That(fakeAuthenticationHandlerCallback.AbstainCalled, Is.True);
        }

        [Test]
        public void ShouldCallAbstainIfAuthenticateUserTokenReturnsFalse()
        {
            var mockTopicManager = new Mock<IAuthenticationDiffusionTopicManager>();
            var mockUserAuthenticationService = new Mock<IUserAuthenticationService>();
            var mockDiffusionSessionManager = new Mock<IDiffusionSessionManager>();


            var authResult = new AuthenticationResult { IsAuthenticated = false };
            mockUserAuthenticationService.Setup(x => x.AuthenticateToken(It.IsAny<string>(), It.IsAny<string>())).Returns(authResult);

            var mockBus = new Mock<IBus>();
            var externalCredentialsAuthentication = new ExternalCredentialsAuthentication(
                mockTopicManager.Object,
                mockUserAuthenticationService.Object,
                mockBus.Object, mockDiffusionSessionManager.Object);

            #region OldCodeToTestTheAuthenticateTokenOverTheBus

            //var authResult = new AuthenticationResult { IsAuthenticated = false };
            //mockBus.Setup(
            //    m =>
            //        m.Request<AuthenticateUserTokenRequestRpc, AuthenticationResult>(
            //            It.IsAny<AuthenticateUserTokenRequestRpc>())).Returns(authResult);

            #endregion

            var fakeAuthenticationHandlerCallback = new FakeAuthenticationHandlerCallback();
            var fakeCredentials = new FakeCredentials();
            var fakeSessionDetails = new FakeSessionDetails(AddressType.GLOBAL);

            // Ensure we go through a proper Diffusion Authentication.  
            fakeCredentials.SetCredentialsType(CredentialsType.PLAIN_PASSWORD);

            externalCredentialsAuthentication.Auth(
                "nathan",
                fakeCredentials,
                fakeSessionDetails,
                fakeAuthenticationHandlerCallback);

            Assert.That(fakeAuthenticationHandlerCallback.AbstainCalled, Is.True);
        }
    }
}