﻿using AOM.ControlClient.Authentication;
using NUnit.Framework;

namespace AOM.ControlClient.Tests.Authentication
{
    [TestFixture]
	public class AuthenticationCallbackHandlerServiceTests
	{
		private AuthenticationCallbackHandlerService authCallbackService;

		[SetUp]
		public void Setup()
		{
			authCallbackService = new AuthenticationCallbackHandlerService();
		}

		[Test]
		public void ShouldReturnZeroForTheNumberOfClientCallbacks()
		{
			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(0));
		}

		[Test]
		public void ShouldReturnNullForAUserThatDoesNotExists()
		{
			var authUserCallback = authCallbackService.GetUserAuthenticationCallback("nathan.bellamore@argusmedia.com");
			Assert.That(authUserCallback, Is.Null);
		}

		[Test]
		public void RegisteringAUserCallbackShouldIncreaseTheNumberOfCallbacksByOne()
		{
			var callback = new FakeAuthenticationHandlerCallback();
			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(0));
			authCallbackService.RegisterUserCallback("nathan.bellamore@hotmail.com", callback);
			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(1));
		}

		[Test]
		public void RegisteringAUserCallbackTwiceForTheSameUserShouldResultInASingleCallback()
		{
			var callback = new FakeAuthenticationHandlerCallback();
			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(0));
			authCallbackService.RegisterUserCallback("nathan.bellamore@hotmail.com", callback);
			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(1));

			var callback2 = new FakeAuthenticationHandlerCallback();
			authCallbackService.RegisterUserCallback("nathan.bellamore@hotmail.com", callback2);
			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(1));
		}

		[Test]
		public void ShouldReturnAUserCallbackIfThatUserIsRegistered()
		{
			var username = "nathan.bellamore@hotmail.com";
			var callback = new FakeAuthenticationHandlerCallback();

			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(0));
			authCallbackService.RegisterUserCallback(username, callback);
			var userCallback = authCallbackService.GetUserAuthenticationCallback(username);
			Assert.That(userCallback, Is.Not.Null);
		}

		[Test]
		public void FlushShouldRemoveAllClientCallbacks()
		{
			var callback = new FakeAuthenticationHandlerCallback();
			authCallbackService.RegisterUserCallback("nathan.bellamore@hotmail.com", callback);
			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(1));
			authCallbackService.Flush();
			Assert.That(authCallbackService.NumberCallbacks(), Is.EqualTo(0));
		}
	}
}
