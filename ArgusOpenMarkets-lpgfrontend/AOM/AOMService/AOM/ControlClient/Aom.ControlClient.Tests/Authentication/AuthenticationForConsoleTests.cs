﻿using AOM.ControlClient.Authentication;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Details;

namespace AOM.ControlClient.Tests.Authentication
{
    [TestFixture]
    public class AuthenticationForConsoleTests
    {
        [Test]        
        public void ShouldAllowTheConnectionIfCredentialsTypeIsNoneOnLocalSite()
        {
            var authModule = new AuthenticationForConsole();

            var fakeAuthenticationHandlerCallback = new FakeAuthenticationHandlerCallback();
            var fakeCredentials = new FakeCredentials();
            var fakeSessionDetails = new FakeSessionDetails(AddressType.LOCAL);

            authModule.Auth("nathan", fakeCredentials, fakeSessionDetails, fakeAuthenticationHandlerCallback);

            Assert.That(fakeAuthenticationHandlerCallback.AllowCalled, Is.True);            
        }

        [Test]
        public void ShouldAllowTheConnectionIfCredentialsTypeIsNoneOnLoopbackSite()
        {
            var authModule = new AuthenticationForConsole();

            var fakeAuthenticationHandlerCallback = new FakeAuthenticationHandlerCallback();
            var fakeCredentials = new FakeCredentials();
            var fakeSessionDetails = new FakeSessionDetails(AddressType.LOOPBACK);

            authModule.Auth("nathan", fakeCredentials, fakeSessionDetails, fakeAuthenticationHandlerCallback);

            Assert.That(fakeAuthenticationHandlerCallback.AllowCalled, Is.True);
        }

        [Test]
        public void ShouldDenyTheConnectionIfCredentialsTypeIsNoneOnInternet()
        {
            var authModule = new AuthenticationForConsole();

            var fakeAuthenticationHandlerCallback = new FakeAuthenticationHandlerCallback();
            var fakeCredentials = new FakeCredentials();
            var fakeSessionDetails = new FakeSessionDetails(AddressType.GLOBAL);

            authModule.Auth("nathan", fakeCredentials, fakeSessionDetails, fakeAuthenticationHandlerCallback);

            Assert.That(fakeAuthenticationHandlerCallback.AllowCalled, Is.False);
            Assert.That(fakeAuthenticationHandlerCallback.DenyCalled, Is.True);
        }    
    }
}

