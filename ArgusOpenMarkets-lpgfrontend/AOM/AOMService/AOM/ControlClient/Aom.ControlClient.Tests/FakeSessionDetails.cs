﻿using System;
using System.Collections.Generic;
using PushTechnology.ClientInterface.Client.Details;

namespace AOM.ControlClient.Tests
{
    internal class FakeSessionDetails : ISessionDetails
    {
        internal FakeSessionDetails(AddressType addressType)
        {
            Location = new FakeClientLocation(addressType);
        }

        public List<DetailType> AvailableDetails
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string ConnectorName
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IClientLocation Location { get; set; }
     
        public IClientSummary Summary
        {
            get
            {
                throw new NotImplementedException();
            }
        }


        public string ServerName
        {
            get { throw new NotImplementedException(); }
        }
    }

    class FakeClientLocation : IClientLocation
    {
        public FakeClientLocation(AddressType addressType)
        {
            AddressType = addressType;
        }

        public string Address { get; private set; }
        public string HostName { get; private set; }
        public string ResolvedName { get; private set; }
        public ICountryDetails CountryDetails { get; private set; }
        public AddressType AddressType { get; private set; }
        public ICoordinates Coordinates { get; private set; }
    }
}