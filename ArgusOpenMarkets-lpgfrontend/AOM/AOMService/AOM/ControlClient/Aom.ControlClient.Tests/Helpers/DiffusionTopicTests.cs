﻿using AOM.ControlClient.Helpers;
using NUnit.Framework;

namespace AOM.ControlClient.Tests.Helpers
{
    [TestFixture]
    public class DiffusionTopicTests
    {
        [Test]
        public void ShouldReturnAProductIdFromAProductTopic()
        {
            const string productTopic = "AOM/Orders/SEC-Products/1/SEC-Privileges/View_BidAsk_Widget";
            var productId = DiffusionTopic.GetProductIdFromTopicDeleted(productTopic);
            Assert.True(productId.HasValue);
            Assert.That(productId.Value, Is.EqualTo(1));
        }

        [Test]
        public void ShouldReturnAProductIdFromAProductTopicWithMultipleDigits()
        {
            const string productTopic = "AOM/Orders/SEC-Products/123/SEC-Privileges/View_BidAsk_Widget";
            var productId = DiffusionTopic.GetProductIdFromTopicDeleted(productTopic);
            Assert.True(productId.HasValue);
            Assert.That(productId.Value, Is.EqualTo(123));
        }

        [Test]
        public void ShouldReturnNullFromAProductTopicIfTheTopicContainsNoProductId()
        {
            const string productTopic = "AOM/Orders/SEC-Products/ProductId/SEC-Privileges/View_BidAsk_Widget";
            var productId = DiffusionTopic.GetProductIdFromTopicDeleted(productTopic);
            Assert.False(productId.HasValue);
        }
    }
}
