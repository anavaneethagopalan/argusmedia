﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using NUnit.Framework;
using AOM.ControlClient.Helpers;
using AOM.Transport.Events.MarketTickers;

namespace AOM.ControlClient.Tests.Helpers
{
    [TestFixture]
    public class MarketTickerRouterTests
    {
        private static readonly AdditionalMarketTickerData EmptyData = new AdditionalMarketTickerData();

        [Test]
        public void ShouldSendVoidMarketInfoToEveryone()
        {
            var mti = CreateMarketTickerItem(MarketTickerItemType.Info, MarketTickerItemStatus.Void);
            Assert.False(MarketTickerRouter.ShouldSendNonPendingOnlyToOwnerOrganisations(mti, EmptyData));
        }

        [Test]
        [TestCase(MarketTickerItemStatus.Pending)]
        [TestCase(MarketTickerItemStatus.Active)]
        public void ShouldSendNonVoidMarketInfoToAll(MarketTickerItemStatus status)
        {
            var mti = CreateMarketTickerItem(MarketTickerItemType.Info, status);
            Assert.False(MarketTickerRouter.ShouldSendNonPendingOnlyToOwnerOrganisations(mti, EmptyData));
        }

        [Test]
        [TestCase(MarketTickerItemStatus.Pending)]
        [TestCase(MarketTickerItemStatus.Active)]
        [TestCase(MarketTickerItemStatus.Void)]
        public void ShouldSendBidMarketInfoToAll(MarketTickerItemStatus status)
        {
            var mti = CreateMarketTickerItem(MarketTickerItemType.Bid, status);
            Assert.False(MarketTickerRouter.ShouldSendNonPendingOnlyToOwnerOrganisations(mti, EmptyData));
        }

        [Test]
        [TestCase(MarketTickerItemStatus.Pending)]
        [TestCase(MarketTickerItemStatus.Active)]
        [TestCase(MarketTickerItemStatus.Void)]
        public void ShouldSendAskMarketInfoToAll(MarketTickerItemStatus status)
        {
            var mti = CreateMarketTickerItem(MarketTickerItemType.Ask, status);
            Assert.False(MarketTickerRouter.ShouldSendNonPendingOnlyToOwnerOrganisations(mti, EmptyData));
        }

        [Test]
        public void ShouldSendVoidPreviouslyPendingExternalDealOnlyToOwners()
        {
            var mti = CreateMarketTickerItem(MarketTickerItemType.Deal, MarketTickerItemStatus.Void, 1234);
            var additionalData = new AdditionalMarketTickerData {PreviousDealStatus = DealStatus.Pending};
            Assert.True(MarketTickerRouter.ShouldSendNonPendingOnlyToOwnerOrganisations(mti, additionalData));
        }

        [Test]
        public void ShouldSendVoidPreviouslyExecutedExternalDealToAll()
        {
            var mti = CreateMarketTickerItem(MarketTickerItemType.Deal, MarketTickerItemStatus.Void, 1234);
            var additionalData = new AdditionalMarketTickerData {PreviousDealStatus = DealStatus.Executed};
            Assert.False(MarketTickerRouter.ShouldSendNonPendingOnlyToOwnerOrganisations(mti, additionalData));
        }

        [Test]
        public void ShouldThrowIfGivenNullMarketTickerItem_ShouldSend()
        {
            var e =
                Assert.Throws<ArgumentNullException>(
                // ReSharper disable once AssignNullToNotNullAttribute
                    () => MarketTickerRouter.ShouldSendNonPendingOnlyToOwnerOrganisations(null, EmptyData));
            StringAssert.Contains("mti", e.Message);
        }

        #region Editor topic path generation tests


        #endregion

        #region Non-pending market ticker item topic path generation tests



        #endregion

        private static MarketTickerItem CreateMarketTickerItem(MarketTickerItemType itemType,
                                                               MarketTickerItemStatus status,
                                                               long? externalDealId = null,
                                                               long? markInfoId = null)
        {
            return new MarketTickerItem
            {
                MarketTickerItemType = itemType,
                MarketTickerItemStatus = status,
                ExternalDealId = externalDealId,
                MarketInfoId = markInfoId,
                ProductIds = new List<long>()
            };
        }
    }
}
