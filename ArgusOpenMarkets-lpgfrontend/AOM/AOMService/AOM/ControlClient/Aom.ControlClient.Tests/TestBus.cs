﻿using System;
using System.Threading.Tasks;
using Argus.Transport.Infrastructure;

namespace AOM.ControlClient.Tests
{
    public class TestBus : IBus
    {
        public bool PublishedCalled { get; set; }

        public object EntityPublished { get; set; }

        public event Action OnConnected;

        public event Action OnDisconnected;

        public void Publish<T>(T message, string topic) where T : class
        {
            PublishedCalled = true;
            EntityPublished = message;
        }

        public void Publish<T>(T message) where T : class
        {
            PublishedCalled = true;
            EntityPublished = message;
        }

        public Task PublishAsync<T>(T message, string topic) where T : class
        {
            throw new NotImplementedException();
        }

        public Task PublishAsync<T>(T message) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Subscribe<T>(string subscriptionId, Action<T> onMessage, string topic) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Subscribe<T>(string subscriptionId, Action<T> onMessage) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable SubscribeAsync<T>(string subscriptionId, Func<T, Task> onMessage, string topic)
            where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable SubscribeAsync<T>(string subscriptionId, Func<T, Task> onMessage) where T : class
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public TResponse Request<TRequest, TResponse>(TRequest request) where TRequest : class where TResponse : class
        {
            throw new NotImplementedException();
        }

        public Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest request) where TRequest : class
            where TResponse : class
        {
            throw new NotImplementedException();
        }

        public void Respond<TRequest, TResponse>(Func<TRequest, TResponse> responder) where TRequest : class
            where TResponse : class
        {
            throw new NotImplementedException();
        }

        public void RespondAsync<TRequest, TResponse>(Func<TRequest, Task<TResponse>> responder) where TRequest : class
            where TResponse : class
        {
            throw new NotImplementedException();
        }


        public IDisposable Subscribe<T>(string subscriptionId, Action<T> onMessage, Action<ISubscriptionConfiguration> configure) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable SubscribeAsync<T>(string subscriptionId, Func<T, Task> onMessage, Action<ISubscriptionConfiguration> configure) where T : class
        {
            throw new NotImplementedException();
        }

        public void Publish<T>(T message, TimeSpan delay) where T : class
        {
            throw new NotImplementedException();
        }

        public Task PublishAsync<T>(T message, TimeSpan delay) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Subscribe<T>(string subscriptionId, TimeSpan messageDelay, Action<T> onMessage, Action<ISubscriptionConfiguration> configure) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Subscribe<T>(string subscriptionId, TimeSpan messageDelay, Action<T> onMessage) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable SubscribeAsync<T>(string subscriptionId, TimeSpan messageDelay, Func<T, Task> onMessage, Action<ISubscriptionConfiguration> configure) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable SubscribeAsync<T>(string subscriptionId, TimeSpan messageDelay, Func<T, Task> onMessage) where T : class
        {
            throw new NotImplementedException();
        }
    }
}