﻿using System;
using NUnit.Framework;
using Utils.Javascript.Extension;

namespace Utils.Javascript.Tests.Extension
{
    [TestFixture]
    public class ExtensionMethodsTests
    {
        [Test]
        public void SafeReplaceHandleNullReferenceTest()
        {
            Exception nullItem=null;
            var outcome="AA{B}CC".SafeReplace("{B}", () => nullItem.Message );

            Assert.AreEqual("AA?CC", outcome);
        }

        [Test]
        public void DoesNotStartWithTest()
        {
            Assert.IsTrue("ABB".DoesNotStartWith("B"));
            Assert.IsFalse("ABB".DoesNotStartWith("A"));
        }

        [Test]
        public void DoesNotEndWithTest()
        {
            Assert.IsTrue("ABB".DoesNotEndWith("A"));
            Assert.IsFalse("ABB".DoesNotEndWith("B"));
        }

        [Test]
        public void DoesNotContainTest()
        {
            Assert.IsTrue("ABB".DoesNotContain("Z"));
            Assert.IsFalse("ABB".DoesNotContain("B"));
        }

        [Test]
        public void StartWithIgnoreCase()
        {
            Assert.IsTrue("ABB123".StartsWithIgnoreCase("AbB"));
            Assert.IsTrue("ABB123".StartsWithIgnoreCase("abb"));
            Assert.IsTrue("ABB123".StartsWithIgnoreCase("ABB"));
            Assert.IsFalse("ABB123".StartsWithIgnoreCase("BB"));
        }
        
        [Test]
        public void AddWeekDaysSkipsWeekend()
        {
            var monday8Th=new DateTime(2014, 12, 8);
            var friday5Th = new DateTime(2014, 12, 5);
            
            Assert.AreEqual(new DateTime(2014, 12, 9), monday8Th.AddWeekDays(1));
            Assert.AreEqual(new DateTime(2014, 12, 10), monday8Th.AddWeekDays(2));
            Assert.AreEqual(new DateTime(2014, 12, 11), monday8Th.AddWeekDays(3));

            Assert.AreEqual(new DateTime(2014, 12, 5), monday8Th.AddWeekDays(-1));
            Assert.AreEqual(new DateTime(2014, 12, 4), monday8Th.AddWeekDays(-2));
            Assert.AreEqual(new DateTime(2014, 12, 3), monday8Th.AddWeekDays(-3));

            Assert.AreEqual(new DateTime(2014, 12, 8), friday5Th.AddWeekDays(1));
            Assert.AreEqual(new DateTime(2014, 12, 9), friday5Th.AddWeekDays(2));
            Assert.AreEqual(new DateTime(2014, 12, 10), friday5Th.AddWeekDays(3));

            Assert.AreEqual(new DateTime(2014, 12, 4), friday5Th.AddWeekDays(-1));
            Assert.AreEqual(new DateTime(2014, 12, 3), friday5Th.AddWeekDays(-2));
            Assert.AreEqual(new DateTime(2014, 12, 2), friday5Th.AddWeekDays(-3));
        }

        [Test]
        public void StringLikeMatchesExactString()
        {
            Assert.IsTrue("test string".Like("test string"));
        }

        [Test]
        public void StringLikeMatchesWithEndOfStringWildcard()
        {
            Assert.IsTrue("test string".Like("test*"));
        }

        [Test]
        public void StringLikeMatchesWithStartOfStringWildcard()
        {
            Assert.IsTrue("test string".Like("*string"));
        }

        [Test]
        public void StringLikeDoesNotMatchADifferentString()
        {
            Assert.IsFalse("test string".Like("not the same"));
        }

        [Test]
        public void StringLikeDoesNotMatchADifferentStringWithStartOfStringWildcard()
        {
            Assert.IsFalse("test string".Like("*not the same"));
        }

        [Test]
        public void StringLikeDoesNotMatchADifferentStringWithEndOfStringWildcard()
        {
            Assert.IsFalse("test string".Like("not the same*"));
        }

        [Test]
        public void StringLikeMatchesWithMultipleWildcards()
        {
            Assert.IsTrue("test both leading and trailing".Like("*leading*"));
            Assert.IsTrue("test both leading and trailing".Like("*leading*trail*"));
        }

        [Test]
        public void StringLikeDoesNotMatchADifferentStringWithLeadingAndTrailingWildcard()
        {
            Assert.IsFalse("should not match this".Like("*leading*"));
        }

        [Test]
        public void StringLikeMatchesAStringUsingASingleCharacterWildcard()
        {
            Assert.IsTrue("test string".Like("test?string"));
            Assert.IsTrue("test_string".Like("test?string"));
            Assert.IsTrue("test_string".Like("?est?string"));
            Assert.IsTrue("test_string".Like("?est?strin?"));
            Assert.IsTrue("test_string".Like("?est_string"));
            Assert.IsTrue("test_string".Like("test_strin?"));
        }

        [Test]
        public void StringLikeDoesNotMatchDifferentStringWhenUsingSingleCharacterWildcard()
        {
            Assert.IsFalse("test_string".Like("?est string"));
            Assert.IsFalse("test_string".Like("test strin?"));
            Assert.IsFalse("test_string".Like("?est strin?"));
            Assert.IsFalse("badger".Like("?est?strin?"));
        }

        public class DataToJson
        {
            public string Name { get; set; }
            public string Age { get; set; }
        }

        [Test]
        public void ShouldConvertJsonToCamelCase()
        {
            var dataToJson = new DataToJson {Age = "100", Name = "Nathan"};
            var expectedName = "\"name\": \"Nathan\"";
            var expectedAge = "\"age\": \"100\"";

            var dataToJsonCamelCase = dataToJson.ToJsonCamelCase();
            Assert.That(dataToJsonCamelCase.Contains(expectedName));
            Assert.That(dataToJsonCamelCase.Contains(expectedAge));
        }
         
    }
}

