﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Utils.Javascript.Tests.Extension
{
    public static class AssertHelper
    {
        /// <summary>
        /// Checks that a list ONLY contains the expected values by using the Func to extrace the values        
        /// </summary>
        /// <param name="results">The list to check</param>
        /// <param name="func">A Func to extract a value from the list item (you can also use it to build a string)</param>
        /// <param name="expectedValues">A distinct list of expected values</param>
        public static void AssertListOnlyContains<T, TR>(IList<T> results, Func<T, TR> func, params TR[] expectedValues)
        {
            var resultItems = String.Join(", ", results.Distinct().Select(p => func(p).ToString()));
            Assert.AreEqual(expectedValues.Count(), results.Distinct().Count(), "Didnt expect " + resultItems);

            foreach (var actualItem in results)
            {
                var actualValue = func(actualItem);
                Assert.Contains(actualValue, expectedValues);
            }
        } 
    }
}
