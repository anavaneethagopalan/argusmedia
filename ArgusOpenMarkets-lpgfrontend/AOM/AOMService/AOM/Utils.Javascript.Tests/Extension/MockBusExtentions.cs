using System;
using System.Collections.Generic;
using AOM.Transport.Events;
using Argus.Transport.Infrastructure;
using Moq;

namespace Utils.Javascript.Tests.Extension
{
    public static class MockBusExtentions
    {
        private static dynamic _actions;

        public static void SpyOnSubscribe<T>(this Mock<IBus> bus) where T : class
        {
            _actions = Activator.CreateInstance<List<Action<T>>>();
            bus.Setup(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<T>>()))
                .Callback((string s, Action<T> action1) =>
                {
                    _actions.Add(action1);
                });
        }

        public static dynamic GetSubscriptionActions(this Mock<IBus> bus)
        {
            return _actions;
        }

        public static void VerifyThatNoAomErrorMessageWasSent(this Mock<IBus> bus)
        {
            bus.Verify(x => x.Publish(It.Is<AomResponse>(r => (r.Message.MessageType == MessageType.Error))), Times.Never);
            //we don't expect an error
        }
    }
}