﻿using System;
using System.CodeDom;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Services.ErrorService;
using AOM.Services.MarketInfoService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.MarketInfoProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.MarketInfoProcessor.Tests.Consumers
{
    [TestFixture]
    public class MarketInfoConsumerTests
    {
        [Test]
        public void OnStartSubscribesProcessorToBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketInfoService = new Mock<IMarketInfoService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var marketInfoConsumer = new MarketInfoConsumer(MockDbContextFactory.Stub(), mockMarketInfoService.Object, mockUserService.Object,
                mockBus.Object, mockErrorService.Object);
            marketInfoConsumer.Start();

            //Assert
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomMarketInfoRequest>>()), Times.Once);
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomMarketInfoAuthenticationResponse>>()), Times.Once);
        }

        [Test]
        public void ConsumeMarketInfoRequestPutsMarketInfoAuthenticationRequestOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketInfoService = new Mock<IMarketInfoService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            var marketInfo = new MarketInfo
            {
                Id = 1
            };

            //Act
            var marketInfoConsumer = new MarketInfoConsumer(MockDbContextFactory.Stub(), mockMarketInfoService.Object, mockUserService.Object,
                mockBus.Object, mockErrorService.Object);

            marketInfoConsumer.ConsumeMarketInfoRequest<AomMarketInfoRequest,AomMarketInfoAuthenticationRequest>(new AomMarketInfoRequest
            {
                MarketInfo = marketInfo,
                MessageAction = MessageAction.Create
            });

            mockBus.Verify(x => x.Publish(It.Is<AomMarketInfoAuthenticationRequest>(i => i.MarketInfo.Id == 1)));
        }

        [Test]
        public void ConsumeMarketInfoAuthenticationResponsePutsMarketInfoResponseOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketInfoService = new Mock<IMarketInfoService>();
            var mockUserService = new Mock<IUserService>();

            var marketInfo = new MarketInfo
            {
                Id = 1
            };

            var testMessage = new Message
            {
                ClientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" },
                MessageAction = MessageAction.Create,
                MessageType = MessageType.Info,
                MessageBody = marketInfo
            };

            var authenticationResponse = new AomMarketInfoAuthenticationResponse
            {
                MarketInfo = marketInfo,
                Message = testMessage
            };

            //Act
            var mockErrorService = new Mock<IErrorService>();
            var marketInfoConsumer = new MarketInfoConsumer(MockDbContextFactory.Stub(), mockMarketInfoService.Object, mockUserService.Object,
                mockBus.Object, mockErrorService.Object);

            mockMarketInfoService.Setup(x => x.CreateMarketInfo(It.IsAny<IAomModel>(), authenticationResponse)).Returns(marketInfo);

            marketInfoConsumer.ConsumeMarketInfoResponse<AomMarketInfoAuthenticationResponse, AomMarketInfoResponse>(authenticationResponse);

            mockBus.Verify(x => x.Publish(It.Is<AomMarketInfoResponse>(i => i.MarketInfo.Id == 1)));
        }

        [Test]
        public void ConsumeMarketInfoAuthenticationResponsePutsMarketInfoResponseErrorOnBusWhenExceptionOccurs()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockMarketInfoService = new Mock<IMarketInfoService>();
            var mockUserService = new Mock<IUserService>();

            var marketInfo = new MarketInfo
            {
                Id = 1
            };

            var testMessage = new Message
            {
                ClientSessionInfo = new ClientSessionInfo {UserId = 12, SessionId = "TestSessionId"},
                MessageAction = MessageAction.Create,
                MessageType = MessageType.Info,
                MessageBody = marketInfo
            };

            var authenticationResponse = new AomMarketInfoAuthenticationResponse
            {
                MarketInfo = marketInfo,
                Message = testMessage
            };

            //Act
            var mockErrorService = new Mock<IErrorService>();
            mockErrorService.Setup(m => m.LogUserError(It.IsAny<UserError>())).Returns("Error");
            var marketInfoConsumer = new MarketInfoConsumer(MockDbContextFactory.Stub(), mockMarketInfoService.Object, mockUserService.Object,
                mockBus.Object, mockErrorService.Object);

            mockMarketInfoService.Setup(x => x.CreateMarketInfo(It.IsAny<IAomModel>(), authenticationResponse)).Throws(new Exception("Something goes wrong authenticating"));
            marketInfoConsumer.ConsumeMarketInfoResponse<AomMarketInfoAuthenticationResponse, AomMarketInfoResponse>(authenticationResponse);

            mockBus.Verify(x => x.Publish(It.Is<AomMarketInfoResponse>(i => i.MarketInfo == null &&
                                                                       i.Message.MessageType == MessageType.Error &&
                                                                       i.Message.MessageBody.ToString() == "Error")
                                                                       ), Times.Once);
        }
    }
}
