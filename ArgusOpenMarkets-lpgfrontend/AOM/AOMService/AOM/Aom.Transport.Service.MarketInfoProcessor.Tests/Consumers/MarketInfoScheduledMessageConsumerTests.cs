﻿using System;
using AOM.App.Domain.Dates;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Transport.Events.MarketInfos;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.MarketInfoProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.MarketInfoProcessor.Tests.Consumers
{
    [TestFixture]
    public class MarketInfoScheduledMessageConsumerTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private Mock<IDbContextFactory> _mockDbContextfactory;
        private MarketInfoScheduledMessageConsumer _marketInfoScheduledMessageConsumer;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockDbContextfactory = new Mock<IDbContextFactory>();

        }

        [Test]
        public void AValidScheduledMarketInfoRequestShouldResultInPublishMarketInfoRequest()
        {
            var arguments = "{\"MessageText\":\"PurgeMarketInfo5\", \"ProductChannels\": \"[1,2]\"}";
            
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddScheduledTask("Name1", "AomScheduledMarketInfoRequest", arguments);

            var dbContextFactory = new MockDbContextFactory(mockAomModel, null);
            _marketInfoScheduledMessageConsumer = new MarketInfoScheduledMessageConsumer(_mockBus.Object,
               _mockDateTimeProvider.Object,
               dbContextFactory);

            var subs = StartConsumer();
            _marketInfoScheduledMessageConsumer.Start(); 

            var onAomScheduledMarketInfoRequest = subs.AomScheduledMarketInfoRequest;

            var req = new AomScheduledMarketInfoRequest {MessageText = "Anthing Goes", ProductChannels = "[1,2]"};
            onAomScheduledMarketInfoRequest(req);

            _mockBus.Verify(
                b =>
                    b.Publish(
                        It.IsAny<AomMarketInfoRequest>()),
                Times.Once);
        }

        [Test]
        public void AnInValidScheduledMarketInfoRequestWithNoProductIdsShouldNotPublishAnythingToBus()
        {
            var arguments = "{\"MessageText\":\"PurgeMarketInfo5\", \"ProductChannels\": \"[1,2]\"}";
            
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddScheduledTask("Name1", "AomScheduledMarketInfoRequest", arguments);

            var dbContextFactory = new MockDbContextFactory(mockAomModel, null);
            _marketInfoScheduledMessageConsumer = new MarketInfoScheduledMessageConsumer(_mockBus.Object,
               _mockDateTimeProvider.Object,
               dbContextFactory);

            var subs = StartConsumer();
            _marketInfoScheduledMessageConsumer.Start(); 

            var onAomScheduledMarketInfoRequest = subs.AomScheduledMarketInfoRequest;

            var req = new AomScheduledMarketInfoRequest {MessageText = "Anthing Goes", ProductChannels = ""};
            onAomScheduledMarketInfoRequest(req);

            _mockBus.Verify(
                b =>
                    b.Publish(
                        It.IsAny<AomMarketInfoRequest>()),
                Times.Never);
        }

        private class SubscribedActions
        {
            public Action<AomScheduledMarketInfoRequest> AomScheduledMarketInfoRequest { get; set; }
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();
            _mockBus.Setup(
                    b => b.Subscribe("AOM", It.IsAny<Action<AomScheduledMarketInfoRequest>>()))
                .Callback<string, Action<AomScheduledMarketInfoRequest>>(
                    (s, a) => subs.AomScheduledMarketInfoRequest = a);

            
            return subs;
        }
    }
}
