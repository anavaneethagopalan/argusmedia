﻿using System;
using System.Diagnostics;
using System.Linq;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AOM.Services;
using AOM.Transport.Service.Processor.Common;
using Ninject;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.NewsProcessor
{
    internal class Program
    {
        internal static void Main()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(
                    new EFModelBootstrapper(),
                    new LocalBootstrapper(),
                    new ServiceBootstrap(),
                    new ProcessorCommonBootstrap());

                WarmUp(kernel);
                ServiceHelper.BuildAndRunService<NewsProcessorService>(kernel);
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            Stopwatch timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (ICrmModel dbCrm = dbFactory.CreateCrmModel())
                {
                    dbCrm.UserInfoes.Any();
                }

                Log.Info(
                    string.Format(
                        "NewsProcessor -Initial EF model build took {0} msecs.",
                        timer.ElapsedMilliseconds));
            }
            catch (Exception ex)
            {
                Log.Error("NewsProcessor - WarmUp - Error - cannot connect to DB", ex);
                ServiceHelper.BuildAndRunService<NewsProcessorService>(kernel);
            }
        }
    }
}