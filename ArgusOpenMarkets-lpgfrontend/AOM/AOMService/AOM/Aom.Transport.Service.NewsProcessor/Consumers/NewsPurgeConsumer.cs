﻿namespace AOM.Transport.Service.NewsProcessor.Consumers
{
    using AOM.App.Domain.Dates;
    using AOM.Transport.Events.News;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Ninject;

    internal class NewsPurgeConsumer : ScheduledTaskConsumerBase<PurgeNewsRequest>
    {
        public NewsPurgeConsumer([Named("AomBus")] IBus aomBus, IDateTimeProvider dateTimeProvider)
            : base(aomBus, dateTimeProvider)
        {
        }
    }
}