﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;

namespace AOM.Transport.Service.NewsProcessor.Consumers
{
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events.News;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;
    using Argus.Transport.Messages.Dtos;
    using Argus.Transport.Messages.Events;

    using Ninject;

    using Utils.Logging.Utils;

    internal class ReplayArgusNewsConsumer : IConsumer
    {
        private readonly INewsConfig _newsConfig;

        private readonly IDateTimeProvider _dateTimeProvider;

        private IAomNewsService _aomNewsService { get; set; }

        private IBus _argusBus { get; set; }

        private IBus _aomBus { get; set; }

        public ReplayArgusNewsConsumer(
            [Named("AomBus")] IBus aomBus,
            [Named("ArgusDirectBus")] IBus argusBus,
            IAomNewsService aomNewsService,
            INewsConfig newsConfig,
            IDateTimeProvider dateTimeProvider)
        {
            _newsConfig = newsConfig;
            _dateTimeProvider = dateTimeProvider;
            _aomNewsService = aomNewsService;
            _argusBus = argusBus;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<ReplayArgusNews>(SubscriptionId, OnReplayArgusNews);
        }

        private void OnReplayArgusNews(ReplayArgusNews replayNewsRequest)
        {
            Log.Info("ReplayArgusNews.OnReplayArgusNews - has been called.");

            var numberDaysToReplayNews = GetNumberDaysToReplayNews(replayNewsRequest.DaysToReplay, _newsConfig.HisoricalDaysGetNews);

            decimal[] aomContentStreamDescriptions;
            var aomContentStreamIds = _aomNewsService.GetAomContentStreamIds();

            if (replayNewsRequest.ContentStreamsReplay != null && replayNewsRequest.ContentStreamsReplay.Length > 0)
            {
                var validContentStreams = GetValidContentStreams(aomContentStreamIds, replayNewsRequest.ContentStreamsReplay);
                aomContentStreamDescriptions = validContentStreams;
            }
            else
            {
                aomContentStreamDescriptions = aomContentStreamIds;
            }

            if (aomContentStreamDescriptions.Length > 0)
            {
                var startDate = _dateTimeProvider.Now
                    .AddDays(numberDaysToReplayNews);

                var newsRefresh = new NewsRefreshRequested
                {
                    NewsRefreshParameters =
                        new NewsRefreshParameters
                        {
                            ContentStreams =
                                aomContentStreamDescriptions,
                            StartDate =
                                startDate,
                            EndDate =
                                _dateTimeProvider.Now
                        }
                };

                _aomNewsService.PurgeNews(startDate);
                _argusBus.Publish(newsRefresh);
            }
            else
            {
                Log.Error("ReplayArgusNews has been called - no valid content streams to replay");
            }
        }

        private decimal[] GetValidContentStreams(decimal[] aomContentStreamIds, decimal[] contentStreams)
        {
            List<decimal> validContentStreams = new List<decimal>();
            foreach (decimal cs in contentStreams)
            {
                foreach (var aomCs in aomContentStreamIds)
                {
                    if (aomCs == cs)
                    {
                        validContentStreams.Add(cs);
                        break;
                    }
                }
            }

            return validContentStreams.ToArray();
        }

        public int GetNumberDaysToReplayNews(int? busNumDaysToReplay, int configNumDaysToReplay)
        {
            var numberDaysToReplayNews = busNumDaysToReplay
                                         ?? configNumDaysToReplay;

            if (numberDaysToReplayNews > 0)
            {
                numberDaysToReplayNews = numberDaysToReplayNews*-1;
            }

            return numberDaysToReplayNews;
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }


        public bool RequiresArgusBus
        {
            get
            {
                return true;
            }
        }
    }
}