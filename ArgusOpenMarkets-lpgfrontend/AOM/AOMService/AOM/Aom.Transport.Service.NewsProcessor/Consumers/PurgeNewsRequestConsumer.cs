﻿namespace AOM.Transport.Service.NewsProcessor.Consumers
{
    using AOM.Transport.Events.News;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Ninject;

    using System;

    using Utils.Logging.Utils;

    public class PurgeNewsRequestConsumer : IConsumer
    {
        private IBus _aomBus { get; set; }

        public PurgeNewsRequestConsumer(
            [Named("AomBus")] IBus aomBus)
        {
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<PurgeNewsRequest>(
                SubscriptionId,
                ConsumePurgeNewsRequest<PurgeNewsRequest, PurgeNewsResponse>);
        }

        private void ConsumePurgeNewsRequest<TReq, TFwd>(TReq request) where TReq : PurgeNewsRequest
            where TFwd : 
            PurgeNewsResponse, new()
        {
            string requestType = typeof(TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd { DaysToKeep = request.DaysToKeep };

                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                Log.Warn(
                    String.Format("Unable to publish Purge News Command to Control Client: {0}", exception.Message));
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}