﻿using System;
using System.Configuration;

namespace AOM.Transport.Service.NewsProcessor.Consumers
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Transport.Events;
    using AOM.Transport.Events.News;
    using AOM.Transport.Service.Processor.Common;
    using Argus.Transport.Infrastructure;
    using Argus.Transport.Messages.Events;
    using Ninject;
    using System.Collections.Generic;
    using System.Globalization;
    using Utils.Logging.Utils;

    internal class ArgusNewsPublishedConsumer : IConsumer
    {
        private readonly IAomNewsService _aomNewsService;

        private IBus _argusBus { get; set; }

        private IBus _aomBus { get; set; }

        public ArgusNewsPublishedConsumer(
            [Named("AomBus")] IBus aomBus,
            [Named("ArgusDirectBus")] IBus argusBus,
            IAomNewsService aomNewsService)
        {
            _aomNewsService = aomNewsService;
            _argusBus = argusBus;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _argusBus.Subscribe<NewsPersisted>(SubscriptionId, OnArgusNewsPersisted);
            _argusBus.Subscribe<NewsPersistedRefresh>(SubscriptionId, OnArgusNewsPersistedRefresh);
            _argusBus.Subscribe<NewsUpdatePersisted>(SubscriptionId, OnArgusNewsUpdatePersisted);
            _argusBus.Subscribe<PersistedNewsDeleted>(SubscriptionId, OnArgusPersistedNewsDeleted);
        }

        private void OnArgusPersistedNewsDeleted(PersistedNewsDeleted newsDeleted)
        {

            if (newsDeleted == null)
            {
                Log.InfoFormat("News Deleted - passed a null object");
            }

            if (newsDeleted.News == null)
            {
                Log.InfoFormat("News Deleted - passed a null news deleted object");
            }

            var aomNewsToDelete = _aomNewsService.TransformNews(newsDeleted.News);

            if (aomNewsToDelete != null)
            {
                try
                {
                    var contentStreams = _aomNewsService.GetArticleContentStreams(newsDeleted.News.ContentStreams);

                    // NOTE - A work around until Direct Team - Raise a PersistedNewsDeleted event - which contains the full article details.  For now - we will load ALL 
                    // Our content streams - and pass through to the delete functionality.
                    if (contentStreams == null || contentStreams.Length == 0)
                    {
                        // No Content Streams.
                        var aomContentStreams = _aomNewsService.GetAomContentStreamIds();
                        var streams = new List<long>();
                        foreach (var aomCs in aomContentStreams)
                        {
                            streams.Add((long) aomCs);
                        }

                        contentStreams = streams.ToArray();
                    }

                    var cmsId = aomNewsToDelete.CmsId;
                    if (string.IsNullOrEmpty(cmsId))
                    {
                        Log.InfoFormat(
                            "Cms Id is empty - why are we not receiving the cms id when delete news story coming through.");
                    }
                    Log.InfoFormat("Publishing a AomNewsDeleted passing in the Cms Id: {0}", cmsId);
                    // NOTE : The control client - responsible for de-queueing this message is going to actually delete the 
                    // entity from our DB.  We don't want to delete from DB before removing from the queue??
                    _aomBus.Publish(
                        new AomNewsDeleted
                        {
                            CmsId = aomNewsToDelete.CmsId,
                            CommodityId = _aomNewsService.GetCommodityIdFromContentStreamId(contentStreams),
                            ContentStreams = contentStreams,
                            IsFree = newsDeleted.News.IsFree
                        });

                }
                catch (Exception ex)
                {
                    Log.Error("OnArgusPersistedNewsDeleted", ex);
                }
            }
        }

        private void OnArgusNewsUpdatePersisted(NewsUpdatePersisted newsUpdated)
        {
            // We need to send an AOM News Updated Event on our Bus.  
            Log.Info("OnArgusNewsUpdatePersisted - this is real-time Argus News Updates onto our bus");

            try
            {
                if (_aomNewsService.IsAomNewsArticle(newsUpdated.PreviousVersion)
                    || _aomNewsService.IsAomNewsArticle(newsUpdated.News))
                {
                    var previousVersionAomNews = _aomNewsService.TransformNews(newsUpdated.PreviousVersion);
                    var updatedVersionAomNews = _aomNewsService.TransformNews(newsUpdated.News);
                    var aomNewsUpdatedEvent = new AomNewsUpdated
                    {
                        PreviousNews = previousVersionAomNews as AomNews,
                        UpdatedNews = updatedVersionAomNews as AomNews,
                        Message =
                            new Message
                            {
                                MessageType = MessageType.News,
                                MessageAction = MessageAction.Update,
                                MessageBody = null
                            }
                    };

                    _aomNewsService.DeleteNews(previousVersionAomNews.CmsId);
                    Log.InfoFormat("Successfully deleted the news story with CMS ID:{0}", previousVersionAomNews.CmsId);
                    _aomNewsService.CreateNews(updatedVersionAomNews);
                    Log.InfoFormat("Successfully created the news story with CMS ID:{0}", updatedVersionAomNews.CmsId);

                    _aomBus.Publish(aomNewsUpdatedEvent);
                }
            }
            catch (Exception ex)
            {
                Log.Error("OnArgusNewsUpdatePersisted", ex);
            }
        }

        private void OnArgusNewsPersisted(NewsPersisted newsPersisted)
        {
            Log.Info("OnArgusNewsPersisted - this is real-time Argus News onto our bus");

            if (_aomNewsService.IsAomNewsArticle(newsPersisted.News))
            {
                var aomNews = _aomNewsService.TransformNews(newsPersisted.News);
                
                var aomNewsPublishedEvent = new AomNewsPublished
                {
                    News = aomNews as AomNews,
                    Message =
                        new Message
                        {
                            MessageType = MessageType.News,
                            MessageAction =
                                MessageAction.Create,
                            MessageBody = null
                        }
                };

                _aomNewsService.CreateNews(aomNews);
                _aomBus.Publish(aomNewsPublishedEvent);
            }
        }

        private void OnArgusNewsPersistedRefresh(NewsPersistedRefresh newsRefresh)
        {
            Log.Info("OnNewsPersistedRefresh - this is a replay of Argus News onto our bus");

            if (newsRefresh != null)
            {
                var aomNews = _aomNewsService.TransformNews(newsRefresh.News);
                var aomNewsPublishedEvent = new AomNewsPublished
                {
                    News = aomNews as AomNews,
                    Message =
                        new Message
                        {
                            MessageType = MessageType.News,
                            MessageAction =
                                MessageAction.Refresh,
                            MessageBody = null,
                            ClientSessionInfo = null
                        }
                };

                _aomNewsService.CreateNews(aomNews);
                _aomBus.Publish(aomNewsPublishedEvent);
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }

        public bool RequiresArgusBus
        {
            get
            {
                return true;
            }
        }
    }
}