﻿using AOM.App.Domain.Dates;
using AOM.Repository.MySql;
using AOM.Transport.Events.News;
using AOM.Transport.Events.ScheduledTasks;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Ninject;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.NewsProcessor.Consumers
{
    public class NewsScheduledTaskConsumer : ScheduledTaskConsumerBase<PurgeNewsRequest>
    {
        public NewsScheduledTaskConsumer(
            [Named("AomBus")] IBus aomBus,
            IDateTimeProvider dateTimeProvider,
            IDbContextFactory dbContextFactory = null)
            : base(aomBus, dateTimeProvider, dbContextFactory)
        {
            Log.Debug("Starting NewsScheduledTaskConsumer");
        }

        public override void Start()
        {
            AomBus.Respond<CreateNewsScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(ConsumeCreateNewScheduledTaskRequest);
            AomBus.Respond<DeleteNewsScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(ConsumeDeleteScheduledTasksRequest);
            base.Start();
        }
    }
}