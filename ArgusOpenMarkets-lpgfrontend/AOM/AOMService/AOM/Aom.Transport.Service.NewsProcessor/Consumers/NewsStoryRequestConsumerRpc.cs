﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.ErrorService;
using AOM.Transport.Events.News;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Ninject;
using System;
using System.Diagnostics;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.NewsProcessor.Consumers
{
    internal class NewsStoryRequestConsumerRpc : IConsumer
    {
        private readonly IBus _aomBus;
        private readonly IErrorService _errorService;

        private readonly IAomNewsService _aomNewsService;

        public NewsStoryRequestConsumerRpc(IAomNewsService newsService, [Named("AomBus")] IBus aomBus,
            IErrorService errorService)
        {
            _aomNewsService = newsService;
            _aomBus = aomBus;
            _errorService = errorService;
        }

        public void Start()
        {
            _aomBus.Respond<NewsStoryRequestRpc, NewsStoryResponse>(GetNewsStoryRequest);
        }

        internal NewsStoryResponse GetNewsStoryRequest(NewsStoryRequestRpc newsStoryRequest)
        {
            try
            {
                var clock = Stopwatch.StartNew();
                var response = new NewsStoryResponse
                {
                    Story =
                        _aomNewsService.GetNewsStory(
                            newsStoryRequest.CmsId),
                    CmsId = newsStoryRequest.CmsId
                };

                Log.InfoFormat(
                    "Fetch news body for of news  id {0} took {1}msecs",
                    newsStoryRequest.CmsId,
                    clock.ElapsedMilliseconds);
                return response;
            }
            catch (Exception ex)
            {
                var logError =
                    _errorService.LogUserError(new UserError
                    {
                        Error = ex,
                        AdditionalInformation = "Error getting the news story id " + newsStoryRequest.CmsId,
                        ErrorText = string.Empty,
                        Source = "NewsStoryRequestConsumerRpc"
                    });

                return new NewsStoryResponse
                {
                    CmsId = newsStoryRequest.CmsId,
                    Story = logError
                };
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }

        public bool RequiresArgusBus
        {
            get
            {
                return false;
            }
        }
    }
}