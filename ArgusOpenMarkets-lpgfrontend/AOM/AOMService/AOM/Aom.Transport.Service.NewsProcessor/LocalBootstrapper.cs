﻿using System.Configuration;

using AOM.Repository.MySql.Crm;
using AOM.Repository.Oracle;
using AOM.Transport.Service.NewsProcessor.Internals;
using AOM.Transport.Service.Processor.Common;

using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;

using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Modules;

namespace AOM.Transport.Service.NewsProcessor
{
    internal class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<ISubscriptions>().To<Subscriptions>();
            kernel.Bind<IServiceManagement>().To<ServiceManagement>();
            kernel.Bind<IServiceInformation>().To<ServiceInformation>();
            kernel.Bind<INewsConfig>().To<NewsConfig>();
            kernel.Bind<ICrmModel>().To<CrmModel>();
            kernel.Bind<IOracleReader>().To<OracleReader>()
                .WithConstructorArgument("conn", ConfigurationManager.ConnectionStrings["ArgusCrmConnectionString"].ConnectionString);

            var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
            kernel.Bind<IBus>()
                .ToMethod(x=>Argus.Transport.Transport.CreateBus(aomConnectionConfiguration, null, x.Kernel.Get<IBusLogger>())).InSingletonScope()
                .Named("AomBus");

            var argusConnectionConfig = ConnectionConfiguration.FromConnectionStringName("argus.transport.bus");
            kernel.Bind<IBus>()
                .ToMethod(x => Argus.Transport.Transport.CreateBus(argusConnectionConfig, null, x.Kernel.Get<IBusLogger>())).InSingletonScope()
                .Named("ArgusDirectBus");

            kernel.Bind(x => x.FromThisAssembly()
                .IncludingNonePublicTypes()
                .SelectAllClasses()
                .InheritedFrom<IConsumer>()
                .BindAllInterfaces()
                .Configure(b => b.InThreadScope())); // InThreadScope used as orginal code used 'ToConstant'   
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}