﻿using System;
using System.Threading.Tasks;
using Topshelf;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.NewsProcessor
{
    using System.Collections.Generic;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Ninject;

    class NewsProcessorService : ProcessorServiceBase
    {
        private readonly IBus _argusBus;

        public NewsProcessorService(IEnumerable<IConsumer> consumers, IServiceManagement serviceManagement, [Named("AomBus")] IBus aomBus, [Named("ArgusDirectBus")] IBus argusBus) :
            base(consumers, serviceManagement, aomBus)
        {
            _argusBus = argusBus;
        }

        public override bool Stop(HostControl hostControl)
        {
            if (_argusBus != null)
            {
                StopWithinTimeout(() => _argusBus.Dispose(), "Argus Bus");
            }
            return base.Stop(hostControl);
        }

        public override string Name
        {
            get { return "NewsProcessorService"; }
        }
    }
}
