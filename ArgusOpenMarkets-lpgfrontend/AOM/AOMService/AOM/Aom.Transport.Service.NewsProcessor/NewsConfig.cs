﻿using System;
using System.Configuration;

namespace AOM.Transport.Service.NewsProcessor
{
    public class NewsConfig : INewsConfig
    {
        private const int DefaultHistoricalDaysGetNews = -4;

        public int HisoricalDaysGetNews
        {
            get
            {
                return GetHistoricalNews() ?? DefaultHistoricalDaysGetNews;
            }
        }

        private int? GetHistoricalNews()
        {
            var historicalDaysNews = ConfigurationManager.AppSettings["HistoricalDaysReplayNews"];

            int numberDays;
            if (Int32.TryParse(historicalDaysNews, out numberDays))
            {
                return numberDays;
            }

            return null;
        }
    }
}
