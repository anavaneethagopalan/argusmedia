﻿using AOM.Transport.Service.Processor.Common;
using System.Collections.Generic;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.AssessmentProcessor
{
    public class AssessmentServiceProcessor : ProcessorServiceBase
    {
        public AssessmentServiceProcessor(
            IServiceManagement serviceManagement,
            IEnumerable<IConsumer> consumers,
            IBus aomBus)
            : base(consumers, serviceManagement, aomBus)
        {
        }

        public override string Name
        {
            get { return "AssessmentServiceProcessor"; }
        }
    }
}