﻿using AOM.Transport.Service.AssessmentProcessor.Internals;
using AOM.Transport.Service.Processor.Common;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Modules;

namespace AOM.Transport.Service.AssessmentProcessor
{
    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<ISubscriptions>().To<Subscriptions>();
            kernel.Bind<IServiceManagement>().To<ServiceManagement>();
            kernel.Bind<IServiceInformation>().To<ServiceInformation>();

            kernel.Bind(
                x =>
                    x.FromThisAssembly()
                        .IncludingNonePublicTypes()
                        .SelectAllClasses()
                        .InheritedFrom<IConsumer>()
                        .BindAllInterfaces()
                        .Configure(b => b.InThreadScope()));
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}