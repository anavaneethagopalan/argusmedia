﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Services.AssessmentService;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.AssessmentProcessor.Consumers
{
    internal class AssessmentConsumer : IConsumer
    {
        private readonly IBus _aomBus;

        private readonly IAssessmentService _assessmentService;

        private readonly IDateTimeProvider _dateTimeProvider;

        private readonly IDbContextFactory _dbFactory;
        private readonly IErrorService _errorService;

        private readonly IProductService _productService;

        public AssessmentConsumer(
            IBus aomBus,
            IAssessmentService assessmentService,
            IProductService productService,
            IDateTimeProvider dateTimeProvider,
            IDbContextFactory dbFactory,
            IErrorService errorService)
        {
            _aomBus = aomBus;
            _assessmentService = assessmentService;
            _dateTimeProvider = dateTimeProvider;
            _dbFactory = dbFactory;
            _errorService = errorService;
            _productService = productService;
        }

        public void Start()
        {
            _aomBus.Subscribe<AomClearAssessmentRequest>(SubscriptionId, ConsumeClearAssessmentRequest);
            _aomBus.Subscribe<AomRefreshAssessmentRequest>(SubscriptionId, ConsumeRefreshAssessmentRequest);
            _aomBus.Subscribe<AomNewAssessmentRequest>(SubscriptionId, ConsumeInsertAssessmentRequest);
            _aomBus.Subscribe<AomAssessmentAuthenticationResponse>(SubscriptionId, ConsumeAuthenticationResponse);
        }

        private void ConsumeClearAssessmentRequest(AomClearAssessmentRequest aomClearAssessmentRequest)
        {
            if (aomClearAssessmentRequest != null)
            {
                Log.Debug(
                    string.Format(
                        "Aom Clear Assessment Request - for the Product: {0}  and the Period:{1}",
                        aomClearAssessmentRequest.ProductId,
                        aomClearAssessmentRequest.Period));

                long userId = aomClearAssessmentRequest.LastUpdatedUserId;
                if (aomClearAssessmentRequest != null)
                {
                    if (aomClearAssessmentRequest.ClientSessionInfo != null)
                    {
                        userId = aomClearAssessmentRequest.ClientSessionInfo.UserId;
                    }
                }

                var combinedAssessment = _assessmentService.ClearAssessment(
                    aomClearAssessmentRequest.ProductId,
                    aomClearAssessmentRequest.Period,
                    userId);

                if (combinedAssessment != null)
                {
                    var response = new AomNewAssessmentResponse
                    {
                        ProductId = aomClearAssessmentRequest.ProductId,
                        Assessment = combinedAssessment,
                        Message =
                            new Message
                            {
                                MessageBody = null,
                                MessageType = MessageType.Assessment,
                                MessageAction = MessageAction.Update,
                                ClientSessionInfo =
                                    aomClearAssessmentRequest
                                        .ClientSessionInfo
                            }
                    };

                    _aomBus.Publish(response);
                }

            }
        }

        internal void ConsumeInsertAssessmentRequest(AomNewAssessmentRequest request)
        {
            Log.Debug(string.Format("{0} received", request.EventName));
            try
            {
                _aomBus.Publish(
                    new AomAssessmentAuthenticationRequest
                    {
                        MessageAction = MessageAction.Create,
                        Assessment = request.Assessment,
                        ClientSessionInfo = request.ClientSessionInfo,
                        ProductId = request.ProductId
                    });
            }
            catch (Exception ex)
            {
                var logError =
                    _errorService.LogUserError(new UserError
                    {
                        Error = ex,
                        AdditionalInformation = "assessmentService.ConsumeInsertAssessmentRequest",
                        ErrorText = string.Empty,
                        Source = "AssessmentConsumer"
                    });

                _aomBus.Publish(
                    new AomNewAssessmentResponse
                    {
                        ProductId = request.ProductId,
                        Assessment = null,
                        Message =
                            new Message
                            {
                                MessageBody =
                                    logError,
                                MessageType = MessageType.Error,
                                MessageAction = MessageAction.Update,
                                ClientSessionInfo = request.ClientSessionInfo
                            }
                    });
            }
        }

        internal void ConsumeRefreshAssessmentRequest(AomRefreshAssessmentRequest request)
        {
            Log.Debug(string.Format("{0} received", request.EventName));

            var requestedProduct = _productService.GetProduct(request.ProductId);
            if (!requestedProduct.IsInternal && !requestedProduct.IsDeleted)
            {
                try
                {
                    _aomBus.Publish(
                        new AomAssessmentAuthenticationRequest
                        {
                            MessageAction = MessageAction.Refresh,
                            ClientSessionInfo = request.ClientSessionInfo,
                            ProductId = request.ProductId
                        });
                }
                catch (Exception ex)
                {
                    var logError =
                        _errorService.LogUserError(new UserError
                        {
                            Error = ex,
                            AdditionalInformation = "assessmentService.ConsumeRefreshAssessmentRequest",
                            ErrorText = string.Empty,
                            Source = "AssessmentConsumer"
                        });

                    _aomBus.Publish(
                        new AomNewAssessmentResponse
                        {
                            ProductId = request.ProductId,
                            Assessment = null,
                            Message =
                                new Message
                                {
                                    MessageBody =
                                        logError,
                                    MessageType = MessageType.Error,
                                    MessageAction = MessageAction.Update,
                                    ClientSessionInfo = request.ClientSessionInfo
                                }
                        });
                }
            }

        }

        internal void ConsumeAuthenticationResponse(AomAssessmentAuthenticationResponse response)
        {
            Log.Debug(String.Format("{0} received", response.EventName));
            try
            {
                if (response.Message.MessageType == MessageType.Error)
                {
                    var errorMessage = response.Message.MessageBody as string;
                    Log.ErrorFormat("Error: |{0}| for Product Id: {1}", errorMessage, response.ProductId);

                    PublishErrorResponse(
                        response.Message.MessageAction,
                        response.ProductId,
                        response.Message.ClientSessionInfo,
                        errorMessage);
                    return;
                }

                switch (response.Message.MessageAction)
                {
                    case MessageAction.Create:
                        ProcessInsertAssessment(
                            response.ProductId,
                            response.Message.ClientSessionInfo,
                            response.Assessment);
                        break;
                    case MessageAction.Refresh:
                        ProcessRefreshAssessment(response.ProductId, response.Message.ClientSessionInfo);
                        break;
                    default:
                        throw new ArgumentException(
                            string.Format(
                                "Message action {0} not supported for Assessment",
                                response.Message.MessageAction));
                }
            }
            catch (Exception exception)
            {
                var logError =
                    _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation =
                            String.Format("Consume {0}/{1}", response.EventName, response.Message.MessageAction),
                        ErrorText = string.Empty,
                        Source = "AssessmentConsumer"
                    });


                var errMessage = string.Empty;
                if (logError != null)
                {
                    errMessage = logError;
                }

                PublishErrorResponse(
                    response.Message.MessageAction,
                    response.ProductId,
                    response.Message.ClientSessionInfo,
                    errMessage);
            }
        }

        private void ProcessInsertAssessment(long productId, ClientSessionInfo csi, CombinedAssessment assessment)
        {
            _assessmentService.UpdateCombinedAssessment(assessment);
            ProcessRefreshAssessment(productId, csi);
        }

        private void ProcessRefreshAssessment(long productId, ClientSessionInfo csi)
        {
            CombinedAssessment assessment = null;
            try
            {
                assessment = _assessmentService.GetCombinedAssessmentForProduct(productId);
            }
            catch (Exception ex)
            {
                _errorService.LogUserError(new UserError
                {
                    Error = ex,
                    AdditionalInformation = "assessmentService.GetCombinedAssessmentForProduct",
                    ErrorText = string.Empty,
                    Source = "AssessmentConsumer"
                });
            }
            if (assessment == null)
            {
                Log.Info("In ConsumeRefreshAssessmentRequest, no assessments found in DB, exiting...");
                return;
            }
            if (assessment.Today != null && assessment.Previous != null)
            {
                var response = new AomNewAssessmentResponse
                {
                    ProductId = assessment.ProductId,
                    Assessment = assessment,
                    Message =
                        new Message
                        {
                            MessageBody = null,
                            MessageType = MessageType.Assessment,
                            MessageAction = MessageAction.Update,
                            ClientSessionInfo = csi
                        }
                };

                _aomBus.Publish(response);
            }

                // no assessments in DB -send back a dummy assessment
                // this is a fail-safe mechanism to in case of db failure or no assessments in DB
            else
            {
                if (assessment.Today == null)
                {
                    assessment.Today = new Assessment
                    {
                        BusinessDate = _dateTimeProvider.UtcNow.Date,
                        PriceHigh = 0,
                        PriceLow = 0,
                        LastUpdated = _dateTimeProvider.UtcNow,
                        LastUpdatedUserId = -1,
                        ProductId = productId,
                        DateCreated = _dateTimeProvider.UtcNow,
                        EnteredByUserId = -1,
                        AssessmentStatus = AssessmentStatus.None
                    };

                    // _assessmentService.InsertSingleAssessment(assessment.Today, true);
                }
                if (assessment.Previous == null)
                {
                    //previous is null - create one in the DB
                    assessment.Previous = assessment.Today.CopyAssessment();
                    assessment.Previous.BusinessDate = _dateTimeProvider.UtcNow.AddDays(-1).Date;
                    assessment.Previous.PriceHigh = 0;
                    assessment.Previous.PriceLow = 0;

                    // _assessmentService.InsertSingleAssessment(assessment.Previous, true);
                }

                PublishResponse(productId, assessment);
            }
        }

        private void PublishResponse(long productId, CombinedAssessment assessment)
        {
            var response = new AomNewAssessmentResponse
            {
                ProductId = productId,
                Assessment = assessment,
                Message =
                    new Message
                    {
                        MessageBody = null,
                        MessageType = MessageType.Assessment,
                        MessageAction = MessageAction.Update
                    }
            };

            _aomBus.Publish(response);
        }

        private void PublishErrorResponse(
            MessageAction messageAction,
            long productId,
            ClientSessionInfo csi,
            string errorMessage)
        {
            var response = new AomNewAssessmentResponse
            {
                ProductId = productId,
                Message =
                    new Message
                    {
                        ClientSessionInfo = csi,
                        MessageAction = messageAction,
                        MessageType = MessageType.Error,
                        MessageBody = errorMessage
                    },
            };
            _aomBus.Publish(response);
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}