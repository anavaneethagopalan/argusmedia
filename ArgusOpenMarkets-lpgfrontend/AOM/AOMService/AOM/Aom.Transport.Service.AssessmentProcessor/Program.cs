﻿using System;
using System.Diagnostics;
using System.Linq;
using AOM.Repository.MySql;
using AOM.Services;
using AOM.Transport.Service.Processor.Common;
using Utils.Logging.Utils;
using Ninject;

namespace AOM.Transport.Service.AssessmentProcessor
{
    public class Program
    {
        internal static void Main()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(
                    new EFModelBootstrapper(),
                    new LocalBootstrapper(),
                    new ServiceBootstrap(),
                    new ProcessorCommonBootstrap());

                WarmUp(kernel);
                ServiceHelper.BuildAndRunService<AssessmentServiceProcessor>(kernel);
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (var dbCrm = dbFactory.CreateCrmModel())
                {
                    dbCrm.UserInfoes.Any();
                }

                using (var dbAom = dbFactory.CreateAomModel())
                {
                    dbAom.Assessments.Any();
                }

                Log.Info(string.Format("AssessmentProcessor - Initial EF model build took {0} msecs.",
                    timer.ElapsedMilliseconds));

            }
            catch (Exception ex)
            {
                Log.Error("AssessmentProcessor - WarmUp - Error - cannot connect to DB", ex);
                ServiceHelper.BuildAndRunService<AssessmentServiceProcessor>(kernel);
            }
        }
    }
}