﻿using System.Net;
using AOM.AdminService.Services;
using Microsoft.Owin.Hosting;
using System;
using System.Configuration;
using Utils.Logging.Utils;

namespace AOM.AdminService
{
    public class AomAdminService
    {
        private IDisposable _webServer;

        public void OnStart()
        {
            try
            {
                string webServicePort = ConfigurationManager.AppSettings["webserviceport"];
                string ipAddress = ConfigurationManager.AppSettings["webservicebaseaddress"];

                if (string.IsNullOrEmpty(webServicePort))
                {
                    webServicePort = "8012";
                }

                if (string.IsNullOrEmpty(ipAddress))
                {
                    ipAddress = GetIPAddress();
                }
                var webServiceBaseAddress = String.Format("http://{0}:{1}/", ipAddress, webServicePort);

                _webServer = WebApp.Start<Startup>(webServiceBaseAddress);
                Log.Debug("AOM.AdminService.OnStart - " + String.Format("Service has started listening on:{0}", webServiceBaseAddress));
            }
            catch (Exception ex)
            {
                Log.Error("AOM.AdminService.OnStart - Exception", ex);
            }
        }

        public void OnStop()
        {
            if (_webServer != null)
            {
                _webServer.Dispose();
            }
        }

        private string GetIPAddress()
        {
            var localIp = string.Empty;
            try
            {
                IPHostEntry host;
                localIp = "?";
                host = Dns.GetHostEntry(Dns.GetHostName());

                foreach (var ip in host.AddressList)
                {
                    Log.InfoFormat("IP Address: {0} / Address Family: {1}", ip.ToString(), ip.AddressFamily.ToString());
                }

                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily.ToString() == "InterNetwork")
                    {
                        localIp = ip.ToString();
                        break;
                    }
                }
            }

            catch (Exception ex)
            {
                Log.Error("AOM.AdminService.GetIPAddress - Exception", ex);
            }
            return localIp;
        }
    }
}