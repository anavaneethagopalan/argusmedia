﻿using System;
using AOM.AdminService.Domain;
using AOM.AdminService.Helpers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.CrmService;
using AOM.Services.ProductService;
using AOM.Transport.Events.Users;
using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;
using EasyNetQ.Events;
using AOM.Services.EncryptionService;
using Utils.Logging.Utils;

namespace AOM.AdminService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuthenticationController : ApiController
    {
        protected readonly IUserService _userService;

        protected readonly IEncryptionService _encryptionService;

        protected readonly IUserAuthenticationService _userAuthenticationService;

        protected readonly IBus _aomBus;

        public AuthenticationController(IUserService userService, IEncryptionService encryptionService, IBus aomBus)
        {
            _userService = userService;
            _encryptionService = encryptionService;
            _aomBus = aomBus;
        }

        public AuthenticationController()
        {
            var dateTimeProvider = new DateTimeProvider();
            var dbContextFactory = new DbContextFactory();
            var productService = new ProductService(dbContextFactory, dateTimeProvider);
            _userService = new UserService(dbContextFactory, dateTimeProvider, productService);
            _encryptionService = new EncryptionService();

            var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
            _aomBus = Argus.Transport.Transport.CreateBus(aomConnectionConfiguration);
        }

        // POST api/values 
        public HttpResponseMessage Post([FromBody] AdminUser user)
        {
            bool authenticated = false;

            if (user != null)
            {
                Log.Debug("AuthenticationController.Post - Authentication Request");

                var userObject = new User {Id = _userService.GetUserId(user.Username)};
                var hashedPassword = _encryptionService.Hash(userObject, user.Password);

                var authResult =
                    _aomBus.Request<UserAuthenticationRequestRpc, AuthenticationResult>(
                        new UserAuthenticationRequestRpc
                        {
                            IpAddress = "0.0.0.0",
                            Password = hashedPassword,
                            Username = user.Username
                        });

                Log.DebugFormat("AuthenticationController.Post - Result for User{0} is: {1}", user.Username, authResult.IsAuthentiacted);
                authenticated = authResult.IsAuthentiacted;
            }

            var authJsonResult = new AuthenticationResult
            {
                IsAuthentiacted = authenticated
            };

            return HttpResponseHelper.JsonResponse(authJsonResult.ToJsonCamelCase(), Configuration.Formatters.JsonFormatter);
        }

        // GET api/values 
        public HttpResponseMessage Get()
        {
            try
            {
                Log.Debug("UsersController.GetUsers - Initial Call");
                var result = new User {Username = "Nathan", Id = 100};

                return HttpResponseHelper.JsonResponse(result.ToJsonCamelCase(), Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                Log.Error("UsersController.GetUsers", ex);
            }

            return null;
        }
    }

    public class AuthenticationResult
    {
        public bool IsAuthentiacted { get; set; }
    }
}