﻿using System.Linq;
using AOM.AdminService.Helpers;
using AOM.AdminService.Services;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Utils.Logging.Utils;

namespace AOM.AdminService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DealsController : ApiController
    {
        protected readonly IDealsAdminService _dealsAdminService;

        public DealsController(IDealsAdminService dealsAdminService)
        {
            _dealsAdminService = dealsAdminService;
        }

        public DealsController()
        {

            _dealsAdminService = new DealsAdminService();
        }

        // GET api/values 
        public HttpResponseMessage Get()
        {
            Log.Debug("DealsController.GetDeals - Initial Call");
            var deals = _dealsAdminService.GetDeals();

            return HttpResponseHelper.JsonResponse(deals.ToJsonCamelCase(), Configuration.Formatters.JsonFormatter);
        }
    }
}