﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AOM.AdminService.Domain;
using AOM.AdminService.Helpers;
using AOM.AdminService.Services;

namespace AOM.AdminService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserActionController : ApiController
    {
        private const string ResetUserPasswordAction = "reset";
        private const string TerminateUserAction = "terminate";
        private const string BlockUserAction = "block";

        protected readonly IUserAdminService _userAdminService;

        protected readonly IAdminAuditService _adminAuditService;

        public UserActionController(IUserAdminService userService, IAdminAuditService adminAuditService)
        {
            _userAdminService = userService;
            _adminAuditService = adminAuditService;
        }

        public UserActionController()
        {
            _userAdminService = new UserAdminService();
            _adminAuditService = new AdminAuditService();
        }

        // GET api/values 
        public HttpResponseMessage Get()
        {
            var actions = new List<string>();

            try
            {
                actions.Add(TerminateUserAction);
                actions.Add(BlockUserAction);
                actions.Add(ResetUserPasswordAction);

                return HttpResponseHelper.JsonResponse(actions.ToJsonCamelCase(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _adminAuditService.AuditAction("UserActionController.GetUser", "", ex);
            }

            return null;
        }


        // POST api/values 
        public void Post([FromBody] AdminAction adminAction)
        {
            if (adminAction != null)
            {
                switch (adminAction.Action.ToLower())
                {
                    case ResetUserPasswordAction:
                        _userAdminService.ResetUserPassword(adminAction.AdminUser);
                        break;
                    case TerminateUserAction:
                        _userAdminService.TerminateUser(adminAction.AdminUser);
                        break;
                    case BlockUserAction:
                        _userAdminService.BlockUser(adminAction.AdminUser);
                        break;
                }
            }
        }

        // PUT api/values/5 
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5 
        public void Delete(int id)
        {
        }
    }
}