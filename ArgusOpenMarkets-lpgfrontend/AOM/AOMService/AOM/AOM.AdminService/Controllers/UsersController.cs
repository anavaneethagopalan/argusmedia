﻿using AOM.AdminService.Domain;
using AOM.AdminService.Helpers;
using AOM.AdminService.Services;
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Utils.Logging.Utils;

namespace AOM.AdminService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        protected readonly IUserAdminService _userAdminService;

        protected readonly IAdminAuditService _AdminAuditService;

        public UsersController(IUserAdminService userService, IAdminAuditService adminAuditService)
        {
            _userAdminService = userService;
            _AdminAuditService = adminAuditService;
        }

        public UsersController()
        {
            _userAdminService = new UserAdminService();
            _AdminAuditService = new AdminAuditService();
        }

        // GET api/values 
        public HttpResponseMessage Get()
        {
            try
            {
                Log.Debug("UsersController.GetUsers - Initial Call");
                var users = _userAdminService.GetUsers();
                Log.Debug("UsersController.GetUsers" + string.Format("Returning: {0} users", users.Count));

                return HttpResponseHelper.JsonResponse(users.ToJsonCamelCase(), Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                Log.Error("UsersController.GetUsers", ex);
            }

            return null;
        }

        //        // GET api/values/5 
        //        public string Get(int id)
        //        {
        //            // TODO: We need to return a list of users....
        //            return "value";
        //        }

        // POST api/values 
        public void Post([FromBody] AdminUser user)
        {
            if (user != null)
            {
                // TODO: We need to place a message on the bus to reset the users password.
                _userAdminService.ResetUserPassword(user);
            }
        }

        // PUT api/values/5 
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5 
        public void Delete(int id)
        {
        }
    }
}