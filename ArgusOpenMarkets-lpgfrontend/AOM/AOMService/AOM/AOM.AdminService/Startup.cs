﻿using System;
using System.Web.Http;
using AOM.AdminService.Services;
using Owin;

namespace AOM.AdminService
{
    public class Startup
    {
        protected readonly IAdminAuditService _adminAuditService;

        public Startup(IAdminAuditService adminAuditService)
        {
            _adminAuditService = adminAuditService;
        }

        public Startup()
        {
            _adminAuditService =new AdminAuditService();
        }
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {

            try
            {
                // Configure Web API for self-host. 
                HttpConfiguration config = new HttpConfiguration();

                // Enable CORS request on this service.
                config.EnableCors();

                config.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "api/{controller}/{id}",
                    defaults: new {id = RouteParameter.Optional}
                    );

                appBuilder.UseWebApi(config);

            }
            catch (Exception ex)
            {
                _adminAuditService.AuditAction("Startup.Start", "", ex);
            }
        }
    }
}