﻿using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AOM.AdminService.Helpers
{
    public static class ExtensionMethods
    {
        public static string ToJsonCamelCase<T>(this T arg)
        {
            var result = JsonConvert.SerializeObject(arg, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ContractResolver = new JsonContractResolver()
                });

            return result;
        }
    }

    public class JsonContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            property.ShouldSerialize = instance => true;
            return property;
        }
    }
}