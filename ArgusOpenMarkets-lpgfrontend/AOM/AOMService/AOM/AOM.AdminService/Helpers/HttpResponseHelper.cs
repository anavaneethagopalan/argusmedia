﻿using System.Net.Http;
using System.Net.Http.Formatting;

namespace AOM.AdminService.Helpers
{
    public static class HttpResponseHelper
    {
        public static HttpResponseMessage JsonResponse(string json, MediaTypeFormatter mediaTypeFormatter)
        {
            var content = new ObjectContent<string>(json, mediaTypeFormatter);
            return new HttpResponseMessage()
            {
                Content = content
            };
        }
    }
}
