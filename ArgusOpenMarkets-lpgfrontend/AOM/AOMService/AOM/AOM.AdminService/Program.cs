﻿using Topshelf;

namespace AOM.AdminService
{
    public class Program
    {
        private static void Main()
        {

            HostFactory.Run(configuration =>
            {
                configuration.Service<AomAdminService>(
                    service =>
                    {
                        service.ConstructUsing(x => new AomAdminService());
                        service.WhenStarted(x => x.OnStart());
                        service.WhenStopped(x => x.OnStop());
                    });

                configuration.RunAsLocalSystem();
                configuration.SetServiceName("AomAdminService");
                configuration.SetDisplayName("Aom Admin Service - OWIN Self Hosted");
                configuration.SetDescription("Aom Admin Service 2.0");
            });


        }
    }
}