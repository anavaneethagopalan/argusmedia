﻿using System;

namespace AOM.AdminService.Domain
{
    public class AdminUser
    {
        public bool IsAuthenticated { get; set; }
        public long Id { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public string Name { get; set; }
        public string Organisation { get; set; }
        public bool IsActive { get; set; }
        public bool IsBlocked { get; set; }
        public DateTime? LastSeen { get; set; }
    }
}