﻿namespace AOM.AdminService.Domain
{
    public class AdminAction
    {
        public string Action { get; set; }

        public AdminUser AdminUser { get; set; }
    }
}