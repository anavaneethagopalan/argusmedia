﻿using System.Linq;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using System.Collections.Generic;
using AOM.Repository.MySql.Crm;

namespace AOM.AdminService.Services
{
    public class DealsAdminService : IDealsAdminService
    {
        protected readonly IDbContextFactory _dbContextFactory;

        public DealsAdminService(IDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public DealsAdminService()
        {
            _dbContextFactory = new DbContextFactory();
        }

        public IList<DealDto> GetDeals()
        {
            using (IAomModel aomModel = _dbContextFactory.CreateAomModel())
            {
                var deals = aomModel.Deals.ToList();
                return deals;
            }
        }
    }
}


