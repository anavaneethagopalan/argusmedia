﻿using AOM.Repository.MySql.Aom;
using System.Collections.Generic;

namespace AOM.AdminService.Services
{
    public interface IDealsAdminService
    {
        IList<DealDto> GetDeals();
    }
}
