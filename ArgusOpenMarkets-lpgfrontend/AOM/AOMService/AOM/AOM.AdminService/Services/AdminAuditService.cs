﻿using System;
using System.Configuration;
using AOM.App.Domain.Entities;
using AOM.Services.EmailService;

namespace AOM.AdminService.Services
{
    public class AdminAuditService : IAdminAuditService
    {
        private const string DefaultRecipient = "nathan.bellamore@argusmedia.com";

        private const string UnknownEnvironment = "UNKNOWN";

        private string _environment = string.Empty;

        protected readonly ISmtpService _smtpService;

        public AdminAuditService(ISmtpService smtpService)
        {
            _smtpService = smtpService;
        }

        public AdminAuditService()
        {
            _smtpService = new SmtpService();
        }

        public void AuditAction(string action, string detail, Exception ex)
        {
            // NOTE: May want to just log this - hence the override
            SendEmail(action, detail + ex.ToString());
        }

        public void AuditAction(string action, string detail)
        {
            SendEmail(action, detail);
        }

        private void SendEmail(string action, string detail)
        {
            var recipient = ConfigurationManager.AppSettings["auditadminrecipient"];
            if (string.IsNullOrEmpty(recipient))
            {
                recipient = DefaultRecipient;
            }

            action = string.Format("({0})-{1}", GetEnvironment(), action);
            var emailToSend = new Email
            {
                Body = detail,
                Recipient = recipient,
                Subject = action
            };

            _smtpService.SendEmail(emailToSend);
        }


        public string GetEnvironment()
        {
            if (string.IsNullOrEmpty(_environment))
            {
                _environment = ConfigurationManager.AppSettings["environment"];
                if (string.IsNullOrEmpty(_environment))
                {
                    _environment = UnknownEnvironment;
                }
            }

            return _environment;
        }
    }
}
