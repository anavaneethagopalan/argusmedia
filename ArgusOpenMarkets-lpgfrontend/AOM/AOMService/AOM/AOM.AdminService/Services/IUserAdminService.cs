﻿using System.Collections.Generic;
using AOM.AdminService.Domain;

namespace AOM.AdminService.Services
{
    public interface IUserAdminService
    {
        List<AdminUser> GetUsers();

        void ResetUserPassword(AdminUser user);

        void TerminateUser(AdminUser user);

        void BlockUser(AdminUser adminUser);
    }
}
