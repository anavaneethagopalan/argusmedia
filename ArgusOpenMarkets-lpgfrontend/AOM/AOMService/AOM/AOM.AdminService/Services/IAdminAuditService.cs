﻿using System;

namespace AOM.AdminService.Services
{
    public interface IAdminAuditService
    {
        void AuditAction(string action, string detail);
        void AuditAction(string action, string detail, Exception ex);

        string GetEnvironment();
    }
}
