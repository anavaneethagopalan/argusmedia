﻿using System;

namespace AOM.AdminService.Services
{
    public class RandomPasswordGenerator : IRandomPasswordGenerator
    {
        public string GenerateRandomPassword()
        {
            var pwd = Guid.NewGuid().ToString();
            return pwd.Substring(0, 8);
        }
    }
}