﻿namespace AOM.AdminService.Services
{
    public interface IRandomPasswordGenerator
    {
        string GenerateRandomPassword();
    }
}
