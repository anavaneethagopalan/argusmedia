﻿using System.Text;
using AOM.AdminService.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Services.CrmService;
using AOM.Services.EmailService;
using AOM.Transport.Events;
using AOM.Transport.Events.Users;
using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using AOM.Services.ProductService;
using AOM.Services.EncryptionService;

namespace AOM.AdminService.Services
{
    public class UserAdminService : IUserAdminService
    {
        private const string ResetPasswordAction = "AOM Admin Tool - Password Reset";

        protected readonly IBus _aomBus;
        private IUserService _userService;
        private IOrganisationService _organisationService;
        private readonly IUserTransformationService _userTransformationService;
        private readonly IEncryptionService _encryptionService;
        private readonly IRandomPasswordGenerator _randomPasswordGenerator;
        private readonly IAdminAuditService _adminAuditService;

        public UserAdminService(IBus aomBus, IUserService userService, IOrganisationService organisationService, IEncryptionService encryptionService,
            IUserTransformationService userTransformationService, IRandomPasswordGenerator randomPasswordGenerator, IAdminAuditService adminAuditService)
        {
            _aomBus = aomBus;
            _userService = userService;
            _organisationService = organisationService;
            _encryptionService = encryptionService;
            _userTransformationService = userTransformationService;
            _randomPasswordGenerator = randomPasswordGenerator;
            _adminAuditService = adminAuditService;
        }

        public UserAdminService()
        {
            var conn = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
            _aomBus = Argus.Transport.Transport.CreateBus(conn, null, null);
            var dbContextFactory = new DbContextFactory();
            var dateTimeProvider = new DateTimeProvider();
            var productService = new ProductService(dbContextFactory, dateTimeProvider);
            _userService = new UserService(new DbContextFactory(), new DateTimeProvider(), productService);
            _organisationService = new OrganisationService(new DbContextFactory(), new BilateralPermissionsBuilder());
            _encryptionService = new EncryptionService();
            _userTransformationService = new UserTransformationService();
            _randomPasswordGenerator = new RandomPasswordGenerator();
            _adminAuditService = new AdminAuditService();
        }

        public List<AdminUser> GetUsers()
        {
            var adminUsers = new List<AdminUser>();

            try
            {
                var users = _userService.GetUsers(-1);
                var organisations = _organisationService.GetOrganisations();
                var loggedOnUsers = GetLoggedOnUsers();
                adminUsers = _userTransformationService.TransformUsers(users, organisations, loggedOnUsers);
            }
            catch (Exception ex)
            {
                _adminAuditService.AuditAction("UserAdminService.GetUsers", "", ex);
            }

            return adminUsers;
        }

        private List<LoggedOnUserInfo> GetLoggedOnUsers()
        {
            var loggedOnUsers = new List<LoggedOnUserInfo>();
            var getLoggedOnUsersRequest = new GetLoggedOnUsersRequest();

            try
            {
                var loggedOnUsersResponse = _aomBus.Request<GetLoggedOnUsersRequest, GetLoggedOnUsersResponse>(getLoggedOnUsersRequest);
                loggedOnUsers = loggedOnUsersResponse.LoggedOnUsers;
            }
            catch (Exception ex)
            {
                _adminAuditService.AuditAction("UserAdminService.GetLoggedOnUsers", "", ex);
            }

            return loggedOnUsers;
        }

        public void ResetUserPassword(AdminUser user)
        {
            if (user != null)
            {
                var passwordExpireHours = 48;

                string temporaryPassword = _randomPasswordGenerator.GenerateRandomPassword();
                var hashedPassword = _encryptionService.Hash(user.Id, temporaryPassword);

                var request = new ChangePasswordRequestRpc
                {
                    Username = user.Username,
                    AdminResetPassword = true,
                    IsTemporary = true,
                    NewPassword = hashedPassword,
                    OldPassword = hashedPassword,
                    CheckOldPassword = false,
                    RequestorUserId = 5062,
                    TempPassExpirInHours = passwordExpireHours
                };

                var changePwdResponse = string.Empty;
                var changePasswordResponse = _aomBus.Request<ChangePasswordRequestRpc, ChangePasswordResponse>(request);
                if (changePasswordResponse != null)
                {
                    changePwdResponse = changePasswordResponse.MessageBody;
                }

                AuditResetUserPassword(user, temporaryPassword, changePwdResponse);

                if (changePwdResponse.ToLower() == "success")
                {
                    // We have successfully changed the users password.  
                    SendEmailToUserStatingPasswordReset(user, temporaryPassword, passwordExpireHours);
                }
            }
        }

        private void SendEmailToUserStatingPasswordReset(AdminUser user, string temporaryPassword, int passwordExpireHours)
        {
            var messageBody = new StringBuilder();
            var expiryDate = DateTime.UtcNow.AddHours(passwordExpireHours);

            messageBody.AppendEmailLine("Dear  {0},", user.Name);
            messageBody.AppendBlankEmailLine();
            messageBody.AppendEmailLine("We have we have received a password reset for your account with the user name: {0}", user.Username);

            messageBody.AppendEmailLine("You password has been reset to: {0}.  Please note this is a temporary password and the ability to login using this password will expire on: {1} UTC",
                    temporaryPassword, expiryDate);

            _adminAuditService.AuditAction("AOM - Your password has been reset.", messageBody.ToString());
        }

        private void AuditResetUserPassword(AdminUser user, string temporaryPassword, string changePwdResponse)
        {
            var messageBody = new StringBuilder();

            messageBody.AppendEmailLine(String.Format("Reset User Password requested for the environment: {0}", _adminAuditService.GetEnvironment()));
            messageBody.AppendBlankEmailLine();
            messageBody.AppendEmailLine("We have reset the password for the user: {0}", user.Username);
            messageBody.AppendEmailLine("The password has been reset to: {0}", temporaryPassword);
            messageBody.AppendEmailLine("The password was reset on: {0} UTC", DateTime.UtcNow);
            messageBody.AppendEmailLine("The response from the service was: {0}", changePwdResponse);

            _adminAuditService.AuditAction(ResetPasswordAction, messageBody.ToString());
        }

        public void TerminateUser(AdminUser user)
        {
            try
            {
                var terminateUserRequest = new TerminateUserRequest {UserName = user.Username};
                _aomBus.Publish(terminateUserRequest);

                var body = string.Format("We have terminated the user: {0} (disconnected the user session), on: {1}", user.Username, DateTime.Now);
                var action = "AOM Admin Tool - Terminate User";
                _adminAuditService.AuditAction(action, body);
            }
            catch (Exception ex)
            {
                _adminAuditService.AuditAction("TerminateUser", "UserAdminService", ex);
            }
        }

        public void BlockUser(AdminUser user)
        {
            try
            {
                var request = new BlockUserRequestRpc {UserName = user.Username, RequestorUserId = -1};
                var blockUserResponse = _aomBus.Request<BlockUserRequestRpc, BlockUserResponse>(request);

                var blockUserMessage = string.Empty;
                if (blockUserResponse != null)
                {
                    blockUserMessage = blockUserResponse.MessageBody;
                }

                var body = string.Format("We have blocked the user: {0}, on: {1}.  The block user response message was: {2}", user.Username, DateTime.Now, blockUserMessage);
                var action = "AOM Admin Tool - Block User";
                _adminAuditService.AuditAction(action, body);

            }
            catch (Exception ex)
            {
                _adminAuditService.AuditAction("BlockUser", "UserAdminService", ex);
            }
        }
    }
}