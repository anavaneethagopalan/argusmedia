﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.AdminService.Domain;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;

namespace AOM.AdminService.Services
{
    public interface IUserTransformationService
    {
        List<AdminUser> TransformUsers(IList<User> users, IList<Organisation> organisations, List<LoggedOnUserInfo> loggedOnUsers);
    }

    public class UserTransformationService : IUserTransformationService
    {
        public List<AdminUser> TransformUsers(IList<User> users,
            IList<Organisation> organisations,
            List<LoggedOnUserInfo> loggedOnUsers)
        {
            List<AdminUser> adminUsers = new List<AdminUser>();

            if (users == null)
            {
                return adminUsers;
            }

            foreach (var user in users)
            {

                var orgName = string.Empty;
                if (user.UserOrganisation != null)
                {
                    orgName = user.UserOrganisation.Name;
                }

                if (string.IsNullOrEmpty(orgName))
                {
                    foreach (var org in organisations)
                    {
                        if (org.Id == user.OrganisationId)
                        {
                            orgName = org.Name;
                            break;
                        }
                    }
                }

                var adminUser = new AdminUser
                {
                    Id = user.Id,
                    IsActive = user.IsActive,
                    IsBlocked = user.IsBlocked, 
                    Name = user.Name,
                    Organisation = orgName,
                    Username = user.Username,
                    LastSeen = IsLoggedOn(user.Id, loggedOnUsers)
                };

                adminUsers.Add(adminUser);
            }

            return adminUsers.OrderByDescending(o => o.LastSeen).ToList();
        }

        private DateTime? IsLoggedOn(long userId, List<LoggedOnUserInfo> loggedOnUsers)
        {
            // Todo - work if user is logged in
            foreach (var user in loggedOnUsers)
            {
                if (user.UserId == userId)
                {
                    return user.LastSeen;
                }
            }
            return null;
        }
    }
}