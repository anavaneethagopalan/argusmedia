﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Utils.Logging.Utils;

namespace AOM.Repository.Oracle
{
    public class OracleReader : IDisposable, IOracleReader
    {
        private string ConnectionString { get; set; }
        private bool IsDisposed { get; set; }
        private OracleConnection Connection { get; set; }
        private OracleDataReader Reader { get; set; }

        public OracleReader(string conn)
        {
            ConnectionString = conn;
        }

        public IDataReader ExecuteSqlReader(string sql)
        {
            IsDisposed = false;
            Connection = new OracleConnection(ConnectionString);
            var command = new OracleCommand(sql, Connection);

            Connection.Open();
            Reader = command.ExecuteReader();

            return Reader;
        }

        #region IDisposable Implementation
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (IsDisposed)
                return;

            if (isDisposing)
            {
                if (Reader != null)
                {
                    Reader.Close();
                    Reader = null;
                }

                if (Connection != null)
                {
                    Connection.Close();
                    Connection = null;
                }
            }

            IsDisposed = true;
        }
        #endregion

        // TODO: Refactor to generics.
        public long ExecuteSqlLong(string sql)
        {
            IsDisposed = false;
            long result = 0;

            Connection = new OracleConnection(ConnectionString);
            var command = new OracleCommand(sql, Connection);
            Connection.Open();

            try
            {
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    result = reader.GetInt64(0);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
            }

            return result;
        }

        public string ExecuteSqlString(string sql)
        {
            IsDisposed = false;
            var result = string.Empty;

            Connection = new OracleConnection(ConnectionString);
            var command = new OracleCommand(sql, Connection);
            Connection.Open();

            try
            {
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    result = reader.GetString(0);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
            }

            return result;
        }
    }
}
