﻿using System.Data;

namespace AOM.Repository.Oracle
{
    using global::Oracle.ManagedDataAccess.Client;

    public interface IOracleReader
    {
        IDataReader ExecuteSqlReader(string sql);

        string ExecuteSqlString(string sql);

        long ExecuteSqlLong(string sql);
    }
}