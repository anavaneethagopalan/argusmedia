﻿namespace AOM.Repository.Orcle.Tests
{
    using System;

    using AOM.Repository.Oracle;
    using NUnit.Framework;


    [TestFixture]
    public class OracleReaderTests
    {
//        [Test]
//        public void GetReader()
//        {
//            try
//            {
//                using (
//                    var reader =
//                        new OracleReader(
//                            "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=web04.us.argusmedia.com)(PORT=1522))(CONNECT_DATA=(SERVICE_NAME=Master1.argus)));User ID=argus4_test;Password=argus4_test;")
//                    )
//                {
//                    Assert.IsInstanceOf<OracleDataReader>(reader.ExecuteSqlReader("SELECT * from V_AOM_USER_MODULE"));
//                }
//            }
//            catch (OracleException e)
//            {
//                if (e.Number == -1000)
//                    Assert.Inconclusive("Inconclusive due to db connection timeout");
//                else
//                    throw;
//            }
//        }

        [Test]
        public void GetReaderException()
        {
            using (var reader = new OracleReader(""))
            {
                Assert.Throws<InvalidOperationException>(() => reader.ExecuteSqlReader(""));
            }
        }

        [Test]
        public void Dispose()
        {
            using (var reader = new OracleReader(""))
            {
                Assert.DoesNotThrow(reader.Dispose);
            }
        }

//        [Test]
//        public void TestExecuteSqlStringShouldReturnData()
//        {
//            try
//            {
//                string story;
//
//                using (
//                    var reader =
//                        new OracleReader(
//                            "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=web04.us.argusmedia.com)(PORT=1522))(CONNECT_DATA=(SERVICE_NAME=Master1.argus)));User ID=argus4_test;Password=argus4_test;")
//                    )
//                {
//                    story = reader.ExecuteSqlString("SELECT Story from news where rownum < 2");
//
//                }
//
//                Assert.That(story, Is.Not.Null);
//            }
//            catch (OracleException e)
//            {
//                if (e.Number == -1000)
//                    Assert.Inconclusive("Inconclusive due to db connection timeout");
//                else
//                    throw;
//            }
//        }
        
//        [Test]
//        public void TestExecuteSqlLongShouldReturnData()
//        {
//            try
//            {
//                long id;
//                using (
//                    var reader =
//                        new OracleReader(
//                            "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=web04.us.argusmedia.com)(PORT=1522))(CONNECT_DATA=(SERVICE_NAME=Master1.argus)));User ID=argus4_test;Password=argus4_test;")
//                    )
//                {
//                    id = reader.ExecuteSqlLong("SELECT news_id from news where rownum < 2");
//
//                }
//
//                Assert.That(id, Is.GreaterThan(0));
//            }                  
//            catch (OracleException e)
//            {
//                if (e.Number == -1000)
//                    Assert.Inconclusive("Inconclusive due to db connection timeout");
//                else
//                    throw;
//            }
//        }
    }
}