﻿namespace Argus.Common.DataAccess
{
    using Ninject.Modules;

    public class DataAccessModule : NinjectModule
    {
        private readonly string connectionString;

        public DataAccessModule(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public override void Load()
        {
            Bind<IDataAccessHelper>().To<DataAccessHelper>()
                                     .InSingletonScope()
                                     .WithConstructorArgument("connectionString", connectionString);
        }
    }
}