﻿namespace Argus.Common.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    using MySql.Data.MySqlClient;

	public interface IDataAccessHelper
    {
        IEnumerable<T> ExecuteProc<T>(string storedProcName);

        IEnumerable<T> ExecuteProc<T>(string storedProcName, MySqlParameter[] parameters);

		IEnumerable<T1> ExecuteProc<T1, T2>(string storedProcName, Func<T1, T2, T1> funcMap, MySqlParameter[] parameters, string splitOn);

		IEnumerable<T1> ExecuteProc<T1, T2, T3>(string storedProcName, Func<T1, T2, T3, T1> funcMap, MySqlParameter[] parameters, string splitOn);

		IEnumerable<T1> ExecuteProc<T1, T2, T3, T4>(string storedProcName, Func<T1, T2, T3, T4, T1> funcMap, MySqlParameter[] parameters, string splitOn);

		void ExecuteProc(string storedProcName, MySqlParameter[] parameters);

        IEnumerable<T> ExecuteSql<T>(string sql, object param = null);

        IEnumerable<T> ExecuteSql<T, T2>(string sql, Func<T, T2, T> func, string splitParam, object param = null);

        IEnumerable<T> ExecuteSql<T, T2, T3>(string sql, Func<T, T2, T3, T> func, string splitParam, object param = null);

        IEnumerable<T> ExecuteSql<T, T2, T3, T4>(string sql, Func<T, T2, T3, T4, T> func, string splitParam, object param = null);

        IEnumerable<T> ExecuteSql<T, T2, T3, T4, T5>(string sql, Func<T, T2, T3, T4, T5, T> func, string splitParam, object param = null);

        IEnumerable<T> ExecuteSql<T, T2, T3, T4, T5, T6>(string sql, Func<T, T2, T3, T4, T5, T6, T> func, string splitParam, object param = null);

        int ExecuteSql(string sql, object param = null);

        int ExecuteProcs(IEnumerable<KeyValuePair<string, object>> sqlParam);

        int UsingTransaction(Func<IDbConnection, IDbTransaction, int> execute);

        MultiResult<TFirst, TSecond> ExecuteProc<TFirst, TSecond>(
            string procedureName,
			MySqlParameter[] parameters);

        MultiResult<TFirst, TSecond, TThird> ExecuteProc<TFirst, TSecond, TThird>(
            string procedureName,
			MySqlParameter[] parameters);

        MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TSecond1, TSecond2>(
            string procedureName,
			MySqlParameter[] parameters,
            Func<TFirst1, TFirst2, TFirst1> func1,
            Func<TSecond1, TSecond2, TSecond1> func2,
            string splitOn1,
            string splitOn2);

        MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TFirst3, TSecond1, TSecond2>(
            string procedureName,
			MySqlParameter[] parameters,
            Func<TFirst1, TFirst2, TFirst3, TFirst1> func1,
            Func<TSecond1, TSecond2, TSecond1> func2,
            string splitOn1,
            string splitOn2);

        MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TFirst3, TSecond1, TSecond2, TSecond3>(
            string procedureName,
			MySqlParameter[] parameters,
            Func<TFirst1, TFirst2, TFirst3, TFirst1> func1,
            Func<TSecond1, TSecond2, TSecond3, TSecond1> func2,
            string splitOn1,
            string splitOn2);

        MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TFirst3, TFirst4, TSecond1, TSecond2, TSecond3>(
            string procedureName,
			MySqlParameter[] parameters,
            Func<TFirst1, TFirst2, TFirst3, TFirst4, TFirst1> func1,
            Func<TSecond1, TSecond2, TSecond3, TSecond1> func2,
            string splitOn1,
            string splitOn2);

        MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TFirst3, TFirst4, TFirst5, TSecond1, TSecond2, TSecond3>(
            string procedureName,
			MySqlParameter[] parameters,
            Func<TFirst1, TFirst2, TFirst3, TFirst4, TFirst5, TFirst1> func1,
            Func<TSecond1, TSecond2, TSecond3, TSecond1> func2,
            string splitOn1,
            string splitOn2);

        MultiResult<TFirst1, TSecond1, TThird1> ExecuteProc<TFirst1, TFirst2, TSecond1, TSecond2, TThird1, TThird2>(
            string procedureName,
			MySqlParameter[] parameters,
            Func<TFirst1, TFirst2, TFirst1> func1,
            Func<TSecond1, TSecond2, TSecond1> func2,
            Func<TThird1, TThird2, TThird1> func3,
            string splitOn1,
            string splitOn2,
            string splitOn3);

        MultiResult<TFirst1, TSecond1, TThird1> ExecuteProc<TFirst1, TFirst2, TFirst3, TSecond1, TSecond2, TThird1, TThird2>(
            string procedureName,
			MySqlParameter[] parameters,
            Func<TFirst1, TFirst2, TFirst3, TFirst1> func1,
            Func<TSecond1, TSecond2, TSecond1> func2,
            Func<TThird1, TThird2, TThird1> func3,
            string splitOn1,
            string splitOn2,
            string splitOn3);
    }
}