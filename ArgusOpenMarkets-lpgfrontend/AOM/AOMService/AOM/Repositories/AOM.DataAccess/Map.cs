﻿namespace Argus.Common.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Dapper;

    public static class DataAccessExtensions
    {
        /// <summary>
        /// Used to extend the Dapper mapper to allow multiple results to be returned and mapped
        /// from a single DB call
        /// </summary>
        /// <typeparam name="TFirst">The type top level entity to be mapped.</typeparam>
        /// <typeparam name="TSecond">The type second level entity to be mapped.</typeparam>
        /// <typeparam name="TKey">The type of the key to be used to link the first and second types together.</typeparam>
        /// <param name="reader"></param>
        /// <param name="firstKey"></param>
        /// <param name="secondKey"></param>
        /// <param name="addChildren"></param>
        /// <returns></returns>
        public static IEnumerable<TFirst> Map<TFirst, TSecond, TKey>(
            this SqlMapper.GridReader reader,
            Func<TFirst, TKey> firstKey,
            Func<TSecond, TKey> secondKey,
            Action<TFirst, IEnumerable<TSecond>> addChildren)
        {
            var first = reader.Read<TFirst>().ToList();
            var childMap = reader.Read<TSecond>()
                                 .GroupBy(secondKey)
                                 .ToDictionary(g => g.Key, g => g.AsEnumerable());

            foreach (var item in first)
            {
                IEnumerable<TSecond> children;

                if (childMap.TryGetValue(firstKey(item), out children))
                {
                    addChildren(item, children);
                }
            }

            return first;
        }
    }
}
