﻿namespace Argus.Common.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using Dapper;

    using MySql.Data.MySqlClient;

	public class DataAccessHelper : IDataAccessHelper
    {
        private readonly string connectionString;

        public DataAccessHelper(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IEnumerable<T> ExecuteProc<T>(string storedProcName)
        {
            return Execute(conn => conn.Query<T>(storedProcName, null, null, true, 10, CommandType.StoredProcedure));
        }

		public IEnumerable<T> ExecuteProc<T>(string storedProcName, MySqlParameter[] parameters)
        {
            return Execute(conn => conn.Query<T>(storedProcName, parameters, null, true, 10, CommandType.StoredProcedure));
        }

		public IEnumerable<T1> ExecuteProc<T1, T2>(string storedProcName, Func<T1, T2, T1> funcMap, MySqlParameter[] parameters, string splitOn)
        {
            return Execute(conn => conn.Query(storedProcName, funcMap, parameters, null, true, splitOn, 10, CommandType.StoredProcedure));
        }

		public IEnumerable<T1> ExecuteProc<T1, T2, T3>(string storedProcName, Func<T1, T2, T3, T1> funcMap, MySqlParameter[] parameters, string splitOn)
        {
            return Execute(conn => conn.Query(storedProcName, funcMap, parameters, null, true, splitOn, 10, CommandType.StoredProcedure));
        }

		public IEnumerable<T1> ExecuteProc<T1, T2, T3, T4>(string storedProcName, Func<T1, T2, T3, T4, T1> funcMap, MySqlParameter[] parameters, string splitOn)
        {
            return Execute(conn => conn.Query(storedProcName, funcMap, parameters, null, true, splitOn, 10, CommandType.StoredProcedure));
        }

		public void ExecuteProc(string storedProcName, MySqlParameter[] parameters)
        {
            Execute(conn => conn.Execute(storedProcName, parameters, null, 10, CommandType.StoredProcedure));
        }

        public MultiResult<TFirst, TSecond> ExecuteProc<TFirst, TSecond>(
                            string procedureName,
							MySqlParameter[] parameters)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst, TSecond>
                {
                    Result1 = gridreader.Read<TFirst>(),
                    Result2 = gridreader.Read<TSecond>()
                };
                return multiResult;
            });
        }

        public MultiResult<TFirst, TSecond, TThird> ExecuteProc<TFirst, TSecond, TThird>(
                            string procedureName,
							MySqlParameter[] parameters)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst, TSecond, TThird>
                {
                    Result1 = gridreader.Read<TFirst>(),
                    Result2 = gridreader.Read<TSecond>(),
                    Result3 = gridreader.Read<TThird>()
                };
                return multiResult;
            });
        }

        public MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TSecond1, TSecond2>(
                            string procedureName,
							MySqlParameter[] parameters,
                            Func<TFirst1, TFirst2, TFirst1> func1,
                            Func<TSecond1, TSecond2, TSecond1> func2,
                            string splitOn1,
                            string splitOn2)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst1, TSecond1>
                {
                    Result1 = gridreader.Read(func1, splitOn1),
                    Result2 = gridreader.Read(func2, splitOn2)
                };
                return multiResult;
            });
        }

        public MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TFirst3, TSecond1, TSecond2>(
                            string procedureName,
                            MySqlParameter[] parameters,
                            Func<TFirst1, TFirst2, TFirst3, TFirst1> func1,
                            Func<TSecond1, TSecond2, TSecond1> func2,
                            string splitOn1,
                            string splitOn2)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst1, TSecond1>
                {
                    Result1 = gridreader.Read(func1, splitOn1),
                    Result2 = gridreader.Read(func2, splitOn2)
                };
                return multiResult;
            });
        }

        public MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TFirst3, TSecond1, TSecond2, TSecond3>(
                            string procedureName,
							MySqlParameter[] parameters,
                            Func<TFirst1, TFirst2, TFirst3, TFirst1> func1,
                            Func<TSecond1, TSecond2, TSecond3, TSecond1> func2,
                            string splitOn1,
                            string splitOn2)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst1, TSecond1>
                {
                    Result1 = gridreader.Read(func1, splitOn1),
                    Result2 = gridreader.Read(func2, splitOn2)
                };
                return multiResult;
            });
        }

        public MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TFirst3, TFirst4, TSecond1, TSecond2, TSecond3>(
                            string procedureName,
							MySqlParameter[] parameters,
                            Func<TFirst1, TFirst2, TFirst3, TFirst4, TFirst1> func1,
                            Func<TSecond1, TSecond2, TSecond3, TSecond1> func2,
                            string splitOn1,
                            string splitOn2)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst1, TSecond1>
                {
                    Result1 = gridreader.Read(func1, splitOn1),
                    Result2 = gridreader.Read(func2, splitOn2)
                };
                return multiResult;
            });
        }

        public MultiResult<TFirst1, TSecond1> ExecuteProc<TFirst1, TFirst2, TFirst3, TFirst4, TFirst5, TSecond1, TSecond2, TSecond3>(
                            string procedureName,
							MySqlParameter[] parameters,
                            Func<TFirst1, TFirst2, TFirst3, TFirst4, TFirst5, TFirst1> func1,
                            Func<TSecond1, TSecond2, TSecond3, TSecond1> func2,
                            string splitOn1,
                            string splitOn2)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst1, TSecond1>
                {
                    Result1 = gridreader.Read(func1, splitOn1),
                    Result2 = gridreader.Read(func2, splitOn2)
                };
                return multiResult;
            });
        }

        public MultiResult<TFirst1, TSecond1, TThird1> ExecuteProc<TFirst1, TFirst2, TSecond1, TSecond2, TThird1, TThird2>(
                            string procedureName,
							MySqlParameter[] parameters,
                            Func<TFirst1, TFirst2, TFirst1> func1,
                            Func<TSecond1, TSecond2, TSecond1> func2,
                            Func<TThird1, TThird2, TThird1> func3,
                            string splitOn1,
                            string splitOn2,
                            string splitOn3)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst1, TSecond1, TThird1>
                {
                    Result1 = gridreader.Read(func1, splitOn1),
                    Result2 = gridreader.Read(func2, splitOn2),
                    Result3 = gridreader.Read(func3, splitOn3)
                };
                return multiResult;
            });
        }

        public MultiResult<TFirst1, TSecond1, TThird1> ExecuteProc<TFirst1, TFirst2, TFirst3, TSecond1, TSecond2, TThird1, TThird2>(
                            string procedureName,
							MySqlParameter[] parameters,
                            Func<TFirst1, TFirst2, TFirst3, TFirst1> func1,
                            Func<TSecond1, TSecond2, TSecond1> func2,
                            Func<TThird1, TThird2, TThird1> func3,
                            string splitOn1,
                            string splitOn2,
                            string splitOn3)
        {
            return Execute(conn =>
            {
                var gridreader = conn.QueryMultiple(procedureName, parameters, null, 10, CommandType.StoredProcedure);
                var multiResult = new MultiResult<TFirst1, TSecond1, TThird1>
                {
                    Result1 = gridreader.Read(func1, splitOn1),
                    Result2 = gridreader.Read(func2, splitOn2),
                    Result3 = gridreader.Read(func3, splitOn3)
                };
                return multiResult;
            });
        }

        public IEnumerable<T> ExecuteSql<T>(string sql, object param = null)
        {
            return Execute(conn => conn.Query<T>(sql, param));
        }

        public IEnumerable<T> ExecuteSql<T, T2>(string sql, Func<T, T2, T> func, string splitParam, object param = null)
        {
            return Execute(conn => conn.Query(sql, func, param, splitOn: splitParam));
        }

        public IEnumerable<T> ExecuteSql<T, T2, T3>(string sql, Func<T, T2, T3, T> func, string splitParam, object param = null)
        {
            return Execute(conn => conn.Query(sql, func, param, splitOn: splitParam));
        }

        public IEnumerable<T> ExecuteSql<T, T2, T3, T4>(string sql, Func<T, T2, T3, T4, T> func, string splitParam, object param = null)
        {
            return Execute(conn => conn.Query(sql, func, param, splitOn: splitParam));
        }

        public IEnumerable<T> ExecuteSql<T, T2, T3, T4, T5>(string sql, Func<T, T2, T3, T4, T5, T> func, string splitParam, object param = null)
        {
            return Execute(conn => conn.Query(sql, func, param, splitOn: splitParam));
        }

        public IEnumerable<T> ExecuteSql<T, T2, T3, T4, T5, T6>(string sql, Func<T, T2, T3, T4, T5, T6, T> func, string splitParam, object param = null)
        {
            return Execute(conn => conn.Query(sql, func, param, splitOn: splitParam));
        }

        public int ExecuteSql(string sql, object param = null)
        {
            return Execute(conn => conn.Execute(sql, param));
        }

        public int ExecuteProcs(IEnumerable<KeyValuePair<string, object>> sqlParam)
        {
            var rowsAffected = 0;

            using (var conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    rowsAffected += sqlParam.Sum(keyValuePair => conn.Execute(keyValuePair.Key, keyValuePair.Value, transaction));

                    transaction.Commit();
                }
            }

            return rowsAffected;
        }

        public int UsingTransaction(Func<IDbConnection, IDbTransaction, int> execute)
        {
            int rowsAffected;

            using (var conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    rowsAffected = execute(conn, transaction);

                    transaction.Commit();
                }
            }

            return rowsAffected;
        }
        
        private TResult Execute<TResult>(Func<MySqlConnection, TResult> action)
        {
            using (var conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                return action.Invoke(conn);
            }
        }
    }
}