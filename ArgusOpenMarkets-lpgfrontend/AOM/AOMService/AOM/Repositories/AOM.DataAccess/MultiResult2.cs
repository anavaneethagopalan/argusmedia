﻿namespace Argus.Common.DataAccess
{
    using System.Collections.Generic;

    public class MultiResult<T, T2>
    {
        public IEnumerable<T> Result1 { get; set; }
        public IEnumerable<T2> Result2 { get; set; } 
    }
}
