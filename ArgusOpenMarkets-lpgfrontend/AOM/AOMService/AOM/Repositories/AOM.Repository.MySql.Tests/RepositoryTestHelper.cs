﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace AOM.Repository.MySql.Tests
{
    class RepositoryTestHelper
    {
        private const string ErrorMessage = "Expect all DTO to load from DB without error";

        public static void AssertModelHasLazyLoadDisabled<T>() where T : DbContext, new()
        {
            using (var db = new T())
            {
                Assert.IsFalse(db.Configuration.LazyLoadingEnabled,
                    "LAZY Loading should be off - LINQ queries should return data that is required and not rely on subsequent lazy loads");
            }
        }

        public static void AssertCanLoadObjectsInModel<T>() where T : DbContext, new()
        {
            var sets = from p in typeof(T).GetProperties()
                       where p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(IDbSet<>)
                       let entityType = p.PropertyType.GetGenericArguments().First()
                       select p;

            using (var db = new T())
            {
                bool success = true;
                foreach (var set in sets)
                {
                    Debug.WriteLine("->{0}.{1}", db.GetType().Name, set.Name);

                    var accessor = set.GetValue(db) as dynamic;
                    var exceptionMessage = string.Empty;
                    try
                    {
                        //Load one item
                        (Queryable.Take(accessor, 1) as IQueryable).Load();
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                        Debug.WriteLine(e.InnerException);
                        exceptionMessage = e.ToString();
                        success = false;
                    }

                    Assert.IsTrue(success, CreateErrorMessage(exceptionMessage));
                }

            }
        }

        private static string CreateErrorMessage(string exceptionMessage)
        {
            return string.IsNullOrEmpty(exceptionMessage) ? ErrorMessage : ErrorMessage + ": " + exceptionMessage;
        }
    }
}