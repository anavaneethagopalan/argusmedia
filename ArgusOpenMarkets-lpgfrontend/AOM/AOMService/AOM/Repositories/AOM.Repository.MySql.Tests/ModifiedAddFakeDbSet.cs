﻿using System;

namespace AOM.Repository.MySql.Tests
{
    // In lieu of a full implementation of .Include in FakeDbSet this class provides
    // a rather more low-tech, manual approach where a mutating Action<T> can be
    // provided that is applied to every entity as it is Added to the set.
    // (e.g. Fill in the ProductTenor when adding an OrderDto)
    public class ModifiedAddFakeDbSet<T> : FakeDbSet<T> where T : class
    {
        private readonly Action<T> _mutateOnAdd;

        public ModifiedAddFakeDbSet()
            : this(entity => { })
        { }

        public ModifiedAddFakeDbSet(Action<T> mutateOnAdd)
        {
            _mutateOnAdd = mutateOnAdd;
        }

        public override T Add(T entity)
        {
            _mutateOnAdd(entity);
            return base.Add(entity);
        }
    }
}
