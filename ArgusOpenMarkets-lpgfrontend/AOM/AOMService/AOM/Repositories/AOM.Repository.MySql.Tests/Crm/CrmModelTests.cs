﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm.AOM.Repository.MySql.Crm;
using MySql.Data.MySqlClient;

namespace AOM.Repository.MySql.Tests.Crm
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using AOM.Repository.MySql.Crm;

    using Ninject.Infrastructure.Language;

    using NUnit.Framework;

    [TestFixture]
    public class CrmModelTests
    {
        public void DatabaseShouldPointAtDev()
        {
            const string crmDrv =
                @"server=aominstancedevpri.co46fwzab7bh.eu-west-1.rds.amazonaws.com;user id=crm_user;password=Argu5Med1a;persistsecurityinfo=True;database=crm";
            string filePath = Directory.GetParent(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath).Parent.Parent.Parent.FullName;
            string[] configFiles = Directory.GetFiles(filePath, "*Processor.exe", SearchOption.AllDirectories);
            Assert.Greater(configFiles.Length, 10, "Expected to find several config files to test");
            foreach (var configFile in configFiles)
            {
                ConfigurationManager.OpenExeConfiguration(configFile);
                var connectionString = ConfigurationManager.ConnectionStrings["CrmModel"];
                if (connectionString != null)
                {
                    Assert.True(crmDrv.Equals(connectionString.ConnectionString));
                }
            }
        }

        [Test]
        public void ValidateLogEntiesHaveMandatoryProperties()
        {
            const string logDtoSuffix = "_LogDto";
            var anchorType = typeof(UserInfoLogDto);

            foreach (
                var logDto in
                    anchorType.Assembly.ExportedTypes.Where(o => o.Name.Contains(logDtoSuffix))
                        .Where(x => x.Namespace.Equals(anchorType.Namespace)))
            {
                Debug.WriteLine("->Checking CrmModel." + logDto.Name);
                Assert.IsNotNull(
                    logDto.GetProperties()
                        .Any(p => p.Name == "LastUpdated" && !p.IsPrivate() && p.PropertyType == typeof(DateTime)),
                    logDto.Name + " missing public LastUpdated string property");
                Assert.IsNotNull(
                    logDto.GetProperties()
                        .Any(p => p.Name == "LastUpdatedUserId" && !p.IsPrivate() && p.PropertyType == typeof(long)),
                    logDto.Name + " missing public LastUpdatedUserId long property");
                ;
            }
        }

        [Test]
        public void PropertiesOnDtoShouldExistOnLogEnties()
        {
            var anchorType = typeof(UserInfoLogDto);
            const string logDtoSuffix = "LogDto";

            var logDtos = anchorType.Assembly.ExportedTypes.Where(o => o.Name.EndsWith(logDtoSuffix) && o.IsClass).Where(x => x.Namespace.Equals(anchorType.Namespace));

            Assert.Greater(logDtos.Count(), 0);

            foreach (var logDto in logDtos)
            {
                Debug.WriteLine("->Checking CrmModel." + logDto.Name);
                string logDtoName = logDto.Name;
                var expectedMatchingDto = logDtoName.Substring(0, logDtoName.Length - logDtoSuffix.Length) + "Dto";

                var associatedDtoForLogDto = anchorType.Assembly.ExportedTypes.SingleOrDefault(o => o.Name == expectedMatchingDto);
                Assert.IsNotNull(associatedDtoForLogDto, "Expected to find a DTO Class called '" + expectedMatchingDto + "' to match the log DTO class '" + logDtoName + "'");

                foreach (var dtoProperty in associatedDtoForLogDto.GetProperties().Where(p => !p.IsPrivate()))
                {
                    Debug.WriteLine(" CrmModel." + associatedDtoForLogDto.Name + "." + dtoProperty.Name);
                    
                    if (dtoProperty.PropertyType.FullName.EndsWith("Dto") || dtoProperty.PropertyType.FullName.Contains("Dto"))
                    {
                        //skip objects which should have an associated Id field the test would check
                        Debug.WriteLine("------- skipped DTO property " + dtoProperty.Name);
                        continue;
                    }

                    if (dtoProperty.PropertyType.IsInterface)
                    {
                        Debug.WriteLine("------- skipped IsInterface property " + dtoProperty.Name);
                        continue;
                    }

                    var logDtoProperty = logDto.GetProperties().SingleOrDefault(p => p.Name == dtoProperty.Name);

                    Assert.IsNotNull(logDtoProperty, "Expected the DTO " + logDto.Name + " to have the property " + dtoProperty.Name + " as the DTO has it.");
                    Assert.AreEqual(logDtoProperty.PropertyType, dtoProperty.PropertyType, "Expected the DTO " + logDto.Name + " to have the same property type " + dtoProperty.Name + " as the DTO.");
                }
            }
        }

        [Test]        
        public void ModelShouldNotSupportLazyLoading()
        {            
            RepositoryTestHelper.AssertModelHasLazyLoadDisabled<CrmModel>();
            RepositoryTestHelper.AssertModelHasLazyLoadDisabled<ArgusCrmFeedModel>();
            RepositoryTestHelper.AssertModelHasLazyLoadDisabled<AuthenticationModel>();
            RepositoryTestHelper.AssertModelHasLazyLoadDisabled<UserModel>();
            RepositoryTestHelper.AssertModelHasLazyLoadDisabled<OrganisationModel>();
            RepositoryTestHelper.AssertModelHasLazyLoadDisabled<PrivilegeModel>();
        }


        [Test]
        [Category("local")]
        public void CanQueryAllCRMDtos()
        {            
            RepositoryTestHelper.AssertCanLoadObjectsInModel<CrmModel>();
            RepositoryTestHelper.AssertCanLoadObjectsInModel<ArgusCrmFeedModel>();
            RepositoryTestHelper.AssertCanLoadObjectsInModel<AuthenticationModel>();
            RepositoryTestHelper.AssertCanLoadObjectsInModel<UserModel>();
            RepositoryTestHelper.AssertCanLoadObjectsInModel<OrganisationModel>();
            RepositoryTestHelper.AssertCanLoadObjectsInModel<PrivilegeModel>();
        }

        [Test]
        public void AllStringPropertiesOnDtoHaveMaxLengthSetToMatchDB()
        {
            var efEntites = typeof(ProductDto).Assembly.GetExportedTypes().Where(t => t.HasAttribute(typeof(TableAttribute))).ToList();

            //Build a list EF entites with their string columns and their associated StringLengthAttribute if present
            var efProps = efEntites.SelectMany(t => t.GetProperties().Where(p => p.PropertyType == typeof(string)).Select(
                p => new
                {
                    TableName = ((TableAttribute)t.GetCustomAttributes(typeof(TableAttribute), true).Single()).Name,
                    ColumnName = ((ColumnAttribute)p.GetCustomAttributes(typeof(ColumnAttribute), true).SingleOrDefault()) != null
                               ? ((ColumnAttribute)p.GetCustomAttributes(typeof(ColumnAttribute), true).Single()).Name : p.Name,
                    ClassName = t.Name,
                    PropertyName = p.Name,
                    DeclaredStringLength = ((StringLengthAttribute)p.GetCustomAttributes(typeof(StringLengthAttribute), true).SingleOrDefault())
                })).ToList();

            using (var dbConnector = new MySqlConnection(CrmModelSchemaTests.CrmConnectionString))
            {
                dbConnector.Open();

                var qry =
                    @"SELECT tbl.table_schema, tbl.table_name, cols.COLUMN_NAME, cols.DATA_TYPE, cols.CHARACTER_MAXIMUM_LENGTH
                            FROM INFORMATION_SCHEMA.columns cols 
                            INNER JOIN INFORMATION_SCHEMA.tables tbl ON tbl.table_schema = cols.table_schema and tbl.table_name=cols.TABLE_NAME
                            WHERE tbl.table_schema = 'crm'
                            AND cols.data_type in ('char', 'varchar')";

                var mysqlCmd = new MySqlCommand(qry, dbConnector);

                var reader = mysqlCmd.ExecuteReader();

                while (reader.Read())
                {
                    string fullTableName = String.Format("{0}.{1}", reader.GetString("table_schema"), reader.GetString("table_name"));

                    string columnName = reader.GetString("COLUMN_NAME");
                    int columnWidthInDb = reader.GetInt32("CHARACTER_MAXIMUM_LENGTH");

                    var efItem = efProps.Where(e => String.Equals(fullTableName, e.TableName, StringComparison.CurrentCultureIgnoreCase)
                        && String.Equals(columnName, e.ColumnName, StringComparison.CurrentCultureIgnoreCase)).SingleOrDefault();

                    if (efItem != null)
                    {
                        Trace.WriteLine(">Checking " + fullTableName + "." + columnName);
                        if (efItem.DeclaredStringLength != null)
                        {
                            Assert.AreEqual(columnWidthInDb, efItem.DeclaredStringLength.MaximumLength, string.Format("The column {0}.{1} has a maxlength in the database less than defined in EF.", efItem.ClassName, efItem.PropertyName));
                        }
                        else
                        {
                            Assert.Fail("\tDTO property {0}.{1} should have max length defined", efItem.ClassName, efItem.PropertyName);
                        }
                    }
                }
            }
        }
    }
}