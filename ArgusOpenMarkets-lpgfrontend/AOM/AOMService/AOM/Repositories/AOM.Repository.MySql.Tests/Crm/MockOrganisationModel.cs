using System.Data.Entity;
using AOM.Repository.MySql.Crm;

namespace AOM.Repository.MySql.Tests.Crm
{
    public class MockOrganisationModel : IOrganisationModel
    {
        public IDbSet<OrganisationDto> Organisations { get; set; }
        public IDbSet<OrganisationLogDto> OrganisationLogs { get; set; }
        public IDbSet<UserInfoDto> UserInfoes { get; set; }
        public IDbSet<SubscribedProductDto> SubscribedProducts { get; set; }
        public IDbSet<SubscribedProductLogDto> SubscribedProductLogs { get; set; }
        public IDbSet<OrganisationRoleDto> OrganisationRoles { get; set; }
        public IDbSet<OrganisationRoleLogDto> OrganisationRoleLogs { get; set; }
        public IDbSet<CounterpartyPermissionDto> CounterpartyPermissions { get; set; }
        public IDbSet<CounterpartyPermissionLogDto> CounterpartyPermissionLogs { get; set; }
        public IDbSet<BrokerPermissionDto> BrokerPermissions { get; set; }
        public IDbSet<BrokerPermissionLogDto> BrokerPermissionLogs { get; set; }
        public IDbSet<SystemEventNotificationDto> SystemEventNotifications { get; set; }

        public int SaveChangesAndLog(long userId)
        {
            SaveChangesCalledCount += 1;
            return 0;
        }

        public MockOrganisationModel()
        {
            Organisations = new FakeDbSet<OrganisationDto>();
            OrganisationLogs = new FakeDbSet<OrganisationLogDto>();
            OrganisationRoles = new FakeDbSet<OrganisationRoleDto>();
            OrganisationRoleLogs = new FakeDbSet<OrganisationRoleLogDto>();
            SubscribedProducts = new FakeDbSet<SubscribedProductDto>();
            SubscribedProductLogs = new FakeDbSet<SubscribedProductLogDto>();
            CounterpartyPermissions = new FakeDbSet<CounterpartyPermissionDto>();
            BrokerPermissions = new FakeDbSet<BrokerPermissionDto>();
            UserInfoes = new FakeDbSet<UserInfoDto>();
            SystemEventNotifications = new FakeDbSet<SystemEventNotificationDto>();

        }

        public int SaveChangesCalledCount { get; set; }

        public void Dispose()
        {
        }
    }
}