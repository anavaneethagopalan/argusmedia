﻿using System;
using System.Data.Entity;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Crm.AOM.Repository.MySql.Crm;

namespace AOM.Repository.MySql.Tests.Crm
{
    public class MockCrmModel : ICrmModel, IUserModel, IOrganisationModel, IArgusCrmFeedModel, IPrivilegeModel, IAuthenticationModel
    {
        public IDbSet<CounterpartyPermissionDto> CounterpartyPermissions { get; set; }
        public IDbSet<CounterpartyPermissionLogDto> CounterpartyPermissionLogs { get; set; }
        public IDbSet<BrokerPermissionDto> BrokerPermissions { get; set; }
        public IDbSet<BrokerPermissionLogDto> BrokerPermissionLogs { get; set; }

        public IDbSet<OrganisationDto> Organisations { get; set; }

        public IDbSet<ProductPrivilegeDto> ProductPrivileges { get; set; }
        public IDbSet<ProductPrivilegeLogDto> ProductPrivilegeLogs { get; set; }

        public IDbSet<SubscribedProductDto> SubscribedProducts { get; set; }
        public IDbSet<SubscribedProductLogDto> SubscribedProductLogs { get; set; }

        public IDbSet<SubscribedProductPrivilegeDto> SubscribedProductPrivileges { get; set; }
        public IDbSet<SubscribedProductPrivilegeLogDto> SubscribedProductPrivilegeLogs { get; set; }

        public IDbSet<SystemPrivilegeDto> SystemPrivileges { get; set; }
        public IDbSet<SystemPrivilegeLogDto> SystemPrivilegeLogs { get; set; }
        public IDbSet<SystemRolePrivilegeDto> SystemRolePrivileges { get; set; }
        public IDbSet<SystemRolePrivilegeLogDto> SystemRolePrivilegeLogs { get; set; }

        public IDbSet<UserInfoDto> UserInfoes { get; set; }

        public IDbSet<UserTokenDto> UserTokens { get; set; }

        public IDbSet<OrganisationRoleDto> OrganisationRoles { get; set; }
        public IDbSet<OrganisationRoleLogDto> OrganisationRoleLogs { get; set; }

        public IDbSet<UserInfoRoleDto> UserInfoRoles { get; set; }
        public IDbSet<UserInfoRoleLogDto> UserInfoRolelogs { get; set; }
        public IDbSet<UserInfoLogDto> UserInfoLogs { get; set; }

        public IDbSet<ProductRolePrivilegeDto> ProductRolePrivileges { get; set; }
        public IDbSet<ProductRolePrivilegeLogDto> ProductRolePrivilegeLogs { get; set; }
        public bool AutoDetectChangesEnabled { get; set; }

        public IDbSet<AuthenticationHistoryDto> AuthenticationHistory { get; set; }
        public IDbSet<DisconnectHistoryDto> DisconnectHistory { get; set; }
        
        public IDbSet<UserLoginAttemptsDto> UserLoginAttempts { get; set; }
        public IDbSet<UserLoginSourceDto> UserLoginSources { get; set; }
        public IDbSet<UserLoginExternalAccountDto> UserLoginExternalAccounts { get; set; }
        public IDbSet<UserLoginExternalAccountLogDto> UserLoginExternalAccountsLogs { get; set; }

        public IDbSet<UserCredentialsDto> UserCredentials { get; set; }
        public IDbSet<UserCredentialsLogDto> UserCredentialLogs { get; set; }

        public IDbSet<ModuleDto> Modules { get; set; }
        public IDbSet<UserModuleDto> UserModules { get; set; }

        public IDbSet<SystemEventNotificationDto> SystemEventNotifications { get; set; }
        public IDbSet<SystemEventNotificationLogDto> SystemEventNotificationLogs { get; set; }

        public IDbSet<ContentStreamDto> ContentStreams { get; set; }
        public IDbSet<OrganisationLogDto> OrganisationLogs { get; set; }

        public int SaveChangesCalledCount { get; set; }

        public int SaveChangesAndLog(long userId)
        {
            SaveChangesCalledCount += 1;
            return (int)userId;
        }


        public MockCrmModel()
        {
            Organisations = new FakeDbSet<OrganisationDto>();
            OrganisationLogs = new FakeDbSet<OrganisationLogDto>();
            UserInfoRoles = new FakeDbSet<UserInfoRoleDto>();
            UserInfoRolelogs = new FakeDbSet<UserInfoRoleLogDto>();
            OrganisationRoles = new FakeDbSet<OrganisationRoleDto>();
            OrganisationRoleLogs = new FakeDbSet<OrganisationRoleLogDto>();
            ProductPrivileges = new FakeDbSet<ProductPrivilegeDto>();
            ProductPrivilegeLogs = new FakeDbSet<ProductPrivilegeLogDto>();
            SubscribedProducts = new FakeDbSet<SubscribedProductDto>();
            SubscribedProductLogs = new FakeDbSet<SubscribedProductLogDto>();
            SubscribedProductPrivileges = new FakeDbSet<SubscribedProductPrivilegeDto>();
            SubscribedProductPrivilegeLogs = new FakeDbSet<SubscribedProductPrivilegeLogDto>();
            SystemRolePrivileges = new FakeDbSet<SystemRolePrivilegeDto>();
            SystemRolePrivilegeLogs = new FakeDbSet<SystemRolePrivilegeLogDto>();
            SystemPrivileges = new FakeDbSet<SystemPrivilegeDto>();
            SystemPrivilegeLogs = new FakeDbSet<SystemPrivilegeLogDto>();
            UserInfoes = new FakeDbSet<UserInfoDto>();
            UserInfoLogs = new FakeDbSet<UserInfoLogDto>();
            UserTokens = new FakeDbSet<UserTokenDto>();
            ProductRolePrivileges = new FakeDbSet<ProductRolePrivilegeDto>();
            ProductRolePrivilegeLogs = new FakeDbSet<ProductRolePrivilegeLogDto>();
            AuthenticationHistory = new FakeDbSet<AuthenticationHistoryDto>();
            DisconnectHistory = new FakeDbSet<DisconnectHistoryDto>();
            CounterpartyPermissions = new FakeDbSet<CounterpartyPermissionDto>();
            UserCredentials = new FakeDbSet<UserCredentialsDto>(new[]
            {
                new UserCredentialsDto { User = new UserInfoDto {Id = 2, Username = "test"}, CredentialType = "P" }, 
                new UserCredentialsDto { User = new UserInfoDto {Id = 1, Username = "test"}, CredentialType = "T" }, 
            });
            UserCredentialLogs = new FakeDbSet<UserCredentialsLogDto>();
            Modules = new FakeDbSet<ModuleDto>();
            UserModules = new FakeDbSet<UserModuleDto>();
            ContentStreams = new FakeDbSet<ContentStreamDto>();
            BrokerPermissions = new FakeDbSet<BrokerPermissionDto>();
            SystemEventNotifications = new FakeDbSet<SystemEventNotificationDto>();
            UserLoginAttempts = new FakeDbSet<UserLoginAttemptsDto>();
            UserLoginSources = new FakeDbSet<UserLoginSourceDto>();
            UserLoginExternalAccounts = new FakeDbSet<UserLoginExternalAccountDto>();
        }

        public void Dispose()
        {
        }
    }
}