﻿using System.Data.Entity;
using AOM.Repository.MySql.Crm;

namespace AOM.Repository.MySql.Tests.Crm
{
    public class CrmDataBuilder
    {
        static int id;

        public int NextId()
        {
            if (DBContext is DbContext) return 0;
            id -= 1;
            return id;
        }

        public ICrmModel DBContext { get; private set; }

        private CrmDataBuilder(ICrmModel model)
        {
            DBContext = model;
        }

        public static CrmDataBuilder WithModel(ICrmModel model)
        {
            return new CrmDataBuilder(model);
        }     
    }
}
