﻿namespace AOM.Repository.MySql.Tests.Crm
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using global::MySql.Data.MySqlClient;

    using NUnit.Framework;

    using System.Configuration;

    [TestFixture]
    public class CrmModelSchemaTests
    {
        private MySqlConnection _dbConnector;

        private static string _connString;

        public static string CrmConnectionString
        {
            get
            {
                var crmConn = "";

                if (string.IsNullOrEmpty(_connString))
                {
                    string filePath = Directory.GetParent(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath).Parent.Parent.Parent.Parent.FullName;
                    string[] configFiles = Directory.GetFiles(filePath, "app.config", SearchOption.AllDirectories);
                    foreach (var configFile in configFiles)
                    {
                        ConfigurationManager.OpenExeConfiguration(configFile);

                        try
                        {
                            crmConn = ConfigurationManager.ConnectionStrings["CrmModel"].ConnectionString;
                        }
                        catch (Exception)
                        {
                            crmConn = "";
                        }

                        if (crmConn != null)
                        {
                            _connString = crmConn;
                            break;
                        }
                    }
                }

                if (string.IsNullOrEmpty(_connString))
                {
                    _connString = "server=aominstancedevpri.co46fwzab7bh.eu-west-1.rds.amazonaws.com;user id=crm_user;password=Argu5Med1a;persistsecurityinfo=True;database=crm";
                }

                return _connString;
            }
        }

        [SetUp]
        public void Setup()
        {
            try
            {
                _dbConnector = new MySqlConnection(CrmConnectionString);
                _dbConnector.Open();
            }
            catch(MySqlException msqlEx)
            {
                Assert.Fail(msqlEx.Message);
            }
        }

        [TearDown]
        public void TearDown()
        {
            if(_dbConnector != null)
            {
                _dbConnector.Close();
            }
        }

        [Test]
        public void CheckAllBigIntsValid()
        {
            var selectAomMetaData = SqlTestStatements.CrmBigIntCheck;
            var mysqlCmd = new MySqlCommand(selectAomMetaData, _dbConnector);

            var reader = mysqlCmd.ExecuteReader();

            //no results should be returned
            //if results are returned capture the offending row
            var resultCount = 0;
            var tableNames = new List<string>();
            var columnNames = new List<string>();
            
            while(reader.Read())
            {
                tableNames.Add(reader.GetString("table_name"));
                columnNames.Add(reader.GetString("column_name"));
                resultCount++;
            }

            Assert.IsTrue(tableNames.Capacity == 0, "Tables: " + String.Join(",", tableNames));
            Assert.IsTrue(columnNames.Capacity == 0, "Column Name: " + String.Join(",", columnNames));
            Assert.IsTrue(resultCount == 0);
        }

        [Test]
        public void CheckAllTinyIntsValid()
        {
            var selectAomMetaData = SqlTestStatements.CrmTinyIntCheck;
            var mysqlCmd = new MySqlCommand(selectAomMetaData, _dbConnector);

            var reader = mysqlCmd.ExecuteReader();

            //no results should be returned
            //if results are returned capture the offending row
            var resultCount = 0;
            var tableNames = new List<string>();
            var columnNames = new List<string>();

            while (reader.Read())
            {
                tableNames.Add(reader.GetString("table_name"));
                columnNames.Add(reader.GetString("column_name"));
                resultCount++;
            }

            Assert.IsTrue(tableNames.Capacity == 0, "Tables: " + String.Join(",", tableNames));
            Assert.IsTrue(columnNames.Capacity == 0, "Column Name: " + String.Join(",", columnNames));
            Assert.IsTrue(resultCount == 0);
        }

        [Test]
        public void CheckAllDateTimesValid()
        {
            var selectAomMetaData = SqlTestStatements.CrmDateTimeCheck;
            var mysqlCmd = new MySqlCommand(selectAomMetaData, _dbConnector);

            var reader = mysqlCmd.ExecuteReader();

            //no results should be returned
            //if results are returned capture the offending row
            var resultCount = 0;
            var tableNames = new List<string>();
            var columnNames = new List<string>();

            while (reader.Read())
            {
                tableNames.Add(reader.GetString("table_name"));
                columnNames.Add(reader.GetString("column_name"));
                resultCount++;
            }

            Assert.IsTrue(tableNames.Capacity == 0, "Tables: " + String.Join(",", tableNames));
            Assert.IsTrue(columnNames.Capacity == 0, "Column Name: " + String.Join(",", columnNames));
            Assert.IsTrue(resultCount == 0);
        }

        [Test]
        public void CheckAllPresenceOfPk()
        {
            var selectAomMetaData = SqlTestStatements.CrmPrimaryKeyCheck;
            var mysqlCmd = new MySqlCommand(selectAomMetaData, _dbConnector);

            var reader = mysqlCmd.ExecuteReader();

            //no results should be returned
            //if results are returned capture the offending row
            var resultCount = 0;
            var tableNames = new List<string>();

            while (reader.Read())
            {
                tableNames.Add(reader.GetString("table_name"));
                resultCount++;
            }

            Assert.IsTrue(tableNames.Capacity == 0, "Tables: " + String.Join(",", tableNames));
            Assert.IsTrue(resultCount == 0);
        }
    }
}

