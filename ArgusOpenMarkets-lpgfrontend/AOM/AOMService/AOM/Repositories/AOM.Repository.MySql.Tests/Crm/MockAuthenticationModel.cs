﻿using System.Data.Entity;
using AOM.Repository.MySql.Crm;

namespace AOM.Repository.MySql.Tests.Crm
{
    public class MockAuthenticationModel : IAuthenticationModel
    {
        public IDbSet<UserCredentialsDto> UserCredentials { get; set; }
        public IDbSet<UserCredentialsLogDto> UserCredentialLogs { get; set; }
        public IDbSet<UserTokenDto> UserTokens { get; set; }
        public IDbSet<AuthenticationHistoryDto> AuthenticationHistory { get; set; }
        public IDbSet<DisconnectHistoryDto> DisconnectHistory { get; set; }
        public IDbSet<UserLoginAttemptsDto> UserLoginAttempts { get; set; }
        
        public int SaveChangesCalledCount { get; set; }
        public int SaveChangesAndLog(long userId)
        {
            SaveChangesCalledCount += 1;
            return (int)userId;
        }

        public MockAuthenticationModel()
        {
            UserCredentials = new FakeDbSet<UserCredentialsDto>(new[]
            {
                new UserCredentialsDto { User = new UserInfoDto {Id = 2, Username = "test"}, CredentialType = "P" },
                new UserCredentialsDto { User = new UserInfoDto {Id = 1, Username = "test"}, CredentialType = "T" },
            });
            UserCredentialLogs = new FakeDbSet<UserCredentialsLogDto>();
            UserTokens = new FakeDbSet<UserTokenDto>();
            AuthenticationHistory = new FakeDbSet<AuthenticationHistoryDto>();
            DisconnectHistory = new FakeDbSet<DisconnectHistoryDto>();
            UserLoginAttempts = new FakeDbSet<UserLoginAttemptsDto>();
        }

        public void Dispose()
        {
        }
    }
}
