﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;

namespace AOM.Repository.MySql.Tests.Crm
{
    public static class CrmDataBuilderExtensions
    {
        public static T ExecuteAndReportErrors<T>(this ICrmModel context, Func<ICrmModel, T> f)
        {
            try
            {
                return f.Invoke(context);
            }
            catch (DbEntityValidationException dbe)
            {
                foreach (var error in dbe.EntityValidationErrors.SelectMany(e => e.ValidationErrors))
                {
                    Debug.WriteLine("DEBUG::" + error.ErrorMessage);
                }
                // ReSharper disable once PossibleIntendedRethrow
                throw dbe;
            }
        }

        public static CrmDataBuilder AddContentStream(this CrmDataBuilder builder,out ContentStreamDto contentStreamDto,long Id,string description,long contentStreamId)
        {
            contentStreamDto = builder.DBContext.ContentStreams.FirstOrDefault(cs => cs.Id == Id);
            if (contentStreamDto != null)
            {
                return builder;
            }

            contentStreamDto = new ContentStreamDto
            {
                Id = builder.NextId(),
                Description = description,
                ContentStreamId = contentStreamId
            };

            builder.DBContext.ContentStreams.Add(contentStreamDto);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddModule(this CrmDataBuilder builder, long id, out ModuleDto moduleDto)
        {
            moduleDto = builder.DBContext.Modules.FirstOrDefault(m => m.Id == id);
            if (moduleDto != null)
            {
                return builder;
            }

            moduleDto = new ModuleDto { Id = id, Description = "Desc" };

            builder.DBContext.Modules.Add(moduleDto);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddUserModule(this CrmDataBuilder builder,long id,out UserModuleDto userModule,long streamId,string externalUsername,long moduleId)
        {
            userModule = builder.DBContext.UserModules.FirstOrDefault(um => um.Id == id);
            if (userModule != null)
            {
                return builder;
            }

            userModule = new UserModuleDto
            {
                Id = id,
                StreamId = streamId,
                ArgusCrmUsername = externalUsername,
                ModuleId_fk = moduleId
            };

            builder.DBContext.UserModules.Add(userModule);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddOrganisation(this CrmDataBuilder builder, string name, out OrganisationDto org, OrganisationType organisationType = OrganisationType.Trading, bool isDeleted = false)
        {
            org = builder.DBContext.Organisations.FirstOrDefault(o => o.Name == name);
            if (org != null) return builder;

            org = new OrganisationDto
            {
                Id = builder.NextId(),
                Name = name,
                OrganisationType = DtoMappingAttribute.GetValueFromEnum(organisationType),
                IsDeleted = isDeleted
            };

            builder.DBContext.Organisations.Add(org);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddOrganisationRole(this CrmDataBuilder builder,long organisationId,string rolename)
        {
            if (builder.DBContext.OrganisationRoles.Any(o => o.Name == rolename && o.Organisation_Id_fk == organisationId)) return builder;

            var orgRole = new OrganisationRoleDto { Id = builder.NextId(), Name = rolename };
            builder.DBContext.OrganisationRoles.Add(orgRole);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddCounterpartyPermission(this CrmDataBuilder builder, OrganisationDto org1, OrganisationDto org2, BuyOrSell buySellInd, long productId, Permission permission = Permission.Allow)
        {         
            var perm = new CounterpartyPermissionDto
            {
                Id = builder.NextId(),
                OurOrganisation = org1.Id,
                TheirOrganisation = org2.Id,
                AllowOrDeny = DtoMappingAttribute.GetValueFromEnum(permission),
                BuyOrSell = DtoMappingAttribute.GetValueFromEnum(buySellInd),
                LastUpdated = DateTime.UtcNow,
                LastUpdatedUserId = builder.DBContext.UserInfoes.Last().Id,
                ProductId = productId
            };

            builder.DBContext.CounterpartyPermissions.Add(perm);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        /// <summary>
        /// Buy and Sell are both permissioned
        /// </summary>
        public static CrmDataBuilder AddBrokerBuySellPermission(this CrmDataBuilder builder, OrganisationDto org1, OrganisationDto org2, long productId, Permission permission = Permission.Allow)
        {
            AddBrokerPermission(builder, org1, org2, BuyOrSell.Buy, productId, permission);
            AddBrokerPermission(builder, org1, org2, BuyOrSell.Sell, productId, permission);
            return builder;
        }

        public static CrmDataBuilder AddBrokerPermission(this CrmDataBuilder builder, OrganisationDto org1, OrganisationDto org2, BuyOrSell buySellInd, long productId, Permission permission = Permission.Allow)
        {
            var perm = new BrokerPermissionDto
            {
                Id = builder.NextId(),
                OurOrganisation = org1.Id,
                TheirOrganisation = org2.Id,
                AllowOrDeny = DtoMappingAttribute.GetValueFromEnum(permission),
                BuyOrSell = DtoMappingAttribute.GetValueFromEnum(buySellInd),
                LastUpdated = DateTime.UtcNow,
                LastUpdatedUserId = builder.DBContext.UserInfoes.Last().Id,
                ProductId = productId
            };

            builder.DBContext.BrokerPermissions.Add(perm);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddSystemPrivilege(this CrmDataBuilder builder,string name, string organisationType, out SystemPrivilegeDto sp)
        {
            sp = builder.DBContext.SystemPrivileges.FirstOrDefault(p => p.Name == name)
                 ?? new SystemPrivilegeDto { Id = builder.NextId(), Name = name, Description = "Desc" + name, OrganisationType = organisationType };

            builder.DBContext.SystemPrivileges.Add(sp);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddSystemRolePrivilege(this CrmDataBuilder builder, long roleId, long privilegeId, out SystemRolePrivilegeDto systemRolePrivilege)
        {
            systemRolePrivilege = builder.DBContext.SystemRolePrivileges.FirstOrDefault(srp => srp.Role_Id_fk == roleId && srp.Privilege_Id_fk == privilegeId)
                 ?? new SystemRolePrivilegeDto() { System_Role_Privilege_Id = builder.NextId(), Role_Id_fk = roleId, Privilege_Id_fk = privilegeId};

            builder.DBContext.SystemRolePrivileges.Add(systemRolePrivilege);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddProductPrivelege(this CrmDataBuilder builder,string name,out ProductPrivilegeDto pp)
        {
            pp = builder.DBContext.ProductPrivileges.FirstOrDefault(p => p.Name == name);
            if (pp == null)
            {
                pp = new ProductPrivilegeDto { Id = builder.NextId(), Name = name, Description = "Desc" + name, OrganisationType = "A" };
            }

            builder.DBContext.ProductPrivileges.Add(pp);
            builder.DBContext.SaveChangesAndLog(-1);

            return builder;
        }

        public static CrmDataBuilder AddUserOrganisationRole(this CrmDataBuilder builder,UserInfoDto user,OrganisationDto org,string roleName)
        {
            var role = builder.DBContext.OrganisationRoles.Single(o => o.Name == roleName);

            if (user != null)
            {
                user.OrganisationDto = org;
                user.Organisation_Id_fk = org.Id;
            }

            var uor = new UserInfoRoleDto { Role_Id_fk = role.Id, UserInfo_Id_fk = user.Id, OrganisationRole = role, User = user };
            builder.DBContext.UserInfoRoles.Add(uor);

            return builder;
        }


        public static CrmDataBuilder AddProductRolePrivilege(this CrmDataBuilder builder,string rolename,long productId,string privilegeText,long? organisationId = null,long? productPrivilegeId = null)
        {
            var prp = new ProductRolePrivilegeDto { Id = builder.NextId() };

            if (organisationId.HasValue)
            {
                prp.Organisation_Id_fk = organisationId.Value;
            }
            if (productPrivilegeId.HasValue)
            {
                prp.ProductPrivilege_Id_fk = productPrivilegeId.Value;
            }

            prp.OrganisationRole = builder.DBContext.OrganisationRoles.Single(p => p.Name == rolename);
            prp.Role_Id_fk = prp.OrganisationRole.Id;
            prp.Product_Id_fk = productId;
            
            ProductPrivilegeDto pp;
            AddProductPrivelege(builder, privilegeText, out pp);
            prp.ProductPrivilege = pp;

            builder.DBContext.ProductRolePrivileges.Add(prp);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }


        public static CrmDataBuilder AddSystemPrivilegeToRole(this CrmDataBuilder builder,string systemprivname,string orgRoleName)
        {
            var sp = builder.DBContext.SystemPrivileges.FirstOrDefault(p => p.Name == systemprivname) ?? 
                new SystemPrivilegeDto
                {
                    Id = builder.NextId(),
                    Name = systemprivname,
                    Description = "Desc" + systemprivname
                };

            var or = builder.DBContext.OrganisationRoles.FirstOrDefault(p => p.Name == orgRoleName) ?? 
                new OrganisationRoleDto
                {
                    Id = builder.NextId(),
                    Name = orgRoleName,
                    Description = "Desc" + orgRoleName
                };

            builder.DBContext.SystemRolePrivileges.Add(
                new SystemRolePrivilegeDto
                {
                    OrganisationRole = or,
                    Privilege_Id_fk = sp.Id,
                    Role_Id_fk = or.Id,
                    System_Role_Privilege_Id = builder.NextId(),
                    SystemPrivilege = sp
                });
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddSubscribedProductPrivilege(this CrmDataBuilder builder, long organisationId, long productId, long productPrivilegeId, out SubscribedProductPrivilegeDto subscribedProductPrivilegeDto)
        {
            var ppDto = new ProductPrivilegeDto
            {
                DefaultRoleGroup = "T",
                Id = productPrivilegeId,
                Description = "Desc" + productPrivilegeId.ToString(),
                MarketMustBeOpen = true,
                Name = "Name" + productPrivilegeId.ToString(),
                OrganisationType = "T"
            };

            SubscribedProductPrivilegeDto sppDto = new SubscribedProductPrivilegeDto
            {
                CreatedBy = "1",
                Organisation_Id_fk = organisationId,
                Product_Id_fk = productId,
                ProductPrivilege_Id_fk = productPrivilegeId,
                ProductPrivilegeDto = ppDto
            };

            builder.DBContext.SubscribedProductPrivileges.Add(sppDto);

            var subProd = builder.DBContext.SubscribedProducts.FirstOrDefault(sp => sp.Product_Id_fk == productId && sp.Organisation_Id_fk == organisationId);

            if (subProd != null)
            {
                subProd.SubscribedProductPrivileges.Add(sppDto);
            }

            builder.DBContext.SaveChangesAndLog(-1);

            subscribedProductPrivilegeDto = sppDto;
            return builder;
        }

        public static CrmDataBuilder AddSubscribedProduct(this CrmDataBuilder builder,OrganisationDto org,long productId, out SubscribedProductDto subscribedProduct)
        {
            subscribedProduct = new SubscribedProductDto { OrganisationDto = org, Organisation_Id_fk = org.Id, Product_Id_fk = productId };
            subscribedProduct.SubscribedProductPrivileges =new Collection<SubscribedProductPrivilegeDto>();

            builder.DBContext.SubscribedProducts.Add(subscribedProduct);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddSystemEventNotification(this CrmDataBuilder builder, string notificationEmailAddress, long productId, OrganisationDto org, SystemEventType systemEventType,
            out SystemEventNotificationDto systemEventNotification)
        {
            systemEventNotification = new SystemEventNotificationDto
            {
                Id = builder.NextId(),
                NotificationEmailAddress = notificationEmailAddress,
                Organisation = org,
                OrganisationId_fk = org.Id,
                ProductId = productId,
                SystemEventId = (long) systemEventType
            };

            builder.DBContext.SystemEventNotifications.Add(systemEventNotification);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddUser(this CrmDataBuilder builder,string name,bool isBlocked,out UserInfoDto itm,out UserCredentialsDto credentialsDto,OrganisationDto org = null)
        {
            itm = new UserInfoDto
            {
                Email = name + "@testing.com",
                Id = builder.NextId(),
                Title = "Mr",
                Name = name,
                Username = name,
                IsBlocked = isBlocked,
                IsActive = true,
                ArgusCrmUsername = name
            };

            itm.OrganisationDto = org ?? builder.DBContext.Organisations.FirstOrDefault();

            builder.DBContext.UserInfoes.Add(itm);
            builder.DBContext.SaveChangesAndLog(-1);

            credentialsDto = new UserCredentialsDto
            {
                DateCreated = DateTime.Today,
                DefaultExpirationInMonths = 12,
                Expiration = DateTime.Today.AddMonths(1),
                User = itm,
                UserInfo_Id_fk = itm.Id,
                FirstTimeLogin = true,
                Id = builder.NextId()
            };

            builder.DBContext.UserCredentials.Add(credentialsDto);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddUser(this CrmDataBuilder builder,string name,bool isBlocked,out UserInfoDto userItem)
        {
            UserCredentialsDto credentialsDto;

            AddUser(builder, name, isBlocked, out userItem, out credentialsDto);
            return builder;
        }

        public static CrmDataBuilder AddUser(this CrmDataBuilder builder, string name, out UserInfoDto userItem)
        {
            UserCredentialsDto credentialsDto;

            AddUser(builder, name, false, out userItem, out credentialsDto);
            return builder;
        }

        public static CrmDataBuilder AddCounterpartyPermission(this CrmDataBuilder builder, OrganisationDto org1, OrganisationDto org2, string buyOrSell, string permission, long product)
        {
            CreateCounterpartyPermission(builder, org1, org2, buyOrSell, permission,product);
            return builder;
        }
     
        public static CrmDataBuilder CreateCounterpartyPermission(CrmDataBuilder builder, OrganisationDto org1, OrganisationDto org2, string buyOrSell, string permission,long product)
        {
            var permissionToAdd = new CounterpartyPermissionDto
            {
                AllowOrDeny = permission,
                BuyOrSell = buyOrSell,
                Id = builder.NextId(),
                LastUpdated = DateTime.Now,
                LastUpdatedUserId = builder.DBContext.UserInfoes.First().Id,
                OurOrganisation = org1.Id,
                TheirOrganisation = org2.Id,
                ProductId = product
                
            };
            builder.DBContext.CounterpartyPermissions.Add(permissionToAdd);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder CreatePermissionBroker(CrmDataBuilder builder, OrganisationDto org1, OrganisationDto org2, string buyOrSell, string permission, long product)
        {
            var permissionToAdd = new BrokerPermissionDto
            {
                AllowOrDeny = permission,
                BuyOrSell = buyOrSell,
                Id = builder.NextId(),
                LastUpdated = DateTime.Now,
                LastUpdatedUserId = builder.DBContext.UserInfoes.First().Id,
                OurOrganisation = org1.Id,
                TheirOrganisation = org2.Id,
                ProductId = product

            };
            builder.DBContext.BrokerPermissions.Add(permissionToAdd);
            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static CrmDataBuilder AddUser(this CrmDataBuilder builder, string name, bool isBlocked, out UserInfoDto userItem, OrganisationDto org)
        {
            UserCredentialsDto credentialsDto;

            AddUser(builder, name, false, out userItem, out credentialsDto, org);
            return builder;
        }

        public static CrmDataBuilder AddUser(this CrmDataBuilder builder,  string name, out UserInfoDto itm, out UserCredentialsDto credentialsDto)
        {
            AddUser(builder, name, false, out itm, out credentialsDto);
            return builder;
        }

        public static CrmDataBuilder AddExternalSystemAccount(this CrmDataBuilder builder, string externalSystemCode, string externalUserCode, UserInfoDto user, OrganisationDto org,
            out UserLoginExternalAccountDto externalSystemAccount)
        {
            var userLoginSource = new UserLoginSourceDto()
            {
                Id = builder.NextId(),
                Code = externalSystemCode,
                Name = externalSystemCode,
                OrganisationDto = org,
            };

            builder.DBContext.UserLoginSources.Add(userLoginSource);
            builder.DBContext.SaveChangesAndLog(-1);

            externalSystemAccount = new UserLoginExternalAccountDto()
            {
                Id = builder.NextId(),
                UserLoginSourceDto = userLoginSource,
                UserDto = user,
                ExternalUserCode = externalUserCode,
                ExternalUserName = externalUserCode
            };

            builder.DBContext.UserLoginExternalAccounts.Add(externalSystemAccount);
            builder.DBContext.SaveChangesAndLog(-1);
            
            return builder;
        }
    }
}