﻿using System;
using System.Linq;
using AOM.Repository.MySql.Crm;
using NUnit.Framework;

namespace AOM.Repository.MySql.Tests.Crm
{
    [TestFixture]
    public class CrmLogTests
    {
        [Test]
        public void MapperConfigured()
        {
            CrmLogMapper.GetLogObject(typeof(String), -1);
        }

        [Test]
        [Category("local")]
        public void LogsCrmOrganisationChanges()
        {
            using (var db = new CrmModel())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    var orgLogCount = db.OrganisationLogs.Count();
                    OrganisationDto organisation;
                    CrmDataBuilder.WithModel(db).AddOrganisation("testOrg", out organisation);
                    db.Set<OrganisationDto>().Add(organisation);
                    db.SaveChangesAndLog(-1);
                    Assert.That(db.OrganisationLogs.Count() > orgLogCount);
                    dbContextTransaction.Rollback();
                }
            }
        }

        [Test]
        [Category("local")]
        public void LogsCrmUserChanges()
        {
            using (var db = new CrmModel())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    var logCount = db.UserInfoLogs.Count();
                    var user = new UserInfoDto
                    {
                        Username = "test22",
                        Email = "a",
                        IsActive = true,
                        IsBlocked = false,
                        Name = "steve",
                        Organisation_Id_fk = db.Organisations.First().Id,
                        Title = "mr"
                    };
                    db.UserInfoes.Add(user);
                    db.SaveChangesAndLog(-1);
                    Assert.That(db.UserInfoLogs.Count() > logCount);
                    dbContextTransaction.Rollback();
                }
            }
        }
    }
}
