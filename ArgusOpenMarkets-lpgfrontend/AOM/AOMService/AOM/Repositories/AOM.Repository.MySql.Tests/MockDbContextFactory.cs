﻿namespace AOM.Repository.MySql.Tests
{
    using AOM.Repository.MySql.Aom;
    using AOM.Repository.MySql.Crm;

    public class MockDbContextFactory : IDbContextFactory
    {
        private readonly IAomModel _aomModelToReturn;

        private readonly ICrmModel _crmModelToReturn;

        private long _numberCallaToCreatePrivilegeModel = 0;

        public long NumberCallsToCreatePrivilegeModel
        {
            get { return _numberCallaToCreatePrivilegeModel; }
            set { _numberCallaToCreatePrivilegeModel = value; }
        }

        public MockDbContextFactory(IAomModel aomModelToReturn, ICrmModel crmModelToReturn)
        {
            _aomModelToReturn = aomModelToReturn;
            _crmModelToReturn = crmModelToReturn;
        }

        public static MockDbContextFactory Stub()
        {
            return new MockDbContextFactory(null, null);
        }

        public IAomModel CreateAomModel()
        {
            return _aomModelToReturn;
        }

        public ICrmModel CreateCrmModel()
        {
            return _crmModelToReturn;
        }

        public IOrganisationModel CreateOrganisationModel()
        {
            return _crmModelToReturn as IOrganisationModel;
        }

        public IUserModel CreateUserModel()
        {
            return _crmModelToReturn as IUserModel;
        }

        public IAuthenticationModel CreateAuthenticationModel()
        {
            return _crmModelToReturn as IAuthenticationModel;
        }

        public IClientLogModel CreateClientLogModel()
        {
            return _crmModelToReturn as IClientLogModel;
        }

        public IPrivilegeModel CreatePrivilegeModel()
        {
            NumberCallsToCreatePrivilegeModel = NumberCallsToCreatePrivilegeModel + 1;
            return _crmModelToReturn as IPrivilegeModel;
        }

        public IArgusCrmFeedModel CreateArgusCrmFeedModel()
        {
            return _crmModelToReturn as IArgusCrmFeedModel;
        }

        public IProductMetaDataModel CreateProductMetaDataModel()
        {
            return _aomModelToReturn as IProductMetaDataModel;
        }
    }
}