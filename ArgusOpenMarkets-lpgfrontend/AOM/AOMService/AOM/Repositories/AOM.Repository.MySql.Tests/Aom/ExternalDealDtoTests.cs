﻿using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using NUnit.Framework;

namespace AOM.Repository.MySql.Tests.Aom
{
    [TestFixture]
    public class ExternalDealDtoTests
    {
        [Test]
        [Category("local")]
        public void CanSaveAndRetrieveMetaDataOnAnExternalDealAgainstDatabase()
        {
            // This an important test as it confirms the serialisation is working for MetaData
            using (var trans = new TransactionScope())
            {
                using (var dbWrite = new AomModel())
                {
                    ExternalDealDto externalDeal = new ExternalDealDto();
                    PriceLineDto priceLine = null;

                    AomDataBuilder.WithModel(dbWrite)
                        .AddProduct("Product1")
                        .AddTenorCode("P")
                        .AddProductTenor("Product1_Tenor1")
                        .AddPriceLine(out priceLine, 111)
                        .AddProductMetaDataItem("Product1", "Quantity", 1, MetaDataTypes.String)
                        .AddProductMetaDataList("ColourList", -666, new List<MetaDataListItemDto>())
                        .AddProductMetaDataItem("Product1", "Colour", 2, MetaDataTypes.IntegerEnum, "ColourList")
                        .DBContext.SaveChangesAndLog(-1);

                    long productId = dbWrite.Products.Single(p => p.Name == "Product1").Id;

                    var metaDataItemStringDto = dbWrite.ProductMetaDataItems.Single(m => m.ProductId == productId && m.DisplayName == "Quantity");
                    var metaDataItemListDto = dbWrite.ProductMetaDataItems.Single(m => m.ProductId == productId && m.DisplayName == "Colour");

                    externalDeal.ProductId = productId;
                    externalDeal.DealStatus = DtoMappingAttribute.GetValueFromEnum(DealStatus.Pending);

                    externalDeal.MetaData = new MetaDataDto[] 
                    {
                        new MetaDataStringDto
                        {
                            ProductMetaDataId = metaDataItemStringDto.Id,
                            DisplayName = metaDataItemStringDto.DisplayName,
                            DisplayValue = "AValue"
                        },

                        new MetaDataIntegerEnumDto 
                        {
                            ProductMetaDataId = metaDataItemListDto.Id,
                            DisplayName = metaDataItemListDto.DisplayName,
                            DisplayValue = "yellowish",   
                            ItemValue = 100
                        }
                    };

                    //ACT
                    dbWrite.ExternalDeals.Add(externalDeal);
                    dbWrite.ExecuteAndReportErrors(db => db.SaveChangesAndLog(0));

                    StringAssert.DoesNotContain(typeof(MetaDataDto).Name, externalDeal.MetaDataJson, "Expected custom serialisation NOT to include c# type info");
                    StringAssert.DoesNotContain(typeof(MetaDataIntegerEnumDto).Name, externalDeal.MetaDataJson, "Expected custom serialisation NOT to include c# type info");
                    StringAssert.DoesNotContain(typeof(MetaDataStringDto).Name, externalDeal.MetaDataJson, "Expected custom serialisation NOT to include c# type info");


                    using (var dbRead = new AomModel())
                    {
                        var externalDealRead = dbRead.ExternalDeals.SingleOrDefault(o => o.Id == externalDeal.Id);

                        //ASSERT
                        Assert.IsNotNull(externalDealRead);

                        Assert.IsNotNull(externalDealRead.MetaData, "Expected metadata to be persisted and retrieved from database");
                        Assert.AreEqual(2, externalDealRead.MetaData.Length);
                        Assert.AreEqual(typeof(MetaDataStringDto), externalDealRead.MetaData[0].GetType(), "Expected correct type to be created when read from database");
                        Assert.AreEqual(MetaDataTypes.String, externalDealRead.MetaData[0].MetaDataType);
                        Assert.AreEqual("AValue", externalDealRead.MetaData[0].DisplayValue);
                        Assert.AreEqual("Quantity", externalDealRead.MetaData[0].DisplayName);

                        Assert.AreEqual(typeof(MetaDataIntegerEnumDto), externalDealRead.MetaData[1].GetType(), "Expected correct type to be created when read from database");
                        Assert.AreEqual(MetaDataTypes.IntegerEnum, externalDealRead.MetaData[1].MetaDataType);
                        Assert.AreEqual("yellowish", externalDealRead.MetaData[1].DisplayValue);
                        Assert.AreEqual("Colour", externalDealRead.MetaData[1].DisplayName);
                        Assert.AreEqual(100, ((MetaDataIntegerEnumDto)externalDealRead.MetaData[1]).ItemValue);
                    }
                }
                trans.Dispose();
            }
        }
    }
}
