﻿using System.Data.Entity;
using AOM.Repository.MySql.Aom;

namespace AOM.Repository.MySql.Tests.Aom
{
    public class MockAomModel : IAomModel
    {
        public IDbSet<NewsDto> News { get; set; }
        public IDbSet<NewsContentStreamDto> NewsContentStream { get; set; } 
        public IDbSet<AssessmentDto> Assessments { get; set; }
        public IDbSet<CommodityDto> Commodities { get; set; }
        public IDbSet<CommodityClassDto> CommodityClasses { get; set; }
        public IDbSet<CommodityGroupDto> CommodityGroups { get; set; }
        public IDbSet<CommodityTypeDto> CommodityTypes { get; set; }
        public IDbSet<CommodityContentStreamDto> CommodityContentStreams { get; set; }
        public IDbSet<ContractTypeDto> ContractTypes { get; set; }
        public IDbSet<UnitLogDto> UnitLogs { get; set; } 
        public IDbSet<CurrencyDto> Currencies { get; set; }
        public IDbSet<DealDto> Deals { get; set; }
        public IDbSet<DealStatusDto> DealStatus { get; set; }
        public IDbSet<DeliveryLocationDto> DeliveryLocations { get; set; }
        public IDbSet<EmailDto> Emails { get; set; }
        public IDbSet<ErrorLogDto> Errors { get; set; } 
        public IDbSet<ExternalDealDto> ExternalDeals { get; set; }
        public IDbSet<MarketDto> Markets { get; set; }
        public IDbSet<MarketInfoDto> MarketInfoItems { get; set; } 
        public IDbSet<MarketInfoProductChannelDto> MarketInfoProductChannels { get; set; } 
        public IDbSet<MarketItemStatusDto> MarketItemStatus { get; set; }
        public IDbSet<MarketItemTypeDto> MarketItemTypes { get; set; }
        public IDbSet<MarketTickerItemDto> MarketTickerItems { get; set; }
        public IDbSet<MarketTickerItemProductChannelDto> MarketTickerItemProductChannels { get; set; }
        public IDbSet<OrderDto> Orders { get; set; }
        public IDbSet<OrderStatusDto> OrderStatus { get; set; }
        public IDbSet<ProductDto> Products { get; set; }
        public IDbSet<TenorCodeDto> TenorCodes { get; set; }
        public IDbSet<ProductTenorDto> ProductTenors { get; set; }
        public IDbSet<ScheduledTaskDto> ScheduledTasks { get; set; } 
        public IDbSet<UnitDto> Units { get; set; }
        public IDbSet<PriceTypeDto> PriceTypes { get; set; }
        public IDbSet<PriceBasisDto> PriceBases { get; set; }
        public IDbSet<ProductPriceLineDto> ProductPriceLines { get; set; }
        public IDbSet<ProductPriceLineBasisPeriodDto> ProductPriceLineBasesPeriods { get; set; }
        //public IDbSet<ProductPriceLinePeriodDto> ProductPriceLinePeriods { get; set; }
        public IDbSet<PriceLineDto> PriceLines { get; set; }
        public IDbSet<OrderPriceLineDto> OrderPriceLines { get; set; }
        public IDbSet<PriceLineBasisDto> PriceLineBases { get; set; }
        public IDbSet<PeriodDto> Periods { get; set; }
        public IDbSet<ProductTenorPeriodDto> ProductTenorPeriods { get; set; }

        // MetaData tables
        public IDbSet<ProductMetaDataItemDto> ProductMetaDataItems{ get; set; }
        public IDbSet<MetaDataListItemDto> MetaDataListItems { get; set; }
        public IDbSet<MetaDataListDto> MetaDataLists { get; set; }
        public IDbSet<ProductMetaDataItemLogDto> ProductMetaDataItemsLogs { get; set; }
        public IDbSet<MetaDataListItemLogDto> MetaDataListItemsLogs { get; set; }
        public IDbSet<MetaDataListLogDto> MetaDataListsLogs { get; set; }
        
        // Log Tables
        public IDbSet<AssessmentLogDto> AssessmentsLogs { get; set; }
        public IDbSet<CommodityLogDto> CommodityLogs { get; set; }
        public IDbSet<ContractTypeLogDto> ContractTypeLogs { get; set; }
        public IDbSet<CommonQuantityValuesLogDto> CommonQuantityValueLogs { get; set; }
        public IDbSet<CommodityClassLogDto> CommodityClassLogs { get; set; } 
        public IDbSet<DealLogDto> DealLogs { get; set; }
        public IDbSet<ExternalDealLogDto> ExternalDealLogs { get; set; }
        public IDbSet<MarketInfoLogDto> MarketInfoItemLogs { get; set; }
        public IDbSet<MarketTickerItemLogDto> MarketTickerItemLogs { get; set; }
        public IDbSet<OrderLogDto> OrderLogs { get; set; }
        public IDbSet<ProductLogDto> ProductLogs { get; set; }
        public IDbSet<TenorCodeLogDto> TenorLogs { get; set; }
        public IDbSet<ProductTenorLogDto> ProductTenorLogs { get; set; }
        public IDbSet<PriceBasisLogDto> PriceBasisLogs { get; set; }
        public IDbSet<PriceLineLogDto> PriceLineLogs { get; set; }
        public IDbSet<PriceLineBasisLogDto> PriceLineBaseLogs { get; set; }
        public IDbSet<PeriodLogDto> PricePeriodLogs { get; set; }
        public IDbSet<ProductTenorPeriodLogDto> ProductTenorPeriodLogs { get; set; }
        public IDbSet<ProductPriceLineLogDto> ProductPriceLineLogs { get; set; }
        public IDbSet<ProductPriceLineBasisPeriodLogDto> ProductPriceLineBasesPeriodLogs { get; set; }

        public MockAomModel()
        {
            News = new FakeDbSet<NewsDto>();
            NewsContentStream = new FakeDbSet<NewsContentStreamDto>();
            Assessments = new FakeDbSet<AssessmentDto>();
            Commodities = new FakeDbSet<CommodityDto>();
            CommodityClasses = new FakeDbSet<CommodityClassDto>();
            CommodityGroups = new FakeDbSet<CommodityGroupDto>();
            CommodityTypes = new FakeDbSet<CommodityTypeDto>();
            ContractTypes = new FakeDbSet<ContractTypeDto>();
            Currencies = new FakeDbSet<CurrencyDto>();
            UnitLogs = new FakeDbSet<UnitLogDto>();
            Deals = new FakeDbSet<DealDto>();
            DealStatus = new FakeDbSet<DealStatusDto>();
            DeliveryLocations = new FakeDbSet<DeliveryLocationDto>();
            Emails = new FakeDbSet<EmailDto>();
            ExternalDeals = new FakeDbSet<ExternalDealDto>();
            Markets = new FakeDbSet<MarketDto>();
            MarketInfoItems = new FakeDbSet<MarketInfoDto>();
            MarketInfoProductChannels = new FakeDbSet<MarketInfoProductChannelDto>();
            MarketItemStatus = new FakeDbSet<MarketItemStatusDto>();
            MarketItemTypes = new FakeDbSet<MarketItemTypeDto>();
            MarketTickerItems = new FakeDbSet<MarketTickerItemDto>();
            MarketTickerItemProductChannels = new FakeDbSet<MarketTickerItemProductChannelDto>();
            Orders = new FakeDbSet<OrderDto>();
            OrderStatus = new FakeDbSet<OrderStatusDto>();
            Products = new FakeDbSet<ProductDto>();
            TenorCodes = new FakeDbSet<TenorCodeDto>();
            ProductTenors = new FakeDbSet<ProductTenorDto>();   
            ScheduledTasks = new FakeDbSet<ScheduledTaskDto>();
            Units = new FakeDbSet<UnitDto>();
            Errors = new FakeDbSet<ErrorLogDto>();
            PriceTypes = new FakeDbSet<PriceTypeDto>();
            PriceBases = new FakeDbSet<PriceBasisDto>();
            ProductPriceLines = new FakeDbSet<ProductPriceLineDto>();
            //ProductPriceLineBases = new FakeDbSet<ProductPriceLineBasisDto>();
            //ProductPriceLinePeriods = new FakeDbSet<ProductPriceLinePeriodDto>();
            PriceLines = new FakeDbSet<PriceLineDto>();
            OrderPriceLines = new FakeDbSet<OrderPriceLineDto>();
            PriceLineBases = new FakeDbSet<PriceLineBasisDto>();

            ProductMetaDataItems = new FakeDbSet<ProductMetaDataItemDto>();
            MetaDataListItems = new FakeDbSet<MetaDataListItemDto>();
            MetaDataLists = new FakeDbSet<MetaDataListDto>();
            
            // Log Tables
            AssessmentsLogs = new FakeDbSet<AssessmentLogDto>();
            CommodityLogs = new FakeDbSet<CommodityLogDto>();
            ContractTypeLogs = new FakeDbSet<ContractTypeLogDto>();
            CommonQuantityValueLogs = new FakeDbSet<CommonQuantityValuesLogDto>();
            CommodityClassLogs = new FakeDbSet<CommodityClassLogDto>();
            DealLogs = new FakeDbSet<DealLogDto>();
            ExternalDealLogs = new FakeDbSet<ExternalDealLogDto>();
            MarketInfoItemLogs = new FakeDbSet<MarketInfoLogDto>();
            MarketTickerItemLogs = new FakeDbSet<MarketTickerItemLogDto>();
            OrderLogs = new FakeDbSet<OrderLogDto>();
            ProductLogs = new FakeDbSet<ProductLogDto>();
            TenorLogs = new FakeDbSet<TenorCodeLogDto>();
            ProductTenorLogs = new FakeDbSet<ProductTenorLogDto>();
            PriceBasisLogs = new FakeDbSet<PriceBasisLogDto>();
            PriceLineLogs = new FakeDbSet<PriceLineLogDto>();
            PriceLineBaseLogs = new FakeDbSet<PriceLineBasisLogDto>();

            // Error Log
            Errors = new FakeDbSet<ErrorLogDto>();
        }

        public void Dispose()
        {           
        }

        public int SaveChangesCalledCount { get;  set; }

        public int SaveChangesAndLog(long userId)
        {
            SaveChangesCalledCount+=1;
            return 0;
        }
    }
    
}
