﻿using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AOM.Repository.MySql.Aom;
using NUnit.Framework;

namespace AOM.Repository.MySql.Tests.Aom
{
    [TestFixture]
    public class OrderDtoTests
    {             
        [Test]
        //[Category("local")]
        public void CanSaveAndRetrieveMetaDataOnAnOrderAgainstDatabase()
        {
           // This an important test as it confirms the serialisation is working for MetaData
           using (var trans = new TransactionScope())
            {
                using (var dbWrite = new MockAomModel())
                {
                    //ARRANGE
                    OrderDto orderToBeWritten;
                    PriceBasisDto priceBasisDto;
                    PriceLineDto priceLineDto;
                    var price = 800;
                    var amount = 20000;
                    var testUserId = -666;
                    
                    AomDataBuilder.WithModel(dbWrite)
                        .AddDeliveryLocation("DeliveryLocation1")
                        .AddProduct("Product1")
                        .AddTenorCode("P")
                        .AddProductTenor("Product1_Tenor1", dbWrite.Products.First().Id)
                        .AddProductMetaDataItem("Product1", "Quantity", 1, MetaDataTypes.String)                        
                        .AddProductMetaDataList("ColourList", -666, new List<MetaDataListItemDto>())
                        .AddProductMetaDataItem("Product1", "Colour", 2, MetaDataTypes.IntegerEnum, "ColourList")
                        .AddPriceType("FIXED")
                        .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                        .AddPriceLine(out priceLineDto, testUserId)
                        .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                        .AddOrder(userId: testUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "A", itm: out orderToBeWritten, orderStatus: "A",  price: price, quantity: amount, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                        .OpenAllMarkets()
                        .DBContext.SaveChangesAndLog(-1);

                    long productId = dbWrite.Products.Single(p => p.Name == "Product1").Id;
                    var metaDataItemStringDto = dbWrite.ProductMetaDataItems.Single(m => m.ProductId == productId && m.DisplayName=="Quantity");
                    var metaDataItemListDto = dbWrite.ProductMetaDataItems.Single(m => m.ProductId == productId && m.DisplayName == "Colour");

                    orderToBeWritten.MetaData = new MetaDataDto[] 
                    {
                        new MetaDataStringDto
                        {
                            ProductMetaDataId = metaDataItemStringDto.Id,
                            DisplayName = metaDataItemStringDto.DisplayName,
                            DisplayValue = "AValue"
                        },

                        new MetaDataIntegerEnumDto
                        {
                            ProductMetaDataId = metaDataItemListDto.Id,
                            DisplayName = metaDataItemListDto.DisplayName,
                            DisplayValue = "yellowish",   
                            ItemValue = 100
                        }
                    };

                    //ACT
                    //dbWrite.Orders.Add(orderToBeWritten);
                    dbWrite.ExecuteAndReportErrors(db => db.SaveChangesAndLog(0));

                    StringAssert.DoesNotContain(typeof(MetaDataDto).Name, orderToBeWritten.MetaDataJson, "Expected custom serialisation NOT to include c# type info");
                    StringAssert.DoesNotContain(typeof(MetaDataIntegerEnumDto).Name, orderToBeWritten.MetaDataJson, "Expected custom serialisation NOT to include c# type info");
                    StringAssert.DoesNotContain(typeof(MetaDataStringDto).Name, orderToBeWritten.MetaDataJson, "Expected custom serialisation NOT to include c# type info");

                    //using (var dbRead = new AomModel())
                    //using (var dbRead = new MockAomModel()) 
                    //{
                        var orderRead = dbWrite.Orders.SingleOrDefault(o => o.Id == orderToBeWritten.Id);

                        //ASSERT
                        Assert.IsNotNull(orderRead);

                        Assert.IsNotNull(orderRead.MetaData, "Expected metadata to be persisted and retrieved from database");
                        Assert.AreEqual(2, orderRead.MetaData.Length);
                        Assert.AreEqual(typeof(MetaDataStringDto), orderRead.MetaData[0].GetType(), "Expected correct type to be created when read from database");
                        Assert.AreEqual(MetaDataTypes.String, orderRead.MetaData[0].MetaDataType);
                        Assert.AreEqual("AValue", orderRead.MetaData[0].DisplayValue);
                        Assert.AreEqual("Quantity", orderRead.MetaData[0].DisplayName);

                        Assert.AreEqual(typeof(MetaDataIntegerEnumDto), orderRead.MetaData[1].GetType(), "Expected correct type to be created when read from database");
                        Assert.AreEqual(MetaDataTypes.IntegerEnum, orderRead.MetaData[1].MetaDataType);
                        Assert.AreEqual("yellowish", orderRead.MetaData[1].DisplayValue);
                        Assert.AreEqual("Colour", orderRead.MetaData[1].DisplayName);
                        Assert.AreEqual(100, ((MetaDataIntegerEnumDto)orderRead.MetaData[1]).ItemValue);                                             
                    //}                    
                }
                trans.Dispose();
            }            
        }
    }
}

