﻿using System.Data.Entity;
using AOM.Repository.MySql.Aom;

namespace AOM.Repository.MySql.Tests.Aom
{
    public class AomDataBuilder
    {
        public static int id;

        public int NextId()
        {
            if (DBContext is DbContext) return 0;
            id -= 1;
            return id;
        }

        public IAomModel DBContext { get; private set; }

        private AomDataBuilder(IAomModel model)
        {
            DBContext = model;
        }

        public static AomDataBuilder WithModel(IAomModel model)
        {             
            return new AomDataBuilder(model);
        }     
    }
}
