﻿namespace AOM.Repository.MySql.Tests.Aom
{
    using System.Text;
    using Utils.Javascript.Extension;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using AOM.Repository.MySql.Aom;
    using global::MySql.Data.MySqlClient;
    using Ninject.Infrastructure.Language;
    using NUnit.Framework;

    [TestFixture]
    public class AomModelTests
    {
        [Test]
        public void DatabaseShouldNotPointAtLive()
        {
            const string liveDb = "aom-mysql.cs3w6qomvukg.eu-west-1.rds.amazonaws.com";
            string filePath = Directory.GetParent(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath).Parent.Parent.Parent.FullName;

            string[] configFiles = Directory.GetFiles(filePath, "*.config", SearchOption.AllDirectories).Where(x => x.EndsWith("App.config") || x.EndsWith("Web.config")).ToArray();

            foreach (var configFile in configFiles)
            {
                string contents = File.ReadAllText(configFile);
                Assert.False(contents.Contains(liveDb), configFile + " points at live containing:" + liveDb);
            }
        }

        [Ignore("ignore")]
        public void DatabaseShouldPointAtDev()
        {
            const string AOMdev = @"server=aominstancedevpri.co46fwzab7bh.eu-west-1.rds.amazonaws.com;user id=aom_user;password=Argu5Med1a;persistsecurityinfo=True;database=aom";
            string filePath = Directory.GetParent(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath).Parent.Parent.Parent.FullName;
            string[] configFiles = Directory.GetFiles(filePath, "*Processor.exe", SearchOption.AllDirectories);
            Assert.Greater(configFiles.Length, 10, "Expected to find several config files to test");
            foreach (var configFile in configFiles)
            {
                ConfigurationManager.OpenExeConfiguration(configFile);
                var aom = ConfigurationManager.ConnectionStrings["AomModel"];
                if (aom != null)
                {
                    Assert.True(AOMdev.Equals(aom.ConnectionString));
                }
            }
        }

        [Test]
        public void PropertiesOnDtoShouldExistOnLogEnties()
        {
            var anchorType = typeof(OrderLogDto);
            const string logDtoSuffix = "LogDto";

            var logDtos = anchorType.Assembly.ExportedTypes.Where(o => o.Name.EndsWith(logDtoSuffix))
                .Where(o => o.IsClass)
                .Where(x => x.Namespace.Equals(anchorType.Namespace)).ToList();

            Assert.Greater(logDtos.Count(), 0);

            foreach (var logDto in logDtos)
            {
                Debug.WriteLine("->Checking AomModel." + logDto.Name);

                if (logDto.Name == typeof(ErrorLogDto).Name) continue;

                string logDtoName = logDto.Name;
                var expectedMatchingDto = logDtoName.Substring(0, logDtoName.Length - logDtoSuffix.Length) + "Dto";

                var associatedDtoForLogDto = anchorType.Assembly.ExportedTypes.SingleOrDefault(o => o.Name == expectedMatchingDto);
                Assert.IsNotNull(associatedDtoForLogDto, "Expected to find a DTO Class called '" + expectedMatchingDto + "' to match the log DTO class '" + logDtoName + "'");

                foreach (var dtoProperty in associatedDtoForLogDto.GetProperties().Where(p => !p.IsPrivate()))
                {
                    Debug.WriteLine(" AomModel." + associatedDtoForLogDto.Name + "." + dtoProperty.Name);
                    
                    if (dtoProperty.PropertyType.Name.EndsWith("Dto"))
                    {
                        //skip objects which should have an associated Id field the test would check
                        Debug.WriteLine("------- skipped DTO property " + dtoProperty.Name);
                        continue;
                    }

                    if (dtoProperty.PropertyType.IsInterface)
                    {
                        Debug.WriteLine("------- skipped IsInterface property " + dtoProperty.Name);
                        continue;
                    }

                    var logDtoProperty = logDto.GetProperties().SingleOrDefault(p => p.Name == dtoProperty.Name);

                    Assert.IsNotNull(logDtoProperty, "Expected the DTO " + logDto.Name + " to have the property " + dtoProperty.Name + " as the DTO has it.");
                    Assert.AreEqual(logDtoProperty.PropertyType, dtoProperty.PropertyType, "Expected the DTO " + logDto.Name + " to have the same property type " + dtoProperty.Name + " as the DTO.");
                }
            }
        }

        [Test]
        public void ValidateLogEntiesHaveMandatoryProperties()
        {
            const string logDtoSuffix = "_LogDto";
            var anchorType = typeof(OrderLogDto);

            foreach (var logDto in anchorType.Assembly.ExportedTypes.Where(o => o.Name.EndsWith(logDtoSuffix)).Where(x => x.Namespace.Equals(anchorType.Namespace)))
            {
                Debug.WriteLine("->Checking AomModel." + logDto.Name);
                Assert.IsNotNull(logDto.GetProperties().SingleOrDefault(p => p.Name == "LastUpdated" && !p.IsPrivate() && p.PropertyType == typeof(DateTime)),
                    logDto.Name + " missing public LastUpdated string property");
                Assert.IsNotNull(logDto.GetProperties().SingleOrDefault(p => p.Name == "LastUpdatedUserId" && !p.IsPrivate() && p.PropertyType == typeof(long)),
                    logDto.Name + " missing public LastUpdatedUserId long property");
            }
        }

        [Test]
        public void ModelShouldNotSupportLazyLoading()
        {
            RepositoryTestHelper.AssertModelHasLazyLoadDisabled<AomModel>();
            RepositoryTestHelper.AssertModelHasLazyLoadDisabled<ProductMetaDataModel>();
        }

        [Test]
        [Category("local")]
        public void CanQueryAllAomDtos()
        {
            RepositoryTestHelper.AssertCanLoadObjectsInModel<AomModel>();
            RepositoryTestHelper.AssertCanLoadObjectsInModel<ProductMetaDataModel>();
        }

        [Test]
        public void AllLastUpdatedPropertiesShouldHaveConcurrencyCheckAttribute()
        {
            var efEntites = typeof(ProductDto).Assembly.GetExportedTypes().Where(t => t.HasAttribute(typeof(TableAttribute))).ToList();

            var efProps = efEntites.SelectMany(t => t.GetProperties().Select(p => new
            {
                TableName = ((TableAttribute) t.GetCustomAttributes(typeof(TableAttribute), true).Single()).Name,
                ColumnName =
                ((ColumnAttribute) p.GetCustomAttributes(typeof(ColumnAttribute), true).SingleOrDefault()) != null
                    ? ((ColumnAttribute) p.GetCustomAttributes(typeof(ColumnAttribute), true).Single()).Name : p.Name,
                ClassName = t.Name,
                PropertyName = p.Name,
                ConcurrencyCheckAttribute = ((ConcurrencyCheckAttribute)p.GetCustomAttributes(typeof(ConcurrencyCheckAttribute), true).SingleOrDefault())
            })).ToList();

            Assert.Greater(efProps.Count(), 1, "Expected to have initialised data to find issues within it !");

            var offenders = efProps.Where(p => String.Compare(p.PropertyName, "LastUpdated", StringComparison.OrdinalIgnoreCase) == 0
            && p.ConcurrencyCheckAttribute == null
            && p.ClassName.DoesNotEndWith("LogDto")).ToList();

            var sb = new StringBuilder();
            offenders.ForEach(p => sb.AppendFormat("{0}.{1} missing concurreny check.\n", p.ClassName, p.PropertyName));

            Assert.AreEqual(0, offenders.Count(), sb.ToString());
        }

        [Test]
        public void AllStringPropertiesOnDtoHaveMaxLengthSetToMatchDB()
        {
            var efEntites = typeof(ProductDto).Assembly.GetExportedTypes().Where(t => t.HasAttribute(typeof(TableAttribute))).ToList();

            //Build a list EF entites with their string columns and their associated StringLengthAttribute if present
            var efProps = efEntites.SelectMany(t => t.GetProperties().Where(p => p.PropertyType == typeof(string)).Select(p => new
            {
                TableName = ((TableAttribute) t.GetCustomAttributes(typeof(TableAttribute), true).Single()).Name,
                ColumnName = ((ColumnAttribute) p.GetCustomAttributes(typeof(ColumnAttribute), true).SingleOrDefault()) != null
                    ? ((ColumnAttribute) p.GetCustomAttributes(typeof(ColumnAttribute), true).Single()).Name : p.Name,
                ClassName = t.Name,
                PropertyName = p.Name,
                StringLengthAttribute = ((StringLengthAttribute)p.GetCustomAttributes(typeof(StringLengthAttribute), true).SingleOrDefault())
            })).ToList();

            using (var dbConnector = new MySqlConnection(AomModelSchemaTests.AomConnectionString))
            {
                dbConnector.Open();
                var schema = dbConnector.ConnectionString.Split(';')[4].Split('=')[1];

                var qry = string.Format(@"SELECT tbl.table_schema, tbl.table_name, cols.COLUMN_NAME, cols.DATA_TYPE, cols.CHARACTER_MAXIMUM_LENGTH
                            FROM INFORMATION_SCHEMA.columns cols 
                            INNER JOIN INFORMATION_SCHEMA.tables tbl ON tbl.table_schema = cols.table_schema and tbl.table_name=cols.TABLE_NAME
                            WHERE tbl.table_schema = '{0}'
                            AND cols.data_type in ('char', 'varchar')", schema);

                var mysqlCmd = new MySqlCommand(qry, dbConnector);

                var reader = mysqlCmd.ExecuteReader();

                while (reader.Read())
                {
                    var tableSchema = reader.GetString("table_schema");
                    string fullTableName = String.Format("{0}.{1}", reader.GetString("table_schema"), reader.GetString("table_name"));

                    string columnName = reader.GetString("COLUMN_NAME");
                    int columnWidthInDb = reader.GetInt32("CHARACTER_MAXIMUM_LENGTH");

                    var efItem = efProps.Where(e => String.Equals(fullTableName, e.TableName, StringComparison.CurrentCultureIgnoreCase)
                    && String.Equals(columnName, e.ColumnName, StringComparison.CurrentCultureIgnoreCase)).SingleOrDefault();

                    if (efItem != null)
                    {
                        Trace.WriteLine(">Checking " + fullTableName + "." + columnName);
                        if (efItem.StringLengthAttribute != null)
                        {
                            Assert.AreEqual(columnWidthInDb, efItem.StringLengthAttribute.MaximumLength, string.Format(
                                "The column {0}.{1} has a maxlength in the database less than defined in EF. Conn String: {2}  Column Name: {3}   table schema: {4}",
                                efItem.ClassName, efItem.PropertyName, AomModelSchemaTests.AomConnectionString, columnName, tableSchema));
                        }
                        else
                        {
                            Assert.Fail("\tDTO property {0}.{1} should have max length defined", efItem.ClassName, efItem.PropertyName);
                        }
                    }
                }
            }
        }
    }
}