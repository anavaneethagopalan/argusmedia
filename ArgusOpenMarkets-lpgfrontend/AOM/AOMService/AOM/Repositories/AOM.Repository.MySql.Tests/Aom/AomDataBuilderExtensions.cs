﻿using System;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;

namespace AOM.Repository.MySql.Tests.Aom
{
    public static class AomDataBuilderExtensions
    {
        private static DateTimeProvider _dateTimeProvider;

        static AomDataBuilderExtensions()
        {
            _dateTimeProvider = new DateTimeProvider();
        }

        public static AomDataBuilder AddNews(this AomDataBuilder builder, string cmsId, string story, bool isFree, DateTime publicationDate, bool addContentStream = true)
        {
            if (builder.DBContext.News.Any(x => x.CmsId == cmsId)) return builder;

            var newsItem = new NewsDto
            {
                CmsId = cmsId,
                Story = story,
                IsFree = isFree,
                PublicationDate = publicationDate,
                ContentStreams = new List<NewsContentStreamDto>()
            };

            if (addContentStream)
            {
                newsItem.ContentStreams.Add(new NewsContentStreamDto {ContentStreamId = 1});
            }

            builder.DBContext.News.Add(newsItem);
            if (addContentStream)
            {
                var newsContentStreamDto = new NewsContentStreamDto {ContentStreamId = 1, News = newsItem};
                builder.DBContext.NewsContentStream.Add(newsContentStreamDto);

            }
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddOrderPriceLine(this AomDataBuilder builder, OrderDto orderDto, PriceLineDto priceLineDto, OrderStatus status)
        {
            var itm = new OrderPriceLineDto
            {
                Id = builder.NextId(),
                PriceLineDto = priceLineDto,
                OrderDto = orderDto,
                OrderStatusCode = status.ToString()
            };

            builder.DBContext.OrderPriceLines.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddOrder(this AomDataBuilder builder, long userId, string productTenorName, string bidOrAsk, out OrderDto itm, 
            string orderStatus = "A", decimal quantity = 20000, decimal price = 123M, long? brokerId = null, long? principalOrganisationId = null, string brokerRestriction = "A", 
            string lastOrderStatusCode = null, long? brokerOrganisationId = null, List<PriceLineDto> pLinesDto = null)
        {
            itm = new OrderDto
            {
                Id = builder.NextId(),
                Price = price,
                InternalOrExternal = "I",
                BidOrAsk = bidOrAsk,
                UserId = userId,
                OrderStatusCode = orderStatus,
                Quantity = quantity,
                LastUpdatedUserId = userId,
                LastUpdated = _dateTimeProvider.UtcNow,
                BrokerId = brokerId,
                BrokerOrganisationId = brokerOrganisationId,
                DeliveryStartDate = DateTime.Today.AddDays(1),
                DeliveryEndDate = DateTime.Today.AddDays(4),
                BrokerRestriction = brokerRestriction,
                LastOrderStatusCode = lastOrderStatusCode,
                OrganisationId = principalOrganisationId ?? 0,
                ProductTenorDto = builder.DBContext.ProductTenors.OrderByDescending(pt => pt.LastUpdated).First(pt => pt.Name == productTenorName)
            };

            itm.ProductTenorId = itm.ProductTenorDto.Id;

            if (pLinesDto != null)
            {
                itm.OrderPriceLines = new List<OrderPriceLineDto>();
                foreach (var pLineDto in pLinesDto)
                {
                    var orderPriceLine = new OrderPriceLineDto
                    {
                        Id = builder.NextId(),
                        PriceLineDto = pLineDto,
                        OrderDto = itm,
                        OrderStatusCode = orderStatus,
                        LastUpdatedUserId = itm.LastUpdatedUserId,
                        LastUpdated = itm.LastUpdated
                    };
                    itm.OrderPriceLines.Add(orderPriceLine);
                }
            }
            builder.DBContext.Orders.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            
            return builder;
        }

        public static AomDataBuilder AddOrderStatus(this AomDataBuilder builder, string code, string name)
        {
            if (builder.DBContext.OrderStatus.Any(x => x.Code == code)) return builder;

            var itm = new OrderStatusDto { Code = code, Name = name };
            builder.DBContext.OrderStatus.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddDeal(this AomDataBuilder builder, int userId, long initialOrderId, long matchingOrderId, out DealDto itm, string dealStatus = "E")
        {
            itm = new DealDto
            {
                Id = builder.NextId(),
                InitialOrderId = initialOrderId,
                InitialOrderDto = builder.DBContext.Orders.FirstOrDefault(o => o.Id == initialOrderId),
                MatchingOrderDto = builder.DBContext.Orders.FirstOrDefault(o => o.Id == matchingOrderId),
                MatchingOrderId = matchingOrderId,
                DealStatus = dealStatus
            };

            itm.LastUpdated = _dateTimeProvider.UtcNow;
            itm.LastUpdatedUserId = -1;

            builder.DBContext.Deals.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddDealStatus(this AomDataBuilder builder, string code, string name)
        {
            if (builder.DBContext.DealStatus.Any(x => x.Code == code)) return builder;

            var itm = new DealStatusDto { Code = code, Name = name };
            builder.DBContext.DealStatus.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddProductMetaDataList(this AomDataBuilder builder, string listName, int listId, List<MetaDataListItemDto> elements)
        {
            var list = builder.DBContext.MetaDataLists.FirstOrDefault(i => i.Description == listName);
       
            if (list == null)
            {
                list = new MetaDataListDto
                {
                    Description = listName,
                    Id = listId,
                    IsDeleted = false,
                    MetaDataListItems = new Collection<MetaDataListItemDto>()
                };
                builder.DBContext.MetaDataLists.Add(list);
                builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            }

            foreach (MetaDataListItemDto metaDataListItemDto in elements)
            {
                //var valueText = elements[i];

                if (!builder.DBContext.MetaDataListItems.Any(e => e.ValueText == metaDataListItemDto.ValueText))
                {
                    var newItem = new MetaDataListItemDto()
                    {
                        MetaDataListId = list.Id,
                        MetaDataList = list,
                        ValueText = metaDataListItemDto.ValueText,
                        ValueLong = metaDataListItemDto.ValueLong,
                        Id = metaDataListItemDto.Id,
                        DisplayOrder = metaDataListItemDto.DisplayOrder,
                        IsDeleted = metaDataListItemDto.IsDeleted
                    };
                    list.MetaDataListItems.Add(newItem);
                    //builder.DBContext.MetaDataListItems.Add(newItem);
                    builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
                }
            }
            
            return builder;
        }

        public static AomDataBuilder AddProductMetaDataItem(this AomDataBuilder builder, string productName, string displayName, int displayOrder, MetaDataTypes metaDataType, string listName = null, int valMin = 0, int valMax = 100, bool isDeleted = false)
        {
            var list = builder.DBContext.MetaDataLists.FirstOrDefault(i => i.Description == listName);
            var product = builder.DBContext.Products.First(x => x.Name == productName);
            var itm = new ProductMetaDataItemDto { Id = builder.NextId(), ProductId = product.Id, DisplayOrder = displayOrder };
            
            if (list != null)
            {
                itm.MetaDataListId = list.Id;
                itm.MetaDataList = list;
            }

            itm.DisplayName = displayName;
            itm.MetaDataTypeId = metaDataType;
            itm.ValueMinimum = valMin;
            itm.ValueMaximum = valMax;
            itm.IsDeleted = isDeleted;
            
            builder.DBContext.ProductMetaDataItems.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));

            return builder;
        }

        public static AomDataBuilder AddCommonQuantities(this AomDataBuilder builder, long productId, long commodityId, List<long> commonQuantities)
        {
            var product = builder.DBContext.Products.First(x => x.Id == productId);

            if (product == null) return builder;

            product.CommodityId = commodityId;
            product.CommonQuantityValues = new List<CommonQuantityValuesDto>();
            
            foreach (long quantity in commonQuantities)
            {
                product.CommonQuantityValues.Add(new CommonQuantityValuesDto()
                {
                    ProductId = productId,
                    ProductDto = product,
                    Quantity = quantity
                });
            }
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddProduct(this AomDataBuilder builder, string name, long productId = -1, MarketStatus marketStatus = MarketStatus.Open,
            decimal minPrice = 0, decimal maxPrice = 999, decimal minQty = 0, decimal maxQty = 32000, 
            decimal? priceIncrement = null, decimal? qtyIncrement = null, decimal defaultQty = 30000,  bool coBrokering  = false, long marketId = -1)
        {
            if (builder.DBContext.Products.Any(x => x.Name == name)) return builder;

            if (productId < 0)
            {
                productId = builder.NextId();
            }

            var itm = new ProductDto
            {
                Id = productId,
                Name = name,
                OrderPriceMinimum = minPrice,
                OrderPriceMaximum = maxPrice,
                OrderVolumeMinimum = minQty,
                OrderVolumeMaximum = maxQty,
                OrderVolumeDefault = defaultQty,
                //CurrencyDto = builder.DBContext.Currencies.FirstOrDefault(),
                DeliveryLocations = builder.DBContext.DeliveryLocations.Count() == 0 ? null : new List<DeliveryLocationDto>() { builder.DBContext.DeliveryLocations.OrderByDescending(o => o.Id).First()},
                CommodityDto = builder.DBContext.Commodities.FirstOrDefault(),
                MarketDto = builder.DBContext.Markets.FirstOrDefault(),
                ContractTypeDto = builder.DBContext.ContractTypes.FirstOrDefault(),
                //PriceUnitDto = builder.DBContext.Units.FirstOrDefault(),
                //PriceUnit = builder.DBContext.Units.FirstOrDefault() != null ?  builder.DBContext.Units.FirstOrDefault().Code : string.Empty,
                VolumeUnitDto = builder.DBContext.Units.FirstOrDefault(),
                Status = DtoMappingAttribute.GetValueFromEnum(marketStatus),
                LastCloseDate = DateTime.Today,
                LastOpenDate = DateTime.Today,
                LastUpdated = DateTime.Today,
                CoBrokering = coBrokering,
                MarketId = marketId,
                BidAskStackStyleCode = "N"
            };
            itm.LastUpdated = _dateTimeProvider.UtcNow;
            itm.LastUpdatedUserId = -1;

            if (priceIncrement.HasValue)
            {
                itm.OrderPriceIncrement = priceIncrement.Value;
            }
            if (qtyIncrement.HasValue)
            {
                itm.OrderVolumeIncrement = qtyIncrement.Value;
            }

            builder.DBContext.Products.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));

            return builder;
        }

        public static T ExecuteAndReportErrors<T>(this IAomModel context, Func<IAomModel, T> f)
        {
            try
            {
                return f.Invoke(context);
            }
            catch (DbEntityValidationException dbe)
            {
                foreach (var error in dbe.EntityValidationErrors.SelectMany(e => e.ValidationErrors))
                {
                    Debug.WriteLine("DEBUG::" + error.ErrorMessage);
                }
                // ReSharper disable once PossibleIntendedRethrow
                throw dbe;
            }
        }

        public static AomDataBuilder OpenAllMarkets(this AomDataBuilder builder)
        {
            (from p in builder.DBContext.Products select p).ToList().ForEach(p => OpenMarket(builder, p.Id));

            builder.DBContext.SaveChangesAndLog(-1);
            return builder;
        }

        public static AomDataBuilder OpenMarket(this AomDataBuilder builder, long productId)
        {
            var p = builder.DBContext.Products.First(prod => prod.Id == productId);
            p.Status = DtoMappingAttribute.GetValueFromEnum(MarketStatus.Open);
            //p.LastUpdated = _dateTimeProvider.UtcNow;
            return builder;
        }

        //public static AomDataBuilder AddTenorCode(this AomDataBuilder builder, string name, out TenorCodeDto tenor)
        //{
        //    if (builder.DBContext.TenorCodes.Any(x => x.Name == name))
        //    {
        //        tenor = builder.DBContext.TenorCodes.First(t => t.Name == name);
        //        return builder;
        //    }

        //    tenor = new TenorCodeDto { Name = name};
        //    tenor.Code = "P";
        //    tenor.DateCreated = _dateTimeProvider.UtcNow;
        //    tenor.LastUpdated = _dateTimeProvider.UtcNow;
        //    tenor.LastUpdatedUserId = -1;

        //    builder.DBContext.TenorCodes.Add(tenor);
        //    builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));

        //    return builder;
        //}

        public static AomDataBuilder AddTenorCode(this AomDataBuilder builder, string code)
        {
            TenorCodeDto tenor = null;
            if (builder.DBContext.TenorCodes.Any(x => x.Code == code))
            {
                tenor = builder.DBContext.TenorCodes.First(t => t.Code == code);
                return builder;
            }
            tenor = new TenorCodeDto { Code = code };
            tenor.DateCreated = _dateTimeProvider.UtcNow;
            tenor.LastUpdated = _dateTimeProvider.UtcNow;
            tenor.Name = "TenorName";
            tenor.LastUpdatedUserId = -1;

            builder.DBContext.TenorCodes.Add(tenor);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));

            return builder;
        }

        public static AomDataBuilder AddProductTenor(this AomDataBuilder builder, string name, long productId = -1, string tenorCode = "P")
        {
            if (builder.DBContext.ProductTenors.Any(x => x.Name == name)) return builder;

            ProductDto firstProduct = productId == -1 ? 
                builder.DBContext.Products.OrderByDescending(o => o.Id).First() : builder.DBContext.Products.OrderByDescending(o => o.Id == productId).First();

            if (builder.DBContext.DeliveryLocations.Count() > 0)
                firstProduct.DeliveryLocations = new List<DeliveryLocationDto> {builder.DBContext.DeliveryLocations.First()};

            //TenorCodeDto firstTenor = builder.DBContext.TenorCodes.OrderByDescending(o => o.Code == tenorCode).First();

            var productTenor = new ProductTenorDto { Id = builder.NextId(), Name = name};
            //productTenor.DeliveryDateStart = "+1d";
            //productTenor.DeliveryDateEnd = "+20d";
            //productTenor.DefaultStartDate = "+1d";
            //productTenor.DefaultEndDate = "+20d";
            //productTenor.MinimumDeliveryRange = 3;
            productTenor.LastUpdated = _dateTimeProvider.UtcNow;
            productTenor.LastUpdatedUserId = -1;
            productTenor.ProductId = firstProduct.Id;
            productTenor.ProductDto = firstProduct;
            //productTenor.TenorId = firstTenor.Id;
            //productTenor.TenorDto = firstTenor;

            builder.DBContext.ProductTenors.Add(productTenor);

            firstProduct.ProductTenors = new Collection<ProductTenorDto>() { productTenor };
            //firstTenor.ProductTenors = new Collection<ProductTenorDto>() { productTenor };
            
            //var matchedProduct = builder.DBContext.Products.Where(p => p.Id == firstProduct.Id).FirstOrDefault();
            //if (matchedProduct != null)
            //{
            //    if (matchedProduct.ProductTenors == null)
            //    {
            //        matchedProduct.ProductTenors = new Collection<ProductTenorDto>();
            //    }
            //    matchedProduct.ProductTenors.Add(productTenor);
            //}

            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));

            return builder;
        }

        //public static AomDataBuilder AddPriceLine(this AomDataBuilder builder, int seqNo)
        //{
        //    var itm = new PriceLineDto { Id = builder.NextId(), SequenceNo = seqNo};
        //    builder.DBContext.PriceLines.Add(itm);
        //    builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
        //    return builder;
        //}

        public static AomDataBuilder AddDeliveryLocation(this AomDataBuilder builder, string name)
        {
            if (builder.DBContext.DeliveryLocations.Any(x => x.Name == name)) return builder;

            var itm = new DeliveryLocationDto { Id = builder.NextId(), Name = name };
            builder.DBContext.DeliveryLocations.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddScheduledTask(this AomDataBuilder builder, string taskName, string taskType, string arguments)
        {
            var st = new ScheduledTaskDto {Id = builder.NextId(), Interval = "D", TaskName = taskName, LastUpdated = DateTime.Now, TaskType = taskType, Arguments = arguments};
            builder.DBContext.ScheduledTasks.Add(st);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddMarketInfoItem(this AomDataBuilder builder,  int userId, string info,  string marketInfoStatus,  DateTime lastUpdated, out MarketInfoDto marketInfo)
        {
            if (string.IsNullOrEmpty(marketInfoStatus))
            {
                marketInfoStatus = "A";
            }

            if (marketInfoStatus.Length > 1)
            {
                marketInfoStatus = "A";
            }

            marketInfo = new MarketInfoDto
            {
                Id = builder.NextId(),
                LastUpdated = lastUpdated, 
                LastUpdatedUserId = userId,
                Info = info, 
                MarketInfoStatus = marketInfoStatus
            };

            builder.DBContext.MarketInfoItems.Add(marketInfo);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddMarketTickerItem(this AomDataBuilder builder, int userId, long? orderId, DateTime dateCreated, DateTime lastUpdated, string productName,
            out MarketTickerItemDto item, string tickerItemStatus = "P", string tickerItemType = "I", 
            long? ownerOrganisation1 = null, long? ownerOrganisation2 = null,long? ownerOrganisation3 = null, long? ownerOrganisation4 = null)
        {
            item = new MarketTickerItemDto
            {
                Id = builder.NextId(),
                LastUpdated = lastUpdated,
                LastUpdatedUserId = userId,
                DateCreated = dateCreated,
                EnteredByUserId = userId,
                MarketTickerItemType = tickerItemType,
                MarketTickerItemStatus = tickerItemStatus,
                Message = "This is a market ticker item message!",
                Product = builder.DBContext.Products.OrderByDescending(o => o.Name == productName).First(),
                ProductId = builder.DBContext.Products.OrderByDescending(o => o.Name == productName).First().Id,
                BrokerRestriction = "A",
                OwnerOrganisationId1 = ownerOrganisation1,
                OwnerOrganisationId2 = ownerOrganisation2,
                OwnerOrganisationId3 = ownerOrganisation3,
                OwnerOrganisationId4 = ownerOrganisation4,
                OrderId = orderId
            };

            builder.DBContext.MarketTickerItems.Add(item);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddMarketInfoMarketTickerItem(this AomDataBuilder builder, long marketInfoId, out MarketTickerItemDto item, string tickerItemStatus = "A")
        {
            item = new MarketTickerItemDto
            {
                Id = builder.NextId(),
                MarketInfoId = marketInfoId,
                MarketTickerItemType = "I",
                MarketTickerItemStatus = tickerItemStatus
            };
            builder.DBContext.MarketTickerItems.Add(item);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddUnit(this AomDataBuilder builder, string code, string name)
        {
            var itm = new UnitDto { Code = code, Name = name, DateCreated = _dateTimeProvider.UtcNow };
            builder.DBContext.Units.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddCurrency(this AomDataBuilder builder, string code, string name)
        {
            var itm = new CurrencyDto
            {
                Code = code,
                Name = name,
                DisplayName = name,
                DateCreated = _dateTimeProvider.UtcNow
            };
            builder.DBContext.Currencies.Add(itm);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddPriceLine(this AomDataBuilder builder, out PriceLineDto item, long userId)
        {
            item = new PriceLineDto()
            {
                Id = builder.NextId(),
                SequenceNo = 1,
                LastUpdatedUserId = userId
            };
            builder.DBContext.PriceLines.Add(item);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddPriceType(this AomDataBuilder builder, string name)
        {
            PriceTypeDto priceTypeDto;
            if (builder.DBContext.PriceTypes.Any(x => x.Name == name))
            {
                priceTypeDto = builder.DBContext.PriceTypes.First(t => t.Name == name);
                return builder;
            }

            priceTypeDto = new PriceTypeDto()
            {
                Id = builder.NextId(),
                Name = name
            };

            builder.DBContext.PriceTypes.Add(priceTypeDto);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));

            return builder;
        }

        public static AomDataBuilder AddPriceBasis(this AomDataBuilder builder, string name, out PriceBasisDto priceBasisDto, string currency = "EUR", string unit = "BBL")
        {
            if (builder.DBContext.PriceBases.Any(x => x.Name == name))
            {
                priceBasisDto = builder.DBContext.PriceBases.First(t => t.Name == name);
                priceBasisDto.PriceTypeDto = builder.DBContext.PriceTypes.FirstOrDefault(pb => pb.Name == name);
                priceBasisDto.PriceUnitDto = builder.DBContext.Units.FirstOrDefault(pb => pb.Code == unit);
                priceBasisDto.CurrencyDto = builder.DBContext.Currencies.FirstOrDefault(pb => pb.Code == currency);
                return builder;
            }

            priceBasisDto = new PriceBasisDto()
            {
                PriceTypeId = builder.DBContext.PriceTypes.FirstOrDefault(pb => pb.Name == name).Id,
                PriceTypeDto = builder.DBContext.PriceTypes.FirstOrDefault(pb => pb.Name == name),
                PriceUnitCode = unit,
                PriceUnitDto = builder.DBContext.Units.FirstOrDefault(pb => pb.Code == unit),
                CurrencyCode = currency,
                CurrencyDto = builder.DBContext.Currencies.FirstOrDefault(pb => pb.Code == currency),
                ShortName = name,
                Name = name,
                PriceMinimum = 10,
                PriceMaximum = 10000,
                PriceIncrement = 1,
                PriceDecimalPlaces = 2,
                PriceSignificantFigures = 3,
                DateCreated = DateTime.Now,
                LastUpdatedUserId = 1,
                LastUpdated = DateTime.Now
            };

            builder.DBContext.PriceBases.Add(priceBasisDto);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddPriceLineBasis(this AomDataBuilder builder, decimal priceLineBasisValue, PriceLineDto priceLineDto, long productId, int sequenceNo, string tenorCode = "P", long priceBasisID = -1)
        {
            //if (builder.DBContext.PriceLineBases.Any(x => x.PriceLineDto.Id == priceLineDto.Id && x.ProductId == productId && x.TenorId == tenorId && x.SequenceNo == sequenceNo)) return builder;

            var item = new PriceLineBasisDto()
            {
                Id = builder.NextId(),
                SequenceNo = sequenceNo,
                PriceLineBasisValue = priceLineBasisValue,
                ProductId = productId,
                TenorCode = tenorCode,
                PriceBasisId = priceBasisID,
                PriceBasisDto = builder.DBContext.PriceBases.FirstOrDefault(pb => pb.Id == priceBasisID)
            };
            priceLineDto.PriceLinesBases = new List<PriceLineBasisDto>{item};

            builder.DBContext.PriceLineBases.Add(item);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        // Naughty: this doesn't fit the model used in the rest of the code.
        public static AomDataBuilder AddExternalDeal(this AomDataBuilder builder, ExternalDealDto externalDealDto, PriceLineDto priceLineDto = null)
        {
            if (priceLineDto != null)
                externalDealDto.PriceLineDto = priceLineDto;
            builder.DBContext.ExternalDeals.Add(externalDealDto);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddMarketInfoProductChannel(this AomDataBuilder builder, long productId, long marketInfoItemId, 
            out MarketInfoProductChannelDto productChannel, MarketInfoDto marketInfo = null)
        {
            productChannel = new MarketInfoProductChannelDto
            {
                Id = builder.NextId(),
                ProductId = productId,
                MarketInfoItemId = marketInfoItemId,
                MarketInfo = marketInfo
            };
            builder.DBContext.MarketInfoProductChannels.Add(productChannel);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }

        public static AomDataBuilder AddEmail(this AomDataBuilder builder, out EmailDto emailDto, 
            string body = "", DateTime? dateCreated = null, DateTime? dateSent = null, string recipient = "", string status = "P", string subject = "")
        {
            emailDto = new EmailDto
            {
                Body = body,
                DateCreated = dateCreated ?? DateTime.UtcNow,
                DateSent = dateSent ?? DateTime.UtcNow,
                Id = builder.NextId(),
                Recipient = recipient,
                Status = status,
                Subject = subject
            };
            builder.DBContext.Emails.Add(emailDto);
            builder.DBContext.ExecuteAndReportErrors(d => d.SaveChangesAndLog(-1));
            return builder;
        }
    }
}