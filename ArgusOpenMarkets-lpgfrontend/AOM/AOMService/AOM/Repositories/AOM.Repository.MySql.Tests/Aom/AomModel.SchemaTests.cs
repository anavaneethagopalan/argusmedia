﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using NUnit.Framework;

namespace AOM.Repository.MySql.Tests.Aom
{
    using System.Configuration;
    using System.IO;

    [TestFixture]
    public class AomModelSchemaTests
    {
        private static string _connString;
        private MySqlConnection _dbConnector;

        public static string AomConnectionString
        {
            get
            {
                var aomConn = "";

                if (string.IsNullOrEmpty(_connString))
                {
                    string filePath = Directory.GetParent(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath).Parent.Parent.Parent.Parent.FullName;
                    string[] configFiles = Directory.GetFiles(filePath, "app.config", SearchOption.AllDirectories);
                    foreach (var configFile in configFiles)
                    {
                        ConfigurationManager.OpenExeConfiguration(configFile);
                        try
                        {
                            aomConn = ConfigurationManager.ConnectionStrings["AomModel"].ConnectionString;
                        }
                        catch (Exception)
                        {
                            aomConn = "";
                        }

                        if (aomConn != null)
                        {
                            _connString = aomConn;
                            break;
                        }
                    }
                }

                if (string.IsNullOrEmpty(_connString))
                {
                    _connString = "server=aominstancedevpri.co46fwzab7bh.eu-west-1.rds.amazonaws.com;user id=aom_user;password=Argu5Med1a;persistsecurityinfo=True;database=aom.temp";
                    // Just take the connection string from check-in.
                    // _connString = ConfigurationManager.ConnectionStrings["AomModel"].ConnectionString;
                }

                return _connString;
            }
        }

        [SetUp]
        public void Setup()
        {
            try
            {
                _dbConnector = new MySqlConnection(AomConnectionString);
                _dbConnector.Open();
            }
            catch(MySqlException msqlEx)
            {
                Assert.Fail(msqlEx.Message);
            }
        }

        [TearDown]
        public void TearDown()
        {
            if(_dbConnector != null)
            {
                _dbConnector.Close();
            }
        }

        [Test]
        public void CheckAllBigIntsValid()
        {
            var selectAomMetaData = SqlTestStatements.AomBigIntCheck;
            var mysqlCmd = new MySqlCommand(selectAomMetaData, _dbConnector);

            var reader = mysqlCmd.ExecuteReader();

            //no results should be returned
            //if results are returned capture the offending row
            var resultCount = 0;
            var tableNames = new List<string>();
            var columnNames = new List<string>();
            
            while(reader.Read())
            {
                tableNames.Add(reader.GetString("table_name"));
                columnNames.Add(reader.GetString("column_name"));
                resultCount++;
            }

            Assert.IsTrue(tableNames.Capacity == 0, "Tables: " + String.Join(",", tableNames));
            Assert.IsTrue(columnNames.Capacity == 0, "Column Name: " + String.Join(",", columnNames));
            Assert.IsTrue(resultCount == 0);
        }

        [Test]
        public void CheckAllTinyIntsValid()
        {
            var selectAomMetaData = SqlTestStatements.AomTinyIntCheck;
            var mysqlCmd = new MySqlCommand(selectAomMetaData, _dbConnector);

            var reader = mysqlCmd.ExecuteReader();

            //no results should be returned
            //if results are returned capture the offending row
            var resultCount = 0;
            var tableNames = new List<string>();
            var columnNames = new List<string>();

            while (reader.Read())
            {
                tableNames.Add(reader.GetString("table_name"));
                columnNames.Add(reader.GetString("column_name"));
                resultCount++;
            }

            Assert.IsTrue(tableNames.Capacity == 0, "Tables: " + String.Join(",", tableNames));
            Assert.IsTrue(columnNames.Capacity == 0, "Column Name: " + String.Join(",", columnNames));
            Assert.IsTrue(resultCount == 0);
        }

        [Test]
        public void CheckAllDateTimesValid()
        {
            var selectAomMetaData = SqlTestStatements.AomDateTimeCheck;
            var mysqlCmd = new MySqlCommand(selectAomMetaData, _dbConnector);

            var reader = mysqlCmd.ExecuteReader();

            //no results should be returned
            //if results are returned capture the offending row
            var resultCount = 0;
            var tableNames = new List<string>();
            var columnNames = new List<string>();

            while (reader.Read())
            {
                tableNames.Add(reader.GetString("table_name"));
                columnNames.Add(reader.GetString("column_name"));
                resultCount++;
            }

            Assert.IsTrue(tableNames.Capacity == 0, "Tables: " + String.Join(",", tableNames));
            Assert.IsTrue(columnNames.Capacity == 0, "Column Name: " + String.Join(",", columnNames));
            Assert.IsTrue(resultCount == 0);
        }

        [Test]
        public void CheckAllPresenceOfPk()
        {
            var selectAomMetaData = SqlTestStatements.AomPrimaryKeyCheck;
            var mysqlCmd = new MySqlCommand(selectAomMetaData, _dbConnector);

            var reader = mysqlCmd.ExecuteReader();

            //no results should be returned
            //if results are returned capture the offending row
            var resultCount = 0;
            var tableNames = new List<string>();

            while (reader.Read())
            {
                tableNames.Add(reader.GetString("table_name"));
                resultCount++;
            }

            Assert.IsTrue(tableNames.Capacity == 0, "Tables: " + String.Join(",", tableNames));
            Assert.IsTrue(resultCount == 0);
        }
    }
}

