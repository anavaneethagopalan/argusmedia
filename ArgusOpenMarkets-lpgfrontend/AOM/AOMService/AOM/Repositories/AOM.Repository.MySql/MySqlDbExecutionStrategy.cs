﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;

using MySql.Data.MySqlClient;
using Utils.Logging.Utils;

namespace AOM.Repository.MySql
{
    public class MySqlDbExecutionStrategy : DbExecutionStrategy
    {
        public MySqlDbExecutionStrategy() { }

        //Can add catch statements when calling save to capture if retry limit exceeded
        //
        //   catch (RetryLimitExceededException /* ex */)
        //   {
        //       //Log the error
        //   }
        //
        public MySqlDbExecutionStrategy(int maxRetryCount, TimeSpan maxDelay) 
            : base(maxRetryCount, maxDelay)
        { }

        protected override bool ShouldRetryOn(Exception ex)
        {
            bool retry = false;
            Log.Error(string.Format("ERROR DB exception caught by DBExecutionStrategy: {0}", ex.StackTrace));

            MySqlException sqlException = ex as MySqlException;
            if (sqlException != null)
            {
                int[] errorsToRetry =
                {
                    1205, //Timeout - https://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html#error_er_lock_wait_timeout
                    1213  //Deadlock - https://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html#error_er_lock_deadlock
                };

                if (errorsToRetry.Contains(sqlException.Number))
                {
                    Log.Error(string.Format("ERROR saving record to DB - will retry save : {0} : {1} : {2}", sqlException.Number, sqlException.Message, sqlException.StackTrace));
                    retry = true;
                }
                else
                {
                    Log.Error(string.Format("ERROR saving record to DB - unknown SqlException code: {0} : {1} : {2}", sqlException.Number, sqlException.Message, sqlException.StackTrace ));
                }
            }

            if (ex is TimeoutException)
            {
                retry = true;
            }
            return retry;
        }
    }
}
