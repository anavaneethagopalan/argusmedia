using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.CommodityType")]
    public class CommodityTypeDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column]
        [StringLength(50)]
        public string Name { get; set; }

        [Column("CommodityGroup_Id_fk")]
        public long CommodityGroupId { get; set; }
        
        [ForeignKey("CommodityGroupId")]
        public virtual CommodityGroupDto CommodityGroupDto { get; set; }        
    }
}
