﻿using ServiceStack.Text;

namespace AOM.Repository.MySql.Aom
{
    public class MetaDataIntegerEnumDto : MetaDataDto
    {
        static MetaDataIntegerEnumDto()
        {
            // ReSharper disable once UnusedMember.Local
            JsConfig<MetaDataIntegerEnumDto>.IncludeTypeInfo = true;
        }
        public MetaDataIntegerEnumDto() : base(MetaDataTypes.IntegerEnum) { }

        public long Id { get; set; }
        public int ItemValue { get; set; }
    }
}