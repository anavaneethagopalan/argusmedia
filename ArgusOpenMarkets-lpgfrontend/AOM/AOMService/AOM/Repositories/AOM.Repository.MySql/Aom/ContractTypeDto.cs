using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ContractType")]
    public class ContractTypeDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column]
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        private DateTime? _dateCreated;

        [Column]
        public DateTime? DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;

        [ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdatedUser_Id")]
        public long LastUpdatedUserId { get; set; }


        [Column("EnteredByUser_Id")]
        public long? EnteredByUserId { get; set; }
    }
}
