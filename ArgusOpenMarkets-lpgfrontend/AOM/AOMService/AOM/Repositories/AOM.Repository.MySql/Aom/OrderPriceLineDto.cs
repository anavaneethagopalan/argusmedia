﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.OrderPriceLine")]
    public class OrderPriceLineDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column("Order_Id_fk")]
        public long OrderId { get; set; }

        [Column("PriceLine_Id_fk")]
        public long PriceLineId { get; set; }

        [Column("OrderStatus_Code_fk")]
        [StringLength(1)]
        public string OrderStatusCode { get; set; }

        [ForeignKey("OrderId")]
        public virtual OrderDto OrderDto { get; set; }
        
        [ForeignKey("OrderStatusCode")]
        public virtual OrderStatusDto OrderStatusDto { get; set; }

        [ForeignKey("PriceLineId")]
        public virtual PriceLineDto PriceLineDto { get; set; }
        
        private DateTime _dateCreated;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateCreated
        {
            get { return DateTime.SpecifyKind(_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        private DateTime _lastUpdated;
        [DataType(DataType.DateTime), ConcurrencyCheck]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        //public virtual ICollection<PriceLineDto> PriceLines { get; set; }
    }
}
