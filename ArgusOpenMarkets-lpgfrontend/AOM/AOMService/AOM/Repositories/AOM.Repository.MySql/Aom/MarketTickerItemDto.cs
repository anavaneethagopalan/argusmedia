using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.MarketTickerItem")]
    public class MarketTickerItemDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        
        [ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(2000)]
        public string Message { get; set; }

        [StringLength(500)]
        public string Notes { get; set; }

        [Column("CoBrokering")]
        public bool? CoBrokering { get; set; }

        [Column("EnteredByUser_fk")]
        public long EnteredByUserId { get; set; }

        public DateTime DateCreated { get; set; }

        [Column("DealId_fk")]
        public long? DealId { get; set; }

        [ForeignKey("DealId")]
        public virtual DealDto Deal { get; set; }

        [Column("MarketItemStatus_Code_fk")]
        [StringLength(1)]
        public string MarketTickerItemStatus { get; set; }

        [ForeignKey("MarketTickerItemStatus")]
        public virtual MarketItemStatusDto MarketItemStatus { get; set; }

        [Column("MarketItemType_Code_fk")]
        [StringLength(1)]
        public string MarketTickerItemType { get; set; }

        [ForeignKey("MarketTickerItemType")]
        public virtual MarketItemTypeDto MarketItemType { get; set; }
        
        [Column("OrderId_fk")]
        public long? OrderId { get; set; }

        [ForeignKey("OrderId")]
        public virtual OrderDto Order { get; set; }

        [Column("Product_Id_fk")]
        public long? ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual ProductDto Product { get; set; }

        public long? OwnerOrganisationId1 { get; set;}
        public long? OwnerOrganisationId2 { get; set;}
        public long? OwnerOrganisationId3 { get; set;}
        public long? OwnerOrganisationId4 { get; set; }

        [Column("ExternalDeal_Id_fk")]
        public long? ExternalDealId { get; set; }

        [Column("MarketInfo_Id")]
        public long? MarketInfoId { get; set; }

        [Column("BrokerRestriction", TypeName = "char")]
        [StringLength(1)]
        public string BrokerRestriction { get; set; }

    }
}