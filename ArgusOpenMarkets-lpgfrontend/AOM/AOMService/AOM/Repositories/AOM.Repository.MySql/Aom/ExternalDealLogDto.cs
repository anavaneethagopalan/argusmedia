﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ExternalDeal_Log")]
    public class ExternalDealLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [Key]
        [Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Required]
        public long LastUpdatedUserId { get; set; }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get { return DateTime.SpecifyKind(_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [StringLength(100)]
        public string BuyerName { get; set; }
        public long? BuyerId { get; set; }
        [StringLength(100)]
        public string SellerName { get; set; }
        public long? SellerId { get; set; }
        [StringLength(100)]
        public string BrokerName { get; set; }
        public long? BrokerId { get; set; }
        [StringLength(100)]
        public string DeliveryLocation { get; set; }
        public decimal? Price { get; set; }
        public decimal? Quantity { get; set; }
        [StringLength(50)]
        public string QuantityText { get; set; }
        public long ProductId { get; set; }
        public long ReporterId { get; set; }

        private DateTime _deliveryStartDate;
        public DateTime DeliveryStartDate
        {
            get { return DateTime.SpecifyKind(_deliveryStartDate, DateTimeKind.Utc); }
            set { _deliveryStartDate = value; }
        }

        private DateTime _deliveryEndDate;
        public DateTime DeliveryEndDate
        {
            get { return DateTime.SpecifyKind(_deliveryEndDate, DateTimeKind.Utc); }
            set { _deliveryEndDate = value; }
        }

        [Column("DealMessage")]
        [StringLength(400)]
        public string FreeFormDealMessage { get; set; }
        [StringLength(1)]
        public string DealStatus { get; set; }

        public char VoidReason { get; set; }

        [StringLength(400)]
        public string Notes { get; set; }

        public bool UseCustomFreeFormNotes { get; set; }

        [StringLength(100)]
        public string ContractInput { get; set; }

        [StringLength(100)]
        public string OptionalPriceDetail { get; set; }

        [StringLength(1)]
        public string PreviousDealStatus { get; set; }

        [Column("MetaData")]
        [StringLength(5000)]
        public string MetaDataJson
        {
            get { return JsonConvert.SerializeObject(MetaData, Formatting.None, new JsonConverter[] { new MetaDataDtoJsonConverter() }); }
            set { MetaData = JsonConvert.DeserializeObject<MetaDataDto[]>(value, new JsonConverter[] { new MetaDataDtoJsonConverter() }); }
        }

        [NotMapped]
        public MetaDataDto[] MetaData { get; set; }

        [Column("PriceLine_Id_fk")]
        public long? PriceLineId { get; set; }
    }
}