using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace AOM.Repository.MySql.Aom
{
    public class MetaDataDtoJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {            
            serializer.Serialize(writer, value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);
            var metaDataType = (MetaDataTypes) Enum.Parse(typeof(MetaDataTypes), jObject.GetValue("MetaDataType").ToString());

            switch (metaDataType)
            {
                case MetaDataTypes.IntegerEnum:
                {                
                    var target = new MetaDataIntegerEnumDto();
                    serializer.Populate(jObject.CreateReader(), target);
                    return target;
                }
                case MetaDataTypes.String:
                {
                    var target = new MetaDataStringDto();
                    serializer.Populate(jObject.CreateReader(), target);
                    return target;
                }

                default:
                    throw new NotImplementedException(string.Format("No support for metadata DeSerialisation of: {0} ", jObject));
            }            
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(MetaDataDto);
        }
    }
}