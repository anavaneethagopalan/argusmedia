﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ProductPriceLineBasisPeriod")]
    public class ProductPriceLineBasisPeriodDto
    {
        [Key, Column("ProductPriceLine_Id_fk", Order = 0)]
        public long ProductPriceLineId { get; set; }

        [Key, Column("PriceBasis_Id_fk", Order = 1)]
        public long PriceBasisId { get; set; }

        [Key, Column("Period_Id_fk", Order = 2)]
        public long PeriodId { get; set; }

        public int DisplayOrder { get; set; }

        private DateTime _dateCreated;
        [DataType(DataType.DateTime), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [ForeignKey("ProductPriceLineId")]
        public ProductPriceLineDto ProductPriceLineDto { get; set; }

        [ForeignKey("PriceBasisId")]
        public PriceBasisDto PriceBasisDto { get; set; }

        [ForeignKey("PeriodId")]
        public PeriodDto PeriodDto { get; set; }
    }
    
}
