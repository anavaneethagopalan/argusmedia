using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.MarketTickerItem_Log")]
    public class MarketTickerItemLogDto : ILogDto
    {
        [Key][Column(Order = 0)][DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [Key]
        [Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(2000)]
        public string Message { get; set; }

        [StringLength(500)]
        public string Notes { get; set; }

        [Column("CoBrokering")]
        public bool? CoBrokering { get; set; }

        [Column("EnteredByUser_fk")]
        public long EnteredByUserId { get; set; }

        [Column]
        public DateTime DateCreated { get; set; }

        [Column("Deal_Id")]
        public long? DealId { get; set; }

        [Required]
        [StringLength(1)]
        [Column("MarketItemStatus_Code_fk", TypeName = "char")]
        public string MarketTickerItemStatus { get; set; }

        [Required]
        [StringLength(1)]
        [Column("MarketItemType_Code_fk", TypeName = "char")]
        public string MarketTickerItemType { get; set; }

        [Column("ExternalDeal_Id")]
        public long? ExternalDealId { get; set; }

        [Column("MarketInfo_Id")]
        public long? MarketInfoId { get; set; }

        [Column("Order_Id")]
        public long? OrderId { get; set; }

        [Column("Product_Id_fk")]
        public long? ProductId { get; set; }

        public long? OwnerOrganisationId1 { get; set; }
        public long? OwnerOrganisationId2 { get; set; }
        public long? OwnerOrganisationId3 { get; set; }
        public long? OwnerOrganisationId4 { get; set; }

        [Column("BrokerRestriction", TypeName = "char")]
        [StringLength(1)]
        public string BrokerRestriction { get; set; }

    }
}
