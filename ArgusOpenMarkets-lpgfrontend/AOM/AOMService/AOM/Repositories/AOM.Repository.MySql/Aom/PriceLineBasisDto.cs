﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.PriceLineBasis")]
    public class PriceLineBasisDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime _dateCreated;
        [Column(TypeName = "date")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateCreated
        {
            get { return DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [Column("PriceLine_Id_fk")]
        public long PriceLineId { get; set; }

        [Column("PriceBasis_Id_fk")]
        public long PriceBasisId  { get; set; }

        [Column("Product_Id_fk")]
        public long ProductId { get; set; }

        [Column("TenorCode_fk"), StringLength(10)]
        public string TenorCode { get; set; }

        [StringLength(400)]
        public string Description { get; set; }

        public int SequenceNo { get; set; }
        public int PercentageSplit { get; set; }
        public decimal PriceLineBasisValue { get; set; }

        [StringLength(50)]
        public string PricingPeriod { get; set; }
        public DateTime PricingPeriodFrom { get; set; }
        public DateTime PricingPeriodTo { get; set; }

        [ForeignKey("PriceLineId")]
        public virtual PriceLineDto PriceLineDto { get; set; }

        [ForeignKey("PriceBasisId")]
        public virtual PriceBasisDto PriceBasisDto { get; set; }

        [ForeignKey("ProductId")]
        public virtual ProductDto ProductDto { get; set; }

        [ForeignKey("TenorCode")]
        public virtual TenorCodeDto TenorDto { get; set; }
    }
}
