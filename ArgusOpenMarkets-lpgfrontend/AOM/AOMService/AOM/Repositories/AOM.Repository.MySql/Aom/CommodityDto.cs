using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.Commodity")]
    public class CommodityDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column]
        [StringLength(50)]
        public string Name { get; set; }

        private DateTime _dateCreated;

        [Column]
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("CommodityType_Id_fk")]
        public long CommodityTypeId { get; set; }

        [ForeignKey("CommodityTypeId")]
        public virtual CommodityTypeDto CommodityTypeDto { get; set; }

        [Column("EnteredByUser_Id")]
        public long? EnteredByUserId { get; set; }

        [Column("LastUpdatedUser_Id")]
        public long LastUpdatedUserId { get; set; }

        private DateTime _lastUpdated;

        [ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }
    }
}