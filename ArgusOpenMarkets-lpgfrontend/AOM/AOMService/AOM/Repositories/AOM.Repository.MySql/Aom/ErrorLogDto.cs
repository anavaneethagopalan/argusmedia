using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ErrorLog")]
    public class ErrorLogDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [StringLength(100)]
        public string Type { get; set; }

        [StringLength(200)]
        public string Source { get; set; }

        [StringLength(500)]
        public string Message { get; set; }

        [StringLength(4000)]
        public string StackTrace { get; set; }

        [StringLength(200)]
        public string AdditionalInfo { get; set; }
    }
}
