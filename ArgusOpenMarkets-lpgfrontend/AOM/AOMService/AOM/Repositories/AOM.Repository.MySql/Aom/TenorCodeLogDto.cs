﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.TenorCode_Log")]
    public class TenorCodeLogDto : ILogDto
    {
        [Key, Column(Order = 0), StringLength(10)]
        public string Code { get; set; }
 
        private DateTime _lastUpdated;
        [Key, Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [StringLength(50)]
        public string Name { get; set; }

        private DateTime _dateCreated;
        [DataType(DataType.DateTime)]
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }
    }
}
