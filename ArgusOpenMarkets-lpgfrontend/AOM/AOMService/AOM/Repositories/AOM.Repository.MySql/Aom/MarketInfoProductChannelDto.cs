﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.MarketInfoProductChannel")]
    public class MarketInfoProductChannelDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, Column("Product_Id_fk")]
        public long ProductId { get; set; }

        [Required, Column("MarketInfoItem_Id_fk")]
        public long MarketInfoItemId { get; set; }

        [ForeignKey("ProductId")]
        public ProductDto Product { get; set; }

        [ForeignKey("MarketInfoItemId")]
        public MarketInfoDto MarketInfo { get; set; }
    }
}