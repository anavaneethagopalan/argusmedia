﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.MarketInfoItem_Log")]
    public class MarketInfoLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }
        [Required]
        public long LastUpdatedUserId { get; set; }


        [Required]
        [StringLength(500)]
        public string Info { get; set; }

        [Required]
        [StringLength(1)]
        public string MarketInfoStatus { get; set; }

        [Required]
        public long OwnerOrganisationId { get; set; }

        [StringLength(1)]
        public string MarketInfoType { get; set; }
    }
}