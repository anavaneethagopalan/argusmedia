using System.Data.Entity;

namespace AOM.Repository.MySql.Aom
{
    public interface IAomModelWithLogs : IAomModel
    {
        IDbSet<AssessmentLogDto> AssessmentsLogs { get; set; }
        IDbSet<CommodityLogDto> CommodityLogs { get; set; }
        IDbSet<ContractTypeLogDto> ContractTypeLogs { get; set; }
        IDbSet<CommonQuantityValuesLogDto> CommonQuantityValueLogs { get; set; }
        IDbSet<CommodityClassLogDto> CommodityClassLogs { get; set; }
        IDbSet<ExternalDealLogDto> ExternalDealLogs { get; set; }
        IDbSet<MarketInfoLogDto> MarketInfoItemLogs { get; set; }
        IDbSet<MarketTickerItemLogDto> MarketTickerItemLogs { get; set; }
        IDbSet<OrderLogDto> OrderLogs { get; set; }
        IDbSet<ProductLogDto> ProductLogs { get; set; }
        IDbSet<TenorCodeLogDto> TenorLogs { get; set; }
        IDbSet<ProductTenorLogDto> ProductTenorLogs { get; set; }
        IDbSet<OrderPriceLineLogDto> OrderPriceLineLogs { get; set; }
        IDbSet<PriceLineLogDto> PriceLineLogs { get; set; }
        IDbSet<PriceLineBasisLogDto> PriceLineBasisLogs { get; set; }
        IDbSet<ProductPriceLineLogDto> ProductPriceLineLogs { get; set; }
        IDbSet<ProductPriceLineBasisPeriodLogDto> ProductPriceLineBasesPeriodLogs { get; set; }
        IDbSet<PeriodLogDto> PricePeriodLogs { get; set; }
        IDbSet<ProductTenorPeriodLogDto> ProductTenorPeriodLogs { get; set; }
    }
}