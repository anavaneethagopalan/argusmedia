﻿namespace AOM.Repository.MySql.Aom
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("aom.CommodityContentStreams")]
    public class CommodityContentStreamDto
    {
        [Key]
        [Column(Order = 0)]
        public long CommodityId { get; set; }

        [Key]
        [Column(Order = 1)]
        public long ContentStreamId { get; set; }
    }
}