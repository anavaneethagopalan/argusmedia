using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.Deal")]
    public class DealDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [DataType(DataType.DateTime), ConcurrencyCheck]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [Column("EnteredByUser_fk")]
        public long EnteredByUserId { get; set; }

        [StringLength(400)]
        public string Message { get; set; }

        [Column("DealStatus_Code_fk", TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string DealStatus { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateCreated { get; set; }

        [Column("Initial_Order_Id_fk")]
        public long InitialOrderId { get; set; }

        [Column("Matching_Order_Id_fk")]
        public long MatchingOrderId { get; set; }

        [ForeignKey("InitialOrderId")]
        public virtual OrderDto InitialOrderDto { get; set; }

        [ForeignKey("MatchingOrderId")]
        public virtual OrderDto MatchingOrderDto { get; set; }

    }
}