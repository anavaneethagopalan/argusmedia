﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.OrderPriceLine_Log")]
    public class OrderPriceLineLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [Key]
        [Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("Order_Id_fk")]
        public long OrderId { get; set; }

        [Column("PriceLine_Id_fk")]
        public long PriceLineId { get; set; }

        [Column("OrderStatus_Code_fk")]
        [StringLength(1)]
        public string OrderStatusCode { get; set; }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        //public virtual ICollection<PriceLineDto> PriceLines { get; set; }
    }
}
