﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.MarketTickerItemProductChannel")]
    public class MarketTickerItemProductChannelDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, Column("Product_Id_fk")]
        public long ProductId { get; set; }

        [Required, Column("MarketTickerItem_Id_fk")]
        public long MarketTickerItemItemId { get; set; }

        [ForeignKey("ProductId")]
        public ProductDto Product { get; set; }

        [ForeignKey("MarketTickerItemItemId")]
        public MarketTickerItemDto MarketTickerItem { get; set; }
    }
}