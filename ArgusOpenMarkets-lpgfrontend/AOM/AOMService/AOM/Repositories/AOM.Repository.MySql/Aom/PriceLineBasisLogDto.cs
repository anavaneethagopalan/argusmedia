﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.PriceLineBasis_Log")]
    public class PriceLineBasisLogDto : ILogDto
    {
        [Key, Column(Order = 0)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [Key, Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [Column("PriceLine_Id_fk")]
        public long PriceLineId { get; set; }

        [Column("PriceBasis_Id_fk")]
        public long PriceBasisId { get; set; }

        [Column("Product_Id_fk")]
        public long ProductId { get; set; }

        [Column("TenorCode_fk"), StringLength(10)]
        public string TenorCode { get; set; }

        [StringLength(400)]
        public string Description { get; set; }

        public int SequenceNo { get; set; }
        public int PercentageSplit { get; set; }
        public decimal PriceLineBasisValue { get; set; }

        [StringLength(50)]
        public string PricingPeriod { get; set; }
        public DateTime PricingPeriodFrom { get; set; }
        public DateTime PricingPeriodTo { get; set; }
    }
}