﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ProductPriceLineBasisPeriod_Log")]
    public class ProductPriceLineBasisPeriodLogDto
    {
        [Key, Column("ProductPriceLine_Id_fk", Order = 0)]
        public long ProductPriceLineId { get; set; }

        [Key, Column("PriceBasis_Id_fk", Order = 1)]
        public long PriceBasisId { get; set; }

        [Key, Column("Period_Id_fk", Order = 2)]
        public long PeriodId { get; set; }

        public int DisplayOrder { get; set; }

        private DateTime _lastUpdated;
        [Key, Column(Order = 3)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        private DateTime _dateCreated;
        [DataType(DataType.DateTime)]
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }
    }

}
