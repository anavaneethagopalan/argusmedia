using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Aom
{
    public class AomModel : LoggingDbContext, IAomModel
    {
        public AomModel() : base("AomModel")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DoNothingInitialiser<AomModel>());
        }

        public override int SaveChanges()
        {
            throw new NotImplementedException("Please use the SaveChangesAndLog Method");
        }
        
        // Primary Tables - in IAomModel
        public virtual IDbSet<NewsDto> News { get; set; }
        public virtual IDbSet<NewsContentStreamDto> NewsContentStream { get; set; } 
        public virtual IDbSet<AssessmentDto> Assessments { get; set; }
        public virtual IDbSet<CommodityDto> Commodities { get; set; }
        public virtual IDbSet<CommodityClassDto> CommodityClasses { get; set; }
        public virtual IDbSet<CommodityGroupDto> CommodityGroups { get; set; }
        public virtual IDbSet<CommodityTypeDto> CommodityTypes { get; set; }
        public virtual IDbSet<ContractTypeDto> ContractTypes { get; set; }
        public virtual IDbSet<UnitLogDto> UnitLogs { get; set; } 
        public virtual IDbSet<CommodityClassLogDto> CommodityClassLogs { get; set; }
        public virtual IDbSet<CommodityContentStreamDto> CommodityContentStreams { get; set; }
        public virtual IDbSet<CurrencyDto> Currencies { get; set; }
        public virtual IDbSet<DealDto> Deals { get; set; }
        public virtual IDbSet<DealStatusDto> DealStatus { get; set; }
        public virtual IDbSet<DeliveryLocationDto> DeliveryLocations { get; set; }
        public virtual IDbSet<EmailDto> Emails { get; set; }
        public virtual IDbSet<ErrorLogDto> Errors { get; set; } 
        public virtual IDbSet<ExternalDealDto> ExternalDeals { get; set; }
        public virtual IDbSet<MarketDto> Markets { get; set; }
        public virtual IDbSet<MarketInfoDto> MarketInfoItems { get; set; }
        public virtual IDbSet<MarketInfoProductChannelDto> MarketInfoProductChannels { get; set; }
        public virtual IDbSet<MarketItemStatusDto> MarketItemStatus { get; set; }
        public virtual IDbSet<MarketItemTypeDto> MarketItemTypes { get; set; }
        public virtual IDbSet<MarketTickerItemDto> MarketTickerItems { get; set; }
        public virtual IDbSet<MarketTickerItemProductChannelDto> MarketTickerItemProductChannels { get; set; }
        public virtual IDbSet<CommonQuantityValuesDto> CommonQuantityValues { get; set; }
        public virtual IDbSet<OrderDto> Orders { get; set; }
        public virtual IDbSet<OrderStatusDto> OrderStatus { get; set; }
        public virtual IDbSet<ProductDto> Products { get; set; }
        public virtual IDbSet<TenorCodeDto> TenorCodes { get; set; }
        public virtual IDbSet<ProductTenorDto> ProductTenors { get; set; }
        public virtual IDbSet<ScheduledTaskDto> ScheduledTasks { get; set; } 
        public virtual IDbSet<UnitDto> Units { get; set; }
        public virtual IDbSet<PriceTypeDto> PriceTypes { get; set; }
        public virtual IDbSet<PriceBasisDto> PriceBases { get; set; }
        public virtual IDbSet<ProductPriceLineDto> ProductPriceLines { get; set; }
        public virtual IDbSet<ProductPriceLineBasisPeriodDto> ProductPriceLineBasesPeriods { get; set; }
        //public virtual IDbSet<ProductPriceLinePeriodDto> ProductPriceLinePeriods { get; set; }
        public virtual IDbSet<PriceLineDto> PriceLines { get; set; }
        public virtual IDbSet<OrderPriceLineDto> OrderPriceLines { get; set; }
        public virtual IDbSet<PriceLineBasisDto> PriceLineBases { get; set; }
        public virtual IDbSet<PeriodDto> Periods { get; set; }
        public virtual IDbSet<ProductTenorPeriodDto> ProductTenorPeriods { get; set; }


        // Log Tables - in IAomModelWithLogs
        public virtual IDbSet<AssessmentLogDto> AssessmentsLogs { get; set; }
        public virtual IDbSet<CommodityLogDto> CommodityLogs { get; set; }
        public virtual IDbSet<ContractTypeLogDto> ContractTypeLogs { get; set; }
        public virtual IDbSet<CommonQuantityValuesLogDto> CommonQuantityValueLogs { get; set; }
        public virtual IDbSet<DealLogDto> DealLogs { get; set; }
        public virtual IDbSet<ExternalDealLogDto> ExternalDealLogs { get; set; }
        public virtual IDbSet<MarketInfoLogDto> MarketInfoItemLogs { get; set; }
        public virtual IDbSet<MarketTickerItemLogDto> MarketTickerItemLogs { get; set; }
        public virtual IDbSet<OrderLogDto> OrderLogs { get; set; }
        public virtual IDbSet<ProductLogDto> ProductLogs { get; set; }
        public virtual IDbSet<TenorCodeLogDto> TenorCodeLogs { get; set; }
        public virtual IDbSet<ProductTenorLogDto> ProductTenorLogs { get; set; }
        public virtual IDbSet<OrderPriceLineLogDto> OrderPriceLineLogs { get; set; }
        public virtual IDbSet<PriceLineLogDto> PriceLineLogs { get; set; }
        public virtual IDbSet<PriceLineBasisLogDto> PriceLineBasisLogs { get; set; }
        public virtual IDbSet<PriceBasisLogDto> PriceBasisLogs { get; set; }
        public virtual IDbSet<ProductPriceLineLogDto> ProductPriceLineLogs { get; set; }
        public virtual IDbSet<ProductPriceLineBasisPeriodLogDto> ProductPriceLineBasesPeriodLogs { get; set; }
        public virtual IDbSet<PeriodLogDto> PricePeriodLogs { get; set; }
        public virtual IDbSet<ProductTenorPeriodLogDto> ProductTenorPeriodLogs { get; set; }

        //IProductMetaDataModel
        public virtual IDbSet<ProductMetaDataItemDto> ProductMetaDataItems { get; set; }
        public virtual IDbSet<MetaDataListItemDto> MetaDataListItems { get; set; }
        public virtual IDbSet<MetaDataListDto> MetaDataLists { get; set; }

        public virtual IDbSet<ProductMetaDataItemLogDto> ProductMetaDataItemsLogs { get; set; }
        public virtual IDbSet<MetaDataListItemLogDto> MetaDataListItemsLogs { get; set; }
        public virtual IDbSet<MetaDataListLogDto> MetaDataListsLogs { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductDto>()
                .HasMany(e => e.ProductTenors)
                .WithRequired(e => e.ProductDto)
                .HasForeignKey(e => e.ProductId)
                .WillCascadeOnDelete(false);
                
            modelBuilder.Entity<ProductDto>()
                .HasMany(e => e.CommonQuantityValues)
                .WithRequired(e => e.ProductDto)
                .HasForeignKey(e => e.ProductId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductDto>()
                .HasMany(x => x.DeliveryLocations)
                .WithMany(x => x.Products)
                .Map(mapper =>
                {
                    mapper.MapLeftKey("Product_Id_fk");
                    mapper.MapRightKey("DeliveryLocation_Id_fk");
                    mapper.ToTable("ProductDeliveryLocation");
                });
            
            modelBuilder.Entity<ProductTenorDto>()
                .HasRequired(pt => pt.ProductDto)
                .WithMany(pt => pt.ProductTenors)
                .HasForeignKey(pt => pt.ProductId);

            modelBuilder.Entity<ProductTenorDto>()
                .HasMany(pt => pt.ProductPriceLines)
                .WithRequired(pt => pt.ProductTenorDto)
                .HasForeignKey(pt => pt.ProductTenorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductTenorDto>()
                .HasMany(pt => pt.ProductTenorPeriods)
                .WithRequired(pt => pt.ProductTenorDto)
                .HasForeignKey(pt => pt.ProductTenorId)
                .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<ProductPriceLineDto>()
                .HasMany(ppl => ppl.ProductPriceLineBasesPeriods)
                .WithRequired(ppl => ppl.ProductPriceLineDto)
                .HasForeignKey(ppl => ppl.ProductPriceLineId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PriceBasisDto>()
                .HasMany(pb => pb.ProductPriceLineBasesPeriods)
                .WithRequired(pb => pb.PriceBasisDto)
                .HasForeignKey(pb => pb.PriceBasisId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PeriodDto>()
                .HasMany(ppl => ppl.ProductPriceLineBasesPeriods)
                .WithRequired(ppl => ppl.PeriodDto)
                .HasForeignKey(ppl => ppl.PeriodId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductPriceLineBasisPeriodDto>()
                .HasRequired(pt => pt.PriceBasisDto)
                .WithMany(pt => pt.ProductPriceLineBasesPeriods)
                .HasForeignKey(pt => pt.PriceBasisId);

            modelBuilder.Entity<ProductPriceLineBasisPeriodDto>()
                .HasRequired(pt => pt.PeriodDto)
                .WithMany(pt => pt.ProductPriceLineBasesPeriods)
                .HasForeignKey(pt => pt.PeriodId);

            modelBuilder.Entity<CommodityGroupDto>()
                .HasMany(e => e.CommodityTypes)
                .WithRequired(e => e.CommodityGroupDto)
                .HasForeignKey(e => e.CommodityGroupId)
                .WillCascadeOnDelete(false);       

            modelBuilder.Entity<CommodityClassDto>()
                .HasMany(e => e.CommodityGroups)
                .WithRequired(e => e.CommodityClassDto)
                .HasForeignKey(e => e.CommodityClassId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CommodityGroupDto>()
                .HasMany(e => e.CommodityTypes)
                .WithRequired(e => e.CommodityGroupDto)
                .HasForeignKey(e => e.CommodityGroupId)
                .WillCascadeOnDelete(false);


            /*
             * 
                modelBuilder.Entity<CommodityTypeDto>()
                        .HasMany(e => e.Commodities)
                        .WithRequired(e => e.CommodityTypeDto)
                        .HasForeignKey(e => e.CommodityTypeId)
                        .WillCascadeOnDelete(false);
             
                  modelBuilder.Entity<CommodityDto>()
                        .HasMany(e => e.Products)
                        .WithRequired(e => e.CommodityDto)
                        .HasForeignKey(e => e.CommodityId)
                        .WillCascadeOnDelete(false); 
             
                        modelBuilder.Entity<DeliveryLocationDto>()
                            .HasMany(e => e.Products)
                            .WithRequired(e => e.DeliveryLocationDto)
                            .HasForeignKey(e => e.LocationId)
                            .WillCascadeOnDelete(false);

                        modelBuilder.Entity<ExchangeDto>()
                            .HasMany(e => e.Products)
                            .WithRequired(e => e.ExchangeDto)
                            .HasForeignKey(e => e.ExchangeId)
                            .WillCascadeOnDelete(false);

                        modelBuilder.Entity<MarketDto>()
                            .HasMany(e => e.Products)
                            .WithRequired(e => e.MarketDto)
                            .HasForeignKey(e => e.MarketId)
                            .WillCascadeOnDelete(false);
            */

            /* AVOID ADDING MORE DEFINITIONS HERE
            Don't add foreign key back references here unliss your code explitly needs it
            it. They create complex SQL queries.
            e.g a product has a location....a location does not have products. 
            If you need products at a location then this can be achied through querying product.location
            */
        }

    }
}
