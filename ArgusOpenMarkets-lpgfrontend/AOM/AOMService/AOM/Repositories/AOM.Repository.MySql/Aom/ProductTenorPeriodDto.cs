﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ProductTenorPeriod")]
    public class ProductTenorPeriodDto
    {
        [Key, Column("ProductTenor_Id_fk", Order = 0)]
        public long ProductTenorId { get; set; }

        [Key, Column("DeliveryPeriod_Id_fk", Order = 1)]
        public long DeliveryPeriodId { get; set; }

        [Key, Column("DefaultPeriod_Id_fk", Order = 2)]
        public long DefaultPeriodId { get; set; }

        private DateTime _dateCreated;
        [DataType(DataType.DateTime), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [ForeignKey("ProductTenorId")]
        public ProductTenorDto ProductTenorDto { get; set; }

        [ForeignKey("DeliveryPeriodId")]
        public PeriodDto DeliveryPeriodDto { get; set; }

        [ForeignKey("DefaultPeriodId")]
        public PeriodDto DefaultPeriodDto { get; set; }
    }

}
