﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ScheduledTask")]
    public class ScheduledTaskDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime? _dateCreated;

        [Column(TypeName = "date")]
        public DateTime? DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime) _dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column]
        [StringLength(45)]
        public string TaskName { get; set; }

        [Column]
        [StringLength(45)]
        public string TaskType { get; set; }

        [Column]
        [StringLength(400)]
        public string Arguments { get; set; }

        [Column]
        [StringLength(1)]
        public string Interval { get; set; }
        
        private DateTime _nextRun;
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime NextRun
        {
            get { return DateTime.SpecifyKind(_nextRun, DateTimeKind.Utc); }
            set { _nextRun = value; }
        }

        private DateTime? _lastRun;
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime? LastRun
        {
            get { return _lastRun == null ? _lastRun : DateTime.SpecifyKind((DateTime) _lastRun, DateTimeKind.Utc); }
            set { _lastRun = value; }
        }
        
        [Column]
        public bool SkipIfMissed { get; set; }

        [Column]
        public bool IgnoreTask { get; set; }

        private DateTime _lastUpdated;
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }
    }
}