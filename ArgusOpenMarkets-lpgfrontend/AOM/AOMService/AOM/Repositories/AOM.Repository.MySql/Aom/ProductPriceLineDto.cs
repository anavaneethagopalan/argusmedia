﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ProductPriceLine")]
    public class ProductPriceLineDto
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column("ProductTenor_Id_fk")]
        public long ProductTenorId { get; set; }

        [Column("BasisProduct_Id_fk")]
        public long? BasisProductId { get; set; }

        public int SequenceNo { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [StringLength(100)]
        public string PercentageOptionList { get; set; }

        public bool DefaultDisplay { get; set; }

        private DateTime _dateCreated;
        [DataType(DataType.DateTime), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;
        [DataType(DataType.DateTime), ConcurrencyCheck, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        public virtual ICollection<ProductPriceLineBasisPeriodDto> ProductPriceLineBasesPeriods { get; set; }
        
        [ForeignKey("ProductTenorId")]
        public virtual ProductTenorDto ProductTenorDto { get; set; }

        [ForeignKey("BasisProductId")]
        public virtual ProductDto ProductDto { get; set; }

        //public ProductPriceLineDto()
        //{
        //    if (PriceBases == null) PriceBases = new List<PriceBasisDto>();
        //    if (PricePeriods == null) PricePeriods = new List<PricePeriodDto>();
        //}
    }
}
