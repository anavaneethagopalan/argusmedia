using System;

namespace AOM.Repository.MySql.Aom
{
    public interface ILogDto
    {
        DateTime LastUpdated { get; } //set; }
        long LastUpdatedUserId { get; set; }
    }
}