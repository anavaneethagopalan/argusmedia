
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.CommodityGroup")]
    public class CommodityGroupDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column]
        [StringLength(50)]
        public string Name { get; set; }

        [Column("CommodityClass_Id_fk")]
        public long CommodityClassId { get; set; }

        [ForeignKey("CommodityClass_Id_fk")]
        public virtual CommodityClassDto CommodityClassDto { get; set; }

        public virtual ICollection<CommodityTypeDto> CommodityTypes { get; set; }
    }
}
