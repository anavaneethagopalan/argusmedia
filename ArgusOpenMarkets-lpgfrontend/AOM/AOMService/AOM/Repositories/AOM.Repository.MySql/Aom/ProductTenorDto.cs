using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ProductTenor")]
    public class ProductTenorDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column("Product_Id_fk")]
        public long ProductId { get; set; }

        //[Column("Tenor_Id_fk")]
        //public long TenorId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string DisplayName { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string RollDate { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string RollDateRule { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string DeliveryDateStart { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string DeliveryDateEnd { get; set; }

        //[Column]
        //[Required]
        //public long MinimumDeliveryRange { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string DefaultStartDate { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string DefaultEndDate { get; set; }

        private DateTime? _dateCreated;
        [Column(TypeName = "date")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }


        [ForeignKey("ProductId")]
        public virtual ProductDto ProductDto { get; set; }

        public virtual ICollection<ProductPriceLineDto> ProductPriceLines { get; set; }

        public virtual ICollection<ProductTenorPeriodDto> ProductTenorPeriods { get; set; }

    }
}