﻿namespace AOM.Repository.MySql.Aom
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("aom.News")]
    public class NewsDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long NewsId { get; set; }

        [StringLength(50)]
        public string CmsId { get; set; }

        public DateTime PublicationDate { get; set; }

        public bool IsFree { get; set; }

        public string Story { get; set; }

        public List<NewsContentStreamDto> ContentStreams { get; set; }
    }
}