using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.Email")]
    public class EmailDto
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime _dateCreated;

        [Required]
        public DateTime DateCreated
        {
            get { return DateTime.SpecifyKind(_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _dateSent;
        public DateTime DateSent
        {
            get { return DateTime.SpecifyKind(_dateSent, DateTimeKind.Utc); }
            set { _dateSent = value; }
        }

        [Required, StringLength(1)]
        public string Status { get; set; }

        [Required]
        [StringLength(100)]
        public string Recipient { get; set; }

        [Required]
        [StringLength(100)]
        public string Subject { get; set; }

        [Required]
        [StringLength(8000)]
        public string Body { get; set; }
    }
}