using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ProductTenor_Log")]
    public class ProductTenorLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [Key]
        [Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        private DateTime? _dateCreated;
        [Column(TypeName = "date")]
        public DateTime? DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("Product_Id_fk")]
        public long ProductId { get; set; }

        //[Column("Tenor_Id_fk")]
        //public long TenorId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string DisplayName { get; set; }
        
        //[Column]
        //[StringLength(50)]
        //public string RollDate { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string RollDateRule { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string DeliveryDateStart { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string DeliveryDateEnd { get; set; }

        //[Column]
        //[Required]
        //public long MinimumDeliveryRange { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string DefaultStartDate { get; set; }

        //[Column]
        //[StringLength(50)]
        //public string DefaultEndDate { get; set; }
    }
}