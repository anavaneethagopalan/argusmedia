﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AOM.Repository.MySql.Aom
{
    public abstract class MetaDataDto 
    {        
        public long ProductMetaDataId { get; set; }
        public string DisplayName { get; set; }
        public string DisplayValue { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public MetaDataTypes MetaDataType { get; protected set; }

        protected MetaDataDto(MetaDataTypes metaDataType)
        {
            MetaDataType = metaDataType;
        }
       
    }
}
