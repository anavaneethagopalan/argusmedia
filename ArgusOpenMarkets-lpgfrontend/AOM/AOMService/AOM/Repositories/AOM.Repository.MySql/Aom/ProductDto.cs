namespace AOM.Repository.MySql.Aom
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("aom.Product")]
    public class ProductDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        private DateTime _dateCreated;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateCreated
        {
            get { return DateTime.SpecifyKind(_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        public bool IsDeleted { get; set; }

        public bool IsInternal { get; set; }

        private DateTime _lastUpdated;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [Column("Commodity_Id_fk")]
        public long CommodityId { get; set; }

        [Column("Market_Id_fk")]
        public long MarketId { get; set; }

        [Column("Contracttype_Id_fk")]
        public long ContractTypeId { get; set; }

        //[Column("Location_Id_fk")]
        //public long LocationId { get; set; }

        //[Column("Currency_Code_fk")]
        //[Required]
        //[StringLength(3)]
        //public string Currency { get; set; }

        [Column("VolumeUnit_code_fk")]
        [Required]
        [StringLength(10)]
        public string VolumeUnit { get; set; }

        //[Column("PriceUnit_Code_fk")]
        //[Required]
        //[StringLength(10)]
        //public string PriceUnit { get; set; }

        public decimal OrderVolumeDefault { get; set; }

        public decimal OrderVolumeMinimum { get; set; }

        public decimal OrderVolumeMaximum { get; set; }

        public decimal OrderVolumeIncrement { get; set; }

        public decimal OrderVolumeDecimalPlaces { get; set; }

        public decimal OrderVolumeSignificantFigures { get; set; }

        public decimal OrderPriceMinimum { get; set; }

        public decimal OrderPriceMaximum { get; set; }

        public decimal OrderPriceIncrement { get; set; }

        public decimal OrderPriceDecimalPlaces { get; set; }

        public decimal OrderPriceSignificantFigures { get; set; }

        public int BidAskStackNumberRows { get; set; }

        [Column("BidAskStackStyle_Code_fk")]
        [Required]
        [StringLength(1)]
        public string BidAskStackStyleCode { get; set; }

        public bool IgnoreAutomaticMarketStatusTriggers { get; set; }

        public bool AllowNegativePriceForExternalDeals { get; set; }

        public bool DisplayOptionalPriceDetail { get; set; }

        public bool HasAssessment { get; set; }

        [Column("Status", TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string Status { get; set; }

        private DateTime? _lastOpenDate;
        public DateTime? LastOpenDate
        {
            get { return _lastOpenDate == null ? _lastOpenDate : DateTime.SpecifyKind((DateTime)_lastOpenDate, DateTimeKind.Utc); }
            set { _lastOpenDate = value; }
        }

        private DateTime? _lastCloseDate;
        public DateTime? LastCloseDate
        {
            get { return _lastCloseDate == null ? _lastCloseDate : DateTime.SpecifyKind((DateTime)_lastCloseDate, DateTimeKind.Utc); }
            set { _lastCloseDate = value; }
        }

        private TimeSpan? _openTime;
        public TimeSpan? OpenTime
        {
            get { return _openTime; }
            set { _openTime = value; }
        }

        private TimeSpan? _closeTime;
        public TimeSpan? CloseTime
        {
            get { return _closeTime; }
            set { _closeTime = value; }
        }

        private TimeSpan? _purgeTimeOfDay;
        public TimeSpan? PurgeTimeOfDay
        {
            get { return _purgeTimeOfDay; }
            set { _purgeTimeOfDay = value; }
        }

        private DateTime? _purgeDate;
        public DateTime? PurgeDate
        {
            get { return _purgeDate == null ? _purgeDate : DateTime.SpecifyKind((DateTime)_purgeDate, DateTimeKind.Utc); }
            set { _purgeDate = value; }
        }

        public int PurgeFrequency { get; set; }

        //[StringLength(45)]
        //public string LocationLabel { get; set; }

        [StringLength(45)]
        public string DeliveryPeriodLabel { get; set; }

        public int DisplayOrder { get; set; }

        public bool CoBrokering { get; set; }

        public bool OrderShowNotes { get; set; }

        public bool ExternalDealShowNotes { get; set; }

        public bool DisplayContractName { get; set; }

        [ForeignKey("CommodityId")]
        public virtual CommodityDto CommodityDto { get; set; }

        [ForeignKey("ContractTypeId")]
        public virtual ContractTypeDto ContractTypeDto { get; set; }

        //[ForeignKey("Currency")]
        //public virtual CurrencyDto CurrencyDto { get; set; }

        //[ForeignKey("LocationId")]
        //public virtual DeliveryLocationDto DeliveryLocationDto { get; set; }

        //[ForeignKey("ExchangeId")]
        //public virtual ExchangeDto ExchangeDto { get; set; }

        [ForeignKey("MarketId")]
        public virtual MarketDto MarketDto { get; set; }

        [ForeignKey("VolumeUnit")]
        public virtual UnitDto VolumeUnitDto { get; set; }

        //[ForeignKey("PriceUnit")]
        //public virtual UnitDto PriceUnitDto { get; set; }

        public virtual ICollection<DeliveryLocationDto> DeliveryLocations { get; set; }

        public virtual ICollection<CommonQuantityValuesDto> CommonQuantityValues { get; set; }

        public virtual ICollection<ProductTenorDto> ProductTenors { get; set; }
    }
}