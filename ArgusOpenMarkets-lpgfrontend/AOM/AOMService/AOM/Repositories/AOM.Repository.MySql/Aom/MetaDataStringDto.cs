﻿using ServiceStack.Text;

namespace AOM.Repository.MySql.Aom
{
    public class MetaDataStringDto : MetaDataDto
    {
        static MetaDataStringDto()
        {
            // ReSharper disable once UnusedMember.Local
            JsConfig<MetaDataStringDto>.IncludeTypeInfo = true;
        }
        public MetaDataStringDto() : base(MetaDataTypes.String) { }
    }
}