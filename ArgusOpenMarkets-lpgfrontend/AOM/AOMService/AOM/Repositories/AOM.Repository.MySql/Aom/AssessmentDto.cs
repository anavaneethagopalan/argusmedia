﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.Assessment")]
    public class AssessmentDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public DateTime DateCreated { get; set; }

        private DateTime _lastUpdated;
        
        [ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("EnteredByUser_Id")]
        public long EnteredByUserId { get; set; }

        [Column("LastUpdatedUser_Id")]
        public long LastUpdatedUserId { get; set; }

        [Column("Product_Id_fk")]
        public long ProductId { get; set; }

        public decimal? PriceHigh { get; set; }

        public decimal? PriceLow { get; set; }

        public DateTime BusinessDate { get; set; }

        [Required]
        [StringLength(1)]
        public string AssessmentStatus { get; set; }

    }
}
