using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.OrderStatus")]
    public class OrderStatusDto
    {
        [Key]
        [Column(TypeName = "char")]
        [StringLength(1)]
        public string Code { get; set; }

        [StringLength(20)]
        public string Name { get; set; }        
    }
}