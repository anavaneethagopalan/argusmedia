﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace AOM.Repository.MySql.Aom
{
    public class ProductMetaDataModel : LoggingDbContext, IProductMetaDataModel
    {
        public ProductMetaDataModel()
            : base("AomModel")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DoNothingInitialiser<AomModel>());
        }

        public override int SaveChanges()
        {
            throw new NotImplementedException("Please use the SaveChangesAndLog Method");
        }

        public virtual IDbSet<ProductMetaDataItemDto> ProductMetaDataItems { get; set; }
        public virtual IDbSet<MetaDataListItemDto> MetaDataListItems { get; set; }
        public virtual IDbSet<MetaDataListDto> MetaDataLists { get; set; }

        public virtual IDbSet<ProductMetaDataItemLogDto> ProductMetaDataItemsLogs { get; set; }
        public virtual IDbSet<MetaDataListItemLogDto> MetaDataListItemsLogs { get; set; }
        public virtual IDbSet<MetaDataListLogDto> MetaDataListsLogs { get; set; }
    }

    [Table("aom.MetaDataList")]
    public class MetaDataListDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, StringLength(50)]
        public string Description { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        public virtual ICollection<MetaDataListItemDto> MetaDataListItems { get; set; }
    }

    [Table("aom.MetaDataList_Log")]
    public class MetaDataListLogDto
    {
        [Key, Column(Order = 0)]
        public long Id { get; set; }

        [Required, StringLength(50)]
        public string Description { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        private DateTime _lastUpdated;

        [Key]
        [Column(Order = 1, TypeName = "DATETIME")]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }
    }

    [Table("aom.MetaDataListItem")]
    public class MetaDataListItemDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, Column("MetaDataList_Id_fk")]
        public long MetaDataListId { get; set; }

        [Required, ForeignKey("MetaDataListId")]
        public virtual MetaDataListDto MetaDataList { get; set; }

        [Column("DisplayOrder")]
        public long DisplayOrder { get; set; }

        [Required, StringLength(50)]
        public string ValueText { get; set; }

        [Required, Column("ValueInt")]
        public long ValueLong { get; set; }

        [Required]
        public bool IsDeleted { get; set; }
    }

    [Table("aom.MetaDataListItem_Log")]
    public class MetaDataListItemLogDto
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, Column("MetaDataList_Id_fk")]
        public long MetaDataListId { get; set; }

        [Column("DisplayOrder")]
        public long DisplayOrder { get; set; }

        [Required, StringLength(50)]
        public string ValueText { get; set; }

        [Required, Column("ValueInt")]
        public long ValueLong { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        private DateTime _lastUpdated;

        [Key]
        [Column(Order = 1, TypeName = "DATETIME")]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }
    }

    public enum MetaDataTypes
    {
        IntegerEnum = 10,
        String = 20,
        Unknown = 0
    }

    [Table("aom.ProductMetaDataItem")]
    public class ProductMetaDataItemDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, Column("Product_Id_fk")]
        public long ProductId { get; set; }

        [Required, Column("DisplayOrder")]
        public long DisplayOrder { get; set; }

        [Required, StringLength(50)]
        public string DisplayName { get; set; }

        [Required, Column("MetaDataType_Id_fk")]
        public MetaDataTypes MetaDataTypeId { get; set; }

        [Column("MetaDataList_Id_fk")]
        public long? MetaDataListId { get; set; }

        [ForeignKey("MetaDataListId")]
        public virtual MetaDataListDto MetaDataList { get; set; }

        [Column("ValueMinimum")]
        public int? ValueMinimum { get; set; }

        [Column("ValueMaximum")]
        public int? ValueMaximum { get; set; }

        [Required, Column("IsDeleted")]
        public bool IsDeleted { get; set; }

        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated { get; set; }
    }

    [Table("aom.ProductMetaDataItem_Log")]
    public class ProductMetaDataItemLogDto
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, Column("Product_Id_fk")]
        public long ProductId { get; set; }

        [Required, Column("DisplayOrder")]
        public long DisplayOrder { get; set; }

        [Required, StringLength(50)]
        public string DisplayName { get; set; }

        [Required, Column("MetaDataType_Id_fk")]
        public MetaDataTypes MetaDataTypeId { get; set; }

        [Column("MetaDataList_Id_fk")]
        public long? MetaDataListId { get; set; }

        [Column("ValueMinimum")]
        public int? ValueMinimum { get; set; }

        [Column("ValueMaximum")]
        public int? ValueMaximum { get; set; }

        [Required, Column("IsDeleted")]
        public bool IsDeleted { get; set; }

        [Key, Column(Order = 1)]
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated { get; set; }


        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }
    }

    public interface IProductMetaDataModel : IDisposable
    {
        IDbSet<ProductMetaDataItemDto> ProductMetaDataItems { get; set; }
        IDbSet<MetaDataListItemDto> MetaDataListItems { get; set; }
        IDbSet<MetaDataListDto> MetaDataLists { get; set; }

        IDbSet<ProductMetaDataItemLogDto> ProductMetaDataItemsLogs { get; set; }
        IDbSet<MetaDataListItemLogDto> MetaDataListItemsLogs { get; set; }
        IDbSet<MetaDataListLogDto> MetaDataListsLogs { get; set; }

        int SaveChangesAndLog(long userId);
    }
}