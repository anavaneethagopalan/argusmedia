﻿namespace AOM.Repository.MySql.Aom
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("aom.NewsContentStream")]
    public class NewsContentStreamDto
    {
        [Key]
        [Column(Order = 0)]
        public long NewsId { get; set; }

        [Key]
        [Column(Order = 1)]
        public long ContentStreamId { get; set; }

        [ForeignKey("NewsId")]
        public virtual NewsDto News { get; set; }
    }
}