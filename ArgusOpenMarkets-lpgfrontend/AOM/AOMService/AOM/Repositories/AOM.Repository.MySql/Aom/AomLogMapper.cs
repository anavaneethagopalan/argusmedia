﻿namespace AOM.Repository.MySql.Aom
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    using AutoMapper;

    public static class AomLogMapper
    {
        public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> map,
            Expression<Func<TDestination, object>> selector)
        {
            map.ForMember(selector, config => config.Ignore());
            return map;
        }

        static AomLogMapper()
        {
            Mapper.CreateMap<AssessmentDto, AssessmentLogDto>();
            Mapper.CreateMap<CommodityDto, CommodityLogDto>();
            Mapper.CreateMap<ContractTypeDto, ContractTypeLogDto>();
            Mapper.CreateMap<CommodityClassDto, CommodityClassLogDto>();
            Mapper.CreateMap<CommonQuantityValuesDto, CommonQuantityValuesLogDto>();
            Mapper.CreateMap<UnitDto, UnitLogDto>();
            Mapper.CreateMap<DealDto, DealLogDto>();
            Mapper.CreateMap<ExternalDealDto, ExternalDealLogDto>();
            Mapper.CreateMap<MarketInfoDto, MarketInfoLogDto>();
            Mapper.CreateMap<MarketTickerItemDto, MarketTickerItemLogDto>();
            Mapper.CreateMap<OrderDto, OrderLogDto>();
            Mapper.CreateMap<ProductDto, ProductLogDto>();
            Mapper.CreateMap<TenorCodeDto, TenorCodeLogDto>();
            Mapper.CreateMap<ProductTenorDto, ProductTenorLogDto>();
            Mapper.CreateMap<OrderPriceLineDto, OrderPriceLineLogDto>();
            Mapper.CreateMap<PriceLineDto, PriceLineLogDto>();
            Mapper.CreateMap<PriceLineBasisDto, PriceLineBasisLogDto>();
            Mapper.CreateMap<PriceBasisDto, PriceBasisLogDto>();
            Mapper.AssertConfigurationIsValid();
        }

        public static object GetLogObject<T>(T entity, long userId)
        {
            var entityType = entity.GetType();

            if (entityType.BaseType != null && entityType.Namespace == "System.Data.Entity.DynamicProxies")
            {
                entityType = entityType.BaseType; // ensure we use the base type for objects returned from cache
            }

            var map = Mapper.GetAllTypeMaps().FirstOrDefault(x => x.SourceType == entityType);
            if (map != null)
            {
                var dest = map.DestinationType;
                var destination = Mapper.Map(entity, entityType, dest);
                var logdto = destination as ILogDto;

                logdto.LastUpdatedUserId = logdto.LastUpdatedUserId == 0 ? userId : logdto.LastUpdatedUserId;
                //logdto.LastUpdated = DateTime.UtcNow;

                return destination;
            }

            return null;
        }
    }
}