using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.Currency")]
    public class CurrencyDto
    {
        [Column, Key, StringLength(3)]
        public string Code { get; set; }

        [Column, Required, StringLength(3)]
        public string DisplayName { get; set; }

        [Column, StringLength(100)]
        public string Name { get; set; }

        private DateTime? _dateCreated;
        [Column]
        public DateTime? DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }
        
    }
}
