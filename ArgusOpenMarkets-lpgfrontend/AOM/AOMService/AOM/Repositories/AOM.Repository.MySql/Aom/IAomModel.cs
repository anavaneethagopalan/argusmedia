using System.Data.Entity;

namespace AOM.Repository.MySql.Aom
{
    public interface IAomModel : IProductMetaDataModel
    {
        IDbSet<NewsDto> News { get; set; }
        IDbSet<NewsContentStreamDto> NewsContentStream { get; set; }
        IDbSet<AssessmentDto> Assessments { get; set; }
        IDbSet<CommodityDto> Commodities { get; set; }
        IDbSet<CommodityClassDto> CommodityClasses { get; set; }
        IDbSet<CommodityGroupDto> CommodityGroups { get; set; }
        IDbSet<CommodityTypeDto> CommodityTypes { get; set; }
        IDbSet<ContractTypeDto> ContractTypes { get; set; }
        IDbSet<UnitLogDto> UnitLogs { get; set; }
        IDbSet<CommodityContentStreamDto> CommodityContentStreams { get; set; }
        IDbSet<CurrencyDto> Currencies { get; set; }
        IDbSet<DealDto> Deals { get; set; }
        IDbSet<DealStatusDto> DealStatus { get; set; }
        IDbSet<DeliveryLocationDto> DeliveryLocations { get; set; }
        IDbSet<EmailDto> Emails { get; set; }
        IDbSet<ExternalDealDto> ExternalDeals { get; set; }
        IDbSet<ErrorLogDto> Errors { get; set; }
        IDbSet<MarketDto> Markets { get; set; }
        IDbSet<MarketInfoDto> MarketInfoItems { get; set; }
        IDbSet<MarketInfoProductChannelDto> MarketInfoProductChannels { get; set; }
        IDbSet<MarketItemStatusDto> MarketItemStatus { get; set; }
        IDbSet<MarketItemTypeDto> MarketItemTypes { get; set; }
        IDbSet<MarketTickerItemDto> MarketTickerItems { get; set; }
        IDbSet<MarketTickerItemProductChannelDto> MarketTickerItemProductChannels { get; set; }
        IDbSet<OrderDto> Orders { get; set; }
        IDbSet<OrderStatusDto> OrderStatus { get; set; }
        IDbSet<ProductDto> Products { get; set; }
        IDbSet<TenorCodeDto> TenorCodes { get; set; }
        IDbSet<ProductTenorDto> ProductTenors { get; set; }
        IDbSet<ScheduledTaskDto> ScheduledTasks { get; set; } 
        IDbSet<UnitDto> Units { get; set; }
        IDbSet<PriceTypeDto> PriceTypes { get; set; }
        IDbSet<PriceBasisDto> PriceBases { get; set; }
        IDbSet<ProductPriceLineDto> ProductPriceLines { get; set; }
        IDbSet<ProductPriceLineBasisPeriodDto> ProductPriceLineBasesPeriods { get; set; }
        IDbSet<PriceLineDto> PriceLines { get; set; }
        IDbSet<OrderPriceLineDto> OrderPriceLines { get; set; }
        IDbSet<PriceLineBasisDto> PriceLineBases { get; set; }
        IDbSet<PeriodDto> Periods { get; set; }
        IDbSet<ProductTenorPeriodDto> ProductTenorPeriods { get; set; }
        //IDbSet<ProductPriceLinePeriodDto> ProductPriceLinePeriods { get; set; }
    }
}