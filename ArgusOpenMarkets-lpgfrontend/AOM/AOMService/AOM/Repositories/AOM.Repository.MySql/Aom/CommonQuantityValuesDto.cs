﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.CommonQuantityValues")]
    public class CommonQuantityValuesDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column]
        public decimal Quantity { get; set; }

        [Column("Product_Id_Fk")]
        public long ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual ProductDto ProductDto { get; set; }

        private DateTime? _dateCreated;
        [Column]
        public DateTime? DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;
        [ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdatedUser_Id")]
        public long LastUpdatedUserId { get; set; }
        
        [Column("EnteredByUser_Id")]
        public long? EnteredByUserId { get; set; }
        
        [StringLength(50)]
        public string QuantityText { get; set; }

        public int DisplayOrder { get; set; }
        
        public bool DisplayQuantityText { get; set; }
    }
}
