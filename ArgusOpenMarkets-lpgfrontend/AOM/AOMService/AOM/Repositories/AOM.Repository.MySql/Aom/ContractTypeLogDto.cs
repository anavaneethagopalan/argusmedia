﻿namespace AOM.Repository.MySql.Aom
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("aom.ContractType_Log")]
    public class ContractTypeLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public DateTime? DateCreated { get; set; }


        private DateTime _lastUpdated;
        [Key]
        [Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdatedUser_Id")]
        public long LastUpdatedUserId { get; set; }

        
        [Column("EnteredByUser_Id")]
        public long? EnteredByUserId { get; set; }

        [Column("Name")]
        [StringLength(100)]
        public string Name { get; set; }
    }
}