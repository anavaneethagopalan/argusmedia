﻿namespace AOM.Repository.MySql.Aom
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("aom.Unit_Log")]
    public class UnitLogDto : ILogDto
    {
        [Key]
        [Column("Code")]
        [StringLength(10)]
        public string Code { get; set; }

        [Column("Name")]
        [StringLength(10)]
        public string Name { get; set; }

        [Column("Description")]
        [StringLength(20)]
        public string Description { get; set; }

        private DateTime? _dateCreated;

        public DateTime? DateCreated
        {
            get
            {
                return _dateCreated == null
                    ? _dateCreated
                    : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc);
            }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;

        [ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdatedUser_Id")]
        public long LastUpdatedUserId { get; set; }


        [Column("EnteredByUser_Id")]
        public long? EnteredByUserId { get; set; }
    }
}