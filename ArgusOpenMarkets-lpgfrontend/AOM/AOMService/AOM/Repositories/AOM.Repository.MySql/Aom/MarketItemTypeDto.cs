using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.MarketItemType")]
    public class MarketItemTypeDto
    {
        [Key]
        [Column(TypeName = "char")]
        [StringLength(1)]
        public string Code { get; set; }

        [StringLength(20)]
        public string Name { get; set; }

        public virtual ICollection<MarketTickerItemDto> MarketTickerItems { get; set; }
    }
}