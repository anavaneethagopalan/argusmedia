﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.Assessment_Log")]
    public class AssessmentLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [Key]
        [Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdatedUser_Id")]
        public long LastUpdatedUserId { get; set; }

        public DateTime DateCreated { get; set; }

        [Column("EnteredByUser_Id")]
        public long EnteredByUserId { get; set; }

        [Column("Product_Id_fk")]
        public long ProductId { get; set; }

        public decimal? PriceHigh { get; set; }

        public decimal? PriceLow { get; set; }

        public DateTime BusinessDate { get; set; }

        [Required]
        [StringLength(1)]
        public string AssessmentStatus { get; set; }
    }
}