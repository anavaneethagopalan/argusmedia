﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.Period_Log")]
    public class PeriodLogDto : ILogDto
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [Key, Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("TenorCode_fk"), StringLength(10)]
        public string TenorCode { get; set; }

        [Column("PeriodActionCode_fk"), StringLength(10)]
        public string PeriodActionCode { get; set; }

        [StringLength(20)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(50)]
        public string RollDate { get; set; }

        //[StringLength(50)]
        //public string RollDateRule { get; set; }

        public long? MinimumDeliveryRange { get; set; }

        public int? RollingPeriodFrom { get; set; }

        public int? RollingPeriodTo { get; set; }

        [StringLength(20)]
        public string FixedDateFrom { get; set; }

        [StringLength(20)]
        public string FixedDateTo { get; set; }

        private DateTime _dateCreated;
        [DataType(DataType.DateTime)]
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }
    }
}