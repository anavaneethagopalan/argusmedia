using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Newtonsoft.Json;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.Order")]
    public class OrderDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime _lastUpdated;

        [DataType(DataType.DateTime), ConcurrencyCheck]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string BidOrAsk { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string InternalOrExternal { get; set; }

        [Column("isVirtual")]
        public sbyte IsVirtual { get; set; }

        public decimal Quantity { get; set; }

        [StringLength(50)]
        public string QuantityText { get; set; }

        public decimal Price { get; set; }

        [StringLength(50)]
        public string DeliveryPeriod { get; set; }

        private DateTime _deliveryStartDate;

        [Column(TypeName = "date")]
        public DateTime DeliveryStartDate
        {
            get { return DateTime.SpecifyKind(_deliveryStartDate, DateTimeKind.Utc); }
            set { _deliveryStartDate = value; }
        }

        private DateTime _deliveryEndDate;

        [Column(TypeName = "date")]
        public DateTime DeliveryEndDate
        {
            get { return DateTime.SpecifyKind(_deliveryEndDate, DateTimeKind.Utc); }
            set { _deliveryEndDate = value; }
        }

        [StringLength(1000)]
        public string Message { get; set; }

        [Column("User_Id_fk")]
        public long? UserId { get; set; }

        [Column("Organisation_Id_fk")]
        public long OrganisationId { get; set; }

        [Column("Broker_Id_fk")]
        public long? BrokerId { get; set; }

        [Column("BrokerOrganisation_Id_fk")]
        public long? BrokerOrganisationId { get; set; }

        [Column("OrderStatus_Code_fk", TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string OrderStatusCode { get; set; }

        [Column("LastStatus_Code", TypeName = "char")]
        [StringLength(1)]
        public string LastOrderStatusCode { get; set; }

        [Column("EnteredByUser_fk")]
        public long? EnteredByUserId { get; set; }

        private DateTime _dateCreated;

        [DataType(DataType.DateTime)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] // cannot use computed columns and concurrencycheck columns in mysql due to a bug https://bugs.mysql.com/bug.php?id=81477
        public DateTime DateCreated
        {
            get { return DateTime.SpecifyKind(_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("ProductTenor_Id_fk")]
        [Required]
        public long ProductTenorId { get; set; }

        [ForeignKey("ProductTenorId")]
        public virtual ProductTenorDto ProductTenorDto { get; set; }

        [Column("BrokerRestriction", TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string BrokerRestriction { get; set; }

        [Column("CoBrokering")]
        public bool CoBrokering { get; set; }

        [Column("MetaData")]
        [StringLength(5000)]
        public string MetaDataJson
        {
            get { return JsonConvert.SerializeObject(MetaData, Formatting.None, new JsonConverter[] { new MetaDataDtoJsonConverter() }); }
            set { MetaData = JsonConvert.DeserializeObject<MetaDataDto[]>(value, new JsonConverter[] { new MetaDataDtoJsonConverter() }); }
        }

        [NotMapped]
        public MetaDataDto[] MetaData { get; set; }

        public virtual ICollection<OrderPriceLineDto> OrderPriceLines { get; set; }
    }
}