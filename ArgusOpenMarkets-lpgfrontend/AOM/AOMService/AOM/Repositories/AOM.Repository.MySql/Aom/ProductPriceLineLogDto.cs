﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.ProductPriceLine_Log")]
    public class ProductPriceLineLogDto
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        private DateTime _lastUpdated;
        [Key, Column(Order = 1)]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("ProductTenor_Id_fk")]
        public long ProductTenorId { get; set; }

        [Column("BasisProduct_Id_fk")]
        public long? BasisProductId { get; set; }

        public int SequenceNo { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [StringLength(100)]
        public string PercentageOptionList { get; set; }

        public bool DefaultDisplay { get; set; }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }
    }
}

