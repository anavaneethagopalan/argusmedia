﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Aom
{
    [Table("aom.PriceBasis")]
    public class PriceBasisDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime? _dateCreated;
        [Column(TypeName = "date")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? DateCreated
        {
            get { return _dateCreated == null ? _dateCreated : DateTime.SpecifyKind((DateTime)_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }

        private DateTime _lastUpdated;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind(_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        [Column("LastUpdated_User_Id")]
        public long LastUpdatedUserId { get; set; }

        [Column("PriceType_Id_fk")]
        public long PriceTypeId { get; set; }

        [Column("PriceUnit_Code_fk")]
        public string PriceUnitCode { get; set; }

        [Column("Currency_Code_fk")]
        public string CurrencyCode { get; set; }

        [StringLength(20)]
        public string ShortName { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
        public decimal PriceMinimum { get; set; }
        public decimal PriceMaximum { get; set; }
        public decimal PriceIncrement { get; set; }
        public decimal PriceDecimalPlaces { get; set; }
        public decimal PriceSignificantFigures { get; set; }

        [ForeignKey("PriceTypeId")]
        public virtual PriceTypeDto PriceTypeDto { get; set; }

        [ForeignKey("PriceUnitCode")]
        public virtual UnitDto PriceUnitDto { get; set; }

        [ForeignKey("CurrencyCode")]
        public virtual CurrencyDto CurrencyDto { get; set; }

        public virtual ICollection<ProductPriceLineBasisPeriodDto> ProductPriceLineBasesPeriods { get; set; }
    }
}
