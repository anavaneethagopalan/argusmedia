﻿using Ninject;
using Ninject.Modules;

namespace AOM.Repository.MySql
{
    public class EFModelBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<IDbContextFactory>().To<DbContextFactory>();    
        }

        public override void Load()
        {
            Load(Kernel);            
        }
    }
}
