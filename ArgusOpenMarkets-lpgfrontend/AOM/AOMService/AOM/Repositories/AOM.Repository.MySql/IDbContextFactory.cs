﻿using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm;

namespace AOM.Repository.MySql
{
    public interface IDbContextFactory
    {
        IAomModel CreateAomModel();
        ICrmModel CreateCrmModel();
        IOrganisationModel CreateOrganisationModel();
        IUserModel CreateUserModel();
        IAuthenticationModel CreateAuthenticationModel();
        IPrivilegeModel CreatePrivilegeModel();
        IArgusCrmFeedModel CreateArgusCrmFeedModel();
        IProductMetaDataModel CreateProductMetaDataModel();
        IClientLogModel CreateClientLogModel();
    }
}