﻿using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;

namespace AOM.Repository.MySql
{
    /// <summary>
    /// This class has been added to overcome a bug in the mysql driver: https://bugs.mysql.com/bug.php?id=73271.
    /// An additional where clause is added for computed columns that prevents the id of an updated record from being returned.
    /// </summary>
    public class MySqlEFCommandInterceptor : IDbCommandInterceptor
    {
        public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            //Console.WriteLine("here");
        }

        public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            //Console.WriteLine("here");
        }

        public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            //Console.WriteLine("here");
            //DateTime? LastUpdated = null;
            //if (command.CommandText.Contains("UPDATE"))
            //{
                //if (command.CommandText.Contains("\r\nSELECT\r\n`DateCreated`, \r\n`LastUpdated`\r\nFROM `ExternalDeal`\r\n WHERE  row_count() > 0"))
                 //   LastUpdated = interceptionContext.Result[1] as DateTime?;
            //}
        }

        public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            //HACK to overcome mysql bug https://bugs.mysql.com/bug.php?id=73271
            if (command.CommandText.Contains("UPDATE "))
            {
                if (command.CommandText.Contains("row_count()"))
                {
                    // just to be safe, check if only an update on an allowed table is being hacked.
                    string[] validTables = new string[] { "Order", "OrderPriceLine", "PriceLine", "PriceLineBasis", "ExternalDeal", "Deal", "Tenor", "ProductTenor", "Product" };
                    var tableNameStartIndex = command.CommandText.IndexOf("`") + 1;
                    var tableNameEndIndex = command.CommandText.IndexOf("`", tableNameStartIndex + 1);
                    if (validTables.Contains(command.CommandText.Substring(tableNameStartIndex, tableNameEndIndex - tableNameStartIndex)))
                    {
                        var concurrencyClauseIndex = command.CommandText.LastIndexOf("AND (`LastUpdated` =");
                        command.CommandText = command.CommandText.Substring(0, concurrencyClauseIndex);
                    }
                }
            }
        }

        public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            //Console.WriteLine("here");
        }

        public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            //Console.WriteLine("here");
        }
    }
}
