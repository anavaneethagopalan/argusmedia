﻿using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Crm.AOM.Repository.MySql.Crm;

namespace AOM.Repository.MySql
{
    public class DbContextFactory : IDbContextFactory
    {
        public IAomModel CreateAomModel()
        {
            return new AomModel();
        }

        public ICrmModel CreateCrmModel()
        {
            return new CrmModel();
        }

        public IOrganisationModel CreateOrganisationModel()
        {
            return new OrganisationModel();
        }

        public IUserModel CreateUserModel()
        {
            return new UserModel();
        }

        public IAuthenticationModel CreateAuthenticationModel()
        {
            return new AuthenticationModel();
        }

        public IClientLogModel CreateClientLogModel()
        {
            return new ClientLogModel();
        }


        public IPrivilegeModel CreatePrivilegeModel()
        {
            return new PrivilegeModel();
        }

        public IArgusCrmFeedModel CreateArgusCrmFeedModel()
        {
            return new ArgusCrmFeedModel();
        }

        public IProductMetaDataModel CreateProductMetaDataModel()
        {
            return new ProductMetaDataModel();
        }
    }
}
