﻿using System.Transactions;

namespace AOM.Repository.MySql
{
    public static class TransactionScopeHelper
    {
        static public TransactionScope CreateTransactionScopeForConcurrencyCheck()
        {
            return new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted });
        }
    }
}