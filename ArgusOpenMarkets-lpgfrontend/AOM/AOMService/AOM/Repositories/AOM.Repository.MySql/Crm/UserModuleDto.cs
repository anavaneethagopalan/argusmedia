﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserModule")]
    public class UserModuleDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column("UserName")]
        [StringLength(100)]
        public string ArgusCrmUsername { get; set; }

        public long ModuleId_fk { get; set; }

        [StringLength(200)]
        public string Stream { get; set; }

        public long? StreamId { get; set; }

        public long? UserOracleCrmId { get; set; }

        [StringLength(45)]
        public string TypeName { get; set; }

        [ForeignKey("ModuleId_fk")]
        public ModuleDto Module { get; set; }
    }
}