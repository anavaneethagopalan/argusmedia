﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserLoginSource")]
    public class UserLoginSourceDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(10)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public long Organisation_Id_fk { get; set; }
        [ForeignKey("Organisation_Id_fk")]
        public virtual OrganisationDto OrganisationDto { get; set; }
    }
}