﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public class ArgusCrmFeedModel : LoggingDbContext, IArgusCrmFeedModel
    {
        public ArgusCrmFeedModel()
            : base("CrmModel")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DoNothingInitialiser<CrmModel>());
        }

        public IDbSet<UserInfoDto> UserInfoes { get; set; }
        public IDbSet<ModuleDto> Modules { get; set; }
        public IDbSet<UserModuleDto> UserModules { get; set; }
        public IDbSet<ContentStreamDto> ContentStreams { get; set; }

    }
}