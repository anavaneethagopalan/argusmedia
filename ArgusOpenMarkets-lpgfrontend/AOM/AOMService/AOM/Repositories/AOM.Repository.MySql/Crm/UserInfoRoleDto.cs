﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserInfoRole")]
    public class UserInfoRoleDto
    {
        [Key,Column(Order = 0)]
        public long UserInfo_Id_fk { get; set; }

        [Key, Column(Order = 1)]
        public long Role_Id_fk { get; set; }

        [ForeignKey("UserInfo_Id_fk")]        
        public UserInfoDto User { get; set; }

        [ForeignKey("Role_Id_fk")]
        public OrganisationRoleDto OrganisationRole { get; set; }
    }
}


