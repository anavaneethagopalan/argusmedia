using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    public interface IIntraOrganisationPermission
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        long Id { get; set; }

        DateTime LastUpdated { get; set; }
        long LastUpdatedUserId { get; set; }
        long ProductId { get; set; }

        [Column]
        [Index("ix_brokerpermission_1", 1)]
        [Required]
        long OurOrganisation { get; set; }

        [Column(TypeName = "char")]
        [Index("ix_brokerpermission_1", 2)]
        [Required]
        [StringLength(1)]
        string BuyOrSell { get; set; }

        [Column]
        [Index("ix_brokerpermission_1", 3)]
        [Required]
        long TheirOrganisation { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        string AllowOrDeny { get; set; }
    }
}