﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserInfoRole_Log")]
    public class UserInfoRoleLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        public long UserInfo_Id_fk { get; set; }

        public long Role_Id_fk { get; set; }

        [ForeignKey("UserInfo_Id_fk")]        
        public UserInfoDto User { get; set; }

        [ForeignKey("Role_Id_fk")]
        public OrganisationRoleDto OrganisationRole { get; set; }
    }
}