﻿namespace AOM.Repository.MySql.Crm
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("crm.SystemEventNotifications")]
    public class SystemEventNotificationDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Index("UNIQUE_EMAIL_EVENT", 1, IsUnique = true)]
        public long SystemEventId { get; set; }

        [Index("UNIQUE_EMAIL_EVENT", 2, IsUnique = true)]
        public long OrganisationId_fk { get; set; }

        [ForeignKey("OrganisationId_fk")]
        public virtual OrganisationDto Organisation { get; set; }

        [Index("UNIQUE_EMAIL_EVENT", 3, IsUnique = true)]
        public long ProductId { get; set; }

        [StringLength(90)]
        [Index("UNIQUE_EMAIL_EVENT", 4, IsUnique = true)]
        public string NotificationEmailAddress { get; set; }
    }
}