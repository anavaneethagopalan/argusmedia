using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserInfo_Log")]
    public class UserInfoLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Telephone { get; set; }

        [Required]
        [StringLength(100)]
        public string Username { get; set; }
   
        public bool IsActive { get; set; }

        [Column]
        public bool IsDeleted { get; set; }

        [Column]
        public bool IsBlocked { get; set; }
        
        [Column]
        [StringLength(100)]
        public string ArgusCrmUsername { get; set; }

        public long Organisation_Id_fk { get; set; }
        [ForeignKey("Organisation_Id_fk")]
        public virtual OrganisationDto OrganisationDto { get; set; }
    }
}