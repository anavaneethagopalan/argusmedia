using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.CounterpartyPermission")]
    public class CounterpartyPermissionDto : IIntraOrganisationPermission
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [DataType(DataType.DateTime), ConcurrencyCheck]
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }
        private DateTime _lastUpdated;

        public long LastUpdatedUserId { get; set; }
        public long ProductId { get; set; }

        [Column]
        [Index("ix_counterpartypermission_1", 1)]
        [Required]
        public long OurOrganisation { get; set; }

        [Column(TypeName = "char")]
        [Index("ix_counterpartypermission_1", 2)]
        [Required]
        [StringLength(1)]
        public string BuyOrSell { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string AllowOrDeny { get; set; }

        [Column]
        [Index("ix_counterpartypermission_1", 3)]
        [Required]
        public long TheirOrganisation { get; set; }
    }
}