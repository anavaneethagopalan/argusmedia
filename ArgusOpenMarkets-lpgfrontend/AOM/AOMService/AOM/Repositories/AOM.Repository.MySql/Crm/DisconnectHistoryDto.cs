﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.DisconnectHistory")]
    public class DisconnectHistoryDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long UserId { get; set; }

        [StringLength(255)]
        public string DisconnectReason { get; set; }

        private DateTime _DateCreated;

        public DateTime DateCreated
        {
            get { return _DateCreated == null ? _DateCreated : DateTime.SpecifyKind(_DateCreated, DateTimeKind.Utc); }
            set { _DateCreated = value; }
        }
    }
}