﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserCredentials_Log")]
    public class UserCredentialsLogDto: ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        public long UserInfo_Id_fk { get; set; }
        
        [Required]
        [StringLength(100),Column("Password")]        
        public string PasswordHashed { get; set; }

        [Required]
        [StringLength(1)]
        public string CredentialType { get; set; }

        private DateTime _Expiration;
        public DateTime Expiration
        {
            get { return DateTime.SpecifyKind(_Expiration, DateTimeKind.Utc); }
            set { _Expiration = value; }
        }

        private DateTime? _DateCreated { get; set; }
        public DateTime? DateCreated
        {
            get { return _DateCreated; }
            set { _DateCreated = value; }
        }

        public bool FirstTimeLogin { get; set; }

        public int DefaultExpirationInMonths { get; set; }

        [ForeignKey("UserInfo_Id_fk")]
        public UserInfoDto User { get; set; }

    }
}