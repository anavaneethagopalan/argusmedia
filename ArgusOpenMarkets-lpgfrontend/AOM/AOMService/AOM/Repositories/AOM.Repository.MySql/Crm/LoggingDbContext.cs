using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using Utils.Logging.Utils;

namespace AOM.Repository.MySql.Crm
{
    public abstract class LoggingDbContext : DbContext
    {
        protected LoggingDbContext(string name): base(name)
        {
            //Uncomment line to log out sql queries to console from entity framework
#if DEBUG
            Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            //Database.Log = s => Log.Info(s);
#endif
        }

        public override int SaveChanges()
        {
            throw new NotImplementedException("Please use the SaveChangesAndLog Method");
        }

        public int SaveChangesAndLog(long userId = 0)
        {
            int num;

            ChangeTracker.DetectChanges();

            // store the inserted items - we can only process them after they have their ids.
            var addedEntries = ChangeTracker.Entries().Where(e => e.State == EntityState.Added).ToList();
            var modifiedEntries = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Deleted).ToList();

            // attempt to create a log entry for every update/delete.
            //foreach (var entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Deleted).ToList())
            //{
            //CreateLogEntry(entry, userId);
            //}

            num = -1;
            try
            {
                // save everything.
                num = base.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Log.Error("Error saving records in crm.LoggingDbContext: ", ex);
            }
            catch (DbEntityValidationException ex2)
            {
                Log.Error("EntityValidation Error saving records in crm.LoggingDbContext: ", ex2);
            }

            // attempt to create a log entry for every insert.
            foreach (var entry in addedEntries)
                CreateLogEntry(entry, userId);

            // attempt to create a log entry for every modify.
            foreach (var entry in modifiedEntries)
                CreateLogEntry(entry, userId);

            try
            {
                // save all created log entries.
                num += base.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Log.Error("Error saving LOG records in crm.LoggingDbContext: ", ex);
            }
            catch (DbEntityValidationException ex2)
            {
                Log.Error("EntityValidation Error saving LOG records in crm.LoggingDbContext: ", ex2);
            }

            return num;
        }

        private void CreateLogEntry(DbEntityEntry change, long userId)
        {
            var logEntry = CrmLogMapper.GetLogObject(change.Entity, userId);
            if (logEntry != null)
            {
                Set(logEntry.GetType()).Add(logEntry);
            }
        }

    }
}