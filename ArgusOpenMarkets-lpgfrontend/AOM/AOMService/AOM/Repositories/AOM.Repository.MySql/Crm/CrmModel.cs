using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public class CrmModel : LoggingDbContext, ICrmModel
    {
        public CrmModel()
            : base("CrmModel")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DoNothingInitialiser<CrmModel>());
            //Uncomment this line to output the generated sql to the output window for EF queries.
            //Database.Log = s => System.Diagnostics.Debug.WriteLine(s); 
        }

        public override int SaveChanges()
        {
            throw new NotImplementedException("Please use the SaveChangesAndLog Method");
        }

        public virtual IDbSet<OrganisationDto> Organisations { get; set; }
        public virtual IDbSet<OrganisationLogDto> OrganisationLogs { get; set; }

        public virtual IDbSet<ProductPrivilegeDto> ProductPrivileges { get; set; }
        public IDbSet<ProductPrivilegeLogDto> ProductPrivilegeLogs { get; set; }

        public virtual IDbSet<SubscribedProductDto> SubscribedProducts { get; set; }
        public virtual IDbSet<SubscribedProductLogDto> SubscribedProductLogs { get; set; }

        public virtual IDbSet<SubscribedProductPrivilegeDto> SubscribedProductPrivileges { get; set; }
        public virtual IDbSet<SubscribedProductPrivilegeLogDto> SubscribedProductPrivilegeLogs { get; set; }

        public virtual IDbSet<SystemPrivilegeDto> SystemPrivileges { get; set; }
        public virtual IDbSet<SystemPrivilegeLogDto> SystemPrivilegeLogs { get; set; }
        public virtual IDbSet<SystemRolePrivilegeDto> SystemRolePrivileges { get; set; }
        public virtual IDbSet<SystemRolePrivilegeLogDto> SystemRolePrivilegeLogs { get; set; }

        public IDbSet<CounterpartyPermissionDto> CounterpartyPermissions { get; set; }
        public IDbSet<CounterpartyPermissionLogDto> CounterpartyPermissionLogs { get; set; }
        public IDbSet<BrokerPermissionDto> BrokerPermissions { get; set; }
        public IDbSet<BrokerPermissionLogDto> BrokerPermissionLogs { get; set; }

        public virtual IDbSet<UserInfoDto> UserInfoes { get; set; } //crm //credentials // org // user
        public virtual IDbSet<UserInfoLogDto> UserInfoLogs { get; set; }
        public virtual IDbSet<UserTokenDto> UserTokens { get; set; }

        public virtual IDbSet<OrganisationRoleDto> OrganisationRoles { get; set; }
        public virtual IDbSet<OrganisationRoleLogDto> OrganisationRoleLogs { get; set; }
        public virtual IDbSet<UserInfoRoleDto> UserInfoRoles { get; set; }
        public virtual IDbSet<UserInfoRoleLogDto> UserInfoRolelogs { get; set; }
        
        public virtual IDbSet<ProductRolePrivilegeDto> ProductRolePrivileges { get; set; }
        public virtual IDbSet<ProductRolePrivilegeLogDto> ProductRolePrivilegeLogs { get; set; }

        public virtual IDbSet<AuthenticationHistoryDto> AuthenticationHistory { get; set; }
        public virtual IDbSet<DisconnectHistoryDto> DisconnectHistory { get; set; }

        public virtual IDbSet<UserLoginAttemptsDto> UserLoginAttempts { get; set; } 
        public virtual IDbSet<UserCredentialsDto> UserCredentials { get; set; }
        public virtual IDbSet<UserCredentialsLogDto> UserCredentialLogs { get; set; }
        public virtual IDbSet<UserLoginSourceDto> UserLoginSources { get; set; }
        public virtual IDbSet<UserLoginExternalAccountDto> UserLoginExternalAccounts { get; set; }
        public virtual IDbSet<UserLoginExternalAccountLogDto> UserLoginExternalAccountsLogs { get; set; }

        public virtual IDbSet<ModuleDto> Modules { get; set; }
        public virtual IDbSet<UserModuleDto> UserModules { get; set; }

        public virtual IDbSet<SystemEventNotificationDto> SystemEventNotifications { get; set; }
        public virtual IDbSet<SystemEventNotificationLogDto> SystemEventNotificationLogs { get; set; }

        public virtual IDbSet<ContentStreamDto> ContentStreams { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductPrivilegeDto>()
                .HasMany(e => e.SubscribedProductPrivilegesDtos)
                .WithRequired(e => e.ProductPrivilegeDto)
                .HasForeignKey(e => e.ProductPrivilege_Id_fk)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubscribedProductDto>()
                .HasMany(e => e.SubscribedProductPrivileges)
                .WithRequired(e => e.SubscribedProductDto)
                .HasForeignKey(e => new { e.Product_Id_fk, e.Organisation_Id_fk })
                .WillCascadeOnDelete(false);

            /* AVOID ADDING MORE DEFINITIONS HERE
          Don't add foreign key back references here unliss your code explitly needs
          it. They create complex SQL queries.
          e.g a product has a location....a location does not have products. 
          If you need products at a location then this can be achieved through querying product.location
          */
        }
    }
}
