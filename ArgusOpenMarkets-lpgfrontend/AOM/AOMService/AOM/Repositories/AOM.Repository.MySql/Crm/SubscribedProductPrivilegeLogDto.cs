using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.SubscribedProductPrivilege_Log")]
    public class SubscribedProductPrivilegeLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        public long Product_Id_fk { get; set; }

        public long Organisation_Id_fk { get; set; }

        public long ProductPrivilege_Id_fk { get; set; }

        private DateTime _DateCreated;

        public DateTime DateCreated
        {
            get
            {
                return DateTime.SpecifyKind(_DateCreated, DateTimeKind.Utc);
            }
            set
            {
                _DateCreated = value;
            }
        }

        [StringLength(45)]
        public string CreatedBy { get; set; }

        [ForeignKey("ProductPrivilege_Id_fk")]
        public virtual ProductPrivilegeDto ProductPrivilegeDto { get; set; }

        public virtual SubscribedProductDto SubscribedProductDto { get; set; }
    }
}