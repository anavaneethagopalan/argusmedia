﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
     [Table("crm.AuthenticationHistory")]
    public class AuthenticationHistoryDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime _DateCreated;
        public DateTime DateCreated
        {
            get { return _DateCreated == null ? _DateCreated : DateTime.SpecifyKind(_DateCreated, DateTimeKind.Utc); }
            set { _DateCreated = value; }
        }

        [StringLength(45)]
        public string IpAddress { get; set; }

        [StringLength(50)]
        public string Username { get; set; }

        public bool SuccessfulLogin { get; set; }

        [StringLength(255)]
        public string LoginFailureReason { get; set; }

        public long UserLoginSource_Id_fk { get; set; }
        [ForeignKey("UserLoginSource_Id_fk")]
        public virtual UserLoginSourceDto UserLoginSourceDto { get; set; }
    }
}
