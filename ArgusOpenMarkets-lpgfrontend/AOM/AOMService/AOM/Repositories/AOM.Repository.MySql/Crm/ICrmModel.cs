using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public interface ICrmModel : IDisposable
    {
        IDbSet<OrganisationDto> Organisations { get; set; }
        IDbSet<OrganisationLogDto> OrganisationLogs { get; set; }

        IDbSet<ProductPrivilegeDto> ProductPrivileges { get; set; }
        IDbSet<ProductPrivilegeLogDto> ProductPrivilegeLogs { get; set; }

        IDbSet<SubscribedProductDto> SubscribedProducts { get; set; }
        IDbSet<SubscribedProductLogDto> SubscribedProductLogs { get; set; }

        IDbSet<SubscribedProductPrivilegeDto> SubscribedProductPrivileges { get; set; }
        IDbSet<SubscribedProductPrivilegeLogDto> SubscribedProductPrivilegeLogs { get; set; }

        IDbSet<SystemPrivilegeDto> SystemPrivileges { get; set; }
        IDbSet<SystemPrivilegeLogDto> SystemPrivilegeLogs { get; set; }

        IDbSet<SystemRolePrivilegeDto> SystemRolePrivileges { get; set; }
        IDbSet<SystemRolePrivilegeLogDto> SystemRolePrivilegeLogs { get; set; }

        IDbSet<CounterpartyPermissionDto> CounterpartyPermissions { get; set; }
        IDbSet<CounterpartyPermissionLogDto> CounterpartyPermissionLogs { get; set; }

        IDbSet<BrokerPermissionDto> BrokerPermissions { get; set; }
        IDbSet<BrokerPermissionLogDto> BrokerPermissionLogs { get; set; }
        
        IDbSet<UserInfoDto> UserInfoes { get; set; }
        IDbSet<UserInfoLogDto> UserInfoLogs { get; set; }

        IDbSet<OrganisationRoleDto> OrganisationRoles { get; set; }
        IDbSet<OrganisationRoleLogDto> OrganisationRoleLogs { get; set; }

        IDbSet<UserInfoRoleDto> UserInfoRoles { get; set; }
        IDbSet<UserInfoRoleLogDto> UserInfoRolelogs { get; set; }

        IDbSet<ProductRolePrivilegeDto> ProductRolePrivileges { get; set; }
        IDbSet<ProductRolePrivilegeLogDto> ProductRolePrivilegeLogs { get; set; }

        IDbSet<UserCredentialsDto> UserCredentials { get; set; }
        IDbSet<UserCredentialsLogDto> UserCredentialLogs { get; set; }

        IDbSet<SystemEventNotificationDto> SystemEventNotifications { get; set; }
        IDbSet<SystemEventNotificationLogDto> SystemEventNotificationLogs { get; set; }

        IDbSet<ModuleDto> Modules { get; set; }
        IDbSet<UserModuleDto> UserModules { get; set; }
        IDbSet<ContentStreamDto> ContentStreams { get; set; }
        IDbSet<UserTokenDto> UserTokens { get; set; }
        IDbSet<AuthenticationHistoryDto> AuthenticationHistory { get; set; }
        IDbSet<DisconnectHistoryDto> DisconnectHistory { get; set; }

        IDbSet<UserLoginAttemptsDto> UserLoginAttempts { get; set; }
        IDbSet<UserLoginSourceDto> UserLoginSources { get; set; }
        IDbSet<UserLoginExternalAccountDto> UserLoginExternalAccounts { get; set; }
        IDbSet<UserLoginExternalAccountLogDto> UserLoginExternalAccountsLogs { get; set; }

        int SaveChangesAndLog(long userId);
    }
}