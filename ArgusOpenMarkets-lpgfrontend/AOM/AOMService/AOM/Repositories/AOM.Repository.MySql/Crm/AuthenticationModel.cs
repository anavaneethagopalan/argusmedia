﻿using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public class AuthenticationModel : LoggingDbContext, IAuthenticationModel
    {
        public AuthenticationModel()
            : base("CrmModel")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DoNothingInitialiser<AuthenticationModel>());
        }

        public IDbSet<UserCredentialsDto> UserCredentials { get; set; }
        public IDbSet<UserCredentialsLogDto> UserCredentialLogs { get; set; }
        public IDbSet<UserTokenDto> UserTokens { get; set; }
        public IDbSet<AuthenticationHistoryDto> AuthenticationHistory { get; set; }
        public IDbSet<DisconnectHistoryDto> DisconnectHistory { get; set; }
        public IDbSet<UserLoginAttemptsDto> UserLoginAttempts { get; set; }
        public IDbSet<ClientLoggingDto> ClientLogs { get; set; }
    }
}