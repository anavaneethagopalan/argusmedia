﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.SystemRolePrivilege_Log")]
    public class SystemRolePrivilegeLogDto:ILogDto
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long System_Role_Privilege_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        [Column]
        public long Role_Id_fk { get; set; }

        [Column]
        public long Privilege_Id_fk { get; set; }

        [ForeignKey("Privilege_Id_fk")]
        public SystemPrivilegeDto SystemPrivilege { get; set; }

        [ForeignKey("Role_Id_fk")]
        public OrganisationRoleDto OrganisationRole { get; set; }
    }
}