using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.ProductPrivilege")]
    public class ProductPrivilegeDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(50)]
        public string DefaultRoleGroup { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string OrganisationType { get; set; }

        public Boolean MarketMustBeOpen { get; set; }
        
        public virtual ICollection<SubscribedProductPrivilegeDto> SubscribedProductPrivilegesDtos { get; set; }
    }
}