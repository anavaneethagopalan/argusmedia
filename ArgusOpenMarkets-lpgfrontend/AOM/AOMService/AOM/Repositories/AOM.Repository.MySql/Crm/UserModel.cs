﻿namespace AOM.Repository.MySql.Crm
{
    using System;
    using System.Data.Entity;

    namespace AOM.Repository.MySql.Crm
    {
        public class UserModel : LoggingDbContext, IUserModel
        {
            public UserModel()
                : base("CrmModel")
            {
                Configuration.LazyLoadingEnabled = false;
            }

            public bool AutoDetectChangesEnabled
            {
                get { return Configuration.AutoDetectChangesEnabled; }
                set { Configuration.AutoDetectChangesEnabled = value; }
            }

            public virtual IDbSet<UserInfoDto> UserInfoes { get; set; }
            public virtual IDbSet<UserInfoLogDto> UserInfoLogs { get; set; }
            public virtual IDbSet<OrganisationDto> Organisations { get; set; }
            public virtual IDbSet<OrganisationLogDto> OrganisationLogs { get; set; }
            public virtual IDbSet<UserLoginSourceDto> UserLoginSources { get; set; }
            public virtual IDbSet<UserLoginExternalAccountDto> UserLoginExternalAccounts { get; set; }
        }

    }
}






