﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{    
    [Table("crm.ProductRolePrivilege")]
    public class ProductRolePrivilegeDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long  Id { get; set; }
   
        [Required]
        public long Role_Id_fk { get; set; }

        [Required]
        public long Organisation_Id_fk { get; set; }

        [Required]
        public long Product_Id_fk { get; set; }
    
        [Required,ForeignKey("Role_Id_fk")]
        public virtual OrganisationRoleDto OrganisationRole { get; set; }

        [Required]
        public long ProductPrivilege_Id_fk { get; set; }

        [Required, ForeignKey("ProductPrivilege_Id_fk")]
        public virtual ProductPrivilegeDto ProductPrivilege { get; set; }
    }
}
