﻿namespace AOM.Repository.MySql.Crm
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("crm.SystemRolePrivilege")]
    public class SystemRolePrivilegeDto
    {
        [Key]
        [Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long System_Role_Privilege_Id { get; set; }

        [Column]
        public long Role_Id_fk { get; set; }

        [Column]
        public long Privilege_Id_fk { get; set; }

        [ForeignKey("Privilege_Id_fk")]
        public SystemPrivilegeDto SystemPrivilege { get; set; }

        [ForeignKey("Role_Id_fk")]
        public OrganisationRoleDto OrganisationRole { get; set; }
    }
}