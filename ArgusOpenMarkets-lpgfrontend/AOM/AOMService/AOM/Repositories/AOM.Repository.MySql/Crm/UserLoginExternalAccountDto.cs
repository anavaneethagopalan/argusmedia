﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserLoginExternalAccount")]
    public class UserLoginExternalAccountDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long UserLoginSource_Id_fk { get; set; }
        [ForeignKey("UserLoginSource_Id_fk")]
        public virtual UserLoginSourceDto UserLoginSourceDto { get; set; }

        public long UserInfo_Id_fk { get; set; }
        [ForeignKey("UserInfo_Id_fk ")]
        public virtual UserInfoDto UserDto { get; set; }

        [StringLength(100)]
        public string ExternalUserCode { get; set; }

        [StringLength(100)]
        public string ExternalUserName { get; set; }

        [StringLength(100)]
        public string ExternalUserEmail { get; set; }

        [StringLength(2000)]
        public string SSOCredential { get; set; }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get { return DateTime.SpecifyKind(_dateCreated, DateTimeKind.Utc); }
            set { _dateCreated = value; }
        }
        
        public bool IsDeleted { get; set; }
    }
}