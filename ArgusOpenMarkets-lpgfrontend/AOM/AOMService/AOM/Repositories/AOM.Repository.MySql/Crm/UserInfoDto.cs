using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserInfo")]
    public class UserInfoDto
    {       
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        
        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Telephone { get; set; }

        [Required]
        [StringLength(100)]
        public string Username { get; set; }
   
        public bool IsActive { get; set; }

        [Column]
        public bool IsDeleted { get; set; }

        [Column]
        public bool IsBlocked { get; set; }
        
        [Column]
        [StringLength(100)]
        public string ArgusCrmUsername { get; set; }

        public long Organisation_Id_fk { get; set; }
        [ForeignKey("Organisation_Id_fk")]
        public virtual OrganisationDto OrganisationDto { get; set; }
    }
}
