﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.UserToken")]
    public class UserTokenDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        private DateTime _TimeCreated;
        public DateTime TimeCreated
        {
            get { return DateTime.SpecifyKind(_TimeCreated, DateTimeKind.Utc); }
            set { _TimeCreated = value; }
        }

        [StringLength(45)]
        public string SessionId { get; set; }

        [StringLength(45)]
        [Column("LoginTokenString")]
        public string LoginTokenString { get; set; }

        [StringLength(45)]
        public string SessionTokenString { get; set; }

        [StringLength(45)]
        public string UserIpAddress { get; set; }

        [Column("UserInfo_Id_fk")]
        public long UserInfo_Id_fk { get; set; }

        [ForeignKey("UserInfo_Id_fk")]
        public virtual UserInfoDto UserInfoDto { get; set; }
    }
}