﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public interface IUserModel : IDisposable
    {
        IDbSet<UserInfoDto> UserInfoes { get; set; }
        IDbSet<UserInfoLogDto> UserInfoLogs { get; set; }
        IDbSet<OrganisationDto> Organisations { get; set; }
        IDbSet<OrganisationLogDto> OrganisationLogs { get; set; }
        IDbSet<UserLoginSourceDto> UserLoginSources { get; set; }
        IDbSet<UserLoginExternalAccountDto> UserLoginExternalAccounts { get; set; }
        
        int SaveChangesAndLog(long userId);
        bool AutoDetectChangesEnabled { get; set; }
    }
}