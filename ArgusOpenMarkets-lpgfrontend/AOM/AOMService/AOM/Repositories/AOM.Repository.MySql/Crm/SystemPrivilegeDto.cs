using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.SystemPrivilege")]
    public class SystemPrivilegeDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column]
        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [Column]
        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(50)]
        public string DefaultRoleGroup { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string OrganisationType { get; set; }
    }
}