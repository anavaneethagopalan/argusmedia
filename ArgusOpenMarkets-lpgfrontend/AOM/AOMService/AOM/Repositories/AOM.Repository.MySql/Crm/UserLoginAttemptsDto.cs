﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    using System;
    using System.ComponentModel.DataAnnotations;

    [Table("UserLoginAttempts")]
    public class UserLoginAttemptsDto
    {
        [Key]
        public long Id { get; set; }

        [Required, Column("UserInfo_Id_fk")]
        public long UserId { get; set; }

        [Required]
        public long NumFailedLogins { get; set; }

        [Required]
        public DateTime LastLoginAttemptTime { get; set; }

        public long UserLoginSource_Id_fk { get; set; }
        [ForeignKey("UserLoginSource_Id_fk")]
        public virtual UserLoginSourceDto UserLoginSourceDto { get; set; }
    }
}