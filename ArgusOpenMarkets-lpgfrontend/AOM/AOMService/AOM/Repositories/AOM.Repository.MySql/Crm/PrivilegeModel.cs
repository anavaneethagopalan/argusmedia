﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public class PrivilegeModel : LoggingDbContext, IPrivilegeModel
    {
        public PrivilegeModel()
            : base("CrmModel")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DoNothingInitialiser<PrivilegeModel>());
            Database.Log = s => System.Diagnostics.Debug.WriteLine(s); 
        }

        public bool AutoDetectChangesEnabled
        {
            get { return Configuration.AutoDetectChangesEnabled; }
            set { Configuration.AutoDetectChangesEnabled = value; }
        }

        public IDbSet<UserInfoDto> UserInfoes { get; set; }
        public IDbSet<ProductPrivilegeDto> ProductPrivileges { get; set; }
        public IDbSet<ProductPrivilegeLogDto> ProductPrivilegeLogs { get; set; }
        public IDbSet<SubscribedProductDto> SubscribedProducts { get; set; }
        public IDbSet<SubscribedProductPrivilegeDto> SubscribedProductPrivileges { get; set; }
        public IDbSet<SubscribedProductPrivilegeLogDto> SubscribedProductPrivilegeLogs { get; set; }
        public IDbSet<SystemPrivilegeDto> SystemPrivileges { get; set; }
        public IDbSet<SystemPrivilegeLogDto> SystemPrivilegeLogs { get; set; }
        public IDbSet<SystemRolePrivilegeDto> SystemRolePrivileges { get; set; }
        public IDbSet<SystemRolePrivilegeLogDto> SystemRolePrivilegeLogs { get; set; }
        public IDbSet<UserInfoRoleDto> UserInfoRoles { get; set; }
        public IDbSet<UserInfoRoleLogDto> UserInfoRolelogs { get; set; }
        public IDbSet<ProductRolePrivilegeDto> ProductRolePrivileges { get; set; }
        public IDbSet<ProductRolePrivilegeLogDto> ProductRolePrivilegeLogs { get; set; }
    }
}