﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public interface IOrganisationModel : IDisposable
    {
        IDbSet<OrganisationDto> Organisations { get; set; }
        IDbSet<OrganisationLogDto> OrganisationLogs { get; set; }

        IDbSet<UserInfoDto> UserInfoes { get; set; }
        
        IDbSet<SubscribedProductDto> SubscribedProducts { get; set; }
        IDbSet<SubscribedProductLogDto> SubscribedProductLogs { get; set; }

        IDbSet<OrganisationRoleDto> OrganisationRoles { get; set; }
        IDbSet<OrganisationRoleLogDto> OrganisationRoleLogs { get; set; }

        IDbSet<CounterpartyPermissionDto> CounterpartyPermissions { get; set; }
        IDbSet<CounterpartyPermissionLogDto> CounterpartyPermissionLogs { get; set; }

        IDbSet<BrokerPermissionDto> BrokerPermissions { get; set; }
        IDbSet<BrokerPermissionLogDto> BrokerPermissionLogs { get; set; }

        IDbSet<SystemEventNotificationDto> SystemEventNotifications { get; set; } 

        int SaveChangesAndLog(long userId);
    }
}
