namespace AOM.Repository.MySql.Crm
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("crm.SubscribedProduct_Log")]
    public class SubscribedProductLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        public long Product_Id_fk { get; set; }

        public long Organisation_Id_fk { get; set; }

        [ForeignKey("Organisation_Id_fk")]
        public virtual OrganisationDto OrganisationDto { get; set; }
    }
}