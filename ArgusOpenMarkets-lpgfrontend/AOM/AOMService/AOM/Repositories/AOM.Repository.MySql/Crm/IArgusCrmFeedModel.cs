﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public interface IArgusCrmFeedModel : IDisposable
    {
        IDbSet<UserInfoDto> UserInfoes { get; set; }
        IDbSet<ModuleDto> Modules { get; set; }
        IDbSet<UserModuleDto> UserModules { get; set; }
        IDbSet<ContentStreamDto> ContentStreams { get; set; }
        int SaveChangesAndLog(long userId);
    }
}