﻿using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public class ClientLogModel : LoggingDbContext, IClientLogModel
    {
        public ClientLogModel() : base("CrmModel")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DoNothingInitialiser<AuthenticationModel>());
        }

        public IDbSet<ClientLoggingDto> ClientLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ClientLoggingDto>()
                //.Property(e => e.LoginPageLoadedClient).HasPrecision(3);
        }
    }
}