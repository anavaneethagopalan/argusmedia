namespace AOM.Repository.MySql.Crm
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("crm.ProductPrivilege_Log")]
    public class ProductPrivilegeLogDto : ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(50)]
        public string DefaultRoleGroup { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string OrganisationType { get; set; }

        public Boolean MarketMustBeOpen { get; set; }
    }
}