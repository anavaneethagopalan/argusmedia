using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.SubscribedProductPrivilege")]
    [DebuggerDisplay(
        "Product_Id_fk,Organisation_Id_fk,ProductPrivilege_Id_fk = {Product_Id_fk},{Organisation_Id_fk},{ProductPrivilege_Id_fk}"
        )]
    public class SubscribedProductPrivilegeDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Product_Id_fk { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Organisation_Id_fk { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ProductPrivilege_Id_fk { get; set; }

        private DateTime _DateCreated;

        public DateTime DateCreated
        {
            get { return DateTime.SpecifyKind(_DateCreated, DateTimeKind.Utc); }
            set { _DateCreated = value; }
        }

        [StringLength(45)]
        public string CreatedBy { get; set; }

        [ForeignKey("ProductPrivilege_Id_fk")]
        public virtual ProductPrivilegeDto ProductPrivilegeDto { get; set; }

        public virtual SubscribedProductDto SubscribedProductDto { get; set; }
    }

}