using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.CounterpartyPermission_Log")]
    public class CounterpartyPermissionLogDto: ILogDto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        [Column]
        [Required]
        public long ProductId { get; set; }

        [Column]
        [Required]
        public long OurOrganisation { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string BuyOrSell { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string AllowOrDeny { get; set; }

        [Column]
        [Required]
        public long TheirOrganisation { get; set; }
    }
}