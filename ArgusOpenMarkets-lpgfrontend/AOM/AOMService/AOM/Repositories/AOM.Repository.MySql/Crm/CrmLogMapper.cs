﻿using System.Data.Entity.Core.Objects;

namespace AOM.Repository.MySql.Crm
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    using AutoMapper;

    public static class CrmLogMapper
    {
        public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> map,
            Expression<Func<TDestination, object>> selector)
        {
            map.ForMember(selector, config => config.Ignore());
            return map;
        }

        static CrmLogMapper()
        {
            Mapper.CreateMap<BrokerPermissionDto, BrokerPermissionLogDto>();
            Mapper.CreateMap<CounterpartyPermissionDto, CounterpartyPermissionLogDto>();
            Mapper.CreateMap<OrganisationDto, OrganisationLogDto>()
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<OrganisationRoleDto, OrganisationRoleLogDto>()
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<ProductPrivilegeDto, ProductPrivilegeLogDto>()
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<ProductRolePrivilegeDto, ProductRolePrivilegeLogDto>()
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<SubscribedProductDto, SubscribedProductLogDto>()
                .Ignore(x => x.Id)
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<SubscribedProductPrivilegeDto, SubscribedProductPrivilegeLogDto>()
                .Ignore(x => x.Id)
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<SystemPrivilegeDto, SystemPrivilegeLogDto>()
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<SystemRolePrivilegeDto, SystemRolePrivilegeLogDto>()
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<UserInfoDto, UserInfoLogDto>()
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<UserInfoRoleDto, UserInfoRoleLogDto>()
                .Ignore(x => x.Id)
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<UserCredentialsDto, UserCredentialsLogDto>()
                .ForMember(x => x.PasswordHashed, y => y.MapFrom(x => "Not saved"))
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<UserLoginExternalAccountDto, UserLoginExternalAccountLogDto>()
                .Ignore(x => x.Id)
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.CreateMap<SystemEventNotificationDto, SystemEventNotificationLogDto>()
                .Ignore(x => x.LastUpdated)
                .Ignore(x => x.LastUpdatedUserId);
            Mapper.AssertConfigurationIsValid();
        }

        public static object GetLogObject<T>(T entity, long userId)
        {
            var entityType = ObjectContext.GetObjectType(entity.GetType());
            var map = Mapper.GetAllTypeMaps().FirstOrDefault(x => x.SourceType == entityType);
            if (map != null)
            {
                var dest = map.DestinationType;
                var destination = Mapper.Map(entity, entityType, dest);
                var logdto = destination as ILogDto;
                logdto.LastUpdated = logdto.LastUpdated == default(DateTime) ? DateTime.UtcNow : logdto.LastUpdated;
                logdto.LastUpdatedUserId = logdto.LastUpdatedUserId == default(long) ? userId : logdto.LastUpdatedUserId;

                return destination;
            }
            return null;
        }
    }
}