﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
     [Table("crm.UserCredentials")]
    public class UserCredentialsDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
         
        public long UserInfo_Id_fk { get; set; }
        
        [Required]
        [StringLength(100),Column("Password")]        
        public string PasswordHashed { get; set; }

        [Required]
        [StringLength(1)]
        public string CredentialType { get; set; }

        private DateTime _expiration;
        public DateTime Expiration
        {
            get { return DateTime.SpecifyKind(_expiration, DateTimeKind.Utc); }
            set { _expiration = value; }
        }

        private DateTime? _dateCreated;
        public DateTime? DateCreated
        {
            get
            {
                return _dateCreated.HasValue
                    ? DateTime.SpecifyKind((DateTime) _dateCreated, DateTimeKind.Utc)
                    : (DateTime?) null;
            }
            set { _dateCreated = value; }
        }

        public bool FirstTimeLogin { get; set; }

        public int DefaultExpirationInMonths { get; set; }

        [ForeignKey("UserInfo_Id_fk")]
        public UserInfoDto User { get; set; }

    }
}
