﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.ProductRolePrivilege_Log")]
    public class ProductRolePrivilegeLogDto:ILogDto
    {

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        private DateTime _lastUpdated;
        public DateTime LastUpdated
        {
            get { return DateTime.SpecifyKind((DateTime)_lastUpdated, DateTimeKind.Utc); }
            set { _lastUpdated = value; }
        }

        public long LastUpdatedUserId { get; set; }

        [Required]
        public long Role_Id_fk { get; set; }

        [Required]
        public long Organisation_Id_fk { get; set; }

        [Required]
        public long Product_Id_fk { get; set; }
    
        [Required,ForeignKey("Role_Id_fk")]
        public virtual OrganisationRoleDto OrganisationRole { get; set; }

        [Required]
        public long ProductPrivilege_Id_fk { get; set; }

        [Required, ForeignKey("ProductPrivilege_Id_fk")]
        public virtual ProductPrivilegeDto ProductPrivilege { get; set; }
    }
}