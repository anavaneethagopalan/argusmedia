namespace AOM.Repository.MySql.Crm
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("crm.Organisation")]
    public class OrganisationDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [StringLength(5)]
        public string ShortCode { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        public string LegalName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        private DateTime? _DateCreated;

        public DateTime? DateCreated
        {
            get
            {
                return _DateCreated == null
                    ? _DateCreated
                    : DateTime.SpecifyKind((DateTime)_DateCreated, DateTimeKind.Utc);
            }
            set
            {
                _DateCreated = value;
            }
        }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string OrganisationType { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [Column]
        public bool IsDeleted { get; set; }

    }
}