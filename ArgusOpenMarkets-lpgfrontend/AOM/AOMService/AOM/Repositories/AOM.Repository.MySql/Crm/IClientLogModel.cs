﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public interface IClientLogModel : IDisposable
    {
        IDbSet<ClientLoggingDto> ClientLogs { get; set; }

        int SaveChangesAndLog(long userId);
    }
}
