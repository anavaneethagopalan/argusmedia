using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.OrganisationRole")]
    [DebuggerDisplay("{Id},{Name}")]
    public class OrganisationRoleDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [Required]
        public long Organisation_Id_fk { get; set; }

        [ForeignKey("Organisation_Id_fk")]
        public virtual OrganisationDto OrganisationDto { get; set; }  
                     
    }
}