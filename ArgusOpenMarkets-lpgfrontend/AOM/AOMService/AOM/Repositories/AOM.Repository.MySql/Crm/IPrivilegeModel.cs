﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public interface IPrivilegeModel : IDisposable
    {
        IDbSet<UserInfoDto> UserInfoes { get; set; }
        IDbSet<ProductPrivilegeDto> ProductPrivileges { get; set; }
        IDbSet<ProductPrivilegeLogDto> ProductPrivilegeLogs { get; set; }

        IDbSet<SubscribedProductDto> SubscribedProducts { get; set; }
        IDbSet<SubscribedProductPrivilegeDto> SubscribedProductPrivileges { get; set; }
        IDbSet<SubscribedProductPrivilegeLogDto> SubscribedProductPrivilegeLogs { get; set; }

        IDbSet<SystemPrivilegeDto> SystemPrivileges { get; set; }
        IDbSet<SystemPrivilegeLogDto> SystemPrivilegeLogs { get; set; }


        IDbSet<SystemRolePrivilegeDto> SystemRolePrivileges { get; set; }
        IDbSet<SystemRolePrivilegeLogDto> SystemRolePrivilegeLogs { get; set; }

        IDbSet<UserInfoRoleDto> UserInfoRoles { get; set; }
        IDbSet<UserInfoRoleLogDto> UserInfoRolelogs { get; set; }

        IDbSet<ProductRolePrivilegeDto> ProductRolePrivileges { get; set; }
        IDbSet<ProductRolePrivilegeLogDto> ProductRolePrivilegeLogs { get; set; }

        bool AutoDetectChangesEnabled { get; set; }
    }
}