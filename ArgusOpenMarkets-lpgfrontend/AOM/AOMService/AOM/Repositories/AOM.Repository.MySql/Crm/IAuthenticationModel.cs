﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public interface IAuthenticationModel : IDisposable
    {
        IDbSet<UserCredentialsDto> UserCredentials { get; set; }
        IDbSet<UserCredentialsLogDto> UserCredentialLogs { get; set; }
        IDbSet<UserTokenDto> UserTokens { get; set; }
        IDbSet<AuthenticationHistoryDto> AuthenticationHistory { get; set; }
        IDbSet<DisconnectHistoryDto> DisconnectHistory { get; set; }
        IDbSet<UserLoginAttemptsDto> UserLoginAttempts { get; set; }

        int SaveChangesAndLog(long userId);
    }
}