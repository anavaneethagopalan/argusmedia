﻿using System;
using System.Data.Entity;

namespace AOM.Repository.MySql.Crm
{
    public class OrganisationModel : LoggingDbContext, IOrganisationModel
    {
        public OrganisationModel()
            : base("CrmModel")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DoNothingInitialiser<OrganisationModel>());
        }

        public override int SaveChanges()
        {
            throw new NotImplementedException("Please use the SaveChangesAndLog Method");
        }

        public virtual IDbSet<OrganisationDto> Organisations { get; set; }
        public virtual IDbSet<OrganisationLogDto> OrganisationLogs { get; set; }

        public virtual IDbSet<UserInfoDto> UserInfoes { get; set; }
        public virtual IDbSet<UserInfoLogDto> UserInfoLogs { get; set; }

        public virtual IDbSet<OrganisationRoleDto> OrganisationRoles { get; set; }
        public virtual IDbSet<OrganisationRoleLogDto> OrganisationRoleLogs { get; set; }

        public virtual IDbSet<CounterpartyPermissionDto> CounterpartyPermissions { get; set; }
        public virtual IDbSet<CounterpartyPermissionLogDto> CounterpartyPermissionLogs { get; set; }
        public IDbSet<BrokerPermissionDto> BrokerPermissions { get; set; }
        public IDbSet<BrokerPermissionLogDto> BrokerPermissionLogs { get; set; }

        public virtual IDbSet<SubscribedProductDto> SubscribedProducts { get; set; }
        public virtual IDbSet<SubscribedProductLogDto> SubscribedProductLogs { get; set; }

        public virtual IDbSet<SystemEventNotificationDto> SystemEventNotifications { get; set; } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SubscribedProductDto>()
                .HasMany(e => e.SubscribedProductPrivileges)
                .WithRequired(e => e.SubscribedProductDto)
                .HasForeignKey(e => new { e.Product_Id_fk, e.Organisation_Id_fk })
                .WillCascadeOnDelete(false);

            /* AVOID ADDING MORE DEFINITIONS HERE
          Don't add foreign key back references here unliss your code explitly needs it
          it. They create complex SQL queries.
          e.g a product has a location....a location does not have products. 
          If you need products at a location then this can be achied through querying product.location
          */
        }
    }

}