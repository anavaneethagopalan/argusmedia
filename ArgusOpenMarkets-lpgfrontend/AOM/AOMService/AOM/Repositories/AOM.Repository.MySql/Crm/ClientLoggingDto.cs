﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AOM.Repository.MySql.Crm
{
    [Table("crm.ClientLog")]
    public class ClientLoggingDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long UserId { get; set; }
        public long? RunJob { get; set; }

        [StringLength(1000)]
        public string Message { get; set; }

        public DateTime? LoginPageLoadedClient { get; set; }
        public DateTime? LoginButtonClickedClient { get; set; }

        public DateTime? WebSvcAuthenticatedClient { get; set; }

        public DateTime? DiffusionAuthenticatedClient { get; set; }
        public DateTime? WebSktUserJsonReceivedClient { get; set; }
        public DateTime? WebSktProdDefJsonReceivedClient { get; set; }
        public DateTime? WebSktAllJsonReceivedClient { get; set; }

        public DateTime? TransitionToDashboardClient { get; set; }
        public DateTime? DashboardRenderedClient { get; set; }
    }
}
