using System;

namespace AOM.Repository.MySql.Crm
{
    public interface ILogDto
    {
        DateTime LastUpdated { get; set; }
        long LastUpdatedUserId { get; set; }
    }
}