﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Interception;
using System.Data.Entity.SqlServer;
using System.Runtime.Remoting.Messaging;

namespace AOM.Repository.MySql
{
    public class MySqlDbConfiguration : DbConfiguration
    {
        public MySqlDbConfiguration()
        {
            //SetExecutionStrategy("System.Data.SqlClient", () => new MySqlDbExecutionStrategy(3, TimeSpan.FromSeconds(2)));
            SetExecutionStrategy("System.Data.SqlClient", () => SuspendExecutionStrategy ? (IDbExecutionStrategy)new MySqlDbExecutionStrategy(3, TimeSpan.FromSeconds(2)) : null);
            var commandInterceptor = new MySqlEFCommandInterceptor();
            DbInterception.Add(commandInterceptor);
        }

        //Usage in DB transaction: MySqlDbConfiguration.SuspendExecutionStrategy = true/false; 
        //Default is true
        public static bool SuspendExecutionStrategy
        {
            get
            {
                return (bool?)CallContext.LogicalGetData("SuspendExecutionStrategy") ?? true;
            }
            set
            {
                CallContext.LogicalSetData("SuspendExecutionStrategy", value);
            }
        }
    }
}
