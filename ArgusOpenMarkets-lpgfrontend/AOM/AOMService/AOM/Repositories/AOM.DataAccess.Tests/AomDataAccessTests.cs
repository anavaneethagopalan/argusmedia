﻿namespace AOM.DataAccess.Tests
{
	using System.Linq;

	using AOM.App.Domain;

	using Argus.Common.DataAccess;

	using NUnit.Framework;

	[TestFixture]
    public class AomDataAccessTests
	{
		private DataAccessHelper _dataAccessHelper;

		private const string ConnectionString =  "server=aominstance.co46fwzab7bh.eu-west-1.rds.amazonaws.com;user id=crm_user;password=Argu5Med1a;persistsecurityinfo=True;database=crm";
		
		[SetUp]
		public void Setup()
		{
			_dataAccessHelper = new DataAccessHelper(ConnectionString);
		}
		
		[Test]
		public void ShouldConnectToTheDatabaseAndReturnAListOfUsers()
		{
			var users = _dataAccessHelper.ExecuteSql<User>("select * from crm.UserInfo");

			Assert.That(users, Is.Not.Null);
			var numberUsers = users.ToList().Count();

			Assert.That(numberUsers, Is.GreaterThan(0));
		}

		[Test]
		public void ShouldInsertANewUser()
		{
			long nextUserId = _dataAccessHelper.ExecuteSql<long>("SELECT MAX(Id) FROM crm.UserInfo").FirstOrDefault();
			nextUserId = nextUserId + 100L;

			var user = new User { Email = "nathanbellamore@hotmail.com", Id = nextUserId, Name = "Nathan Bellamore", OrganisationId = 1 };

			var sqlParams = new { user.Id, user.Email, user.Name, Organisation_Id_fk = user.OrganisationId };
			_dataAccessHelper.ExecuteSql("Insert Into crm.UserInfo (Id, Email, Name, Organisation_Id_fk) Values(@Id, @Email, @Name, @Organisation_Id_fk)", sqlParams);


			User insertedUser = _dataAccessHelper.ExecuteSql<User>("SELECT * FROM crm.UserInfo WHERE Id=@Id", new { Id = nextUserId }).FirstOrDefault();

			long insertedUserId = 0;
			if (insertedUser != null)
			{
				insertedUserId = insertedUser.Id;
			}
			Assert.That(insertedUserId, Is.GreaterThan(0));

			_dataAccessHelper.ExecuteSql("DELETE FROM crm.UserInfo WHERE Id=@Id", new { Id = nextUserId });

		}
    }
}
