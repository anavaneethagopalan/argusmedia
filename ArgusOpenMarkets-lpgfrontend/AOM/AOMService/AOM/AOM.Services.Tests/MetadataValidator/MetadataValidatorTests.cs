﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.MetadataValidator;
using Moq;
using NUnit.Framework;
using ServiceStack.Text;

namespace AOM.Services.Tests.MetadataValidator
{
    [TestFixture]
    public class MetadataValidatorTests
    {
        private const int FakeProductId = 12345;
        private const int FakeProductIdWithNoMetaData = 12346;
        private const string FakeProductName = "FakeProduct";
        private IMetadataValidator _metadataValidator;
        private IAomModel _mockAomModel;
        private long _stringMetadataItemId;
        private long _enumMetadataItemId;

        [SetUp]
        public void SetUp()
        {
            var metaDataListElements = new List<MetaDataListItemDto>
            {
                new MetaDataListItemDto {Id = 1, ValueText = "PortA", ValueLong = 100},
                new MetaDataListItemDto {Id = 2, ValueText = "PortB", ValueLong = 101},
                new MetaDataListItemDto {Id = 3, ValueText = "PortC", ValueLong = 102}
            };

            _mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(_mockAomModel)
                .AddDeliveryLocation("DeliveryLocation1")
                .AddProduct(FakeProductName,
                    FakeProductId,
                    minPrice: 1.0m,
                    maxPrice: 100m,
                    minQty: 200m,
                    maxQty: 1000m,
                    priceIncrement: 0.1m,
                    qtyIncrement: 100m)
                .AddProduct("ProductWithoutMetaData",
                    FakeProductIdWithNoMetaData,
                    minPrice: 1.0m,
                    maxPrice: 100m,
                    minQty: 200m,
                    maxQty: 1000m,
                    priceIncrement: 0.1m,
                    qtyIncrement: 100m)
                .AddProductMetaDataList("TestingList", 1, metaDataListElements)
                .AddProductMetaDataItem(FakeProductName, "DeliveryPort", 1, MetaDataTypes.IntegerEnum, "TestingList")
                .AddProductMetaDataItem(FakeProductName, "DeliveryDescription", 1, MetaDataTypes.String, null, 5, 20)
                .DBContext.SaveChangesAndLog(-1);

            _stringMetadataItemId =
                _mockAomModel.ProductMetaDataItems.First(m => m.DisplayName == "DeliveryDescription").Id;
            _enumMetadataItemId = _mockAomModel.ProductMetaDataItems.First(m => m.DisplayName == "DeliveryPort").Id;

            var validators = typeof(IMetadataItemValidator).Assembly.GetTypes()
                .Where(t => t.IsClass && t.HasInterface(typeof(IMetadataItemValidator)))
                .Select(t => t.CreateInstance())
                .Cast<IMetadataItemValidator>()
                .ToList();

            Assert.AreEqual(3, validators.Count,
                "Expected 3 validators to be availble (increase assert value if you add more)");

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                new MockDbContextFactory(_mockAomModel, null),
                Mock.Of<IDateTimeProvider>());

            _metadataValidator = new Services.MetadataValidator.MetadataValidator(productMetadataService, validators);
        }

        [Test]
        public void MetadataContainingInvalidMetaDataIdIsRejected()
        {
            var metadata = new MetaDataStringDto
            {
                DisplayName = "DeliveryPort",
                DisplayValue = "Port C",
                ProductMetaDataId = 999
            };

            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
            StringAssert.Contains("There is no valid Product MetaData", e.Message);
        }

        [Test]
        public void MetadataWithInvalidDisplayNameIsRejected()
        {
            var metadata = new MetaDataStringDto
            {
                DisplayName = "DeliveryPort",
                DisplayValue = "normal string",
                ProductMetaDataId = _stringMetadataItemId
            };

            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
            StringAssert.Contains("The Display Name on the MetaData", e.Message);
        }

        [Test]
        public void MetadataForDifferentProductIsRejected()
        {
            var metadata = new MetaDataStringDto
            {
                DisplayName = "DeliveryDescription",
                DisplayValue = "My Desc",
                ProductMetaDataId = _stringMetadataItemId
            };

            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductIdWithNoMetaData));
            StringAssert.Contains("There is no valid Product MetaData", e.Message);
        }

        [Test]
        public void MetadataStringTooShortIsRejected()
        {
            var metadata = new MetaDataStringDto
            {
                DisplayName = "DeliveryDescription",
                DisplayValue = "Test",
                ProductMetaDataId = _stringMetadataItemId
            };

            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
            StringAssert.Contains("The Product MetaData item length is less than", e.Message);
        }

        [Test]
        public void MetadataStringTooLongIsRejected()
        {
            var metadata = new MetaDataStringDto
            {
                DisplayName = "DeliveryDescription",
                DisplayValue = "abcdeabcdeabcdeabcdeF",
                ProductMetaDataId = _stringMetadataItemId
            };

            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
            StringAssert.Contains("The Product MetaData item length is greater than", e.Message);
        }

        [Test]
        public void MetadataNormalStringIsAccepted()
        {
            var metadata = new MetaDataStringDto
            {
                DisplayName = "DeliveryDescription",
                DisplayValue = "normal string",
                ProductMetaDataId = _stringMetadataItemId
            };

            Assert.DoesNotThrow(() => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
        }

        [Test]
        public void MetadataContainingInvalidMetaDataListItemIdIsRejected()
        {
            var metadata = new MetaDataIntegerEnumDto
            {
                DisplayName = "DeliveryPort",
                DisplayValue = "PortC",
                ProductMetaDataId = _enumMetadataItemId,
                ItemValue = 102,
                Id = -999
            };

            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
            StringAssert.Contains("There is no valid MetaData list item", e.Message);
        }

        [Test]
        public void MetadataContainingInvalidItemValueIsRejected()
        {
            var metadata = new MetaDataIntegerEnumDto
            {
                DisplayName = "DeliveryPort",
                DisplayValue = "PortC",
                ProductMetaDataId = _enumMetadataItemId,
                ItemValue = 103,
                Id = 2
            };

            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
            StringAssert.Contains("There is no valid MetaData list item", e.Message);
        }

        [Test]
        public void MetadataContainingInvalidEnumDisplayValueIsRejected()
        {
            var metadata = new MetaDataIntegerEnumDto
            {
                DisplayName = "DeliveryPort",
                DisplayValue = "PortF",
                ProductMetaDataId = _enumMetadataItemId,
                ItemValue = 102,
                Id = 3
            };

            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
            StringAssert.Contains("The metadata display value", e.Message);
        }

        [Test]
        public void MetadataContainingValidItemValueAndDisplayValueIsAccepted()
        {
            var metadata = new MetaDataIntegerEnumDto
            {
                DisplayName = "DeliveryPort",
                DisplayValue = "PortB",
                ProductMetaDataId = _enumMetadataItemId,
                ItemValue = 101,
                Id = 2
            };

            Assert.DoesNotThrow(() => _metadataValidator.Validate(new MetaDataDto[] {metadata}, FakeProductId));
        }
    }
}