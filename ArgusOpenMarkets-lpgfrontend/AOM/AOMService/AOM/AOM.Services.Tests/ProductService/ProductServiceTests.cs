﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Transport.Events.Products;

using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.ProductService
{
    using Services.ProductService;
    
    [TestFixture]
    public class ProductServiceTests
    {
        [Test]
        public void CanCreateAomModel()
        {
            using (var aomModel = new AomModel())
            {
                Assert.IsNotNull(aomModel);
            }
        }

        [Test]
        public void ShouldReturnProductDefinitionList()
        {
            var productDefinitionService = new ProductService();
            var productDefinitions = productDefinitionService.GetAllProductDefinitions();

            Assert.IsNotNull(productDefinitions);
            Assert.Greater(productDefinitions.Count, 0);
        }

        [Test]
        public void ShouldReturnAlIstOfAllProducts()
        {
            var productDefinitionService = new ProductService();
            var allProducts = productDefinitionService.GetAllProducts();

            Assert.IsNotNull(allProducts);
            Assert.That(allProducts.Count, Is.GreaterThan(0));
        }

        [Test]
        public void ShouldReturnAProduct()
        {
            var productService = new ProductService();
            var allProducts = productService.GetAllProducts();
            var productId = allProducts[0].ProductId;
            var product = productService.GetProduct(productId);

            Assert.That(product, Is.Not.Null);
            Assert.That(product.ProductId, Is.EqualTo(productId));
        }

        [Test]
        public void ShouldReturnAllMarketsAndProducts()
        {
            var productService = new ProductService();
            var allMarketsAndProducts = productService.GetAllMarketsAndProducts();
            var productId = allMarketsAndProducts[0].ProductId;
            var marketId = allMarketsAndProducts[0].MarketId;

            var market = productService.GetMarkets()[0];
            var product = productService.GetProduct(productId);

            Assert.That(market, Is.Not.Null);
            Assert.That(market.Id, Is.EqualTo(marketId));
            Assert.That(product, Is.Not.Null);
            Assert.That(product.ProductId, Is.EqualTo(productId));
        }

        [Test]
        public void GetProductSettingsShouldReturnAListOfProductSettings()
        {
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct", 1, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("ProductTenor1", 1);

            var productService = new ProductService(new MockDbContextFactory(aomModel, null), mockDateTimeProvider.Object);

            var productIds = new long[1] {1};
            var productSettings = productService.GetProductSettings(productIds);

            Assert.That(productSettings, Is.Not.Null);
            Assert.That(productSettings.Count, Is.EqualTo(1));

            var firstProductSetting = productSettings[0];
            Assert.That(firstProductSetting, Is.Not.Null);
            Assert.That(firstProductSetting.ProductTenors, Is.Not.Null);
            Assert.That(firstProductSetting.ProductTenors.Count, Is.EqualTo(1));
            //Assert.That(firstProductSetting.ProductTenors[0].ParentTenor, Is.Not.Null);
            Assert.That(firstProductSetting.ProductTenors[0].ProductId, Is.EqualTo(firstProductSetting.Product.ProductId));
            //Assert.That(firstProductSetting.ProductTenors[0].TenorId, Is.EqualTo(firstProductSetting.ProductTenors[0].ParentTenor.));
        }

        [Test]
        public void ShouldReturnProductDefinitionListThatHasAtLeastOneItem()
        {
            var productDefinitionService = new ProductService();
            var productDefinitions = productDefinitionService.GetAllProductDefinitions();

            Assert.Greater(productDefinitions.Count, 0);
        }

        [Test]
        public void ShouldReturnProductDefinitionListWithOrderedCommonQuantities()
        {
            var productDefinitionService = new ProductService();
            var productDefinitions = productDefinitionService.GetAllProductDefinitions();

            Assert.IsNotNull(productDefinitions);
            Assert.Greater(productDefinitions.Count, 0);
            var anyCommonQuantities = productDefinitions.Where(pd => pd.ContractSpecification.Volume.CommonQuantityValues.Count > 0).ToList();
            Assert.Greater(anyCommonQuantities.Count, 0);
        }

        [Test]
        public void SaveProductShouldThrownABusinessRuleExceptionIfTheOrderVolumeDefaultIsLessThanOrderVolumeMinimum()
        {
            var productService = MakeProductService();
            var productDto = MakeValidProductDto();

            productDto.OrderVolumeDefault = productDto.OrderVolumeMinimum - 1;

            var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
            StringAssert.Contains("Order Volume Default cannot be less that Order Volume Minimum", exMessage);
        }

        [Test]
        public void SaveProductShouldThrownABusinessRuleExceptionIfTheOrderVolumeDefaultIsLessThanThanOrderVolumeMinimum()
        {
            var productService = MakeProductService();
            var productDto = MakeValidProductDto();

            productDto.OrderVolumeMinimum = productDto.OrderVolumeMaximum + 1;

            var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
            StringAssert.Contains("Order Volume Default cannot be less that Order Volume Minimum", exMessage);
        }

        //[Test]
        //public void SaveProductShouldThrownABusinessRuleExceptionIfNoCurrencyCode()
        //{
        //    var productService = MakeProductService();
        //    var productDto = MakeValidProductDto();

        //    //productDto.Currency = string.Empty;

        //    var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
        //    StringAssert.Contains("No Currency Code specified", exMessage);
        //}

        [Test]
        public void SaveProductShouldThrownABusinessRuleExceptionIfNoVolumeUnit()
        {
            var productService = MakeProductService();
            var productDto = MakeValidProductDto();

            productDto.VolumeUnit = string.Empty;

            var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
            StringAssert.Contains("No Volume Unit specified", exMessage);
        }

        //[Test]
        //public void SaveProductShouldThrownABusinessRuleExceptionIfNoPriceUnit()
        //{
        //    var productService = MakeProductService();
        //    var productDto = MakeValidProductDto();

        //    productDto.PriceUnit = string.Empty;

        //    var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
        //    StringAssert.Contains("No Price Unit specified", exMessage);
        //}

        [Test]
        public void SaveProductShouldThrownABusinessRuleExceptionIfNoCommodityId()
        {
            var productService = MakeProductService();
            var productDto = MakeValidProductDto();

            productDto.CommodityId = 0;

            var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
            StringAssert.Contains("No Commodity specified", exMessage);
        }


        [Test]
        public void SaveProductShouldThrownABusinessRuleExceptionIfNoMarketId()
        {
            var productService = MakeProductService();
            var productDto = MakeValidProductDto();
            productDto.MarketId = 0;

            var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
            StringAssert.Contains("No Market Specified", exMessage);
        }

        [Test]
        public void SaveProductShouldThrownABusinessRuleExceptionIfNoContractTypeId()
        {
            var productService = MakeProductService();
            var productDto = MakeValidProductDto();
            productDto.ContractTypeId = 0;

            var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
            StringAssert.Contains("No Contract Type specified", exMessage);
        }

        [Test]
        public void SaveProductShouldThrownABusinessRuleExceptionIfNoLocationId()
        {
            var productService = MakeProductService();
            var productDto = MakeValidProductDto();
            productDto.DeliveryLocations = null;

            var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
            StringAssert.Contains("No Location specified", exMessage);
        }

        [Test]
        public void SaveProductShouldThrownABusinessRuleExceptionIfProductTenorProductIdDoesNotMatchProductId()
        {
            var productService = MakeProductService();
            var productDto = MakeValidProductDto();
            var productTenorDto = MakeValidProductTenorDto(productDto);

            productTenorDto.ProductId = productTenorDto.ProductId + 1;
            productDto.ProductTenors = new Collection<ProductTenorDto> {productTenorDto};
            productDto.DeliveryLocations = null;

            var exMessage = SaveProductAndGetExceptionMessage(productService, productDto);
            StringAssert.Contains("Product Tenor - Product Id does not match Product - Product Id", exMessage);
        }

        [Test]
        public void ChangeMarketStatusShouldReturnANullProductIfTheMarketStatusHasAlreadyBeenUpdatedToTheCorrectValue()
        {
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct", 1, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("ProductTenor1", 1);

            var productService = new ProductService(new MockDbContextFactory(aomModel, null), mockDateTimeProvider.Object);

            // Setup the Product - with the Market Status of open.  
            var aomProductResponse = new AomProductResponse
            {
                Product = new Product {Status = MarketStatus.Open, ProductId = 1}
            };

            var result = productService.ChangeMarketStatus(aomModel, aomProductResponse, MarketStatus.Open);
            Assert.IsNull(result);
        }

        [Test]
        public void GetMarketStatusShouldReturnClosedIfTheOpenTimeIsNull()
        {
            var productService = new ProductService();
            var marketStatus = productService.CalculateProductStatus(null, new TimeSpan(0, 22, 0, 0));

            Assert.That(marketStatus, Is.EqualTo("C"));
        }

        [Test]
        public void GetMarketStatusShouldReturnClosedIfTheCloseTimeIsNull()
        {
            var productService = new ProductService();
            var marketStatus = productService.CalculateProductStatus(new TimeSpan(0, 10, 0, 0), null);

            Assert.That(marketStatus, Is.EqualTo("C"));
        }

        [Test]
        public void GetMarketStatusShouldReturnClosedIfTheOpenAndCloseTimesAreNull()
        {
            var productService = new ProductService();
            var marketStatus = productService.CalculateProductStatus(null, null);

            Assert.That(marketStatus, Is.EqualTo("C"));
        }

        [Test]
        public void GetMarketStatusShouldReturnOpenIfUtcTimeNowIsBetweenOpenAndCloseTimes()
        {
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var aomModel = new MockAomModel();

            mockDateTimeProvider.Setup(p => p.UtcNow).Returns(new DateTime(2015, 1, 1, 14, 01, 22));

            var productService = new ProductService(new MockDbContextFactory(aomModel, null), mockDateTimeProvider.Object);
            var marketStatus = productService.CalculateProductStatus(new TimeSpan(0, 10, 0, 0), new TimeSpan(0, 22, 0, 0));

            Assert.That(marketStatus, Is.EqualTo("O"));
        }

        [Test]
        public void GetMarketStatusShouldReturnClosedIfUtcTimeNowIsNotBetweenOpenAndCloseTimes()
        {
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var aomModel = new MockAomModel();

            mockDateTimeProvider.Setup(p => p.UtcNow).Returns(new DateTime(2015, 1, 1, 14, 01, 22));

            var productService = new ProductService(new MockDbContextFactory(aomModel, null), mockDateTimeProvider.Object);
            var marketStatus = productService.CalculateProductStatus(new TimeSpan(0, 10, 0, 0), new TimeSpan(0, 12, 0, 0));

            Assert.That(marketStatus, Is.EqualTo("C"));
        }

        [Test]
        public void ShouldReturnAListOfProductDefinitionWithEachProductContainingACoBrokeringToggleSwitch()
        {
            var productDefinitionService = new ProductService();
            var allProducts = productDefinitionService.GetAllProducts();

            Assert.IsNotNull(allProducts);
            Assert.That(allProducts.Count, Is.GreaterThan(0));

            foreach (var p in allProducts)
            {
                var coBrokering = p.CoBrokering;
                Assert.That(coBrokering, Is.Not.Null);
            }
        }

        [Test]
        [TestCase(true, true)]
        public void GetCoBrokeringAllowedShouldReturnFalseIfCoBrokeringIsNotAllowedForAProduct(bool coBrokeringValue,
            bool productCoBrokered)
        {
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct", 1, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m, marketStatus: MarketStatus.Open, 
                    minQty: 0, maxQty: 3200, qtyIncrement: null, defaultQty: 3000, coBrokering: coBrokeringValue)
                .AddTenorCode("Tenor1")
                .AddProductTenor("ProductTenor1", 1);
            
            var productService = new ProductService(new MockDbContextFactory(aomModel, null), mockDateTimeProvider.Object);
            var coBrokeringAllowed = productService.CoBrokeringAllowed(1);

            Assert.That(coBrokeringAllowed, Is.EqualTo(productCoBrokered));
        }

        private ProductTenorDto MakeValidProductTenorDto(ProductDto product)
        {
            return new ProductTenorDto {ProductId = product.Id};
        }

        private string SaveProductAndGetExceptionMessage(IProductService productService, ProductDto productDto)
        {
            string exMessage = string.Empty;
            try
            {
                productService.SaveProduct(productDto, 1);
            }
            catch (Exception ex)
            {
                exMessage = ex.Message;
            }

            return exMessage;
        }

        private IProductService MakeProductService()
        {
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var aomModel = new MockAomModel();
            var productService = new ProductService( new MockDbContextFactory(aomModel, null), mockDateTimeProvider.Object);

            return productService;
        }

        private ProductDto MakeValidProductDto()
        {
            var product = new ProductDto
            {
                Name = "PRODUCT1",
                IsDeleted = false,
                IsInternal = false,
                CommodityId = 1,
                ContractTypeId = 1,
                MarketId = 1,
                DeliveryLocations = new List<DeliveryLocationDto>() { new DeliveryLocationDto() {Id = 1}},
                //Currency = "USD",
                VolumeUnit = "MT",
                //PriceUnit = "MT",
                OrderVolumeDefault = 500,
                OrderVolumeMinimum = 100,
                OrderVolumeMaximum = 1000,
                OrderVolumeIncrement = 5,
                OrderVolumeDecimalPlaces = 2,
                OrderVolumeSignificantFigures = 10,
                OrderPriceMinimum = 100,
                OrderPriceMaximum = 1000,
                OrderPriceIncrement = 5,
                OrderPriceDecimalPlaces = 2,
                OrderPriceSignificantFigures = 10,
                BidAskStackNumberRows = 6,
                OpenTime = new TimeSpan(0, 10, 0, 0),
                CloseTime = new TimeSpan(0, 10, 0, 0),
                Status = "O",
                PurgeFrequency = 7,
                PurgeTimeOfDay = new TimeSpan(0, 10, 0, 0),
                IgnoreAutomaticMarketStatusTriggers = false,
                DisplayOptionalPriceDetail = false,
                AllowNegativePriceForExternalDeals = false
            };

            return product;
        }
    }
}