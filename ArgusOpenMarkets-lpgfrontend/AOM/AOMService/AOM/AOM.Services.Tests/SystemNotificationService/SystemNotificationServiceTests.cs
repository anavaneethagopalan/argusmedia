﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.SystemNotificationService;

namespace AOM.Services.Tests.ProductService
{
    using NUnit.Framework;

    [TestFixture]
    public class SystemNotificationServiceTests
    {
        private ISystemNotificationService _systemNotificationService;

        private MockDbContextFactory _mockDbContextFactory;

        private SystemEventNotificationDto _systemEventNotificationDto;

        [SetUp]
        public void Setup()
        {
            var model = new MockCrmModel();
            var organisation = new OrganisationDto {Id = 1, Name = "Acme", Address = "London"};
            CrmDataBuilder.WithModel(model)
                .AddSystemEventNotification("nathan.bellamore@argusmedia.com", 1, organisation,
                    SystemEventType.DealConfirmation,
                    out _systemEventNotificationDto);

            _mockDbContextFactory = new MockDbContextFactory(null, model);

            _systemNotificationService = new SystemNotificationService.SystemNotificationService(_mockDbContextFactory);
        }

        [Test]
        public void ShouldReturnASystemNotificationIfItExists()
        {
            var systemNotifications = _systemNotificationService.GetSystemNotification(_systemEventNotificationDto.Id);
            Assert.That(systemNotifications, Is.Not.Null);
            Assert.That(systemNotifications.Id, Is.EqualTo(_systemEventNotificationDto.Id));
        }

        [Test]
        public void GetSystemNotificationShouldReturnNullIfTheNotificationDoesNotExist()
        {
            var systemNotifications = _systemNotificationService.GetSystemNotification(1000000);
            Assert.That(systemNotifications, Is.Null);
        }

        [Test]
        public void GetSystemNotificationByNotificationDetailsShouldReturnNotification()
        {
            var result = _systemNotificationService.GetSystemNotification(
                _systemEventNotificationDto.OrganisationId_fk, _systemEventNotificationDto.ProductId,
                _systemEventNotificationDto.NotificationEmailAddress, SystemEventType.DealConfirmation);

            Assert.That(result, Is.Not.Null);
        }
    }
}