﻿using AOM.Repository.MySql;

namespace AOM.Services.Tests.DealService
{
    using System;
    using System.Linq;

    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Mappers;
    using AOM.App.Domain;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql.Aom;
    using AOM.Repository.MySql.Tests.Aom;
    using AOM.Services.DealService;
    using AOM.Transport.Events;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class DealServiceEditDealTests
    {
        private const int TestUserId = 666;
        private readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();
        private Mock<IUserService> _mockUserService;
        private Mock<IOrganisationService> _mockOrganisationService;
        private IAomModel _mockAomModel;

        public void AssertErrorMessageContains(string errorMsg, string expectedContains)
        {
            Assert.IsTrue(errorMsg.Contains(expectedContains), string.Format("Expect error message to contain '{0}' but it was: '{1}'", expectedContains, errorMsg));
        }

        [SetUp]
        public void Setup()
        {
            _mockUserService = new Mock<IUserService>();
            _mockOrganisationService = new Mock<IOrganisationService>();
            _mockUserService.Setup(x => x.GetUsersOrganisation(TestUserId)).Returns(new Organisation { Name = "TestingOrg", ShortCode = "XXX" });

            _mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(_mockAomModel)
                .AddDealStatus("E", "Executed")
                .AddDealStatus("V", "Void")
                .DBContext.SaveChangesAndLog(-1);
        }


        [Test]
        public void CanEditDealRunningAgainstDatabase()
        {
            using (var transaction = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                using (var aomModel = new MockAomModel())
                {
                    OrderDto testOrderDto1, testOrderDto2;
                    DealDto testDealDto;

                    //ARRANGE
                    AomDataBuilder.WithModel(aomModel)
                        .OpenAllMarkets()
                        .AddDeliveryLocation("Farringdon")
                        .AddProduct("Argus Naphtha CIF NWE - Outright", 1)
                        .AddTenorCode("P")
                        .AddProductTenor("Product1_Tenor1")
                        .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto1)
                        .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto2)
                        .AddDeal(TestUserId, testOrderDto1.Id, testOrderDto2.Id, out testDealDto, "P")
                        .DBContext.SaveChangesAndLog(-1);

                    // Set up changes                
                    var testDeal1 = new Deal
                    {
                        Id = testDealDto.Id,
                        DealStatus = DealStatus.Pending,
                        InitialOrderId = testDealDto.InitialOrderId,
                        MatchingOrderId = testDealDto.MatchingOrderId,
                        LastUpdated = testDealDto.LastUpdated,
                        Initial = new Order { ProductId = 1, }
                    };

                    IDealService dealService = new DealService(_mockUserService.Object, _mockOrganisationService.Object, _dateTimeProvider);

                    //ACT
                    Message serviceReturns = dealService.EditDeal(aomModel, testDeal1);

                    // ASSERT
                    Assert.AreEqual(MessageType.Deal, serviceReturns.MessageType, (serviceReturns.MessageBody.ToString()));

                    var returnedDeal = (Deal)serviceReturns.MessageBody;

                    Assert.AreEqual(DealStatus.Pending, returnedDeal.DealStatus);
                    Assert.AreEqual(testDealDto.InitialOrderId, returnedDeal.InitialOrderId);
                    Assert.AreEqual(testDealDto.MatchingOrderId, returnedDeal.MatchingOrderId);

                    transaction.Dispose();
                }
            }
        }

        [Test]
        public void CanEditDealAgainstMockDatabase()
        {
            OrderDto testOrderDto1, testOrderDto2;
            DealDto testDealDto;

            //ARRANGE
            AomDataBuilder.WithModel(_mockAomModel)
                .AddDeliveryLocation("Farringdon")
                .AddProduct("Prod1", 345)
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1")
                .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto1)
                .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto2)
                .AddDeal(TestUserId, testOrderDto1.Id, testOrderDto2.Id, out testDealDto, "P")
                .DBContext.SaveChangesAndLog(-1);
            
            // Set up changes                
            var testDeal1 = new Deal
            {
                Id = testDealDto.Id,
                DealStatus = DealStatus.Pending,
                InitialOrderId = testDealDto.InitialOrderId,
                MatchingOrderId = testDealDto.MatchingOrderId,
                LastUpdated = testDealDto.LastUpdated,
                Initial = new Order { ProductId = 345 }
            };

            IDealService dealService = new DealService(_mockUserService.Object, _mockOrganisationService.Object, _dateTimeProvider);

            //ACT
            Message serviceReturns = dealService.EditDeal(_mockAomModel, testDeal1);

            // ASSERT
            Assert.AreEqual(MessageType.Deal, serviceReturns.MessageType, (serviceReturns.MessageBody.ToString()));

            var returnedDeal = serviceReturns.MessageBody as Deal;

            Assert.AreEqual(DealStatus.Pending, returnedDeal.DealStatus);
            Assert.AreEqual(testDealDto.InitialOrderId, returnedDeal.InitialOrderId);
            Assert.AreEqual(testDealDto.MatchingOrderId, returnedDeal.MatchingOrderId);
        }

        [Test]
        public void CantEditNonExistentDeal()
        {
            OrderDto testOrderDto1, testOrderDto2;
            DealDto testDealDto;

            //ARRANGE

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddDeliveryLocation("Farringdon")
                .AddProduct("Argus Naphtha CIF NWE - Outright", 1)
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1")
                .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto1)
                .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto2)
                .AddDeal(TestUserId, testOrderDto1.Id, testOrderDto2.Id, out testDealDto, "P")
                .DBContext.SaveChangesAndLog(-1);

            // Set up changes                
            var testDeal1 = new Deal
            {
                Id = -232323,
                DealStatus = DealStatus.Pending,
                InitialOrderId = 1,
                MatchingOrderId = 2,
                LastUpdated = testDealDto.LastUpdated,
                Initial = new Order { ProductId = _mockAomModel.Products.FirstOrDefault().Id, }
            };

            IDealService dealService = new DealService(_mockUserService.Object, _mockOrganisationService.Object, _dateTimeProvider);
            Assert.Catch(typeof(InvalidOperationException), () => dealService.EditDeal(_mockAomModel, testDeal1));
        }

        [Test]
        public void CantEditLastUpdateChanged()
        {
            OrderDto testOrderDto1, testOrderDto2;
            DealDto testDealDto;

            //ARRANGE
            AomDataBuilder.WithModel(_mockAomModel)
                .AddDeliveryLocation("Farringdon")
                .AddProduct("Argus Naphtha CIF NWE - Outright", 1)
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1")
                .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto1)
                .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto2)
                .AddDeal(TestUserId, testOrderDto1.Id, testOrderDto2.Id, out testDealDto, "P")
                .OpenAllMarkets()
                .DBContext.SaveChangesAndLog(-1);

            // Set up changes                
            var testDeal1 = new Deal
            {
                Id = testDealDto.Id,
                DealStatus = DealStatus.Pending,
                InitialOrderId = testOrderDto1.Id,
                MatchingOrderId = testOrderDto2.Id,
                LastUpdated = _dateTimeProvider.UtcNow.AddMilliseconds(1),
                Initial = new Order { ProductId = _mockAomModel.Products.FirstOrDefault().Id, }
            };

            IDealService dealService = new DealService(_mockUserService.Object, _mockOrganisationService.Object, _dateTimeProvider);

            //ACT
            var e = Assert.Catch(typeof(BusinessRuleException), () => dealService.EditDeal(_mockAomModel, testDeal1));

            // ASSERT
            AssertErrorMessageContains(e.Message, "changed by someone else");
        }

        [Test]
        public void CantChangeAVoidDeal()
        {
            DealDto testDealDto;

            //ARRANGE
            AomDataBuilder.WithModel(_mockAomModel)
                .AddDeliveryLocation("Farringdon")
                .AddProduct("Argus Naphtha CIF NWE - Outright", 234)
                .AddDeal(TestUserId, 1, 2, out testDealDto, "V")
                .DBContext.SaveChangesAndLog(-1);

            // Set up changes                
            var testDeal1 = new Deal
            {
                Id = testDealDto.Id,
                DealStatus = DealStatus.Pending,
                InitialOrderId = 1,
                MatchingOrderId = 2,
                LastUpdated = testDealDto.LastUpdated,
                Initial = new Order { ProductId = 234 }
            };

            IDealService dealService = new DealService(_mockUserService.Object, _mockOrganisationService.Object, _dateTimeProvider);

            //ACT
            var e = Assert.Catch(typeof(BusinessRuleException), () => dealService.EditDeal(_mockAomModel, testDeal1));
            AssertErrorMessageContains(e.Message, "The deal is void and cannot be amended.");
        }

        [Test]
        [Description("AR-928 Editor must be able to void executed deals")]
        [TestCase(MarketStatus.Open, Description = "Market open")]
        [TestCase(MarketStatus.Closed, Description = "Market closed")]
        public void CanVoidExecutedDeal(MarketStatus marketStatus)
        {
            DealDto testDealDto;

            //ARRANGE
            AomDataBuilder.WithModel(_mockAomModel)
                .AddProduct("Argus Naphtha CIF NWE - Outright", 234, marketStatus)
                .AddDeal(TestUserId, 1, 2, out testDealDto, "E")
                .DBContext.SaveChangesAndLog(-1);

            IDealService dealService = new DealService(_mockUserService.Object,  _mockOrganisationService.Object, _dateTimeProvider);

            var testDeal = testDealDto.ToEntity(_mockOrganisationService.Object, _mockUserService.Object);

            // ACT
            var updated = dealService.VoidDeal(_mockAomModel, testDeal);

            // ASSERT
            Assert.AreEqual(MessageAction.Void, updated.MessageAction);
            var updatedDeal = updated.MessageBody as Deal;
            Assert.IsNotNull(updatedDeal);
            Assert.AreEqual(testDeal.Id, updatedDeal.Id);
            Assert.AreEqual(DealStatus.Void, updatedDeal.DealStatus);
        }

    }
}