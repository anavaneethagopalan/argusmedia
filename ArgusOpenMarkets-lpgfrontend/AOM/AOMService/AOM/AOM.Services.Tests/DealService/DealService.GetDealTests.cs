﻿using System.Transactions;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Transport.Events.Deals;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.DealService
{
    [TestFixture]
    public class DealServiceGetDealTests
    {
        private const int TestUserId = 666;
        Mock<IUserService> _mockUserService;
        readonly Organisation _testOrganisation = new Organisation {Name = "TestingOrg", ShortCode = "XXX"};
        private Mock<IOrganisationService> _mockOrgService;
        DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        [SetUp]
        public void Setup()
        {
            _mockUserService = new Mock<IUserService>();
            _mockOrgService = new Mock<IOrganisationService>();
            _mockUserService.Setup(x => x.GetUsersOrganisation(It.IsAny<long>())).Returns(_testOrganisation);
        }

        [Test]
        public void AssertGetDealByIdReturnsNonNullForValidDealAndLoadsOrganisation()
        {
            using (var trans = new TransactionScope())
            {
                using (var aomModel = new MockAomModel())
                {
                    OrderDto testOrderDto1, testOrderDto2;
                    DealDto testDealDto;

                    //ARRANGE                    
                    AomDataBuilder.WithModel(aomModel)
                        .AddDeliveryLocation("Farringdon")
                        .AddProduct("Product1", 1)
                        .AddTenorCode("Tenor1")
                        .AddProductTenor("Product1_Tenor1")
                        .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto1)
                        .AddOrder(TestUserId, "Product1_Tenor1", "A", out testOrderDto2)
                        .AddDeal(TestUserId, testOrderDto1.Id, testOrderDto2.Id, out testDealDto)
                        .DBContext.SaveChangesAndLog(-1);

                    //ACT
                    var resp = new AomDealAuthenticationResponse
                    {
                        Deal = new Deal {Id = testDealDto.Id}
                    };
                    var deal = new Services.DealService.DealService(_mockUserService.Object, _mockOrgService.Object, _dateTimeProvider).GetDealById(aomModel, resp);

                    //ASSERT
                    Assert.That(deal, Is.Not.Null);
                    Assert.AreEqual(testDealDto.Id, deal.Id);
                    Assert.AreEqual(testOrderDto1.Id, deal.InitialOrderId);
                    Assert.AreEqual(testOrderDto2.Id, deal.MatchingOrderId);
                    Assert.AreEqual(_testOrganisation.Name, deal.Initial.PrincipalOrganisationName);
                }

                trans.Dispose();
            }
        }
    }
}