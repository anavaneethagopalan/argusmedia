﻿using System;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Transport.Events;
using AOM.Transport.Events.ExternalDeals;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.ExternalDealService
{
    internal static class TestShared
    {
        public const int FakeUserId = 5150;
        public const int FakeOrganisationId = 69;
        public const int FakeOutrightProductId = 1;
        public const int FakeOtherProductId = 3;
        public const string FakeDeliveryLocation = "Farringdon";
        public const string FakeContractInput = "Chicken Cottage";
        public const string FakeProductTenor = "Product1_Tenor1";

        public static ExternalDeal CreateFakeExternalDeal(decimal? fakePrice = 99, decimal? fakeQuantity = 100, bool asVerfied = true, long fakeProductId = -1)
        {
            return new ExternalDeal
            {
                DealStatus = DealStatus.Pending,
                DeliveryLocation = TestShared.FakeDeliveryLocation,
                AsVerified = asVerfied,
                ProductId = fakeProductId,
                Price = fakePrice,
                Quantity = fakeQuantity,
                LastUpdatedUserId = TestShared.FakeUserId
            };
        }
        
        public static AomExternalDealAuthenticationResponse CreateFakeResponse(ExternalDeal fakeExternalDeal, bool marketMustBeOpen = false, Message fakeMessage = null)
        {
            var message = fakeMessage ?? new Message
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    UserId = FakeUserId,
                    SessionId = "FakeSessionId",
                    OrganisationId = FakeOrganisationId,
                },
                MessageAction = MessageAction.Create,
                MessageType = MessageType.ExternalDeal,
                MessageBody = fakeExternalDeal
            };

            return new AomExternalDealAuthenticationResponse
            {
                ExternalDeal = fakeExternalDeal,
                MarketMustBeOpen = marketMustBeOpen,
                Message = message
            };
        }

        public static AomExternalDealAuthenticationResponse CreateFakeResponse(ExternalDealDto fakeExternalDealDto, ICrmAdministrationService crmService, 
            bool marketMustBeOpen = false, Message fakeMessage = null)
        {
            return CreateFakeResponse(fakeExternalDealDto.ToEntity(crmService), marketMustBeOpen, fakeMessage);
        }

        public static void SetupCrmService(Mock<IUserService> mockCrmService)
        {
            mockCrmService.Setup(x => x.GetUsersOrganisation(FakeUserId)).Returns(new Organisation
            {
                Name = "TestingOrg",
                ShortCode = "XXX",
                Id = FakeOrganisationId
            });
        }

        public static void AddDealStatuses(IAomModel aomModel)
        {
            AomDataBuilder.WithModel(aomModel)
                          .AddDealStatus("E", "Executed")
                          .AddDealStatus("P", "Pending")
                          .AddDealStatus("V", "Void")
                          .DBContext.SaveChangesAndLog(-1);
        }

        public static void AddProducts(IAomModel aomModel)
        {
            AomDataBuilder.WithModel(aomModel)
                          .AddDeliveryLocation(FakeDeliveryLocation)
                          .AddProduct("Argus Naphtha CIF NWE - Outright", FakeOutrightProductId)
                          .AddProduct("Other", FakeOtherProductId)
                          .AddTenorCode("Tenor1")
                          .AddProductTenor(FakeProductTenor);
        }

        public static void AssertExceptionMessageContains(string expected, string message)
        {
            Assert.IsTrue(message.ToUpper().Contains(expected.ToUpper()), String.Format("Expected error message to contain {0} but was {1}", expected, message));
        }
    }
}
