﻿using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.ExternalDealService;
using AOM.Services.MetadataValidator;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Services.ProductMetadataService;

namespace AOM.Services.Tests.ExternalDealService
{
    [TestFixture]
    public class EditExternalDealTests
    {
        private Mock<ICrmAdministrationService> _mockCrmAdminService;
        private Mock<IUserService> _mockUserService;
        private readonly IDateTimeProvider _dateTimeProvider = new DateTimeProvider();
        private IAomModel _mockAomModel;
        private static readonly DateTime FakeDealTime = new DateTime(2014, 11, 12);
        private IExternalDealService _externalDealService;


        [SetUp]
        public void Setup()
        {
            _mockCrmAdminService = new Mock<ICrmAdministrationService>();
            _mockUserService = new Mock<IUserService>();
            TestShared.SetupCrmService(_mockUserService);

            _mockAomModel = new MockAomModel();
            TestShared.AddDealStatuses(_mockAomModel);
            TestShared.AddProducts(_mockAomModel);

            _externalDealService = new Services.ExternalDealService.ExternalDealService(_mockCrmAdminService.Object,_dateTimeProvider,
                new Services.MetadataValidator.MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));
        }

        [Test]
        public void TestThatEditedDealIsUpdatedToNewValues()
        {
            var fakeExternalDealDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOutrightProductId).ToDto();
            var fakeUpdatedDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOutrightProductId).ToDto();
            PriceLineDto priceLineDto = null;

            const decimal fakePrice = 123.45m;
            const decimal fakeQty = 12345m;

            AomDataBuilder.WithModel(_mockAomModel)
                .AddPriceLine(out priceLineDto, 111)
                .AddPriceLineBasis(fakePrice, priceLineDto, TestShared.FakeOutrightProductId, 1)
                .AddExternalDeal(fakeExternalDealDto, priceLineDto);

            fakeUpdatedDto.Price = fakePrice;
            fakeUpdatedDto.Quantity = fakeQty;
            var fakeResponse = TestShared.CreateFakeResponse(fakeUpdatedDto, _mockCrmAdminService.Object);

            var updatedDeal = _externalDealService.EditExternalDeal(_mockAomModel, fakeResponse);
            Assert.AreEqual(fakeUpdatedDto.Price, updatedDeal.Price, "Price should have been updated");
            Assert.AreEqual(fakeUpdatedDto.Quantity, updatedDeal.Quantity, "Quantity should have been updated");
        }

        [Test]
        public void TestThatEditedDealDtosAreUpdatedToNewValues()
        {
            DateTime dateTime = DateTime.Now.ToUniversalTime();
            ExternalDealDto externalDealDto2;

            //using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            //{
            //    using (AomModel aomModel = new AomModel())
            //    {
                    ExternalDealDto externalDealDto = TestShared.CreateFakeExternalDeal(new decimal?(123.45m), new decimal?(12345m), false, 1).ToDto();
                    PriceLineDto priceLineDto = null;
                    PriceBasisDto priceBasisDto;

                    AomDataBuilder.WithModel(_mockAomModel)
                        .AddCurrency("EUR", "Eur")
                        .AddUnit("BBL", "bbl")
                        .AddTenorCode("P")
                        .AddPriceType("FIXED")
                        .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                        .AddPriceLine(out priceLineDto, TestShared.FakeUserId)
                        .AddPriceLineBasis(123.45m, priceLineDto, 1L, 1, "P", priceBasisDto.Id)
                        .AddExternalDeal(externalDealDto, priceLineDto)
                        .DBContext.SaveChangesAndLog(-1);

                    //externalDealDto2 = aomModel.ExternalDeals.Last(ed => ed.LastUpdatedUserId == TestShared.FakeUserId);
                    externalDealDto2 = _mockAomModel.ExternalDeals.OrderByDescending(ed => ed.LastUpdated).First(ed => ed.LastUpdatedUserId == TestShared.FakeUserId);
                    externalDealDto2.Price = 200;
                    _externalDealService.EditExternalDeal(_mockAomModel, TestShared.CreateFakeResponse(externalDealDto2, _mockCrmAdminService.Object));
            //    }
            //}
            Assert.IsNotNull(externalDealDto2.PriceLineDto);
            Assert.That(externalDealDto2.PriceLineDto.PriceLinesBases.Count(), Is.EqualTo(1));
            Assert.AreEqual(externalDealDto2.Price, 200);
            Assert.AreEqual(externalDealDto2.Price, externalDealDto2.PriceLineDto.PriceLinesBases.First().PriceLineBasisValue);
            Assert.IsNotNull(externalDealDto2.LastUpdated);
            Assert.IsNotNull(externalDealDto2.DateCreated);
            Assert.IsNotNull(externalDealDto2.PriceLineDto.PriceLinesBases.First().LastUpdated);
            Assert.IsNotNull(externalDealDto2.PriceLineDto.PriceLinesBases.First().DateCreated);
        }

        [Test]
        public void TestVoidExternalDealSetsStatusToVoid()
        {
            var fakeExternalDealDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOutrightProductId).ToDto();
            AomDataBuilder.WithModel(_mockAomModel)
                .AddExternalDeal(fakeExternalDealDto);

            var fakeResponse = TestShared.CreateFakeResponse(fakeExternalDealDto, _mockCrmAdminService.Object);
            var updatedDeal = _externalDealService.VoidExternalDeal(_mockAomModel, fakeResponse);

            Assert.AreEqual(DealStatus.Void, updatedDeal.DealStatus, "Deal status should have been set to Void");
        }

        [Test]
        public void TestVerifyExternalDealSetsStatusToExecuted()
        {
            var fakeExternalDealDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOutrightProductId).ToDto();
            AomDataBuilder.WithModel(_mockAomModel)
                .AddExternalDeal(fakeExternalDealDto);

            var fakeResponse = TestShared.CreateFakeResponse(fakeExternalDealDto, _mockCrmAdminService.Object);
            var updatedDeal = _externalDealService.VerifyExternalDeal(_mockAomModel, fakeResponse);

            Assert.AreEqual(DealStatus.Executed, updatedDeal.DealStatus, "Deal status should have been set to Executed");
        }

        [Test]
        public void TestThatVoidDealCannotBeUpdated()
        {
            // Arrange
            var fakeExternalDealDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOutrightProductId).ToDto();
            fakeExternalDealDto.DealStatus = DtoMappingAttribute.GetValueFromEnum(DealStatus.Void);
            AomDataBuilder.WithModel(_mockAomModel)
                .AddExternalDeal(fakeExternalDealDto);

            var fakeResponse = TestShared.CreateFakeResponse(fakeExternalDealDto, _mockCrmAdminService.Object);

            // Act and assert
            var e = Assert.Catch(typeof(BusinessRuleException), () => _externalDealService.VerifyExternalDeal(_mockAomModel, fakeResponse));
            TestShared.AssertExceptionMessageContains("deal is void", e.Message);
        }

        [Test]
        public void TestThatAlreadyUpdatedDealCannotBeUpdatedByInFlightUpdate()
        {
            // Arrange
            var fakeExternalDealDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOutrightProductId).ToDto();
            var fakeUpdatedDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOutrightProductId).ToDto();

            AomDataBuilder.WithModel(_mockAomModel)
                .AddExternalDeal(fakeExternalDealDto);

            const decimal fakePrice = 123.45m;
            const decimal fakeQty = 12345m;
            fakeUpdatedDto.Price = fakePrice;
            fakeUpdatedDto.Quantity = fakeQty;
            fakeUpdatedDto.LastUpdated = fakeExternalDealDto.LastUpdated.AddMinutes(1); //Would actually happen via DB timestamp

            var fakeResponse = TestShared.CreateFakeResponse(fakeUpdatedDto, _mockCrmAdminService.Object);

            // Act and assert
            var e = Assert.Catch(typeof(BusinessRuleException), () => _externalDealService.VerifyExternalDeal(_mockAomModel, fakeResponse));
            TestShared.AssertExceptionMessageContains("been updated", e.Message);
        }

        [Test]
        [TestCase(null, Description = "null contract")]
        [TestCase("", Description = "empty string for contract")]
        [TestCase(" ", Description = "single space for contract")]
        [TestCase("    ", Description = "multiple spaces for contract")]
        public void TestThatADealForOtherCannotHaveContractUpdatedToEmptyValue(string newContract)
        {
            // Arrange
            var fakeExternalDealDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOtherProductId).ToDto();
            var fakeUpdatedDto = TestShared.CreateFakeExternalDeal(fakeProductId: TestShared.FakeOtherProductId).ToDto();

            AomDataBuilder.WithModel(_mockAomModel)
                .AddExternalDeal(fakeExternalDealDto);

            fakeUpdatedDto.ContractInput = newContract;
            var fakeResponse = TestShared.CreateFakeResponse(fakeUpdatedDto, _mockCrmAdminService.Object);

            // Act and assert
            var e = Assert.Catch(typeof(BusinessRuleException), () => _externalDealService.VerifyExternalDeal(_mockAomModel, fakeResponse));
            TestShared.AssertExceptionMessageContains(ErrorMessages.OtherProductMustSpecifyContract, e.Message);
        }
    }
}