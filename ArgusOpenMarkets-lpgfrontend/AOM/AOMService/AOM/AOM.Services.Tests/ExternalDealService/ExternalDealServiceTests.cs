﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.ExternalDealService;
using AOM.Services.MetadataValidator;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using AOM.Services.ProductMetadataService;

namespace AOM.Services.Tests.ExternalDealService
{
    [TestFixture]
    public class ExternalDealServiceTests
    {
        private MockAomModel _mockAomModel;

        private Mock<IDateTimeProvider> _mockDateTimeProvider;

        private Mock<ICrmAdministrationService> _mockCrmAdminService;

        private IExternalDealService _externalDealService;

        [SetUp]
        public void Setup()
        {
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockCrmAdminService = new Mock<ICrmAdministrationService>();

            var productMetadataService = new Mock<IProductMetadataService>();

            _externalDealService = new Services.ExternalDealService.ExternalDealService(
                _mockCrmAdminService.Object,
                _mockDateTimeProvider.Object,
                new Services.MetadataValidator.MetadataValidator(productMetadataService.Object,
                    new IMetadataItemValidator[0]));
        }

        [Test]
        public void CallingGetExternalDealByIdShouldReturnValidExternalDeal()
        {
            List<MetaDataDto> metaDataList = new List<MetaDataDto>
            {
                new MetaDataStringDto
                {
                    DisplayName = "DisplayName",
                    DisplayValue = "DisplayValue",
                    ProductMetaDataId = 1
                }
            };
            var externalDealDto = new ExternalDealDto {Id = 1, MetaData = metaDataList.ToArray()};

            _mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(_mockAomModel)
                .AddExternalDeal(externalDealDto);

            var externalDealEntity = _externalDealService.GetExternalDealById(_mockAomModel, 1);

            Assert.That(externalDealEntity, Is.Not.Null);

        }

        [Test]
        public void CallingGetExternalDealByIdShouldReturnValidExternalDealWithMetaData()
        {
            List<MetaDataDto> metaDataList = new List<MetaDataDto>
            {
                new MetaDataStringDto
                {
                    DisplayName = "DisplayName_02",
                    DisplayValue = "DisplayValue_02",
                    ProductMetaDataId = 1
                }
            };
            var externalDealDto = new ExternalDealDto {Id = 1, MetaData = metaDataList.ToArray()};

            _mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(_mockAomModel)
                .AddExternalDeal(externalDealDto);

            var externalDealEntity = _externalDealService.GetExternalDealById(_mockAomModel, 1);

            Assert.That(externalDealEntity, Is.Not.Null);
            Assert.That(externalDealEntity.MetaData[0].DisplayName, Is.EqualTo("DisplayName_02"));
            Assert.That(externalDealEntity.MetaData[0].DisplayValue, Is.EqualTo("DisplayValue_02"));
        }
    }
}