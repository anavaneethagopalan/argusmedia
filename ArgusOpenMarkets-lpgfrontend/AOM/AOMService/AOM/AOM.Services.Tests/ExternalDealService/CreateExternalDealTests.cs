﻿using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.ExternalDealService;
using AOM.Services.MetadataValidator;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Services.ProductMetadataService;
using AOM.Transport.Events.ExternalDeals;

namespace AOM.Services.Tests.ExternalDealService
{
    [TestFixture]
    public class CreateExternalDealTests
    {
        private Mock<ICrmAdministrationService> _mockCrmAdminService;
        private Mock<IUserService> _mockUserService;
        private readonly IDateTimeProvider _dateTimeProvider = new DateTimeProvider();
        private IAomModel _mockAomModel;
        private IExternalDealService _externalDealService;

        [SetUp]
        public void Setup()
        {
            _mockCrmAdminService = new Mock<ICrmAdministrationService>();
            _mockUserService = new Mock<IUserService>();
            TestShared.SetupCrmService(_mockUserService);

            _mockAomModel = new MockAomModel();
            TestShared.AddDealStatuses(_mockAomModel);
            TestShared.AddProducts(_mockAomModel);

            _externalDealService = new Services.ExternalDealService.ExternalDealService(_mockCrmAdminService.Object, _dateTimeProvider,
                new Services.MetadataValidator.MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));
        }

        [Test]
        public void CanCreateExternalDealForRegularProduct()
        {
            const decimal fakePrice = 123.45m;
            const decimal fakeQty = 12345m;

            var fakeDeal = TestShared.CreateFakeExternalDeal(fakePrice, fakeQty, false, TestShared.FakeOutrightProductId);
            var fakeResponse = TestShared.CreateFakeResponse(fakeDeal);

            // Act
            var createdDeal = _externalDealService.CreateExternalDeal(_mockAomModel, fakeResponse);

            // Assert
            //Assert.AreNotEqual(new DateTime(), createdDeal.LastUpdated, "Last updated time for the deal should have been set");
            Assert.AreEqual(TestShared.FakeUserId, createdDeal.LastUpdatedUserId, "Last updated user ID should have been set to session user");
            Assert.AreEqual(TestShared.FakeOrganisationId, createdDeal.ReporterId, "Reporter ID should have been set to session company");
            Assert.AreEqual(fakePrice, createdDeal.Price, "Deal price should be as set originally");
            Assert.AreEqual(fakeQty, createdDeal.Quantity, "Deal price should be as set originally");
        }

        [Test]
        public void CreateExternalDealDtoCorrectlySavesPriceLineRecords()
        {
            //DateTime dateTime = DateTime.Now.ToUniversalTime();
            ExternalDealDto externalDealDto2;
            
            //using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            //{
            //    using (AomModel aomModel = new AomModel())
            //    {
                    ExternalDealDto externalDealDto = TestShared.CreateFakeExternalDeal(new decimal?(123.45m), new decimal?(12345m), false, 1).ToDto();
                    PriceLineDto priceLineDto = null;
                    PriceBasisDto priceBasisDto;

                    AomDataBuilder.WithModel(_mockAomModel)
                        .AddUnit("BBL", "bbl")
                        .AddTenorCode("P")
                        .AddPriceType("FIXED")
                        .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                        .AddPriceLine(out priceLineDto, TestShared.FakeUserId)
                        .AddPriceLineBasis(123.45m, priceLineDto, 1L, 1, "P", priceBasisDto.Id)
                        .AddExternalDeal(externalDealDto, priceLineDto)
                        .DBContext.SaveChangesAndLog(-1);

                    externalDealDto2 = _mockAomModel.ExternalDeals.First(ed => ed.LastUpdatedUserId == TestShared.FakeUserId);
            //    }
            //}
            Assert.IsNotNull(externalDealDto2.PriceLineDto);
            Assert.That(externalDealDto2.PriceLineDto.PriceLinesBases.Count(), Is.EqualTo(1));
            Assert.AreEqual(externalDealDto2.Price, externalDealDto2.PriceLineDto.PriceLinesBases.First().PriceLineBasisValue);
            Assert.IsNotNull(externalDealDto2.LastUpdated);
            Assert.IsNotNull(externalDealDto2.DateCreated);
            Assert.IsNotNull(externalDealDto2.PriceLineDto.PriceLinesBases.First().LastUpdated);
            Assert.IsNotNull(externalDealDto2.PriceLineDto.PriceLinesBases.First().DateCreated);
        }

        [Test]
        public void CreateExternalDealCorrectlySavesPriceLineRecords()
        {
            ExternalDeal fakeExternalDeal = TestShared.CreateFakeExternalDeal(new decimal?(123.45m), new decimal?(12345m), false, 1);
            AomExternalDealAuthenticationResponse aomExternalDealAuthenticationResponse = TestShared.CreateFakeResponse(fakeExternalDeal, false, null);
            ExternalDeal externalDeal = _externalDealService.CreateExternalDeal(this._mockAomModel, aomExternalDealAuthenticationResponse);
            ExternalDeal externalDealById = _externalDealService.GetExternalDealById(this._mockAomModel, externalDeal.Id);

            Assert.IsNotNull(externalDeal.PriceLine);
            Assert.That(externalDeal.PriceLine.PriceLinesBases.Count(), Is.EqualTo(1));
            Assert.AreEqual(externalDeal.Price, externalDeal.PriceLine.PriceLinesBases.First().PriceLineBasisValue);
            Assert.IsNotNull(externalDealById.PriceLine);
            Assert.That(externalDealById.PriceLine.PriceLinesBases.Count(), Is.EqualTo(1));
            Assert.AreEqual(externalDealById.Price, externalDealById.PriceLine.PriceLinesBases.First().PriceLineBasisValue);
        }


        [Test]
        public void ExternalDealCreatedAsVerifiedHasStatusSetToExecuted()
        {
            var fakeDeal = TestShared.CreateFakeExternalDeal(null, null, true, TestShared.FakeOutrightProductId);

            var fakeResponse = TestShared.CreateFakeResponse(fakeDeal);
            var createdDeal = _externalDealService.CreateExternalDeal(_mockAomModel, fakeResponse);
            Assert.AreEqual(DealStatus.Executed, createdDeal.DealStatus,"Deal status should be set to executed when deal is marked as verified");
        }

        [Test]
        public void CanCreateExternalDealForOtherProductWithContractSpecified()
        {
            var fakeDeal = TestShared.CreateFakeExternalDeal(null, null, false, TestShared.FakeOtherProductId);
            fakeDeal.ContractInput = TestShared.FakeContractInput;

            var fakeResponse = TestShared.CreateFakeResponse(fakeDeal);
            var createdDeal = _externalDealService.CreateExternalDeal(_mockAomModel, fakeResponse);

            Assert.AreEqual(TestShared.FakeContractInput, createdDeal.ContractInput, "ContractInput should have been set for the deal");
            Assert.AreEqual(TestShared.FakeOtherProductId, createdDeal.ProductId, "ProductId should be that of the Other product");
        }

        [Test]
        public void CannotCreateExternalDealForOtherProductWithNoContractSpecified()
        {
            // Arrange
            var fakeDeal = TestShared.CreateFakeExternalDeal(null, null, false, TestShared.FakeOtherProductId);
            var fakeResponse = TestShared.CreateFakeResponse(fakeDeal);

            // Act and assert
            var e = Assert.Catch(typeof (BusinessRuleException), () => _externalDealService.CreateExternalDeal(_mockAomModel, fakeResponse));

            TestShared.AssertExceptionMessageContains("'Other' product but did not specify a contract", e.Message);
        }
    }
}

