﻿namespace AOM.Services.Tests.NewsService
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Tests.Aom;
    using AOM.Services.NewsService;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Linq;

    [TestFixture]
    public class AomNewsRepositoryTests
    {
        private AomNewsRepository _aomNewsRepository;

        private Mock<IDbContextFactory> _mockDbContextFactory;

        [SetUp]
        public void TestSetup()
        {
            _mockDbContextFactory = new Mock<IDbContextFactory>();
            _aomNewsRepository = new AomNewsRepository(_mockDbContextFactory.Object);
        }

        [Test]
        public void ShouldCreateANewAomNewsModel()
        {
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("1", "40 Days", true, DateTime.Now, true)
                .DBContext.SaveChangesAndLog(-1);

            var news = mockAomModel.News.FirstOrDefault(n => n.CmsId == "1");
            Assert.That(news, Is.Not.Null);
            Assert.That(news.Story, Is.EqualTo("40 Days"));
            Assert.That(news.ContentStreams.Count(), Is.EqualTo(1));
        }

        [Test]
        public void CallingCreateNewsShouldPersistTheNews()
        {
            var cmsId = "1";
            var story = "Once upon a time...";

            var aomNews = new AomNews
            {
                CmsId = cmsId,
                ContentStreams = null,
                IsFree = true,
                PublicationDate = DateTime.Now,
                Story = story,
                CommodityId = 1,
                Headline = "Headline"
            };

            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("2", "40 Days", true, DateTime.Now, true)
                .DBContext.SaveChangesAndLog(-1);

            _mockDbContextFactory.Setup(m => m.CreateAomModel()).Returns(mockAomModel);
            var newsId = _aomNewsRepository.CreateAomNews(aomNews);

            Assert.That(newsId, Is.GreaterThanOrEqualTo(0));
            var news = _aomNewsRepository.GetAomNews(cmsId);
            Assert.That(news, Is.Not.Null);
            Assert.That(news.Story, Is.EqualTo(story));
        }

        [Test]
        public void CallingCreateNewsShouldNotPersistTheNewsIfTheNewsAlreadyExists()
        {
            var cmsId = "1";
            var story = "Once upon a time...";

            var aomNews = new AomNews
            {
                CmsId = cmsId,
                ContentStreams = null,
                IsFree = true,
                PublicationDate = DateTime.Now,
                Story = story,
                CommodityId = 1,
                Headline = "Headline"
            };

            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("1", "40 Days", true, DateTime.Now, true)
                .DBContext.SaveChangesAndLog(-1);

            _mockDbContextFactory.Setup(m => m.CreateAomModel()).Returns(mockAomModel);
            var newsId = _aomNewsRepository.CreateAomNews(aomNews);

            Assert.That(newsId, Is.GreaterThanOrEqualTo(0));
            var news = _aomNewsRepository.GetAomNews(cmsId);
            Assert.That(news, Is.Not.Null);
            Assert.That(news.Story, Is.EqualTo("40 Days"));
        }

        [Test]
        public void ShouldDeleteANewsStoryWhenItExistsWithContentStreams()
        {
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("1", "A long time ago", true, DateTime.Now, true)
                .DBContext.SaveChangesAndLog(-1);

            _mockDbContextFactory.Setup(m => m.CreateAomModel()).Returns(mockAomModel);

            bool result = _aomNewsRepository.DeleteAomNews("1");

            var news = mockAomModel.News.FirstOrDefault(n => n.CmsId == "1");
            Assert.That(news, Is.Null);
        }

        [Test]
        public void ShouldDeleteANewsStoryWhenItExistsWithoutContentStreams()
        {
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("1", "A long time ago", true, DateTime.Now, false)
                .DBContext.SaveChangesAndLog(-1);

            _mockDbContextFactory.Setup(m => m.CreateAomModel()).Returns(mockAomModel);

            bool result = _aomNewsRepository.DeleteAomNews("1");

            var news = mockAomModel.News.FirstOrDefault(n => n.CmsId == "1");
            Assert.That(news, Is.Null);
        }

        [Test]
        public void CallingGetAomNewsShouldReturnAomNews()
        {
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("1", "How the west was won?", true, DateTime.Now)
                .DBContext.SaveChangesAndLog(-1);

            _mockDbContextFactory.Setup(m => m.CreateAomModel()).Returns(mockAomModel);

            var news = _aomNewsRepository.GetAomNews("1");

            Assert.That(news, Is.Not.Null);
            Assert.That(news.Story, Is.EqualTo("How the west was won?"));
        }

        [Test]
        public void UpdatingANewsStoryShouldPersistTheChanges()
        {
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("1", "How the west was won?", true, DateTime.Now)
                .DBContext.SaveChangesAndLog(-1);

            _mockDbContextFactory.Setup(m => m.CreateAomModel()).Returns(mockAomModel);

            var aomNews = new AomNews
            {
                CmsId = "1",
                Story = "How the west was lost?",
                IsFree = true
            };
            _aomNewsRepository.UpdateAomNews(aomNews);

            var updatedNews = mockAomModel.News.FirstOrDefault(n => n.CmsId == "1");
            Assert.That(updatedNews.Story, Is.EqualTo("How the west was lost?"));
        }

        [Test]
        public void UpdatingANewsContentStreamsShouldPersistTheChanges()
        {
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("1", "How the west was won?", true, DateTime.Now)
                .DBContext.SaveChangesAndLog(-1);

            _mockDbContextFactory.Setup(m => m.CreateAomModel()).Returns(mockAomModel);

            var aomNews = new AomNews
            {
                CmsId = "1",
                Story = "How the west was lost?",
                IsFree = true
            };
            _aomNewsRepository.UpdateAomNews(aomNews);

            var updatedNews = mockAomModel.News.FirstOrDefault(n => n.CmsId == "1");
            Assert.That(updatedNews.ContentStreams.Count(), Is.EqualTo(0));
        }

        [Test]
        public void CallingPurgeNewsShouldRemoveOldNewsItems()
        {
            var archivePublicationDate = DateTime.Now.AddDays(-10);
            var newPublicationDate = DateTime.Now.AddDays(-1);
            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddNews("1", "How the west was won?", true, archivePublicationDate, true)
                .AddNews("2", "The good the bad and the ugly", true, newPublicationDate, true)
                .AddNews("3", "When Harry met Sally", true, DateTime.Now, true)
                .DBContext
                .SaveChangesAndLog(-1);

            _mockDbContextFactory.Setup(m => m.CreateAomModel()).Returns(mockAomModel);

            var purgeDateTime = DateTime.Now.AddDays(-5);
            _aomNewsRepository.PurgeNews(purgeDateTime);

            // Ok - we should now have only 2 News Stories remaining.
            var allNews = mockAomModel.News.ToList();
            Assert.That(allNews.Count, Is.EqualTo(2));

            foreach (var news in allNews)
            {
                Assert.That(news.PublicationDate, Is.GreaterThan(purgeDateTime));
            }
        }
    }
}