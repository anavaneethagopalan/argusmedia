﻿namespace AOM.Services.Tests.NewsService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AOM.Repository.MySql.Aom;
    using AOM.Repository.MySql.Tests;
    using AOM.Repository.MySql.Tests.Crm;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql.Crm;
    using AOM.Services.NewsService;
    using AOM.Services.ProductService;

    using Argus.Transport.Messages.Dtos;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class NewsServiceTests
    {
        private IAomNewsService _newsService;

        private Mock<IUserService> _mockUserService;

        Mock<IAomNewsRepository> _mockNewsRepo;

        private Mock<IProductService> _mockProductService;

        [SetUp]
        public void TestSetup()
        {

            List<ContentStreamDto> contentStreams = new List<ContentStreamDto>
            {
                new ContentStreamDto
                {
                    Id = 1,
                    Description = "General Atlantic basin products",
                    ContentStreamId = 95508
                },
                new ContentStreamDto {Id = 2, Description = "Argus European Products", ContentStreamId = 95028},
                new ContentStreamDto {Id = 3, Description = "General global news", ContentStreamId = 95525}
            };

            _mockUserService = new Mock<IUserService>();
            var crmModel = new MockCrmModel();
            _mockNewsRepo = new Mock<IAomNewsRepository>();
            var news = new NewsDto { CmsId = "12", PublicationDate = DateTime.Now, Story = "a", IsFree = true };
            _mockNewsRepo.Setup(x => x.GetAomNews(It.IsAny<string>())).Returns(news);

            _mockProductService = new Mock<IProductService>();


            foreach (var contentStreamDto in contentStreams)
            {
                crmModel.ContentStreams.Add(contentStreamDto);
            }
            _newsService = new AomNewsService(
                _mockUserService.Object,
                _mockNewsRepo.Object,
                new MockDbContextFactory(null, crmModel),
                _mockProductService.Object);
        }

        [Test]
        public void ShouldTrasformArgusNewsIntoAomNewsSettingTheHeadlineAndStory()
        {
            const string Headline = "Breaking News";
            const string Story = "This is a great story";
            var argusNews = MakeArgusNews(Headline, Story);
            var aomNews = _newsService.TransformNews(argusNews);

            Assert.That(aomNews.Headline, Is.EqualTo(Headline));
            Assert.That(aomNews.Story, Is.EqualTo(Story));
        }

        [Test]
        public void TransformAomNewsWithNullArgusNewsShouldNotThrowException()
        {
            var aomNews = _newsService.TransformNews(null);
            Assert.That(aomNews, Is.Null);
        }

        [Test]
        public void TransformAomNewsWithNullArgusNewsShouldHandleNullNewsType()
        {
            var argusNews = new PersistedArticle
            {
                CmsId = "1",
                CmsVersion = "1.0",
                ContentStreams = null,
                DomainId = 1,
                Headline = "Headline",
                IsFeatured = true,
                IsFree = false
            };
            var aomNews = _newsService.TransformNews(argusNews);
            Assert.That(aomNews, Is.Not.Null);
            Assert.That(aomNews.NewsType.Equals(string.Empty));
        }

        [Test]
        public void ShouldTrasformArgusNewsIntoAomNewsSettingTheCommodityId()
        {
            var commodities = new List<CommodityContentStreamDto>
            {
                new CommodityContentStreamDto {CommodityId = 1, ContentStreamId = 1}
            };

            _mockProductService.Setup(m => m.GetCommoditiesForContentStreams(It.IsAny<long[]>())).Returns(commodities);
            const string Headline = "Breaking News";
            const string Story = "This is a great story";
            var argusNews = MakeArgusNews(Headline, Story);
            var aomNews = _newsService.TransformNews(argusNews);

            Assert.That(aomNews.CommodityId, Is.EqualTo(1));
        }

        [Test]
        public void ShouldTrasformArgusNewsIntoAomNewsSettingThePublicationDate()
        {
            var publicationDate = new DateTime(2010, 1, 1);
            var argusNews = MakeArgusNews("", "");
            argusNews.PublicationDate = publicationDate;

            var aomNews = _newsService.TransformNews(argusNews);

            Assert.That(aomNews.PublicationDate, Is.EqualTo(publicationDate));
        }

        [Test]
        public void ShouldTransformArgusNewsIntoAomNewsSettingTheStoryToBeLessThanNNNCharacters()
        {
            var publicationDate = new DateTime(2010, 1, 1);
            var argusNews = MakeArgusNews("", "");
            argusNews.PublicationDate = publicationDate;
            argusNews.Story = String.Concat(Enumerable.Repeat("123456789 ", 35).ToArray());

            var aomNews = _newsService.TransformNews(argusNews);
            aomNews.Story = _newsService.TruncateStory(aomNews.Story);
            Assert.That(aomNews.Story.Length, Is.LessThan(304));
        }


        [Test]
        public void ShouldTransformArgusNewsIntoAomNewsSettingTheStoryToBeFirstNNNCharactersIfNoMoreSpaces()
        {
            var argusNews = MakeArgusNews("", "");
            argusNews.Story = "1234567890".PadLeft(999, 'X');

            var aomNews = _newsService.TransformNews(argusNews);
            aomNews.Story = _newsService.TruncateStory(aomNews.Story);
            Assert.That(aomNews.Story.Length, Is.EqualTo(304));
        }

        [Test]
        public void
            ShouldTransformArgusNewsIntoAomNewsSettingTheStoryToBeFirstNNNCharactersIfNoMoreSpacesAndAddMoreStoryIndicatorToTheEnd
            ()
        {
            var publicationDate = new DateTime(2010, 1, 1);
            var argusNews = MakeArgusNews("", "");
            argusNews.PublicationDate = publicationDate;
            argusNews.Story = String.Concat(Enumerable.Repeat("1234567890", 35).ToArray()) + "123";

            var aomNews = _newsService.TransformNews(argusNews);
            aomNews.Story = _newsService.TruncateStory(aomNews.Story);
            Assert.That(aomNews.Story.Length, Is.EqualTo(304));
            Assert.That(aomNews.Story.EndsWith("..."));
        }

        [Test]
        public void
            ShouldTransformArgusNewsIntoAomNewsIfTheLengthOfTheStoryIsLessThanMaxCharactersAndTheStoryContainsHtmlThenTheHtmlWillBeRemoved
            ()
        {
            const string SmallStoryWithHtml =
                "<html><body>Breaking News<p>This is a html story the <em>story</em> </p><body></html>";

            var publicationDate = new DateTime(2010, 1, 1);
            var argusNews = MakeArgusNews("", "");
            argusNews.PublicationDate = publicationDate;
            argusNews.Story = SmallStoryWithHtml;

            var aomNews = _newsService.TransformNews(argusNews);
            aomNews.Story = _newsService.TruncateStory(aomNews.Story);
            Assert.That(aomNews.Story.StartsWith("Breaking"),
                "Expected story to start with 'Breaking' instead it was :" + aomNews.Story);
        }

        [Test]
        public void
            ShouldTransformArgusNewsIntoAomNewsIfTheLengthOfTheStoryIsLessThan250CharactersAndTheStoryContainsHtmlThenTheHtmlWillBeLeftInTheStory
            ()
        {
            const string SmallStoryWithHtml =
                "<h1>Breaking News</h1><p>This is a html story the <em>story</em> passed back should not contain any html markup</p>";

            var publicationDate = new DateTime(2010, 1, 1);
            var argusNews = MakeArgusNews("", "");
            argusNews.PublicationDate = publicationDate;
            argusNews.Story = SmallStoryWithHtml;

            var aomNews = _newsService.TransformNews(argusNews);

            Assert.That(aomNews.Story, Is.EqualTo(SmallStoryWithHtml));
        }

        [Test]
        public void ShouldTransformArgusNewsIntoAomNewsSettingTheStoryToBeTheStoryPassedInWithNoMoreStoryIndicator()
        {
            var publicationDate = new DateTime(2010, 1, 1);
            var argusNews = MakeArgusNews("", "");
            argusNews.PublicationDate = publicationDate;
            argusNews.Story = "AShortStory";

            var aomNews = _newsService.TransformNews(argusNews);

            Assert.That(aomNews.Story.Length, Is.EqualTo(11));
            Assert.That(aomNews.Story.EndsWith("AShortStory"));
        }

        [Test]
        public void ShouldReturnFalseForAomNewsIfPublishedAricleIsNull()
        {
            var isAomNews = _newsService.IsAomNewsArticle(null);

            Assert.That(isAomNews, Is.False);
        }

        [Test]
        public void ShouldReturnFalseForIsAomNewsIfAPublishedArticleHasNoContentStreams()
        {
            var persistedNews = MakePersistedNewsArticle();

            var isAomNews = _newsService.IsAomNewsArticle(persistedNews);

            Assert.That(isAomNews, Is.False);
        }

        [Test]
        public void ShouldReturnFalseForIsAomNewsIfAPublishedArticleHasContentStreamsButNoneOfThemAreAomRelated()
        {
            var publishedNews = MakePersistedNewsArticle();
            publishedNews.ContentStreams = new[]
            {
                new PersistedMetaTypeDatum {DomainTypeId = 200},
                new PersistedMetaTypeDatum {DomainTypeId = 201},
                new PersistedMetaTypeDatum {DomainTypeId = 202}
            };

            var isAomNews = _newsService.IsAomNewsArticle(publishedNews);
            Assert.That(isAomNews, Is.False);
        }

        [Test]
        public void ShouldReturnTrueForIsAomNewsIfAPublishedArticleHasContentStreamsWhichOneOfWhichIsRelatedToAom()
        {
            var publishedNews = MakePersistedNewsArticle();
            publishedNews.ContentStreams = new[]
            {
                new PersistedMetaTypeDatum {DomainId = 300L},
                new PersistedMetaTypeDatum {DomainId = 95L},
                new PersistedMetaTypeDatum {DomainId = 95028L}
            };

            var isAomNews = _newsService.IsAomNewsArticle(publishedNews);
            Assert.That(isAomNews, Is.True);
        }

        [Test]
        public void ShouldReturnTrueForIsAomNewsIfAPublishedArticleHasContentStreamsAllOfWhichAreRelatedToAom()
        {
            var publishedNews = MakePersistedNewsArticle();
            publishedNews.ContentStreams = new[] { new PersistedMetaTypeDatum { DomainId = 95028L } };

            var isAomNews = _newsService.IsAomNewsArticle(publishedNews);
            Assert.That(isAomNews, Is.True);
        }

        [Test]
        public void GetNewsStoryShouldReplaceTargetParentToTargetBlank()
        {
            var news = new NewsDto
            {
                CmsId = "12",
                PublicationDate = DateTime.Now,
                Story = "This is a story <a href='' target='_parent'>Click</a>",
                IsFree = true
            };
            _mockNewsRepo.Setup(x => x.GetAomNews(It.IsAny<string>())).Returns(news);

            var story = _newsService.GetNewsStory("1", 1);
            Assert.That(story, Is.EqualTo("This is a story <a href='' target='_blank'>Click</a>"));
        }

        [Test]
        public void GetNewsStoryShouldReturnAnArticleStoryForAValidNewsStory()
        {
            const string fakeNewsId = "1234";
            var fakeNews = new NewsDto
            {
                CmsId = fakeNewsId
            };
            _mockNewsRepo.Setup(r => r.GetAomNews(fakeNewsId)).Returns(fakeNews);
            try
            {
                var story = _newsService.GetNewsStory(fakeNewsId, 1);
                Assert.That(story, Is.Not.Null);
            }
            catch (Exception e)
            {
                if (e.Message == "Connection request timed out")
                    Assert.Inconclusive("Inconclusive due to db connection timeout");
                else throw;
            }
        }

        [Test]
        public void IsUserEntitledToNewsStoryShouldReturnTrueIfTheStoryIsFree()
        {
            var entitledToNews = _newsService.UserEntitledToStory(1, 1, true);
            Assert.That(entitledToNews, Is.EqualTo(true));
        }

        [Test]
        public void IsUserEntitledToNewsStoryShouldReturnFalseIfTheStoryIsNotFreeAndTheStoryDoesNotHaveAnyContentStreams
            ()
        {
            _mockUserService = new Mock<IUserService>();
            var crmModel = new MockCrmModel();
            var mockAomNewsRepository = new Mock<IAomNewsRepository>();

            var newsService = new AomNewsService(
                _mockUserService.Object,
                mockAomNewsRepository.Object,
                new MockDbContextFactory(null, crmModel),
                _mockProductService.Object);
            ;
            mockAomNewsRepository.Setup(m => m.GetContentStreamsForArticle(It.IsAny<long>())).Returns(new List<long>());

            var entitledToNews = newsService.UserEntitledToStory(1, 1, false);
            Assert.That(entitledToNews, Is.EqualTo(false));
        }

        [Test]
        public void
            IsUserEntitledToNewsStoryShouldReturnFalseIfTheStoryIsNotFreeAndTheUserDoesNotHaveAnyContentStreamsThatMatchesTheArticleContentStreams
            ()
        {
            _mockUserService = new Mock<IUserService>();
            var crmModel = new MockCrmModel();
            var mockAomNewsRepository = new Mock<IAomNewsRepository>();

            _mockUserService.Setup(m => m.CheckUserHasContentStreams(It.IsAny<long>(), It.IsAny<List<long>>()))
                .Returns(false);

            var newsService = new AomNewsService(
                _mockUserService.Object,
                mockAomNewsRepository.Object,
                new MockDbContextFactory(null, crmModel),
                _mockProductService.Object);
            mockAomNewsRepository.Setup(m => m.GetContentStreamsForArticle(It.IsAny<long>()))
                .Returns(new List<long> { 100, 101 });

            var entitledToNews = newsService.UserEntitledToStory(1, 1, false);
            Assert.That(entitledToNews, Is.EqualTo(false));
        }

        [Test]
        public void ShouldRemoveOuterHtmlTagsFromArgusNewsMessageThatUsesXmlNamespaces()
        {
            var publicationDate = new DateTime(2014, 11, 17);
            var argusNews = MakeArgusNews("", "");
            argusNews.PublicationDate = publicationDate;
            argusNews.Story = TrimmedSampleNewsArticleFromLive;

            var aomNews = _newsService.TransformNews(argusNews);
            aomNews.Story = _newsService.TruncateStory(aomNews.Story);

            const string expectedStoryStart = "London, 17 November (Argus)";
            Assert.That(
                aomNews.Story.StartsWith(expectedStoryStart),
                string.Format(
                    "Expected message to start with {0} but started with {1}",
                    expectedStoryStart,
                    aomNews.Story.Substring(0, expectedStoryStart.Length)));
        }

        private PersistedArticle MakePersistedNewsArticle()
        {
            return new PersistedArticle { CmsId = "100", IsFree = false, ContentStreams = null };
        }

        private PersistedArticle MakeArgusNews(string headline, string story)
        {
            return new PersistedArticle
            {
                CmsId = "1",
                CmsVersion = "1.0",
                ContentStreams = null,
                Headline = headline,
                IsFeatured = true,
                IsFree = true,
                Keywords = new[] { "ONE", "TWO" },
                Story = story,
                PublicationDate = DateTime.Now,
                NewsType = new PersistedMetaDatum { DomainId = 1, Name = "NewsType" }
            };
        }

        // Full html and head elements but the body text is heavily truncated
        private const string TrimmedSampleNewsArticleFromLive =
            "<html xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:my=\"http://www.censhare.com/xml/3.0.0/my\" xmlns:cs=\"http://www.censhare.com/xml/3.0.0/xpath-functions\"><head><link href=\"../../ArgusStaticContent/news.css\" type=\"text/css\" rel=\"stylesheet\"></link><title >Oil futures: Brent falls on Japan GDP data</title><meta charset=\"utf-8\"></meta></head><body><p>London, 17 November (Argus) — Ice Brent crude futures declined this afternoon</p></body></html>";

    }
}