﻿namespace AOM.Services.Tests.CrmService
{
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;

    using NUnit.Framework;

    [TestFixture]
    public class LoginSessionTests
    {
        private DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        [Test]
        public void CreateSessiopDoesNotThrowTest()
        {
            Assert.DoesNotThrow(
                () =>
                        CreateLoginSession());
        }

        [Test]
        public void CreateSessionTest()
        {
            var session = CreateLoginSession();
            Assert.IsNotNull(session);
            Assert.NotNull(session.Username);
            Assert.NotNull(session.IpAddress);
            Assert.IsNotNull(session.SessionStarted);
        }

        [Test]
        public void HashCodeTest()
        {
            var session = CreateLoginSession();

            Assert.DoesNotThrow(() => session.GetHashCode());
            var hash = session.GetHashCode();
            Assert.IsTrue(hash != 0);
        }

        [Test]
        public void CompareSameSessionsTest()
        {
            var createdTimeStamp = _dateTimeProvider.Now;
            var session1 = CreateLoginSession();
            var session2 = CreateLoginSession();

            Assert.AreEqual(session1.GetHashCode(), session2.GetHashCode());
            Assert.True(session1.Equals(session2));
        }

        [Test]
        public void CompareameDifferentSessionsTest()
        {
            var session1 = CreateLoginSession(1);
            var session2 = CreateLoginSession(1);

            Assert.True(session1.GetHashCode() == session2.GetHashCode());
        }

        private LoginSession CreateLoginSession(int numberAttemps = 2)
        {

            return new LoginSession
            {
                IpAddress = "1.1.1.1",
                IsAlive = true,
                NumberOfAttempts = numberAttemps,
                SessionStarted = _dateTimeProvider.Now,
                Username = "ja"
            };
        }
    }
}