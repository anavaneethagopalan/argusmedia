﻿using System;
﻿using System.Collections.Generic;
using System.Configuration;
using System.Globalization;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.CrmService;

using log4net.Core;
using Utils.Logging;
using Moq;
using NUnit.Framework;


namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class UserAuthenticationServiceTests
    {
        private const string SessionTokenString = "12345";
        private const string UsersIpAddess = "1.1.1.1";
        private const long AomUserLoginSourceId = 1;
        readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        private Mock<IUserLoginTrackingService> _mockLoginService;
        private Mock<IUserTokenService> _mockTokenService;
        private Mock<IUserService> _mockUserService;
        private Mock<ICredentialsService> _mockCredentialsService;
        private Mock<ICrmAdministrationService> _mockCrmService;
   
        private MemoryAppenderForTests _testLog;

        [SetUp]
        public void TestFixtureSetup()
        {
            _testLog = new MemoryAppenderForTests();
        }

        [TearDown]
        public void TestFixtureTearDown()
        {
            if (_testLog != null)
            {
                _testLog.Dispose();
                _testLog = null;
            }
        }

        [SetUp]
        public void Setup()
        {
            _mockLoginService = new Mock<IUserLoginTrackingService>();
            _mockTokenService = new Mock<IUserTokenService>();
            _mockUserService = new Mock<IUserService>();
            _mockCredentialsService = new Mock<ICredentialsService>();
            _mockCrmService = new Mock<ICrmAdministrationService>();
        }

        [TearDown]
        public void Teardown()
        {
            if (_testLog != null)
            {
                _testLog.Clear();
            }
        }

        private UserToken GetUserToken(string sessionToken, string ipAddress)
        {
            return new UserToken
            {
                Id = 1,
                LoginTokenString = "TestGuid123",
                SessionTokenString = sessionToken,
                UserIpAddress = ipAddress,
                SessionId = string.Empty,
                TimeCreated = DateTime.Now.ToUniversalTime(),
                User = null,
                UserInfo_Id_fk = 1
            };
        }

        public IUserTokenService GetUserTokenService(string sessionToken, string ipAddress)
        {
            var mockTokenService = new Mock<IUserTokenService>();
            mockTokenService.Setup(s => s.InsertNewUserToken(It.IsAny<long>(), It.IsAny<String>(), It.IsAny<string>())).Returns(GetUserToken(SessionTokenString, UsersIpAddess));
            mockTokenService.Setup(s => s.GetSessionTokenByGuid(It.IsAny<string>())).Returns(GetUserToken(SessionTokenString, UsersIpAddess));
            return mockTokenService.Object;
        }

        [TestCase("t1")]
        [TestCase("T1")]
        public void AuthenticateForValidUserNamePassword(string username)
        {
            const string correctPassword = "correctPass";

            var mockUser = new User
            {
                Id = 989898,
                Username = username,
                IsBlocked = false,
                IsActive = true,
                IsDeleted = false
            };

            var mockCredentials = new List<UserCredentials>
            {
                new UserCredentials
                {
                Id = 9999,
                DateCreated = DateTime.Now,
                PasswordHashed = correctPassword,
                Expiration = DateTime.Now.AddMonths(1).ToUniversalTime(),
                FirstTimeLogin = false,
                User = mockUser,
                // UserId = mockUser.Id
                }
            };

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            mockLoginService.Setup(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId)).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true).Verifiable();

            var mockCredentialsService = new Mock<ICredentialsService>();
            //mockCredentialsService.Setup(c => c.GetUserCredentials(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(mockCredentials).Verifiable();
            mockCredentialsService.Setup(c => c.GetUsersCredentialsByUsername(It.IsAny<string>())).Returns(mockCredentials).Verifiable();

            var mockUserService = new Mock<IUserService>();

            var usersOrganisation = new Organisation { Id = 1, Name = "Nathans Organisation", IsDeleted = false };
            mockUserService.Setup(m => m.GetUsersOrganisation(It.IsAny<long>())).Returns(usersOrganisation);
            mockUserService.Setup(m => m.GetUserLoginSources()).Returns(new List<UserLoginSource>() { new UserLoginSource() {Id = 1, Code = "aom" } });

            var crmService = new UserAuthenticationService(mockLoginService.Object,GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var authenticationResult = crmService.AuthenticateUser(username, correctPassword, 1);

            Assert.That(authenticationResult.IsAuthenticated, Is.True);
            Assert.That(authenticationResult.FirstLogin, Is.False);

            mockCredentialsService.Verify(a => a.GetUsersCredentialsByUsername(username), Times.Exactly(1));
            //should not have called blockUser
            mockCrmService.Verify(a => a.BlockUser(username, -1), Times.Never);
            //should not validate user in cache
            mockLoginService.Verify(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId), Times.Never);
            //should remove the username from the cache
            mockLoginService.Verify(a => a.ResetUserAccount(mockUser.Id, AomUserLoginSourceId), Times.Exactly(1));

            _testLog.AssertNoLogMessagesAtLevel(Level.Warn, Level.Error);
        }


   

        [TestCase("t1")]
        [TestCase("T1")]
        public void ShouldAuthenticateCorrectSessionTokenTest(string username)
        {
            const string correctPassword = "correctPass";
            const string userId = "1";

            var mockCredentials = new UserCredentials
            {
                Id = 9999,
                DateCreated = DateTime.Now,
                PasswordHashed = correctPassword,
                Expiration = DateTime.Now.AddMonths(1).ToUniversalTime(),
                FirstTimeLogin = false
            };
            var mockUser = new User
            {
                Id = 989898,
                Username = username,
                IsBlocked = false,
                IsActive = true,
                IsDeleted = false
            };

            mockCredentials.User = mockUser;

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            mockLoginService.Setup(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId)).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true).Verifiable();

            var mockCredentialsService = new Mock<ICredentialsService>();
            mockCredentialsService.Setup(c => c.GetUserCredentials(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(mockCredentials).Verifiable();

            var mockUserService = new Mock<IUserService>();

            var crmService = new UserAuthenticationService(mockLoginService.Object,GetUserTokenService(SessionTokenString, userId),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var authenticationResult = crmService.AuthenticateSessionToken(SessionTokenString, userId);

            Assert.That(authenticationResult.IsAuthenticated, Is.True);
            Assert.That(authenticationResult.FirstLogin, Is.False);

            _testLog.AssertNoLogMessagesAtLevel(Level.Warn, Level.Error);
        }

        [Test]
        public void ShouldNotAuthenticateInvalidSessionTokenTest()
        {
            const string userId = "1";

            _mockTokenService.Setup(s => s.GetSessionTokenByGuid(It.IsAny<string>())).Returns((IUserToken) null);

            var crmService = new UserAuthenticationService(_mockLoginService.Object,_mockTokenService.Object, _mockUserService.Object,
                _mockCredentialsService.Object, _mockCrmService.Object, _dateTimeProvider);

            var authenticationResult = crmService.AuthenticateSessionToken(SessionTokenString, userId);

            Assert.That(authenticationResult.IsAuthenticated, Is.False);

            _testLog.AssertALogMessageContains(Level.Info, "Could not get a token");
            _testLog.AssertALogMessageContains(Level.Info, userId);
        }

        [Test]
        public void ShouldNotAuthenticateSessionTokenWithDifferentUserIdTest()
        {
            const string userId = "1";
            const int wrongUserId = 2;

            var fakeToken = new UserToken
            {
                UserInfo_Id_fk = wrongUserId
            };

            _mockTokenService.Setup(s => s.GetSessionTokenByGuid(It.IsAny<string>())).Returns(fakeToken);

            var crmService = new UserAuthenticationService(_mockLoginService.Object, _mockTokenService.Object, _mockUserService.Object,
                _mockCredentialsService.Object, _mockCrmService.Object, _dateTimeProvider);

            var authenticationResult = crmService.AuthenticateSessionToken(SessionTokenString, userId);

            Assert.That(authenticationResult.IsAuthenticated, Is.False);

            _testLog.AssertALogMessageContains(Level.Warn, "Different username for token");
            _testLog.AssertALogMessageContains(Level.Warn, string.Format("User id of request: {0}", userId));
            _testLog.AssertALogMessageContains(Level.Warn, string.Format("user id of token from db: {0}", wrongUserId));
        }

        [Test]
        public void AuthenticateForAnInvalidUserShouldReturnFalseTest()
        {
            const string username = "SomeUser";
            const string password = "fakepass";

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            var mockCrmService = new Mock<ICrmAdministrationService>();
            var mockCredentialsService = new Mock<ICredentialsService>();

            IList<UserCredentials> emptyUserCredentials = new List<UserCredentials>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true);
            mockCredentialsService.Setup(c => c.GetUsersCredentialsByUsername(It.IsAny<string>())).Returns(emptyUserCredentials);

            var mockUserService = new Mock<IUserService>();

            var authService = new UserAuthenticationService(mockLoginService.Object, GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var authenticationResult = authService.AuthenticateUser(username, password, 1);

            Assert.That(authenticationResult.IsAuthenticated, Is.False);

            _testLog.AssertALogMessageContains(Level.Info, string.Format("No User found with username - {0}", username));
        }

        [Test]
        public void ShouldCallValidateUserForWrongPasswordTest()
        {
            const string username = "t1";
            const string correctPassword = "correctPass";
            const string incorrectPassword = "fakepass";

            var mockUser = new User
            {
                Id = 989898,
                Username = username,
                IsBlocked = false,
                IsDeleted = false,
                IsActive = true
            };

            var mockCredentials = new List<UserCredentials>
            {
                new UserCredentials
                {
                    Id = 9999,
                    DateCreated = DateTime.Now,
                    PasswordHashed = incorrectPassword,
                    Expiration = DateTime.Now.AddMonths(1).ToUniversalTime(),
                    FirstTimeLogin = false,
                    User = mockUser
                }
            };

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            mockLoginService.Setup(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId)).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true);

            var mockCredentialsService = new Mock<ICredentialsService>();
            mockCredentialsService.Setup(c => c.GetUsersCredentialsByUsername(It.IsAny<string>())).Returns(mockCredentials);

            var mockUserService = new Mock<IUserService>();
            var usersOrganisation = new Organisation { Id = 1, Name = "Nathans Organisation", IsDeleted = false };
            mockUserService.Setup(m => m.GetUsersOrganisation(It.IsAny<long>())).Returns(usersOrganisation);
            mockUserService.Setup(m => m.GetUserLoginSources()).Returns(new List<UserLoginSource>() { new UserLoginSource() {Id = 1, Code = "aom" } });

            var authService = new UserAuthenticationService(mockLoginService.Object, GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var result = authService.AuthenticateUser(username, correctPassword, 1);
            Assert.That(result.IsAuthenticated, Is.False);
            Assert.That(result.FirstLogin, Is.False);
            mockLoginService.Verify(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId), Times.Once);
            _testLog.AssertALogMessageContains(Level.Info, string.Format("Password incorrect for user {0}", username));
        }

        [Test]
        public void ShouldNotAuthenticateBlockedUserTest()
        {
            const string username = "t1";
            const string correctPassword = "correctPass";

            var mockUser = new User { Id = 989898, Username = username, IsBlocked = true };

            var mockCredentials = new List<UserCredentials>
            {
                new UserCredentials
                {
                    Id = 9999,
                    DateCreated = DateTime.Now,
                    PasswordHashed = correctPassword,
                    Expiration = DateTime.Now.AddMonths(1).ToUniversalTime(),
                    FirstTimeLogin = false,
                    User = mockUser,
                    //UserId = mockUser.Id
                }
            };

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            mockLoginService.Setup(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId)).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true);

            var mockCredentialsService = new Mock<ICredentialsService>();
            mockCredentialsService.Setup(c => c.GetUsersCredentialsByUsername(It.IsAny<string>())).Returns(mockCredentials);

            var mockUserService = new Mock<IUserService>();

            var authService = new UserAuthenticationService(mockLoginService.Object, GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var result = authService.AuthenticateUser(username, correctPassword, 1);
            Assert.That(result.IsAuthenticated, Is.False);
            Assert.That(result.FirstLogin, Is.False);
            mockLoginService.Verify(s => s.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId), Times.Never);
            _testLog.AssertALogMessageContains(Level.Info, String.Format("User {0} is blocked", username));
        }

        [Test]
        public void ShouldNotAuthenticateInactiveUserTest()
        {
            const string username = "t1";
            const string correctPassword = "correctPass";

            var mockUser = new User
            {
                Id = 989898,
                Username = username,
                IsBlocked = false,
                IsDeleted = false,
                IsActive = false
            };

            var mockCredentials = new List<UserCredentials>
            {
                new UserCredentials
                {
                    Id = 9999,
                    DateCreated = DateTime.Now,
                    PasswordHashed = correctPassword,
                    Expiration = DateTime.Now.AddDays(-1).ToUniversalTime(),
                    FirstTimeLogin = false,
                    User = mockUser,
                    //UserId = mockUser.Id
                }
            };

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            mockLoginService.Setup(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId)).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true);

            var mockCredentialsService = new Mock<ICredentialsService>();
            mockCredentialsService.Setup(c => c.GetUsersCredentialsByUsername(It.IsAny<string>())).Returns(mockCredentials);

            var mockTokenService = new Mock<IUserTokenService>();
            mockTokenService.Setup(a => a.InsertNewUserToken(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>())).Returns(It.IsAny<IUserToken>).Verifiable();

            var mockUserService = new Mock<IUserService>();

            var authService = new UserAuthenticationService(mockLoginService.Object, GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var result = authService.AuthenticateUser(username, correctPassword, 1);

            Assert.That(result.IsAuthenticated, Is.False);
            Assert.That(result.FirstLogin, Is.False);
            mockLoginService.Verify(s => s.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId), Times.Never);
            mockTokenService.Verify(s => s.InsertNewUserToken(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _testLog.AssertALogMessageContains(Level.Info, String.Format("User {0} is not active", username));
        }

        [Test]
        public void ShouldNotAuthenticateDeletedUserTest()
        {
            const string username = "t1";
            const string correctPassword = "correctPass";

            var mockUser = new User
            {
                Id = 989898,
                Username = username,
                IsBlocked = false,
                IsDeleted = true,
                IsActive = true
            };

            var mockCredentials = new List<UserCredentials>
            {
                new UserCredentials
                {
                    Id = 9999,
                    DateCreated = DateTime.Now,
                    PasswordHashed = correctPassword,
                    Expiration = DateTime.Now.AddDays(-1).ToUniversalTime(),
                    FirstTimeLogin = false,
                    User = mockUser,
                    //UserId = mockUser.Id
                }
            };

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            mockLoginService.Setup(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId)).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true);

            var mockCredentialsService = new Mock<ICredentialsService>();
            mockCredentialsService.Setup(c => c.GetUsersCredentialsByUsername(It.IsAny<string>())).Returns(mockCredentials);

            var mockTokenService = new Mock<IUserTokenService>();
            mockTokenService.Setup(a => a.InsertNewUserToken(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>())).Returns(It.IsAny<IUserToken>).Verifiable();

            var mockUserService = new Mock<IUserService>();

            var authService = new UserAuthenticationService(mockLoginService.Object, GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var result = authService.AuthenticateUser(username, correctPassword, 1);

            Assert.That(result.IsAuthenticated, Is.False);
            Assert.That(result.FirstLogin, Is.False);
            mockLoginService.Verify(s => s.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId), Times.Never);
            mockTokenService.Verify(s => s.InsertNewUserToken(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _testLog.AssertALogMessageContains(Level.Info, String.Format("User {0} is deleted", username));
        }

        [Test]
        public void ShouldNotAuthenticateUserWithExpiredPassword()
        {
            const string username = "t1";
            const string correctPassword = "correctPass";

            var mockUser = new User
            {
                Id = 989898,
                Username = username,
                IsBlocked = false,
                IsDeleted = false,
                IsActive = true
            };

            var mockCredentials = new List<UserCredentials>
            {
                new UserCredentials
                {
                    Id = 9999,
                    DateCreated = DateTime.Now,
                    PasswordHashed = correctPassword,
                    Expiration = DateTime.Now.AddDays(-1).ToUniversalTime(),
                    FirstTimeLogin = false,
                    User = mockUser
                }
            };

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            mockLoginService.Setup(a => a.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId)).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true);

            var mockCredentialsService = new Mock<ICredentialsService>();
            mockCredentialsService.Setup(
                    c => c.GetUsersCredentialsByUsername(It.IsAny<string>()))
                .Returns(mockCredentials);

            var mockTokenService = new Mock<IUserTokenService>();
            mockTokenService.Setup(a => a.InsertNewUserToken(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>())).Returns(It.IsAny<IUserToken>).Verifiable();

            var mockUserService = new Mock<IUserService>();
            var usersOrganisation = new Organisation { Id = 1, Name = "Any Organisation", IsDeleted = false };
            mockUserService.Setup(m => m.GetUsersOrganisation(It.IsAny<long>())).Returns(usersOrganisation);

            var authService = new UserAuthenticationService(mockLoginService.Object, GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var result = authService.AuthenticateUser(username, correctPassword, 1);

            Assert.That(result.IsAuthenticated, Is.False);
            Assert.That(result.FirstLogin, Is.False);
            mockLoginService.Verify(s => s.RegisterLoginAttempt(mockUser.Id, AomUserLoginSourceId), Times.Never);
            mockTokenService.Verify(s => s.InsertNewUserToken(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _testLog.AssertALogMessageContains(Level.Info, String.Format("Password has expired for user {0}", username));
        }

        [TestCase("Test1")]
        [TestCase("test1")]
        [TestCase("TEST1")]
        public void CallsBlockUserAfterDefinedNumbOfAttemptsTest(string username)
        {

            IAuthenticationResult authenticationResult = null;

            var correctPassword = "correctPass";
            var incorrectPassword = "fakepass";

            var createUsername = username.ToLower();
            var mockUser = new User
            {
                Id = 989898,
                Username = createUsername,
                IsBlocked = false,
                IsDeleted = false,
                IsActive = true
            };

            var mockCredentials = new List<UserCredentials>
            {
                new UserCredentials
                {
                    Id = 9999,
                    DateCreated = DateTime.Now,
                    PasswordHashed = correctPassword,
                    Expiration = DateTime.Now.AddDays(1).ToUniversalTime(),
                    FirstTimeLogin = false,
                    User = mockUser
                }
            };            

            const int allowedNumberOfAttempts = 3;
            ConfigurationManager.AppSettings["MaxNumberOFLoginAttempts"] = allowedNumberOfAttempts.ToString(CultureInfo.InvariantCulture);

            var contextFactory = new Mock<IDbContextFactory>();
            var crmModel = new MockCrmModel();
            var authenticationModel = new MockAuthenticationModel();

            contextFactory.Setup(c => c.CreateCrmModel()).Returns(() => crmModel);
            contextFactory.Setup(c => c.CreateAuthenticationModel()).Returns(() => authenticationModel);

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(a => a.GetUserId(It.IsAny<string>())).Returns(1);
            var usersOrganisation = new Organisation { Id = 1, Name = "Nathans Organisation", IsDeleted = false };
            mockUserService.Setup(m => m.GetUsersOrganisation(It.IsAny<long>())).Returns(usersOrganisation);

            mockUserService.Setup(m => m.GetUsersOrganisation(It.IsAny<long>())).Returns(usersOrganisation);

            var loginService = new UserLoginTrackingService(contextFactory.Object);

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true);

            var mockCredentialsService = new Mock<ICredentialsService>();
            mockCredentialsService.Setup(c => c.GetUsersCredentialsByUsername(It.IsAny<string>())).Returns(mockCredentials);

            var mockTokenService = new Mock<IUserTokenService>();
            mockTokenService.Setup(a => a.InsertNewUserToken(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>())).Returns(It.IsAny<IUserToken>).Verifiable();
            UserLoginSource userLoginSource = new UserLoginSource() {Id = 1, Code = "aom"};
            mockUserService.Setup(m => m.GetUserLoginSources()).Returns(new List<UserLoginSource>() { userLoginSource });

            var authService = new UserAuthenticationService(loginService, GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object, mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            authenticationResult = authService.AuthenticateUser(username, correctPassword, userLoginSource.Id);
            Assert.IsNotNull(authenticationResult);
            Assert.True(authenticationResult.IsAuthenticated);

            for (int i = 0; i < allowedNumberOfAttempts; i++)
            {
                authenticationResult = authService.AuthenticateUser(username, incorrectPassword, userLoginSource.Id);
            }

            Assert.That(authenticationResult.IsAuthenticated, Is.False);

            // InsertNewUserToken
            mockCrmService.Verify(s => s.BlockUser(It.IsAny<string>(), -1), Times.AtLeastOnce);
            mockTokenService.Verify(s => s.InsertNewUserToken(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);

            _testLog.AssertALogMessageContains(Level.Info, string.Format("User {0} has been blocked", username));
        }

        [Test]
        public void AuthenticateToken_TokenNotExpiredTest()
        {
            ConfigurationManager.AppSettings["tokenTtlSecs"] = "30";
            var timeCreated = DateTime.Now.ToUniversalTime();
            var username = "t1";
            var tokenGuid = "12345";
            var mockLoginService = new Mock<IUserLoginTrackingService>();
            var mockTokenService = new Mock<IUserTokenService>();

            var user = new User {Username = username, Id = 2};

            var token = new UserToken
            {
                Id = 1,
                LoginTokenString = tokenGuid,
                TimeCreated = timeCreated,
                User = user,
                UserInfo_Id_fk = user.Id
            };

            mockTokenService.Setup(s => s.InsertNewUserToken(It.IsAny<long>(), It.IsAny<String>(), It.IsAny<String>())).Returns(token);
            mockTokenService.Setup(s => s.GetLoginTokenByGuid(It.IsAny<String>())).Returns(token).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            var mockCredentialsService = new Mock<ICredentialsService>();

            var mockUserService = new Mock<IUserService>();

            var crmService = new UserAuthenticationService(mockLoginService.Object, mockTokenService.Object, mockUserService.Object,
                mockCredentialsService.Object, mockCrmService.Object, _dateTimeProvider);

            var result = crmService.AuthenticateToken(username, tokenGuid);

            mockTokenService.Verify(s => s.GetLoginTokenByGuid(token.LoginTokenString), Times.Once);

            Assert.True(result.IsAuthenticated);
            Assert.True(result.Token == tokenGuid);
            Assert.That(result.User.Username, Is.EqualTo(user.Username));
            _testLog.AssertNoLogMessagesAtLevel(Level.Info, Level.Warn, Level.Error);
        }

        [Test]
        public void AuthenticateToken_TokenExpiredTest()
        {
            const string username = "t1";
            const string tokenGuid = "12345";
            const int fakeTokenTtlSecs = 1;
            ConfigurationManager.AppSettings["tokenTtlSecs"] = fakeTokenTtlSecs.ToString(CultureInfo.InvariantCulture);

            var fakeCreatedTime = new DateTime(2015, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var fakeAuthTime = fakeCreatedTime + TimeSpan.FromSeconds(fakeTokenTtlSecs) + TimeSpan.FromMilliseconds(1);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(fakeAuthTime);

            var user = new User {Username = username, Id = 2};

            var token = new UserToken
            {
                Id = 1,
                LoginTokenString = tokenGuid,
                TimeCreated = fakeCreatedTime,
                User = user,
                UserInfo_Id_fk = user.Id
            };

            _mockTokenService.Setup(s => s.InsertNewUserToken(It.IsAny<long>(), It.IsAny<String>(), It.IsAny<String>())).Returns(token);
            _mockTokenService.Setup(s => s.GetLoginTokenByGuid(It.IsAny<String>())).Returns(token).Verifiable();

            var crmService = new UserAuthenticationService(
                _mockLoginService.Object,
                _mockTokenService.Object,
                _mockUserService.Object,
                _mockCredentialsService.Object,
                _mockCrmService.Object,
                mockDateTimeProvider.Object);

            var result = crmService.AuthenticateToken(username, tokenGuid);

            _mockTokenService.Verify(s => s.GetLoginTokenByGuid(token.LoginTokenString), Times.Once);

            Assert.False(result.IsAuthenticated);
            Assert.True(result.Token == string.Empty);
            Assert.That(result.User.Username, Is.EqualTo(user.Username));
            _testLog.AssertALogMessageContains(Level.Info, "Token expired");
        }

        [Test]
        public void ShouldLogErrorAndNotAuthenticateIfGetUsersCredentialsByUsernameThrows()
        {
            const string fakeExceptionMessage = "fakeExceptionMessage";

            _mockCredentialsService.Setup(s => s.GetUsersCredentialsByUsername(It.IsAny<string>())).Throws(new BusinessRuleException(fakeExceptionMessage));
            var crmService = new UserAuthenticationService(_mockLoginService.Object, _mockTokenService.Object, _mockUserService.Object,
                _mockCredentialsService.Object, _mockCrmService.Object, _dateTimeProvider);

            var authRes = crmService.AuthenticateUser("fakeName", "fakePassword", 1, "fakeIpAddress");
            Assert.That(authRes.IsAuthenticated, Is.False);
            _testLog.AssertALogMessageContains(Level.Error, fakeExceptionMessage);
        }

        [Test]
        public void ShouldFailToAuthenticateAndLogIfUserServiceReturnsANullToken()
        {
            _mockTokenService.Setup(s => s.GetLoginTokenByGuid(It.IsAny<string>())).Returns((IUserToken) null);

            var crmService = new UserAuthenticationService(_mockLoginService.Object, _mockTokenService.Object, _mockUserService.Object,
                _mockCredentialsService.Object,_mockCrmService.Object, _dateTimeProvider);

            var authRes = crmService.AuthenticateToken("fakeName", "fakeToken");
            Assert.That(authRes.IsAuthenticated, Is.False);
            _testLog.AssertALogMessageContains(Level.Info, "could not get a token");
        }

        [Test]
        public void ShouldFailToAuthenticateAndLogIfUserServiceReturnsATokenForADifferentUsername()
        {
            const string username = "username";
            const string tokenUsername = "tokenUsername";
            var fakeToken = new UserToken
            {
                User = new User { Username = tokenUsername }
            };
            _mockTokenService.Setup(s => s.GetLoginTokenByGuid(It.IsAny<string>())).Returns(fakeToken);

            var crmService = new UserAuthenticationService(_mockLoginService.Object, _mockTokenService.Object, _mockUserService.Object,
                _mockCredentialsService.Object, _mockCrmService.Object, _dateTimeProvider);

            var authRes = crmService.AuthenticateToken(username, "ignoredForThisTest");
            Assert.That(authRes.IsAuthenticated, Is.False);
            _testLog.AssertALogMessageContains(Level.Info, "Incorrect username for that token");
            _testLog.AssertALogMessageContains(Level.Info, string.Format("token's username: {0}", tokenUsername));
            _testLog.AssertALogMessageContains(Level.Info, string.Format("passed username: {0}", username));
        }

        [TestCase("t1")]
        public void AuthenticateUserWithDeletedOrganisationShouldReturnFalse(string username)
        {
            const string correctPassword = "correctPass";

            var mockCredentials = new UserCredentials
            {
                Id = 9999,
                DateCreated = DateTime.Now,
                PasswordHashed = correctPassword,
                Expiration = DateTime.Now.AddMonths(1).ToUniversalTime(),
                FirstTimeLogin = false,
                // User = mockUser,
                // UserId = mockUser.Id
            };
            var mockUser = new User
            {
                Id = 989898,
                Username = username,
                IsBlocked = false,
                IsActive = true,
                IsDeleted = false
            };

            mockCredentials.User = mockUser;

            var mockLoginService = new Mock<IUserLoginTrackingService>();
            mockLoginService.Setup(a => a.RegisterLoginAttempt(989898, AomUserLoginSourceId)).Verifiable();

            var mockCrmService = new Mock<ICrmAdministrationService>();
            mockCrmService.Setup(s => s.BlockUser(It.IsAny<string>(), -1)).Returns(true).Verifiable();

            var mockCredentialsService = new Mock<ICredentialsService>();
            mockCredentialsService.Setup(c => c.GetUserCredentials(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(mockCredentials).Verifiable();
            
            var mockUserService = new Mock<IUserService>();

            var usersOrganisation = new Organisation { Id = 1, Name = "Nathans Organisation", IsDeleted = true };
            mockUserService.Setup(m => m.GetUsersOrganisation(It.IsAny<long>())).Returns(usersOrganisation);

            var crmService = new UserAuthenticationService(
                mockLoginService.Object,
                GetUserTokenService(SessionTokenString, UsersIpAddess),
                mockUserService.Object,
                mockCredentialsService.Object,
                mockCrmService.Object, _dateTimeProvider);

            var authenticationResult = crmService.AuthenticateUser(username, correctPassword, 1);

            Assert.That(authenticationResult.IsAuthenticated, Is.False);
        }

        //[Test]
        //public void RecordUsersIpShouldCallUserService()
        //{
        //    const string fakeName = "fakeName";
        //    const bool fakeLoginSuccess = true;
        //    const string fakeLoginFailureReason = "fakeFailureReason";

        //    _mockUserService.Setup(s => s.RecordLoginIpForUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>())).Returns(true);

        //    var uaService = new UserAuthenticationService(_mockLoginService.Object, _mockTokenService.Object, _mockUserService.Object,
        //        _mockCredentialsService.Object,_mockCrmService.Object, _dateTimeProvider);

        //    var res = uaService.RecordUsersIp(UsersIpAddess, fakeName, fakeLoginSuccess, fakeLoginFailureReason);
        //    Assert.IsTrue(res);
        //    _mockUserService.Verify(s => s.RecordLoginIpForUser(UsersIpAddess, fakeName, fakeLoginSuccess, fakeLoginFailureReason), Times.Once);
        //}
    }
}