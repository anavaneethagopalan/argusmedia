﻿using AOM.App.Domain.Dates;
using AOM.Services.ProductService;
using Moq;

namespace AOM.Services.Tests.CrmService
{
    using System;
    using System.Transactions;

    using AOM.App.Domain;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Crm;
    using AOM.Repository.MySql.Tests;
    using AOM.Repository.MySql.Tests.Crm;

    using NUnit.Framework;

    [TestFixture]
    public class CrmServiceBlockUserTests
    {

        DateTimeProvider _dateTimeProvider = new DateTimeProvider();
        [Test]
        public void ShouldRaiseExceptionIfNoUsernameSupplied()
        {
            var crmService = new Services.CrmService.CrmAdministrationService(new DbContextFactory(),_dateTimeProvider);
            ShouldRaiseBusinessRuleExceptionWithMessage(crmService, "Username is required", null);
        }

        [Test]
        public void ShouldRaiseExceptionIfUserDoesNotExists()
        {
            //var CrmAdministrationService = new Services.CrmAdministrationService.CrmAdministrationService(new DbContextFactory());
            var model = new MockCrmModel();
            var crmService = new Services.CrmService.CrmAdministrationService(new MockDbContextFactory(null, model),_dateTimeProvider);
            var username = "userwhodoesnotexist@notexists.com";

            var expectedErrorMessage = string.Format("User: {0} does not exist", username);

            ShouldRaiseBusinessRuleExceptionWithMessage(crmService, expectedErrorMessage, username);
        }


        [Test]
        public void ShouldBlockIfAllInputIsCorrectAndNotCurrentlyBlocked()
        {
            const string testingUserName = "testingUser";

            using (var tran = new TransactionScope())
            {
                UserInfoDto user;
                OrganisationDto org;
                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org)
                    .AddUser(testingUserName, out user)
                    .DBContext.SaveChangesAndLog(-1);

                var adminService = new Services.CrmService.CrmAdministrationService(new MockDbContextFactory(null, model),_dateTimeProvider);
                var userService = new Services.CrmService.UserService(new MockDbContextFactory(null, model),_dateTimeProvider, new Mock<IProductService>().Object);

                var usr = userService.GetUserInfoDto(testingUserName);
                usr.IsBlocked = false;

                adminService.BlockUser(testingUserName, -1);

                usr = userService.GetUserInfoDto(testingUserName);
                Assert.IsTrue(usr.IsBlocked, "Expected user to be blocked");

                tran.Dispose();
            }
        }

        [Test]
        public void ShouldBlockIfAllInputIsCorrectAndAlreadyCurrentlyBlocked()
        {
            const string testingUserName = "testingUser";

            using (var tran = new TransactionScope())
            {
                UserInfoDto user;
                OrganisationDto org;
                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org)
                    .AddUser(testingUserName, true, out user)
                    .DBContext.SaveChangesAndLog(-1);

                var crmService = new Services.CrmService.CrmAdministrationService(new MockDbContextFactory(null, model),_dateTimeProvider);
                var userService = new Services.CrmService.UserService(new MockDbContextFactory(null, model),_dateTimeProvider, new Mock<IProductService>().Object);

                var usrPriorToTest = userService.GetUserInfoDto(testingUserName);
                Assert.IsTrue(usrPriorToTest.IsBlocked, "Expected user to be blocked before we start test");

                crmService.BlockUser(testingUserName, -1); //should stay blocked

                var usr = userService.GetUserInfoDto(testingUserName);
                Assert.IsTrue(usr.IsBlocked, "Expected user to be blocked");

                tran.Dispose();
            }
        }

        private void ShouldRaiseBusinessRuleExceptionWithMessage(
            Services.CrmService.CrmAdministrationService crmAdministrationService,
            string expectedMessage,
            string username)
        {
            try
            {
                crmAdministrationService.BlockUser(username, -1);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is BusinessRuleException);
                Assert.That(ex.Message, Is.EqualTo(expectedMessage));
            }
        }

    }
}