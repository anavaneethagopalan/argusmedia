﻿using AOM.App.Domain.Services;
using AOM.Repository.MySql.Crm;
using AOM.Services.Tests.AuthenticationService;
using AOMDiffusion.Common;
using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Details;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class SessionHelperTests
    {
        [Test]
        public void ReturnsCorrectUserInfo()
        {
            var user = AuthenticationHelper.MakeUser(false, 1);
            user.Id = 1;
            user.Name = "TestUser";
            user.OrganisationId = 1;

            Mock<IUserService> mockUserService = new Mock<IUserService>();
            mockUserService.Setup(m => m.GetUserInfoDto(user.Name)).Returns(new UserInfoDto { Id = user.Id, Name = user.Name, Organisation_Id_fk = user.OrganisationId });
            SessionHelper sessionHelper = new SessionHelper(mockUserService.Object);

            Mock<ISessionDetails> mockSessionDetails = new Mock<ISessionDetails>();
            Mock<IClientSummary> clientSummary = new Mock<IClientSummary>();
            clientSummary.SetupGet(p => p.Principal).Returns("AOM-TestUser");
            mockSessionDetails.SetupGet(p => p.Summary).Returns(clientSummary.Object);

            UserInfoDto userInfoDto = sessionHelper.GetUserInfoDto(mockSessionDetails.Object);

            Assert.AreEqual(user.Name, userInfoDto.Name);
        }

        [Test]
        public void ReturnsNonAuthenticatedUser()
        {
            Mock<IUserService> mockUserService = new Mock<IUserService>();
            SessionHelper sessionHelper = new SessionHelper(mockUserService.Object);

            Mock<ISessionDetails> mockSessionDetails = new Mock<ISessionDetails>();
            Mock<IClientSummary> clientSummary = new Mock<IClientSummary>();
            clientSummary.SetupGet(p => p.Principal).Returns("AOM-TestUser");
            mockSessionDetails.SetupGet(p => p.Summary).Returns(clientSummary.Object);

            bool authenticated = sessionHelper.IsAuthenticationUser(mockSessionDetails.Object);

            Assert.IsTrue(!authenticated);
        }

        [Test]
        public void ReturnsAuthenticatedUser()
        {
            Mock<IUserService> mockUserService = new Mock<IUserService>();
            SessionHelper sessionHelper = new SessionHelper(mockUserService.Object);

            Mock<ISessionDetails> mockSessionDetails = new Mock<ISessionDetails>();
            Mock<IClientSummary> clientSummary = new Mock<IClientSummary>();
            clientSummary.SetupGet(p => p.Principal).Returns("AuthenticationAdmin");
            mockSessionDetails.SetupGet(p => p.Summary).Returns(clientSummary.Object);

            bool authenticated = sessionHelper.IsAuthenticationUser(mockSessionDetails.Object);

            Assert.IsTrue(authenticated);
        }
    }
}
