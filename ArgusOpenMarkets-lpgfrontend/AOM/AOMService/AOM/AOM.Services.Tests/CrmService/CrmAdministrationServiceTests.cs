﻿namespace AOM.Services.Tests.CrmService
{
    using System.Linq;

    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql.Crm;
    using AOM.Repository.MySql.Tests;
    using AOM.Repository.MySql.Tests.Crm;
    using AOM.Services.CrmService;

    using log4net.Core;

    using Moq;

    using NUnit.Framework;

    using Utils.Logging;

    [TestFixture]
    internal class CrmAdministrationServiceTests
    {
        [Test]
        public void ShouldReturnAListOfSystemPrivileges()
        {
            var mockModel = new MockCrmModel();
            

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var spDto = new SystemPrivilegeDto();
            CrmDataBuilder.WithModel(mockModel).AddSystemPrivilege("SYSTEM PRIV 1", "A", out spDto);

            mockModel.SaveChangesAndLog(-1);

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, mockModel),
                mockDateTimeProvider.Object);

            var sysPrivs = service.GetSystemPrivileges();

            Assert.IsNotNull(sysPrivs);
            Assert.That(sysPrivs.Count, Is.EqualTo(1));
        }

        [Ignore("ignore")]
        public void ShouldUpdateUserDetails()
        {
            var model = new MockCrmModel();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var userDto = new UserInfoDto() { Id = -1, IsActive = true, IsDeleted = true, Name = "AB" };

            CrmDataBuilder.WithModel(model).AddUser("AB", out userDto);

            model.SaveChangesAndLog(-1);

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, model),
                mockDateTimeProvider.Object);

            var userEntity = new User()
                             {
                                 Id = -1,
                                 IsActive = false,
                                 IsDeleted = false,
                                 Name = "Az",
                                 Email = "Az@am.com",
                                 Telephone = "123",
                                 Title = "Mr",
                                 ArgusCrmUsername = "un@am.com"
                             };

            var result = service.UpdateUserDetails(-1, userEntity, 1);


            Assert.IsTrue(result);
            //check if the dto has been updated properly
            Assert.That(userDto.IsActive, Is.EqualTo(userEntity.IsActive));
            Assert.That(userDto.IsDeleted, Is.EqualTo(userEntity.IsDeleted));
            Assert.That(userDto.Name, Is.EqualTo(userEntity.Name));
            Assert.That(userDto.Email, Is.EqualTo(userEntity.Email));
            Assert.That(userDto.Telephone, Is.EqualTo(userEntity.Telephone));
            Assert.That(userDto.Title, Is.EqualTo(userEntity.Title));
            Assert.That(userDto.ArgusCrmUsername, Is.EqualTo(userEntity.ArgusCrmUsername));
        }

        [Test]
        public void ShouldReturnAListOfProductPrivileges()
        {
            var model = new MockCrmModel();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            ProductPrivilegeDto prodPrivDto;
            CrmDataBuilder.WithModel(model).AddProductPrivelege("PP1", out prodPrivDto);
            model.SaveChangesAndLog(-1);

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, model),
                mockDateTimeProvider.Object);
            var prodPrivs = service.GetProductPrivileges();

            Assert.IsNotNull(prodPrivs);
            Assert.That(prodPrivs.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldReturnAListOfSystemRolePrivileges()
        {
            var model = new MockCrmModel();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            SystemRolePrivilegeDto systemRolePrivilegeDto;
            CrmDataBuilder.WithModel(model)
                .AddSystemRolePrivilege(1, 1, out systemRolePrivilegeDto)
                .AddSystemRolePrivilege(2, 2, out systemRolePrivilegeDto);

            model.SaveChangesAndLog(-1);

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, model),
                mockDateTimeProvider.Object);
            var systemRolePrivileges = service.GetSystemRolePrivileges(1);

            Assert.IsNotNull(systemRolePrivileges);
            Assert.That(systemRolePrivileges.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldAddSystemPrivilegeToRoleIfItDoesNotExist()
        {
            var model = new MockCrmModel();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, model),
                mockDateTimeProvider.Object);

            var systemRolePrivilege = new SystemRolePrivilege { PrivilegeId = 1, RoleId = 1 };
            var result = service.AddSystemPrivilegeToRole(systemRolePrivilege, 1);

            var systemRolePrivileges = service.GetSystemRolePrivileges(1);

            Assert.IsNotNull(systemRolePrivileges);
            Assert.That(systemRolePrivileges.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldNotAddTheSameSystemPrivilegeToRoleIfItAlreadyExists()
        {
            var model = new MockCrmModel();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, model),
                mockDateTimeProvider.Object);

            var systemRolePrivilege = new SystemRolePrivilege { PrivilegeId = 1, RoleId = 1 };
            service.AddSystemPrivilegeToRole(systemRolePrivilege, 1);

            var systemRolePrivileges = service.GetSystemRolePrivileges(1);

            Assert.IsNotNull(systemRolePrivileges);
            Assert.That(systemRolePrivileges.Count, Is.EqualTo(1));

            service.AddSystemPrivilegeToRole(systemRolePrivilege, 1);

            Assert.IsNotNull(systemRolePrivileges);
            Assert.That(systemRolePrivileges.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldNotDeleteASystemPrivilegeFromRoleIfNotAssigned()
        {
            var model = new MockCrmModel();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, model),
                mockDateTimeProvider.Object);

            var systemRolePrivilege = new SystemRolePrivilege { PrivilegeId = 1, RoleId = 1 };
            var result = service.RemoveSystemPrivilegeFromRole(systemRolePrivilege, 1);

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldSuccessfullyDeleteASystemPrivilegeFromRoleIfExists()
        {
            var model = new MockCrmModel();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, model),
                mockDateTimeProvider.Object);

            var systemRolePrivilege = new SystemRolePrivilege { PrivilegeId = 1, RoleId = 1 };
            service.AddSystemPrivilegeToRole(systemRolePrivilege, 1);

            var systemRolePrivileges = service.GetSystemRolePrivileges(1);

            Assert.IsNotNull(systemRolePrivileges);
            Assert.That(systemRolePrivileges.Count, Is.EqualTo(1));

            service.RemoveSystemPrivilegeFromRole(systemRolePrivilege, 1);
            systemRolePrivileges = service.GetSystemRolePrivileges(1);

            Assert.IsNotNull(systemRolePrivileges);
            Assert.That(systemRolePrivileges.Count, Is.EqualTo(0));
        }

        [Test]
        public void ShouldNotSaveAnOrganisationIfTheOrganisationServiceValidationFails()
        {
            var testLogAppender = new MemoryAppenderForTests();
            var model = new MockCrmModel();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, model),
                mockDateTimeProvider.Object);

            var mockOrganisationService = new Mock<IOrganisationService>();
            const string fakeErrorMessage = "unit test fake save error message";
            mockOrganisationService.Setup(s => s.ValidateOrganisation(It.IsAny<IOrganisation>()))
                .Throws(new BusinessRuleException(fakeErrorMessage));

            var ret = service.SaveOrganisation(new Organisation(), -1, mockOrganisationService.Object);
            Assert.That(ret, Is.EqualTo(-1));
            testLogAppender.AssertALogMessageContains(Level.Warn, fakeErrorMessage);
        }

        [Test]
        public void ShouldRemoveSubscribedProductWhenRemovingTheLastSubscribedProductPrivilegeForProductAndOrganisation()
        {
            var mockModel = new MockCrmModel();

            SubscribedProductDto spDto;
            SubscribedProductPrivilegeDto sppDto;
            var productId = 1;
            var productPrivilegeId = 1;

            var orgDto = new OrganisationDto { Id = 1, Name = "Org1", IsDeleted = false };
            CrmDataBuilder.WithModel(mockModel)
                .AddSubscribedProduct(orgDto, productId, out spDto)
                .AddSubscribedProductPrivilege(orgDto.Id, productId, productPrivilegeId, out sppDto);

            mockModel.SaveChangesAndLog(-1);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, mockModel),
                mockDateTimeProvider.Object);

            var subscribedProducts = mockModel.SubscribedProducts.ToList();
            Assert.That(subscribedProducts.Count, Is.EqualTo(1));

            service.RemoveProductPrivilegeFromSubscribedProductPrivileges(1, 1, 1, 1);

            subscribedProducts = mockModel.SubscribedProducts.ToList();
            Assert.That(subscribedProducts.Count, Is.EqualTo(0));
        }

        [Test]
        public void ShouldNotRemoveSubscribedProductWhenRemovingASubscribedProductPrivilegeAndOthersRemain()
        {
            var mockModel = new MockCrmModel();

            SubscribedProductDto spDto;
            SubscribedProductPrivilegeDto sppDto;
            var productId = 1;
            var productPrivilegeId = 1;

            var orgDto = new OrganisationDto { Id = 1, Name = "Org1", IsDeleted = false };
            CrmDataBuilder.WithModel(mockModel)
                .AddSubscribedProduct(orgDto, productId, out spDto)
                .AddSubscribedProductPrivilege(orgDto.Id, productId, productPrivilegeId, out sppDto)
                .AddSubscribedProductPrivilege(orgDto.Id, productId, 2, out sppDto);

            mockModel.SaveChangesAndLog(-1);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, mockModel),
                mockDateTimeProvider.Object);

            var subscribedProducts = mockModel.SubscribedProducts.ToList();
            Assert.That(subscribedProducts.Count, Is.EqualTo(1));

            service.RemoveProductPrivilegeFromSubscribedProductPrivileges(1, 1, 1, 1);

            subscribedProducts = mockModel.SubscribedProducts.ToList();
            Assert.That(subscribedProducts.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldReturnAListOfSystemPrivilegesFilteredForTheOrganisationType()
        {
            var mockModel = new MockCrmModel();

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var spDto = new SystemPrivilegeDto();
            var spDto2 = new SystemPrivilegeDto();
            var spDto3 = new SystemPrivilegeDto();
            var spDto4 = new SystemPrivilegeDto();

            CrmDataBuilder.WithModel(mockModel)
                .AddSystemPrivilege("SYSTEM PRIV 1", "T", out spDto)
                .AddSystemPrivilege("SYSTEM PRIV 2", "", out spDto2)
                .AddSystemPrivilege("SYSTEM PRIV 3", "A", out spDto3)
                .AddSystemPrivilege("SYSTEM PRIV 4", "B", out spDto4);

            mockModel.SaveChangesAndLog(-1);

            var service = new CrmAdministrationService(
                new MockDbContextFactory(null, mockModel),
                mockDateTimeProvider.Object);

            var sysPrivs = service.GetSystemPrivileges(OrganisationType.Brokerage);

            Assert.IsNotNull(sysPrivs);
            Assert.That(sysPrivs.Count, Is.EqualTo(2));
        }
    }
}