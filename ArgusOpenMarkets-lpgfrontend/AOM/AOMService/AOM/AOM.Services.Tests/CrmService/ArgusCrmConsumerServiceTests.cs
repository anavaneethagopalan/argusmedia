﻿namespace AOM.Services.Tests.CrmService
{
    using System;
    using System.Data;
    using System.Linq;

    using AOM.App.Domain.Entities;
    using AOM.Repository.Oracle;
    using AOM.Services.CrmService;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class ArgusCrmConsumerServiceTests
    {

        private AomUserModuleViewEntity testModel = new AomUserModuleViewEntity()
                                                    {
                                                        UserName = "testUser",
                                                        ModuleId = 123,
                                                        Description = "descript",
                                                        Stream = "stream",
                                                        StreamId = 001,
                                                        UserOracleCrmId = 9
                                                    };

        [Test]
        public void GetLatestData_ShoulReturnModulesTest()
        {
            var isReadEnabled = true;
            var mockReader = new Mock<IOracleReader>();
            var mockDataReader = new Mock<IDataReader>();
            mockDataReader.Setup(a => a.Read()).Returns(() => isReadEnabled).Callback(() => isReadEnabled = false);

            mockDataReader.Setup(a => a.IsDBNull(It.IsAny<int>())).Returns(false);

            mockDataReader.Setup(a => a.GetString(0)).Returns(testModel.UserName);
            mockDataReader.Setup(a => a.GetInt64(1)).Returns(testModel.ModuleId);
            mockDataReader.Setup(a => a.GetString(2)).Returns(testModel.Description);
            mockDataReader.Setup(a => a.GetString(3)).Returns(testModel.Stream);
            mockDataReader.Setup(a => a.GetInt64(4)).Returns((long)testModel.StreamId);
            mockDataReader.Setup(a => a.GetInt64(5)).Returns((long)testModel.UserOracleCrmId);

            mockReader.Setup(a => a.ExecuteSqlReader(It.IsAny<String>())).Returns(mockDataReader.Object);

            var service = new ArgusCrmConsumerService(mockReader.Object);

            var allModules = service.GetLatestData("connectionString");

            Assert.That(allModules.Count(), Is.EqualTo(1));

            var result = allModules.FirstOrDefault();

            Assert.That(result, Is.Not.Null);
            //check if the data returned is the same as in the testModel
            Assert.That(result.UserName, Is.EqualTo(testModel.UserName));
            Assert.That(result.ModuleId, Is.EqualTo(testModel.ModuleId));
            Assert.That(result.Description, Is.EqualTo(testModel.Description));
            Assert.That(result.Stream, Is.EqualTo(testModel.Stream));
            Assert.That(result.StreamId, Is.EqualTo(testModel.StreamId));
            Assert.That(result.UserOracleCrmId, Is.EqualTo(testModel.UserOracleCrmId));

        }


        [Test]
        public void GetLatestDataTest_ShouldReturnNoModulesIfNoneInArgusDbTest()
        {
            var mockReader = new Mock<IOracleReader>();
            var mockDataReader = new Mock<IDataReader>();

            //IDataReader.Read will return false and never go into the while loop
            mockDataReader.Setup(a => a.Read()).Returns(() => false).Verifiable();

            mockDataReader.Setup(a => a.GetString(0)).Returns(testModel.UserName).Verifiable();

            mockReader.Setup(a => a.ExecuteSqlReader(It.IsAny<String>())).Returns(mockDataReader.Object);

            var service = new ArgusCrmConsumerService(mockReader.Object);

            var result = service.GetLatestData("connectionString").FirstOrDefault();

            Assert.That(result, Is.Null);
            mockDataReader.Verify(a => a.GetString(0), Times.Never);
            mockDataReader.Verify(a => a.Read(), Times.Exactly(1));
        }

    }
}