﻿using AOM.App.Domain;
using AOM.App.Domain.Interfaces;
using AOM.Services.CrmService;
using Moq;
using NUnit.Framework;
using System;

namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class OrganisationValidatorTests
    {
        private Mock<IOrganisation> _mockOrg;

        [SetUp]
        public void Setup()
        {
            _mockOrg = new Mock<IOrganisation>();
            _mockOrg.Setup(o => o.Name).Returns("A valid name");
            _mockOrg.Setup(o => o.ShortCode).Returns("COD");
            _mockOrg.Setup(o => o.Email).Returns("notreal@nodomain.com");
        }

        [Test]
        public void ValidateOrganisationShouldThrowIfOrganisationNull()
        {
            Assert.Throws<ArgumentNullException>(
                () => OrganisationValidator.ValidateOrganisation(null));
        }

        [Test]
        public void ValidateOrganisationShouldNotThrowIfOrganisationValid()
        {
            Assert.DoesNotThrow(
                () => OrganisationValidator.ValidateOrganisation(_mockOrg.Object));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ValidateOrganisationShouldThrowIfNameIsEmpty(string name)
        {
            _mockOrg.Setup(o => o.Name).Returns(name);
            var e = Assert.Throws<BusinessRuleException>(
                () => OrganisationValidator.ValidateOrganisation(_mockOrg.Object));
            StringAssert.Contains("Name must not be empty", e.Message);
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ValidateOrganisationShouldThrowIfShortCodeIsEmpty(string shortCode)
        {
            _mockOrg.Setup(o => o.ShortCode).Returns(shortCode);
            var e = Assert.Throws<BusinessRuleException>(
                () => OrganisationValidator.ValidateOrganisation(_mockOrg.Object));
            StringAssert.Contains("ShortCode must not be empty", e.Message);
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ValidateOrganisationShouldThrowIfEmailIsEmpty(string email)
        {
            _mockOrg.Setup(o => o.Email).Returns(email);
            var e = Assert.Throws<BusinessRuleException>(
                () => OrganisationValidator.ValidateOrganisation(_mockOrg.Object));
            StringAssert.Contains("Organisation must have an email associated with it", e.Message);
        }
    }
}
