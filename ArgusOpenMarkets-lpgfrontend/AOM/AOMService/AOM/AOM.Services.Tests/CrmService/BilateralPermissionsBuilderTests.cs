﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;
using AOM.Services.CrmService;
using NUnit.Framework;

namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class BilateralPermissionsBuilderTests
    {
        private BilateralPermissionsBuilder _permissionsBuilder;

        private const long _ourOrgId = 1;
        private const long _theirOrgId = 2;
        private const long _productId = 1;
        private const long _marketId = 1;

        private Dictionary<long, string> _userCache;
        private List<long> _ourOrganisations;
        private List<long> _allOrganisations;

        [SetUp]
        public void Setup()
        {
            _permissionsBuilder = new BilateralPermissionsBuilder();
            _userCache = new Dictionary<long, string>();
            _ourOrganisations = new List<long> { _ourOrgId };
            _allOrganisations = new List<long> { _ourOrgId, _theirOrgId };
        }

        [Test]
        [TestCase(null, null, null, null,
                  Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.NotSet)]
        [TestCase(Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.NotSet,
                  Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.NotSet)]
        [TestCase(Permission.Allow, null, null, null,
                  Permission.Allow, Permission.NotSet, Permission.NotSet, Permission.NotSet)]
        [TestCase(null, Permission.Allow, null, null,
                  Permission.NotSet, Permission.Allow, Permission.NotSet, Permission.NotSet)]
        [TestCase(null, null, Permission.Allow, null,
                  Permission.NotSet, Permission.NotSet, Permission.Allow, Permission.NotSet)]
        [TestCase(null, null, null, Permission.Allow,
                  Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.Allow)]
        [TestCase(Permission.Allow, Permission.Allow, null, null,
                  Permission.Allow, Permission.Allow, Permission.NotSet, Permission.NotSet)]
        [TestCase(null, Permission.Allow, null, Permission.Allow,
                  Permission.NotSet, Permission.Allow, Permission.NotSet, Permission.Allow)]
        [TestCase(Permission.Allow, Permission.Deny, Permission.Deny, Permission.Allow,
                  Permission.Allow, Permission.Deny, Permission.Deny, Permission.Allow)]
        [TestCase(Permission.NotSet, Permission.Deny, Permission.Allow, Permission.Allow,
                  Permission.NotSet, Permission.Deny, Permission.Allow, Permission.Allow)]
        public void GetIntraOrganisationPairs_CounterpartyPermissionsTest(Permission? ourBuySide, Permission? theirBuySide, Permission? ourSellSide, Permission? theirSellSide,
            Permission expectedOurBuySide, Permission expectedTheirSellSide, Permission expectedOurSellSide, Permission expectedTheirBuySide)
        {

            var orgPermissionsForProduct = BuildOrgPermissionsForProduct(ourBuySide, theirBuySide, ourSellSide, theirSellSide);
            var organisationList = BuildOrganisations(_allOrganisations);

            var permissions = _permissionsBuilder.GetIntraOrganisationPairs(_userCache, orgPermissionsForProduct,
                _ourOrganisations, _allOrganisations, _productId, _marketId, organisationList);

            var permission =
                permissions.FirstOrDefault(
                    p => p.ProductId == _productId && p.MarketId == _marketId && p.OrgId == _theirOrgId);
            Assert.That(permission, Is.Not.Null);
            Assert.That(permission.BuySide.OurPermAllowOrDeny, Is.EqualTo(expectedOurBuySide));
            Assert.That(permission.BuySide.TheirPermAllowOrDeny, Is.EqualTo(expectedTheirBuySide));
            Assert.That(permission.SellSide.OurPermAllowOrDeny, Is.EqualTo(expectedOurSellSide));
            Assert.That(permission.SellSide.TheirPermAllowOrDeny, Is.EqualTo(expectedTheirSellSide));
        }

        private List<IIntraOrganisationPermission> BuildOrgPermissionsForProduct(Permission? ourBuySide, Permission? theirBuySide, Permission? ourSellSide, Permission? theirSellSide)
        {
            var permissions = new List<CounterpartyPermissionDto>();

            if (ourBuySide != null)
            {
                permissions.Add(new CounterpartyPermissionDto
                {
                    Id = 1,
                    ProductId = 1,
                    OurOrganisation = 1,
                    BuyOrSell = "B",
                    TheirOrganisation = 2,
                    AllowOrDeny = ourBuySide.Value.ToDtoFormat()
                });
            }
            if (theirBuySide != null)
            {
                permissions.Add(new CounterpartyPermissionDto
                {
                    Id = 1,
                    ProductId = 1,
                    OurOrganisation = 2,
                    BuyOrSell = "S",
                    TheirOrganisation = 1,
                    AllowOrDeny = theirBuySide.Value.ToDtoFormat()
                });
            }
            if (ourSellSide != null)
            {
                permissions.Add(new CounterpartyPermissionDto
                {
                    Id = 1,
                    ProductId = 1,
                    OurOrganisation = 1,
                    BuyOrSell = "S",
                    TheirOrganisation = 2,
                    AllowOrDeny = ourSellSide.Value.ToDtoFormat()
                });
            }
            if (theirSellSide != null)
            {
                permissions.Add(new CounterpartyPermissionDto
                {
                    Id = 1,
                    ProductId = 1,
                    OurOrganisation = 2,
                    BuyOrSell = "B",
                    TheirOrganisation = 1,
                    AllowOrDeny = theirSellSide.Value.ToDtoFormat()
                });
            }

            return permissions.Cast<IIntraOrganisationPermission>().ToList();
        }

        private List<OrganisationDto> BuildOrganisations(List<long> allOrganisations)
        {
            var orgList = new List<OrganisationDto>();

            foreach (var orgId in allOrganisations)
            {
                orgList.Add(new OrganisationDto
                {
                    Id = orgId,
                    ShortCode = "ORG" + orgId,
                    Name = "Organisation" + orgId,
                    LegalName = "Organisation" + orgId,
                    OrganisationType = "T"
                });
            }

            return orgList;
        }
    }
}
