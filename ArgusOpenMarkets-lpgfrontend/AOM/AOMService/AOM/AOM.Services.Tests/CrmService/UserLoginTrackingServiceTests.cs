﻿using System.Configuration;
using System.Linq;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.CrmService;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class UserLoginTrackingServiceTests
    {
        [Test]
        public void ShouldReturnTrueWhenUserHasRegistered3Times()
        {
            var allowedNumberOfAttempts = 3;
            ConfigurationManager.AppSettings["MaxNumberOFLoginAttempts"] = allowedNumberOfAttempts.ToString();

            //string username = "ja";
            long userId = 1;

            //var mockUserService = new Mock<IUserService>();
            //mockUserService.Setup(s => s.GetUserId(It.IsAny<string>())).Returns(1);

            var mockDbContext = new MockDbContextFactory(null, new MockCrmModel());
            var service = new UserLoginTrackingService(mockDbContext);
                //mockUserService.Object);

            for (int i = 0; i < allowedNumberOfAttempts; i++)
            {
                service.RegisterLoginAttempt(userId, 1);

                var blocked = service.IsUserAtLoginAttemptLimit(userId, 1);

                if (allowedNumberOfAttempts-1 > i)
                    Assert.IsFalse(blocked);
                else
                    Assert.IsTrue(blocked);      
            }
        }

        [Test]
        public void ResetUserTest()
        {
            //string username = "ja";
            //var mockUserService = new Mock<IUserService>();
            //mockUserService.Setup(s => s.GetUserId(It.IsAny<string>())).Returns(1);
            long userId = 1;
            long userLoginSourceId = 1;
            var mockCrmModel = new MockCrmModel();
            var mockDbContext = new MockDbContextFactory(null, mockCrmModel);

            var service = new UserLoginTrackingService(mockDbContext);
                //mockUserService.Object);

            service.RegisterLoginAttempt(userId, userLoginSourceId);
            Assert.AreEqual(mockCrmModel.UserLoginAttempts.Single(s => s.UserId == 1).NumFailedLogins, 1);
            service.ResetUserAccount(userId, userLoginSourceId);
            Assert.AreEqual(mockCrmModel.UserLoginAttempts.Single(s => s.UserId == 1).NumFailedLogins, 0);
        }
    }
}