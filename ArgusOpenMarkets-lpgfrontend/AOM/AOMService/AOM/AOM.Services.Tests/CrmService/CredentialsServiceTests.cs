using AOM.Services.CrmService;
using System;
using AOM.App.Domain.Dates;
using AOM.Repository.MySql;
using Moq;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Crm;
using NUnit.Framework;

namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class CredentialsServiceTests
    {
        private const string Username = "t1";
        private readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();
        private const string CorrectPassword = "correctPass";

        [Test]
        public void GetCorrectCredentialsTest()
        {
            var model = new MockCrmModel();

            UserInfoDto userDto;
            UserCredentialsDto credentialsDto;
            CrmDataBuilder.WithModel(model).AddUser(Username, out userDto, out credentialsDto);
            credentialsDto.CredentialType = "P";
            model.SaveChangesAndLog(-1);

            var service = new Services.CrmService.CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            var userFromDb = service.GetUserCredentials(Username);

            Assert.IsNotNull(userFromDb);
            Assert.AreEqual(Username, userFromDb.User.Name);
        }

        [Test]
        public void ReturnNullForNonExistingUserTest()
        {
            var model = new MockCrmModel();
            const string fakeUsername = "bla-bla";

            var service = new Services.CrmService.CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            var credentials = service.GetUserCredentials(fakeUsername);
            Assert.That(credentials, Is.Null);
        }

        [Test]
        public void ShouldUpdateCredentialsForUserTest()
        {
            var model = new MockCrmModel();
            var oldPassword = CorrectPassword;
            var newPassword = "ThisIsANewPassword";

            UserInfoDto userDto;
            UserCredentialsDto credentialsDto;
            CrmDataBuilder.WithModel(model).AddUser(Username, out userDto, out credentialsDto);
            credentialsDto.CredentialType = "P";
            credentialsDto.FirstTimeLogin = true;
            credentialsDto.PasswordHashed = oldPassword;

            var service = new CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            var saveChangesCallesCound = model.SaveChangesCalledCount;
            service.UpdateUserCredentials(userDto.Id, oldPassword, newPassword, false, false, -1, true);

            //save changes should be called again after UpdateUserCredentials
            Assert.That(model.SaveChangesCalledCount, Is.EqualTo(saveChangesCallesCound + 1));
        }

        [Test]
        public void CanUpdateCredentialsWithCorrectOldPassword()
        {
            var model = new MockCrmModel();
            var oldPassword = CorrectPassword;
            var newPassword = "ThisIsANewPassword";

            UserInfoDto userDto;
            UserCredentialsDto credentialsDto;
            CrmDataBuilder.WithModel(model).AddUser(Username, out userDto, out credentialsDto);
            credentialsDto.CredentialType = "P";
            credentialsDto.FirstTimeLogin = true;
            credentialsDto.PasswordHashed = CorrectPassword;

            var service = new CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            var saveChangesCallesCound = model.SaveChangesCalledCount;

            service.UpdateUserCredentials(userDto.Id, oldPassword, newPassword, true, false, -1, true);
            
            Assert.That(model.SaveChangesCalledCount, Is.EqualTo(saveChangesCallesCound + 1));
        }

        [Test]
        public void CannotUpdateCredentialsWithIncorrectOldPassword()
        {
            var model = new MockCrmModel();
            var oldPassword = "ThisIsIncorrect";
            var newPassword = "ThisIsANewPassword";

            UserInfoDto userDto;
            UserCredentialsDto credentialsDto;
            CrmDataBuilder.WithModel(model).AddUser(Username, out userDto, out credentialsDto);
            credentialsDto.CredentialType = "P";
            credentialsDto.FirstTimeLogin = true;
            credentialsDto.PasswordHashed = CorrectPassword;

            var service = new CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            var saveChangesCallesCound = model.SaveChangesCalledCount;
            
            Exception ex = Assert.Throws(typeof(AuthenticationErrorException), () => service.UpdateUserCredentials(userDto.Id, oldPassword, newPassword, true, false, -1, true));
            
            Assert.That(ex.Message.Contains("Old password does not match"));
        }

        [Test]
        public void ShouldThowExceptionForExpiredPasswordTest()
        {
            var model = new MockCrmModel();
            var oldPassword = CorrectPassword;
            const string newPassword = "ThisIsANewPassword";

            UserInfoDto userDto;
            UserCredentialsDto credentialsDto;
            CrmDataBuilder.WithModel(model).AddUser(Username, out userDto, out credentialsDto);
            credentialsDto.CredentialType = "P";

            //password expired a few mins ago
            credentialsDto.Expiration = _dateTimeProvider.Now.AddMinutes(-3).ToUniversalTime();

            var service = new CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            var saveChangesCallesCound = model.SaveChangesCalledCount;

            Assert.Throws(typeof(AuthenticationErrorException), () => service.UpdateUserCredentials(userDto.Id, oldPassword, newPassword, false, false, -1, true));
            //save changes should not have been called
            Assert.That(model.SaveChangesCalledCount, Is.EqualTo(saveChangesCallesCound));
        }

        [Test]
        public void ShouldNotThowExceptionIfNotFirstTimeLoginTest()
        {
            var model = new MockCrmModel();
            var oldPassword = CorrectPassword;
            var newPassword = "ThisIsANewPassword";
            
            UserInfoDto userDto;
            UserCredentialsDto credentialsDto;
            CrmDataBuilder.WithModel(model).AddUser(Username, out userDto, out credentialsDto);
            credentialsDto.CredentialType = "P";

            credentialsDto.FirstTimeLogin = false;
            credentialsDto.PasswordHashed = oldPassword;

            var service = new CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            var saveChangesCallesCound = model.SaveChangesCalledCount;

            Assert.DoesNotThrow(() => service.UpdateUserCredentials(userDto.Id, oldPassword, newPassword, false, false, -1, true));
            Assert.IsTrue(model.SaveChangesCalledCount != saveChangesCallesCound);
        }

        [Test]
        public void ShouldRaiseBusinessRuleExceptionIfNoPasswordSupplied()
        {
            var model = new MockCrmModel();
            var service = new Services.CrmService.CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            ShouldRaiseBusinessRuleExceptionWithMessage(service, "New password is required", "A", string.Empty);
        }

        [Test]
        public void ShouldRaiseBusinessRuleExceptionIfNoNewPasswordSupplied()
        {
            var model = new MockCrmModel();
            var service = new Services.CrmService.CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            ShouldRaiseBusinessRuleExceptionWithMessage(service, "New password is required", "A", "B");
        }

        [Test]
        public void CreateTempUserCredentialsException()
        {
            var model = new MockCrmModel();
            var service = new Services.CrmService.CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);
            Assert.Throws<NullReferenceException>(() => service.CreateTempUserCredentials(string.Empty, string.Empty));
        }

        [Test]
        public void CreateTempUserCredentials()
        {
            var contextFactory = new Mock<IDbContextFactory>();
            var crmModel = new MockCrmModel();

            contextFactory.Setup(c => c.CreateCrmModel()).Returns(() => crmModel);

            var model = new MockCrmModel();
            var service = new Services.CrmService.CredentialsService(new MockDbContextFactory(null, model), _dateTimeProvider);

            Assert.AreEqual(true, service.CreateTempUserCredentials("test", string.Empty));
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void ShoulReturnNullForDeletedUserIdIfReturnDeletedFalse(bool isTempCredentials)
        {
            var service = new CredentialsService(new MockDbContextFactory(null, CreateMockCrmModelForDeletedUserTests()), _dateTimeProvider);
            var credentials = service.GetUserCredentials(_deletedUser.Id,isTempCredentials, returnDeleted: false);
            Assert.IsNull(credentials);
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void ShoulReturnNullForDeletedUserNameIfReturnDeletedFalse(bool isTempCredentials)
        {
            var service = new CredentialsService(new MockDbContextFactory(null, CreateMockCrmModelForDeletedUserTests()), _dateTimeProvider);
            var credentials = service.GetUserCredentials(_deletedUser.Username, isTempCredentials, returnDeleted: false);
            Assert.IsNull(credentials);            
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void ShoulReturnCredentialsForDeletedUserIdIfReturnDeletedTrue(bool isTempCredentials)
        {
            var service = new CredentialsService(new MockDbContextFactory(null, CreateMockCrmModelForDeletedUserTests()), _dateTimeProvider);
            var credentials = service.GetUserCredentials(_activeUser.Id, isTempCredentials, returnDeleted: true);
            Assert.NotNull(credentials);
            Assert.That(credentials.User.Id, Is.EqualTo(_activeUser.Id));
            Assert.That(credentials.CredentialType, Is.EqualTo(isTempCredentials ? CredentialType.Temporary : CredentialType.Permanent));
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void ShoulReturnCredentialsForDeletedUserNameIfReturnDeletedTrue(bool isTempCredentials)
        {
            var service = new CredentialsService(new MockDbContextFactory(null, CreateMockCrmModelForDeletedUserTests()), _dateTimeProvider);
            var credentials = service.GetUserCredentials(_activeUser.Username,isTempCredentials, returnDeleted: true);
            Assert.NotNull(credentials);
            Assert.That(credentials.User.Id, Is.EqualTo(_activeUser.Id));
            Assert.That(credentials.CredentialType, Is.EqualTo(isTempCredentials ? CredentialType.Temporary : CredentialType.Permanent));
        }

        private readonly UserInfoDto _deletedUser = new UserInfoDto
        {
            Id = 123,
            IsDeleted = true,
            Username = "Deleted User"
        };

        private readonly UserInfoDto _activeUser = new UserInfoDto
        {
            Id = 234,
            IsDeleted = false,
            Username = "Not a deleted user"
        };

        private UserCredentialsDto CreateDeletedUserCredentials(string credentialsType)
        {
            return new UserCredentialsDto
            {
                User = _deletedUser,
                CredentialType = credentialsType
            };
        }

        private UserCredentialsDto CreateNotDeletedUserCredentials(string credentialsType)
        {
            return new UserCredentialsDto
            {
                User = _activeUser,
                CredentialType = credentialsType
            };
        }

        private MockCrmModel CreateMockCrmModelForDeletedUserTests()
        {
            var m = new MockCrmModel();
            m.UserCredentials.Add(CreateDeletedUserCredentials("T"));
            m.UserCredentials.Add(CreateDeletedUserCredentials("P"));
            m.UserCredentials.Add(CreateNotDeletedUserCredentials("T"));
            m.UserCredentials.Add(CreateNotDeletedUserCredentials("P"));
            return m;
        }

        private void ShouldRaiseBusinessRuleExceptionWithMessage(Services.CrmService.CredentialsService crmService, string expectedMessage, 
            string username = "", string password = "", string newPassword = "", bool adminRequest = false)
        {
            try
            {
                //crmService.ChangePassword(username, password, newPassword, adminRequest);
                crmService.UpdateUserCredentials(1, password, newPassword, false, true, -1, adminRequest);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is BusinessRuleException);
                Assert.That(ex.Message, Is.EqualTo(expectedMessage));
            }
        }
    }
}