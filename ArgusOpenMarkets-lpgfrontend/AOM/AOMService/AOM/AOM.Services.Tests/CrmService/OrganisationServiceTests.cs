﻿using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.CrmService;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Interfaces;
using Moq;

namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class OrganisationServiceTests
    {
        private MockAomModel _mockAomModel;
        private MockCrmModel _mockCrmModel;

        [SetUp]
        public void Setup()
        {
            _mockAomModel = new MockAomModel();
            _mockCrmModel = new MockCrmModel();
            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddDeliveryLocation("DeliveryLocation1")
                .AddProduct("Product1")
                .AddTenorCode("Tenor1")
                .AddProductTenor("Product1_Tenor1")
                .DBContext.SaveChangesAndLog(-1);
        }

        [Test]
        public void GetCounterpartyPermissionsNoCounterparties()
        {
            CrmDataBuilder.WithModel(_mockCrmModel);
            AomDataBuilder.WithModel(_mockAomModel);
            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> counterpartyMatrix = organisationService.GetAllCounterpartyPermissions();
            Assert.That(counterpartyMatrix.Count.Equals(0));
        }

        [Test]
        public void GetCounterpartyPermissionsOneCounterparties()
        {
            OrganisationDto org;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> counterpartyMatrix = organisationService.GetAllCounterpartyPermissions();
            Assert.That(counterpartyMatrix.Count.Equals(0));
        }

        [Test]
        public void GetCounterpartyPermissionsMultipleCounterparties()
        {
            OrganisationDto org;
            OrganisationDto org2;
            UserInfoDto usr;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("b", out org2)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> counterpartyMatrix = organisationService.GetAllCounterpartyPermissions();
            Assert.That(counterpartyMatrix.Count.Equals(2));
        }
        
        [Test]
        public void GetCommonCounterparties_Sell()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInBrokerage;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1,OrganisationType.Brokerage)
                .AddUser("Test User", false, out usrInBrokerage, bro1)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)

                .AddBrokerPermission(tro1, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro2, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro2, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInBrokerage.Id, subscribedProduct.Product_Id_fk, tro1.Id, null, BuyOrSell.Sell);
            Assert.AreEqual(1, orgs.Count());
            Assert.AreEqual(tro2.Id, orgs.First().Id);
        }

        [Test]
        public void CoBrokerableGetCommonCounterparties_Sell_OrgBroker_NoCommon()
        {
            //https://argusmedia.atlassian.net/browse/AOMK-261
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            OrganisationDto bro2;
            UserInfoDto usrInBrokerage2;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddUser("Test User", false, out usrInBrokerage2, bro1)
                .AddOrganisation("bro2", out bro2, OrganisationType.Brokerage)

                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddSubscribedProduct(bro2, _mockAomModel.Products.First().Id, out subscribedProduct)

                .AddBrokerBuySellPermission(tro1, bro1, subscribedProduct.Product_Id_fk)
                .AddBrokerBuySellPermission(bro1, tro1, subscribedProduct.Product_Id_fk)

                .AddBrokerBuySellPermission(tro2, bro1, subscribedProduct.Product_Id_fk, Permission.Deny)
                .AddBrokerBuySellPermission(bro1, tro2, subscribedProduct.Product_Id_fk, Permission.Deny)

                .AddBrokerBuySellPermission(tro2, bro2, subscribedProduct.Product_Id_fk)
                .AddBrokerBuySellPermission(bro2, tro2, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro2, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInBrokerage2.Id, subscribedProduct.Product_Id_fk, tro1.Id, bro1.Id, BuyOrSell.Sell);
            Assert.AreEqual(0, orgs.Count());          
        }

        [Test]
        public void CoBrokerableGetCommonCounterparties_Sell_Multi_OrgBroker_InCommon()
        {
            //https://argusmedia.atlassian.net/browse/AOMK-261
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            OrganisationDto bro2;
            UserInfoDto usrInBrokerage2;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddUser("Test User", false, out usrInBrokerage2, bro1)
                .AddOrganisation("bro2", out bro2, OrganisationType.Brokerage)

                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddSubscribedProduct(bro2, _mockAomModel.Products.First().Id, out subscribedProduct)

                .AddBrokerBuySellPermission(tro1, bro1, subscribedProduct.Product_Id_fk)
                .AddBrokerBuySellPermission(bro1, tro1, subscribedProduct.Product_Id_fk)

                .AddBrokerBuySellPermission(tro2, bro1, subscribedProduct.Product_Id_fk)
                .AddBrokerBuySellPermission(bro1, tro2, subscribedProduct.Product_Id_fk)
                
                .AddBrokerBuySellPermission(tro2, bro2, subscribedProduct.Product_Id_fk)                
                .AddBrokerBuySellPermission(bro2, tro2, subscribedProduct.Product_Id_fk)
               
                .AddCounterpartyPermission(tro2, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInBrokerage2.Id, subscribedProduct.Product_Id_fk, tro1.Id, bro1.Id, BuyOrSell.Sell);
            Assert.AreEqual(1, orgs.Count());
            Assert.AreEqual(tro2.Id, orgs.First().Id);
            
        }


        [Test]
        public void GetCommonCounterparties_Buy()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInBrokerage;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddUser("Test User", false, out usrInBrokerage, bro1)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)

                .AddBrokerPermission(tro1, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro2, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro2, tro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro1, tro2, BuyOrSell.Sell, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInBrokerage.Id, subscribedProduct.Product_Id_fk, tro1.Id, null, BuyOrSell.Buy);
            Assert.AreEqual(1, orgs.Count());
            Assert.AreEqual(tro2.Id, orgs.First().Id);
        }

        [Test]
        public void GetCommonCounterparties_None_CounterpartiesCantTrade()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInBrokerage;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddUser("Test User", false, out usrInBrokerage, bro1)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)

                .AddBrokerPermission(tro1, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro2, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro1, tro2, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro2, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInBrokerage.Id, subscribedProduct.Product_Id_fk, tro1.Id, null, BuyOrSell.Buy);
            Assert.AreEqual(0, orgs.Count());
        }

        [Test]
        public void GetCommonCounterparties_BrokerCantTrade()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInBrokerage;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddUser("Test User", false, out usrInBrokerage, bro1)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)

                .AddBrokerPermission(tro1, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro2, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro1, tro2, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro2, tro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInBrokerage.Id, subscribedProduct.Product_Id_fk, tro1.Id, null, BuyOrSell.Buy);
            Assert.AreEqual(0, orgs.Count());
        }

        [Test]
        public void GetCommonCounterparties_Multiple()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            OrganisationDto tro3;
            UserInfoDto usrInBrokerage;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro3", out tro3)
                .AddSubscribedProduct(tro3, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddUser("Test User", false, out usrInBrokerage, bro1)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)


                .AddBrokerPermission(tro1, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro2, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro2, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro3, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro3, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro3, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro1, tro3, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInBrokerage.Id, subscribedProduct.Product_Id_fk, tro1.Id, null, BuyOrSell.Sell);
            Assert.Contains(tro2.Id, orgs.Select(x => x.Id).ToList());
            Assert.Contains(tro3.Id, orgs.Select(x => x.Id).ToList());
            Assert.AreEqual(2, orgs.Count());

        }

        [Test]
        public void GetCommonCounterparties_ShouldNotReturnDeletedCompanies()
        {
            GetCommonCounterparties_ShouldNotReturnTro3Impl(OrganisationType.Trading, tro3IsDeleted: true);
        }

        [Test]
        public void GetCommonCounterparties_ShouldNotReturnNonTradingCompanies()
        {
            GetCommonCounterparties_ShouldNotReturnTro3Impl(OrganisationType.Argus, tro3IsDeleted: false);
        }

        private void GetCommonCounterparties_ShouldNotReturnTro3Impl(
            OrganisationType tro3OrganisationType, bool tro3IsDeleted)
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            OrganisationDto tro3;
            UserInfoDto usrInBrokerage;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro3", out tro3, tro3OrganisationType, tro3IsDeleted)
                .AddSubscribedProduct(tro3, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddUser("Test User", false, out usrInBrokerage, bro1)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)

                .AddBrokerPermission(tro1, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro2, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro2, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro3, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro3, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddCounterpartyPermission(tro3, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddCounterpartyPermission(tro1, tro3, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInBrokerage.Id, subscribedProduct.Product_Id_fk, tro1.Id, null, BuyOrSell.Sell);
            Assert.That(orgs.Count, Is.EqualTo(1));
            Assert.That(orgs.First().Id, Is.EqualTo(tro2.Id));
        }



        [Test]
        public void GetCommonBrokers_Sell()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInOrganisation;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddUser("Test User", false, out usrInOrganisation, tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddBrokerPermission(tro1, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInOrganisation.Id,subscribedProduct.Product_Id_fk,tro2.Id, null,BuyOrSell.Sell);
            Assert.AreEqual(1, orgs.Count());
            Assert.AreEqual(bro1.Id, orgs.First().Id);

        }

        [Test]
        public void GetCommonBrokers_Buy()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInOrganisation;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddUser("Test User", false, out usrInOrganisation, tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddBrokerPermission(tro1, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Sell, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInOrganisation.Id, subscribedProduct.Product_Id_fk, tro2.Id, null, BuyOrSell.Buy);
            Assert.AreEqual(1, orgs.Count());
        }

        [Test]
        public void GetCommonBrokers_None()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInOrganisation;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddUser("Test User", false, out usrInOrganisation, tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddBrokerPermission(tro1, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInOrganisation.Id, subscribedProduct.Product_Id_fk, tro2.Id, null, BuyOrSell.Buy);
            Assert.AreEqual(0, orgs.Count());
        }


        [Test]
        public void GetCommonBrokers_Multiple()
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInOrganisation;
            OrganisationDto bro2;
            OrganisationDto bro3;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddUser("Test User", false, out usrInOrganisation, tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro2", out bro2, OrganisationType.Brokerage)
                .AddSubscribedProduct(bro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro3", out bro3, OrganisationType.Brokerage)
                .AddSubscribedProduct(bro3, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddBrokerPermission(tro1, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro1, bro2, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro2, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro2, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro1, bro3, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro3, tro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro3, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro3, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInOrganisation.Id, subscribedProduct.Product_Id_fk, tro2.Id, null, BuyOrSell.Sell);
            Assert.Contains(bro1.Id, orgs.Select(x=>x.Id).ToList());
            Assert.Contains(bro2.Id, orgs.Select(x => x.Id).ToList());
            Assert.AreEqual(2,orgs.Count());
        }

        [Test]
        public void GetCommonBrokers_ShouldNotReturnDeletedOrganisation()
        {
            GetCommonBrokers_ShouldNotReturnBro3Impl(OrganisationType.Brokerage, bro3IsDeleted: true);
        }

        [Test]
        public void GetCommonBrokers_ShouldNotReturnNonBrokerOrganisation()
        {
            GetCommonBrokers_ShouldNotReturnBro3Impl(OrganisationType.Argus, bro3IsDeleted: false);
        }

        private void GetCommonBrokers_ShouldNotReturnBro3Impl(
            OrganisationType bro3OrganisationType, bool bro3IsDeleted)
        {
            OrganisationDto tro1;
            OrganisationDto tro2;
            OrganisationDto bro1;
            UserInfoDto usrInOrganisation;
            OrganisationDto bro2;
            OrganisationDto bro3;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("tro1", out tro1)
                .AddUser("Test User", false, out usrInOrganisation, tro1)
                .AddSubscribedProduct(tro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("tro2", out tro2)
                .AddSubscribedProduct(tro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro1", out bro1, OrganisationType.Brokerage)
                .AddSubscribedProduct(bro1, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro2", out bro2, OrganisationType.Brokerage)
                .AddSubscribedProduct(bro2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("bro3", out bro3, bro3OrganisationType, bro3IsDeleted)
                .AddSubscribedProduct(bro3, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddBrokerPermission(tro1, bro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro1, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro1, bro2, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro2, tro1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro2, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)

                .AddBrokerPermission(tro1, bro3, BuyOrSell.Sell, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro3, tro1, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(tro2, bro3, BuyOrSell.Buy, subscribedProduct.Product_Id_fk)
                .AddBrokerPermission(bro3, tro2, BuyOrSell.Buy, subscribedProduct.Product_Id_fk);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            var orgs = organisationService.GetCommonPermissionedOrganisations(usrInOrganisation.Id, subscribedProduct.Product_Id_fk, tro2.Id, null, BuyOrSell.Sell);
            Assert.That(orgs.Count, Is.EqualTo(2), "Incorrect number of organisations returned");
            Assert.Contains(bro1.Id, orgs.Select(o => o.Id).ToList(), "bro1 ID not found");
            Assert.Contains(bro2.Id, orgs.Select(o => o.Id).ToList(), "bro2 ID not found");
        }


        //[Test]
        //public void EditCounterpartyPermission_CreatesNewPermissionAndReturnsPermissionPair()
        //{
        //    OrganisationDto org1;
        //    OrganisationDto org2;
        //    UserInfoDto usr;

        //    SubscribedProductDto subscribedProduct;
        //    CrmDataBuilder.WithModel(_mockCrmModel)
        //        .AddOrganisation("a", out org1)
        //        .AddUser("Test User", false, out usr, org1)
        //        .AddSubscribedProduct(org1, _mockAomModel.Products.First().Id, out subscribedProduct)
        //        .AddOrganisation("b", out org2)
        //        .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct)
        //        .AddCounterpartyPermission(org2, org1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk);
        //    CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

        //    var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
        //    var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            
        //    var newPermission = new MatrixPermission
        //    {
        //        LastUpdated = DateTime.UtcNow,
        //        LastUpdatedUserId = -666,

        //        BuyOrSell = BuyOrSell.Buy,
        //        AllowOrDeny = Permission.Allow,
        //        OurOrganisation = org1.Id,
        //        TheirOrganisation = org2.Id,
        //        ProductId = subscribedProduct.Product_Id_fk,         
        //    };

        //    var permissionPair = organisationService.EditCounterpartyPermission(newPermission);

        //    var dbPerm = _mockCrmModel.CounterpartyPermissions.Where(p => p.LastUpdatedUserId == newPermission.LastUpdatedUserId).Select(p => p).Single();

        //    Assert.AreEqual(DtoMappingAttribute.GetValueFromEnum(BuyOrSell.Buy), dbPerm.BuyOrSell);
        //    Assert.AreEqual(DtoMappingAttribute.GetValueFromEnum(Permission.Allow), dbPerm.AllowOrDeny);
        //    Assert.AreEqual(org1.Id, dbPerm.OurOrganisation);
        //    Assert.AreEqual(org2.Id, dbPerm.TheirOrganisation);

        //    Assert.IsNotNull(permissionPair.BuySide);
        //    Assert.IsNotNull(permissionPair.SellSide);

        //    Assert.AreEqual(BuyOrSell.Buy, permissionPair.BuySide.BuyOrSell);
        //    Assert.AreEqual(Permission.Allow, permissionPair.BuySide.OurPermAllowOrDeny);
        //    Assert.AreEqual(org2.Id, permissionPair.BuySide.TheirOrganisation);
        //    Assert.AreEqual(org1.Id, permissionPair.BuySide.OurOrganisation);

        //    Assert.AreEqual(BuyOrSell.Sell, permissionPair.SellSide.BuyOrSell);
        //    Assert.AreEqual(Permission.Allow, permissionPair.SellSide.OurPermAllowOrDeny, "Expected other side db permission to be read from DB");
        //    Assert.AreEqual(org1.Id, permissionPair.SellSide.TheirOrganisation);
        //    Assert.AreEqual(org2.Id, permissionPair.SellSide.OurOrganisation);

            
        //}

        //[Test]
        //public void EditBrokerPermission_CreatesNewPermissionAndReturnsPermissionPair()
        //{
        //    OrganisationDto org1;
        //    OrganisationDto org2;
        //    UserInfoDto usr;

        //    SubscribedProductDto subscribedProduct;
        //    CrmDataBuilder.WithModel(_mockCrmModel)
        //        .AddOrganisation("a", out org1)
        //        .AddUser("Test User", false, out usr, org1)
        //        .AddSubscribedProduct(org1, _mockAomModel.Products.First().Id, out subscribedProduct)
        //        .AddOrganisation("b", out org2)
        //        .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct)              
        //        .AddBrokerPermission(org2, org1, BuyOrSell.Sell, subscribedProduct.Product_Id_fk);
        //    CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);
            
        //    var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
        //    var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());

        //    var newPermission = new List<MatrixPermission>()
        //    {
        //        new MatrixPermission()
        //        {
        //            LastUpdated = DateTime.UtcNow,
        //            LastUpdatedUserId = -666,

        //            BuyOrSell = BuyOrSell.Buy,
        //            AllowOrDeny = Permission.Allow,
        //            OurOrganisation = org1.Id,
        //            TheirOrganisation = org2.Id,
        //            ProductId = subscribedProduct.Product_Id_fk,
        //        }
        //    };

        //    var permissionPair = organisationService.EditBrokerPermission(newPermission, usr.Id);

        //    var dbPerm = _mockCrmModel.BrokerPermissions.Where(p => p.LastUpdatedUserId == newPermission.LastUpdatedUserId).Select(p => p).Single();

        //    Assert.AreEqual(DtoMappingAttribute.GetValueFromEnum(BuyOrSell.Buy), dbPerm.BuyOrSell);
        //    Assert.AreEqual(DtoMappingAttribute.GetValueFromEnum(Permission.Allow), dbPerm.AllowOrDeny);
        //    Assert.AreEqual(org1.Id, dbPerm.OurOrganisation);
        //    Assert.AreEqual(org2.Id, dbPerm.TheirOrganisation);

        //    Assert.IsNotNull(permissionPair.BuySide);
        //    Assert.IsNotNull(permissionPair.SellSide);

        //    Assert.AreEqual(BuyOrSell.Buy, permissionPair.BuySide.BuyOrSell);
        //    Assert.AreEqual(Permission.Allow, permissionPair.BuySide.OurPermAllowOrDeny);
        //    Assert.AreEqual(org2.Id, permissionPair.BuySide.TheirOrganisation);
        //    Assert.AreEqual(org1.Id, permissionPair.BuySide.OurOrganisation);

        //    Assert.AreEqual(BuyOrSell.Buy, permissionPair.SellSide.BuyOrSell);
        //    Assert.AreEqual(Permission.Allow, permissionPair.BuySide.OurPermAllowOrDeny, "Expected other side db permission to be read from DB");
        //    Assert.AreEqual(org1.Id, permissionPair.SellSide.TheirOrganisation);
        //    Assert.AreEqual(org2.Id, permissionPair.SellSide.OurOrganisation);
            
        //}

        [Test]
        public void GetCounterpartyPermissionsDefaultIsNotSet()
        {
            OrganisationDto org;
            OrganisationDto org2;
            UserInfoDto usr;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("b", out org2)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> counterpartyMatrix = organisationService.GetAllCounterpartyPermissions();
            Assert.That(counterpartyMatrix.Count.Equals(2));
            Assert.That(counterpartyMatrix[0].SellSide.OurPermAllowOrDeny.Equals(Permission.NotSet));
            Assert.That(counterpartyMatrix[0].BuySide.OurPermAllowOrDeny.Equals(Permission.NotSet));
        }


        [Test]
        public void GetCounterpartyPermissionsAllow()
        {
            OrganisationDto org;
            OrganisationDto org2;
            UserInfoDto usr;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("b", out org2)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddCounterpartyPermission(org, org2, "B", "A", _mockAomModel.Products.First().Id);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> counterpartyMatrix = organisationService.GetAllCounterpartyPermissions();
            Assert.That(counterpartyMatrix.Count.Equals(2));
            Assert.That(
                counterpartyMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Allow)).Equals(1));
        }

        [Test]
        public void GetCounterpartyPermissionsDeny()
        {
            OrganisationDto org;
            OrganisationDto org2;
            UserInfoDto usr;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("b", out org2)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddCounterpartyPermission(org, org2, "B", "D", _mockAomModel.Products.First().Id);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> counterpartyMatrix = organisationService.GetAllCounterpartyPermissions();
            Assert.That(counterpartyMatrix.Count.Equals(2));
            Assert.That(counterpartyMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Deny)).Equals(1));
        }


        [Test]
        public void GetCounterpartyPermissionsLargeScale()
        {
            OrganisationDto org;
            OrganisationDto org2;
            OrganisationDto org5;
            OrganisationDto org4;
            OrganisationDto org3;
            SubscribedProductDto subscribedProduct;
            UserInfoDto usr;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("b", out org2)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("c", out org3)
                .AddSubscribedProduct(org3, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("d", out org4)
                .AddSubscribedProduct(org4, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("e", out org5)
                .AddSubscribedProduct(org5, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddCounterpartyPermission(org, org2, "B", "D", _mockAomModel.Products.First().Id)
                .AddCounterpartyPermission(org4, org5, "B", "A", _mockAomModel.Products.First().Id)
                .AddCounterpartyPermission(org5, org4, "B", "A", _mockAomModel.Products.First().Id);

            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> counterpartyMatrix = organisationService.GetAllCounterpartyPermissions();
            Assert.That(counterpartyMatrix.Count.Equals(20));
            Assert.That(counterpartyMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Deny)).Equals(1));
            Assert.That(counterpartyMatrix.First(p =>
                p.BuySide.OurOrganisation.Equals(org.Id) && 
                p.BuySide.TheirOrganisation.Equals(org2.Id) && 
                p.ProductId.Equals(_mockAomModel.Products.First().Id)).BuySide.OurPermAllowOrDeny.Equals(Permission.Deny));
            Assert.That(counterpartyMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Allow)).Equals(2));
            Assert.That(counterpartyMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.NotSet)).Equals(17));
            Assert.That(counterpartyMatrix.Count(x => x.SellSide.OurPermAllowOrDeny.Equals(Permission.NotSet)).Equals(20));
        }

        [Test]
        public void GetBrokerPermissionsNoCounterparties()
        {
            CrmDataBuilder.WithModel(_mockCrmModel);
            AomDataBuilder.WithModel(_mockAomModel);
            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerMatrix = organisationService.GetBrokerPermissions(-1);
            Assert.That(brokerMatrix.Count.Equals(0));
        }

        [Test]
        public void GetBrokerPermissionsOneCounterparties()
        {
            OrganisationDto org;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerMatrix = organisationService.GetBrokerPermissions(-1);
            Assert.That(brokerMatrix.Count.Equals(0));
        }

        [Test]
        public void GetBrokerPermissionsMultipleCounterparties()
        {
            OrganisationDto org;
            OrganisationDto org2;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("c", out org2, OrganisationType.Brokerage)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct);


            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerMatrix = organisationService.GetBrokerPermissions(-1);
            Assert.AreEqual(2, brokerMatrix.Count);
        }

        [Test]
        public void GetBrokerPermissionsDefaultIsNotSet()
        {
            OrganisationDto org;
            OrganisationDto org2;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("c", out org2, OrganisationType.Brokerage)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct);

            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerMatrix = organisationService.GetBrokerPermissions(-1);
            Assert.AreEqual(2, brokerMatrix.Count);
            
            Assert.That(brokerMatrix[0].BuySide.OurPermAllowOrDeny.Equals(Permission.NotSet));
        }

        [Test]
        public void GetBrokerPermissionsCreatesForBuyAndSell()
        {
            OrganisationDto org;
            OrganisationDto org2broker;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("c", out org2broker, OrganisationType.Brokerage)
                .AddSubscribedProduct(org2broker, _mockAomModel.Products.First().Id, out subscribedProduct);

            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerMatrix = organisationService.GetBrokerPermissions(-1);
            Assert.AreEqual(2,brokerMatrix.Count);
            Assert.AreEqual(2, brokerMatrix.Count(x => x.SellSide.BuyOrSell.Equals(BuyOrSell.Sell)));
            Assert.AreEqual(2, brokerMatrix.Count(x => x.BuySide.BuyOrSell.Equals(BuyOrSell.Buy)));

        }

        [Test]
        public void GetBrokerPermissionsAllow()
        {
            OrganisationDto org;
            OrganisationDto org2;
            UserInfoDto usr;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("c", out org2, OrganisationType.Brokerage)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddBrokerPermission(org, org2, BuyOrSell.Buy, _mockAomModel.Products.First().Id, Permission.Allow);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerMatrix = organisationService.GetBrokerPermissions(-1);
            Assert.AreEqual(2, brokerMatrix.Count);
            Assert.That(brokerMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Allow)).Equals(1));
        }

        [Test]
        public void GetBrokerPermissionsDeny()
        {
            OrganisationDto org;
            OrganisationDto org2;
            UserInfoDto usr;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("c", out org2, OrganisationType.Brokerage)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddBrokerPermission(org, org2, BuyOrSell.Buy, _mockAomModel.Products.First().Id, Permission.Deny);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerMatrix = organisationService.GetBrokerPermissions(-1);
            Assert.AreEqual(2, brokerMatrix.Count);
            Assert.That(brokerMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Deny)).Equals(1));
        }

        [Test]
        public void NoBrokerMatrixPermissionIfNotSubscribedToTopic()
        {
            OrganisationDto org;
            OrganisationDto org2;
            UserInfoDto usr;

            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("b", out org2, OrganisationType.Brokerage)
                .AddBrokerPermission(org, org2, BuyOrSell.Buy, _mockAomModel.Products.First().Id, Permission.Deny);
            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerPermissions = organisationService.GetBrokerPermissions(-1);
            Assert.That(brokerPermissions.Count.Equals(0));
            Assert.That(brokerPermissions.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Deny)).Equals(0));
        }

        [Test]
        public void GetBrokerPermissionsLargeScale()
        {
            OrganisationDto org;
            OrganisationDto org2;
            OrganisationDto org5;
            OrganisationDto org4;
            OrganisationDto org3;
            OrganisationDto org6;
            UserInfoDto usr;
            SubscribedProductDto subscribedProduct;
            CrmDataBuilder.WithModel(_mockCrmModel)
                .AddOrganisation("a", out org)
                .AddUser("Test User", false, out usr, org)
                .AddSubscribedProduct(org, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("b", out org2)
                .AddSubscribedProduct(org2, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("c", out org3)
                .AddSubscribedProduct(org3, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("d", out org4, OrganisationType.Brokerage)
                .AddSubscribedProduct(org4, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("e", out org5, OrganisationType.Brokerage)
                .AddSubscribedProduct(org5, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddOrganisation("f", out org6, OrganisationType.Brokerage)
                .AddSubscribedProduct(org6, _mockAomModel.Products.First().Id, out subscribedProduct)
                .AddBrokerPermission(org, org4, BuyOrSell.Buy, _mockAomModel.Products.First().Id, Permission.Deny)
                .AddBrokerPermission(org2, org5, BuyOrSell.Buy, _mockAomModel.Products.First().Id, Permission.Allow)
                .AddBrokerPermission(org3, org4, BuyOrSell.Buy, _mockAomModel.Products.First().Id, Permission.Allow);

            CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            IList<BilateralMatrixPermissionPair> brokerMatrix = organisationService.GetBrokerPermissions(-1);
            Assert.AreEqual(30, brokerMatrix.Count);
            Assert.AreEqual(2, brokerMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Allow)));
            Assert.AreEqual(27, brokerMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.NotSet)));
            Assert.AreEqual(1,brokerMatrix.Count(x => x.BuySide.OurPermAllowOrDeny.Equals(Permission.Deny)));
            Assert.That(
                brokerMatrix.First(p => p.BuySide.OurOrganisation.Equals(org.Id) && p.BuySide.TheirOrganisation.Equals(org4.Id)
                                        && p.ProductId.Equals(_mockAomModel.Products.First().Id))
                    .BuySide.OurPermAllowOrDeny.Equals(Permission.Deny));

        }

        [Test]
        public void ShouldSayOrganisationIsInvalidIfItHasEmptyName()
        {
            var org = new Organisation
            {
                Email = "fakeemail@fake.fake",
                ShortCode = "FAK"
            };
            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            Assert.Throws<BusinessRuleException>(() => organisationService.ValidateOrganisation(org));
            Assert.That(organisationService.IsOrganisationValid(org), Is.False);
        }

        [Test]
        public void ShouldSayOrganisationIsInvalidIfItHasEmptyShotCode()
        {
            var org = new Organisation
            {
                Email = "fakeemail@fake.fake",
                Name = "Fake company"
            };
            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            Assert.Throws<BusinessRuleException>(() => organisationService.ValidateOrganisation(org));
            Assert.That(organisationService.IsOrganisationValid(org), Is.False);
        }

        [Test]
        public void ShouldSayOrganisationIsInvalidIfItHasEmptyEmail()
        {
            var org = new Organisation
            {
                Name = "Fake company",
                ShortCode = "FAK"
            };
            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            Assert.Throws<BusinessRuleException>(() => organisationService.ValidateOrganisation(org));
            Assert.That(organisationService.IsOrganisationValid(org), Is.False);
        }

        // Simple, symmetric counterparty and broker permission test cases
        [Test]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, BuyOrSell.Buy, new[] {"com2", "com3"})]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Allow, BuyOrSell.Sell, new[] {"com2", "com3"})]
        [TestCase(Permission.Deny, Permission.Allow, Permission.Allow, BuyOrSell.Buy, new[] {"com3"})]
        [TestCase(Permission.Deny, Permission.Allow, Permission.Allow, BuyOrSell.Sell, new[] {"com3"})]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Deny, BuyOrSell.Buy, new[] {"com3"})]
        [TestCase(Permission.Allow, Permission.Allow, Permission.Deny, BuyOrSell.Sell, new[] {"com3"})]
        [TestCase(Permission.Allow, Permission.Deny, Permission.Allow, BuyOrSell.Buy, new string[] {})]
        [TestCase(Permission.Allow, Permission.Deny, Permission.Allow, BuyOrSell.Sell, new string[] {})]
        public void GetCommonOrganisationsForBrokerExecutingOrderWithSymmetricPermissionsShouldObeyPermissions(
            Permission com1ToCom2,
            Permission brokerCanActForCom1,
            Permission brokerCanActForCom2,
            BuyOrSell orderType,
            string[] expectedCounterparties)
        {
            GetCounterpartiesListForBrokerTestImpl(com1ToCom2,
                                                   com1ToCom2,
                                                   brokerCanActForCom1,
                                                   brokerCanActForCom1,
                                                   brokerCanActForCom2,
                                                   brokerCanActForCom2,
                                                   orderType,
                                                   expectedCounterparties);
        }


        // Symmetric broker (all permissioned to allow), asymmetric counterparty permission test cases
        [Test]
        [TestCase(Permission.Allow, Permission.Deny, BuyOrSell.Buy, new[] {"com2", "com3"})]
        [TestCase(Permission.Allow, Permission.Deny, BuyOrSell.Sell, new[] {"com3"})]
        [TestCase(Permission.Deny, Permission.Allow, BuyOrSell.Buy, new[] { "com3" })]
        [TestCase(Permission.Deny, Permission.Allow, BuyOrSell.Sell, new[] { "com2", "com3" })]
        public void GetCounterpartiesListForBrokerExecutingOrderWithAsymmetricCountepartyPermsShouldObeyPermissions(
            Permission com2BuyFromCom1,
            Permission com2SellToCom1,
            BuyOrSell orderType,
            string[] expectedCounterparties)
        {
            var com1BuyFromCom2 = com2SellToCom1;
            var com1SellToCom2 = com2BuyFromCom1;
            GetCounterpartiesListForBrokerTestImpl(com1BuyFromCom2,
                                                   com1SellToCom2,
                                                   Permission.Allow,
                                                   Permission.Allow,
                                                   Permission.Allow,
                                                   Permission.Allow,
                                                   orderType,
                                                   expectedCounterparties);
        }

        
        // TODO: Test asymmetric broker permissions
        // When we support asymmetric broker permissions (after v1.5.2) then either this test should be
        // extended or new tests written to specifically test the idea of can[Buy|Sell]OnBehalfOf.
        // This really ought to inculde tests with asymmetric counterparty and asymmetric broker permissions.

        private void GetCounterpartiesListForBrokerTestImpl(
            Permission com1BuyFromCom2,
            Permission com1SellToCom2,
            Permission brokerCanBuyForCom1,
            Permission brokerCanSellForCom1,
            Permission brokerCanBuyForCom2,
            Permission brokerCanSellForCom2,
            BuyOrSell orderType,
            IEnumerable<string> expectedCounterparties)
        {
            // There are 3 trading companies (id: -1,-2,-3) and com3 is always setup to be able to trade
            // with anyone via any broker. com1 is placing the order, and we are checking the counterparty
            // list for attempting to aggress the order by bro1 as a broker.
            OrganisationDto com1Dto;
            OrganisationDto com2Dto;
            OrganisationDto com3Dto;
            OrganisationDto bro1Dto;
            CrmDataBuilder
                .WithModel(_mockCrmModel)
                .AddOrganisation("com1", out com1Dto, OrganisationType.Trading)
                .AddOrganisation("com2", out com2Dto, OrganisationType.Trading)
                .AddOrganisation("com3", out com3Dto, OrganisationType.Trading)
                .AddOrganisation("bro1", out bro1Dto, OrganisationType.Brokerage);

            const long fakeProductId = 1234;

            UserInfoDto user1bro1Dto;
            UserCredentialsDto user1CredsDto;
            CrmDataBuilder.WithModel(_mockCrmModel)
                          .AddUser("user1", false, out user1bro1Dto, out user1CredsDto, bro1Dto);

            CrmDataBuilder
                .WithModel(_mockCrmModel)
                .AddBrokerPermission(bro1Dto, com1Dto, BuyOrSell.Buy, fakeProductId, brokerCanBuyForCom1)
                .AddBrokerPermission(bro1Dto, com1Dto, BuyOrSell.Sell, fakeProductId, brokerCanSellForCom1)
                .AddBrokerPermission(bro1Dto, com2Dto, BuyOrSell.Buy, fakeProductId, brokerCanBuyForCom2)
                .AddBrokerPermission(bro1Dto, com2Dto, BuyOrSell.Sell, fakeProductId, brokerCanSellForCom2)
                .AddBrokerPermission(bro1Dto, com3Dto, BuyOrSell.Buy, fakeProductId, Permission.Allow)
                .AddBrokerPermission(bro1Dto, com3Dto, BuyOrSell.Sell, fakeProductId, Permission.Allow);

            CrmDataBuilder
                .WithModel(_mockCrmModel)
                .AddCounterpartyPermission(com1Dto, com2Dto, BuyOrSell.Buy, fakeProductId, com1BuyFromCom2)
                .AddCounterpartyPermission(com1Dto, com2Dto, BuyOrSell.Sell, fakeProductId, com1SellToCom2)
                .AddCounterpartyPermission(com1Dto, com3Dto, BuyOrSell.Buy, fakeProductId, Permission.Allow)
                .AddCounterpartyPermission(com1Dto, com3Dto, BuyOrSell.Sell, fakeProductId, Permission.Allow);

            // Add all broker permissions from the trading companies to bro1 as allow, since for the purpose
            // of this test we control the ability to act just from bro1 to com1/2/3. Also add counterparty
            // permissions from com2/3 back to com1
            foreach (var buySell in new[] { BuyOrSell.Buy, BuyOrSell.Sell })
            {
                foreach (var comDto in new[] { com1Dto, com2Dto, com3Dto })
                {
                    CrmDataBuilder.WithModel(_mockCrmModel).AddBrokerPermission(comDto, bro1Dto, buySell, fakeProductId);
                }
                foreach (var comDto in new[] { com2Dto, com3Dto })
                {
                    CrmDataBuilder.WithModel(_mockCrmModel)
                                  .AddCounterpartyPermission(comDto, com1Dto, buySell, fakeProductId);
                }
                CrmDataBuilder.WithModel(_mockCrmModel)
                              .AddCounterpartyPermission(com2Dto, com3Dto, buySell, fakeProductId)
                              .AddCounterpartyPermission(com3Dto, com2Dto, buySell, fakeProductId);
            }


            var dbContextFactory = new MockDbContextFactory(null, _mockCrmModel);
            var permissionsBuilder = new BilateralPermissionsBuilder();
            var organisationService = new OrganisationService(dbContextFactory, permissionsBuilder);

            var organisations = organisationService.GetCommonPermissionedOrganisations(user1bro1Dto.Id,
                                                                                       fakeProductId,
                                                                                       com1Dto.Id, null,
                                                                                       orderType);

            var organisationNames = organisations.Select(o => o.Name).ToList();
            Assert.That(organisationNames, Is.EquivalentTo(expectedCounterparties));
        }

        [Test]
        [Category("local")]
        public void CanCounterpartiesTradeGeneratesValidSqlAndWorksAgainstDatabase()
        {
            var service = new OrganisationService(new DbContextFactory(), null);
            Assert.IsFalse(service.CanCounterpartiesTrade(1, BuyOrSell.Sell, -1, -2));
        }

        [TestCase(Permission.Allow, Permission.Allow, true)]
        [TestCase(Permission.Deny, Permission.Allow, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        [TestCase(Permission.Allow, Permission.Deny, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        [TestCase(Permission.Deny, Permission.Deny, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        [TestCase(Permission.NotSet, Permission.Deny, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        [TestCase(Permission.Deny, Permission.NotSet, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        [TestCase(Permission.Allow, Permission.NotSet, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        [TestCase(Permission.NotSet, Permission.Allow, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        [TestCase(null, Permission.Allow, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        [TestCase(Permission.Allow, null, false, Description = "Counterparties can only trade only if both sides explicitly permit it")]
        public void CanCounterpartiesTradeTests(Permission? t1Tot2Perm, Permission? t2Tot1Perm, bool expectedCanTradeResult)
        {
            OrganisationDto t1, t2;
            var factory = new MockDbContextFactory(null, new MockCrmModel());
            var model = factory.CreateCrmModel();

            UserInfoDto user1;
            CrmDataBuilder.WithModel(model)
                .AddUser("TestUser", out user1)
                .AddOrganisation("TradeOrg#1", out t1, OrganisationType.Trading)
                .AddOrganisation("TradeOrg#2", out t2, OrganisationType.Trading);

            //NOTE: NotSet maps to NULL column value and we model missing row too
            if (t1Tot2Perm.HasValue)
            {
                CrmDataBuilder.WithModel(model)
                    .AddCounterpartyPermission(t1, t2, BuyOrSell.Sell, 1, t1Tot2Perm.Value);// t1 will sell to t2
            }

            if (t2Tot1Perm.HasValue)
            {
                CrmDataBuilder.WithModel(model)
                    .AddCounterpartyPermission(t2, t1, BuyOrSell.Buy, 1, t2Tot1Perm.Value);// t2 will buy what t2 is selling
            }
            //ACT
            var service = new OrganisationService(factory, null);
            var result=service.CanCounterpartiesTrade(1, BuyOrSell.Sell, t1.Id, t2.Id);
            //ASSERT
            Assert.AreEqual(expectedCanTradeResult, result, expectedCanTradeResult ? "Counterparties should be able to trade" : "Counterparties should not be able to trade");
        }

        [Test]
        [Category("local")]
        public void CanCounterpartyTradeWithBrokerGeneratesValidSqlAndWorksAgainstDatabase()
        {       
            var service = new OrganisationService(new DbContextFactory(), null);
            Assert.IsFalse(service.CanCounterpartyTradeWithBroker(1, BuyOrSell.Sell, -1, -2));
        }

        [TestCase(Permission.Allow, Permission.Allow, true)]
        [TestCase(Permission.Deny, Permission.Allow, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        [TestCase(Permission.Allow, Permission.Deny, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        [TestCase(Permission.Deny, Permission.Deny, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        [TestCase(Permission.NotSet, Permission.Deny, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        [TestCase(Permission.Deny, Permission.NotSet, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        [TestCase(Permission.Allow, Permission.NotSet, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        [TestCase(Permission.NotSet, Permission.Allow, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        [TestCase(null, Permission.Allow, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        [TestCase(Permission.Allow, null, false, Description = "Counterparties can trade through broker only if both sides explicitly permit it")]
        public void CanCounterpartyTradeWithBrokerTests(Permission? t1Tob1Perm, Permission? b1Tot1Perm, bool expectedCanTradeResult)
        {            
            OrganisationDto t1, b1;
            var factory = new MockDbContextFactory(null, new MockCrmModel());
            var model = factory.CreateCrmModel();

            UserInfoDto user1;
            CrmDataBuilder.WithModel(model)
                .AddUser("TestUser", out user1)
                .AddOrganisation("TradeOrg#1", out t1, OrganisationType.Trading)
                .AddOrganisation("BrokerOrg#2", out b1, OrganisationType.Brokerage);
            
            //NOTE: NotSet maps to NULL column value and we model missing row too
            if (t1Tob1Perm.HasValue)
            {
                CrmDataBuilder.WithModel(model)
                    .AddBrokerPermission(t1, b1, BuyOrSell.Sell, 1, t1Tob1Perm.Value);// cpty allows broker to sell on its behalf
            }

            if (b1Tot1Perm.HasValue)
            {
                CrmDataBuilder.WithModel(model)
                    .AddBrokerPermission(b1, t1, BuyOrSell.Sell, 1, b1Tot1Perm.Value);// broker agrees that they will sell on behalf of cpty
            }
            //ACT
            var service = new OrganisationService(factory, null);
            var result = service.CanCounterpartyTradeWithBroker(1, BuyOrSell.Sell, t1.Id, b1.Id);
            //ASSERT
            Assert.AreEqual(expectedCanTradeResult, result, expectedCanTradeResult ? "Counterparties should be able to trade through broker" : "Counterparties should not be able to trade through broker");
        }

        [Test]
        public void GetPermissionedBrokersForUsersOrganisationReturnsBrokersWithCanTradeTrue()
        {
            var allBrokers = new OrganisationsWithPermissions
            {
                BuyOrganisations = new List<OrganisationPermissionSummary>
                {
                    new OrganisationPermissionSummary { CanTrade = false, Organisation = new OrganisationMinimumDetails { Id = 1, Name = "Broker_Buy_False" } }
                },
                SellOrganisations = new List<OrganisationPermissionSummary>
                {
                    new OrganisationPermissionSummary { CanTrade = true, Organisation = new OrganisationMinimumDetails { Id = 2, Name = "Broker_Sell_True" } },
                    new OrganisationPermissionSummary { CanTrade = false, Organisation = new OrganisationMinimumDetails { Id = 3, Name = "Broker_Sell_False" } }
                }
            };

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new Mock<OrganisationService>(dbContextFactory, new BilateralPermissionsBuilder());
            organisationService.Setup(os => os.GetAllBrokersForUsersOrganisation(0, 0)).Returns(allBrokers);
            var permissionedBrokers = organisationService.Object.GetPermissionedBrokersForUsersOrganisation(0, 0);

            Assert.AreEqual(permissionedBrokers.BuyOrganisations.Count, 0);
            Assert.AreEqual(permissionedBrokers.SellOrganisations.Count, 1);
        }

        [Test]
        public void GetPermissionedPrinciplesForUsersOrganisationReturnsBrokersWithCanTradeTrue()
        {
            var allPrinciples = new OrganisationsWithPermissions
            {
                BuyOrganisations = new List<OrganisationPermissionSummary>
                {
                    new OrganisationPermissionSummary { CanTrade = true, Organisation = new OrganisationMinimumDetails { Id = 2, Name = "Principle_Buy_True" } },
                    new OrganisationPermissionSummary { CanTrade = false, Organisation = new OrganisationMinimumDetails { Id = 3, Name = "Principle_Buy_False" } }
                },
                SellOrganisations = new List<OrganisationPermissionSummary>
                {
                    new OrganisationPermissionSummary { CanTrade = true, Organisation = new OrganisationMinimumDetails { Id = 2, Name = "Principle_Sell_True1" } },
                    new OrganisationPermissionSummary { CanTrade = true, Organisation = new OrganisationMinimumDetails { Id = 2, Name = "Principle_Sell_True2" } },
                    new OrganisationPermissionSummary { CanTrade = false, Organisation = new OrganisationMinimumDetails { Id = 3, Name = "Principle_Sell_False" } }
                }
            };

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var organisationService = new Mock<OrganisationService>(dbContextFactory, new BilateralPermissionsBuilder());
            organisationService.Setup(os => os.GetAllPrincipalsForUsersBrokerage(0, 0)).Returns(allPrinciples);
            var permissionedPrinciples = organisationService.Object.GetPermissionedPrincipalsForUsersBrokerage(0, 0);

            Assert.AreEqual(permissionedPrinciples.BuyOrganisations.Count, 1);
            Assert.AreEqual(permissionedPrinciples.SellOrganisations.Count, 2);
        }
    }
}