﻿using AOM.Services.ProductService;
using Moq;

namespace AOM.Services.Tests.CrmService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Transactions;

    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Mappers;
    using AOM.Repository.MySql.Crm;
    using AOM.Repository.MySql.Tests;
    using AOM.Repository.MySql.Tests.Crm;
    using AOM.Services.AuthenticationService;
    using AOM.Services.CrmService;

    using NUnit.Framework;

    [TestFixture]
    public class CrmServiceTests
    {
        private readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        private enum Blockedstatus
        {
            Blocked,

            NotBlocked
        }

        private UserInfoDto CreateTestUser(Blockedstatus accountBlocked, ICrmModel model)
        {
            UserInfoDto user;
            OrganisationDto organisation;

            const string testUsername = "testuser-name";
            const string testName = "TestUser";
            const string testEmail = "some@email.com";

            CrmDataBuilder.WithModel(model)
                .AddOrganisation("TestingOrg", out organisation)
                .AddUser(testUsername, accountBlocked == Blockedstatus.Blocked, out user);

            user.Email = testEmail;
            user.Username = testUsername;
            user.Name = testName;

            return user;
        }

        private UserLoginExternalAccountDto CreateTestUserWithExternalAccount(ICrmModel model)
        {
            UserInfoDto user;
            OrganisationDto org;
            UserLoginExternalAccountDto externalSystemAccount;

            const string testUsername = "testusername";
   
            CrmDataBuilder.WithModel(model)
                .AddOrganisation("TestingOrg", out org)
                .AddUser(testUsername, out user)
                .AddExternalSystemAccount("CME", "CMEUser1", user, org, out externalSystemAccount);

            return externalSystemAccount;
        }

        private UserService CreateUserService(ICrmModel passedModel = null)
        {
            ICrmModel model = passedModel ?? new MockCrmModel();
            return new UserService(new MockDbContextFactory(null, model), _dateTimeProvider, new Mock<IProductService>().Object);
        }

        private CrmAdministrationService CreateCrmService(ICrmModel passedModel = null)
        {
            ICrmModel model = passedModel ?? new MockCrmModel();
            return new CrmAdministrationService(new MockDbContextFactory(null, model), _dateTimeProvider);
        }

        private UserCredentials GetMockCredentials()
        {
            //var mockUser = GetMockUser();
            return new UserCredentials
                   {
                       Id = 9999,
                       DateCreated = _dateTimeProvider.Now,
                       PasswordHashed = "correctPass",
                       Expiration = _dateTimeProvider.Now.AddMonths(1).ToUniversalTime(),
                       FirstTimeLogin = false
                   };
        }

        [Test]
        public void CreateUserExternalAccountSuccessfully()
        {
            var model = new MockCrmModel();
            using (new TransactionScope())
            {
                UserLoginExternalAccountDto testExternalAccount = CreateTestUserWithExternalAccount(model);
                Assert.IsNotNull(testExternalAccount);
                Assert.AreEqual(model.UserLoginSources.ToList()[0].Code, "CME");
                Assert.AreEqual(testExternalAccount.ExternalUserCode, "CMEUser1");
            }
        }

        [Test]
        public void CheckUserHasContentStreamShouldReturnFalseIfTheUserHasNoUserModules()
        {
            using (new TransactionScope())
            {
                var db = new MockCrmModel();

                CrmDataBuilder.WithModel(db);

                var userService = new UserService(new MockDbContextFactory(null, db), _dateTimeProvider, new Mock<IProductService>().Object);
                bool hasContentStream = userService.CheckUserHasContentStream("random user", 10);

                Assert.That(hasContentStream, Is.False);
            }
        }

        [Test]
        public void CheckUserHasContentStreamShouldReturnTrueIfTheUserHasAModuleWithTheContentStream()
        {
            using (new TransactionScope())
            {
                var mockCrmModel = new MockCrmModel();
                ModuleDto moduleDto;
                UserModuleDto userModuleDto;

                UserInfoDto usrInfo;
                CrmDataBuilder.WithModel(mockCrmModel)
                    .AddModule(1, out moduleDto)
                    .AddUser("nathan", out usrInfo)
                    .AddUserModule(1, out userModuleDto, 101, "externalNathan", 1);

                usrInfo.ArgusCrmUsername = "externalNathan";

                UserService userService = CreateUserService(mockCrmModel);
                bool hasContentStream = userService.CheckUserHasContentStream("nathan", 101);

                Assert.That(hasContentStream, Is.True);
            }
        }

        [Test]
        public void CheckUserHasContentStreamsShouldReturnFalseIfContentStreamsExistButNoMatches()
        {
            using (new TransactionScope())
            {
                var userContentStreams = new List<long?> { 20, 21 };
                var contentStreamsToCheck = new List<long> { 10, 11 };

                var mockCrmModel = new MockCrmModel();
                UserService userService = CreateUserService(mockCrmModel);
                bool hasContentStream = userService.CheckUserHasContentStreams(
                    userContentStreams,
                    contentStreamsToCheck);

                Assert.That(hasContentStream, Is.False);
            }
        }

        [Test]
        public void CheckUserHasContentStreamsShouldReturnFalseIfNoContentStreamsToCheck()
        {
            using (new TransactionScope())
            {
                var userContentStreams = new List<long?> { 10, 11 };
                var mockCrmModel = new MockCrmModel();
                UserService userService = CreateUserService(mockCrmModel);
                bool hasContentStream = userService.CheckUserHasContentStreams(userContentStreams, new List<long>());

                Assert.That(hasContentStream, Is.False);
            }
        }

        [Test]
        public void CheckUserHasContentStreamsShouldReturnFalseIfNoUserContentStreams()
        {
            using (new TransactionScope())
            {
                var userContentStreams = new List<long?>();
                var contentStreamsToCheck = new List<long> { 10, 11 };

                var mockCrmModel = new MockCrmModel();
                UserService userService = CreateUserService(mockCrmModel);
                bool hasContentStream = userService.CheckUserHasContentStreams(
                    userContentStreams,
                    contentStreamsToCheck);

                Assert.That(hasContentStream, Is.False);
            }
        }

        [Test]
        public void CheckUserHasContentStreamsShouldReturnTrueIfContentStreamsExistWithMatches()
        {
            using (new TransactionScope())
            {
                var userContentStreams = new List<long?> { 20, 21, 10 };
                var contentStreamsToCheck = new List<long> { 10, 11 };

                var mockCrmModel = new MockCrmModel();
                UserService userService = CreateUserService(mockCrmModel);
                bool hasContentStream = userService.CheckUserHasContentStreams(
                    userContentStreams,
                    contentStreamsToCheck);

                Assert.That(hasContentStream, Is.True);
            }
        }


        [Test]
        public void GetUserWithPrivilegesBuildsUserwithCorrectPrivilegesTest()
        {
            using (new TransactionScope())
            {
                var db = new MockCrmModel();

                OrganisationDto org1;
                const int prod1 = 1;
                const int prod2 = 2;

                UserInfoDto usr;
                SubscribedProductDto sp1;

                CrmDataBuilder.WithModel(db)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "role1")
                    .AddSubscribedProduct(org1, prod1, out sp1)
                    .AddSubscribedProduct(org1, prod2, out sp1)
                    .AddProductRolePrivilege("role1", prod1, ProductPrivileges.Orders.Amend.Own)
                    .AddProductRolePrivilege("role1", prod2, ProductPrivileges.Orders.Amend.Own)
                    .AddUser("testUser", false, out usr)
                    .AddUserOrganisationRole(usr, org1, "role1");

                Mock<IProductService> mockProductService = new Mock<IProductService>();
                mockProductService.Setup(m => m.GetAllProductDefinitions()).Returns(new List<ProductDefinitionItem>
                {
                    new ProductDefinitionItem {ProductId = prod1, ProductName = "Product1"},
                    new ProductDefinitionItem {ProductId = prod2, ProductName = "Product2"}
                });

                var userService = new UserService(new MockDbContextFactory(null, db), _dateTimeProvider, mockProductService.Object);


                var user = userService.GetUserWithPrivileges(usr.Id);

                var prod1Privs = user.ProductPrivileges.Where(p => p.ProductId == prod1).Select(p => p.Privileges).First();
                var prod2Privs = user.ProductPrivileges.Where(p => p.ProductId == prod2).Select(p => p.Privileges).First();

                Assert.IsTrue(prod1Privs.ContainsKey(ProductPrivileges.Orders.Amend.Own));
                Assert.IsTrue(prod2Privs.ContainsKey(ProductPrivileges.Orders.Amend.Own));
            }
        }

        [Test]
        public void CheckUserHasPrivilegedTests()
        {
            using (new TransactionScope())
            {
                var db = new MockCrmModel();

                OrganisationDto org1;
                int prod1 = 1;
                int prod2 = 2;

                UserInfoDto usr;

                CrmDataBuilder.WithModel(db)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "role1")
                    .AddProductRolePrivilege("role1", prod1, ProductPrivileges.Orders.Amend.Own)
                    .AddProductRolePrivilege("role1", prod2, ProductPrivileges.Orders.Amend.Own)
                    .AddUser("testUser", false, out usr)
                    .AddUserOrganisationRole(usr, org1, "role1");

                var userService = new UserService(new MockDbContextFactory(null, db), _dateTimeProvider, new Mock<IProductService>().Object);

                Assert.IsTrue(userService.CheckUserHasPrivilege("testUser", org1.Id, prod1, ProductPrivileges.Orders.Amend.Own));
                Assert.IsTrue(userService.CheckUserHasPrivilege("testUser", org1.Id, prod2, ProductPrivileges.Orders.Amend.Own));
                Assert.IsFalse(userService.CheckUserHasPrivilege("testUser", org1.Id, prod2, "XXXX"));
                Assert.IsTrue(userService.CheckUserHasPrivilege("testUser", org1.Id, prod2, null));
                Assert.IsTrue(userService.CheckUserHasPrivilege("testUser", org1.Id, -1, "XXXX"));
                Assert.IsTrue(userService.CheckUserHasPrivilege("testUser", -1, prod2, null)); // only product check
            }
        }

        [Test]
        public void ShouldBlockUser()
        {
            var model = new MockCrmModel();
            using (new TransactionScope())
            {
                UserInfoDto testUser = CreateTestUser(Blockedstatus.NotBlocked, model);
                Assert.False(testUser.IsBlocked);

                CrmAdministrationService crmService = CreateCrmService(model);
                UserService userService = CreateUserService(model);

                crmService.BlockUser(testUser.Username, -1);
                UserInfoDto userFromDb = userService.GetUserInfoDto(testUser.Username);
                Assert.True(userFromDb.IsBlocked);
                Assert.AreEqual(testUser.Username, userFromDb.Username);
            }
        }

        [Test]
        public void ShouldGetBrokersForPrincipal()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";
            UserInfoDto user1;
            UserInfoDto user2;
            OrganisationDto org1;
            OrganisationDto org2;
            SubscribedProductDto sp1;
            SubscribedProductDto sp2;
            long testProductId = 1;

            var model = new MockCrmModel();

            CrmDataBuilder.WithModel(model)
                .AddOrganisation("Org1", out org1, OrganisationType.Brokerage)
                .AddOrganisationRole(org1.Id, "Test Broker 1")
                .AddOrganisation("Org2", out org2, OrganisationType.Brokerage)
                .AddOrganisationRole(org2.Id, "Test Broker 2")
                .AddSubscribedProduct(org1, testProductId, out sp1)
                .AddSubscribedProduct(org2, testProductId, out sp2)
                .AddUser(testingUserName1, false, out user1, org1)
                .AddUser(testingUserName2, false, out user2, org2);
            //.DBContext.SaveChanges();

            var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
            var brokers = organisationService.GetAllBrokersForUsersOrganisation(user1.Id,testProductId);
            Assert.That(brokers, Is.Not.Null);
            Assert.That(brokers.BuyOrganisations.FirstOrDefault(o => o.Organisation.OrganisationType == OrganisationType.Trading), Is.Null);
            Assert.That(
                brokers.BuyOrganisations.Where(o => o.Organisation.OrganisationType == OrganisationType.Brokerage).ToArray().Length,
                Is.GreaterThan(0));
            Assert.That(brokers.BuyOrganisations.Where(o => o.Organisation.Id == org1.Id).ToArray().Length, Is.EqualTo(1));
            Assert.That(brokers.BuyOrganisations.Where(o => o.Organisation.Id == org2.Id).ToArray().Length, Is.EqualTo(1));
            Assert.That(brokers.BuyOrganisations.Where(o => o.Organisation.Name == org1.Name).ToArray().Length, Is.EqualTo(1));
            Assert.That(brokers.BuyOrganisations.Where(o => o.Organisation.Name == org2.Name).ToArray().Length, Is.EqualTo(1));
        }
        [Test]
        public void ShouldGetAnyAndNoneBrokersWhenGettingBrokersForPrincipal()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";
            UserInfoDto user1;
            UserInfoDto user2;
            OrganisationDto org1;
            OrganisationDto org2;
            SubscribedProductDto sp1;
            SubscribedProductDto sp2;
            long testProductId = 1;

            var model = new MockCrmModel();

            CrmDataBuilder.WithModel(model)
                .AddOrganisation("Org1", out org1, OrganisationType.Brokerage)
                .AddOrganisationRole(org1.Id, "Test Broker 1")
                .AddOrganisation("Org2", out org2, OrganisationType.Brokerage)
                .AddOrganisationRole(org2.Id, "Test Broker 2")
                .AddSubscribedProduct(org1, testProductId, out sp1)
                .AddSubscribedProduct(org2, testProductId, out sp2)
                .AddUser(testingUserName1, false, out user1, org1)
                .AddUser(testingUserName2, false, out user2, org2);
            //.DBContext.SaveChanges();

            var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
            var brokers = organisationService.GetAllBrokersForUsersOrganisation(user1.Id, testProductId);
            Assert.That(brokers, Is.Not.Null);
            Assert.That(brokers.BuyOrganisations.FirstOrDefault(o => o.Organisation.OrganisationType == OrganisationType.Trading), Is.Null);
            Assert.That(
                brokers.BuyOrganisations.Where(o => o.Organisation.OrganisationType == OrganisationType.Brokerage).ToArray().Length,
                Is.GreaterThan(0));
            var noneBrokerBuy =
                brokers.BuyOrganisations.FirstOrDefault(b => b.Organisation.BrokerRestriction == BrokerRestriction.None);
            var anyBrokerBuy = brokers.BuyOrganisations.FirstOrDefault(b => b.Organisation.BrokerRestriction == BrokerRestriction.Any);
            var noneBrokerSell =
                brokers.SellOrganisations.FirstOrDefault(b => b.Organisation.BrokerRestriction == BrokerRestriction.None);
            var anyBrokerSell = brokers.SellOrganisations.FirstOrDefault(b => b.Organisation.BrokerRestriction == BrokerRestriction.Any);

            Assert.That(noneBrokerBuy.Organisation.Name, Is.EqualTo("Bilateral Only (No Broker)"));
            Assert.That(anyBrokerBuy.Organisation.Name, Is.EqualTo("Any Broker"));
            Assert.That(noneBrokerSell.Organisation.Name, Is.EqualTo("Bilateral Only (No Broker)"));
            Assert.That(anyBrokerSell.Organisation.Name, Is.EqualTo("Any Broker"));
        }

        [Test]
        public void ShouldGetOrganisationById()
        {
            var model = new MockCrmModel();

            OrganisationDto org1;
            OrganisationDto org2;
            using (new TransactionScope())
            {
                CrmDataBuilder.WithModel(model).AddOrganisation("Org1", out org1).AddOrganisation("Org2", out org2);

                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                IOrganisation resOrg1 = organisationService.GetOrganisationById(org1.Id);
                IOrganisation resOrg2 = organisationService.GetOrganisationById(org2.Id);
                Assert.That(resOrg1, Is.Not.Null);
                Assert.That(resOrg1.Name, Is.EqualTo(org1.Name));
                Assert.That(resOrg1.Id, Is.EqualTo(org1.Id));
                Assert.That(resOrg2, Is.Not.Null);
                Assert.That(resOrg2.Name, Is.EqualTo(org2.Name));
                Assert.That(resOrg2.Id, Is.EqualTo(org2.Id));
            }
        }



        [Test]
        public void ShouldGetPrincipalsWithPermissions_Sell()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";

            using (new TransactionScope())
            {
                UserInfoDto user1;
                UserInfoDto user2;
                OrganisationDto org1;
                OrganisationDto org2;
                SubscribedProductDto sp1;
                SubscribedProductDto sp2;
                long testProductId = 1;

                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "Test Trader 1")
                    .AddOrganisation("Org2", out org2)
                    .AddOrganisationRole(org2.Id, "Test Trader 2")
                    .AddSubscribedProduct(org1, testProductId, out sp1)
                    .AddSubscribedProduct(org2, testProductId, out sp2)
                    .AddUser(testingUserName1, false, out user1, org1)
                    .AddUser(testingUserName2, false, out user2, org2)
                    .AddCounterpartyPermission(org1, org2, BuyOrSell.Buy, 1)
                    .AddCounterpartyPermission(org2, org1, BuyOrSell.Sell, 1);


                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                var principals = organisationService.GetAllPrincipalsForUsersBrokerage(user1.Id, testProductId);
                Assert.AreEqual(principals.SellOrganisations.First(o => o.Organisation.Id.Equals(org2.Id)).CanTrade, true);
            }
        }

        [Test]
        public void ShouldGetBrokersWithPermissions_Sell()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";

            using (new TransactionScope())
            {
                UserInfoDto user1;
                UserInfoDto user2;
                OrganisationDto org1;
                OrganisationDto org2;
                SubscribedProductDto sp1;
                SubscribedProductDto sp2;
                long testProductId = 1;

                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "Test Trader 1")
                    .AddOrganisation("Org2", out org2, OrganisationType.Brokerage)
                    .AddOrganisationRole(org2.Id, "Test Broker 2")
                    .AddSubscribedProduct(org1, testProductId, out sp1)
                    .AddSubscribedProduct(org2, testProductId, out sp2)
                    .AddUser(testingUserName1, false, out user1, org1)
                    .AddUser(testingUserName2, false, out user2, org2)
                    .AddBrokerPermission(org1, org2, BuyOrSell.Sell, 1)
                    .AddBrokerPermission(org2, org1, BuyOrSell.Sell, 1);


                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                var brokers = organisationService.GetAllBrokersForUsersOrganisation(user1.Id, testProductId);
                Assert.AreEqual(brokers.SellOrganisations.First(o => o.Organisation.Id.Equals(org2.Id)).CanTrade, true);
            }
        }


        [Test]
        public void ShouldGetPrincipalsWithPermissions_Buy()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";

            using (new TransactionScope())
            {
                UserInfoDto user1;
                UserInfoDto user2;
                OrganisationDto org1;
                OrganisationDto org2;
                SubscribedProductDto sp1;
                SubscribedProductDto sp2;
                long testProductId = 1;

                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "Test Trader 1")
                    .AddOrganisation("Org2", out org2)
                    .AddOrganisationRole(org2.Id, "Test Trader 2")
                    .AddSubscribedProduct(org1, testProductId, out sp1)
                    .AddSubscribedProduct(org2, testProductId, out sp2)
                    .AddUser(testingUserName1, false, out user1, org1)
                    .AddUser(testingUserName2, false, out user2, org2)
                    .AddCounterpartyPermission(org1, org2, BuyOrSell.Sell, 1)
                    .AddCounterpartyPermission(org2, org1, BuyOrSell.Buy, 1);


                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                var principals = organisationService.GetAllPrincipalsForUsersBrokerage(user1.Id, testProductId);
                Assert.AreEqual(principals.BuyOrganisations.First(o => o.Organisation.Id.Equals(org2.Id)).CanTrade,true);
            }
        }

        [Test]
        public void ShouldGetBrokersWithPermissions_Buy()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";

            using (new TransactionScope())
            {
                UserInfoDto user1;
                UserInfoDto user2;
                OrganisationDto org1;
                OrganisationDto org2;
                SubscribedProductDto sp1;
                SubscribedProductDto sp2;
                long testProductId = 1;

                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "Test Trader 1")
                    .AddOrganisation("Org2", out org2, OrganisationType.Brokerage)
                    .AddOrganisationRole(org2.Id, "Test Broker 2")
                    .AddSubscribedProduct(org1, testProductId, out sp1)
                    .AddSubscribedProduct(org2, testProductId, out sp2)
                    .AddUser(testingUserName1, false, out user1, org1)
                    .AddUser(testingUserName2, false, out user2, org2)
                    .AddBrokerPermission(org1, org2, BuyOrSell.Buy, 1)
                    .AddBrokerPermission(org2, org1, BuyOrSell.Buy, 1);


                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                var brokers = organisationService.GetAllBrokersForUsersOrganisation(user1.Id, testProductId);
                Assert.AreEqual(brokers.BuyOrganisations.First(o => o.Organisation.Id.Equals(org2.Id)).CanTrade, true);
            }
        }


        [Test]
        public void ShouldGetCommonPermissionedBrokers()
        {
            using (new TransactionScope())
            {
                UserInfoDto t1;
                UserInfoDto t2;
                OrganisationDto to1;
                OrganisationDto to2;
                OrganisationDto bo1;
                SubscribedProductDto sp1;
                SubscribedProductDto sp2;
                UserInfoDto b1;
                long testProductId = 1;

                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out to1)
                    .AddOrganisationRole(to1.Id, "Test Trader 1")
                    .AddOrganisation("Org2", out to2)
                    .AddOrganisationRole(to1.Id, "Test Trader 1")
                    .AddOrganisation("Org3", out bo1, OrganisationType.Brokerage)
                    .AddOrganisationRole(bo1.Id, "Test Broker 2")
                    .AddSubscribedProduct(to1, testProductId, out sp1)
                    .AddSubscribedProduct(to2, testProductId, out sp2)
                    .AddUser("t1", false, out t1, to1)
                    .AddUser("t2", false, out t2, to2)
                    .AddUser("b1", false, out b1, to2)
                    .AddBrokerPermission(to1, bo1, BuyOrSell.Buy, 1)
                    .AddBrokerPermission(bo1, to1, BuyOrSell.Buy, 1)
                    .AddBrokerPermission(to2, bo1, BuyOrSell.Sell, 1)
                    .AddBrokerPermission(bo1, to2, BuyOrSell.Sell, 1);


                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                var brokers = organisationService.GetCommonPermissionedOrganisations(t1.Id, testProductId,to2.Id,null,BuyOrSell.Buy);
                Assert.AreEqual(brokers.First().Id, bo1.Id);
            }
        }

        [Test]
        public void ShouldGetBrokersWithPermissions_None()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";

            using (new TransactionScope())
            {
                UserInfoDto user1;
                UserInfoDto user2;
                OrganisationDto org1;
                OrganisationDto org2;
                SubscribedProductDto sp1;
                SubscribedProductDto sp2;
                long testProductId = 1;

                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "Test Trader 1")
                    .AddOrganisation("Org2", out org2, OrganisationType.Brokerage)
                    .AddOrganisationRole(org2.Id, "Test Broker 2")
                    .AddSubscribedProduct(org1, testProductId, out sp1)
                    .AddSubscribedProduct(org2, testProductId, out sp2)
                    .AddUser(testingUserName1, false, out user1, org1)
                    .AddUser(testingUserName2, false, out user2, org2);


                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                var brokers = organisationService.GetAllBrokersForUsersOrganisation(user1.Id, testProductId);
                Assert.AreEqual(brokers.BuyOrganisations.First(o => o.Organisation.Id.Equals(org2.Id)).CanTrade, false);
                Assert.AreEqual(brokers.SellOrganisations.First(o => o.Organisation.Id.Equals(org2.Id)).CanTrade, false);
            }
        }

        [Test]
        public void ShouldGetCounterpartyWithPermissions_None()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";

            using (new TransactionScope())
            {
                UserInfoDto user1;
                UserInfoDto user2;
                OrganisationDto org1;
                OrganisationDto org2;
                SubscribedProductDto sp1;
                SubscribedProductDto sp2;
                long testProductId = 1;

                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "Test Trader 1")
                    .AddOrganisation("Org2", out org2)
                    .AddOrganisationRole(org2.Id, "Test Trader 2")
                    .AddSubscribedProduct(org1, testProductId, out sp1)
                    .AddSubscribedProduct(org2, testProductId, out sp2)
                    .AddUser(testingUserName1, false, out user1, org1)
                    .AddUser(testingUserName2, false, out user2, org2);


                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                var counterparties = organisationService.GetAllPrincipalsForUsersBrokerage(user1.Id, testProductId);
                Assert.AreEqual(counterparties.BuyOrganisations.First(o => o.Organisation.Id.Equals(org2.Id)).CanTrade, false);
                Assert.AreEqual(counterparties.SellOrganisations.First(o => o.Organisation.Id.Equals(org2.Id)).CanTrade, false);
            }
        }

        [Test]
        public void ShouldGetPrincipalsForBroker()
        {
            const string testingUserName1 = "testingUser1";
            const string testingUserName2 = "testingUser2";

            using (new TransactionScope())
            {
                UserInfoDto user1;
                UserInfoDto user2;
                OrganisationDto org1;
                OrganisationDto org2;
                SubscribedProductDto sp1;
                SubscribedProductDto sp2;
                long testProductId = 1;

                var model = new MockCrmModel();

                CrmDataBuilder.WithModel(model)
                    .AddOrganisation("Org1", out org1)
                    .AddOrganisationRole(org1.Id, "Test Broker 1")
                    .AddOrganisation("Org2", out org2)
                    .AddOrganisationRole(org2.Id, "Test Broker 2")
                    .AddSubscribedProduct(org1, testProductId, out sp1)
                    .AddSubscribedProduct(org2, testProductId, out sp2)
                    .AddUser(testingUserName1, false, out user1, org1)
                    .AddUser(testingUserName2, false, out user2, org2);

                var organisationService = new OrganisationService(new MockDbContextFactory(null, model), new BilateralPermissionsBuilder());
                var principals = organisationService.GetAllPrincipalsForUsersBrokerage(user1.Id,testProductId);
                Assert.That(principals, Is.Not.Null);
                Assert.That(principals.BuyOrganisations.FirstOrDefault(o => o.Organisation.OrganisationType == OrganisationType.Brokerage), Is.Null);
                Assert.That(
                    principals.BuyOrganisations.Where(o => o.Organisation.OrganisationType == OrganisationType.Trading).ToArray().Length,
                    Is.GreaterThan(0));
                Assert.That(principals.BuyOrganisations.Where(o => o.Organisation.Id == org1.Id).ToArray().Length, Is.EqualTo(1));
                Assert.That(principals.BuyOrganisations.Where(o => o.Organisation.Id == org2.Id).ToArray().Length, Is.EqualTo(1));
                Assert.That(principals.BuyOrganisations.Where(o => o.Organisation.Name == org1.Name).ToArray().Length, Is.EqualTo(1));
                Assert.That(principals.BuyOrganisations.Where(o => o.Organisation.Name == org2.Name).ToArray().Length, Is.EqualTo(1));
            }
        }

        [Test]
        public void ShouldReturnAUserWhoIsLinkedToAnOrganisation()
        {
            var model = new MockCrmModel();

            using (new TransactionScope())
            {
                UserInfoDto testUser = CreateTestUser(Blockedstatus.NotBlocked, model);
                UserService userService = CreateUserService(model);

                IUser user = userService.GetUserWithPrivileges(testUser.Id);

                Assert.That(user, Is.Not.Null);
                Assert.That(user.UserOrganisation, Is.Not.Null);
                Assert.AreEqual(testUser.Username, user.Username);
            }
        }

        [Test]
        public void ShouldReturnAValidUserObjectWhenCallingGetUserById()
        {
            var model = new MockCrmModel();
            using (new TransactionScope())
            {
                UserInfoDto testUser = CreateTestUser(Blockedstatus.NotBlocked, model);
                UserService userService = CreateUserService(model);

                IUser user = userService.GetUserWithPrivileges(testUser.Id);

                Assert.That(user, Is.Not.Null);
                Assert.AreEqual(testUser.Username, user.Username);
            }
        }

        [Test]
        public void ShouldReturnAValidUserObjectWhenCallingGetUserByUsername()
        {
            var model = new MockCrmModel();
            using (new TransactionScope())
            {
                UserInfoDto testUser = CreateTestUser(Blockedstatus.NotBlocked, model);
                UserService userService = CreateUserService(model);

                IUser user = userService.GetUserWithPrivileges(testUser.Username);

                Assert.That(user, Is.Not.Null);
                Assert.AreEqual(testUser.Username, user.Username);
            }
        }

        [Test]
        public void ShouldReturnNullWhenCallingGetUserByNameIfTheUserDoesNotExist()
        {
            var model = new MockCrmModel();
            using (new TransactionScope())
            {
                UserService userService = CreateUserService(model);

                IUser user = userService.GetUserWithPrivileges(Guid.NewGuid().ToString());

                Assert.That(user, Is.Null);
            }
        }

        [Test]
        public void ShouldThrowWhenCallingGetUserByIdIfTheUserDoesNotExist()
        {
            var model = new MockCrmModel();
            using (new TransactionScope())
            {
                UserService userService = CreateUserService(model);
                const long userId = long.MinValue;

                Assert.Throws(typeof(InvalidOperationException), () => userService.GetUserWithPrivileges(userId));
            }
        }

        [Test]
        public void ShouldUnblockUser()
        {
            var model = new MockCrmModel();
            using (new TransactionScope())
            {
                UserInfoDto testUser = CreateTestUser(Blockedstatus.Blocked, model);
                Assert.True(testUser.IsBlocked);
                UserCredentialsDto userCredentials = GetMockCredentials().ToDto();
                userCredentials.User = testUser;

                CrmAdministrationService crmService = CreateCrmService(model);
                UserService userService = CreateUserService(model);
                crmService.UnblockUser(testUser.Username, -1);
                UserInfoDto userFromDb = userService.GetUserInfoDto(testUser.Username);
                Assert.False(userFromDb.IsBlocked);
                Assert.AreEqual(testUser.Username, userFromDb.Username);
            }
        }

        [Test]
        public void WhenAddingNewProductRolePrivilegesAllShouldSucceed()
        {
            var model = new MockCrmModel();
            OrganisationDto org1;
            const int prod1 = 1;
            const int prod2 = 2;
            const int userId = 123;
            CrmDataBuilder.WithModel(model)
                          .AddOrganisation("Org1", out org1)
                          .AddOrganisationRole(org1.Id, "role1");

            var roleId = model.OrganisationRoles.Single().Id;

            var newPrivs = new List<ProductRolePrivilege>
            {
                new ProductRolePrivilege
                {
                    OrganisationId = org1.Id,
                    ProductId = prod1,
                    ProductPrivilegeId = 1,
                    RoleId = roleId
                },
                new ProductRolePrivilege
                {
                    OrganisationId = org1.Id,
                    ProductId = prod2,
                    ProductPrivilegeId = 2,
                    RoleId = roleId
                }
            };

            var adminService = CreateCrmService(model);

            var res = adminService.AddProductRolePrivileges(newPrivs, userId);

            Assert.AreEqual(newPrivs.Count, res);
        }

        [Test]
        public void WhenAddingExistingProductRolePrivilegesAllShouldFail()
        {
            var model = new MockCrmModel();
            OrganisationDto org1;
            ProductPrivilegeDto priv1;
            const int prod1 = 1;
            const int prod2 = 2;
            const int userId = 123;
            CrmDataBuilder.WithModel(model)
                          .AddOrganisation("Org1", out org1)
                          .AddOrganisationRole(org1.Id, "role1")
                          .AddProductPrivelege(ProductPrivileges.Orders.Amend.Own, out priv1)
                          .AddProductRolePrivilege("role1",
                                                   prod1,
                                                   ProductPrivileges.Orders.Amend.Own,
                                                   organisationId: org1.Id,
                                                   productPrivilegeId: priv1.Id)
                          .AddProductRolePrivilege("role1",
                                                   prod2,
                                                   ProductPrivileges.Orders.Amend.Own,
                                                   organisationId: org1.Id,
                                                   productPrivilegeId: priv1.Id);

            var roleId = model.OrganisationRoles.Single().Id;

            var newPrivs = new List<ProductRolePrivilege>
            {
                new ProductRolePrivilege
                {
                    OrganisationId = org1.Id,
                    ProductId = prod1,
                    ProductPrivilegeId = priv1.Id,
                    RoleId = roleId
                },
                new ProductRolePrivilege
                {
                    OrganisationId = org1.Id,
                    ProductId = prod2,
                    ProductPrivilegeId = priv1.Id,
                    RoleId = roleId
                }
            };

            var adminService = CreateCrmService(model);

            var res = adminService.AddProductRolePrivileges(newPrivs, userId);

            Assert.AreEqual(0, res);
        }
    }
}