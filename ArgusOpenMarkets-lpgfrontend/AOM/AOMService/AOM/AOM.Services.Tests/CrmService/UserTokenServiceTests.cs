﻿using System;
using AOM.App.Domain.Dates;
using System.Transactions;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.CrmService;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class UserTokenServiceTests
    {
        private const string TestUsername = "ik2";        
        private DateTimeProvider _dateTimeProvider = new DateTimeProvider();
      
        [TestCase(1, "Test1")]
        [TestCase(2, "test1")]
        [TestCase(3, "TEST1")]
        public void CreateTokenTest(long userId, string username)
        {
            var model = new MockCrmModel();
            
            using (var tran = new TransactionScope())
            {
                CreateTestUser(userId, username.ToLower(), model);
                var service = new UserTokenService(new MockDbContextFactory(null, model), _dateTimeProvider);

                service.InsertNewUserToken(userId, username.ToLower());
                var result = service.GetTokenByUserId(userId);
                Assert.NotNull(result);

                tran.Dispose();
            }
        }

        [TestCase(1, "Test1")]
        [TestCase(2, "test1")]
        [TestCase(3, "TEST1")]
        public void GetTokenTest(long userId, string username)
        {
            var model = new MockCrmModel();

            using (var tran = new TransactionScope())
            {
                var createUsername = username.ToLower();
                CreateTestUser(userId, createUsername, model);
                var service = new UserTokenService(new MockDbContextFactory(null, model), _dateTimeProvider);
                var token = service.InsertNewUserToken(userId, createUsername);
                var result = service.GetTokenByUserId(userId);
                Assert.NotNull(result);
                Assert.True(token.LoginTokenString == result.LoginTokenString);
                Assert.That(token.SessionTokenString, Is.EqualTo(result.SessionTokenString));
                Assert.That(token.UserIpAddress, Is.EqualTo(result.UserIpAddress));
                Assert.True(token.UserInfo_Id_fk == result.UserInfo_Id_fk);
                Assert.True(token.TimeCreated.Value.ToShortTimeString() == result.TimeCreated.Value.ToShortTimeString());

                tran.Dispose();
            }
        }
        
        [Test]
        public void DeleteTokenTest()
        {
            var model = new MockCrmModel();
            long userId = 1;
            using (var tran = new TransactionScope())
            {
                CreateTestUser(userId, TestUsername, model);
                var service = new UserTokenService(new MockDbContextFactory(null, model), _dateTimeProvider);
                var token = service.InsertNewUserToken(userId, TestUsername);
                var result = service.GetTokenByUserId(userId);
                Assert.NotNull(result);
                Assert.True(token.LoginTokenString == result.LoginTokenString);

                service.DeleteTokensForUser(1);
                result = service.GetTokenByUserId(userId);
                Assert.IsNull(result);

                tran.Dispose();
            }
        }

        [TestCase(1, "Test1")]
        [TestCase(2, "test1")]
        [TestCase(3, "TEST1")]
        public void DeleteTokenTestDifferentCaseForSameUser(long userId, string username)
        {
            var model = new MockCrmModel();

            using (var tran = new TransactionScope())
            {
                var createUser = username.ToLower();

                CreateTestUser(userId, createUser, model);
                var service = new UserTokenService(new MockDbContextFactory(null, model), _dateTimeProvider);
                var token = service.InsertNewUserToken(userId, createUser);

                var result = service.GetTokenByUserId(userId);
                Assert.NotNull(result);
                Assert.True(token.LoginTokenString == result.LoginTokenString);

                service.DeleteTokensForUser(userId);
                result = service.GetTokenByUserId(userId);
                Assert.IsNull(result);

                tran.Dispose();
            }
        }


        private void CreateTestUser(long userId, string userName, ICrmModel model)
        {
            var user = new UserInfoDto
            {
                Id = userId,
                Username = userName,
                Name = "TestUser-" + userName,
                Email = "some@email.com",
                IsDeleted = false,
                IsBlocked = false,
                IsActive = true,
                Organisation_Id_fk = 1
            };

            using (var context = model ?? new CrmModel())
            {
                context.UserInfoes.Add(user);
                context.SaveChangesAndLog(-1);
            }
        }
    }
}