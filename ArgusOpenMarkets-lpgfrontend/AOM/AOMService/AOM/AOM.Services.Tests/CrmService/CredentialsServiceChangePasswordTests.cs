namespace AOM.Services.Tests.CrmService
{
    using AOM.App.Domain.Dates;

    using System;

    using AOM.App.Domain;
    using AOM.Repository.MySql.Crm;
    using AOM.Repository.MySql.Tests;
    using AOM.Repository.MySql.Tests.Crm;

    using NUnit.Framework;

    [TestFixture]
    public class CredentialsServiceChangePasswordTests
    {
        [Test]
        public void ShouldRaiseBusinessRuleExceptionIfNoPasswordSupplied()
        {
            var crmService = CreateCredentialsService();

            ShouldRaiseExceptionWithMessage<BusinessRuleException>(crmService,
                                                                   "Old password is required",
                                                                   1,
                                                                   string.Empty);
        }

        [Test]
        public void ShouldRaiseBusinessRuleExceptionIfNoNewPasswordSupplied()
        {
            var crmService = CreateCredentialsService();

            ShouldRaiseExceptionWithMessage<BusinessRuleException>(crmService, "New password is required", 1, "B");
        }

        [Test]
        public void ShouldRaiseBusinessRuleExceptionIfUserDoesNotExists()
        {
            var crmService = CreateCredentialsService();

            var expectedErrorMessage = string.Format("User does not exist");

            ShouldRaiseExceptionWithMessage<AuthenticationErrorException>(crmService,
                                                                          expectedErrorMessage,
                                                                          9999,
                                                                          "B",
                                                                          "BOB");
        }

        [Test]
        public void ShouldRaiseBusinessRuleExceptionIfUserUsersPasswordDoesNotMatchAndThisIsNotAnAdminRequest()
        {
            var model = new MockCrmModel();
            const string username = "tim";
            UserInfoDto user;
            CrmDataBuilder.WithModel(model).AddUser(username, true, out user).DBContext.SaveChangesAndLog(-1);

            var crmService = CreateCredentialsService(model);

            const string expectedErrorMessage = "Invalid password";

            ShouldRaiseExceptionWithMessage<BusinessRuleException>(crmService, expectedErrorMessage, user.Id, "B", "BOB");
        }

        [Test]
        public void ShouldRaiseBusinessRuleExceptionIfUserUsersIsDeleted()
        {
            var model = new MockCrmModel();
            const string username = "tim";
            UserInfoDto user;
            CrmDataBuilder.WithModel(model).AddUser(username, true, out user).DBContext.SaveChangesAndLog(-1);

            user.IsDeleted = true;

            var crmService = CreateCredentialsService(model);

            string expectedErrorMessage = string.Format("User: {0} is deleted and password cannot be changed.", user.Username);

            ShouldRaiseExceptionWithMessage<AuthenticationErrorException>(crmService,
                                                                          expectedErrorMessage,
                                                                          user.Id,
                                                                          "B",
                                                                          "BOB");
        }

        [Test]
        public void ShouldChangeTheUsersPasswordIfAllInputIsCorrect()
        {
            var model = new MockCrmModel();
            const string username = "tim";
            UserInfoDto user;
            CrmDataBuilder.WithModel(model)
                //.AddOrganisation("Org1", out org)
                .AddUser(username, true, out user).DBContext.SaveChangesAndLog(-1);

            var crmService = CreateCredentialsService(model);

            const string expectedErrorMessage = "Invalid password";

            ShouldRaiseExceptionWithMessage<BusinessRuleException>(crmService, expectedErrorMessage, user.Id, "B", "BOB");
        }

        private static void ShouldRaiseExceptionWithMessage<TException>(Services.CrmService.CredentialsService crmService, string expectedMessage, long userId,
            string oldPassword = "", string newPassword = "", bool adminRequest = false)
        {
            try
            {
                crmService.UpdateUserCredentials(userId, oldPassword, newPassword, false, adminRequest, -1);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is TException);
                Assert.That(ex.Message, Is.EqualTo(expectedMessage));
            }
        }


        private Services.CrmService.CredentialsService CreateCredentialsService(ICrmModel passedModel = null)
        {
            var model = passedModel == null ? new MockCrmModel() : passedModel;
            foreach (var credentials in model.UserCredentials)
            {
                credentials.CredentialType = "P";
            }
            return new Services.CrmService.CredentialsService(
                new MockDbContextFactory(null, model),
                new DateTimeProvider());
        }
    }
}