﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.AuthenticationService;
using AOM.Services.CrmService;
using AOM.Services.ProductService;
using Moq;
using NUnit.Framework;
using Utils.Logging;
using log4net.Core;

namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class UserServicePerformanceTests
    {
        private UserService _userService;
        private MockDbContextFactory _mockDbContextFactory;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private Mock<IProductService> _mockProductService;
        private MockCrmModel _mockCrmModel;

        [SetUp]
        public void Setup()
        {
            _mockCrmModel = new MockCrmModel();
            _mockDbContextFactory = new MockDbContextFactory(null, _mockCrmModel);
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockProductService = new Mock<IProductService>();
            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(new DateTime());

            _userService = new UserService(_mockDbContextFactory, _mockDateTimeProvider.Object, _mockProductService.Object);
        }

        [Test]
        public void GetUserWithPrivilegesShouldCacheTheUserDetailsForTheSameUser()
        {
            using (var db = new CrmModel())
            {
                OrganisationDto org;
                UserInfoDto userDto;
                CrmDataBuilder.WithModel(_mockCrmModel)
                    .AddOrganisation("Org", out org, OrganisationType.Trading)
                    .AddUser("nathan", false, out userDto, org);

                CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

                Mock<IProductService> mockProductService = new Mock<IProductService>();
                mockProductService.Setup(m => m.GetAllProductDefinitions()).Returns(new List<ProductDefinitionItem>
                {
                    new ProductDefinitionItem {ProductId = 1, ProductName = "Product1"},
                });

                var svc = new UserService(_mockDbContextFactory, new DateTimeProvider(), mockProductService.Object);

                var user = svc.GetUserWithPrivileges(userDto.Id);
                Assert.That(user, Is.Not.Null);
                var user2 = svc.GetUserWithPrivileges(userDto.Id);
                Assert.That(user2, Is.Not.Null);
                var user3 = svc.GetUserWithPrivileges(userDto.Id);
                Assert.That(user3, Is.Not.Null);

                // If not cached called once for prod privileges, once for system privileges and once for user
                // If cached called once for user
                Assert.That(_mockDbContextFactory.NumberCallsToCreatePrivilegeModel, Is.EqualTo(1).Or.EqualTo(3));
            }
        }

        [Test]
        public void GetUserWithPrivilegesShouldCallTheDatabaseMultipleTimesForDifferentUsers()
        {
            using (var db = new CrmModel())
            {
                OrganisationDto org;
                OrganisationDto orgTwo;
                UserInfoDto userDtoOne;
                UserInfoDto userDtoTwo;

                CrmDataBuilder.WithModel(_mockCrmModel)
                    .AddOrganisation("Org", out org, OrganisationType.Trading)
                    .AddUser("nathan", false, out userDtoOne, org)
                    .AddOrganisation("Org2", out orgTwo, OrganisationType.Trading)
                    .AddUser("bertie", false, out userDtoTwo, orgTwo);

                CrmDataBuilder.WithModel(_mockCrmModel).DBContext.SaveChangesAndLog(-1);

                Mock<IProductService> mockProductService = new Mock<IProductService>();
                mockProductService.Setup(m => m.GetAllProductDefinitions()).Returns(new List<ProductDefinitionItem>
                {
                    new ProductDefinitionItem {ProductId = 1, ProductName = "Product1"},
                });

                var svc = new UserService(_mockDbContextFactory, new DateTimeProvider(), mockProductService.Object);

                var user = svc.GetUserWithPrivileges(userDtoOne.Id);
                Assert.That(user, Is.Not.Null);
                var user2 = svc.GetUserWithPrivileges(userDtoTwo.Id);
                Assert.That(user2, Is.Not.Null);

                // If not cached called once for prod privileges, once for system privileges and twcie for users
                // If cached called twice for users
                Assert.That(_mockDbContextFactory.NumberCallsToCreatePrivilegeModel, Is.EqualTo(2).Or.EqualTo(4));
            }
        }

        [Test]
        public void CheckUserHasPrivilegeFastEnoughTest()
        {
            var collectedTimes = new List<long>();

            using (var db = new CrmModel())
            {
                foreach (var usr in db.UserInfoes.Select(u => new { u.Username, Organisation_Id = u.Organisation_Id_fk, u.Id }).Take(10))
                {
                    var svc = new UserService(new DbContextFactory(), new DateTimeProvider(), new Mock<IProductService>().Object);
                    var clk = Stopwatch.StartNew();
                    var outcome = svc.CheckUserHasPrivilege(usr.Username, usr.Organisation_Id, -1, ProductPrivileges.Deal.Authenticate);

                    collectedTimes.Add(clk.ElapsedMilliseconds);
                    Trace.WriteLine(string.Format("{0}msecs to check usr {1} result={2}", clk.ElapsedMilliseconds, usr.Username, outcome));
                }
            }

            collectedTimes.RemoveAt(0);//remove this first item as this includes EF initialisation

            long avgTime = collectedTimes.Sum() / collectedTimes.Count();
            Trace.WriteLine(string.Format("* Average is {0}msecs ", avgTime));

            //This users privileges are checked a lot by the application if this method becomes slow then AOM will be impacted
            Assert.LessOrEqual(avgTime, 200, "Average time higher than expected for checking a users priviliges");
        }

        [Test]
        public void RecordLoginIpForUserShouldNotProvideFailureReasonForSuccessfulLogin()
        {
            const string fakeIp = "1.2.3.4";
            const string fakeName = "fakeName";
            const string fakeLoginFailureReason = "fakeFailureReason";
            const long aomUserLoginSourceId = 1;

            _userService.RecordLoginIpForUser(fakeIp, fakeName, true, fakeLoginFailureReason, aomUserLoginSourceId);

            var authModel = _mockDbContextFactory.CreateAuthenticationModel();
            Assert.NotNull(authModel);
            Assert.NotNull(authModel.AuthenticationHistory);
            Assert.That(authModel.AuthenticationHistory.Count(), Is.EqualTo(1));
            var authHistory = authModel.AuthenticationHistory.First();
            Assert.That(authHistory.SuccessfulLogin, Is.True);
            Assert.IsEmpty(authHistory.LoginFailureReason);
        }

        [Test]
        public void RecordLoginIpForUserShouldProvideFailureReasonForFailedLogin()
        {
            const string fakeIp = "1.2.3.4";
            const string fakeName = "fakeName";
            const string fakeLoginFailureReason = "fakeFailureReason";
            const long aomUserLoginSourceId = 1;

            _userService.RecordLoginIpForUser(fakeIp, fakeName, false, fakeLoginFailureReason, aomUserLoginSourceId);

            var authModel = _mockDbContextFactory.CreateAuthenticationModel();
            Assert.NotNull(authModel);
            Assert.NotNull(authModel.AuthenticationHistory);
            Assert.That(authModel.AuthenticationHistory.Count(), Is.EqualTo(1));
            var authHistory = authModel.AuthenticationHistory.First();
            Assert.That(authHistory.SuccessfulLogin, Is.False);
            StringAssert.AreEqualIgnoringCase(fakeLoginFailureReason, authHistory.LoginFailureReason);
        }
    }

    [TestFixture]
    public class UserServiceTests
    {
        private Mock<ICrmModel> _mockCrmModel;
        private MockDbContextFactory _mockDbContextFactory;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private UserService _userService;

        [SetUp]
        public void Setup()
        {
            _mockCrmModel = new Mock<ICrmModel>();
            _mockDbContextFactory = new MockDbContextFactory(null, _mockCrmModel.Object);
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _userService = new UserService(_mockDbContextFactory, _mockDateTimeProvider.Object, new Mock<IProductService>().Object);
        }

        [Test]
        public void CheckUserHasContentStreamsShouldReturnTrueIfUserHasOneOfTheStreamIds()
        {
            Assert.IsTrue(_userService.CheckUserHasContentStreams(new List<long?> { 111, 222 }, new List<long> { 222 }));
            Assert.IsTrue(_userService.CheckUserHasContentStreams(new List<long?> { 111, 222 }, new List<long> { 222, 333 }));
            Assert.IsTrue(_userService.CheckUserHasContentStreams(new List<long?> { null, 222 }, new List<long> { 222 }));
        }

        [Test]
        public void CheckUserHasContentStreamsShouldLogInfoMessageWithStreamIdsIfCheckSuccessful()
        {
            using (var testAppender = new MemoryAppenderForTests())
            {
                Assert.IsTrue(_userService.CheckUserHasContentStreams(new List<long?> { 111, 222 }, new List<long> { 222 }));
                testAppender.AssertALogMessageContains(Level.Info, "Returning true");
                testAppender.AssertALogMessageContains(Level.Info, "[111,222]");
                testAppender.AssertALogMessageContains(Level.Info, "[222]");
            }
        }

        [Test]
        public void CheckUserHasContentStreamsShouldReturnFalseIfUserHasNoneOfTheStreamIds()
        {
            Assert.IsFalse(_userService.CheckUserHasContentStreams(new List<long?> { 111, 222 }, new List<long> { 333 }));
        }

        [Test]
        public void CheckUserHasContentStreamsShouldReturnFalseIfPassedNullStreamsToCheck()
        {
            Assert.IsFalse(_userService.CheckUserHasContentStreams(new List<long?> { 111, 222 }, null));
        }

        [Test]
        public void CheckUserHasContentStreamsShouldReturnFalseIfPassedEmptyStreamsToCheck()
        {
            Assert.IsFalse(_userService.CheckUserHasContentStreams(new List<long?> { 111, 222 }, new List<long>()));
        }
    }
}