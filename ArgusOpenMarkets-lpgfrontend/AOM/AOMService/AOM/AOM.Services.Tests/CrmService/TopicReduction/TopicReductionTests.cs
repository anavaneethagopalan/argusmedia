﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Services.CrmService.TopicReduction;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace AOM.Services.Tests.CrmService.TopicReduction
{
    [TestFixture]
    internal class TopicReductionTests
    {
        private TopicReductionService _topicReductionService;

        [SetUp]
        public void Setup()
        {
            _topicReductionService = new TopicReductionService();
        }

        [Test]
        [TestCase(1, 60, 62, Permission.Allow, Permission.Allow, Permission.Allow, Permission.Allow, "a", "a", "a", "a")]
        [TestCase(1, 60, 62, Permission.Deny, Permission.Deny, Permission.Deny, Permission.Deny, "d", "d", "d", "d")]
        [TestCase(1, 60, 62, Permission.NotSet, Permission.NotSet, Permission.NotSet, Permission.NotSet, "u", "u", "u", "u")]
        [TestCase(1, 60, 62, Permission.Allow, Permission.Deny, Permission.NotSet, Permission.NotSet, "a", "d", "u", "u")]
        [TestCase(1, 60, 62, Permission.Allow, Permission.Deny, Permission.Deny, Permission.Allow, "a", "d", "d", "a")]
        public void ShouldReturnConflatedTopicsForTheOrganisationWithAllPermissionsSetToTrue(long productId, long ourOrganisationId, long theirOrganisationId,
            Permission buyOurDecision, Permission sellTheirDecision, Permission sellOurDecision, Permission buyTheirDecision,
            string expectedBuyOurPermission, string expectedSellTheirDecision, string expectedSellOurPermission, string expectedBuyTheirPermission)

        {
            var brokerDataToReduce = new List<BilateralMatrixPermissionPair>();
            brokerDataToReduce.AddRange(MakeBrokerPermissionData(productId, ourOrganisationId, theirOrganisationId, buyOurDecision, sellOurDecision, buyTheirDecision, sellTheirDecision, ""));
            var conflatedBrokerTopics = _topicReductionService.ReduceOrganisationTopicCount(brokerDataToReduce, true);

            Assert.That(conflatedBrokerTopics, Is.Not.Null);

            // An entry for us to them - and another entry for the reverse - them to us.
            Assert.That(conflatedBrokerTopics.Count, Is.EqualTo(1));

            Assert.That(conflatedBrokerTopics[0].Bps.Count, Is.EqualTo(1));
            Assert.That(conflatedBrokerTopics[0].Bps[0].BuyOd, Is.EqualTo(expectedBuyOurPermission));
            Assert.That(conflatedBrokerTopics[0].Bps[0].BuyTd, Is.EqualTo(expectedSellTheirDecision));
            Assert.That(conflatedBrokerTopics[0].Bps[0].SellOd, Is.EqualTo(expectedSellOurPermission));
            Assert.That(conflatedBrokerTopics[0].Bps[0].SellTd, Is.EqualTo(expectedBuyTheirPermission));
        }


        [Test]
        public void ShouldPopulateTheLastUpdatedUsername()
        {
            var bilateralPermission = new List<BilateralMatrixPermissionPair>();
            bilateralPermission.AddRange(MakeBrokerPermissionData(1, 60, 62, Permission.Allow, Permission.Allow, Permission.Allow, Permission.Allow, "Fred Smith"));
            bilateralPermission.AddRange(MakeBrokerPermissionData(1, 60, 63, Permission.Allow, Permission.Allow, Permission.Allow, Permission.Allow, "Fred Smith"));

            var conflatedBrokerTopics = _topicReductionService.ReduceOrganisationTopicCount(bilateralPermission, true);

            Assert.That(conflatedBrokerTopics, Is.Not.Null);
            Assert.That(conflatedBrokerTopics[0].UserDetails.Count == 2);
        }


        private List<BilateralMatrixPermissionPair> MakeBrokerPermissionData(long productId, long ourOrganisationId,
            long theirOrganisationId, Permission buyOd, Permission sellOd, Permission buyTd, Permission sellTd, string lastUpdatedUsername)
        {
            var existingBilateralPermissions = new List<BilateralMatrixPermissionPair>();

            // Us to them
            var orgAToOrgBPermission = MarkBilateralPermission(productId, ourOrganisationId, theirOrganisationId, buyOd, sellOd, buyTd, sellTd, lastUpdatedUsername);
            existingBilateralPermissions.Add(orgAToOrgBPermission);

            //// THem to us
            //var orgBToOrgAPermission = MarkBilateralPermission(productId, theirOrganisationId, ourOrganisationId, buyTd, sellOd, buyTd, sellOd, lastUpdatedUsername);
            //existingBilateralPermissions.Add(orgBToOrgAPermission);

            return existingBilateralPermissions;
        }

        private BilateralMatrixPermissionPair MarkBilateralPermission(long productId, long ourOrganisationId, long theirOrgansationId,
            Permission buyOd, Permission sellOd, Permission buyTd, Permission sellTd, string lastUpdatedUsername)
        {
            var bilateralPermission = new BilateralMatrixPermissionPair { ProductId = productId };

            if (string.IsNullOrEmpty(lastUpdatedUsername))
            {
                lastUpdatedUsername = "John Doe";
            }

            bilateralPermission.BuySide = new BilateralMatrixPermission
            {
                BuyOrSell = BuyOrSell.Buy,
                OurPermAllowOrDeny = buyOd,
                OurLastUpdated = DateTime.Now,
                LastUpdatedByUser = lastUpdatedUsername,
                OurOrganisation = ourOrganisationId,
                TheirOrganisation = theirOrgansationId,
                TheirPermAllowOrDeny = sellTd
            };

            bilateralPermission.SellSide = new BilateralMatrixPermission
            {
                BuyOrSell = BuyOrSell.Sell,
                OurPermAllowOrDeny = sellOd,
                OurLastUpdated = DateTime.Now,
                LastUpdatedByUser = "Nathan Bellamore",
                OurOrganisation = theirOrgansationId,
                TheirOrganisation = ourOrganisationId,
                TheirPermAllowOrDeny = buyTd
            };

            return bilateralPermission;
        }
    }
}