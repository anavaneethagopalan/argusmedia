﻿
namespace AOM.Services.Tests.CrmService
{
    using System.Collections.Generic;
    using System.Linq;

    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Crm;
    using AOM.Repository.MySql.Tests;
    using AOM.Repository.MySql.Tests.Crm;
    using AOM.Services.CrmService;

    using NUnit.Framework;

    [TestFixture]
    public class UserModuleServiceTests
    {

        private AomUserModuleViewEntity GetAomUserModuleViewEntity(long moduleId, string username, string description)
        {
            return new AomUserModuleViewEntity { ModuleId = moduleId, UserName = username, Description = description };
        }

        private UserInfoDto userInfo1 = new UserInfoDto
        {
            Id = 1,
            Username = "testUser1",
            Name = "Name",
            Email = "email",
            IsDeleted = false,
            IsBlocked = false,
            IsActive = true,
            Organisation_Id_fk = 1,
            OrganisationDto =
                new OrganisationDto { Id = 1, OrganisationType = "T" }
        };

        private UserInfoDto userInfo2 = new UserInfoDto
        {
            Id = 2,
            Username = "testUser1",
            Name = "Name",
            Email = "email",
            IsDeleted = false,
            IsBlocked = false,
            IsActive = true,
            Organisation_Id_fk = 1,
            OrganisationDto =
                new OrganisationDto { Id = 1, OrganisationType = "T" },
            ArgusCrmUsername = "user2@a.com"
        };

        private UserModuleDto GetUserModuleDto(long id, string username, ModuleDto module)
        {
            return new UserModuleDto {Id = id, ArgusCrmUsername = username, Module = module, ModuleId_fk = module.Id};
        }

        private ModuleDto GetModuleDto(long id, string descr)
        {
            return new ModuleDto { Id = id, Description = descr };
        }

        [Test]
        public void UpdateUserModulesTest()
        {
            var model = new MockCrmModel();
            //add dtos to model
            var module1 = GetModuleDto(1, "description1");
            var module2 = GetModuleDto(2, "description2");
            var module4 = GetModuleDto(4, "description4");
            //var module3 = GetModuleDto(3, "username3");

            model.Modules.Add(module1);
            model.Modules.Add(module2);
            model.Modules.Add(module4);

            var userModule1 = GetUserModuleDto(1, "username1", module1);
            var userModule2 = GetUserModuleDto(2, "username2", module2);
            var userModule4 = GetUserModuleDto(4, "username4", module4);

            model.UserModules.Add(userModule1);
            model.UserModules.Add(userModule2);
            model.UserModules.Add(userModule4);

            var aomViewEntity1 = GetAomUserModuleViewEntity(1, "username1", "Aom");
            var aomViewEntity2 = GetAomUserModuleViewEntity(4, "username4", "News");

            var service = new UserModuleService(new MockDbContextFactory(null, model));
            service.UpdateUserModules(new List<AomUserModuleViewEntity> { aomViewEntity1, aomViewEntity2 });

            //save changes has been called
            Assert.That(model.SaveChangesCalledCount, Is.GreaterThan(0));

            //userModule2 with username2 should get removed
            Assert.IsNull(model.UserModules.FirstOrDefault(um => um.ModuleId_fk == userModule2.ModuleId_fk));

            //should be 2 modules left now
            Assert.That(model.UserModules.Count(), Is.EqualTo(2));

            //should have aomViewEntity1 and aomViewEntity2
            Assert.IsNotNull(model.UserModules.FirstOrDefault(um => um.ModuleId_fk == aomViewEntity1.ModuleId));
            Assert.IsNotNull(model.UserModules.FirstOrDefault(um => um.ModuleId_fk == aomViewEntity2.ModuleId));
            //check description 
            Assert.IsNotNull(model.UserModules.FirstOrDefault(um => um.Module.Description == module1.Description));
            Assert.IsNotNull(model.UserModules.FirstOrDefault(um => um.Module.Description == module4.Description));
            //check  username
            Assert.IsNotNull(model.UserModules.FirstOrDefault(um => um.ArgusCrmUsername == aomViewEntity1.UserName));
            Assert.IsNotNull(model.UserModules.FirstOrDefault(um => um.ArgusCrmUsername == aomViewEntity1.UserName));
            //check module id
            Assert.IsNotNull(model.UserModules.FirstOrDefault(um => um.Module.Id == module1.Id));
            Assert.IsNotNull(model.UserModules.FirstOrDefault(um => um.Module.Id == module4.Id));

        }

        [Test]
        public void UpdateUserInfoTest()
        {
            var model = new MockCrmModel();
            //add userInfoes
            model.UserInfoes.Add(userInfo1);
            model.UserInfoes.Add(userInfo2);

            var aomViewEntity1 = GetAomUserModuleViewEntity(1, "username1", "Aom");
            var aomViewEntity2 = GetAomUserModuleViewEntity(4, "username4", "News");

            var service = new UserModuleService(new MockDbContextFactory(null, model));

            service.UpdateUserInfo(new List<AomUserModuleViewEntity> { aomViewEntity1, aomViewEntity2 });

            //save changes has been called
            Assert.That(model.SaveChangesCalledCount, Is.GreaterThan(0));

            //no new users have been added
            Assert.That(model.UserInfoes.Count(), Is.EqualTo(2));

            //argus
            Assert.IsNull(model.UserInfoes.FirstOrDefault(u => u.Id == 1).ArgusCrmUsername);

            //user info2 should be inactive now
            Assert.That(model.UserInfoes.FirstOrDefault(u => u.Id == 2).IsActive, Is.False);
        }

        [Test]
        public void UpdateModules_ShouldAddNewModulesTest()
        {
            var model = new MockCrmModel();
            //add dtos to model
            var module1 = GetModuleDto(1, "description1");
            var module2 = GetModuleDto(2, "description2");
            //var module3 = GetModuleDto(3, "username3");

            model.Modules.Add(module1);
            model.Modules.Add(module2);

            var aomViewEntity1 = GetAomUserModuleViewEntity(1, "username1", "Aom");
            var aomViewEntity2 = GetAomUserModuleViewEntity(4, "username4", "News");

            var service = new UserModuleService(new MockDbContextFactory(null, model));

            service.UpdateModules(new List<AomUserModuleViewEntity> { aomViewEntity1, aomViewEntity2 });

            var returnedModules = model.Modules.Select(a => a);

            //save changes has been called
            Assert.That(model.SaveChangesCalledCount, Is.GreaterThan(0));

            //should have 3 modules now - 1 has been added
            Assert.That(returnedModules.Count(), Is.EqualTo(3));

            //module 1 should still be there
            Assert.IsNotNull(returnedModules.FirstOrDefault(a => a.Id == 1));

            //module 4 should be there as well
            Assert.IsNotNull(returnedModules.FirstOrDefault(a => a.Id == 4));
        }

        [Test]
        public void UpdateModules_ShouldUpdateDescriptionTest()
        {
            var model = new MockCrmModel();
            //add dtos to model
            var module1 = GetModuleDto(1, "description1");
            var module2 = GetModuleDto(2, "description2");
            //var module3 = GetModuleDto(3, "username3");

            model.Modules.Add(module1);
            model.Modules.Add(module2);

            var aomViewEntity1 = GetAomUserModuleViewEntity(1, "username1", "Aom");
            var aomViewEntity2 = GetAomUserModuleViewEntity(4, "username4", "News");

            var service = new UserModuleService(new MockDbContextFactory(null, model));

            service.UpdateModules(new List<AomUserModuleViewEntity> { aomViewEntity1, aomViewEntity2 });

            var returnedModules = model.Modules.Select(a => a);

            //save changes has been called
            Assert.That(model.SaveChangesCalledCount, Is.GreaterThan(0));

            //should have 3 modules now - 1 has been added
            Assert.That(returnedModules.Count(), Is.EqualTo(3));

            //module 1 desciption shoudld have changed
            Assert.That(
                returnedModules.FirstOrDefault(a => a.Id == 1).Description,
                Is.EqualTo(aomViewEntity1.Description));

            //the other module descr should not have changed
            Assert.That(returnedModules.FirstOrDefault(a => a.Id == 2).Description, Is.EqualTo(module2.Description));
        }

        [Test]
        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("nameThatIsPresent", true)]
        [TestCase("nameThatIsNotPresent", false)]
        public void UserInfoWithEmptyOrNullCrmUserbaneShouldNotBeDeactivatedIfNotInModulesList(string name,
                                                                                               bool shouldBeActive)
        {
            var fakeModules = new[]
            {
                new AomUserModuleViewEntity
                {
                    UserName = "nameThatIsPresent"
                }
            };

            var crmModel = new MockCrmModel();
            UserInfoDto fakeUserInfo;
            CrmDataBuilder.WithModel(crmModel).AddUser(name, out fakeUserInfo);

            var service = new UserModuleService(new MockDbContextFactory(null, crmModel));

            service.UpdateUserInfo(fakeModules);

            var updatedUserInfo = GetFirstUserAndVerifyName(crmModel, name);
            Assert.That(updatedUserInfo.IsActive, Is.EqualTo(shouldBeActive), "IsActive value for user is incorrect");
        }

        [Test]
        public void InactiveUserInfoShouldBeReactivatedIfNameIsInModulesList()
        {
            const string userName = "nameThatIsPresent";
            var fakeModules = new[]
            {
                new AomUserModuleViewEntity
                {
                    UserName = userName
                }
            };

            var crmModel = new MockCrmModel();
            UserInfoDto fakeUserInfo;
            CrmDataBuilder.WithModel(crmModel).AddUser(userName, out fakeUserInfo);
            fakeUserInfo.IsActive = false;

            var service = new UserModuleService(new MockDbContextFactory(null, crmModel));

            var beforeUserInfo = GetFirstUserAndVerifyName(crmModel, userName);
            Assert.That(beforeUserInfo.IsActive, Is.False, "User should be inactive before update"); 

            service.UpdateUserInfo(fakeModules);

            var updatedUserInfo = GetFirstUserAndVerifyName(crmModel, userName);
            Assert.That(updatedUserInfo.IsActive, Is.True, "User should be active after update"); 
        }

        private static UserInfoDto GetFirstUserAndVerifyName(ICrmModel crmModel, string expectedName)
        {
            Assert.That(crmModel.UserInfoes.Count(), Is.EqualTo(1));
            var userInfo = crmModel.UserInfoes.First();
            Assert.IsNotNull(userInfo);
            Assert.That(userInfo.Name, Is.EqualTo(expectedName));
            return userInfo;
        }

    }
}