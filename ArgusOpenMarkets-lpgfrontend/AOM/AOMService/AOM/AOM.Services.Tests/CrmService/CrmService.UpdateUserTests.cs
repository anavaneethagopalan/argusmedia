﻿using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Services.CrmService;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace AOM.Services.Tests.CrmService
{
    [TestFixture]
    public class CrmServiceUpdateUserTests
    {
        private static readonly UserInfoDto FakeUserInfoDto = new UserInfoDto
        {
            ArgusCrmUsername = "FakeArgusCrmUsername",
            Email = "FakeEmail@fake.com",
            Id = 123,
            IsActive = true,
            IsBlocked = false,
            IsDeleted = false,
            Name = "FakeName",
            Telephone = "01234 567890",
            Username = "FakeTrader",
            Title = "Admiral of the Fleet"
        };

        private Mock<IAomModel> _mockAomModel;
        private Mock<ICrmModel> _mockCrmModel;
        private MockDbContextFactory _mockDb;
        private readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();
        private CrmAdministrationService _service;

        [SetUp]
        public void Setup()
        {
            _mockAomModel = new Mock<IAomModel>();
            _mockCrmModel = new Mock<ICrmModel>();
            _mockDb = new MockDbContextFactory(_mockAomModel.Object, _mockCrmModel.Object);
            _service = new CrmAdministrationService(_mockDb, _dateTimeProvider);
        }

        [Test]
        public void UpdatingNonExistantUserThrowsException()
        {
            _mockCrmModel.Setup(m => m.UserInfoes)
                         .Returns(() => new FakeDbSet<UserInfoDto>(new[] {FakeUserInfoDto}));
            var e = Assert.Throws<BusinessRuleException>(
                () => _service.UpdateUserDetails(456, new User(), 999));
            StringAssert.Contains("user 456", e.Message);
            StringAssert.Contains("user not found", e.Message);
        }

        [Test]
        public void UpdateWhereMoreThanOneUserHasTheIdThrowsException()
        {
            _mockCrmModel.Setup(m => m.UserInfoes)
                         .Returns(() => new FakeDbSet<UserInfoDto>(new[] {FakeUserInfoDto, FakeUserInfoDto}));
            var e = Assert.Throws<BusinessRuleException>(
                () => _service.UpdateUserDetails(FakeUserInfoDto.Id, new User(), 999));
            StringAssert.Contains(string.Format("user {0}", FakeUserInfoDto.Id), e.Message);
            StringAssert.Contains("unique user not found", e.Message);
        }

        [Test]
        public void UpdatingAUserSavesChangesToContextCorrectly()
        {
            var userInfoSet = new FakeDbSet<UserInfoDto>(new[] {FakeUserInfoDto});
            _mockCrmModel.Setup(m => m.UserInfoes)
                         .Returns(() => userInfoSet);

            var newUserInfo = new User
            {
                ArgusCrmUsername = "NewArgusCrmUsername",
                Email = "NewEmail@fake.com",
                Name = "NewName",
                Telephone = "1-800-UTRADE",
                Title = "Professor Sir"
            };

            _service.UpdateUserDetails(FakeUserInfoDto.Id, newUserInfo, 999);

            var user = userInfoSet.Single(i => i.Id == FakeUserInfoDto.Id);
            Assert.AreEqual(newUserInfo.ArgusCrmUsername, user.ArgusCrmUsername);
            Assert.AreEqual(newUserInfo.Email, user.Email);
            Assert.AreEqual(newUserInfo.Name, user.Name);
            Assert.AreEqual(newUserInfo.Telephone, user.Telephone);
            Assert.AreEqual(newUserInfo.Title, user.Title);
        }
    }
}
