﻿using AOM.App.Domain.Entities;
using AOM.Services.MarketTickerService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Utils.Javascript.Extension;

namespace AOM.Services.Tests.MarketTickerService
{
    [TestFixture]
    public class MarketTickerItemMessageGeneratorTests
    {
        private MarketTickerItemMessageGenerator _marketTickerMessageItemGenerator;
        private Mock<IProductService> _mockProductService;

        private const long FakeProductTenorId = 234;
        private const long FakeProductId = 1;
        private static readonly DateTime FakeDeliveryStart = new DateTime(2014, 9, 17);
        private static readonly DateTime FakeDeliveryEnd = new DateTime(2014, 9, 25);

        
        [SetUp]
        public void Setup()
        {
            _mockProductService = new Mock<IProductService>();
            _marketTickerMessageItemGenerator = new MarketTickerItemMessageGenerator(_mockProductService.Object);
        }

        [Test]
        public void GenerateMarketTickerItemMessageForNullDealShouldReturnAnEmptyString()
        {
            var mtMessage = _marketTickerMessageItemGenerator.GenerateMarketItemMessage((Deal) null, MessageAction.Create);
            Assert.That(mtMessage, Is.EqualTo(string.Empty));
        }

        [Test]
        public void GenerateMarketTickerItemMessageShouldReturnAPendingMessageWithAgressorSellingToRealBid()
        {
            Deal deal = GenerateDeal();
            deal.Matching.IsVirtual = true;

            _mockProductService.Setup(m => m.GetProductTenor(It.IsAny<long>())).Returns(MakeProductTenor());
            _mockProductService.Setup(m => m.GetProduct(It.IsAny<long>())).Returns(MakeProduct());
            var mtMessage = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(deal, MessageAction.Create);

            Assert.That(mtMessage, Is.EqualTo("PENDING:  XYZP sells 00bbl Naphtha, Dover, PortA to ABCP for 27-Sep-2014 - 10-Oct-2014 at $9.99/t"));
        }

        [Test]
        public void GenerateMarketTickerItemMessageShouldReturnDealVoidReasonWhenVoidingInternalDeal()
        {
            const string voidReason = "VOID reason was 'deal terms amended'";
            const string dealNotes = "Deal notes";
            var deal = GenerateDeal();
            deal.DealStatus = DealStatus.Void;
            deal.Message = string.Format("{0}. \n{1}", dealNotes, voidReason);
            deal.Initial.Notes = dealNotes;
            _mockProductService.Setup(m => m.GetProductTenor(It.IsAny<long>())).Returns(MakeProductTenor());
            _mockProductService.Setup(m => m.GetProduct(It.IsAny<long>())).Returns(MakeProduct());
            var mtMessage = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(deal, MessageAction.Create);

            StringAssert.StartsWith("VOIDED:  " + voidReason, mtMessage);
        }

        [Test]
        [TestCase(OrderType.Bid, "bids")]
        [TestCase(OrderType.Ask, "asks")]
        public void GenerateOrderMessageContainsCorrectBidAskString(OrderType orderType, string expectedString)
        {
            var order = GenerateOrder();
            order.OrderType = orderType;
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            StringAssert.Contains(expectedString, message);
        }

        [Test]
        public void GenerateOrderMessageContainsMetaDataBasedOnDisplayValues()
        {
            var order = GenerateOrder();
            order.OrderType = OrderType.Ask;
            
            order.MetaData = order.MetaData = new MetaData[]
            {
                new MetaData() {DisplayValue = "Dover"},
                new MetaData() {DisplayValue = "PortA"}
            };

            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            StringAssert.Contains(", Dover, PortA", message);
        }

        [Test]
        public void GenerateOrderMessageContainsOrderValues()
        {
            var order = GenerateOrder();
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            StringAssert.Contains(order.PrincipalOrganisationName, message);
            StringAssert.Contains(string.Format("{0:0,0}", order.Quantity), message);
            StringAssert.Contains(string.Format("{0:F}", order.Price), message);
            StringAssert.Contains(ToArgusDateTimeFormat(FakeDeliveryStart.AddDays(10)), message);
            StringAssert.Contains(ToArgusDateTimeFormat(FakeDeliveryEnd.AddDays(15)), message);
        }

        private static string ToArgusDateTimeFormat(DateTime dt)
        {
            return (new DateTime?(dt)).ToArgusDateTimeFormat();
        }

        [Test]
        public void GenerateOrderMessageForOrderWithBrokerOrganisationHasBrokerOrganisationName()
        {
            const string brokerName = "Brokers Inc";
            const long brokerId = 1234;
            var order = GenerateOrder();
            order.BrokerOrganisationId = brokerId;
            order.BrokerOrganisationName = brokerName;
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            StringAssert.Contains(brokerName, message);
        }

        [Test]
        public void GenerateOrderMessageForNonNullProductTenorHasTenorInformationInMessage()
        {
            var product = MakeProduct();
            _mockProductService.Setup(m => m.GetProductTenor(It.IsAny<long>())).Returns(MakeProductTenor());
            _mockProductService.Setup(s => s.GetProduct(FakeProductId)).Returns(() => product);
            var order = GenerateOrder();
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            //StringAssert.Contains(product.PriceUnitDisplayName, message);
            StringAssert.Contains(product.VolumeUnitDisplayName, message);
            //StringAssert.Contains(product.CurrencyDisplayName, message);
            StringAssert.Contains(product.Name, message);
        }

        [Test]
        public void GenerateOrderMessageForNullOrderReturnsEmptyString()
        {
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage((Order) null, MessageAction.Create);
            Assert.AreEqual(string.Empty, message);
        }

        [Test]
        public void TestThatProductTenorRelatedToNullProductIsHandledAndReturnsNonEmptyMessage()
        {
            _mockProductService.Setup(m => m.GetProductTenor(It.IsAny<long>())).Returns(MakeProductTenor());
            _mockProductService.Setup(s => s.GetProduct(It.IsAny<long>())).Returns(() => null);
            var order = GenerateOrder();
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            Assert.AreNotEqual(string.Empty, message);
        }

        [Test]
        public void TestThatExternalDealEnteredByPrincipalHasPrincipalOrganisationName()
        {
            var order = GenerateOrder();
            order.ExecutionMode = ExecutionMode.Internal;
            order.PrincipalOrganisationName = "Principal Organisation Ltd";
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            StringAssert.Contains(order.PrincipalOrganisationName, message);
        }

        [Test]
        public void TestThatExternalDealShowsPrincipalOrganisationName()
        {
            var order = GenerateOrder();
            order.ExecutionMode = ExecutionMode.External;
            order.PrincipalOrganisationId = 1234;
            order.PrincipalOrganisationName = "Principal Organisation Ltd";
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            StringAssert.Contains(order.PrincipalOrganisationName, message);
        }

        [Test]
        public void TestThatExternalDealAddedByBrokerOnlyShowsBrokerOrganisationName()
        {
            var order = GenerateOrder();
            order.ExecutionMode = ExecutionMode.External;
            order.PrincipalOrganisationId = 0;
            order.PrincipalOrganisationName = null;
            order.BrokerOrganisationId = 1234;
            order.BrokerOrganisationName = "Broker Org";
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(order, MessageAction.Create);
            StringAssert.Contains(order.BrokerOrganisationName, message);
        }

        [Test]
        public void TestThatNullMarketInfoIsHandledAndReturnsEmptyMessage()
        {
            string message = "test";
            Assert.DoesNotThrow(() => message = _marketTickerMessageItemGenerator.GenerateMarketInfoMessage(null));
            Assert.AreEqual(string.Empty, message);
        }

        [Test]
        [TestCase(MarketInfoStatus.Active, "NEW")]
        [TestCase(MarketInfoStatus.Pending, "PENDING")]
        [TestCase(MarketInfoStatus.Void, "VOIDED")]
        public void TestThatInfoStatusIsShownInMessage(MarketInfoStatus status, string expected)
        {
            var mi = new MarketInfo
            {
                Info = "Some fake market info",
                MarketInfoStatus = status
            };
            var message = _marketTickerMessageItemGenerator.GenerateMarketInfoMessage(mi);
            StringAssert.Contains(expected, message);
            StringAssert.Contains(mi.Info, message);
        }

        [Test]
        [TestCase(MessageAction.Open, "open")]
        [TestCase(MessageAction.Close, "close")]
        public void MarketOpenCloseMessageContainsCorrectActionAndProductName(MessageAction action, string expected)
        {
            var product = MakeProduct();
            var message = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(product, action);
            StringAssert.Contains(expected, message);
            StringAssert.Contains(product.Name, message);
        }


        [Test]
        public void GenerateOrderMessageForCoBrokeredOrderContainsAllPartyNames()
        {
            const string voidReason = "VOID reason was 'deal terms amended'";
            const string dealNotes = "Deal notes";
            var deal = GenerateDeal();
            deal.DealStatus = DealStatus.Executed;
            deal.Message = string.Format("{0}. \n{1}", dealNotes, voidReason);
            deal.Initial.Notes = dealNotes;
            deal.Initial.BrokerOrganisationId = 20;
            deal.Initial.BrokerOrganisationName = "NATHAN BROKERS";

            deal.Matching.BrokerOrganisationId = 30;
            deal.Matching.BrokerOrganisationName = "ARGUS BROKERS";
            
            _mockProductService.Setup(m => m.GetProductTenor(It.IsAny<long>())).Returns(MakeProductTenor());
            _mockProductService.Setup(m => m.GetProduct(It.IsAny<long>())).Returns(MakeProduct());
            var mtMessage = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(deal, MessageAction.Create);

            Assert.That(mtMessage, Is.EqualTo("NEW:  XYZP/ARGUS BROKERS sells 00bbl Naphtha, Dover, PortA to ABCP/NATHAN BROKERS for 27-Sep-2014 - 10-Oct-2014 at $9.99/t"));
        }

        [Test]
        public void GenerateExternalDealMessageWithMetadata()
        {
            var externalDeal = new ExternalDeal
            {
                DealStatus = DealStatus.Pending,
                FreeFormDealMessage = "Oleg buys 23t of Naphtha CIF NWE - Cargoes 09/Aug/2016 - 11/Aug/2016 from Peter at $123.00/t, Port A, 160/220, description",
                MetaData = new[]
                {
                    new MetaData {DisplayName = "Delivery Port", DisplayValue = "Port A"},
                    new MetaData {DisplayName = "Bitumen Grade", DisplayValue = "160/220"},
                    new MetaData {DisplayName = "Delivery Description", DisplayValue = "description"}
                }
            };

            var marketTickerMessage = _marketTickerMessageItemGenerator.GenerateMarketItemMessage(externalDeal, MessageAction.Create);
            Assert.That(marketTickerMessage, Is.EqualTo("PENDING:  Oleg buys 23t of Naphtha CIF NWE - Cargoes 09/Aug/2016 - 11/Aug/2016 from Peter at $123.00/t, Port A, 160/220, description"));
        }

        private Product MakeProduct()
        {
            var product = new Product
            {
                //PriceUnitCode = "MT",
                //PriceUnitDisplayName = "t",
                VolumeUnitCode = "BBL",
                VolumeUnitDisplayName = "bbl",
                //CurrencyCode = "USD",
                //CurrencyDisplayName = "$",
                Name = "Naphtha"
            };

            return product;
        }

        private ProductTenor MakeProductTenor()
        {
            var productTenor = new ProductTenor { DisplayName = "Product Tenor", ProductId = 1 };

            return productTenor;
        }

        private Deal GenerateDeal()
        {
            //Fixture fixture = new Fixture();
            // Populate the information required for a deal.  
            var testDate = new DateTime(2014, 9, 17);

            var deal = new Deal
            {
                DealStatus = DealStatus.Pending,
                Initial = GenerateOrder(),
                Matching = new Order
                {
                    OrderType = OrderType.Ask,
                    Price = 9.98M,
                    BrokerOrganisationName = "XYZB",
                    PrincipalOrganisationName = "XYZP",
                    DeliveryStartDate = testDate.AddDays(10),
                    DeliveryEndDate = testDate.AddDays(15),
                    ProductId = 1,
                    ProductName = "Foo"
                }
            };

            return deal;
        }

        private static Order GenerateOrder()
        {
            return new Order
            {
                OrderType = OrderType.Bid,
                Price = 9.99M,
                BrokerOrganisationName = "ABCB",
                PrincipalOrganisationName = "ABCP",
                DeliveryStartDate = FakeDeliveryStart.AddDays(10),
                DeliveryEndDate = FakeDeliveryEnd.AddDays(15),
                ProductId = 1,
                ProductName = "Foo",
                ProductTenor = new ProductTenor {Id = FakeProductTenorId},
                MetaData = new MetaData[]
                {
                    new MetaData() {DisplayValue = "Dover"},
                    new MetaData() {DisplayValue = "PortA"}
                },
                OrderPriceLines = new List<OrderPriceLine>()
                {
                    new OrderPriceLine()
                    {
                        PriceLine = new PriceLine()
                        {
                            SequenceNo = 1,
                            PriceLinesBases = new List<PriceLineBasis>()
                            {
                                new PriceLineBasis()
                                {
                                    SequenceNo = 1,
                                    PriceBasis = new PriceBasis()
                                    {
                                        PriceUnitCode = "t",
                                        CurrencyCode = "$"
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }

    }
}