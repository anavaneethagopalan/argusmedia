﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;
using Moq;
using NUnit.Framework;

// Avoid a clash between the actual service class and the test namespace
using MarketTickerServiceClass = AOM.Services.MarketTickerService.MarketTickerService;
using AOM.Transport.Events.Deals;

namespace AOM.Services.Tests.MarketTickerService
{
    [TestFixture]
    public class MarketTickerServiceTests
    {
        private Mock<IMarketTickerItemMessageGenerator> _mockMessageGenerator;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private MarketTickerServiceClass _service;
        private MockAomModel _aomModel;

        private const string ProductName = "FakeProduct";
        private const string TenorName = "FakeTenor";
        private const string ProductTenorName = "FakeProductTenor";
        private const string DeliveryLocation = "FakeLocation";

        [SetUp]
        public void Setup()
        {
            _mockMessageGenerator = new Mock<IMarketTickerItemMessageGenerator>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _service = new MarketTickerServiceClass(_mockMessageGenerator.Object,
                                                    _mockDateTimeProvider.Object);
            _aomModel = new MockAomModel();
            AomDataBuilder.WithModel(_aomModel)
                .AddDeliveryLocation(DeliveryLocation)
                .AddProduct(ProductName)
                .AddTenorCode(TenorName)
                .AddProductTenor(ProductTenorName);
            }

        //-------------------------------------------------
        // Market Info tests
        //-------------------------------------------------
        private static AomMarketInfoResponse CreateMarketInfoResponse(
            MarketInfoStatus status = MarketInfoStatus.Active,
            long id = 1234,
            long ownerOrganisationId = 123,
            string info = "I am some info",
            List<long> productIds = null,
            long messageOrganisationId = 234,
            long messageUserId = 345)
        {
            var mi = new MarketInfo
            {
                Id = id,
                OwnerOrganisationId = ownerOrganisationId,
                Info = info,
                MarketInfoStatus = status,
                ProductIds = productIds ?? new List<long>()
            };
            var message = new Message
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = messageOrganisationId,
                    UserId = messageUserId
                }
            };
            return new AomMarketInfoResponse
            {
                MarketInfo = mi,
                Message = message,
                UserContactDetails = new UserContactDetails()
            };
        }

        private static MarketTickerItem GetItemFromSingleMessage(IList<Message> messages)
        {
            Assert.IsNotNull(messages);
            Assert.That(messages.Count, Is.EqualTo(1));
            return GetItemFromMessage(messages, 0);
        }

        private static MarketTickerItem GetItemFromMessage(IList<Message> messages, int messageIndex)
        {
            Assert.IsNotNull(messages);
            Assert.That(messages.Count, Is.GreaterThanOrEqualTo(messageIndex+1));
            var m = messages[messageIndex];
            var messageItem = m.MessageBody as MarketTickerItem;
            Assert.IsNotNull(messageItem);
            return messageItem;
        }

        [Test]
        public void CreatedMarketTickShouldHaveValuesFromMarketInfo()
        {
            const long id = 56789;
            const long ownerOrganisationId = 4567;
            const long messageOrganisationId = 1234;
            const long messageUserId = 2345;
            var response = CreateMarketInfoResponse(id: id,
                                                    ownerOrganisationId: ownerOrganisationId,
                                                    messageOrganisationId: messageOrganisationId,
                                                    messageUserId: messageUserId);
            var messages = _service.CreateMarketInfoMarketTick(_aomModel, response);
            var messageItem = GetItemFromSingleMessage(messages);
            Assert.That(messageItem.MarketInfoId, Is.EqualTo(id));
            Assert.That(messageItem.OwnerOrganisations.Count(i => i == ownerOrganisationId), Is.EqualTo(1));
            Assert.That(messageItem.OwnerOrganisations.Count(i => i == messageOrganisationId), Is.EqualTo(1));
            Assert.That(messageItem.MarketTickerItemType, Is.EqualTo(MarketTickerItemType.Info));
            Assert.That(messageItem.LastUpdatedUserId, Is.EqualTo(messageUserId));
        }

        [Test]
        public void CreatedMarketTickShouldHaveMessageCreatedByMessageGenerator()
        {
            const string fakeMessage = "I am a market info message. How interesting.";
            _mockMessageGenerator.Setup(g => g.GenerateMarketInfoMessage(It.IsAny<MarketInfo>()))
                                 .Returns(fakeMessage);
            var response = CreateMarketInfoResponse();
            var messages = _service.CreateMarketInfoMarketTick(_aomModel, response);
            var messageItem = GetItemFromSingleMessage(messages);
            Assert.That(messageItem.Message, Is.EqualTo(fakeMessage));
        }

        [Test]
        public void CreatedMarketTickShouldHaveLastUpdatedSetToUtcNow()
        {
            var fakeLastUpdatedTime = new DateTime(2015, 01, 09, 14, 45, 0);
            _mockDateTimeProvider.Setup(p => p.UtcNow).Returns(fakeLastUpdatedTime);
            var response = CreateMarketInfoResponse();
            var messages = _service.CreateMarketInfoMarketTick(_aomModel, response);
            var messageItem = GetItemFromSingleMessage(messages);
            Assert.That(messageItem.LastUpdated, Is.EqualTo(fakeLastUpdatedTime));
        }

        [Test]
        [TestCase(MarketInfoStatus.Active, MarketTickerItemStatus.Active)]
        [TestCase(MarketInfoStatus.Pending, MarketTickerItemStatus.Pending)]
        [TestCase(MarketInfoStatus.Void, MarketTickerItemStatus.Void)]
        public void ShouldSetCorrectItemStatusBasedOnStatusOfMarketInfo(
            MarketInfoStatus marketInfoStatus, MarketTickerItemStatus expectedStatus)
        {
            var response = CreateMarketInfoResponse(status: marketInfoStatus);
            var messages = _service.CreateMarketInfoMarketTick(_aomModel, response);
            var messageItem = GetItemFromSingleMessage(messages);
            Assert.That(messageItem.MarketTickerItemStatus, Is.EqualTo(expectedStatus));
        }

        [Test]
        public void EnsureExistingPendingMarketInfoIsMarkedAsVoidWhenThereIsANewMarketInfo()
        {
            MarketInfoDto infoDto;
            MarketTickerItemDto itemDto;
            AomDataBuilder.WithModel(_aomModel)
                          .AddMarketInfoItem(1, "info", "P", new DateTime(2015, 01, 09), out infoDto)
                          .AddMarketInfoMarketTickerItem(infoDto.Id, out itemDto, "P");
            var response = CreateMarketInfoResponse(MarketInfoStatus.Active, infoDto.Id);
            var messages = _service.CreateMarketInfoMarketTick(_aomModel, response);
            Assert.IsNotNull(messages);
            Assert.That(messages.Count, Is.EqualTo(2));
            var firstMessageItem = GetItemFromMessage(messages, messageIndex: 0);
            Assert.That(firstMessageItem.Id, Is.EqualTo(itemDto.Id));
            Assert.That(firstMessageItem.MarketTickerItemStatus, Is.EqualTo(MarketTickerItemStatus.Void));
        }

        [Test]
        public void ShouldAddAMarketTickerItemProductChannelPerProductInTheMarketInfoMessage()
        {
            var productChannels = new List<long> {2, 4, 6};
            var response = CreateMarketInfoResponse(productIds: productChannels);
            _service.CreateMarketInfoMarketTick(_aomModel, response);
            var aomChannelProductIds = _aomModel.MarketTickerItemProductChannels.Select(c => c.ProductId);
            Assert.That(aomChannelProductIds, Is.EquivalentTo(productChannels));
        }

        [Test]
        public void GetItemsForRecoveryShouldIncludeOnlyItemsWithinTheRecoveryTimeRange()
        {
            // Arrange
            const int userId = 123;
            var dateCreated = new DateTime(2015, 1, 15);
            var t1 = new DateTime(2015, 1, 16);
            var t2 = new DateTime(2015, 1, 17);
            var itemData = new[]
            {
                new {Time = t1 - TimeSpan.FromHours(1), OrderId = 1},
                new {Time = t1 + TimeSpan.FromHours(1), OrderId = 2},
                new {Time = t2 - TimeSpan.FromHours(1), OrderId = 3},
                new {Time = t2 + TimeSpan.FromHours(1), OrderId = 4}
            };
            var mtiDtos = new List<MarketTickerItemDto>();
            foreach (var data in itemData)
            {
                MarketTickerItemDto item;
                AomDataBuilder
                    .WithModel(_aomModel)
                    .AddMarketTickerItem(userId,
                                         data.OrderId,
                                         dateCreated,
                                         data.Time,
                                         ProductName,
                                         out item,
                                         DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Active),
                                         DtoMappingAttribute.GetValueFromEnum(MarketTickerItemType.Bid));
                mtiDtos.Add(item);
            }

            // Act
            var recoveredMessages = _service.GetItemsForRecovery(_aomModel, t1, t2);

            // Assert
            Assert.IsNotNull(recoveredMessages);
            Assert.That(recoveredMessages.Count, Is.EqualTo(2));
            var recoveredItems =
                recoveredMessages.Select(m => m.MessageBody).OfType<MarketTickerItem>().ToList();
            Assert.That(recoveredItems.Count, Is.EqualTo(2));
            Assert.That(recoveredItems[0].Id, Is.EqualTo(mtiDtos[1].Id));
            Assert.That(recoveredItems[1].Id, Is.EqualTo(mtiDtos[2].Id));
        }

        [Test]
        public void ShouldNotOverwriteProductIdForOrderMarketTickItemsInGetItemsForRecovery()
        {
            // Arrange
            var t1 = new DateTime(2015, 1, 16);
            var t2 = new DateTime(2015, 1, 17);
            MarketTickerItemDto mtiDto;
            AomDataBuilder
                .WithModel(_aomModel)
                .AddMarketTickerItem(123,
                                     456,
                                     t1,
                                     t1 + TimeSpan.FromHours(1),
                                     ProductName,
                                     out mtiDto,
                                     DtoMappingAttribute.GetValueFromEnum(MarketTickerItemStatus.Active),
                                     DtoMappingAttribute.GetValueFromEnum(MarketTickerItemType.Bid));

            // Act
            var recoveredMessages = _service.GetItemsForRecovery(_aomModel, t1, t2);

            // Assert
            Assert.IsNotNull(recoveredMessages);
            Assert.That(recoveredMessages.Count, Is.EqualTo(1));
            var recoveredItems =
                recoveredMessages.Select(m => m.MessageBody).OfType<MarketTickerItem>().ToList();
            Assert.That(recoveredItems.Count, Is.EqualTo(1));
            var item1 = recoveredItems[0];
            var expectedProductIds =
                _aomModel.Products.Where(p => p.Name == ProductName).Select(p => p.Id).ToList();
            Assert.That(expectedProductIds.Count, Is.EqualTo(1));
            Assert.That(item1.ProductIds, Is.EquivalentTo(expectedProductIds));
        }

        const int FakeTraderId = 123;
        const long FakeEditorId = 456;
        const long FakeMarketInfoId = 789;

        [Test]
        public void ShouldHaveTraderAsOwnerForEditorVoidPendingInfoFromTrader()
        {
            // Arrange
            var response = CreateMarketInfoResponse(status: MarketInfoStatus.Pending,
                                                    id: FakeMarketInfoId,
                                                    ownerOrganisationId: FakeTraderId);
            response.Message.MessageAction = MessageAction.Void;
            response.Message.ClientSessionInfo.OrganisationId = FakeEditorId;

            // Act
            _service.CreateMarketInfoMarketTick(_aomModel, response);

            // Assert
            Assert.That(_aomModel.MarketTickerItems.Count(), Is.EqualTo(1));
            var mti = _aomModel.MarketTickerItems.First();
            Assert.That(mti.OwnerOrganisationId1, Is.EqualTo(FakeTraderId));
            Assert.That(mti.OwnerOrganisationId2.HasValue, Is.False);
            Assert.That(mti.OwnerOrganisationId3.HasValue, Is.False);
            Assert.That(mti.OwnerOrganisationId4.HasValue, Is.False);
        }

        [Test]
        public void ShouldHaveTraderAsOwnerForEditorVerifyPendingInfoFromTrader()
        {
            // Arrange
            var response = CreateMarketInfoResponse(status: MarketInfoStatus.Pending,
                                                    ownerOrganisationId: FakeTraderId);
            response.Message.MessageAction = MessageAction.Verify;
            response.Message.ClientSessionInfo.OrganisationId = FakeEditorId;

            // Act
            _service.CreateMarketInfoMarketTick(_aomModel, response);

            // Assert
            Assert.That(_aomModel.MarketTickerItems.Count(), Is.EqualTo(1));
            var mti = _aomModel.MarketTickerItems.First();
            Assert.That(mti.OwnerOrganisationId1, Is.EqualTo(FakeTraderId));
            Assert.That(mti.OwnerOrganisationId2.HasValue, Is.False);
            Assert.That(mti.OwnerOrganisationId3.HasValue, Is.False);
            Assert.That(mti.OwnerOrganisationId4.HasValue, Is.False);
        }

        [Test]
        public void ShouldHaveTraderAndEditorAsOwnersForEditorUpdatePendingInfoFromTrader()
        {
            // Arrange
            var response = CreateMarketInfoResponse(status: MarketInfoStatus.Pending,
                                                    ownerOrganisationId: FakeTraderId);
            response.Message.MessageAction = MessageAction.Update;
            response.Message.ClientSessionInfo.OrganisationId = FakeEditorId;

            // Act
            _service.CreateMarketInfoMarketTick(_aomModel, response);

            // Assert
            Assert.That(_aomModel.MarketTickerItems.Count(), Is.EqualTo(1));
            var mti = _aomModel.MarketTickerItems.First();
            Assert.That(mti.OwnerOrganisationId1, Is.EqualTo(FakeTraderId));
            Assert.That(mti.OwnerOrganisationId2, Is.EqualTo(FakeEditorId));
            Assert.That(mti.OwnerOrganisationId3.HasValue, Is.False);
            Assert.That(mti.OwnerOrganisationId4.HasValue, Is.False);
        }

        [Test]
        public void ShouldHaveTraderAndEditorAsOwnersForEditorUpdateThenVoidPendingInfoFromTrader()
        {
            // Arrange
            var response1 = CreateMarketInfoResponse(status: MarketInfoStatus.Pending,
                                                     id: FakeMarketInfoId,
                                                     ownerOrganisationId: FakeTraderId);
            response1.Message.MessageAction = MessageAction.Update;
            response1.Message.ClientSessionInfo.OrganisationId = FakeEditorId;

            var response2 = CreateMarketInfoResponse(status: MarketInfoStatus.Pending,
                                                     id: FakeMarketInfoId,
                                                     ownerOrganisationId: FakeTraderId);
            response2.Message.MessageAction = MessageAction.Void;
            response2.Message.ClientSessionInfo.OrganisationId = FakeEditorId;

            // Act
            _service.CreateMarketInfoMarketTick(_aomModel, response1);
            _service.CreateMarketInfoMarketTick(_aomModel, response2);

            // Assert
            Assert.That(_aomModel.MarketTickerItems.Count(), Is.EqualTo(2));
            var mti = _aomModel.MarketTickerItems.Last();
            Assert.That(mti.OwnerOrganisationId1, Is.EqualTo(FakeTraderId));
            Assert.That(mti.OwnerOrganisationId2, Is.EqualTo(FakeEditorId));
            Assert.That(mti.OwnerOrganisationId3.HasValue, Is.False);
            Assert.That(mti.OwnerOrganisationId4.HasValue, Is.False);
        }

        [Test]
        public void ShouldHaveTraderAndEditorAsOwnersForEditorUpdateThenVerifyPendingInfoFromTrader()
        {
            // Arrange
            var response1 = CreateMarketInfoResponse(status: MarketInfoStatus.Pending,
                                                     id: FakeMarketInfoId,
                                                     ownerOrganisationId: FakeTraderId);
            response1.Message.MessageAction = MessageAction.Update;
            response1.Message.ClientSessionInfo.OrganisationId = FakeEditorId;

            var response2 = CreateMarketInfoResponse(status: MarketInfoStatus.Pending,
                                                     id: FakeMarketInfoId,
                                                     ownerOrganisationId: FakeTraderId);
            response2.Message.MessageAction = MessageAction.Verify;
            response2.Message.ClientSessionInfo.OrganisationId = FakeEditorId;

            // Act
            _service.CreateMarketInfoMarketTick(_aomModel, response1);
            _service.CreateMarketInfoMarketTick(_aomModel, response2);

            // Assert
            Assert.That(_aomModel.MarketTickerItems.Count(), Is.EqualTo(2));
            var mti = _aomModel.MarketTickerItems.Last();
            Assert.That(mti.OwnerOrganisationId1, Is.EqualTo(FakeTraderId));
            Assert.That(mti.OwnerOrganisationId2, Is.EqualTo(FakeEditorId));
            Assert.That(mti.OwnerOrganisationId3.HasValue, Is.False);
            Assert.That(mti.OwnerOrganisationId4.HasValue, Is.False);
        }

        [Test]
        public void EnsureCorrectMarketTickerItemCreatedFromDealResponse()
        {
            // Arrange
            var deal = new Deal
            {
                Id = 9,
                DealStatus = DealStatus.Executed,
                InitialOrderId = 4,
                Initial = new Order { ProductId = 4, PrincipalOrganisationId = 2, Notes = "note1", CoBrokering = true },
                Matching = new Order { ProductId = 4, BrokerOrganisationId = 4, PrincipalOrganisationId = 3, Notes = "note2", CoBrokering = false }
            };

            var message = new Message
            {
                MessageAction = MessageAction.Create,
                MessageType = MessageType.Deal,
                ClientSessionInfo = new ClientSessionInfo { UserId = 123, OrganisationId = 3, SessionId = "BCDEFG" }
            };

            var dealResponse = new AomDealResponse { Deal = deal, Message = message };

            var utcNow = new DateTime(2016, 6, 14, 12, 30, 0);
            _mockDateTimeProvider.Setup(x => x.UtcNow).Returns(utcNow);

            // Act
            _service.CreateDealMarketTick(_aomModel, dealResponse);

            // Assert
            var marketTickerItem = _aomModel.MarketTickerItems.FirstOrDefault();

            Assert.NotNull(marketTickerItem);
            Assert.AreEqual(deal.Id, marketTickerItem.DealId);
            Assert.AreEqual("A", marketTickerItem.MarketTickerItemStatus);
            Assert.AreEqual("D", marketTickerItem.MarketTickerItemType);
            Assert.AreEqual(deal.Initial.ProductId, marketTickerItem.ProductId);
            Assert.AreEqual(deal.InitialOrderId, marketTickerItem.OrderId);
            Assert.AreEqual(deal.Initial.PrincipalOrganisationId, marketTickerItem.OwnerOrganisationId1);
            Assert.AreEqual(deal.Matching.BrokerOrganisationId, marketTickerItem.OwnerOrganisationId4);
            Assert.AreEqual(deal.Matching.PrincipalOrganisationId, marketTickerItem.OwnerOrganisationId3);
            Assert.AreEqual(deal.Initial.Notes, marketTickerItem.Notes);
            Assert.AreEqual(false, marketTickerItem.CoBrokering);
            Assert.AreEqual(utcNow, marketTickerItem.DateCreated);
            Assert.AreEqual(utcNow, marketTickerItem.LastUpdated);
            Assert.AreEqual(message.ClientSessionInfo.UserId, marketTickerItem.LastUpdatedUserId);
        }

        [Test]
        public void EnsureFourOrganisationsAreIncludedInMarketTickerItemWhenFourWayDealExecuted()
        {
            // Arrange
            var deal = new Deal
            {
                Id = 9,
                DealStatus = DealStatus.Executed,
                InitialOrderId = 4,
                Initial = new Order { ProductId = 4, PrincipalOrganisationId = 2, BrokerOrganisationId = 3, BrokerId = 3, Notes = "note1", CoBrokering = true },
                Matching = new Order { ProductId = 4, BrokerOrganisationId = 5, PrincipalOrganisationId = 6, Notes = "note2", CoBrokering = false }
            };

            var message = new Message
            {
                MessageAction = MessageAction.Create,
                MessageType = MessageType.Deal,
                ClientSessionInfo = new ClientSessionInfo { UserId = 123, OrganisationId = 3, SessionId = "BCDEFG" }
            };

            var dealResponse = new AomDealResponse { Deal = deal, Message = message };

            var utcNow = new DateTime(2016, 6, 14, 12, 30, 0);
            _mockDateTimeProvider.Setup(x => x.UtcNow).Returns(utcNow);

            // Act
            _service.CreateDealMarketTick(_aomModel, dealResponse);

            // Assert
            var marketTickerItem = _aomModel.MarketTickerItems.FirstOrDefault();


            Assert.That(marketTickerItem.OwnerOrganisationId1.Value, Is.EqualTo(2));
            Assert.That(marketTickerItem.OwnerOrganisationId2.Value, Is.EqualTo(3));
            Assert.That(marketTickerItem.OwnerOrganisationId3.Value, Is.EqualTo(6));
            Assert.That(marketTickerItem.OwnerOrganisationId4.Value, Is.EqualTo(5));
        }
    }
}
