﻿using AOM.Transport.Service.Processor.Common;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Modules;

namespace AOM.Services.Tests
{
    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {

            kernel.Bind(
                x =>
                    x.FromThisAssembly()
                        .IncludingNonePublicTypes()
                        .SelectAllClasses()
                        .InheritedFrom<IConsumer>()
                        .BindAllInterfaces()
                        .Configure(b => b.InThreadScope()));
            // InThreadScope used as orginal code used 'ToConstant'          
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}