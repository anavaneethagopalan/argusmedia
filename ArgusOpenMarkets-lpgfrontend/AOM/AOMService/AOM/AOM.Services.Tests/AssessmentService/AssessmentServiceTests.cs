namespace AOM.Services.Tests.AssessmentService
{
    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Mappers;
    using AOM.Repository.MySql.Aom;
    using AOM.Repository.MySql.Tests;
    using AOM.Repository.MySql.Tests.Aom;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class AssessmentServiceTests
    {
        private static readonly DateTimeProvider DateTimeProvider = new DateTimeProvider();
        private MockAomModel _mockModel;
        private const long FakeProductId = 1;
        private const long NonExistantProductId = 90210;

        private readonly Dictionary<string, AssessmentDto> _fakeAssessments = new Dictionary<string, AssessmentDto>
        {
            {
                "Today", new AssessmentDto
                {
                    Id = 1001,
                    BusinessDate = DateTimeProvider.UtcNow.Date,
                    ProductId = FakeProductId,
                    PriceHigh = 999.50M,
                    PriceLow = 650.00M,
                    DateCreated = DateTimeProvider.Now.ToUniversalTime(),
                    LastUpdated = DateTimeProvider.Now.ToUniversalTime(),
                    EnteredByUserId = 999,
                    LastUpdatedUserId = 999,
                    AssessmentStatus = "n"
                }
            },
            {
                "Previous", new AssessmentDto
                {
                    Id = 1000,
                    BusinessDate = new DateTime(2009, 01, 23).Date,
                    ProductId = FakeProductId,
                    PriceHigh = 888.50M,
                    PriceLow = 777.00M,
                    DateCreated = DateTimeProvider.Now.ToUniversalTime(),
                    LastUpdated = DateTimeProvider.Now.ToUniversalTime(),
                    EnteredByUserId = 999,
                    LastUpdatedUserId = 999,
                    AssessmentStatus = "n"
                }
            }
        };

        private static void AssertErrorMessageContains(string errorMsg, string expectedContains)
        {
            Assert.IsTrue(errorMsg.ToUpperInvariant().Contains(expectedContains.ToUpperInvariant()), 
                String.Format("Expect error message to contain '{0}' but it was: '{1}'", expectedContains, errorMsg));
        }

        [SetUp]
        public void Setup()
        {
            // It would be better to use const decimal values for the setup min/max/stepping values and
            // then the test case argument attributes, but .NET doesn't allow decimal attribute arguments.
            _mockModel = new MockAomModel();
            AomDataBuilder.WithModel(_mockModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct", 1, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m);
        }

        [Test]
        public void GetCombinedAssessmentTest()
        {
            var previous = _fakeAssessments["Previous"];
            var today = _fakeAssessments["Today"];

            _mockModel.Assessments.Add(previous);
            _mockModel.Assessments.Add(today);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);
            var assessment = service.GetCombinedAssessmentForProduct(1);

            //check today
            Assert.That(assessment.Today.Id, Is.EqualTo(today.Id));
            Assert.That(assessment.Today.BusinessDate, Is.EqualTo(today.BusinessDate));
            Assert.That(assessment.Today.PriceHigh, Is.EqualTo(today.PriceHigh));

            //check previous
            Assert.That(assessment.Previous.Id, Is.EqualTo(previous.Id));
            Assert.That(assessment.Previous.BusinessDate, Is.EqualTo(previous.BusinessDate));
            Assert.That(assessment.Previous.PriceHigh, Is.EqualTo(previous.PriceHigh));
        }

        [Test]
        public void InsertNewAssessmentTest()
        {
            var today = _fakeAssessments["Today"];
            var previous = _fakeAssessments["Previous"];

            var newtoday = new AssessmentDto
            {
                Id = 1003,
                BusinessDate = new DateTime(2009, 01, 24).Date,
                ProductId = 1,
                PriceHigh = 2000,
                PriceLow = 1000,
                DateCreated = DateTimeProvider.Now.ToUniversalTime(),
                LastUpdated = DateTimeProvider.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "n"
            };

            _mockModel.Assessments.Add(today);
            _mockModel.Assessments.Add(previous);

            var combinedAssessment = new CombinedAssessment
            {
                Today = newtoday.ToEntity(),
                Previous = previous.ToEntity(),
                ProductId = 1
            };

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);
            service.UpdateCombinedAssessment(combinedAssessment);

            //assert that save changes has been called
            Assert.That(_mockModel.SaveChangesCalledCount, Is.GreaterThan(0));
            //check that no new assessments were added, just the existing ones changed
            Assert.That(_mockModel.Assessments.Count(), Is.EqualTo(3));
        }

        private static AssessmentDto MakeAssessmentDto(long id, DateTime businessDate, long productId, decimal priceLow = 50M, decimal priceHigh = 500M, string assessmentStatus = "R")
        {
            var newtoday = new AssessmentDto
            {
                Id = id,
                BusinessDate = businessDate,
                ProductId = productId,
                PriceLow = priceLow,
                PriceHigh =  priceHigh, 
                DateCreated = DateTimeProvider.Now.ToUniversalTime(),
                LastUpdated = DateTimeProvider.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = assessmentStatus
            };
            return newtoday;
        }

        [Test]
        public void CalculateTrendFromPreviousAssesment_Upward()
        {
            var tminus2 = MakeAssessmentDto(1001, new DateTime(2009, 01, 22).Date, 1, 900.50m, 500.00m);
            var previous = MakeAssessmentDto(1002, new DateTime(2009, 01, 23).Date, 1, 777.00m, 877.5m);
            var today = MakeAssessmentDto(1003, DateTimeProvider.UtcNow.Date, 1, 778.00m, 877.5m); 

            _mockModel.Assessments.Add(tminus2);
            _mockModel.Assessments.Add(previous);
            _mockModel.Assessments.Add(today);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());
            var result = service.GetCombinedAssessmentForProduct(1);
            Assert.That(result.Previous.TrendFromPreviousAssesment.Equals("Upward"));
            Assert.That(result.Today.TrendFromPreviousAssesment.Equals("Upward"));
        }

        [Test]
        public void CalculateTrendFromPreviousAssesment_Downward()
        {
            var tminus2 = new AssessmentDto
            {
                Id = 1001,
                //BusinessDate = "22.01.2009",
                BusinessDate = new DateTime(2009, 01, 22).Date,
                ProductId = 1,
                PriceHigh = (decimal) 200.50,
                PriceLow = (decimal) 777.00,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };
            var previous = new AssessmentDto
            {
                Id = 1002,
                //BusinessDate = "23.01.2009",
                BusinessDate = new DateTime(2009, 01, 23).Date,
                ProductId = 1,
                PriceHigh = (decimal) 777.00,
                PriceLow = (decimal) 100.50,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };
            var today = new AssessmentDto
            {
                Id = 1003,
                BusinessDate = DateTimeProvider.UtcNow.Date,
                ProductId = 1,
                PriceHigh = (decimal) 790.00,
                PriceLow = (decimal) 70.50,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };

            _mockModel.Assessments.Add(tminus2);
            _mockModel.Assessments.Add(previous);
            _mockModel.Assessments.Add(today);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());
            var result = service.GetCombinedAssessmentForProduct(1);
            Assert.That(result.Previous.TrendFromPreviousAssesment.Equals("Downward"));
            Assert.That(result.Today.TrendFromPreviousAssesment.Equals("Downward"));
        }
        
        [Test]
        public void CalculateTrendFromPreviousAssesment_MixedMean()
        {
            var tminus2 = new AssessmentDto
            {
                Id = 1001,
                BusinessDate = DateTimeProvider.Now.AddDays(-3).Date,
                ProductId = 1,
                PriceHigh = (decimal) 777.50,
                PriceLow = (decimal) 106.00,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };
            var previous = new AssessmentDto
            {
                Id = 1002,
                BusinessDate = DateTimeProvider.Now.AddDays(-1).Date,
                ProductId = 1,
                PriceHigh = (decimal) 770.00,
                PriceLow = (decimal) 100.50,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };
            var today = new AssessmentDto
            {
                Id = 1003,
                BusinessDate = DateTimeProvider.UtcNow.Date,
                ProductId = 1,
                PriceHigh = (decimal) 770.00,
                PriceLow = (decimal) 105.50,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };

            _mockModel.Assessments.Add(tminus2);
            _mockModel.Assessments.Add(previous);
            _mockModel.Assessments.Add(today);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());
            var result = service.GetCombinedAssessmentForProduct(1);
            Assert.That(result.Previous.TrendFromPreviousAssesment.Equals("Downward"));
            Assert.That(result.Today.TrendFromPreviousAssesment.Equals("Upward"));
        }


        [Test]
        public void CalculateTrendFromPreviousAssesment_UpEqual()
        {
            var tminus2 = new AssessmentDto
            {
                Id = 1001,
                //BusinessDate = "22.01.2009",
                BusinessDate = new DateTime(2009, 01, 22).Date,
                ProductId = 1,
                PriceHigh = (decimal) 200.50,
                PriceLow = (decimal) 777.00,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };
            var previous = new AssessmentDto
            {
                Id = 1002,
                //BusinessDate = "23.01.2009",
                BusinessDate = new DateTime(2009, 01, 23).Date,
                ProductId = 1,
                PriceHigh = (decimal) 300.50,
                PriceLow = (decimal) 777.00,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };
            var today = new AssessmentDto
            {
                Id = 1003,
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                ProductId = 1,
                PriceHigh = (decimal) 300.50,
                PriceLow = (decimal) 777.00,
                DateCreated = DateTime.Now.ToUniversalTime(),
                LastUpdated = DateTime.Now.ToUniversalTime(),
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                AssessmentStatus = "F"
            };

            _mockModel.Assessments.Add(tminus2);
            _mockModel.Assessments.Add(previous);
            _mockModel.Assessments.Add(today);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());
            var result = service.GetCombinedAssessmentForProduct(1);
            Assert.That(result.Previous.TrendFromPreviousAssesment.Equals("Upward"));
            Assert.That(result.Today.TrendFromPreviousAssesment.Equals("Equal"));
        }

        [Test]
        public void SingleAssessmentCannotBeAddedForNonExistantProduct()
        {
            // Arrange
            var assessment = new Assessment
            {
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                DateCreated = DateTimeProvider.UtcNow,
                LastUpdated = DateTimeProvider.UtcNow,
                ProductId = NonExistantProductId,
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                PriceLow = 500m,
                PriceHigh = 1000m
            };
            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);

            // Act and assert
            var e = Assert.Throws<BusinessRuleException>(() => service.InsertSingleAssessment(assessment));

            AssertErrorMessageContains(e.Message, "Product");
            AssertErrorMessageContains(e.Message, "could not be found");
        }

        [Test]
        public void SingleAssessmentCannotBeAddedIfLowPriceGreaterThanHighPrice()
        {
            const decimal lowPrice = 200m;
            const decimal highPrice = 199m;

            // Arrange
            var assessment = new Assessment
            {
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                DateCreated = DateTimeProvider.UtcNow,
                LastUpdated = DateTimeProvider.UtcNow,
                ProductId = FakeProductId,
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                PriceLow = lowPrice,
                PriceHigh = highPrice,
                AssessmentStatus = AssessmentStatus.None
            };
            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);

            // Act and assert
            var e = Assert.Throws<BusinessRuleException>(() => service.InsertSingleAssessment(assessment));

            AssertErrorMessageContains(e.Message, string.Format("price high ({0}) cannot be lower than price low ({1})", highPrice, lowPrice));
        }

        [Test]
        public void SingleAssessmentCannotBeAddedIfLowPriceIsLessThanProductMin()
        {
            // Arrange
            var assessment = new Assessment
            {
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                DateCreated = DateTimeProvider.UtcNow,
                LastUpdated = DateTimeProvider.UtcNow,
                ProductId = FakeProductId,
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                PriceLow = 95m,
                PriceHigh = 500m,
                AssessmentStatus = AssessmentStatus.None
            };
            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);

            // Act and assert
            var e = Assert.Throws<BusinessRuleException>(() => service.InsertSingleAssessment(assessment));

            AssertErrorMessageContains(e.Message, "low");
            AssertErrorMessageContains(e.Message, "is invalid");
        }

        [Test]
        public void SingleAssessmentCannotBeAddedIfHighPriceIsGreaterThanProductMax()
        {
            // Arrange
            var assessment = new Assessment
            {
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                DateCreated = DateTimeProvider.UtcNow,
                LastUpdated = DateTimeProvider.UtcNow,
                ProductId = FakeProductId,
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                PriceLow = 500m,
                PriceHigh = 6000m,
                AssessmentStatus = AssessmentStatus.None
            };
            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);

            // Act and assert
            var e = Assert.Throws<BusinessRuleException>(() => service.InsertSingleAssessment(assessment));

            AssertErrorMessageContains(e.Message, "high");
            AssertErrorMessageContains(e.Message, "is invalid");
        }

        [Test]
        [TestCase("100.0", "100.25", "high")]
        [TestCase("100.25", "100.5", "low")]
        public void SingleAssessmentCannotBeAddedIfPriceNotMultipleOfStepping(decimal lowPrice, decimal highPrice, string priceName)
        {
            // Arrange
            var assessment = new Assessment
            {
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                DateCreated = DateTimeProvider.UtcNow,
                LastUpdated = DateTimeProvider.UtcNow,
                ProductId = FakeProductId,
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                PriceLow = lowPrice,
                PriceHigh = highPrice,
                AssessmentStatus = AssessmentStatus.None
            };
            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);

            // Act and assert
            var e = Assert.Throws<BusinessRuleException>(() => service.InsertSingleAssessment(assessment));

            AssertErrorMessageContains(e.Message, priceName);
            AssertErrorMessageContains(e.Message, "is invalid");
        }


        [Test]
        [TestCase("100", "100.25", "high")]
        [TestCase("100.25", "100.5", "low")]
        public void CombinedAssessmentCannotBeUpdatedIfPriceIsNotMultipleOfStepping(decimal lowPrice, decimal highPrice, string priceName)
        {
            // Arrange
            var today = new Assessment
            {
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                DateCreated = DateTimeProvider.UtcNow,
                LastUpdated = DateTimeProvider.UtcNow,
                ProductId = FakeProductId,
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                PriceLow = lowPrice,
                PriceHigh = highPrice,
                AssessmentStatus = AssessmentStatus.None
            };
            var previous = _fakeAssessments["Previous"].ToEntity();
            var assessment = new CombinedAssessment
            {
                Previous = previous,
                Today = today,
                ProductId = FakeProductId
            };

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);

            // Act and assert
            var e = Assert.Throws<BusinessRuleException>(() => service.UpdateCombinedAssessment(assessment));

            AssertErrorMessageContains(e.Message, priceName);
            AssertErrorMessageContains(e.Message, "is invalid");
        }

        [Test]
        public void CombinedAssessmentCannotBeUpdatedIfLowPriceIsLessThanProductMin()
        {
            // Arrange
            var today = new Assessment
            {
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                DateCreated = DateTimeProvider.UtcNow,
                LastUpdated = DateTimeProvider.UtcNow,
                ProductId = FakeProductId,
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                PriceLow = 95m,
                PriceHigh = 500m,
                AssessmentStatus = AssessmentStatus.None
            };
            var previous = _fakeAssessments["Previous"].ToEntity();
            var assessment = new CombinedAssessment
            {
                Previous = previous,
                Today = today,
                ProductId = FakeProductId
            };

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), DateTimeProvider);

            // Act and assert
            var e = Assert.Throws<BusinessRuleException>(() => service.UpdateCombinedAssessment(assessment));

            AssertErrorMessageContains(e.Message, "low");
            AssertErrorMessageContains(e.Message, "is invalid");
        }

        [Test]
        public void CombinedAssessmentCannotBeUpdatedIfHighPriceIsGreaterThanProductMax()
        {
            // Arrange
            var today = new Assessment
            {
                //BusinessDate = CreateDtoBusinessDate(),
                BusinessDate = DateTimeProvider.UtcNow.Date,
                DateCreated = DateTimeProvider.UtcNow,
                LastUpdated = DateTimeProvider.UtcNow,
                ProductId = FakeProductId,
                EnteredByUserId = 999,
                LastUpdatedUserId = 999,
                PriceLow = 500m,
                PriceHigh = 6000m,
                AssessmentStatus = AssessmentStatus.None
            };
            var previous = _fakeAssessments["Previous"].ToEntity();
            var assessment = new CombinedAssessment
            {
                Previous = previous,
                Today = today,
                ProductId = FakeProductId
            };

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null),DateTimeProvider);

            // Act and assert
            var e = Assert.Throws<BusinessRuleException>(() => service.UpdateCombinedAssessment(assessment));

            AssertErrorMessageContains(e.Message, "high");
            AssertErrorMessageContains(e.Message, "is invalid");
        }

        [Test]
        public void ClearAssessmentsForTodayShouldClearPriceHighAndPriceLowForAllOccurrencesOfProductForASingleDay()
        {
            var today = DateTimeProvider.UtcNow.Date;
            var assessment1 = MakeAssessmentDto(1001, today, 1, 900.50m, 500.00m);
            var assessment2 = MakeAssessmentDto(1002, today, 1, 777.00m, 877.5m);
            var assessment3 = MakeAssessmentDto(1003, today, 1, 778.00m, 877.5m);

            _mockModel.Assessments.Add(assessment1);
            _mockModel.Assessments.Add(assessment2);
            _mockModel.Assessments.Add(assessment3);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());

            var combinedAssessment = service.ClearAssessment(1, "Today", 1);

            foreach (var assessment in _mockModel.Assessments)
            {
                Assert.That(assessment.PriceHigh, Is.Null);
                Assert.That(assessment.PriceLow, Is.Null);
            }
        }

        public void ClearASsessmentForTodayShouldReturnACombinedViewWithTodayHavingPriceHighAndLowSetToNull()
        {
            var today = DateTimeProvider.UtcNow.Date;
            var assessment1 = MakeAssessmentDto(1001, today, 1, 900.50m, 500.00m);
            var assessment2 = MakeAssessmentDto(1002, today, 1, 777.00m, 877.5m);
            var assessment3 = MakeAssessmentDto(1003, today, 1, 778.00m, 877.5m);

            _mockModel.Assessments.Add(assessment1);
            _mockModel.Assessments.Add(assessment2);
            _mockModel.Assessments.Add(assessment3);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());

            var combinedAssessment = service.ClearAssessment(1, "Today", 1);

            Assert.That(combinedAssessment, Is.Not.Null);
            Assert.That(combinedAssessment.Previous, Is.Null);
            Assert.That(combinedAssessment.Today, Is.Not.Null);
            Assert.That(combinedAssessment.Today.PriceHigh, Is.Null);
            Assert.That(combinedAssessment.Today.PriceLow, Is.Null);
            Assert.That(combinedAssessment.Today.LastUpdatedUserId, Is.EqualTo(1));
        }

        [Test]
        public void ClearAsessmentForPreviousShouldLeaveTodaysPriceDataInTact()
        {
            var today = DateTimeProvider.UtcNow.Date;
            var previous = DateTimeProvider.UtcNow.AddDays(-1).Date;

            var assessment1 = MakeAssessmentDto(1001, today, 1, 100M, 110M);
            var assessment2 = MakeAssessmentDto(1002, previous, 1, 444M, 478M);
            _mockModel.Assessments.Add(assessment1);
            _mockModel.Assessments.Add(assessment2);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());

            var combinedAssessment = service.ClearAssessment(1, "Previous", 1);

            Assert.That(combinedAssessment, Is.Not.Null);
            Assert.That(combinedAssessment.Today, Is.Not.Null);
            Assert.That(combinedAssessment.Today.PriceHigh, Is.EqualTo(110M));
            Assert.That(combinedAssessment.Today.PriceLow, Is.EqualTo(100M));
        }

        [Test]
        public void ClearAsessmentForPreviousShouldNullThePreviousPriceData()
        {
            var today = DateTimeProvider.UtcNow.Date;
            var previous = DateTimeProvider.UtcNow.AddDays(-1).Date;

            var assessment1 = MakeAssessmentDto(1001, today, 1, 100M, 110M);
            var assessment2 = MakeAssessmentDto(1002, previous, 1, 444M, 478M);
            _mockModel.Assessments.Add(assessment1);
            _mockModel.Assessments.Add(assessment2);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());

            var combinedAssessment = service.ClearAssessment(1, "Previous", 1);

            Assert.That(combinedAssessment, Is.Not.Null);
            Assert.That(combinedAssessment.Previous, Is.Not.Null);
            Assert.That(combinedAssessment.Previous.PriceHigh, Is.Null);
            Assert.That(combinedAssessment.Previous.PriceLow, Is.Null);
        }

        [Test]
        public void ClearAsessmentForTodayShouldNullTodaysPriceData()
        {
            var today = DateTimeProvider.UtcNow.Date;
            var previous = DateTimeProvider.UtcNow.AddDays(-1).Date;

            var assessment1 = MakeAssessmentDto(1001, today, 1, 100M, 110M);
            var assessment2 = MakeAssessmentDto(1002, previous, 1, 444M, 478M);
            _mockModel.Assessments.Add(assessment1);
            _mockModel.Assessments.Add(assessment2);

            var service = new Services.AssessmentService.AssessmentService(new MockDbContextFactory(_mockModel, null), new DateTimeProvider());

            var combinedAssessment = service.ClearAssessment(1, "Today", 1);

            Assert.That(combinedAssessment, Is.Not.Null);
            Assert.That(combinedAssessment.Today, Is.Not.Null);
            Assert.That(combinedAssessment.Today.PriceHigh, Is.Null);
            Assert.That(combinedAssessment.Today.PriceLow, Is.Null);
        }
    }
}