﻿using System.Linq;

namespace AOM.Services.Tests
{
    using AOM.App.Domain.Dates;
    using AOM.Repository.MySql.Tests;
    using AOM.Repository.MySql.Tests.Aom;
    using ErrorService;
    using NUnit.Framework;
    using System;

    [TestFixture]
    internal class ErrorServiceTests
    {
        private IErrorService _errorService;

        private MockDbContextFactory _mockDbContextFactory;

        private MockAomModel _mockAomModel;

        [SetUp]
        public void Setup()
        {
            _mockAomModel = new MockAomModel();
            var dateTimeProvider = new DateTimeProvider();
            _mockDbContextFactory = new MockDbContextFactory(_mockAomModel, null);

            _errorService = new ErrorService(_mockDbContextFactory, dateTimeProvider);
        }

        [Test]
        public void ShouldReturnAnErrorMessageContainingTheErrorIdWhenLoggingAnErrorMessage()
        {
            var errorMessage = _errorService.LogMessage("ERROR TEXT", "SOURCE OF ERROR");
            Assert.That(errorMessage,
                Is.EqualTo(
                    "An internal system error has occurred.  Please contact Aom Support on aomsupport@argusmedia.com or call +44(0)2071999430.  The error reference code is #0."));
        }

        [Test]
        public void ShouldReturnAnEmptyErrorMessageWhenLoggingANullException()
        {
            var additionalInfo = "ADDITIONAL INFO";

            var errorMessage = _errorService.LogException(null, additionalInfo);
            Assert.That(errorMessage,
                Is.EqualTo(string.Empty));
        }

        [Test]
        public void ShouldAddAnErrorIntoTheErrorLogTableWhenLoggingAnErrorMessage()
        {
            _errorService.LogMessage("ERROR TEXT", "SOURCE OF ERROR");

            var errors = _mockAomModel.Errors.ToList();
            Assert.That(errors.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldAddAnErrorIntoTheErrorLogTableWhenLoggingAnException()
        {
            var ex = GenerateException();
            _errorService.LogException(ex, "ADDITIONAL INFO");

            var errors = _mockAomModel.Errors.ToList();
            Assert.That(errors.Count, Is.EqualTo(1));
        }

        [Test]
        public void LoggingAnErrorMessageShouldPopulateTheMessageAndSourceWithBlankStackTrace()
        {
            _errorService.LogMessage("ERROR TEXT", "SOURCE");

            var errors = _mockAomModel.Errors.ToList();
            Assert.That(errors[0].Message, Is.EqualTo("ERROR TEXT"));
            Assert.That(errors[0].Source, Is.EqualTo("SOURCE"));
            Assert.That(errors[0].StackTrace, Is.EqualTo(string.Empty));
        }

        [Test]
        public void LoggingAnExceptionShouldPopulateTheMessageAndSourceWithBlankStackTrace()
        {
            var ex = GenerateException();
            var additionalInfo = "ADDITIONAL INFO";
            _errorService.LogException(ex, additionalInfo);

            var errors = _mockAomModel.Errors.ToList();
            Assert.That(errors[0].Message, Is.EqualTo(ex.Message));
            Assert.That(errors[0].Source, Is.EqualTo(ex.Source));
            Assert.That(errors[0].StackTrace, Is.EqualTo(ex.StackTrace));
            Assert.That(errors[0].AdditionalInfo, Is.EqualTo(additionalInfo));
        }

        [Test] public void LoggingAnExceptionShouldPopulateAdditionalnfoTruncatedTo200Characters()
        {
            var ex = GenerateException();
            var additionalInfo = new string('A', 300);
            var expectedAdditionalInfo = new string('A', 200);
            _errorService.LogException(ex, additionalInfo);

            var errors = _mockAomModel.Errors.ToList();
            Assert.That(errors[0].AdditionalInfo, Is.EqualTo(expectedAdditionalInfo));
        }

        [Test]
        public void LoggingAnErrorMessageShouldPopulateMessageTruncatedTo500Characters()
        {
            var ex = GenerateException();
            var message = new string('M', 600);
            var expectedMessage = new string('M', 500);
            _errorService.LogMessage(message, string.Empty);

            var errors = _mockAomModel.Errors.ToList();
            Assert.That(errors[0].Message, Is.EqualTo(expectedMessage));
        }

        private Exception GenerateException()
        {
            Exception ex = null;
            var divisor = 0;
            try
            {
                var num = 10 / divisor;
            }
            catch (Exception e)
            {
                ex = e;
            }

            return ex;
        }
    }
}