﻿//using System;
//using System.Collections.Generic;
//using System.Linq;

//using AOM.App.Domain;
//using AOM.App.Domain.Dates;
//using AOM.App.Domain.Entities;
//using AOM.App.Domain.Mappers;
//using AOM.App.Domain.Services;
//using AOM.Repository.MySql;
//using AOM.Repository.MySql.Aom;
//using AOM.Repository.MySql.Tests;
//using AOM.Repository.MySql.Tests.Aom;
//using AOM.Services.OrderService;
//using AOM.Services.ProductService;
//using AOM.Transport.Events;
//using AOM.Transport.Events.Orders;
//using AOM.Services.MetadataValidator;
//using AOM.Services.ProductMetadataService;

//using Moq;
//using NUnit.Framework;

//namespace AOM.Services.Tests.OrderService
//{
//    [TestFixture]
//    public class EditOrderTests
//    {
//        private readonly DateTime _testStartDate = DateTime.Today.AddDays(1);
//        private readonly DateTime _testEndDate = DateTime.Today.AddDays(4);
//        private readonly string _fakeProductTenorName = "Product1_Tenor1";
//        private readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();

//        private const int TestUserId = 666;
//        private const int TestOrgId = TestUserId;

//        private Mock<IUserService> _mockUserService;
//        private Mock<IOrganisationService> _mockOrganisationService;
//        private Mock<IProductService> _mockProductService;

//        private IAomModel _mockAomModel;
//        private IOrderService _orderService;

//        public void AssertErrorMessageContains(string errorMsg, string expectedContains)
//        {
//            Assert.IsTrue(errorMsg.Contains(expectedContains), String.Format("Expect error message to contain '{0}' but it was: '{1}'", expectedContains, errorMsg));
//        }

//        [SetUp]
//        public void Setup()
//        {
//            _mockProductService = new Mock<IProductService>();
//            _mockOrganisationService = new Mock<IOrganisationService>();
//            _mockUserService = new Mock<IUserService>();
//            _mockUserService.Setup(x => x.GetUsersOrganisation(TestUserId)).Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});
//            _mockOrganisationService.Setup(x => x.GetOrganisationById(TestOrgId)).Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});

//            _mockAomModel = new MockAomModel();
//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddOrderStatus("A", "Active")
//                .AddOrderStatus("E", "Executed")
//                .AddCurrency("EUR", "€")
//                .AddCurrency("USD", "$")
//                .AddUnit("BBL", "bbl")
//                .AddDeliveryLocation("DeliveryLocation1")
//                .AddProduct("Product1")
//                .AddTenorCode("Tenor1")
//                .AddProductTenor(_fakeProductTenorName)
//                .DBContext.SaveChangesAndLog(-1);

//            _orderService = new Services.OrderService.OrderService(_mockOrganisationService.Object,
//                null, _mockUserService.Object, _dateTimeProvider, _mockProductService.Object,
//                new Services.MetadataValidator.MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));
//        }

//        private Order PopulateOrder(long? orderId = null)
//        {
//            //using (_mockAomModel)
//            //{
//                if (orderId.HasValue) return _orderService.GetOrderById(_mockAomModel, orderId.Value);
//                OrderDto testOrderDto;
//                PriceBasisDto priceBasisDto;
//                PriceLineDto priceLineDto;
//                PriceLineDto priceLineDto2;
                   
//                var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
//                var price = 800;

//                AomDataBuilder.WithModel(_mockAomModel)
//                    .OpenAllMarkets()
//                    .AddTenorCode("P")
//                    .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
//                    .AddPriceType("FIXED")
//                    .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
//                    .AddPriceLine(out priceLineDto, TestUserId)
//                    .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
//                    .AddPriceLine(out priceLineDto2, TestUserId)
//                    .AddPriceLineBasis(price, priceLineDto2, 1, 1, "P", priceBasisDto.Id)
//                    .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "A", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto, priceLineDto2 })
//                    .DBContext.SaveChangesAndLog(-1);

//                // Set up changes                
//                var testOrder1 = new AomOrderAuthenticationResponse
//                {
//                    MarketMustBeOpen = false,
//                    Message = new Message
//                    {
//                        MessageAction = MessageAction.Update,
//                        ClientSessionInfo = new ClientSessionInfo {UserId = 1}
//                    },
//                    Order = new Order
//                    {
//                        Quantity = 20000,
//                        Price = 123M,
//                        Id = testOrderDto.Id,
//                        ExecutionMode = ExecutionMode.Internal,
//                        OrderStatus = OrderStatus.Active,
//                        OrderType = OrderType.Ask,
//                        DeliveryStartDate = _testStartDate,
//                        DeliveryEndDate = _testEndDate,
//                        LastUpdated = testOrderDto.LastUpdated,
//                        PrincipalUserId = TestUserId,
//                        BrokerRestriction = BrokerRestriction.Any,
//                        Notes = "CN:备注, © ®, KO:참고",
//                        OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                    }
//                };
//                testOrder1.Order.ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id};

//                //ACT
//                return _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open);
//            //}
//        }

//        [Test]
//        public void CanEditOrderAgainstMockDatabase()
//        {
//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;
//            PriceBasisDto priceBasisDto;

//            var price = 800;
            
//            AomDataBuilder.WithModel(_mockAomModel)
//                .OpenAllMarkets()
//                .AddTenorCode("P")
//                .AddProductTenor("Product1_Tenor1", productId: _mockAomModel.Products.Min(p => p.Id))
//                .AddPriceType("FIXED")
//                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
//                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);

//            var mockDb = new MockDbContextFactory(_mockAomModel, null);

//            // Set up changes                
//            var testOrder1 = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = false,
//                Message = new Message
//                {
//                    MessageAction = MessageAction.Update,
//                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                },
//                Order = new Order
//                {
//                    Quantity = 200,
//                    Price = 123M,
//                    Id = testOrderDto.Id,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Bid,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testEndDate,
//                    PrincipalUserId = TestUserId,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    LastUpdatedUserId = TestUserId,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            //ACT
//            Order returnedOrder = _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open);

//            // ASSERT       
//            Assert.AreEqual(200, returnedOrder.Quantity);
//            Assert.AreEqual(123M, returnedOrder.Price);
//            Assert.AreEqual(OrderType.Bid, returnedOrder.OrderType);
//            Assert.AreEqual(OrderStatus.Active, returnedOrder.OrderStatus);
//            Assert.AreEqual(_testStartDate, returnedOrder.DeliveryStartDate);
//            Assert.AreEqual(_testEndDate, returnedOrder.DeliveryEndDate);
//        }

//        [Test]
//        public void CanEditOrderRunningAgainstDatabase()
//        {
//            using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
//            {
//                Order memOrder = PopulateOrder();
//                Order dbOrder = PopulateOrder(memOrder.Id);
                
//                // ASSERT
//                Assert.NotNull(memOrder);
//                Assert.NotNull(dbOrder);
//                Assert.AreEqual(memOrder.Quantity, dbOrder.Quantity);
//                Assert.AreEqual(memOrder.Price, dbOrder.Price);
//                Assert.AreEqual(memOrder.OrderType, dbOrder.OrderType);
//                Assert.AreEqual(memOrder.OrderStatus, dbOrder.OrderStatus);
//                Assert.AreEqual(memOrder.DeliveryStartDate, dbOrder.DeliveryStartDate);
//                Assert.AreEqual(memOrder.DeliveryEndDate, dbOrder.DeliveryEndDate);
//                Assert.AreEqual(memOrder.Notes, dbOrder.Notes);
//            }
//        }

//        [Test]
//        public void CanAddPriceLineInDatabase()
//        {
//            using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
//            {
//                using (var aomModel = new AomModel())
//                {
//                    var entityHelper = new EntityHelper(_mockUserService.Object, _mockOrganisationService.Object, _mockAomModel, _testStartDate, _testEndDate, _fakeProductTenorName, TestUserId);

//                    Order memOrder = PopulateOrder();
//                    var currentOrderPriceLineCount = memOrder.OrderPriceLines.Count;

//                    var orderRequest = new AomOrderAuthenticationResponse
//                    {
//                        Order = memOrder,
//                        MarketMustBeOpen = false,
//                        Message = new Message
//                        {
//                            MessageAction = MessageAction.Create,
//                            MessageType = MessageType.Order,
//                            ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                        }
//                    };

//                    // APPLY CHANGES
//                    var existingPriceLineBase = memOrder.OrderPriceLines.First().PriceLine.PriceLinesBases.First();
//                    var priceLineBaseEntity = entityHelper.CreatePriceLineBasis(0, 50, orderRequest.Order.Price, 1, existingPriceLineBase.ProductId, existingPriceLineBase.TenorCode);
//                    priceLineBaseEntity.PriceBasis = _mockAomModel.PriceBases.First().ToEntity();
//                    var priceLineEntity = entityHelper.CreatePriceLineEntity(0);
//                    priceLineEntity.PriceLinesBases = new List<PriceLineBasis>() { priceLineBaseEntity };
//                    var orderPriceLineEntity = entityHelper.CreateOrderPriceLineEntity(memOrder.Id, DtoMappingAttribute.GetValueFromEnum(memOrder.OrderStatus), priceLineEntity.Id);
//                    orderPriceLineEntity.PriceLine = priceLineEntity;
//                    memOrder.OrderPriceLines.Add(orderPriceLineEntity);
//                    var updatedOrder = _orderService.EditOrder(_mockAomModel, orderRequest, MarketStatus.Open);

//                    var insertedOrderPriceLine = updatedOrder.OrderPriceLines.OrderBy(opl => opl.Id).Last();

//                    // ASSERT
//                    Assert.NotNull(updatedOrder);
//                    Assert.AreNotEqual(orderPriceLineEntity.Id, insertedOrderPriceLine);
//                    Assert.AreEqual(currentOrderPriceLineCount + 1, updatedOrder.OrderPriceLines.Count);
//                    Assert.NotNull(insertedOrderPriceLine.PriceLine);
//                    Assert.AreNotEqual(insertedOrderPriceLine.PriceLine.Id, priceLineEntity.Id);
//                    Assert.NotNull(insertedOrderPriceLine.PriceLine.PriceLinesBases);
//                    Assert.AreNotEqual(insertedOrderPriceLine.PriceLine.PriceLinesBases.First().Id, priceLineBaseEntity.Id);
//                }
//            }
//        }

//        //[Test]
//        //public void OrderWithoutOrderPriceLinesIsRejected()
//        //{
//        //    //TODO: Implement when front-end will send all entities populated
//        //}

//        [Test]
//        public void OrderPriceLineWithoutPriceLinesIsRejected()
//        {
//            var entityHelper = new EntityHelper(_mockUserService.Object, _mockOrganisationService.Object, _mockAomModel, _testStartDate, _testEndDate, _fakeProductTenorName, TestUserId);
//            var orderEntity = entityHelper.PopulateOrderInMockDb().ToEntity(_mockOrganisationService.Object, _mockUserService.Object);

//            var newOrderRequest = new AomOrderAuthenticationResponse
//            {
//                Order = orderEntity,
//                MarketMustBeOpen = false,
//                Message = new Message
//                {
//                    MessageAction = MessageAction.Create,
//                    MessageType = MessageType.Order,
//                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                }
//            };

//            var currentFailMessage = "Test: First Null PriceLine test.";
//            orderEntity.OrderPriceLines[0].PriceLine = null;
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(
//                () => _orderService.EditOrder(_mockAomModel, newOrderRequest, MarketStatus.Open), "must have associated PriceLines populated", currentFailMessage);

//            currentFailMessage = "Test: Second Null PriceLine test.";
//            var priceLineEntity = entityHelper.CreatePriceLineEntity(0);
//            orderEntity.OrderPriceLines[0].PriceLine = priceLineEntity; // as price line basis check is done after price line check, no need to set price line bases
//            orderEntity.OrderPriceLines[1].PriceLine = null;
//            //var orderPriceLineEntity2 = entityHelper.CreateOrderPriceLineEntity(orderEntity.Id, DtoMappingAttribute.GetValueFromEnum(orderEntity.OrderStatus), -2);
//            //orderEntity.OrderPriceLines.Add(orderPriceLineEntity2);
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.EditOrder(_mockAomModel, newOrderRequest, MarketStatus.Open),
//                "must have associated PriceLines populated", currentFailMessage);
//        }

//        [Test]
//        public void PriceLineWithoutPriceLineBasesIsRejected()
//        {
//            var entityHelper = new EntityHelper(_mockUserService.Object, _mockOrganisationService.Object, _mockAomModel, _testStartDate, _testEndDate, _fakeProductTenorName, TestUserId);
//            var orderEntity = entityHelper.PopulateOrderInMockDb().ToEntity(_mockOrganisationService.Object, _mockUserService.Object);

//            var newOrderRequest = new AomOrderAuthenticationResponse
//            {
//                Order = orderEntity,
//                MarketMustBeOpen = false,
//                Message = new Message
//                {
//                    MessageAction = MessageAction.Create,
//                    MessageType = MessageType.Order,
//                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                }
//            };

//            var errorMessageExpectedContain = "must have associated PriceLinesBases populated";

//            var currentFailMessage = "Test: Null PriceLinesBases test.";
//            orderEntity.OrderPriceLines[0].PriceLine.PriceLinesBases = null;
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open), 
//                errorMessageExpectedContain, currentFailMessage);

//            currentFailMessage = "Test: Empty PriceLinesBases test.";
//            newOrderRequest.Order.OrderPriceLines[0].PriceLine.PriceLinesBases = new List<PriceLineBasis>() { };
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open), 
//                errorMessageExpectedContain, currentFailMessage);

//            currentFailMessage = "Test: Second PriceLine with null PriceLinesBases test.";
//            var existingPriceLineBase = orderEntity.OrderPriceLines[1].PriceLine.PriceLinesBases[0];
//            var priceLineBaseEntity = entityHelper.CreatePriceLineBasis(0, 50, newOrderRequest.Order.Price, 1, existingPriceLineBase.ProductId, existingPriceLineBase.TenorCode);
//            newOrderRequest.Order.OrderPriceLines[0].PriceLine.PriceLinesBases = new List<PriceLineBasis>() { priceLineBaseEntity };
//            orderEntity.OrderPriceLines[1].PriceLine.PriceLinesBases = null;
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open), 
//                errorMessageExpectedContain, currentFailMessage);

//            currentFailMessage = "Test: Second PriceLine with empty PriceLinesBases test.";
//            newOrderRequest.Order.OrderPriceLines[1].PriceLine.PriceLinesBases = new List<PriceLineBasis>() { };
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open), 
//                errorMessageExpectedContain, currentFailMessage);
//        }

//        [Test]
//        public void RemovingPriceLineBasisIsRejected()
//        {
//            var entityHelper = new EntityHelper(_mockUserService.Object, _mockOrganisationService.Object, _mockAomModel, _testStartDate, _testEndDate, _fakeProductTenorName, TestUserId);
//            var orderEntity = entityHelper.PopulateOrderInMockDb().ToEntity(_mockOrganisationService.Object, _mockUserService.Object);

//            var orderRequest = new AomOrderAuthenticationResponse
//            {
//                Order = orderEntity,
//                MarketMustBeOpen = false,
//                Message = new Message
//                {
//                    MessageAction = MessageAction.Create,
//                    MessageType = MessageType.Order,
//                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                }
//            };

//            // APPLY CHANGES
//            var existingPriceLineBase = orderEntity.OrderPriceLines[1].PriceLine.PriceLinesBases[0];
//            var pricelineBasis = entityHelper.CreatePriceLineBasis(1, 50, orderEntity.Price, 1, existingPriceLineBase.ProductId, existingPriceLineBase.TenorCode);
//            orderEntity.OrderPriceLines[0].PriceLine.PriceLinesBases.Add(pricelineBasis);
//            orderEntity.OrderPriceLines[0].PriceLine.PriceLinesBases.Remove(orderEntity.OrderPriceLines[0].PriceLine.PriceLinesBases.First());

//            // ASSERT
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.EditOrder(_mockAomModel, orderRequest, MarketStatus.Open), "PriceLineBases cannot be deleted");
//        }

//        [Test]
//        public void CanKillWithInvalidDeliveryPeriod()
//        {
//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;

//            //ARRANGE
//            var price = 800;
//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1)
//                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);

//            // Set up changes                
//            var testOrder1 = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = false,
//                Message = new Message
//                {
//                    MessageAction = MessageAction.Kill,
//                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                },
//                Order = new Order
//                {
//                    Quantity = -200,
//                    Price = 123M,
//                    Id = testOrderDto.Id,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Bid,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testStartDate.AddDays(-1),
//                    PrincipalUserId = TestUserId,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            Order returnedOrder = _orderService.KillOrder(_mockAomModel, testOrder1, MarketStatus.Open);
//            Assert.AreEqual(returnedOrder.OrderStatus, OrderStatus.Killed);
//        }

//        [Test]
//        public void CantChangeAKilledOrder()
//        {
//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;

//            var price = 800;

//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1)
//                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, orderStatus:"K", price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);
            
//            // Set up changes                
//            var testOrder1 = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = false,
//                Message = new Message { MessageAction = MessageAction.Update },
//                Order = new Order
//                {
//                    Quantity = 200,
//                    Price = 123M,
//                    Id = testOrderDto.Id,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Bid,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testStartDate,
//                    PrincipalUserId = TestUserId,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            var ex = Assert.Throws<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open));
//            Assert.That(ex.Message.Contains("order is killed"));
//        }

//        [Test]
//        public void CantChangeBidToAsk()
//        {
//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;

//            var price = 800;

//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1)
//                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);

//            // Set up changes                
//            var testOrder1 = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = false,
//                Message = new Message { MessageAction = MessageAction.Update },
//                Order = new Order
//                {
//                    Quantity = 200,
//                    Price = 123M,
//                    Id = testOrderDto.Id,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Ask,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testStartDate,
//                    PrincipalUserId = TestUserId,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            var ex = Assert.Throws<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open));
//            Assert.That(ex.Message.Contains("from a Bid to an Ask"));
//        }

//        //TODO
//        //[Test]
//        //public void CantChangeToInvalidDeliveryPeriod()
//        //{
//        //    OrderDto testOrderDto;
//        //    PriceLineDto priceLineDto;

//        //    var price = 800;

//        //    AomDataBuilder.WithModel(_mockAomModel)
//        //        .AddPriceLine(out priceLineDto, TestUserId)
//        //        .AddPriceLineBasis(price, priceLineDto, 1, 1)
//        //        .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//        //        .DBContext.SaveChangesAndLog(-1);

//        //    // Set up changes                
//        //    var testOrder1 = new AomOrderAuthenticationResponse
//        //    {
//        //        MarketMustBeOpen = false,
//        //        Message = new Message { MessageAction = MessageAction.Update },
//        //        Order = new Order
//        //        {
//        //            Quantity = 200,
//        //            Price = 123M,
//        //            Id = testOrderDto.Id,
//        //            ExecutionMode = ExecutionMode.Internal,
//        //            OrderStatus = OrderStatus.Active,
//        //            OrderType = OrderType.Bid,
//        //            DeliveryStartDate = _testStartDate,
//        //            DeliveryEndDate = _testStartDate.AddDays(-1),
//        //            PrincipalUserId = TestUserId,
//        //            LastUpdated = testOrderDto.LastUpdated,
//        //            BrokerRestriction = BrokerRestriction.Any,
//        //            ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//        //            OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//        //        }
//        //    };

//        //    var ex = Assert.Throws<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open));
//        //    Assert.That(ex.Message.Contains("delivery period is invalid"));
//        //}

//        [Test]
//        public void CantChangeToInvalidPrice()
//        {
//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;
//            PriceBasisDto priceBasisDto;

//            var price = 800;

//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddPriceType("FIXED")
//                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1)
//                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);

//            // Set up changes                
//            var testOrder1 = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = false,
//                Message = new Message { MessageAction = MessageAction.Update },
//                Order = new Order
//                {
//                    Quantity = 200,
//                    Price = -123M,
//                    Id = testOrderDto.Id,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Bid,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testStartDate,
//                    PrincipalUserId = TestUserId,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            var ex = Assert.Throws<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open));
//            Assert.That(ex.Message.Contains("price is invalid"));
//        }

//        [Test]
//        public void CantChangeToInvalidProduct()
//        {
//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;

//            var price = 800;

//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1)
//                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);

//            // Set up changes                
//            var testOrder1 = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = false,
//                Message = new Message { MessageAction = MessageAction.Update },
//                Order = new Order
//                {
//                    Quantity = 200,
//                    Price = 123M,
//                    Id = testOrderDto.Id,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Bid,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testStartDate,
//                    PrincipalUserId = TestUserId,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id + 99 },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            var ex = Assert.Throws<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open));
//            Assert.That(ex.Message.Contains("invalid Product Tenor"));
//        }

//        [Test]
//        public void CantChangeToInvalidQuantity()
//        {
//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;

//            var price = 800;

//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1)
//                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);

//            // Set up changes                
//            var testOrder1 = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = false,
//                Message = new Message { MessageAction = MessageAction.Update },
//                Order = new Order
//                {
//                    Quantity = -200,
//                    Price = 123M,
//                    Id = testOrderDto.Id,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Bid,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testStartDate,
//                    PrincipalUserId = TestUserId,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            var ex = Assert.Throws<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open));
//            Assert.That(ex.Message.Contains("quantity is invalid"));
//        }

//        [Test]
//        public void CantEditLastUpdateChanged()
//        {
//            //ARRANGE
//            using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
//            {
//                // We have to use two models to make this test worthwhile, otherwise the order will be cached and will still include all 
//                // referenced DTOs making any ".Include()" irrelevant.
//                var model1 = new AomModel();
//                var model2 = new AomModel();

//                OrderDto testOrderDto;
//                PriceBasisDto priceBasisDto;
//                PriceLineDto priceLineDto;

//                var lowestProductId = model1.Products.Min(p => p.Id);
//                var price = 800;

//                AomDataBuilder.WithModel(model1)
//                    .OpenAllMarkets()
//                    .AddTenorCode("P")
//                    .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
//                    .AddPriceType("FIXED")
//                    .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
//                    .AddPriceLine(out priceLineDto, TestUserId)
//                    .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
//                    .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                    .DBContext.SaveChangesAndLog(-1);

//                // Set up changes                
//                var testOrder1 = new AomOrderAuthenticationResponse
//                {
//                    MarketMustBeOpen = false,
//                    Message = new Message { MessageAction = MessageAction.Update },
//                    Order = new Order
//                    {
//                        Quantity = 200,
//                        Price = 123M,
//                        Id = testOrderDto.Id,
//                        ExecutionMode = ExecutionMode.Internal,
//                        OrderStatus = OrderStatus.Active,
//                        OrderType = OrderType.Bid,
//                        DeliveryStartDate = _testStartDate,
//                        DeliveryEndDate = _testStartDate,
//                        PrincipalUserId = TestUserId,
//                        LastUpdated = _dateTimeProvider.UtcNow,
//                        BrokerRestriction = BrokerRestriction.Any,
//                        ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                        OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                    }
//                };

//                var ex = Assert.Throws<BusinessRuleException>(() => _orderService.EditOrder(model2, testOrder1, MarketStatus.Open));
//                Assert.That(ex.Message.Contains("could not be amended as TestingOrg updated this order"));
//            }
//        }

//        [Test]
//        public void CantEditNonExistentOrder()
//        {
//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;

//            var price = 800;

//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1)
//                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);

//            // Set up changes                
//            var testOrder1 = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = false,
//                Message = new Message { MessageAction = MessageAction.Update },
//                Order = new Order
//                {
//                    Quantity = 200,
//                    Price = 123M,
//                    Id = -2222222222,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Bid,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testStartDate,
//                    PrincipalUserId = TestUserId,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            var ex = Assert.Throws<BusinessRuleException>( () => _orderService.EditOrder(_mockAomModel, testOrder1, MarketStatus.Open));
//            Assert.That(ex.Message.Contains("does not exist"));
//        }

//        [Test]
//        public void HoldReinstateHoldReinstate()
//        {
//            using (var aomModel = new AomModel())
//            {
//                using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
//                {
//                    OrderDto testOrderDto;
//                    PriceBasisDto priceBasisDto;
//                    PriceLineDto priceLineDto;

//                    var lowestProductId = aomModel.Products.Min(p => p.Id);
//                    var price = 800;

//                    AomDataBuilder.WithModel(aomModel)
//                        .OpenAllMarkets()
//                        .AddTenorCode("P")
//                        .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
//                        .AddPriceType("FIXED")
//                        .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
//                        .AddPriceLine(out priceLineDto, TestUserId)
//                        .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
//                        .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                        .DBContext.SaveChangesAndLog(-1);

//                    // Set up changes                
//                    var testOrder1 = new AomOrderAuthenticationResponse
//                    {
//                        MarketMustBeOpen = false,
//                        Message = new Message
//                        {
//                            MessageAction = MessageAction.Update,
//                            ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                        },
//                        Order = new Order
//                        {
//                            Quantity = 20000,
//                            Price = 123M,
//                            Id = testOrderDto.Id,
//                            ExecutionMode = ExecutionMode.Internal,
//                            OrderStatus = OrderStatus.Active,
//                            OrderType = OrderType.Bid,
//                            DeliveryStartDate = _testStartDate,
//                            DeliveryEndDate = _testEndDate,
//                            PrincipalUserId = TestUserId,
//                            BrokerRestriction = BrokerRestriction.Any,
//                            LastUpdated = testOrderDto.LastUpdated,
//                            ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                            OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                        }
//                    };

//                    //ACT
//                    Order heldOrder = _orderService.HoldOrder(aomModel, testOrder1, MarketStatus.Open);
//                    Assert.AreEqual(heldOrder.Id, testOrder1.Order.Id);
//                    testOrder1.Order = heldOrder;
//                    Order reinstated = _orderService.ReinstateOrder(aomModel, testOrder1, MarketStatus.Open);
//                    Assert.AreEqual(reinstated.Id, testOrder1.Order.Id);
//                }
//            }
//        }

//        [Test]
//        public void UpdatingOrderToActiveWhenMarketIsClosedGivesErrorThatProductMarketIsClosed()
//        {
//            const string productName = "Product1";
//            const string tenorName = "Product1_Tenor1";

//            OrderDto testOrderDto;
//            PriceLineDto priceLineDto;

//            var price = 800;

//            AomDataBuilder.WithModel(_mockAomModel)
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1)
//                .AddOrder(userId: TestUserId, productTenorName: tenorName, bidOrAsk: "B", orderStatus: "H", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                .DBContext.SaveChangesAndLog(-1);

//            var orderRequest = new AomOrderAuthenticationResponse
//            {
//                MarketMustBeOpen = true,
//                Message = new Message
//                {
//                    MessageAction = MessageAction.Update,
//                    ClientSessionInfo = new ClientSessionInfo {UserId = 1}
//                },
//                Order = new Order
//                {
//                    Quantity = testOrderDto.Quantity,
//                    Price = testOrderDto.Price,
//                    Id = testOrderDto.Id,
//                    ExecutionMode = ExecutionMode.Internal,
//                    OrderStatus = OrderStatus.Active,
//                    OrderType = OrderType.Bid,
//                    DeliveryStartDate = _testStartDate,
//                    DeliveryEndDate = _testEndDate,
//                    PrincipalUserId = TestUserId,
//                    BrokerRestriction = BrokerRestriction.Any,
//                    LastUpdated = testOrderDto.LastUpdated,
//                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
//                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
//                }
//            };

//            // Act and Assert
//            var e = Assert.Throws<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, orderRequest, MarketStatus.Closed));

//            StringAssert.Contains(productName, e.Message);
//            StringAssert.DoesNotContain(tenorName, e.Message);
//            StringAssert.Contains("market is closed", e.Message);
//        }
//    }
//}