﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.OrderService;
using AOM.Services.ProductService;
using AOM.Services.MetadataValidator;
using AOM.Services.ProductMetadataService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;

using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.OrderService
{
    using MetadataValidator = AOM.Services.MetadataValidator.MetadataValidator ;
    
    [TestFixture]
    public class VoidOrderTests
    {
        private Mock<IOrganisationService> _mockOrganisationService;
        private Mock<IUserService> _mockUserService;
        private IDateTimeProvider _dateTimeProvider;
        private Mock<IProductService> _mockProductService;
        private IAomModel _mockAomModel;
        private const int TestUserId = 666;
        private const int TestOrgId = 123;
        private readonly DateTime _testStartDate = DateTime.Today.AddDays(1);
        private readonly DateTime _testEndDate = DateTime.Today.AddDays(4);
        private readonly string _fakeProductTenorName = "Product1_Tenor1";
        private IOrderService _orderService;

        [SetUp]
        public void Setup()
        {
            _mockProductService = new Mock<IProductService>();
            _mockOrganisationService = new Mock<IOrganisationService>();
            _mockOrganisationService.Setup(x => x.GetOrganisationById(TestOrgId)).Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});
            _mockUserService = new Mock<IUserService>();
            _mockUserService.Setup(x => x.GetUsersOrganisation(TestUserId)).Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});

            _mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(_mockAomModel)
                .AddOrderStatus("A", "Active")
                .AddOrderStatus("H", "Held")
                .AddOrderStatus("V", "VoidAfterExecuted")
                .AddOrderStatus("E", "Executed")
                .AddOrderStatus("K", "Killed")
                .AddCurrency("EUR", "€")
                .AddUnit("BBL", "bbl")
                .AddDeliveryLocation("DeliveryLocation1")
                .AddProduct("Product1")
                .AddTenorCode("Tenor1")
                .AddProductTenor(_fakeProductTenorName)
                .DBContext.SaveChangesAndLog(-1);

            _dateTimeProvider = new DateTimeProvider();

            _orderService = new Services.OrderService.OrderService(_mockOrganisationService.Object, null, _mockUserService.Object, _dateTimeProvider, _mockProductService.Object,
                new MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));
        }

        [Test]
        public void AssertThatOrderStatusesContainsACodeOfLetterV()
        {
            using (var context = new AomModel())
            {
                var orderStatuses = from orderStatus in context.OrderStatus
                                    select orderStatus;

                Assert.IsTrue(orderStatuses.Any(x => x.Code == "V"));
            }
        }

        [Test]
        public void AssertThatOrderStatusesContainsOneCodeWithLetterV()
        {
            using (var context = new AomModel())
            {
                var orderStatuses = from orderStatus in context.OrderStatus
                                    select orderStatus;

                var voidStatus = orderStatuses.SingleOrDefault(x => x.Code == "V");

                Assert.IsNotNull(voidStatus);
                Assert.IsTrue(voidStatus.Code == "V");
            }
        }

        [Test]
        [TestCase(OrderStatus.Active)]
        [TestCase(OrderStatus.Held)]
        [TestCase(OrderStatus.Killed)]
        public void OrderThanHasNotBeenExecutedCannotBeMarkedAsVoid(OrderStatus orderStatus)
        {
            // arrange
            var mockDb = new MockDbContextFactory(_mockAomModel, null);
            IOrderService orderService = new Services.OrderService.OrderService(_mockOrganisationService.Object, null, _mockUserService.Object, _dateTimeProvider, _mockProductService.Object,
                new MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));

            var testOrder = CreateFakeOrder(orderStatus);

            var e = Assert.Throws<BusinessRuleException>(() => orderService.VoidOrder(mockDb.CreateAomModel(), testOrder, MarketStatus.Open));
            Assert.IsTrue(e.Message.Contains("however the order was not Executed"));
            Assert.IsTrue(e.Message.Contains(DtoMappingAttribute.GetValueFromEnum(orderStatus)));
            Assert.IsTrue(e.Message.Contains(testOrder.Order.Id.ToString(CultureInfo.InvariantCulture)));
        }

        [Test]
        public void OrderThatHasBeenExecutedCanBeMarkedAsVoid()
        {
            // arrange
            var mockDb = new MockDbContextFactory(_mockAomModel, null);
            var testOrder = CreateFakeOrder(OrderStatus.Executed);

            // act
            var returnedOrder = _orderService.VoidOrder(mockDb.CreateAomModel(), testOrder, MarketStatus.Open);

            // assert
            Assert.AreEqual(OrderStatus.VoidAfterExecuted, returnedOrder.OrderStatus,"order status should be VoidAfterExecuted");
        }

        [Test]
        public void OrderThatHasBeenExecutedCanBeMarkedAsVoidEvenIfMarketClosed()
        {
            // arrange
            var mockDb = new MockDbContextFactory(_mockAomModel, null);
            var testOrder = CreateFakeOrder(OrderStatus.Executed);

            // act
            var returnedOrder = _orderService.VoidOrder(mockDb.CreateAomModel(), testOrder, MarketStatus.Closed);

            // assert
            Assert.AreEqual(OrderStatus.VoidAfterExecuted, returnedOrder.OrderStatus,"order status should be VoidAfterExecuted"); 
        }

        //[Test]
        //public void OnlyOneOrderPriceLineWithMatchingStatusAfterExecutionIsAllowed()
        //{
        //    var entityHelper = new EntityHelper(_mockUserService.Object, _mockOrganisationService.Object, _mockAomModel, _testStartDate, _testEndDate, _fakeProductTenorName, TestUserId);
        //    var orderEntity = entityHelper.PopulateOrderInMockDb().ToEntity(_mockOrganisationService.Object, _mockUserService.Object);

        //    var orderRequest = new AomOrderAuthenticationResponse
        //    {
        //        Order = orderEntity,
        //        MarketMustBeOpen = false,
        //        Message = new Message
        //        {
        //            MessageAction = MessageAction.Create,
        //            MessageType = MessageType.Order,
        //            ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
        //        }
        //    };

        //    orderRequest.Order.OrderStatus = OrderStatus.VoidAfterExecuted;
        //    orderRequest.Order.OrderPriceLines.ForEach(opl => opl.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.VoidAfterExecuted));

        //    var errorMessageExpectedContain = "only one order price line may have a status matching order status";

        //    var currentTest = "Test: Order executed and all order price line status are matching";
        //    TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(
        //        () => _orderService.EditOrder(_mockAomModel, orderRequest, MarketStatus.Open),
        //        errorMessageExpectedContain,
        //        currentTest
        //    );

        //    currentTest = "Test: Order executed and more than one order price lines status are matching";
        //    // add another order price line
        //    var priceLineEntity = entityHelper.CreatePriceLineEntity(2);
        //    var productId = orderRequest.Order.OrderPriceLines.Select(opl => opl.PriceLine.PriceLinesBases.First().ProductId).First();
        //    var tenorCode = orderRequest.Order.OrderPriceLines.Select(opl => opl.PriceLine.PriceLinesBases.First().TenorCode).First();
        //    var priceLineBasis1 = entityHelper.CreatePriceLineBasis(0, 50, orderRequest.Order.Price, 1, productId, tenorCode);
        //    priceLineEntity.PriceLinesBases = new List<PriceLineBasis>() { priceLineBasis1 };
        //    var priceLineBasis2 = entityHelper.CreatePriceLineBasis(1, 50, orderRequest.Order.Price, 1, productId, tenorCode);
        //    priceLineEntity.PriceLinesBases = new List<PriceLineBasis>() { priceLineBasis2 };
        //    var orderPriceLineEntity = entityHelper.CreateOrderPriceLineEntity(orderRequest.Order.Id, DtoMappingAttribute.GetValueFromEnum(OrderStatus.VoidAfterExecuted), priceLineEntity.Id);
        //    orderPriceLineEntity.PriceLine = priceLineEntity;
        //    orderRequest.Order.OrderPriceLines.Add(orderPriceLineEntity);

        //    TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, orderRequest, MarketStatus.Open), errorMessageExpectedContain, currentTest);
        //}

        private AomOrderAuthenticationResponse CreateFakeOrder(OrderStatus orderStatus)
        {
            var dtoOrderStatus = DtoMappingAttribute.GetValueFromEnum(orderStatus);
            OrderDto testOrderDto;
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, TestUserId)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            return new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Void,
                    ClientSessionInfo = new ClientSessionInfo {UserId = TestUserId}
                },
                Order = new Order
                {
                    Quantity = 200,
                    Price = 123M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = orderStatus,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testStartDate,
                    DeliveryEndDate = _testEndDate,
                    PrincipalUserId = TestUserId,
                    LastUpdated = testOrderDto.LastUpdated,
                    BrokerRestriction = BrokerRestriction.Any,
                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
                }
            };
        }

    }
}