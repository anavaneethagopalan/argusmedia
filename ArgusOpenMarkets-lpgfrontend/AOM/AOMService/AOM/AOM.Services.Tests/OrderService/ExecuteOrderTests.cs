﻿using System.Linq;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.MetadataValidator;
using AOM.Services.ProductMetadataService;
using AOM.Services.ProductService;

namespace AOM.Services.Tests.OrderService
{
    using System;

    using App.Domain;
    using App.Domain.Entities;
    using App.Domain.Services;
    using Repository.MySql.Aom;
    using Repository.MySql.Tests;
    using Repository.MySql.Tests.Aom;
    using Services.OrderService;
    using Transport.Events;
    using Transport.Events.Orders;

    using Moq;

    using NUnit.Framework;
    using MetadataValidator = AOM.Services.MetadataValidator.MetadataValidator;
    using System.Collections.Generic;

    [TestFixture]
    public class OrderServiceExecuteOrderTests
    {
        private UserInfoDto _user1InOrg1, _user1InOrg2, _user1InBroker1, _user1InBroker2;
        private OrganisationDto _tradingOrg1, _tradingOrg2, _brokerOrg1, _brokerOrg2;
        readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        private readonly DateTime _testDate = DateTime.Today;
        private readonly string _fakeProductTenorName = "Product1_Tenor1";

        private Mock<IUserService> _mockUserService;
        private Mock<IOrganisationService> _mockOrganisationService;
        private Mock<IProductService> _mockProductService;
        private IOrderService _orderService;
        private IAomModel _mockAomModel;

        public void AssertErrorMessageContains(string errorMsg, string expectedContains)
        {
            Assert.IsTrue(errorMsg.Contains(expectedContains), String.Format("Expect error message to contain '{0}' but it was: '{1}'", expectedContains, errorMsg));
        }

        [SetUp]
        public void Setup()
        {
            var model = new MockCrmModel();
            CrmDataBuilder.WithModel(model)
                .AddOrganisation("TestingOrg", out _tradingOrg1, OrganisationType.Trading)
                .AddUser("U1T1", false, out _user1InOrg1, _tradingOrg1)

                .AddOrganisation("TestingOrg2", out _tradingOrg2, OrganisationType.Trading)
                .AddUser("U1T1", false, out _user1InOrg2, _tradingOrg1)

                .AddOrganisation("TestingBroker1", out _brokerOrg1, OrganisationType.Brokerage)
                .AddUser("U1T1", false, out _user1InBroker1, _brokerOrg1)

                .AddOrganisation("TestingBroker2", out _brokerOrg2, OrganisationType.Brokerage)
                .AddUser("U1T1", false, out _user1InBroker2, _brokerOrg2);

            _mockOrganisationService = new Mock<IOrganisationService>();
            _mockOrganisationService.Setup(x => x.GetOrganisationById(_tradingOrg1.Id)).Returns(_tradingOrg1.ToEntity());
            _mockOrganisationService.Setup(x => x.GetOrganisationById(_tradingOrg2.Id)).Returns(_tradingOrg2.ToEntity());
            _mockOrganisationService.Setup(x => x.GetOrganisationById(_brokerOrg1.Id)).Returns(_brokerOrg1.ToEntity());
            _mockOrganisationService.Setup(x => x.GetOrganisationById(_brokerOrg2.Id)).Returns(_brokerOrg2.ToEntity());

            _mockUserService = new Mock<IUserService>();
            _mockUserService.Setup(x => x.GetUserWithPrivileges(_user1InOrg1.Id)).Returns(_user1InOrg1.ToEntity());
            _mockUserService.Setup(x => x.GetUserWithPrivileges(_user1InOrg2.Id)).Returns(_user1InOrg2.ToEntity());
            _mockUserService.Setup(x => x.GetUserWithPrivileges(_user1InBroker1.Id)).Returns(_user1InBroker1.ToEntity());
            _mockUserService.Setup(x => x.GetUserWithPrivileges(_user1InBroker2.Id)).Returns(_user1InBroker2.ToEntity());
            _mockUserService.Setup(x => x.GetUsersOrganisation(_user1InOrg1.Id)).Returns(_tradingOrg1.ToEntity());
            _mockUserService.Setup(x => x.GetUsersOrganisation(_user1InOrg2.Id)).Returns(_tradingOrg2.ToEntity());
            _mockUserService.Setup(x => x.GetUsersOrganisation(_user1InBroker1.Id)).Returns(_brokerOrg1.ToEntity());
            _mockUserService.Setup(x => x.GetUsersOrganisation(_user1InBroker2.Id)).Returns(_brokerOrg2.ToEntity());

            _mockProductService = new Mock<IProductService>();

            _mockAomModel = new MockAomModel
            {
                Orders = new ModifiedAddFakeDbSet<OrderDto>(FillInOrderDetails())
            };

            AomDataBuilder.WithModel(_mockAomModel)
                .AddOrderStatus("A", "Active")
                .AddOrderStatus("E", "Executed")
                .AddCurrency("EUR", "Eur")
                .AddUnit("BBL", "bbl")
                .AddDeliveryLocation("DeliveryLocation1")
                .AddProduct("Product1")
                .AddTenorCode("Tenor1")
                .AddProductTenor(_fakeProductTenorName)
                .DBContext.SaveChangesAndLog(-1);

            _orderService = new OrderService(_mockOrganisationService.Object, null, _mockUserService.Object, _dateTimeProvider, _mockProductService.Object,
                new MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));
        }

        //[Test]
        //public void CanExecuteOrderRunningAgainstDatabase()
        //{
        //    using (var transaction = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
        //    {
        //        using (var aomModel = new AomModel())
        //        {
        //            OrderDto testOrderDto;
                    
        //            PriceBasisDto priceBasisDto;
        //            PriceLineDto priceLineDto;

        //            var lowestProductId = aomModel.Products.Min(p => p.Id);
        //            var price = 800;

        //            AomDataBuilder.WithModel(aomModel)
        //                .OpenAllMarkets()
        //                .AddTenorCode("P")
        //                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
        //                .AddPriceType("FIXED")
        //                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
        //                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
        //                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
        //                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "A", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
        //                .DBContext.SaveChangesAndLog(-1);

        //            testOrderDto.BrokerOrganisationId = 998;
        //            testOrderDto.OrganisationId = 999;

        //            // Set up changes                
        //            var testOrder1 = new AomOrderAuthenticationResponse
        //            {
        //                MarketMustBeOpen = false,
        //                Message = new Message
        //                {
        //                    MessageAction = MessageAction.Update,
        //                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InOrg2.Id }
        //                },
        //                Order = new Order
        //                {
        //                    Quantity = testOrderDto.Quantity,
        //                    Price = testOrderDto.Price,
        //                    Id = testOrderDto.Id,
        //                    ExecutionMode = ExecutionMode.Internal,
        //                    OrderStatus = OrderStatus.Active,
        //                    OrderType = OrderType.Ask,
        //                    DeliveryStartDate = testOrderDto.DeliveryStartDate,
        //                    DeliveryEndDate = testOrderDto.DeliveryEndDate,
        //                    LastUpdated = testOrderDto.LastUpdated,
        //                    PrincipalUserId = testOrderDto.UserId,
        //                    ExecutionInfo = new ExecutionInfo()
        //                    {
        //                        PrincipalOrganisationId = _tradingOrg2.Id,
        //                        BrokerOrganisationId = _brokerOrg1.Id
        //                    }
        //                }
        //            };

        //            testOrder1.Order.ProductTenor = testOrderDto.ProductTenorDto.ToEntity();

        //            //ACT
        //            long matchingOrderId;
        //            var returnedOrder = _orderService.ExecuteOrder(aomModel, testOrder1, out matchingOrderId, MarketStatus.Open);

        //            // ASSERT
        //            Assert.AreEqual(testOrder1.Order.Quantity, returnedOrder.Quantity);
        //            Assert.AreEqual(testOrder1.Order.Price, returnedOrder.Price);
        //            Assert.AreEqual(testOrder1.Order.OrderType, returnedOrder.OrderType);
        //            Assert.AreEqual(OrderStatus.Executed, returnedOrder.OrderStatus);
        //            Assert.AreEqual(testOrder1.Order.DeliveryStartDate, returnedOrder.DeliveryStartDate);
        //            Assert.AreEqual(testOrder1.Order.DeliveryEndDate, returnedOrder.DeliveryEndDate);
        //            Assert.AreNotEqual(matchingOrderId, -1);

        //            var matchingOrder = aomModel.Orders.First(o => o.Id == matchingOrderId);
        //            Assert.AreEqual(_tradingOrg2.Id, matchingOrder.OrganisationId, "Expected execution info used to populate principal on new order");
        //            Assert.IsTrue(matchingOrder.BrokerOrganisationId.HasValue);
        //            Assert.AreEqual(_brokerOrg1.Id, matchingOrder.BrokerOrganisationId.Value, "Expected execution info used to populate broker on new order");

        //            transaction.Dispose();
        //        }
        //    }
        //}

        [Test]
        public void CanExecuteOrderAgainstMockDatabase()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            var mockDb = new MockDbContextFactory(_mockAomModel, null);

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InOrg2.Id }
                },
                Order = new Order
                {
                    Quantity = testOrderDto.Quantity,
                    Price = testOrderDto.Price,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Active,
                    OrderType = OrderType.Ask,
                    DeliveryStartDate = testOrderDto.DeliveryStartDate,
                    DeliveryEndDate = testOrderDto.DeliveryEndDate,
                    LastUpdated = testOrderDto.LastUpdated,
                    PrincipalUserId = testOrderDto.UserId,
                    ExecutionInfo = new ExecutionInfo() {PrincipalOrganisationId = _tradingOrg2.Id},
                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
                }
            };

            testOrder1.Order.ProductTenor = testOrderDto.ProductTenorDto.ToEntity();
            
            //ACT
            using (var aomDb = mockDb.CreateAomModel())
            {
                long matchingOrderId;
                Order returnedOrder = _orderService.ExecuteOrder(aomDb, testOrder1, out matchingOrderId, MarketStatus.Open);

                // ASSERT
                Assert.AreEqual(testOrder1.Order.Quantity, returnedOrder.Quantity);
                Assert.AreEqual(testOrder1.Order.Price, returnedOrder.Price);
                Assert.AreNotEqual(testOrder1.Order.OrderType, returnedOrder.OrderType);
                Assert.AreEqual(OrderStatus.Executed, returnedOrder.OrderStatus);
                Assert.AreEqual(testOrder1.Order.DeliveryStartDate, returnedOrder.DeliveryStartDate);
                Assert.AreEqual(testOrder1.Order.DeliveryEndDate, returnedOrder.DeliveryEndDate);
                Assert.True(matchingOrderId != -1);
            }
        }

        [Test]
        public void CantExecuteNonExistentOrder()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() {priceLineDto})
                .DBContext.SaveChangesAndLog(-1);

            var mockDb = new MockDbContextFactory(_mockAomModel, null);

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo {UserId = _user1InOrg2.Id}
                },
                Order = new Order
                {
                    Quantity = 200,
                    Price = 123M,
                    Id = -2222222222,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Active,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testDate,
                    DeliveryEndDate = _testDate,
                    PrincipalUserId = _user1InOrg1.Id,
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity()
                }
            };
        
            //ACT
            long matchingOrderId = -1;
            var ex = Assert.Throws<BusinessRuleException>( () => _orderService.ExecuteOrder( mockDb.CreateAomModel(), testOrder1, out matchingOrderId, MarketStatus.Open));
            Assert.That(ex.Message.Contains("does not exist"));
            // ASSERT
            Assert.True(matchingOrderId == -1);
        }

        [Test]
        public void CantExecuteLastUpdateChanged()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            var mockDb = new MockDbContextFactory(_mockAomModel, null);

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InOrg2.Id }
                },
                Order = new Order
                {
                    Quantity = 200,
                    Price = 123M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Active,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testDate,
                    DeliveryEndDate = _testDate,
                    PrincipalUserId = _user1InOrg1.Id,
                    LastUpdated = _dateTimeProvider.UtcNow,
                    ExecutionInfo = new ExecutionInfo(),
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity()
                }
            };

            //ACT
            long matchingOrderId = -1;

            // ASSERT
            var ex = Assert.Throws<BusinessRuleException>( () => _orderService.ExecuteOrder( mockDb.CreateAomModel(), testOrder1, out matchingOrderId, MarketStatus.Open));
            Assert.That(ex.Message.Contains("could not be hit as TestingOrg updated this order"), "Did not expect error :" + ex.Message);
            Assert.True(matchingOrderId == -1);
        }

        [Test]
        public void CantExecuteAKilledOrder()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", orderStatus: "K", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            var mockDb = new MockDbContextFactory(_mockAomModel, null);

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InOrg2.Id }
                },
                Order = new Order()
                {
                    Quantity = 200,
                    Price = 123M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Killed,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testDate,
                    DeliveryEndDate = _testDate,
                    PrincipalUserId = _user1InOrg1.Id,
                    LastUpdated = testOrderDto.LastUpdated,
                    ExecutionInfo = new ExecutionInfo(),
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity()
                }
            };

            //ACT
            long matchingOrderId = -1;
            // ASSERT
            var ex =Assert.Throws<BusinessRuleException>(  () =>_orderService.ExecuteOrder( mockDb.CreateAomModel(), testOrder1, out matchingOrderId, MarketStatus.Open));
            Assert.That(ex.Message.Contains("order is killed"), "Did not expect error :" + ex.Message);
            Assert.True(matchingOrderId == -1);
        }

        [Test]
        public void CantExecuteAnExecutedOrder()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", orderStatus: "K", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            var mockDb = new MockDbContextFactory(_mockAomModel, null);

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InOrg2.Id }
                },
                Order = new Order
                {
                    Quantity = 200,
                    Price = 123M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Executed,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testDate,
                    DeliveryEndDate = _testDate,
                    PrincipalUserId = _user1InOrg1.Id,
                    LastUpdated = testOrderDto.LastUpdated,
                    ExecutionInfo = new ExecutionInfo(),
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity()
                }
            };

            //ACT
            long matchingOrderId = -1;
            // ASSERT

            var ex = Assert.Throws<BusinessRuleException>( () => _orderService.ExecuteOrder( mockDb.CreateAomModel(), testOrder1, out matchingOrderId, MarketStatus.Open));
            Assert.That(ex.Message.Contains("order is killed"), "Did not expect error :" + ex.Message);
            Assert.True(matchingOrderId == -1);
        }

        [Test]
        public void CantExecuteOwnOrderPrincipal()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            testOrderDto.OrganisationId = _tradingOrg1.Id;

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InOrg1.Id }
                },
                Order = new Order
                {
                    Quantity = 200,
                    Price = 123M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Executed,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testDate,
                    DeliveryEndDate = _testDate,
                    PrincipalUserId = _user1InOrg1.Id,
                    LastUpdated = testOrderDto.LastUpdated,
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity(),
                    ExecutionInfo = new ExecutionInfo() {PrincipalOrganisationId = _tradingOrg1.Id}
                }
            };

            //ACT
            long matchingOrderId = -1;
            // ASSERT
            var ex = Assert.Throws<BusinessRuleException>( () => _orderService.ExecuteOrder(_mockAomModel, testOrder1, out matchingOrderId, MarketStatus.Open));
            Assert.That(ex.Message.Contains("You cannot execute your own order"));
            Assert.True(matchingOrderId == -1);
        }

        [Test]
        public void CantExecuteOrderBrokerSamePrincipals()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto }, brokerId: _user1InBroker1.Id)
                .DBContext.SaveChangesAndLog(-1);

            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InBroker1.Id }
                },
                Order = new Order
                {
                    Quantity = 200,
                    Price = 123M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Executed,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testDate,
                    DeliveryEndDate = _testDate,
                    PrincipalUserId = _user1InOrg1.Id,
                    PrincipalOrganisationId = testOrderDto.OrganisationId,
                    LastUpdated = testOrderDto.LastUpdated,
                    ExecutionInfo = new ExecutionInfo {PrincipalOrganisationId = testOrderDto.OrganisationId},
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity(),
                    BrokerId = _user1InBroker1.Id
                }
            };

            long matchingOrderId = -1;
            var ex =Assert.Throws<BusinessRuleException>( () => _orderService.ExecuteOrder(_mockAomModel, testOrder1, out matchingOrderId, MarketStatus.Open));
            Assert.That(ex.Message.Contains("same principal on both sides"));
            Assert.True(matchingOrderId == -1);
        }

        [Test]
        public void CanExecuteOrderAsBrokerWithSameBrokerBothSides()
        {
            OrderDto testOrderDto;
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto }, brokerId: _user1InBroker1.Id)
                .DBContext.SaveChangesAndLog(-1);

            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InBroker1.Id }
                },
                Order =new Order
                {
                    Quantity = testOrderDto.Quantity,
                    Price = testOrderDto.Price,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Executed,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = testOrderDto.DeliveryStartDate,
                    DeliveryEndDate = testOrderDto.DeliveryEndDate,
                    PrincipalUserId = _user1InOrg2.Id,
                    PrincipalOrganisationId = _tradingOrg2.Id,
                    LastUpdated = testOrderDto.LastUpdated,
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity(),
                    BrokerId = _user1InBroker1.Id,
                    ExecutionInfo = new ExecutionInfo { BrokerOrganisationId = _brokerOrg1.Id, PrincipalOrganisationId = _tradingOrg1.Id }
                }
            };

            long matchingOrderId;
            var order = _orderService.ExecuteOrder(_mockAomModel, testOrder1, out matchingOrderId, MarketStatus.Open);

            Assert.That(order.BrokerId, Is.EqualTo(_user1InBroker1.Id));
            Assert.That(order.Quantity, Is.EqualTo(testOrder1.Order.Quantity));
            Assert.That(order.Price, Is.EqualTo(testOrder1.Order.Price));
            Assert.That(order.Id, Is.EqualTo(testOrder1.Order.Id));
            Assert.That(order.OrderStatus, Is.EqualTo(OrderStatus.Executed));
            Assert.That(order.OrderType, Is.EqualTo(testOrder1.Order.OrderType));
            Assert.That(order.DeliveryStartDate, Is.EqualTo(testOrder1.Order.DeliveryStartDate));
            Assert.That(order.DeliveryEndDate, Is.EqualTo(testOrder1.Order.DeliveryEndDate));
            Assert.That(order.PrincipalUserId, Is.EqualTo(testOrderDto.UserId));
            Assert.That(order.PrincipalOrganisationId, Is.EqualTo(testOrderDto.OrganisationId));
            Assert.That(order.ProductTenor.Id, Is.EqualTo(testOrder1.Order.ProductTenor.Id));
            Assert.False(matchingOrderId == -1);
        }

        [TestCase("Cant execute when product is not co-brokerable", false, true, true)]
        [TestCase("Cant execute when order is not co-brokerable", true, false, true)]
        [TestCase("Cant execute when both product & order are not co-brokerable", false, false, true)]
        [TestCase("Happy path", true, true, false)]
        public void AsBrokerICantExecuteBrokeredOrderWhenEitherOrderOrProductAreNotCoBrokerable(string description, bool productIsCoBrokerable, bool orderIsCoBrokerable, bool exceptionExpected)
        {
            OrderDto testOrderDto;
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("Tenor1")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, brokerId: _user1InBroker1.Id, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            testOrderDto.CoBrokering = orderIsCoBrokerable;
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InBroker2.Id }
                },
                Order = new Order
                {
                    CoBrokering = testOrderDto.CoBrokering,
                    Quantity = testOrderDto.Quantity,
                    Price = testOrderDto.Price,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Executed,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = testOrderDto.DeliveryStartDate,
                    DeliveryEndDate = testOrderDto.DeliveryEndDate,
                    PrincipalUserId = _user1InOrg1.Id,
                    PrincipalOrganisationId = _tradingOrg1.Id,
                    LastUpdated = testOrderDto.LastUpdated,
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity(),
                    BrokerId = _user1InBroker1.Id,
                    BrokerOrganisationId = 100,
                    ExecutionInfo = new ExecutionInfo
                    {
                        BrokerOrganisationId = _brokerOrg2.Id,
                        PrincipalOrganisationId = _tradingOrg2.Id
                    }
                }
            };

            _mockProductService.Setup(m => m.CoBrokeringAllowed(It.IsAny<long>())).Returns(productIsCoBrokerable);
            var nonMatchingOrganisation = new Organisation
            {
                Id = 300,
                Name = "CoBrokered Organisation",
                OrganisationType = OrganisationType.Brokerage
            };
            _mockUserService.Setup(x => x.GetUsersOrganisation(It.Is<long>(p => p == 1000))).Returns(nonMatchingOrganisation);

            bool exceptionOccured = false;

            try
            {
                long matchingOrderId;
                var order = _orderService.ExecuteOrder(_mockAomModel, testOrder1, out matchingOrderId, MarketStatus.Open);
            }
            catch (BusinessRuleException ex)
            {
                exceptionOccured = true;

                Assert.That(ex.Message, Is.EqualTo("Four-way deals are only supported if the order and product are co-brokerable"),
                    String.Format("Exception expect when ProductIsCoBrokerable={0} and OrderIsCoBrokerable={1}", productIsCoBrokerable, orderIsCoBrokerable));
            }

            Assert.AreEqual(exceptionExpected, exceptionOccured, "expected exception " + (exceptionExpected ? "to" : "not") + " occur");
        }

        [Test]
        public void CanExecuteOrderAsBrokerForCoBrokeredOrder()
        {
            OrderDto testOrderDto;
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto }, brokerId: _user1InBroker1.Id)
                .DBContext.SaveChangesAndLog(-1);

            testOrderDto.CoBrokering = true;
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InBroker1.Id } //a user within the ExecutionInfo.BrokerOrganisationId
                },
                Order = new Order
                {
                    CoBrokering = testOrderDto.CoBrokering,
                    Quantity = testOrderDto.Quantity,
                    Price = testOrderDto.Price,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Executed,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = testOrderDto.DeliveryStartDate,
                    DeliveryEndDate = testOrderDto.DeliveryEndDate,
                    //PrincipalUserId = _user1InOrg2.Id,
                    PrincipalOrganisationId = _tradingOrg2.Id,
                    LastUpdated = testOrderDto.LastUpdated,
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity(),
                    BrokerId = _user1InBroker1.Id,
                    BrokerOrganisationId = 100,
                    ExecutionInfo = new ExecutionInfo
                    {
                        BrokerOrganisationId = _brokerOrg1.Id,
                        PrincipalOrganisationId = _tradingOrg1.Id
                    }
                }
            };
            _mockProductService.Setup(m => m.CoBrokeringAllowed(It.IsAny<long>())).Returns(true);
            var nonMatchingOrganisation = new Organisation
            {
                Id = 300,
                Name = "CoBrokered Organisation",
                OrganisationType = OrganisationType.Brokerage
            };
            _mockUserService.Setup(x => x.GetUsersOrganisation(It.Is<long>(p => p == 1000))).Returns(nonMatchingOrganisation);

            string exceptionMessage = string.Empty;
            long matchingOrderId = long.MaxValue;
            try
            {
                _orderService.ExecuteOrder(_mockAomModel, testOrder1, out matchingOrderId, MarketStatus.Open);
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
            }

            Assert.That(exceptionMessage, Is.Empty);

            var matchingOrder = _mockAomModel.Orders.First(o => o.Id == matchingOrderId);
            Assert.IsTrue(matchingOrder.CoBrokering, "new order should have cobrokering=true (as aggressed by a broker)");
            Assert.AreEqual(matchingOrder.BrokerOrganisationId, testOrder1.Order.ExecutionInfo.BrokerOrganisationId);
            Assert.AreEqual(matchingOrder.OrganisationId, testOrder1.Order.ExecutionInfo.PrincipalOrganisationId);
        }

        [Test]
        public void CanExecuteOrderAsPrincipalForCoBrokeredOrder()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto }, brokerId: _user1InBroker1.Id)
                .DBContext.SaveChangesAndLog(-1);

            testOrderDto.CoBrokering = true;

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo { UserId = _user1InOrg1.Id }//a user within the ExecutionInfo.PrincipalOrganisationId }
                },
                Order = new Order
                {
                    CoBrokering = testOrderDto.CoBrokering,
                    Quantity = testOrderDto.Quantity,
                    Price = testOrderDto.Price,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Executed,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = testOrderDto.DeliveryStartDate,
                    DeliveryEndDate = testOrderDto.DeliveryEndDate,
                    PrincipalUserId = _user1InOrg2.Id,
                    PrincipalOrganisationId = _tradingOrg2.Id,
                    LastUpdated = testOrderDto.LastUpdated,
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity(),
                    BrokerId = _user1InBroker1.Id,
                    BrokerOrganisationId = 100,
                    ExecutionInfo = new ExecutionInfo {BrokerOrganisationId = null, PrincipalOrganisationId = _tradingOrg1.Id}
                }
            };


            _mockProductService.Setup(m => m.CoBrokeringAllowed(It.IsAny<long>())).Returns(true);
            var nonMatchingOrganisation = new Organisation
            {
                Id = 300,
                Name = "CoBrokered Organisation",
                OrganisationType = OrganisationType.Brokerage
            };
            _mockUserService.Setup(x => x.GetUsersOrganisation(It.Is<long>(p => p == 1000))).Returns(nonMatchingOrganisation);

            string exceptionMessage = string.Empty;
            long matchingOrderId = long.MaxValue;
            try
            {
                _orderService.ExecuteOrder(_mockAomModel, testOrder1, out matchingOrderId, MarketStatus.Open);

            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
            }

            Assert.That(exceptionMessage, Is.Empty);

            var matchingOrder = _mockAomModel.Orders.First(o => o.Id == matchingOrderId);
            Assert.IsFalse(matchingOrder.CoBrokering, "new order should have cobrokering=false (as aggressed by principal)");
            Assert.IsNull(matchingOrder.BrokerOrganisationId);
            Assert.AreEqual(matchingOrder.OrganisationId, testOrder1.Order.ExecutionInfo.PrincipalOrganisationId);
        }

        [Test]
        public void CannotExecuteOrderUpdatedToHeld()
        {
            CannotExecuteHeldOrderImpl(orderLastUpdatedDelay: TimeSpan.FromSeconds(1), errorMessageShouldContain: "withdrew this order");
        }

        [Test]
        public void CannotExecuteHeldOrder()
        {
            CannotExecuteHeldOrderImpl(orderLastUpdatedDelay: null, errorMessageShouldContain: "held order cannot be executed");
        }

        private void CannotExecuteHeldOrderImpl(TimeSpan? orderLastUpdatedDelay, string errorMessageShouldContain)
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "A", itm: out testOrderDto, orderStatus: DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held),
                    principalOrganisationId: _tradingOrg2.Id,
                    brokerId: _user1InBroker1.Id, 
                    price: price,
                    pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            // Set up changes
            var orderLastUpdated = testOrderDto.LastUpdated;
            if (orderLastUpdatedDelay.HasValue)
            {
                orderLastUpdated += orderLastUpdatedDelay.Value;
            }

            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo {UserId = _user1InBroker1.Id}
                },
                Order = new Order
                {
                    Quantity = 200,
                    Price = 123M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Held,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testDate,
                    DeliveryEndDate = _testDate,
                    PrincipalUserId = _user1InOrg2.Id,
                    PrincipalOrganisationId = _tradingOrg2.Id,
                    LastUpdated = orderLastUpdated,
                    ProductTenor = testOrderDto.ProductTenorDto.ToEntity(),
                    BrokerId = _user1InBroker1.Id,
                    ExecutionInfo = new ExecutionInfo {BrokerOrganisationId = 3}
                }
            };

            //ACT
            long matchingOrderId = -1;
            // ASSERT
            var ex = Assert.Throws<BusinessRuleException>( () => _orderService.ExecuteOrder(_mockAomModel, testOrder1, out matchingOrderId, MarketStatus.Open));
            Assert.That(ex.Message.Contains(errorMessageShouldContain), "Message was: " + ex.Message);
            Assert.True(matchingOrderId == -1);
        }

        //[Test]
        //public void OnlyOneOrderPriceLineWithMatchingStatusAfterExecutionIsAllowed()
        //{
        //    var entityHelper = new EntityHelper(_mockUserService.Object, _mockOrganisationService.Object, _mockAomModel, _testDate, _testDate, _fakeProductTenorName, (int)_user1InOrg1.Id);
        //    var orderEntity = entityHelper.PopulateOrderInMockDb().ToEntity(_mockOrganisationService.Object, _mockUserService.Object);

        //    var orderRequest = new AomOrderAuthenticationResponse
        //    {
        //        Order = orderEntity,
        //        MarketMustBeOpen = false,
        //        Message = new Message
        //        {
        //            MessageAction = MessageAction.Create,
        //            MessageType = MessageType.Order,
        //            ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
        //        }
        //    };

        //    orderRequest.Order.OrderStatus = OrderStatus.Executed;
        //    orderRequest.Order.OrderPriceLines.ForEach(opl => opl.OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Executed));

        //    var errorMessageExpectedContain = "only one order price line may have a status matching order status";

        //    var currentTest = "Order executed and all order price line status are matching";
        //    TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, orderRequest, MarketStatus.Open),errorMessageExpectedContain, currentTest);

        //    currentTest = "Order executed and more than one order price lines status are matching";
        //    // add another order price line
        //    var priceLineEntity = entityHelper.CreatePriceLineEntity(2);
        //    var productId = orderRequest.Order.OrderPriceLines.Select(opl => opl.PriceLine.PriceLinesBases.First().ProductId).First();
        //    var tenorId = orderRequest.Order.OrderPriceLines.Select(opl => opl.PriceLine.PriceLinesBases.First().TenorCode).First();
        //    var priceLineBasis1 = entityHelper.CreatePriceLineBasis(0, 50, orderRequest.Order.Price, 1, productId, tenorId);
        //    priceLineEntity.PriceLinesBases = new List<PriceLineBasis>() { priceLineBasis1 };
        //    var priceLineBasis2 = entityHelper.CreatePriceLineBasis(1, 50, orderRequest.Order.Price, 1, productId, tenorId);
        //    priceLineEntity.PriceLinesBases = new List<PriceLineBasis>() { priceLineBasis2 };
        //    var orderPriceLineEntity = entityHelper.CreateOrderPriceLineEntity(orderRequest.Order.Id, DtoMappingAttribute.GetValueFromEnum(OrderStatus.VoidAfterExecuted), priceLineEntity.Id);
        //    orderPriceLineEntity.PriceLine = priceLineEntity;
        //    orderRequest.Order.OrderPriceLines.Add(orderPriceLineEntity);

        //    TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.EditOrder(_mockAomModel, orderRequest, MarketStatus.Open), errorMessageExpectedContain, currentTest);
        //}

        [Test]
        public void VirtualMatchingOrderFromExecutionHasAggressorAsPrincipal()
        {
            OrderDto testOrderDto;
            
            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;

            var lowestProductId = _mockAomModel.Products.Min(p => p.Id);
            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", productId: lowestProductId)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, _user1InOrg1.Id)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: _user1InOrg1.Id, productTenorName: "Product1_Tenor1", bidOrAsk: "B", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);

            var orderToExecute = testOrderDto.ToEntity(_mockOrganisationService.Object, _mockUserService.Object);

            orderToExecute.ExecutionInfo = new ExecutionInfo
            {
                PrincipalOrganisationId = _tradingOrg2.Id,
                BrokerOrganisationId = null
            };
            orderToExecute.ProductTenor = testOrderDto.ProductTenorDto.ToEntity();
            
            var authResponse = CreateAuthRepsonse(orderToExecute, _user1InOrg2.Id);

            // Act
            long matchingOrderId;
            var updatedOrder = _orderService.ExecuteOrder(_mockAomModel, authResponse, out matchingOrderId, MarketStatus.Open);

            // Assert
            Assert.IsNotNull(updatedOrder);
            Assert.That(updatedOrder.OrderStatus, Is.EqualTo(OrderStatus.Executed));

            var virtualOrderDto = _mockAomModel.Orders.FirstOrDefault(o => o.Id == matchingOrderId);
            Assert.IsNotNull(virtualOrderDto);
            var virtualOrder = virtualOrderDto.ToEntity(_mockOrganisationService.Object, _mockUserService.Object);
            Assert.That(virtualOrder.IsVirtual, Is.True);
            Assert.That(virtualOrder.OrderType, Is.EqualTo(OrderType.Ask));
            Assert.That(virtualOrder.Price, Is.EqualTo(testOrderDto.Price));
            Assert.That(virtualOrder.Quantity, Is.EqualTo(testOrderDto.Quantity));
            Assert.That(virtualOrder.PrincipalUserId, Is.EqualTo(_user1InOrg2.Id));
            Assert.That(virtualOrder.PrincipalOrganisationId, Is.EqualTo(_tradingOrg2.Id));
            Assert.That(virtualOrder.PrincipalOrganisationName, Is.EqualTo("TestingOrg2"));
        }

        private static AomOrderAuthenticationResponse CreateAuthRepsonse(Order order, long userId, MessageAction messageAction = MessageAction.Execute, bool marketMustBeOpen = true)
        {
            return new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = marketMustBeOpen,
                Order = order,
                Message = new Message
                {
                    MessageAction = messageAction,
                    ClientSessionInfo = new ClientSessionInfo {UserId = userId}
                }
            };
        }

        private Action<OrderDto> FillInOrderDetails()
        {
            return orderDto =>
            {
                if (orderDto.Id == -1)
                {
                    orderDto.Id = AomDataBuilder.WithModel(_mockAomModel).NextId();
                }
                orderDto.ProductTenorDto = _mockAomModel.ProductTenors.First(t => t.Id == orderDto.ProductTenorId);
            };
        }
    }
}