﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using ServiceStack.Text;

namespace AOM.Services.Tests.OrderService
{
    class EntityHelper
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly IAomModel _mockAomModel;

        private readonly int TestUserId;
        private readonly DateTime _fakeStartDate;
        private readonly DateTime _fakeEndDate;
        private readonly string FakeProductTenorName;

        public EntityHelper(IUserService userService, IOrganisationService organisationService, IAomModel mockAomModel,
             DateTime fakeStartDate, DateTime fakeEndDate, string fakeProductTenorName, int testUserId)
        {
            _userService = userService;
            _organisationService = organisationService;
            _mockAomModel = mockAomModel;
            _fakeStartDate = fakeStartDate;
            _fakeEndDate = fakeEndDate;
            FakeProductTenorName = fakeProductTenorName;
            TestUserId = testUserId;
        }

        public Order CreateOrderEntity(decimal price, decimal qty, ProductTenorDto productTenorDto, long orderId = -1)
        {
            List<DeliveryLocation> delLocs = new List<DeliveryLocation>();
            foreach (DeliveryLocationDto delLoc in productTenorDto.ProductDto.DeliveryLocations)
            {
                delLocs.Add(new DeliveryLocation() {
                    Id = delLoc.Id,
                    Name = delLoc.Name,
                    DateCreated = delLoc.DateCreated
                });
            }

            return new Order
            {
                Quantity = qty,
                Price = price,
                Id = orderId,
                ExecutionMode = ExecutionMode.Internal,
                OrderStatus = OrderStatus.Active,
                OrderType = OrderType.Ask,
                DeliveryStartDate = _fakeStartDate,
                DeliveryEndDate = _fakeEndDate,
                PrincipalUserId = TestUserId,
                BrokerRestriction = BrokerRestriction.Any,
                ProductId = productTenorDto.ProductId,
                ProductTenor = new ProductTenor
                {
                    Id = productTenorDto.Id,
                    Product = new Product
                    {
                        ProductId = productTenorDto.ProductId,
                        DeliveryLocations = delLocs
                    }
                }
            };
        }

        public OrderPriceLine CreateOrderPriceLineEntity(long orderId, string orderStatus, long priceLineId, long entityId = -1)
        {
            return new OrderPriceLine
            {
                Id = entityId,
                OrderId = orderId,
                OrderStatusCode = orderStatus,
                OrderStatus = DtoMappingAttribute.FindEnumWithValue<OrderStatus>(orderStatus),
                PriceLineId = priceLineId,
            };
        }

        public PriceLine CreatePriceLineEntity(int sequenceNo, long entityId = -1)
        {
            return new PriceLine
            {
                Id = entityId,
                SequenceNo = sequenceNo
            };
        }

        public PriceLineBasis CreatePriceLineBasis(int sequenceNo, int percentageSplit, decimal priceLineBasisValue, long priceBasisId, long productId, string tenorCode, long entityId = -1)
        {
            return new PriceLineBasis
            {
                Id = entityId,
                Description = "testtest",
                SequenceNo = sequenceNo,
                PercentageSplit = percentageSplit,
                PricingPeriodFrom = DateTime.UtcNow,
                PricingPeriodTo = DateTime.UtcNow.AddDays(7),
                PriceLineBasisValue = priceLineBasisValue,
                //PriceLineId = priceLineId,
                PriceBasisId = priceBasisId,
                ProductId = productId,
                TenorCode = tenorCode,
            };
        }

        internal AomOrderAuthenticationResponse CreateOrder(decimal price = 10.0m, decimal qty = 500m)
        {
            var db = AomDataBuilder.WithModel(_mockAomModel);
            var productTenorDto = db.DBContext.ProductTenors.First(x => x.Name == FakeProductTenorName);

            var newOrder = CreateOrderEntity(price, qty, productTenorDto);

            var priceLineBasis1 = CreatePriceLineBasis(0, 50, newOrder.Price, 1, productTenorDto.ProductId, "P");
            var priceLineBasis2 = CreatePriceLineBasis(1, 50, newOrder.Price, 1, productTenorDto.ProductId, "P");
            var priceLineBasis3 = CreatePriceLineBasis(0, 50, newOrder.Price, 1, productTenorDto.ProductId, "P");
            var priceLineBasis4 = CreatePriceLineBasis(1, 50, newOrder.Price, 1, productTenorDto.ProductId, "P");

            var priceLine1 = CreatePriceLineEntity(0, -4);
            priceLine1.PriceLinesBases = new List<PriceLineBasis>() {priceLineBasis1, priceLineBasis2};

            var priceLine2 = CreatePriceLineEntity(priceLine1.SequenceNo - 1, priceLine1.Id - 1);
            priceLine2.PriceLinesBases = new List<PriceLineBasis>() {priceLineBasis3, priceLineBasis4};
            
            var orderPriceLine1 = CreateOrderPriceLineEntity(newOrder.Id, DtoMappingAttribute.GetValueFromEnum(newOrder.OrderStatus), priceLine1.Id, -4);
            orderPriceLine1.PriceLine = priceLine1;

            var orderPriceLine2 = CreateOrderPriceLineEntity(newOrder.Id, DtoMappingAttribute.GetValueFromEnum(newOrder.OrderStatus), priceLine2.Id, -5);
            orderPriceLine2.PriceLine = priceLine2;

            newOrder.OrderPriceLines = new List<OrderPriceLine>()
            {
                orderPriceLine1,
                orderPriceLine2
            };

            return new AomOrderAuthenticationResponse
            {
                Order = newOrder,
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Create,
                    MessageType = MessageType.Order,
                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
                }
            };
        }

        internal OrderDto PopulateOrderInMockDb()
        {
            PriceBasisDto priceBasisDto;
            OrderDto testOrderDto;
            PriceLineDto priceLineDto;
            PriceLineDto priceLineDto2;

            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1")
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, TestUserId)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddPriceLine(out priceLineDto2, TestUserId)
                .AddPriceLineBasis(price, priceLineDto2, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "A", itm: out testOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto, priceLineDto2 })
                .DBContext.SaveChangesAndLog(-1);

            return testOrderDto;
        }
    }
}
