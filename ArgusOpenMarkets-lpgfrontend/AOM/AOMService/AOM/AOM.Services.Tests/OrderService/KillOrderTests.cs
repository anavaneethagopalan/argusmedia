﻿using System.Collections.Generic;
using System.Linq;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.DealService;
using AOM.Services.MetadataValidator;
using AOM.Services.OrderService;
using AOM.Services.ProductMetadataService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.OrderService
{
    [TestFixture]
    public class KillOrderTests
    {
        private Mock<IOrganisationService> _mockOrganisationService;
        private Mock<IDealService> _mockDealService;
        private Mock<IUserService> _mockUserService;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private Mock<IProductService> _mockProductService;

        private Services.OrderService.OrderService _service;
        private MockAomModel _mockAomModel;

        private const int FakeUserId = 123;
        private const string FakeProductName = "FakeProductName";
        private const string FakeTenorName = "FakeTenorName";
        private const string FakeProductTenorName = "FakeProductTenorName";
        private const string FakeDeliveryLocation = "FakeDeliveryLocation";
        private OrderDto _fakeOrderDto;

        [SetUp]
        public void Setup()
        {
            _mockOrganisationService = new Mock<IOrganisationService>();
            _mockDealService = new Mock<IDealService>();
            _mockUserService = new Mock<IUserService>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockProductService = new Mock<IProductService>();

            _service = new Services.OrderService.OrderService(_mockOrganisationService.Object, _mockDealService.Object, _mockUserService.Object,_mockDateTimeProvider.Object, 
                _mockProductService.Object, new Services.MetadataValidator.MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));

            PriceBasisDto priceBasisDto;
            PriceLineDto priceLineDto;
            long testUserId = 666;
            var price = 800;

            _mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(_mockAomModel)
                .AddCurrency("EUR", "Eur")
                .AddUnit("BBL", "bbl")
                .AddDeliveryLocation(FakeDeliveryLocation)
                .AddProduct(FakeProductName, 1)
                .AddTenorCode("P")
                .AddProductTenor(FakeProductTenorName, 1)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, testUserId)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: testUserId, productTenorName: FakeProductTenorName, bidOrAsk: "B", itm: out _fakeOrderDto, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
                .DBContext.SaveChangesAndLog(-1);
        }

        [Test]
        public void OrderStatusFromEFShouldNotBeNull()
        {
            using (var context = new AomModel())
            {
                //2. get Kill order status
                var orderStatuses = from orderStatus in context.OrderStatus
                    select orderStatus;

                Assert.IsNotNull(orderStatuses);
            }
        }

        [Test]
        public void OrderStatusCountFromEfShouldNotBeEmpty()
        {
            using (var context = new AomModel())
            {
                //2. get Kill order status
                var orderStatuses = from orderStatus in context.OrderStatus
                                    select orderStatus;

                Assert.IsTrue(orderStatuses.Any());
            }
        }

        [Test]
        public void AssertThatOrderStatusesContainsACodeOfLetterK()
        {
            using (var context = new AomModel())
            {
                var orderStatuses = from orderStatus in context.OrderStatus
                                    select orderStatus;

                Assert.IsTrue(orderStatuses.Any(x => x.Code == "K"));
            }
        }

        [Test]
        public void AssertThatOrderStatusesContainsOneCodeWithLetterK()
        {
            using (var context = new AomModel())
            {
                var orderStatuses = from orderStatus in context.OrderStatus
                                    select orderStatus;

                var killStatus = orderStatuses.SingleOrDefault(x => x.Code == "K");

                Assert.IsNotNull(killStatus);
                Assert.IsTrue(killStatus.Code == "K");
            }
        }

        [Test]
        [TestCase(MarketStatus.Closed)]
        [TestCase(MarketStatus.Open)]
        public void AssertThatCanKillActiveOrderIfMarketIsOpenOrClosed(MarketStatus marketStatus)
        {
            // Arrange
            var response = new AomOrderAuthenticationResponse
            {
                Order = _fakeOrderDto.ToEntity(_mockOrganisationService.Object, _mockUserService.Object),
                MarketMustBeOpen = false,
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo
                    {
                        UserId = FakeUserId
                    }
                }
            };

            // Act
            var afterKill = _service.KillOrder(_mockAomModel, response, marketStatus);

            // Assert
            Assert.That(afterKill.OrderStatus, Is.EqualTo(OrderStatus.Killed));
        }
    }
}
