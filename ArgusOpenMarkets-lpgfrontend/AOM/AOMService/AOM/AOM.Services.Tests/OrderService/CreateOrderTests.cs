﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Transactions;

//using AOM.App.Domain;
//using AOM.App.Domain.Dates;
//using AOM.App.Domain.Entities;
//using AOM.App.Domain.Mappers;
//using AOM.App.Domain.Services;
//using AOM.Repository.MySql;
//using AOM.Repository.MySql.Aom;
//using AOM.Repository.MySql.Tests;
//using AOM.Repository.MySql.Tests.Aom;
//using AOM.Services.CrmService;
//using AOM.Services.MetadataValidator;
//using AOM.Services.OrderService;
//using AOM.Services.ProductService;
//using AOM.Transport.Events;
//using AOM.Transport.Events.Orders;
//using Moq;
//using NUnit.Framework;
//using ServiceStack.Text;

//namespace AOM.Services.Tests.OrderService
//{
//    [TestFixture]
//    public class CreateOrderTests
//    {
//        private IOrderService _orderService;
//        private Mock<IDateTimeProvider> _mockDateTimeProvider;
//        private readonly DateTimeProvider _realDateTimeProvider = new DateTimeProvider();
//        private IUserService _userService;
//        private IOrganisationService _organisationService;

//        private const int TestUserId = 123;
//        private const int TestOrgId = 321;
//        private const decimal FakeMinPrice = 1.0m;
//        private const decimal FakeMaxPrice = 100m;
//        private const decimal FakeOrderPriceIncrement = 0.1m;
//        private const decimal FakeMinQty = 200m;
//        private const decimal FakeMaxQty = 1000m;
//        private const decimal FakeOrderQtyIncrement = 100m;
//        private const string FakeProductName = "FakeProduct";
//        private const string FakeTenorName = "FakeTenor";
//        private const string FakeProductTenorName = "FakeProductTenor";
//        private const int FakeProductId = 12345;

//        private List<MetaDataListItemDto> _metaDataListElements;
//        private readonly DateTime _fakeStartDate = DateTime.Today.AddDays(1);
//        private readonly DateTime _fakeEndDate = DateTime.Today.AddDays(4);

//        private Mock<IUserService> _mockUserService;
//        private Mock<IOrganisationService> _mockOrganisationService;
//        private IAomModel _mockAomModel;
//        private OrderDto _orderDto;
//        private EntityHelper _entityHelper;

//        [SetUp]
//        public void Setup()
//        {
//            _userService = new UserService(new DbContextFactory(), _realDateTimeProvider, new Mock<IProductService>().Object);
//            _organisationService = new OrganisationService(new DbContextFactory(), new BilateralPermissionsBuilder());

//            _mockOrganisationService = new Mock<IOrganisationService>();
//            _mockUserService = new Mock<IUserService>();
//            _mockUserService.Setup(x => x.GetUsersOrganisation(TestUserId)).Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});
//            _mockOrganisationService.Setup(x => x.GetOrganisationById(TestOrgId)).Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});

//            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
//            _mockDateTimeProvider.SetupGet(p => p.Now).Returns(_realDateTimeProvider.Now);
//            _mockDateTimeProvider.SetupGet(p => p.UtcNow).Returns(_realDateTimeProvider.UtcNow);
//            _mockDateTimeProvider.SetupGet(p => p.Today).Returns(_realDateTimeProvider.Today);

//            _metaDataListElements = new List<MetaDataListItemDto>
//            {
//                new MetaDataListItemDto() {Id = 1, ValueText = "PortA", ValueLong = 100},
//                new MetaDataListItemDto() {Id = 2, ValueText = "PortB", ValueLong = 101},
//                new MetaDataListItemDto() {Id = 3, ValueText = "PortC", ValueLong = 102}
//            };

//            _mockAomModel = new MockAomModel();
//            PriceBasisDto priceBasisDto;
//            PriceLineDto priceLineDto;
//            var price = 10m;

//            AomDataBuilder.WithModel(_mockAomModel)
//                .OpenAllMarkets()
//                .AddOrderStatus("A", "Active")
//                .AddOrderStatus("E", "Executed")
//                .AddOrderStatus("V", "VoidAfterExecuted")
//                .AddCurrency("EUR", "Eur")
//                .AddUnit("BBL", "bbl")
//                .AddDeliveryLocation("DeliveryLocation1")
//                .AddProduct(FakeProductName, FakeProductId, minPrice: FakeMinPrice, maxPrice: FakeMaxPrice, minQty: FakeMinQty, maxQty: FakeMaxQty, priceIncrement: FakeOrderPriceIncrement, qtyIncrement: FakeOrderQtyIncrement)
//                .AddTenorCode("P")
//                .AddProductTenor(FakeProductTenorName, FakeProductId)
//                .AddPriceType("FIXED")
//                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
//                .AddPriceLine(out priceLineDto, TestUserId)
//                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
//                .AddOrder(userId: TestUserId, productTenorName: FakeProductTenorName, bidOrAsk: "A", quantity: 500m, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto }, itm: out _orderDto)
//                .AddProductMetaDataList("TestingList", 1, _metaDataListElements)
//                .AddProductMetaDataItem(FakeProductName, "DeliveryPort", 1, MetaDataTypes.IntegerEnum, "TestingList")
//                .AddProductMetaDataItem(FakeProductName, "DeliveryDescription", 1, MetaDataTypes.String, null, 5, 20)
//                .DBContext.SaveChangesAndLog(-1);
            
//            var productService = new Services.ProductService.ProductService(new MockDbContextFactory(_mockAomModel, null), _mockDateTimeProvider.Object);
//            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(new MockDbContextFactory(_mockAomModel, null), _mockDateTimeProvider.Object);

//            _orderService = new Services.OrderService.OrderService(_organisationService, new Services.DealService.DealService(_userService, _organisationService, _realDateTimeProvider),
//                _userService, _realDateTimeProvider, productService, new Services.MetadataValidator.MetadataValidator(productMetadataService, new IMetadataItemValidator[]
//                {new MetadataCommonValidator(), new MetadataItemEnumValidator(), new MetadataStringValidator()}));

//            _entityHelper = new EntityHelper(_userService, _organisationService, _mockAomModel, _fakeStartDate, _fakeEndDate, FakeProductTenorName, TestUserId);
//        }

//        [Test]
//        public void CanCreateOrderRunningAgainstDatabase()
//        {
//            using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
//            {
//                using (var aomModel = new AomModel())
//                {
//                    OrderDto itm;
//                    PriceBasisDto priceBasisDto;
//                    PriceLineDto priceLineDto;
//                    var price = 800;

//                    AomDataBuilder.WithModel(aomModel)
//                        .AddDeliveryLocation("DeliveryLocation1")
//                        .AddProduct("Product1")
//                        .AddTenorCode("P")
//                        .AddProductTenor("Product1_Tenor1")
//                        .AddProductMetaDataItem("Product1", "Quantity", 1, MetaDataTypes.String)
//                        .AddPriceType("FIXED")
//                        .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
//                        .AddPriceLine(out priceLineDto, TestUserId)
//                        .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
//                        .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "A", itm: out itm, orderStatus: "A", quantity: 20000, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                        .OpenAllMarkets()
//                        .DBContext.SaveChangesAndLog(-1);

//                    long productId = aomModel.Products.Single(p => p.Name == "Product1").Id;
                    
//                    var metaDataItemDto = aomModel.ProductMetaDataItems.First(m => m.ProductId == productId);
//                    itm.MetaData = new[]
//                    {
//                        new MetaDataStringDto()
//                        {
//                            ProductMetaDataId = metaDataItemDto.Id,
//                            DisplayName = metaDataItemDto.DisplayName,
//                            DisplayValue = "AValue"
//                        }
//                    };

//                    var response = new AomOrderAuthenticationResponse
//                    {
//                        Order = itm.ToEntity(_organisationService, _userService),
//                        MarketMustBeOpen = true,
//                        Message = new Message
//                        {
//                            MessageAction = MessageAction.Create,
//                            MessageType = MessageType.Order,
//                            ClientSessionInfo = new ClientSessionInfo
//                            {
//                                OrganisationId = TestUserId,
//                                UserId = TestUserId
//                            }
//                        }
//                    };

//                    var realProductService = new Services.ProductService.ProductService(new DbContextFactory(), _realDateTimeProvider);
//                    var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(new DbContextFactory(), _realDateTimeProvider);
//                    var orderService = new Services.OrderService.OrderService(_organisationService,
//                        new Services.DealService.DealService(_userService, _organisationService, _realDateTimeProvider), _userService, _realDateTimeProvider, realProductService,
//                        new Services.MetadataValidator.MetadataValidator(productMetadataService, new IMetadataItemValidator[0]));

//                    Order order = orderService.NewOrder(aomModel, response, MarketStatus.Open);

//                    Assert.That(order.OrderStatus == OrderStatus.Active);
//                    Assert.AreEqual(DateTime.Today.AddDays(1), order.DeliveryStartDate);
//                    Assert.AreEqual(DateTime.Today.AddDays(4), order.DeliveryEndDate);
//                    Assert.IsNotNull(order.MetaData);
//                    Assert.AreEqual(1, order.MetaData.Length);
//                    Assert.AreEqual("AValue", order.MetaData[0].DisplayValue);
//                }
//            }
//        }

//        [Test]
//        public void CreatingOrderWhenMarketClosedIsHeld()
//        {
//            using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
//            {
//                using (var aomModel = new AomModel())
//                {
//                    OrderDto itm;
//                    PriceBasisDto priceBasisDto;
//                    PriceLineDto priceLineDto;

//                    var price = 800;

//                    AomDataBuilder.WithModel(aomModel)
//                        .OpenAllMarkets()
//                        .AddDeliveryLocation("DeliveryLocation1")
//                        .AddProduct("Product1")
//                        .AddTenorCode("P")
//                        .AddProductTenor("Product1_Tenor1")
//                        .AddPriceType("FIXED")
//                        .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
//                        .AddPriceLine(out priceLineDto, TestUserId)
//                        .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
//                        .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "A", itm: out itm, orderStatus: "A", quantity: 20000, price: price, pLinesDto: new List<PriceLineDto>() { priceLineDto })
//                        .DBContext.SaveChangesAndLog(-1);

//                    var userService = new UserService(new DbContextFactory(), _realDateTimeProvider, new Mock<IProductService>().Object);
//                    var organisationService = new OrganisationService(new DbContextFactory(), new BilateralPermissionsBuilder());

//                    var response = new AomOrderAuthenticationResponse
//                    {
//                        Order = itm.ToEntity(organisationService, userService),
//                        MarketMustBeOpen = true,
//                        Message = new Message
//                        {
//                            MessageAction = MessageAction.Create,
//                            MessageType = MessageType.Order,
//                            ClientSessionInfo = new ClientSessionInfo
//                            {
//                                OrganisationId = TestUserId,
//                                UserId = TestUserId
//                            }
//                        }
//                    };

//                    Order order = _orderService.NewOrder(aomModel, response, MarketStatus.Closed);
//                    Assert.That(order.OrderStatus == OrderStatus.Held);
//                }
//            }
//        }

//        //[Test]
//        //public void OrderWithoutOrderPriceLinesIsRejected()
//        //{
//        //    //TODO: Implement when front-end will send all entities populated
//        //}

//        [Test]
//        public void OrderPriceLineWithoutPriceLinesIsRejected()
//        {
//            var productTenorDto = _mockAomModel.ProductTenors.First(x => x.Name == FakeProductTenorName);

//            var orderEntity = _entityHelper.CreateOrderEntity(50, 500, productTenorDto);
//            var orderPriceLineEntity = _entityHelper.CreateOrderPriceLineEntity(orderEntity.Id, DtoMappingAttribute.GetValueFromEnum(orderEntity.OrderStatus), -1);
//            orderEntity.OrderPriceLines = new List<OrderPriceLine>() {orderPriceLineEntity};

//            var newOrderRequest = new AomOrderAuthenticationResponse
//            {
//                Order = orderEntity,
//                MarketMustBeOpen = false,
//                Message = new Message
//                {
//                    MessageAction = MessageAction.Create,
//                    MessageType = MessageType.Order,
//                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                }
//            };

//            var currentFailMessage = "Test: First Null PriceLine test.";
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open),
//                "must have associated PriceLines populated",currentFailMessage);

//            currentFailMessage = "Test: Second Null PriceLine test.";
//            var priceLineEntity = _entityHelper.CreatePriceLineEntity(0);
//            orderEntity.OrderPriceLines[0].PriceLine = priceLineEntity; // as price line basis check is done after price line check, no need to set price line bases
//            var orderPriceLineEntity2 = _entityHelper.CreateOrderPriceLineEntity(orderEntity.Id, DtoMappingAttribute.GetValueFromEnum(orderEntity.OrderStatus), -2);
//            orderEntity.OrderPriceLines.Add(orderPriceLineEntity2);
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open),
//                "must have associated PriceLines populated",currentFailMessage);
//        }

//        [Test]
//        public void PriceLineWithoutPriceLineBasesIsRejected()
//        {
//            var productTenorDto = _mockAomModel.ProductTenors.First(x => x.Name == FakeProductTenorName);

//            var orderEntity = _entityHelper.CreateOrderEntity(50, 500, productTenorDto);
//            var priceLineEntity = _entityHelper.CreatePriceLineEntity(0);
//            var orderPriceLineEntity = _entityHelper.CreateOrderPriceLineEntity(orderEntity.Id, DtoMappingAttribute.GetValueFromEnum(orderEntity.OrderStatus), -1);
//            orderPriceLineEntity.PriceLine = priceLineEntity;
//            orderEntity.OrderPriceLines = new List<OrderPriceLine>() { orderPriceLineEntity };

//            var newOrderRequest = new AomOrderAuthenticationResponse
//            {
//                Order = orderEntity,
//                MarketMustBeOpen = false,
//                Message = new Message
//                {
//                    MessageAction = MessageAction.Create,
//                    MessageType = MessageType.Order,
//                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
//                }
//            };

//            var errorMessageExpectedContain = "must have associated PriceLinesBases populated";

//            var currentFailMessage = "Test: Null PriceLinesBases test.";
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open), errorMessageExpectedContain, currentFailMessage);
            
//            currentFailMessage = "Test: Empty PriceLinesBases test.";
//            newOrderRequest.Order.OrderPriceLines[0].PriceLine.PriceLinesBases = new List<PriceLineBasis>() { };
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open),  errorMessageExpectedContain, currentFailMessage);

//            currentFailMessage = "Test: Second PriceLine with null PriceLinesBases test.";
//            var priceLineBaseEntity = _entityHelper.CreatePriceLineBasis(0, 50, newOrderRequest.Order.Price, 1, productTenorDto.ProductId, "P");
//            newOrderRequest.Order.OrderPriceLines[0].PriceLine.PriceLinesBases = new List<PriceLineBasis>() { priceLineBaseEntity };
//            var secondOrderPriceLine = _entityHelper.CreateOrderPriceLineEntity(orderEntity.Id, DtoMappingAttribute.GetValueFromEnum(orderEntity.OrderStatus), -2);
//            var priceLineEntity2 = _entityHelper.CreatePriceLineEntity(1);
//            secondOrderPriceLine.PriceLine = priceLineEntity2;
//            newOrderRequest.Order.OrderPriceLines.Add(secondOrderPriceLine);
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open), errorMessageExpectedContain, currentFailMessage);

//            currentFailMessage = "Test: Second PriceLine with empty PriceLinesBases test.";
//            newOrderRequest.Order.OrderPriceLines[1].PriceLine.PriceLinesBases = new List<PriceLineBasis>() { };
//            TestHelper.AssertThrowsExceptionMessageContains<InvalidOperationException>(() => _orderService.NewOrder(_mockAomModel, newOrderRequest, MarketStatus.Open), errorMessageExpectedContain, currentFailMessage);
//        }

//        [Test]
//        public void CreatingOrderInsertsCorrectEntitiesInDatabase()
//        {
//            var newOrderRequst = _entityHelper.CreateOrder(50, 500);
//            var result = _orderService.NewOrder(_mockAomModel, newOrderRequst, MarketStatus.Open);

//            Assert.AreEqual(result.OrderPriceLines.Count, newOrderRequst.Order.OrderPriceLines.Count, "Order Price Lines count is not matching");
//            Assert.NotNull(result.OrderPriceLines[0].PriceLine, "First Order Price Line does not have a Price Line");
//            Assert.AreEqual(result.OrderPriceLines[0].PriceLine.PriceLinesBases.Count, newOrderRequst.Order.OrderPriceLines[0].PriceLine.PriceLinesBases.Count, 
//                "Price Line Bases count is not matching for first Order Price Line");
//            Assert.NotNull(result.OrderPriceLines[1].PriceLine, "Second Order Price Line does not have a Price Line");
//            Assert.AreEqual(result.OrderPriceLines[1].PriceLine.PriceLinesBases.Count, newOrderRequst.Order.OrderPriceLines[0].PriceLine.PriceLinesBases.Count,
//                "Price Line Bases count is not matching for second Order Price Line");
//        }

//        [Test]
//        public void OrderPriceLineWithNotMatchingStatusBeforeExecutionIsRejected()
//        {
//            var orderRequest = _entityHelper.CreateOrder();
            
//            //make sure that status is matching
//            orderRequest.Order.OrderStatus = OrderStatus.Active;
//            orderRequest.Order.OrderPriceLines.ForEach(opl => opl.OrderStatus = OrderStatus.Active);

//            var currentTest = "Test: First Order Price Line status is not matching";
//            orderRequest.Order.OrderPriceLines[0].OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Held);
//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderRequest, MarketStatus.Open), "status on order price lines must match", currentTest);

//            //reset first order price line status
//            orderRequest.Order.OrderPriceLines[0].OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(orderRequest.Order.OrderStatus);

//            currentTest = "Test: Second Order Price Line is not matching";
//            orderRequest.Order.OrderPriceLines[0].OrderStatusCode = DtoMappingAttribute.GetValueFromEnum(OrderStatus.Killed);
//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderRequest, MarketStatus.Open),"status on order price lines must match", currentTest);
//        }

//        [Test]
//        public void OrderThatViolatesOrderPriceIncrementIsRejected()
//        {
//            var orderResponse = _entityHelper.CreateOrder(price: 1.01m);
//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "The price is invalid");
//        }

//        [Test]
//        public void OrderWithPriceGreaterThanMaximumPriceIsRejected()
//        {
//            var orderResponse = _entityHelper.CreateOrder(FakeMaxPrice + FakeOrderPriceIncrement);
//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "The price is invalid");
//        }

//        [Test]
//        public void OrderWithPriceLessThanMinimumPriceIsRejected()
//        {
//            var orderResponse = _entityHelper.CreateOrder(FakeMinPrice - FakeOrderPriceIncrement);
//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "The price is invalid");
//        }

//        //[Test]
//        //public void OrderForProductWithInvalidIntervalStringFor_DeliveryDateStart_IsRejectedWithBusinessRuleException()
//        //{
//        //    //AOMK-352
//        //    _mockDateTimeProvider.SetupGet(p => p.Today).Returns(new DateTime(2015, 3, 24, 0, 0, 0, DateTimeKind.Local));

//        //    var orderResponse = _entityHelper.CreateOrder();
//        //    var tenor = _mockAomModel.ProductTenors.FirstOrDefault(t => t.Id == orderResponse.Order.ProductTenor.Id);
//        //    Assert.IsNotNull(tenor);

//        //    tenor.DeliveryDateStart = "+3";

//        //    TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), 
//        //        "ProductTenor delivery start or end interval string is invalid");
//        //}

//        //[Test]
//        //public void OrderForProductWithInvalidIntervalStringFor_DeliveryDateEnd_IsRejectedWithBusinessRuleException()
//        //{
//        //    //AOMK-352
//        //    _mockDateTimeProvider.SetupGet(p => p.Today).Returns(new DateTime(2015, 3, 24, 0, 0, 0, DateTimeKind.Local));

//        //    var orderResponse = _entityHelper.CreateOrder();

//        //    var tenor = _mockAomModel.ProductTenors.FirstOrDefault(t => t.Id == orderResponse.Order.ProductTenor.Id);
//        //    Assert.IsNotNull(tenor);

//        //    tenor.DeliveryDateEnd = "+3";

//        //    TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open),
//        //        "ProductTenor delivery start or end interval string is invalid");
//        //}

//        [Test]
//        public void OrderThatViolatesOrderQuantityIncrementIsRejected()
//        {
//            var orderResponse = _entityHelper.CreateOrder(qty: 501);
//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "The quantity is invalid");
//        }

//        [Test]
//        public void OrderWithQuantityGreaterThanMaximumQuantityIsRejected()
//        {
//            var orderResponse = _entityHelper.CreateOrder(qty: FakeMaxQty + FakeOrderQtyIncrement);
//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open),"The quantity is invalid");
//        }

//        [Test]
//        public void OrderWithQuantityLessThanMinimumQuantityIsRejected()
//        {
//            var orderResponse = _entityHelper.CreateOrder(qty: FakeMinQty - FakeOrderQtyIncrement);
//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open),"The quantity is invalid" );
//        }

//        //[Test]
//        //public void OrderWithDeliveryPeriodLessThanMinimumIsRejected()
//        //{
//        //    _mockDateTimeProvider.SetupGet(p => p.Today).Returns(new DateTime(2015, 3, 24, 0, 0, 0, DateTimeKind.Local));

//        //    var orderResponse = _entityHelper.CreateOrder();

//        //    var tenor = _mockAomModel.ProductTenors.FirstOrDefault(t => t.Id == orderResponse.Order.ProductTenor.Id);
//        //    Assert.IsNotNull(tenor);

//        //    var start = new DateTime(2015, 3, 28, 0, 0, 0, 0, DateTimeKind.Utc);
//        //    var end = start.AddDays(tenor.MinimumDeliveryRange - 2);
//        //    Assert.That(end, Is.GreaterThan(start));

//        //    orderResponse.Order.DeliveryStartDate = start;
//        //    orderResponse.Order.DeliveryEndDate = end;

//        //    TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "delivery period is invalid");
//        //}

//        [Test]
//        public void OrderWithMetaDataContainingInValidMetaDataIdIsRejected()
//        {
//            var orderResponse = _entityHelper.CreateOrder();
//            var metaDataJson = "{\"ProductMetaDataId\": 999,\"DisplayName\": \"DeliveryPort\",\"MetaDataListItemId\": 3,\"DisplayValue\":\"Port C\", \"ItemValue\": 102}";

//            MetaData orderMetaData = metaDataJson.FromJson<MetaData>();
//            orderMetaData.ItemType = MetaDataTypes.String;
//            MetaData[] metaDataList = new MetaData[1] {orderMetaData};

//            (orderResponse.Order).MetaData = metaDataList;

//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "There is no valid Product MetaData");
//        }

//        [Test]
//        public void OrderWithMetaDataContainingInValidMetaDataListItemIdIsRejected()
//        {
//            var metaDataItemDefn = _mockAomModel.ProductMetaDataItems.First();

//            var orderResponse = _entityHelper.CreateOrder();
//            var metaDataJson = "{\"ProductMetaDataId\": @ID,\"DisplayName\": \"DeliveryPort\",\"MetaDataListItemId\": -999,\"DisplayValue\":\"PortC\", \"ItemValue\": 102}";

//            metaDataJson = metaDataJson.Replace("@ID", metaDataItemDefn.Id.ToString());

//            MetaData orderMetaData = metaDataJson.FromJson<MetaData>();
//            orderMetaData.ItemType = MetaDataTypes.IntegerEnum;
//            MetaData[] metaDataList = new MetaData[1] {orderMetaData};

//            (orderResponse.Order).MetaData = metaDataList;

//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "There is no valid MetaData list item");
//        }

//        [Test]
//        public void OrderWithMetaDataContainsValidMetaDataListItemId()
//        {
//            using (TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
//            {
//                using (var aomModel = new AomModel())
//                {
//                    OrderDto itm;

//                    List<MetaDataListItemDto> metaDataList = new List<MetaDataListItemDto>
//                    {
//                        new MetaDataListItemDto() {Id = -100, ValueText = "TestPort", ValueLong = 88}
//                    };

//                    AomDataBuilder.WithModel(aomModel)
//                        .OpenAllMarkets()
//                        .AddDeliveryLocation("DeliveryLocation1")
//                        .AddProduct("Product1")
//                        .AddTenorCode("Tenor1")
//                        .AddProductTenor("Product1_Tenor1")
//                        .AddProductMetaDataList("MetaDataList", 4, metaDataList)
//                        .AddProductMetaDataItem("Product1", "Quantity", 1, MetaDataTypes.IntegerEnum, "MetaDataList")
//                        .AddOrder(TestUserId, "Product1_Tenor1", "A", out itm, "A", 20000, 800)
//                        .DBContext.SaveChangesAndLog(-1);

//                    long productId = aomModel.Products.Single(p => p.Name == "Product1").Id;

//                    var metaDataItemDto = aomModel.ProductMetaDataItems.First(m => m.ProductId == productId);
//                    itm.MetaData = new[]
//                    {
//                        new MetaDataIntegerEnumDto()
//                        {
//                            ProductMetaDataId = metaDataItemDto.Id,
//                            DisplayName = metaDataItemDto.DisplayName,
//                            DisplayValue = metaDataItemDto.MetaDataList.MetaDataListItems.Single(m => m.ValueText == "TestPort").ValueText,
//                            ItemValue = int.Parse(metaDataItemDto.MetaDataList.MetaDataListItems.Single(m => m.ValueText == "TestPort").ValueLong.ToString()),
//                            Id = metaDataItemDto.MetaDataList.MetaDataListItems.Single(m => m.ValueText == "TestPort").Id
//                        }
//                    };

//                    var response = new AomOrderAuthenticationResponse
//                    {
//                        Order = itm.ToEntity(_organisationService, _userService),
//                        MarketMustBeOpen = true,
//                        Message = new Message
//                        {
//                            MessageAction = MessageAction.Create,
//                            MessageType = MessageType.Order,
//                            ClientSessionInfo = new ClientSessionInfo
//                            {
//                                OrganisationId = TestUserId,
//                                UserId = TestUserId
//                            }
//                        }
//                    };

//                    var realProductService = new Services.ProductService.ProductService(new DbContextFactory(), _realDateTimeProvider);
//                    var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(new DbContextFactory(), _realDateTimeProvider);
//                    var orderService = new Services.OrderService.OrderService(_organisationService,new Services.DealService.DealService(_userService, _organisationService, _realDateTimeProvider),
//                        _userService, _realDateTimeProvider, realProductService, new Services.MetadataValidator.MetadataValidator(productMetadataService, new IMetadataItemValidator[0]));

//                    Order newOrder = orderService.NewOrder(aomModel, response, MarketStatus.Open);

//                    Assert.IsTrue(newOrder.MetaData.Length == itm.MetaData.Length);
//                    Assert.IsTrue(newOrder.MetaData[0].MetaDataListItemId == ((MetaDataIntegerEnumDto) itm.MetaData[0]).Id);
//                    Assert.IsTrue(newOrder.MetaData[0].ItemValue == ((MetaDataIntegerEnumDto) itm.MetaData[0]).ItemValue);
//                }
//            }
//        }

//        [Test]
//        public void OrderWithMetaDataStringTooShortIsRejected()
//        {
//            var metaDataItemDefn = _mockAomModel.ProductMetaDataItems.First(m => m.DisplayName == "DeliveryDescription");

//            var orderResponse = _entityHelper.CreateOrder();
//            var metaDataJson = "{\"ProductMetaDataId\": @ID,\"DisplayName\": \"DeliveryDescription\",\"DisplayValue\":\"Test\"}";

//            metaDataJson = metaDataJson.Replace("@ID", metaDataItemDefn.Id.ToString());

//            MetaData orderMetaData = metaDataJson.FromJson<MetaData>();
//            orderMetaData.ItemType = MetaDataTypes.String;
//            MetaData[] metaDataList = new MetaData[1] {orderMetaData};

//            (orderResponse.Order).MetaData = metaDataList;

//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "The Product MetaData item length is less than");
//        }

//        [Test]
//        public void OrderWithMetaDataStringTooLongIsRejected()
//        {
//            var metaDataItemDefn = _mockAomModel.ProductMetaDataItems.First(m => m.DisplayName == "DeliveryDescription");

//            var orderResponse = _entityHelper.CreateOrder();
//            var metaDataJson = "{\"ProductMetaDataId\": @ID,\"DisplayName\": \"DeliveryDescription\",\"DisplayValue\":\"abcdeabcdeabcdeabcdeF\"}";

//            metaDataJson = metaDataJson.Replace("@ID", metaDataItemDefn.Id.ToString());

//            MetaData orderMetaData = metaDataJson.FromJson<MetaData>();
//            orderMetaData.ItemType = MetaDataTypes.String;
//            MetaData[] metaDataList = new MetaData[1] {orderMetaData};

//            (orderResponse.Order).MetaData = metaDataList;

//            TestHelper.AssertThrowsExceptionMessageContains<BusinessRuleException>(() => _orderService.NewOrder(_mockAomModel, orderResponse, MarketStatus.Open), "The Product MetaData item length is greater than");
//        }
//    }
//}