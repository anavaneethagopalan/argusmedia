﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.MetadataValidator;
using AOM.Services.OrderService;
using AOM.Services.ProductService;
using AOM.Services.ProductMetadataService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;

using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.OrderService
{
    //public static class HoldOrderExtensions
    //{
    //    public static void Clear<T>(this IDbSet<T> dbSet) where T : class
    //    {
    //        (dbSet as DbSet).RemoveRange(dbSet);
    //    }
    //}

    [TestFixture]
    public class HoldOrderTests
    {
        private IAomModel _mockAomModel;
        private Mock<IOrganisationService> _mockOrganisationService;
        private Mock<IUserService> _mockUserService;
        private IDateTimeProvider _dateTimeProvider;
        private Mock<IProductService> _mockProductService;
        private Services.OrderService.OrderService _orderService;

        private const int TestUserId = 123;
        private const int TestOrgId = 321;
        private readonly DateTime _testStartDate = DateTime.Today.AddDays(1);
        private readonly DateTime _testEndDate = DateTime.Today.AddDays(4);

        [SetUp]
        public void Setup()
        {
            _mockProductService = new Mock<IProductService>();
            _dateTimeProvider = new DateTimeProvider();
            _mockOrganisationService = new Mock<IOrganisationService>();
            _mockOrganisationService.Setup(x => x.GetOrganisationById(TestOrgId)).Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});
            _mockUserService = new Mock<IUserService>();
            _mockUserService.Setup(x => x.GetUsersOrganisation(TestUserId)).Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});
            
            _orderService = new Services.OrderService.OrderService(_mockOrganisationService.Object, null, _mockUserService.Object, _dateTimeProvider, _mockProductService.Object,
                new Services.MetadataValidator.MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));
        }

        private void BuildMockAomModel()
        {
            AomDataBuilder.id = 0;
            
            if (_mockAomModel != null)
            {
                _mockAomModel.Dispose();
                _mockAomModel = null;
            }

            _mockAomModel = new MockAomModel();
            var mockDb = new MockDbContextFactory(_mockAomModel, null);
            AomDataBuilder.WithModel(mockDb.CreateAomModel())
                .AddOrderStatus("A", "Active")
                .AddOrderStatus("E", "Executed")
                .AddOrderStatus("V", "VoidAfterExecuted")
                .AddCurrency("EUR", "Eur")
                .AddUnit("BBL", "bbl")
                .AddDeliveryLocation("DeliveryLocation1")
                .DBContext.SaveChangesAndLog(-1);
        }

        private OrderDto AddOrderToModel()
        {
            PriceBasisDto priceBasisDto;
            OrderDto testOrderDto;
            PriceLineDto priceLineDto;

            var price = 800;

            AomDataBuilder.WithModel(_mockAomModel)
                .OpenAllMarkets()
                .AddProduct("Product1")
                .AddTenorCode("P")
                .AddProductTenor("Product1_Tenor1", _mockAomModel.Products.First().Id)
                .AddPriceType("FIXED")
                .AddPriceBasis("FIXED", out priceBasisDto, "EUR", "BBL")
                .AddPriceLine(out priceLineDto, TestUserId)
                .AddPriceLineBasis(price, priceLineDto, 1, 1, "P", priceBasisDto.Id)
                .AddOrder(userId: TestUserId, productTenorName: "Product1_Tenor1", bidOrAsk: "B", price: price, pLinesDto: new List<PriceLineDto>() {priceLineDto}, itm: out testOrderDto)
                .DBContext.SaveChangesAndLog(-1);

            return testOrderDto;
        }
        
        [Test]
        public void OrderStatusFromEFShouldNotBeNull()
        {
            using (var context = new AomModel())
            {
                //2. get Hold order status
                var orderStatuses = from orderStatus in context.OrderStatus
                    select orderStatus;

                Assert.IsNotNull(orderStatuses);
            }
        }

        [Test]
        public void OrderStatusCountFromEFShouldNotBeEmpty()
        {
            using (var context = new AomModel())
            {
                //2. get Hold order status
                var orderStatuses = from orderStatus in context.OrderStatus
                    select orderStatus;

                Assert.IsTrue(orderStatuses.Any());
            }
        }

        [Test]
        public void AssertThatOrderStatusesContainsACodeOfLetterH()
        {
            using (var context = new AomModel())
            {
                var orderStatuses = from orderStatus in context.OrderStatus
                    select orderStatus;

                Assert.IsTrue(orderStatuses.Any(x => x.Code == "H"));
            }
        }

        [Test]
        public void AssertThatOrderStatusesContainsOneCodeWithLetterH()
        {
            using (var context = new AomModel())
            {
                var orderStatuses = from orderStatus in context.OrderStatus
                    select orderStatus;

                var heldStatus = orderStatuses.SingleOrDefault(x => x.Code == "H");

                Assert.IsNotNull(heldStatus);
                Assert.IsTrue(heldStatus.Code == "H");
            }
        }

        [Test]
        public void CanHoldOrderEvenIfMarketIsClosed()
        {
            BuildMockAomModel();
            OrderDto testOrderDto = AddOrderToModel();

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Hold,
                    ClientSessionInfo = new ClientSessionInfo { UserId = 1 }
                },
                Order = new Order
                {
                    Quantity = 12500,
                    Price = 123M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Held,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testStartDate,
                    DeliveryEndDate = _testEndDate,
                    PrincipalUserId = TestUserId,
                    LastUpdated = testOrderDto.LastUpdated,
                    BrokerRestriction = BrokerRestriction.Any,
                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
                }
            };
            
            var heldOrder = _orderService.HoldOrder(_mockAomModel, testOrder1, MarketStatus.Closed);
            Assert.AreEqual(OrderStatus.Held, heldOrder.OrderStatus);
        }

        [Test]
        public void CannotHoldOrderIfOrderChangedBySomeoneElse()
        {
            BuildMockAomModel();
            OrderDto testOrderDto = AddOrderToModel();
            testOrderDto.LastUpdated = DateTime.Now;

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Hold,
                    ClientSessionInfo = new ClientSessionInfo {UserId = 1}
                },
                Order = new Order
                {
                    Quantity = 12500,
                    Price = 123.43M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Held,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testStartDate,
                    DeliveryEndDate = _testEndDate,
                    PrincipalUserId = TestUserId,
                    LastUpdated = testOrderDto.LastUpdated.AddMinutes(-1),
                    BrokerRestriction = BrokerRestriction.Any,
                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
                }
            };

            var e = Assert.Throws<BusinessRuleException>(() => _orderService.HoldOrder(_mockAomModel, testOrder1, MarketStatus.Closed));
            StringAssert.Contains(string.Format("The order (Ref. #{0}: Bid 20,000 BBL at 800.00 Eur/bbl) could not be held as TestingOrg updated this order at", testOrderDto.Id), e.Message);
        }

        [Test]
        public void CannotHoldOrderIfOrderReinstatedBySomeoneElse()
        {
            BuildMockAomModel();
            OrderDto testOrderDto = AddOrderToModel();

            testOrderDto.LastUpdated = new DateTime(2000, 1, 1, 11, 00, 00);
            testOrderDto.LastOrderStatusCode = "H";

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Hold,
                    ClientSessionInfo = new ClientSessionInfo {UserId = 1}
                },
                Order = new Order
                {
                    Quantity = 200,
                    Price = 123.4321M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Held,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testStartDate,
                    DeliveryEndDate = _testEndDate,
                    PrincipalUserId = TestUserId,
                    LastUpdated = testOrderDto.LastUpdated.AddMinutes(-1),
                    BrokerRestriction = BrokerRestriction.Any,
                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
                }
            };

            var e = Assert.Throws<BusinessRuleException>(() => _orderService.HoldOrder(_mockAomModel, testOrder1, MarketStatus.Closed));
            StringAssert.Contains(string.Format("The order (Ref. #{0}: Bid 20,000 BBL at 800.00 Eur/bbl) could not be held as TestingOrg reinstated this order at 11:00:00", testOrderDto.Id), e.Message);
        }

        [Test]
        public void CannotHoldOrderIfOrderChangedBySomeoneElseAndUserIsInvalidTest()
        {
            BuildMockAomModel();
            OrderDto testOrderDto = AddOrderToModel();

            testOrderDto.LastUpdated = new DateTime(2000, 1, 1, 11, 00, 00);
            testOrderDto.LastUpdatedUserId = int.MaxValue;

            // Set up changes                
            var testOrder1 = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Message = new Message
                {
                    MessageAction = MessageAction.Hold,
                    ClientSessionInfo = new ClientSessionInfo {UserId = 1}
                },
                Order = new Order
                {
                    Quantity = 12500,
                    Price = 123.43M,
                    Id = testOrderDto.Id,
                    ExecutionMode = ExecutionMode.Internal,
                    OrderStatus = OrderStatus.Held,
                    OrderType = OrderType.Bid,
                    DeliveryStartDate = _testStartDate,
                    DeliveryEndDate = _testEndDate,
                    PrincipalUserId = TestUserId,
                    LastUpdated = testOrderDto.LastUpdated.AddMinutes(-1),
                    BrokerRestriction = BrokerRestriction.Any,
                    ProductTenor = new ProductTenor { Id = testOrderDto.ProductTenorDto.Id },
                    OrderPriceLines = testOrderDto.OrderPriceLines.Select(opl => opl.ToEntity()).ToList()
                }
            };

            var e = Assert.Throws<BusinessRuleException>(() => _orderService.HoldOrder(_mockAomModel, testOrder1, MarketStatus.Closed));
            StringAssert.Contains(string.Format("The order (Ref. #{0}: Bid 20,000 BBL at 800.00 Eur/bbl) could not be held as ? updated this order at 11:00:00", testOrderDto.Id), e.Message);
        }

    }
}