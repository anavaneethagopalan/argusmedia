﻿using AOM.App.Domain.Dates;
using AOM.Services.CrmService;
using AOM.Services.MetadataValidator;
using AOM.Services.ProductMetadataService;
using AOM.Services.ProductService;

namespace AOM.Services.Tests.OrderService
{
    using AOM.App.Domain.Services;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Aom;
    using AOM.Repository.MySql.Tests.Aom;
    using AOM.Services.DealService;
    using AOM.Services.OrderService;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Linq;
    using System.Transactions;
    using MetadataValidator = AOM.Services.MetadataValidator.MetadataValidator;


    [TestFixture]
    public class OrderServiceGetOrderTests
    {
        private Mock<IProductService> _mockProductService;
        private IOrderService _orderService;
        private IUserService _userService;
        private IOrganisationService _organisationService;

        [SetUp]
        public void Setup()
        {
            _mockProductService = new Mock<IProductService>();
            _userService = CreateUserService();
            _organisationService = new OrganisationService(new DbContextFactory(), new BilateralPermissionsBuilder());
            IDealService dealService = null;
            _orderService = new OrderService(_organisationService, dealService, _userService, _dateTimeProvider,
                _mockProductService.Object, new MetadataValidator(Mock.Of<IProductMetadataService>(), new IMetadataItemValidator[0]));
        }

        private DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        [Test]
        public void AssertGetOrderByIdReturnsNonNullForValidOrder()
        {
            var _mockProductService = new Mock<IProductService>();


            using (var transaction = new TransactionScope())
            {
                using (var aomModel = new AomModel())
                {
                    //ARRANGE
                    AomDataBuilder.WithModel(aomModel).OpenAllMarkets().DBContext.SaveChangesAndLog(-1);
                    var builder = AomDataBuilder.WithModel(aomModel);

                    OrderDto itm = new OrderDto
                    {
                        Id = builder.NextId(),
                        Price = 800,
                        InternalOrExternal = "I",
                        BidOrAsk = "A",
                        UserId = 1,
                        OrderStatusCode = "A",
                        Quantity = 20000,
                        LastUpdatedUserId = 1,
                        LastUpdated = _dateTimeProvider.UtcNow,
                        BrokerId = null,
                        BrokerRestriction = "A",
                        ProductTenorId = builder.DBContext.ProductTenors.First().Id,
                    };
                    aomModel.Orders.Add(itm);
                    aomModel.SaveChangesAndLog();

                    var order = _orderService.GetOrderById(aomModel, itm.Id);

                    Assert.That(order, Is.Not.Null);
                }
            }
        }

        [Test]
        public void AssertThatReturnedOrderHasCorrectId()
        {
            using (var transaction = new TransactionScope())
            {
                using (var aomModel = new AomModel())
                {
                    //ARRANGE
                    AomDataBuilder.WithModel(aomModel).OpenAllMarkets().DBContext.SaveChangesAndLog(-1);
                    var builder = AomDataBuilder.WithModel(aomModel);

                    OrderDto itm = new OrderDto
                    {
                        Id = builder.NextId(),
                        Price = 800,
                        InternalOrExternal = "I",
                        BidOrAsk = "A",
                        UserId = 1,
                        OrderStatusCode = "A",
                        Quantity = 20000,
                        LastUpdatedUserId = 1,
                        LastUpdated = _dateTimeProvider.UtcNow,
                        BrokerId = null,
                        BrokerRestriction = "A",
                        ProductTenorId = builder.DBContext.ProductTenors.First().Id,
                    };
                    aomModel.Orders.Add(itm);
                    aomModel.SaveChangesAndLog();

                    var order = _orderService.GetOrderById(aomModel, itm.Id);

                    Assert.AreEqual(order.Id, itm.Id);
                }
            }
        }

        //[Test]
        //public void AssertThatReturnsDeliveryLocationForValidOrderId()
        //{
        //    using (var transaction = new TransactionScope())
        //    {
        //        using (var aomModel = new AomModel())
        //        {
        //            //ARRANGE
        //            AomDataBuilder.WithModel(aomModel).OpenAllMarkets().DBContext.SaveChangesAndLog(-1);
        //            var builder = AomDataBuilder.WithModel(aomModel);

        //            OrderDto itm = new OrderDto
        //            {
        //                Id = builder.NextId(),
        //                Price = 800,
        //                InternalOrExternal = "I",
        //                BidOrAsk = "A",
        //                UserId = 1,
        //                OrderStatusCode = "A",
        //                Quantity = 20000,
        //                LastUpdatedUserId = 1,
        //                LastUpdated = _dateTimeProvider.UtcNow,
        //                BrokerId = null,
        //                BrokerRestriction = "A",
        //                ProductTenorId = builder.DBContext.ProductTenors.First().Id,
        //            };
        //            aomModel.Orders.Add(itm);
        //            aomModel.SaveChangesAndLog();

        //            var order = _orderService.GetOrderById(aomModel, itm.Id);

        //            Assert.IsNotNull(order.Tenor.DeliveryLocation);
        //            Assert.IsNotNull(order.Tenor.DeliveryLocation.Name);
        //        }
        //    }
        //}

        [Test]
        public void AssertThatGetOrdersOnLastUpdatedDateByProductIdReturnsNonNull()
        {
            const int productId = 2;

            using (var aomModel = new AomModel())
            {

                var orders = _orderService.GetOrdersForLastUpdatedDateByProductId(aomModel, DateTime.Today, productId);

                Assert.IsNotNull(orders);
            }
        }

        private IUserService CreateUserService()
        {
            IUserService userService = new UserService(new DbContextFactory(), _dateTimeProvider, new Mock<IProductService>().Object);

            return userService;
        }
    }
}