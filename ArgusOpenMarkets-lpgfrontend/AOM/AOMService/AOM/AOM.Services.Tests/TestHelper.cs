﻿using System;
using NUnit.Framework;

namespace AOM.Services.Tests
{
    public static class TestHelper
    {
        public static T AssertThrowsExceptionMessageContains<T>(TestDelegate code, string expectedContains, string message = "") where T: Exception
        {
            var result = Assert.Throws<T>(code, message);

            Assert.IsTrue(result.Message.Contains(expectedContains),
                String.Format("{2}\r\nExpect error message to contain '{0}' but it was: '{1}'", expectedContains, result.Message, message));

            return result;
        }
    }
}
