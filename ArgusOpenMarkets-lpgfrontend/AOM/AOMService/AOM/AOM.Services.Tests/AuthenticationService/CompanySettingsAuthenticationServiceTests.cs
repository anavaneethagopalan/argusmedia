﻿using AOM.App.Domain.Services;
using AOM.Services.AuthenticationService;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    internal class CompanySettingsAuthenticationServiceTests
    {
        [Test]
        public void UpdateCompanyPermissionsShouldReturnTheCorrectPermissions()
        {
            var mockUserService = new Mock<IUserService>();
            var compAuthService = new CompanySettingsAuthenticationService(mockUserService.Object);

            var permissionsUpdateCompany = compAuthService.PermissionsToUpdateCompanySettings;

            Assert.That(permissionsUpdateCompany.Length, Is.EqualTo(1));
            Assert.That(permissionsUpdateCompany[0], Is.EqualTo(SystemPrivileges.CompanySettings.Amend.Own));
        }
    }
}