﻿using AOM.App.Domain.Dates;
using System.Transactions;

using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.ProductService;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    public class SystemPrivilegeTests
    {
        private MockCrmModel _model;

        private Services.CrmService.UserService _userService;
        DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        [SetUp]
        public void Setup()
        {
            _model = new MockCrmModel();
            _userService = new Services.CrmService.UserService(new MockDbContextFactory(null, _model), _dateTimeProvider, new Mock<IProductService>().Object);
        }

        [Test]
        public void AssertThatUserHasMultipleSystemPrivilege()
        {
            using (new TransactionScope())
            {
                OrganisationDto organisation;
                UserInfoDto usr;
                SystemPrivilegeDto sp;
                CrmDataBuilder.WithModel(_model)
                    .AddOrganisation("testOrg", out organisation)
                    .AddOrganisationRole(organisation.Id, "role1")
                    .AddSystemPrivilege("admin", "A", out sp)
                    .AddSystemPrivilege("admin2", "A", out sp)
                    .AddSystemPrivilegeToRole("admin", "role1")
                    .AddSystemPrivilegeToRole("admin2", "role1")
                    .AddUser("testUser", false, out usr)
                    .AddUserOrganisationRole(usr, organisation, "role1");
                var systemPrivs = _userService.GetSystemPrivileges(usr.Id);
                Assert.That(systemPrivs.Privileges.Contains("admin"));
                Assert.That(systemPrivs.Privileges.Contains("admin2"));
            }

        }

        [TearDown]
        public void TearDown()
        {
            _model.Dispose();
        }
    }
}