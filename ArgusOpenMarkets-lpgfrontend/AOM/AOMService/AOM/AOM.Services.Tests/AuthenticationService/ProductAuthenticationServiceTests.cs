﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    internal class ProductAuthenticationServiceTests
    {
        private Mock<IUserService> _mockUserService;

        private ProductAuthenticationService _productAuthenticationService;

        [SetUp]
        public void Setup()
        {
            var mockErrorService = new Mock<IErrorService>();
            _mockUserService = new Mock<IUserService>();
            _productAuthenticationService = new ProductAuthenticationService(_mockUserService.Object,
                mockErrorService.Object);
        }

        [Test]
        [TestCase("ConfigurePurge")]
        [TestCase("Purge")]
        [TestCase("Open")]
        [TestCase("Close")]
        [TestCase("Update")]
        public void ShouldReturnAuthorisedAndNotCallGetUserCredentialsWhenAuthorisingProcessorUserForConfigurePurge(
            string operation)
        {
            var messageAction = (MessageAction) Enum.Parse(typeof(MessageAction), operation);
            var csi = new ClientSessionInfo {OrganisationId = 1, UserId = -1, SessionId = ""};
            var aomProductRequest = new AomProductRequest
            {
                ClientSessionInfo = csi,
                MessageAction = messageAction,
                Product = new Product {ProductId = 100}
            };
            var mockErrorService = new Mock<IErrorService>();
            _productAuthenticationService = new ProductAuthenticationService(_mockUserService.Object,
                mockErrorService.Object);
            var result = _productAuthenticationService.AuthenticateProductRequest(aomProductRequest);

            Assert.That(result.MessageType == MessageType.Product);
            _mockUserService.Verify(m => m.GetUserWithPrivileges(It.IsAny<long>()), Times.Exactly(0));
        }

        [Test]
        [TestCase("ConfigurePurge")]
        [TestCase("Purge")]
        [TestCase("Open")]
        [TestCase("Close")]
        [TestCase("Update")]
        public void ShouldReturnNotAuthorisedAndCallGetUserCredentialsWhenAuthorisingNormalUserWithoutPrivileges(
            string operation)
        {
            var messageAction = (MessageAction) Enum.Parse(typeof(MessageAction), operation);
            var csi = new ClientSessionInfo {OrganisationId = 1, UserId = 100, SessionId = ""};
            var aomProductRequest = new AomProductRequest
            {
                ClientSessionInfo = csi,
                MessageAction = messageAction,
                Product = new Product {ProductId = 100}
            };
            var mockErrorService = new Mock<IErrorService>();
            var user = new User {Id = 100, ProductPrivileges = new List<UserProductPrivilege>()};
            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(user);
            _productAuthenticationService = new ProductAuthenticationService(_mockUserService.Object,
                mockErrorService.Object);
            var result = _productAuthenticationService.AuthenticateProductRequest(aomProductRequest);

            Assert.That(result.MessageType == MessageType.Error);
            _mockUserService.Verify(m => m.GetUserWithPrivileges(It.IsAny<long>()), Times.Exactly(1));
        }
    }
}