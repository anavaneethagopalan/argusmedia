﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.AuthenticationService;
using AOM.Transport.Events;
using AOM.Transport.Events.CounterpartyPermissions;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    public class CounterpartyPermissionAuthenticationServiceMessageValidityTests
    {
        private CounterpartyPermissionAuthenticationService _counterpartyPermissionsAuthenticationService;

        [SetUp]
        public void Setup()
        {
            var mockUserService = new Mock<IUserService>();

            var principal1User = MakeUser(1, 1, false);
            var principal2User = MakeUser(2, 2, false);
            var principal3User = MakeUser(3, 1, true);

            mockUserService.Setup(x => x.GetUserWithPrivileges(1)).Returns(principal1User);
            mockUserService.Setup(x => x.GetUserWithPrivileges(2)).Returns(principal2User);
            mockUserService.Setup(x => x.GetUserWithPrivileges(3)).Returns(principal3User);
            _counterpartyPermissionsAuthenticationService =
                new CounterpartyPermissionAuthenticationService(mockUserService.Object);
        }

        private User MakeUser(long userId, long organisationId, bool blankPrivs)
        {
            List<string> privs = new List<string>();
            if (!blankPrivs)
            {
                privs = new List<string>
                {
                    Services.AuthenticationService.SystemPrivileges.CounterpartyPermissions.Amend.Own
                };
            }

            return new User
            {
                Id = userId,
                OrganisationId = organisationId,
                UserOrganisation = new Organisation {Id = 1},
                IsActive = true,
                SystemPrivileges = new App.Domain.Entities.SystemPrivileges
                {
                    Privileges = privs
                }
            };
        }

        [Test]
        public void ChangeCounterpartyPermissionValidMessage()
        {
            var request = MakeRequest(1, 1);
            var result = _counterpartyPermissionsAuthenticationService.AuthenticateCounterpartyPermissionRequest(request);
            Assert.True(result.Allow);
        }

        [Test]
        public void ChangeCounterpartyPermissionValidMessage_NotPermissioned()
        {
            var request = MakeRequest(1, 3);
            var result = _counterpartyPermissionsAuthenticationService.AuthenticateCounterpartyPermissionRequest(request);
            Assert.False(result.Allow);
            Assert.AreEqual("You are not permissioned to edit CounterpartyPermissions", result.DenyReason);
        }

        [Test]
        public void ChangeCounterpartyPermissionInvalidMessage()
        {
            var request = MakeRequest(2, 2);
            var result = _counterpartyPermissionsAuthenticationService.AuthenticateCounterpartyPermissionRequest(request);
            Assert.False(result.Allow);
        }

        private AomCounterpartyPermissionAuthenticationRequest MakeRequest(long organisationId, long userId)
        {
            var permissions = new List<MatrixPermission> { MakeMatrixPermissions() };

            var request = new AomCounterpartyPermissionAuthenticationRequest
            {
                CounterpartyPermissions = permissions,
                ClientSessionInfo = new ClientSessionInfo { OrganisationId = organisationId, SessionId = "sessionId", UserId = userId },
                MessageAction = MessageAction.Update
            };

            return request;
        }

        private MatrixPermission MakeMatrixPermissions()
        {
            return new MatrixPermission
            {
                AllowOrDeny = Permission.Allow,
                BuyOrSell = BuyOrSell.Buy,
                Id = 12,
                LastUpdated = DateTime.Now,
                LastUpdatedUserId = 1,
                OurOrganisation = 1,
                ProductId = 2,
                TheirOrganisation = 2
            };
        }
    }
}