﻿using System.Linq;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.AuthenticationService;
using AOM.Services.CrmService;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    public class OrderAuthenticationServiceCanTradeWithTests
    {
        private ICrmModel _model;
        private OrganisationDto t1, t3aggressor;
        private OrganisationDto b2, b4aggressor;

        private CommonOrderRequestValidator _validator;
        private Mock<INeededOrderPrivilegesProvider> _mockNeededOrderPrivileges;

        private readonly long? NoBroker = null;

        [SetUp]
        public void Setup()
        {
            var factory = new MockDbContextFactory(null, new MockCrmModel());
            _model = factory.CreateCrmModel();

            _mockNeededOrderPrivileges = new Mock<INeededOrderPrivilegesProvider>();

            _validator =
                new CommonOrderRequestValidator(new OrganisationService(factory, new BilateralPermissionsBuilder()),
                    _mockNeededOrderPrivileges.Object);

            UserInfoDto user1;
            CrmDataBuilder.WithModel(_model)
                .AddOrganisation("TradeOrg#1", out t1, OrganisationType.Trading)
                .AddOrganisation("Broker#2", out b2, OrganisationType.Brokerage)
                .AddOrganisation("TradeOrg#3aggressor", out t3aggressor, OrganisationType.Trading)
                .AddOrganisation("Broker#4aggressor", out b4aggressor, OrganisationType.Brokerage)
                .AddUser("TestUser", out user1)

                // t1 sells => t3, both sides so allow.
                .AddCounterpartyPermission(t1, t3aggressor, BuyOrSell.Sell, 1, Permission.Allow)
                .AddCounterpartyPermission(t3aggressor, t1, BuyOrSell.Buy, 1, Permission.Allow)

                // t1 buys through B2, both sides so allow.
                .AddBrokerPermission(t3aggressor, b2, BuyOrSell.Buy, 1, Permission.Allow)
                .AddBrokerPermission(b2, t3aggressor, BuyOrSell.Buy, 1, Permission.Allow)

                // t1 sells through B2, both sides so allow.
                .AddBrokerPermission(t1, b2, BuyOrSell.Sell, 1, Permission.Allow)
                .AddBrokerPermission(b2, t1, BuyOrSell.Sell, 1, Permission.Allow)

                // t3 sells through B4, both sides so allow.
                .AddBrokerPermission(t3aggressor, b4aggressor, BuyOrSell.Buy, 1, Permission.Allow)
                .AddBrokerPermission(b4aggressor, t3aggressor, BuyOrSell.Buy, 1, Permission.Allow)

                // t1 sells through B4, both sides so allow.
                .AddBrokerPermission(t1, b4aggressor, BuyOrSell.Sell, 1, Permission.Allow)
                .AddBrokerPermission(b4aggressor, t1, BuyOrSell.Sell, 1, Permission.Allow);
        }

        [Test]
        public void BrokerAgressesPrincpalOrder_OrderIsValidThroughAgressingBrokerDirectWithPermissions()
        {
            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, NoBroker, AggressorDetails(t3aggressor.Id, b4aggressor.Id)),
                    out errorMsg);
            Assert.True(result, "Failed because " + errorMsg);
        }

        [Test()]
        public void BrokerAgressesPrincpalOrder_OrderIsNotValidThroughAgressingBrokerDirectWithouPermissions_Case1()
        {
            RemoveBrokerRights(b4aggressor, BuyOrSell.Buy, t3aggressor);

            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, NoBroker, AggressorDetails(t3aggressor.Id, b4aggressor.Id)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for Broker#4aggressor to buy through TradeOrg#3aggressor for this product",
                errorMsg);
        }

        [Test]
        public void BrokerAgressesPrincpalOrder_OrderIsValidThroughAgressingBrokerDirectWithouPermissions_Case2()
        {
            RemoveBrokerRights(b4aggressor, BuyOrSell.Sell, t1);

            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, NoBroker, AggressorDetails(t3aggressor.Id, b4aggressor.Id)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for TradeOrg#1 to sell through Broker#4aggressor for this product",
                errorMsg);
        }

        [Test]
        public void BrokerAgressesPrincpalOrder_OrderIsValidThroughAgressingBrokerDirectWithouPermissions_Case3()
        {
            RemoveCounterpartyRights(t1, BuyOrSell.Sell, t3aggressor);

            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, NoBroker, AggressorDetails(t3aggressor.Id, b4aggressor.Id)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for TradeOrg#1 to sell to TradeOrg#3aggressor for this product",
                errorMsg);
        }

        [Test]
        public void PrincipalAgressesBrokeredOrder_OrderIsValidWithAllPermissions()
        {
            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, b2.Id, AggressorDetails(t3aggressor.Id, NoBroker)),
                    out errorMsg);
            Assert.True(result, "Failed because " + errorMsg);
        }

        [Test]
        public void PrincipalAgressesOrder_OrderNotValid_WhenExplicitDenyPermissionIsSet()
        {
            // t1 =Sells=>t3: t3 denies buy
            string errorMsg;

            RemoveCounterpartyRights(t3aggressor, BuyOrSell.Buy, t1);

            CrmDataBuilder.WithModel(_model)
                .AddCounterpartyPermission(t3aggressor, t1, BuyOrSell.Buy, 1, Permission.Deny);

            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, NoBroker, AggressorDetails(t3aggressor.Id, NoBroker)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for TradeOrg#1 to sell to TradeOrg#3aggressor for this product",
                errorMsg);
        }

        [TestCase(BuyOrSell.Buy)]
        [TestCase(BuyOrSell.Sell)]
        public void PrincipalAgressesBrokeredOrder_OrderIsNotValid_Case1(BuyOrSell permToRemove)
        {
            if (permToRemove == BuyOrSell.Buy)
            {
                RemoveCounterpartyRights(t3aggressor, BuyOrSell.Buy, t1);
            }
            else
            {
                RemoveCounterpartyRights(t1, BuyOrSell.Sell, t3aggressor);
            }

            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, b2.Id, AggressorDetails(t3aggressor.Id, NoBroker)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for TradeOrg#1 to sell to TradeOrg#3aggressor for this product",
                errorMsg);
        }

        [Test]
        public void PrincipalAgressesBrokeredOrder_OrderIsNotValid_Case2()
        {
            //"(t1, b2) ->Sells-> (t3)"
            // t1=>t3: ok
            // t1=>b2: not_ok                                  
            // b2=>t3: ok             

            RemoveBrokerRights(t1, BuyOrSell.Sell, b2);

            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, b2.Id, AggressorDetails(t3aggressor.Id, NoBroker)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for TradeOrg#1 to sell through Broker#2 for this product",
                errorMsg);
        }

        [Test]
        public void PrincipalAgressesBrokeredOrder_OrderIsNotValid_Case3()
        {
            //"(t1, b2) ->Sells-> (t3)"
            // t1=>t3: ok
            // t1=>b2: ok                                  
            // b2=>t3: not_ok             

            RemoveBrokerRights(t3aggressor, BuyOrSell.Buy, b2);

            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, b2.Id, AggressorDetails(t3aggressor.Id, NoBroker)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for TradeOrg#3aggressor to buy through Broker#2 for this product",
                errorMsg);
        }

        [Test]
        public void CoBrokeredOrderIsValidThroughAgressingBrokerDirectWithPermissions()
        {
            //"(t1, b2) ->Sells-> (t3, b4)"
            // t1=>t3: ok
            // t1=>b2: ok
            // t1=>b4: ok            
            // t3=>t4: ok
            // t3=>t2: ok
            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, b2.Id, AggressorDetails(t3aggressor.Id, b4aggressor.Id)),
                    out errorMsg);
            Assert.True(result, "Failed because " + errorMsg);
        }

        [Test]
        public void
            CoBrokeredOrderIsNotValidThroughAgressingBrokerDirectWithPermissionsWhenAggressingBrokerCantTradeWithOrganisation
            ()
        {
            //"(t1, b2) ->Sells-> (t3, b4)"
            // t1=>t3: ok
            // t1=>b2: ok
            // t1=>b4: not_ok            
            // t3=>t4: ok
            // t3=>b2: ok

            RemoveBrokerRights(b4aggressor, BuyOrSell.Sell, t1);

            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, b2.Id, AggressorDetails(t3aggressor.Id, b4aggressor.Id)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for TradeOrg#1 to sell through Broker#4aggressor for this product",
                errorMsg);
        }

        [Test]
        public void
            CoBrokeredOrderIsNotValidThroughAgressingBrokerDirectWithPermissionsWhenAggressingOrgansisationCantTradeWithOrganisation
            ()
        {
            //"(t1, b2) ->Sells-> (t3, b4)"
            // t1=>t3: ok
            // t1=>b2: ok
            // t1=>b4: ok            
            // t3=>t4: ok
            // t3=>b2: not_ok

            RemoveBrokerRights(t3aggressor, BuyOrSell.Buy, b2);

            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, b2.Id, AggressorDetails(t3aggressor.Id, b4aggressor.Id)),
                    out errorMsg);
            Assert.False(result);
            Assert.AreEqual(
                "There is no bilateral agreement in place for TradeOrg#3aggressor to buy through Broker#2 for this product",
                errorMsg);
        }

        [Test]
        public void PrincipalAgressesOrder_OrderIsValidWithPermissions()
        {
            // t1=>t3: ok
            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(
                    CreateOrder(1, BuyOrSell.Sell, t1.Id, NoBroker, AggressorDetails(t3aggressor.Id, NoBroker)),
                    out errorMsg);
            Assert.True(result, "Failed because ", errorMsg);
        }

        [Test]
        public void BrokeredOrderIsValid_NoAgressors()
        {
            // t1=>b2: ok, no counterparty (i.e. when a broker is specified in an order, but not yet executed).
            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(CreateOrder(1, BuyOrSell.Sell, t1.Id, b2.Id, null),
                    out errorMsg);
            Assert.True(result, "Failed because ", errorMsg);
        }

        [Test]
        public void PrincipalOrderIsValid_NoAgressors()
        {
            // t1: ok
            string errorMsg;
            var result =
                _validator.CheckCounterpartyAndBrokerPermissions(CreateOrder(1, BuyOrSell.Sell, t1.Id, NoBroker, null),
                    out errorMsg);
            Assert.True(result, "Failed because ", errorMsg);
        }

        private Order CreateOrder(long productId, BuyOrSell buyOrSell, long cp1OrganisationId,
            long? brokerOrganisationId, ExecutionInfo aggressingDetail)
        {
            var order = new Order
            {
                ProductId = productId,
                PrincipalOrganisationId = cp1OrganisationId,
                BrokerOrganisationId = brokerOrganisationId,
                ExecutionInfo = aggressingDetail,
                OrderType = buyOrSell == BuyOrSell.Buy ? OrderType.Bid : OrderType.Ask
            };
            return order;
        }

        ExecutionInfo AggressorDetails(long? cpty, long? broker)
        {
            return cpty != null
                ? new ExecutionInfo {PrincipalOrganisationId = cpty.Value, BrokerOrganisationId = broker}
                : null;
        }

        public void RemoveBrokerRights(OrganisationDto ourOrg, BuyOrSell bs, OrganisationDto theirOrg)
        {
            var bsAsString = bs.ToDtoFormat();
            var list =
                _model.BrokerPermissions.Where(
                    p =>
                        p.ProductId == 1 && p.BuyOrSell == bsAsString &&
                        (p.TheirOrganisation == theirOrg.Id && p.OurOrganisation == ourOrg.Id)).ToList();
            Assert.IsTrue(list.Count > 0, "# internal test error - look closely at test setup #");
            list.ForEach(p => _model.BrokerPermissions.Remove(p));
        }

        public void RemoveCounterpartyRights(OrganisationDto ourOrg, BuyOrSell bs, OrganisationDto theirOrg)
        {
            var bsAsString = bs.ToDtoFormat();
            var list =
                _model.CounterpartyPermissions.Where(
                    p =>
                        p.ProductId == 1 && p.BuyOrSell == bsAsString &&
                        (p.TheirOrganisation == theirOrg.Id && p.OurOrganisation == ourOrg.Id)).ToList();
            Assert.IsTrue(list.Count > 0, "# internal test error - look closely at test setup #");
            list.ForEach(p => _model.CounterpartyPermissions.Remove(p));
        }
    }
}