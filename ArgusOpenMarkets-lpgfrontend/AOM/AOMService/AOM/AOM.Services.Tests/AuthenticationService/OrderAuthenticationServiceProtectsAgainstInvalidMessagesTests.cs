using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Services.AuthenticationService;
using AOM.Services.OrderService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture(MessageAction.Create)]
    [TestFixture(MessageAction.Hold)]
    [TestFixture(MessageAction.Update)]
    [TestFixture(MessageAction.Reinstate)]
    [TestFixture(MessageAction.Kill)]
    [TestFixture(MessageAction.Execute)]
    public class OrderAuthenticationServiceProtectsAgainstInvalidMessagesTests
    {
        private IOrderAuthenticationService _orderAuthenticationService;
        private Mock<IUserService> _mockUserService;
        private Mock<IOrganisationService> _mockOrgService;
        private readonly MessageAction _action;
        private List<IValidator<AomOrderAuthenticationRequest>> _validators;
        private Mock<IOrderService> _mockOrderService;
        private NeededOrderPrivilegesProvider _neededOrderPrivilegesProvider;

        public OrderAuthenticationServiceProtectsAgainstInvalidMessagesTests(MessageAction action)
        {
            _action = action;
        }


        [SetUp]
        public void Setup()
        {
            var principal1User = new User
            {
                Id = 10,
                OrganisationId = 100,
                UserOrganisation = new Organisation {Id = 100},
                IsActive = true,
                ProductPrivileges =
                    new List<UserProductPrivilege>
                    {
                        new UserProductPrivilege
                        {
                            Privileges = new Dictionary<string, bool>
                            {
                                {ProductPrivileges.Orders.Create.AsPrincipal, true},
                                {ProductPrivileges.Orders.Amend.OwnOrg, true},
                                {ProductPrivileges.Orders.Amend.Own, true},
                                {ProductPrivileges.Orders.Amend.KillOwn, true},
                                {ProductPrivileges.Orders.Aggress.AsPrincipal, true}
                            },
                            ProductId = 1,
                        }
                    }
            };
            var principal11User_NoOwnOrgRights = new User
            {
                Id = 11,
                Name = "A user who can only update their own orders",
                OrganisationId = 100,
                UserOrganisation = new Organisation {Id = 100},
                IsActive = true,
                ProductPrivileges =
                    new List<UserProductPrivilege>
                    {
                        new UserProductPrivilege
                        {
                            Privileges = new Dictionary<string, bool>
                            {
                                {ProductPrivileges.Orders.Create.AsPrincipal, true},
                                //No Rights for {ProductPrivileges.Orders.Amend.OwnOrg, true},                                 
                                {ProductPrivileges.Orders.Amend.Own, true},
                                {ProductPrivileges.Orders.Amend.KillOwn, true},
                                {ProductPrivileges.Orders.Aggress.AsPrincipal, true}
                            },
                            ProductId = 1,
                        }
                    }
            };
            var principal12User = new User
            {
                Id = 12,
                Name = "A user who can  update orders for other users within org",
                OrganisationId = 100,
                UserOrganisation = new Organisation {Id = 100},
                IsActive = true,
                ProductPrivileges =
                    new List<UserProductPrivilege>
                    {
                        new UserProductPrivilege
                        {
                            Privileges = new Dictionary<string, bool>
                            {
                                {ProductPrivileges.Orders.Create.AsPrincipal, true},
                                {ProductPrivileges.Orders.Amend.OwnOrg, true},
                                {ProductPrivileges.Orders.Amend.Own, true},
                                {ProductPrivileges.Orders.Amend.KillOwn, true},
                                {ProductPrivileges.Orders.Aggress.AsPrincipal, true}
                            },
                            ProductId = 1,
                        }
                    }
            };

            var principal2User = new User
            {
                Id = 20,
                OrganisationId = 200,
                UserOrganisation = new Organisation {Id = 200},
                IsActive = true,
                ProductPrivileges =
                    new List<UserProductPrivilege>
                    {
                        new UserProductPrivilege
                        {
                            Privileges = new Dictionary<string, bool>
                            {
                                {ProductPrivileges.Orders.Create.AsPrincipal, true},
                                {ProductPrivileges.Orders.Amend.Own, true},
                                {ProductPrivileges.Orders.Amend.OwnOrg, true},
                                {ProductPrivileges.Orders.Amend.KillOwn, true}
                            },
                            ProductId = 1,
                        }
                    }
            };

            var broker1User = new User
            {
                Id = 30,
                OrganisationId = 300,
                UserOrganisation = new Organisation {Id = 300},
                IsActive = true,
                ProductPrivileges =
                    new List<UserProductPrivilege>
                    {
                        new UserProductPrivilege
                        {
                            Privileges = new Dictionary<string, bool>
                            {
                                {ProductPrivileges.Orders.Create.AsBroker, true},
                                {ProductPrivileges.Orders.Amend.OwnOrg, true},
                                {ProductPrivileges.Orders.Amend.Own, true},
                                {ProductPrivileges.Orders.Amend.KillOwn, true},
                                {ProductPrivileges.Orders.Aggress.AsBroker, true}
                            },
                            ProductId = 1,
                        }
                    }
            };
            var broker2User = new User
            {
                Id = 40,
                OrganisationId = 400,
                UserOrganisation = new Organisation {Id = 400},
                IsActive = true,
                ProductPrivileges =
                    new List<UserProductPrivilege>
                    {
                        new UserProductPrivilege
                        {
                            Privileges = new Dictionary<string, bool>
                            {
                                {ProductPrivileges.Orders.Create.AsBroker, true},
                                {ProductPrivileges.Orders.Amend.OwnOrg, true},
                                {ProductPrivileges.Orders.Amend.Own, true},
                                {ProductPrivileges.Orders.Amend.KillOwn, true}
                            },
                            ProductId = 1,
                        }
                    }
            };

            _mockUserService = new Mock<IUserService>();
            _mockUserService.Setup(x => x.GetUserWithPrivileges(principal1User.Id)).Returns(principal1User);
            _mockUserService.Setup(x => x.GetUserWithPrivileges(principal11User_NoOwnOrgRights.Id))
                .Returns(principal11User_NoOwnOrgRights);
            _mockUserService.Setup(x => x.GetUserWithPrivileges(principal12User.Id)).Returns(principal12User);
            _mockUserService.Setup(x => x.GetUserWithPrivileges(principal2User.Id)).Returns(principal2User);
            _mockUserService.Setup(x => x.GetUserWithPrivileges(broker1User.Id)).Returns(broker1User);
            _mockUserService.Setup(x => x.GetUserWithPrivileges(broker2User.Id)).Returns(broker2User);

            _mockUserService.Setup(x => x.GetUsersOrganisation(principal1User.Id))
                .Returns(new Organisation {Id = principal1User.OrganisationId});
            _mockUserService.Setup(x => x.GetUsersOrganisation(principal11User_NoOwnOrgRights.Id))
                .Returns(new Organisation {Id = principal11User_NoOwnOrgRights.OrganisationId});
            _mockUserService.Setup(x => x.GetUsersOrganisation(principal12User.Id))
                .Returns(new Organisation {Id = principal12User.OrganisationId});

            _mockUserService.Setup(x => x.GetUsersOrganisation(principal2User.Id))
                .Returns(new Organisation {Id = principal2User.OrganisationId});

            _mockUserService.Setup(x => x.GetUsersOrganisation(broker1User.Id))
                .Returns(new Organisation {Id = broker1User.OrganisationId});
            _mockUserService.Setup(x => x.GetUsersOrganisation(broker2User.Id))
                .Returns(new Organisation {Id = broker2User.OrganisationId});

            _mockOrgService = new Mock<IOrganisationService>();
            _mockOrgService.Setup(
                    x =>
                        x.CanCounterpartyTradeWithBroker(It.IsAny<long>(), It.IsAny<BuyOrSell>(), It.IsAny<long>(),
                            It.IsAny<long>()))
                .Returns(true);

            _mockOrderService = new Mock<IOrderService>();
            _mockOrderService.Setup(x => x.GetOrderById(It.IsAny<IAomModel>(), It.Is<long>(u => u != 90)))
                .Returns(new Order {PrincipalOrganisationId = 100});
            _mockOrderService.Setup(x => x.GetOrderById(It.IsAny<IAomModel>(), It.Is<long>(u => u == 90)))
                .Returns(new Order {PrincipalOrganisationId = 300});

            _neededOrderPrivilegesProvider = new NeededOrderPrivilegesProvider(new NeededOrderPrivilegesFactory());

            _validators = new List<IValidator<AomOrderAuthenticationRequest>>
            {
                new PrincipalOrderRequestValidator(_mockOrderService.Object, new MockDbContextFactory(null, null)),
                new BrokerOrderRequestValidator(_mockUserService.Object, _mockOrgService.Object,
                    _mockOrderService.Object, new MockDbContextFactory(null, null)),
                new CommonOrderRequestValidator(_mockOrgService.Object, _neededOrderPrivilegesProvider)
            };
        }


        [Test]
        public void AuthenticateNewOrderRequestValidMessage_Principal()
        {
            _orderAuthenticationService = new OrderAuthenticationService(_mockUserService.Object,
                _neededOrderPrivilegesProvider,
                _validators);


            var orderRequest = new AomOrderAuthenticationRequest
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = 100,
                    SessionId = "SessionToken",
                    UserId = 10
                },
                MessageAction = _action,
                Order = MakeOrder(100,10)
            };
            CreateValidExecutionInfoOnOrder(orderRequest);

            var result = _orderAuthenticationService.AuthenticateOrderRequest(orderRequest);
            Assert.True(result.Allow);
        }

        [Test]
        public void AuthenticateExistingOrderRequest_InValidMessage_OtherPrincipalSameOrg_NoRights()
        {
            _orderAuthenticationService = new OrderAuthenticationService(_mockUserService.Object,
                _neededOrderPrivilegesProvider,
                _validators);
            if (_action == MessageAction.Create || _action == MessageAction.Execute)
            {
                Assert.Ignore("Ingore test - as test is for changing state of existing order");
            }
            var orderRequest = new AomOrderAuthenticationRequest
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = 100,
                    SessionId = "SessionToken",
                    UserId = 11
                },
                MessageAction = _action,
                Order = MakeOrder(100,10)
            };

            _mockOrderService.Setup(x => x.GetOrderById(It.IsAny<IAomModel>(), It.Is<long>(u => u == -666)))
                .Returns(orderRequest.Order);

            CreateValidExecutionInfoOnOrder(orderRequest);

            Assert.AreNotEqual(orderRequest.Order.PrincipalUserId, orderRequest.ClientSessionInfo.UserId,
                "setup error - user must be different to order creator");
            Assert.AreNotEqual(_mockUserService.Object.GetUserWithPrivileges(orderRequest.Order.PrincipalUserId.Value),
                _mockUserService.Object.GetUserWithPrivileges(orderRequest.ClientSessionInfo.UserId),
                "Setup error - the two users must be in same org");

            var result = _orderAuthenticationService.AuthenticateOrderRequest(orderRequest);
            Assert.False(result.Allow);
        }

        [Test]
        public void AuthenticateExistingOrderRequest_ValidMessage_OtherPrincipalSameOrg_WithRights()
        {
            _orderAuthenticationService = new OrderAuthenticationService(_mockUserService.Object,
                _neededOrderPrivilegesProvider,
                _validators);
            if (_action == MessageAction.Create || _action == MessageAction.Execute)
            {
                Assert.Ignore("Ingore test - as test is for changing state of existing order");
            }
            var orderRequest = new AomOrderAuthenticationRequest
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = 100,
                    SessionId = "SessionToken",
                    UserId = 12
                },
                MessageAction = _action,
                Order = MakeOrder(100,10)
            };

            //Setup the order so it appears to come from DB
            //_mockOrderService.Setup(x => x.GetOrderById(It.IsAny<IAomModel>(), It.Is<long>(u => u == -666))).Returns(orderRequest.Order);

            CreateValidExecutionInfoOnOrder(orderRequest);

            Assert.AreNotEqual(orderRequest.Order.PrincipalUserId, orderRequest.ClientSessionInfo.UserId,
                "setup error - user must be different order creator");
            Assert.AreNotEqual(_mockUserService.Object.GetUserWithPrivileges(orderRequest.Order.PrincipalUserId.Value),
                _mockUserService.Object.GetUserWithPrivileges(orderRequest.ClientSessionInfo.UserId),
                "Setup error - the two users must be in same org");
            var result = _orderAuthenticationService.AuthenticateOrderRequest(orderRequest);
            Assert.True(result.Allow, "Expect other user to be able to change order");
        }

        /* These tests were created as a result of penetration testing - 
         * They send invalid messages and ensure that they are rejected*/

        [Test]
        //csiUserId,enteredBy,principalUserId,csiOrgId,principalOrgId
        [TestCase(10, 10, 200, 200, Description = "OrgId Should Match UserOrgId")]
        [TestCase(10, 10, 100, 200, Description = "CsiOrgId Should Match Principal OrgId")]
        [TestCase(20, 10, 100, 100, Description = "CsiUserId Should Match UserId")]
        public void AuthenticateNewOrderRequestWithWrongOrgMessage_Principal(int csiUserId, int principalUserId,
            int csiOrgId, int principalOrgId)
        {
            _orderAuthenticationService = new OrderAuthenticationService(_mockUserService.Object,
                _neededOrderPrivilegesProvider,
                _validators);

            var orderRequest = new AomOrderAuthenticationRequest
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = csiOrgId,
                    SessionId = "SessionToken",
                    UserId = csiUserId
                },
                MessageAction = _action,
                Order = MakeOrder(principalOrgId, principalUserId)
            };
            CreateValidExecutionInfoOnOrder(orderRequest);

            var result = _orderAuthenticationService.AuthenticateOrderRequest(orderRequest);
            Console.WriteLine(result.DenyReason);
            Assert.False(result.Allow);
        }

        [Test]
        public void AuthenticateNewOrderRequestValidMessage_Broker()
        {
            _orderAuthenticationService = new OrderAuthenticationService(_mockUserService.Object,
                _neededOrderPrivilegesProvider,
                _validators);

            var orderRequest = new AomOrderAuthenticationRequest
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = 300,
                    SessionId = "SessionToken",
                    UserId = 30
                },
                MessageAction = _action,
                Order = MakeFakeOrder(30, 300, null, 100)
            };
            CreateValidExecutionInfoOnOrder(orderRequest);

            var result = _orderAuthenticationService.AuthenticateOrderRequest(orderRequest);
            Assert.True(result.Allow);
            Assert.True(result.Allow);
        }

        [Test]
        [TestCase(30L, 30L, 400L, 400L, null, Description = "OrgId doesnt Match UserOrgId")]
        [TestCase(40L, 30L, 300L, 300L, null, Description = "csiUserId doesnt Match UserId")]
        [TestCase(30L, 30L, 300L, 400L, MessageAction.Execute,
             Description =
                 "csiOrganisationId doesnt Match BrokerOrgId (except when executing then that we will probably be different person/org)"
         )]
        [TestCase(30L, 40L, 300L, 400L, MessageAction.Execute,
             Description =
                 "csiUserId doesnt Match brokerUserId (except when executing then that we will probably be different person/org)"
         )]
        public void AuthenticateNewOrderRequestWithWrongOrgMessage_Broker(long csiUserId, long? brokerUserId,
            long csiOrganisationId, long? brokerOrganisationId, MessageAction? skipAction = null)
        {
            _orderAuthenticationService = new OrderAuthenticationService(_mockUserService.Object,
                _neededOrderPrivilegesProvider,
                _validators);

            var orderRequest = new AomOrderAuthenticationRequest
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = csiOrganisationId,
                    SessionId = "SessionToken",
                    UserId = csiUserId
                },
                MessageAction = _action,
                Order = new Order
                {
                    BrokerId = brokerUserId,
                    BrokerOrganisationId = brokerOrganisationId,
                    BrokerRestriction = BrokerRestriction.Specific,
                    BrokerOrganisationName = "BrokerOrgName",
                    BrokerShortCode = "BrokerShortCode",
                    DateCreated = new DateTime(2000, 1, 1),
                    DeliveryStartDate = new DateTime(2000, 1, 5),
                    DeliveryEndDate = new DateTime(2000, 1, 10),
                    OrderType = OrderType.Bid,
                    ProductId = 1,
                    PrincipalOrganisationId = 100,
                    PrincipalUserId = null,
                    Price = 300,
                    Quantity = 5000,
                    LastUpdatedUserId = 1,
                }
            };
            CreateValidExecutionInfoOnOrder(orderRequest);

            if (skipAction != null && _action == skipAction.Value)
            {
                Assert.Ignore(
                    "cobrokering means that another broker can now {0} an order. user={1}@{2} brokerOnOrder={3}@{4}",
                    _action, csiUserId, csiOrganisationId, brokerUserId, brokerOrganisationId);
            }

            var result = _orderAuthenticationService.AuthenticateOrderRequest(orderRequest);
            Console.WriteLine(result.DenyReason);
            Assert.False(result.Allow);
        }

        [Test]
        public void AuthenticateNewOrderRequestWithWrongOrgMessageForOrder_Principal()
        {
            _orderAuthenticationService = new OrderAuthenticationService(_mockUserService.Object,
                _neededOrderPrivilegesProvider,
                _validators);
            if (_action == MessageAction.Create)
            {
                Assert.Ignore("Not valid for create as there is no previous orderId - test will run for other actions");
            }
            var orderRequest = new AomOrderAuthenticationRequest
            {
                ClientSessionInfo = new ClientSessionInfo
                {
                    OrganisationId = 100,
                    SessionId = "SessionToken",
                    UserId = 10
                },
                MessageAction = _action,
                Order = new Order
                {
                    BrokerRestriction = BrokerRestriction.Any,
                    BrokerOrganisationName = "BrokerOrgName",
                    BrokerShortCode = "BrokerShortCode",
                    DateCreated = new DateTime(2000, 1, 1),
                    DeliveryStartDate = new DateTime(2000, 1, 5),
                    DeliveryEndDate = new DateTime(2000, 1, 10),
                    OrderType = OrderType.Bid,
                    ProductId = 1,
                    PrincipalOrganisationId = 1000,
                    PrincipalUserId = 10,
                    Price = 300,
                    Quantity = 5000,
                    LastUpdatedUserId = 10,
                    Id = 90
                }
            };
            CreateValidExecutionInfoOnOrder(orderRequest);

            if (_action == MessageAction.Execute)
            {
                Assert.Ignore("Not valid for execute as principal will be different to underlying order");
            }

            Assert.AreNotEqual(orderRequest.Order.PrincipalOrganisationId, orderRequest.ClientSessionInfo.OrganisationId,
                "test setup expects user org to be different to order org");

            var result = _orderAuthenticationService.AuthenticateOrderRequest(orderRequest);
            Assert.False(result.Allow);
        }

        private static void CreateValidExecutionInfoOnOrder(AomOrderAuthenticationRequest orderRequest)
        {
            if (orderRequest.Order.PrincipalUserId.HasValue || orderRequest.Order.BrokerId.HasValue)
            {
                Assert.AreEqual(orderRequest.Order.PrincipalUserId.HasValue, !orderRequest.Order.BrokerId.HasValue,
                    "#Setup error as both brokerid and principalid cannot be set");
            }

            if (orderRequest.MessageAction == MessageAction.Execute)
            {
                orderRequest.Order.ExecutionInfo = new ExecutionInfo()
                {
                    BrokerOrganisationId = orderRequest.Order.BrokerOrganisationId,
                    PrincipalOrganisationId = orderRequest.Order.PrincipalOrganisationId
                };
            }
        }

        private Order MakeFakeOrder(long brokerUserId, long brokerOrganisationId, long? principalUserId, long principalOrganisationId)
        {
            return new Order
            {
                BrokerId = brokerUserId,
                BrokerOrganisationId = brokerOrganisationId,
                BrokerRestriction = BrokerRestriction.Specific,
                BrokerOrganisationName = "BrokerOrgName",
                BrokerShortCode = "BrokerShortCode",
                DateCreated = new DateTime(2000, 1, 1),
                DeliveryStartDate = new DateTime(2000, 1, 5),
                DeliveryEndDate = new DateTime(2000, 1, 10),
                EnteredByUserId = 30,
                PrincipalUserId = principalUserId,
                OrderType = OrderType.Bid,
                ProductId = 1,
                PrincipalOrganisationId = principalOrganisationId,
                Price = 300,
                Quantity = 5000,
                LastUpdatedUserId = 1
            };
        }

        private Order MakeOrder(long principalOrgId, long principalUserId)
        {
            return new Order
            {
                BrokerId = null,
                BrokerOrganisationId = 300,
                BrokerRestriction = BrokerRestriction.Specific,
                BrokerOrganisationName = "BrokerOrgName",
                BrokerShortCode = "BrokerShortCode",
                DateCreated = new DateTime(2000, 1, 1),
                DeliveryStartDate = new DateTime(2000, 1, 5),
                DeliveryEndDate = new DateTime(2000, 1, 10),
                EnteredByUserId = 10,
                OrderType = OrderType.Bid,
                ProductId = 1,
                PrincipalOrganisationId = principalOrgId,
                Price = 300,
                Quantity = 5000,
                LastUpdatedUserId = 1,
                PrincipalUserId = principalUserId,
            };
        }
    }
}