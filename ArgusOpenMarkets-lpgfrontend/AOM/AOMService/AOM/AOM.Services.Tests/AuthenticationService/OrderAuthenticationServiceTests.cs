﻿using System.Collections.Generic;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using Moq;
using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using NUnit.Framework;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    public class OrderAuthenticationServiceTests
    {
        private Mock<IUserService> _mockUserService;
        private Mock<IUser> _mockUser;
        private Mock<IValidator<AomOrderAuthenticationRequest>> _v1;
        private Mock<IValidator<AomOrderAuthenticationRequest>> _v2;
        private NeededOrderPrivilegesProvider _orderPrivileges;
        private IValidator<AomOrderAuthenticationRequest>[] _validatorsImpl;
        private OrderAuthenticationService _orderAuthenticationService;

        [SetUp]
        public void Setup()
        {
            _mockUser = new Mock<IUser>();
            _mockUserService = new Mock<IUserService>();
            _mockUserService.Setup(us => us.GetUserWithPrivileges(It.IsAny<long>())).Returns(_mockUser.Object);
            _v1 = new Mock<IValidator<AomOrderAuthenticationRequest>>();
            _v2 = new Mock<IValidator<AomOrderAuthenticationRequest>>();
            _validatorsImpl = new[] {_v1.Object, _v2.Object};
            _orderPrivileges = new NeededOrderPrivilegesProvider(new NeededOrderPrivilegesFactory());
            _orderAuthenticationService = new OrderAuthenticationService(_mockUserService.Object,
                _orderPrivileges,
                _validatorsImpl);
        }

        [Test]
        public void ShouldReturnAllowedIfAllValidatorsReturnAllow()
        {
            SetupValidator(_v1, ValidatorResult.Allow());
            SetupValidator(_v2, ValidatorResult.Allow());
            var result = _orderAuthenticationService.AuthenticateOrderRequest(CreateRequest());
            Assert.That(result.Allow, Is.True);
        }

        [Test]
        public void ShouldReturnDeniedIfOneValidatorReturnsDeny()
        {
            const string denyReason = "request denied";
            SetupValidator(_v1, ValidatorResult.Allow());
            SetupValidator(_v2, ValidatorResult.Deny(denyReason));
            var result = _orderAuthenticationService.AuthenticateOrderRequest(CreateRequest());
            Assert.That(result.Allow, Is.False);
            Assert.That(result.DenyReason, Is.EqualTo(denyReason));
        }

        [Test]
        public void ShouldReturnMarketMustBeOpenIfOneOrMoreValidatorSpecifiesIt()
        {
            SetupValidator(_v1, ValidatorResult.Allow(false));
            SetupValidator(_v2, ValidatorResult.Allow(true));
            var result = _orderAuthenticationService.AuthenticateOrderRequest(CreateRequest());
            Assert.That(result.Allow, Is.True);
            Assert.That(result.MarketMustBeOpen, Is.True);
        }

        [Test]
        public void ShouldAllowIfAllValidatorsAbstain()
        {
            SetupValidator(_v1, ValidatorResult.Abstain());
            SetupValidator(_v2, ValidatorResult.Abstain());
            var result = _orderAuthenticationService.AuthenticateOrderRequest(CreateRequest());
            Assert.That(result.Allow, Is.True);
        }

        [Test]
        public void DetermineRoleShouldReturnNoneForNullUser()
        {
            var role = _orderAuthenticationService.DetermineRole(CreateRequest(), null);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.None));
        }

        [Test]
        public void DetermineRoleShouldReturnInactiveForInactiveUser()
        {
            var user = Mock.Of<IUser>(u => u.IsActive == false);
            var role = _orderAuthenticationService.DetermineRole(CreateRequest(), user);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.Inactive));
        }

        [Test]
        public void DetermineRoleShouldReturnNoneIfNullProductPrivileges()
        {
            var user = Mock.Of<IUser>(u => u.IsActive == true &&
                                           u.ProductPrivileges == new List<UserProductPrivilege>());
            var role = _orderAuthenticationService.DetermineRole(CreateRequest(), user);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.None));
        }

        [Test]
        public void DetermineRoleShouldReturnNoneForCreateAsDifferentPrincipalAndBrokerButNoPrivs()
        {
            var user = Mock.Of<IUser>(u => u.IsActive == true &&
                                           u.ProductPrivileges == new List<UserProductPrivilege>());
            const long connectionUserId = 2;
            const long connectionOrgId = 2;
            var request = CreateRequest();
            request.MessageAction = MessageAction.Create;
            request.ClientSessionInfo.UserId = connectionUserId;
            request.ClientSessionInfo.OrganisationId = connectionOrgId;
            var role = _orderAuthenticationService.DetermineRole(request, user);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.None));
        }

        [Test]
        public void DetermineRoleShouldReturnSuperForCreateAsDifferentPrincipalAndBrokerWithSuperPrivs()
        {
            const long connectionUserId = 2;
            const long connectionOrgId = 2;
            var request = CreateRequest();
            request.MessageAction = MessageAction.Create;
            request.ClientSessionInfo.UserId = connectionUserId;
            request.ClientSessionInfo.OrganisationId = connectionOrgId;

            var privs = new UserProductPrivilege
            {
                ProductId = request.Order.ProductId,
                Privileges = new Dictionary<string, bool> {{ProductPrivileges.Orders.Create.AsAnyone, true}}
            };
            var user = Mock.Of<IUser>(
                u => u.IsActive == true && u.ProductPrivileges == new List<UserProductPrivilege> {privs});

            var role = _orderAuthenticationService.DetermineRole(request, user);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.Super));
        }

        [Test]
        public void DetermineRoleShouldReturnNoneForCreateAsPrincipalWithNoPrivs()
        {
            var request = CreateRequest();
            request.MessageAction = MessageAction.Create;
            request.ClientSessionInfo.UserId = request.Order.PrincipalUserId.Value;
            request.ClientSessionInfo.OrganisationId = request.Order.PrincipalOrganisationId;

            var user = Mock.Of<IUser>(
                u => u.IsActive == true &&
                     u.Id == request.ClientSessionInfo.UserId &&
                     u.ProductPrivileges == new List<UserProductPrivilege>());

            var role = _orderAuthenticationService.DetermineRole(request, user);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.None));
        }

        [TestCase(MessageAction.Create, true)]
        [TestCase(MessageAction.Execute, true)]
        [TestCase(MessageAction.Hold, true)]
        [TestCase(MessageAction.Create, false)]
        [TestCase(MessageAction.Execute, false)]
        [TestCase(MessageAction.Hold, false)]
        public void DetermineRoleShouldReturnPrincipalWithPrivsRegardlessOfCoBrokering(MessageAction actionForTest,
            bool coBrokeringSettingForTestRun)
        {
            var request = CreateRequest();
            request.MessageAction = actionForTest;
            request.ClientSessionInfo.UserId = request.Order.PrincipalUserId.Value;
            request.ClientSessionInfo.OrganisationId = request.Order.PrincipalOrganisationId;
            request.Order.CoBrokering = coBrokeringSettingForTestRun;
            request.Order.BrokerId = null;

            var minimumPrivForAction = new Dictionary<MessageAction, string>
            {
                {MessageAction.Execute, ProductPrivileges.Orders.Aggress.AsPrincipal},
                {MessageAction.Hold, ProductPrivileges.Orders.Amend.Own},
                {MessageAction.Create, ProductPrivileges.Orders.Create.AsPrincipal}
            };

            AddExecutionInfoForExecuteAction(request, false);

            var privs = new UserProductPrivilege
            {
                ProductId = request.Order.ProductId,
                Privileges = new Dictionary<string, bool>
                {
                    {minimumPrivForAction[actionForTest], true},
                }
            };
            var user = Mock.Of<IUser>(
                u => u.IsActive == true &&
                     u.Id == request.ClientSessionInfo.UserId &&
                     u.UserOrganisation == new Organisation {Id = request.ClientSessionInfo.OrganisationId} &&
                     u.ProductPrivileges == new List<UserProductPrivilege> {privs});

            var role = _orderAuthenticationService.DetermineRole(request, user);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.Principal));
        }

        private static void AddExecutionInfoForExecuteAction(AomOrderAuthenticationRequest request, bool withABroker)
        {
            if (request.Order.PrincipalUserId.HasValue || request.Order.BrokerId.HasValue)
            {
                Assert.AreEqual(request.Order.PrincipalUserId.HasValue, !request.Order.BrokerId.HasValue,
                    "#Setup error as both brokerid and principalid cannot be set");
            }

            if (request.MessageAction == MessageAction.Execute)
            {
                request.Order.ExecutionInfo = new ExecutionInfo {PrincipalOrganisationId = 666};

                if (withABroker)
                {
                    request.Order.ExecutionInfo.BrokerOrganisationId = request.Order.BrokerOrganisationId.Value;
                }
            }
        }

        [Test]
        public void DetermineRoleShouldReturnNoneForCreateAsBrokerWithNoPrivs()
        {
            var request = CreateRequest();
            request.MessageAction = MessageAction.Create;
            request.ClientSessionInfo.UserId = request.Order.BrokerId.Value;
            request.ClientSessionInfo.OrganisationId = request.Order.BrokerOrganisationId.Value;

            var user = Mock.Of<IUser>(
                u => u.IsActive == true &&
                     u.Id == request.ClientSessionInfo.UserId &&
                     u.ProductPrivileges == new List<UserProductPrivilege>());

            var role = _orderAuthenticationService.DetermineRole(request, user);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.None));
        }

        [TestCase(MessageAction.Create, true)]
        [TestCase(MessageAction.Execute, true)]
        [TestCase(MessageAction.Hold, true)]
        [TestCase(MessageAction.Create, false)]
        [TestCase(MessageAction.Execute, false)]
        [TestCase(MessageAction.Hold, false)]
        public void DetermineRoleShouldReturnBrokerAsBrokerWithPrivsRegardlessOfCoBrokering(MessageAction actionForTest,
            bool coBrokeringSettingForTestRun)
        {
            var request = CreateRequest();

            request.MessageAction = actionForTest;
            request.ClientSessionInfo.UserId = request.Order.BrokerId.Value;
            request.ClientSessionInfo.OrganisationId = request.Order.BrokerOrganisationId.Value;
            request.Order.CoBrokering = coBrokeringSettingForTestRun;
            request.Order.PrincipalUserId = null;

            AddExecutionInfoForExecuteAction(request, true);

            var minimumPrivForAction = new Dictionary<MessageAction, string>
            {
                {MessageAction.Execute, ProductPrivileges.Orders.Aggress.AsBroker},
                {MessageAction.Hold, ProductPrivileges.Orders.Amend.Own},
                {MessageAction.Create, ProductPrivileges.Orders.Create.AsBroker}
            };

            var privs = new UserProductPrivilege
            {
                ProductId = request.Order.ProductId,
                Privileges = new Dictionary<string, bool>
                {
                    {minimumPrivForAction[actionForTest], true}
                }
            };

            var user = Mock.Of<IUser>(
                u => u.IsActive == true &&
                     u.Id == request.ClientSessionInfo.UserId &&
                     u.UserOrganisation == new Organisation {Id = request.ClientSessionInfo.OrganisationId} &&
                     u.ProductPrivileges == new List<UserProductPrivilege> {privs});

            var role = _orderAuthenticationService.DetermineRole(request, user);
            Assert.That(role, Is.EqualTo(AuthenticationDataTypes.Role.Broker));
        }

        // TODO Further DetermineRole tests to check for Super/Principal/Broker where
        //      acting from same org

        private static AomOrderAuthenticationRequest CreateRequest()
        {
            var aomOrderAuthenticationRequest = new AomOrderAuthenticationRequest
            {
                Order = new Order
                {
                    BrokerId = 7,
                    BrokerOrganisationId = 5,
                    EnteredByUserId = 3,
                    OrderStatus = OrderStatus.Active,
                    ProductId = 1,
                    PrincipalUserId = 3,
                    PrincipalOrganisationId = 3
                },
                ClientSessionInfo = new ClientSessionInfo {UserId = 3, OrganisationId = 3}
            };
            return aomOrderAuthenticationRequest;
        }

        private static void SetupValidator(Mock<IValidator<AomOrderAuthenticationRequest>> mv,
            ValidatorResult result)
        {
            var res = result;
            mv.Setup(v => v.IsValid(It.IsAny<AomOrderAuthenticationRequest>(),
                    It.IsAny<AuthenticationDataTypes.Role>(),
                    It.IsAny<IUser>()))
                .Returns(res);
        }
    }
}