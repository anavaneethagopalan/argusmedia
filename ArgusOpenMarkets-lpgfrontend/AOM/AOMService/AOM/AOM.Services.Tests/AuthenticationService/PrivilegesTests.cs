﻿namespace AOM.Services.Tests.AuthenticationService
{
    using AOM.Repository.MySql.Crm;
    using AOM.Services.AuthenticationService;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class PrivilegesTests
    {
        [Test]
        public void AssertThatPrivilegesStringsExistInDbTest()
        {
            using (var model = new CrmModel())
            {
                var qry = from p in model.ProductPrivileges select p.Name;
                var privilegesInDb = qry.ToList();

                //TOOD: use reflection to retrieve string
                
                //AssertListContains(ProductPrivileges.Orders.Aggress.Any, privilegesInDb);
                AssertListContains(ProductPrivileges.Orders.Aggress.AsBroker, privilegesInDb);
                AssertListContains(ProductPrivileges.Orders.Aggress.AsPrincipal, privilegesInDb);
                //AssertListContains(ProductPrivileges.Orders.Aggress.OwnOrg.Broker, privilegesInDb);
                AssertListContains(ProductPrivileges.Orders.Aggress.OwnOrg.Principal, privilegesInDb);
                                
                AssertListContains(ProductPrivileges.Deal.Authenticate, privilegesInDb);
                AssertListContains(ProductPrivileges.Deal.Amend.Any, privilegesInDb);
                AssertListContains(ProductPrivileges.Deal.Create.AsSuperUser, privilegesInDb);
                AssertListContains(ProductPrivileges.Deal.Create.AsBroker, privilegesInDb);
                AssertListContains(ProductPrivileges.Deal.Create.AsPrincipal, privilegesInDb);

                AssertListContains(ProductPrivileges.ExternalDeal.Authenticate, privilegesInDb);
                AssertListContains(ProductPrivileges.ExternalDeal.Amend.Any, privilegesInDb);
                AssertListContains(ProductPrivileges.ExternalDeal.Create.AsSuperUser, privilegesInDb);
                AssertListContains(ProductPrivileges.ExternalDeal.Create.AsBroker, privilegesInDb);
                AssertListContains(ProductPrivileges.ExternalDeal.Create.AsPrincipal, privilegesInDb);
                
                AssertListContains(ProductPrivileges.MarketTicker.Authenticate, privilegesInDb);
                AssertListContains(ProductPrivileges.MarketTicker.Create, privilegesInDb);

                AssertListContains(ProductPrivileges.MarketTickerPlusInfo.Create, privilegesInDb);
            }
        }

        private void AssertListContains(string expectedString, List<string> list)
        {
            if (list.Contains(expectedString)) return;
            Assert.Fail("Expected " + expectedString + " to be present within db.");
        }
    }
}

