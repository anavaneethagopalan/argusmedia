﻿namespace AOM.Services.Tests.AuthenticationService
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Services.AuthenticationService;
    using AOM.Transport.Events;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class ExternalDealAuthenticationServiceTests
    {
        private ExternalDealAuthenticationService _externalDealAuthenticationService;

        private Mock<IUserService> _mockUserService;

        [SetUp]
        public void Setup()
        {
            _mockUserService = new Mock<IUserService>();
            _externalDealAuthenticationService = new ExternalDealAuthenticationService(_mockUserService.Object);
        }

        [Test]
        public void ShouldAuthenticeExternalDealWithUserDetailsThatCanPerformThisAction()
        {
            var externalDeal = MakeDeal();
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.ExternalDeal.Authenticate));

            var authResult = _externalDealAuthenticationService.AuthenticateEditExternalDealRequest(externalDeal, csi);

            Assert.That(authResult.Allow, Is.True);
        }


        private ClientSessionInfo MakeClientSessionInfo()
        {
            return new ClientSessionInfo {OrganisationId = 1, SessionId = "1", UserId = 1};
        }

        private ExternalDeal MakeDeal()
        {
            return new ExternalDeal {ProductId = 1};
        }
    }
}