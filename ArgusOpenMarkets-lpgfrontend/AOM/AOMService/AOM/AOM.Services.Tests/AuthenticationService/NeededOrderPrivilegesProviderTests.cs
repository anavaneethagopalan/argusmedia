﻿using System;
using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Transport.Events;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    public class NeededOrderPrivilegesProviderTests
    {
        private Mock<INeededOrderPrivileges> _mockPrivileges;
        private NeededOrderPrivilegesProvider _provider;

        [SetUp]
        public void Setup()
        {
            _mockPrivileges = new Mock<INeededOrderPrivileges>();

            var mockFactory = new Mock<INeededOrderPrivilegesFactory>();
            mockFactory.Setup(f => f.CreatePrivileges(It.IsAny<ExecutionMode>())).Returns(() => _mockPrivileges.Object);
            _provider = new NeededOrderPrivilegesProvider(mockFactory.Object);
        }

        [Test]
        [TestCase(ExecutionMode.Internal)]
        [TestCase(ExecutionMode.External)]
        [TestCase(ExecutionMode.None)]
        public void NeededOrderPrivilegesFactoryShouldCreatePrivilegesForValidExecutionModes(ExecutionMode executionMode)
        {
            var factory = new NeededOrderPrivilegesFactory();
            Assert.NotNull(factory.CreatePrivileges(executionMode));
        }

        [Test]
        public void NeededOrderPrivilegesFactoryShouldThrowForInvalidExecutionModes()
        {
            var factory = new NeededOrderPrivilegesFactory();
            Assert.Throws<ArgumentOutOfRangeException>(
                () => factory.CreatePrivileges((ExecutionMode) 9999));
        }

        [Test]
        public void GettingRequiredPrivilegesForInternalOrderShouldAskFactoryForInternal()
        {
            var mockFactory = new Mock<INeededOrderPrivilegesFactory>();
            var provider = new NeededOrderPrivilegesProvider(mockFactory.Object);
            provider.GetRequiredPrivilegesForAction(MessageAction.Create, ExecutionMode.Internal);
            mockFactory.Verify(f => f.CreatePrivileges(It.Is<ExecutionMode>(m => m == ExecutionMode.Internal)));
        }

        [Test]
        public void GettingRequiredPrivilegesForExternalOrderShouldAskFactoryForExternal()
        {
            var mockFactory = new Mock<INeededOrderPrivilegesFactory>();
            var provider = new NeededOrderPrivilegesProvider(mockFactory.Object);
            provider.GetRequiredPrivilegesForAction(MessageAction.Create, ExecutionMode.External);
            mockFactory.Verify(f => f.CreatePrivileges(It.Is<ExecutionMode>(m => m == ExecutionMode.External)));
        }

        [Test]
        public void CreateMessageActionShouldAskForCreatePrivileges()
        {
            _provider.GetRequiredPrivilegesForAction(MessageAction.Create, ExecutionMode.Internal);
            _mockPrivileges.Verify(p => p.ForCreate, Times.Once);
        }


    }
}
