﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using AOM.Services.AuthenticationService;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    public class AssessmentAuthenticationServiceTests
    {
        private AssessmentAuthenticationService _authenticationService;

        private const long FakeProductId = 11;
        private const long PermissionedUserId = 1;
        private const long NonPermissionedUserId = 2;
        private const long InactiveUserId = 3;

        private static readonly IUser PermissionedUser = new User
        {
            Id = PermissionedUserId,
            IsActive = true,
            ProductPrivileges = new List<UserProductPrivilege>
            {
                new UserProductPrivilege
                {
                    ProductId = FakeProductId,
                    Privileges = new Dictionary<string, bool>
                    {
                        {ProductPrivileges.Assessment.Create, true}
                    }
                }
            }
        };

        private static readonly IUser NonPermissionedUser = new User
        {
            Id = NonPermissionedUserId,
            IsActive = true,
            ProductPrivileges = new List<UserProductPrivilege>()
        };

        private static readonly IUser InactiveUser = new User
        {
            Id = PermissionedUserId,
            IsActive = false,
            ProductPrivileges = new List<UserProductPrivilege>
            {
                new UserProductPrivilege
                {
                    ProductId = FakeProductId,
                    Privileges = new Dictionary<string, bool>
                    {
                        {ProductPrivileges.Assessment.Create, true}
                    }
                }
            }
        };

        [SetUp]
        public void Setup()
        {
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(s => s.GetUserWithPrivileges(It.IsAny<long>()))
                           .Returns<long>(id =>
                           {
                               switch (id)
                               {
                                   case PermissionedUserId:
                                       return PermissionedUser;
                                   case NonPermissionedUserId:
                                       return NonPermissionedUser;
                                   case InactiveUserId:
                                       return InactiveUser;
                                   default:
                                       throw new ArgumentException("Invalid user ID");
                               }
                           });

            _authenticationService = new AssessmentAuthenticationService(mockUserService.Object);
        }

        [Test]
        public void PermissionedUserIsAuthorisedToCreateAssessment()
        {
            var csi = new ClientSessionInfo {UserId = PermissionedUserId};
            var request = new AomAssessmentAuthenticationRequest
            {
                ClientSessionInfo = csi,
                MessageAction = MessageAction.Create,
                ProductId = FakeProductId
            };
            var authResult = _authenticationService.AuthenticateAssessmentRequest(request);
            Assert.IsTrue(authResult.Allow);
            Assert.IsNull(authResult.DenyReason);
        }

        [Test]
        public void PermissionedUserIsAuthorisedToRefreshAssessment()
        {
            var csi = new ClientSessionInfo {UserId = PermissionedUserId};
            var request = new AomAssessmentAuthenticationRequest
            {
                ClientSessionInfo = csi,
                MessageAction = MessageAction.Refresh,
                ProductId = FakeProductId
            };
            var authResult = _authenticationService.AuthenticateAssessmentRequest(request);
            Assert.IsTrue(authResult.Allow);
            Assert.IsNull(authResult.DenyReason);
        }

        [Test]
        public void NonPermissionedUserNotAuthorisedToCreateAssessment()
        {
            var csi = new ClientSessionInfo {UserId = NonPermissionedUserId};
            var request = new AomAssessmentAuthenticationRequest
            {
                ClientSessionInfo = csi,
                MessageAction = MessageAction.Create,
                ProductId = FakeProductId
            };
            var authResult = _authenticationService.AuthenticateAssessmentRequest(request);
            Assert.IsFalse(authResult.Allow);
            Assert.NotNull(authResult.DenyReason);
        }

        [Test]
        public void NonPermissionedUserNotAuthorisedToRefreshAssessment()
        {
            var csi = new ClientSessionInfo {UserId = NonPermissionedUserId};
            var request = new AomAssessmentAuthenticationRequest
            {
                ClientSessionInfo = csi,
                MessageAction = MessageAction.Refresh,
                ProductId = FakeProductId
            };
            var authResult = _authenticationService.AuthenticateAssessmentRequest(request);
            Assert.IsFalse(authResult.Allow);
            Assert.NotNull(authResult.DenyReason);
        }

        [Test]
        public void InactivePermissionedUserNotAuthorisedToCreateAssessment()
        {
            var csi = new ClientSessionInfo {UserId = InactiveUserId};
            var request = new AomAssessmentAuthenticationRequest
            {
                ClientSessionInfo = csi,
                MessageAction = MessageAction.Create,
                ProductId = FakeProductId
            };
            var authResult = _authenticationService.AuthenticateAssessmentRequest(request);
            Assert.IsFalse(authResult.Allow);
            Assert.NotNull(authResult.DenyReason);
        }

        [Test]
        public void InactivePermissionedUserNotAuthorisedToRefreshAssessment()
        {
            var csi = new ClientSessionInfo {UserId = InactiveUserId};
            var request = new AomAssessmentAuthenticationRequest
            {
                ClientSessionInfo = csi,
                MessageAction = MessageAction.Refresh,
                ProductId = FakeProductId
            };
            var authResult = _authenticationService.AuthenticateAssessmentRequest(request);
            Assert.IsFalse(authResult.Allow);
            Assert.NotNull(authResult.DenyReason);
        }

        [Test]
        public void RequestWithNullClientSessionInfoIsRejected()
        {
            var request = new AomAssessmentAuthenticationRequest
            {
                ClientSessionInfo = null,
                MessageAction = MessageAction.Refresh,
                ProductId = FakeProductId
            };
            var authResult = _authenticationService.AuthenticateAssessmentRequest(request);
            Assert.IsFalse(authResult.Allow);
            Assert.NotNull(authResult.DenyReason);
        }
    }
}
