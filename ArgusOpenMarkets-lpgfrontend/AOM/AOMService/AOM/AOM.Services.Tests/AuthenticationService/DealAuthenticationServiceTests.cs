﻿namespace AOM.Services.Tests.AuthenticationService
{
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.App.Domain.Services;
    using AOM.Services.AuthenticationService;
    using AOM.Transport.Events;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class DealAuthenticationServiceTests
    {
        private Mock<IUserService> _mockUserService;

        private DealAuthenticationService _dealAuthenticationService;

        [SetUp]
        public void Setup()
        {
            _mockUserService = new Mock<IUserService>();
            _dealAuthenticationService = new DealAuthenticationService(_mockUserService.Object);
        }

        [Test]
        public void ShouldReturnNotAuthorisedIfTheUserHasTheCorrectPrivilegesButIsNotActive()
        {

            var deal = MakeDeal();
            var csi = new ClientSessionInfo {UserId = 1, OrganisationId = 1, SessionId = "1"};
            var user = AuthenticationHelper.MakeUser(false, 1);

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(user);
            AuthResult authResult = _dealAuthenticationService.AuthenticateNewDealRequest(deal, csi);

            Assert.That(authResult.Allow, Is.False);
        }

        [Test]
        public void ShouldReturnNotAuthorisedIfTheUserHasTheCorrectPrivilegesButForADifferentProductAndTheUserIsActive()
        {

            var deal = MakeDeal();
            var csi = new ClientSessionInfo {UserId = 1, OrganisationId = 1, SessionId = "1"};
            var user = AuthenticationHelper.MakeUser(true, 100);

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(user);
            AuthResult authResult = _dealAuthenticationService.AuthenticateNewDealRequest(deal, csi);

            Assert.That(authResult.Allow, Is.False);
            Assert.That(authResult.MarketMustBeOpen, Is.False);
        }

        [Test]
        public void ShouldReturnAuthorisedIfTheUserHasTheCorrectPrivilegesAndTheUserIsActive()
        {

            var deal = MakeDeal();
            var csi = new ClientSessionInfo {UserId = 1, OrganisationId = 1, SessionId = "1"};
            var user = AuthenticationHelper.MakeUser(true, 1);

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(user);
            AuthResult authResult = _dealAuthenticationService.AuthenticateNewDealRequest(deal, csi);

            Assert.That(authResult.Allow, Is.True, "User Should Be Allowed");
        }

        [Test]
        public void AuthoriseDealShouldReturnMarketMustBeOpenTrueIfTheUserHasRoleCreateAsPrincipal()
        {
            var deal = MakeDeal();
            var csi = new ClientSessionInfo {UserId = 1, OrganisationId = 1, SessionId = "1"};
            var user = AuthenticationHelper.MakeUser(true, 1);

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(user);
            AuthResult authResult = _dealAuthenticationService.AuthenticateNewDealRequest(deal, csi);

            Assert.That(authResult.MarketMustBeOpen, Is.True, "Market Should Be Open");
        }

        [Test]
        public void AuthoriseDealShouldReturnMarketMustBeOpenFalseIfTheUserHasRoleCreateRoleAsSuperUser()
        {
            var deal = MakeDeal();
            var csi = new ClientSessionInfo {UserId = 1, OrganisationId = 1, SessionId = "1"};
            var user = AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.Deal.Create.AsSuperUser);

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(user);
            AuthResult authResult = _dealAuthenticationService.AuthenticateNewDealRequest(deal, csi);

            Assert.That(authResult.MarketMustBeOpen, Is.True, "Market Should Be Open");
        }

        [Test]
        public void
            ShouldReturnNotAuthorisedIfTheUserHasPrivilegesNoneOfWhichEntitleHimToAuthoriseADealAndTheUserIsActive()
        {

            var deal = MakeDeal();
            var csi = new ClientSessionInfo {UserId = 1, OrganisationId = 1, SessionId = "1"};
            var user = MakeUserWithoutAuthorisePrivileges(true, 1);

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(user);
            AuthResult authResult = _dealAuthenticationService.AuthenticateNewDealRequest(deal, csi);

            Assert.That(authResult.Allow, Is.False, "User Should Be Allowed");
        }

        private IUser MakeUserWithoutAuthorisePrivileges(bool active, int productId)
        {
            var productPrivilege = new UserProductPrivilege
            {
                Privileges = new Dictionary<string, bool>(),
                ProductId = productId
            };

            productPrivilege.Privileges.Add(ProductPrivileges.MarketTicker.Create, true);
            var userProductPrivileges = new List<UserProductPrivilege>
            {
                productPrivilege
            };
            var user = new User {Id = 1, Username = "hi", ProductPrivileges = userProductPrivileges, IsActive = active};

            return user;
        }

        private Deal MakeDeal()
        {
            return new Deal
            {
                Id = 1,
                DealStatus = DealStatus.Executed,
                Initial = new Order {ProductId = 1, Id = 2}
            };
        }
    }
}