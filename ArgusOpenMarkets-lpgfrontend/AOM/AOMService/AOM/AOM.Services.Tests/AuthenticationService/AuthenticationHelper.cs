﻿namespace AOM.Services.Tests.AuthenticationService
{
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Interfaces;
    using AOM.Services.AuthenticationService;

    public static class AuthenticationHelper
    {
        public static IUser MakeUser(bool active, int productId, string role = ProductPrivileges.Deal.Create.AsPrincipal)
        {
            var productPrivilege = new UserProductPrivilege
            {
                Privileges = new Dictionary<string, bool>(),
                ProductId = productId
            };

            productPrivilege.Privileges.Add(role, true);
            var userProductPrivileges = new List<UserProductPrivilege>
                                        {
                                            productPrivilege
                                        };
            var user = new User { Id = 1, Username = "hi", ProductPrivileges = userProductPrivileges, IsActive = active };

            return user;
        }
    }
}
