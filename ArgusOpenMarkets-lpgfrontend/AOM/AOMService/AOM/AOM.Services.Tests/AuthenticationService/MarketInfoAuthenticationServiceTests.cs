﻿namespace AOM.Services.Tests.AuthenticationService
{
    using AOM.App.Domain.Entities;
    using AOM.App.Domain.Services;
    using AOM.Services.AuthenticationService;
    using AOM.Transport.Events;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class MarketInfoAuthenticationServiceTests
    {
        private MarketInfoAuthenticationService _marketInfoAuthenticationService;

        private Mock<IUserService> _mockUserService;

        [SetUp]
        public void Setup()
        {
            _mockUserService = new Mock<IUserService>();
            _marketInfoAuthenticationService = new MarketInfoAuthenticationService(_mockUserService.Object);
        }

        [Test]
        public void ShouldAuthenticeExternalDealWithUserDetailsThatCanPerformThisAction()
        {
            var marketInfo = MakeMarketInfo();
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Authenticate));

            var authResult = _marketInfoAuthenticationService.AuthenticateEditMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.True);
        }

        [Test]
        public void ShouldAllowCreationOfAPendingMarketInfoForUserWithCreatePrivilege()
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Pending;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateNewMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.True);
        }

        [Test]
        public void ShouldNotAllowCreationOfAnActiveMarketInfoForUserThatCannotAuthenticate()
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateNewMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.False);
        }

        [Test]
        public void ShouldBeAbleToEditAPendingInfoWithCreatePrivilege()
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Pending;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateEditMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.True);
        }

        [Test]
        public void ShouldNotBeAbleToEditAnInfoToActiveWithOnlyCreatePrivilege()
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateEditMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.False);
        }

        [Test]
        public void ShouldBeAbleToEditAnInfoToActiveWithAuthenticatePrivilege()
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Authenticate));

            var authResult = _marketInfoAuthenticationService.AuthenticateEditMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.True);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void ShouldBeAbleToCreateAPendingPlusInfoWithPlusInfoCreatePrivilege(MarketInfoType marketInfoType)
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Pending;
            marketInfo.MarketInfoType = marketInfoType;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTickerPlusInfo.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateNewMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.True);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void ShouldNotBeAbleToCreateAPendingPlusInfoWithRegularTickerCreatePrivilege(MarketInfoType marketInfoType)
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Pending;
            marketInfo.MarketInfoType = marketInfoType;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateNewMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.False);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void ShouldBeAbleToEditAPendingPlusInfoWithPlusInfoCreatePrivilege(MarketInfoType marketInfoType)
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Pending;
            marketInfo.MarketInfoType = marketInfoType;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTickerPlusInfo.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateEditMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.True);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void ShouldNotBeAbleToEditAPendingPlusInfoWithRegularTickerCreatePrivilege(MarketInfoType marketInfoType)
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Pending;
            marketInfo.MarketInfoType = marketInfoType;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateEditMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.False);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void ShouldBeAbleToCreateAnActivePlusInfoWithAuthenticatePrivilege(MarketInfoType marketInfoType)
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            marketInfo.MarketInfoType = marketInfoType;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Authenticate));

            var authResult = _marketInfoAuthenticationService.AuthenticateNewMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.True);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void ShouldNotBeAbleToCreateAnActivePlusInfoWithOnlyPlusInfoCreatePrivilege(MarketInfoType marketInfoType)
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            marketInfo.MarketInfoType = marketInfoType;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTickerPlusInfo.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateNewMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.False);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void ShouldBeAbleToEditToAnActivePlusInfoWithAuthenticatePrivilege(MarketInfoType marketInfoType)
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            marketInfo.MarketInfoType = marketInfoType;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTicker.Authenticate));

            var authResult = _marketInfoAuthenticationService.AuthenticateEditMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.True);
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void ShouldNotBeAbleToEditToAnActivePlusInfoWithOnlyPlusInfoCreatePrivilege(MarketInfoType marketInfoType)
        {
            var marketInfo = MakeMarketInfo();
            marketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            marketInfo.MarketInfoType = marketInfoType;
            var csi = MakeClientSessionInfo();

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>()))
                            .Returns(AuthenticationHelper.MakeUser(true, 1, ProductPrivileges.MarketTickerPlusInfo.Create));

            var authResult = _marketInfoAuthenticationService.AuthenticateEditMarketInfoRequest(marketInfo, csi);

            Assert.That(authResult.Allow, Is.False);
        }

        private static ClientSessionInfo MakeClientSessionInfo()
        {
            return new ClientSessionInfo { OrganisationId = 1, SessionId = "1", UserId = 1 };
        }

        private static MarketInfo MakeMarketInfo()
        {
            return new MarketInfo { OwnerOrganisationId = 1 };
        }
    }
}
