﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.AuthenticationService;
using AOM.Transport.Events;
using AOM.Transport.Events.BrokerPermissions;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.AuthenticationService
{
    [TestFixture]
    public class BrokerPermissionAuthenticationServiceMessageValidityTests
    {
        private BrokerPermissionAuthenticationService _brokerPermissionsAuthenticationService;

        [SetUp]
        public void Setup()
        {
            var mockUserService = new Mock<IUserService>();

            var principal1User = MakeUser(1, 1, false);

            var principal2User = MakeUser(2, 2, false);

            var principal3User = MakeUser(3, 1, true);

            mockUserService.Setup(x => x.GetUserWithPrivileges(1)).Returns(principal1User);
            mockUserService.Setup(x => x.GetUserWithPrivileges(2)).Returns(principal2User);
            mockUserService.Setup(x => x.GetUserWithPrivileges(3)).Returns(principal3User);
            _brokerPermissionsAuthenticationService = new BrokerPermissionAuthenticationService(mockUserService.Object);
        }

        private User MakeUser(long userId, long organisationId, bool emptyPrivs)
        {
            List<string> privs = new List<string>();

            if (!emptyPrivs)
            {
                privs = new List<string> {Services.AuthenticationService.SystemPrivileges.BrokerPermissions.Amend.Own};
            }

            return new User
            {
                Id = userId,
                OrganisationId = organisationId,
                UserOrganisation = new Organisation { Id = organisationId },
                IsActive = true,
                SystemPrivileges = new App.Domain.Entities.SystemPrivileges
                {
                    Privileges = privs
                }
            };
        }

        [Test]
        public void ChangeBrokerPermissionValidMessage()
        {
            var brokerPerms = new List<MatrixPermission>
            {MakeBrokerPermissions()};

            var request = new AomBrokerPermissionAuthenticationRequest
            {
                BrokerPermissions = brokerPerms,
                ClientSessionInfo = new ClientSessionInfo {OrganisationId = 1, SessionId = "sessionId", UserId = 1},
                MessageAction = MessageAction.Update
            };
            var result = _brokerPermissionsAuthenticationService.AuthenticateBrokerPermissionRequest(request);
            Assert.True(result.Allow);
        }


        [Test]
        public void ChangeBrokerPermissionValidMessage_NotPermissioned()
        {
            var brokerPerms = new List<MatrixPermission>();
            brokerPerms.Add(MakeBrokerPermissions());

            var request = new AomBrokerPermissionAuthenticationRequest
            {
                BrokerPermissions = brokerPerms,
                ClientSessionInfo = new ClientSessionInfo {OrganisationId = 1, SessionId = "sessionId", UserId = 3},
                MessageAction = MessageAction.Update
            };
            var result = _brokerPermissionsAuthenticationService.AuthenticateBrokerPermissionRequest(request);
            Assert.False(result.Allow);
            Assert.AreEqual("You are not permissioned to edit BrokerPermissions", result.DenyReason);
        }

        [Test]
        public void ChangBrokerPermissionInvalidMessage()
        {
            var brokerPerms = new List<MatrixPermission>();
            brokerPerms.Add(MakeBrokerPermissions());

            var request = new AomBrokerPermissionAuthenticationRequest
            {
                BrokerPermissions = brokerPerms,
                ClientSessionInfo = new ClientSessionInfo {OrganisationId = 2, SessionId = "sessionId", UserId = 2},
                MessageAction = MessageAction.Update
            };
            var result = _brokerPermissionsAuthenticationService.AuthenticateBrokerPermissionRequest(request);
            Assert.False(result.Allow);
        }

        private MatrixPermission MakeBrokerPermissions()
        {
            return new MatrixPermission
            {
                AllowOrDeny = Permission.Allow,
                BuyOrSell = BuyOrSell.Buy,
                Id = 12,
                LastUpdated = DateTime.Now,
                LastUpdatedUserId = 1,
                OurOrganisation = 1,
                ProductId = 2,
                TheirOrganisation = 2
            };
        }
    }
}