﻿using AOM.Repository.MySql;

namespace AOM.Services.Tests.MarketInfoService
{
    using AOM.App.Domain;
    using AOM.App.Domain.Dates;
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;
    using AOM.Repository.MySql.Tests.Aom;
    using AOM.Transport.Events;
    using AOM.Transport.Events.MarketInfos;

    using Moq;

    using NUnit.Framework;
    using AOM.Services.MarketInfoService;

    using System;
    using System.Collections.Generic;
    using System.Transactions;
    using System.Globalization;
    using System.Linq;

    [TestFixture]
    public class MarketInfoServiceTests
    {
        private Mock<IDateTimeProvider> _MockDateTimeProvider;

        private MockAomModel _mockAomModel;

        private MarketInfoService _MarketInfoService;

        private const int FakeUserId = 123;
        private const long FakeProductId = 234;
        private const string FakeInfo = "fake info";
        private readonly DateTime _fakeLastUpdated = new DateTime(2015, 1, 1, 9, 0, 0);

        [SetUp]
        public void Setup()
        {
            _MockDateTimeProvider = new Mock<IDateTimeProvider>();

            _MarketInfoService = new MarketInfoService(_MockDateTimeProvider.Object);

            MarketInfoDto miDto;
            MarketInfoProductChannelDto mipcDto;
            _mockAomModel = new MockAomModel();
            AomDataBuilder
                .WithModel(_mockAomModel)
                .AddMarketInfoItem(FakeUserId, FakeInfo, "P", _fakeLastUpdated, out miDto)
                .AddMarketInfoProductChannel(FakeProductId, miDto.Id, out mipcDto, miDto);
        }

        [Test]
        public void VerifyMarketInfoShouldThrowBusinessRuleExceptionIfTheMarketInfoDateTimeDoesNotMatchThatInTheDatabase
            ()
        {
            var expectedMarketInfo = GenerateMarketInfo(DateTime.Now);
            var message = GenerateMessage();
            var aomMarketInfoAuthenticationResponse = GenerateMarketInfoAuthenticationResponse(expectedMarketInfo, message);

            var exceptionMessage = string.Empty;
            using (var transaction = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                using (var aomModel = new AomModel())
                {
                    var differentDateTime = DateTime.Now.AddYears(-1);
                    MarketInfoDto marketInfo;
                    AomDataBuilder.WithModel(aomModel).AddMarketInfoItem(1, "Info", "P", differentDateTime, out marketInfo);
                    aomMarketInfoAuthenticationResponse.MarketInfo.Id = marketInfo.Id;

                    try
                    {
                        var result = _MarketInfoService.VerifyMarketInfo(aomModel, aomMarketInfoAuthenticationResponse);
                    }
                    catch (BusinessRuleException ex)
                    {
                        exceptionMessage = ex.Message;
                    }

                    transaction.Dispose();
                }
            }


            Assert.That(exceptionMessage, Is.EqualTo("Market Info item has already been updated by another user."));
        }

        [Test]
        public void VerifyMarketInfoShouldReturnAMarketInfoItemIfTheMarketInfoItemIsVerified()
        {
            var matchingDateTime = DateTime.Now;
            var expectedMarketInfo = GenerateMarketInfo(matchingDateTime);
            var message = GenerateMessage();
            var aomMarketInfoAuthenticationResponse = GenerateMarketInfoAuthenticationResponse(expectedMarketInfo, message);

            MarketInfo actualMarketInfoResult = null;
            using (var transaction = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                using (var aomModel = new AomModel())
                {
                    MarketInfoDto marketInfo;
                    AomDataBuilder.WithModel(aomModel).AddMarketInfoItem(1, "Info", "O", matchingDateTime, out marketInfo);
                    aomMarketInfoAuthenticationResponse.MarketInfo.Id = marketInfo.Id;

                    try
                    {
                        actualMarketInfoResult = _MarketInfoService.VerifyMarketInfo(aomModel, aomMarketInfoAuthenticationResponse);
                    }
                    catch (BusinessRuleException)
                    {}

                    transaction.Dispose();
                }
            }

            Assert.That(actualMarketInfoResult, Is.Not.Null);
        }

        [Test]
        public void GetMarketInfoItemByIdReturnsCorrectMarketInfoItem()
        {
            var miDto = _mockAomModel.MarketInfoItems.First();
            var mi = _MarketInfoService.GetMarketInfoItemById(_mockAomModel, miDto.Id);
            Assert.AreEqual(miDto.Id, mi.Id);
        }

        [Test]
        public void GetMarketNonExistantInfoItemByIdReturnsNull()
        {
            const long invalidId = 1234567890;
            var mi = _MarketInfoService.GetMarketInfoItemById(_mockAomModel, invalidId);
            Assert.IsNull(mi);
        }

        [Test]
        public void VoidingMarketInfoShouldSetStatusToVoid()
        {
            var miDto = _mockAomModel.MarketInfoItems.First();
            miDto.MarketInfoStatus = DtoMappingAttribute.GetValueFromEnum(MarketInfoStatus.Pending);
            var message = GenerateMessage(MessageAction.Void);

            var marketInfoItem = _MarketInfoService.GetMarketInfoItemById(_mockAomModel, miDto.Id);
            Assert.IsNotNull(marketInfoItem);
            var authResponse = GenerateMarketInfoAuthenticationResponse(marketInfoItem, message);
            var updated = _MarketInfoService.VoidMarketInfo(_mockAomModel, authResponse);
            Assert.AreEqual(MarketInfoStatus.Void, updated.MarketInfoStatus);
        }

        [Test]
        public void VoidingAnAlreadyVoidedMarketInfoShouldThrowBusinessRuleException()
        {
            var miDto = _mockAomModel.MarketInfoItems.First();
            miDto.MarketInfoStatus = DtoMappingAttribute.GetValueFromEnum(MarketInfoStatus.Void);
            var message = GenerateMessage(MessageAction.Void);
            var marketInfoItem = _MarketInfoService.GetMarketInfoItemById(_mockAomModel, miDto.Id);
            Assert.IsNotNull(marketInfoItem);
            var authResponse = GenerateMarketInfoAuthenticationResponse(marketInfoItem, message);
            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _MarketInfoService.VoidMarketInfo(_mockAomModel, authResponse));
            StringAssert.Contains("item is void", e.Message);
        }

        [Test]
        public void EditMarketInfoSetsTheMarketInfoToTheEditedValues()
        {
            const string updatedInfo = "updated info";
            var originalDto = _mockAomModel.MarketInfoItems.First();
            var message = GenerateMessage(MessageAction.Update);
            var marketInfoItem = _MarketInfoService.GetMarketInfoItemById(_mockAomModel, originalDto.Id);
            Assert.IsNotNull(marketInfoItem);
            marketInfoItem.Info = updatedInfo;
            var authResponse = GenerateMarketInfoAuthenticationResponse(marketInfoItem, message);
            var updated = _MarketInfoService.EditMarketInfo(_mockAomModel, authResponse);
            Assert.AreEqual(updatedInfo, updated.Info);
        }

        [Test]
        public void TryingToUpdateAMarketInfoItemThatDoesNotExistThrowsABusinessRuleException()
        {
            const long invalidId = 1234567890;
            const string updatedInfo = "updated info";
            var originalDto = _mockAomModel.MarketInfoItems.First();
            var message = GenerateMessage(MessageAction.Update);
            var marketInfoItem = _MarketInfoService.GetMarketInfoItemById(_mockAomModel, originalDto.Id);
            Assert.IsNotNull(marketInfoItem);
            marketInfoItem.Info = updatedInfo;
            marketInfoItem.Id = invalidId;
            var authResponse = GenerateMarketInfoAuthenticationResponse(marketInfoItem, message);
            var e =
                Assert.Throws<BusinessRuleException>(
                    () => _MarketInfoService.EditMarketInfo(_mockAomModel, authResponse));
            StringAssert.Contains(invalidId.ToString(CultureInfo.InvariantCulture), e.Message);
            StringAssert.Contains("does not exist", e.Message);
        }

        [Test]
        public void CreateMarketInfoCreatesTheCorrectMarketInfo()
        {
            var mi = GenerateMarketInfo(DateTime.Now);
            var message = GenerateMessage(MessageAction.Create);
            var authResponse = GenerateMarketInfoAuthenticationResponse(mi, message);
            var newMi = _MarketInfoService.CreateMarketInfo(_mockAomModel, authResponse);
            Assert.AreEqual(mi.Info, newMi.Info);
            Assert.AreEqual(mi.MarketInfoStatus, newMi.MarketInfoStatus);
            Assert.AreEqual(mi.OwnerOrganisationId, newMi.OwnerOrganisationId);
        }

        [Test]
        public void CreateMarketInfoCreatesTheCorrectMarketInfoProductChannels()
        {
            _mockAomModel = new MockAomModel();
            var productIds = new List<long> {1, 3, 5};
            var mi = GenerateMarketInfo(DateTime.Now);
            mi.ProductIds = productIds;
            var message = GenerateMessage(MessageAction.Create);
            var authResponse = GenerateMarketInfoAuthenticationResponse(mi, message);
            Assert.That(_mockAomModel.MarketInfoProductChannels, Is.Empty);
            var newMi = _MarketInfoService.CreateMarketInfo(_mockAomModel, authResponse);
            Assert.That(newMi.ProductIds, Is.EquivalentTo(mi.ProductIds));
            var productChannelProductIds = _mockAomModel.MarketInfoProductChannels.Select(c => c.ProductId);
            Assert.That(newMi.ProductIds, Is.EquivalentTo(productChannelProductIds));
        }

        [Test]
        [TestCase(MarketInfoType.Info)]
        [TestCase(MarketInfoType.Bid)]
        [TestCase(MarketInfoType.Ask)]
        [TestCase(MarketInfoType.Deal)]
        public void CreatePlusInfoCreatesTheCorrectMarketInfoType(MarketInfoType marketInfoType)
        {
            _mockAomModel = new MockAomModel();
            var mi = GenerateMarketInfo(DateTime.UtcNow, miType: marketInfoType);
            var message = GenerateMessage(MessageAction.Create);
            var authResponse = GenerateMarketInfoAuthenticationResponse(mi, message);
            var newMi = _MarketInfoService.CreateMarketInfo(_mockAomModel, authResponse);
            Assert.NotNull(newMi);
            Assert.That(newMi.MarketInfoType, Is.EqualTo(marketInfoType));
        }

        private static AomMarketInfoAuthenticationResponse GenerateMarketInfoAuthenticationResponse(
            MarketInfo expectedMarketInfo, Message message)
        {
            return new AomMarketInfoAuthenticationResponse
            {
                MarketInfo = expectedMarketInfo,
                MarketMustBeOpen = true,
                Message = message
            };
        }

        private static Message GenerateMessage(MessageAction messageAction = MessageAction.Verify)
        {
            return new Message
            {
                MessageAction = messageAction,
                ClientSessionInfo = new ClientSessionInfo {OrganisationId = 1, SessionId = "1", UserId = 1},
                MessageBody = "",
                MessageType = MessageType.Info
            };
        }

        private static MarketInfo GenerateMarketInfo(DateTime lastUpdated,
            MarketInfoStatus status = MarketInfoStatus.Active,
            MarketInfoType miType = MarketInfoType.Unspecified)
        {
            return new MarketInfo
            {
                Id = 1,
                Info = "Info",
                LastUpdatedUserId = 1,
                LastUpdated = lastUpdated,
                OwnerOrganisationId = 1,
                MarketInfoStatus = status,
                ProductIds = new List<long>(),
                MarketInfoType = miType
            };
        }
    }
}