using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Services.EmailService;
using AOM.Services.ProductService;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.EmailService
{
    [TestFixture]
    public class EmailContentBuilderCounterpartyEmailBodyTests
    {
        private string _ourEmailBody;
        private string _theirEmailBody;
        private TradingMatrixUpdateNotification _tradingMatrixUpdateNotification;
        Mock<IOrganisationService> _mockOrganisationService;
        Mock<IProductService> _mockProductService;

        [SetUp]
        public void Setup()
        {
            _mockOrganisationService = new Mock<IOrganisationService>();
            _mockProductService = new Mock<IProductService>();
            var ourOrg = new Organisation() {Id = 1, Name = "Our Company"};
            var theirOrg2 = new Organisation() { Id = 2, Name = "Their Company2" };
            var theirOrg3 = new Organisation() { Id = 3, Name = "Their Company3" };

            _mockProductService.Setup(x => x.GetProduct(1)).Returns(new Product() { Name = "Product 1" });
            _mockProductService.Setup(x => x.GetProduct(2)).Returns(new Product { Name = "Product 2" });
            _mockOrganisationService.Setup(x => x.GetOrganisationById(1)).Returns(ourOrg);
            _mockOrganisationService.Setup(x => x.GetOrganisationById(2)).Returns(theirOrg2);
            _mockOrganisationService.Setup(x => x.GetOrganisationById(3)).Returns(theirOrg3);

            _tradingMatrixUpdateNotification = new TradingMatrixUpdateNotification()
            {
                MatrixPermissions = new List<MatrixPermission>()
                {
                    new MatrixPermission
                    {
                        AllowOrDeny = Permission.Allow,
                        BuyOrSell = BuyOrSell.Buy,
                        Id = 1,
                        LastUpdated = DateTime.Now,
                        LastUpdatedUserId = 1,
                        OurOrganisation = ourOrg.Id,
                        ProductId = 1,
                        TheirOrganisation = theirOrg2.Id
                    },
                    new MatrixPermission
                    {
                        AllowOrDeny = Permission.Allow,
                        BuyOrSell = BuyOrSell.Buy,
                        Id = 1,
                        LastUpdated = DateTime.Now,
                        LastUpdatedUserId = 1,
                        OurOrganisation = ourOrg.Id,
                        ProductId = 1,
                        TheirOrganisation = theirOrg3.Id
                    }
                },
                BrokerOrCounterpartyMatrixUpdate = SystemEventType.CounterPartyPermissionChanges
            };

            _ourEmailBody = EmailContentBuilder.CreateEmailBody(_tradingMatrixUpdateNotification, _mockOrganisationService.Object, _mockProductService.Object, ourOrg.Id, false);
            _theirEmailBody = EmailContentBuilder.CreateEmailBody(_tradingMatrixUpdateNotification, _mockOrganisationService.Object, _mockProductService.Object, theirOrg2.Id, true);
        }

        [Test]
        public void ShouldFormatTheCounterPartyPermissionsChangedEmailBodyCorrectlyWithTradePermissionsAmendedText()
        {
            Assert.That(_ourEmailBody, Is.Not.Null);
            Assert.That(_ourEmailBody.Contains("The following trade permissions have been amended in AOM:"));
        }

        [Test]
        public void ShouldFormatTheCounterPartyPermissionsChangedEmailBodyCorrectlyWithProductDescription()
        {
            Assert.That(_ourEmailBody, Is.Not.Null);
            Assert.That(_ourEmailBody.Contains("Product: Product 1"));
        }

        [Test]
        public void ShouldFormatTheCounterPartyPermissionsChangedEmailBodyCorrectlyWithInitiatingOrg()
        {
            Assert.That(_ourEmailBody, Is.Not.Null);
            Assert.That(_ourEmailBody.Contains("Organisation initiating permission amendment: Our Company"));
        }

        [Test]
        public void ShouldFormatTheCounterPartyPermissionsChangedEmailBodyCorrectlyWithPermissionAmmendment()
        {
            Assert.That(_ourEmailBody, Is.Not.Null);
            Assert.That(_ourEmailBody.Contains("Permission Amendment: Our Company allows Buying from Their Company"));
        }

        [Test]
        public void ShouldFormatOurOrgPermissionsChangedEmailBodyCorrectlyWithCorrectNumberOfChanges()
        {
            Assert.That(_ourEmailBody, Is.Not.Null);
            Assert.That(_ourEmailBody.Contains("Permission Amendment: Our Company allows Buying from Their Company"));
            Assert.AreEqual(Regex.Matches(_ourEmailBody, "Permission Amendment:").Count, 2);
        }

        [Test]
        public void ShouldFormatTheirOrgPermissionsChangedEmailBodyCorrectlyWithCorrectNumberOfChanges()
        {
            Assert.That(_theirEmailBody, Is.Not.Null);
            Assert.That(_theirEmailBody.Contains("Permission Amendment: Our Company allows Buying from Their Company"));
            Assert.AreEqual(Regex.Matches(_theirEmailBody, "Permission Amendment:").Count, 1);
        }

        [Test]
        public void ShouldCreateEmailBodyTextWithSellingForChangeThatModifiesSellingPermission()
        {
            _tradingMatrixUpdateNotification.MatrixPermissions[0].AllowOrDeny = Permission.Allow;
            _tradingMatrixUpdateNotification.MatrixPermissions[0].BuyOrSell = BuyOrSell.Sell;

            var emailBody = EmailContentBuilder.CreateEmailBody(_tradingMatrixUpdateNotification, _mockOrganisationService.Object, _mockProductService.Object,
                _tradingMatrixUpdateNotification.MatrixPermissions[0].OurOrganisation, false);

            Assert.That(emailBody, Is.Not.Null);
            Assert.That(emailBody.Contains("Permission Amendment: Our Company allows Selling to Their Company"));
        }

        [Test]
        public void ShouldCreateEmailBodyTextWithDeniesForChangeThatDeniesPermission()
        {
            _tradingMatrixUpdateNotification.MatrixPermissions[0].AllowOrDeny = Permission.Deny;
            _tradingMatrixUpdateNotification.MatrixPermissions[0].BuyOrSell = BuyOrSell.Sell;

            var emailBody = EmailContentBuilder.CreateEmailBody(_tradingMatrixUpdateNotification, _mockOrganisationService.Object, _mockProductService.Object,
                _tradingMatrixUpdateNotification.MatrixPermissions[0].OurOrganisation, false);

            Assert.That(emailBody, Is.Not.Null);
            Assert.That(emailBody.Contains("Permission Amendment: Our Company denies Selling to Their Company"));
        }

        [Test]
        public void ShouldThrowBusinessRuleExceptionWhenCreatingEmailSubjectForUnknownUpdateType()
        {
            var notification = new TradingMatrixUpdateNotification
            {
                BrokerOrCounterpartyMatrixUpdate = (SystemEventType)999
            };
            Assert.Throws<BusinessRuleException>(() => EmailContentBuilder.CreateEmailSubject(notification));
        }

        [Test]
        public void ShouldIncludeLiabilityDisclaimerWhenCreatingNotificationEmail()
        {
            var notification = new TradingMatrixUpdateNotification
            {
                MatrixPermissions = new List<MatrixPermission>() {
                    new MatrixPermission
                    {
                        AllowOrDeny = Permission.Deny,
                        ProductId = 1,
                        OurOrganisation = 1,
                        TheirOrganisation = 2
                    }}
            };
            var emailBody = EmailContentBuilder.CreateEmailBody(notification, _mockOrganisationService.Object, _mockProductService.Object, 1, false);
            StringAssert.Contains(EmailContentBuilder.LiabilityDisclaimer, emailBody);
        }

        [Test]
        [TestCase("VOID reason was 'other: A custom void reason'")]
        [TestCase("VOID reason was 'deal terms amended'")]
        [TestCase("VOID reason was 'other: A reason with 'quotes' of \"both\" kinds in'")]
        public void VoidDealEmailShouldIncludeVoidReason(string voidReason)
        {
            const string emailLinePrefix = "Deal Voided: ";

            var notification = new DealEmailNotification
            {
                DealStatus = DealStatus.Void,
                DealVoidReason = voidReason
            };
            var emailBody = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.Contains(emailLinePrefix + voidReason, emailBody);
        }
    }
}