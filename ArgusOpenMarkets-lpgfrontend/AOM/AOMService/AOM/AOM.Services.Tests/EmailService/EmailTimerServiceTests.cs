﻿using AOM.Services.EmailService;
using AOM.Transport.Events;
using AOM.Transport.Events.Emails;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System.Timers;

namespace AOM.Services.Tests.EmailService
{
    [TestFixture]
    public class EmailTimerServiceTests
    {
        private Mock<IBus> _mockBus;
        private MockTimerFactory _mockTimerFactory;
        private EmailTimerService _service;

        private class MockTimerFactory : ITimerFactory
        {
            public Mock<IEmailTimer> MockTimer { get; private set; }

            public MockTimerFactory()
            {
                MockTimer = new Mock<IEmailTimer>();
            }

            public IEmailTimer CreateTimer(int timeoutMilliseconds)
            {
                return MockTimer.Object;
            }
        }

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockTimerFactory = new MockTimerFactory();
        }

        [TearDown]
        public void TearDown()
        {
            if (_service != null)
            {
                _service.Stop();
                _service = null;
            }
        }

        [Test]
        public void ShouldPublishEmailNotificationCheckOnStart()
        {
            _service = new EmailTimerService(_mockBus.Object, _mockTimerFactory);
            _service.Start();
            _mockBus.Verify(
                b => b.Publish(It.Is<AomSendEmailRequest>(r => r.MessageAction == MessageAction.Check)),
                Times.Once);
        }

        [Test]
        public void ShouldPublishEmailNotificationCheckWhenTimerElapses()
        {
            _service = new EmailTimerService(_mockBus.Object, _mockTimerFactory);
            _service.Start();
            _mockBus.Verify(
                b => b.Publish(It.Is<AomSendEmailRequest>(r => r.MessageAction == MessageAction.Check)),
                Times.Once());
            _mockTimerFactory.MockTimer.Raise(t => t.Elapsed += null, (ElapsedEventArgs)null);
            _mockBus.Verify(
                b => b.Publish(It.Is<AomSendEmailRequest>(r => r.MessageAction == MessageAction.Check)),
                Times.Exactly(2));
        }

        [Test]
        public void StopShouldStopAndDisposeTimer()
        {
            _service = new EmailTimerService(_mockBus.Object, _mockTimerFactory);
            _service.Start();
            _mockTimerFactory.MockTimer.Verify(t => t.Start(), Times.Once);
            _service.Stop();
            _mockTimerFactory.MockTimer.Verify(t => t.Stop(), Times.Once);
            _mockTimerFactory.MockTimer.Verify(t => t.Dispose(), Times.Once);
        }

        [Test]
        public void SecondCallToStartShouldPublishAnotherEmailNotificationCheckAndStartNewTimer()
        {
            _service = new EmailTimerService(_mockBus.Object, _mockTimerFactory);
            _service.Start();
            _mockBus.Verify(
                b => b.Publish(It.Is<AomSendEmailRequest>(r => r.MessageAction == MessageAction.Check)),
                Times.Once);
            _mockTimerFactory.MockTimer.Verify(t => t.Start(), Times.Once);

            _service.Start();

            _mockTimerFactory.MockTimer.Verify(t => t.Stop(), Times.Once);
            _mockTimerFactory.MockTimer.Verify(t => t.Dispose(), Times.Once);
            _mockBus.Verify(
                b => b.Publish(It.Is<AomSendEmailRequest>(r => r.MessageAction == MessageAction.Check)),
                Times.Exactly(2));
            _mockTimerFactory.MockTimer.Verify(t => t.Start(), Times.Exactly(2));
        }
    }
}
