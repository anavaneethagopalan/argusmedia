﻿using System;
using AOM.App.Domain.Entities;
using AOM.Services.EmailService;
using NUnit.Framework;

namespace AOM.Services.Tests.EmailService
{
    [TestFixture]
    public class SmtpServiceTests
    {
        [Test]
        public void Dispose()
        {
            var smtpSvc = new SmtpService();
            Assert.DoesNotThrow(smtpSvc.Dispose);
        }
    }
}
