﻿using System.Collections.Generic;

namespace AOM.Services.Tests.EmailService
{
    using System;
    using App.Domain.Entities;
    using App.Domain.Services;
    using Services.EmailService;
    using Services.ProductService;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class EmailContentBuilderTests
    {
        private const string FakeUrl = "http://notarealwebsite";

        [Test]
        public void CreateEmailBody()
        {
            Assert.NotNull(EmailContentBuilder.CreateEmailBody(new DealEmailNotification()));
            Assert.NotNull(EmailContentBuilder.CreateEmailBody(new ForgotPasswordEmailNotification {Url = ""}));
        }

        [Test]
        public void CreateEmailSubject()
        {
            Assert.NotNull(EmailContentBuilder.CreateEmailSubject(new DealEmailNotification()));
            Assert.NotNull(EmailContentBuilder.CreateEmailSubject(new ForgotPasswordEmailNotification()));
            Assert.NotNull(EmailContentBuilder.CreateEmailSubject(new TradingMatrixUpdateNotification
            {
                BrokerOrCounterpartyMatrixUpdate = SystemEventType.BrokerMatrixChanges
            }));
            Assert.NotNull(EmailContentBuilder.CreateEmailSubject(new TradingMatrixUpdateNotification
            {
                BrokerOrCounterpartyMatrixUpdate = SystemEventType.CounterPartyPermissionChanges
            }));
        }

        [Test]
        public void ShouldReturnEmailSubjectOfArgusMediaAOMCounterpartyPermissionUpdateNotification()
        {
            var emailSubject = EmailContentBuilder.CreateEmailSubject(new TradingMatrixUpdateNotification
            {
                BrokerOrCounterpartyMatrixUpdate = SystemEventType.CounterPartyPermissionChanges
            });
            Assert.That(emailSubject, Is.EqualTo("Argus Media AOM : Counterparty Permission Update Notification"));
        }

        [Test]
        public void CreateForgotPasswordEmailBodyException()
        {
            Assert.Throws<NullReferenceException>(() => EmailContentBuilder.CreateEmailBody(new ForgotPasswordEmailNotification()));
        }

        [Test]
        public void CreateEmailBodyForAssertForCounterpartyMatrixUpdateNotification()
        {
            var mockOrganisationService = new Mock<IOrganisationService>();
            var mockProductService = new Mock<IProductService>();
            var org = new Organisation() {Id = 1, Name = "Test Organisation Name"};
            mockProductService.Setup(x => x.GetProduct(It.IsAny<long>())).Returns(new Product() { Name = "Test" });
            mockOrganisationService.Setup(x => x.GetOrganisationById(It.IsAny<long>())).Returns(org);

            Assert.NotNull(EmailContentBuilder.CreateEmailBody(
                new TradingMatrixUpdateNotification()
                {
                    MatrixPermissions = new List<MatrixPermission>() { new MatrixPermission() { OurOrganisation = org.Id } },
                    BrokerOrCounterpartyMatrixUpdate = SystemEventType.CounterPartyPermissionChanges
                },
                mockOrganisationService.Object,
                mockProductService.Object, org.Id, false));
        }

        [Test]
        public void ShouldIncludeQuantityUnitsWhenCreatingDealNotificationEmail()
        {
            var notification = new DealEmailNotification
            {
                Units = "fakeUnits",
                Quantity = 10000
            };
            var emailBody = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.Contains("Quantity (fakeUnits): 10,000", emailBody);
        }

        [Test]
        public void ShouldIncludeMetaDataWhenCreatingDealNotificationEmailAndItShouldMaintainOrder()
        {
            var notification = new DealEmailNotification
            {
                OrderMetaData = new[] { new MetaData() { DisplayName = "Port", DisplayValue = "Dover" }, new MetaData() { DisplayName = "Tolerance", DisplayValue = "10%" } }
            };
            var emailBody = EmailContentBuilder.CreateEmailBody(notification);

            StringAssert.Contains("Port: Dover", emailBody);
            StringAssert.Contains("Tolerance: 10%", emailBody);

            int firstItemPosition = emailBody.IndexOf(notification.OrderMetaData[0].DisplayName, System.StringComparison.Ordinal);
            int secondItemPosition = emailBody.IndexOf(notification.OrderMetaData[1].DisplayName, System.StringComparison.Ordinal);

            Assert.Greater(secondItemPosition, firstItemPosition, "Expect first item of metadata to be before the second within the email content");
        }

        [Test]
        public void ShouldIncludeLiabilityDisclaimerWhenCreatingDealNotificationEmail()
        {
            var emailBody = EmailContentBuilder.CreateEmailBody(new DealEmailNotification());
            StringAssert.Contains(EmailContentBuilder.LiabilityDisclaimer, emailBody);
        }

        [Test]
        public void ShouldIncludeTheSuppliedUrlWhenCreatingEmailGeneratedByText()
        {
            var text = EmailContentBuilder.AppendStandardAomEmailFooter("", FakeUrl);
            StringAssert.Contains(FakeUrl, text);
        }

        [Test]
        public void ShouldAppendEmailGeneratedToTextToMessage()
        {
            const string initialText = "test";
            var s = initialText;
            s = EmailContentBuilder.AppendStandardAomEmailFooter(s, FakeUrl);
            StringAssert.Contains(initialText, s);
            StringAssert.Contains(FakeUrl, s);
        }

        [Test]
        public void EmailBodyShouldIncludeDealExecutionTimeForExectutedDeal()
        {
            var fakeDealDate = new DateTime(2112, 1, 1, 9, 0, 0, DateTimeKind.Utc);
            var notification = new DealEmailNotification
            {
                DealStatus = DealStatus.Executed,
                DealDate = fakeDealDate
            };
            var text = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.Contains("Deal Time: 09:00:00 (UTC)", text);
        }

        [Test]
        public void EmailBodyShouldIncludeBuyBrokerOrganisationNameIfSet()
        {
            const string fakeBrokerName = "LiarsPoker";
            var notification = new DealEmailNotification
            {
                BuyerBrokerOrganisationName = fakeBrokerName
            };
            var text = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.Contains("Broker Company: " + fakeBrokerName, text);
        }

        [Test]
        public void EmailBodyShouldIncludeSellBrokerOrganisationNameIfSet()
        {
            const string fakeBrokerName = "LiarsPoker";
            var notification = new DealEmailNotification
            {
                SellerBrokerOrganisationName = fakeBrokerName
            };
            var text = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.Contains("Broker Company: " + fakeBrokerName, text);
        }

        [Test]
        public void EmailBodyShouldIncludeBuyerAndSellBrokerOrganisationNamesIfSet()
        {
            const string fakeBuyerBrokerName = "LiarsPokerBuyer";
            const string fakeSellerBrokerName = "LiarsPokerSeller";
            var notification = new DealEmailNotification
            {
                BuyerBrokerOrganisationName = fakeBuyerBrokerName,
                SellerBrokerOrganisationName = fakeSellerBrokerName
            };
            var text = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.Contains("Buyer Broker Company: " + fakeBuyerBrokerName, text);
            StringAssert.Contains("Seller Broker Company: " + fakeSellerBrokerName, text);
        }

        [Test]
        public void EmailBodyShouldNotIncludeBrokerOrganisationNamesIfNotSet()
        {
            var notification = new DealEmailNotification();
            var text = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.DoesNotContain("Broker", text);
        }

        [Test]
        public void ShouldAddDealNotesToInternalDealEmailIfTheyAreNotEmpty()
        {
            const string dealNotes = "some fake deal notes";
            var emailBody = EmailContentBuilder.CreateEmailBody(new DealEmailNotification { DealNotes = dealNotes });
            StringAssert.Contains("Deal Notes", emailBody);
            StringAssert.Contains(dealNotes, emailBody);
        }

        [Test]
        public void ShouldNotAddDealNotesFieldToInternalDealEmailIfTheyAreEmpty()
        {
            var emailBody = EmailContentBuilder.CreateEmailBody(new DealEmailNotification { DealNotes = string.Empty });
            StringAssert.DoesNotContain("Deal Notes", emailBody);
        }

        [Test]
        public void ShouldAddGeneralDisclaimerAsPartOfEmailFooter()
        {
            var emailFooter = EmailContentBuilder.AppendStandardAomEmailFooter("", FakeUrl);
            StringAssert.Contains(EmailContentBuilder.GeneralDisclaimer, emailFooter);
        }

        [Test]
        public void ShouldAddAddressAsPartOfEmailFooter()
        {
            var emailFooter = EmailContentBuilder.AppendStandardAomEmailFooter("", FakeUrl);
            StringAssert.Contains(EmailContentBuilder.ArgusAddress, emailFooter);
        }

        [Test]
        public void ShouldAddSupportDetailsAsPartOfEmailFooter()
        {
            var emailFooter = EmailContentBuilder.AppendStandardAomEmailFooter("", FakeUrl);
            StringAssert.Contains(EmailContentBuilder.AomSupportDetails, emailFooter);
        }

        [Test]
        public void ShouldAddCompanyRegistrationDetailsAsPartOfEmailFooter()
        {
            var emailFooter = EmailContentBuilder.AppendStandardAomEmailFooter("", FakeUrl);
            StringAssert.Contains(EmailContentBuilder.CompanyRegistration, emailFooter);
        }

        [Test]
        public void ShouldAddVatRegistrationDetailsAsPartOfEmailFooter()
        {
            var emailFooter = EmailContentBuilder.AppendStandardAomEmailFooter("", FakeUrl);
            StringAssert.Contains(EmailContentBuilder.VatRegistration, emailFooter);
        }

        [Test]
        public void ShouldUseTheProductSpecificDeliveryPeriodLabelInTheEmail()
        {
            var deliveryPeriodLabel = "Nathans Delivery Period";

            var notification = new DealEmailNotification
            {
                DealStatus = DealStatus.Void,
                DeliveryStartDate = new DateTime(),
                DeliveryEndDate = new DateTime().AddDays(5),
                DeliveryPeriodLabel = deliveryPeriodLabel
            };
            var emailBody = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.Contains(deliveryPeriodLabel, emailBody);
        }

        [Test]
        public void IfTheDeliveryPeriodLabelIsNotSetForAProductShouldUseTheDefault()
        {
            var deliveryPeriodLabel = string.Empty;
            var defaultDeliveryPeriodLabel = "Delivery Period";

            var notification = new DealEmailNotification
            {
                DealStatus = DealStatus.Void,
                DeliveryStartDate = new DateTime(),
                DeliveryEndDate = new DateTime().AddDays(5),
                DeliveryPeriodLabel = deliveryPeriodLabel
            };
            var emailBody = EmailContentBuilder.CreateEmailBody(notification);
            StringAssert.Contains(defaultDeliveryPeriodLabel, emailBody);
        }
    }
}