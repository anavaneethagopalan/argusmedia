﻿using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Services.ProductService;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using EmailSvc = AOM.Services.EmailService;

namespace AOM.Services.Tests.EmailService
{
    [TestFixture]
    public class EmailServiceTests
    {
        private Mock<IOrganisationService> _organizationSvc;
        private Mock<IDateTimeProvider> _dateTimeProvider;
        private EmailSvc.EmailService _emailSvc;
        private List<String> _testEmails;
        private Deal _fakeDeal;
        private AomModel _aomModel;
        private OrderDto _initialOrderDto1;
        private OrderDto _matchingOrderDto1;

        private const string FakeWebsiteUrl = "http://bogus";

        [SetUp]
        public void SetUp()
        {
            const long principalOrganisationId1 = 1;
            const long principalOrganisationId2 = 1;
            const long brokerOrganisationId = 3;

            _organizationSvc = new Mock<IOrganisationService>();
            _dateTimeProvider = new Mock<IDateTimeProvider>();
            _emailSvc = null;

            _organizationSvc.Setup(o => o.GetOrganisationById(principalOrganisationId1)).Returns((Organisation) null);
            _organizationSvc.Setup(o => o.GetOrganisationById(brokerOrganisationId)).Returns(new Organisation() {Id = brokerOrganisationId});

            _testEmails = new List<String>()
            {
                "email1@am.com",
                "email2@am.com",
                "email3@am.com",
            };

            var fakeProductTenor = new ProductTenorDto()
            {
                Id = 1,
                ProductDto = new ProductDto()
                {
                    Id = 1,
                    Name = string.Empty,
                    DeliveryLocations = new List<DeliveryLocationDto>()
                },
                ProductId = 1
            };
            _initialOrderDto1 = new OrderDto()
            {
                Id = 1,
                OrganisationId = principalOrganisationId1,
                BrokerOrganisationId = brokerOrganisationId,
                BrokerRestriction = "A",
                ProductTenorDto = fakeProductTenor,
                MetaData = new MetaDataDto[] {new MetaDataStringDto() {DisplayName = "Delivery Port", DisplayValue = "Dover"}}
            };

            _matchingOrderDto1 = new OrderDto()
            {
                Id = 2,
                OrganisationId = principalOrganisationId2,
                BrokerOrganisationId = null,
                BrokerRestriction = "A",
                ProductTenorDto = fakeProductTenor
            };

            _aomModel = new AomModel()
            {
                Emails = new FakeDbSet<EmailDto>(new[]
                {
                    new EmailDto {Status = "P", Body = "created in test setup"}
                }),
                Orders = new FakeDbSet<OrderDto>(new OrderDto[]
                {
                    _initialOrderDto1,
                    _matchingOrderDto1
                }),
                Products = new FakeDbSet<ProductDto>(new ProductDto[]
                {
                    new ProductDto() {Id = 1}
                }),
            };

            _fakeDeal = new Deal()
            {
                Id = 1,
                InitialOrderId = _initialOrderDto1.Id,
                CreatedCompanyId = 1,
                CreatedDate = DateTime.Now.ToUniversalTime(),
                DealStatus = DealStatus.Pending,
                EnteredByUserId = 100,
                Initial = new Order(),
                LastUpdated = DateTime.UtcNow,
                LastUpdatedUserId = 100,
                Matching = new Order(),
                MatchingOrderId = _matchingOrderDto1.Id,
                Message = "Testing deal emails"
            };
        }

        [Test]
        public void SendAnyOutstandingEmailNotificationsException()
        {
            _emailSvc = new EmailSvc.EmailService(null, null, null, null);
            Assert.Throws<NullReferenceException>(() => _emailSvc.SendAnyOutstandingEmailNotifications(null, null, null));
        }

        [Test]
        public void SendAnyOutstandingEmailNotifications()
        {
            var aomModel = new AomModel
            {
                Emails = new FakeDbSet<EmailDto>(new[]
                {
                    new EmailDto {Status = "P"}
                })
            };
            var smtpSvc = new Mock<EmailSvc.ISmtpService>();
            smtpSvc.Setup(c => c.SendEmail(It.IsAny<Email>()));
            _emailSvc = new EmailSvc.EmailService(null, null, _dateTimeProvider.Object, null);

            Assert.DoesNotThrow(() => _emailSvc.SendAnyOutstandingEmailNotifications(aomModel, smtpSvc.Object, FakeWebsiteUrl));
        }

        [Test]
        public void SendEmailNotificationException()
        {
            _emailSvc = new EmailSvc.EmailService(null, null, null, null);
            Assert.Throws<NullReferenceException>(() => _emailSvc.SendEmailNotification(null, null, 0, null));
        }

        [Test]
        public void SendEmailNotification()
        {
            var aomModel = new AomModel
            {
                Emails = new FakeDbSet<EmailDto>(new[]
                {
                    new EmailDto {Status = "P"}
                })
            };
            var smtpSvc = new Mock<EmailSvc.ISmtpService>();
            smtpSvc.Setup(c => c.SendEmail(It.IsAny<Email>()));
            _emailSvc = new EmailSvc.EmailService(null, null, _dateTimeProvider.Object, null);

            Assert.DoesNotThrow(() => _emailSvc.SendEmailNotification(aomModel, smtpSvc.Object, 0, FakeWebsiteUrl));
        }

        [Test]
        public void CreateDealEmailException()
        {
            _emailSvc = new EmailSvc.EmailService(null, null, null, null);
            Assert.Throws<NullReferenceException>(() => _emailSvc.CreateEmail(null, new DealEmailNotification()));
        }

        [Test]
        public void CreateDealEmail()
        {
            var aomModel = new AomModel
            {
                Emails = new FakeDbSet<EmailDto>(new[]
                {
                    new EmailDto {Status = "P"}
                })
            };

            _emailSvc = new EmailSvc.EmailService(null, null, _dateTimeProvider.Object, null);
            Assert.IsTrue(_emailSvc.CreateEmail(aomModel, new DealEmailNotification()) >= 0);
        }


        [Test]
        public void CreateDealEmailShouldReturnAllEmailIds()
        {
            _organizationSvc.Setup(o => o.GetEmailContactsForEvent(It.IsAny<SystemEventType>(), It.IsAny<long>(), It.IsAny<long>())).Returns(_testEmails).Verifiable();

            _emailSvc = new EmailSvc.EmailService(null, _organizationSvc.Object, _dateTimeProvider.Object, null);
            IEnumerable<long> emailIds = _emailSvc.CreateInternalDealEmails(_aomModel, _fakeDeal);

            var emailIdCount = emailIds.Count();
            Assert.That(emailIdCount, Is.GreaterThan(0));
            Assert.That(emailIdCount, Is.EqualTo(9));
        }

        [Test]
        public void CreateDealEmailShouldIncludeMetaDataFromInitialOrder()
        {
            _organizationSvc.Setup(o => o.GetEmailContactsForEvent(It.IsAny<SystemEventType>(), It.IsAny<long>(), It.IsAny<long>())).Returns(_testEmails).Verifiable();
            _emailSvc = new EmailSvc.EmailService(null, _organizationSvc.Object, _dateTimeProvider.Object, null);

            _aomModel.Emails.ToList().ForEach(e => _aomModel.Emails.Remove(e));
            _emailSvc.CreateInternalDealEmails(_aomModel, _fakeDeal);

            Assert.IsTrue(_aomModel.Emails.ToList().All(e => e.Body.Contains(_initialOrderDto1.MetaData[0].DisplayName) & e.Body.Contains(_initialOrderDto1.MetaData[0].DisplayValue)),
                "Expected all generated emails to refer to metadata");
        }

        [Test]
        public void CreateDealEmailShouldCallOrgServiceMethods()
        {
            _organizationSvc.Setup(o => o.GetEmailContactsForEvent(It.IsAny<SystemEventType>(), It.IsAny<long>(), It.IsAny<long>())).Returns(_testEmails).Verifiable();
            _emailSvc = new EmailSvc.EmailService(null, _organizationSvc.Object, _dateTimeProvider.Object, null);

            _emailSvc.CreateInternalDealEmails(_aomModel, _fakeDeal);
            _organizationSvc.Verify(o => o.GetEmailContactsForEvent(It.IsAny<SystemEventType>(), It.IsAny<long>(), It.IsAny<long>()), Times.Exactly(3));
        }

        [Test]
        public void CreateDealEmailShouldNotSendEmailsToBrokers()
        {
            //setting the broker org id to a non existing one should prevent the broker being found => no emails for broker
            _initialOrderDto1.BrokerOrganisationId = null;
            //fakeDeal.Initial.BrokerOrganisationId = -3;
            _organizationSvc.Setup(o => o.GetEmailContactsForEvent(It.IsAny<SystemEventType>(), It.IsAny<long>(), It.IsAny<long>())).Returns(_testEmails).Verifiable();

            _emailSvc = new EmailSvc.EmailService(null, _organizationSvc.Object, _dateTimeProvider.Object, null);
            IEnumerable<long> emailIds = _emailSvc.CreateInternalDealEmails(_aomModel, _fakeDeal);

            var emailIdsCount = emailIds.Count();
            Assert.That(emailIdsCount, Is.GreaterThan(0));
            Assert.That(emailIdsCount, Is.EqualTo(6));
        }


        [Test]
        public void CreateDealEmailForCoBrokeredDealShouldSendEmailToAllFourPeopleInvolved()
        {
            List<string> coBrokeredEmails = new List<String>()
            {
                "email1@am.com"
            };

            //setting the broker org id to a non existing one should prevent the broker being found => no emails for broker
            _initialOrderDto1.BrokerOrganisationId = 1;
            _initialOrderDto1.UserId = 1;

            _matchingOrderDto1.OrganisationId = 2;
            _matchingOrderDto1.UserId = 2;

            var brokerOrganisation = new Organisation {Id = 100, Address = "Address", Description = "Description"};
            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(m => m.GetUsersOrganisation(It.IsAny<long>())).Returns(brokerOrganisation);

            //fakeDeal.Initial.BrokerOrganisationId = -3;
            _organizationSvc.Setup(o => o.GetEmailContactsForEvent(It.IsAny<SystemEventType>(), It.IsAny<long>(), It.IsAny<long>())).Returns(coBrokeredEmails).Verifiable();
            _organizationSvc.Setup(m => m.GetOrganisationById(It.Is<long>(p => p.Equals(1)))).Returns(new Organisation {Id = 1});
            _organizationSvc.Setup(m => m.GetOrganisationById(It.Is<long>(p => p.Equals(2)))).Returns(new Organisation {Id = 2});

            _initialOrderDto1.BrokerOrganisationId = 1;
            _matchingOrderDto1.BrokerOrganisationId = 2;
            _aomModel.Orders = new FakeDbSet<OrderDto>(new OrderDto[]
            {
                _initialOrderDto1,
                _matchingOrderDto1
            });

            _emailSvc = new EmailSvc.EmailService(mockUserService.Object, _organizationSvc.Object,_dateTimeProvider.Object, null);
            IEnumerable<long> emailIds = _emailSvc.CreateInternalDealEmails(_aomModel, _fakeDeal);

            var emailIdsCount = emailIds.Count();
            Assert.That(emailIdsCount, Is.GreaterThan(0));
            Assert.That(emailIdsCount, Is.EqualTo(4));
        }

        [Test]
        public void CreateResetPasswordEmailException()
        {
            _emailSvc = new EmailSvc.EmailService(null, null, null, null);

            Assert.Throws<NullReferenceException>(() => _emailSvc.CreateEmail(null, new ForgotPasswordEmailNotification()));
        }

        [Test]
        public void CreateResetPasswordEmail()
        {
            var aomModel = new AomModel
            {
                Emails = new FakeDbSet<EmailDto>(new[]
                {
                    new EmailDto {Status = "P"}
                })
            };

            _emailSvc = new EmailSvc.EmailService(null, null, _dateTimeProvider.Object, null);
            Assert.IsTrue(_emailSvc.CreateEmail(aomModel, new ForgotPasswordEmailNotification {Url = string.Empty}) >= 0);
        }

        [Test]
        public void CreateCounterpartyPermissionsMatrixChangeEmail()
        {
            var aomModel = new AomModel
            {
                Emails = new FakeDbSet<EmailDto>(new[]
                {
                    new EmailDto {Status = "P"}
                })
            };

            var mockOrganisationService = new Mock<IOrganisationService>();
            var mockProductService = new Mock<IProductService>();

            mockProductService.Setup(x => x.GetProduct(It.IsAny<long>())).Returns(new Product() {Name = "Test"});
            mockOrganisationService.Setup(x => x.GetOrganisationById(It.IsAny<long>())).Returns(new Organisation() {Name = "Test Organisation Name"});
            mockOrganisationService.Setup(x => x.GetEmailContactsForEvent(It.IsAny<SystemEventType>(), It.IsAny<long>(), It.IsAny<long>()))
                .Returns(new List<string>
                {
                    "test@test.com",
                    "test2@test2.com"
                });


            _emailSvc = new EmailSvc.EmailService(null, mockOrganisationService.Object, _dateTimeProvider.Object, mockProductService.Object);

            Assert.IsTrue(_emailSvc.CreateEmail(aomModel,new TradingMatrixUpdateNotification()
            {
                MatrixPermissions = new List<MatrixPermission>(),
                BrokerOrCounterpartyMatrixUpdate = SystemEventType.CounterPartyPermissionChanges
            }).Count() >= 0);
        }

        [Test]
        public void SendEmailNotificationShouldAppendTheWebsiteUrl()
        {
            // Arrange
            var mockSmtpService = new Mock<EmailSvc.ISmtpService>();
            var mockAomModel = new MockAomModel();
            EmailDto emailDto;
            AomDataBuilder.WithModel(mockAomModel).AddEmail(out emailDto, body: "test");
            _emailSvc = new EmailSvc.EmailService(null, null, _dateTimeProvider.Object, null);

            // Act
            _emailSvc.SendEmailNotification(mockAomModel, mockSmtpService.Object, emailDto.Id, FakeWebsiteUrl);

            // Assert
            mockSmtpService.Verify(s => s.SendEmail(It.Is<Email>(e => e.Body.Contains(FakeWebsiteUrl))));
        }

        [Test]
        [TestCase("TWOPRINCIPALSONLY", 2)]
        [TestCase("MATCHINGBROKER", 3)]
        [TestCase("INITIALBROKER", 3)]
        [TestCase("INITIALBROKERANDMATCHINGBROKER", 4)]
        public void ShouldSendEmailWhenDealDoneToMatchingBrokerOrganisation(string operationMode,
            int numberDealEmailsExpected)
        {
            // Arrange
            var mockUserService = new Mock<IUserService>();
            var mockOrganisationService = new Mock<IOrganisationService>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            var mockProductService = new Mock<IProductService>();

            List<string> emailRecipients = new List<string> {"john.doe@argusmedia.com"};

            // Setup mock objects
            var brokerOrganisation = new Organisation {Id = 100, Address = "Address", Description = "Description"};
            mockUserService.Setup(m => m.GetUsersOrganisation(It.IsAny<long>())).Returns(brokerOrganisation);
            mockOrganisationService.Setup(m => m.GetEmailContactsForEvent(It.IsAny<SystemEventType>(), It.IsAny<long>(), It.IsAny<long>())).Returns(emailRecipients);
            mockOrganisationService.Setup(m => m.GetOrganisationById(It.IsAny<long>())).Returns(brokerOrganisation);

            _emailSvc = new EmailSvc.EmailService(mockUserService.Object, mockOrganisationService.Object, mockDateTimeProvider.Object, mockProductService.Object);

            OrderDto orderDto;
            OrderDto order2Dto;

            int? matchingBrokerId = null;
            int? initialBrokerId = null;
            switch (operationMode)
            {
                case "TWOPRINCIPALSONLY":
                    break;
                case "INITIALBROKER":
                    initialBrokerId = 100;
                    break;
                case "MATCHINGBROKER":
                    matchingBrokerId = 100;
                    break;
                case "INITIALBROKERANDMATCHINGBROKER":
                    initialBrokerId = 100;
                    matchingBrokerId = 101;
                    break;
            }


            var mockModel = new MockAomModel();
            AomDataBuilder.WithModel(mockModel)
                .AddCurrency("USD", "US Dollars")
                .AddDeliveryLocation("London")
                .AddProduct("Product1")
                .AddTenorCode("Tenor1")
                .AddProductTenor("Product1_Tenor1")
                .AddOrder(1, "Product1_Tenor1", "B", out orderDto, "A", 2000, 123, initialBrokerId, null, "A", null, initialBrokerId)
                .AddOrder(2, "Product1_Tenor1", "A", out order2Dto, "A", 2000, 123, matchingBrokerId, null, "A", null, matchingBrokerId)
                .DBContext.SaveChangesAndLog(-1);

            var deal = new Deal
            {
                Id = 1,
                EnteredByUserId = 1,
                InitialOrderId = orderDto.Id,
                MatchingOrderId = order2Dto.Id,
                CreatedCompanyId = 1,
                DealStatus = DealStatus.Executed,
                LastUpdatedUserId = 1
            };

            // Act
            var listIds = _emailSvc.CreateInternalDealEmails(mockModel, deal);

            // Assert
            Assert.That(listIds, Is.Not.Null);
            Assert.That(listIds.Count(), Is.EqualTo(numberDealEmailsExpected));
        }
    }
}