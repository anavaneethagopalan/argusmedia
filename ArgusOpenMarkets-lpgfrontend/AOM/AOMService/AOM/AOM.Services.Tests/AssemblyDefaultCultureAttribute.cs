using System;
using System.Globalization;
using System.Threading;

namespace AOM.Services.Tests
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Assembly)]
    public class AssemblyDefaultCultureAttribute : Attribute
    {
        public AssemblyDefaultCultureAttribute(string cultureCode)
        {
            var currentCulture = new CultureInfo(cultureCode);
            CultureInfo.DefaultThreadCurrentCulture = currentCulture;
            Thread.CurrentThread.CurrentCulture = currentCulture;
        }
    }
}