﻿using System.Collections.Generic;

using AOM.App.Domain.Entities;
using AOM.Services.PermissionService;
using NUnit.Framework;

namespace AOM.Services.Tests.PermissionService
{
    [TestFixture]
    class PermissionsHelperTests
    {
        [Test]
        public void ShouldReturnAnEmptyListOfOrganisationsIfNullPermissionsChangedListPassedIn()
        {
            var organisationAffected = PermissionsHelper.BuildListOfOrganisationsAffected(null);
            Assert.That(organisationAffected, Is.Not.Null);
            Assert.That(organisationAffected.Count, Is.EqualTo(0));
        }

        [Test]
        public void ShouldReturnAnEmptyListOfOrganisationsIfEmptyListOfPermissionsChangedListPassedIn()
        {
            var organisationAffected = PermissionsHelper.BuildListOfOrganisationsAffected(new List<MatrixPermission>());
            Assert.That(organisationAffected, Is.Not.Null);
            Assert.That(organisationAffected.Count, Is.EqualTo(0));
        }

        [Test]
        public void ShouldReturnTwoOrganisationsAffectedIfDifferentOrgIdsPassedInOnSingleMatrixPermission()
        {
            var matrixPermissions = new List<MatrixPermission>();
            matrixPermissions.Add(new MatrixPermission
            {
                OurOrganisation = 1, 
                TheirOrganisation = 2
            });

            var organisationAffected = PermissionsHelper.BuildListOfOrganisationsAffected(matrixPermissions);
            Assert.That(organisationAffected.Count, Is.EqualTo(2));
            Assert.That(organisationAffected.Contains(1), Is.True);
            Assert.That(organisationAffected.Contains(2), Is.True);
        }

        [Test]
        public void ShouldReturnTwoOrganisationsAffectedWithDuplicateValuesDeDuped()
        {
            var matrixPermissions = new List<MatrixPermission>();
            matrixPermissions.Add(new MatrixPermission
            {
                OurOrganisation = 1,
                TheirOrganisation = 2
            });

            matrixPermissions.Add(new MatrixPermission {OurOrganisation = 1, TheirOrganisation = 3});

            var organisationAffected = PermissionsHelper.BuildListOfOrganisationsAffected(matrixPermissions);
            Assert.That(organisationAffected.Count, Is.EqualTo(3));
            Assert.That(organisationAffected.Contains(1), Is.True);
            Assert.That(organisationAffected.Contains(2), Is.True);
            Assert.That(organisationAffected.Contains(3), Is.True);
        }

        [Test]
        public void ShouldReturnTwoProductsForTwoMatrixPermissionsWithDifferentProducts()
        {
            var matrixPermissions = new List<MatrixPermission>();
            matrixPermissions.Add(new MatrixPermission { ProductId = 1 });
            matrixPermissions.Add(new MatrixPermission { ProductId = 2 });

            var products = PermissionsHelper.BuildListOfProductsAffected(matrixPermissions);
            Assert.That(products.Count, Is.EqualTo(2));
            Assert.That(products.Contains(1), Is.True);
            Assert.That(products.Contains(2), Is.True);
        }
    }
}
