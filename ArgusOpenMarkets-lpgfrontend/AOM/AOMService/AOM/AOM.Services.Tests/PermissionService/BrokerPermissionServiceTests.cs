﻿using System;
using System.Collections.Generic;
using System.Threading;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.CrmService;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Services.PermissionService;
using AOM.Transport.Events;
using AOM.Transport.Events.BrokerPermissions;
using AOM.Transport.Events.Emails;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.PermissionService
{
    [TestFixture]
    public class BrokerPermissionServiceTests
    {
        private IBrokerPermissionService _permissionService;
        private MockCrmModel _mockCrmModel;
        private MockAomModel _mockAomModel;
        private Mock<IBus> _mockBus;
        private IOrganisationService _mockOrganisationService;
        private Mock<IErrorService> _mockErrorService;
        private Mock<IPermissionTaskScheduler> _permissionTaskScheduler;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockBus.Setup(mb => mb.PublishAsync(new AomSendEmailRequest() {MessageAction = MessageAction.Send, EmailId = 1}));
            _mockCrmModel = new MockCrmModel();
            _mockAomModel = new MockAomModel();

            Organisation org1 = new Organisation() { Id = 10, Name = "TestOrg1", OrganisationType = OrganisationType.Trading, ShortCode = "ORG1", LegalName = "OrganisationOne" };
            Organisation org2 = new Organisation() { Id = 20, Name = "TestOrg2", OrganisationType = OrganisationType.Trading, ShortCode = "ORG2", LegalName = "OrganisationTwo" };

            _mockCrmModel.Organisations.Add(org1.ToDto());
            _mockCrmModel.Organisations.Add(org2.ToDto());

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var mockEmailService = new Mock<IEmailService>();
            mockEmailService.Setup(mes => mes.CreateEmail(_mockAomModel, It.IsAny<TradingMatrixUpdateNotification>())).Returns(It.IsAny<List<long>>());
            _mockErrorService = new Mock<IErrorService>();
            _mockErrorService.Setup(es => es.LogUserError(It.IsAny<UserError>())).Returns(It.IsAny<string>());
            _mockOrganisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            _permissionService = new BrokerPermissionService(_mockBus.Object, dbContextFactory, _mockOrganisationService, mockEmailService.Object, _mockErrorService.Object);
            _permissionTaskScheduler = new Mock<IPermissionTaskScheduler>();
            _permissionTaskScheduler.Setup(pts => pts.QueueBackgroundWorkItem(new CancellationToken(), It.IsAny<List<MatrixPermission>>()));
        }

        [Test]
        public void UpdatePermissionIsSuccessful()
        {
            long orgId = 10;
            DateTime updateTime = DateTime.UtcNow;
            var newPermission = new MatrixPermission
            {
                LastUpdated = updateTime,
                LastUpdatedUserId = -666,
                BuyOrSell = BuyOrSell.Buy,
                AllowOrDeny = Permission.Allow,
                OurOrganisation = orgId,
                TheirOrganisation = 20,
                ProductId = 1,
            };

            AomBrokerPermissionAuthenticationResponse authenticatedPermission = new AomBrokerPermissionAuthenticationResponse()
            {
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo() { OrganisationId = orgId, UserId = 1 },
                    MessageType = MessageType.BrokerPermission
                },
                BrokerPermissions = new List<MatrixPermission>() { newPermission },
                MarketMustBeOpen = false
            };
            
            var brokerPermissionResponse = _permissionService.UpdatePermissions(authenticatedPermission, orgId);

            Assert.That(brokerPermissionResponse.UpdateSuccessful);
            Assert.That(brokerPermissionResponse.BrokerPermissions[0].LastUpdated > updateTime);
        }

        [Test]
        public void UpdatePermissionAlreadyUpdatedIsRejected()
        {
            long orgId = 10;
            DateTime updateTime = DateTime.UtcNow;
            var newPermission = new MatrixPermission
            {
                LastUpdated = updateTime,
                LastUpdatedUserId = -666,
                BuyOrSell = BuyOrSell.Buy,
                AllowOrDeny = Permission.Allow,
                OurOrganisation = orgId,
                TheirOrganisation = 20,
                ProductId = 1,
            };

            AomBrokerPermissionAuthenticationResponse authenticatedPermission = new AomBrokerPermissionAuthenticationResponse()
            {
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo() { OrganisationId = orgId, UserId = 1 },
                    MessageType = MessageType.BrokerPermission
                },
                BrokerPermissions = new List<MatrixPermission>() { newPermission },
                MarketMustBeOpen = false 
            };

            var brokerPermissionResponse = _permissionService.UpdatePermissions(authenticatedPermission, orgId);

            Assert.That(brokerPermissionResponse.UpdateSuccessful);
            Assert.That(brokerPermissionResponse.BrokerPermissions[0].LastUpdated > updateTime);

            authenticatedPermission.BrokerPermissions[0].LastUpdated = updateTime;
            var brokerPermissionResponse2 = _permissionService.UpdatePermissions(authenticatedPermission, orgId);

            Assert.That(!brokerPermissionResponse2.UpdateSuccessful);
            Assert.That(!string.IsNullOrEmpty(brokerPermissionResponse2.BrokerPermissions[0].UpdateError));
        }

    }
}
