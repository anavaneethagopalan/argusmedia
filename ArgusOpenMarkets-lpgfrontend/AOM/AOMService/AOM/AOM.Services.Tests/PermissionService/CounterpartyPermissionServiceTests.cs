﻿using System;
using System.Collections.Generic;

using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Repository.MySql.Tests.Crm;
using AOM.Services.CrmService;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Services.PermissionService;
using AOM.Transport.Events;
using AOM.Transport.Events.CounterpartyPermissions;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.PermissionService
{
    [TestFixture]
    public class CounterpartyPermissionServiceTests
    {
        private CounterpartyPermissionService _permissionService;
        private MockCrmModel _mockCrmModel;
        private MockAomModel _mockAomModel;
        private Mock<IBus> _mockBus;
        private OrganisationService _mockOrganisationService;
        private Mock<IErrorService> _mockErrorService;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockCrmModel = new MockCrmModel();
            _mockAomModel = new MockAomModel();

            Organisation org1 = new Organisation() { Id = 10, Name = "TestOrg1", OrganisationType = OrganisationType.Trading, ShortCode = "ORG1", LegalName = "OrganisationOne" };
            Organisation org2 = new Organisation() { Id = 20, Name = "TestOrg2", OrganisationType = OrganisationType.Trading, ShortCode = "ORG2", LegalName = "OrganisationTwo" };

            _mockCrmModel.Organisations.Add(org1.ToDto());
            _mockCrmModel.Organisations.Add(org2.ToDto());

            var dbContextFactory = new MockDbContextFactory(_mockAomModel, _mockCrmModel);
            var mockEmailService = new Mock<IEmailService>();
            _mockErrorService = new Mock<IErrorService>();

            _mockErrorService.Setup(es => es.LogUserError(It.IsAny<UserError>())).Returns(It.IsAny<string>());
            _mockOrganisationService = new OrganisationService(dbContextFactory, new BilateralPermissionsBuilder());
            _permissionService = new CounterpartyPermissionService(_mockBus.Object, dbContextFactory, _mockOrganisationService, mockEmailService.Object, _mockErrorService.Object);
        }

        [Test]
        public void UpdatePermissionIsSuccessful()
        {
            long orgId = 10;
            DateTime updateTime = DateTime.UtcNow;
            var newPermission = new MatrixPermission
            {
                LastUpdated = updateTime,
                LastUpdatedUserId = -666,
                BuyOrSell = BuyOrSell.Buy,
                AllowOrDeny = Permission.Allow,
                OurOrganisation = orgId,
                TheirOrganisation = 20,
                ProductId = 1,
            };

            AomCounterpartyPermissionAuthenticationResponse authenticatedPermission = new AomCounterpartyPermissionAuthenticationResponse()
            {
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo() { OrganisationId = orgId, UserId = 1 },
                    MessageType = MessageType.BrokerPermission
                },
                CounterpartyPermissions = new List<MatrixPermission>() { newPermission },
                MarketMustBeOpen = false
            };
            
            var cpPermissionResponse = _permissionService.UpdatePermissions(authenticatedPermission, orgId);

            Assert.That(cpPermissionResponse.UpdateSuccessful);
            Assert.That(cpPermissionResponse.CounterpartyPermissions[0].LastUpdated > updateTime);
        }

        [Test]
        public void UpdatePermissionAlreadyUpdatedIsRejected()
        {
            long orgId = 10;
            DateTime updateTime = DateTime.UtcNow;
            var newPermission = new MatrixPermission
            {
                LastUpdated = updateTime,
                LastUpdatedUserId = -666,
                BuyOrSell = BuyOrSell.Buy,
                AllowOrDeny = Permission.Allow,
                OurOrganisation = orgId,
                TheirOrganisation = 20,
                ProductId = 1,
            };

            AomCounterpartyPermissionAuthenticationResponse authenticatedPermission = new AomCounterpartyPermissionAuthenticationResponse()
            {
                Message = new Message
                {
                    MessageAction = MessageAction.Update,
                    ClientSessionInfo = new ClientSessionInfo() { OrganisationId = orgId, UserId = 1 },
                    MessageType = MessageType.BrokerPermission
                },
                CounterpartyPermissions = new List<MatrixPermission>() { newPermission },
                MarketMustBeOpen = false 
            };

            var cpPermissionResponse = _permissionService.UpdatePermissions(authenticatedPermission, orgId);

            Assert.That(cpPermissionResponse.UpdateSuccessful);
            Assert.That(cpPermissionResponse.CounterpartyPermissions[0].LastUpdated > updateTime);

            authenticatedPermission.CounterpartyPermissions[0].LastUpdated = updateTime;
            var cpPermissionResponse2 = _permissionService.UpdatePermissions(authenticatedPermission, orgId);

            Assert.That(!cpPermissionResponse2.UpdateSuccessful);
            Assert.That(!string.IsNullOrEmpty(cpPermissionResponse2.CounterpartyPermissions[0].UpdateError));
        }

    }
}
