﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AOM.App.Domain;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using Moq;
using NUnit.Framework;

namespace AOM.Services.Tests.ProductMetadataService
{
    [TestFixture]
    public class ProductMetadataServiceTests
    {
        [Test]
        [Category("local")]
        public void ShouldReturnProductMetaDataFromDatabase()
        {
            //Dictionary<long, string> listTextElements = new Dictionary<long, string> { { 1, "AA" }, { 2, "BB" }, { 3, "CC" } };
            List<MetaDataListItemDto> listTextElements = new List<MetaDataListItemDto>
            {
                new MetaDataListItemDto() {Id = 1, ValueText = "PortA", ValueLong = 100, IsDeleted = false},
                new MetaDataListItemDto() {Id = 2, ValueText = "PortB", ValueLong = 101, IsDeleted = false},
                new MetaDataListItemDto() {Id = 3, ValueText = "PortC", ValueLong = 102, IsDeleted = false}
            };

            using (new TransactionScope())
            {
                using (var db = new AomModel())
                {
                    AomDataBuilder.WithModel(db)
                        .AddProduct("TestProduct")
                        .AddProductMetaDataList("MyListForTesting", -999, listTextElements)
                        .AddProductMetaDataItem("TestProduct", "fieldPickList", 1, MetaDataTypes.IntegerEnum, "MyListForTesting")
                        .AddProductMetaDataItem("TestProduct", "field2", 1, MetaDataTypes.String)
                        .DBContext.SaveChangesAndLog(-1);

                    long testProductId = db.Products.Where(p => p.Name == "TestProduct").Select(i => i.Id).Single();

                    var productDefinitionService = new Services.ProductMetadataService.ProductMetadataService(new MockDbContextFactory(db, null), new DateTimeProvider());

                    //ACT
                    var productMetaData = productDefinitionService.GetAllProductMetaData();

                    //ASSERT
                    Assert.IsNotNull(productMetaData);

                    var metaDataToAssert = productMetaData.Single(i => i.ProductId == testProductId);

                    Assert.AreEqual(testProductId, metaDataToAssert.ProductId);
                    Assert.AreEqual(2, metaDataToAssert.Fields.Length);

                    CollectionAssert.AreEquivalent(metaDataToAssert.Fields.Select(i => i.FieldType), new[] { MetaDataTypes.IntegerEnum, MetaDataTypes.String, }, "Expect two fields of these types");

                    foreach (var field in metaDataToAssert.Fields)
                    {
                        Assert.IsNotNull(field.DisplayName);

                        if (field.FieldType == MetaDataTypes.IntegerEnum)
                        {
                            Assert.AreEqual("fieldPickList", field.DisplayName);
                            CollectionAssert.AreEquivalent(
                                ((ProductMetaDataItemEnum)field).MetadataList.FieldList.Select(s => s.DisplayName), listTextElements.Select(l => l.ValueText));
                        }
                        else
                        {
                            Assert.AreEqual("field2", field.DisplayName);
                        }
                    }
                }
            }
        }

        [Test]
        public void ShouldReturnProductMetaDataForRequestedProductIdAndInDisplayOrder()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddProduct("FakeProduct2", 20, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("ProductTenor 1", 10)
                .AddProductTenor("ProductTenor 2", 20)
                .AddProductMetaDataItem("FakeProduct1", "FieldA", 1, MetaDataTypes.IntegerEnum)
                .AddProductMetaDataItem("FakeProduct2", "FieldC", 2, MetaDataTypes.IntegerEnum)
                .AddProductMetaDataItem("FakeProduct2", "FieldD", 3, MetaDataTypes.IntegerEnum)
                .AddProductMetaDataItem("FakeProduct2", "FieldB", 1, MetaDataTypes.IntegerEnum);

            var productService = new Services.ProductMetadataService.ProductMetadataService(
                new MockDbContextFactory(aomModel, null),
                Mock.Of<IDateTimeProvider>());

            var productMetaData = productService.GetProductMetaData(20);

            Assert.That(productMetaData, Is.Not.Null);
            Assert.That(productMetaData.ProductId, Is.EqualTo(20));
            Assert.That(productMetaData.Fields.Length, Is.EqualTo(3));
            Assert.That(productMetaData.Fields[0].DisplayName, Is.EqualTo("FieldB"));
            Assert.That(productMetaData.Fields[1].DisplayName, Is.EqualTo("FieldC"));
            Assert.That(productMetaData.Fields[2].DisplayName, Is.EqualTo("FieldD"));
        }

        [Test]
        public void ShouldNotReturnDeletedMetadataItems()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 1, MetaDataTypes.String)
                .AddProductMetaDataItem("FakeProduct1", "Delivary Description-deleted", 2, MetaDataTypes.String, isDeleted: true);

            var productService = new Services.ProductMetadataService.ProductMetadataService(
                new MockDbContextFactory(aomModel, null),
                Mock.Of<IDateTimeProvider>());

            var productMetaData = productService.GetProductMetaData(10);

            Assert.That(productMetaData, Is.Not.Null);
            Assert.That(productMetaData.ProductId, Is.EqualTo(10));
            Assert.That(productMetaData.Fields.Length, Is.EqualTo(1));
            Assert.That(productMetaData.Fields[0].DisplayName, Is.EqualTo("Delivery Description"));
        }

        [Test]
        public void ShouldAddMetadataItemToDatabase()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10);

            var creationTime = new DateTime(2016, 8, 17, 12, 0, 0);
            var dateTimeProviderMock = new Mock<IDateTimeProvider>();
            dateTimeProviderMock.Setup(x => x.UtcNow).Returns(creationTime);

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               dateTimeProviderMock.Object);

            var metadataItem = new ProductMetaDataItemString { DisplayName = "Delivery Description", ProductId = 10, ValueMinimum = 1, ValueMaximum = 100 };

            productService.AddProductMetadataItem(metadataItem, 123);

            Assert.That(aomModel.ProductMetaDataItems.Count(), Is.EqualTo(1));
            var metadataItemDto = aomModel.ProductMetaDataItems.FirstOrDefault();
            Assert.That(metadataItemDto.DisplayName, Is.EqualTo("Delivery Description"));
            Assert.That(metadataItemDto.LastUpdated, Is.EqualTo(creationTime));
            Assert.That(metadataItemDto.IsDeleted, Is.False);
        }

        [TestCase(1, 100, "", TestName = "ShouldNotAddMetadataItemWithEmptyDisplayName")]
        [TestCase(100, 1, "Delivery Description", TestName = "ShouldNotAddMetadataItemWithMinimumGreaterThanMaximum")]
        [TestCase(10, 10, "Delivery Description", TestName = "ShouldNotAddMetadataItemWithMinimumEqualToMaximum")]
        public void ShouldNotAddWrongStringMetadataItemToDatabase(int minimum, int maximum, string displayName)
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10);

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               Mock.Of<IDateTimeProvider>());

            var metadataItem = new ProductMetaDataItemString { ProductId = 10, ValueMinimum = minimum, ValueMaximum = maximum, DisplayName = displayName };

            Assert.Throws<BusinessRuleException>(() => productService.AddProductMetadataItem(metadataItem, 123));
        }

        [Test]
        public void ShouldAddProductMetadataItemWithCorrectDisplayOrder()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 0, MetaDataTypes.String)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Location", 1, MetaDataTypes.String)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Port", 3, MetaDataTypes.String);

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               Mock.Of<IDateTimeProvider>());

            productService.AddProductMetadataItem(new ProductMetaDataItemString
            {
                DisplayName = "Delivery Description", 
                ProductId = 10, 
                ValueMinimum = 1, 
                ValueMaximum = 100
            }, 123);

            Assert.That(aomModel.ProductMetaDataItems.ElementAt(3).DisplayOrder, Is.EqualTo(4));
        }

        [Test]
        public void ShouldDeleteMetadataItemFromDatabase()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 1, MetaDataTypes.String);

            var metadataItemDto = aomModel.ProductMetaDataItems.FirstOrDefault();

            var removalTime = new DateTime(2016, 8, 17, 12, 0, 0);
            var dateTimeProviderMock = new Mock<IDateTimeProvider>();
            dateTimeProviderMock.Setup(x => x.UtcNow).Returns(removalTime);

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               dateTimeProviderMock.Object);

            productService.DeleteProductMetadataItem(metadataItemDto.Id, 123);

            Assert.That(aomModel.ProductMetaDataItems.Count(), Is.EqualTo(1));
            Assert.That(metadataItemDto.DisplayName, Is.EqualTo("Delivery Description"));
            Assert.That(metadataItemDto.LastUpdated, Is.EqualTo(removalTime));
            Assert.That(metadataItemDto.IsDeleted, Is.True);
        }

        [Test]
        public void ShouldUpdateMetadataItemFromDatabase()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 1, MetaDataTypes.String);

            var existingMetadataItemDto = aomModel.ProductMetaDataItems.FirstOrDefault();

            var updateTime = new DateTime(2016, 8, 17, 12, 0, 0);
            var dateTimeProviderMock = new Mock<IDateTimeProvider>();
            dateTimeProviderMock.Setup(x => x.UtcNow).Returns(updateTime);

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               dateTimeProviderMock.Object);

            var metadataItem = new ProductMetaDataItemEnum
            {
                DisplayName = "Delivery Port", 
                ProductId = 10, 
                Id = existingMetadataItemDto.Id,
                MetadataList = new MetaDataList(1, string.Empty, new MetaDataListItem<long>[0])
            };

            productService.UpdateProductMetadataItem(metadataItem, 123);

            Assert.That(existingMetadataItemDto.DisplayName, Is.EqualTo("Delivery Port"));
            Assert.That(existingMetadataItemDto.MetaDataTypeId, Is.EqualTo(MetaDataTypes.IntegerEnum));
            Assert.That(existingMetadataItemDto.MetaDataListId, Is.EqualTo(metadataItem.MetadataList.Id));
            Assert.That(existingMetadataItemDto.LastUpdated, Is.EqualTo(updateTime));
            Assert.That(existingMetadataItemDto.IsDeleted, Is.False);
        }

        [Test]
        public void ShouldReturnMetadataListsFromDatabase()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto { Id = 1, ValueText = "Port A", ValueLong = 100, DisplayOrder = 3 },
                    new MetaDataListItemDto { Id = 2, ValueText = "Port B", ValueLong = 101, DisplayOrder = 2 },
                    new MetaDataListItemDto { Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1 }
                })
                .AddProductMetaDataList("Bitumen Grade", 2, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto { Id = 4, ValueText = "20/30", ValueLong = 1 },
                    new MetaDataListItemDto { Id = 5, ValueText = "50/70", ValueLong = 2 },
                    new MetaDataListItemDto { Id = 6, ValueText = "160/220", ValueLong = 3, IsDeleted = true }
                });

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            var metadataLists = productMetadataService.GetProductMetadataLists();
            Assert.That(metadataLists, Is.Not.Null);
            Assert.That(metadataLists.Count, Is.EqualTo(2));
            Assert.That(metadataLists[0].Description, Is.EqualTo("Singapore Ports"));
            Assert.That(metadataLists[0].FieldList.Length, Is.EqualTo(3));
            Assert.That(metadataLists[0].FieldList[0].DisplayName, Is.EqualTo("Port C"));
            Assert.That(metadataLists[0].FieldList[1].DisplayName, Is.EqualTo("Port B"));
            Assert.That(metadataLists[0].FieldList[2].DisplayName, Is.EqualTo("Port A"));

            Assert.That(metadataLists[1].Description, Is.EqualTo("Bitumen Grade"));
            Assert.That(metadataLists[1].FieldList.Length, Is.EqualTo(2));
            Assert.That(metadataLists[1].FieldList[0].DisplayName, Is.EqualTo("20/30"));
            Assert.That(metadataLists[1].FieldList[1].DisplayName, Is.EqualTo("50/70"));
        }

        [Test]
        public void ShouldNotReturnDeletedMetadataListItemsFromDatabase()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto { Id = 1, ValueText = "Port A", ValueLong = 100, DisplayOrder = 3, IsDeleted = true },
                    new MetaDataListItemDto { Id = 2, ValueText = "Port B", ValueLong = 101, DisplayOrder = 2 },
                    new MetaDataListItemDto { Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1, IsDeleted = true }
                });

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            var metadataLists = productMetadataService.GetProductMetadataLists();
            Assert.That(metadataLists, Is.Not.Null);
            Assert.That(metadataLists.Count, Is.EqualTo(1));
            Assert.That(metadataLists[0].FieldList.Length, Is.EqualTo(1));
            Assert.That(metadataLists[0].FieldList[0].DisplayName, Is.EqualTo("Port B"));
        }

        [Test]
        public void ShouldNotReturnDeletedMetadataListsFromDatabase()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto { Id = 1, ValueText = "Port A", ValueLong = 100, DisplayOrder = 1 }
                });

            aomModel.MetaDataLists.ElementAt(0).IsDeleted = true;

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            var metadataLists = productMetadataService.GetProductMetadataLists();
            Assert.That(metadataLists, Is.Not.Null);
            Assert.That(metadataLists.Count, Is.EqualTo(0));
        }

        [Test]
        public void ShouldUpdateProductMetadataItemsDisplayOrder()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 0, MetaDataTypes.String)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Location", 0, MetaDataTypes.String)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Port", 0, MetaDataTypes.String);

            var productMetadataItems = aomModel.ProductMetaDataItems.ToArray();

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               Mock.Of<IDateTimeProvider>());

            productService.UpdateProductMetadataItemsDisplayOrder(10, new[] { productMetadataItems[2].Id, productMetadataItems[1].Id, productMetadataItems[0].Id }, 123);

            Assert.That(productMetadataItems[0].DisplayOrder, Is.EqualTo(3));
            Assert.That(productMetadataItems[1].DisplayOrder, Is.EqualTo(2));
            Assert.That(productMetadataItems[2].DisplayOrder, Is.EqualTo(1));
        }

        [Test]
        public void ShouldNotUpdateNonexistentProductMetadataItemsDisplayOrder()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 0, MetaDataTypes.String);

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               Mock.Of<IDateTimeProvider>());

            Assert.Throws<BusinessRuleException>(() => productService.UpdateProductMetadataItemsDisplayOrder(10, new long[] { 12345 }, 123));
        }

        [Test]
        public void ShouldNotUpdateProductMetadataItemsDisplayOrderPartially()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 0, MetaDataTypes.String)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Location", 0, MetaDataTypes.String)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Port", 0, MetaDataTypes.String);

            var productMetadataItems = aomModel.ProductMetaDataItems.ToArray();

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               Mock.Of<IDateTimeProvider>());

            Assert.Throws<BusinessRuleException>(() => productService.UpdateProductMetadataItemsDisplayOrder(10, new[] { productMetadataItems[1].Id, productMetadataItems[0].Id }, 123));
        }

        [Test]
        public void ShouldNotUpdateDeletedProductMetadataItemDisplayOrder()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 0, MetaDataTypes.String)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Location", 0, MetaDataTypes.String, isDeleted: true)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Port", 0, MetaDataTypes.String);

            var productMetadataItems = aomModel.ProductMetaDataItems.ToArray();

            var productService = new Services.ProductMetadataService.ProductMetadataService(
               new MockDbContextFactory(aomModel, null),
               Mock.Of<IDateTimeProvider>());

            productService.UpdateProductMetadataItemsDisplayOrder(10, new[] { productMetadataItems[0].Id, productMetadataItems[2].Id }, 123);

            Assert.That(productMetadataItems[1].DisplayOrder, Is.EqualTo(0));
        }

        [Test]
        public void ShouldUpdateExistingMetadataList()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto { Id = 1, ValueText = "Port A", ValueLong = 100, DisplayOrder = 3 },
                    new MetaDataListItemDto { Id = 2, ValueText = "Port B", ValueLong = 101, DisplayOrder = 2 },
                    new MetaDataListItemDto { Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1 }
                });

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            var updatedMetadataList = new MetaDataList(1, "Updated Singapore Ports", new[]
            {
                new MetaDataListItem<long>(3, 103, "Port C"), 
                new MetaDataListItem<long>(2, 101, "Updated Port B"),
                new MetaDataListItem<long>(4, 104, "Port D") 
            });

            productMetadataService.UpdateProductMetadataLists(new[] { updatedMetadataList }, 123);

            var productMetadataList = aomModel.MetaDataLists.FirstOrDefault();
            Assert.That(productMetadataList, Is.Not.Null);
            Assert.That(productMetadataList.Description, Is.EqualTo("Updated Singapore Ports"));
            Assert.That(productMetadataList.MetaDataListItems.Count, Is.EqualTo(4));
            var metadataListItems = productMetadataList.MetaDataListItems.ToArray();

            Assert.That(metadataListItems[0].Id, Is.EqualTo(1));
            Assert.That(metadataListItems[0].IsDeleted, Is.True);

            Assert.That(metadataListItems[1].Id, Is.EqualTo(2));
            Assert.That(metadataListItems[1].ValueText, Is.EqualTo("Updated Port B"));
            Assert.That(metadataListItems[1].ValueLong, Is.EqualTo(101));
            Assert.That(metadataListItems[1].DisplayOrder, Is.EqualTo(2));
            Assert.That(metadataListItems[1].IsDeleted, Is.False);

            Assert.That(metadataListItems[2].Id, Is.EqualTo(3));
            Assert.That(metadataListItems[2].ValueText, Is.EqualTo("Port C"));
            Assert.That(metadataListItems[2].ValueLong, Is.EqualTo(103));
            Assert.That(metadataListItems[2].DisplayOrder, Is.EqualTo(1));
            Assert.That(metadataListItems[2].IsDeleted, Is.False);

            Assert.That(metadataListItems[3].Id, Is.EqualTo(4));
            Assert.That(metadataListItems[3].ValueText, Is.EqualTo("Port D"));
            Assert.That(metadataListItems[3].ValueLong, Is.EqualTo(104));
            Assert.That(metadataListItems[3].DisplayOrder, Is.EqualTo(3));
            Assert.That(metadataListItems[3].IsDeleted, Is.False);
        }

        [Test]
        public void ShouldDeleteExistingMetadataList()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto { Id = 1, ValueText = "Port A", ValueLong = 100, DisplayOrder = 3 },
                    new MetaDataListItemDto { Id = 2, ValueText = "Port B", ValueLong = 101, DisplayOrder = 2 },
                    new MetaDataListItemDto { Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1 }
                });

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            productMetadataService.UpdateProductMetadataLists(new MetaDataList[0], 123);

            var productMetadataList = aomModel.MetaDataLists.FirstOrDefault();
            Assert.NotNull(productMetadataList);
            Assert.That(productMetadataList.IsDeleted, Is.True);
            Assert.That(productMetadataList.MetaDataListItems.Count, Is.EqualTo(3));
            var metadataListItems = productMetadataList.MetaDataListItems.ToArray();
            Assert.That(metadataListItems[0].IsDeleted, Is.True);
            Assert.That(metadataListItems[1].IsDeleted, Is.True);
            Assert.That(metadataListItems[2].IsDeleted, Is.True);
        }

        [Test]
        public void ShouldUpdateExistingMetadataLists()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto { Id = 1, ValueText = "Port A", ValueLong = 100 }
                })
                .AddProductMetaDataList("Bitumen Grade", 2, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto { Id = 2, ValueText = "20/30", ValueLong = 1 }
                });

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                new MockDbContextFactory(aomModel, null),
                Mock.Of<IDateTimeProvider>());

            var updatedMetadataLists = new[]
            {
                new MetaDataList(2, "Bitumen Grade", new[] { new MetaDataListItem<long>(2, 1, "20/30") }),
                new MetaDataList(3, "English Tenses", new[] { new MetaDataListItem<long>(3, 100, "Present Perfect") }) 
            };

            productMetadataService.UpdateProductMetadataLists(updatedMetadataLists, 123);

            var metadataLists = aomModel.MetaDataLists.ToArray();
            Assert.That(metadataLists[0].Id, Is.EqualTo(1));
            Assert.That(metadataLists[0].IsDeleted, Is.True);

            Assert.That(metadataLists[1].Id, Is.EqualTo(2));
            Assert.That(metadataLists[1].Description, Is.EqualTo("Bitumen Grade"));
            Assert.That(metadataLists[1].IsDeleted, Is.False);
            
            Assert.That(metadataLists[2].Id, Is.EqualTo(3));
            Assert.That(metadataLists[2].Description, Is.EqualTo("English Tenses"));
            Assert.That(metadataLists[2].IsDeleted, Is.False);

             var listItem = metadataLists[2].MetaDataListItems.FirstOrDefault();
            Assert.NotNull(listItem);
            Assert.That(listItem.Id, Is.EqualTo(3));
            Assert.That(listItem.ValueLong, Is.EqualTo(100));
            Assert.That(listItem.ValueText, Is.EqualTo("Present Perfect"));
            Assert.That(listItem.DisplayOrder, Is.EqualTo(1));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ShouldNotUpdateMetadataListWithEmptyDescription(bool metadataListAlreadyExists)
        {
            var aomModel = new MockAomModel();

            if (metadataListAlreadyExists)
            {
                AomDataBuilder.WithModel(aomModel)
                    .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                    {
                        new MetaDataListItemDto {Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1}
                    });
            }

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            var updatedMetadataList = new MetaDataList(1, string.Empty, new[]
            {
                new MetaDataListItem<long>(3, 103, "Port C")
            });

            Assert.Throws<BusinessRuleException>(() => productMetadataService.UpdateProductMetadataLists(new[] { updatedMetadataList }, 123));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ShouldNotUpdateMetadataListWithNoItems(bool metadataListAlreadyExists)
        {
            var aomModel = new MockAomModel();

            if (metadataListAlreadyExists)
            {
                AomDataBuilder.WithModel(aomModel)
                    .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                    {
                        new MetaDataListItemDto { Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1 }
                    });
            }

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            var updatedMetadataList = new MetaDataList(1, "Singapore Ports", new MetaDataListItem<long>[0]);

            Assert.Throws<BusinessRuleException>(() => productMetadataService.UpdateProductMetadataLists(new[] { updatedMetadataList }, 123));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ShouldNotUpdateMetadataListWithItemsThatHasNoDisplayName(bool metadataListAlreadyExists)
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                    .AddProductMetaDataList("Singapore Ports", 1, 
                    metadataListAlreadyExists ?
                    new List<MetaDataListItemDto> { new MetaDataListItemDto { Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1 } }:
                    new List<MetaDataListItemDto>());

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            var updatedMetadataList = new MetaDataList(1, "Singapore Ports", new[]
            {
                new MetaDataListItem<long>(3, 103, string.Empty)
            });

            Assert.Throws<BusinessRuleException>(() => productMetadataService.UpdateProductMetadataLists(new[] { updatedMetadataList }, 123));
        }

        [TestCase]
        public void ShouldNotDeleteMetadataListIfItIsReferencedByMetadataItem()
        {
            var aomModel = new MockAomModel();

            AomDataBuilder.WithModel(aomModel)
                .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto>
                {
                    new MetaDataListItemDto {Id = 3, ValueText = "Port C", ValueLong = 102, DisplayOrder = 1}
                })
                .AddDeliveryLocation("FakeDeliveryLocation")
                .AddProduct("FakeProduct1", 10, minPrice: 100m, maxPrice: 3000m, priceIncrement: 0.5m)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Tenor 1", 10)
                .AddProductMetaDataItem("FakeProduct1", "Delivery Description", 0, MetaDataTypes.IntegerEnum, "Singapore Ports");

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            Assert.Throws<BusinessRuleException>(() => productMetadataService.UpdateProductMetadataLists(new MetaDataList[0], 123));
        }

        [TestCase]
        public void ShouldRecreateMetadataListItemIfSameMetadataItemIsAdded()
        {
            var aomModel = new MockAomModel();
            AomDataBuilder.WithModel(aomModel)
                .AddProductMetaDataList("Singapore Ports", 1, new List<MetaDataListItemDto> 
                {
                    new MetaDataListItemDto { Id = 1, ValueText = "Port A", ValueLong = 100, DisplayOrder = 3, IsDeleted = true
                }});

            var productMetadataService = new Services.ProductMetadataService.ProductMetadataService(
                 new MockDbContextFactory(aomModel, null),
                 Mock.Of<IDateTimeProvider>());

            var updatedMetadataList = new MetaDataList(1, "Singapore Ports", new[] { new MetaDataListItem<long>(3, 100, "Port A") });

            productMetadataService.UpdateProductMetadataLists(new[] { updatedMetadataList }, 123);

            var metadataList = aomModel.MetaDataLists.FirstOrDefault();
            Assert.NotNull(metadataList);

            var deletedMetadataListItem = metadataList.MetaDataListItems.FirstOrDefault();
            Assert.NotNull(deletedMetadataListItem);

            Assert.That(deletedMetadataListItem.IsDeleted, Is.False);
            Assert.That(deletedMetadataListItem.DisplayOrder, Is.EqualTo(1));
        }
    }
}
