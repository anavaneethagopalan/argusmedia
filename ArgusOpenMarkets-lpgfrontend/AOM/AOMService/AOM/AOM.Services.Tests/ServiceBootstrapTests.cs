﻿namespace AOM.Services.Tests
{
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class ServiceBootstrapTests
    {
        [Test]
        public void ShouldInitialiseWithoutErrors()
        {
            string message = string.Empty;

            try
            {
                var serviceBootstrap = new ServiceBootstrap();
                serviceBootstrap.Load();
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }

            Assert.That(message, Is.Empty);
        }
    }
}