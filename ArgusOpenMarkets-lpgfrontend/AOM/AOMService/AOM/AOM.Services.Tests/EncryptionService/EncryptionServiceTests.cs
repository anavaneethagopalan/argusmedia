﻿using NUnit.Framework;

using AOM.App.Domain.Entities;

namespace AOM.Services.EncryptionService.Tests
{
    [TestFixture]
    class EncryptionServiceTests
    {
        [Test]
        public void ShouldHashAValue()
        {
            var encryptionConfig = new EncryptionConfig();
            var valueToHash = "value";
            var encryptionService = new EncryptionService(encryptionConfig);

            var user = new User{Id = 1};
            var hashedValue = encryptionService.Hash(user, valueToHash);

            Assert.That(hashedValue, Is.Not.EqualTo(valueToHash));
        }

        [Test]
        public void ShouldEncodeANumber()
        {
            var encryptionConfig = new EncryptionConfig();
            var encryptionService = new EncryptionService(encryptionConfig);

            var encodedValue = encryptionService.Encode(100);

            Assert.That(encodedValue, Is.Not.Empty);
            Assert.That(encodedValue, Is.EqualTo("YZZ"));
        }

        [Test]
        public void ShouldDecodeAString()
        {
            var encryptionConfig = new EncryptionConfig();
            var encryptionService = new EncryptionService(encryptionConfig);

            var decodedValue = encryptionService.Decode("YZZ");

            Assert.That(decodedValue, Is.EqualTo(100));
        }

        [Test]
        public void ShouldEncodeThenDecodeBackToTheOriginalValue()
        {
            var encryptionConfig = new EncryptionConfig();
            var encryptionService = new EncryptionService(encryptionConfig);

            var encodedValue = encryptionService.Encode(100);
            var decodedValue = encryptionService.Decode(encodedValue);

            Assert.That(decodedValue, Is.EqualTo(100));
        }
    }
}
