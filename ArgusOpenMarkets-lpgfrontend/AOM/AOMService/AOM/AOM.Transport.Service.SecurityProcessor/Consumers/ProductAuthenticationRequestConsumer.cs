﻿using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.SecurityProcessor.Consumers
{
    internal class ProductAuthenticationRequestConsumer : IConsumer
    {
        private readonly IBus _aomBus;
        private readonly IErrorService _errorService;
        private readonly IProductAuthenticationService _productAuthenticationService;

        public ProductAuthenticationRequestConsumer(
            IProductAuthenticationService productAuthenticationService,
            IBus aomBus, IErrorService errorService)
        {
            _productAuthenticationService = productAuthenticationService;
            _errorService = errorService;
            _aomBus = aomBus;
        }

        internal string SubscriptionId
        {
            get { return string.Empty; }
        }

        public void Start()
        {
            _aomBus.Subscribe<AomProductPurgeOrdersAuthenticationRequest>(
                SubscriptionId,
                ConsumeProductAuthenticationRequest
                    <AomProductPurgeOrdersAuthenticationRequest, AomProductPurgeOrdersAuthenticationResponse>);
            _aomBus.Subscribe<AomMarketStatusChangeAuthenticationRequest>(
                SubscriptionId,
                ConsumeProductAuthenticationRequest
                    <AomMarketStatusChangeAuthenticationRequest, AomMarketStatusChangeAuthenticationResponse>);
            _aomBus.Subscribe<AomProductConfigurePurgeAuthenticationRequest>(
                SubscriptionId,
                ConsumeProductAuthenticationRequest
                    <AomProductConfigurePurgeAuthenticationRequest, AomProductConfigurePurgeAuthenticationResponse>);
            _aomBus.Subscribe<AomProductTenorAuthenticationRequest>(
                SubscriptionId,
                ConsumeProductTenorAuthenticationRequest);
        }

        internal void ConsumeProductAuthenticationRequest<TReq, TResp>(TReq authenticationRequest)
            where TReq : AomProductRequest where TResp : AomProductResponse, new()
        {
            Message productResponse;
            try
            {
                productResponse = _productAuthenticationService.AuthenticateProductRequest(authenticationRequest);

                if (productResponse.MessageType==MessageType.Error)
                {
                    Log.Warn(String.Format("{0}: Authorisation was denied to user id {1} while attempting {2}/{3}",
                          ToString(),
                          authenticationRequest.ClientSessionInfo.UserId,
                          authenticationRequest.EventName,
                          authenticationRequest.MessageAction));
                }
            }
            catch (Exception error)
            {
                var errorMessage =
                    _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "OrderAuthenticationRequestConsumer.ConsumeProductAuthenticationRequest",
                        Error = error,
                        AdditionalInformation = "Error authenticating " + authenticationRequest.EventName + ": "
                    });

                productResponse = new Message
                {
                    ClientSessionInfo = authenticationRequest.ClientSessionInfo,
                    MessageAction = MessageAction.Create,
                    MessageType = MessageType.Error,
                    MessageBody = errorMessage
                };
            }
            try
            {
                var response = new TResp {Message = productResponse, Product = authenticationRequest.Product};
                response.Message.MessageBody = response.Message.MessageBody.ToJsonCamelCase();
                _aomBus.Publish(response);
            }
            catch (Exception error)
            {
                var errorMessage =
                    _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "OrderAuthenticationRequestConsumer.ConsumeProductAuthenticationRequest",
                        Error = error,
                        AdditionalInformation = "Error publishing to bus"
                    });
            }
        }

        internal void ConsumeProductTenorAuthenticationRequest(
            AomProductTenorAuthenticationRequest authenticationRequest)
        {
            Message productTenorResponse;
            try
            {
                productTenorResponse =
                    _productAuthenticationService.AuthenticateProductTenorRequest(authenticationRequest);

                if (productTenorResponse.MessageType == MessageType.Error)
                {
                    Log.Warn(String.Format("{0}: Authorisation was denied to user id {1} while attempting {2}/{3}",
                          ToString(),
                          authenticationRequest.ClientSessionInfo.UserId,
                          authenticationRequest.EventName,
                          authenticationRequest.MessageAction));
                }
            }
            catch (Exception error)
            {
                string errMsg = "Error authenticating " + authenticationRequest.EventName + ": ";

                var errorMessage = _errorService.LogUserError(new UserError
                {
                    Error = error,
                    AdditionalInformation = errMsg,
                    ErrorText = string.Empty,
                    Source = "ProductAuthenticationRequestConsumer"
                });

                productTenorResponse = new Message
                {
                    ClientSessionInfo = authenticationRequest.ClientSessionInfo,
                    MessageAction = MessageAction.Create,
                    MessageType = MessageType.Error,
                    MessageBody = errorMessage
                };
            }
            try
            {
                var response = new AomProductTenorAuthenticationResponse
                {
                    Message = productTenorResponse,
                    ProductTenor = authenticationRequest.Tenor
                };
                response.Message.MessageBody = response.Message.MessageBody.ToJsonCamelCase();
                _aomBus.Publish(response);
            }
            catch (Exception error)
            {
                _errorService.LogUserError(new UserError
                {
                    Error = error,
                    AdditionalInformation = "Error publishing to bus",
                    ErrorText = string.Empty,
                    Source = "ProductAuthenticationRequestConsumer"
                });
            }
        }
    }
}