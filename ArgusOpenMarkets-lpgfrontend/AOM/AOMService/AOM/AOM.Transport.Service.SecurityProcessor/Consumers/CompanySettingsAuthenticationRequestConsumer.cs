﻿using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.CompanySettings;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.SecurityProcessor.Consumers
{
    public class CompanySettingsAuthenticationRequestConsumer : IConsumer
    {
        private readonly ICompanySettingsAuthenticationService _authenticationService;

        private readonly IBus _aomBus;
        private readonly IErrorService _errorService;

        public CompanySettingsAuthenticationRequestConsumer(
            ICompanySettingsAuthenticationService authenticationService,
            IBus aomBus, 
            IErrorService errorService)
        {
            _authenticationService = authenticationService;
            _aomBus = aomBus;
            _errorService = errorService;
        }

        public void Start()
        {
            _aomBus.Respond<AomCompanySettingsAuthenticationRpcRequest, AomCompanySettingsAuthenticationRpcResponse>(
                ConsumeAuthenticationRequest);
        }

        internal AomCompanySettingsAuthenticationRpcResponse ConsumeAuthenticationRequest(
            AomCompanySettingsAuthenticationRpcRequest authenticationRequest)
        {
            if (authenticationRequest.ClientSessionInfo == null)
            {
                Log.Error("AomCompanySettingsAuthenticationRpcRequest request received with null ClientSessionInfo");
            }

            var response = new AomCompanySettingsAuthenticationRpcResponse
            {
                Message =
                    new Message
                    {
                        ClientSessionInfo =
                            authenticationRequest
                                .ClientSessionInfo,
                        MessageAction =
                            authenticationRequest
                                .MessageAction
                    },
                CompanyDetails =
                    authenticationRequest.CompanyDetails
            };

            try
            {
                var authResult =
                    _authenticationService.AuthenticateCompanySettingsPermissionRequest(authenticationRequest);
                if (authResult.Allow)
                {
                    response.Message.MessageType = MessageType.OrganisationNotificationDetail;
                    response.Message.MessageBody = null;
                    response.MarketMustBeOpen = authResult.MarketMustBeOpen;
                }
                else
                {
                    response.Message.MessageType = MessageType.Error;
                    response.Message.MessageBody = authResult.DenyReason;

                    Log.Warn(
                        String.Format(
                            "{0}: Authorisation was denied to user id {1} while attempting {2}/{3} [{4}]",
                            ToString(),
                            authenticationRequest.ClientSessionInfo.UserId,
                            authenticationRequest.EventName,
                            authenticationRequest.MessageAction,
                            authResult.DenyReason));
                }

            }
            catch (Exception error)
            {
                response.Message.MessageType = MessageType.Error;

                _errorService.LogUserError(new UserError
                {
                    Error = error,
                    AdditionalInformation = "Error authenticating " + authenticationRequest.EventName + "/"
                                            + authenticationRequest.MessageAction + ": ",
                    ErrorText = string.Empty,
                    Source = "CompanySettingsAuthenticationRequestConsumer"
                });
            }

            return response;

        }

        public string SubscriptionId
        {
            get
            {
                return "AOM_COmpanySettingsAuthenticationRequestConsumer";
            }
        }
    }
}