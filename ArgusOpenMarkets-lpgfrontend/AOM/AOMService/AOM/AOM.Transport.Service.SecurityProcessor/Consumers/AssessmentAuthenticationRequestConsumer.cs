﻿using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.SecurityProcessor.Consumers
{
    public class AssessmentAuthenticationRequestConsumer : IConsumer
    {
        private const int ProcessorUserId = -1;
        private readonly IAssessmentAuthenticationService _authenticationService;
        private readonly IErrorService _errorService;

        private readonly IBus _aomBus;

        public AssessmentAuthenticationRequestConsumer(
            IAssessmentAuthenticationService authenticationService,
            IBus aomBus, 
            IErrorService errorService)
        {
            _authenticationService = authenticationService;
            _errorService = errorService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<AomAssessmentAuthenticationRequest>(SubscriptionId, ConsumeAuthenticationRequest);
        }

        internal void ConsumeAuthenticationRequest(AomAssessmentAuthenticationRequest authenticationRequest)
        {
            if (authenticationRequest.ClientSessionInfo == null)
            {
                Log.Error("AomAssessmentAuthenticationRequest request received with null ClientSessionInfo");
                return;
            }

            var response = new AomAssessmentAuthenticationResponse
                           {
                               Message =
                                   new Message
                                   {
                                       ClientSessionInfo =
                                           authenticationRequest
                                           .ClientSessionInfo,
                                       MessageAction =
                                           authenticationRequest
                                           .MessageAction
                                   },
                               ProductId = authenticationRequest.ProductId,
                               Assessment = authenticationRequest.Assessment
                           };

            if (authenticationRequest.ClientSessionInfo.UserId == ProcessorUserId)
            {
                Log.InfoFormat("ConsumeAuthenticationRequest  Allowing the Processor User to perform the action.");
                response.Message.MessageType = MessageType.Assessment;
                response.Message.MessageBody = null;
                response.MarketMustBeOpen = false;
                _aomBus.Publish(response);
            }
            else
            {
                try
                {
                    var authResult = _authenticationService.AuthenticateAssessmentRequest(authenticationRequest);
                    if (authResult.Allow)
                    {
                        response.Message.MessageType = MessageType.Assessment;
                        response.Message.MessageBody = null;
                        response.MarketMustBeOpen = authResult.MarketMustBeOpen;
                    }
                    else
                    {
                        response.Message.MessageType = MessageType.Error;
                        response.Message.MessageBody = authResult.DenyReason;

                        Log.Warn(
                            String.Format(
                                "{0}: Authorisation was denied to user id {1} while attempting {2}/{3} [{4}]",
                                ToString(),
                                authenticationRequest.ClientSessionInfo.UserId,
                                authenticationRequest.EventName,
                                authenticationRequest.MessageAction,
                                authResult.DenyReason));
                    }

                }
                catch (Exception error)
                {
                    response.Message.MessageType = MessageType.Error;

                    _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "ConsumeAuthenticationRequest",
                        Error = error,
                        AdditionalInformation = "Error authenticating " + authenticationRequest.EventName + "/"
                                                + authenticationRequest.MessageAction + ": "
                    });
                }

                try
                {
                    response.Message.MessageBody = response.Message.MessageBody.ToJsonCamelCase();
                    _aomBus.Publish(response);
                }
                catch (Exception error)
                {
                    _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "ConsumeAuthenticationRequest",
                        Error = error,
                        AdditionalInformation = "Error authenticating " + authenticationRequest.EventName + "/"
                                                + authenticationRequest.MessageAction + ": "
                    });
                }
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}