﻿using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.SecurityProcessor.Consumers
{
    public class InternalDealAuthenticationRequestConsumer : IConsumer
    {
        protected readonly IDealAuthenticationService InternalDealAuthenticationService;

        private readonly IBus _aomBus;

        private readonly IErrorService _errorService;

        public InternalDealAuthenticationRequestConsumer(
            IDealAuthenticationService internalDealAuthenticationService,
            IBus aomBus, 
            IErrorService errorService)
        {
            InternalDealAuthenticationService = internalDealAuthenticationService;
            _errorService = errorService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<AomDealAuthenticationRequest>(
                SubscriptionId,
                ConsumeAuthenticationRequest<AomDealAuthenticationRequest, AomDealAuthenticationResponse>);
        }

        internal void ConsumeAuthenticationRequest<TReq, TResp>(TReq authenticationRequest)
            where TReq : AomDealAuthenticationRequest where TResp : AomDealAuthenticationResponse, new()
        {
            var response = new TResp
            {
                Message =
                    new Message
                    {
                        ClientSessionInfo = authenticationRequest.ClientSessionInfo,
                        MessageAction = authenticationRequest.MessageAction
                    },
                Deal = authenticationRequest.Deal
            };

            try
            {
                AuthResult authResult = InternalDealAuthenticationService.AuthenticateDealRequest(authenticationRequest);
                if (authResult.Allow)
                {
                    response.Message.MessageType = MessageType.Deal;
                    response.Message.MessageBody = null;
                    response.MarketMustBeOpen = authResult.MarketMustBeOpen;
                }
                else
                {
                    response.Message.MessageType = MessageType.Error;
                    response.Message.MessageBody = authResult.DenyReason;

                    Log.Warn(
                        String.Format(
                            "{0}: Authorisation was denied to user id {1} while attempting {2}/{3}",
                            ToString(),
                            authenticationRequest.ClientSessionInfo.UserId,
                            authenticationRequest.EventName,
                            authenticationRequest.MessageAction));
                }

            }
            catch (Exception error)
            {
                response.Message.MessageType = MessageType.Error;

                var errMess =
                    _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "OrderAuthenticationRequestConsumer.ConsumeAuthenticationRequest",
                        Error = error,
                        AdditionalInformation = "Error authenticating " + authenticationRequest.EventName + "/"
                                                + authenticationRequest.MessageAction + ": "
                    });

                response.Message.MessageBody = errMess;
            }

            try
            {
                response.Message.MessageBody = response.Message.MessageBody.ToJsonCamelCase();
                _aomBus.Publish(response);

            }
            catch (Exception error)
            {
                _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "OrderAuthenticationRequestConsumer.ConsumeAuthenticationRequest",
                        Error = error,
                        AdditionalInformation = "Error publishing to bus"
                    });
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}