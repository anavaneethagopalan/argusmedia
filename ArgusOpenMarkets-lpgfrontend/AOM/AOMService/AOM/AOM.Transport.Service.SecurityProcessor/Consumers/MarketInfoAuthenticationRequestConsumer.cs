﻿using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;
using AOM.Transport.Events.Users;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.SecurityProcessor.Consumers
{
    public class MarketInfoAuthenticationRequestConsumer : IConsumer
    {
        protected readonly IMarketInfoAuthenticationService MarketInfoAuthenticationService;

        private readonly IBus _aomBus;
        private readonly IErrorService _errorService;

        public MarketInfoAuthenticationRequestConsumer(
            IMarketInfoAuthenticationService marketInfoAuthenticationService,
            IBus aomBus, 
            IErrorService errorService)
        {
            MarketInfoAuthenticationService = marketInfoAuthenticationService;
            _aomBus = aomBus;
            _errorService = errorService;
        }

        public void Start()
        {
            _aomBus.Subscribe<AomMarketInfoAuthenticationRequest>(
                SubscriptionId,
                ConsumeAuthenticationRequest<AomMarketInfoAuthenticationRequest, AomMarketInfoAuthenticationResponse>);
        }


        internal void ConsumeAuthenticationRequest<TReq, TResp>(TReq authenticationRequest)
            where TReq : AomMarketInfoAuthenticationRequest where TResp : AomMarketInfoAuthenticationResponse, new()
        {
            var response = new TResp
            {
                Message =
                    new Message
                    {
                        ClientSessionInfo = authenticationRequest.ClientSessionInfo,
                        MessageAction = authenticationRequest.MessageAction
                    },
                MarketInfo = authenticationRequest.MarketInfo
            };

            try
            {
                AuthResult authResult =
                    MarketInfoAuthenticationService.AuthenticateMarketInfoRequest(authenticationRequest);
                if (authResult.Allow)
                {
                    response.Message.MessageType = MessageType.Info;
                    response.Message.MessageBody = null;
                    response.MarketMustBeOpen = authResult.MarketMustBeOpen;
                }
                else
                {
                    response.Message.MessageType = MessageType.Error;
                    response.Message.MessageBody = authResult.DenyReason;

                    Log.Warn(
                        String.Format(
                            "{0}: Authorisation was denied to user id {1} while attempting {2}/{3}",
                            ToString(),
                            authenticationRequest.ClientSessionInfo.UserId,
                            authenticationRequest.EventName,
                            authenticationRequest.MessageAction));
                }

            }
            catch (Exception error)
            {
                response.Message.MessageType = MessageType.Error;

                _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "ConsumeAuthenticationRequest",
                        Error = error,
                        AdditionalInformation = "Error authenticating " + authenticationRequest.EventName + "/"
                                                + authenticationRequest.MessageAction + ": "
                    });
            }

            try
            {
                response.Message.MessageBody = response.Message.MessageBody.ToJsonCamelCase();
                _aomBus.Publish(response);

            }
            catch (Exception error)
            {
                _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "ConsumeAuthenticationRequest",
                        Error = error,
                        AdditionalInformation = "Error publishing to bus"
                    });
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }

    }
}