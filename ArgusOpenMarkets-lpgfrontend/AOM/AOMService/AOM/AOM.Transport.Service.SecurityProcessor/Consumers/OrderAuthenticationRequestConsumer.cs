﻿using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.SecurityProcessor.Consumers
{
    public class OrderAuthenticationRequestConsumer : IConsumer
    {
        private readonly IOrderAuthenticationService _orderAuthenticationService;

        private readonly IBus _aomBus;

        private readonly IErrorService _errorService;

        public OrderAuthenticationRequestConsumer(IOrderAuthenticationService orderAuthenticationService, 
            IBus aomBus, 
            IErrorService errorService)
        {
            _orderAuthenticationService = orderAuthenticationService;
            _errorService = errorService;
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<AomOrderAuthenticationRequest>(
                SubscriptionId,
                ConsumeAuthenticationRequest<AomOrderAuthenticationRequest, AomOrderAuthenticationResponse>);
        }

        internal void ConsumeAuthenticationRequest<TReq, TResp>(TReq authenticationRequest)
            where TReq : AomOrderAuthenticationRequest where TResp : AomOrderAuthenticationResponse, new()
        {
            var response = new TResp
                           {
                               Message =
                                   new Message
                                   {
                                       ClientSessionInfo = authenticationRequest.ClientSessionInfo,
                                       MessageAction = authenticationRequest.MessageAction
                                   },
                               Order = authenticationRequest.Order
                           };

            try
            {
                AuthResult authResult = _orderAuthenticationService.AuthenticateOrderRequest(authenticationRequest);
                if (authResult.Allow)
                {
                    response.Message.MessageType = MessageType.Order;
                    response.Message.MessageBody = null;
                    response.MarketMustBeOpen = authResult.MarketMustBeOpen;
                }
                else
                {
                    response.Message.MessageType = MessageType.Error;
                    response.Message.MessageBody = authResult.DenyReason;

                    Log.Warn(
                        String.Format(
                            "{0}: Authorisation was denied to user id {1} while attempting {2}/{3}",
                            ToString(),
                            authenticationRequest.ClientSessionInfo.UserId,
                            authenticationRequest.EventName,
                            authenticationRequest.MessageAction));
                }

            }
            catch (Exception error)
            {
                Log.Error("Error authenticating " + authenticationRequest.EventName + "/" + authenticationRequest.MessageAction, error);
                response.Message.MessageType = MessageType.Error;

                var errorMessage =
                   _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "OrderAuthenticationConsumer.ConsumeAuthenticationRequest", 
                        Error = error, 
                        AdditionalInformation = "Error authenticating " + authenticationRequest.EventName + "/"
                    + authenticationRequest.MessageAction + ": "
                    });
            }

            try
            {
                _aomBus.Publish(response);

            }
            catch (Exception error)
            {
                var logUserError =
                    _errorService.LogUserError(new UserError
                    {
                        ErrorText = error.ToString(),
                        Source = "OrderAuthenticationConsumer.ConsumeAuthenticationRequest.Publish",
                        Error = error,
                        AdditionalInformation = "Error publishing to bus"
                    });
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}