﻿using System.Collections.Generic;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.SecurityProcessor
{
    public class SecurityService : ProcessorServiceBase
    {
        public SecurityService(
            IBus aomBus,
            IEnumerable<IConsumer> consumers,
            IServiceManagement serviceManagement)
            : base(consumers, serviceManagement, aomBus)
        {

        }

        public override string Name
        {
            get { return "SecurityService"; }
        }
    }
}