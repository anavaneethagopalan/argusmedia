﻿using CommandLine;
using CommandLine.Text;

namespace AOM.RollProductDeliveryPeriods
{
    public class CommandLineOptions
    {
        [Option('p', "productTenorIds", Required = true,
            HelpText = "A comma separated list of tenor ids to set.  These should correspond with tenor ids in the CSV")
        ]
        public string ProductTenorIds { get; set; }

        [Option('t', "timeZone", HelpText = "Timezone of the rolled market (offset from UTC)")]
        public string TimeZoneOffset { get; set; }

        [Option('c', "confirmationEmail", HelpText = "The address to email a confirmation to when work is completed")]
        public string ConfirmationEmail { get; set; }

        [Option('f', "file", HelpText = "The file containing tenor rolling date information (CSV)")]
        public string FileName { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}