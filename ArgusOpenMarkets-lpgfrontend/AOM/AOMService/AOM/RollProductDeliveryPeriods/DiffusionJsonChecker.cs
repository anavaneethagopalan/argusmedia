﻿using System;
using System.Threading;
using AOM.App.Domain.Entities;
using Newtonsoft.Json;
using PushTechnology.ClientInterface.Client.Features;
using Utils.Logging.Utils;

namespace AOM.RollProductDeliveryPeriods
{
    public class DiffusionJsonChecker : IFetchStream
    {
        public ManualResetEvent WaitHandle { get; private set; }
        public bool DatesCorrect { get; private set; }

        private readonly string _expectedStartDateOffset;
        private readonly string _expectedEndDateOffset;

        public DiffusionJsonChecker(string expectedStartOffset, string expectedEndOffset)
        {
            _expectedStartDateOffset = expectedStartOffset;
            _expectedEndDateOffset = expectedEndOffset;
            WaitHandle = new ManualResetEvent(false);
            DatesCorrect = false;
        }

        public void OnFetchReply(string topicPath, PushTechnology.ClientInterface.Client.Content.IContent content)
        {
            Log.Debug("FETCH REPLY: " + content.AsString());

            var productDefinition = JsonConvert.DeserializeObject<ProductDefinitionItem>(content.AsString());

            //Log.Debug("Tenor Start!: " + productDefinition.ContractSpecification.ProductTenors[0].DeliveryDateStart);
            //Log.Debug("Tenor End!: " + productDefinition.ContractSpecification.ProductTenors[0].DeliveryDateEnd);

            //DatesCorrect = (productDefinition.ContractSpecification.ProductTenors[0].DeliveryDateStart == _expectedStartDateOffset) &&
              //             (productDefinition.ContractSpecification.ProductTenors[0].DeliveryDateEnd == _expectedEndDateOffset);

            WaitHandle.Set();
        }

        public void OnClose()
        {
            Log.Warn("OnClose()!");
            Environment.Exit(0);
        }

        public void OnDiscard()
        {
            Log.Warn("OnDiscard()!");
            Environment.Exit(0);
        }
    }
}