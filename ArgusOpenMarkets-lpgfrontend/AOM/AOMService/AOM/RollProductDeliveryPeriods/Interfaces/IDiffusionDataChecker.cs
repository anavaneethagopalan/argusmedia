﻿namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    internal interface IDiffusionDataChecker
    {
        string CheckDiffusionDates(CommandLineOptions options);
    }
}