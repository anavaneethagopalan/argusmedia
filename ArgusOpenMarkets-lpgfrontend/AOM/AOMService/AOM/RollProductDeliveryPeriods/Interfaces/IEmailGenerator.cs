﻿
namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    public interface IEmailGenerator
    {
        string GenerateSuccessEmail(CommandLineOptions options, string rolledProducts);

        string GenerateFailureEmail(CommandLineOptions options, string reasonWeFailed);
    }
}
