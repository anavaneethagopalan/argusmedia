﻿namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    public interface ICommandLineOptionsValidator
    {
        string ValidateCommandLineOptions(CommandLineOptions options);
    }
}
