﻿namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    public interface IFileChecker
    {
        bool FileExists(string fileName);
    }
}