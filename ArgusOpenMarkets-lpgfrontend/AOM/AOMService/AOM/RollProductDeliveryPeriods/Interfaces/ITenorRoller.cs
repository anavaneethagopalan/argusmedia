﻿using System;

namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    internal interface ITenorRoller
    {
        bool RollTenor(CommandLineOptions options);

        string GetStartDateOffset(DateTime rollStartDate, string timezoneOffset);

        string GetEndDateOffset(DateTime rollEndDate, string timezoneOffset);
    }
}