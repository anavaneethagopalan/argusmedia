﻿using System;
using PushTechnology.ClientInterface.Client.Session;

namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    public interface IDiffusionSessionConnection : IDisposable
    {
        bool ConnectAndStartSession();

        void Disconnect();

        ISession Session { get; }
    }
}