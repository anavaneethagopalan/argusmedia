﻿using System;
using CsvHelper;

namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    public interface ICSVFileParser
    {
        void LoadFile(string filePath);
        void LoadFile(ICsvReader csvReader);

        DateTime GetStartDate(DateTime nextDate, int tenorId);
        DateTime GetEndDate(DateTime nextDate, int tenorId);

        DateTime GetEarliestRollDateInCsvDataFile(int tenorId);
        DateTime GetLatestRollDateInCsvDataFile(int tenorId);
    }
}