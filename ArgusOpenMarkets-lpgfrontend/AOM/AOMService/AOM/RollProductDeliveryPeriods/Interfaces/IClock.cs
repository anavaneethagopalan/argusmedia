﻿using System;

namespace AOM.RollProductDeliveryPeriods.Interfaces
{
    public interface IClock
    {
        DateTime UtcNow();
    }
}
