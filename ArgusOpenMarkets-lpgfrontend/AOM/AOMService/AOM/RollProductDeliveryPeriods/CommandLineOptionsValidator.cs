﻿using System;
using System.Linq;
using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Services.ProductService;

namespace AOM.RollProductDeliveryPeriods
{
    public class CommandLineOptionsValidator : ICommandLineOptionsValidator
    {
        private readonly IFileChecker _fileChecker;
        private readonly IProductService _productService;

        public CommandLineOptionsValidator(IFileChecker fileChecker, IProductService productService)
        {
            _fileChecker = fileChecker;
            _productService = productService;
        }

        public string ValidateCommandLineOptions(CommandLineOptions options)
        {
            if (!_fileChecker.FileExists(options.FileName))
            {
                return string.Format("File: {0} does not exist.", options.FileName);
            }

            int timeZoneOffset;
            Int32.TryParse(options.TimeZoneOffset, out timeZoneOffset);
            if (timeZoneOffset > 12.45)
            {
                // Invalid time-zone offset.   
                // CHAST – Chatham Island Standard Time (Standard Time) = 12:45 hours ahead.
                return string.Format("Timezone offset cannot be greater than 12:45 - Chatham Island");
            }
            if (timeZoneOffset < -12)
            {
                return string.Format("Timezone offset cannot be less than 12 - Military time zone");
            }

            if (string.IsNullOrEmpty(options.ConfirmationEmail))
            {
                return string.Format("No confirmation email has been supplied");
            }
            if (!IsValidEmail(options.ConfirmationEmail))
            {
                return string.Format("Confirmation email address supplied is not a valid email address");
            }

            // TODO: Product Tenors  - they should exist in the system - otherwise this is pointless.
            if (string.IsNullOrEmpty(options.ProductTenorIds))
            {
                return string.Format("No product tenors supplied.");
            }

            var productTenors = _productService.GetAllProductTenors();
            var tenorsToRoll = options.ProductTenorIds.Split(',');

            foreach (var tenorToRoll in tenorsToRoll)
            {
                int tenorToRollId;
                Int32.TryParse(tenorToRoll, out tenorToRollId);

                var matchingTenor = productTenors.FirstOrDefault(t => t.Id == tenorToRollId);
                if (matchingTenor == null)
                {
                    return string.Format("Could not find the tenor: {0}", tenorToRoll);
                }
            }


            // Finally - return an empty string - no errors - all parameters are valid.
            return string.Empty;
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

    }
}