﻿using System;
using System.Collections.Generic;

namespace AOM.RollProductDeliveryPeriods
{
    internal class RollData
    {
        public class TenorRollData
        {
            public int TenorId { get; set; }
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
        }

        public RollData(DateTime rollDate, List<TenorRollData> rollInfo)
        {
            RollDate = rollDate;
            ListTenorRollData = rollInfo;
        }

        public DateTime RollDate { get; private set; }

        public List<TenorRollData> ListTenorRollData { get; private set; }
    }
}