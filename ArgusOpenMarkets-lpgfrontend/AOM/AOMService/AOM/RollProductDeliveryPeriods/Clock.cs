﻿using System;
using AOM.RollProductDeliveryPeriods.Interfaces;

namespace AOM.RollProductDeliveryPeriods
{
    public class Clock : IClock
    {
        public DateTime UtcNow()
        {
            return DateTime.UtcNow;
        }
    }
}