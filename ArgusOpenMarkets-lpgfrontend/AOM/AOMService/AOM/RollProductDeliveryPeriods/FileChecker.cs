﻿using System.IO;
using AOM.RollProductDeliveryPeriods.Interfaces;

namespace AOM.RollProductDeliveryPeriods
{
    public class FileChecker : IFileChecker
    {
        public bool FileExists(string fileName)
        {
            return File.Exists(fileName);
        }
    }
}