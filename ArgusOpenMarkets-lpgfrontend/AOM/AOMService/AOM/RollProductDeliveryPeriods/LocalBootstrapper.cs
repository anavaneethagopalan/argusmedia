﻿using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Transport.Service.Processor.Common.ArgusBus;
using Argus.Transport.Configuration;
using Argus.Transport.Infrastructure;
using Ninject;
using Ninject.Modules;

namespace AOM.RollProductDeliveryPeriods
{
    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<ITenorRoller>().To<TenorRoller>();
            kernel.Bind<ICSVFileParser>().To<CsvFileParser>().InSingletonScope();
            kernel.Bind<IBusLogger>().To<BusLogger>();
            kernel.Bind<IDiffusionSessionConnection>().To<DiffusionSessionConnection>().InSingletonScope();
            kernel.Bind<IDiffusionDataChecker>().To<DiffusionDataChecker>();
            kernel.Bind<ICommandLineOptionsValidator>().To<CommandLineOptionsValidator>();
            kernel.Bind<IFileChecker>().To<FileChecker>();
            kernel.Bind<IClock>().To<Clock>();
            kernel.Bind<IEmailGenerator>().To<EmailGenerator>();
            var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
            kernel.Bind<IBus>()
                .ToMethod(
                    x => Argus.Transport.Transport.CreateBus(aomConnectionConfiguration,
                        null,
                        x.Kernel.Get<IBusLogger>())).InSingletonScope();
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}