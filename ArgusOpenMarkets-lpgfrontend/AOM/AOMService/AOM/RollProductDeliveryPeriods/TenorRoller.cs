﻿using System;
using System.Linq;
using System.Threading;
using AOM.App.Domain;
using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using AOM.Transport.Events.System;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.RollProductDeliveryPeriods
{
    public class TenorRoller : ITenorRoller
    {
        private const int DelayTopicRebuildMilliseconds = 10000;

        private readonly IProductService _productService;
        private readonly ICSVFileParser _csvFileParser;
        private readonly IBus _aomBus;
        private readonly IClock _clock;

        public TenorRoller(IProductService productService, ICSVFileParser csvFileParser, IBus aomBus, IClock clock)
        {
            _productService = productService;
            _csvFileParser = csvFileParser;
            _aomBus = aomBus;
            _clock = clock;
        }

        public string GetStartDateOffset(DateTime rollDateStart, string timeZoneOffset)
        {
            var currentDateTime = _clock.UtcNow().AddHours(Int32.Parse(timeZoneOffset)).Date;
            var nextStartOffset = (rollDateStart - currentDateTime).Days;

            if (nextStartOffset < 0)
            {
                return string.Empty;
            }

            var tenorOffset = MarkDeliveryPeriodOffset(nextStartOffset);
            Log.DebugFormat("Next Start Date Offset is: {0}", tenorOffset);

            return tenorOffset;
        }

        public string GetEndDateOffset(DateTime rollDateEnd, string timeZoneOffset)
        {
            var currentDateTime = _clock.UtcNow().AddHours(Int32.Parse(timeZoneOffset)).Date;
            var nextEndDateOffset = (rollDateEnd - currentDateTime).Days;

            if (nextEndDateOffset < 0)
            {
                return string.Empty;
            }

            var tenorOffset = MarkDeliveryPeriodOffset(nextEndDateOffset);
            Log.DebugFormat("Next End Date Offset is: {0}", tenorOffset);

            return tenorOffset;
        }

        private string MarkDeliveryPeriodOffset(int offSet)
        {
            return string.Format("+{0}d", offSet);
        }

        public bool RollTenor(CommandLineOptions options)
        {
            var currentDateTime = DateTime.UtcNow.AddHours(Int32.Parse(options.TimeZoneOffset)).Date;

            foreach (var tenorId in options.ProductTenorIds.Split(',').Select(int.Parse))
            {
                DateTime rollDateStart;
                DateTime rollDateEnd;

                try
                {
                    rollDateStart = _csvFileParser.GetStartDate(currentDateTime, tenorId);
                }
                catch (BusinessRuleException brEx)
                {
                    Log.Error("Roll Tenor - Error: " + brEx.Message, brEx);
                    return false;
                }

                try
                {
                    rollDateEnd = _csvFileParser.GetEndDate(currentDateTime, tenorId);
                }
                catch (BusinessRuleException brEx)
                {
                    Log.Error("Roll Tenor GetEndDate - Error: " + brEx.Message, brEx);
                    return false;
                }
                
                var newDeliveryPeriodStartDateOffset = GetStartDateOffset(rollDateStart, options.TimeZoneOffset);
                var newDeliveryPeriodEndDateOffset = GetEndDateOffset(rollDateEnd, options.TimeZoneOffset);
                
                if (string.IsNullOrEmpty(newDeliveryPeriodStartDateOffset))
                {
                    Log.ErrorFormat("Error Calculating the Delivery Period Start Date Offset.  Tenor Id: {0}", tenorId);
                    return false;
                }

                if (string.IsNullOrEmpty(newDeliveryPeriodEndDateOffset))
                {
                    Log.ErrorFormat("Error Calculating the Delivery Period End Date Offset.  Tenor Id: {0}", tenorId);
                    return false;
                }

                Log.Debug("Getting tenor from the database");
                var tenor = _productService.GetAllProductTenors().Single(x => x.Id == tenorId);

                if (tenor != null)
                {
                    //tenor.DeliveryDateStart = newDeliveryPeriodStartDateOffset;
                    //tenor.DeliveryDateEnd = newDeliveryPeriodEndDateOffset;
                    //tenor.DefaultStartDate = newDeliveryPeriodStartDateOffset;
                    //tenor.DefaultEndDate = newDeliveryPeriodEndDateOffset;
                    //tenor.MinimumDeliveryRange = CalculateMinimumDeliveryRange(rollDateStart, rollDateEnd);

                    _aomBus.Publish(new AomProductTenorRequest
                    {
                        Tenor = tenor,
                        MessageAction = MessageAction.Update,
                        ClientSessionInfo = new ClientSessionInfo
                        {
                            UserId = -1
                        }
                    });

                    //Log.DebugFormat("We have just updated the following product Tenor: Id-{0}.  With the following new values: DeliveryDateStart-{1}  DeliveryDateEnd-{2} MinimumDeliveryRange-{3}", tenorId, newDeliveryPeriodStartDateOffset, newDeliveryPeriodEndDateOffset, tenor.MinimumDeliveryRange);
                    Log.Debug("Removing topic from diffusion for Product " + tenor.ProductId);
                    _aomBus.Publish(new DeleteDiffusionTopicRequest
                    {
                        TopicToDelete = "AOM/SEC-Products/" + tenor.ProductId,
                        LoggedOnUserId = -1
                    });
                }
                else
                {
                    Log.ErrorFormat(
                        "Could not find the tenor to update the Delivery Period Start/End Dates.  Tenor Id: {0}",
                        tenorId);
                    return false;
                }
            }

            Log.Info("Waiting for changes to be propagated to the database...");
            Thread.Sleep(DelayTopicRebuildMilliseconds);
            Log.Info("Telling AOM to rebuild the product topics");
            _aomBus.Publish(new GetProductConfigRequest());

            return true;
        }

        public long CalculateMinimumDeliveryRange(DateTime rollDateStart, DateTime rollDateEnd)
        {
            var numDays = (rollDateEnd - rollDateStart).Days;
            numDays = numDays + 1;

            return numDays;
        }
    }
}