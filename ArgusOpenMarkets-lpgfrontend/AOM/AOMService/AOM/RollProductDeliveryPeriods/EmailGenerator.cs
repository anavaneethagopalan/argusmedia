﻿using System;
using System.Configuration;
using System.Text;
using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Services.EmailService;

namespace AOM.RollProductDeliveryPeriods
{
    public class EmailGenerator : IEmailGenerator
    {
        private readonly ICSVFileParser _csvFileParser;

        public EmailGenerator(ICSVFileParser csvFileParser)
        {
            _csvFileParser = csvFileParser;
        }

        public string GenerateSuccessEmail(CommandLineOptions options, string rolledProducts)
        {
            var body = GenerateBodyHeader(options, "Roll Delivery Periods Tenors - SUCCESS");
            body.AppendEmailLine("We have just rolled the following Product Tenors");
            body.AppendEmailLine(rolledProducts);
            body.AppendEmailLine(
                "-------------------------------------------------------------- --- -------------------------------------------------");
            body.AppendLine("ConnectionStrings");

            var connStrings = ConfigurationManager.ConnectionStrings;
            foreach (var connString in connStrings)
            {
                try
                {
                    // Just add in the host or the server - we don't wany anything else.  
                    var connParts = connString.ToString().Split(';');
                    if (connParts != null)
                    {
                        body.AppendLine(connParts[0]);
                    }
                }
                catch (Exception)
                {

                }
            }
            body.AppendEmailLine(
                "-------------------------------------------------------------- --- -------------------------------------------------");
            body.AppendLine("Delivery Periods Latest Dates For Tenors");

            var productTenorIds = options.ProductTenorIds.Split('.');
            foreach (var productTenorId in productTenorIds)
            {
                int ptId = 0;
                Int32.TryParse(productTenorId, out ptId);
                var latestRollDate = _csvFileParser.GetLatestRollDateInCsvDataFile(ptId);
                body.AppendLine(string.Format("Latest Roll Date for Product Tenor Id: {0}  is: {1}", productTenorId,
                    latestRollDate));
            }

            return body.ToString();
        }

        public string GenerateFailureEmail(CommandLineOptions options, string reasonWeFailed)
        {
            var body = GenerateBodyHeader(options, reasonWeFailed);
            return body.ToString();
        }

        private StringBuilder GenerateBodyHeader(CommandLineOptions options, string successFailure)
        {
            StringBuilder body = new StringBuilder();

            body.AppendLine(successFailure);
            body.AppendEmailLine("Roll Product (Tenor) Delivery Periods");
            body.AppendEmailLine(string.Format("Execution UTC Date/Time: {0}", DateTime.UtcNow));
            body.AppendEmailLine(string.Format("Machine Name: {0}", Environment.MachineName));

            body.AppendEmailLine(string.Format("Options.ProductTenorIds: {0}", options.ProductTenorIds));
            body.AppendEmailLine(string.Format("Options.TimezoneOffset: {0}", options.TimeZoneOffset));
            body.AppendEmailLine(string.Format("Options.Filename: {0}", options.FileName));
            body.AppendEmailLine(string.Format("Options.ConfirmationEmail: {0}", options.ConfirmationEmail));
            body.AppendEmailLine(
                "-------------------------------------------------------------- --- -------------------------------------------------");

            return body;
        }
    }
}