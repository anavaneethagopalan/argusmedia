﻿using System;
using System.Linq;
using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Services.ProductService;
using Utils.Logging.Utils;

namespace AOM.RollProductDeliveryPeriods
{
    internal class DiffusionDataChecker : IDiffusionDataChecker
    {
        private readonly IDiffusionSessionConnection _diffusionSessionConnection;
        private readonly ICSVFileParser _fileParser;
        private readonly IProductService _productService;
        private readonly IClock _clock;
        private readonly ITenorRoller _tenorRoller;

        public DiffusionDataChecker(
            IProductService productService,
            IDiffusionSessionConnection diffusionSessionConnection,
            ICSVFileParser fileParser, 
            IClock clock, 
            ITenorRoller tenorRoller)
        {
            _diffusionSessionConnection = diffusionSessionConnection;
            _fileParser = fileParser;
            _productService = productService;
            _clock = clock;
            _tenorRoller = tenorRoller;
        }

        public string CheckDiffusionDates(CommandLineOptions options)
        {
            var rolledDatesInformation = "";
            var currentDateTime = _clock.UtcNow().AddHours(Int32.Parse(options.TimeZoneOffset)).Date;

            foreach (var tenorId in options.ProductTenorIds.Split(',').Select(int.Parse))
            {
                Log.Info("Checking diffusion has the expected data for tenor id: " + tenorId);
                var nextStart = _fileParser.GetStartDate(currentDateTime, tenorId);
                var nextEnd = _fileParser.GetEndDate(currentDateTime, tenorId);

                var nextStartDateOffset = _tenorRoller.GetStartDateOffset(nextStart, options.TimeZoneOffset);
                var nextEndDateOffset = _tenorRoller.GetEndDateOffset(nextEnd, options.TimeZoneOffset);

                if (!IsDateCorrect(tenorId, nextStartDateOffset, nextEndDateOffset))
                {
                    throw new Exception("Check for tenor " + tenorId + " Failed! Throwing...");
                }
                else
                {
                    rolledDatesInformation +=
                        string.Format(
                            "Rolled dates for the tenor: {0}   Start Date Offset set to: {1}   End Date Offset set to: {2}",
                            tenorId, nextStartDateOffset, nextEndDateOffset);
                    ;
                }
            }

            return rolledDatesInformation;
        }

        private bool IsDateCorrect(int tenorId, string expectedStartDateOffset, string expectedEndDateOffset)
        {
            var tenor = _productService.GetAllProductTenors().Single(x => x.Id == tenorId);

            var session = _diffusionSessionConnection.Session;
            var topics = session.GetTopicsFeature();

            var topicCompletionCallback = new DiffusionJsonChecker(expectedStartDateOffset, expectedEndDateOffset);

            topics.Fetch(
                String.Format(">/AOM/SEC-Products/{0}/Definition", tenor.ProductId),
                topicCompletionCallback);

            topicCompletionCallback.WaitHandle.WaitOne();

            return topicCompletionCallback.DatesCorrect;
        }
    }
}