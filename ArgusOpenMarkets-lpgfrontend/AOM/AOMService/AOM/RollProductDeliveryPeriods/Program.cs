﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Services;
using AOM.Services.EmailService;
using Ninject;
using Utils.Logging.Utils;

namespace AOM.RollProductDeliveryPeriods
{
    internal class Program
    {
        // NOTE Command Line Arguments to run Console App are: 
        // RollProductDeliveryPeriods.exe -p1 -t0 -cnathanbellamore@hotmail.com -fC:\Users\nathanb\Documents\rolls.csv

        private const string AutomaticTenorRollingFailedSubject = "URGENT - Roll Product Delivery Periods - Failed";
        private const string AutomaticTenorRollingPassedSubject = "Roll Product Delivery Periods - Passed";
        private const string CouldNotConnectToDiffusion = "Could not connect to diffusion";
        private const string WaitingForDiffusionToPublishChanges = "Waiting for diffusion to catch up with us before checking";
        private const string AllTenorsCheckedSuccessfully = "All tenors checked sucessfully!";
        private const string FailedToRollTenorManualIntervention = "Failed to roll tenor automatically.  Manual intervention is now required!";
        private string FailedToRollTenorManualInterventionBody = "Failed to roll tenor automatically.  Manual intervention is now required!";

        private static void Main(string[] args)
        {
            const int diffusionStartupWaitTime = 10000;

            var options = new CommandLineOptions();

            if (!CommandLine.Parser.Default.ParseArguments(args, options))
            {
                Environment.Exit(CommandLine.Parser.DefaultExitCodeFail);
            }
            
            using (var kernel = new StandardKernel())
            {
                kernel.Load(new EFModelBootstrapper(), new ServiceBootstrap(), new LocalBootstrapper());
                WarmUp(kernel);

                var errorString = kernel.Get<ICommandLineOptionsValidator>().ValidateCommandLineOptions(options);
                if (!string.IsNullOrEmpty(errorString))
                {
                    Log.Error("Command line options are invalid");
                    Environment.Exit(0);
                }

                if (!kernel.Get<IDiffusionSessionConnection>().ConnectAndStartSession())
                {
                    Log.Error(CouldNotConnectToDiffusion);
                    Environment.Exit(0);
                }

                try
                {
                    Log.Debug("Loading CSV file: " + options.FileName);
                    var csvFileParser = kernel.Get<ICSVFileParser>();
                    csvFileParser.LoadFile(options.FileName);

                    // Roll the tenor
                    kernel.Get<ITenorRoller>().RollTenor(options);

                    Log.Info(WaitingForDiffusionToPublishChanges);
                    Thread.Sleep(diffusionStartupWaitTime);
                }
                catch (Exception ex)
                {
                    Log.Error("Load CSV file.", ex);
                }
                
                try
                {
                    var rolledPeriods = kernel.Get<IDiffusionDataChecker>().CheckDiffusionDates(options);
                    Log.Info(AllTenorsCheckedSuccessfully);
                    var passedRollProductDeliveryPeriodsPassedBody = kernel.Get<IEmailGenerator>().GenerateSuccessEmail(options, rolledPeriods);

                    SendEmail(kernel, options, AutomaticTenorRollingPassedSubject, passedRollProductDeliveryPeriodsPassedBody);
                }
                catch (Exception e)
                {
                    var failedBody = kernel.Get<IEmailGenerator>().GenerateFailureEmail(options, "Failed are rolling delivery periods for tenors");
                    Log.Error(FailedToRollTenorManualIntervention, e);
                    SendEmail(kernel, options, AutomaticTenorRollingFailedSubject, failedBody);
                }

                try
                {
                    kernel.Get<IDiffusionSessionConnection>().Disconnect();
                }
                catch (Exception ex)
                {
                    Log.Error("Could not disconnect from diffusion.", ex);
                }

                Environment.Exit(-1);               
            }
        }

        private static void SendEmail(StandardKernel kernel, CommandLineOptions options, string subject, string body)
        {
            kernel.Get<ISmtpService>().SendEmail(new Email
            {
                Body = body,
                DateCreated = DateTime.UtcNow,
                DateSent = DateTime.UtcNow,
                Id = -1,
                Recipient = options.ConfirmationEmail,
                Status = EmailStatus.Pending,
                Subject = subject
            });
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (var dbCrm = dbFactory.CreateCrmModel())
                {
                    dbCrm.Organisations.Any();
                }

                Log.Info(
                    string.Format(
                        "OrganisationProcessor -Initial EF model build took {0} msecs.",
                        timer.ElapsedMilliseconds));

            }
            catch (Exception ex)
            {
                Log.Error("OrganisationProcessor - WarmUp - Error - cannot connect to DB", ex);
                Environment.Exit(1);
            }
        }
    }
}