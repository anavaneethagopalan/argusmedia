﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AOM.App.Domain;
using AOM.RollProductDeliveryPeriods.Interfaces;
using CsvHelper;
using Utils.Logging.Utils;

namespace AOM.RollProductDeliveryPeriods
{
    public class CsvFileParser : ICSVFileParser
    {
        private readonly List<RollData> _data = new List<RollData>();

        public void LoadFile(ICsvReader csvReader)
        {
            ParseCsvData(csvReader);
        }

        public void LoadFile(string filePath)
        {
            var csvContent = new CsvReader(new StreamReader(filePath));
            ParseCsvData(csvContent);
        }

        private void ParseCsvData(ICsvReader csv)
        {
            try
            {
                csv.GetRecords<dynamic>().GroupBy(x => x.Date)
                    .ToList()
                    .ForEach(x =>
                    {
                        var tenorRollData = new List<RollData.TenorRollData>();
                        foreach (var s in x)
                        {
                            tenorRollData.Add(new RollData.TenorRollData
                            {
                                TenorId = Int32.Parse(s.TenorId),
                                Start = DateTime.Parse(s.Start),
                                End = DateTime.Parse(s.End)
                            });
                        }

                        _data.Add(new RollData(DateTime.Parse(x.Key), tenorRollData));

                    });

                Log.Info("CSV file loaded sucessfully");
            }
            catch (Exception e)
            {
                Log.Error("There was an error loading the specified file: " + e.Message);
            }
        }

        private RollData.TenorRollData GetNextRollDate(DateTime rollDate, int tenorId)
        {
            var nextDate = _data.Select(x => x.RollDate).OrderBy(x => x).Last(x => x <= rollDate);

            return _data
                .Single(x => x.RollDate == nextDate)
                .ListTenorRollData.Single(x => x.TenorId == tenorId);
        }

        public DateTime GetStartDate(DateTime rollDate, int tenorId)
        {
            var earliestDataSetRollDate = GetEarliestRollDateInCsvDataFile(tenorId);
            if (rollDate < earliestDataSetRollDate)
            {
                throw new BusinessRuleException("Roll Date is earlier than supplied date ranges");
            }

            var nextStartDate = GetNextRollDate(rollDate, tenorId).Start;

            if (nextStartDate <= rollDate)
            {
                throw new BusinessRuleException("Roll Date is greater than supplied date ranges");
            }

            return nextStartDate;
        }

        public DateTime GetEarliestRollDateInCsvDataFile(int tenorId)
        {
            var earliestDateForTenor = _data.OrderBy(d => d.RollDate);
            foreach (var rollItem in earliestDateForTenor)
            {
                var earliestRollItem = rollItem.ListTenorRollData.Where(r => r.TenorId == tenorId).OrderBy(r => r.Start).FirstOrDefault();
                if (earliestRollItem != null)
                {
                    return earliestRollItem.Start;
                }
            }

            return DateTime.MinValue;
        }

        public DateTime GetLatestRollDateInCsvDataFile(int tenorId)
        {
            var rollDatesForTenor = _data.OrderByDescending(d => d.RollDate);
            foreach (var rollItem in rollDatesForTenor)
            {
                var latestRollItem = rollItem.ListTenorRollData.Where(r => r.TenorId == tenorId).OrderByDescending(r => r.Start).FirstOrDefault();
                if (latestRollItem != null)
                {
                    return latestRollItem.Start;
                }
            }

            return DateTime.MinValue;
        }

        public DateTime GetEndDate(DateTime rollDate, int tenorId)
        {
            return GetNextRollDate(rollDate, tenorId).End;
        }
    }
}