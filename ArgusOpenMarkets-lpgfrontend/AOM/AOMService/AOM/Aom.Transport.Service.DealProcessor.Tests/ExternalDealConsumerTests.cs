﻿using System;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Services.ErrorService;
using AOM.Services.ExternalDealService;
using AOM.Transport.Events;
using AOM.Transport.Events.ExternalDeals;
using AOM.Transport.Service.DealProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.DealProcessor.Tests
{
    [TestFixture]
    public class ExternalDealConsumerTests
    {

        [Test]
        public void OnStartSubscribesProcessorToBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockExternalDealService = new Mock<IExternalDealService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var externalDealConsumer = new ExternalDealRequestConsumer(MockDbContextFactory.Stub(), mockExternalDealService.Object, mockUserService.Object,
                null, mockBus.Object, mockErrorService.Object);
            externalDealConsumer.Start();

            //Assert
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomExternalDealRequest>>()), Times.Once);
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomExternalDealAuthenticationResponse>>()), Times.Once);
        }

        [Test]
        public void ConsumeExternalDealRequestPutsExternalDealAuthenticationRequestOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockExternalDealService = new Mock<IExternalDealService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            var externalDeal = new ExternalDeal
            {
                BrokerId = 1,
                Id = 123,
            };

            var externalDealRequest = new AomExternalDealRequest
            {
                ClientSessionInfo = new ClientSessionInfo {UserId = 12, SessionId = "TestSessionId"},
                MessageAction = MessageAction.Create,
                ExternalDeal = externalDeal
            };

            //Act
            var externalDealConsumer = new ExternalDealRequestConsumer(MockDbContextFactory.Stub(), mockExternalDealService.Object, mockUserService.Object,
                null, mockBus.Object, mockErrorService.Object);
            externalDealConsumer.ConsumeExternalDealRequest<AomExternalDealRequest, AomExternalDealAuthenticationRequest>(externalDealRequest);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomExternalDealAuthenticationRequest>(r => (r.ExternalDeal.Id == 123 && r.ExternalDeal.BrokerId == 1))), Times.Once);
        }

        [Test]
        public void ConsumeExternalDealAuthenticationResponsePutsExternalDealResponseOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockExternalDealService = new Mock<IExternalDealService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            var externalDeal = new ExternalDeal
            {
                BrokerId = 1,
                Id = 123,
            };

            var testMessage = new Message
            {
                ClientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" },
                MessageAction = MessageAction.Create,
                MessageType = MessageType.ExternalDeal,
                MessageBody = externalDeal
            };

            var externalDealRequest = new AomExternalDealAuthenticationResponse
            {
                Message = testMessage,
                ExternalDeal = externalDeal
            };

            mockExternalDealService.Setup(x => x.CreateExternalDeal(It.IsAny<IAomModel>(), externalDealRequest))
                .Returns(externalDeal);

            //Act
            var externalDealConsumer = new ExternalDealRequestConsumer(MockDbContextFactory.Stub(), mockExternalDealService.Object, mockUserService.Object,
                null, mockBus.Object, mockErrorService.Object);
            externalDealConsumer.ConsumeExternalDealResponse<AomExternalDealAuthenticationResponse, AomExternalDealResponse>(externalDealRequest);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomExternalDealResponse>(r => (r.ExternalDeal.Id == 123 &&
                                                                               r.ExternalDeal.BrokerId == 1 &&
                                                                               r.Message.MessageType == MessageType.ExternalDeal))), Times.Once);
        }

        [Test]
        public void ConsumeExternalDealAuthenticationResponseInErrorPutsErrorExternalDealResponseOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockExternalDealService = new Mock<IExternalDealService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            var externalDeal = new ExternalDeal
            {
                BrokerId = 1,
                Id = 123,
            };

            var testMessage = new Message
            {
                ClientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" },
                MessageAction = MessageAction.Create,
                MessageType = MessageType.Error,
                MessageBody = "Current user is not permissioned for this product"
            };

            var externalDealRequest = new AomExternalDealAuthenticationResponse
            {
                Message = testMessage,
                ExternalDeal = externalDeal
            };

            //Act
            var externalDealConsumer = new ExternalDealRequestConsumer(MockDbContextFactory.Stub(), mockExternalDealService.Object, mockUserService.Object,
                null, mockBus.Object, mockErrorService.Object);
            externalDealConsumer.ConsumeExternalDealResponse<AomExternalDealAuthenticationResponse, AomExternalDealResponse>(externalDealRequest);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomExternalDealResponse>(r => (r.Message.MessageType == MessageType.Error &&
                                                                               r.Message.MessageBody.ToString() == "Current user is not permissioned for this product")
                                                                         )), Times.Once);
        }

        [Test]
        public void ConsumeExternalDealAuthenticationResponsePutsExternalDealResponseOnBusOnExceptionThrown()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockExternalDealService = new Mock<IExternalDealService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            var externalDeal = new ExternalDeal
            {
                BrokerId = 1,
                Id = 123,
            };

            var testMessage = new Message
            {
                ClientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" },
                MessageAction = MessageAction.Create,
                MessageType = MessageType.ExternalDeal,
                MessageBody = externalDeal
            };

            var externalDealRequest = new AomExternalDealAuthenticationResponse
            {
                Message = testMessage,
                ExternalDeal = externalDeal
            };

            mockExternalDealService.Setup(x => x.CreateExternalDeal(It.IsAny<IAomModel>(), externalDealRequest))
                .Throws(new BusinessRuleException("Something went wrong"));

            //Act
            var externalDealConsumer = new ExternalDealRequestConsumer(MockDbContextFactory.Stub(), mockExternalDealService.Object, mockUserService.Object, null, mockBus.Object, mockErrorService.Object);
            externalDealConsumer.ConsumeExternalDealResponse<AomExternalDealAuthenticationResponse, AomExternalDealResponse>(externalDealRequest);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomExternalDealResponse>(r => (r.Message.MessageType == MessageType.Error))), Times.Once);
        }

        [Test]
        public void ConsumeVoidExternalDealRequestPutsVoidExternalDealAuthenticationRequestOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockExternalDealService = new Mock<IExternalDealService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            var externalDeal = new ExternalDeal
            {
                BrokerId = 1,
                Id = 123,
            };

            var externalDealRequest = new AomExternalDealRequest
            {
                ClientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" },
                MessageAction = MessageAction.Void,
                ExternalDeal = externalDeal
            };

            //Act
            var externalDealConsumer = new ExternalDealRequestConsumer(MockDbContextFactory.Stub(), mockExternalDealService.Object, mockUserService.Object,
                null, mockBus.Object, mockErrorService.Object);
            externalDealConsumer.ConsumeExternalDealRequest<AomExternalDealRequest, AomExternalDealAuthenticationRequest>(externalDealRequest);

            //Assert
            mockBus.Verify(x => x.Publish(It.Is<AomExternalDealAuthenticationRequest>(r => (r.ExternalDeal.Id == 123 && r.ExternalDeal.BrokerId == 1))), Times.Once);
        }

        [Test]
        public void ConsumeVoidExternalDealAuthenticationResponseInErrorPutsErrorExternalDealResponseOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockExternalDealService = new Mock<IExternalDealService>();
            var mockUserService = new Mock<IUserService>();
            var mockErrorService = new Mock<IErrorService>();

            var externalDeal = new ExternalDeal
            {
                BrokerId = 1,
                Id = 123,
            };

            var testMessage = new Message
            {
                ClientSessionInfo = new ClientSessionInfo {UserId = 12, SessionId = "TestSessionId"},
                MessageAction = MessageAction.Create,
                MessageType = MessageType.Error,
                MessageBody = "Current user is not permissioned to void external deal"
            };

            var externalDealRequest = new AomExternalDealAuthenticationResponse
            {
                Message = testMessage,
                ExternalDeal = externalDeal
            };

            //Act
            var externalDealConsumer = new ExternalDealRequestConsumer(MockDbContextFactory.Stub(),
                mockExternalDealService.Object, mockUserService.Object,
                null, mockBus.Object, mockErrorService.Object);
            externalDealConsumer
                .ConsumeExternalDealResponse<AomExternalDealAuthenticationResponse, AomExternalDealResponse>(
                    externalDealRequest);

            //Assert
            mockBus.Verify(
                x => x.Publish(It.Is<AomExternalDealResponse>(r => (r.Message.MessageType == MessageType.Error &&
                                                                    r.Message.MessageBody.ToString() ==
                                                                    "Current user is not permissioned to void external deal")
                    )), Times.Once);
        }
    }
}
