﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Services.DealService;
using AOM.Services.EmailService;
using AOM.Services.ErrorService;
using AOM.Services.OrderService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOM.Transport.Events.Orders;
using AOM.Transport.Service.DealProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;

namespace AOM.Transport.Service.DealProcessor.Tests
{
    [TestFixture]
    public class InternalDealConsumerTests
    {
        private const long TestUserId = 123;
        private const long TestOrgId = 234;
        private const string FakeSessionId = "FakeSession123_234";

        private readonly ClientSessionInfo _fakeClientSessionInfo = new ClientSessionInfo
        {
            OrganisationId = TestOrgId,
            SessionId = FakeSessionId,
            UserId = TestUserId
        };

        private Mock<IUserService> _mockUserService;
        private Mock<IProductService> _mockProductService;
        private Mock<IEmailService> _mockEmailService;
        private Mock<IDealService> _mockDealService;
        private Mock<IOrderService> _mockOrderService;
        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockUserService = new Mock<IUserService>();
            _mockUserService.Setup(x => x.GetUsersOrganisation(TestUserId))
                            .Returns(new Organisation {Name = "TestingOrg", ShortCode = "XXX"});

            _mockProductService = new Mock<IProductService>();
            _mockProductService.Setup(x => x.GetMarketStatus(It.IsAny<long>()))
                               .Returns(MarketStatus.Open);

            _mockEmailService = new Mock<IEmailService>();

            _mockDealService = new Mock<IDealService>();
            _mockDealService.Setup(x => x.VoidDeal(It.IsAny<IAomModel>(), It.IsAny<Deal>()))
                            .Returns((IAomModel m, Deal deal) => new Message
                            {
                                ClientSessionInfo =_fakeClientSessionInfo,
                                MessageAction = MessageAction.Void,
                                MessageBody = SetStatusAsVoid(deal),
                                MessageType = MessageType.Deal
                            });

            _mockOrderService = new Mock<IOrderService>();
            _mockOrderService
                .Setup(x => x.VoidOrder(It.IsAny<IAomModel>(),
                                        It.IsAny<AomOrderAuthenticationResponse>(),
                                        It.IsAny<MarketStatus>()))
                .Returns((IAomModel m,
                          AomOrderAuthenticationResponse r,
                          MarketStatus s) =>
                {
                    r.Order.OrderStatus = OrderStatus.VoidAfterExecuted;
                    return r.Order;
                });

            _mockBus = new Mock<IBus>();
        }

        [Test]
        public void OnStartSubscribesProcessorToBus()
        {
            // Arrange
            var externalDealConsumer = CreateConsumer();

            // Act
            externalDealConsumer.Start();

            // Assert
            _mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomDealRequest>>()), Times.Once);
            _mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomDealAuthenticationResponse>>()), Times.Once);
        }

        [Test]
        public void ExecutedDealVoidRequestPutsAomDealResponseOnBusAndVoidsExecutedOrders()
        {
            // Arrange
            InternalDealRequestConsumer externalDealConsumer = CreateConsumer();
            externalDealConsumer.Start();

            var initialOrder = new Order
            {
                Id = 1,
                OrderStatus = OrderStatus.Executed,
                OrderType = OrderType.Bid
            };

            var matchingOrder = new Order
            {
                Id = 2,
                OrderStatus = OrderStatus.Executed,
                IsVirtual = true,
                OrderType = OrderType.Ask
            };

            var fakeDeal = new Deal
            {
                Id = 1,
                DealStatus = DealStatus.Executed,
                Initial = initialOrder,
                InitialOrderId = initialOrder.Id,
                Matching = matchingOrder,
                MatchingOrderId = matchingOrder.Id
            };

            var dealAuthResponse = new AomDealAuthenticationResponse
            {
                Deal = fakeDeal,
                MarketMustBeOpen = false,
                Message = new Message
                {
                    ClientSessionInfo = _fakeClientSessionInfo,
                    MessageAction = MessageAction.Void,
                    MessageBody = fakeDeal,
                    MessageType = MessageType.Deal
                }
            };

            // Act
            externalDealConsumer.ConsumeDealResponse<AomDealAuthenticationResponse, AomDealResponse>(dealAuthResponse);

            // Assert

            // Should have one deal response for the voided deal and two order responses
            // for the voided orders from the deal.
            _mockBus.Verify(x => x.Publish(It.Is<AomDealResponse>(r => (r != null &&
                                                                        r.Deal.Id == fakeDeal.Id &&
                                                                        r.Deal.DealStatus == DealStatus.Void))),
                            Times.Once);

            _mockBus.Verify(x => x.Publish(It.Is<AomOrderResponse>(r => (r != null &&
                                                                         r.Order.Id == initialOrder.Id &&
                                                                         r.Order.OrderStatus ==
                                                                            OrderStatus.VoidAfterExecuted))),
                            Times.Once());

            _mockBus.Verify(x => x.Publish(It.Is<AomOrderResponse>(r => (r != null &&
                                                                         r.Order.Id == matchingOrder.Id &&
                                                                         r.Order.OrderStatus ==
                                                                            OrderStatus.VoidAfterExecuted))),
                            Times.Once());

            // Should also have a call to the order service to void each of the orders
            _mockOrderService.Verify(x => x.VoidOrder(
                It.IsAny<IAomModel>(),
                It.Is<AomOrderAuthenticationResponse>(
                    r => r.Order.Id == initialOrder.Id && r.Message.MessageAction == MessageAction.Void),
                It.IsAny<MarketStatus>()),
                Times.Once());

            _mockOrderService.Verify(x => x.VoidOrder(
                It.IsAny<IAomModel>(),
                It.Is<AomOrderAuthenticationResponse>(
                    r => r.Order.Id == matchingOrder.Id && r.Message.MessageAction == MessageAction.Void),
                It.IsAny<MarketStatus>()),
                Times.Once());
        }

        private InternalDealRequestConsumer CreateConsumer()
        {
            var mockErrorService = new Mock<IErrorService>();

            return new InternalDealRequestConsumer(MockDbContextFactory.Stub(),
                                                   _mockDealService.Object,
                                                   _mockUserService.Object,
                                                   _mockEmailService.Object,
                                                   _mockBus.Object,
                                                   _mockProductService.Object,
                                                   _mockOrderService.Object, mockErrorService.Object);
        }

        private static Deal SetStatusAsVoid(Deal deal)
        {
            deal.DealStatus = DealStatus.Void;
            return deal;
        }
    }
}
