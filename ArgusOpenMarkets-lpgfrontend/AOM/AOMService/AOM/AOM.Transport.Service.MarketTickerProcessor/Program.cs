﻿namespace AOM.Transport.Service.MarketTickerProcessor
{
    using System;
    using System.Diagnostics;
    using System.Linq;

    using AOM.Repository.MySql;
    using AOM.Services;
    using AOM.Transport.Service.Processor.Common;

    using log4net;

    using Utils.Logging.Utils;

    using Ninject;

    internal class Program
    {
        internal static void Main()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(
                    new EFModelBootstrapper(),
                    new LocalBootstrapper(),
                    new ServiceBootstrap(),
                    new ProcessorCommonBootstrap());

                WarmUp(kernel);
                ServiceHelper.BuildAndRunService<MarketTickerProcessorService>(kernel);
            }
        }

        private static void WarmUp(StandardKernel kernel)
        {
            var timer = Stopwatch.StartNew();

            try
            {
                var dbFactory = kernel.Get<IDbContextFactory>();
                using (var dbCrm = dbFactory.CreateCrmModel())
                {
                    dbCrm.UserInfoes.Any();
                }

                using (var dbAom = dbFactory.CreateAomModel())
                {
                    dbAom.MarketInfoItems.Any();
                }

                Log.Info(
                    string.Format(
                        "MarketTickerProcessor -Initial EF model build took {0} msecs.",
                        timer.ElapsedMilliseconds));

            }
            catch (Exception ex)
            {
                Log.Error("MarketTickerProcessor - WarmUp - Error - cannot connect to DB", ex);
                ServiceHelper.BuildAndRunService<MarketTickerProcessorService>(kernel);
            }
        }

        private static void StopLogging()
        {
            LogManager.Shutdown();
        }
    }
}