﻿using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.MarketTickerProcessor.Internals
{
    class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get { return DisplayName.Replace(" ", ""); }
        }

        public string DisplayName
        {
            get { return "AOM Transport Service Market Ticker Processor"; }
        }

        public string Description
        {
            get { return "AOM Transport Service Market Ticker Processor"; }
        }
    }
}