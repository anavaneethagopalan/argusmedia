﻿namespace AOM.Transport.Service.MarketTickerProcessor
{
    using System.Collections.Generic;

    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    internal class MarketTickerProcessorService : ProcessorServiceBase
    {
        public MarketTickerProcessorService(
            IBus aomBus,
            IEnumerable<IConsumer> consumers,
            IServiceManagement serviceManagement)
            : base(consumers, serviceManagement, aomBus)
        {
        }

        public override string Name
        {
            get { return "MarketTickerProcessorService"; }
        }
    }
}