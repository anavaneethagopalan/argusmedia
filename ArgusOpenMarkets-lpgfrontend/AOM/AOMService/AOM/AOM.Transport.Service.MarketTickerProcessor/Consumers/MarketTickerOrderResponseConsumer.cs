﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using Argus.Transport.Infrastructure;
using System;
using System.Data.Common;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    public class MarketTickerOrderResponseConsumer : MarketTickerConsumerBase
    {

        public MarketTickerOrderResponseConsumer(
            IDbContextFactory dbFactory,
            IMarketTickerService marketTickerService,
            IBus aomBus)
            : base(dbFactory, marketTickerService, aomBus)
        {
        }

        public override void Start()
        {
            _aomBus.Subscribe<AomOrderResponse>(SubscriptionId, ConsumeAomOrderResponse,
                configuration => configuration.WithTopic("Market." + MarketStatus.Open));
        }

        internal void ConsumeAomOrderResponse(AomOrderResponse orderResponse)
        {
            Log.Debug("Order Response pickedup in Market Ticker Processor");

            switch (orderResponse.Message.MessageAction)
            {
                case MessageAction.Execute:
                    return; // no tick for this action

                default:
                    try
                    {
                        if (orderResponse.Message.MessageType == MessageType.Error)
                        {
                            return;
                        }

                        var order = orderResponse.Order;
                        if (order == null)
                        {
                            throw new ArgumentException("Null order in orderResponse");
                        }
                        if (order.IsVirtual
                            || (order.LastOrderStatus == OrderStatus.Held
                                && orderResponse.Message.MessageAction == MessageAction.Kill))
                        {
                            return;
                        }

                        using (var aomDb = DbFactory.CreateAomModel())
                        {
                            var incomingMessage = orderResponse.Message;
                            var message = _marketTickerService.CreateOrderMarketTickerItemMessage(
                                aomDb,
                                order,
                                incomingMessage);
                            message.ClientSessionInfo = orderResponse.Message.ClientSessionInfo;
                            CreateAndPublishMarketTickerResponse(message);
                        }
                    }
                    catch (DbException exception)
                    {
                        // We have encountered a DB Exception - Re-throwing the original exception.  
                        Log.Error("Db Exception - Error Creating Market Tick for Order", exception);
                        throw;
                    }
                    catch (Exception exception)
                    {
                        Log.Error("Error Creating Market Tick", exception);
                    }
                    break;
            }
        }
    }
}