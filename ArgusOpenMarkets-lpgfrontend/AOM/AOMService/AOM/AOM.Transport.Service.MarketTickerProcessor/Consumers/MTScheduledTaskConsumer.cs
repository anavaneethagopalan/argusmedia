﻿using AOM.App.Domain.Dates;
using AOM.Repository.MySql;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Events.ScheduledTasks;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;

namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    internal class MtScheduledTaskConsumer : ScheduledTaskConsumerBase<PurgeMarketTickerRequest>
    {
        public MtScheduledTaskConsumer(IBus aomBus, IDateTimeProvider dateTimeProvider, IDbContextFactory dbContextFactory = null)
            : base(aomBus, dateTimeProvider, dbContextFactory)
        { }

        public override void Start()
        {
            AomBus.Respond<CreateMTScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(ConsumeCreateNewScheduledTaskRequest);
            AomBus.Respond<DeleteMTScheduledTaskRequestRpc, ScheduledTaskResponseRpc>(ConsumeDeleteScheduledTasksRequest);
            base.Start();
        }
    }
}