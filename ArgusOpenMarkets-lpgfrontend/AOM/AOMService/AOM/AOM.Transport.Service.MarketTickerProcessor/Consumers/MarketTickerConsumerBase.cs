﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketTickers;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    public abstract class MarketTickerConsumerBase : IConsumer
    {
        protected readonly IDbContextFactory DbFactory;

        protected readonly IMarketTickerService _marketTickerService;

        protected readonly IBus _aomBus;

        protected MarketTickerConsumerBase(
            IDbContextFactory dbFactory,
            IMarketTickerService marketTickerService,
            IBus aomBus)
        {
            DbFactory = dbFactory;
            _marketTickerService = marketTickerService;
            _aomBus = aomBus;
        }

        public abstract void Start();

        /// <summary>
        /// Publishes a MarketTickerResponse given a message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="additionalData">Optional, additional data related to the ticker message</param>
        protected void CreateAndPublishMarketTickerResponse(Message message,
            AdditionalMarketTickerData additionalData = null)
        {
            try
            {
                var response = new AomNewMarketTickResponse
                {
                    MarketTickerItem = message.MessageBody as MarketTickerItem,
                    AdditionalData = additionalData ?? new AdditionalMarketTickerData(),
                    Message =
                        new Message
                        {
                            ClientSessionInfo = message.ClientSessionInfo,
                            MessageAction = message.MessageAction,
                            MessageType = message.MessageType,
                            MessageBody = ""
                        }
                };

                if (response.MarketTickerItem != null)
                {
                    _aomBus.Publish(response);
                }
            }
            catch (Exception exception)
            {
                Log.Error("Error sending market tick to market", exception);
            }
        }

        protected virtual string SubscriptionId
        {
            get
            {
                return "MarketTicker";
            }
        }
    }
}