﻿namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    using System;
    using Utils.Logging.Utils;
    using Argus.Transport.Infrastructure;
    using Processor.Common;
    using Events.MarketTickers;

    internal class PurgeMarketTickerRequestConsumer : IConsumer
    {
        private readonly IBus _aomBus;

        public PurgeMarketTickerRequestConsumer(IBus aomBus)
        {
            _aomBus = aomBus;
        }

        public void Start()
        {
            _aomBus.Subscribe<PurgeMarketTickerRequest>(
                SubscriptionId,
                ConsumePurgeMarketTickerRequest<PurgeMarketTickerRequest, PurgeMarketTickerResponse>);
        }

        private void ConsumePurgeMarketTickerRequest<TReq, TFwd>(TReq request) where TReq : PurgeMarketTickerRequest
            where TFwd : PurgeMarketTickerResponse, new()
        {
            string requestType = typeof(TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd {DaysToKeep = request.DaysToKeep};

                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                Log.Warn(
                    String.Format("Unable to publish Purge MT Command to Control Client: {0}", exception.Message));
            }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }
    }
}