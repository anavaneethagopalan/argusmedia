﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events.MarketTickers;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Generic;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    internal class MarketTickerContinuousScrollRequestConsumer : MarketTickerConsumerBase
    {
        public MarketTickerContinuousScrollRequestConsumer(
            IDbContextFactory dbFactory,
            IMarketTickerService marketTickerService,
            IBus aomBus)
            : base(dbFactory, marketTickerService, aomBus)
        {
        }

        public override void Start()
        {
            _aomBus.Respond<AomMarketTickerContinuousScrollRequestRpc, AomMarketTickerContinuousScrollResponseRpc>(
                RetrieveNextMarketTickerItems);
        }

        internal AomMarketTickerContinuousScrollResponseRpc RetrieveNextMarketTickerItems(
            AomMarketTickerContinuousScrollRequestRpc continuousScrollRequest)
        {
            try
            {
                var responseRpc = new AomMarketTickerContinuousScrollResponseRpc();

                using (var aomDb = DbFactory.CreateAomModel())
                {
                    try
                    {
                        responseRpc = _marketTickerService.ReturnNextMarketTickerItemsFromDb(
                            aomDb,
                            continuousScrollRequest.EarliestMarketTickerItemId,
                            continuousScrollRequest.NumberOfMarketTickerItemsRequired,
                            continuousScrollRequest.ProductIdList,
                            continuousScrollRequest.Pending,
                            continuousScrollRequest.MarketTickerItemTypes);
                        return responseRpc;
                    }
                    catch (Exception exception)
                    {
                        Log.Error("Failed to retrieve MarketTickerItems for continuous scroll", exception);
                        responseRpc.ErrorMessage = "Failed to retrieve MarketTickerItems";
                        return responseRpc;
                    }
                }
            }
            catch (Exception e)
            {

                Log.Error("RetrieveNextMarketTickerItems", e);
            }

            return new AomMarketTickerContinuousScrollResponseRpc
            {
                EarliestTickerItemId = -1,
                MarketTickerItems = new List<MarketTickerItem>()
            };
        }

    }
}