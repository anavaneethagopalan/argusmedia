﻿using System;
using System.Data.Common;

using AOM.Repository.MySql;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.Deals;
using AOM.Transport.Events.ExternalDeals;
using AOM.Transport.Events.MarketTickers;
using Argus.Transport.Infrastructure;

using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    public class MarketTickerDealConsumer : MarketTickerConsumerBase
    {
        public MarketTickerDealConsumer(
            IDbContextFactory dbFactory,
            IMarketTickerService marketTickerService,
            IBus aomBus)
            : base(dbFactory, marketTickerService, aomBus)
        {
        }

        public override void Start()
        {
            // We had an issue - whereby we consumed the Deal Response - and a Deadlock Error was generated.
            // We are going to re-throw this exception - so the message should be placed back onto the bus.
            // Hopefully the next time it's picked up - the deadlock error can be avoided.
            _aomBus.Subscribe<AomDealResponse>(SubscriptionId, ConsumeAomDealResponse);
            _aomBus.Subscribe<AomExternalDealResponse>(SubscriptionId, ConsumeExternalDealResponse);
        }

        internal void ConsumeAomDealResponse(AomDealResponse response)
        {
            Log.Debug("New deal response pickedup in Market Ticker Processor");

            if (response.Message.MessageType == MessageType.Error ||
                response.Message.MessageAction == MessageAction.Request)
            {
                return;
            }

            try
            {
                using (var aomDb = DbFactory.CreateAomModel())
                {
                    var messages = _marketTickerService.CreateDealMarketTick(aomDb, response);
                    foreach (var message in messages)
                    {
                        message.ClientSessionInfo = response.Message.ClientSessionInfo;
                        CreateAndPublishMarketTickerResponse(message);
                    }
                }
            }
            catch (DbException ex)
            {
                Log.Error("DbException - Error Creating Market Tick for Deal Done", ex);
                // Throw the exception - so should be placed back onto the bus to be executed again.
                throw;
            }
            catch (Exception exception)
            {
                Log.Error("Error Creating Market Tick ", exception);
            }
        }

        private void ConsumeExternalDealResponse(AomExternalDealResponse response)
        {
            Log.Debug("New external deal response pickedup in Market Ticker Processor");

            if (response.Message.MessageType == MessageType.Error ||
                response.Message.MessageAction == MessageAction.Request)
            {
                return;
            }

            try
            {
                using (var aomDb = DbFactory.CreateAomModel())
                {
                    var messages = _marketTickerService.CreateExternalDealMarketTick(aomDb, response);
                    foreach (var message in messages)
                    {
                        message.ClientSessionInfo = response.Message.ClientSessionInfo;
                        var additionalData = new AdditionalMarketTickerData
                        {
                            PreviousDealStatus = response.ExternalDeal.PreviousDealStatus
                        };
                        CreateAndPublishMarketTickerResponse(message, additionalData);
                    }
                }
            }
            catch (DbException exception)
            {
                // We have encountered a DB Exception - Re-throwing the original exception.  
                Log.Error("DB Exception - Error Creating Market Tick for Deal", exception);
                throw;
            }
            catch (Exception exception)
            {
                Log.Error("Error Creating Market Tick ", exception);
            }
        }

    }
}