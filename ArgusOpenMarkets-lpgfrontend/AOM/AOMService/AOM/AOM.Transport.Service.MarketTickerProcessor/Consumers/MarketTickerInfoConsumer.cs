﻿using System;
using System.Data.Common;

using AOM.Repository.MySql;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    public class MarketTickerInfoConsumer : MarketTickerConsumerBase
    {
        public MarketTickerInfoConsumer(IDbContextFactory dbFactory, IMarketTickerService marketTickerService,
            IBus aomBus)
            : base(dbFactory, marketTickerService, aomBus)
        {
        }

        public override void Start()
        {
            _aomBus.Subscribe<AomMarketInfoResponse>(SubscriptionId, ConsumeMarketInfo);
        }

        private void ConsumeMarketInfo(AomMarketInfoResponse response)
        {
            Log.Debug("New market info response picked up in Market Ticker Processor");

            if (response.Message.MessageType == MessageType.Error ||
                response.Message.MessageAction == MessageAction.Request)
            {
                return;
            }

            try
            {
                using (var aomDb = DbFactory.CreateAomModel())
                {
                    var messages = _marketTickerService.CreateMarketInfoMarketTick(aomDb, response);
                    foreach (var message in messages)
                    {
                        message.ClientSessionInfo = response.Message.ClientSessionInfo;
                        CreateAndPublishMarketTickerResponse(message);
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error("Error Creating Market Tick for Market Info.", exception);
            }
        }
    }
}