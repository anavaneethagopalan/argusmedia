﻿using AOM.Repository.MySql;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events;
using AOM.Transport.Events.Products;
using Argus.Transport.Infrastructure;
using System;
using System.Data.Common;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    public class MarketTickerMarketStatusChangeConsumer : MarketTickerConsumerBase
    {

        public MarketTickerMarketStatusChangeConsumer(
            IDbContextFactory dbFactory,
            IMarketTickerService marketTickerService,
            IBus aomBus)
            : base(dbFactory, marketTickerService, aomBus)
        {
        }

        public override void Start()
        {
            _aomBus.Subscribe<AomMarketStatusChange>(SubscriptionId, ConsumeMarketStatusChange);
        }

        internal void ConsumeMarketStatusChange(AomMarketStatusChange marketChange)
        {
            Log.Debug("New market status change pickedup in Market Ticker Processor");

            if (marketChange.Message.MessageType == MessageType.Error
                || marketChange.Product.IsInternal)
            {
                return;
            }

            try
            {
                using (var aomDb = DbFactory.CreateAomModel())
                {
                    var message = _marketTickerService.CreateMarketStatusChangeMarketTick(aomDb, marketChange);
                    message.ClientSessionInfo = marketChange.Message.ClientSessionInfo;
                    CreateAndPublishMarketTickerResponse(message);
                }
            }
            catch (DbException exception)
            {
                // We have encountered a DB Exception - Re-throwing the original exception.  
                Log.Error("DB Exception - Error Creating Market Tick for Market Status Change", exception);
                throw;
            }
            catch (Exception exception)
            {
                Log.Error("Error Creating Market Tick ", exception);
            }
        }
    }
}