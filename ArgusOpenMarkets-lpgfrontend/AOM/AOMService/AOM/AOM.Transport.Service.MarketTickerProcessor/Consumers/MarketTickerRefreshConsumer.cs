﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Services.ErrorService;
using AOM.Services.MarketTickerService;
using AOM.Transport.Events.MarketTickers;
using Argus.Transport.Infrastructure;
using System;
using System.Diagnostics;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    /// <summary>
    /// The MarketTickerDealConsumer consumes DealResponses for all actions performed
    /// against a Deal.  These messages are then sent to the MarketTickerService which creates
    /// the Deal Market Ticker message which is published to the clients.
    /// </summary>
    public class MarketTickerRefreshConsumer : MarketTickerConsumerBase
    {
        private readonly IErrorService _errorService;

        public MarketTickerRefreshConsumer(
            IDbContextFactory dbFactory,
            IMarketTickerService marketTickerService,
            IBus aomBus,
            IErrorService errorService)
            : base(dbFactory, marketTickerService, aomBus)
        {
            _errorService = errorService;
        }

        public override void Start()
        {
            _aomBus.Subscribe<AomRefreshMarketTickerRequest>(SubscriptionId, ConsumeAomRefreshMarketTickerRequest);
        }

        internal void ConsumeAomRefreshMarketTickerRequest(AomRefreshMarketTickerRequest request)
        {
            Log.Debug("AomRefreshMarketTickerRequest picked up in Market Ticker Processor");

            try
            {
                using (var aomDb = DbFactory.CreateAomModel())
                {
                    var timer = Stopwatch.StartNew();

                    var recoveryItems = _marketTickerService.GetItemsForRecovery(aomDb, request.RefreshFromTime,
                        request.RefreshToTime);

                    foreach (var r in recoveryItems)
                    {
                        CreateAndPublishMarketTickerResponse(r);
                    }

                    Log.Info(string.Format("Refresh of {0} market ticker items took {1} msecs", recoveryItems.Count,
                        timer.ElapsedMilliseconds));
                }
            }
            catch (Exception e)
            {
                _errorService.LogUserError(new UserError
                {
                    Error = e,
                    AdditionalInformation = "MarketTickerRestartRecovery Failure",
                    ErrorText = string.Empty,
                    Source = "MarketTickerRefreshConsumer"
                });

                throw;
            }
        }
    }
}