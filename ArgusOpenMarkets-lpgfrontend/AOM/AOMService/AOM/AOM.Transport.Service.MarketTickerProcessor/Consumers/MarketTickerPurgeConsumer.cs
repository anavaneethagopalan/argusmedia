﻿namespace AOM.Transport.Service.MarketTickerProcessor.Consumers
{
    using AOM.App.Domain.Dates;
    using AOM.Repository.MySql;
    using AOM.Transport.Events.MarketTickers;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    internal class MarketTickerPurgeConsumer : ScheduledTaskConsumerBase<PurgeMarketTickerRequest>
    {
        public MarketTickerPurgeConsumer(IBus aomBus, IDateTimeProvider dateTimeProvider, IDbContextFactory dbContextFactory = null)
            : base(aomBus, dateTimeProvider, dbContextFactory)
        { }
    }
}