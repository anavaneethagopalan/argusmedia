﻿using AOM.App.Domain.Interfaces;

namespace AOM.TopicManager
{
    public static class UserTopicManager
    {
        private const string RootTopicForUser = "AOM/Users/";

        public static string UserTopicPath(long userId)
        {
            return RootTopicForUser + userId;
        }

        public static string GetUserTopic(IUser user)
        {
            return user != null ? UserTopicPath(user.Id) : string.Empty;
        }
    }
}