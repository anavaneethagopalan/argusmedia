﻿using System.Collections.Generic;

namespace AOM.TopicManager
{
    public static class NewsTopicManager
    {
        private const string FreeNewsTopic = "AOM/News/Free";
        private const string RootTopicForNews = "AOM/News/ContentStreams/";

        public static List<string> SubscribeUserToNewsTopicsForContentStream(long contentStreamId)
        {
            var topics = new List<string>();
            topics.Add(TopicHelper.MakeWildCardTopic(RootTopicForNews + contentStreamId));
            return topics;
        }

        public static List<string> SubscribeUserToFreeNews()
        {
            var topics = new List<string>();
            topics.Add(TopicHelper.MakeWildCardTopic(FreeNewsTopic));
            return topics;
        }

        public static string NewsTopicPath(ulong commodityId, string cmsId, bool isFree, long contentStreamId)
        {
            var topicPath = "AOM/News/";

            if (isFree)
            {
                // NOTE: Do not require a SEC-Commodity here - checked.
                topicPath += string.Format("Free/{0}/{1}", commodityId, cmsId);
            }
            else
            {
                // NTB - Moving to Non-Secure Topics paths...
                topicPath += string.Format("ContentStreams/{0}/{1}", contentStreamId, cmsId);
            }

            return topicPath;
        }
    }
}