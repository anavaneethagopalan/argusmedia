﻿namespace AOM.TopicManager
{
    public static class ContentStreamTopicManager
    {
        private const string RootTopicForContentStreams = "AOM/Products/";

        public static string ContentStreamTopicPath(object productId)
        {
            return RootTopicForContentStreams + productId + "/ContentStreams";
        }
    }
}