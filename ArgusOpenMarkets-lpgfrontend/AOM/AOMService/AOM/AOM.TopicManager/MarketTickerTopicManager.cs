﻿using System.Collections.Generic;

using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;

namespace AOM.TopicManager
{
    public static class MarketTickerTopicManager
    {
        private const string MarketTickerRootTopic = "AOM/MarketTicker/";

        public static string GetRootMarketTickerTopic()
        {
            // ALL ORDERS WILL NOW BE PLACED UNDER THE PRODUCT ID - THESE ARE ORDERS THAT ARE NOT HELD - SO AVAILABLE TO EVERYONE IN THE MARKET
            return MarketTickerRootTopic;
        }

        public static IEnumerable<string> CreateTopicsForAllOrgMarketTickerItem(long marketTickerId, List<long> productIds, MarketTickerItemType marketTickerItemType)
        {
            return CreateProductSpecificTopicPaths(marketTickerId, productIds, marketTickerItemType);
        }

        public static List<string> CreateProductSpecificTopicPaths(long marketTickerId, List<long> productIds, MarketTickerItemType marketTickerItemType)
        {
            List<string> allOrgTopicPaths = new List<string>();
            foreach (var productId in productIds)
            {
                // NOTE: USED TO GENERATE A TOPIC PATH OF: 
                // AOM/MarketTicker/SEC-ViewableBy/All/Products/1/Bid/1
                // WE ARE CHANGING THIS TOPIC PATH TO: 
                // AOM/MarketTicker/Products/1/All/<MarketTickerId>
                // NOTE: DO WE NEED TO DISTINGUISH BETWEEN A BID AND A ASK?

                // TODO: What do we need to do if the user is an Editor??
                var allOrgProductPath = MarketTickerRootTopic + "Products/" + productId + "/All/" + marketTickerId;
                allOrgTopicPaths.Add(allOrgProductPath);
            }

            return allOrgTopicPaths;
        }

        public static List<string> CreateOwnOrganisationMarketTickerTopics(MarketTickerItem marketTickerItem)
        {
            var topics = new List<string>();
            topics.AddRange(CreateTopicsForEditorMarketTickerItem(marketTickerItem.Id, marketTickerItem.ProductIds, marketTickerItem.MarketTickerItemStatus));
            topics.AddRange(CreateTopicsForOwnOrgMarketTickerItem(marketTickerItem.Id, marketTickerItem.ProductIds, marketTickerItem.OwnerOrganisations));

            return topics;
        }

        public static List<string> GetOwnOrganisationTopicsToDelete(long marketTickerItemId, List<long> productIds, List<long> ownerOrganisationIds)
        {
            List<string> ownOrgTopicsToDelete = new List<string>();

            ownerOrganisationIds = MakeDistinctList(ownerOrganisationIds);
            // NTB - OLD CODE - REPLACED - THE OWN ORGANISATION TOPIC SHOULD NOW BE BELOW....
            // ownOrgTopicsToDelete.Add(MarketTickerRootTopic + ".*/Deal/" + marketTickerItemId);
            // ownOrgTopicsToDelete.Add(MarketTickerRootTopic + ".*/Info/" + marketTickerItemId);

            // THE NEW OWN ORG TOPIC IS: 
            // AOM/MarketTicker/Products/1/60/3019
            foreach (var productId in productIds)
            {
                foreach (var orgId in ownerOrganisationIds)
                {
                    ownOrgTopicsToDelete.Add(MarketTickerRootTopic + "Products/" + productId + "/" + orgId + "/" + marketTickerItemId);

                    // NOTE: Should be a valid delete for organisation: 
                    // AOM/MarketTicker/Products/1/60/3047
                    // AOM/MarketTicker/Products/1/Editor/3047
                }

                // For each product - we also have to knock out the Editor Topic.
                ownOrgTopicsToDelete.Add(MarketTickerRootTopic + "Products/" + productId + "/Editor/" + marketTickerItemId);
            }

            return ownOrgTopicsToDelete;
        }

        public static IEnumerable<string> CreateTopicsForEditorMarketTickerItem(long marketTickerId, List<long> productIds, MarketTickerItemStatus marketTickerItemStatus)
        {
            // NOTE: THE OLD CODE WAS PUTTING EDITOR MARKET TICKER ITEM(S) HERE: 
            // AOM/MarketTicker/SEC-ViewableBy/Editors/Products/1/Info/30480

            // WHEN A USER ADDS A MARKET TICKER ITEM IT GOES TO EDITOR AND ORGANISATION
            // EDITOR - AOM/MarketTicker/SEC-ViewableBy/Editors/Products/1/Info/30509
            // USER -   AOM/MarketTicker/SEC-ViewableBy/35/Products/1/Info/30509

            // THE NEW CODE IS PUTTING MARKET TICKER ITEMS FOR EDITORS HERE: 
            // AOM/MarketTicker/Products/<ProductId>/Editor/<MarketTickerId>

            List<string> editorsTopics = new List<string>();

            if (marketTickerItemStatus != MarketTickerItemStatus.Held)
            {
                foreach (var productId in productIds)
                {
                    editorsTopics.Add(MarketTickerRootTopic + "Products/" + productId + "/Editor/" + marketTickerId);
                }
            }

            return editorsTopics;
        }

        private static List<string> CreateTopicsForOwnOrgMarketTickerItem(long marketTickerId,
            List<long> productIds,
            List<long> ownerOrganisations)
        {
            // NOTE: USED TO BUILD THE FOLLOWING TOPIC PATH: 
            // "AOM/MarketTicker//SEC-ViewableBy/60/Products/1/Info/4"
            // NOW BUILDING THE TOPIC PATH: 
            // Aom/MarketTicker/Products/<ProductId>/<OrganisationId>/<MarketTickerId>
            List<string> topicsForOwnOrg = new List<string>();
            foreach (var orgId in MakeDistinctList(ownerOrganisations))
            {
                foreach (var prodId in productIds)
                {
                    var ownOrgMarketTickerTopic = MarketTickerRootTopic + "Products/" + prodId + "/" + orgId + "/" + marketTickerId;
                    topicsForOwnOrg.Add(ownOrgMarketTickerTopic);
                }
            }

            return topicsForOwnOrg;
        }

        private static List<long> MakeDistinctList(List<long> ownerOrganisations)
        {
            List<long> distinctIds = new List<long>();

            if (ownerOrganisations != null)
            {
                foreach (var id in ownerOrganisations)
                {
                    if (!distinctIds.Contains(id))
                    {
                        distinctIds.Add(id);
                    }
                }
            }

            return distinctIds;
        }

        public static List<string> SubscribeUserToMarketTickerTopics(long userId, long productId, long organisationId, Dictionary<string, bool> productPrivileges)
        {
            var topics = new List<string>();
            
            // Subscribe the user to ALL Tickers Items for this Product.  
            topics.Add(TopicHelper.MakeWildCardTopic(MarketTickerRootTopic + "Products/" + productId + "/All/"));

            // Subscribe the User to His Own Organisation Topics for this Product.
            topics.Add(TopicHelper.MakeWildCardTopic(MarketTickerRootTopic + "Products/" + productId + "/" + organisationId + "/"));

            // Is this user an Editor?  If he/she is - then we need to subscribe the user to the /Editor topic path.
            // AOM/MarketTicker/Products/<ProductId>/Editor/<MarketTickerId>

            if (IsUserEditorForThisProduct(productId, productPrivileges))
            {
                // TODO: Build this topic and add into the list of Topic(s)....
                topics.Add(TopicHelper.MakeWildCardTopic(MarketTickerRootTopic + "Products/" + productId + "/Editor"));
            }

            return topics;
        }
        
        public static bool IsUserEditorForThisProduct(long productId, Dictionary<string, bool> productPrivileges)
        {
            if (productPrivileges == null)
            {
                return false;
            }

            return productPrivileges.ContainsKey(ProductPrivileges.MarketTicker.Authenticate);
        }
    }
}