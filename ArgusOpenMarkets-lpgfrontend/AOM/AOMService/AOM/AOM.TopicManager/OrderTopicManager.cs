﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using Utils.Logging.Utils;

namespace AOM.TopicManager
{
    public static class OrderTopicManager
    {
        public static string CreateProductTopicPath(long productId)
        {
            var createProductPath = string.Join("/", "AOM", "Orders", "Products", productId);
            //Log.InfoFormat("CreateProductTopicPath_TOPIC_PATH:{0}", createProductPath);
            return createProductPath;
        }

        public static string CreateAllOrdersRootTopicPath(long productId)
        {
            // ALL ORDERS WILL NOW BE PLACED UNDER THE PRODUCT ID - THESE ARE ORDERS THAT ARE NOT HELD - SO AVAILABLE TO EVERYONE IN THE MARKET
            var allOrdersRootTopicPath = string.Join("/", CreateProductTopicPath(productId), "All");
            //Log.InfoFormat("CreateAllOrdersRootTopicPath_TOPIC_PATH:{0}", allOrdersRootTopicPath);
            return allOrdersRootTopicPath;
        }

        public static string CreateAllOrdersTopicPathForOrder(long productId, long orderId)
        {
            var allOrdersTopicPathForSpecificOrder = string.Join("/", CreateAllOrdersRootTopicPath(productId), orderId);
            return allOrdersTopicPathForSpecificOrder;
        }

        public static string CreateOrganisationOrdersTopicPathForOrder(long productId, long organisationId, long orderId)
        {
            var allOrdersTopicPathForSpecificOrder = string.Join("/", CreateProductTopicPath(productId), organisationId, orderId);
            Log.InfoFormat("CreateOrganisationOrdersTopicPathForOrder_TOPIC_PATH:{0}", allOrdersTopicPathForSpecificOrder);
            return allOrdersTopicPathForSpecificOrder;
        }

        public static List<string> CreateHeldOrderTopicPaths(long productId, ExecutionMode executionMode, long orderId, long principalOrganisationId, long? brokerOrganisationId)
        {
            // TODO: Need to do something with the Execution Mode.
            // - Internal = Normal Order
            // - External = AOM LITE Order.

            List<string> heldTopicPaths = new List<string>();
            var heldTopicForProductForPrincipal = string.Join("/", CreateProductTopicPath(productId), principalOrganisationId, orderId);
            heldTopicPaths.Add(heldTopicForProductForPrincipal);
            Log.InfoFormat("CreateHeldOrderTopicPathsForPrincipal_TOPIC_PATH:{0}", heldTopicForProductForPrincipal);

            if (brokerOrganisationId.HasValue)
            {
                var heldTopicForProductForBroker = string.Join("/", CreateProductTopicPath(productId), brokerOrganisationId.Value, orderId);
                heldTopicPaths.Add(heldTopicForProductForBroker);
                Log.InfoFormat("CreateHeldOrderTopicPathsForBroker_TOPIC_PATH:{0}", heldTopicForProductForBroker);
            }

            return heldTopicPaths;
        }

        public static List<string> SubscribeUserToOrderTopicsForProduct(long productId, long organisationId)
        {
            var topics = new List<string>();

            // Example Order Topics
            // AOM/Orders/Products/12/All
            // AOM/Orders/Products/12/34/All  -- Where 34 is the Organisation Id
            var rootTopicForProduct = CreateProductTopicPath(productId);
            topics.Add(TopicHelper.MakeWildCardTopic(rootTopicForProduct + "/All"));
            topics.Add(TopicHelper.MakeWildCardTopic(rootTopicForProduct + "/" + organisationId));

            return topics;
        }
    }
}