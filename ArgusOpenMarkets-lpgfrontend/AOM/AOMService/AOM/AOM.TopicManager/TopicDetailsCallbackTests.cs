﻿using AOMDiffusion.Common;
using NUnit.Framework;

namespace AOM.TopicManager
{
    [TestFixture]
    public class TopicDetailsCallbackTests
    {
        [Test]
        public void OnUnknownTopicShouldCallTopicDoesNotExistPassingNoTopic()
        {
            var unknownTopic = string.Empty;

            var topicDetailsCallback = new TopicDetailsCallback();
            topicDetailsCallback.TopicDoesNotExist += (sender, s) =>
            {
                unknownTopic = s;
            };

            topicDetailsCallback.OnTopicUnknown("AOM/Data");

            Assert.That(unknownTopic, Is.EqualTo("NoTopic"));

        }
    }
}