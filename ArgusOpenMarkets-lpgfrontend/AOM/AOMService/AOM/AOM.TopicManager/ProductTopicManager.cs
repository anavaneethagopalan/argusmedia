﻿using System.Collections.Generic;

namespace AOM.TopicManager
{
    public static class ProductTopicManager
    {
        private const string RootTopicForProduct = "AOM/Products/";

        public static string ProductTopicPath(long productId)
        {
            return RootTopicForProduct + productId;
        }

        public static string MarketStatusTopicPath(long productId)
        {
            return ProductTopicPath(productId) + "/MarketStatus";
        }

        public static List<string> SubscribeUserToProductTopics(long ProductId)
        {
            var topics = new List<string>();


            var topicSelector = string.Format("AOM/Products/{0}", ProductId);
            topics.Add(TopicHelper.MakeWildCardTopic(topicSelector));

            return topics;
        }

        private static long ExtractProductIdFromTopicSelector(string topicSelector)
        {
            // Sample topic path for Orders: 
            // AOM/Products/<Product_Id>/Definition
            long productId = -1;
            if (!string.IsNullOrEmpty(topicSelector))
            {
                var topicParts = topicSelector.Split('/');
                if (topicParts.Length > 2)
                {
                    var prodId = topicParts[2];
                    long.TryParse(prodId, out productId);
                }
            }
            return productId;
        }
    }
}