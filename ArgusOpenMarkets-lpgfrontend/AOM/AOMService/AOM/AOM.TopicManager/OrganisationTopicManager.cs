﻿using System.Collections.Generic;
using Utils.Logging.Utils;

namespace AOM.TopicManager
{
    public static class OrganisationTopicManager
    {
        private const string OrganisationTopicPath = "AOM/Organisations/{0}";

        private const string SubscribeProductsTopicPath = "AOM/Organisations/{0}/SubscribedProducts";

        private const string PermissionsMatrixTopicPath = "AOM/Organisations/{0}/PermissionsMatrix/Summary/Products/";

        private const string SystemNotificationsTopicPath = "AOM/Organisations/{0}/SystemNotifications/";

        private const string SystemNotificationTypesTopicPath = "AOM/Organisations/{0}/SystemNotificationTypes/";

        public static List<string> SubscribeUserToOrganisationTopics(App.Domain.Interfaces.IUser user, string topicSelector)
        {
            List<string> topicsToSubscribeUserTo = new List<string>();
            var topicSelectorUpper = topicSelector.ToUpper();
            if (topicSelectorUpper.Contains("SUBSCRIBEDPRODUCTS"))
            {
                // Only subscribe the user to Subscribed Products for his/her organisation.  
                // Sample topic path to subscribe user to: AOM/Organisations/63/SubscribedProducts
                topicsToSubscribeUserTo.Add(TopicHelper.MakeWildCardTopic(string.Format(SubscribeProductsTopicPath, user.OrganisationId)));
            }
            else if (topicSelectorUpper.Contains("PERMISSIONSMATRIX"))
            {
                topicsToSubscribeUserTo.Add(TopicHelper.MakeWildCardTopic(string.Format(PermissionsMatrixTopicPath, user.OrganisationId)));
            }
            else if (topicSelectorUpper.Contains("SYSTEMNOTIFICATIONS"))
            {
                topicsToSubscribeUserTo.Add(TopicHelper.MakeWildCardTopic(string.Format(SystemNotificationsTopicPath, user.OrganisationId)));
            }
            else if (topicSelectorUpper.Contains("SYSTEMNOTIFICATIONTYPES"))
            {
                topicsToSubscribeUserTo.Add(TopicHelper.MakeWildCardTopic(string.Format(SystemNotificationTypesTopicPath, user.OrganisationId)));
            }
            else
            {
                Log.Error("Subscribe User to Organisation Topic request for an unsported topic");
            }
            return topicsToSubscribeUserTo;
        }

        internal static IEnumerable<string> SubscribeUserToOrganisationTopics(long userId, long organisationId)
        {
            List<string> topicsToSubscribeUserTo = new List<string>();
            topicsToSubscribeUserTo.Add(TopicHelper.MakeWildCardTopic(string.Format(OrganisationTopicPath, organisationId)));
            return topicsToSubscribeUserTo;
        }
    }
}
