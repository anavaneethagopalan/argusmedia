﻿using System.Collections.Generic;
using AOM.App.Domain.Interfaces;

namespace AOM.TopicManager
{
    public static class AssessmentTopicManager
    {
        private const string RootTopicForAssessments = "AOM/Products/";

        public static List<string> SubscribeUserToAssessmentTopics(IUser user, string topicSelector)
        {
            // Sample topic the user is subscribing with: 
            // AOM/Products/<productId>/Assessment

            var topics = new List<string>();
            var productId = GetProductIdFromTopicSelector(topicSelector);

            foreach (var product in user.ProductPrivileges)
            {
                if (product.ProductId == productId)
                {
                    // User is subscribed to this topic - ALLOW.   
                    topics.Add(TopicHelper.MakeWildCardTopic(RootTopicForAssessments + productId + "/Assessment"));
                    break;
                }
            }

            return topics;
        }


        private static long GetProductIdFromTopicSelector(string topicSelector)
        {
            // Sample topic path for Orders: 
            // AOM/Orders/Products/12 -- Where 12 is the Product Id.
            long productId = -1;
            if (!string.IsNullOrEmpty(topicSelector))
            {
                var topicParts = topicSelector.Split('/');
                if (topicParts.Length >= 2)
                {
                    var prodId = topicParts[2];
                    long.TryParse(prodId, out productId);
                }
            }
            return productId;
        }

        public static string AssessmentTopicPath(object productId)
        {
            return RootTopicForAssessments + productId + "/Assessment";
        }
    }
}