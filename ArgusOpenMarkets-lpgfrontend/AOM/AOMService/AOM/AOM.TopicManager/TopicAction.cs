﻿namespace AOM.TopicManager
{
    public enum TopicAction
    {
        Subscribe,
        UnSubscribe
    }
}