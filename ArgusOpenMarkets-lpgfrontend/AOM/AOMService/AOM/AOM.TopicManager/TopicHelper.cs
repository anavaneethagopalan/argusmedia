﻿namespace AOM.TopicManager
{
    public static class TopicHelper
    {
        public static string MakeWildCardTopic(string topicFunction)
        {
            if (string.IsNullOrEmpty(topicFunction))
            {
                return string.Empty;
            }

            if (topicFunction.StartsWith("/"))
            {
                topicFunction = topicFunction.Substring(1);
            }

            topicFunction = "?" + topicFunction;

            if (topicFunction.EndsWith("/"))
            {
                topicFunction = topicFunction + "/";
            }
            else
            {
                topicFunction = topicFunction + "//";
            }

            return topicFunction;
        }
    }
}
