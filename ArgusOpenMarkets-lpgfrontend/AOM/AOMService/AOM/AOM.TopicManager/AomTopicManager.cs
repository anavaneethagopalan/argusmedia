﻿using System.Collections.Generic;
using AOM.App.Domain.Services;

namespace AOM.TopicManager
{
    public class AomTopicManager : IAomTopicManager
    {
        private readonly IUserService _userService;

        public  AomTopicManager(IUserService userService)
        {
            _userService = userService;
        }

        public List<string> GetAllTopicsToSubscribeForUser(long userId)
        {
            var user = _userService.GetUserWithPrivileges(userId);
            var userContentStreams = _userService.GetUserContentStreams(userId);

            List<string> topicsToSubscribeUserTo = new List<string>();

            // ADD USER TOPICS TO THE TOPICS
            topicsToSubscribeUserTo.Add(string.Format("AOM/Users/{0}", userId));

            // ADD PRODUCT TOPICS / ORDER TOPICS
            foreach (var product in user.ProductPrivileges)
            {
                // PRODUCTS
                topicsToSubscribeUserTo.AddRange(ProductTopicManager.SubscribeUserToProductTopics(product.ProductId));

                // ORDERS
                topicsToSubscribeUserTo.AddRange(OrderTopicManager.SubscribeUserToOrderTopicsForProduct(product.ProductId, user.OrganisationId));

                // Market Ticker
                topicsToSubscribeUserTo.AddRange(MarketTickerTopicManager.SubscribeUserToMarketTickerTopics(userId, product.ProductId, user.OrganisationId, product.Privileges));
            }

            if (userContentStreams != null)
            {
                foreach (var csId in userContentStreams)
                {
                    if (csId.HasValue)
                    {
                        topicsToSubscribeUserTo.AddRange(
                            NewsTopicManager.SubscribeUserToNewsTopicsForContentStream(csId.Value));
                    }
                }
            }
            // Add in the Free News for the user.
            topicsToSubscribeUserTo.AddRange(NewsTopicManager.SubscribeUserToFreeNews());

            // Add the Organisation Topic to the list of subscribed topics.
            topicsToSubscribeUserTo.AddRange(OrganisationTopicManager.SubscribeUserToOrganisationTopics(userId, user.OrganisationId));

            return topicsToSubscribeUserTo;
        }
    }
}
