﻿using System.Collections.Generic;

namespace AOM.TopicManager
{
    public interface IAomTopicManager
    {
        List<string> GetAllTopicsToSubscribeForUser(long userId);
    }
}