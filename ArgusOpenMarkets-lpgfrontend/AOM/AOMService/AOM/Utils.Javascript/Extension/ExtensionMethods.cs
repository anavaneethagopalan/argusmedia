﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using AOM.Transport.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Utils.Logging.Utils;

namespace Utils.Javascript.Extension
{
    public static class ExtensionMethods
    {
        public static string ToJsonCamelCase<T>(this T arg)
        {
            var csiTypeName = typeof(ClientSessionInfo).Name;
            var messageType = typeof (T).Name;
            var result = JsonConvert.SerializeObject(arg, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ContractResolver = new JsonWithoutCsiMessageContractResolver()
                });
            if (result.IndexOf(csiTypeName, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                Log.Warn(string.Format("CSI should not be sent to the user: Message Type: {0} | Message: {1}",messageType, result ));
            }
            return result;
        }

        public static string ToArgusDateTimeFormat(this DateTime? dateTime)
        {
            return dateTime != null ? string.Format("{0:dd-MMM-yyyy}", dateTime) : string.Empty;
        }

        public static DateTime AddWeekDays(this DateTime date,int days)
        {
            int cnt = Math.Abs(days);
            int adj = Math.Sign(days);
            
            while(cnt>0)
            {
                date=date.AddDays(adj);
                if(date.DayOfWeek==DayOfWeek.Sunday)continue;
                if(date.DayOfWeek==DayOfWeek.Saturday)continue;
                cnt +=-1;
            }
            return date;
        }

        /// <summary>
        /// Replace the pattern with the result of the passed in action (value)
        /// if an exception is thrown then a ? is used as the replacement value and an exception is logged
        /// </summary>        
        public static string SafeReplace(this string s, string pattern, Func<string> value)
        {
            try
            {
                return s.Replace(pattern, value.Invoke());
            }
            catch (Exception e)
            {
                Log.Error("SafeReplace errored for field: " + pattern, e);
                return s.Replace(pattern, "?");
            }
        }

        public static bool DoesNotEndWith(this string a, string s)
        {
            return !a.EndsWith(s);
        }

        public static bool DoesNotContain(this string a, string s)
        {
            return !a.Contains(s);
        }

        public static bool StartsWithIgnoreCase(this string a, string s)
        {
            return a.StartsWith(s, StringComparison.CurrentCultureIgnoreCase);
        }

        public static bool DoesNotStartWith(this string a, string s)
        {
            return !a.StartsWith(s);
        }

        /// <summary>
        /// Compares the string against a given pattern. Uses Regex to mimic unix-like globbing
        /// (eg "*.txt", "*Refresh*")
        /// </summary>
        /// <param name="str">The string to check against the pattern</param>
        /// <param name="pattern">
        /// The pattern to match, where "*" means any sequence of characters, and "?" means any single character.
        /// </param>
        /// <returns><c>true</c> if the string matches the given pattern; otherwise <c>false</c>.</returns>
        /// <remarks>https://stackoverflow.com/questions/188892/glob-pattern-matching-in-net</remarks>
        public static bool Like(this string str, string pattern)
        {
            return new Regex(
                "^" + Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", ".") + "$",
                RegexOptions.IgnoreCase | RegexOptions.Singleline
                ).IsMatch(str);
        }

    }

    public class JsonWithoutCsiMessageContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            var propertyType = property.PropertyType;
            if (propertyType == typeof(ClientSessionInfo))
            {
                property.ShouldSerialize = instance => false;
            }
            else
            {
                property.ShouldSerialize = instance => true;
            }
            return property;
        }
    }
}
