﻿using AOM.AdminService.Services;
using AOM.App.Domain.Entities;
using AOM.Services.EmailService;
using Moq;
using NUnit.Framework;

namespace AOM.AdminService.Tests.Services
{
    [TestFixture]
    internal class AdminAuditServiceTests
    {
        private Mock<ISmtpService> _smtpService;
        private AdminAuditService _adminAuditService;

        [SetUp]
        public void Setup()
        {
            _smtpService = new Mock<ISmtpService>();
            _adminAuditService = new AdminAuditService(_smtpService.Object);
        }

        [Test]
        public void AuditingAnEventShouldCallTheSmtpServiceOnce()
        {
            _adminAuditService.AuditAction("ACTION", "DETAIL");
            _smtpService.Verify(m => m.SendEmail(It.IsAny<Email>()), Times.Exactly(1));
        }
    }
}