﻿using AOM.AdminService.Domain;
using AOM.AdminService.Services;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Transport.Events;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AOM.AdminService.Tests.Services
{
    [TestFixture]
    class UserAdminServiceTests
    {
        private UserAdminService _userAdminService;
        private Mock<IUserService> _mockUserService;
        private Mock<IOrganisationService> _mockOrganisationService;
        private Mock<IEncryptionService> _mockEncryptionService;
        private Mock<IUserTransformationService> _mockUserTransformationService;
        private Mock<IRandomPasswordGenerator> _mockRandomPasswordGenerator;
        private Mock<IAdminAuditService> _mockAdminAuditService;

        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockUserService = new Mock<IUserService>();
            _mockOrganisationService = new Mock<IOrganisationService>();
            _mockEncryptionService = new Mock<IEncryptionService>();
            _mockUserTransformationService = new Mock<IUserTransformationService>();
            _mockRandomPasswordGenerator = new Mock<IRandomPasswordGenerator>();
            _mockAdminAuditService = new Mock<IAdminAuditService>();

            _userAdminService = new UserAdminService(_mockBus.Object, _mockUserService.Object,
                _mockOrganisationService.Object, _mockEncryptionService.Object,_mockUserTransformationService.Object,
                _mockRandomPasswordGenerator.Object, _mockAdminAuditService.Object);
        }

        [Test]
        public void GetUsersShouldReturnAnEmptyListOfUsers()
        {
            _mockUserTransformationService.Setup(
                m =>
                    m.TransformUsers(It.IsAny<List<User>>(), It.IsAny<List<Organisation>>(), It.IsAny<List<LoggedOnUserInfo>>())).Returns(new List<AdminUser>());

            var users = _userAdminService.GetUsers();
            Assert.That(users, Is.Not.Null);
            Assert.That(users.Count, Is.EqualTo(0));
        }

        [Test]
        public void GetUsersShouldReturnAListOfUsers()
        {
            var listAdminUsers = MakeAdminUsers(2);

            _mockUserTransformationService.Setup(
                m =>
                    m.TransformUsers(It.IsAny<List<User>>(), It.IsAny<List<Organisation>>(), It.IsAny<List<LoggedOnUserInfo>>())).Returns(listAdminUsers);

            var users = _userAdminService.GetUsers();
            Assert.That(users, Is.Not.Null);
            Assert.That(users.Count, Is.EqualTo(2));
        }

        private List<AdminUser> MakeAdminUsers(int numberToMake)
        {
            var adminUsers = new List<AdminUser>();
            for (int counter = 0; counter < numberToMake; counter++)
            {
                adminUsers.Add(new AdminUser
                {
                    Id = counter,
                    IsActive = true,
                    Username = string.Format("User-{0}", counter)
                });
            }

            return adminUsers;
        }
    }
}
