﻿using AOM.AdminService.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Crm;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using NUnit.Framework;
using System.Linq;

namespace AomAdminService.Tests.Services
{
    [TestFixture]
    public class DealsAdminServiceTests
    {
        private IDealsAdminService _dealsAdminService;

        private MockDbContextFactory _mockDbContextFactory;

        [SetUp]
        public void Setup()
        {
            var testUserId = 1;

            OrderDto order1;
            OrderDto order2;
            DealDto deal;

            var mockAomModel = new MockAomModel();
            AomDataBuilder.WithModel(mockAomModel)
                .AddDeliveryLocation("Farringdon")
                .AddProduct("Argus Naphtha CIF NWE - Outright", 234)
                .AddTenorCode("Tenor1")
                .AddProductTenor("Product1_Tenor1", 234)
                .AddOrder(testUserId, "Product1_Tenor1", "A", out order1)
                .AddOrder(testUserId, "Product1_Tenor1", "A", out order2)
                .AddDeal(testUserId, order1.Id, order2.Id, out deal, "P")
                .DBContext.SaveChangesAndLog(-1);

            _mockDbContextFactory = new MockDbContextFactory(mockAomModel, null);
            _dealsAdminService = new DealsAdminService(_mockDbContextFactory);
        }

        [Test]
        public void ShouldReturnAListOfDeals()
        {
            var deals = _dealsAdminService.GetDeals().ToList();

            Assert.That(deals, Is.Not.Null);
            Assert.That(deals.Count, Is.EqualTo(1));
        }


        [Test]
        public void ShouldCallTheDBAndReturnDeals()
        {
            var dealsService = new DealsAdminService(new DbContextFactory());

            var deals = dealsService.GetDeals();
            Assert.That(deals, Is.Not.Null);
        }
    }
}