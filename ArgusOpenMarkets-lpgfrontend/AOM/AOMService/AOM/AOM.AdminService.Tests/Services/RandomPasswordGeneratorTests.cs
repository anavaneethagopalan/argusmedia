﻿using AOM.AdminService.Services;
using NUnit.Framework;

namespace AOM.AdminService.Tests.Services
{
    [TestFixture]
    class RandomPasswordGeneratorTests
    {
        private RandomPasswordGenerator _randomPasswordGenerator;

        [SetUp]
        public void Setup()
        {
            _randomPasswordGenerator = new RandomPasswordGenerator();
        }

        [Test]
        public void ShouldReturnARandomPasswordThatIsNotNull()
        {
            var randomPassword = _randomPasswordGenerator.GenerateRandomPassword();
            Assert.That(randomPassword, Is.Not.Null);
        }

        [Test]
        public void SubsequentCallsToGenerateRandomPasswordShouldResultInDifferentPasswords()
        {
            var randomPasswordOne = _randomPasswordGenerator.GenerateRandomPassword();
            var randomPasswordTwo = _randomPasswordGenerator.GenerateRandomPassword();

            Assert.That(randomPasswordOne, Is.Not.EqualTo(randomPasswordTwo));
        }
    }
}
