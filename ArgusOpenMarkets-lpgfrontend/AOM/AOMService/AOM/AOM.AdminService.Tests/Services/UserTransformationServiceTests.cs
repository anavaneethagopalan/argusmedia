﻿using AOM.AdminService.Services;
using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace AOM.AdminService.Tests.Services
{
    [TestFixture]
    internal class UserTransformationServiceTests
    {
        private UserTransformationService _userTransformationService;

        [SetUp]
        public void Setup()
        {
            _userTransformationService = new UserTransformationService();
        }

        [Test]
        public void ShouldReturnAnEmptyListOfAdminUsersIfNullListsPassedIn()
        {
            List<User> users = null;
            List<Organisation> organisations = null;
            List<LoggedOnUserInfo> loggedOnUsers = null;

            var adminUsers = _userTransformationService.TransformUsers(users, organisations, loggedOnUsers);

            Assert.That(adminUsers, Is.Not.Null);
            Assert.That(adminUsers.Count, Is.EqualTo(0));
        }

        [Test]
        public void ShouldReturnAnEmptyListOfAdminUsersIfEmptyListsPassedIn()
        {
            var users = new List<User>();
            var organisations = new List<Organisation>();
            var loggedOnUsers = new List<LoggedOnUserInfo>();

            var adminUsers = _userTransformationService.TransformUsers(users, organisations, loggedOnUsers);

            Assert.That(adminUsers, Is.Not.Null);
            Assert.That(adminUsers.Count, Is.EqualTo(0));
        }

        [Test]
        public void ShouldReturnASingleUserWithNoOrganisationAndNoLastLoginDetails()
        {
            var users = MakeUsers(1);
            var organisations = new List<Organisation>();
            var loggedOnUsers = new List<LoggedOnUserInfo>();

            var adminUsers = _userTransformationService.TransformUsers(users, organisations, loggedOnUsers);

            Assert.That(adminUsers, Is.Not.Null);
            Assert.That(adminUsers.Count, Is.EqualTo(1));
            Assert.That(adminUsers[0].Organisation, Is.EqualTo(string.Empty));
            Assert.That(adminUsers[0].LastSeen, Is.EqualTo(null));
        }

        [Test]
        public void ShouldReturnASingleUserWithAnOrganisationAndNoLastLoginDetails()
        {
            var users = MakeUsers(1);
            var organisations = MakeOrganisations(1);
            var loggedOnUsers = new List<LoggedOnUserInfo>();

            var adminUsers = _userTransformationService.TransformUsers(users, organisations, loggedOnUsers);

            Assert.That(adminUsers, Is.Not.Null);
            Assert.That(adminUsers.Count, Is.EqualTo(1));
            Assert.That(adminUsers[0].Organisation, Is.EqualTo(organisations[0].Name));
            Assert.That(adminUsers[0].LastSeen, Is.EqualTo(null));
        }

        [Test]
        public void ShouldReturnASingleUserWithAnOrganisationAndLastLoginDetails()
        {
            var users = MakeUsers(1);
            var organisations = MakeOrganisations(1);
            var loggedOnUsers = MakeLoggedOnUsers(1);

            var adminUsers = _userTransformationService.TransformUsers(users, organisations, loggedOnUsers);

            Assert.That(adminUsers, Is.Not.Null);
            Assert.That(adminUsers.Count, Is.EqualTo(1));
            Assert.That(adminUsers[0].Organisation, Is.EqualTo(organisations[0].Name));
            Assert.That(adminUsers[0].LastSeen, Is.Not.Null);
        }

        [Test]
        public void ShouldReturnTwenyUsersWithOrganisationAndLastLoginDetails()
        {
            var numberUsersToMake = 20;
            var users = MakeUsers(numberUsersToMake);
            var organisations = MakeOrganisations(numberUsersToMake);
            var loggedOnUsers = MakeLoggedOnUsers(numberUsersToMake);

            var adminUsers = _userTransformationService.TransformUsers(users, organisations, loggedOnUsers);

            for (int counter = 0; counter < numberUsersToMake; counter++)
            {
                Assert.That(adminUsers, Is.Not.Null);
                Assert.That(adminUsers.Count, Is.EqualTo(numberUsersToMake));
                Assert.That(adminUsers[counter].Organisation, Is.EqualTo(organisations[counter].Name));
                Assert.That(adminUsers[counter].LastSeen, Is.Not.Null);
            }
        }

        private List<LoggedOnUserInfo> MakeLoggedOnUsers(int numberLoggedOnUsers)
        {
            var loggedOnUsers = new List<LoggedOnUserInfo>();

            for (int counter = 0; counter < numberLoggedOnUsers; counter++)
            {
                var loggedOnUser = new LoggedOnUserInfo
                {
                    LastSeen = DateTime.Now,
                    UserId = counter
                };

                loggedOnUsers.Add(loggedOnUser);
            }

            return loggedOnUsers;
        }

        private List<Organisation> MakeOrganisations(int numberOrganisations)
        {
            List<Organisation> organisations = new List<Organisation>();

            for (int counter = 0; counter < numberOrganisations; counter++)
            {
                var organisation = new Organisation
                {
                    Id = counter,
                    Name = string.Format("Organisation", counter),
                    Description = string.Format("Organisation Description ", counter)
                };

                organisations.Add(organisation);
            }

            return organisations;
        }

        private List<User> MakeUsers(int numberUsers)
        {
            List<User> users = new List<User>();
            for (int counter = 0; counter < numberUsers; counter++)
            {
                var user = new User
                {
                    Username = "USER" + counter,
                    Email = string.Format("email{0}@acme.com", counter),
                    IsDeleted = false,
                    OrganisationId = counter,
                    Title = "Mr", 
                    Id = counter
                };

                users.Add(user);
            }

            return users;
        }
    }
}
