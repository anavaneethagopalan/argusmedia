﻿using System.Collections.Generic;
using NUnit.Framework;

namespace AOM.RollProductDeliveryPeriods.Tests
{
    [TestFixture]
    public class CommandLineOptionsTests
    {
        [Test]
        public void ShouldReturnFalseForInvalidCommandLineArguments()
        {
            List<string> args = new List<string>();
            args.Add("");

            var options = new CommandLineOptions();

            var result = CommandLine.Parser.Default.ParseArguments(args.ToArray(), options);
            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldReturnTrueForValidCommandLineArguments()
        {
            List<string> args = MakeValidArgs();
            var options = new CommandLineOptions();

            var result = CommandLine.Parser.Default.ParseArguments(args.ToArray(), options);
            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldReturnFileNameFromCsvData()
        {
            List<string> args = MakeValidArgs();

            var options = new CommandLineOptions();

            CommandLine.Parser.Default.ParseArguments(args.ToArray(), options);

            Assert.That(options.FileName, Is.EqualTo("data.csv"));
        }

        [Test]
        public void ShouldReturnProductTenorIdsFromCsvData()
        {
            List<string> args = MakeValidArgs();

            var options = new CommandLineOptions();

            CommandLine.Parser.Default.ParseArguments(args.ToArray(), options);
            Assert.That(options.ProductTenorIds, Is.EqualTo("1,2"));
        }

        [Test]
        public void ShouldReturnEmailAddressFromCsvData()
        {
            List<string> args = MakeValidArgs();

            var options = new CommandLineOptions();

            CommandLine.Parser.Default.ParseArguments(args.ToArray(), options);
            Assert.That(options.ConfirmationEmail, Is.EqualTo("nathanbellamore@hotmail.com"));
        }

        private List<string> MakeValidArgs()
        {
            var args = new List<string>();
            args.Add("-p1,2");
            args.Add("-t0");
            args.Add("-cnathanbellamore@hotmail.com");
            args.Add("-fdata.csv");

            return args;
        }
    }
}
