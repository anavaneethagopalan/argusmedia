﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Services.ProductService;
using Moq;
using NUnit.Framework;

namespace AOM.RollProductDeliveryPeriods.Tests
{
    [TestFixture]
    public class CommandLineOptionsValidatorTests
    {
        private CommandLineOptionsValidator _commandLineOptionsValidator;
        private Mock<IFileChecker> _mockFileChecker;
        private Mock<IProductService> _mockProductService;

        [SetUp]
        public void Setup()
        {
            _mockFileChecker = new Mock<IFileChecker>();
            _mockFileChecker.Setup(m => m.FileExists(It.IsAny<string>())).Returns(true);
            
            _mockProductService = new Mock<IProductService>();

            List<ProductTenor> listProductTenors = new List<ProductTenor>();
            listProductTenors.Add(new ProductTenor{ Id = 1, Name = "Test Tenor"});
            _mockProductService.Setup(m => m.GetAllProductTenors()).Returns(listProductTenors);

            _commandLineOptionsValidator = new CommandLineOptionsValidator(_mockFileChecker.Object,
                _mockProductService.Object);
        }

        [Test]
        public void ShouldReturnEmptyStringWhenCommandLineArgumentsAreValid()
        {
            var options = MakeValidOptions();

            var message = _commandLineOptionsValidator.ValidateCommandLineOptions(options);
            Assert.That(message, Is.EqualTo(string.Empty));
        }

        [Test]
        public void ShouldReturnErrorThatTheTimeZoneOffsetIsGreaterThanTheMaxRange()
        {
            var options = MakeValidOptions();
            options.TimeZoneOffset = "30";

            var message = _commandLineOptionsValidator.ValidateCommandLineOptions(options);
            Assert.That(message, Is.EqualTo("Timezone offset cannot be greater than 12:45 - Chatham Island"));
        }

        [Test]
        public void ShouldReturnErrorThatTheTimeZoneOffsetIsLessThanTheMinRange()
        {
            var options = MakeValidOptions();
            options.TimeZoneOffset = "-13";

            var message = _commandLineOptionsValidator.ValidateCommandLineOptions(options);
            Assert.That(message, Is.EqualTo("Timezone offset cannot be less than 12 - Military time zone"));
        }

        [Test]
        public void ShouldReturnErrorThatTheFileDoesNotExist()
        {
            var options = MakeValidOptions();
            options.TimeZoneOffset = "30";
            _mockFileChecker.Setup(m => m.FileExists(It.IsAny<string>())).Returns(false);

            var message = _commandLineOptionsValidator.ValidateCommandLineOptions(options);
            Assert.That(message, Is.EqualTo("File: test.csv does not exist."));
        }

        [Test]
        public void ShouldReturnErrorThatTheConfirmationEmailAddressIsNotValid()
        {
            var options = MakeValidOptions();
            options.ConfirmationEmail = "nathan";

            var message = _commandLineOptionsValidator.ValidateCommandLineOptions(options);
            Assert.That(message, Is.EqualTo("Confirmation email address supplied is not a valid email address"));
        }

        [Test]
        public void ShouldReturnErrorThatTheProductTenorDoesNotExist()
        {
            var options = MakeValidOptions();
            options.ProductTenorIds = "2";

            var message = _commandLineOptionsValidator.ValidateCommandLineOptions(options);
            Assert.That(message, Is.EqualTo("Could not find the tenor: 2"));
        }

        [Test]
        public void ShouldReturnErrorThatTheProductTenorDoesNotExistWhenProcessingMultipleTenors()
        {
            var options = MakeValidOptions();
            options.ProductTenorIds = "1,2";

            var message = _commandLineOptionsValidator.ValidateCommandLineOptions(options);
            Assert.That(message, Is.EqualTo("Could not find the tenor: 2"));
        }

        private CommandLineOptions MakeValidOptions()
        {
            return new CommandLineOptions
            {
                ConfirmationEmail = "nathan.bellamore@argusmedia.com",
                FileName = "test.csv",
                ProductTenorIds = "1",
                TimeZoneOffset = "5"
            };
        }
    }
}
