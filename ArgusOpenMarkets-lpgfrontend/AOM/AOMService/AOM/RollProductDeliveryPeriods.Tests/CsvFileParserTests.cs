﻿using System;
using System.IO;
using System.Text;
using CsvHelper;
using NUnit.Framework;

namespace AOM.RollProductDeliveryPeriods.Tests
{
    [TestFixture]
    public class CsvFileParserTests
    {
        private CsvFileParser _csvFileParser;

        [SetUp]
        public void Setup()
        {
            _csvFileParser = new CsvFileParser();
        }

        [Test]
        public void ShouldReturnNextStartDateIfCsvContainsDataForTheStartDate()
        {
            var expectedDate = new DateTime(2016, 7, 1);
            _csvFileParser.LoadFile(MakeStreamData());

            var rollDate = new DateTime(2016, 6, 16);
            var startDate = _csvFileParser.GetStartDate(rollDate, 1);

            Assert.That(startDate, Is.EqualTo(expectedDate));
        }

        [Test]
        public void ShouldThrowBusinessRuleExceptionIfRollDateIsGreaterThanTheMaxStartDateOfCsvData()
        {
            _csvFileParser.LoadFile(MakeStreamData());
            var message = string.Empty;

            var rollDate = new DateTime(2017, 5, 16);
            try
            {
                var startDate = _csvFileParser.GetStartDate(rollDate, 1);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            Assert.That(message, Is.EqualTo("Roll Date is greater than supplied date ranges"));
        }

        [Test]
        public void ShouldThrowBusinessRuleExceptionIfRollDateIsLessThanTheMinStartDateOfCsvData()
        {
            _csvFileParser.LoadFile(MakeStreamData());
            var message = string.Empty;

            var rollDate = new DateTime(2015, 5, 16);
            try
            {
                var startDate = _csvFileParser.GetStartDate(rollDate, 1);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            Assert.That(message, Is.EqualTo("Roll Date is earlier than supplied date ranges"));
        }

        [Test]
        public void GetLastestRollDateInCsvFileForTenorShouldReturnTheLastRollDate()
        {
            _csvFileParser.LoadFile(MakeStreamData());
            var latestRollDate = _csvFileParser.GetLatestRollDateInCsvDataFile(1);

            Assert.That(latestRollDate, Is.EqualTo(new DateTime(2016, 7, 16)));
        }

        [Test]
        public void GetEarliestRollDateInCsvFileForTenorShouldReturnTheEarliestRollDate()
        {
            _csvFileParser.LoadFile(MakeStreamData());
            var latestRollDate = _csvFileParser.GetEarliestRollDateInCsvDataFile(1);

            Assert.That(latestRollDate, Is.EqualTo(new DateTime(2016,6, 1)));
        }

        private static StreamReader CreateMockStream()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("TenorId,Date,Start,End");
            stringBuilder.AppendLine("1,16/05/2016,01/06/2016,15/06/2016");
            stringBuilder.AppendLine("1,01/06/2016,16/06/2016,30/06/2016");
            stringBuilder.AppendLine("1,16/06/2016,01/07/2016,15/07/2016");
            stringBuilder.AppendLine("1,01/07/2016,16/07/2016,31/07/2016");
            stringBuilder.AppendLine("2,16/05/2016,16/06/2016,30/06/2016");
            stringBuilder.AppendLine("2,01/06/2016,01/07/2016,15/07/2016");
            stringBuilder.AppendLine("2,16/06/2016,16/07/2016,31/07/2016");
            stringBuilder.AppendLine("2,01/07/2016,16/08/2016,31/08/2016");
            stringBuilder.AppendLine("3,16/05/2016,01/07/2016,15/07/2016");
            stringBuilder.AppendLine("3,01/06/2016,16/07/2016,31/07/2016");
            stringBuilder.AppendLine("3,16/06/2016,16/08/2016,31/08/2016");
            stringBuilder.AppendLine("3,01/07/2016,01/09/2016,15/09/2016");

//TenorId,Date,Start,End
//1,16/05/2016,01/06/2016,15/06/2016
//1,01/06/2016,16/06/2016,30/06/2016
//1,16/06/2016,01/07/2016,15/07/2016
//1,01/07/2016,16/07/2016,31/07/2016
//2,16/05/2016,16/06/2016,30/06/2016
//2,01/06/2016,01/07/2016,15/07/2016
//2,16/06/2016,16/07/2016,31/07/2016
//2,01/07/2016,16/08/2016,31/08/2016
//3,16/05/2016,01/07/2016,15/07/2016
//3,01/06/2016,16/07/2016,31/07/2016
//3,16/06/2016,16/08/2016,31/08/2016
//3,01/07/2016,01/09/2016,15/09/2016


            var streamReader = new StreamReader(new MemoryStream(Encoding.ASCII.GetBytes(stringBuilder.ToString())));
            return streamReader;
        }

        private CsvReader MakeStreamData()
        {
            var streamData = CreateMockStream();
            var csvData = new CsvReader(streamData);

            return csvData;
        }
    }
}