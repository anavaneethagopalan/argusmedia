﻿using System;
using AOM.RollProductDeliveryPeriods.Interfaces;
using AOM.Services.ProductService;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.RollProductDeliveryPeriods.Tests
{
    [TestFixture]
    class TenorRollTests
    {
        private TenorRoller _tenorRoller;
        private Mock<IProductService> _mockProductService;
        private Mock<ICSVFileParser> _mockCsvFileParser;
        private Mock<IBus> _mockBus;
        private Mock<IClock> _mockClock;

        [SetUp]
        public void Setup()
        {
            _mockProductService = new Mock<IProductService>();
            _mockCsvFileParser = new Mock<ICSVFileParser>();
            _mockBus = new Mock<IBus>();
            _mockClock = new Mock<IClock>();

            _tenorRoller = new TenorRoller(_mockProductService.Object, _mockCsvFileParser.Object, _mockBus.Object, _mockClock.Object);
        }

        [Test]
        public void ShouldReturnStartDateOffsetWithNoUtcOffset()
        {
            var rollDateStart = new DateTime(2010, 10, 10);
            _mockClock.Setup(m => m.UtcNow()).Returns(new DateTime(2010, 10, 1));

            string startDateOffset = _tenorRoller.GetStartDateOffset(rollDateStart, "0");

            Assert.That(startDateOffset, Is.EqualTo("+9d"));
        }

        [Test]
        public void ShouldReturnStartDateOffsetTakingIntoAccountTimeZoneOffsetThatIncreaseTheDay()
        {
            var rollDateStart = new DateTime(2010, 10, 10);
            _mockClock.Setup(m => m.UtcNow()).Returns(new DateTime(2010, 10, 1, 23, 0, 0));

            string startDateOffset = _tenorRoller.GetStartDateOffset(rollDateStart, "4");

            Assert.That(startDateOffset, Is.EqualTo("+8d"));
        }

        [Test]
        public void ShouldReturnStartDateOffsetTakingIntoAccountTimeZoneOffsetThatDecreasesTheDay()
        {
            var rollDateStart = new DateTime(2010, 10, 10);
            _mockClock.Setup(m => m.UtcNow()).Returns(new DateTime(2010, 10, 1, 02, 0, 0));

            string startDateOffset = _tenorRoller.GetStartDateOffset(rollDateStart, "-3");

            Assert.That(startDateOffset, Is.EqualTo("+10d"));
        }

        [Test]
        public void ShouldReturnEmptyStartDateOffsetIfTodaysDateIsGreaterThanRollDateStart()
        {
            var rollDateStart = new DateTime(2010, 10, 10);
            _mockClock.Setup(m => m.UtcNow()).Returns(new DateTime(2011, 10, 5, 07, 0, 0));

            string startDateOffset = _tenorRoller.GetStartDateOffset(rollDateStart, "0");

            Assert.That(startDateOffset, Is.EqualTo(string.Empty));
        }

        [Test]
        public void ShouldReturnEndDateOffsetWithNoUtcOffset()
        {
            var rollDateEnd = new DateTime(2010, 10, 10);
            _mockClock.Setup(m => m.UtcNow()).Returns(new DateTime(2010, 10, 5));

            string endDateOffset = _tenorRoller.GetEndDateOffset(rollDateEnd, "0");

            Assert.That(endDateOffset, Is.EqualTo("+5d"));
        }

        [Test]
        public void ShouldReturnEndDateOffsetWithUtcOffsetPlus3Hours()
        {
            var rollDateEnd = new DateTime(2010, 10, 10);
            _mockClock.Setup(m => m.UtcNow()).Returns(new DateTime(2010, 10, 5, 23, 0, 0));

            string endDateOffset = _tenorRoller.GetEndDateOffset(rollDateEnd, "3");

            Assert.That(endDateOffset, Is.EqualTo("+4d"));
        }

        [Test]
        public void ShouldReturnEndDateOffsetWithUtcOffsetMinus8Hours()
        {
            var rollDateEnd = new DateTime(2010, 10, 10);
            _mockClock.Setup(m => m.UtcNow()).Returns(new DateTime(2010, 10, 5, 07, 0, 0));

            string endDateOffset = _tenorRoller.GetEndDateOffset(rollDateEnd, "-8");

            Assert.That(endDateOffset, Is.EqualTo("+6d"));
        }

        [Test]
        public void ShouldReturnEmptyEndDateOffsetIfTodaysDateIsGreaterThanRollDateEnd()
        {
            var rollDateEnd = new DateTime(2010, 10, 10);
            _mockClock.Setup(m => m.UtcNow()).Returns(new DateTime(2011, 10, 5, 07, 0, 0));

            string endDateOffset = _tenorRoller.GetEndDateOffset(rollDateEnd, "0");

            Assert.That(endDateOffset, Is.EqualTo(string.Empty));
        }

        [Test]
        [TestCase("01/01/2017", "10/01/2017", 10)]
        [TestCase("16/07/17","31/07/17", 16)]
        [TestCase("16/05/17", "30/05/17", 15)]
        [TestCase("01/07/17", "16/07/17", 16)]
        [TestCase("01/07/17", "15/07/17", 15)]
        public void ShouldCaculateTheMinimumDeliveryRange(string sd, string ed, int minimumDeliveryRange)
        {
            DateTime startDate;
            DateTime.TryParse(sd, out startDate);

            DateTime endDate;
            DateTime.TryParse(ed, out endDate);

            var minDeliveryRange = _tenorRoller.CalculateMinimumDeliveryRange(startDate, endDate);
            Assert.That(minDeliveryRange, Is.EqualTo(minimumDeliveryRange));
        }
    }
}
