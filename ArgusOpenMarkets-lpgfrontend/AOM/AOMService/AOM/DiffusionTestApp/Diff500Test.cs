using PushTechnology.ClientInterface.Client.Content;
using PushTechnology.ClientInterface.Client.Factories;
using PushTechnology.ClientInterface.Client.Features;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using PushTechnology.ClientInterface.Client.Session;
using PushTechnology.ClientInterface.Client.Topics;
using PushTechnology.ClientInterface.Client.Types;
using System;
using System.Diagnostics;
using System.Threading;


namespace TestHarness
{
    class Diff500
    {
        static ITopicSourceUpdater Updater;
        static ITopicControl TopicControl;
        static ITopicControlAddCallback TopicControlAddCallback;
        static ITopicUpdateOptions TopicUpdateOptions;

        static IMessaging ClientMessaging;
        static ISendCallback SendCallback;
        public static IMessagingControl ControlClientMessagingControl;

        static System.Timers.Timer MessageTimer;

        static readonly string SERVER_URL = "dpt://localhost:8081";
        static readonly string ROOT_TOPIC = "AOM";
        // set to true to run both clients and control client here.
        static readonly bool RUN_CLIENT = false;

        public static void Main(string[] args)
        {
            TopicControlAddCallback = new TopicControlAddCallback();
            SendCallback = new SendCallback();
            ISession theClientSession = Diffusion.Sessions.Open(SERVER_URL);

            ISession theControlSession = Diffusion.Sessions.Open(SERVER_URL);

            MessageTimer = new System.Timers.Timer(1000);
            MessageTimer.Enabled = false;
            MessageTimer.Elapsed += MessageTimer_Elapsed;

            // ========== start :: bypass 9838
            Thread.Sleep(1000);
            ClientConnected(theClientSession);
            ControlClientConnected(theControlSession);
            // ========== end :: bypass 9838

            Out.L("Press any key to exit...");
            Console.ReadKey();

            MessageTimer.Enabled = false;
            theClientSession.Close();
            theControlSession.Close();

            Out.Exit(0);
        }

        static void ClientConnected(ISession session)
        {
            ClientMessaging = session.GetMessagingFeature();
            ClientMessaging.AddListener("*" + ROOT_TOPIC + "/", new MessagingListener());
        }

        static void ControlClientConnected(ISession session)
        {
            ControlClientMessagingControl = session.GetMessagingControlFeature();
            ControlClientMessagingControl.AddMessageHandler(ROOT_TOPIC, new MessageHandler());

            TopicControl = session.GetTopicControlFeature();
            ITopicUpdateControl topicUpdateControl = session.GetTopicUpdateControlFeature();
            TopicUpdateOptions = topicUpdateControl.CreateUpdateBuilder().SetUpdateType(TopicUpdateType.SNAPSHOT).Build();

            topicUpdateControl.AddTopicSource(ROOT_TOPIC, new TopicSource());
        }

        static void MessageTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Out.L("Sending Client messages...");
            for (int i = 0; i < 10; i++)
            {
                ClientMessaging.Send(string.Format("{0}/{1}", ROOT_TOPIC, i), Diffusion.Content.NewContent(string.Format("{0} - {1}", i, DateTime.Now.Millisecond)), SendCallback);
            }
        }

        public static void IsActive(ITopicSourceUpdater updater)
        {
            Updater = updater;
            CreateTopic(ROOT_TOPIC, TopicType.STATELESS);
            for (int i = 0; i < 10; i++)
            {
                CreateTopic(string.Format("{0}/{1}", ROOT_TOPIC, i), TopicType.SINGLE_VALUE);
            }

            Thread.Sleep(1000); // just let everything register ...

            MessageTimer.Enabled = RUN_CLIENT;
        }

        public static void CreateTopic(string topicPath, TopicType topicType)
        {
            Out.D("Creating topic '{0}' with state '{1}'...", topicPath, topicType);
            TopicControl.AddTopic(topicPath, topicType, TopicControlAddCallback);
        }

    }

    class MessageHandler : IMessageHandler
    {
        public void OnMessage(SessionId sessionId, string topicPath, IContent content, IReceiveContext context)
        {
            Out.L("MessageHandler ||| Message recieved on '{0}' :: '{1}'", topicPath, content.AsString());
        }

        public void OnActive(string topicPath, IRegisteredHandler registeredHandler)
        {
            Out.D("MessageHandler#OnActive");
        }

        public void OnClose(string topicPath)
        {
            Out.D("MessageHandler#OnClose");
        }
    }

    class MessagingListener : IMessagingListener
    {
        public bool OnMessageReceived(string topicPath, IContent content, IReceiveContext context)
        {
            Out.L("MessagingListener ||| Message recieved on '{0}' :: '{1}'", topicPath, content.AsString());
            return true;
        }
    }

    class SendCallback : ISendCallback
    {
        public void OnComplete()
        {
            Out.D("SendCallback#OnComplete");
        }

        public void OnDiscard()
        {
            Out.D("SendCallback#OnDiscard");
        }
    }

    class TopicSource : ITopicSource
    {

        public void OnActive(string topicPath, IRegisteredHandler registeredHandler, ITopicSourceUpdater updater)
        {
            Out.T("TopicSourceImpl::OnActive for {0}", topicPath);
        }

        public void OnClosed(string topicPath)
        {
            Out.T("TopicSourceImpl::OnClosed for {0}", topicPath);
        }

        public void OnStandby(string topicPath)
        {
            Out.T("TopicSourceImpl::OnStandby for {0}", topicPath);
        }
    }

    class TopicControlAddCallback : ITopicControlAddCallback
    {

        public void OnTopicAddFailed(string topicPath, TopicAddFailReason reason)
        {
            Out.T("TopicControlAddCallback#OnTopicAddFailed path: '{0}', reason: '{1}'", topicPath, reason);
            if (!reason.Equals(TopicAddFailReason.EXISTS))
            {
                Out.E("Adding topic: '{0}' failed for reason: '{1}'", topicPath, reason);
            }
        }

        public void OnTopicAdded(string topicPath)
        {
            Out.L("Topic '{0}' added.", topicPath);
        }

        public void OnDiscard()
        {
            Out.E("TopicControlAddCallbackImpl#OnDiscard() :: Notification that a call context was closed prematurely, typically due to a timeout or the session being closed.");
        }
    }

    class Out
    {
        private static readonly bool IsDebug = false;
        private static readonly bool IsTrace = false;

        public static void Usage(int exitCode, string err, params object[] args)
        {
            L(err, args);
            O("");
            O("");
            Exit(exitCode);
        }

        public static void Exit(int exitCode)
        {
            D("Exiting with status: '{0}'", exitCode);
            O("Press any key to exit...");
            Console.ReadKey();
            Environment.Exit(exitCode);
        }

        public static void E(string str, params object[] args)
        {
            Console.Error.WriteLine(TimeStr() + "[ERROR] " + str, args);
        }

        static void O(string str, params object[] args)
        {
            if (IsDebug || IsTrace)
            {
                Debug.WriteLine(str, args);
            }
            Console.Out.WriteLine(str, args);
        }

        public static void L(string str, params object[] args)
        {
            O(TimeStr() + str, args);
        }

        public static void D(string str, params object[] args)
        {
            if (IsDebug || IsTrace)
            {
                L("[DEBUG] " + str, args);
            }
        }

        public static void T(string str, params object[] args)
        {
            if (IsTrace)
            {
                L("[TRACE] " + str, args);
            }
        }

        static string TimeStr()
        {
            return string.Format("{0:yyyy-MM-dd HH:mm:ss.ffff} ::: ", DateTime.Now);
        }
    }
}
