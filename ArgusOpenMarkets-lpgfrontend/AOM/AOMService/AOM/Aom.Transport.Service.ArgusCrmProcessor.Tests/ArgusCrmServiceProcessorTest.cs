﻿using System.Collections.Generic;
using AOM.Transport.Service.ArgusCrmProcessor;
using AOM.Transport.Service.ArgusCrmProcessor.TimerService;
using AOM.Transport.Service.Processor.Common;
using Moq;
using NUnit.Framework;
using Topshelf;
using IBus = Argus.Transport.Infrastructure.IBus;

namespace Aom.Transport.Service.ArgusCrmProcessor.Tests
{
    public class ArgusCrmServiceProcessorTest
    {
        private Mock<IServiceManagement> _mockServiceManagement;
        private IEnumerable<IConsumer> _mockConsumers;
        private Mock<IBus> _mockAomBus;
        private Mock<ITimerService> _mockArgusCrmTimerService;
        private Mock<HostControl> _mockHostControl;

        [SetUp]
        public void SetUp()
        {
            _mockServiceManagement = new Mock<IServiceManagement>();
            _mockConsumers = new List<IConsumer>() { new Mock<IConsumer>().Object };
            _mockAomBus = new Mock<IBus>();
            _mockHostControl = new Mock<HostControl>();
            _mockArgusCrmTimerService = new Mock<ITimerService>();
            _mockArgusCrmTimerService.Setup(a=> a.StartTimer(It.IsAny<long>())).Verifiable();
            _mockArgusCrmTimerService.Setup(a => a.StopTimer()).Verifiable();
        }

        [Test]
        public void StartTest()
        {
            var service = new ArgusCrmServiceProcessor(_mockServiceManagement.Object, _mockConsumers, _mockAomBus.Object,
            _mockArgusCrmTimerService.Object);
            service.Start(_mockHostControl.Object);
            _mockArgusCrmTimerService.Verify(a => a.StartTimer(It.IsAny<long>()), Times.Exactly(1));
        }

        [Test]
        public void StopTest()
        {
            var service = new ArgusCrmServiceProcessor(_mockServiceManagement.Object, _mockConsumers, _mockAomBus.Object,
            _mockArgusCrmTimerService.Object);
            service.Start(_mockHostControl.Object);
            _mockArgusCrmTimerService.Verify(a => a.StartTimer(It.IsAny<long>()), Times.Exactly(1));
            service.Stop(_mockHostControl.Object);
            _mockArgusCrmTimerService.Verify(a => a.StopTimer(), Times.Exactly(1));
        }
    }
}
