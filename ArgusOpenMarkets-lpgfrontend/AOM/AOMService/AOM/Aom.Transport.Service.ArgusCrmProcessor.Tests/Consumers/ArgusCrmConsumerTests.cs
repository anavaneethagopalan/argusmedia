﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Services.CrmService;
using AOM.Transport.Service.ArgusCrmProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using AOM.Transport.Events.ArgusCrm;

namespace Aom.Transport.Service.ArgusCrmProcessor.Tests.Consumers
{
    [TestFixture]
    public class ArgusCrmConsumerTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IArgusCrmConsumerService> _mockConsumerService;
        private Mock<IUserModuleService> _mockModuleService;

        private IEnumerable<AomUserModuleViewEntity> modulesStub = new List<AomUserModuleViewEntity>()
        {
            new AomUserModuleViewEntity()
            {

                UserName = "testUser",
                ModuleId = 123,
                Description = "descript",
                Stream = "stream",
                StreamId = 001,
                UserOracleCrmId = 9,
            }
        };

        [SetUp]
        public void SetUp()
        {
            _mockBus = new Mock<IBus>();
            _mockConsumerService = new Mock<IArgusCrmConsumerService>();
            _mockModuleService = new Mock<IUserModuleService>();
            _mockBus.Setup(
                    a => a.Subscribe<ArgusCrmRefreshRequest>(It.IsAny<String>(), It.IsAny<Action<ArgusCrmRefreshRequest>>()))
                .Verifiable();
            _mockConsumerService.Setup(a => a.GetLatestData(It.IsAny<String>())).Returns(modulesStub);

        }

        [Test]
        public void OnStartSubscribesProcessorToBus()
        {
            var argusCrmConsumer = CreateConsumer();

            argusCrmConsumer.Start();

            _mockBus.Verify(
                a => a.Subscribe<ArgusCrmRefreshRequest>(It.IsAny<String>(), It.IsAny<Action<ArgusCrmRefreshRequest>>()),
                Times.Once);
        }

        [Test]
        public void ArgusCrmConsumer_ConsumeRefreshArgusCrmRequest()
        {
            var argusCrmConsumer = CreateConsumer();
            argusCrmConsumer.Start();
            var request = new ArgusCrmRefreshRequest();
            argusCrmConsumer.ConsumeRefreshArgusCrmRequest(request);

            _mockConsumerService.Verify(a => a.GetLatestData(It.IsAny<string>()), Times.Exactly(1));
            _mockModuleService.Verify(a => a.UpdateModules(modulesStub), Times.Exactly(1));
            _mockModuleService.Verify(a => a.UpdateUserInfo(modulesStub), Times.Exactly(1));
            _mockModuleService.Verify(a => a.UpdateUserModules(modulesStub), Times.Exactly(1));

        }

        private ArgusCrmConsumer CreateConsumer()
        {
            return new ArgusCrmConsumer(_mockBus.Object, _mockModuleService.Object, _mockConsumerService.Object);
        }
    }
}