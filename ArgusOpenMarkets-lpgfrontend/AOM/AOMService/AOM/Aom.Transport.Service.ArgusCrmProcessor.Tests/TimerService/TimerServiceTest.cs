﻿using System.Threading;
using AOM.Transport.Events.ArgusCrm;
using AOM.Transport.Service.ArgusCrmProcessor.TimerService;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace Aom.Transport.Service.ArgusCrmProcessor.Tests.TimerService
{
    [TestFixture]
    public class TimerServiceTest
    {

        private Mock<IBus> _mockBus;

        [SetUp]
        public void SetUp()
        {
            _mockBus = new Mock<IBus>();
            _mockBus.Setup(a => a.Publish<ArgusCrmRefreshRequest>(It.IsAny<ArgusCrmRefreshRequest>())).Verifiable();
        }

        [Test]
        public void StartTimerTest()
        {
            var service = new ArgusCrmTimerService(_mockBus.Object);
            service.StartTimer(1);
            Thread.Sleep(150);
            _mockBus.Verify(a => a.Publish<ArgusCrmRefreshRequest>(It.IsAny<ArgusCrmRefreshRequest>()), Times.Exactly(1));
        }
    }
}