﻿using System;
using AOM.Transport.Service.Processor.Common.PerfmonBusImpl;
using Argus.Transport.Infrastructure;
using log4net.Core;
using Moq;
using NUnit.Framework;
using Utils.Logging;
using Utils.Javascript.Tests.Extension;

namespace AOM.Transport.Service.Processor.Common.Tests
{
    [TestFixture]
    public class PerfmonBusTests
    {
        private PerfmonBus _perfmonBus;
        private Mock<IBus> _mockBus;
        private Mock<ISlowMessageProcessingChecker> _mockSlowMessageProcessingChecker;

        // Needs to be public for MockBusExtensions to work properly
        public class FakeActionType { }

        private const string FakeSubscriptionId = "FakeSubscriptionIdString";

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockBus.SpyOnSubscribe<FakeActionType>();
            _mockSlowMessageProcessingChecker = new Mock<ISlowMessageProcessingChecker>();
            _perfmonBus = CreatePerfmonBus();
        }

        private PerfmonBus CreatePerfmonBus(bool useNullBus = false,
                                            bool useNullSlowMessageProcessingChecker = false)
        {
            return new PerfmonBus(
                useNullBus ? null : _mockBus.Object,
                useNullSlowMessageProcessingChecker ? null : _mockSlowMessageProcessingChecker.Object);
        }

        [Test]
        public void ShouldThrowOnConstructionIfArgusBusNull()
        {
            Assert.Throws<ArgumentNullException>(() => CreatePerfmonBus(useNullBus: true));
        }
        
        [Test]
        public void ShouldThrowOnConstructionIfSlowMessageProcessingCheckerNull()
        {
            Assert.Throws<ArgumentNullException>(() => CreatePerfmonBus(useNullSlowMessageProcessingChecker: true));
        }

        private Action<T> GetSingleSubscriptionAction<T>()
        {
            var actions = _mockBus.GetSubscriptionActions();
            Assert.That(actions.Count, Is.EqualTo(1));
            var action = actions[0];
            return action;
        }

        [Test]
        public void ShouldSubscribeToTheUnderlyingBusWhenSubscribedTo()
        {
            _perfmonBus.Subscribe<FakeActionType>(FakeSubscriptionId, type => { });
            var action = GetSingleSubscriptionAction<FakeActionType>();
            Assert.IsNotNull(action);
            Assert.That(action, Is.TypeOf<Action<FakeActionType>>());
        }

        [Test]
        public void ShouldNotLogWarningOrErrorIfMessageCheckerReturnsFalseForBothWarningAndError()
        {
            _mockSlowMessageProcessingChecker.Setup(
                c => c.ShouldWarn<FakeActionType>(It.IsAny<long>())).Returns(false);
            _mockSlowMessageProcessingChecker.Setup(
                c => c.ShouldError<FakeActionType>(It.IsAny<long>())).Returns(false);

            using (var testAppender = new MemoryAppenderForTests())
            {
                _perfmonBus.Subscribe<FakeActionType>(FakeSubscriptionId, type => { });
                var action = GetSingleSubscriptionAction<FakeActionType>();
                action.Invoke(new FakeActionType());
                testAppender.AssertNoLogMessagesAtLevel(Level.Warn, Level.Error);
            }
        }

        [Test]
        public void ShouldLogWarningIfMessageCheckerReturnsTrueForWarning()
        {
            _mockSlowMessageProcessingChecker.Setup(
                c => c.ShouldWarn<FakeActionType>(It.IsAny<long>())).Returns(true);
            _mockSlowMessageProcessingChecker.Setup(
                c => c.ShouldError<FakeActionType>(It.IsAny<long>())).Returns(false);

            using (var testAppender = new MemoryAppenderForTests())
            {
                _perfmonBus.Subscribe<FakeActionType>(FakeSubscriptionId, type => { });
                var action = GetSingleSubscriptionAction<FakeActionType>();
                action.Invoke(new FakeActionType());
                testAppender.AssertALogMessageContains(Level.Warn, "Message processing is too slow");
                testAppender.AssertALogMessageContains(Level.Warn, typeof(FakeActionType).Name);
                testAppender.AssertNoLogMessagesAtLevel(Level.Error);
            }
        }

        [Test]
        public void ShouldLogErrorIfMessageCheckerReturnsTrueForError()
        {
            _mockSlowMessageProcessingChecker.Setup(
                c => c.ShouldWarn<FakeActionType>(It.IsAny<long>())).Returns(false);
            _mockSlowMessageProcessingChecker.Setup(
                c => c.ShouldError<FakeActionType>(It.IsAny<long>())).Returns(true);

            using (var testAppender = new MemoryAppenderForTests())
            {
                _perfmonBus.Subscribe<FakeActionType>(FakeSubscriptionId, type => { });
                var action = GetSingleSubscriptionAction<FakeActionType>();
                action.Invoke(new FakeActionType());
                testAppender.AssertALogMessageContains(Level.Error, "Message processing is too slow");
                testAppender.AssertALogMessageContains(Level.Error, typeof(FakeActionType).Name);
                testAppender.AssertNoLogMessagesAtLevel(Level.Warn);
            }
        }

        [Test]
        public void ShouldLogOnlyErrorIfMessageCheckerReturnsTrueForWarningAndError()
        {
            _mockSlowMessageProcessingChecker.Setup(
                c => c.ShouldWarn<FakeActionType>(It.IsAny<long>())).Returns(true);
            _mockSlowMessageProcessingChecker.Setup(
                c => c.ShouldError<FakeActionType>(It.IsAny<long>())).Returns(true);

            using (var testAppender = new MemoryAppenderForTests())
            {
                _perfmonBus.Subscribe<FakeActionType>(FakeSubscriptionId, type => { });
                var action = GetSingleSubscriptionAction<FakeActionType>();
                action.Invoke(new FakeActionType());
                testAppender.AssertALogMessageContains(Level.Error, "Message processing is too slow");
                testAppender.AssertALogMessageContains(Level.Error, typeof(FakeActionType).Name);
                testAppender.AssertNoLogMessagesAtLevel(Level.Warn);
            }
        }
    }
}