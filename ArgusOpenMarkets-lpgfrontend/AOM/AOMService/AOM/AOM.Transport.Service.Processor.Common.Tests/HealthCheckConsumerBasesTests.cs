﻿using System.Linq;
using Argus.Transport.Infrastructure;
using Ninject;
using NUnit.Framework;

namespace AOM.Transport.Service.Processor.Common.Tests
{
    using Moq;

    [TestFixture]
    public class HealthCheckConsumerBaseTests
    {
        [Test]
        public void HealthCheckConsumerBaseShouldReturnAValueForEnvironmentMachineName()
        {
            Mock<IBus> mockBus = new Mock<IBus>();
            var fakeHealthCheckConsumer = new FakeHealthCheckConsumer(mockBus.Object);

            Assert.That(!string.IsNullOrEmpty(fakeHealthCheckConsumer.EnvironmentMachineName));
        }

        public class FakeHealthCheckConsumer : HealthCheckConsumerBase
        {
            public FakeHealthCheckConsumer(IBus bus) : base(bus)
            {
                
            }

            public override string ProcessorName
            {
                get
                {
                    return "FakeProcessor";
                }
            }

            public override string ProcessorFullName
            {
                get
                {
                    return "FakeProcessorFullName";
                }
            }
        }
    }
}