﻿using AOM.Transport.Events.System;
using Argus.Transport.Infrastructure;
using log4net.Core;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Topshelf;
using Utils.Logging;
using Utils.Javascript.Tests.Extension;

namespace AOM.Transport.Service.Processor.Common.Tests
{
    [TestFixture]
    public class ProcessorServiceBaseTests
    {
        private class FakeService : ProcessorServiceBase
        {
            public FakeService(IEnumerable<IConsumer> consumers, IServiceManagement serviceManagement, IBus aomBus)
                : base(consumers, serviceManagement, aomBus)
            {
            }

            public override string Name
            {
                get { return "FakeService"; }
            }
        }

        private Mock<IConsumer> _consumer1;
        private Mock<IConsumerWithInitialise> _consumer2;
        private Mock<IServiceManagement> _mockServiceManagement;
        private Mock<IBus> _mockBus;
        private Mock<HostControl> _mockHostControl;
        private FakeService _sut;

        [SetUp]
        public void Setup()
        {
            _consumer1 = new Mock<IConsumer>();
            _consumer2 = new Mock<IConsumerWithInitialise>();
            _mockServiceManagement = new Mock<IServiceManagement>();
            _mockBus = new Mock<IBus>();
            _sut = new FakeService(new [] {_consumer1.Object, _consumer2.Object},
                                   _mockServiceManagement.Object,
                                   _mockBus.Object);
            _mockHostControl = new Mock<HostControl>();
        }


        [Test]
        public void ShouldLogServiceNameOnStart()
        {
            using (var log = new MemoryAppenderForTests())
            {
                _sut.Start(_mockHostControl.Object);
                log.AssertALogMessageContains(Level.Info, "Starting " + _sut.Name);
            }
        }

        [Test]
        public void ShouldStartServiceManagementOnStart()
        {
            _sut.Start(_mockHostControl.Object);
            _mockServiceManagement.Verify(m => m.OnStart(), Times.Once);
        }

        [Test]
        public void ShouldStartEachConsumerOnStart()
        {
            _sut.Start(_mockHostControl.Object);
            _consumer1.Verify(c => c.Start(), Times.Once);
            _consumer2.Verify(c => c.Start(), Times.Once);
        }

        [Test]
        public void ShouldRegisterWarmupForConsumerWithInitialiseOnStart()
        {
            _mockBus.SpyOnSubscribe<WarmUpRequest>();
            _sut.Start(_mockHostControl.Object);
            var subs = _mockBus.GetSubscriptionActions() as List<Action<WarmUpRequest>>;
            Assert.NotNull(subs);
            Assert.That(subs.Count, Is.EqualTo(1));
            _mockBus.Verify(b => b.Publish(It.IsAny<WarmUpRequest>(), _consumer2.Object.GetType().Name), Times.Once);
        }

        [Test]
        public void ShouldLogErrorAndThrowIfConsumerThrowsOnStart()
        {
            var e = new ApplicationException("Fake exception");
            _consumer1.Setup(c => c.Start()).Throws(e);
            using (var log = new MemoryAppenderForTests())
            {
                Assert.Throws<ApplicationException>(() => _sut.Start(_mockHostControl.Object));
                log.AssertALogMessageContains(Level.Error, "Fake exception");
            }
        }

        [Test]
        public void ShouldLogStartupSucessMessageIfNoErrorsOnStart()
        {
            using (var log = new MemoryAppenderForTests())
            {
                _sut.Start(_mockHostControl.Object);
                log.AssertALogMessageContains(Level.Info, "started successfully");
            }
        }

        [Test]
        public void ShouldLogServiceNameOnStop()
        {
            using (var log = new MemoryAppenderForTests())
            {
                _sut.Stop(_mockHostControl.Object);
                log.AssertALogMessageContains(Level.Info, "Stopping " + _sut.Name);
            }
        }

        [Test]
        public void ShouldDisposeAomBusOnStop()
        {
            _sut.Stop(_mockHostControl.Object);
            _mockBus.Verify(b => b.Dispose(), Times.Once);
        }

        [Test]
        public void ShouldStopServiceManagementOnStop()
        {
            _sut.Stop(_mockHostControl.Object);
            _mockServiceManagement.Verify(sm => sm.OnStop(), Times.Once);
        }

        [Test]
        public void ShouldLogErrorIfBusDisposeThrows()
        {
            var e = new ApplicationException("Fake exception");
            _mockBus.Setup(b => b.Dispose()).Throws(e);
            using (var log = new MemoryAppenderForTests())
            {
                _sut.Stop(_mockHostControl.Object);
                log.AssertALogMessageContains(Level.Error, "AOM Bus Dispose");
                log.AssertALogMessageContains(Level.Error, "Fake exception");
            }
        }

        [Test]
        public void ShouldLogErrorIfServiceManagementStopThrows()
        {
            var e = new ApplicationException("Fake exception");
            _mockServiceManagement.Setup(sm => sm.OnStop()).Throws(e);
            using (var log = new MemoryAppenderForTests())
            {
                _sut.Stop(_mockHostControl.Object);
                log.AssertALogMessageContains(Level.Error, "Service Management Stop");
                log.AssertALogMessageContains(Level.Error, "Fake exception");
            }
        }

        [Test]
        public void ShouldLogServiceNameWhenStopped()
        {
            using (var log = new MemoryAppenderForTests())
            {
                _sut.Stop(_mockHostControl.Object);
                log.AssertALogMessageContains(Level.Info, _sut.Name + " stopped");
            }
        }

        // TODO marksh How to unit test service stop timeout without having a >10s test?
    }
}
