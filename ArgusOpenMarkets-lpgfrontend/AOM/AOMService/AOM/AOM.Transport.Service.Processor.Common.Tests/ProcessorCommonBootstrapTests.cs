﻿
using System.Linq;
using Argus.Transport.Infrastructure;
using Ninject;
using NUnit.Framework;

namespace AOM.Transport.Service.Processor.Common.Tests
{
    [TestFixture]
    public class ProcessorCommonBootstrapTests
    {
        [Test]
        public void PerfmonBusWrapsIBusTest()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(new ProcessorCommonBootstrap());
                
                var bus = kernel.Get<IBus>();

                Assert.AreEqual(typeof(PerfmonBus).Name, bus.GetType().Name, "Expected IBus to be wrapped by the permonance monitoring logic" );
            }            
        }

        [Test]
        public void CommonProcessorAlwaysIncludeHeathCheck()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(new ProcessorCommonBootstrap());

                var consumers = kernel.GetAll<IConsumer>();

                Assert.IsNotNull(consumers.Where(c=>c.GetType()==typeof(HealthCheckConsumer)).Single(), "HeathCheck consumer was expect to have been loaded into ninject kernel");
            }
        }   
    }
}

