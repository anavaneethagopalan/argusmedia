﻿using System;
using System.Linq;
using System.Threading;

using AOM.App.Domain.Dates;
using AOM.Repository.MySql.Aom;
using AOM.Repository.MySql.Tests;
using AOM.Repository.MySql.Tests.Aom;
using AOM.Transport.Events.ScheduledTasks;
using Utils.Logging;

using Argus.Transport.Infrastructure;
using Moq;
using Moq.Language;
using NUnit.Framework;
using log4net.Core;

namespace AOM.Transport.Service.Processor.Common.Tests
{
    internal class MockScheduledTask
    {
        public string StringVar { get; set; }

    };

    [TestFixture]
    public class ScheduledTaskConsumerBaseTests
    {
        private IAomModel _mockAomModel;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private Mock<IBus> _mockBus;

        [SetUp]
        public void Setup()
        {
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockAomModel = new MockAomModel();
            _mockBus = new Mock<IBus>();
        }

//        [Test]
//        [Category("local")]
//        public void TryAndBreakScheduledConcurrency()
//        {
//            const int secondsToRunFor = 120;
//            const int intervalSeconds = 5; //TaskRepeatInterval.Test
//
//            string scheduledTaskType = typeof(MessageGenerateByScheduler).Name;
//
//            var nextrun = DateTime.UtcNow.AddSeconds(10);
//
//            var testTask = new ScheduledTaskDto()
//            {
//                TaskType = scheduledTaskType,
//                TaskName = "for unit testing",
//                DateCreated = nextrun.AddYears(-1).Date,
//                LastRun = nextrun.AddDays(-1),
//                Arguments = "{\"MessageText\":\"REMINDER\"}",
//                Interval = DtoMappingAttribute.GetValueFromEnum(TaskRepeatInterval.Test),
//                NextRun = nextrun,
//                LastUpdated = DateTime.UtcNow,
//                SkipIfMissed = true
//            };
//
//            var multipleSchedulers = new List<ScheduledTaskConsumerBase<MessageGenerateByScheduler>>();
//
//            using (var aom = new AomModel())
//            {
//                var existingTestTasks = from s in aom.ScheduledTasks where s.TaskType == scheduledTaskType select s;
//                existingTestTasks.ToList().ForEach(s => aom.ScheduledTasks.Remove(s));
//
//                aom.ScheduledTasks.Add(testTask);
//                aom.SaveChangesAndLog(-1);
//            }
//
//            for (int i = 0; i < 5; i++)
//            {
//                multipleSchedulers.Add(new ScheduledTaskConsumerBase<MessageGenerateByScheduler>(_mockBus.Object, new DateTimeProvider(), new DbContextFactory()));
//            }
//
//            Parallel.ForEach(multipleSchedulers, (s) => s.Start());
//            Thread.Sleep(TimeSpan.FromSeconds(secondsToRunFor));
//            multipleSchedulers.ForEach(s => s.Dispose());
//
//            multipleSchedulers.Clear();
//
//            _mockBus.Verify(x => x.Publish(It.IsAny<MessageGenerateByScheduler>()), Times.Exactly(secondsToRunFor / intervalSeconds));
//        }

        internal class MessageGenerateByScheduler
        {
            public string MessageText { get; set; }
        }

        public void SetupSingleSchedule()
        {
            _mockDateTimeProvider.SetupGet(x => x.UtcNow).Returns(new DateTime(2014, 1, 7));
            _mockAomModel.ScheduledTasks.Add(
                new ScheduledTaskDto()
                {
                    Arguments = "{\"StringVar\":\"TestString\"}",
                    DateCreated = new DateTime(2014, 1, 1),
                    Id = 1,
                    Interval = "D",
                    LastRun = null,
                    NextRun = new DateTime(1974, 1, 1, 0, 0, 0),
                    TaskName = "Test Market Info Ticker Purge",
                    TaskType = "MockScheduledTask",
                    SkipIfMissed = false,
                    IgnoreTask = false
                });
        }

        [Test]
        public void TestScheduledTaskActivationAndRollDaily()
        {
            SetupSingleSchedule();

            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).Interval = "D";

            var purgeRequestConsumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object, _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));
            purgeRequestConsumer.Start();

            // Wait for the callback to execute
            Thread.Sleep(1000);

            _mockBus.Verify(x => x.Publish(It.IsAny<MockScheduledTask>()), Times.Once);

            //Assert the DTO has been updated.
            var taskDto = _mockAomModel.ScheduledTasks.Single(x => x.Id == 1);
            // Have we set lastrun
            Assert.AreEqual(taskDto.LastRun.Value.Date, _mockDateTimeProvider.Object.UtcNow.Date);

            // Have we scheduled the task to run tomorrow?
            Assert.AreEqual(new DateTime(2014, 1, 8).Date, taskDto.NextRun.Date);
        }

        [Test]
        public void TestScheduledTaskActivationIfSetInFutureAndRollDaily()
        {
            var timeNow = new DateTime(2014, 1, 7);
            SetupSingleSchedule();

            _mockDateTimeProvider.SetupSequence(x => x.UtcNow).ReturnsIncrementingDateTime(timeNow, TimeSpan.FromSeconds(1), 10);

            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).Interval = "D";
            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).NextRun = timeNow.AddSeconds(1);

            var purgeRequestConsumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object, _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));
            purgeRequestConsumer.Start();

            // Wait for the callback to execute
            Thread.Sleep(1000);

            _mockBus.Verify(x => x.Publish(It.IsAny<MockScheduledTask>()), Times.AtLeastOnce);
        }

        [Test]
        public void TestScheduledTaskActivationAndRollWeekly()
        {
            SetupSingleSchedule();

            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).Interval = "W";
            var purgeRequestConsumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object, _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));
            purgeRequestConsumer.Start();

            // Wait for the callback to execute
            Thread.Sleep(1000);

            _mockBus.Verify(x => x.Publish(It.IsAny<MockScheduledTask>()), Times.Once);

            //Assert the DTO has been updated.
            var taskDto = _mockAomModel.ScheduledTasks.Single(x => x.Id == 1);
            // Have we set lastrun
            Assert.AreEqual(taskDto.LastRun.Value.Date, _mockDateTimeProvider.Object.UtcNow.Date);

            // Have we scheduled the task to run tomorrow?
            Assert.AreEqual(new DateTime(2014, 1, 14).Date.Date, taskDto.NextRun.Date);
        }

        [Test]
        public void TestScheduledTaskActivationAndRollMonthly()
        {
            SetupSingleSchedule();

            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).Interval = "M";

            var purgeRequestConsumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object, _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));
            purgeRequestConsumer.Start();

            // Wait for the callback to execute
            Thread.Sleep(1000);

            _mockBus.Verify(x => x.Publish(It.IsAny<MockScheduledTask>()), Times.Once);

            //Assert the DTO has been updated.
            var taskDto = _mockAomModel.ScheduledTasks.Single(x => x.Id == 1);
            // Have we set lastrun
            Assert.AreEqual(taskDto.LastRun.Value.Date, _mockDateTimeProvider.Object.UtcNow.Date);

            // Have we scheduled the task to run tomorrow?
            Assert.AreEqual(new DateTime(2014, 2, 1), taskDto.NextRun.Date);
        }

        [Test]
        public void TestScheduledTaskSkippedIfMissed()
        {
            SetupSingleSchedule();
            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).Interval = "D";
            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).SkipIfMissed = true;

            var purgeRequestConsumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object, _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));
            purgeRequestConsumer.Start();

            // Wait for the callback to execute
            Thread.Sleep(1000);

            // Don't execute the task!
            _mockBus.Verify(x => x.Publish(It.IsAny<MockScheduledTask>()), Times.Never);

            //Assert the DTO has been updated.
            var taskDto = _mockAomModel.ScheduledTasks.Single(x => x.Id == 1);
            // Have we scheduled the task to run tomorrow?
            Assert.AreEqual(new DateTime(2014, 1, 8), taskDto.NextRun.Date);
        }

        [Test]
        public void TestScheduledTaskIgnoredIfSet()
        {
            SetupSingleSchedule();
            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).Interval = "D";
            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).IgnoreTask = true;

            var purgeRequestConsumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object, _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));
            purgeRequestConsumer.Start();

            // Wait for the callback to execute
            Thread.Sleep(1000);

            // Don't execute the task!
            _mockBus.Verify(x => x.Publish(It.IsAny<MockScheduledTask>()), Times.Never);

            //Assert the DTO has been updated.
            var taskDto = _mockAomModel.ScheduledTasks.Single(x => x.Id == 1);
            // Have we scheduled the task to run tomorrow?
            Assert.AreEqual(new DateTime(2014, 1, 8), taskDto.NextRun.Date);
        }

        [Test]
        public void TestScheduledTaskLogsMessageIfNotScheduled()
        {
            SetupSingleSchedule();
            _mockAomModel.ScheduledTasks.Single(t => t.Id == 1).TaskType = "NotAnActualTask";

            var purgeRequestConsumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object,  _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));

            using (var log = new MemoryAppenderForTests())
            {
                purgeRequestConsumer.Start();
                log.AssertALogMessageContains(
                    Level.Debug,
                    "No tasks of type " + typeof(MockScheduledTask).Name);
            }
        }

        [Test]
        public void ShouldGetListOfTasksWhenConsumingGetAllScheduledTasksRequestRpc()
        {
            SetupSingleSchedule();
            Assert.That(_mockAomModel.ScheduledTasks.Count(), Is.EqualTo(1));

            var consumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object, _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));
            var responseFn = StartAndGetRpcResponseFunction<GetAllScheduledTasksRequestRpc, GetAllScheduledTasksResponseRpc>(consumer, _mockBus);
            var request = new GetAllScheduledTasksRequestRpc
            {
                TaskType = typeof(MockScheduledTask).Name
            };
            var response = responseFn(request);
            Assert.That(response.ScheduledTasks.Count, Is.EqualTo(1));
            Assert.That(response.ScheduledTasks[0].TaskType, Is.EqualTo(request.TaskType));
        }

        [Test]
        public void ShouldReturnEmptyListWhenAskingForTypeOfTaskThatIsNotInDb()
        {
            SetupSingleSchedule();
            Assert.That(_mockAomModel.ScheduledTasks.Count(), Is.EqualTo(1));

            var consumer = new ScheduledTaskConsumerBase<MockScheduledTask>(_mockBus.Object, _mockDateTimeProvider.Object, new MockDbContextFactory(_mockAomModel, null));
            var responseFn = StartAndGetRpcResponseFunction<GetAllScheduledTasksRequestRpc, GetAllScheduledTasksResponseRpc>(consumer, _mockBus);
            var request = new GetAllScheduledTasksRequestRpc
            {
                TaskType = "A different type that does not exist"
            };
            var response = responseFn(request);
            Assert.That(response.ScheduledTasks.Count, Is.EqualTo(0));
        }

        private static Func<TRpcRequest, TResponse> StartAndGetRpcResponseFunction<TRpcRequest, TResponse>(IConsumer consumer, Mock<IBus> mockBus) where TRpcRequest : class where TResponse : class
        {
            Func<TRpcRequest, TResponse> responseFunc = null;
            mockBus.Setup(b => b.Respond(It.IsAny<Func<TRpcRequest, TResponse>>())).Callback((Func<TRpcRequest, TResponse> fn) => responseFunc = fn);
            consumer.Start();
            Assert.IsNotNull(responseFunc);
            return responseFunc;
        }
    }

    public static class MoqExtensions
    {
        public static ISetupSequentialResult<DateTime> ReturnsIncrementingDateTime(this ISetupSequentialResult<DateTime> setupSequence, DateTime start, TimeSpan span, int totalSteps)
        {
            for (int i = 0; i < totalSteps; i++)
            {
                setupSequence = setupSequence.Returns(start);
                start += span;
            }
            return setupSequence;
        }
    }
}