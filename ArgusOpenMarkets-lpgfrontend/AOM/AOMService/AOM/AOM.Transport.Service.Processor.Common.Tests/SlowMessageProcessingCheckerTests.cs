﻿using AOM.App.Domain.Dates;
using AOM.Transport.Events.Emails;
using AOM.Transport.Service.Processor.Common.PerfmonBusImpl;
using Moq;
using NUnit.Framework;
using System;

namespace AOM.Transport.Service.Processor.Common.Tests
{
    [TestFixture]
    public class SlowMessageProcessingCheckerTests
    {
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private ActionPerfmonBusConfiguration _config;
        private SlowMessageProcessingChecker _checker;
        
        private readonly DateTime _fakeStartTime = new DateTime(2015, 06, 15);

        // ReSharper disable ClassNeverInstantiated.Local
        class FakeActionType { }
        class FakeRefreshType { }
        class FakeWarmupRequest { }
        // ReSharper restore ClassNeverInstantiated.Local


        [SetUp]
        public void Setup()
        {
            _config = new ActionPerfmonBusConfiguration();

            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockDateTimeProvider.SetupGet(dtp => dtp.UtcNow).Returns(_fakeStartTime);

            _checker = CreateChecker();
            
            _mockDateTimeProvider.SetupGet(dtp => dtp.UtcNow)
                     .Returns(_fakeStartTime.AddMinutes(_config.DelayInReportingToAllowForStartupMinutes + 1));
        }

        private SlowMessageProcessingChecker CreateChecker(bool useNullDateTimeProvider = false,
                                                           bool useNullConfig = false)
        {
            return new SlowMessageProcessingChecker(useNullConfig ? null : _config,
                                                    useNullDateTimeProvider ? null : _mockDateTimeProvider.Object);
        }

        [Test]
        public void ShouldThrowOnConstructionIfDateTimeProviderNull()
        {
            Assert.Throws<ArgumentNullException>(() => CreateChecker(useNullDateTimeProvider: true));
        }

        [Test]
        public void ShouldThrowOnConstructionIfConfigNull()
        {
            Assert.Throws<ArgumentNullException>(() => CreateChecker(useNullConfig: true));
        }

        [Test]
        public void ShouldNotSayWarnOrErrorIfActionTimeBelowBothThresholds()
        {
            var threshold = Math.Min(_config.SlowProcessingWarningThresholdMsecs,
                                     _config.SlowProcessingErrorThresholdMsecs);
            Assert.IsFalse(_checker.ShouldWarn<FakeActionType>(threshold - 100));
            Assert.IsFalse(_checker.ShouldWarn<FakeActionType>(threshold - 100));
        }

        [Test]
        public void ShouldSayWarnIfActionTimeAboveWarningThreshold()
        {
            Assert.True(_checker.ShouldWarn<FakeActionType>(_config.SlowProcessingWarningThresholdMsecs + 100));
        }

        [Test]
        public void ShouldSayErrorIfActionTimeAboveErrorThreshold()
        {
            Assert.True(_checker.ShouldWarn<FakeActionType>(_config.SlowProcessingErrorThresholdMsecs + 100));
        }

        [Test]
        public void ShouldSayWarnAndErrorIfActionTimeAboveBothThresholds()
        {
            var threshold = Math.Max(_config.SlowProcessingWarningThresholdMsecs,
                                     _config.SlowProcessingErrorThresholdMsecs);
            Assert.True(_checker.ShouldWarn<FakeActionType>(threshold + 100));
            Assert.True(_checker.ShouldError<FakeActionType>(threshold + 100));
        }

        [Test]
        public void ShouldNotWarnOrErrorIfActionIsOneOfAcceptedSlowMessageTypes()
        {
            var threshold = Math.Max(_config.SlowProcessingWarningThresholdMsecs,
                                     _config.SlowProcessingErrorThresholdMsecs);
            var time = threshold + 1000;
            Assert.False(_checker.ShouldError<AomSendEmailRequest>(time));
            Assert.False(_checker.ShouldWarn<AomSendEmailRequest>(time));
            Assert.False(_checker.ShouldError<FakeRefreshType>(time));
            Assert.False(_checker.ShouldWarn<FakeRefreshType>(time));
            Assert.False(_checker.ShouldError<FakeWarmupRequest>(time));
            Assert.False(_checker.ShouldWarn<FakeWarmupRequest>(time));
        }

        [Test]
        public void ShouldNotWarnOrErrorIfDuringStartup()
        {
            var threshold = Math.Max(_config.SlowProcessingWarningThresholdMsecs,
                                     _config.SlowProcessingErrorThresholdMsecs);
            var time = threshold + 1000;
            _mockDateTimeProvider.SetupGet(dtp => dtp.UtcNow).Returns(_fakeStartTime);
            Assert.False(_checker.ShouldWarn<FakeActionType>(time));
            Assert.False(_checker.ShouldError<FakeActionType>(time));
        }
    }
}
