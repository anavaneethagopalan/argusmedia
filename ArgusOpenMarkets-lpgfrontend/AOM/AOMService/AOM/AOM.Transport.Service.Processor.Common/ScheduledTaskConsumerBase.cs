﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Threading;

using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Transport.Events.ScheduledTasks;

using Argus.Transport.Infrastructure;
using Ninject;
using Utils.Database;
using Utils.Javascript.Extension;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.Processor.Common
{
    public class ScheduledTaskConsumerBase<TTaskEvent> : IConsumer where TTaskEvent : class
    {
        public IBus AomBus
        {
            get { return _aomBus; }
        }

        public int TimerCount
        {
            get { return _taskTimers.Count; }
        }

        private readonly IDbContextFactory _dbContextFactory;
        private readonly IBus _aomBus;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly ConcurrentDictionary<long, Timer> _taskTimers = new ConcurrentDictionary<long, Timer>();

        public ScheduledTaskConsumerBase([Named("AomBus")] IBus aomBus, IDateTimeProvider dateTimeProvider, IDbContextFactory dbContextFactory = null)
        {
            _dbContextFactory = dbContextFactory ?? new DbContextFactory();
            _aomBus = aomBus;
            _dateTimeProvider = dateTimeProvider;
        }

        public virtual void Start()
        {
            Console.WriteLine("Starting the bus from ScheduledTaskConsumerBase");
            _aomBus.Respond<GetAllScheduledTasksRequestRpc, GetAllScheduledTasksResponseRpc>(ConsumeGetAllScheduledTasksRequest);
            InitializeTimersFromDatabase();
        }

        public void InitializeTimersFromDatabase()
        {
            using (var aom = _dbContextFactory.CreateAomModel())
            {
                var tasksOfThisEventType = aom.ScheduledTasks.Where(task => task.TaskType == typeof(TTaskEvent).Name).ToList();

                if (tasksOfThisEventType.Count == 0)
                {
                    Log.DebugFormat("No tasks of type {0} scheduled to run in ScheduledTaskConsumerBase", typeof(TTaskEvent).Name);
                    return;
                }

                foreach (var taskDto in tasksOfThisEventType)
                {
                    var task = taskDto.ToEntity();
                    var nextRunCheck = IncrementNextTimeToRun(task.Interval, _dateTimeProvider.UtcNow);

                    if (task.NextRun > nextRunCheck)
                    {
                        //just log the weirdness that the nextrun is futher in the future than we would expect with the interval 
                        Log.ErrorFormat(LogPrefix(task) + 
                            "The scheduled next run: {0} for the task [{1}] is further in the future: {2} than would be expected with the configured interval. No adjustment will be made.",
                            task.NextRun, nextRunCheck, task.ToLogString());
                    }

                    if (DoTaskRollAndResetTimer(taskDto.Id) && !task.SkipIfMissed)
                    {
                        ExecuteTask(task);
                    }
                }
            }
        }

        private void OnTimerTickCheckTask(object data)
        {
            var task = data as ScheduledTask;
            Log.InfoFormat(LogPrefix(task) + "OnTimerTickCheckTask fired for {0}", task != null ? task.ToLogString() : "null");

            if (task == null)
            {
                Log.WarnFormat("OnTimerTickCheckTask called with invalid data [{0}]", data == null ? "null" : data.GetType().Name);
                return;
            }

            Log.Info("Calling DeleteScheduledTaskTimers from OnTimerTickCheckTask");
            DeleteAndDisposeTimerForTaskIfPresent(task.Id);

            if (DoTaskRollAndResetTimer(task.Id))
            {
                ExecuteTask(task);
            }
            else
            {
                Log.InfoFormat(LogPrefix(task) + "Skipped Execution");
            }
        }

        private void BuildTimerForNextCallback(ScheduledTask task)
        {
            var runInMsecs = task.NextRun - _dateTimeProvider.UtcNow;

            if (runInMsecs < TimeSpan.FromMilliseconds(0))
            {
                // -ve is a disabled timer so we change schedule for immediate execution
                Log.WarnFormat(LogPrefix(task) + "Attempt to create timer with -ve value (Disabled) so will schedule immediate execute : Interval={0}", runInMsecs);
                runInMsecs = TimeSpan.FromMilliseconds(1);
            }

            Debug.Assert(runInMsecs.TotalMilliseconds > 0, "-ve interval for timer would disable it");

            Log.Info("Attempting to remove and re-add timer to dictionary for: " + task.Id);
            if (!_taskTimers.TryAdd(task.Id, new Timer(OnTimerTickCheckTask, task, runInMsecs, new TimeSpan(0, 0, 0, 0, -1))))
            {
                Log.ErrorFormat(LogPrefix(task) + "ERROR: Failed to add timer to dictionary for task as it is already present: {0}", task.ToLogString());
            }

            Log.InfoFormat(LogPrefix(task) + "Successfully added a new timer to dictionary: {0}, Process: {1} Existing timers: {2}.  Executing in {3}.",
                task.ToLogString(), Process.GetCurrentProcess().ProcessName, GetTaskIdsFromTimers(), runInMsecs);
        }

        /// <summary>
        /// Rolls dates and resets timers
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns>bool - true if it rolled the dates</returns>
        private bool DoTaskRollAndResetTimer(long taskId)
        {
            return ConcurrencyHelper.RetryOnConcurrencyException(String.Format("Updating task TaskId={0}", taskId), 3, () =>
                {
                    using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
                    {
                        bool thisProcessUpdatedTheTaskTheTask = false;
                        using (var aom = _dbContextFactory.CreateAomModel())
                        {
                            var taskInDatabase = aom.ScheduledTasks.Single(x => x.Id == taskId);
                            var timeNow = _dateTimeProvider.UtcNow;
                            var interval = taskInDatabase.NextRun - timeNow;

                            if (interval <= TimeSpan.FromSeconds(0))
                            {
                                Log.InfoFormat(LogPrefix(taskInDatabase) + "Scheduled to run now - attempt to update database. Interval until nextrun is {0}", interval);
                                taskInDatabase.LastRun = timeNow;
                                taskInDatabase.NextRun = CalculateFutureTimeToCallTaskAgain(_dateTimeProvider, taskInDatabase.ToEntity());
                                taskInDatabase.LastUpdated = _dateTimeProvider.UtcNow;

                                aom.SaveChangesAndLog(-1);

                                thisProcessUpdatedTheTaskTheTask = true;
                                Log.InfoFormat(LogPrefix(taskInDatabase) + "Database updated with next run. Nextrun at {0}", taskInDatabase.NextRun);
                            }
                            else
                            {
                                Log.InfoFormat(LogPrefix(taskInDatabase) + "Not scheduled to run now. Interval until nextrun is {0}", interval);
                            }

                            BuildTimerForNextCallback(taskInDatabase.ToEntity());

                            trans.Complete();

                            if (taskInDatabase.IgnoreTask)
                            {
                                //if task set to Ignore then return false so Execute doesn't happen but we have rolled dates etc so timers are maintained
                                Log.InfoFormat(LogPrefix(taskInDatabase) + "set to Ignore in DB table");
                                return false;
                            }
                            return thisProcessUpdatedTheTaskTheTask;
                        }
                    }
                });
        }

        private void ExecuteTask(ScheduledTask task)
        {
            var command = (TTaskEvent)Activator.CreateInstance(typeof(TTaskEvent));

            // Set the properties (that we have available)
            foreach (var prop in typeof(TTaskEvent).GetProperties())
            {
                try
                {
                    if (task.Arguments.ContainsKey(prop.Name))
                    {
                        prop.SetValue(command, Convert.ChangeType(task.GetArgument(prop.Name), prop.PropertyType));
                    }
                }
                catch (Exception e)
                {
                    Log.Error(LogPrefix(task) + string.Format("Failed to initialise property '{0}' whilst executing task {1}", prop.Name, task.ToLogString()), e);
                }
            }

            _aomBus.Publish(command);

            Log.InfoFormat(LogPrefix(task) + "Published Task Message CurrentTimers={0} : Message={1}", GetTaskIdsFromTimers(), task.Arguments.ToJsonCamelCase());
        }

        protected ScheduledTaskResponseRpc ConsumeDeleteScheduledTasksRequest(DeleteScheduledTaskRequestRpc arg)
        {
            Log.InfoFormat("ConsumeDeleteScheduledTasksRequest called for task id {0}, Process: {1} Existing timers: {2}  ", arg.TaskId, Process.GetCurrentProcess().ProcessName, GetTaskIdsFromTimers());
            using (var aom = _dbContextFactory.CreateAomModel())
            {
                string taskName;
                try
                {
                    var task = aom.ScheduledTasks.Where(t => t.TaskType == typeof(TTaskEvent).Name).Single(t => t.Id == arg.TaskId);

                    taskName = task.TaskName;
                    aom.ScheduledTasks.Remove(task);
                    Log.Info("Removed tasks: " + task.Id);
                    aom.SaveChangesAndLog(-1);
                }
                catch (Exception ex)
                {
                    string error = string.Format("TaskId={0} Failed to delete task process: {1} timers: {2}: {3}",
                        arg.TaskId, Process.GetCurrentProcess().ProcessName, GetTaskIdsFromTimers(), ex);
                    Log.Error(error);
                    return new ScheduledTaskResponseRpc(false, error);
                }

                try
                {
                    Log.Info("Calling DeleteScheduledTaskTimers from ConsumeDeleteScheduledTasksRequest");
                    DeleteAndDisposeTimerForTaskIfPresent(arg.TaskId);
                }
                catch (Exception ex)
                {
                    string error = string.Format("TaskId={0} Failed to delete timer: to process: {1}. Existing timers: {2}: {3}",
                        arg.TaskId, Process.GetCurrentProcess().ProcessName, GetTaskIdsFromTimers(), ex);
                    Log.Error(error);
                    return new ScheduledTaskResponseRpc(false, error);
                }

                string success = string.Format("TaskId={0} Successfully Deleted Event {1} to process: {2}. Existing timers: {3} ",
                    arg.TaskId, taskName, Process.GetCurrentProcess().ProcessName, GetTaskIdsFromTimers());

                Log.InfoFormat(success);
                return new ScheduledTaskResponseRpc(true, success);
            }
        }

        private string GetTaskIdsFromTimers()
        {
            return string.Format("[{0}]", string.Join(",", _taskTimers.Keys.Select(a => a)));
        }

        protected ScheduledTaskResponseRpc ConsumeCreateNewScheduledTaskRequest(CreateNewScheduledTaskRequestRpc arg)
        {
            using (var aom = _dbContextFactory.CreateAomModel())
            {
                var scheduledTaskDto = arg.Task.ToDto();
                scheduledTaskDto.NextRun = CalculateFutureTimeToCallTaskAgain(_dateTimeProvider, arg.Task);
                scheduledTaskDto.LastUpdated = _dateTimeProvider.UtcNow;

                aom.ScheduledTasks.Add(scheduledTaskDto);
                try
                {
                    aom.SaveChangesAndLog(-1);
                    Log.Info("Calling ClearTimers from ConsumeCreateNewScheduledTaskRequest");
                    ClearTimers();
                    InitializeTimersFromDatabase();
                }
                catch (DbUpdateException ex)
                {
                    return new ScheduledTaskResponseRpc(false, 
                        string.Format("Failed to add event: {0}. {1}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : string.Empty));
                }
                return new ScheduledTaskResponseRpc(true,
                    string.Format("Successfully added event \"{0}\" to process: \"{1}\". Existing timers: {2} ",
                    arg.Task.TaskName, Process.GetCurrentProcess().ProcessName, string.Join(",", _taskTimers.Keys.Select(a => a))));
            }
        }

        private GetAllScheduledTasksResponseRpc ConsumeGetAllScheduledTasksRequest(GetAllScheduledTasksRequestRpc arg)
        {
            List<ScheduledTask> taskEntities = new List<ScheduledTask>();
            
            try
            {
                using (var aom = _dbContextFactory.CreateAomModel())
                {
                    taskEntities = aom.ScheduledTasks.Where(task => task.TaskType == arg.TaskType).ToList().Select(st => st.ToEntity()).ToList();
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error in ConsumeGetAllScheduledTasksRequest: {0}", ex);
            }

            return new GetAllScheduledTasksResponseRpc { ScheduledTasks = taskEntities };
        }

        //public void Dispose()
        //{
        //    StackTrace t = new StackTrace();
        //    Log.Info("Disposing and calling ClearTimers: " + t.ToString());
            
        //    ClearTimers();
        //}

        private void ClearTimers()
        {
            foreach (var taskId in _taskTimers.Keys.ToArray())
            {
                Log.Info("Calling DeleteScheduledTaskTimers from ClearTimers");
                DeleteAndDisposeTimerForTaskIfPresent(taskId);
            }

            Debug.Assert(_taskTimers.Count == 0);
        }

        private void DeleteAndDisposeTimerForTaskIfPresent(long taskId)
        {
            Timer existingTimer;
            if (_taskTimers.TryRemove(taskId, out existingTimer))
            {
                existingTimer.Dispose();
                Log.InfoFormat("TaskId={0} Removed Timer", taskId);
            }
        }

        private static DateTime CalculateFutureTimeToCallTaskAgain(IDateTimeProvider dateTimeProvider, ScheduledTask task)
        {
            DateTime nextRun = task.NextRun;
            var interval = nextRun - dateTimeProvider.UtcNow;

            while (interval <= TimeSpan.FromSeconds(0))
            {
                nextRun = IncrementNextTimeToRun(task.Interval, nextRun);
                interval = nextRun - dateTimeProvider.UtcNow;
            }
            Log.InfoFormat(LogPrefix(task) + "CalculateFutureTimeToCallTaskAgain. Next execution to be {0}", nextRun.ToString("s"));
            return nextRun;
        }

        private static DateTime IncrementNextTimeToRun(TaskRepeatInterval repeatInterval, DateTime nextRun)
        {
            switch (repeatInterval)
            {
                case TaskRepeatInterval.Test:
                    nextRun = nextRun.AddSeconds(120);
                    break;
                case TaskRepeatInterval.Hour:
                    nextRun = nextRun.AddHours(1);
                    break;
                case TaskRepeatInterval.Day:
                    nextRun = nextRun.AddDays(1);
                    break;
                case TaskRepeatInterval.Week:
                    nextRun = nextRun.AddDays(7);
                    break;
                case TaskRepeatInterval.Month:
                    nextRun = nextRun.AddMonths(1);
                    break;
                case TaskRepeatInterval.WeekDay:
                    nextRun = nextRun.AddWeekDays(1);
                    break;
                default:
                    Log.WarnFormat("Unknown task repeat interval {0}", repeatInterval);
                    break;
            }
            return nextRun;
        }

        static string LogPrefix(ScheduledTask task)
        {
            if (task == null) return "";
            return string.Format("TaskID:{0} ", task.Id);
        }

        static string LogPrefix(ScheduledTaskDto task)
        {
            if (task == null) return "";
            return string.Format("TaskID:{0} ", task.Id);
        }
    }

    static class ScheduleTaskExtensions
    {
        static public string ToLogString(this ScheduledTask task)
        {
            return string.Format("TaskID:{0} Type:{1} TaskName:'{2}'", task.Id, task.TaskType, task.TaskName);
        }
    }
}