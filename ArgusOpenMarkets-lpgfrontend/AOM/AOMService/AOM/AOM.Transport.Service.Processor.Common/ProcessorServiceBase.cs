﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;
using Topshelf;

namespace AOM.Transport.Service.Processor.Common
{
    public abstract class ProcessorServiceBase : IServiceBase
    {
        private readonly IBus _aomBus;
        private readonly IEnumerable<IConsumer> _consumers;
        private readonly IServiceManagement _serviceManagement;

        protected ProcessorServiceBase(
            IEnumerable<IConsumer> consumers,
            IServiceManagement serviceManagement,
            IBus aomBus)
        {
            _consumers = consumers;
            _serviceManagement = serviceManagement;
            _aomBus = aomBus;
        }

        public virtual bool Start(HostControl hostControl)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Log.InfoFormat("Starting {0}", Name);

            _serviceManagement.OnStart();

            foreach (var consumer in _consumers)
            {
                try
                {
                    consumer.Start();
                    Log.Info("Consumer Started: " + consumer.GetType().Name);

                    var warmup = consumer as IConsumerWithInitialise;
                    if (warmup != null)
                    {
                        WarmUpConsumersHelper.RegisterInitialiseCall(_aomBus, warmup);
                    }

                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Unable to start consumer: {0}", consumer), ex);
                    throw;
                }
            }

            Log.InfoFormat("{0} started successfully", Name);
            return true;
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Error("Unhandled exception within processor " + Name, e.ExceptionObject as Exception);
        }

        public virtual bool Stop(HostControl hostControl)
        {
            Log.InfoFormat("Stopping {0}", Name);
            var t = Task.Run(() =>
            {
                Log.Debug("Disposing AOM Bus");
                try
                {
                    if (_aomBus != null)
                    {
                        _aomBus.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(Name + " AOM Bus Dispose exception", ex);
                }

                Log.Debug("Disposing Service Management");
                try
                {
                    _serviceManagement.OnStop();
                }
                catch (Exception ex)
                {
                    Log.Error(Name + " Service Management Stop exception", ex);
                }
            });
            

            if (!t.Wait(TimeSpan.FromSeconds(10)))
            {
                Log.ErrorFormat("{0}: timed out waiting for service to stop gracefully", this.Name);
            }

            Log.InfoFormat("{0} stopped", Name);
            return true;
        }

        public abstract string Name { get; }

        protected bool StopWithinTimeout(Action action, string description, int timeoutInSeconds = 10)
        {
            return StopWithinTimeout(action, description, TimeSpan.FromSeconds(timeoutInSeconds));
        }

        protected bool StopWithinTimeout(Action action, string description, TimeSpan timeout)
        {
            var timedOut = !ActionWithTimeout(action, timeout);
            if (timedOut)
            {
                Log.ErrorFormat("{0} timed out stopping {1}", Name, description);
            }
            return !timedOut;            
        }

        protected bool ActionWithTimeout(Action action, TimeSpan timeout)
        {
            var t = Task.Run(action);
            return t.Wait(timeout);
        }
    }

    public interface IServiceBase
    {
        bool Start(HostControl hostControl);
        bool Stop(HostControl hostControl);
    }
}
