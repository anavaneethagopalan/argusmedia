﻿using System.Collections.Generic;

namespace AOM.Transport.Service.Processor.Common.PerfmonBusImpl
{
    public interface IPerfmonBusConfiguration
    {
        long SlowProcessingWarningThresholdMsecs { get; }
        long SlowProcessingErrorThresholdMsecs { get; }
        long DelayInReportingToAllowForStartupMinutes { get; }
        List<string> AcceptedSlowMessageTypes { get; }
    }
}