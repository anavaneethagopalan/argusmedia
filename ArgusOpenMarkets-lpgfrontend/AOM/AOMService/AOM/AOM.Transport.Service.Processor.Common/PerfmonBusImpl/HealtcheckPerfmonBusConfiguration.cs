﻿using System.Collections.Generic;

namespace AOM.Transport.Service.Processor.Common.PerfmonBusImpl
{
    public class HealtcheckPerfmonBusConfiguration : IPerfmonBusConfiguration
    {
        private long _slowProcessingWarningThresholdMsecs = 2000;
        private long _slowProcessingErrorThresholdMsecs = 3000;

        public HealtcheckPerfmonBusConfiguration()
        {
            DelayInReportingToAllowForStartupMinutes = 2;
            AcceptedSlowMessageTypes = new List<string>
            {
                "*Refresh*",
                "*WarmUpRequest",
                "AomSendEmailRequest"
            };
        }

        public long SlowProcessingWarningThresholdMsecs
        {
            get
            {
                return _slowProcessingWarningThresholdMsecs;
            }
        }

        public long SlowProcessingErrorThresholdMsecs
        {
            get
            {
                return _slowProcessingErrorThresholdMsecs;
            }
        }

        public long DelayInReportingToAllowForStartupMinutes { get; set; }
        public List<string> AcceptedSlowMessageTypes { get; set; }
    }
}