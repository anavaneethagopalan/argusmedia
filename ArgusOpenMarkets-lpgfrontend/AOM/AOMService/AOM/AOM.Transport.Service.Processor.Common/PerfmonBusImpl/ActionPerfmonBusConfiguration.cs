﻿using System.Collections.Generic;

namespace AOM.Transport.Service.Processor.Common.PerfmonBusImpl
{
    public class ActionPerfmonBusConfiguration : IPerfmonBusConfiguration
    {
        public ActionPerfmonBusConfiguration()
        {
            SlowProcessingWarningThresholdMsecs = 1000;
            SlowProcessingErrorThresholdMsecs = 2000;
            DelayInReportingToAllowForStartupMinutes = 2;
            AcceptedSlowMessageTypes = new List<string>
            {
                "*Refresh*",
                "*WarmUpRequest",
                "AomSendEmailRequest"
            };
        }

        public long SlowProcessingWarningThresholdMsecs { get; set; }
        public long SlowProcessingErrorThresholdMsecs { get; set; }
        public long DelayInReportingToAllowForStartupMinutes { get; set; }
        public List<string> AcceptedSlowMessageTypes { get; set; } 
    }
}