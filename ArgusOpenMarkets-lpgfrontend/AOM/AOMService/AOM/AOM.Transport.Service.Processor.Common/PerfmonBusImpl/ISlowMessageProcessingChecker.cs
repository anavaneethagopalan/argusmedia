﻿
namespace AOM.Transport.Service.Processor.Common.PerfmonBusImpl
{
    public interface ISlowMessageProcessingChecker
    {
        bool ShouldWarn<T>(long elapsedMilliseconds);
        bool ShouldError<T>(long elapsedMilliseconds);
    }
}
