using System;
using System.Linq;
using AOM.App.Domain.Dates;
using Utils.Javascript.Extension;

namespace AOM.Transport.Service.Processor.Common.PerfmonBusImpl
{
    public class SlowMessageProcessingChecker : ISlowMessageProcessingChecker
    {
        private readonly IPerfmonBusConfiguration _config;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly DateTime _initTime;

        public SlowMessageProcessingChecker(IPerfmonBusConfiguration config,
                                            IDateTimeProvider dateTimeProvider)
        {
            if (config == null) throw new ArgumentNullException("config");
            if (dateTimeProvider == null) throw new ArgumentNullException("dateTimeProvider");
            _config = config;
            _dateTimeProvider = dateTimeProvider;
            _initTime = _dateTimeProvider.UtcNow;
        }

        public bool ShouldWarn<T>(long elapsedMilliseconds)
        {
            if (IsWithinStartupDelay() || IsAcceptedSlowMessageType<T>())
            {
                return false;
            }
            return IsGreaterThanWarningThreshold<T>(elapsedMilliseconds);
        }

        public bool ShouldError<T>(long elapsedMilliseconds)
        {
            if (IsWithinStartupDelay() || IsAcceptedSlowMessageType<T>())
            {
                return false;
            }
            return IsGreaterThanErrorThreshold<T>(elapsedMilliseconds);
        }

        private bool IsWithinStartupDelay()
        {
            return _dateTimeProvider.UtcNow.Subtract(_initTime).TotalMinutes <=
                   _config.DelayInReportingToAllowForStartupMinutes;
        }

        private bool IsAcceptedSlowMessageType<T>()
        {
            var typeName = (typeof(T)).Name;
            return _config.AcceptedSlowMessageTypes.Any(s => typeName.Like(s));
        }

        private bool IsGreaterThanWarningThreshold<T>(long elapsedMilliseconds)
        {
            return elapsedMilliseconds > _config.SlowProcessingWarningThresholdMsecs;
        }

        private bool IsGreaterThanErrorThreshold<T>(long elapsedMilliseconds)
        {
            return elapsedMilliseconds > _config.SlowProcessingErrorThresholdMsecs;            
        }
    }
}