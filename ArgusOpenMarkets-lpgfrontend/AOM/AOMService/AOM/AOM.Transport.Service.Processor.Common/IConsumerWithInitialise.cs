﻿namespace AOM.Transport.Service.Processor.Common
{
    public interface IConsumerWithInitialise : IConsumer
    {
        void InitialiseAndWarmupConsumer();
    }
}
