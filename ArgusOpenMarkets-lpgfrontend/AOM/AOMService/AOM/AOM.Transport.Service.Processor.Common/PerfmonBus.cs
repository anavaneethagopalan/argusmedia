using AOM.Transport.Service.Processor.Common.PerfmonBusImpl;
using ServiceStack.Text;
using Argus.Transport.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.Processor.Common
{
    internal class PerfmonBus : IBus
    {
        private readonly IBus _argusbus;
        private readonly ISlowMessageProcessingChecker _slowMessageProcessingChecker;

        private readonly ConcurrentDictionary<string, PerformanceCounter> _timingCounters = new ConcurrentDictionary<string, PerformanceCounter>();

        private readonly ConcurrentDictionary<string, PerformanceCounter> _frequencyCounters = new ConcurrentDictionary<string, PerformanceCounter>();

        public PerfmonBus(IBus argusbus, ISlowMessageProcessingChecker slowMessageProcessingChecker)
        {
            if (argusbus == null) throw new ArgumentNullException("argusbus");
            if (slowMessageProcessingChecker == null) throw new ArgumentNullException("slowMessageProcessingChecker");
            _argusbus = argusbus;
            _slowMessageProcessingChecker = slowMessageProcessingChecker;
        }

        public void Dispose()
        {
            _argusbus.Dispose();
        }

        public void Publish<T>(T message) where T : class
        {
            _argusbus.Publish(message);
        }

        public void Publish<T>(T message, string topic) where T : class
        {
            _argusbus.Publish(message, topic);
        }

        public Task PublishAsync<T>(T message) where T : class
        {
            return _argusbus.PublishAsync(message);
        }

        public Task PublishAsync<T>(T message, string topic) where T : class
        {
            return _argusbus.PublishAsync(message, topic);
        }

        public IDisposable Subscribe<T>(string subscriptionId, Action<T> onMessage) where T : class
        {
            return _argusbus.Subscribe(subscriptionId, CreateActionWrapper(onMessage, subscriptionId, null));
        }

        public IDisposable SubscribeAsync<T>(string subscriptionId, Func<T, Task> onMessage) where T : class
        {
            return _argusbus.SubscribeAsync(subscriptionId, onMessage);
        }

        public TResponse Request<TRequest, TResponse>(TRequest request) where TRequest : class where TResponse : class
        {
            return _argusbus.Request<TRequest, TResponse>(request);
        }

        public Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest request) where TRequest : class
            where TResponse : class
        {
            return _argusbus.RequestAsync<TRequest, TResponse>(request);
        }

        public void Respond<TRequest, TResponse>(Func<TRequest, TResponse> responder) where TRequest : class
            where TResponse : class
        {
            _argusbus.Respond(CreateReponseWrapper(responder));
        }

        public void RespondAsync<TRequest, TResponse>(Func<TRequest, Task<TResponse>> responder) where TRequest : class
            where TResponse : class
        {
            _argusbus.RespondAsync(responder);
        }

        private Func<TRequest, TResponse> CreateReponseWrapper<TRequest, TResponse>(Func<TRequest, TResponse> responder)
        {
            var instanceName = BuildInstanceName<TRequest>("", "");
            AllocationPerformanceCounters(instanceName);

            return (request =>
            {
                var clock = Stopwatch.StartNew();
                var response = responder.Invoke(request);

                if (_timingCounters.ContainsKey(instanceName))
                {
                    _timingCounters[instanceName].RawValue = clock.ElapsedMilliseconds;
                }

                if (_frequencyCounters.ContainsKey(instanceName))
                {
                    _frequencyCounters[instanceName].Increment();
                }

                clock.Stop();
                return response;
            });
        }

        private Action<T> CreateActionWrapper<T>(Action<T> actionToWrap, string subscriptionId, string topicId)
        {
            var instanceName = BuildInstanceName<T>(subscriptionId, topicId);
            AllocationPerformanceCounters(instanceName);

            return msg =>
            {
                var clock = Stopwatch.StartNew();
                actionToWrap(msg);

                if (_timingCounters != null)
                {
                    if (_timingCounters.ContainsKey(instanceName))
                    {
                        try
                        {
                            _timingCounters[instanceName].RawValue = clock.ElapsedMilliseconds;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(
                                "Error PerfmonBus.CreateActionWrapper - Updating an entry for the TimingCounters",
                                ex);
                        }

                    }
                }

                if (_frequencyCounters != null)
                {
                    if (_frequencyCounters.ContainsKey(instanceName))
                    {
                        try
                        {
                            _frequencyCounters[instanceName].Increment();
                        }
                        catch (Exception ex)
                        {
                            Log.Error(
                                "Error PerfmonBus.CreateActionWrapper - Updating an entry for the FrequencyCounters",
                                ex);
                        }
                    }
                }

                clock.Stop();

                LogIfSlowProcessing(clock.ElapsedMilliseconds, instanceName, msg);
            };
        }

        private delegate void LogAction(string s, params object[] args);

        private void LogIfSlowProcessing<T>(long elapsedMilliseconds, string instanceName, T msg)
        {
            const string messageFormatString = "Message processing is too slow {0}msecs for {1}. Message={2}";
            if (_slowMessageProcessingChecker.ShouldError<T>(elapsedMilliseconds))
            {
                Log.ErrorFormat(messageFormatString, elapsedMilliseconds, instanceName, msg.ToJson());
            }
            else if (_slowMessageProcessingChecker.ShouldWarn<T>(elapsedMilliseconds))
            {
                Log.WarnFormat(messageFormatString, elapsedMilliseconds, instanceName, msg.ToJson());
            }
        }

        private string BuildInstanceName<T>(string subscriptionId, string topicId)
        {
            var entryAssembly = Assembly.GetEntryAssembly();
            string assemblyName = entryAssembly != null ? entryAssembly.GetName().Name : "";
            if (assemblyName.LastIndexOf('.') > 0)
            {
                assemblyName = assemblyName.Substring(assemblyName.LastIndexOf('.') + 1);
            }
            string name = assemblyName + "." + typeof(T).Name;
            if (!String.IsNullOrEmpty(subscriptionId)) name += string.Format(" [S={0}]", subscriptionId);
            if (!String.IsNullOrEmpty(topicId)) name += string.Format(" [T={0}]", topicId);
            return name;
        }

        private static string PerfmonCategoryName
        {
            get
            {
                return "AOM";
            }
        }

        private static string PerfmonTimingCounterName
        {
            get
            {
                return "EasynetQ Subscribe timing/msecs";
            }
        }

        private static string PerfmonFreqCounterName
        {
            get
            {
                return "EasynetQ Subscribe msg/secs";
            }
        }

        private static string Version
        {
            get
            {
                return "v0.1";
            }
        }

        private void AllocationPerformanceCounters(string instanceName)
        {
            try
            {
                AllocateCounterIfExists(_timingCounters, PerfmonCategoryName, PerfmonTimingCounterName, instanceName);
                AllocateCounterIfExists(_frequencyCounters, PerfmonCategoryName, PerfmonFreqCounterName, instanceName);
            }
            catch (Exception e)
            {
                Log.Error("Failed to register performance counters ", e);
            }
        }

        private void AllocateCounterIfExists(
            ConcurrentDictionary<string, PerformanceCounter> counters,
            string categoryName,
            string counterName,
            string instanceName)
        {
            if (PerformanceCounterCategory.Exists(categoryName))
            {
                counters.GetOrAdd(instanceName, new PerformanceCounter(categoryName, counterName, instanceName, false));
            }
        }

        public static void RegisterPerformanceCountersWithWindows()
        {
            if (PerformanceCounterCategory.Exists(PerfmonCategoryName))
            {
                var existingCategory = new PerformanceCounterCategory(PerfmonCategoryName);

                if (!existingCategory.CategoryHelp.EndsWith(Version))
                {
                    PerformanceCounterCategory.Delete(PerfmonCategoryName);
                }
                else
                {
                    return;
                }
            }

            var mycounters = new CounterCreationDataCollection();
            var c1 = new CounterCreationData
                     {
                         CounterName = PerfmonTimingCounterName,
                         CounterType = PerformanceCounterType.NumberOfItems32,
                         CounterHelp = "Time taken for message subscribe processing"
                     };
            mycounters.Add(c1);

            var c2 = new CounterCreationData
                     {
                         CounterName = PerfmonFreqCounterName,
                         CounterType = PerformanceCounterType.RateOfCountsPerSecond32,
                         CounterHelp = "Message rate"
                     };
            mycounters.Add(c2);

            PerformanceCounterCategory.Create(
                PerfmonCategoryName,
                "AOM Performance Counters " + Version,
                PerformanceCounterCategoryType.MultiInstance,
                mycounters);

            Log.Info(
                string.Format("Created {0} performance counters under '{1}'", mycounters.Count, PerfmonCategoryName));
        }

        // NTB - 27th July 2015 - Upgrading to new Version of Argus.transport - new interface to implement:


//        public IDisposable Subscribe<T>(string subscriptionId, Action<T> onMessage, string topic) where T : class
//        {
//
//            return _argusbus.Subscribe(subscriptionId, CreateActionWrapper(onMessage, subscriptionId, topic), topic);
//        }

        public IDisposable Subscribe<T>(
            string subscriptionId,
            Action<T> onMessage,
            Action<ISubscriptionConfiguration> configure) where T : class
        {
            return _argusbus.Subscribe(subscriptionId, CreateActionWrapper(onMessage, subscriptionId,  string.Empty), configure);
        }


//        public IDisposable SubscribeAsync<T>(string subscriptionId, Func<T, Task> onMessage, string topic) where T : class
//        {
//            return _argusbus.SubscribeAsync(subscriptionId, onMessage, topic);
//        }

        public IDisposable SubscribeAsync<T>(
            string subscriptionId,
            Func<T, Task> onMessage,
            Action<ISubscriptionConfiguration> configure) where T : class
        {
            return _argusbus.SubscribeAsync(subscriptionId, onMessage);
        }

        public void Publish<T>(T message, TimeSpan delay) where T : class
        {
            throw new NotImplementedException();
        }

        public Task PublishAsync<T>(T message, TimeSpan delay) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Subscribe<T>(
            string subscriptionId,
            TimeSpan messageDelay,
            Action<T> onMessage,
            Action<ISubscriptionConfiguration> configure) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable Subscribe<T>(string subscriptionId, TimeSpan messageDelay, Action<T> onMessage)
            where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable SubscribeAsync<T>(
            string subscriptionId,
            TimeSpan messageDelay,
            Func<T, Task> onMessage,
            Action<ISubscriptionConfiguration> configure) where T : class
        {
            throw new NotImplementedException();
        }

        public IDisposable SubscribeAsync<T>(string subscriptionId, TimeSpan messageDelay, Func<T, Task> onMessage)
            where T : class
        {
            throw new NotImplementedException();
        }
    }
}
