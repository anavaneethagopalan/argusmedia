﻿namespace AOM.Transport.Service.Processor.Common
{
    using System.Diagnostics;
    using System.Reflection;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public class HealthCheckConsumer : HealthCheckConsumerBase
    {
        private readonly string _processorFullName;

        private readonly string _processorName;

        public HealthCheckConsumer(IBus bus)
            : base(bus)
        {
            var timer = Stopwatch.StartNew();
            var entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly != null)
            {
                _processorFullName = entryAssembly.FullName;
                _processorName = entryAssembly.GetName().Name;
            }

            Log.Info(
                string.Format("HealthCheckConsumer Ctor - GetCallingAssembly took: {0} ms for ProcessorFullName:{1}   ProcessorName:{2}", timer.ElapsedMilliseconds, _processorName, _processorName));
        }

        public override string ProcessorName
        {
            get
            {
                return _processorName;
            }
        }

        public override string ProcessorFullName
        {
            get
            {
                return _processorFullName;
            }
        }
    }
}