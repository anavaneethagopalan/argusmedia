﻿namespace AOM.Transport.Service.Processor.Common
{
    public interface IConsumer
    {        
        void Start();
    }
}
