﻿namespace AOM.Transport.Service.Processor.Common
{
    using System;
    using System.Diagnostics;

    using log4net;

    using Ninject;

    using Topshelf;
    using Topshelf.HostConfigurators;

    using Utils.Logging.Utils;

    public class ServiceHelper
    {
        private const int RestartServiceMinutesFirst = 1;

        private const int RestartServiceMinutesSecond = 1;

        private const int RestartServiceMinutesSubsequent = 1;

        private const int ServiceResetPeriodDays = 1;

        public static void ConfigureDefaultAomServiceParameters(HostConfigurator x)
        {
            //Unsure how to test these calls as there actually extension methods for HostConfigurator
            x.StartAutomaticallyDelayed();
            x.RunAsLocalSystem();
            x.EnableServiceRecovery(
                rc =>
                    rc.RestartService(RestartServiceMinutesFirst)
                        .RestartService(RestartServiceMinutesSecond)
                        .RestartService(RestartServiceMinutesSubsequent)
                        .SetResetPeriod(ServiceResetPeriodDays));
        }

        public static void BuildAndRunService<T>(StandardKernel kernel) where T : class, IServiceBase
        {
            var serviceInformation = kernel.Get<IServiceInformation>();
            HostFactory.Run(
                hostConfigurator =>
                {
                    hostConfigurator.Service<T>(
                        serviceConfigurator =>
                        {
                            serviceConfigurator.ConstructUsing(name => kernel.Get<T>());
                            serviceConfigurator.WhenStarted((s, hostControl) => s.Start(hostControl));
                            serviceConfigurator.WhenStopped((s, hostControl) => s.Stop(hostControl));
                            serviceConfigurator.AfterStoppingService(StopLogging);
                        });
                    // hostConfigurator.EnableShutdown();
                    hostConfigurator.SetDisplayName(serviceInformation.DisplayName);
                    hostConfigurator.SetServiceName(serviceInformation.ServiceName.Replace(" ", string.Empty));
                    hostConfigurator.SetDescription(serviceInformation.Description);
                    hostConfigurator.AfterInstall(CreateAnyPerfMonEntries);

                    ConfigureDefaultAomServiceParameters(hostConfigurator);

                    Debug.Assert(serviceInformation.DisplayName.StartsWith("AOM "));
                });
        }

        public static void CreateAnyPerfMonEntries()
        {
            try
            {
                PerfmonBus.RegisterPerformanceCountersWithWindows();
            }
            catch (Exception e)
            {
                Log.Error("Exception with CreateAnyPerfMonEntries", e);
            }
        }

        private static void StopLogging(HostStoppedContext ctx)
        {
            LogManager.Shutdown();
        }
    }
}