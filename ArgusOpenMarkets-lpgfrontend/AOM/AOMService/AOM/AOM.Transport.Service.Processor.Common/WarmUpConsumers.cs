﻿namespace AOM.Transport.Service.Processor.Common
{
    using System;
    using System.Diagnostics;

    using AOM.Transport.Events.System;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    internal static class WarmUpConsumersHelper
    {
        public static void RegisterInitialiseCall(IBus bus, IConsumerWithInitialise consumerToCall)
        {
            //we use topics so every consumer doesn't pick up theute warmup message
            string topic = consumerToCall.GetType().Name;

            //the consumer may be running on multiple machines so we set a subscriber ID specific to this machine
            var subscriptionId = Environment.MachineName;
            bus.Subscribe<WarmUpRequest>(subscriptionId, x => CallConsumer(consumerToCall));
            bus.Publish(new WarmUpRequest(), topic);
        }

        private static void CallConsumer(IConsumerWithInitialise consumerToCall)
        {
            Log.Debug(String.Format("Calling {0} Warmup", consumerToCall.GetType().Name));
            var time = Stopwatch.StartNew();
            consumerToCall.InitialiseAndWarmupConsumer();
            time.Stop();
            Log.Info(
                String.Format("{0} Warmup took {1} msecs", consumerToCall.GetType().Name, time.ElapsedMilliseconds));
        }
    }
}