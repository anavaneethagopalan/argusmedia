﻿namespace AOM.Transport.Service.Processor.Common
{
    public interface IServiceManagement
    {
        void OnStart();
        void OnStop();
    }
}