﻿namespace AOM.Transport.Service.Processor.Common
{
    public interface IServiceInformation
    {
        string ServiceName { get; }
        string DisplayName { get; }
        string Description { get; }
    }
}