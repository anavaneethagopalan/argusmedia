﻿using System;
using System.Collections.Generic;

namespace AOM.Transport.Service.Processor.Common
{
    public interface ISubscriptions
    {
        void Add(IDisposable subscription);
        void AddRange(IEnumerable<IDisposable> subscriptions);
        void CancelAll();
    }
}
