﻿using System;
using AOM.Transport.Events;
using Argus.Transport.Infrastructure;
using log4net.Core;
using log4net.Repository.Hierarchy;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.Processor.Common
{
    public static class PubSubBusExtensions
    {
        public static void PublishErrorResponseAndLogError<TResponse>(this IPubSubBus bus, MessageAction messageAction, ClientSessionInfo csi, string errorMessage, string topicOverride = null)
            where TResponse : AomResponse, new()
        {
            bus.PublishErrorResponse<TResponse>(messageAction, csi, errorMessage, topicOverride, Level.Error);
        }

        public static void PublishErrorResponseAndLogWarning<TResponse>(this IPubSubBus bus, MessageAction messageAction, ClientSessionInfo csi, string errorMessage, string topicOverride = null)
            where TResponse : AomResponse, new()
        {
            try
            {
                bus.PublishErrorResponse<TResponse>(messageAction, csi, errorMessage, topicOverride, Level.Warn);
            }
            catch (Exception ex)
            {
                Log.Error("PublishErrorResponseAndLogWarning", ex);
            }
        }

        public static void PublishErrorResponseAndLogInfo<TResponse>(this IPubSubBus bus, MessageAction messageAction, ClientSessionInfo csi, string errorMessage, string topicOverride = null)
            where TResponse : AomResponse, new()
        {
            try
            {
                bus.PublishErrorResponse<TResponse>(messageAction, csi, errorMessage, topicOverride, Level.Info);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("PublishErrorResponseAndLogInfo", ex);
            }
        }

        public static void PublishErrorResponseAndLogDebug<TResponse>(this IPubSubBus bus, MessageAction messageAction, ClientSessionInfo csi, string errorMessage, string topicOverride = null)
            where TResponse : AomResponse, new()
        {
            try
            {
                bus.PublishErrorResponse<TResponse>(messageAction, csi, errorMessage, topicOverride, Level.Debug);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("PublishErrorResponseAndLogDebug", ex);
            }
        }

        public static void PublishErrorResponse<TResponse>(this IPubSubBus bus, MessageAction messageAction, ClientSessionInfo csi, string errorMessage, string topicOverride = null, Level logMessageAtLevel = null)
            where TResponse : AomResponse, new()
        {
            try
            {
                errorMessage = errorMessage ?? "<error not specified>";
                if (logMessageAtLevel != null)
                {
                    Log.Write(logMessageAtLevel, errorMessage);
                }
                var externalDealErrorResponse = new TResponse
                {
                    Message = new Message
                    {
                        ClientSessionInfo = csi,
                        MessageAction = messageAction,
                        MessageType = MessageType.Error,
                        MessageBody = errorMessage
                    }
                };
                if (topicOverride != null)
                {
                    bus.Publish(externalDealErrorResponse, topic: topicOverride);
                }
                else
                {
                    bus.Publish(externalDealErrorResponse);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("PublishErrorResponse", ex);
            }
        }
    }
}