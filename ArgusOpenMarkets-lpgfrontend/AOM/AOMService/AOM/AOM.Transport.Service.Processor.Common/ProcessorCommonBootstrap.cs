﻿using AOM.App.Domain.Dates;
using AOM.Transport.Service.Processor.Common.PerfmonBusImpl;

namespace AOM.Transport.Service.Processor.Common
{
    using Argus.Transport.Configuration;
    using Argus.Transport.Infrastructure;

    using Ninject;
    using Ninject.Extensions.Conventions;
    using Ninject.Modules;

    public class ProcessorCommonBootstrap : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind(x => x.FromThisAssembly().SelectAllClasses().BindAllInterfaces());
            kernel.Bind<IDateTimeProvider>().To<DateTimeProvider>();

            var aomConnectionConfiguration = ConnectionConfiguration.FromConnectionStringName("aom.transport.bus");
            kernel.Bind<IBus>()
                  .ToMethod(
                      x => new PerfmonBus(
                            Argus.Transport.Transport.CreateBus(aomConnectionConfiguration,
                                                                null,
                                                                x.Kernel.Get<IBusLogger>()),
                            new SlowMessageProcessingChecker(new ActionPerfmonBusConfiguration(),
                                                             x.Kernel.Get<IDateTimeProvider>())))
                  .InSingletonScope();

            kernel.Bind<IBus>()
                  .ToMethod(
                      x => new PerfmonBus(
                            Argus.Transport.Transport.CreateBus(aomConnectionConfiguration,
                                                                null,
                                                                x.Kernel.Get<IBusLogger>()),
                            new SlowMessageProcessingChecker(new HealtcheckPerfmonBusConfiguration(),
                                                             x.Kernel.Get<IDateTimeProvider>())))
                  .WhenInjectedInto<HealthCheckConsumer>()
                  .InSingletonScope();
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
    
}