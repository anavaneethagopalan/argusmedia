﻿using System;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.Processor.Common.ArgusBus
{
    public sealed class BusLogger : IBusLogger
    {
        public void DebugWrite(string format, params object[] args)
        {
            //SafeWriteToConsole(format, args);
        }

        public void InfoWrite(string format, params object[] args)
        {
            Log.InfoFormat(format, args);
        }

        public void ErrorWrite(string format, params object[] args)
        {
            Log.ErrorFormat(format, args);
        }

        public void ErrorWrite(Exception exception)
        {
            Log.Error("[ArgusBus Exception]", exception);
        }
    }
}