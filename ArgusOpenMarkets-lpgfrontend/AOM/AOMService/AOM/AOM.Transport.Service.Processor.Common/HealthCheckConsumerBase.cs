﻿namespace AOM.Transport.Service.Processor.Common
{
    using AOM.Transport.Events.HealthCheck;

    using Argus.Transport.Infrastructure;

    using System;

    using Utils.Logging.Utils;

    public abstract class HealthCheckConsumerBase : IConsumer
    {
        private readonly IBus _aomBus;

        private string _environmentMachineName;

        protected HealthCheckConsumerBase(IBus aomBus)
        {
            _aomBus = aomBus;
        }

        public string EnvironmentMachineName
        {
            get
            {
                return _environmentMachineName = String.IsNullOrEmpty(_environmentMachineName) ? Environment.MachineName : _environmentMachineName;
            }
        }

        public abstract string ProcessorName { get; }

        public abstract string ProcessorFullName { get; }

        public void Start()
        {
            var processorName = ProcessorFullName;

            if (processorName.ToLower().Contains("aom.transport.service"))
            {
                _aomBus.Subscribe<ProcessorHealthCheck>(SubscriptionId, CheckHealthStatus);
            }
            else
            {
                Log.InfoFormat(
                    "We are not subscribing this class to the processor health check.  Processor - {0}",
                    processorName);
            }
        }

        private void CheckHealthStatus(ProcessorHealthCheck processorHealthCheck)
        {
            var status = new ProcessorHealthCheckStatus
                         {
                             Processor = ProcessorName,
                             ProcessorFullName = ProcessorFullName,
                             MachineName = EnvironmentMachineName,
                             Status = true
                         };
            _aomBus.Publish(status);
        }


        public string SubscriptionId
        {
            get
            {
                return ProcessorName;
            }
        }
    }
}
