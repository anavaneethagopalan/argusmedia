﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.ErrorService;
using AOM.Services.MarketInfoService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketInfoProcessor.Consumers
{
    internal class MarketInfoConsumer : MarketInfoConsumerBase
    {
        public MarketInfoConsumer(IDbContextFactory dbFactory, IMarketInfoService marketInfoService, IUserService _userService, IBus aomBus, IErrorService errorService)
            : base(dbFactory, marketInfoService, _userService, aomBus, errorService)
        { }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomMarketInfoRequest>(SubscriptionId, ConsumeMarketInfoRequest<AomMarketInfoRequest, AomMarketInfoAuthenticationRequest>);
            _aomBus.Subscribe<AomMarketInfoAuthenticationResponse>(SubscriptionId, ConsumeMarketInfoResponse<AomMarketInfoAuthenticationResponse, AomMarketInfoResponse>);
        }

        internal override MarketInfo ConsumeMarketInfoResponseAdditionalProcessing<T>(IAomModel aomDb, T response, out UserContactDetails userContactDetails)
        {
            userContactDetails = null;

            switch (response.Message.MessageAction)
            {
                case MessageAction.Create:
                    Log.Info("ConsumeMarketInfoResponseAdditionalProcessing-Create: " + response.MarketInfo.Id);
                    return _marketInfoService.CreateMarketInfo(aomDb, response);

                case MessageAction.Request:
                    Log.Info("ConsumeMarketInfoResponseAdditionalProcessing-Request: " + response.MarketInfo.Id);
                    var mi = _marketInfoService.GetMarketInfoItemById(aomDb, response);
                    userContactDetails = _userService.GetContactDetails(mi.LastUpdatedUserId);
                    return mi;

                case MessageAction.Void:
                    Log.Info("ConsumeMarketInfoResponseAdditionalProcessing-Void:" + response.MarketInfo.Id);
                    return _marketInfoService.VoidMarketInfo(aomDb, response);

                case MessageAction.Update:
                    Log.Info("ConsumeMarketInfoResponseAdditionalProcessing-Update: " + response.MarketInfo.Id);
                    return _marketInfoService.EditMarketInfo(aomDb, response);

                case MessageAction.Verify:
                    Log.Info("ConsumeMarketInfoResponseAdditionalProcessing-Verify: " + response.MarketInfo.Id);
                    return _marketInfoService.VerifyMarketInfo(aomDb, response);
            }
            return null;
        }
    }
}