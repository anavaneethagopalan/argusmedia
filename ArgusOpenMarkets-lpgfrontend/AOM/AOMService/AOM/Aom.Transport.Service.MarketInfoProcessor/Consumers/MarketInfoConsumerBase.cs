﻿using System;

using AOM.Services.ErrorService;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Services;
using AOM.Repository.MySql;
using AOM.Repository.MySql.Aom;
using AOM.Services.MarketInfoService;
using AOM.Transport.Events;
using AOM.Transport.Events.MarketInfos;
using AOM.Transport.Service.Processor.Common;

using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketInfoProcessor.Consumers
{
    public abstract class MarketInfoConsumerBase : IConsumer
    {
        protected readonly IDbContextFactory _dbFactory;
        protected readonly IMarketInfoService _marketInfoService;
        protected readonly IUserService _userService;
        protected IBus _aomBus;
        private readonly IErrorService _errorService;

        protected MarketInfoConsumerBase(IDbContextFactory dbFactory, IMarketInfoService marketInfoService, IUserService userService, IBus aomBus, IErrorService errorService)
        {
            _dbFactory = dbFactory;
            _marketInfoService = marketInfoService;
            _aomBus = aomBus;
            _errorService = errorService;
            _userService = userService;
        }

        public string SubscriptionId { get { return "AOM"; } }

        public void Start()
        {
            SubscribeToEvents();
        }

        internal virtual void SubscribeToEvents()
        {
            throw new NotImplementedException("You must subscribe to the events to consume in this method");
        }

        internal void ConsumeMarketInfoRequest<TReq, TFwd>(TReq request) where TReq : AomMarketInfoRequest where TFwd : AomMarketInfoAuthenticationRequest, new()
        {
            string requestType = typeof(TReq).Name;

            Log.Debug(String.Format("{0} received for id: {1}", requestType, request.MarketInfo.Id));
            try
            {
                var forwardedRequest = new TFwd
                {
                    MessageAction = request.MessageAction,
                    ClientSessionInfo = request.ClientSessionInfo,
                    MarketInfo = request.MarketInfo
                };
                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                var logError =
                    _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = String.Format("Authenticating {0}", requestType),
                        ErrorText = string.Empty,
                        Source = "MarketInfoConsumerBase"
                    });

                _aomBus.PublishErrorResponse<AomMarketInfoResponse>(MessageAction.Update,  request.ClientSessionInfo, logError);
            }
        }

        internal void ConsumeMarketInfoResponse<TResp, TFwd>(TResp response) where TResp : AomMarketInfoAuthenticationResponse where TFwd : AomMarketInfoResponse, new()
        {
            string responseType = typeof(TResp).Name;

            Log.Debug(String.Format("{0} received for id: {1}", responseType, response.MarketInfo.Id));
            try
            {
                if (response.Message.MessageType == MessageType.Error)
                {
                    _aomBus.PublishErrorResponseAndLogError<AomMarketInfoResponse>(response.Message.MessageAction,  response.Message.ClientSessionInfo, response.Message.MessageBody as string);
                }
                else
                {
                    using (var aomDb = _dbFactory.CreateAomModel())
                    {
                        UserContactDetails returnedUserContactDetails;
                        var returnedMarketInfo = ConsumeMarketInfoResponseAdditionalProcessing(aomDb,  response, out returnedUserContactDetails);
                        try
                        {
                            var forwardedResponse = new TFwd
                            {
                                MarketInfo = returnedMarketInfo,
                                UserContactDetails = returnedUserContactDetails,
                                Message = response.Message
                            };
                            _aomBus.Publish(forwardedResponse);
                        }
                        catch (Exception exception)
                        {
                            _errorService.LogUserError(new UserError
                            {
                                Error = exception,
                                AdditionalInformation = String.Format("{0} error", responseType),
                                ErrorText = string.Empty,
                                Source = "MarketInfoConsumerBase"
                            });
                        }
                    }
                }
            }
            catch (BusinessRuleException exception)
            {
                _aomBus.PublishErrorResponseAndLogInfo<AomMarketInfoResponse>(response.Message.MessageAction, response.Message.ClientSessionInfo, exception.Message);
            }
            catch (Exception exception)
            {
                var logError =
                    _errorService.LogUserError(new UserError
                    {
                        Error = exception,
                        AdditionalInformation = String.Format("{0} error", responseType),
                        ErrorText = string.Empty,
                        Source = "MarketInfoConsumerBase"
                    });

                _aomBus.PublishErrorResponse<AomMarketInfoResponse>(response.Message.MessageAction,  response.Message.ClientSessionInfo, logError);
            }
        }

        private UserContactDetails AddContactDetails(AomMarketInfoAuthenticationResponse response)
        {
            return response.Message.MessageAction == MessageAction.Request ? _userService.GetContactDetails(response.MarketInfo.LastUpdatedUserId) : null;
        }

        internal virtual MarketInfo ConsumeMarketInfoResponseAdditionalProcessing<T>(IAomModel aomDb, T response,
            out UserContactDetails returnedUserContactDetails) where T : AomMarketInfoAuthenticationResponse
        {
            throw new NotImplementedException("You must add code in here if consuming responses. This is only called if the request being consumed is not indicating an error.");
        }
    }
}