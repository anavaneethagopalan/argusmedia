﻿using System;
using System.Collections.Generic;
using System.Linq;

using AOM.App.Domain.Entities;
using AOM.Transport.Events;
using Newtonsoft.Json;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.MarketInfoProcessor.Consumers
{
    using Processor.Common;
    using App.Domain.Dates;
    using Repository.MySql;
    using Events.MarketInfos;
    using Argus.Transport.Infrastructure;

    internal class MarketInfoScheduledMessageConsumer : ScheduledTaskConsumerBase<AomScheduledMarketInfoRequest>
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        public string SubscriptionId { get { return "AOM"; } }

        public MarketInfoScheduledMessageConsumer(IBus aomBus, IDateTimeProvider dateTimeProvider, IDbContextFactory dbContextFactory = null)
            : base(aomBus, dateTimeProvider, dbContextFactory)
        {
            _dateTimeProvider = dateTimeProvider;
        }

        public override void Start()
        {
            AomBus.Subscribe<AomScheduledMarketInfoRequest>(SubscriptionId, ConsumeScheduledMarketInfoRequest<AomScheduledMarketInfoRequest, AomMarketInfoRequest>);
            base.Start();
        }

        private void ConsumeScheduledMarketInfoRequest<TResp, TFwd>(TResp response) where TResp : AomScheduledMarketInfoRequest where TFwd : AomMarketInfoRequest, new()
        {
            var marketInfoRequest = new TFwd();

            var marketInfo = new MarketInfo();
            marketInfo.Info = response.MessageText;
            marketInfo.LastUpdated = _dateTimeProvider.UtcNow;
            marketInfo.LastUpdatedUserId = -1;
            marketInfo.MarketInfoStatus = MarketInfoStatus.Active;
            marketInfo.OwnerOrganisationId = 1;

            List<long> productIds = new List<long>();
            try
            {
                productIds= JsonConvert.DeserializeObject<long[]>(response.ProductChannels).ToList();
            }
            catch (Exception ex)
            {
                Log.Error("ConsumeScheduledMarketInfoRequest", ex);
            }
            marketInfo.ProductIds = productIds;
            marketInfoRequest.MarketInfo = marketInfo;
            marketInfoRequest.MessageAction = MessageAction.Create;
            marketInfoRequest.ClientSessionInfo = new ClientSessionInfo
            {
                UserId = -1
            };
            
            /*
            var info = JsonConvert.DeserializeObject<MarketInfo>(receivedMsg.MessageBody.ToString());
            requestToSend.MarketInfo = info;
            requestToSend.ClientSessionInfo = receivedMsg.ClientSessionInfo;
            requestToSend.MessageAction = receivedMsg.MessageAction;
            */
            if (productIds.Count > 0)
            {
                AomBus.Publish(marketInfoRequest);
            }
            else
            {
                Log.Info("NOT Publishing Market Info Request as NO product ids specified.");
            }
        }
    }
}
