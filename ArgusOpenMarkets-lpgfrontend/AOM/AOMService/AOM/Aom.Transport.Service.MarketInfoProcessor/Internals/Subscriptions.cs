﻿using System;
using System.Collections.Generic;
using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.MarketInfoProcessor.Internals
{
    internal class Subscriptions : ISubscriptions
    {
        private readonly List<IDisposable> _subscriptions;

        public Subscriptions()
        {
            _subscriptions = new List<IDisposable>();
        }

        public void Add(IDisposable subscription)
        {
            _subscriptions.Add(subscription);
        }

        public void AddRange(IEnumerable<IDisposable> subscriptions)
        {
            _subscriptions.AddRange(subscriptions);
        }

        public void CancelAll()
        {
            foreach (var subscription in _subscriptions)
            {
                subscription.Dispose();
            }

            _subscriptions.Clear();
        }
    }
}