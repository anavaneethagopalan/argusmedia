﻿using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.MarketInfoProcessor.Internals
{
    internal class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get
            {
                return DisplayName.Replace(" ", "");
            }
        }

        public string DisplayName
        {
            get
            {
                return "AOM Transport Service Market Ticker Info Processor";
            }
        }

        public string Description
        {
            get
            {
                return "AOM Transport Service Market Ticker Info Processor";
            }
        }
    }
}