﻿namespace AOM.Transport.Service.MarketInfoProcessor
{
    using AOM.Transport.Service.Processor.Common;
    using Argus.Transport.Infrastructure;
    using System.Collections.Generic;

    internal class MarketInfoProcessorService : ProcessorServiceBase
    {
        public MarketInfoProcessorService(
            IBus aomBus,
            IEnumerable<IConsumer> consumers,
            IServiceManagement serviceManagement)
            : base(consumers, serviceManagement, aomBus)
        {

        }

        public override string Name
        {
            get { return "MarketInfoProcessorService"; }
        }
    }
}