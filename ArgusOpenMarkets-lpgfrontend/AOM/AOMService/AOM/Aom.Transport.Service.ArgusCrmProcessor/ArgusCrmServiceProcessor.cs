﻿using System.Collections.Generic;
using AOM.Transport.Service.ArgusCrmProcessor.TimerService;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Topshelf;

namespace AOM.Transport.Service.ArgusCrmProcessor
{
    public class ArgusCrmServiceProcessor : ProcessorServiceBase
    {
        private readonly ITimerService _argusCrmTimerService;

        //how long to wait before we start the timer
        private const long TimerDueTime = 3000;

        public ArgusCrmServiceProcessor(
            IServiceManagement serviceManagement,
            IEnumerable<IConsumer> consumers,
            IBus aomBus,
            ITimerService argusCrmTimerService)
            : base(consumers, serviceManagement, aomBus)
        {
            _argusCrmTimerService = argusCrmTimerService;
        }

        public override bool Start(HostControl hostControl)
        {
            base.Start(hostControl);
            _argusCrmTimerService.StartTimer(TimerDueTime);
            return true;
        }

        public override bool Stop(HostControl hostControl)
        {
            StopWithinTimeout(() => _argusCrmTimerService.StopTimer(), "timer service");
            base.Stop(hostControl);
            return true;
        }

        public override string Name
        {
            get { return "ArgusCrmServiceProcessor"; }
        }
    }
}