﻿using System.Configuration;
using AOM.Repository.Oracle;
using AOM.Transport.Service.ArgusCrmProcessor.Internals;
using AOM.Transport.Service.ArgusCrmProcessor.TimerService;
using AOM.Transport.Service.Processor.Common;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Modules;

namespace AOM.Transport.Service.ArgusCrmProcessor
{
    public class LocalBootstrapper : NinjectModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<ISubscriptions>().To<Subscriptions>();
            kernel.Bind<IServiceManagement>().To<ServiceManagement>();
            kernel.Bind<IServiceInformation>().To<ServiceInformation>();
            kernel.Bind<ITimerService>().To<ArgusCrmTimerService>();

            kernel.Bind<IOracleReader>()
                .To<OracleReader>()
                .WithConstructorArgument(
                    "conn",
                    ConfigurationManager.ConnectionStrings["ArgusCrmConnectionString"].ConnectionString);

            kernel.Bind(
                x =>
                    x.FromThisAssembly()
                        .IncludingNonePublicTypes()
                        .SelectAllClasses()
                        .InheritedFrom<IConsumer>()
                        .BindAllInterfaces()
                        .Configure(b => b.InThreadScope()));
        }

        public override void Load()
        {
            Load(Kernel);
        }
    }
}