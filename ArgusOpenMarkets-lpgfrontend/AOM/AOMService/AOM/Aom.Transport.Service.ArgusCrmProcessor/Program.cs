﻿using AOM.Repository.MySql;
using AOM.Services;
using AOM.Transport.Service.Processor.Common;
using Ninject;

namespace AOM.Transport.Service.ArgusCrmProcessor
{
    public class Program
    {
        internal static void Main()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(new EFModelBootstrapper(), new LocalBootstrapper(), new ServiceBootstrap(), new ProcessorCommonBootstrap());
                ServiceHelper.BuildAndRunService<ArgusCrmServiceProcessor>(kernel);                                                                                     
            }
        }
    }
}