﻿using AOM.Transport.Events.ArgusCrm;
using Argus.Transport.Infrastructure;
using System;
using System.Configuration;
using System.Threading;

namespace AOM.Transport.Service.ArgusCrmProcessor.TimerService
{
    public interface ITimerService
    {
        void StartTimer(long dueTime);

        void StopTimer();
    }

    public class ArgusCrmTimerService : ITimerService
    {
        private readonly IBus _crmBus;

        private Timer _timer;

        public ArgusCrmTimerService(IBus crmBus)
        {
            _crmBus = crmBus;
        }

        public void StartTimer(long dueTime)
        {
            var period = (ConfigurationManager.AppSettings["RefreshIntervalMillis"] != null)
                ? Int32.Parse(ConfigurationManager.AppSettings["RefreshIntervalMillis"])
                : 3600000;

            _timer = new Timer(TimerTick, null, dueTime, period);

        }

        //on timer tick - connect to Argus oracle db and extract the current data from the view
        private void TimerTick(object data)
        {
            _crmBus.Publish(new ArgusCrmRefreshRequest());
        }

        public void StopTimer()
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
        }

    }
}