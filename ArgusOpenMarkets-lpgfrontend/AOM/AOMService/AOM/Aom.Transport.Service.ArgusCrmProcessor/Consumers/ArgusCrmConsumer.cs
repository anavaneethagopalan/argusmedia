﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.Services.CrmService;
using AOM.Transport.Events.ArgusCrm;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.ArgusCrmProcessor.Consumers
{
    public class ArgusCrmConsumer : IConsumer
    {
        private readonly IBus _crmBus;

        private IUserModuleService _userModuleService;

        private IArgusCrmConsumerService _crmConsumerService;

        private const string QueryString = "SELECT * from V_AOM_USER_MODULE";

        public ArgusCrmConsumer(IBus bus, IUserModuleService userModuleService,
            IArgusCrmConsumerService crmConsumerService)
        {
            _crmBus = bus;
            _userModuleService = userModuleService;
            _crmConsumerService = crmConsumerService;
        }

        public void Start()
        {
            _crmBus.Subscribe<ArgusCrmRefreshRequest>(SubscriptionId, ConsumeRefreshArgusCrmRequest);
        }

        internal void ConsumeRefreshArgusCrmRequest(ArgusCrmRefreshRequest request)
        {
            //get latest data from Argus oracle DB
            IEnumerable<AomUserModuleViewEntity> userModules = null;
            try
            {
                userModules = _crmConsumerService.GetLatestData(QueryString);
            }
            catch (Exception ex)
            {
                Log.Error(
                    "In ArgusCrmConsumer.ConsumeRefreshArgusCrmRequest(). Error while connecting or reading from Oracle db! Will try again after set interval. ",
                    ex);
                return;
            }
            if (userModules != null)
            {
                UpdateModules(userModules);
            }
        }

        private void UpdateModules(IEnumerable<AomUserModuleViewEntity> modules)
        {
            try
            {
                _userModuleService.UpdateModules(modules);
            }
            catch (Exception ex)
            {
                Log.Error("In ArgusCrmConsumer.UpdateModules. Error while trying to update Modules!", ex);
            }

            try
            {
                _userModuleService.UpdateUserInfo(modules);
            }
            catch (Exception ex)
            {
                Log.Error("In ArgusCrmConsumer.UpdateModules. Error while trying to update UserInfos!", ex);
            }

            try
            {
                _userModuleService.UpdateUserModules(modules);
            }
            catch (Exception ex)
            {
                Log.Error("In ArgusCrmConsumer.UpdateUserModules. Error while trying to update UserModules!", ex);
            }
        }

        public string SubscriptionId
        {
            get { return "AOM"; }
        }
    }
}