﻿using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class HealthCheckDiffusionTopicManagerTests
    {
        [Test]
        public void ShouldReturnTheRootTopicThatHealthcheckTopicManagerOwns()
        {
            var pdtm = new HealthCheckDiffusionTopicManager();
            Assert.That(pdtm.TopicRoot, Is.EqualTo("AOM/HealthCheck"));
        }
    }
}
