﻿using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class ProductsDiffusionTopicManagerTests
    {
        [Test]
        public void ShouldReturnTheRootTopicThatProductsTopicManagerOwns()
        {
            var pdtm = new ProductsDiffusionTopicManager();
            Assert.That(pdtm.TopicRoot, Is.EqualTo("AOM/Products"));
        }
    }
}
