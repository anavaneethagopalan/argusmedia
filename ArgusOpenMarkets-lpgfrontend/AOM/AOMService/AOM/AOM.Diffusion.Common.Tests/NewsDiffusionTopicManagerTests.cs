﻿using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class NewsDiffusionTopicManagerTests
    {
        [Test]
        public void ShouldReturnTheRootTopicThatHealthcheckTopicManagerOwns()
        {
            var pdtm = new NewsDiffusionTopicManager();
            Assert.That(pdtm.TopicRoot, Is.EqualTo("AOM/News"));
        }
    }
}
