﻿using AOMDiffusion.Common.Interfaces;
using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;
using PushTechnology.ClientInterface.Client.Session;
using ISession = PushTechnology.ClientInterface.Client.Session.ISession;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class DiffusionTopicManagerTests
    {
        private Mock<ISession> _mockSession;
        private Mock<ITopicUpdaterUpdateCallback> _mockTopicsFeature;
        
        private Mock<ISubscriptionControl> _mockSubscriptionControl;
        private Mock<ITopicUpdateControl> _mockTopicUpdateControl;
        private Mock<IMessagingControl> _mockMessagingControl;
        private Mock<ITopicControl> _mockTopicControl;
        private TestTopicManager _testTopicManager;

        [SetUp]
        public void Setup()
        {
            _testTopicManager = new TestTopicManager("AOM/DATA");

            _mockTopicsFeature = new Mock<ITopicUpdaterUpdateCallback>();
            _mockSubscriptionControl = new Mock<ISubscriptionControl>();
            _mockTopicUpdateControl = new Mock<ITopicUpdateControl>();
            _mockTopicControl = new Mock<ITopicControl>();
            _mockMessagingControl = new Mock<IMessagingControl>();

            _mockSession = new Mock<ISession>();
            _mockSession.Setup(m => m.GetSubscriptionControlFeature()).Returns(_mockSubscriptionControl.Object);
            _mockSession.Setup(m => m.GetTopicControlFeature()).Returns(_mockTopicControl.Object);
            _mockSession.Setup(m => m.GetTopicUpdateControlFeature()).Returns(_mockTopicUpdateControl.Object);
            _mockSession.Setup(m => m.GetMessagingControlFeature()).Returns(_mockMessagingControl.Object);
        }

        [Test]
        public void ShouldReturnTheRootTopic()
        {
            Assert.That(_testTopicManager.TopicRoot, Is.EqualTo("AOM/DATA"));
        }

        [Test]
        public void AddMessageHandlerShouldCallMessageControlAddMessageHandler()
        {
            SessionId sessionId = new SessionId();
            _mockSession.Setup(p => p.SessionId).Returns(sessionId);

            _testTopicManager.AttachToSessionAndRegisterTopicSource(_mockSession.Object);
            var mockMessageHandler = new Mock<IMessageHandler>();
            _testTopicManager.AddMessageHandler("AOM/DATA/AOM", mockMessageHandler.Object);

            _mockMessagingControl.Verify(m => m.AddMessageHandler("AOM/DATA/AOM", It.IsAny<IMessageHandler>()), Times.Once);
        }

        [Test]
        public void CallingDisposeShouldCloseTheSession()
        {
            SessionId sessionId = new SessionId();
            _mockSession.Setup(p => p.SessionId).Returns(sessionId);
            _testTopicManager.AttachToSessionAndRegisterTopicSource(_mockSession.Object);
            _testTopicManager.Dispose();
            _mockSession.Verify(m => m.Close(), Times.Once);
        }
    }

    public class TestTopicManager : AomDiffusionTopicManager
    {
        public bool GetUpdaterCalled { get; set; }

        private ISimpleTopicSourceHandler _topicSourceHandler = new SimpleTopicSourceHandler();

        public TestTopicManager(string topicRoot) : base(topicRoot)
        {
            GetUpdaterCalled = false;
        }

        public override ISimpleTopicSourceHandler GetUpdater()
        {
            GetUpdaterCalled = true;
            return _topicSourceHandler;
        }
    }
}