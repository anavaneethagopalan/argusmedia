﻿using AOMDiffusion.Common.Interfaces;
using Moq;
using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class DiffusionSessionConnectionTests
    {
        Mock<IDiffusionConfig> _mockDiffusionConfig;

        private Mock<IAomDiffusionSessionFactory> _mockAomDiffusionSessionFactory;

        private DiffusionSessionConnection _diffusionSessionConnection;

        [SetUp]
        public void Setup()
        {
            _mockDiffusionConfig = new Mock<IDiffusionConfig>();
            _mockDiffusionConfig.SetupGet(p => p.DiffusionEndPoint).Returns("dpt://1.1.1.1");
            _mockDiffusionConfig.SetupGet(p => p.DiffusionUsername).Returns("admin");
            _mockDiffusionConfig.SetupGet(p => p.DiffusionPassword).Returns("password");
            _mockDiffusionConfig.SetupGet(p => p.DiffusionHandlerName).Returns("handler");

            _mockAomDiffusionSessionFactory = new Mock<IAomDiffusionSessionFactory>();
            _diffusionSessionConnection = new DiffusionSessionConnection(_mockDiffusionConfig.Object, _mockAomDiffusionSessionFactory.Object);
        }

        [Test]
        public void ShouldInitializeDiffusionSessionConnection()
        {
            Assert.That(_diffusionSessionConnection, Is.Not.Null);
        }

        [Test]
        public void RunningShouldReturnFalseIfSessionNotStarted()
        {
            Assert.That(_diffusionSessionConnection.Running, Is.False);
        }

        public void ConnectAndStartSessionShouldReturnTrueIfConnectedAndStarted()
        {
            // NOTE: Cannot test this as Diffusion sealed all the components.  Too much work!
        }
    }
}