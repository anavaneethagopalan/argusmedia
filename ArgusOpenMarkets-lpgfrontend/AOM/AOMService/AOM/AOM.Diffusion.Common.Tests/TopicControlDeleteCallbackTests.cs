﻿using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class TopicControlDeleteCallbackTests
    {
        [Test]
        public void TopicRemovedWasCalled()
        {
            var topicRemoved = false;
            object eventDetails = null;
            TopicControlDeleteCallback topicControlDeleteCallback = new TopicControlDeleteCallback();

            topicControlDeleteCallback.TopicRemoved += (o, e) =>
            {
                topicRemoved = true;
                eventDetails = e;
            };
            topicControlDeleteCallback.OnTopicsRemoved();

            Assert.IsTrue(topicRemoved);
            Assert.AreEqual(eventDetails, "Topic Removed");
        }
    }
}
