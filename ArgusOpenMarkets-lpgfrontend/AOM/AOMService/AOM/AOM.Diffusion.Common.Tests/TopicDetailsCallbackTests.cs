﻿using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Topics;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class TopicDetailsCallbackTests
    {
        [Test]
        public void TopicDetailsWasCalled()
        {
            var topicDetails = false;
            object eventDetails = null;
            TopicDetailsCallback topicDetailsCallback = new TopicDetailsCallback();
            
            topicDetailsCallback.TopicDetails += (o, e) => { 
                topicDetails = true;
                eventDetails = e;
            };
            topicDetailsCallback.OnTopicDetails("AOM/HealthCheck", new Mock<ITopicDetails>().Object);

            Assert.IsTrue(topicDetails);
            Assert.AreEqual(eventDetails, "GotDetails");
        }

        [Test]
        public void TopicUnknownWasCalled()
        {
            var topicUnknonwn = false;
            object eventDetails = null;
            TopicDetailsCallback topicDetailsCallback = new TopicDetailsCallback();

            topicDetailsCallback.TopicDoesNotExist += (o, e) =>
            {
                topicUnknonwn = true;
                eventDetails = e;
            };
            topicDetailsCallback.OnTopicUnknown("AOM/HealthCheckUnknown");

            Assert.IsTrue(topicUnknonwn);
            Assert.AreEqual(eventDetails, "NoTopic");
        }
    }
}
