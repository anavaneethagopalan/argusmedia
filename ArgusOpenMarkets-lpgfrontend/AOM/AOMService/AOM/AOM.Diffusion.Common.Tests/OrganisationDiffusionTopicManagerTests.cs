﻿using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class OrganisationDiffusionTopicManagerTests
    {
        [Test]
        public void ShouldReturnTheRootTopicThatOrganisationTopicManagerOwns()
        {
            var pdtm = new OrganisationDiffusionTopicManager();
            Assert.That(pdtm.TopicRoot, Is.EqualTo("AOM/Organisations"));
        }
    }
}
