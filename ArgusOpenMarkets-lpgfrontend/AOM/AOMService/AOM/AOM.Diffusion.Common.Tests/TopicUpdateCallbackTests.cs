﻿using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class TopicUpdateCallbackTests
    {
        [Test]
        public void TopicUpdatedSuccessfully()
        {
            var topicUpdate = false;
            object eventDetails = null;
            TopicUpdateCallback topicUpdateCallback = new TopicUpdateCallback("AOM/HealthCheck", "New check");

            topicUpdateCallback.OnUpdate += (o, e) =>
            {
                topicUpdate = true;
                eventDetails = e;
            };

            topicUpdateCallback.OnSuccess();

            Assert.IsTrue(topicUpdate);
            StringAssert.Contains("New check", eventDetails.ToString());
        }

        [Test]
        public void TopicUpdateErrored()
        {
            var topicUpdate = false;
            object eventDetails = null;
            TopicUpdateCallback topicUpdateCallback = new TopicUpdateCallback("AOM/HealthCheck", "New check");

            topicUpdateCallback.OnTopicUpdateError += (o, e) =>
            {
                topicUpdate = true;
                eventDetails = e;
            };

            topicUpdateCallback.OnError(PushTechnology.ClientInterface.Client.Callbacks.ErrorReason.SESSION_CLOSED);

            Assert.IsTrue(topicUpdate);
            StringAssert.Contains("New check", eventDetails.ToString());
            StringAssert.Contains(PushTechnology.ClientInterface.Client.Callbacks.ErrorReason.SESSION_CLOSED.ToString(), eventDetails.ToString());
        }
    }
}
