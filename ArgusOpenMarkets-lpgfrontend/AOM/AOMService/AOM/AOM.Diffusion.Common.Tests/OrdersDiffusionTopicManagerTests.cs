﻿using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class OrdersDiffusionTopicManagerTests
    {
        [Test]
        public void ShouldReturnTheRootTopicThatOrderTopicManagerOwns()
        {
            var pdtm = new OrdersDiffusionTopicManager();
            Assert.That(pdtm.TopicRoot, Is.EqualTo("AOM/Orders"));
        }
    }
}