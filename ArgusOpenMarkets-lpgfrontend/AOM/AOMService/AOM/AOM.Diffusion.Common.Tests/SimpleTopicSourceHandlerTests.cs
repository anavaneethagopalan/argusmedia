﻿using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Factories;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class SimpleTopicSourceHandlerTests
    {
        private bool _topicActive;
        private object _eventDetails = null;
        private SimpleTopicSourceHandler _simpleTopicSourceHandler;

        [SetUp]
        public void Setup()
        {
            _topicActive = false;
            _eventDetails = null;
            _simpleTopicSourceHandler = new SimpleTopicSourceHandler();
        }

        [Test]
        public void SimpleTopicSourceHandlerActivates()
        {
            _simpleTopicSourceHandler.Active += (o, e) =>
            {
                _topicActive = true;
                _eventDetails = e;
            };

            _simpleTopicSourceHandler.OnActive("AOM/HealthCheck", new Mock<ITopicUpdater>().Object);

            Assert.IsTrue(_topicActive);
            Assert.AreEqual("Active", _eventDetails);
            Assert.AreEqual(_simpleTopicSourceHandler.State(), TopicSourceState.Active);
            Assert.IsTrue(_simpleTopicSourceHandler.IsActive());
        }

        [Test]
        public void SimpleTopicSourceHandlerCloses()
        {

            _simpleTopicSourceHandler.Closed += (o, e) =>
            {
                _topicActive = true;
                _eventDetails = e;
            };

            _simpleTopicSourceHandler.OnClose("AOM/HealthCheck");

            Assert.IsTrue(_topicActive);
            Assert.AreEqual("Closed", _eventDetails);
            Assert.AreEqual(_simpleTopicSourceHandler.State(), TopicSourceState.Closed);
        }

        [Test]
        public void SimpleTopicSourceHandlerStandsBy()
        {
            _simpleTopicSourceHandler.StandBy += (o, e) =>
            {
                _topicActive = true;
                _eventDetails = e;
            };

            _simpleTopicSourceHandler.OnStandby("AOM/HealthCheck");

            Assert.IsTrue(_topicActive);
            Assert.AreEqual("StandBy", _eventDetails);
            Assert.AreEqual(_simpleTopicSourceHandler.State(), TopicSourceState.StandBy);
        }

        [Test]
        public void SimpleTopicSourceHandlerPushToTopicUpdates()
        {
            var topicUpdaterUpdateCallback = new Mock<ITopicUpdaterUpdateCallback>();

            _simpleTopicSourceHandler.Active += (o, e) =>
            {
                _topicActive = true;
                _eventDetails = e;
            };

            _simpleTopicSourceHandler.OnActive("AOM/HealthCheck", new Mock<ITopicUpdater>().Object);

            var jsonData = Diffusion.DataTypes.JSON.FromJSONString("");
            _simpleTopicSourceHandler.PushToTopic("AOM/HealthCheck", jsonData, false, topicUpdaterUpdateCallback.Object);

            Assert.IsTrue(_topicActive);
        }

        [Test]
        public void SimpleTopicSourceHandlerIsActiveShouldReturnTrueIfActive()
        {
            _simpleTopicSourceHandler.Active += (o, e) =>
            {
                _topicActive = true;
                _eventDetails = e;
            };

            _simpleTopicSourceHandler.OnActive("AOM/HealthCheck", new Mock<ITopicUpdater>().Object);
            var isActive = _simpleTopicSourceHandler.IsActive();

            Assert.That(isActive, Is.True);
        }

        [Test]
        public void SimpleTopicSourceHandlerIsActiveShouldFalseIfNotActive()
        {
            _simpleTopicSourceHandler.StandBy += (sender, s) =>
            {

            };

            _simpleTopicSourceHandler.OnStandby("AOM/HealthCheck");
            var isActive = _simpleTopicSourceHandler.IsActive();

            Assert.That(isActive, Is.False);
        }
    }
}