﻿using AOMDiffusion.Common.Interfaces;

namespace AOMDiffusion.Common.Tests
{
    public class FakeDiffusionConfig : IDiffusionConfig
    {
        public string DiffusionEndPoint { get; set; }
        public string DiffusionUsername { get; set; }
        public string DiffusionPassword { get; set; }
        public string DiffusionHandlerName { get; set; }
    }
}
