﻿using AOMDiffusionCommon.Exceptions;

namespace AOMDiffusion.Common.Tests.Exceptions
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class DiffusionConnectionExceptionTests
    {
        [Test]
        public void ShouldInheritFromException()
        {
            var diffusionConnectionException = new DiffusionConnectionException("Message");
            Assert.That(diffusionConnectionException.GetType().BaseType, Is.EqualTo(typeof(Exception)));
            Assert.That(diffusionConnectionException.Message, Is.EqualTo("Message"));
        }
        
        [Test]
        public void ShouldInheritFromExceptionWithDefaultMessage()
        {
            var DiffusionConnectionException = new DiffusionConnectionException();
            Assert.That(DiffusionConnectionException.GetType().BaseType, Is.EqualTo(typeof(Exception)));
            Assert.That(DiffusionConnectionException.Message, Is.EqualTo("Exception of type 'AOMDiffusionCommon.Exceptions.DiffusionConnectionException' was thrown."));
        }
    }
}