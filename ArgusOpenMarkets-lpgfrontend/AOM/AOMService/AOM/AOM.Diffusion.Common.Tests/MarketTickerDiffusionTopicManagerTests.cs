﻿using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class MarketTickerDiffusionTopicManagerTests
    {
        [Test]
        public void ShouldReturnTheRootTopicThatMarketTickerTopicManagerOwns()
        {
            var pdtm = new MarketTickerDiffusionTopicManager();
            Assert.That(pdtm.TopicRoot, Is.EqualTo("AOM/MarketTicker"));
        }
    }
}