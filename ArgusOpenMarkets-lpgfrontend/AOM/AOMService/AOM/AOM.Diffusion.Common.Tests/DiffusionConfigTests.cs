﻿using System;
using NLog.Internal;
using NUnit.Framework;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class DiffusionConfigTests
    {
        [Test]
        public void ShouldNotThrowExceptionIfAllConfigurationExists()
        {
            var error = string.Empty;
            InitializeConfig("dtp:10.10.10.10/", "username", "password", "handler");
            DiffusionConfig diffConfig = null;
            try
            {
                diffConfig = new DiffusionConfig();

            }
            catch (Exception ex)
            {
                error = ex.ToString();
            }

            Assert.That(error, Is.Empty);
            Assert.That(diffConfig, Is.Not.Null);
        }

        [Test]
        [TestCase("", "username", "password", "handler", "Missing 'DiffusionEndPoint' configuration setting")]
        [TestCase("dpt://", "", "password", "handler", "Missing 'DiffusionUsername' configuration setting")]
        [TestCase("dpt://", "username", "", "handler", "Missing 'DiffusionPassword' configuration setting")]
        [TestCase("dpt://", "username", "password", "", "Missing 'DiffusionHandlerName' configuration setting")]
        public void ShouldThrowExceptionIfSpecificConfigItemMissing(string diffusionEndPoint, string username, string password, string handler, string expectedError)
        {
            var error = string.Empty;
            InitializeConfig(diffusionEndPoint, username, password, handler);
            DiffusionConfig diffusionConfig = null;

            try
            {
                diffusionConfig = new DiffusionConfig();

            }
            catch (Exception ex)
            {
                error = ex.ToString();
            }

            Assert.That(error.Contains(expectedError));
            Assert.That(diffusionConfig, Is.Null);
        }

        [Test]
        [TestCase("dtp://10.1.1.1", "username", "password", "handler")]

        public void ShouldInitializeConfigWithCorrectValues(string diffusionEndPoint, string username, string password, string handler)
        {

            var error = string.Empty;
            InitializeConfig(diffusionEndPoint, username, password, handler);

            DiffusionConfig diffusionConfig = null;
            try
            {
                diffusionConfig = new DiffusionConfig();

            }
            catch (Exception ex)
            {
                error = ex.ToString();
            }

            Assert.That(error, Is.Empty);
            if (diffusionConfig == null)
            {
                // Should never run.
                Assert.That(1, Is.EqualTo(100));
            }
            else
            {
                Assert.That(diffusionConfig.DiffusionEndPoint, Is.EqualTo(diffusionEndPoint));
                Assert.That(diffusionConfig.DiffusionUsername, Is.EqualTo(username));
                Assert.That(diffusionConfig.DiffusionPassword, Is.EqualTo(password));
                Assert.That(diffusionConfig.DiffusionHandlerName, Is.EqualTo(handler));
            }
        }

        private void InitializeConfig(string diffusionEndPoint, string diffusionUsername, string diffusionPassword,
            string diffusionHandlerName)
        {
            var cm = new ConfigurationManager();
            cm.AppSettings["DiffusionEndPoint"] = diffusionEndPoint;
            cm.AppSettings["DiffusionUsername"] = diffusionUsername;
            cm.AppSettings["DiffusionPassword"] = diffusionPassword;
            cm.AppSettings["DiffusionHandlerName"] = diffusionHandlerName;
        }
    }
}
