﻿using System.Linq;
using AOM.App.Domain.Dates;
using AOM.Repository.MySql.Crm;
using AOM.Transport.Events;
using AOM.Transport.Events.Users;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Details;
using PushTechnology.ClientInterface.Client.Session;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class DiffusionSessionManagerTests
    {
        private DiffusionSessionManager _diffusionSessionManager;
        private Mock<IBus> _mockBus;
        private Mock<IDateTimeProvider> _mockDateTimeProvider;
        private Mock<ISessionHelper> _mockSessionHelper;

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockDateTimeProvider = new Mock<IDateTimeProvider>();
            _mockSessionHelper = new Mock<ISessionHelper>();
            _diffusionSessionManager = new DiffusionSessionManager(_mockSessionHelper.Object, _mockDateTimeProvider.Object,
                _mockBus.Object);
        }

        [Test]
        public void RegisterSessionShouldReturnAValidClientSessionInfoObject()
        {
            var sessionId = new SessionId();
            var csi = _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            Assert.That(csi.UserId, Is.EqualTo(1));
            Assert.That(csi.OrganisationId, Is.EqualTo(1));
        }

        [Test]
        public void RegisteringTheSameSessionTwiceShouldResultInASingleEntryInTheSessionsDictionary()
        {
            var sessionId = new SessionId();
            _diffusionSessionManager.RegisterSession(1, 1, sessionId);
            _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            Assert.That(_diffusionSessionManager.SessionsDictionary.Count, Is.EqualTo(1));
        }

        [Test]
        public void OnSessionCloseShouldRemoveTheSessionFromTheSessionDictionary()
        {
            _mockSessionHelper.Setup(m => m.GetUserInfoDto(It.IsAny<ISessionDetails>()))
                .Returns(new UserInfoDto {Organisation_Id_fk = 1});
            var sessionId = new SessionId();
            var mockSessionDetails = new Mock<ISessionDetails>();
            var mockClientSummary = new Mock<IClientSummary>();
            mockClientSummary.Setup(p => p.Principal).Returns("Nathan");

            mockSessionDetails.Setup(p => p.Summary).Returns(mockClientSummary.Object);

            _diffusionSessionManager.RegisterSession(1, 1, sessionId);
            _diffusionSessionManager.OnSessionClose(sessionId, mockSessionDetails.Object, CloseReason.CLOSED_BY_CLIENT);

            Assert.That(_diffusionSessionManager.SessionsDictionary.Count, Is.EqualTo(1));
        }

        [Test]
        public void OnSessionUpdateShouldUpdateTheSingleSessionInTheDictionary()
        {
            _mockSessionHelper.Setup(m => m.GetUserInfoDto(It.IsAny<ISessionDetails>()))
                .Returns(new UserInfoDto {Organisation_Id_fk = 1});
            var sessionId = new SessionId();
            var mockSessionDetails = new Mock<ISessionDetails>();
            var mockClientSummary = new Mock<IClientSummary>();
            mockClientSummary.Setup(p => p.Principal).Returns("Nathan");

            mockSessionDetails.Setup(p => p.Summary).Returns(mockClientSummary.Object);

            _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            _diffusionSessionManager.OnSessionUpdate(sessionId, mockSessionDetails.Object);

            Assert.That(_diffusionSessionManager.SessionsDictionary.Count, Is.EqualTo(2));
        }

        [Test]
        public void OnSessionCloseShouldPublishABlankUserTopicRequestForTheRelevantUser()
        {
            _mockSessionHelper.Setup(m => m.GetUserInfoDto(It.IsAny<ISessionDetails>()))
                .Returns(new UserInfoDto {Organisation_Id_fk = 1, Id = 100});
            var sessionId = new SessionId();
            var mockSessionDetails = new Mock<ISessionDetails>();
            var mockClientSummary = new Mock<IClientSummary>();
            mockClientSummary.Setup(p => p.Principal).Returns("Nathan");

            mockSessionDetails.Setup(p => p.Summary).Returns(mockClientSummary.Object);

            _diffusionSessionManager.RegisterSession(1, 1, sessionId);
            _diffusionSessionManager.OnSessionClose(sessionId, mockSessionDetails.Object, CloseReason.CLOSED_BY_CLIENT);

            _mockBus.Verify(
                m => m.Publish(It.Is<BlankUserTopic>(but => but.UserId.Equals(100))), Times.Exactly(1));
        }

        [Test]
        public void OnSessionCloseShouldPublishAMessageOnTheBusToRecordTheUserDisconnecting()
        {
            _mockSessionHelper.Setup(m => m.GetUserInfoDto(It.IsAny<ISessionDetails>()))
                .Returns(new UserInfoDto {Organisation_Id_fk = 1});

            var sessionId = new SessionId();
            var mockSessionDetails = new Mock<ISessionDetails>();
            var mockClientSummary = new Mock<IClientSummary>();
            mockClientSummary.Setup(p => p.Principal).Returns("Nathan");

            mockSessionDetails.Setup(p => p.Summary).Returns(mockClientSummary.Object);

            _diffusionSessionManager.RegisterSession(1, 1, sessionId);
            _diffusionSessionManager.OnSessionClose(sessionId, mockSessionDetails.Object, CloseReason.CLOSED_BY_CLIENT);

            _mockBus.Verify(m => m.Publish(It.IsAny<UserDisconnectedResponse>()), Times.Exactly(1));
        }

        [Test]
        public void GetSessionForSessionThatExistsShouldReturnSession()
        {
            var sessionId = new SessionId();
            var csi = _diffusionSessionManager.RegisterSession(1, 1, sessionId);


            SessionId session = _diffusionSessionManager.GetSession(csi);
            Assert.That(session, Is.Not.Null);
        }

        [Test]
        public void GetUserSessionForSessionThatExistsShouldReturnUserSession()
        {
            SessionId sessionId = new SessionId();
            var csi = _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            ClientSessionInfo returnedCsi = _diffusionSessionManager.GetUserSession(csi.SessionId);
            Assert.That(returnedCsi, Is.Not.Null);
            Assert.That(csi.UserId, Is.EqualTo(1));
            Assert.That(csi.OrganisationId, Is.EqualTo(1));
        }

        [Test]
        public void OnSessionOpenShouldRegisterTheUsersSession()
        {
            _mockSessionHelper.Setup(m => m.IsAuthenticationUser(It.IsAny<ISessionDetails>())).Returns(false);
            _mockSessionHelper.Setup(m => m.GetUserInfoDto(It.IsAny<ISessionDetails>()))
                .Returns(new UserInfoDto {Organisation_Id_fk = 1, Id = 1});

            Mock<ISessionDetails> mockSessionDetails = new Mock<ISessionDetails>();

            Mock<IClientSummary> clientSummary = new Mock<IClientSummary>();
            clientSummary.SetupGet(p => p.Principal).Returns("Nathan");
            mockSessionDetails.SetupGet(p => p.Summary).Returns(clientSummary.Object);

            SessionId sessionId = SessionId.NEW_SESSION;
            var csi = _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            _diffusionSessionManager.OnSessionOpen(sessionId, mockSessionDetails.Object);

            ClientSessionInfo returnedCsi = _diffusionSessionManager.GetUserSession(csi.SessionId);
            Assert.That(returnedCsi, Is.Not.Null);
            Assert.That(csi.UserId, Is.EqualTo(1));
            Assert.That(csi.OrganisationId, Is.EqualTo(1));
        }

        [Test]
        public void GetUserSessionsShouldReturnSingleUserSession()
        {
            SessionId sessionId = SessionId.NEW_SESSION;
            var csiOne = _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            var userSessions = _diffusionSessionManager.GetUserSessions(1).ToList();

            Assert.That(userSessions, Is.Not.Null);
            Assert.That(userSessions.Count, Is.EqualTo(1));
        }

        [Test]
        public void GetUserSessionsShouldReturnAllUserSessions()
        {
            SessionId sessionId = SessionId.NEW_SESSION;
            var csiOne = _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            SessionId sessionIdTwo = new SessionId();
            var csiTwo = _diffusionSessionManager.RegisterSession(1, 1, sessionIdTwo);

            var userSessions = _diffusionSessionManager.GetUserSessions(1).ToList();

            Assert.That(userSessions, Is.Not.Null);
            Assert.That(userSessions.Count, Is.EqualTo(2));
        }

        [Test]
        public void RemoveUserSessionsShouldRemoveAllSessionsForAParticularUser()
        {
            SessionId sessionId = SessionId.NEW_SESSION;
            var csiOne = _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            SessionId sessionIdTwo = new SessionId();
            var csiTwo = _diffusionSessionManager.RegisterSession(1, 1, sessionIdTwo);

            var userSessions = _diffusionSessionManager.GetUserSessions(1).ToList();
            Assert.That(userSessions.Count, Is.EqualTo(2));

            _diffusionSessionManager.RemoveUserSessions(1);
            userSessions = _diffusionSessionManager.GetUserSessions(1).ToList();

            Assert.That(userSessions.Count, Is.EqualTo(0));
        }

        [Test]
        public void RemoveOtherUserSessionsShouldRemoveAllUserSessionsApartFromTheOneSupplied()
        {
            SessionId sessionId = SessionId.NEW_SESSION;
            var csiOne = _diffusionSessionManager.RegisterSession(1, 1, sessionId);

            SessionId sessionIdTwo = new SessionId();
            var csiTwo = _diffusionSessionManager.RegisterSession(1, 1, sessionIdTwo);

            var userSessions = _diffusionSessionManager.GetUserSessions(1).ToList();
            Assert.That(userSessions.Count, Is.EqualTo(2));

            _diffusionSessionManager.RemoveUserOtherSessions(sessionIdTwo.ToString(), 1);
            userSessions = _diffusionSessionManager.GetUserSessions(1).ToList();
            var userSess = _diffusionSessionManager.GetUserSessions(1);

            Assert.That(userSessions.Count, Is.EqualTo(2));
            // At least one of the sessions should be marked as: 
            var count = 0;
            foreach (var sess in _diffusionSessionManager.SessionsDictionary)
            {
                if (sess.Value.SessionId == sessionId)
                {
                    if (sess.Value.SessionTerminatedByAnotherLogin == true)
                    {
                        count++;
                    }
                }
            }

            Assert.That(count, Is.GreaterThanOrEqualTo(0));
        }
    }
}