﻿using NUnit.Framework;
using PushTechnology.ClientInterface.Client.Features.Control.Topics;

namespace AOMDiffusion.Common.Tests
{
    [TestFixture]
    public class TopicControlAddCallbackTests
    {
        [Test]
        public void TopicAddedWasCalled()
        {
            var topicAdded = false;
            object eventDetails = null;
            TopicControlAddCallback topicControlAddCallback = new TopicControlAddCallback();

            topicControlAddCallback.TopicAdded += (o, e) =>
            {
                topicAdded = true;
                eventDetails = e;
            };
            topicControlAddCallback.OnTopicAdded("AOM/HealthCheck");

            Assert.IsTrue(topicAdded);
            Assert.AreEqual(eventDetails, "AOM/HealthCheck");
        }

        [Test]
        public void TopicAddFailedWasCalled()
        {
            var topicAddFailed = false;
            object eventDetails = null;
            TopicControlAddCallback topicControlAddCallback = new TopicControlAddCallback();

            topicControlAddCallback.TopicAddFailed += (o, e) =>
            {
                topicAddFailed = true;
                eventDetails = e;
            };
            topicControlAddCallback.OnTopicAddFailed("AOM/HealthCheckError", TopicAddFailReason.TOPIC_NOT_FOUND);

            Assert.IsTrue(topicAddFailed);
            Assert.AreEqual(eventDetails, TopicAddFailReason.TOPIC_NOT_FOUND);
        }
    }
}
