﻿
namespace AOM.Transport.Service.EmailProcessor
{
    public interface IAomWebsiteDetails
    {
        string WebsiteUrl { get; }
    }
}
