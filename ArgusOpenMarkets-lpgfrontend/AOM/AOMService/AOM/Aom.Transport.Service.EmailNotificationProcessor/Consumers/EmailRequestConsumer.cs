﻿namespace AOM.Transport.Service.EmailProcessor.Consumers
{
    using System;
    using System.Diagnostics;

    using AOM.Repository.MySql;
    using AOM.Services.EmailService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Emails;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public class EmailRequestConsumer : IConsumer
    {
        private readonly IEmailService _emailService;

        private readonly ISmtpService _smtpService;

        private readonly IBus _aomBus;

        private readonly IDbContextFactory _dbFactory;

        private readonly IAomWebsiteDetails _aomWebsiteDetails;

        public EmailRequestConsumer(
            IDbContextFactory dbFactory,
            IEmailService emailService,
            ISmtpService smtpService,
            IBus aomBus,
            IAomWebsiteDetails aomWebsiteDetails)
        {
            _emailService = emailService;
            _smtpService = smtpService;
            _aomBus = aomBus;
            _dbFactory = dbFactory;
            _aomWebsiteDetails = aomWebsiteDetails;
        }

        public void Start()
        {
            _aomBus.Subscribe<AomSendEmailRequest>(SubscriptionId, ConsumeAomSendEmailRequest);
        }

        public void ConsumeAomSendEmailRequest(AomSendEmailRequest emailRequest)
        {
            Debug.WriteLine("Received AomEmailNotificationRequest request");
            try
            {
                switch (emailRequest.MessageAction)
                {
                    case MessageAction.Send:
                        using (var aomDb = _dbFactory.CreateAomModel())
                        {
                            _emailService.SendEmailNotification(aomDb,
                                                                _smtpService,
                                                                emailRequest.EmailId,
                                                                _aomWebsiteDetails.WebsiteUrl);
                        }
                        break;
                    case MessageAction.Check:
                        using (var aomDb = _dbFactory.CreateAomModel())
                        {
                            _emailService.SendAnyOutstandingEmailNotifications(aomDb,
                                                                               _smtpService,
                                                                               _aomWebsiteDetails.WebsiteUrl);
                        }
                        break;
                }
            }
            catch (Exception exception)
            {
                Log.Error(string.Format("Failed to send Email Notification Id: {0}", emailRequest.EmailId), exception);
            }
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}