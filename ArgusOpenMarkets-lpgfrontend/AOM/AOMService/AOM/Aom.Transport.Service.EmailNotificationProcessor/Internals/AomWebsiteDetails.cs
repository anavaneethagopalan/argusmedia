﻿using System.Configuration;

namespace AOM.Transport.Service.EmailProcessor.Internals
{
    internal class AomWebsiteDetails : IAomWebsiteDetails
    {
        public AomWebsiteDetails()
        {
            WebsiteUrl = ConfigurationManager.AppSettings["WebsiteURL"];
        }

        public string WebsiteUrl { get; private set; }
    }
}
