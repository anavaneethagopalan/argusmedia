﻿using Topshelf;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.EmailProcessor
{
    using System.Collections.Generic;

    using AOM.Services.EmailService;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    public class EmailProcessorService : ProcessorServiceBase
    {
        private readonly IEmailTimerService _emailTimerService;

        public EmailProcessorService(
            IBus aomBus,
            IEnumerable<IConsumer> consumers,
            IServiceManagement serviceManagement,
            IEmailTimerService emailTimerService)
            : base(consumers, serviceManagement, aomBus)
        {
            _emailTimerService = emailTimerService;
        }

        public override bool Start(HostControl hostControl)
        {
            var result= base.Start(hostControl);
            _emailTimerService.Start();
            return result;
        }

        public override bool Stop(HostControl hostControl)
        {
            StopWithinTimeout(() => _emailTimerService.Stop(), "email timer service");
            return base.Stop(hostControl);
        }

        public override string Name
        {
            get { return "MarketTickerProcessorService"; }
        }
    }
}