﻿using AOM.Repository.MySql;
using AOM.Services;
using AOM.Services.AuthenticationService;
using AOM.Transport.Service.Processor.Common;
using AOM.Transport.Service.SecurityProcessor.Consumers;
using Ninject;
using NUnit.Framework;
using System.Linq;
using Utils.Javascript.Tests.Extension;

namespace AOM.Transport.Service.SecurityProcessor.Tests
{
    [TestFixture]
    public class BootstrapperTests
    {
        [Test]
        public void LoadsExpectedIConsumerListTest()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(new LocalBootstrapper(), new EFModelBootstrapper(), new ServiceBootstrap(),
                    new ProcessorCommonBootstrap());
                var bindings = kernel.GetAll(typeof (IConsumer)).ToList();

                AssertHelper.AssertListOnlyContains(bindings, x => x.GetType(),
                    typeof (AssessmentAuthenticationRequestConsumer),
                    //typeof (BrokerPermissionAuthenticationRequestConsumer),
                    //typeof (CounterpartyPermissionAuthenticationRequestConsumer),
                    typeof (CompanySettingsAuthenticationRequestConsumer),
                    typeof (InternalDealAuthenticationRequestConsumer),
                    typeof (ExternalDealAuthenticationRequestConsumer),
                    typeof (MarketInfoAuthenticationRequestConsumer),
                    typeof (ProductAuthenticationRequestConsumer),
                    typeof (OrderAuthenticationRequestConsumer),
                    typeof (HealthCheckConsumer)
                    );
            }
        }

        [Test]
        public void CanBindOrderAuthenticationService()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(new LocalBootstrapper(), new EFModelBootstrapper(), new ServiceBootstrap(),
                    new ProcessorCommonBootstrap());

                var binding = kernel.Get<IOrderAuthenticationService>();

                Assert.IsNotNull(binding);
            }
        }
    }
}