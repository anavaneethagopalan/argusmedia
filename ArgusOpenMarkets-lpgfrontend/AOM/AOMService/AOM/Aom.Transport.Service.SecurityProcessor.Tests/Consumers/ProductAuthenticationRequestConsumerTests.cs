﻿using System;
using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Events.Products;
using AOM.Transport.Service.SecurityProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Argus.Transport.Messages.Events;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.SecurityProcessor.Tests.Consumers
{
    [TestFixture]
    public class ProductAuthenticationRequestConsumerTests
    {
        private ProductAuthenticationRequestConsumer _productAuthenticationRequestConsumer;
        private Mock<IProductAuthenticationService> _mockProductAuthenticationService;
        private Mock<IBus> _mockBus;
        private Mock<IErrorService> _mockErrorService;

        [SetUp]
        public void Setup()
        {
            _mockErrorService = new Mock<IErrorService>();
            _mockBus = new Mock<IBus>();
            _mockProductAuthenticationService = new Mock<IProductAuthenticationService>();
            _productAuthenticationRequestConsumer =
                new ProductAuthenticationRequestConsumer(_mockProductAuthenticationService.Object,
                    _mockBus.Object,
                    _mockErrorService.Object);
        }

        [Test]
        public void ShouldReturnAnEmptySubscriptionId()
        {
            Assert.That(_productAuthenticationRequestConsumer.SubscriptionId, Is.EqualTo(string.Empty));
        }

        [Test]
        public void PurgeOrdersAuthenticationRequestShouldPublishProductResponseIfUserAuthorised()
        {
            var subs = StartConsumer();
            var csi = new ClientSessionInfo {OrganisationId = 1, SessionId = "S", UserId = 1};
            var product = new Product {ProductId = 1, Name = "Product1"};
            _productAuthenticationRequestConsumer.Start();
            _mockProductAuthenticationService.Setup(
                    m => m.AuthenticateProductRequest(It.IsAny<AomProductPurgeOrdersAuthenticationRequest>()))
                .Returns(new Message
                {
                    ClientSessionInfo = csi,
                    MessageAction = MessageAction.ConfigurePurge,
                    MessageType = MessageType.Order
                });

            var onProductPurgeOrdersAuthReq = subs.AomProductPurgeOrdersAuthReq;

            var req = new AomProductPurgeOrdersAuthenticationRequest
            {
                ClientSessionInfo = csi,
                Product = product
            };
            onProductPurgeOrdersAuthReq(req);

            _mockBus.Verify(
                b =>
                    b.Publish(
                        It.Is<AomProductResponse>(
                            r => r.Product.ProductId == product.ProductId)),
                Times.Once);
        }

        [Test]
        public void ProductTenorAuthenticationRequestShouldPublishProductTenorAuthResponse()
        {
            var subs = StartConsumer();
            var csi = new ClientSessionInfo {OrganisationId = 1, SessionId = "S", UserId = 1};
            var productTenor = new ProductTenor {Id = 1, ProductId = 1, Name = "Tenor1"};

            _productAuthenticationRequestConsumer.Start();
            _mockProductAuthenticationService.Setup(
                    m => m.AuthenticateProductTenorRequest(It.IsAny<AomProductTenorAuthenticationRequest>()))
                .Returns(new Message
                {
                    ClientSessionInfo = csi,
                    MessageAction = MessageAction.ConfigurePurge,
                    MessageType = MessageType.Order
                });

            var onProductTenorAuthorisationReq = subs.AomProductTenorAuthReq;

            var req = new AomProductTenorAuthenticationRequest
                {ClientSessionInfo = csi, Tenor = productTenor};

            onProductTenorAuthorisationReq(req);

            _mockBus.Verify(
               b =>
                   b.Publish(
                       It.Is<AomProductTenorAuthenticationResponse>(
                           r => r.ProductTenor.ProductId == productTenor.ProductId && r.ProductTenor.Id == productTenor.Id)),
               Times.Once);
        }

        private class SubscribedActions
        {
            public Action<AomProductPurgeOrdersAuthenticationRequest> AomProductPurgeOrdersAuthReq { get; set; }
            public Action<AomProductTenorAuthenticationRequest> AomProductTenorAuthReq { get; set; }
        }

        private SubscribedActions StartConsumer()
        {
            var subs = new SubscribedActions();
            _mockBus.Setup(
                    b => b.Subscribe(string.Empty, It.IsAny<Action<AomProductPurgeOrdersAuthenticationRequest>>()))
                .Callback<string, Action<AomProductPurgeOrdersAuthenticationRequest>>(
                    (s, a) => subs.AomProductPurgeOrdersAuthReq = a);

            _mockBus.Setup(
                    b => b.Subscribe(string.Empty, It.IsAny<Action<AomProductTenorAuthenticationRequest>>()))
                .Callback<string, Action<AomProductTenorAuthenticationRequest>>(
                    (s, a) => subs.AomProductTenorAuthReq = a);

            return subs;
        }
    }
}