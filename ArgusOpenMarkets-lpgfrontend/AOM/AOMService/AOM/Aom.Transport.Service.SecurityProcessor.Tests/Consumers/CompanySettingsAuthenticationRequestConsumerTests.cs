﻿using AOM.App.Domain.Entities;
using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.CompanySettings;
using AOM.Transport.Events.Organisations;
using AOM.Transport.Service.SecurityProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using Utils.Javascript.Tests.Extension;

namespace AOM.Transport.Service.SecurityProcessor.Tests.Consumers
{
    [TestFixture]
    public class CompanySettingsAuthenticationRequestConsumerTests
    {
        private Mock<IBus> _mockBus;
        private Mock<ICompanySettingsAuthenticationService> _mockAuthenticationService;

        private const int FakeProductId = 123;

        private static AomCompanySettingsAuthenticationRpcRequest CreateAuthenticationRequest(
            CompanyDetailsRpcRequest companyDetailsRpc)
        {
            return new AomCompanySettingsAuthenticationRpcRequest
            {
                ClientSessionInfo = companyDetailsRpc.ClientSessionInfo,
                MessageAction = companyDetailsRpc.MessageAction,
                CompanyDetails = companyDetailsRpc
            };
        }

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockAuthenticationService = new Mock<ICompanySettingsAuthenticationService>();
        }


        [Test]
        public void ConsumeCompanyDetailsAuthenticationRequestCallsCompanyDetailsAuthenticationService()
        {
            // Arrange     
            var request = CreateAuthenticationRequest(new CompanyDetailsRpcRequest()
            {
                EmailAddress = "travis@goldhammer.com",
                ProductId = FakeProductId,
                EventType = SystemEventType.DealConfirmation,
                ClientSessionInfo = new ClientSessionInfo() {UserId = -1},
                MessageAction = MessageAction.Create
            });

            var mockErrorService = new Mock<IErrorService>();
            _mockAuthenticationService.Setup(x => x.AuthenticateCompanySettingsPermissionRequest(request))
                .Returns(new AuthResult {Allow = true});

            var consumer = new CompanySettingsAuthenticationRequestConsumer(_mockAuthenticationService.Object,
                _mockBus.Object,
                mockErrorService.Object);

            // Act
            consumer.ConsumeAuthenticationRequest(request);

            // Assert
            _mockBus.VerifyThatNoAomErrorMessageWasSent();

            _mockAuthenticationService.Verify(
                s => s.AuthenticateCompanySettingsPermissionRequest(
                    It.Is<AomCompanySettingsAuthenticationRpcRequest>(
                        r => r.CompanyDetails.ProductId.Equals(FakeProductId))),
                Times.Once);
        }

        [Test]
        [TestCase(MessageAction.Create, true)]
        [TestCase(MessageAction.Delete, true)]
        [TestCase(MessageAction.Create, false)]
        [TestCase(MessageAction.Delete, false)]
        public void ConsumeAllowedRequestPublishesAllowResponse(MessageAction messageAction,
            bool marketMustBeOpen)
        {
            // Arrange   
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new CompanySettingsAuthenticationRequestConsumer(_mockAuthenticationService.Object,
                _mockBus.Object, mockErrorService.Object);

            var request = CreateAuthenticationRequest(new CompanyDetailsRpcRequest()
            {
                EmailAddress = "travis@goldhammer.com",
                ProductId = FakeProductId,
                EventType = SystemEventType.DealConfirmation,
                ClientSessionInfo = new ClientSessionInfo() {UserId = -1},
                MessageAction = messageAction
            });

            _mockAuthenticationService.Setup(s => s.AuthenticateCompanySettingsPermissionRequest(
                    It.IsAny<AomCompanySettingsAuthenticationRpcRequest>()))
                .Returns(new AuthResult
                {
                    Allow = true,
                    MarketMustBeOpen = marketMustBeOpen
                });

            // Act
            var r = consumer.ConsumeAuthenticationRequest(request);

            // Assert
            _mockBus.VerifyThatNoAomErrorMessageWasSent();

            Assert.True(r.Message.MessageType == MessageType.OrganisationNotificationDetail &&
                        r.CompanyDetails.ProductId == FakeProductId && r.MarketMustBeOpen == marketMustBeOpen);

        }

        [Test]
        [TestCase(MessageAction.Create)]
        [TestCase(MessageAction.Delete)]
        public void ConsumeNotAllowedRequestPublishesErrorResponse(MessageAction messageAction)
        {
            const string denialReason = "Request denied";

            // Arrange     
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new CompanySettingsAuthenticationRequestConsumer(_mockAuthenticationService.Object,
                _mockBus.Object,
                mockErrorService.Object);

            var request = CreateAuthenticationRequest(new CompanyDetailsRpcRequest()
            {
                EmailAddress = "travis@goldhammer.com",
                ProductId = FakeProductId,
                EventType = SystemEventType.DealConfirmation,
                ClientSessionInfo = new ClientSessionInfo() {UserId = -1},
                MessageAction = messageAction
            });

            _mockAuthenticationService.Setup(s => s.AuthenticateCompanySettingsPermissionRequest(
                    It.IsAny<AomCompanySettingsAuthenticationRpcRequest>()))
                .Returns(new AuthResult
                {
                    Allow = false,
                    DenyReason = denialReason,
                    MarketMustBeOpen = false
                });

            // Act
            var r = consumer.ConsumeAuthenticationRequest(request);

            // Assert
            Assert.True(r.Message.MessageType == MessageType.Error &&
                        r.CompanyDetails.ProductId == FakeProductId && r.MarketMustBeOpen == false &&
                        ((string) r.Message.MessageBody).Contains(denialReason));
        }
    }
}