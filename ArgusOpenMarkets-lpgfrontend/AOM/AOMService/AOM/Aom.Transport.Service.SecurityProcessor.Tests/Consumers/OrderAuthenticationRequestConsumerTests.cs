﻿using AOM.Services.ErrorService;

namespace AOM.Transport.Service.SecurityProcessor.Tests.Consumers
{
    using System;

    using AOM.App.Domain.Entities;
    using AOM.Services.AuthenticationService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Service.SecurityProcessor.Consumers;
    using Argus.Transport.Infrastructure;
    using Moq;
    using NUnit.Framework;
    using Utils.Javascript.Tests.Extension;

    [TestFixture]
    public class OrderAuthenticationRequestConsumerTests
    {
        private AomOrderAuthenticationRequest _newOrderAuthenticationRequest;

        private ClientSessionInfo _clientSessionInfo;

        [TestFixtureSetUp]
        public void Setup()
        {
            _clientSessionInfo = new ClientSessionInfo { UserId = 12, SessionId = "TestSessionId" };
            var order = new Order { Id = 11111, Notes = "Tushara", Price = 100 };
            _newOrderAuthenticationRequest = new AomOrderAuthenticationRequest
                                             {
                                                 MessageAction = MessageAction.Create,
                                                 Order = order,
                                                 ClientSessionInfo = _clientSessionInfo
                                             };
        }

        [Test]
        public void NewOrderConsumerSubscribesToAuthenticationRequests()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockErrorService = new Mock<IErrorService>();
            var mockOrderAuthenticationService = new Mock<IOrderAuthenticationService>();
            var newOrderConsumer = new OrderAuthenticationRequestConsumer(
                mockOrderAuthenticationService.Object,
                mockBus.Object, mockErrorService.Object);

            //Act
            newOrderConsumer.Start();

            //Assert
            mockBus.Verify(
                x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomOrderAuthenticationRequest>>()),
                Times.Once);
        }

        [Test]
        public void ConsumeNewOrderAuthenticationCallsOrderAuthenticationService()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderAuthenticationService = new Mock<IOrderAuthenticationService>();
            var mockErrorService = new Mock<IErrorService>();

            var newOrderConsumer = new OrderAuthenticationRequestConsumer(
                mockOrderAuthenticationService.Object,
                mockBus.Object,
                mockErrorService.Object);

            mockOrderAuthenticationService.Setup(x => x.AuthenticateOrderRequest(_newOrderAuthenticationRequest))
                .Returns(new AuthResult{Allow = true}); 

            //Act
            newOrderConsumer.ConsumeAuthenticationRequest<AomOrderAuthenticationRequest, AomOrderAuthenticationResponse>
                (_newOrderAuthenticationRequest);

            //Assert
            mockBus.VerifyThatNoAomErrorMessageWasSent(); 

            mockOrderAuthenticationService.Verify(
                x =>
                    x.AuthenticateOrderRequest(
                        It.Is<AomOrderAuthenticationRequest>(r => (r.Order.Notes.Equals("Tushara")))),
                Times.Once);
        }

        [Test]
        public void ConsumeNewOrderAuthenticationRequestPublishesResponse()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            
            var mockOrderAuthenticationService = new Mock<IOrderAuthenticationService>();
            var mockErrorService = new Mock<IErrorService>();

            mockOrderAuthenticationService.Setup(
                x => x.AuthenticateOrderRequest(It.IsAny<AomOrderAuthenticationRequest>()))
                .Returns(new AuthResult { Allow = true, MarketMustBeOpen = false });
            var newOrderConsumer = new OrderAuthenticationRequestConsumer(
                mockOrderAuthenticationService.Object,
                mockBus.Object, 
                mockErrorService.Object);

            //Act
            newOrderConsumer.ConsumeAuthenticationRequest<AomOrderAuthenticationRequest, AomOrderAuthenticationResponse>
                (_newOrderAuthenticationRequest);

            //Assert
            mockBus.VerifyThatNoAomErrorMessageWasSent();          

            mockBus.Verify(
                x => x.Publish(It.Is<AomOrderAuthenticationResponse>(r => (r.Order.Notes.Equals("Tushara")))),
                Times.Once);
        }

        [Test]
        public void ConsumeNewOrderAuthenticationRequestPublishesErrorResponseIfMessageError()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderAuthenticationService = new Mock<IOrderAuthenticationService>();
            var mockErrorService = new Mock<IErrorService>();

            mockOrderAuthenticationService.Setup(
                x => x.AuthenticateOrderRequest(It.IsAny<AomOrderAuthenticationRequest>()))
                .Returns(new AuthResult { Allow = false, DenyReason = "Test Denial", MarketMustBeOpen = false });
            var newOrderConsumer = new OrderAuthenticationRequestConsumer(
                mockOrderAuthenticationService.Object,
                mockBus.Object,
                mockErrorService.Object);

            //Act
            newOrderConsumer.ConsumeAuthenticationRequest<AomOrderAuthenticationRequest, AomOrderAuthenticationResponse>
                (_newOrderAuthenticationRequest);


            //Assert
            mockBus.Verify(
                x => x.Publish(It.Is<AomOrderAuthenticationResponse>(r => (r.Message.MessageType == MessageType.Error))),
                Times.Once);
        }

     

    }
}