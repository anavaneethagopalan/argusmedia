﻿using AOM.Services.AuthenticationService;
using AOM.Services.ErrorService;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using AOM.Transport.Service.SecurityProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using System;
using Utils.Javascript.Tests.Extension;

namespace AOM.Transport.Service.SecurityProcessor.Tests.Consumers
{
    [TestFixture]
    public class AssessmentAuthenticationRequestConsumerTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IAssessmentAuthenticationService> _mockAssessmentService;

        private const int FakeProductId = 123;

        private static AomAssessmentAuthenticationRequest CreateAuthenticationRequest(
            MessageAction messageAction, long userId = 11)
        {
            return new AomAssessmentAuthenticationRequest
            {
                ClientSessionInfo = new ClientSessionInfo {UserId = userId},
                MessageAction = messageAction,
                ProductId = FakeProductId
            };
        }

        [SetUp]
        public void Setup()
        {
            _mockBus = new Mock<IBus>();
            _mockAssessmentService = new Mock<IAssessmentAuthenticationService>();
        }

        public AssessmentAuthenticationRequestConsumer CreateConsumer()
        {
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new AssessmentAuthenticationRequestConsumer(_mockAssessmentService.Object,
                                                                       _mockBus.Object, 
                                                                       mockErrorService.Object);

            return consumer;
        }

        [Test]
        public void ConsumerSubscribesToAuthenticationRequest()
        {
            // Arrange            
            var consumer = CreateConsumer();

            // Act
            consumer.Start();

            // Assert
            _mockBus.VerifyThatNoAomErrorMessageWasSent();   

            _mockBus.Verify(
                x => x.Subscribe(It.IsAny<string>(),
                                 It.IsAny<Action<AomAssessmentAuthenticationRequest>>()),
                Times.Once);
        }

        [Test]
        public void ConsumeAssessmentAuthenticationRequestCallsAssessmentAuthenticationService()
        {            
            // Arrange     
            var request = CreateAuthenticationRequest(MessageAction.Create);
            var mockErrorService = new Mock<IErrorService>();
            _mockAssessmentService.Setup(x => x.AuthenticateAssessmentRequest(request)).Returns(new AuthResult {Allow = true});

            var consumer = new AssessmentAuthenticationRequestConsumer(_mockAssessmentService.Object,
                                                                       _mockBus.Object,
                                                                       mockErrorService.Object);
                        
            // Act
            consumer.ConsumeAuthenticationRequest(request);

            // Assert
            _mockBus.VerifyThatNoAomErrorMessageWasSent();            

            _mockAssessmentService.Verify(
                s => s.AuthenticateAssessmentRequest(
                    It.Is<AomAssessmentAuthenticationRequest>(r => r.ProductId.Equals(FakeProductId))),
                Times.Once);
        }
     
        [Test]
        [TestCase(MessageAction.Create, true)]
        [TestCase(MessageAction.Refresh, true)]
        [TestCase(MessageAction.Create, false)]
        [TestCase(MessageAction.Refresh, false)]
        public void ConsumeAllowedRequestPublishesAllowResponse(MessageAction messageAction,
                                                                bool marketMustBeOpen)
        {
            var mockErrorService = new Mock<IErrorService>();
            // Arrange            
            var consumer = new AssessmentAuthenticationRequestConsumer(_mockAssessmentService.Object,
                                                                       _mockBus.Object,
                                                                       mockErrorService.Object);

            var request = CreateAuthenticationRequest(messageAction);

            _mockAssessmentService.Setup(s => s.AuthenticateAssessmentRequest(
                It.IsAny<AomAssessmentAuthenticationRequest>()))
                                  .Returns(new AuthResult
                                  {
                                      Allow = true,
                                      MarketMustBeOpen = marketMustBeOpen
                                  });

            // Act
            consumer.ConsumeAuthenticationRequest(request);

            // Assert
            _mockBus.VerifyThatNoAomErrorMessageWasSent();   

            _mockBus.Verify(
                b =>
                    b.Publish(
                        It.Is<AomAssessmentAuthenticationResponse>(
                            r => r.Message.MessageType == MessageType.Assessment &&
                                 r.ProductId == FakeProductId &&
                                 r.MarketMustBeOpen == marketMustBeOpen)),
                Times.Once);
        }

        [Test]
        [TestCase(MessageAction.Create)]
        [TestCase(MessageAction.Refresh)]
        public void ConsumeNotAllowedRequestPublishesErrorResponse(MessageAction messageAction)
        {
            const string denialReason = "Request denied";
            var mockErrorService = new Mock<IErrorService>();

            // Arrange            
            var consumer = new AssessmentAuthenticationRequestConsumer(_mockAssessmentService.Object,
                                                                       _mockBus.Object, 
                                                                       mockErrorService.Object);

            var request = CreateAuthenticationRequest(messageAction);

            _mockAssessmentService.Setup(s => s.AuthenticateAssessmentRequest(
                It.IsAny<AomAssessmentAuthenticationRequest>()))
                                  .Returns(new AuthResult
                                  {
                                      Allow = false,
                                      DenyReason = denialReason,
                                      MarketMustBeOpen = false
                                  });

            // Act
            consumer.ConsumeAuthenticationRequest(request);

            // Assert
            _mockBus.Verify(
                b =>
                    b.Publish(
                        It.Is<AomAssessmentAuthenticationResponse>(
                            r => r.Message.MessageType == MessageType.Error &&
                                 r.ProductId == FakeProductId &&
                                 r.Message.MessageBody is string &&
                                 ((string) r.Message.MessageBody).Contains(denialReason))),
                Times.Once);
        }

        [Test]
        public void ConsumeAuthenticationRequestShouldReturnTrueIfTheUserIsTheProcessorUser()
        {
            // Arrange 
            var mockErrorService = new Mock<IErrorService>();
            var consumer = new AssessmentAuthenticationRequestConsumer(_mockAssessmentService.Object,
                                                                       _mockBus.Object, 
                                                                       mockErrorService.Object);

            var request = CreateAuthenticationRequest(MessageAction.Refresh, -1);

            _mockAssessmentService.Setup(s => s.AuthenticateAssessmentRequest(
                It.IsAny<AomAssessmentAuthenticationRequest>()))
                                  .Returns(new AuthResult
                                  {
                                      Allow = false,
                                      DenyReason = "",
                                      MarketMustBeOpen = false
                                  });

            // Act
            consumer.ConsumeAuthenticationRequest(request);

            // Assert
            _mockBus.Verify(
                b =>
                    b.Publish(
                        It.Is<AomAssessmentAuthenticationResponse>(
                            r => r.Message.MessageType == MessageType.Assessment &&
                                 r.Message.MessageBody == null)),
                Times.Once);
        }   
    }
}
