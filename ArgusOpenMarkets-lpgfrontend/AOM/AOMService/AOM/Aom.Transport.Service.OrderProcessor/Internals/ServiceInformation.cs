﻿
using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.OrderProcessor.Internals
{
    internal class ServiceInformation : IServiceInformation
    {
        public string ServiceName
        {
            get { return DisplayName.Replace(" ", ""); }
        }

        public string DisplayName
        {
            get { return "AOM Transport Service Order Processor"; }
        }

        public string Description
        {
			get { return "AOM Transport Service Order Processor"; }
        }
    }
}