﻿using AOM.App.Domain.Entities;
using AOM.Repository.MySql;
using AOM.Services.ErrorService;
using AOM.Services.OrderService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using System;
using System.Diagnostics;
using System.Linq;
using Utils.Logging.Utils;

namespace AOM.Transport.Service.OrderProcessor.Consumers
{
    public class OrderRefreshRequestConsumer : OrderConsumerBase
    {
        private readonly IOrderService _orderService;

        public OrderRefreshRequestConsumer(
            IDbContextFactory dbFactory,
            IOrderService orderService,
            IBus aomBus,
            IProductService productService,
            IErrorService errorService)
            : base(dbFactory, orderService, aomBus, productService, errorService)
        {
            _orderService = orderService;
        }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomOrderRefreshRequest>(SubscriptionId, ConsumeAomOrderRefreshRequest);
        }

        internal void ConsumeAomOrderRefreshRequest(AomOrderRefreshRequest orderRefreshRequest)
        {
            Log.Debug("Order Refresh Request received");
            try
            {
                using (var aomDb = DBFactory.CreateAomModel())
                {
                    var timer = Stopwatch.StartNew();

                    var businessDayOrdersForProduct =
                        _orderService.GetOrdersForTodayByProductId(aomDb, orderRefreshRequest.ProductId).ToList();

                    foreach (var order in businessDayOrdersForProduct)
                    {
                        var orderRefreshResponse = new AomOrderRefreshResponse
                        {
                            Order = order,
                            Message = new Message
                            {
                                ClientSessionInfo = orderRefreshRequest.ClientSessionInfo,
                                MessageAction = MapOrderStatusToMessageAction(order.OrderStatus),
                                MessageType = MessageType.Order,
                                MessageBody = null
                            }
                        };

                        _aomBus.Publish(orderRefreshResponse);
                    }

                    Log.Info(
                        string.Format(
                            "Refresh of {0} orders for product {1} took {2} msecs",
                            businessDayOrdersForProduct.Count,
                            orderRefreshRequest.ProductId,
                            timer.ElapsedMilliseconds));
                }
            }
            catch (Exception exception)
            {
                _aomBus.PublishErrorResponseAndLogWarning<AomOrderResponse>(
                    MessageAction.Request,
                    orderRefreshRequest.ClientSessionInfo,
                    "Refreshing orders for day " + exception);
            }
        }

        private MessageAction MapOrderStatusToMessageAction(OrderStatus orderStatus)
        {
            switch (orderStatus)
            {
                case OrderStatus.Active:
                    return MessageAction.Create;
                case OrderStatus.Executed:
                    return MessageAction.Execute;
                case OrderStatus.Held:
                    return MessageAction.Hold;
                case OrderStatus.Killed:
                    return MessageAction.Kill;
                case OrderStatus.VoidAfterExecuted:
                    return MessageAction.Void;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}