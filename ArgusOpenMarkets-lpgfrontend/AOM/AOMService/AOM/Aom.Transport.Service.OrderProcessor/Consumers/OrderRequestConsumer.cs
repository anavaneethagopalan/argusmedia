﻿using AOM.Services.ErrorService;

namespace AOM.Transport.Service.OrderProcessor.Consumers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using App.Domain.Entities;
    using Repository.MySql;
    using Repository.MySql.Aom;
    using Services.EmailService;
    using Services.OrderService;
    using Services.ProductService;
    using Events;
    using Events.Deals;
    using Events.Emails;
    using Events.Orders;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    internal class OrderRequestConsumer : OrderConsumerBase
    {
        private readonly IDbContextFactory _dbFactory;

        private readonly IOrderService _orderService;

        private IEmailService _emailService;

        public OrderRequestConsumer(
            IDbContextFactory dbFactory,
            IOrderService orderService,
            IEmailService emailService,
            IBus aomBus,
            IProductService productService, 
            IErrorService errorService)
            : base(dbFactory, orderService, aomBus, productService, errorService)
        {
            _dbFactory = dbFactory;
            _orderService = orderService;
            _emailService = emailService;
        }

        internal override void SubscribeToEvents()
        {
            _aomBus.Subscribe<AomOrderRequest>(
                SubscriptionId,
                ConsumeOrderRequest<AomOrderRequest, AomOrderAuthenticationRequest>);

            _aomBus.Subscribe<AomOrderAuthenticationResponse>(
                SubscriptionId,
                ConsumeOrderResponse<AomOrderAuthenticationResponse, AomOrderResponse>);

            _aomBus.Respond<UserOrderRequestRpc, List<Order>>(GetOrdersForUser);
        }

        internal override Order ConsumeOrderResponseAdditionalProcessing<T>(
            IAomModel aomDb,
            T response,
            MarketStatus marketStatus)
        {
            switch (response.Message.MessageAction)
            {
                case MessageAction.Create:
                    return _orderService.NewOrder(aomDb, response, marketStatus);

                case MessageAction.Update:
                    return _orderService.EditOrder(aomDb, response, marketStatus);

                case MessageAction.Request:
                    return _orderService.GetOrderById(aomDb,response.Order.Id);

                case MessageAction.Hold:
                    return _orderService.HoldOrder(aomDb, response, marketStatus);

                case MessageAction.Kill:
                    return _orderService.KillOrder(aomDb, response, marketStatus);

                case MessageAction.Reinstate:
                    return _orderService.ReinstateOrder(aomDb, response, marketStatus);

                case MessageAction.Execute:
                    return ExecuteOrder(aomDb, response, marketStatus);

                case MessageAction.Void:
                    return _orderService.VoidOrder(aomDb, response, marketStatus);

                case MessageAction.Refresh:
                    throw new NotSupportedException(
                        "Refresh is consumed by another consumer as it does not fit in this model.");
            }
            return null;
        }

        private List<Order> GetOrdersForUser(UserOrderRequestRpc request)
        {
            try
            {
                using (var aomDb = _dbFactory.CreateAomModel())
                {
                    return _orderService.GetOpenOrdersInitiatedByUserId(aomDb, request.UserId).ToList();
                }
            }
            catch (Exception e)
            {

                Log.Error("GetOrdersForUser", e);
            }

            return new List<Order>();
        }

        private Order ExecuteOrder(IAomModel aomDb, AomOrderAuthenticationResponse response, MarketStatus marketStatus)
        {
            using (var trans = TransactionScopeHelper.CreateTransactionScopeForConcurrencyCheck())
            {
                long matchingOrderId = -1;
                var hitOrder = _orderService.ExecuteOrder(aomDb, response, out matchingOrderId, marketStatus);
                var deal = _orderService.CreateDealFromOrderExecute(aomDb, hitOrder, matchingOrderId);
                var notificationEmails = _emailService.CreateInternalDealEmails(aomDb, deal);


                var dealResponse = new AomDealResponse
                                   {
                                       Deal = deal,
                                       Message =
                                           new Message
                                           {
                                               ClientSessionInfo =
                                                   response.Message.ClientSessionInfo,
                                               MessageAction =
                                                   response.Message.MessageAction,
                                               MessageType = MessageType.Deal,
                                               MessageBody = null
                                           }
                                   };

                _aomBus.Publish(dealResponse);

                foreach (var email in notificationEmails)
                {
                    var notificationRequest = new AomSendEmailRequest
                                              {
                                                  MessageAction = MessageAction.Send,
                                                  EmailId = email
                                              };

                    _aomBus.Publish(notificationRequest);
                }

                trans.Complete();

                return hitOrder;
            }
        }
    }
}