﻿using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.OrderProcessor.Consumers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using AOM.Repository.MySql;
    using AOM.Services.OrderService;
    using AOM.Services.ProductService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public class PurgeProductOrdersConsumer : BulkOrderUpdateConsumerBase
    {
        protected readonly IDbContextFactory DbFactory;

        protected readonly IOrderService OrderService;

        private readonly IProductService _productService;

        public PurgeProductOrdersConsumer(
            IDbContextFactory dbFactory,
            IOrderService orderService,
            IBus aomBus,
            IProductService productService)
            : base(aomBus)
        {
            DbFactory = dbFactory;
            OrderService = orderService;
            _productService = productService;
            AomBus = aomBus;
        }

        internal override void SubscribeToEvents()
        {
            AomBus.Subscribe<AomProductPurgeOrdersResponse>(SubscriptionId, ConsumeAomProductPurgeOrders);
        }

        private void ConsumeAomProductPurgeOrders(AomProductPurgeOrdersResponse productPurgeOrders)
        {
            Log.InfoFormat("Purge Product Orders received for product {0}", productPurgeOrders.Product.ProductId);
            var marketStatus = _productService.GetMarketStatus(productPurgeOrders.Product.ProductId);
            try
            {
                IList<AomOrderResponse> orderResponseList = new List<AomOrderResponse>();
                using (var aomDb = DbFactory.CreateAomModel())
                {
                    var timer = Stopwatch.StartNew();

                    var openOrders =
                        OrderService.GetOpenAndHeldOrderDtosForProductId(aomDb, productPurgeOrders.Product.ProductId)
                            .ToList();

                    foreach (var orderDto in openOrders)
                    {
                        var result = OrderService.PurgeOrder(aomDb, productPurgeOrders.UserId, orderDto);

                        orderResponseList.Add(CreateOrderResponse(productPurgeOrders, result, MessageAction.Kill));
                    }

                    aomDb.SaveChangesAndLog(productPurgeOrders.UserId);
                    PublishOrderResponses(orderResponseList, aomDb, marketStatus);

                    Log.Info(
                        string.Format(
                            "Purging {0} orders for product {1} took {2} msecs",
                            openOrders.Count(),
                            productPurgeOrders.Product.ProductId,
                            timer.ElapsedMilliseconds));
                }
            }
            catch (Exception exception)
            {
                AomBus.PublishErrorResponseAndLogError<AomOrderResponse>(
                    MessageAction.Request,
                    productPurgeOrders.Message.ClientSessionInfo,
                    "Error occured whilst purging orders" + exception);
            }
        }
    }
}