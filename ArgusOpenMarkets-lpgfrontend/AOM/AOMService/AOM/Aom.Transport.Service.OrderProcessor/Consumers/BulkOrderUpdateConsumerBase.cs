﻿namespace AOM.Transport.Service.OrderProcessor.Consumers
{
    using System;
    using System.Collections.Generic;

    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql.Aom;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public abstract class BulkOrderUpdateConsumerBase : IConsumer
    {
        protected IBus AomBus;

        protected BulkOrderUpdateConsumerBase(IBus aomBus)
        {
            AomBus = aomBus;
        }

        public void Start()
        {
            SubscribeToEvents();
        }

        internal virtual void SubscribeToEvents()
        {
            throw new NotImplementedException("You must subscribe to the events to consume in this method");
        }

        protected void PublishOrderResponses(
            IEnumerable<AomOrderResponse> orderResponses,
            IAomModel aomDb,
            MarketStatus marketStatus)
        {
            foreach (var aomOrderResponse in orderResponses)
            {
                try
                {
                    AomBus.Publish(aomOrderResponse, "Market." + marketStatus);
                }
                catch (Exception ex)
                {
                    Log.Error("PublishOrderResponse", ex);
                }
            }
        }

        protected static AomOrderResponse CreateOrderResponse(
            AomResponse response,
            Order result,
            MessageAction messageAction)
        {
            var heldOrderResponse = new AomOrderResponse { Order = result, Message = response.Message };

            heldOrderResponse.Message.MessageType = MessageType.Order;
            heldOrderResponse.Message.MessageAction = messageAction;

            return heldOrderResponse;
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }
    }
}