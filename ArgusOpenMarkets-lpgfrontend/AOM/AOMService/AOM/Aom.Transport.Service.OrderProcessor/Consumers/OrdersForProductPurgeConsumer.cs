﻿namespace AOM.Transport.Service.OrderProcessor.Consumers
{
    using AOM.Repository.MySql;
    using AOM.Services.OrderService;
    using AOM.Services.ProductService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using Utils.Logging.Utils;

    public class OrdersForProductPurgeConsumer : BulkOrderUpdateConsumerBase
    {
        protected readonly IDbContextFactory DbFactory;

        protected readonly IOrderService OrderService;

        private readonly IProductService _productService;

        public OrdersForProductPurgeConsumer(
            IDbContextFactory dbFactory,
            IOrderService orderService,
            IBus aomBus,
            IProductService productService)
            : base(aomBus)
        {
            DbFactory = dbFactory;
            OrderService = orderService;
            _productService = productService;
            AomBus = aomBus;
        }

        internal override void SubscribeToEvents()
        {
            AomBus.Subscribe<PurgeOrdersForProductResponse>(SubscriptionId, ConsumePurgeOrdersForProductResponse);
        }

        private void ConsumePurgeOrdersForProductResponse(PurgeOrdersForProductResponse productPurgeOrders)
        {
            Log.InfoFormat("Purge Product Orders received for product {0}", productPurgeOrders.ProductId);
            var marketStatus = _productService.GetMarketStatus(productPurgeOrders.ProductId);
            try
            {
                IList<AomOrderResponse> orderResponseList = new List<AomOrderResponse>();
                using (var aomDb = DbFactory.CreateAomModel())
                {
                    var timer = Stopwatch.StartNew();

                    var openOrders =
                        OrderService.GetOpenAndHeldOrderDtosForProductId(aomDb, productPurgeOrders.ProductId).ToList();

                    foreach (var orderDto in openOrders)
                    {
                        var result = OrderService.PurgeOrder(aomDb, productPurgeOrders.UserId, orderDto);

                        var response = new AomOrderResponse()
                                       {
                                           Order = result,
                                           Message =
                                               new Message()
                                               {
                                                   MessageAction = MessageAction.Kill,
                                                   MessageType = MessageType.Order
                                               }
                                       };
                        orderResponseList.Add(response);
                    }

                    aomDb.SaveChangesAndLog(productPurgeOrders.UserId);
                    PublishOrderResponses(orderResponseList, aomDb, marketStatus);

                    Log.Info(
                        string.Format(
                            "Purging {0} orders for product {1} took {2} msecs",
                            openOrders.Count(),
                            productPurgeOrders.ProductId,
                            timer.ElapsedMilliseconds));
                }
            }
            catch (Exception exception)
            {
                Log.Error("Error occured whilst purging orders ", exception);

                //PublishErrorResponse(
                //    MessageAction.Request,
                //    productPurgeOrders.Message.ClientSessionInfo,
                //    "Purging Product Orders " + exception);
            }
        }
    }
}