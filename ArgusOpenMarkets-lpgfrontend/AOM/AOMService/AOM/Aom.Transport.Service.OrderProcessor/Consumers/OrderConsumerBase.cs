﻿using AOM.Services.ErrorService;
using AOM.Transport.Events.Users;

namespace AOM.Transport.Service.OrderProcessor.Consumers
{
    using App.Domain;
    using App.Domain.Entities;

    using Argus.Transport.Infrastructure;

    using Events;
    using Events.Orders;

    using Processor.Common;

    using Repository.MySql;
    using Repository.MySql.Aom;

    using Services.OrderService;
    using Services.ProductService;

    using System;

    using Utils.Logging.Utils;

    public abstract class OrderConsumerBase : IConsumer
    {
        protected readonly IDbContextFactory DBFactory;

        protected readonly IOrderService OrderService;

        private readonly IProductService _productService;

        private readonly IErrorService _errorService;

        protected IBus _aomBus;

        protected OrderConsumerBase(
            IDbContextFactory dbFactory,
            IOrderService orderService,
            IBus aomBus,
            IProductService productService, 
            IErrorService errorService)
        {
            DBFactory = dbFactory;
            OrderService = orderService;
            _aomBus = aomBus;
            _productService = productService;
            _errorService = errorService;
        }

        public string SubscriptionId
        {
            get
            {
                return "AOM";
            }
        }

        public void Start()
        {
            SubscribeToEvents();
        }

        internal virtual void SubscribeToEvents()
        {
            throw new NotImplementedException("You must subscribe to the events to consume in this method");
        }


        internal void ConsumeOrderRequest<TReq, TFwd>(TReq request) where TReq : AomOrderRequest
            where TFwd : AomOrderAuthenticationRequest, new()
        {
            string requestType = typeof(TReq).Name;

            Log.Debug(String.Format("{0} received", requestType));
            try
            {
                var forwardedRequest = new TFwd
                                       {
                                           MessageAction = request.MessageAction,
                                           ClientSessionInfo = request.ClientSessionInfo,
                                           Order = request.Order
                                       };

                _aomBus.Publish(forwardedRequest);
            }
            catch (Exception exception)
            {
                _errorService.LogUserError(new UserError
                {
                    Error = exception,
                    AdditionalInformation = String.Format("Authenticating {0}", requestType),
                    ErrorText = string.Empty,
                    Source = "ProductConsumerBase"
                });
            }
        }

        internal void ConsumeOrderResponse<TResp, TFwd>(TResp response) where TResp : AomOrderAuthenticationResponse
            where TFwd : AomOrderResponse, new()
        {
            string responseType = typeof(TResp).Name;
            var marketStatus = MarketStatus.Closed;

            Log.Debug(String.Format("{0} received", responseType));
            try
            {
                marketStatus = _productService.GetMarketStatus(response.Order.ProductId);

                if (response.Message.MessageType == MessageType.Error)
                {
                    _aomBus.PublishErrorResponseAndLogError<AomOrderResponse>(
                        response.Message.MessageAction,
                        response.Message.ClientSessionInfo,
                        response.Message.MessageBody as string,
                        CreateTopicString(marketStatus));
                }
                else
                {
                    using (IAomModel aomDb = DBFactory.CreateAomModel())
                    {
                        Order returnedOrder = ConsumeOrderResponseAdditionalProcessing(aomDb, response, marketStatus);
                        try
                        {
                            var forwardedResponse = new TFwd { Order = returnedOrder, Message = response.Message };
                            _aomBus.Publish(forwardedResponse, "Market." + marketStatus);
                        }
                        catch (Exception exception)
                        {
                            _errorService.LogUserError(new UserError
                            {
                                Error = exception,
                                AdditionalInformation = String.Format("{0} error", responseType),
                                ErrorText = string.Empty,
                                Source = "OrderConsumerBase"
                            });
                        }
                    }
                }
            }
            catch (BusinessRuleException exception)
            {
                _aomBus.PublishErrorResponseAndLogInfo<AomOrderResponse>(
                    response.Message.MessageAction,
                    response.Message.ClientSessionInfo,
                    exception.Message,
                    CreateTopicString(marketStatus));
            }
            catch (Exception exception)
            {
                var errorMessage = _errorService.LogUserError(new UserError
                {
                    Error = exception,
                    AdditionalInformation = String.Format("{0} error", responseType),
                    ErrorText = string.Empty,
                    Source = "OrderConsumerBase"
                });

                _aomBus.PublishErrorResponseAndLogError<AomOrderResponse>(
                    response.Message.MessageAction,
                    response.Message.ClientSessionInfo,
                    errorMessage,
                    CreateTopicString(marketStatus));
            }
        }

        internal virtual Order ConsumeOrderResponseAdditionalProcessing<T>(
            IAomModel aomDb,
            T response,
            MarketStatus marketStatus) where T : AomOrderAuthenticationResponse
        {
            throw new NotImplementedException(
                "You must add code in here if consuming responses. This is only called if the request being consumed is not indicating an error.");
        }

        protected string CreateTopicString(MarketStatus marketStatus)
        {
            return "Market." + marketStatus;
        }
    }
}