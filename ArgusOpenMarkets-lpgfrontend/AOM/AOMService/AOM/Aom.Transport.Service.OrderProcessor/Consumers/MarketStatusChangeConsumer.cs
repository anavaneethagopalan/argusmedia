﻿using AOM.Transport.Service.Processor.Common;

namespace AOM.Transport.Service.OrderProcessor.Consumers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql;
    using AOM.Services.OrderService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Events.Products;

    using Argus.Transport.Infrastructure;

    using Utils.Logging.Utils;

    public class MarketStatusChangeConsumer : BulkOrderUpdateConsumerBase
    {
        protected readonly IDbContextFactory DbFactory;

        protected readonly IOrderService OrderService;

        public MarketStatusChangeConsumer(IDbContextFactory dbFactory, IOrderService orderService, IBus aomBus)
            : base(aomBus)
        {
            DbFactory = dbFactory;
            OrderService = orderService;
            AomBus = aomBus;
        }

        internal override void SubscribeToEvents()
        {
            AomBus.Subscribe<AomMarketStatusChange>(SubscriptionId, ConsumeAomMarketStatusChange);
        }

        private void ConsumeAomMarketStatusChange(AomMarketStatusChange marketStatusChange)
        {
            Log.InfoFormat("Market Status {0} Change received for product {1}",marketStatusChange.Status, marketStatusChange.Product.ProductId);
            try
            {
                switch (marketStatusChange.Status)
                {
                    case MarketStatus.Closed:
                        IList<AomOrderResponse> orderResponseList = new List<AomOrderResponse>();
                        using (var aomDb = DbFactory.CreateAomModel())
                        {
                            var timer = Stopwatch.StartNew();
                            var openOrders =
                                OrderService.GetOpenOrderDtosForProductId(aomDb, marketStatusChange.Product.ProductId)
                                    .ToList();

                            foreach (var orderDto in openOrders)
                            {
                                var result = OrderService.CloseMarketHoldOrder(
                                    aomDb,
                                    marketStatusChange.Message.ClientSessionInfo.UserId,
                                    orderDto);
                                orderResponseList.Add(
                                    CreateOrderResponse(marketStatusChange, result, MessageAction.Hold));
                            }

                            aomDb.SaveChangesAndLog(marketStatusChange.Message.ClientSessionInfo.UserId);
                            PublishOrderResponses(orderResponseList, aomDb, marketStatusChange.Status);

                            Log.Info(
                                string.Format(
                                    "Setting product {0} market status to {1} for {2} orders took {3} msecs",
                                    marketStatusChange.Product.ProductId,
                                    marketStatusChange.Status,
                                    openOrders.Count(),
                                    timer.ElapsedMilliseconds));
                        }
                        break;
                }
            }

            catch (Exception exception)
            {
                AomBus.PublishErrorResponseAndLogError<AomOrderResponse>(
                    MessageAction.Request,
                    marketStatusChange.Message.ClientSessionInfo,
                    "Error occured whilst processing market status change " + exception);
            }
        }
    }
}