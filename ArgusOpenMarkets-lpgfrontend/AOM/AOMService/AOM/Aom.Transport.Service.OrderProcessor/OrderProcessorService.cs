﻿namespace AOM.Transport.Service.OrderProcessor
{
    using System.Collections.Generic;

    using AOM.Transport.Service.Processor.Common;

    using Argus.Transport.Infrastructure;

    public class OrderProcessorService : ProcessorServiceBase
    {
        public OrderProcessorService(
            IBus aomBus,
            IEnumerable<IConsumer> consumers,
            IServiceManagement serviceManagement)
            : base(consumers, serviceManagement, aomBus)
        {
        }

        public override string Name
        {
            get { return "OrderProcessorService"; }
        }
    }
}