﻿using AOM.Repository.MySql.Tests;
using AOM.Services.ErrorService;
using AOM.Services.ProductService;
using System;
using System.Collections.Generic;
using AOM.App.Domain.Dates;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Mappers;
using AOM.Repository.MySql.Aom;
using AOM.Services.AssessmentService;
using AOM.Transport.Events;
using AOM.Transport.Events.Assessments;
using AOM.Transport.Service.AssessmentProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.AssessmentProcessor.Tests.Consumers
{
    [TestFixture]
    public class AssessmentConsumerTests
    {
        private const long FakeProductId = 1;

        private readonly DateTimeProvider _dateTimeProvider = new DateTimeProvider();

        private static readonly Dictionary<string, AssessmentDto> FakeAssessments =
            new Dictionary<string, AssessmentDto>
            {
                {
                    "Today",
                    new AssessmentDto
                    {
                        BusinessDate = new DateTime(2009, 01, 24).Date,
                        ProductId = FakeProductId,
                        PriceHigh = (decimal) 999.50,
                        PriceLow = (decimal) 650.00,
                        DateCreated = DateTime.Now.ToUniversalTime(),
                        LastUpdated = DateTime.Now.ToUniversalTime(),
                        EnteredByUserId = 999,
                        LastUpdatedUserId = 999,
                        AssessmentStatus = "n"

                    }
                },
                {
                    "Previous",
                    new AssessmentDto
                    {
                        Id = 1001,
                        BusinessDate = new DateTime(2009, 01, 23).Date,
                        ProductId = FakeProductId,
                        PriceHigh = (decimal) 888.50,
                        PriceLow = (decimal) 777.00,
                        DateCreated = DateTime.Now.ToUniversalTime(),
                        LastUpdated = DateTime.Now.ToUniversalTime(),
                        EnteredByUserId = 999,
                        LastUpdatedUserId = 999,
                        AssessmentStatus = "n"
                    }
                }
            };

        private static readonly CombinedAssessment CombiAssessment = new CombinedAssessment
        {
            Today = FakeAssessments["Today"].ToEntity(),
            Previous = FakeAssessments["Previous"].ToEntity(),
            ProductId = FakeProductId
        };

        [Test]
        public void OnStartSubscribesProcessorToBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var assessmentConsumer = new AssessmentConsumer(
                mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(), mockErrorService.Object);
            assessmentConsumer.Start();

            //Assert
            mockBus.Verify(
                x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomRefreshAssessmentRequest>>()),
                Times.Once);
            mockBus.Verify(
                x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomNewAssessmentRequest>>()),
                Times.Once);
            mockBus.Verify(
                x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomAssessmentAuthenticationResponse>>()),
                Times.Once);
        }

        [Test]
        public void ConsumeInsertAssessmentRequestPublishesAuthenticationRequest()
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();
            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(), mockErrorService.Object);

            // Act
            assessmentConsumer.ConsumeInsertAssessmentRequest(
                new AomNewAssessmentRequest
                {
                    Assessment = CombiAssessment,
                    ProductId = 1,
                    ClientSessionInfo = new ClientSessionInfo {UserId = 999}
                });

            // Assert
            mockBus.Verify(
                b =>
                    b.Publish(
                        It.Is<AomAssessmentAuthenticationRequest>(
                            r =>
                                r.ProductId == FakeProductId &&
                                r.MessageAction == MessageAction.Create &&
                                r.Assessment != null)));
        }

        [Test]
        public void ConsumeRefreshAssessmentRequestPublishesAuthenticationRequest()
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            mockProductService.Setup(service => service.GetProduct(It.IsAny<long>())).Returns(new Product()
            {
                ProductId = 1,
                IsDeleted = false,
                IsInternal = false
            });

            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(), mockErrorService.Object);

            // Act
            assessmentConsumer.ConsumeRefreshAssessmentRequest(
                new AomRefreshAssessmentRequest
                {
                    ProductId = 1,
                    ClientSessionInfo = new ClientSessionInfo {UserId = 999}
                });

            // Assert
            mockBus.Verify(
                b =>
                    b.Publish(
                        It.Is<AomAssessmentAuthenticationRequest>(
                            r =>
                                r.ProductId == FakeProductId &&
                                r.MessageAction == MessageAction.Refresh &&
                                r.Assessment == null)));
        }

        [Test]
        public void ConsumeRefreshAssessmentRequestOnInternalProductDoesNotPublishAuthenticationRequest()
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            mockProductService.Setup(service => service.GetProduct(It.IsAny<long>())).Returns(new Product()
            {
                ProductId = 1,
                IsDeleted = false,
                IsInternal = true
            });

            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(), mockErrorService.Object);

            // Act
            assessmentConsumer.ConsumeRefreshAssessmentRequest(
                new AomRefreshAssessmentRequest
                {
                    ProductId = 1,
                    ClientSessionInfo = new ClientSessionInfo {UserId = 999}
                });

            // Assert
            mockBus.Verify(
                b =>
                    b.Publish(
                        It.Is<AomAssessmentAuthenticationRequest>(
                            r =>
                                r.ProductId == FakeProductId &&
                                r.MessageAction == MessageAction.Refresh &&
                                r.Assessment == null)), Times.Never);
        }

        [Test]
        public void ConsumeRefreshAssessmentRequestOnDeletedProductDoesNotPublishAuthenticationRequest()
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            mockProductService.Setup(service => service.GetProduct(It.IsAny<long>())).Returns(new Product()
            {
                ProductId = 1,
                IsDeleted = true,
                IsInternal = false
            });

            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(), mockErrorService.Object);

            // Act
            assessmentConsumer.ConsumeRefreshAssessmentRequest(
                new AomRefreshAssessmentRequest
                {
                    ProductId = 1,
                    ClientSessionInfo = new ClientSessionInfo {UserId = 999}
                });

            // Assert
            mockBus.Verify(
                b =>
                    b.Publish(
                        It.Is<AomAssessmentAuthenticationRequest>(
                            r =>
                                r.ProductId == FakeProductId &&
                                r.MessageAction == MessageAction.Refresh &&
                                r.Assessment == null)), Times.Never);
        }

        [Test]
        public void InsertAssessmentPublishesResponseIfAuthorisationGranted()
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            var fakeAssessment = new CombinedAssessment
            {
                Today = FakeAssessments["Today"].ToEntity(),
                Previous = FakeAssessments["Previous"].ToEntity(),
                ProductId = 1
            };

            mockAssessmentService.Setup(a => a.GetCombinedAssessmentForProduct(It.IsAny<long>()))
                .Returns(fakeAssessment);

            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(), mockErrorService.Object);

            // Act
            assessmentConsumer.ConsumeAuthenticationResponse(
                new AomAssessmentAuthenticationResponse
                {
                    Assessment = CombiAssessment,
                    ProductId = FakeProductId,
                    Message = new Message
                    {
                        MessageAction = MessageAction.Create,
                        MessageType = MessageType.Assessment,
                        ClientSessionInfo = new ClientSessionInfo {UserId = 999}
                    }
                });

            // Assert
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.ProductId == CombiAssessment.ProductId)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Message.MessageType == MessageType.Assessment)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Message.MessageAction == MessageAction.Update)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Assessment.Today.Id == CombiAssessment.Today.Id)));
            mockBus.Verify(
                x =>
                    x.Publish(
                        It.Is<AomNewAssessmentResponse>(i => i.Assessment.Previous.Id == CombiAssessment.Previous.Id)));
        }

        [Test]
        public void InsertAssessmentPublishesAnErrorIfAuthorisationRefused()
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            var fakeAssessment = new CombinedAssessment
            {
                Today = FakeAssessments["Today"].ToEntity(),
                Previous = FakeAssessments["Previous"].ToEntity(),
                ProductId = 1
            };

            mockAssessmentService.Setup(a => a.GetCombinedAssessmentForProduct(It.IsAny<long>()))
                .Returns(fakeAssessment);

            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(), mockErrorService.Object);

            const string fakeError = "You shall not pass";

            // Act
            assessmentConsumer.ConsumeAuthenticationResponse(
                new AomAssessmentAuthenticationResponse
                {
                    Assessment = CombiAssessment,
                    ProductId = FakeProductId,
                    Message = new Message
                    {
                        MessageAction = MessageAction.Create,
                        MessageType = MessageType.Error,
                        MessageBody = fakeError,
                        ClientSessionInfo = new ClientSessionInfo {UserId = 999}
                    }
                });

            // Assert
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.ProductId == CombiAssessment.ProductId)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Message.MessageType == MessageType.Error)));
            mockBus.Verify(
                x =>
                    x.Publish(
                        It.Is<AomNewAssessmentResponse>(i => ((string) i.Message.MessageBody).Contains(fakeError))));
        }

        [Test]
        public void RefreshAssessmentPublishesResponseIfAuthorisationGranted()
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            var fakeAssessment = new CombinedAssessment
            {
                Today = FakeAssessments["Today"].ToEntity(),
                Previous = FakeAssessments["Previous"].ToEntity(),
                ProductId = 1
            };

            mockAssessmentService.Setup(a => a.GetCombinedAssessmentForProduct(It.IsAny<long>()))
                .Returns(fakeAssessment);

            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(), mockErrorService.Object);

            // Act
            assessmentConsumer.ConsumeAuthenticationResponse(
                new AomAssessmentAuthenticationResponse
                {
                    ProductId = FakeProductId,
                    Message = new Message
                    {
                        MessageAction = MessageAction.Refresh,
                        MessageType = MessageType.Assessment,
                        ClientSessionInfo = new ClientSessionInfo {UserId = 999}
                    }
                });

            // Assert
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.ProductId == CombiAssessment.ProductId)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Message.MessageType == MessageType.Assessment)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Message.MessageAction == MessageAction.Update)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Assessment.Today.Id == CombiAssessment.Today.Id)));
            mockBus.Verify(
                x =>
                    x.Publish(
                        It.Is<AomNewAssessmentResponse>(i => i.Assessment.Previous.Id == CombiAssessment.Previous.Id)));
        }

        [Test]
        public void RefreshAssessmentPublishesAnErrorIfAuthorisationRefused()
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            var fakeAssessment = new CombinedAssessment
            {
                Today = FakeAssessments["Today"].ToEntity(),
                Previous = FakeAssessments["Previous"].ToEntity(),
                ProductId = 1
            };

            mockAssessmentService.Setup(a => a.GetCombinedAssessmentForProduct(It.IsAny<long>()))
                .Returns(fakeAssessment);

            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(),
                mockErrorService.Object);

            const string fakeError = "You shall not pass";

            // Act
            assessmentConsumer.ConsumeAuthenticationResponse(
                new AomAssessmentAuthenticationResponse
                {
                    ProductId = FakeProductId,
                    Message = new Message
                    {
                        MessageAction = MessageAction.Refresh,
                        MessageType = MessageType.Error,
                        MessageBody = fakeError,
                        ClientSessionInfo = new ClientSessionInfo {UserId = 999}
                    }
                });

            // Assert
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.ProductId == CombiAssessment.ProductId)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Message.MessageType == MessageType.Error)));
            mockBus.Verify(
                x =>
                    x.Publish(
                        It.Is<AomNewAssessmentResponse>(i => ((string) i.Message.MessageBody).Contains(fakeError))));
        }

        [Test]
        public void InsertAssessmentPublishesAnErrorIfAssessmentServiceThrowsTest()
        {
            var mockBus = new Mock<IBus>();
            var mockAssessmentService = new Mock<IAssessmentService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            mockAssessmentService.Setup(a => a.UpdateCombinedAssessment(It.IsAny<CombinedAssessment>()))
                .Throws(new Exception("Test"));

            var assessmentConsumer = new AssessmentConsumer(mockBus.Object,
                mockAssessmentService.Object,
                mockProductService.Object,
                _dateTimeProvider,
                MockDbContextFactory.Stub(),
                mockErrorService.Object);

            assessmentConsumer.ConsumeAuthenticationResponse(new AomAssessmentAuthenticationResponse
            {
                ProductId = FakeProductId,
                Assessment = CombiAssessment,
                MarketMustBeOpen = false,
                Message = new Message
                {
                    ClientSessionInfo = new ClientSessionInfo {UserId = 999},
                    MessageAction = MessageAction.Create,
                    MessageType = MessageType.Assessment
                }
            });

            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.ProductId == CombiAssessment.ProductId)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Message.MessageType == MessageType.Error)));
            mockBus.Verify(
                x => x.Publish(It.Is<AomNewAssessmentResponse>(i => i.Message.ClientSessionInfo.UserId == 999)));
        }
    }
}