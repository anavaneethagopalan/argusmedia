﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class AssessmentTopicManagerTests
    {
        [Test]
        public void ShouldReturnAssessmentTopicPathForProduct()
        {
            var assessmentPath = AssessmentTopicManager.AssessmentTopicPath(100);

            Assert.That(assessmentPath, Is.Not.Null);
            Assert.That(assessmentPath, Is.EqualTo("AOM/Products/100/Assessment"));
        }

        [Test]
        public void ShouldReturnAssessmentTopicToSubscribeUserTo()
        {
            var topicSelector = "AOM/Products/100/Assessment";
            var user = MakeUserWithProductPrivilege(100);

            var assessmentTopics = AssessmentTopicManager.SubscribeUserToAssessmentTopics(user, topicSelector);
            Assert.That(assessmentTopics, Is.Not.Null);
            Assert.That(assessmentTopics.Count, Is.EqualTo(1));
            Assert.That(assessmentTopics[0], Is.EqualTo("?AOM/Products/100/Assessment//"));
        }

        [Test]
        public void ShouldReturnNoAssessmentTopicToSubscribeUserToIfUserNotEntitledToProduct()
        {
            var topicSelector = "AOM/Products/200/Assessment";
            var user = MakeUserWithProductPrivilege(100);
           
            var assessmentTopics = AssessmentTopicManager.SubscribeUserToAssessmentTopics(user, topicSelector);
            Assert.That(assessmentTopics, Is.Not.Null);
            Assert.That(assessmentTopics.Count, Is.EqualTo(0));
        }

        private User MakeUserWithProductPrivilege(long productPrivilegeId)
        {
            var prodPrivs = new List<UserProductPrivilege>();
            prodPrivs.Add(new UserProductPrivilege
            {
                ProductId = productPrivilegeId, 
                Privileges = new Dictionary<string, bool>()
            });

            var user = new User
            {
                Id = 100,
                ProductPrivileges = prodPrivs
            };

            return user; 
        }
    }
}
