﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class OrderTopicManagerTests
    {
        [Test]
        public void ShouldReturnTheOrdersRootPathForATopic()
        {
            var orderTopicPathForProducts = OrderTopicManager.CreateProductTopicPath(1);
            Assert.That(orderTopicPathForProducts, Is.EqualTo("AOM/Orders/Products/1"));
        }

        [Test]
        public void ShouldReturnTheAllOrdersPathForActiveOrders()
        {
            var orderTopicPathForProducts = OrderTopicManager.CreateAllOrdersRootTopicPath(1);
            Assert.That(orderTopicPathForProducts, Is.EqualTo("AOM/Orders/Products/1/All"));
        }

        [Test]
        public void ShouldReturnTheHeldOrdersTopicPathWhereAnOrderIsForAPrincipalOrganisation()
        {
            var orderTopicPathForProducts = OrderTopicManager.CreateHeldOrderTopicPaths(1, ExecutionMode.Internal, 100,
                500, null);

            Assert.That(orderTopicPathForProducts.Count, Is.EqualTo(1));
            Assert.That(orderTopicPathForProducts[0], Is.EqualTo("AOM/Orders/Products/1/500/100"));
        }

        [Test]
        public void ShouldReturnTheHeldOrdersTopicPathWhereAnOrderIsForAPrincipalAndBroker()
        {
            var orderTopicPathForProducts = OrderTopicManager.CreateHeldOrderTopicPaths(1, ExecutionMode.Internal, 100,
                500, 510);

            Assert.That(orderTopicPathForProducts.Count, Is.EqualTo(2));
            Assert.That(orderTopicPathForProducts[0], Is.EqualTo("AOM/Orders/Products/1/500/100"));
            Assert.That(orderTopicPathForProducts[1], Is.EqualTo("AOM/Orders/Products/1/510/100"));
        }

        [Test]
        public void ShouldReturnAListOfTopicsAUserCanSubscribeTo()
        {
            var topicsToSubscribe = OrderTopicManager.SubscribeUserToOrderTopicsForProduct(100, 500);

            Assert.That(topicsToSubscribe.Count, Is.EqualTo(2));
            Assert.That(topicsToSubscribe[0], Is.EqualTo("?AOM/Orders/Products/100/All//"));
            Assert.That(topicsToSubscribe[1], Is.EqualTo("?AOM/Orders/Products/100/500//"));
        }

        private IUser MakeUserWithPrivilegesForProduct(int productId)
        {
            List<UserProductPrivilege> productPrivs = new List<UserProductPrivilege>();
            productPrivs.Add(new UserProductPrivilege { ProductId = productId });
            var user = new User
            {
                Id = 1,
                OrganisationId = 500,
                ProductPrivileges = productPrivs
            };

            return user;
        }
    }
}
