﻿using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class ContentStreamTopicManagerTests
    {
        [Test]
        public void ShouldReturnContentStreamTopicPath()
        {
            var assessmentPath = ContentStreamTopicManager.ContentStreamTopicPath(100);

            Assert.That(assessmentPath, Is.Not.Null);
            Assert.That(assessmentPath, Is.EqualTo("AOM/Products/100/ContentStreams"));
        }
    }
}
