﻿using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class OrganisationTopicManagerTests
    {
        [Test]
        [TestCase("AOM/Organisations/60/SubscribedProducts")]
        [TestCase("AOM/Organisations/60/PermissionsMatrix/Summary/Products/")]
        [TestCase("AOM/Organisations/60/SystemNotifications/")]
        [TestCase("AOM/Organisations/60/SystemNotificationTypes/")]
        public void ShouldSubscribeUserToSubscribedProductsForHisOrganisation(string topic)
        {
            var user = MakeUser(60);
            var subscribedTopic = OrganisationTopicManager.SubscribeUserToOrganisationTopics(user, topic);
            Assert.That(subscribedTopic.Count, Is.EqualTo(1));
            Assert.That(subscribedTopic[0], Is.EqualTo(TopicHelper.MakeWildCardTopic(topic)));
        }

        [Test]
        [TestCase("AOM/Organisations/60/SubscribedProducts", "AOM/Organisations/100/SubscribedProducts", 100)]
        [TestCase("AOM/Organisations/60/PermissionsMatrix/Summary/Products/", "AOM/Organisations/100/PermissionsMatrix/Summary/Products/", 100)]
        [TestCase("AOM/Organisations/60/SystemNotifications/", "AOM/Organisations/100/SystemNotifications/", 100)]
        [TestCase("AOM/Organisations/60/SystemNotificationTypes/", "AOM/Organisations/100/SystemNotificationTypes/", 100)]
        public void ShouldOverrideTheSubscribeTopicRequestUsingTheUsersOrganisation(string topicRequest, string actualTopicToSubscribeTo, long userOrganisationId)
        {
            var user = MakeUser(100);
            var subscribedTopic = OrganisationTopicManager.SubscribeUserToOrganisationTopics(user, topicRequest);
            Assert.That(subscribedTopic.Count, Is.EqualTo(1));
            Assert.That(subscribedTopic[0], Is.EqualTo(TopicHelper.MakeWildCardTopic(actualTopicToSubscribeTo)));
        }

        private IUser MakeUser(long organisationId)
        {
            return new User {Id = 1, Username = "fred", OrganisationId = organisationId};
        }
    }
}
