﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.Services.AuthenticationService;
using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class MarketTickerTopicManagerTests
    {
        [Test]
        public void ShouldSubscribeUserToMarketTickerTopics()
        {
            var user = MakeUser(false);

            var userTopics = MarketTickerTopicManager.SubscribeUserToMarketTickerTopics(user.Id, 1, 100, user.ProductPrivileges[0].Privileges);

            Assert.That(userTopics, Is.Not.Null);
            Assert.That(userTopics.Count, Is.EqualTo(2));
            Assert.That(userTopics[0], Is.EqualTo("?AOM/MarketTicker/Products/1/All//"));
            Assert.That(userTopics[1], Is.EqualTo("?AOM/MarketTicker/Products/1/100//"));
        }
        
        [Test]
        public void ShouldReturnAWildCardSubscriptionToEditorTopicsForRelevantProduct()
        {
            var user = MakeUser(true);
            //var privileges = new Dictionary<string, Boolean> { { ProductPrivileges.MarketTicker.Create, false } };
            //privileges.Add(ProductPrivileges.MarketTicker.Authenticate, true);
            
            var topicsToSubscribeUserTo = MarketTickerTopicManager.SubscribeUserToMarketTickerTopics(user.Id, 1, 100, user.ProductPrivileges[0].Privileges);
            Assert.That(topicsToSubscribeUserTo, Is.Not.Null);
            Assert.That(topicsToSubscribeUserTo.Count, Is.EqualTo(3));
            Assert.That(topicsToSubscribeUserTo.Contains("?AOM/MarketTicker/Products/1/Editor//"));
        }

        private IUser MakeUser(bool userHasAuthenticateMarketTickerRole)
        {
            var privileges = new Dictionary<string, Boolean> {{ProductPrivileges.MarketTicker.Create, false}};

            if (userHasAuthenticateMarketTickerRole)
            {
                privileges.Add(ProductPrivileges.MarketTicker.Authenticate, true);
            }

            var productPrivileges = new List<UserProductPrivilege>();
            productPrivileges.Add(new UserProductPrivilege {ProductId = 1, Privileges = privileges});

            var user = new User
            {
                Id = 1,
                Username = "Fred",
                ProductPrivileges = productPrivileges
            };

            return user;
        }
    }
}