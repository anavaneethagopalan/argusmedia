﻿using AOM.App.Domain.Entities;
using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class UserTopicManagerTests
    {
        [Test]
        public void ShouldReturnUserTopicPathFromUserId()
        {
            var path = UserTopicManager.UserTopicPath(100);
            Assert.That(path, Is.EqualTo("AOM/Users/100"));
        }

        [Test]
        public void ShouldReturnUserTopicPathFromUserObject()
        {
            var user = new User
            {
                Id = 100
            };
            var path = UserTopicManager.GetUserTopic(user);
            Assert.That(path, Is.EqualTo("AOM/Users/100"));
        }

        [Test]
        public void ShouldReturnEmptyStringForUserTopicPathWhenUserObjectNull()
        {
            var path = UserTopicManager.GetUserTopic(null);
            Assert.That(path, Is.EqualTo(string.Empty));
        }
    }
}