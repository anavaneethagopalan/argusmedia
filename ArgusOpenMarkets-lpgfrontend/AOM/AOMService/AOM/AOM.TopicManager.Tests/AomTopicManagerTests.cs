﻿using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.App.Domain.Services;
using Moq;
using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class AomTopicManagerTests
    {
        private IAomTopicManager _aomTopicManager;

        private Mock<IUserService> _mockUserService;

        [SetUp]
        public void AomTopicManagerTestsSetup()
        {
            _mockUserService = new Mock<IUserService>();
            var mockUserWithPrivs = MakeUserWithPrivs(new long[]{1});
            var userContentStreams = new List<long?> {100};

            _mockUserService.Setup(m => m.GetUserWithPrivileges(It.IsAny<long>())).Returns(mockUserWithPrivs);
            _mockUserService.Setup(m => m.GetUserContentStreams(It.IsAny<long>())).Returns(userContentStreams);
            _aomTopicManager = new AomTopicManager(_mockUserService.Object);
        }

        [Test]
        public void GetTopicsForUserShouldReturnUserTopic()
        {
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("AOM/Users/1"));
        }

        [Test]
        public void GetTopicsForUserShouldReturnProductTopic()
        {
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("?AOM/Products/1//"));
        }

        [Test]
        public void GetTopicsForUserShouldReturnAllOrdersForProductsUserEntitled()
        {
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("?AOM/Orders/Products/1/All//"));
        }

        [Test]
        public void GetTopicsForUserShouldReturnOrdersForUsersOrganisationForProductEntitled()
        {
            // User Organisation ID = 500
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("?AOM/Orders/Products/1/500//"));
        }

        [Test]
        public void GetTopicsForUserShouldReturnFreeNews()
        {
            // User Organisation ID = 500
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("?AOM/News/Free//"));
        }

        [Test]
        public void GetTopicsForUserShouldReturnNewsBasedUponUsersContentStreams()
        {
            // User Organisation ID = 500
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("?AOM/News/ContentStreams/100//"));
        }

        [Test]
        public void GetTopicsForUserShouldReturnUsersOrganisation()
        {
            // User Organisation ID = 500
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("?AOM/Organisations/500//"));
        }

        [Test]
        public void GetTopicsForUserShouldReturnAllMarketTickerForProductsUserIsEntitled()
        {
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("?AOM/MarketTicker/Products/1/All//"));
        }

        [Test]
        public void GetTopicsForUserShouldReturnMarketTickerForProductsUserIsEntitledBasedUponUsersOrganisation()
        {
            var topicsToSubscribe = _aomTopicManager.GetAllTopicsToSubscribeForUser(1);
            Assert.That(topicsToSubscribe.Contains("?AOM/MarketTicker/Products/1/500//"));
        }

        private IUser MakeUserWithPrivs(long[] productIds)
        {
            List<UserProductPrivilege> productPrivs = new List<UserProductPrivilege>();

            foreach (var productId in productIds)
            {
                productPrivs.Add(new UserProductPrivilege {ProductId = 1, Privileges = new Dictionary<string, bool>()});
            }

            var user = new User
            {
                Id = 1,
                OrganisationId = 500,
                ProductPrivileges = productPrivs
            };

            return user;
        }
    }
}
