﻿using System;
using System.Collections.Generic;
using AOM.App.Domain.Entities;
using AOM.App.Domain.Interfaces;
using AOM.Services.AuthenticationService;
using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class ProductTopicManagerTests
    {
        [Test]
        public void ShouldReturnProductTopicPath()
        {
            var productDefTopicPath = ProductTopicManager.ProductTopicPath(100);

            Assert.That(productDefTopicPath, Is.EqualTo("AOM/Products/100"));
        }

        [Test]
        public void ShouldReturnMarketStatusTopicPath()
        {
            var productDefTopicPath = ProductTopicManager.MarketStatusTopicPath(100);

            Assert.That(productDefTopicPath, Is.EqualTo("AOM/Products/100/MarketStatus"));
        }

        [Test]
        public void ShouldSubscribeAUserToProductDefinitionPathIfUserIsSubscribedToProduct()
        {
            var topics = ProductTopicManager.SubscribeUserToProductTopics(100);

            Assert.That(topics.Count, Is.EqualTo(1));
            Assert.That(topics[0], Is.EqualTo("?AOM/Products/100//"));
        }

        [Test]
        public void ShouldSubscribeAUserToMarketStatusPathIfUserIsSubscribedToProduct()
        {
            var topics = ProductTopicManager.SubscribeUserToProductTopics(101);

            Assert.That(topics.Count, Is.EqualTo(1));
            Assert.That(topics[0], Is.EqualTo("?AOM/Products/101//"));
        }

        private IUser MakeUser(long productIdUserSubscribedTo)
        {
            var privileges = new Dictionary<string, Boolean>();
            privileges.Add(ProductPrivileges.MarketTicker.Create, false);

            var productPrivileges = new List<UserProductPrivilege>();
            productPrivileges.Add(new UserProductPrivilege { ProductId = productIdUserSubscribedTo, Privileges = privileges });

            var user = new User
            {
                Id = 1,
                Username = "Fred",
                ProductPrivileges = productPrivileges
            };

            return user;
        }
    }
}