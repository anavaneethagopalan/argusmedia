﻿using System.Collections.Generic;
using NUnit.Framework;

namespace AOM.TopicManager.Tests
{
    [TestFixture]
    public class NewsTopicManagerTests
    {
        [Test]
        public void SubscribeUserToNewsTopicsShouldReturnNewsTopicIfUserEntitledToContentStream()
        {
            var newsPathsToSubscribe = NewsTopicManager.SubscribeUserToNewsTopicsForContentStream(9058);

            Assert.That(newsPathsToSubscribe, Is.Not.Null);
            Assert.That(newsPathsToSubscribe[0], Is.EqualTo("?AOM/News/ContentStreams/9058//"));
        }


        [Test]
        public void ShouldReturnFreeTopicForNewsIfFree()
        {
            ulong commodityId = 1;
            string cmsId = "45353";
            bool isFree = true;
            long contentStreamId = 9058;
            var newsTopic = NewsTopicManager.NewsTopicPath(commodityId, cmsId, isFree, contentStreamId);

            Assert.That(newsTopic, Is.EqualTo("AOM/News/Free/1/45353"));
        }

        [Test]
        public void ShouldReturnContentStreamTopicForNewsIfNotFree()
        {
            ulong commodityId = 1;
            string cmsId = "45353";
            bool isFree = false;
            long contentStreamId = 9058;
            var newsTopic = NewsTopicManager.NewsTopicPath(commodityId, cmsId, isFree, contentStreamId);

            Assert.That(newsTopic, Is.EqualTo("AOM/News/ContentStreams/9058/45353"));
        }
    }
}
