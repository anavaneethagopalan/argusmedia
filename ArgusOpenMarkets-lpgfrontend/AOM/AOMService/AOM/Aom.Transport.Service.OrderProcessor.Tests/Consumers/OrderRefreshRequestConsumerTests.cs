﻿using AOM.Services.ErrorService;

namespace AOM.Transport.Service.OrderProcessor.Tests.Consumers
{
    using AOM.App.Domain.Entities;
    using AOM.Repository.MySql;
    using AOM.Repository.MySql.Aom;
    using AOM.Services.OrderService;
    using AOM.Services.ProductService;
    using AOM.Transport.Events;
    using AOM.Transport.Events.Orders;
    using AOM.Transport.Service.OrderProcessor.Consumers;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    using System;
    using System.Collections.Generic;

    [TestFixture]
    internal class OrderRefreshRequestConsumerTests
    {
        private Mock<IDbContextFactory> _mockDbContextFactory;

        private Mock<IOrderService> _mockOrderService;

        private Mock<IBus> _mockBus;

        private Mock<IProductService> _mockProductService;

        [SetUp]
        public void Setup()
        {
            _mockDbContextFactory = new Mock<IDbContextFactory>();
            _mockOrderService = new Mock<IOrderService>();
            _mockBus = new Mock<IBus>();
            _mockProductService = new Mock<IProductService>();
        }

        [Test]
        public void
            ConsumeAomOrderRefreshRequestShouldPublishAomOrderRefreshResponseMessageOnTheBusForAllOrdersTodayForSpecifiedProduct
            ()
        {
            _mockOrderService.Setup(m => m.GetOrdersForTodayByProductId(It.IsAny<IAomModel>(), It.IsAny<long>()))
                .Returns(GetOrdersForTodayByProduct(true));
            var mockErrorService = new Mock<IErrorService>();

            var orderRefreshRequestConsumer = new OrderRefreshRequestConsumer(
                _mockDbContextFactory.Object,
                _mockOrderService.Object,
                _mockBus.Object,
                _mockProductService.Object,
                mockErrorService.Object);

            var aomOrderRefreshRequest = MakeAomOrderRefreshRequest();
            orderRefreshRequestConsumer.ConsumeAomOrderRefreshRequest(aomOrderRefreshRequest);

            _mockBus.Verify(m => m.Publish(It.IsAny<AomOrderRefreshResponse>()), Times.Exactly(4));
        }

        [Test]
        public void ConsumeAomOrderRefreshRequestShouldThrowAnExceptionIfOrderIsNotValid()
        {
            _mockOrderService.Setup(m => m.GetOrdersForTodayByProductId(It.IsAny<IAomModel>(), It.IsAny<long>()))
                .Returns(GetOrdersForTodayByProduct(false));
            var mockErrorService = new Mock<IErrorService>();

            var orderRefreshRequestConsumer = new OrderRefreshRequestConsumer(
                _mockDbContextFactory.Object,
                _mockOrderService.Object,
                _mockBus.Object,
                _mockProductService.Object,
                mockErrorService.Object);

            var aomOrderRefreshRequest = MakeAomOrderRefreshRequest();
            orderRefreshRequestConsumer.ConsumeAomOrderRefreshRequest(aomOrderRefreshRequest);

            _mockBus.Verify(m => m.Publish(It.IsAny<AomOrderResponse>()), Times.Exactly(1));
        }

        private static AomOrderRefreshRequest MakeAomOrderRefreshRequest()
        {
            return new AomOrderRefreshRequest
                   {
                       BusinessDay = DateTime.Now,
                       ClientSessionInfo =
                           new ClientSessionInfo
                           {
                               OrganisationId = 1,
                               SessionId = "",
                               UserId = 1
                           },
                       MessageAction = MessageAction.Check,
                       ProductId = 1
                   };
        }

        private IEnumerable<Order> GetOrdersForTodayByProduct(bool makeValidOrders)
        {
            yield return MakeOrder(1, makeValidOrders);
            yield return MakeOrder(2, makeValidOrders);
            yield return MakeOrder(3, makeValidOrders);
            yield return MakeOrder(4, makeValidOrders);
        }

        private Order MakeOrder(long id, bool orderIsValid)
        {
            var order = new Order { Id = id, ExecutionMode = ExecutionMode.External, OrderType = OrderType.Bid };

            if (orderIsValid)
            {
                order.OrderStatus = OrderStatus.Executed;
            }

            return order;
        }
    }
}