﻿using System;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Tests;
using AOM.Services.ErrorService;
using AOM.Services.OrderService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Service.OrderProcessor.Consumers;
using Argus.Transport.Infrastructure;
using log4net.Config;
using Moq;
using NUnit.Framework;

[assembly: XmlConfigurator(Watch = true)]

namespace AOM.Transport.Service.OrderProcessor.Tests.Consumers
{
    [TestFixture]
    public class EditOrderRequestConsumerTests
    {
        private AomOrderRequest _editOrderRequest;
        private AomOrderAuthenticationResponse _authenticatedEditOrder;

        [TestFixtureSetUp]
        public void Setup()
        {
            var clientSessionInfo = new ClientSessionInfo {UserId = 12, SessionId = "TestSessionId"};
            var order = new Order {Id = 11111, Notes = "Edit Test Note", Price = 100, ProductId = 1};
            var msg = new Message
            {
                ClientSessionInfo = clientSessionInfo,
                MessageAction = MessageAction.Update,
                MessageType = MessageType.Order
            };
            _editOrderRequest = new AomOrderRequest
            {
                MessageAction = MessageAction.Update,
                Order = order,
                ClientSessionInfo = clientSessionInfo
            };
            _authenticatedEditOrder = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Order = order,
                Message = msg
            };
        }

        [Test]
        public void OnStartSubscribesProcessorToBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IOrderService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var editOrderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null,
                mockBus.Object, mockProductService.Object, mockErrorService.Object);
            editOrderConsumer.Start();

            //Assert
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomOrderRequest>>()), Times.Once);
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomOrderAuthenticationResponse>>()),
                Times.Once);
        }

        [Test]
        public void ConsumeEditOrderRequestPutsOrderOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IOrderService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var editOrderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null,
                mockBus.Object, mockProductService.Object, mockErrorService.Object);
            editOrderConsumer.ConsumeOrderRequest<AomOrderRequest, AomOrderAuthenticationRequest>(_editOrderRequest);

            //Assert
            mockBus.Verify(
                x =>
                    x.Publish(
                        It.Is<AomOrderAuthenticationRequest>(
                            r => (r.Order.Notes.Equals("Edit Test Note") && r.ClientSessionInfo.UserId == 12))),
                Times.Once);
        }

//        [Test]
//        public void ConsumeEditOrderRequestPublishFailurePublishesErrorMessage()
//        {
//            //Arrange
//            var mockBus = new Mock<IBus>();
//            var mockOrderService = new Mock<IOrderService>();
//            var mockProductService = new Mock<IProductService>();
//
//            mockBus.Setup(x => x.Publish(It.IsAny<AomOrderAuthenticationRequest>())).Throws(new BusinessRuleException("Test Error"));
//
//            //Act
//            var editOrderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null, mockBus.Object, mockProductService.Object);
//            editOrderConsumer.ConsumeOrderRequest<AomOrderRequest, AomOrderAuthenticationRequest>(_editOrderRequest);
//
//            //Assert
//            mockBus.Verify(x => x.Publish(It.Is<AomOrderResponse>(r => (r.Message.MessageType == MessageType.Error))), Times.Once);
//        }

        [Test]
        public void ConsumeAomOrderAuthenticationRequestResponsetoCallCreateEditOrder()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IOrderService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var editOrderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null,
                mockBus.Object, mockProductService.Object, mockErrorService.Object);
            editOrderConsumer.ConsumeOrderResponse<AomOrderAuthenticationResponse, AomOrderResponse>(
                _authenticatedEditOrder);

            //Assert
            mockOrderService.Verify(
                x =>
                    x.EditOrder(MockDbContextFactory.Stub().CreateAomModel(),
                        It.Is<AomOrderAuthenticationResponse>(r => (r.Order.Notes.Equals("Edit Test Note"))),
                        MarketStatus.Closed), Times.Once);
        }

        [Test]
        public void SendErrorMessageWhenNewOrderReturnsErrorMessage()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IOrderService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();
            mockProductService.Setup(x => x.GetMarketStatus(1)).Returns(MarketStatus.Open);

            mockOrderService.Setup(
                x =>
                    x.EditOrder(MockDbContextFactory.Stub().CreateAomModel(), It.IsAny<AomOrderAuthenticationResponse>(),
                        MarketStatus.Open)).Throws(new BusinessRuleException("Test"));


            //Act
            var editOrderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null,
                mockBus.Object, mockProductService.Object, mockErrorService.Object);
            editOrderConsumer.ConsumeOrderResponse<AomOrderAuthenticationResponse, AomOrderResponse>(
                _authenticatedEditOrder);

            //Assert
            mockOrderService.Verify(
                x =>
                    x.EditOrder(MockDbContextFactory.Stub().CreateAomModel(),
                        It.Is<AomOrderAuthenticationResponse>(r => (r.Order.Notes.Equals("Edit Test Note"))),
                        MarketStatus.Open), Times.Once);
            mockBus.Verify(
                x =>
                    x.Publish(It.Is<AomOrderResponse>(r => r.Message.MessageType == MessageType.Error),
                        "Market." + MarketStatus.Open));
        }
    }
}