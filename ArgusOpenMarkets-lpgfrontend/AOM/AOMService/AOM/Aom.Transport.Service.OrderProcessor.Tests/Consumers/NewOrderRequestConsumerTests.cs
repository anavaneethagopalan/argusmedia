﻿using System;
using AOM.App.Domain;
using AOM.App.Domain.Entities;
using AOM.Repository.MySql.Tests;
using AOM.Services.ErrorService;
using AOM.Services.OrderService;
using AOM.Services.ProductService;
using AOM.Transport.Events;
using AOM.Transport.Events.Orders;
using AOM.Transport.Service.OrderProcessor.Consumers;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;

namespace AOM.Transport.Service.OrderProcessor.Tests.Consumers
{
    [TestFixture]
    public class NewOrderRequestConsumerTests
    {
        private AomOrderRequest _newOrderRequest;
        private AomOrderAuthenticationResponse _authenticatedNewOrder;

        [TestFixtureSetUp]
        public void Setup()
        {
            var clientSessionInfo = new ClientSessionInfo {UserId = 12, SessionId = "TestSessionId"};
            var order = new Order {Id = 11111, Notes = "Tushara", Price = 100};
            var msg = new Message
            {
                ClientSessionInfo = clientSessionInfo,
                MessageAction = MessageAction.Create,
                MessageType = MessageType.Order
            };
            _newOrderRequest = new AomOrderRequest
            {
                MessageAction = MessageAction.Create,
                Order = order,
                ClientSessionInfo = clientSessionInfo
            };
            _authenticatedNewOrder = new AomOrderAuthenticationResponse
            {
                MarketMustBeOpen = false,
                Order = order,
                Message = msg
            };
        }

        [Test]
        public void OnStartSubscribesProcessorToBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IOrderService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var newOrderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null,
                mockBus.Object, mockProductService.Object, mockErrorService.Object);
            newOrderConsumer.Start();

            //Assert
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomOrderRequest>>()), Times.Once);
            mockBus.Verify(x => x.Subscribe(It.IsAny<string>(), It.IsAny<Action<AomOrderAuthenticationResponse>>()),
                Times.Once);
        }

        [Test]
        public void ConsumeNewOrderRequestPutsOrderOnBus()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IOrderService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();


            //Act
            var newOrderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null,
                mockBus.Object, mockProductService.Object, mockErrorService.Object);
            newOrderConsumer.ConsumeOrderRequest<AomOrderRequest, AomOrderAuthenticationRequest>(_newOrderRequest);

            //Assert
            mockBus.Verify(
                x =>
                    x.Publish(
                        It.Is<AomOrderAuthenticationRequest>(
                            r => (r.Order.Notes.Equals("Tushara") && r.ClientSessionInfo.UserId == 12))), Times.Once);
        }

        [Test]
        public void ConsumeAomOrderAuthenticationRequestResponsetoCallNewOrder()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IOrderService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();

            //Act
            var orderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null,
                mockBus.Object, mockProductService.Object, mockErrorService.Object);
            orderConsumer.ConsumeOrderResponse<AomOrderAuthenticationResponse, AomOrderResponse>(_authenticatedNewOrder);

            //Assert
            mockOrderService.Verify(
                x =>
                    x.NewOrder(MockDbContextFactory.Stub().CreateAomModel(),
                        It.Is<AomOrderAuthenticationResponse>(r => (r.Order.Notes.Equals("Tushara"))),
                        MarketStatus.Closed), Times.Once);
        }

        [Test]
        public void SendErrorMessageWhenNewOrderReturnsErrorMessage()
        {
            //Arrange
            var mockBus = new Mock<IBus>();
            var mockOrderService = new Mock<IOrderService>();
            var mockProductService = new Mock<IProductService>();
            var mockErrorService = new Mock<IErrorService>();
            mockOrderService.Setup(
                x =>
                    x.NewOrder(MockDbContextFactory.Stub().CreateAomModel(), It.IsAny<AomOrderAuthenticationResponse>(),
                        MarketStatus.Closed)).Throws(new BusinessRuleException("Test"));

            //Act
            var OrderConsumer = new OrderRequestConsumer(MockDbContextFactory.Stub(), mockOrderService.Object, null,
                mockBus.Object, mockProductService.Object, mockErrorService.Object);
            OrderConsumer.ConsumeOrderResponse<AomOrderAuthenticationResponse, AomOrderResponse>(_authenticatedNewOrder);

            //Assert
            mockOrderService.Verify(
                x =>
                    x.NewOrder(MockDbContextFactory.Stub().CreateAomModel(),
                        It.Is<AomOrderAuthenticationResponse>(r => (r.Order.Notes.Equals("Tushara"))),
                        MarketStatus.Closed), Times.Once);
            mockBus.Verify(
                x =>
                    x.Publish(It.Is<AomOrderResponse>(r => r.Message.MessageType == MessageType.Error),
                        "Market.Closed"));
        }
    }
}