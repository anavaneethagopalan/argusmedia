﻿namespace AOM.Transport.Service.OrderProcessor.Tests.Consumers
{
    using AOM.Repository.MySql;
    using AOM.Services.OrderService;
    using AOM.Services.ProductService;
    using AOM.Transport.Service.OrderProcessor.Consumers;

    using Argus.Transport.Infrastructure;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class PurgeProductOrdersConsumerTests
    {
        [Test]
        public void ShouldHaveAnEmptySubscriptionId()
        {
            var mockDbContextFactory = new Mock<IDbContextFactory>();
            var mockOrderService = new Mock<IOrderService>();
            var mockBus = new Mock<IBus>();
            var mockProductService = new Mock<IProductService>();

            var purgeProductOrdersRequest = new PurgeProductOrdersConsumer(mockDbContextFactory.Object, 
                mockOrderService.Object, mockBus.Object, mockProductService.Object);

            Assert.That(purgeProductOrdersRequest.SubscriptionId, Is.EqualTo("AOM"));
        }
    }
}
