﻿namespace AOM.Transport.Service.OrderProcessor.Tests
{
    using System.Linq;

    using AOM.Repository.MySql;
    using AOM.Services;
    using AOM.Transport.Service.OrderProcessor.Consumers;
    using AOM.Transport.Service.Processor.Common;

    using Ninject;

    using NUnit.Framework;

    using Utils.Javascript.Tests.Extension;

    [TestFixture]
    public class BootstrapperTests
    {
        [Test]
        public void LoadsExpectedIConsumerListTest()
        {
            using (var kernel = new StandardKernel())
            {
                kernel.Load(
                    new LocalBootstrapper(),
                    new EFModelBootstrapper(),
                    new ServiceBootstrap(),
                    new ProcessorCommonBootstrap());

                var bindings = kernel.GetAll(typeof(IConsumer)).ToList();

                AssertHelper.AssertListOnlyContains(
                    bindings,
                    x => x.GetType(),
                    typeof(MarketStatusChangeConsumer),
                    typeof(OrderRequestConsumer),
                    typeof(OrderRefreshRequestConsumer),
                    typeof(OrdersForProductPurgeConsumer),
                    typeof(PurgeProductOrdersConsumer),
                    typeof(HealthCheckConsumer));
            }
        }

    }
}