﻿using System;
using System.Collections.Generic;
using AOM.Transport.Service.Processor.Common;
using Argus.Transport.Infrastructure;
using Moq;
using NUnit.Framework;
using Topshelf;

namespace AOM.Transport.Service.OrderProcessor.Tests
{
    [TestFixture]
    public class OrderProcessorServiceTests
    {
        private Mock<IBus> _mockBus;
        private Mock<IServiceManagement> _mockServiceManagement;

        [TestFixtureSetUp]
        public void Setup()
        {
             _mockBus = new Mock<IBus>();
             _mockServiceManagement = new Mock<IServiceManagement>();
        }

        [Test]
        public void ProcessorStartsSingleConsumer()
        {
            //Arrange
            var mockHostControl = new Mock<HostControl>();
            var mockConsumer = new Mock<IConsumer>();
            var consumers = new List<IConsumer> {mockConsumer.Object};

            //Act
            var orderProcessor = new OrderProcessorService(_mockBus.Object, consumers, _mockServiceManagement.Object);
            orderProcessor.Start(mockHostControl.Object);

            //Assert
            mockConsumer.Verify(x => x.Start());

        }

        [Test]
        public void ProcessorStartsMultipleConsumers()
        {
            //Arrange
            var mockConsumer1 = new Mock<IConsumer>();
            var mockConsumer2 = new Mock<IConsumer>();
            var mockHostControl = new Mock<HostControl>();
            var consumers = new List<IConsumer> {mockConsumer1.Object, mockConsumer2.Object};

            //Act
            var orderProcessor = new OrderProcessorService(_mockBus.Object, consumers, _mockServiceManagement.Object);
            orderProcessor.Start(mockHostControl.Object);

            //Assert
            mockConsumer1.Verify(x => x.Start());
            mockConsumer2.Verify(x => x.Start());
        }

        [Test]
        public void ProcessorThrowsExceptionIfConsumerFailsToStart()
        {
            //Arrange
            var mockHostControl = new Mock<HostControl>();
            var mockConsumer = new Mock<IConsumer>();
            mockConsumer.Setup(x => x.Start()).Throws(new Exception("Test"));
            var consumers = new List<IConsumer> { mockConsumer.Object };

            //Act
            var orderProcessor = new OrderProcessorService(_mockBus.Object, consumers, _mockServiceManagement.Object);
           
            //Assert
            Assert.Throws<Exception>(() => orderProcessor.Start(mockHostControl.Object));
            mockConsumer.Verify(x => x.Start());        
        }
    }
}
