/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {
    /**
     * The `build_dir` folder is where our projects are compiled during
     * development and the `compile_dir` folder is where our app resides once it's
     * completely built.
     */
    build_dir: 'build',
    compile_dir: 'bin',

    /**
     * This is a collection of file patterns that refer to our app code (the
     * stuff in `src/`). These file paths are used in the configuration of
     * build tasks. `js` is all project javascript, less tests. `ctpl` contains
     * our reusable components' (`src/common`) template HTML files, while
     * `atpl` contains the same, but for our app's code. `html` is just our
     * main HTML file, `less` is our main stylesheet, and `unit` contains our
     * app's unit tests.
     */
    app_files: {
        js: ['src/**/*.js', '!src/**/*.spec.js', '!src/**/*.mock.js', '!src/**/*.pro.js'],
        jsunit: ['src/**/*.spec.js', 'src/**/*.mock.js', 'vendor/moment/moment.js'],
        config: ['src/**/*.config'],
        html: ['src/**/*.html'],
        less: ['vendor/bootstrap/dist/css/bootstrap.css','src/less/main.less','src/less/login.less',
            'src/less/chosen.less',
            'src/less/news.less',
            'src/less/marketticker.less',
            'src/less/assessment.less'
        ],
        defaultless: ['vendor/bootstrap/dist/css/bootstrap.css','src/default.less/main.less','src/default.less/login.less',
            'src/default.less/chosen.less',
            'src/default.less/news.less',
            'src/default.less/marketticker.less',
            'src/default.less/assessment.less'
        ]
    },

    /**
     * This is a collection of files used during testing only.
     */
    test_files: {
        js: [
            'vendor/angular-mocks/angular-mocks.js',
            'vendor/sinon/lib/sinon.js',
            'vendor/sinon/lib/sinon/spy.js',
            'vendor/sinon/lib/sinon/call.js',
            'vendor/sinon/lib/sinon/match.js',
            'vendor/sinon/lib/sinon/stub.js'
        ]
    },

    /**
     * This is the same as `app_files`, except it contains patterns that
     * reference vendor code (`vendor/`) that we need to place into the build
     * process somewhere. While the `app_files` property ensures all
     * standardized files are collected for compilation, it is the user's job
     * to ensure non-standardized (i.e. vendor-related) files are handled
     * appropriately in `vendor_files.js`.
     *
     * The `vendor_files.js` property holds files to be automatically
     * concatenated and minified with our project source files.
     *
     * The `vendor_files.css` property holds any CSS files to be automatically
     * included in our app.
     *
     * The `vendor_files.assets` property holds any assets to be copied along
     * with our app's assets. This structure is flattened, so it is not
     * recommended that you use wildcards.
     */
    vendor_files: {
        js: [
            'vendor/jquery/dist/jquery.js',
            'vendor/jquery-ui/jquery-ui.js',
            'vendor/jquery-ui/ui/datepicker.js',
            'vendor/jquery-ui/ui/slider.js',
            'vendor/angular/angular.js',
            'vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'vendor/angular-ui-router/release/angular-ui-router.js',
            'vendor/angular-resource/angular-resource.js',
            'vendor/angular-ui-utils/ui-utils.js',
            'vendor/angular-ui-utils/ui-utils-ieshiv.js',
            'vendor/angular-cookies/angular-cookies.js',
            'vendor/es5-shim/es5-shim.js',
            'vendor/angular-gridster/src/angular-gridster.js',
            'vendor/ng-grid/build/ng-grid.js',
            'vendor/angular-toastr/dist/angular-toastr.tpls.js',
            'vendor/diffusion/diffusion.uncompressed.js',
            'vendor/diffusion/angularDiffusion.js',
            'vendor/angulartics/src/angulartics.js',
            'vendor/angulartics/src/angulartics-ga.js',
            'vendor/components-underscore/underscore.js',
            'vendor/qtip2/jquery.qtip.js',
            'vendor/has/has.js',
            'vendor/chosen/chosen.jquery.js',
            'vendor/angular-chosen-localytics/chosen.js',
            'vendor/angular-sanitize/angular-sanitize.js',
            'vendor/perfect-scrollbar/src/perfect-scrollbar.js',
            'vendor/angular-perfect-scrollbar/src/angular-perfect-scrollbar.js',
            'vendor/moment/moment.js',
            'vendor/multiselect/multiselect.js',
            'vendor/platform/platform.js',
            'vendor/satellizer/satellizer.js'
        ],
        css: [

            'vendor/jquery-ui/themes/smoothness/jquery-ui.css',
            'vendor/ng-grid/ng-grid.css',
            'vendor/angular-toastr/dist/angular-toastr.css',
            'vendor/chosen/chosen.css',
            'vendor/angular-chosen-localytics/chosen-spinner.css'
        ],
        assets: [
            'vendor/bootstrap/dist/css/bootstrap.css.map',
            'src/app/config/userDashboardConfig.json',
            'src/app/config/userConfig.json',
            'src/app/config/defaultTileConfig.json',
            'src/app/config/diffusionConfig.json',
            'src/app/config/applicationConfig.json',
            'src/app/config/productConfig.json',
            'src/app/config/webServiceConfig.json',
            'vendor/chosen/chosen-sprite.png'

        ],
        assetimages: [

        ]
    }
};
