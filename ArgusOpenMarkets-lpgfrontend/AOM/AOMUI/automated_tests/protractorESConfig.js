// Node packages that are required for the automated testing:
// npm install jasmine-spec-reporter
// npm install jasmine-reporters@1.0.0
// npm install keyword
// npm install async
// npm install replace
// npm install fs-extra
// npm install ya-csv
// npm install

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',

    allScriptsTimeout: 300000,

    onPrepare: function () {

        var SpecReporter = require('jasmine-spec-reporter');
        require('jasmine-reporters');
        jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: true}));
        browser.driver.manage().window().maximize();
        //browser.driver.manage().window().setSize(1366, 768);

        var thisRunId = Math.floor(Math.random() * 10000);
        var username = browser.params.credentials.username;
        var password = browser.params.credentials.password;

        var browserName;
        browser.getCapabilities().then(function (cap) {
            browserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        });

        var replace = require("replace");
        replace({
            regex: "key.webdriver = require",
            replacement: "//key.webdriver = require ",
            paths: ['../node_modules/keyword/lib/keyword.js'],
            recursive: false,
            silent: true
        });

        var d = new Date();
        fs = require('fs');
        browser.params.saveFolder = browser.params.saveFolder + "/" + d.getFullYear() + "." + ('0' + (d.getMonth()+1)).slice(-2) + "." + ('0' + d.getDate()).slice(-2) + "." + browserName;
        if (!fs.existsSync(browser.params.saveFolder)) {
            fs.mkdir(browser.params.saveFolder);
        }

        // *******   jasmine-reporters v1.0.0  **** npm install jasmine-reporters@1.0.0 ****
        jasmine.getEnv().addReporter(new jasmine.JUnitXmlReporter(browser.params.saveFolder, true, true, browserName + ".")); // + "/" + browserName + "."));
    },

    onComplete: function () {
    },

    params: {

        //testEnv: 'dev',
        testEnv: 'local',       // <-- change this to change the test environment eg URL

        name1: 'value1',
        saveFolder: '/temp',

        hostURL: {
            local: 'http://localhost/AOM/#/Dashboard', ///#/Dashboard',
            dev: 'http://web.dev.aom.dev.argusmedia.com/', //#/Dashboard',
            live: 'http://aomweb.argusmedia.com/', //#/Dashboard',
            systest: 'http://web.systest.aom.dev.argusmedia.com' //#/Dashboard'
        },

        local: {
            url: 'http://web.aom.com/' //#/Dashboard'
        },

        credentials: {
            username: 'john',
            password: 'password'
        }

    },

    jasmineNodeOpts: {
        DefaultTimeoutInterval: 600000
    },

    'loggingPrefs': {
        'browser': 'ALL'
    },

    reporters: ['spec', 'coverage']
};

