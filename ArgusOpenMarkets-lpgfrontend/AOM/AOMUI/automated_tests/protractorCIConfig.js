// usage:
// running directly using protractor:
//      protractor protractorCIConfig.js --capabilities.browserName=chrome --capabilities.shardTestFiles=true --capabilities.maxInstances=9
//
// running from grunt:
//      grunt intTest --testenv=local
//      grunt serialTest --testenv=local
//      grunt parallelTest --testenv=local --browser=chrome --maxinstances=[number]

// Node packages that are required for the automated testing:
// npm install jasmine-spec-reporter --save-dev
// npm install jasmine-reporters@1.0.0
// npm install keyword
// npm install async
// npm install fs-extra
// npm install replace
// npm install dictionaryjs
// npm install ya-csv

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',

    allScriptsTimeout: 300000,

    onPrepare: function () {

        require('jasmine-reporters');
        var SpecReporter = require('jasmine-spec-reporter');
        jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: true}));
        browser.driver.manage().window().maximize();
        //browser.driver.manage().window().setSize(1920, 1080);

        var thisRunId = Math.floor(Math.random() * 10000);
        var username = browser.params.credentials.username;
        var password = browser.params.credentials.password;

        //console.log(browser.params);

        var browserName;
        browser.getCapabilities().then(function (cap) {
            browserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        });

        var d = new Date();
        fs = require('fs');
        browser.params.saveFolder = browser.params.saveFolder + "/" + d.getFullYear() + "." + ('0' + (d.getMonth()+1)).slice(-2) + "." + ('0' + d.getDate()).slice(-2) + "." + browserName;
        if (!fs.existsSync(browser.params.saveFolder)) {
            fs.mkdir(browser.params.saveFolder);
        }

        // *******   jasmine-reporters v1.0.0  **** npm install jasmine-reporters@1.0.0 ****
        if (process.env.TEAMCITY_VERSION)
        {
            jasmine.getEnv().addReporter(new jasmine.TeamcityReporter());
        }
        jasmine.getEnv().addReporter(new jasmine.JUnitXmlReporter(browser.params.saveFolder, true, true, browserName + ".")); // + "/" + browserName + "."));
    },

    onComplete: function () {
    },

    params: {

        //testEnv: 'dev',
        testEnv: 'local',       // <-- change this to change the test environment eg URL

        name1: 'value1',
        saveFolder: '/temp',

        hostURL: {
            local: 'http://localhost/AOM/#/Dashboard', ///#/Dashboard',
            dev: 'http://web.dev.aom.dev.argusmedia.com/', //#/Dashboard',
            live: 'http://aomweb.argusmedia.com/', //#/Dashboard',
            systest: 'http://web.systest.aom.dev.argusmedia.com/' //#/Dashboard'
        },

        local: {
            url: 'http://web.aom.com/' //#/Dashboard'
        },

        credentials: {
            username: 'john',
            password: 'password'
        }

    },

    jasmineNodeOpts: {
        DefaultTimeoutInterval: 600000
    },

    'loggingPrefs': {
        'browser': 'ALL'
    },

    reporters: ['spec', 'coverage']
};

