var Resources = require("../helpers/resources.js");
var DashboardPage = require("../pages/dashboardpage");
var NewOrderPage = require("../pages/neworderpage");
var EditOrderPage = require("../pages/editorderpage");
var ExecuteOrderPage = require("../pages/executeorderpage");
var Async = require("async");
var key = require('keyword');
var csv = require('ya-csv');
try { key(require('./just.private.keywords')); } catch(err) { }
var retVal;
var ModalCloseTime = 500;

SetRet = function(retValue)
{
    retVal = retValue;
};

GetRet = function()
{
    return retVal;
};

CreateOrderFunction = function(product, bidask, quantity, price, notes ) {

    GetProductIndexFunction(product).then(function(productId)
    {
    var dashboard = new DashboardPage(); // get the dashboard page.
        console.log("prodId: " + productId + " - " + product)

    if (bidask.substr(0, 1).toUpperCase() === 'B')  {
            dashboard.newBid(productId);
    } else {
            dashboard.newAsk(productId);
    }
    var newOrder = new NewOrderPage();
    newOrder.setPrice(price);
    newOrder.setQuantity(quantity);
    newOrder.setNotes(notes);
    //newOrder.setDeliveryStart(params.deliverystart);
    //newOrder.setDeliveryEnd(params.deliveryend);
    newOrder.create();
    });
};

HoldOrderFunction = function(orderCellIndex) {

    if(orderCellIndex > -1) {
        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var editOrderPage = new EditOrderPage();

        expect(editOrderPage.displayed()).toEqual(true);

        editOrderPage.hold();

        return true;
    }
    else {
        return false;
    }
};

ReinstateOrderFunction = function(orderCellIndex) {

    if(orderCellIndex > -1) {

        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var editOrderPage = new EditOrderPage();

        expect(editOrderPage.displayed()).toEqual(true);

        editOrderPage.reinstate();

        return true;
    }
    else {
        return false;
    }
};

KillOrderFunction = function(orderCellIndex) {

    if(orderCellIndex > -1) {

        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var editOrderPage = new EditOrderPage();

        expect(editOrderPage.displayed()).toEqual(true);

        editOrderPage.kill();

        browser.sleep(ModalCloseTime); // this is because the order takes some time to disappear

        return true;
    }
    else {
        return false;
    }
};

ExecuteOrderFunction = function(orderCellIndex) {

    if(orderCellIndex > -1) {

        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var executeOrderPage = new ExecuteOrderPage();

        expect(executeOrderPage.displayed()).toEqual(true);

        executeOrderPage.execute();

        browser.sleep(ModalCloseTime); // this is because the order takes some time to disappear

        return true;
    }
    else {
        return false;
    }
};

UpdateOrderFunction = function(orderCellIndex, price, quantity, notes) {

    if(orderCellIndex > -1) {

        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var editOrderPage = new EditOrderPage();

        expect(editOrderPage.displayed()).toEqual(true);

        if(price != "")
        {
            editOrderPage.setPrice(price);
        }

        if(quantity != "")
        {
            editOrderPage.setQuantity(quantity);
        }

        editOrderPage.setNotes(notes);

        editOrderPage.update();

        browser.sleep(ModalCloseTime); // this is because the order takes some time to disappear

        return true;
    }
    else {
        return false;
    }
};

GetProductCountFunction = function()
{
    var dashboard = new DashboardPage();
    var iProductCount;

    return dashboard.getProductTitles().then(function (ProductTitles) {
        iProductCount = ProductTitles.length;
        return iProductCount;
    });
};

GetProductIndexFunction = function(product) {

    var dashboard = new DashboardPage();
    var iProductCount;

    return dashboard.getProductTitles().then(function (ProductTitles) {
        iProductCount = ProductTitles.length;

        for(var i = 0; i < iProductCount; i++)
        {
            if(ProductTitles[i].text == product)
            {
                return ProductTitles[i].index;
            }
        }
    });
};

GetStackRowsCountFunction = function(productCount) {

    var dashboard = new DashboardPage();
    var iRows;

    return dashboard.allBidAskRows().then(function (BidAskRows) {
        iRows = BidAskRows.length / (productCount * 2);

        return iRows;
    });
};

GetStartCellIndexFunction = function(bidAsk, productCount, productIndex, rowCount) {

    var iStartCellIndex;

    if (bidAsk === 'BID') {
        iStartCellIndex = productIndex * productCount * rowCount;
    } else {
        iStartCellIndex = productIndex * productCount * rowCount + rowCount;
    }

    return iStartCellIndex;
};

IsInStackFunction = function(inOrNotIn, retryIterationCount, maxRetryWait, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price) {
    var dashboard = new DashboardPage();
    var rowFound = false;
    var keepLooking = true;
    var sQuantity;
    var sPrice;
    var arrOrderElements;
    var endCellIndex = startCellIndex + rowCount - 1;
    var topCellIndex;
    var fromTop;
    var iCell = startCellIndex;
    var rowText;
    var orderCellIndex = -1;
    var retryCount = 5;
    var retryWaitTime = 5000;

    if(retryIterationCount != "" && retryIterationCount != 0 && typeof retryIterationCount != 'undefined') retryCount = retryIterationCount;
    if(maxRetryWait != "" && maxRetryWait != 0 && typeof maxRetryWait != 'undefined') retryWaitTime = maxRetryWait;

    if (bidAsk === 'BID') {
        topCellIndex = productIndex * productCount * rowCount;
    } else {
        topCellIndex = productIndex * productCount * rowCount + rowCount;
    }
    endCellIndex = topCellIndex + rowCount - 1;

    return dashboard.allBidAskRows().then(function (temp) {
        Async.whilst(
            function() {
                if (keepLooking == false)
                {
                    if (inOrNotIn == "In"){ // IsIn
                        if (orderCellIndex == -1)
                        {
                            if (--retryCount > 0)
                            {
                                browser.sleep(retryWaitTime / (retryCount));
                                keepLooking = true;
                                iCell = topCellIndex;
                            }
                        }
                    } else {        //IsNotIn
                        if(orderCellIndex != -1)
                        {
                            if (--retryCount > 0)
                            {
                                browser.sleep(retryWaitTime / (retryCount));
                                keepLooking = true;
                                iCell = topCellIndex;
                            }
                        }
                    }
                }
                return keepLooking;
            },

            function(callback) {
                dashboard.allBidAskRows().then(function (BidAskRows) {
                    BidAskRows[iCell].getText().then(function (RowText) {
                        if (RowText.length > 0) {
                            rowText = RowText;
                            arrOrderElements = RowText.split(/\n/g);

                            if(bidAsk == 'BID')
                            {
                                if (arrOrderElements.length === 5) {
                                    sQuantity = arrOrderElements[3];
                                    sPrice = arrOrderElements[4];
                                } else {
                                    sQuantity = arrOrderElements[2];
                                    sPrice = arrOrderElements[3];
                                }
                            }
                            else
                            {
                                if (arrOrderElements.length === 5) {
                                    sQuantity = arrOrderElements[1];
                                    sPrice = arrOrderElements[0];
                                } else {
                                    sQuantity = arrOrderElements[1];
                                    sPrice = arrOrderElements[0];
                                }
                            }

                            if (Number(sQuantity.replace(',', '')) === Number(quantity)) {
                                if (Number(sPrice.replace(',', '')) === Number(price)) {
                                    console.log("[" + iCell + "]" + "B/A:" + bidAsk + ",Q:"+ quantity + ",P:" + price);
                                    rowFound = true;
                                    keepLooking = false;
                                    orderCellIndex = iCell;
                                }
                            }
                        } else { // Blank means no more rows
                            console.log("blank row");
                            if (inOrNotIn != "In")
                            {
                                orderCellIndex = -1;
                            }
                            keepLooking = false;
                        } // if non-blank row
                        if (keepLooking == true) {
                            iCell++;
                            if (iCell > endCellIndex){
                                console.log("Before expand: " + (BidAskRows.length / (productCount * 2)) + " rows, topcell= " + topCellIndex + ", currentcell=" + iCell + ", lastcell= " + (topCellIndex + (BidAskRows.length / (productCount * 2)) - 1));
                                fromTop = iCell - topCellIndex;
                                dashboard.expandGrid(productIndex);
                                dashboard.allBidAskRows().then(function (BidAskRows) {

                                    if (BidAskRows.length == endCellIndex) {
                                        keepLooking = false;
                                    }
                                    if (bidAsk === 'BID') {
                                        topCellIndex = productIndex * productCount * (BidAskRows.length / (productCount * 2));
                                    } else {
                                        topCellIndex = productIndex * productCount * (BidAskRows.length / (productCount * 2)) + (BidAskRows.length / (productCount * 2));
                                    }
                                    iCell = topCellIndex + fromTop;
                                    endCellIndex = topCellIndex + (BidAskRows.length / (productCount * 2)) - 1;
                                    console.log("After expand: " + (BidAskRows.length / (productCount * 2)) + " rows, topcell= " + topCellIndex + ", currentcell=" + iCell + ", lastcell= " + (topCellIndex + (BidAskRows.length / (productCount * 2)) - 1));
                                });
                            }
                        }
                        setTimeout(callback, 0);
                    }); // BidAskRows[iRow].getText().then(function (RowText) {
                });
            },
            function (err) {
                SetRet(orderCellIndex);
                return orderCellIndex;
            }
        ); // async.whilst
    });
};

GetColourIndexForRow = function(productId, bidAsk, rowIndex) {

    var colourCellIndex;

    if (bidAsk === 'BID') {
        colourCellIndex =  (((productId * 2) + 1) * 6) + (rowIndex * 6) + 5;
    } else {
        colourCellIndex = (((productId * 2) + 2) * 6) + (rowIndex * 6) + 2;
    }

    return colourCellIndex;
};

IsInStackFunctionWithColour = function(inOrNotIn, retryIterationCount, maxRetryWait, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price, colour, style) {
    var resources = new Resources();
    var dashboard = new DashboardPage();
    var rowFound = false;
    var keepLooking = true;
    var sQuantity;
    var sPrice;
    var arrOrderElements;
    var endCellIndex = startCellIndex + rowCount - 1;
    var topCellIndex;
    var fromTop;
    var iCell = startCellIndex;
    var rowText;
    var orderCellIndex = -1;
    var retryCount = 5;
    var retryWaitTime = 5000;

    if(retryIterationCount != "" && retryIterationCount != 0 && typeof retryIterationCount != 'undefined') retryCount = retryIterationCount;
    if(maxRetryWait != "" && maxRetryWait != 0 && typeof maxRetryWait != 'undefined') retryWaitTime = maxRetryWait;

    if (bidAsk === 'BID') {
        topCellIndex = productIndex * productCount * rowCount;
    } else {
        topCellIndex = productIndex * productCount * rowCount + rowCount;
    }
    endCellIndex = topCellIndex + rowCount - 1;

    return dashboard.allBidAskRows().then(function (temp) {
        Async.whilst(
            function() {
                if (keepLooking == false)
                {
                    if (inOrNotIn == "In"){ // IsIn
                        if (orderCellIndex == -1)
                        {
                            if (--retryCount > 0)
                            {
                                browser.sleep(retryWaitTime / (retryCount));
                                keepLooking = true;
                                iCell = topCellIndex;
                            }
                        }
                    } else {        //IsNotIn
                        if(orderCellIndex != -1)
                        {
                            if (--retryCount > 0)
                            {
                                browser.sleep(retryWaitTime / (retryCount));
                                keepLooking = true;
                                iCell = topCellIndex;
                            }
                        }
                    }
                }
                return keepLooking;
            },

            function(callback) {
                dashboard.allBidAskRows().then(function (BidAskRows) {
                    browser.debugger();
                    dashboard.allBidAskRowsColumns().then(function (BidAskRowsColumns) {
                        BidAskRowsColumns[GetColourIndexForRow(productIndex, bidAsk, iCell)].getCssValue("color").then(function (Color) {
                            BidAskRows[iCell].getText().then(function (RowText) {
                                if (RowText.length > 0) {
                                    rowText = RowText;
                                    arrOrderElements = RowText.split(/\n/g);

                                    if(bidAsk == 'BID')
                                    {
                                        if (arrOrderElements.length === 5) {
                                            sQuantity = arrOrderElements[3];
                                            sPrice = arrOrderElements[4];
                                        } else {
                                            sQuantity = arrOrderElements[2];
                                            sPrice = arrOrderElements[3];
                                        }
                                    }
                                    else
                                    {
                                        if (arrOrderElements.length === 5) {
                                            sQuantity = arrOrderElements[1];
                                            sPrice = arrOrderElements[0];
                                        } else {
                                            sQuantity = arrOrderElements[1];
                                            sPrice = arrOrderElements[0];
                                        }
                                    }

                                    if (Number(sQuantity.replace(',', '')) === Number(quantity)) {
                                        if (Number(sPrice.replace(',', '')) === Number(price)) {
                                            if(resources.CompareColour(Color, colour))
                                            {
                                                console.log("[" + iCell + "]" + "B/A:" + bidAsk + ",Q:"+ quantity + ",P:" + price + " - " + Color + " (" + colour + ")");
                                                rowFound = true;
                                                keepLooking = false;
                                                orderCellIndex = iCell;
                                            }
                                        }
                                    }
                                } else { // Blank means no more rows
                                    console.log("blank row");
                                    if (inOrNotIn != "In")
                                    {
                                        orderCellIndex = -1;
                                    }
                                    keepLooking = false;
                                } // if non-blank row
                                if (keepLooking == true) {
                                    iCell++;
                                    if (iCell > endCellIndex){
                                        console.log("Before expand: " + (BidAskRows.length / (productCount * 2)) + " rows, topcell= " + topCellIndex + ", currentcell=" + iCell + ", lastcell= " + (topCellIndex + (BidAskRows.length / (productCount * 2)) - 1));
                                        fromTop = iCell - topCellIndex;
                                        dashboard.expandGrid(productIndex);
                                        dashboard.allBidAskRows().then(function (BidAskRows) {

                                            if (BidAskRows.length == endCellIndex) {
                                                keepLooking = false;
                                            }
                                            if (bidAsk === 'BID') {
                                                topCellIndex = productIndex * productCount * (BidAskRows.length / (productCount * 2));
                                            } else {
                                                topCellIndex = productIndex * productCount * (BidAskRows.length / (productCount * 2)) + (BidAskRows.length / (productCount * 2));
                                            }
                                            iCell = topCellIndex + fromTop;
                                            endCellIndex = topCellIndex + (BidAskRows.length / (productCount * 2)) - 1;
                                            console.log("After expand: " + (BidAskRows.length / (productCount * 2)) + " rows, topcell= " + topCellIndex + ", currentcell=" + iCell + ", lastcell= " + (topCellIndex + (BidAskRows.length / (productCount * 2)) - 1));
                                        });
                                    }
                                }
                                setTimeout(callback, 0);
                            }); // BidAskRows[iRow].getText().then(function (RowText) {
                        }); // BidAskRowsColumns[GetColourIndexForRow(productIndex, bidAsk, iCell)].getCssValue("color").then(function (Color) {
                    }); // BidAskRowsColumns[iCell].getText().then(function (ColText) {
                });
            },
            function (err) {
                SetRet(orderCellIndex);
                return orderCellIndex;
            }
        ); // async.whilst
    });
};

var OrderPrivateKeywords = {

    "Order.GetProductCount": function(next ) {

        var dashboard = new DashboardPage();
        var iProductCount;

        dashboard.getProductTitles().then(function (ProductTitles) {
            iProductCount = ProductTitles.length;
            next(iProductCount);
        });
    },

    "Order.GetProductIndex": function(next, product) {

        var dashboard = new DashboardPage();
        var iProductCount;

        dashboard.getProductTitles().then(function (ProductTitles) {
            iProductCount = ProductTitles.length;

            for(var i = 0; i < iProductCount; i++)
            {
                if(ProductTitles[i].text == product)
                {
                    next(ProductTitles[i].index);
                }
            }
        });
    },

    "Order.GetStackRowsCount": function(next, productCount) {

        var dashboard = new DashboardPage();
        var iRows;

        dashboard.allBidAskRows().then(function (BidAskRows) {
            iRows = BidAskRows.length / (productCount * 2);
            //iColumns = BidAskRows.length / iRows;

            next(iRows);
        });
    },

    "Order.GetStartCellIndex": function(next, bidAsk, productCount, productIndex, rowCount) {

        var iStartCellIndex;

        if (bidAsk === 'BID') {
            iStartCellIndex = productIndex * productCount * rowCount;
        } else {
            iStartCellIndex = productIndex * productCount * rowCount + rowCount;
        }

        next(iStartCellIndex);
    },

    "Order.ExpandGridOnce": function(next, productIndex, productCount, oRowCount) {
        var dashboard = new DashboardPage();

        dashboard.expandGrid(productIndex);
        dashboard.allBidAskRows().then(function (BidAskRows) {
            oRowCount.value = BidAskRows.length / (productCount * 2);
            next( oRowCount.value);
        });
    },

    "Order.IsInStack": function(next, retryIterationCount, maxRetryWait, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price) {

        IsInStackFunction("In", retryIterationCount, maxRetryWait, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result) {
            next(GetRet());
        });
    },

    "Order.IsInStackWithColour": function(next, retryIterationCount, maxRetryWait, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price, colour, style) {
        IsInStackFunctionWithColour("In", retryIterationCount, maxRetryWait, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price, colour, style).then(function(result) {
            next(GetRet());
        });
    },

    "Order.IsNotInStack": function(next, retryIterationCount, maxRetryWait, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price) {

        IsInStackFunction("NotIn", retryIterationCount, maxRetryWait, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result) {
            next(GetRet());
        });
    },

    "Order.CreateFromFile": function(next, retryIterationCount, retryWaitTime, fileName, fileStartIndex, fileEndIndex) {

        var startIndex = 0;
        var endIndex = 999;
        var retryCount = 5;
        var waitTime = 875;

        if(fileStartIndex != "" && typeof fileStartIndex != 'undefined') startIndex = fileStartIndex;
        if(fileEndIndex != "" && typeof fileEndIndex != 'undefined') endIndex = fileEndIndex;
        if(retryIterationCount != "" && retryIterationCount == 0 && typeof retryIterationCount == 'undefined') retryCount = retryIterationCount;
        if(retryWaitTime != "" && retryIterationCount == 0 && typeof retryWaitTime == 'undefined') waitTime = retryWaitTime;

        var reader = csv.createCsvFileReader(".\\" + fileName, {columnsFromHeader:true, 'separator': ','});

        var currentIndex = 0;
        reader.addListener('data', function(data)
        {
            console.log("Currently Reading index: " + currentIndex);

            if(currentIndex >= startIndex && currentIndex <= endIndex)
            {
                console.log("Creating Order at File Index: " + currentIndex);
                console.log("   - Product: " + data['product']);
                console.log("   - BidAsk: " + data['bidask']);
                console.log("   - Quantity: " + data['quantity']);
                console.log("   - Price: " + data['price']);
                console.log("   - Notes: " + data['notes']);

                CreateOrderFunction(data['product'], data['bidask'], data['quantity'], data['price'], data['notes']);

                browser.sleep(ModalCloseTime);
            }
            currentIndex++;
        });

        reader.addListener('end', function()
        {
            console.log('read entire file - end of file reached');
        });
    },

    "Order.HoldFromFile": function(next, retryIterationCount, retryWaitTime, fileName, fileStartIndex, fileEndIndex) {

        var startIndex = 0;
        var endIndex = 999;
        var retryCount = 5;
        var waitTime = 875;

        if(fileStartIndex != "" && typeof fileStartIndex != 'undefined') startIndex = fileStartIndex;
        if(fileEndIndex != "" && typeof fileEndIndex != 'undefined') endIndex = fileEndIndex;

        var reader = csv.createCsvFileReader(".\\" + fileName, {columnsFromHeader:true, 'separator': ','});

        var currentIndex = 0;
        reader.addListener('data', function(data)
        {
            if(currentIndex >= startIndex && currentIndex <= endIndex)
            {
                console.log("Holding Order at File Index: " + currentIndex);
                console.log("   - Product: " + data['product']);
                console.log("   - BidAsk: " + data['bidask']);
                console.log("   - Quantity: " + data['quantity']);
                console.log("   - Price: " + data['price']);
                console.log("   - Notes: " + data['notes']);

                if(retryIterationCount != "" && retryIterationCount == 0 && typeof retryIterationCount == 'undefined') retryCount = retryIterationCount;
                if(retryWaitTime != "" && retryIterationCount == 0 && typeof retryWaitTime == 'undefined') waitTime = retryWaitTime;

                var product = data['product'];
                var bidAsk = data['bidask'];
                var quantity = data['quantity'];
                var price = data['price'];
                var notes = data['notes'];

                var productCount = -1;
                var productIndex;
                var rowCount;
                var startCellIndex;
                var orderCellIndex = -1;
                var orderHeld = false;

                Async.series([
                    function(callback){
                        GetProductCountFunction().then(function(result)
                        {
                            productCount = result;
                            callback(null, productCount);
                        });
                    },
                    function(callback){
                        GetProductIndexFunction(product).then(function(result)
                        {
                            productIndex = result;
                            callback(null, productIndex);
                        });
                    },
                    function(callback){
                        GetStackRowsCountFunction(productCount).then(function(result)
                        {
                            rowCount = result;
                            callback(null, rowCount);
                        });
                    },
                    function(callback){
                        startCellIndex = GetStartCellIndexFunction(bidAsk, productCount, productIndex, rowCount);
                        callback(null, startCellIndex);
                    },
                    function(callback){
                        IsInStackFunction("In", retryCount, waitTime, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result)
                        {
                            orderCellIndex = GetRet();
                            callback(null, GetRet());
                        });
                    },
                    function(callback){
                        orderHeld = HoldOrderFunction(orderCellIndex);
                        callback(null, orderHeld);
                    }
                ],function(err, results)
                {
                    console.log(results);
                    expect(orderCellIndex > -1).toBeTruthy();
                    expect(orderHeld).toEqual(true);
                });
            }
            browser.sleep(ModalCloseTime);
            currentIndex++;
        });

        reader.addListener('end', function()
        {
            console.log('read entire file - end of file reached');
        });
    },

    "Order.ReinstateFromFile": function(next, retryIterationCount, retryWaitTime, fileName, fileStartIndex, fileEndIndex) {

        var startIndex = 0;
        var endIndex = 999;
        var retryCount = 5;
        var waitTime = 875;

        if(fileStartIndex != "" && typeof fileStartIndex != 'undefined') startIndex = fileStartIndex;
        if(fileEndIndex != "" && typeof fileEndIndex != 'undefined') endIndex = fileEndIndex;

        var reader = csv.createCsvFileReader(".\\" + fileName, {columnsFromHeader:true, 'separator': ','});

        var currentIndex = 0;
        reader.addListener('data', function(data)
        {
            if(currentIndex >= startIndex && currentIndex <= endIndex)
            {
                console.log("Reinstating Order at File Index: " + currentIndex);
                console.log("   - Product: " + data['product']);
                console.log("   - BidAsk: " + data['bidask']);
                console.log("   - Quantity: " + data['quantity']);
                console.log("   - Price: " + data['price']);
                console.log("   - Notes: " + data['notes']);

                if(retryIterationCount != "" && retryIterationCount == 0 && typeof retryIterationCount == 'undefined') retryCount = retryIterationCount;
                if(retryWaitTime != "" && retryIterationCount == 0 && typeof retryWaitTime == 'undefined') waitTime = retryWaitTime;

                var product = data['product'];
                var bidAsk = data['bidask'];
                var quantity = data['quantity'];
                var price = data['price'];
                var notes = data['notes'];

                var productCount = -1;
                var productIndex;
                var rowCount;
                var startCellIndex;
                var orderCellIndex = -1;
                var orderReinstated = false;

                Async.series([
                    function(callback){
                        GetProductCountFunction().then(function(result)
                        {
                            productCount = result;
                            callback(null, productCount);
                        });
                    },
                    function(callback){
                        GetProductIndexFunction(product).then(function(result)
                        {
                            productIndex = result;
                            callback(null, productIndex);
                        });
                    },
                    function(callback){
                        GetStackRowsCountFunction(productCount).then(function(result)
                        {
                            rowCount = result;
                            callback(null, rowCount);
                        });
                    },
                    function(callback){
                        startCellIndex = GetStartCellIndexFunction(bidAsk, productCount, productIndex, rowCount);
                        callback(null, startCellIndex);
                    },
                    function(callback){
                        IsInStackFunction("In", retryCount, waitTime, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result)
                        {
                            orderCellIndex = GetRet();
                            callback(null, GetRet());
                        });
                    },
                    function(callback){
                        orderReinstated = ReinstateOrderFunction(orderCellIndex);
                        callback(null, orderReinstated);
                    }
                ],function(err, results)
                {
                    console.log(results);
                    expect(orderCellIndex > -1).toBeTruthy();
                    expect(orderReinstated).toEqual(true);
                });
            }
            browser.sleep(ModalCloseTime);
            currentIndex++;
        });

        reader.addListener('end', function()
        {
            console.log('read entire file - end of file reached');
        });
    },

    "Order.KillFromFile": function(next, retryIterationCount, retryWaitTime, fileName, fileStartIndex, fileEndIndex) {

        var startIndex = 0;
        var endIndex = 999;
        var retryCount = 5;
        var waitTime = 250;

        if(fileStartIndex != "" && typeof fileStartIndex != 'undefined') startIndex = fileStartIndex;
        if(fileEndIndex != "" && typeof fileEndIndex != 'undefined') endIndex = fileEndIndex;

        var reader = csv.createCsvFileReader(".\\" + fileName, {columnsFromHeader:true, 'separator': ','});

        var currentIndex = 0;
        reader.addListener('data', function(data)
        {
            if(currentIndex >= startIndex && currentIndex <= endIndex)
            {
                console.log("Killing Order at File Index: " + currentIndex);
                console.log("   - Product: " + data['product']);
                console.log("   - BidAsk: " + data['bidask']);
                console.log("   - Quantity: " + data['quantity']);
                console.log("   - Price: " + data['price']);
                console.log("   - Notes: " + data['notes']);

                if(retryIterationCount != "" && retryIterationCount == 0 && typeof retryIterationCount == 'undefined') retryCount = retryIterationCount;
                if(retryWaitTime != "" && retryIterationCount == 0 && typeof retryWaitTime == 'undefined') waitTime = retryWaitTime;

                var product = data['product'];
                var bidAsk = data['bidask'];
                var quantity = data['quantity'];
                var price = data['price'];
                var notes = data['notes'];

                var productCount = -1;
                var productIndex;
                var rowCount;
                var startCellIndex;
                var orderCellIndex = -1;
                var orderKilled = false;

                Async.series([
                    function(callback){
                        GetProductCountFunction().then(function(result)
                        {
                            productCount = result;
                            callback(null, productCount);
                        });
                    },
                    function(callback){
                        GetProductIndexFunction(product).then(function(result)
                        {
                            productIndex = result;
                            callback(null, productIndex);
                        });
                    },
                    function(callback){
                        GetStackRowsCountFunction(productCount).then(function(result)
                        {
                            rowCount = result;
                            callback(null, rowCount);
                        });
                    },
                    function(callback){
                        startCellIndex = GetStartCellIndexFunction(bidAsk, productCount, productIndex, rowCount);
                        callback(null, startCellIndex);
                    },
                    function(callback){
                        IsInStackFunction("In", 0, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result)
                        {
                            orderCellIndex = GetRet();
                            callback(null, GetRet());
                        });
                    },
                    function(callback){
                        orderKilled = KillOrderFunction(orderCellIndex);
                        callback(null, orderKilled);
                    },
                    function(callback){
                        IsInStackFunction("In", retryCount, waitTime, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result)
                        {
                            orderCellIndex = GetRet();
                            callback(null, GetRet());
                        });
                    }
                ],function(err, results)
                {
                    console.log(results);
                    expect(orderCellIndex == -1).toBeTruthy();
                    expect(orderKilled).toEqual(true);
                });
            }
            browser.sleep(ModalCloseTime);
            currentIndex++;
        });

        reader.addListener('end', function()
        {
            console.log('read entire file - end of file reached');
        });
    },

    "Order.ExecuteFromFile": function(next, retryIterationCount, retryWaitTime, fileName, fileStartIndex, fileEndIndex) {

        var startIndex = 0;
        var endIndex = 999;
        var retryCount = 5;
        var waitTime = 875;

        if(fileStartIndex != "" && typeof fileStartIndex != 'undefined') startIndex = fileStartIndex;
        if(fileEndIndex != "" && typeof fileEndIndex != 'undefined') endIndex = fileEndIndex;

        var reader = csv.createCsvFileReader(".\\" + fileName, {columnsFromHeader:true, 'separator': ','});

        var currentIndex = 0;
        reader.addListener('data', function(data)
        {
            if(currentIndex >= startIndex && currentIndex <= endIndex)
            {
                console.log("Executing Order at File Index: " + currentIndex);
                console.log("   - Product: " + data['product']);
                console.log("   - BidAsk: " + data['bidask']);
                console.log("   - Quantity: " + data['quantity']);
                console.log("   - Price: " + data['price']);
                console.log("   - Notes: " + data['notes']);

                if(retryIterationCount != "" && retryIterationCount == 0 && typeof retryIterationCount == 'undefined') retryCount = retryIterationCount;
                if(retryWaitTime != "" && retryIterationCount == 0 && typeof retryWaitTime == 'undefined') waitTime = retryWaitTime;

                var product = data['product'];
                var bidAsk = data['bidask'];
                var quantity = data['quantity'];
                var price = data['price'];
                var notes = data['notes'];

                var productCount = -1;
                var productIndex;
                var rowCount;
                var startCellIndex;
                var orderCellIndex = -1;
                var orderExecuted = false;

                Async.series([
                    function(callback){
                        GetProductCountFunction().then(function(result)
                        {
                            productCount = result;
                            callback(null, productCount);
                        });
                    },
                    function(callback){
                        GetProductIndexFunction(product).then(function(result)
                        {
                            productIndex = result;
                            callback(null, productIndex);
                        });
                    },
                    function(callback){
                        GetStackRowsCountFunction(productCount).then(function(result)
                        {
                            rowCount = result;
                            callback(null, rowCount);
                        });
                    },
                    function(callback){
                        startCellIndex = GetStartCellIndexFunction(bidAsk, productCount, productIndex, rowCount);
                        callback(null, startCellIndex);
                    },
                    function(callback){
                        IsInStackFunction("In", retryCount, waitTime, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result)
                        {
                            orderCellIndex = GetRet();
                            callback(null, GetRet());
                        });
                    },
                    function(callback){
                        orderExecuted = ExecuteOrderFunction(orderCellIndex);
                        callback(null, orderExecuted);
            }
                ],function(err, results)
                {
                    console.log(results);
                    expect(orderCellIndex > -1).toBeTruthy();
                    expect(orderExecuted).toEqual(true);
                });
            }
            browser.sleep(ModalCloseTime);
            currentIndex++;
        });

        reader.addListener('end', function()
        {
            console.log('read entire file - end of file reached');
        });
    },

    "Order.IsInStackFromFile": function(next, retryIterationCount, retryWaitTime, fileName, fileStartIndex, fileEndIndex) {

        var startIndex = 0;
        var endIndex = 999;
        var retryCount = 5;
        var waitTime = 875;

        if(fileStartIndex != "" && typeof fileStartIndex != 'undefined') startIndex = fileStartIndex;
        if(fileEndIndex != "" && typeof fileEndIndex != 'undefined') endIndex = fileEndIndex;

        var reader = csv.createCsvFileReader(".\\" + fileName, {columnsFromHeader:true, 'separator': ','});

        var currentIndex = 0;
        reader.addListener('data', function(data)
        {
            if(currentIndex >= startIndex && currentIndex <= endIndex)
            {
                console.log("Holding Order at File Index: " + currentIndex);
                console.log("   - Product: " + data['product']);
                console.log("   - BidAsk: " + data['bidask']);
                console.log("   - Quantity: " + data['quantity']);
                console.log("   - Price: " + data['price']);
                console.log("   - Notes: " + data['notes']);

                if(retryIterationCount != "" && retryIterationCount == 0 && typeof retryIterationCount == 'undefined') retryCount = retryIterationCount;
                if(retryWaitTime != "" && retryIterationCount == 0 && typeof retryWaitTime == 'undefined') waitTime = retryWaitTime;

                var product = data['product'];
                var bidAsk = data['bidask'];
                var quantity = data['quantity'];
                var price = data['price'];
                var notes = data['notes'];

                var productCount = -1;
                var productIndex;
                var rowCount;
                var startCellIndex;
                var orderCellIndex = -1;
                var orderHeld = false;

                Async.series([
                    function(callback){
                        GetProductCountFunction().then(function(result)
                        {
                            productCount = result;
                            callback(null, productCount);
                        });
                    },
                    function(callback){
                        GetProductIndexFunction(product).then(function(result)
                        {
                            productIndex = result;
                            callback(null, productIndex);
                        });
                    },
                    function(callback){
                        GetStackRowsCountFunction(productCount).then(function(result)
                        {
                            rowCount = result;
                            callback(null, rowCount);
                        });
                    },
                    function(callback){
                        startCellIndex = GetStartCellIndexFunction(bidAsk, productCount, productIndex, rowCount);
                        callback(null, startCellIndex);
                    },
                    function(callback){
                        IsInStackFunction("In", retryCount, waitTime, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result)
                        {
                            orderCellIndex = GetRet();
                            callback(null, GetRet());
                        });
                    }
                ],function(err, results)
                {
                    console.log(results);
                    expect(orderCellIndex > -1).toBeTruthy();
                });
            }
            browser.sleep(ModalCloseTime);
            currentIndex++;
        });

        reader.addListener('end', function()
        {
            console.log('read entire file - end of file reached');
        });
    },

    "Order.IsNotInStackFromFile": function(next, retryIterationCount, retryWaitTime, fileName, fileStartIndex, fileEndIndex) {

        var startIndex = 0;
        var endIndex = 999;
        var retryCount = 5;
        var waitTime = 875;

        if(fileStartIndex != "" && typeof fileStartIndex != 'undefined') startIndex = fileStartIndex;
        if(fileEndIndex != "" && typeof fileEndIndex != 'undefined') endIndex = fileEndIndex;

        var reader = csv.createCsvFileReader(".\\" + fileName, {columnsFromHeader:true, 'separator': ','});

        var currentIndex = 0;
        reader.addListener('data', function(data)
        {
            if(currentIndex >= startIndex && currentIndex <= endIndex)
            {
                console.log("Holding Order at File Index: " + currentIndex);
                console.log("   - Product: " + data['product']);
                console.log("   - BidAsk: " + data['bidask']);
                console.log("   - Quantity: " + data['quantity']);
                console.log("   - Price: " + data['price']);
                console.log("   - Notes: " + data['notes']);

                if(retryIterationCount != "" && retryIterationCount == 0 && typeof retryIterationCount == 'undefined') retryCount = retryIterationCount;
                if(retryWaitTime != "" && retryIterationCount == 0 && typeof retryWaitTime == 'undefined') waitTime = retryWaitTime;

                var product = data['product'];
                var bidAsk = data['bidask'];
                var quantity = data['quantity'];
                var price = data['price'];
                var notes = data['notes'];

                var productCount = -1;
                var productIndex;
                var rowCount;
                var startCellIndex;
                var orderCellIndex = -1;
                var orderHeld = false;

                Async.series([
                    function(callback){
                        GetProductCountFunction().then(function(result)
                        {
                            productCount = result;
                            callback(null, productCount);
                        });
                    },
                    function(callback){
                        GetProductIndexFunction(product).then(function(result)
                        {
                            productIndex = result;
                            callback(null, productIndex);
                        });
                    },
                    function(callback){
                        GetStackRowsCountFunction(productCount).then(function(result)
                        {
                            rowCount = result;
                            callback(null, rowCount);
                        });
                    },
                    function(callback){
                        startCellIndex = GetStartCellIndexFunction(bidAsk, productCount, productIndex, rowCount);
                        callback(null, startCellIndex);
                    },
                    function(callback){
                        IsInStackFunction("In", retryCount, waitTime, productIndex, productCount, startCellIndex, rowCount, bidAsk, quantity, price).then(function(result)
                        {
                            orderCellIndex = GetRet();
                            callback(null, GetRet());
                        });
                    }
                ],function(err, results)
                {
                    console.log(results);
                    expect(orderCellIndex == -1).toBeTruthy();
                });
            }
            browser.sleep(ModalCloseTime);
            currentIndex++;
        });

        reader.addListener('end', function()
        {
            console.log('read entire file - end of file reached');
        });
    },

    "Order.CreateOrder": function(next, product, bidask, quantity, price, notes ) {

        CreateOrderFunction(product, bidask, quantity, price, notes);

        next();
    },

    "Order.KillOrder": function(next, delay, orderCellIndex) {

        var delayTime = 0;
        if(delay != "" && delay != 0 && typeof delay != 'undefined') delayTime = delay;

        var orderKilled = false;
        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var editOrderPage = new EditOrderPage();

        expect(editOrderPage.displayed()).toEqual(true);

        browser.sleep(delayTime);

        editOrderPage.kill();
        orderKilled = true;

        next(orderKilled);
    },

    "Order.KillAllOrders": function(next, productCount, stackRowCount) {

        var ordersKilled = 0;
        var ordersExecuted = 0;
        var dashboard = new DashboardPage();
        var iCell = 0;
        var keepKilling = true;
        var moreStacksToClear = true;
        var productIndex = 0;
        var numberOfStacks = productCount * 2;
        var bidAsk = "BID";

        console.log("Clearing all Stacks");

        dashboard.allBidAskRows().then(function (temp) {
            Async.whilst(
                function() {
                    if(keepKilling == false) {
                        if(productIndex == productCount - 1 && bidAsk == "ASK") moreStacksToClear = false;

                        if(moreStacksToClear) {
                            if(bidAsk == "BID") {
                                bidAsk = "ASK";
                            } else {
                                bidAsk = "BID";
                                productIndex++;
                            }
                            keepKilling = true;
                            iCell = GetStartCellIndexFunction(bidAsk, productCount, productIndex, stackRowCount);
                        }
                    }
                    return keepKilling;
                },

                function(callback) {
                    dashboard.allBidAskRows().then(function (BidAskRows) {
                        BidAskRows[iCell].getText().then(function (RowText) {

                            if (RowText.length > 0) {
                                dashboard.openOrder(iCell);
                                var editOrderPage = new EditOrderPage();
                                try {
                                    editOrderPage.kill();
                                    ordersKilled++;
                                } catch (err) {
                                    var executeOrderPage = new ExecuteOrderPage();
                                    expect(executeOrderPage.displayed()).toEqual(true);
                                    executeOrderPage.execute();
                                    ordersExecuted++;
                                }
                                browser.sleep(2000);

                            } else { // Blank means no more rows
                                console.log("Finished clearing Product Id: " + productIndex + " " + bidAsk + " stack");
                                keepKilling = false;
                            } // if non-blank row
                            setTimeout(callback, 0);
                        }); // BidAskRows[iRow].getText().then(function (RowText) {
                    });
                },
                function (err) {
                    console.log("Finished Clearing all stacks " + ordersKilled + " orders killed & " + ordersExecuted + " orders executed");
                    SetRet(ordersKilled);
                    return ordersKilled;
                    next(ordersKilled);
                }
            ); // async.whilst
        });        
    },

    "Order.HoldOrder": function(next, delay, orderCellIndex) {
        
        var delayTime = 0;
        if(delay != "" && delay != 0 && typeof delay != 'undefined') delayTime = delay;

        var orderHeld = false;
        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var editOrderPage = new EditOrderPage();

        expect(editOrderPage.displayed()).toEqual(true);

        browser.sleep(delayTime);

        editOrderPage.hold();
        orderHeld = true;

        next(orderHeld);
    },

    "Order.ReinstateOrder": function(next, delay, orderCellIndex) {

        var delayTime = 0;
        if(delay != "" && delay != 0 && typeof delay != 'undefined') delayTime = delay;

        var orderReinstated = false;
        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var editOrderPage = new EditOrderPage();

        expect(editOrderPage.displayed()).toEqual(true);

        browser.sleep(delayTime);

        editOrderPage.reinstate();
        orderReinstated = true;

        next(orderReinstated);
    },

    "Order.ExecuteOrder": function(next, delay, orderCellIndex) {

        var delayTime = 0;
        if(delay != "" && delay != 0 && typeof delay != 'undefined') delayTime = delay;

        var orderExecuted = false;
        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var executeOrderPage = new ExecuteOrderPage();

        expect(executeOrderPage.displayed()).toEqual(true);

        browser.sleep(delayTime);

        executeOrderPage.execute();
        orderExecuted = true;

        browser.sleep(ModalCloseTime); // this is because the order takes some time to disappear

        next(orderExecuted);
    },


    "Order.UpdateOrder": function(next, delay, orderCellIndex, price, quantity, notes) {
        var delayTime = 0;
        if(delay != "" && delay != 0 && typeof delay != 'undefined') delayTime = delay;

        var orderUpdated = false;
        var dashboard = new DashboardPage();
        dashboard.openOrder(orderCellIndex);

        var editOrderPage = new EditOrderPage();

        expect(editOrderPage.displayed()).toEqual(true);

        if(price != "")
        {
            editOrderPage.setPrice(price);
        }

        if(quantity != "")
        {
            editOrderPage.setQuantity(quantity);
        }

        editOrderPage.setNotes(notes);

        browser.sleep(delayTime);

        editOrderPage.update();
        orderUpdated = true;

        browser.sleep(ModalCloseTime); // this is because the order takes some time to disappear

        next(orderUpdated);
    },

    "Order.IsInCurrentStack": function(next, startCellIndex, rowCount, bidAsk, quantity, price, oRowFound) {

        var dashboard = new DashboardPage();
        var sQuantity;
        var sPrice;
        var arrOrderElements;
        var rowFound = false;
        var endCellIndex = startCellIndex + rowCount - 1;

        var iRow;
        oRowFound.value = rowFound;
        iRow = startCellIndex;
        dashboard.allBidAskRows().then(function (BidAskRows) {
            Async.whilst(
                function() {
                    if (iRow > endCellIndex) {
                        next(rowFound);
                    }
                    return iRow <= endCellIndex;
                },

                function(callback) {
                    BidAskRows[iRow].getText().then(function (RowText) {
                        if (RowText.length > 0) {
                            arrOrderElements = RowText.split(/\n/g);

                            if(bidAsk == 'BID')
                            {
                                if (arrOrderElements.length === 5) {
                                    sQuantity = arrOrderElements[3];
                                    sPrice = arrOrderElements[4];
                                } else {
                                    sQuantity = arrOrderElements[2];
                                    sPrice = arrOrderElements[3];
                                }
                            }
                            else
                            {
                                if (arrOrderElements.length === 5) {
                                    sQuantity = arrOrderElements[1];
                                    sPrice = arrOrderElements[0];
                                } else {
                                    sQuantity = arrOrderElements[1];
                                    sPrice = arrOrderElements[0];
                                }
                            }

                            if (Number(sQuantity.replace(',', '')) === Number(quantity)) {
                                if (Number(sPrice.replace(',', '')) === Number(price)) {
                                    console.log("B/A:" + bidAsk + ",Q:"+ quantity + ",P:" + price);
                                    rowFound = true;
                                    oRowFound.value = rowFound;
                                    next(rowFound);
                                    iRow = endCellIndex+1; // jump out condition
                                }
                            }
                        } else { // Blank means no more rows
                            oRowFound.value = rowFound;
                            next(rowFound);
                            iRow = endCellIndex+1; // jump out condition
                        } // if non-blank row
                        iRow++;
                        setTimeout(callback, 0);
                    }); // BidAskRows[iRow].getText().then(function (RowText) {
                },

                function (err) {}

            ); // async.whilst

        });
    },

    "Order.ExpandGrid": function(next, productIndex, rowCount, productCount) {

        var dashboard = new DashboardPage();
        var prevCount = 0;
        var currentCount = rowCount;

        Async.whilst(
            function() { return currentCount > prevCount; },

            function(callback) {

                dashboard.expandGrid(productIndex);
                dashboard.allBidAskRows().then(function (BidAskRows) {
                    prevCount = currentCount;
                    currentCount = BidAskRows.length;
                    if (currentCount == prevCount) {
                        next(currentCount / (productCount * 2));
                    }
                    setTimeout(callback, 0);
                });
            },

            function (err) { }
        );
    }

};

module.exports = OrderPrivateKeywords;