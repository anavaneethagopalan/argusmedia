// Order.Keywords.js
var DashboardPage = require("../pages/dashboardpage");
var NewOrderPage = require("../pages/neworderpage");
var key = require('keyword'); // keyword functionality
try { key(require('./Order.private.keywords')); } catch(err) { }
try { key(require('./Just.private.keywords')); } catch(err) { }

var OrderKeywords = {

    "NewOrder": function(next, product, bidask, quantity, price, notes ) {
        var dashboard = new DashboardPage(); // get the dashboard page.

        if (bidask.substr(0, 1).toUpperCase() === 'B')  {
            dashboard.newBid(product);
        } else {
            dashboard.newAsk(product);
        }
        var newOrder = new NewOrderPage();
        newOrder.setPrice(price);
        newOrder.create();

        next();
    },

    "NewOrders": function(next, number, product, bidAsk, quantity, startPrice, priceIncrement) {
        var dashboard = new DashboardPage(); // get the dashboard page.

        for (var i = 0; i < number; i++) {
            if (bidAsk.substr(0, 1).toUpperCase() === 'B')  {
                dashboard.newBid(product);
            } else {
                dashboard.newAsk(product);
            }

            newOrder = new NewOrderPage();
            newOrder.setPrice((startPrice+priceIncrement*i).toFixed(2));
            newOrder.create();
            browser.sleep(250);
        }

        next();
    },

    "OrderInBidAskStack": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

    "OrderInMarketTicker": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

    "OrderInDatabase": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

    "Order.Create":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",
        "Just.GetValueFromParams", ["$1", "notes"], "=> $notes",

        "Order.CreateOrder", ["$productName", "$bidAsk", "$quantity", "$price", "$notes"]
    ],

    "Order.CreateOrderFromFile":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "filename"], "=> $fileName",
        "Just.GetValueFromParams", ["$1", "startindex"], "=> $startIndex",
        "Just.GetValueFromParams", ["$1", "endindex"], "=> $endIndex",

        "Order.CreateFromFile", ["$retryCount", "$retryWaitTime", "$fileName", "$startIndex", "$endIndex"]
    ],

    "Order.Hold":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",
        "Just.GetValueFromParams", ["$1", "delay"], "=> $delay",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetProductIndex", ["$productName"], "=> $productIndex",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.GetStartCellIndex", ["$bidAsk", "$productCount", "$productIndex", "$rowCount"], "=> $startCellIndex",
        "Order.IsInStack", ["$retryCount", "$retryWaitTime", "$productIndex", "$productCount", "$startCellIndex", "$rowCount", "$bidAsk", "$quantity", "$price"], "=> $orderCellIndex",

        "Order.HoldOrder", ["$delay", "$orderCellIndex"], "=> $rowHeld",

        "Just.AssertGreaterThan", ["$orderCellIndex", -1],
        "Just.AssertValue", [true, "$rowHeld"]
    ],

    "Order.HoldOrderFromFile":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "filename"], "=> $fileName",
        "Just.GetValueFromParams", ["$1", "startindex"], "=> $startIndex",
        "Just.GetValueFromParams", ["$1", "endindex"], "=> $endIndex",

        "Order.HoldFromFile", ["$retryCount", "$retryWaitTime", "$fileName", "$startIndex", "$endIndex"]
    ],

    "Order.Reinstate":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",
        "Just.GetValueFromParams", ["$1", "delay"], "=> $delay",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetProductIndex", ["$productName"], "=> $productIndex",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.GetStartCellIndex", ["$bidAsk", "$productCount", "$productIndex", "$rowCount"], "=> $startCellIndex",
        "Order.IsInStack", ["$retryCount", "$retryWaitTime", "$productIndex", "$productCount", "$startCellIndex", "$rowCount", "$bidAsk", "$quantity", "$price"], "=> $orderCellIndex",

        "Order.ReinstateOrder", ["$delay", "$orderCellIndex"], "=> $rowReinstated",

        "Just.AssertGreaterThan", ["$orderCellIndex", -1],
        "Just.AssertValue", [true, "$rowReinstated"]
    ],

    "Order.ReinstateOrderFromFile":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "filename"], "=> $fileName",
        "Just.GetValueFromParams", ["$1", "startindex"], "=> $startIndex",
        "Just.GetValueFromParams", ["$1", "endindex"], "=> $endIndex",

        "Order.ReinstateFromFile", ["$retryCount", "$retryWaitTime", "$fileName", "$startIndex", "$endIndex"]
    ],

    "Order.Kill":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",
        "Just.GetValueFromParams", ["$1", "delay"], "=> $delay",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetProductIndex", ["$productName"], "=> $productIndex",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.GetStartCellIndex", ["$bidAsk", "$productCount", "$productIndex", "$rowCount"], "=> $startCellIndex",
        "Order.IsInStack", ["$retryCount", "$retryWaitTime", "$productIndex", "$productCount", "$startCellIndex", "$rowCount", "$bidAsk", "$quantity", "$price"], "=> $orderCellIndex",

        "Order.KillOrder", ["$delay", "$orderCellIndex"], "=> $rowKilled",

        "Just.AssertGreaterThan", ["$orderCellIndex", -1],
        "Just.AssertValue", [true, "$rowKilled"]
    ],

    "Order.ClearBidAskStacks":[

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.KillAllOrders", ["$productCount", "$rowCount"], "=> $rowKilled"
    ],

    "Order.KillOrderFromFile":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "filename"], "=> $fileName",
        "Just.GetValueFromParams", ["$1", "startindex"], "=> $startIndex",
        "Just.GetValueFromParams", ["$1", "endindex"], "=> $endIndex",

        "Order.KillFromFile", ["$retryCount", "$retryWaitTime", "$fileName", "$startIndex", "$endIndex"]
    ],

    "Order.Execute":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",
        "Just.GetValueFromParams", ["$1", "delay"], "=> $delay",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetProductIndex", ["$productName"], "=> $productIndex",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.GetStartCellIndex", ["$bidAsk", "$productCount", "$productIndex", "$rowCount"], "=> $startCellIndex",
        "Order.IsInStack", ["$retryCount", "$retryWaitTime", "$productIndex", "$productCount", "$startCellIndex", "$rowCount", "$bidAsk", "$quantity", "$price"], "=> $orderCellIndex",

        "Just.AssertGreaterThan", ["$orderCellIndex", -1],

        "Order.ExecuteOrder", ["$delay", "$orderCellIndex"], "=> $rowExecuted",

        "Just.AssertValue", [true, "$rowExecuted"]
    ],

    "Order.ExecuteOrderFromFile":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "filename"], "=> $fileName",
        "Just.GetValueFromParams", ["$1", "startindex"], "=> $startIndex",
        "Just.GetValueFromParams", ["$1", "endindex"], "=> $endIndex",

        "Order.ExecuteFromFile", ["$retryCount", "$retryWaitTime", "$fileName", "$startIndex", "$endIndex"]
    ],

    "Order.CheckInStack":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetProductIndex", ["$productName"], "=> $productIndex",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.GetStartCellIndex", ["$bidAsk", "$productCount", "$productIndex", "$rowCount"], "=> $startCellIndex",
        "Order.IsInStack", ["$retryCount", "$retryWaitTime", "$productIndex", "$productCount", "$startCellIndex", "$rowCount", "$bidAsk", "$quantity", "$price"], "=> $orderCellIndex",

        "Just.AssertGreaterThan", ["$orderCellIndex", -1]
    ],

    "Order.CheckInStackFromFile":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "filename"], "=> $fileName",
        "Just.GetValueFromParams", ["$1", "startindex"], "=> $startIndex",
        "Just.GetValueFromParams", ["$1", "endindex"], "=> $endIndex",

        "Order.IsInStackFromFile", ["$retryCount", "$retryWaitTime", "$fileName", "$startIndex", "$endIndex"]
    ],

    "Order.CheckNotInStack":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetProductIndex", ["$productName"], "=> $productIndex",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.GetStartCellIndex", ["$bidAsk", "$productCount", "$productIndex", "$rowCount"], "=> $startCellIndex",
        "Order.IsNotInStack", ["$retryCount", "$retryWaitTime", "$productIndex", "$productCount", "$startCellIndex", "$rowCount", "$bidAsk", "$quantity", "$price"], "=> $orderCellIndex",

        "Just.AssertValue", [-1, "$orderCellIndex"]
    ],

    "Order.CheckNotInStackFromFile":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "filename"], "=> $fileName",
        "Just.GetValueFromParams", ["$1", "startindex"], "=> $startIndex",
        "Just.GetValueFromParams", ["$1", "endindex"], "=> $endIndex",

        "Order.IsNotInStackFromFile", ["$retryCount", "$retryWaitTime", "$fileName", "$startIndex", "$endIndex"]
    ],

    "Order.Update":[

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",
        "Just.GetValueFromParams", ["$1", "delay"], "=> $delay",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",

        "Just.GetValueFromParams", ["$1", "uquantity"], "=> $uquantity",
        "Just.GetValueFromParams", ["$1", "uprice"], "=> $uprice",
        "Just.GetValueFromParams", ["$1", "unotes"], "=> $unotes",

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetProductIndex", ["$productName"], "=> $productIndex",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.GetStartCellIndex", ["$bidAsk", "$productCount", "$productIndex", "$rowCount"], "=> $startCellIndex",
        "Order.IsInStack", ["$retryCount", "$retryWaitTime", "$productIndex", "$productCount", "$startCellIndex", "$rowCount", "$bidAsk", "$quantity", "$price"], "=> $orderCellIndex",

        "Just.AssertGreaterThan", ["$orderCellIndex", -1],

        "Order.UpdateOrder", ["$delay", "$orderCellIndex", "$uprice","$uquantity", "$unotes"], "=> $rowUpdated",

        "Just.AssertValue", [true, "$rowUpdated"]
    ],

    "Order.CheckInStackWithColour": [

        "Just.GetValueFromParams", ["$1", "retryCount"], "=> $retryCount",
        "Just.GetValueFromParams", ["$1", "retryWaitTime"], "=> $retryWaitTime",

        "Just.GetValueFromParams", ["$1", "product"], "=> $productName",
        "Just.GetValueFromParams", ["$1", "bidask"], "=> $bidAsk",
        "Just.GetValueFromParams", ["$1", "quantity"], "=> $quantity",
        "Just.GetValueFromParams", ["$1", "price"], "=> $price",
        "Just.GetValueFromParams", ["$1", "colour"], "=> $colour",
        "Just.GetValueFromParams", ["$1", "style"], "=> $style",

        "Order.GetProductCount", [], "=> $productCount",
        "Order.GetProductIndex", ["$productName"], "=> $productIndex",
        "Order.GetStackRowsCount", ["$productCount"], "=> $rowCount",

        "Order.GetStartCellIndex", ["$bidAsk", "$productCount", "$productIndex", "$rowCount"], "=> $startCellIndex",

        "Order.IsInStackWithColour", ["$retryCount", "$retryWaitTime", "$productIndex", "$productCount", "$startCellIndex", "$rowCount", "$bidAsk", "$quantity", "$price", "$colour", "$style"], "=> $orderCellIndex",

        "Just.AssertGreaterThan", ["$orderCellIndex", -1]

    ],

    "Order.CheckInTicker": function(next, params ) {
        //console.log(this.name);
        next();
    }

};

module.exports = OrderKeywords;