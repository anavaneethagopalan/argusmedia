var Observer1 = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#OBSERVER1.USERNAME#',password:'#OBSERVER1.PASSWORD#'}]}],
	[{action:'AOM.Delay',params:[{delay:'10000',retryCount:'',retryWaitTime:'',product:'',bidask:'',quantity:'',price:'',deliverystart:'',deliveryend:'',broker:'',notes:'',colour:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Reinstate',params:[{delay:'#OBSERVER1.DELAY#',retryCount:'',retryWaitTime:'',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.CheckMessage',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',text:'#EXPECTEDMESSAGE#',closeonexit:'False'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Kill',params:[{delay:'',retryCount:'',retryWaitTime:'',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckNotInStack',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Observer1;
