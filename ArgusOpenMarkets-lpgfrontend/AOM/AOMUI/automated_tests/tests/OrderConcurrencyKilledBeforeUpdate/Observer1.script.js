var Observer1 = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#OBSERVER1.USERNAME#',password:'#OBSERVER1.PASSWORD#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',uquantity:'',uprice:'',unotes:'',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Update',params:[{delay:'#OBSERVER1.DELAY#',retryCount:'',retryWaitTime:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',uquantity:'#OBSERVER1.UPDATEDQUANTITY#',uprice:'#OBSERVER1.UPDATEDBIDPRICE#',unotes:'Notes: BID, #OBSERVER1.UPDATEDQUANTITY#, #OBSERVER1.UPDATEDBIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.CheckMessage',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',text:'#EXPECTEDMESSAGE#',closeonexit:'False'}]}],
	[{action:'Order.CheckNotInStack',params:[{delay:'',retryCount:'',retryWaitTime:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#OBSERVER1.UPDATEDQUANTITY#',price:'#OBSERVER1.UPDATEDBIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #OBSERVER1.UPDATEDQUANTITY#, #OBSERVER1.UPDATEDBIDPRICE#, BrokerCo #1',uquantity:'',uprice:'',unotes:'',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Observer1;
