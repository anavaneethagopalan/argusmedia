var Initiator = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#INITIATOR.USERNAME#',password:'#INITIATOR.PASSWORD#'}]}],
	[{action:'Order.Create',params:[{delay:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#INITIALQUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #INITIALQUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'',uquantity:'',uprice:'',unotes:''}]}],
	[{action:'Order.Hold',params:[{delay:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#INITIALQUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2016',deliveryend:'05/01/2016',broker:'BrokerCo #2',notes:'Notes: BID, #INITIALQUANTITY#, #BIDPRICE#, BrokerCo #2',colour:'',uquantity:'',uprice:'',unotes:''}]}],
	[{action:'Order.Update',params:[{delay:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#INITIALQUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #INITIALQUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#',uquantity:'#QUANTITY#',uprice:'#BIDPRICE#',unotes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{delay:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#',uquantity:'',uprice:'',unotes:''}]}],
	[{action:'Order.Kill',params:[{delay:'#INITIATOR.DELAY#',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'',uquantity:'',uprice:'',unotes:''}]}],
	[{action:'Order.CheckNotInStack',params:[{delay:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#',uquantity:'',uprice:'',unotes:''}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Initiator;
