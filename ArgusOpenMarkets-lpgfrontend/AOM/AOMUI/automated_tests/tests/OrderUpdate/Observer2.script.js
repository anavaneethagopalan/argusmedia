var Observer2 = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#OBSERVER2.USERNAME#',password:'#OBSERVER2.PASSWORD#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE1#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#UPDATEDQUANTITY#',price:'#UPDATEDBIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #UPDATEDQUANTITY#, #UPDATEDBIDPRICE1#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE1#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#UPDATEDQUANTITY#',price:'#UPDATEDASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #UPDATEDQUANTITY#, #UPDATEDASKPRICE1#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Observer2;
