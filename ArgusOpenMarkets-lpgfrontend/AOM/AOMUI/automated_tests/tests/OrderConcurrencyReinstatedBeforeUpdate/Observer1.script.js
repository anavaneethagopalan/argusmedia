var Observer1 = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#OBSERVER1.USERNAME#',password:'#OBSERVER1.PASSWORD#'}]}],
	[{action:'AOM.Delay',params:[{delay:'10000',retryCount:'',retryWaitTime:'',product:'',bidask:'',quantity:'',price:'',deliverystart:'',deliveryend:'',broker:'',notes:'',colour:'',uquantity:'',uprice:'',unotes:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#',uquantity:'',uprice:'',unotes:''}]}],
	[{action:'Order.Update',params:[{delay:'#OBSERVER1.DELAY#',retryCount:'',retryWaitTime:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#',uquantity:'#OBSERVER1.UPDATEDQUANTITY#',uprice:'#OBSERVER1.UPDATEDBIDPRICE#',unotes:'Notes: BID, #OBSERVER1.UPDATEDQUANTITY#, #OBSERVER1.UPDATEDBIDPRICE#, BrokerCo #1'}]}],
	[{action:'AOM.CheckMessage',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',text:'#EXPECTEDMESSAGE#',closeonexit:'False'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Kill',params:[{delay:'',retryCount:'',retryWaitTime:'',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckNotInStack',params:[{delay:'',retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Observer1;
