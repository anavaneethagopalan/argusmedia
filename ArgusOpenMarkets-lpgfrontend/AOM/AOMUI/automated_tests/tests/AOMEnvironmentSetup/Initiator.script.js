var Initiator = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#USERNAME#',password:'#PASSWORD#'}]}],
	[{action:'Order.Create',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Kill',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:''}]}],
	[{action:'Order.CheckNotInStack',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Initiator;
