var Initiator = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#INITIATOR.USERNAME1#',password:'#INITIATOR.PASSWORD1#'}]}],
	[{action:'Order.Create',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Create',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}],
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#INITIATOR.USERNAME2#',password:'#INITIATOR.PASSWORD2#'}]}],
	[{action:'Order.Execute',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.CheckNotInStack',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.Execute',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.CheckNotInStack',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}]
];

module.exports = Initiator;
