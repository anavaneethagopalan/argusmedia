var TestData = (function () {
	   function TestData() {}
//-------------------------------------------------------
	    TestData.prototype.load = function (dictData) {
		dictData.set('#NAPHTHACARGOES#', 'NAPHTHA CIF NWE - OUTRIGHT');
		dictData.set('#NAPHTHABARGES#', 'NAPHTHA CIF NWE - DIFF');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#QUANTITY#', '15500');
		dictData.set('#BIDPRICE#', '890');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#EXPECTEDMESSAGE#', 'could not be amended as TradeCo #1 updated this order at');
		dictData.set('#INITIATOR.UPDATEDQUANTITY#', '16500');
		dictData.set('#INITIATOR.UPDATEDBIDPRICE#', '895');
		dictData.set('#INITIATOR.DELAY#', '3000');
		dictData.set('#OBSERVER1.UPDATEDQUANTITY#', '17500');
		dictData.set('#OBSERVER1.UPDATEDBIDPRICE#', '896');
		dictData.set('#OBSERVER1.DELAY#', '7000');
		dictData.set('#INITIATOR.USERNAME#', 't1');
		dictData.set('#INITIATOR.PASSWORD#', 'password');
		dictData.set('#INITIATOR.ORGANISATION#', 'TradeCo #1');
		dictData.set('#OBSERVER1.USERNAME#', 't2');
		dictData.set('#OBSERVER1.PASSWORD#', 'password');
		dictData.set('#OBSERVER1.ORGANISATION#', 'TradeCo #1');
	};
		return TestData;
})();
module.exports = TestData;
