var TestData = (function () {
	   function TestData() {}
//-------------------------------------------------------
	    TestData.prototype.load = function (dictData) {
		dictData.set('#NAPHTHACARGOES#', 'NAPHTHA CIF NWE - OUTRIGHT');
		dictData.set('#NAPHTHABARGES#', 'NAPHTHA CIF NWE - DIFF');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#QUANTITY#', '29500');
		dictData.set('#BIDPRICE#', '295');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#EXPECTEDMESSAGE#', 'could not be hit as TradeCo #1 withdrew this order at');
		dictData.set('#INITIATOR.DELAY#', '3000');
		dictData.set('#OBSERVER1.DELAY#', '5000');
		dictData.set('#INITIATOR.USERNAME#', 't1');
		dictData.set('#INITIATOR.PASSWORD#', 'password');
		dictData.set('#INITIATOR.ORGANISATION#', 'TradeCo #1');
		dictData.set('#OBSERVER1.USERNAME#', 't3');
		dictData.set('#OBSERVER1.PASSWORD#', 'password');
		dictData.set('#OBSERVER1.ORGANISATION#', 'TradeCo #2');
	};
		return TestData;
})();
module.exports = TestData;
