var TestData = (function () {
	   function TestData() {}
//-------------------------------------------------------
	    TestData.prototype.load = function (dictData) {
		dictData.set('#NAPHTHACARGOES#', 'NAPHTHA CIF NWE - OUTRIGHT');
		dictData.set('#NAPHTHABARGES#', 'NAPHTHA CIF NWE - DIFF');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#QUANTITY#', '29000');
		dictData.set('#BIDPRICE#', '790');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#EXPECTEDMESSAGE#', 'Order has already been executed');
		dictData.set('#OBSERVER1.DELAY#', '3000');
		dictData.set('#OBSERVER2.DELAY#', '5000');
		dictData.set('#INITIATOR.USERNAME#', 't1');
		dictData.set('#INITIATOR.PASSWORD#', 'password');
		dictData.set('#INITIATOR.ORGANISATION#', 'TradeCo #1');
		dictData.set('#OBSERVER1.USERNAME#', 't3');
		dictData.set('#OBSERVER1.PASSWORD#', 'password');
		dictData.set('#OBSERVER1.ORGANISATION#', 'TradeCo #2');
		dictData.set('#OBSERVER2.USERNAME#', 't4');
		dictData.set('#OBSERVER2.PASSWORD#', 'password');
		dictData.set('#OBSERVER2.ORGANISATION#', 'TradeCo #2');
	};
		return TestData;
})();
module.exports = TestData;
