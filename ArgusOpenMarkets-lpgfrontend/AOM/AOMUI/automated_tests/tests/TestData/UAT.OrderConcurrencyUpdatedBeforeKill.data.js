var TestData = (function () {
	   function TestData() {}
//-------------------------------------------------------
	    TestData.prototype.load = function (dictData) {
		dictData.set('#NAPHTHACARGOES#', 'NAPHTHA CIF NWE - OUTRIGHT');
		dictData.set('#NAPHTHABARGES#', 'NAPHTHA CIF NWE - DIFF');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#QUANTITY#', '14500');
		dictData.set('#BIDPRICE#', '900');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#EXPECTEDMESSAGE#', 'could not be killed as TradeCo #1 updated this order at');
		dictData.set('#INITIATOR.DELAY#', '2000');
		dictData.set('#OBSERVER1.DELAY#', '9000');
		dictData.set('#UPDATEDQUANTITY#', '15500');
		dictData.set('#UPDATEDBIDPRICE#', '905');
		dictData.set('#INITIATOR.USERNAME#', 't1');
		dictData.set('#INITIATOR.PASSWORD#', 'password');
		dictData.set('#INITIATOR.ORGANISATION#', 'TradeCo #1');
		dictData.set('#OBSERVER1.USERNAME#', 't2');
		dictData.set('#OBSERVER1.PASSWORD#', 'password');
		dictData.set('#OBSERVER1.ORGANISATION#', 'TradeCo #1');
	};
		return TestData;
})();
module.exports = TestData;
