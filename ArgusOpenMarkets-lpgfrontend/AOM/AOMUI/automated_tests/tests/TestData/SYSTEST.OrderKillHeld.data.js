var TestData = (function () {
	   function TestData() {}
//-------------------------------------------------------
	    TestData.prototype.load = function (dictData) {
		dictData.set('#NAPHTHACARGOES#', 'NAPHTHA CIF NWE - OUTRIGHT');
		dictData.set('#NAPHTHABARGES#', 'NAPHTHA CIF NWE - DIFF');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#QUANTITY#', '30000');
		dictData.set('#BIDPRICE1#', '830');
		dictData.set('#BIDPRICE2#', '831');
		dictData.set('#ASKPRICE1#', '430');
		dictData.set('#ASKPRICE2#', '431');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#INITIATOR.DELAY#', '500');
		dictData.set('#INITIATOR.USERNAME#', 't1');
		dictData.set('#INITIATOR.PASSWORD#', 'password');
		dictData.set('#OBSERVER1.USERNAME#', 't2');
		dictData.set('#OBSERVER1.PASSWORD#', 'password');
		dictData.set('#OBSERVER2.USERNAME#', 't3');
		dictData.set('#OBSERVER2.PASSWORD#', 'password');
		dictData.set('#OBSERVER3.USERNAME#', 't4');
		dictData.set('#OBSERVER3.PASSWORD#', 'password');
		dictData.set('#OBSERVER4.USERNAME#', 'b1');
		dictData.set('#OBSERVER4.PASSWORD#', 'password');
		dictData.set('#OBSERVER5.USERNAME#', 'b3');
		dictData.set('#OBSERVER5.PASSWORD#', 'password');
		dictData.set('#OBSERVER6.USERNAME#', 'ae1');
		dictData.set('#OBSERVER6.PASSWORD#', 'password');
	};
		return TestData;
})();
module.exports = TestData;
