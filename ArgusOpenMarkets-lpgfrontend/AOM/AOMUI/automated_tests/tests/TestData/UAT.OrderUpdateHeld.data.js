var TestData = (function () {
	   function TestData() {}
//-------------------------------------------------------
	    TestData.prototype.load = function (dictData) {
		dictData.set('#NAPHTHACARGOES#', 'NAPHTHA CIF NWE - OUTRIGHT');
		dictData.set('#NAPHTHABARGES#', 'NAPHTHA CIF NWE - DIFF');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#QUANTITY#', '17500');
		dictData.set('#UPDATEDQUANTITY#', '20000');
		dictData.set('#BIDPRICE1#', '870');
		dictData.set('#BIDPRICE2#', '871');
		dictData.set('#ASKPRICE1#', '470');
		dictData.set('#ASKPRICE2#', '471');
		dictData.set('#UPDATEDBIDPRICE1#', '875');
		dictData.set('#UPDATEDBIDPRICE2#', '876');
		dictData.set('#UPDATEDASKPRICE1#', '475');
		dictData.set('#UPDATEDASKPRICE2#', '476');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#INITIATOR.USERNAME#', 't1');
		dictData.set('#INITIATOR.PASSWORD#', 'password');
		dictData.set('#OBSERVER1.USERNAME#', 't2');
		dictData.set('#OBSERVER1.PASSWORD#', 'password');
		dictData.set('#OBSERVER2.USERNAME#', 't3');
		dictData.set('#OBSERVER2.PASSWORD#', 'password');
		dictData.set('#OBSERVER3.USERNAME#', 't4');
		dictData.set('#OBSERVER3.PASSWORD#', 'password');
		dictData.set('#OBSERVER4.USERNAME#', 'b1');
		dictData.set('#OBSERVER4.PASSWORD#', 'password');
		dictData.set('#OBSERVER5.USERNAME#', 'b3');
		dictData.set('#OBSERVER5.PASSWORD#', 'password');
		dictData.set('#OBSERVER6.USERNAME#', 'ae1');
		dictData.set('#OBSERVER6.PASSWORD#', 'password');
	};
		return TestData;
})();
module.exports = TestData;
