var TestData = (function () {
	   function TestData() {}
//-------------------------------------------------------
	    TestData.prototype.load = function (dictData) {
		dictData.set('#NAPHTHACARGOES#', 'NAPHTHA CIF NWE - OUTRIGHT');
		dictData.set('#NAPHTHABARGES#', 'NAPHTHA CIF NWE - DIFF');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#QUANTITY#', '12500');
		dictData.set('#UPDATEDQUANTITY#', '17500');
		dictData.set('#BIDPRICE1#', '860');
		dictData.set('#BIDPRICE2#', '861');
		dictData.set('#ASKPRICE1#', '460');
		dictData.set('#ASKPRICE2#', '461');
		dictData.set('#UPDATEDBIDPRICE1#', '865');
		dictData.set('#UPDATEDBIDPRICE2#', '866');
		dictData.set('#UPDATEDASKPRICE1#', '465');
		dictData.set('#UPDATEDASKPRICE2#', '466');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#INITIATOR.USERNAME#', 't1');
		dictData.set('#INITIATOR.PASSWORD#', 'password');
		dictData.set('#OBSERVER1.USERNAME#', 't2');
		dictData.set('#OBSERVER1.PASSWORD#', 'password');
		dictData.set('#OBSERVER2.USERNAME#', 't3');
		dictData.set('#OBSERVER2.PASSWORD#', 'password');
		dictData.set('#OBSERVER3.USERNAME#', 't4');
		dictData.set('#OBSERVER3.PASSWORD#', 'password');
		dictData.set('#OBSERVER4.USERNAME#', 'b1');
		dictData.set('#OBSERVER4.PASSWORD#', 'password');
		dictData.set('#OBSERVER5.USERNAME#', 'b3');
		dictData.set('#OBSERVER5.PASSWORD#', 'password');
		dictData.set('#OBSERVER6.USERNAME#', 'ae1');
		dictData.set('#OBSERVER6.PASSWORD#', 'password');
	};
		return TestData;
})();
module.exports = TestData;
