var TestData = (function () {
	   function TestData() {}
//-------------------------------------------------------
	    TestData.prototype.load = function (dictData) {
		dictData.set('#NAPHTHACARGOES#', 'NAPHTHA CIF NWE - OUTRIGHT');
		dictData.set('#NAPHTHABARGES#', 'NAPHTHA CIF NWE - DIFF');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#QUANTITY#', '12500');
		dictData.set('#BIDPRICE#', '810');
		dictData.set('#ASKPRICE#', '410');
		dictData.set('#STKCLRMINE#', 'Orange');
		dictData.set('#STKCLROTHER#', 'White');
		dictData.set('#INITIATOR.USERNAME1#', 't1');
		dictData.set('#INITIATOR.PASSWORD1#', 'password');
		dictData.set('#INITIATOR.USERNAME2#', 't3');
		dictData.set('#INITIATOR.PASSWORD2#', 'password');
		dictData.set('#OBSERVER1.USERNAME#', 't2');
		dictData.set('#OBSERVER1.PASSWORD#', 'password');
		dictData.set('#OBSERVER3.USERNAME#', 't4');
		dictData.set('#OBSERVER3.PASSWORD#', 'password');
		dictData.set('#OBSERVER4.USERNAME#', 'b1');
		dictData.set('#OBSERVER4.PASSWORD#', 'password');
		dictData.set('#OBSERVER5.USERNAME#', 'b3');
		dictData.set('#OBSERVER5.PASSWORD#', 'password');
		dictData.set('#OBSERVER6.USERNAME#', 'ae1');
		dictData.set('#OBSERVER6.PASSWORD#', 'password');
	};
		return TestData;
})();
module.exports = TestData;
