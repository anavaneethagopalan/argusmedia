var Initiator = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#INITIATOR.USERNAME#',password:'#INITIATOR.PASSWORD#'}]}],
	[{action:'Order.Create',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE1#, BrokerCo #1',colour:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE1#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Kill',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE1#, BrokerCo #1',colour:''}]}],
	[{action:'Order.Create',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE1#, BrokerCo #1',colour:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE1#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Kill',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE1#, BrokerCo #1',colour:''}]}],
	[{action:'Order.Create',params:[{product:'NAPHTHA CIF NWE - DIFF',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE2#, BrokerCo #1',colour:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{product:'NAPHTHA CIF NWE - DIFF',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE2#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Kill',params:[{product:'NAPHTHA CIF NWE - DIFF',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE2#, BrokerCo #1',colour:''}]}],
	[{action:'Order.Create',params:[{product:'NAPHTHA CIF NWE - DIFF',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE2#, BrokerCo #1',colour:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{product:'NAPHTHA CIF NWE - DIFF',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE2#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Kill',params:[{product:'NAPHTHA CIF NWE - DIFF',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE2#, BrokerCo #1',colour:''}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Initiator;
