var Observer3 = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#OBSERVER3.USERNAME#',password:'#OBSERVER3.PASSWORD#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE1#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.CheckNotInStack',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE1#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE1#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'Order.CheckNotInStack',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE1#, BrokerCo #1',colour:'#STKCLROTHER#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Observer3;
