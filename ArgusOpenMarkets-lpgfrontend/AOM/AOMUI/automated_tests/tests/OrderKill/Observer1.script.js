var Observer1 = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#OBSERVER1.USERNAME#',password:'#OBSERVER1.PASSWORD#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE1#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckNotInStack',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE1#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE1#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckNotInStack',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE1#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE1#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - DIFF',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE2#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckNotInStack',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - DIFF',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE2#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckInStackWithColour',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - DIFF',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE2#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.CheckNotInStack',params:[{retryCount:'10',retryWaitTime:'10000',product:'NAPHTHA CIF NWE - DIFF',bidask:'ASK',quantity:'#QUANTITY#',price:'#ASKPRICE2#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: ASK, #QUANTITY#, #ASKPRICE2#, BrokerCo #1',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Observer1;
