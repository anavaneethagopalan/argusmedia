var Initiator = [
	[{action:'#AOM.Start',params:[{browser:'chrome',url:'http://web.aom.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'#INITIATOR.USERNAME#',password:'#INITIATOR.PASSWORD#'}]}],
	[{action:'Order.Create',params:[{delay:'',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',uquantity:'',uprice:'',unotes:'',colour:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{delay:'',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',uquantity:'',uprice:'',unotes:'',colour:'#STKCLRMINE#'}]}],
	[{action:'Order.Update',params:[{delay:'#INITIATOR.DELAY#',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#QUANTITY#',price:'#BIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #QUANTITY#, #BIDPRICE#, BrokerCo #1',uquantity:'#INITIATOR.UPDATEDQUANTITY#',uprice:'#INITIATOR.UPDATEDBIDPRICE#',unotes:'Notes: BID, #INITIATOR.UPDATEDQUANTITY#, #INITIATOR.UPDATEDBIDPRICE#, BrokerCo #1',colour:''}]}],
	[{action:'Order.CheckInStackWithColour',params:[{delay:'',product:'#NAPHTHACARGOES#',bidask:'BID',quantity:'#INITIATOR.UPDATEDQUANTITY#',price:'#INITIATOR.UPDATEDBIDPRICE#',deliverystart:'01/01/2015',deliveryend:'05/01/2015',broker:'BrokerCo #1',notes:'Notes: BID, #INITIATOR.UPDATEDQUANTITY#, #INITIATOR.UPDATEDBIDPRICE#, BrokerCo #1',uquantity:'',uprice:'',unotes:'',colour:'#STKCLRMINE#'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = Initiator;
