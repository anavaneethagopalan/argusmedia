var Automation = require("../../helpers/automation.js");
var automation = new Automation();
var key = require('keyword'); // keyword functionality

var Dictionary = require('dictionaryjs');
var dictData = new Dictionary();

try {
    var TestData = require("../../tests/TestData/" + browser.params.testEnv + ".OrderCreateAsPrincipal.data.js");
    var testData = new TestData();
    testData.load(dictData);
} catch(err) { }

//console.log(dictData.get('USERNAME') +'/'+ dictData.get('PASSWORD') );


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Observer2', function () {
    var hostURL;
    var iStep = 1;
    var specDescription = this.description;

    var username = browser.params.credentials.username;
    var password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        case 'LIVE':
            hostURL = browser.params.hostURL.live;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
    xit("Does nothing", function () {
    }, 120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("running Observer2 test script", require('./Observer2.script.js'),
        function (step) {
            var sObject = step.action.substring(0, step.action.indexOf('.'));
            //console.log( step.action);
            for (var property in (step.params)[0]) {
                if ((step.params)[0].hasOwnProperty(property)) {
                    //console.log(property + '=' + (step.params)[0][property]);
                    (step.params)[0][property] = automation.resolveParams(dictData, (step.params)[0][property]);
                    //console.log(property + '=' + (step.params)[0][property]);
                }
            }
            //console.log('-------------------------------');

            //Load keyword file (name must be in the form 'object.keywords.js'). "try" handles exception when loading same object.keywords.js file
            try { key(require('../../keywords/' + sObject + '.keywords.js')); } catch(err) { }
            it("Step " + iStep++ + ": Keyword=" + step.action + ", Data=[" + step.params + "]", function () {
                if (step.action.slice(0,1) != '#') {
                    key.run(step.action, step.params).then(function () { });

                    /* framework before keywords
                     key.run("before", step.params).then(function () {
                     key.run(step.action, step.params).then(function () {
                     key.run("after", step.params).then(function () { });
                     });
                     });
                     // framework after keywords */
                }
            }, 600000);
        });

}, 600000);
