var ExecuteOrderPage = (function () {
    var bDisplayed;
    var sFormText;
    var sPrice;
    var arrFormText;

    function ExecuteOrderPage() {
        var timeout = 5;

        for (var i = 0; i <= timeout; i++) {
            element.all(by.id('btnExecuteOrderExit')).then(function (buttons) {
                if (buttons.length > 0) {
                    i = timeout;
                } else {
                    browser.sleep(1000);
                }
            });
        }

        element(by.name('OrderForm')).getText().then( function (FormText) {
            sFormText = FormText;
            arrFormText =  FormText.split('\n');
            //sPrice =  FormText.split('\n')[3];
        });

        //sPrice = element(by.model('order.price'));
        this.allFormText = element(by.name('OrderForm')).getText();
        //sPrice = element(by.name('OrderForm')).getText().split('\n')[3];

        this.executeButton = element(by.id('btnExecuteOrder'));
        this.exitButton = element(by.id('btnExecuteOrderExit'));
        bDisplayed = element(by.name('OrderForm')).isDisplayed();
    }

    ExecuteOrderPage.prototype.price = function () {
        return arrFormText[3];
    };

    ExecuteOrderPage.prototype.displayed = function () {
        return bDisplayed;
    };

    ExecuteOrderPage.prototype.formText = function () {
        return this.allFormText;
    };

    ExecuteOrderPage.prototype.execute = function () {
        this.executeButton.click();
    };

    ExecuteOrderPage.prototype.exit = function () {
        this.exitButton.click();
    };

    return ExecuteOrderPage;
})();

module.exports = ExecuteOrderPage;