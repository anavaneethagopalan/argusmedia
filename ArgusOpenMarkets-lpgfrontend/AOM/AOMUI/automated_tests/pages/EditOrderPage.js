var EditOrderPage = (function () {
    var bDisplayed;
    var sFormText;
    var arrFormText;

    function EditOrderPage() {

        element(by.name('editOrderForm')).getText().then( function (FormText) {
            sFormText = FormText;
            arrFormText =  FormText.split('\n');
        });

        this.priceField = element(by.model('order.price'));
        this.quantityField = element(by.model('order.quantity'));
        this.notesField = element(by.model('order.notes'));
        this.deliveryStart = element(by.id('fromDate'));
        this.deliveryEnd = element(by.id('toDate'));
        this.brokerField = element(by.model('order.broker'));

        this.allFormText = element(by.name('editOrderForm')).getText();
        this.holdButton = element(by.id('btnEditOrderHold'));
        this.updateButton = element(by.id('btnEditOrderEdit'));
        this.reinstateButton = element(by.id('btnEditOrderReinstate'));
        this.executeButton = element(by.id('btnEditOrderExecuteMode'));
        this.killButton = element(by.id('btnEditOrderKill'));
        this.exitButton = element(by.id('btnEditOrderExit'));
        bDisplayed = element(by.name('editOrderForm')).isDisplayed();
    }

    EditOrderPage.prototype.price = function () {
        return arrFormText[3];
    };

    EditOrderPage.prototype.displayed = function () {
        return bDisplayed;
    };

    EditOrderPage.prototype.formText = function () {
        return this.allFormText;
    };

    EditOrderPage.prototype.setNotes = function (sNotes) {
        this.notesField.clear();
        this.notesField.sendKeys(sNotes);
    };

    EditOrderPage.prototype.setBroker = function (sBroker) {
        this.brokerField.sendKeys(sBroker);
    };

    EditOrderPage.prototype.setPrice = function (sPrice) {
        this.priceField.clear();
        this.priceField.sendKeys(sPrice);
        return this.priceField.getAttribute('value');
    };

    EditOrderPage.prototype.setQuantity = function (sQuantity) {
        this.quantityField.clear();
        this.quantityField.sendKeys(sQuantity);
    };

    EditOrderPage.prototype.hold = function () {
        this.holdButton.click();
    };

    EditOrderPage.prototype.reinstate = function () {
        this.reinstateButton.click();
    };

    EditOrderPage.prototype.update = function () {
        this.updateButton.click();
    };

    EditOrderPage.prototype.kill = function () {
        this.killButton.click();
    };

    EditOrderPage.prototype.exit = function () {
        this.exitButton.click();
    };

    return EditOrderPage;
})();

module.exports = EditOrderPage;