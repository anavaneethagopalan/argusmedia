var AssessmentsPage = (function () {
    var product;

    function AssessmentsPage() {

        this.assessments = element.all(by.repeater('assessment in assessments'));
        //this.title = element(by.id('assessmentsTitle'));
        this.title = element(by.className('assessment-header-title'));
        this.products = element.all(by.className("assessment-item-product"));
        this.dates = element.all(by.className("assessment-item-title"));
        this.priceRange1 = element.all(by.binding("{{assessment.PriceRange1}}"));
        this.priceRange2 = element.all(by.binding("{{assessment.PriceRange2}}"));
    }

    AssessmentsPage.prototype.getAssessments = function () {
            return this.assessments;
    };

    AssessmentsPage.prototype.getTitle = function () {
        return this.title;
    };

    AssessmentsPage.prototype.getProducts = function () {
        return this.products;
    };

    AssessmentsPage.prototype.getDates = function () {
        return this.dates;
    };

    AssessmentsPage.prototype.getPriceRange1 = function () {
        return this.priceRange1;
    };

    AssessmentsPage.prototype.getPriceRange2 = function () {
        return this.priceRange2;
    };

    return AssessmentsPage;
})();

module.exports = AssessmentsPage;
