// Resources.js
var Dictionary = require('dictionaryjs');
var dict = new Dictionary();

var Resources = (function () {

    function Resources() {

        dict.set('BLACK', 'rgba(6, 6, 6, 1)'); // Black
        dict.set('BLUE', 'rgba(8, 80, 112, 1)'); // Blue
        dict.set('BLUEGREEN', 'rgba(23, 47, 58, 1)'); // BlueGreen
        dict.set('BLUEGREY', 'rgba(66, 74, 81, 1)'); // BlueGrey
        dict.set('BRIGHTRED', 'rgba(204, 0, 0, 1)'); // BrightRed
        dict.set('BROWN', 'rgba(30, 5, 4, 1)'); // Brown
        dict.set('CANARYYELLOW', 'rgba(255, 204, 0, 1)'); // CanaryYellow
        dict.set('CHARCOALGREY', 'rgba(65, 65, 65, 1)'); // CharcoalGrey
        dict.set('CRIMSONRED', 'rgba(82, 32, 30, 1)'); // CrimsonRed
        dict.set('DARKGREEN', 'rgba(0, 51, 51, 1)'); // DarkGreen
        dict.set('DARKGREY', 'rgba(29, 33, 36, 1)'); // DarkGrey
        dict.set('DOVEGREY', 'rgba(102, 102, 102, 1)'); // DoveGrey
        dict.set('LIGHTBLUE', 'rgba(0, 111, 163, 1)'); // LightBlue
        dict.set('LIGHTERBLUE', 'rgba(26, 136, 186, 1)'); // LighterBlue
        dict.set('LIGHTGREEN', 'rgba(0, 128, 0, 1)'); // LightGreen
        dict.set('MARKETTICKERDARKGREY', 'rgba(102, 102, 102, 1)'); // MarketTickerDarkGrey
        dict.set('MARKETTICKERGREY', 'rgba(211, 211, 211, 1)'); // MarketTickerGrey
        dict.set('MARKETTICKERLIGHTBLACK', 'rgba(42, 46, 51, 1)'); // MarketTickerLightBlack
        dict.set('MAROONRED', 'rgba(129, 6, 0, 1)'); // MaroonRed
        dict.set('MINESHAFTGREY', 'rgba(51, 51, 51, 1)'); // MineShaftGrey
        dict.set('NEWSGREY', 'rgba(153, 153, 153, 1)'); // NewsGrey
        dict.set('NEWSLIGHTGREY', 'rgba(120, 120, 120, 1)'); // NewsLightGrey
        dict.set('OLIVE', 'rgba(2, 52, 52, 1)'); // Olive
        dict.set('ORANGE', 'rgba(255, 116, 0, 1)'); // Orange
        dict.set('PASTELGREY', 'rgba(204, 204, 204, 1)'); // PastelGrey
        dict.set('PEAGREEN', 'rgba(75, 154, 49, 1)'); // PeaGreen
        dict.set('RUSTBROWN', 'rgba(49, 27, 28, 1)'); // RustBrown
        dict.set('SEAGREEN', 'rgba(7, 28, 37, 1)'); // SeaGreen
        dict.set('SHARKGREY', 'rgba(40, 40, 40, 1)'); // SharkGrey
        dict.set('SLATEGREY', 'rgba(24, 24, 24, 1)'); // SlateGrey
        dict.set('WHITE', 'rgba(255, 255, 255, 1)'); // White
        dict.set('YELLOW', 'rgba(255, 255, 51, 1)'); // Yellow

    }

    Resources.prototype.CompareColour = function (colorOnScreen, nameOfColour) {
        var result = false;
        var uppercaseColourName = nameOfColour.toUpperCase();

        if(dict.has(uppercaseColourName))
        {
            if(colorOnScreen.toString() == dict.get(uppercaseColourName)) result = true;
        }

        return result;
    };

    return Resources;

})();

module.exports = Resources;