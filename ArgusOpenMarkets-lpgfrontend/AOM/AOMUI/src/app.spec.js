﻿describe( 'AppCtrl', function() {
    describe( 'isCurrentUrl', function() {
        var AppCtrl, $location, $scope;

        beforeEach( module( 'argusOpenMarkets' ) );

        beforeEach( inject( function( $controller, _$location_, $rootScope ) {
            $location = _$location_;
            $scope = $rootScope.$new();
            AppCtrl = $controller( 'applicationController', { $location: $location, $scope: $scope });
        }));

        
        it( 'should load app controller', inject( function() {
            expect( AppCtrl ).toBeTruthy();
        }));
    });

});
