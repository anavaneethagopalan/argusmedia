angular.module("argusOpenMarkets.userSettings")
    .controller("brokerPermissionsController", function ($scope,
                                                         userService,
                                                         brokerPermissionsService,
                                                         marketService,
                                                         permissionsService,
                                                         messagePopupsService) {
        $scope.updateFailed = false;
        $scope.organisations = [];
        $scope.user = userService.getUser();
        $scope.organisationId = $scope.user.organisationId;
        $scope.organisationType = $scope.user.userOrganisation.organisationType;
        $scope.updateAllowed = $scope.user.systemPrivileges.privileges.indexOf("BPMatrix_Update") > -1;
        $scope.updateInProgress = false;
        $scope.updateWasSuccessful = false;
        $scope.errorMessage = "";
        $scope.userHasMadeAChange = false;
        $scope.listChanges = [];
        $scope.markets = [];

        var getProducts = function () {
            brokerPermissionsService.getProducts(onGetProducts);
        };

        $scope.hasChangesFilter = function (c) {
            return c.changesToCommit === true;
        };

        $scope.onTabSelected = function(selectedTab){
            $scope.selectedTab = selectedTab;
            $scope.filters.productIds = selectedTab.productIds;
            $scope.showSelectedTab(selectedTab);
        };

        $scope.showSelectedTab = function () {
            var productIds,
                pi;

            if ($scope.selectedTab) {
                $scope.waiting = true;
                $scope.organisations = [];
                $scope.counterparties = [];
                $scope.lastUpdatedInfo = {"lastUpdated": "-", "lastUpdatedBy": "System"};
                // Clear the filter.
                $scope.filters.counterparty = "";

                productIds = $scope.selectedTab.productIds;

                if (productIds) {
                    for (pi = 0; pi < $scope.products.length; pi++) {
                        var pr = $scope.products[pi];

                        if (permissionsService.productShouldBeVisible(pr.id, productIds)) {
                            pr.filtered = false;
                        } else {
                            pr.filtered = true;
                        }
                    }
                }
                getMatrixPermissions();
            }
        };

        var getBrokerTabs = function () {
            var userMarkets = marketService.filterMarketsForProducts($scope.markets, $scope.products);
            $scope.brokerTabs = permissionsService.makePermissionsTabs(userMarkets);
        };

        var onGetProducts = function (data) {
            $scope.products = data;
            if ($scope.markets && $scope.markets.length) {
                getBrokerTabs();
            }
        };

        var onGetMarkets = function (data) {
            $scope.markets = data;
            if ($scope.products && $scope.products.length) {
                // We have products and markets.
                getBrokerTabs();
            }
        };

        getProducts();
        marketService.getMarkets(onGetMarkets);
        $scope.waiting = true;

        $scope.filters = {
            counterparty: "",
            product: "",
            productIds: [],
            actionRequired: false
        };

        $scope.translateTriStateToPermission = function(permission){
            return permissionsService.translateTriStateToPermission(permission);
        };

        var onBpmMessage = function (message, arrayPermissions) {
            var counter,
                ourProductId = message.productId,
                matrixPermission;

            if (message) {
                for (counter = 0; counter < message.bps.length; counter++) {
                    matrixPermission = permissionsService.calculateMatrixPerm(message.bps[counter], message.userDetails, true);

                    permissionsService.updateLastUpdatedInfo(matrixPermission.buyLastUpdatedDate, matrixPermission.lastUpdatedUsername, $scope.lastUpdatedInfo);
                    permissionsService.updateLastUpdatedInfo(matrixPermission.sellLastUpdatedDate, matrixPermission.lastUpdatedUsername, $scope.lastUpdatedInfo);

                    $scope.organisations = permissionsService.addOrganisationsToList($scope.organisations, message.permittedTradingOrgs);
                    var bpm = permissionsService.findOrCreateCounterparty(matrixPermission.theirOrgId, arrayPermissions, $scope.products, message.permittedTradingOrgs);

                    for (var pi = 0; pi < bpm.products.length; pi++) {
                        var pr = bpm.products[pi];

                        if (pr.id === ourProductId) {
                            var buyOurPermission = permissionsService.translatePermissionToTriState(matrixPermission.buyOurDecision);
                            var buyTheirPermission = permissionsService.translatePermissionToTriState(matrixPermission.buyTheirDecision);

                            pr.buy.us.originalPermission = buyOurPermission;
                            pr.buy.us.permission = buyOurPermission;

                            pr.buy.us.lastUpdated.by = matrixPermission.lastUpdatedUsername;
                            pr.buy.us.lastUpdated.at = matrixPermission.buyLastUpdatedDate;

                            pr.cpSubscribed = true;
                            pr.buy.them.permission = buyTheirPermission;
                            pr.buy.overallStatus = permissionsService.determineStatus(buyOurPermission, buyTheirPermission);

                            var sellOurPermission = permissionsService.translatePermissionToTriState(matrixPermission.sellOurDecision);
                            pr.sell.us.lastUpdated.by = matrixPermission.lastUpdatedUsername;
                            pr.sell.us.lastUpdated.at = matrixPermission.sellLastUpdatedDate;
                            var sellTheirPermission = permissionsService.translatePermissionToTriState(matrixPermission.sellTheirDecision);
                            pr.sell.us.originalPermission = sellOurPermission;
                            pr.sell.us.permission = sellOurPermission;
                            pr.sell.them.permission = sellTheirPermission;
                            pr.sell.overallStatus = permissionsService.determineStatus(sellOurPermission, sellTheirPermission);
                        }
                    }
                }
            }
        };

        var onGetBrokerPermissions = function (data) {

            // arrayPermissions is built up inside onBpmMessage....
            var arrayPermissions = [];
            if (data) {
                for (var d = 0; d < data.length; d++) {
                    onBpmMessage(data[d], arrayPermissions);
                }
            }

            $scope.counterparties = arrayPermissions;
            $scope.waiting = false;
        };

        var getMatrixPermissions = function () {
            var marketId;

            if ($scope.selectedTab) {
                marketId = $scope.selectedTab.marketId;
                // This should be a list of organisation(s) that are going to appear in our Matrix
                brokerPermissionsService.getBrokerPermissionsByMarketId(marketId, $scope.organisationId, onGetBrokerPermissions);
            }
        };

        $scope.showSelectedTab();
        $scope.lastUpdatedInfo = {"lastUpdated": "-", "lastUpdatedBy": "System"};

        $scope.buildPermissionHover = function (productAction, usThem, ourPermission) {
            return permissionsService.buildHoverContent(productAction, usThem, ourPermission);
        };

        $scope.counterpartyFilterChange = function () {

            var filterString = $scope.filters.counterparty.toLowerCase();

            var setFilter = function (co) {
                co.filtered = !(filterString === "" || co.nameAndCode.toLowerCase().indexOf(filterString) > -1);
                if ($scope.filters.actionRequired === true && !co.filtered) {
                    for (var pi = 0; pi < co.products.length; pi++) {
                        var pr = co.products[pi];

                        if (pr.cpSubscribed && (pr.buy.overallStatus === permissionsService.PermissionStatus.conflict || pr.sell.overallStatus === permissionsService.PermissionStatus.PermissionStatus.conflict)) {
                            return;
                        }
                    }
                    co.filtered = true;
                }
            };

            permissionsService.iterateOverMatrixCounterparties(setFilter, $scope.counterparties);
        };

        $scope.actionRequiredFilterChange = function () {
            $scope.counterpartyFilterChange();
        };

        var changePermissions = function (productOperation) {

            if ($scope.updateAllowed) {

                switch (productOperation.us.permission) {
                    case true:
                        permissionsService.updatePermission(productOperation, false);
                        break;
                    case false:
                        permissionsService.updatePermission(productOperation, null);
                        break;
                    default:
                        permissionsService.updatePermission(productOperation, true);
                }

                $scope.userHasMadeAChange = true;
            }
        };

        $scope.permissionChangeClick = function (buyProductOperation, sellProductOperation) {

            // Hack for the Shim - set the Sell Product Operation to be the same as the buy - i/f only has 1 selector.
            sellProductOperation.us.permission = buyProductOperation.us.permission;
            changePermissions(buyProductOperation);

            changePermissions(sellProductOperation);
        };

        $scope.changeAllCPPermissions = function (counterparty, newPermission) {
            var productIds,
                product,
                matchingProduct,
                pi;

            if ($scope.updateAllowed) {

                productIds = $scope.selectedTab.productIds;
                for (pi = 0; pi < counterparty.products.length; pi++) {

                    product = counterparty.products[pi];
                    matchingProduct = _.find(productIds, {productId: product.id});

                    if(matchingProduct && product.cpSubscribed){
                        permissionsService.updatePermission(product.buy, newPermission);
                        permissionsService.updatePermission(product.sell, newPermission);
                        if (product.buy.us.permission !== product.buy.us.originalPermission || product.sell.us.permission !== product.sell.us.originalPermission) {
                            $scope.userHasMadeAChange = true;
                        }
                    }
                }
            }
        };

        $scope.commitChanges = function () {
            // find all where us != originalUs and commit

            var listOfChanges = [];

            permissionsService.iterateOverMatrixProducts(
                function (co, pr) {
                    co.changesToCommit = false;
                    pr.changesToCommit = false;
                }, $scope.counterparties
            );

            permissionsService.iterateOverMatrixProducts(
                function (co, pr) {

                    if (pr.buy.us.permission !== pr.buy.us.originalPermission) {
                        co.changesToCommit = true;
                        pr.changesToCommit = true;
                        listOfChanges.push({action: "buy", change: pr.buy, "co": co, "pr": pr});
                    }
                    if (pr.sell.us.permission !== pr.sell.us.originalPermission) {
                        co.changesToCommit = true;
                        pr.changesToCommit = true;
                        listOfChanges.push({action: "sell", change: pr.sell, "co": co, "pr": pr});
                    }
                }, $scope.counterparties
            );

            if (listOfChanges.length > 0) {
                $scope.errorMessage = "";
                $scope.listChanges = listOfChanges;
                $scope.updateRequested = true;
            }
        };

        $scope.cancelChanges = function () {
            // find all where us != originalUs and revert

            var revert = function (co, pr) {

                if (pr.buy.us.permission !== pr.buy.us.originalPermission) {
                    permissionsService.updatePermission(pr.buy, pr.buy.us.originalPermission);
                }
                if (pr.sell.us.permission !== pr.sell.us.originalPermission) {
                    permissionsService.updatePermission(pr.sell, pr.sell.us.originalPermission);
                }
            };

            permissionsService.iterateOverMatrixProducts(revert, $scope.counterparties);
            $scope.userHasMadeAChange = false;
        };

        var matrixPermissionUpdated = function(buySellPermission, newUpdatedPermission){

            buySellPermission.us.lastUpdated.at = newUpdatedPermission.lastUpdated;
            buySellPermission.us.lastUpdated.by = $scope.user.name;
            buySellPermission.us.originalPermission = buySellPermission.us.permission;
            permissionsService.updatePermission(buySellPermission, buySellPermission.us.permission);
            permissionsService.updateLastUpdatedInfo(newUpdatedPermission.lastUpdated, $scope.user.name, $scope.lastUpdatedInfo);
        };

        var onSaveBrokerPermissions = function (data) {
            var counter,
                counterPartyCounter,
                productCounter,
                updatedMatrixPerm;

            if (data.updateSuccessful) {
                for (counter = 0; counter < data.brokerPermissions.length; counter++) {
                    updatedMatrixPerm = data.brokerPermissions[counter];

                    // Ok - we need to update this permissions in the list of counterparties.
                    for (counterPartyCounter = 0; counterPartyCounter < $scope.counterparties.length; counterPartyCounter++) {
                        if ($scope.counterparties[counterPartyCounter].id === updatedMatrixPerm.theirOrganisation) {
                            // We have found a match - this is the Organisation we have just updated.

                            for (productCounter = 0; productCounter < $scope.counterparties[counterPartyCounter].products.length; productCounter++) {
                                if ($scope.counterparties[counterPartyCounter].products[productCounter].id === updatedMatrixPerm.productId) {
                                    // We have found our product
                                    if (updatedMatrixPerm.buyOrSell.toLowerCase() === "buy") {
                                        // This is a buy permission
                                        matrixPermissionUpdated($scope.counterparties[counterPartyCounter].products[productCounter].buy, updatedMatrixPerm);
                                        $scope.counterparties[counterPartyCounter].products[productCounter].changesToCommit = false;
                                    } else {
                                        // This is a sell permission.
                                        matrixPermissionUpdated($scope.counterparties[counterPartyCounter].products[productCounter].sell, updatedMatrixPerm);
                                        $scope.counterparties[counterPartyCounter].products[productCounter].changesToCommit = false;
                                    }
                                }
                            }
                        }
                    }
                }

                $scope.updateWasSuccessful = true;
                $scope.userHasMadeAChange = false;
                $scope.updateInProgress = false;
                $scope.errorMessage = "";
            } else {
                $scope.listChanges = [];
                $scope.updateInProgress = true;
                $scope.updateWasSuccessful = false;
                $scope.updateRequested = false;
                $scope.failedUpdates = _.where(data.brokerPermissions, {updateSuccessful: false});
                _.each($scope.failedUpdates, function (failedUpdate) {
                    failedUpdate.organisation = permissionsService.getOrganisationName(failedUpdate.theirOrganisation, $scope.organisations);
                    failedUpdate.product = permissionsService.getProductName(failedUpdate.productId, $scope.products);
                });
                $scope.showUpdateFailed = true;
            }
        };

        $scope.updateFailedReloadTab = function () {
            $scope.listChanges = [];
            $scope.userHasMadeAChange = false;
            $scope.updateInProgress = false;
            $scope.showUpdateFailed = false;
            $scope.errorMessage = "";
            $scope.updateRequested = false;
            $scope.showSelectedTab();
        };

        $scope.updateConfirmReject = function () {
            var counterPartyCounter,
                productCounter;

            for (counterPartyCounter = 0; counterPartyCounter < $scope.counterparties.length; counterPartyCounter++) {
                for (productCounter = 0; productCounter < $scope.counterparties[counterPartyCounter].products.length; productCounter++) {

                    $scope.counterparties[counterPartyCounter].products[productCounter].buy.us.originalPermission = $scope.counterparties[counterPartyCounter].products[productCounter].buy.us.permission;
                    permissionsService.updatePermission($scope.counterparties[counterPartyCounter].products[productCounter].buy, $scope.counterparties[counterPartyCounter].products[productCounter].buy.us.originalPermission);

                    $scope.counterparties[counterPartyCounter].products[productCounter].sell.us.originalPermission = $scope.counterparties[counterPartyCounter].products[productCounter].sell.us.permission;
                    permissionsService.updatePermission($scope.counterparties[counterPartyCounter].products[productCounter].sell, $scope.counterparties[counterPartyCounter].products[productCounter].sell.us.originalPermission);
                }
            }

            $scope.listChanges = [];
            $scope.updateRequested = false;
            $scope.userHasMadeAChange = false;
            $scope.updateInProgress = false;
            $scope.errorMessage = "";
        };

        // Update Confirm pressed
        $scope.updateConfirmAccept = function () {
            $scope.updateInProgress = true;
            $scope.updateWasSuccessful = false;
            $scope.errorMessage = "";

            var matrixPermissions = [];
            // Get all the changes for a single product.
            for (var i = 0; i < $scope.listChanges.length; i++) {

                var pr = $scope.listChanges[i].pr;
                var co = $scope.listChanges[i].co;

                var matrixPermission;
                if ($scope.listChanges[i].action === "buy") {
                    if (pr.buy.us.permission !== pr.buy.us.originalPermission) {
                        matrixPermission = {
                            ourOrganisation: userService.getUser().organisationId,
                            theirOrganisation: co.id,
                            productId: pr.id,
                            buyOrSell: "Buy",
                            allowOrDeny: permissionsService.translateTriStateToPermission(pr.buy.us.permission),
                            lastUpdated: pr.buy.us.lastUpdated.at,
                            lastUpdatedUserId: -1
                        };
                        matrixPermissions.push(matrixPermission);
                    }
                } else {
                    if (pr.sell.us.permission !== pr.sell.us.originalPermission) {
                        matrixPermission = {
                            ourOrganisation: userService.getUser().organisationId,
                            theirOrganisation: co.id,
                            productId: pr.id,
                            buyOrSell: "Sell",
                            allowOrDeny: permissionsService.translateTriStateToPermission(pr.sell.us.permission),
                            lastUpdated: pr.sell.us.lastUpdated.at,
                            lastUpdatedUserId: -1
                        };
                        matrixPermissions.push(matrixPermission);
                    }
                }
            }

            // NOTE - We only send one message up - and expect one message down(????).
            brokerPermissionsService.saveBrokerPermissions(matrixPermissions, $scope.organisationId, onSaveBrokerPermissions);
        };

        $scope.closeSuccessConfirmation = function () {
            $scope.listChanges = [];
            $scope.updateInProgress = false;
            $scope.updateWasSuccessful = false;
            $scope.updateRequested = false;
        };

        $scope.onTabChangeDataLosss = function () {
            // TODO: We need to display a data loss warning to the user...
            messagePopupsService.displayDataLossWarning("Change Tab - Data Loss", "Please save changes or cancel changes before selecting new tab");
        };
    });