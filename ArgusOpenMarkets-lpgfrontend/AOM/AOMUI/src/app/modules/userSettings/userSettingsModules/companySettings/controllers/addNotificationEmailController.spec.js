describe('Add Notification Email Controller', function() {
    "use strict";
    var scope,
        addNotificationEmailController,
        mockProductConfigurationService,
        mockOrganisationDetailsService,
        mockModalInstance,
        sendNewEMailNotificationObject = {};

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.dashboard');
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
    });

    beforeEach(function () {
        inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            mockProductConfigurationService = {
                getOrganisationSubscribedProducts:  function(){ return [];}
            };
            mockOrganisationDetailsService = {
                getMappingCategories: function(){ return []; },
                sendNewMapping: function(obj){ sendNewEMailNotificationObject = obj;}
            };
            mockModalInstance = {
                dismiss: function(exitReason) {}
            };

            addNotificationEmailController =
                $controller('addNotificationEmailController',
                    {
                        $scope: scope,
                        $modalInstance: mockModalInstance,
                        productConfigurationService: mockProductConfigurationService,
                        organisationDetailsService: mockOrganisationDetailsService
                    });
        });
    });

    it("should be defined", function(){
        expect( addNotificationEmailController ).toBeDefined();
    });
});
