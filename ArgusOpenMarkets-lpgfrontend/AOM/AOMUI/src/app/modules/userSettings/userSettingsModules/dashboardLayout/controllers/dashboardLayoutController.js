angular.module('argusOpenMarkets.userSettings')
    .controller('dashboardLayoutController', function ($scope,$window, dashboardConfigurationService,productConfigurationService, userDashboardConfigurationService) {
        $scope.layout = [];
        var layout = dashboardConfigurationService.getDashboardLayout();
        var userLayout = [];
        angular.forEach (layout,function(userConfigItem) {
            if(userDashboardConfigurationService.canViewWidget(userConfigItem)){
                userLayout.push(userConfigItem);
            }
        });
        $scope.layout = angular.copy(userLayout);
        $scope.productIds = productConfigurationService.getAllSubscribedProductIds();
        $scope.canAddWidget = function(productIds,itemType){
            if(productIds ==='all'){
                productIds = $scope.productIds;
            }
            var tile ={"productIds": productIds, "view":itemType};
            var canView = userDashboardConfigurationService.canViewWidget(tile);
            var alreadyExists = _.find($scope.layout,function(x) {return (itemType=== x.view) && (x.productIds.compare(productIds) || itemType !== 'bidAskStack');});
            var notOtherBAStack = productConfigurationService.getProductTitleById(productIds[0]) !== "Other";
            return canView && !alreadyExists && notOtherBAStack;
        };


        $scope.$on('$stateChangeStart', function (event, next, current) {
            if($scope.userHasMadeAChange === true) {
                var answer = window.confirm("Do you want to save your screen settings?");
                if (answer) {
                    $scope.commitChanges();
                }
                else{
                    $scope.cancelChanges();
                }
            }
        });
        calculateWindowSize();

        $scope.gridsterOptions = {
            columns: 3, // the width of the grid, in columns
            pushing: true, // whether to push other items out of the way on move or resize
            floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
            swapping: false, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
            width: 630, // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
            colWidth: 204, // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 16, // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [10, 5], // the pixel distance between each widget
            outerMargin: true, // whether margins apply to outer edges of the grid
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: false, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 2, // the minimum height of the grid, in rows
            maxRows: 100,
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            resizable: {
                enabled: false,
                handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
                stop: function (event, $element, widget) {
                }
            },
            draggable: {
                enabled: true,
                stop: function (event, uiWidget, $element) {
                    $scope.userHasMadeAChange = true;
                }
            }
        };

        $scope.$watch(function () {
            return $window.innerWidth;
        }, function (value) {
            calculateWindowSize();
        });

        function calculateWindowSize() {
            var windowWidth = $window.innerWidth / 2.5;
            $scope.visibleLayout = "repeating-linear-gradient( to right, #ccc, #ccc " + windowWidth + "px, transparent " + windowWidth + "px, transparent " + windowWidth * 2 + "px)";
        }

        $scope.removeTile = function (widget) {
            $scope.layout.splice($scope.layout.indexOf(widget), 1);
            $scope.userHasMadeAChange = true;
        };

        var scaleAndCopyLayout = function(){
            var layoutCopy = angular.copy($scope.layout);

            for (var x = 0; x < layoutCopy.length; x++) {
                layoutCopy[x].tile.row = layoutCopy[x].tile.row * 2.5;
            }
            return layoutCopy;
        };

        var saveLayout = function () {
             var layoutCopy = scaleAndCopyLayout();
            dashboardConfigurationService.updateUserTileConfig(layoutCopy);
            dashboardConfigurationService.saveUserTileConfig();
            userLayout = angular.copy($scope.layout);
        };

        $scope.addTile = function(productIdsArgsArr,tileType){
            var layoutCopy = scaleAndCopyLayout();
            if(productIdsArgsArr ==='all'){
                productIdsArgsArr = $scope.productIds;
            }
            dashboardConfigurationService.updateUserTileConfig(layoutCopy);
            dashboardConfigurationService.addSingleTile(tileType,productIdsArgsArr);
            $scope.layout = dashboardConfigurationService.getDashboardLayout();
            $scope.userHasMadeAChange = true;
        };

        $scope.getProductTitle = function(id){
            return productConfigurationService.getProductTitleById(id);
        };

        $scope.commitChanges = function () {
            saveLayout();
            $scope.userHasMadeAChange = false;
        };

        $scope.cancelChanges = function () {
            $scope.layout =userLayout;
            dashboardConfigurationService.updateUserTileConfig(userLayout);
            dashboardConfigurationService.saveUserTileConfig();
            $scope.layout = dashboardConfigurationService.getDashboardLayout();
            $scope.userHasMadeAChange = false;
        };
    });
