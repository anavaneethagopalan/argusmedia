describe('Settings: System Info controller tests', function() {
    "use strict";
    var scope,
        userDetailsController,
        mockSocketProvider,
        mockUserService,
        mockApplicationService,
        mockConnectionStatusService,
        mockPlatformInfoService;

    var fakeDiffusionVersion = "1.2.3.4";
    var fakeAomVersion = "2.3.4.5";
    var fakeOS = "Fake OS v1234 by FakeSoft";
    var fakeBrowserInfo = {name:"BestBrowser"};

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
    });

    beforeEach(function () {
        inject(function ($controller, $rootScope, _mockUserService_) {
            scope = $rootScope.$new();
            mockUserService = _mockUserService_;
            spyOn(mockUserService, 'getUser').and.callThrough();

            mockSocketProvider = jasmine.createSpyObj('mockSocketProvider', ['getClient']);
            mockSocketProvider.getClient.and.returnValue({version: fakeDiffusionVersion});

            mockApplicationService = jasmine.createSpyObj('mockApplicationService', ['getVersionConfiguration']);
            mockApplicationService.getVersionConfiguration.and.returnValue(fakeAomVersion);

            mockConnectionStatusService = jasmine.createSpyObj('mockConnectionStatusService',
                ['isConnected', 'isReconnected', 'setConnectCallback', 'setDisconnectCallback']);
            mockConnectionStatusService.isConnected.and.returnValue(false);
            mockConnectionStatusService.isReconnected.and.returnValue(false);

            mockPlatformInfoService = jasmine.createSpyObj('mockPlatformInfoService',
                ['getOSDescription', 'getBrowserInfo']);
            mockPlatformInfoService.getOSDescription.and.returnValue(fakeOS);
            mockPlatformInfoService.getBrowserInfo.and.returnValue(fakeBrowserInfo);

            userDetailsController = $controller('systemInfoController', {
                $scope: scope,
                applicationService: mockApplicationService,
                userService: mockUserService,
                socketProvider: mockSocketProvider,
                connectionStatusService: mockConnectionStatusService,
                platformInfoService: mockPlatformInfoService
            });
        });
    });

    it('should have a defined controller', function() {
        expect(userDetailsController).toBeDefined();
    });

    it('should call getUser from the user service', function() {
        expect(mockUserService.getUser).toHaveBeenCalled();
    });

    it('should have the correct user name', function() {
        expect(scope.user.name).toEqual(mockUserService.getUser().username);
    });

    it('should have the correct user id', function() {
        expect(scope.user.id).toEqual(mockUserService.getUser().id);
    });

    it('should have the correct user company name', function() {
        expect(scope.company.name).toEqual(mockUserService.getUser().userOrganisation.name);
    });

    it('should have the correct user company ID', function() {
        expect(scope.company.id).toEqual(mockUserService.getUser().userOrganisation.id);
    });

    it('should have the correct application version', function() {
        expect(scope.applicationVersion).toEqual(fakeAomVersion);
    });

    it('should have the correct diffusion version', function() {
        expect(scope.diffusionVersion).toEqual(fakeDiffusionVersion);
    });

    it('should have the correct OS version', function() {
        expect(scope.osVersion).toEqual(fakeOS);
    });

    it('should have the correct browser information', function() {
        expect(scope.browserInfo).toEqual(fakeBrowserInfo);
    });

    it('should set isConnected based on the connection status service', function() {
        expect(scope.isConnected).toBeFalsy();
    });

    it('should set isReconnected based on the connection status service', function() {
        expect(scope.isReconnected).toBeFalsy();
    });

    it('should update connection status to disconnected when callback is called', function() {
        var callback = mockConnectionStatusService.setDisconnectCallback.calls.mostRecent().args[0];
        expect(callback).toEqual(jasmine.any(Function));
        callback();
        expect(scope.isConnected).toBeFalsy();
        expect(scope.isReconnected).toBeFalsy();
        expect(scope.connectionStatus).toMatch("Disconnected");
    });

    it('should update connection status to connected when callback is called', function() {
        var callback = mockConnectionStatusService.setConnectCallback.calls.mostRecent().args[0];
        expect(callback).toEqual(jasmine.any(Function));
        callback(true, false);
        expect(scope.isConnected).toBeTruthy();
        expect(scope.isReconnected).toBeFalsy();
        expect(scope.connectionStatus).toMatch("Connected");
    });

    it('should update connection status to reconnected when callback is called', function() {
        var callback = mockConnectionStatusService.setConnectCallback.calls.mostRecent().args[0];
        expect(callback).toEqual(jasmine.any(Function));
        callback(true, true);
        expect(scope.isConnected).toBeTruthy();
        expect(scope.isReconnected).toBeTruthy();
        expect(scope.connectionStatus).toMatch("Reconnected");
    });

    it('should register a connect callback with the connection status service', function() {
        expect(mockConnectionStatusService.setConnectCallback).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('should register a disconnect callback with the connection status service', function() {
        expect(mockConnectionStatusService.setDisconnectCallback).toHaveBeenCalledWith(jasmine.any(Function));
    });
});
