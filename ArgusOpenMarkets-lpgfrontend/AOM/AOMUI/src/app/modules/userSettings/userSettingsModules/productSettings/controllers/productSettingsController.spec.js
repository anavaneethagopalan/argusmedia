describe('productSettingsController tests', function() {

    var scope, mockUserApi, productSettingsController, mockProductConfigurationService;

    var fakeProduct1 = {
        product: {
            productId:1
        }
    };
    var fakeProduct2 = {
        product: {
            productId:2
        }
    };
    var fakeProduct3 = {
        product: {
            productId:3
        }
    };
    var fakeProducts = [fakeProduct1, fakeProduct2, fakeProduct3],
        productsUserSubscribedTo = [1,2];

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
    });

    beforeEach(function () {
        inject(function ($controller, $rootScope, $q) {
            scope = $rootScope.$new();

            var deferred = $q.defer();
            deferred.resolve(fakeProducts);

            mockProductConfigurationService = jasmine.createSpyObj('mockProductConfigurationService', ['getAllSubscribedProductIds']);
            mockProductConfigurationService.getAllSubscribedProductIds.and.callFake(function() { return productsUserSubscribedTo; });

            mockUserApi = jasmine.createSpyObj('mockUserApi', ['getProductSettings']);
            mockUserApi.getProductSettings.and.callFake(function() { return deferred.promise; });

            productSettingsController =
                $controller('productSettingsController',
                    {
                        $scope: scope,
                        userApi: mockUserApi,
                        productConfigurationService: mockProductConfigurationService
                    });
        });
    });

    beforeEach(function() {
        scope.$apply();
    });

    it('should have a defined controller and scope', function() {
        expect(productSettingsController).toBeDefined();
        expect(scope).toBeDefined();
    });

    it('should set the products on the scope to those returned by the userApi', function() {
        expect(scope.products).toBeDefined();
        expect(scope.products.length).toEqual(fakeProducts.length -1);
        for (var i = 0; i < scope.products.length; ++i) {
            expect(scope.products[i].product).toEqual(fakeProducts[i].product);
        }
    });

    it('should set all products to not collapsed on creation', function() {
        scope.products.forEach(function(p) {
            expect(p.collapsed).toBe(false);
        });
    });

    it('should report that products are not collapsed', function(){
        expect(scope.isCollapsedProduct(fakeProduct1)).toBeFalsy();
        expect(scope.isCollapsedProduct(fakeProduct2)).toBeFalsy();
    });

    it('should set a product to collapsed when expandCollapseProduct called', function(){
        scope.expandCollapseProduct(fakeProduct1);
        expect(scope.isCollapsedProduct(fakeProduct1)).toBeTruthy();
        expect(scope.isCollapsedProduct(fakeProduct2)).toBeFalsy();
    });

    it('should set a collapsed product to not collapsed when expandCollapseProduct called', function() {
        scope.expandCollapseProduct(fakeProduct1);
        expect(scope.isCollapsedProduct(fakeProduct1)).toBeTruthy();
        scope.expandCollapseProduct(fakeProduct1);
        expect(scope.isCollapsedProduct(fakeProduct1)).toBeFalsy();
    });

    it('should set have ', function() {
        scope.expandCollapseProduct(fakeProduct1);
        expect(scope.isCollapsedProduct(fakeProduct1)).toBeTruthy();
        scope.expandCollapseProduct(fakeProduct1);
        expect(scope.isCollapsedProduct(fakeProduct1)).toBeFalsy();
    });

    it('should only store the products the user is subscribed to', function() {

        expect(scope.products).toBeDefined();
        expect(scope.products.length).toEqual(productsUserSubscribedTo.length);
        for (var i = 0; i < scope.products.length; ++i) {
            expect(scope.products[i].product).toEqual(fakeProducts[i].product);
        }
    });

});
