describe('DashboardLayout controller tests', function() {
    "use strict";
    var scope,window,layoutController;
    var layout;
    var mockDashboardConfigurationService =  {
        getDashboardLayout: function(){},
        saveUserTileConfig: function(){},
        updateUserTileConfig:function(){}
    };
    var mockUserDashboardConfigurationService ={canViewWidget: function(){return true}};

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
    });

    beforeEach(function () {
        layout =[{tile:{row:0,col:0}},{tile:{row:8,col:2}}];
        spyOn(mockDashboardConfigurationService,'saveUserTileConfig');
        spyOn(mockDashboardConfigurationService,'getDashboardLayout').and.returnValue(layout);
        inject(function ($controller, $rootScope,$window, mockProductConfigurationService) {
            window= $window;
            scope = $rootScope.$new();
            layoutController = $controller('dashboardLayoutController',{$scope:scope,$window: window,dashboardConfigurationService:mockDashboardConfigurationService, productConfigurationService: mockProductConfigurationService, userDashboardConfigurationService:mockUserDashboardConfigurationService });
        });
    });


    it("should save the tiles on save", function(){
        scope.commitChanges();
        expect(mockDashboardConfigurationService.saveUserTileConfig).toHaveBeenCalled( );
    });

});
