angular.module('argusOpenMarkets.userSettings')
    .controller('companySettingsController',
    function(
        $scope,
        $modal,
        userService,
        organisationDetailsService){

        $scope.$on("CompanyEventNotifications",function(){
            $scope.mappings = organisationDetailsService.getMappings();
            $scope.notificationTypes = organisationDetailsService.getMappingCategories();
            $scope.$digest();
        });

        $scope.orgEmail = userService.getUsersOrganisation().email;

        $scope.delMapping = function(){
            angular.forEach($scope.selection, function(selected){
                userService.sendStandardFormatMessageToUsersTopic(
                    'OrganisationNotificationDetail',
                    'Delete',
                    selected
                );
            });
            $scope.selection = [];
        };

        $scope.toggleSelection = function (mapping) {
            var idx = $scope.selection.indexOf(mapping);

            // is currently selected
            if (idx > -1) {
                $scope.selection.splice(idx, 1);
            }

            // is newly selected
            else {
                $scope.selection.push(mapping);
            }
        };

        $scope.addMapping = function() {
          var modalInstance = $modal.open({
              templateUrl: 'src/app/modules/userSettings/userSettingsModules/companySettings/views/addNotificationEmail.part.html',
              controller: 'addNotificationEmailController',
              backdrop: 'static'
          });
        };

        if(userService.getUsersOrganisation()){
            $scope.userOrganisationName = userService.getUsersOrganisation().name;
            $scope.userOrganisationLegalName = userService.getUsersOrganisation().legalName;
            $scope.userOrganisationShortCode = userService.getUsersOrganisation().shortCode;
            $scope.userOrganisationAddress = userService.getUsersOrganisation().address;
        }

        $scope.mappings = organisationDetailsService.getMappings();
        $scope.notificationTypes = organisationDetailsService.getMappingCategories();



        // selected fruits
        $scope.selection = [];

        $scope.isNotificationsCollapsed = false;
        var user = userService.getUser();
        $scope.editEnabled = user.systemPrivileges.privileges.indexOf("CompanySettings_Amend") > -1;

    });