﻿angular.module('argusOpenMarkets.shared').service("mockBrokerPermissionsService", function mockBrokerPermissionsService() {
    "use strict";

    var PermissionStatus = {
        allow: 'allow',
        forbid: 'forbid',
        conflict: 'conflict'
    };

    var updatePermission = function (productOperation, permission) {
        productOperation.us = permission;
        productOperation.overallStatus = determineStatus(productOperation.us, productOperation.them);
    };

    var determineStatus = function (us, them) {
        switch (us) {
            case true:
                switch (them) {
                    case true:
                        return PermissionStatus.allow;
                    case false:
                        return PermissionStatus.conflict;
                    default:
                        return PermissionStatus.conflict;
                }
                break;
            case false:
                switch (them) {
                    case true:
                        return PermissionStatus.conflict;
                    case false:
                        return PermissionStatus.forbid;
                    default:
                        return PermissionStatus.forbid;
                }
                break;
            default:
                switch (them) {
                    case true:
                        return PermissionStatus.conflict;
                    case false:
                        return PermissionStatus.forbid;
                    default:
                        return PermissionStatus.forbid;
                }
        }
    };

    var getProducts = function (callback) {
        var products = [
            {
                id: 1,
                name: "Naphtha CIF NWE - Cargoes",
                filtered: false
            },
            {
                id: 2,
                name: "Naphtha FOB NWE - Barges",
                filtered: false
            },
            {
                id: 3,
                name: "Jet Fuel",
                filtered: false
            }
        ];

        callback(products);
    };

    var getMockCounterparties = function () {
        return [
            {
                id: 0,
                name: "BASF",
                nameAndCode: "BASF (BASF)"
            },
            {
                id: 2,
                name: "BP",
                nameAndCode: "BP (BP)"
            },
            {
                id: 3,
                name: "DOW CHEMICAL",
                nameAndCode: "DOW CHEMICAL (DWC)"
            },
            {
                id: 4,
                name: "INDIAN OIL CORP",
                nameAndCode: "INDIAN OIL CORP (IDO)"
            },
            {
                id: 5,
                name: "MERCURIA",
                nameAndCode: "MERCURIA (MER)"
            }
        ];
    };

    var findOrCreateCounterparty = function (theirOrganisation, cpmCounterparties, cpmProducts) {

        for (var i = 0; i < cpmCounterparties.length; i++) {
            var c = cpmCounterparties[i];
            if (c.id === theirOrganisation) {
                return c;
            }
        }
        var newCPM = {
            id: theirOrganisation,
            name: "<Unknown Id:" + theirOrganisation + ">",
            filtered: false,
            products: []
        };
        for (var pi = 0; pi < cpmProducts.length; pi++) {
            newCPM.products.push(
                {
                    id: cpmProducts[pi].id,
                    buy: {
                        us: {permission: null, originalPermission: null, lastUpdated: {by: null, at: null}},
                        them: {permission: null, lastUpdated: {by: null, at: null}},
                        overallStatus: "forbid"
                    },
                    sell: {
                        us: {permission: null, originalPermission: null, lastUpdated: {by: null, at: null}},
                        them: {permission: null, lastUpdated: {by: null, at: null}},
                        overallStatus: "forbid"
                    }
                }
            );
        }
        cpmCounterparties.push(newCPM);
        return newCPM;
    };

    var getBrokerPermissionsByMarketId = function(marketId, organisationId, callback){
        var data = [{
            "productId": 1,
            "marketId": marketId,
            "bps": [{
                "orgId": 5,
                "buyOd": "a",
                "buyOdLastUpdated": "2017-02-16T15:34:42.308Z",
                "buyUpdateUser": "UT1",
                "buyTd": "a",
                "buyTdLastUpdated": "2014-12-18T14:44:47Z",
                "sellOd": "a",
                "sellOdLastUpdated": "2017-02-16T15:34:42.397Z",
                "sellUpdateUser": "",
                "sellTd": "a",
                "sellTdLastUpdated": "2014-12-18T14:44:47Z"
            }, {
                "orgId": 6,
                "buyOd": "a",
                "buyOdLastUpdated": "2017-02-16T15:34:42.484Z",
                "buyUpdateUser": "UT1",
                "buyTd": "a",
                "buyTdLastUpdated": "2014-12-18T14:44:47Z",
                "sellOd": "a",
                "sellOdLastUpdated": "2017-02-16T15:34:42.591Z",
                "sellUpdateUser": "",
                "sellTd": "a",
                "sellTdLastUpdated": "2014-12-18T14:44:47Z"
            }, {
                "orgId": 64,
                "buyOd": "a",
                "buyOdLastUpdated": "2017-02-16T15:34:42.759Z",
                "buyUpdateUser": "UT1",
                "buyTd": "d",
                "buyTdLastUpdated": "2017-02-15T14:20:58.265Z",
                "sellOd": "a",
                "sellOdLastUpdated": "2017-02-16T15:34:43.083Z",
                "sellUpdateUser": "",
                "sellTd": "d",
                "sellTdLastUpdated": "2017-02-15T14:20:58.343Z"
            }, {
                "orgId": 65,
                "buyOd": "a",
                "buyOdLastUpdated": "2017-02-16T15:34:43.162Z",
                "buyUpdateUser": "UT1",
                "buyTd": "a",
                "buyTdLastUpdated": "2014-12-18T14:44:47Z",
                "sellOd": "a",
                "sellOdLastUpdated": "2017-02-16T15:34:43.352Z",
                "sellUpdateUser": "",
                "sellTd": "a",
                "sellTdLastUpdated": "2014-12-18T14:44:47Z"
            }],
            "userDetails": [{"id": "UT1", "username": "Adam Smith"}],
            "permittedTradingOrgs": [{
                "id": 5,
                "shortCode": "BR1",
                "name": "BrokerCo #1",
                "legalName": "BrokerCo #1 Ltd"
            }, {"id": 6, "shortCode": "BR2", "name": "BrokerCo #2", "legalName": "BrokerCo #2 Ltd"}, {
                "id": 64,
                "shortCode": "ZUL",
                "name": "Zulu Brokerage",
                "legalName": "Zulu Brokerage"
            }, {"id": 65, "shortCode": "VIC", "name": "Victor Brokerage", "legalName": "Victor Brokerage"}]
        }];

        callback(data);
    };

    return {
        getProducts: getProducts,
        updatePermission: updatePermission,
        determineStatus: determineStatus,
        findOrCreateCounterparty: findOrCreateCounterparty,
        PermissionStatus: PermissionStatus,
        getBrokerPermissionsByMarketId: getBrokerPermissionsByMarketId
    };
});