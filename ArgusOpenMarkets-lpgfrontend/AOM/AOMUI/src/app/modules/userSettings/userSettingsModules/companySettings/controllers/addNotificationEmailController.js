angular.module('argusOpenMarkets.dashboard')
    .controller('addNotificationEmailController',
    function addNotificationEmailController($scope,
                                           $modalInstance,
                                           productConfigurationService,
                                           organisationDetailsService) {

        $scope.email = {address:""};
        $scope.selectedMappingCategory = "";
        $scope.marketTickerInfoProducts = [];
        angular.forEach(productConfigurationService.getOrganisationSubscribedProducts(), function(product){
            $scope.marketTickerInfoProducts.push(product);
        });
        $scope.mappingCategories = angular.copy(organisationDetailsService.getMappingCategories());

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.submitForm = function() {
            angular.forEach(getTickedProducts(), function(product) {
                organisationDetailsService.sendNewMapping({
                    productid: product.productId,
                    eventType: $scope.selectedMappingCategory.id,
                    emailAddress: $scope.email.address
                });
            });
            $scope.exit();
        };

        $scope.selectMappingCategory = function(category){
            $scope.selectedMappingCategory = category;
        };

        $scope.checkIfFormIsReadyToSubmit = function(){
            if($scope.selectedMappingCategory.description !== ""){
                if(getTickedProducts().length > 0){
                    return true;
                }
            }
            return false;
        };

        var getTickedProducts = function() {
            var tickedProducts = [];
            angular.forEach($scope.marketTickerInfoProducts, function(product){
                if(product.ticked){
                    tickedProducts.push(product);
                }
            });
            return tickedProducts;
        };
    });