angular.module('argusOpenMarkets.userSettings')
    .controller('systemInfoController', function
        ($scope,
         applicationService,
         userService,
         socketProvider,
         connectionStatusService,
         platformInfoService) {

        $scope.isConnected = connectionStatusService.isConnected();
        $scope.isReconnected = connectionStatusService.isReconnected();

        var toConnectionStatus = function () {
            if ($scope.isConnected) {
                return $scope.isReconnected ? "Reconnected" : "Connected";
            }
            return "Disconnected";
        };

        $scope.connectionStatus = toConnectionStatus();

        connectionStatusService.setConnectCallback(function (isConnected, isReconnected) {
            $scope.isConnected = isConnected;
            $scope.isReconnected = isReconnected;
            $scope.connectionStatus = toConnectionStatus();
        });

        connectionStatusService.setDisconnectCallback(function () {
            $scope.isConnected = false;
            $scope.isReconnected = false;
            $scope.connectionStatus = toConnectionStatus();
        });

        var user = userService.getUser();
        $scope.user = {
            name: user.username,
            id: user.id
        };
        $scope.company = {
            name: user.userOrganisation.name,
            id: user.userOrganisation.id
        };
        $scope.applicationVersion = applicationService.getVersionConfiguration();
        $scope.osVersion = platformInfoService.getOSDescription();
        $scope.browserInfo = platformInfoService.getBrowserInfo();
        $scope.isCollapsedBrowserInfo = false;
        $scope.isCollapsedVersionInfo = false;

        $scope.$on('$destroy', function () {
            connectionStatusService.clearConnectCallback();
            connectionStatusService.clearDisconnectCallback();
        });
    });
