describe('User details controller tests', function () {
    "use strict";
    var rootscope,
        scope,
        userDetailsController,
        mockUserService,
        mockProductConfigurationService,
        deferred,
        getResolvedData = function () {
            return "resolvedData";
        }


    var mockUserApi = {
        getChangeDetailsResource: function () {
        },
        getProductPrivileges: function () {

        }
    };


    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
    });

    beforeEach(function () {
        inject(function ($controller, $rootScope, _mockUserService_, _mockProductConfigurationService_, $q) {
            rootscope = $rootScope;
            scope = $rootScope.$new();
            mockUserService = _mockUserService_;
            deferred = $q.defer();
            spyOn(mockUserService, 'getUser').and.callThrough();
            spyOn(mockUserApi, 'getChangeDetailsResource').and.returnValue({
                post: function () {
                    return {$promise: deferred.promise}
                }
            });
            spyOn(mockUserApi, 'getProductPrivileges').and.returnValue(deferred.promise);
            mockProductConfigurationService = _mockProductConfigurationService_;
            userDetailsController = $controller('userDetailsController', {
                $scope: scope,
                userService: mockUserService,
                productConfigurationService: mockProductConfigurationService,
                userApi: mockUserApi
            });
        });
    });

    it("should try to get the user", function () {

        expect(mockUserService.getUser).toHaveBeenCalled();
    });

    it("should allow the user to change password if it is valid", function () {
        scope.changePasswordValid = true;
        scope.changePassword();
        deferred.resolve(getResolvedData());
        scope.$digest();
        expect(scope.changePasswordMessage).toEqual("Password changed");
    });

    it("should not allow the user to change password if it is invalid", function () {
        scope.changePasswordValid = false;
        scope.changePassword();
        scope.$digest();
        expect(mockUserApi.getChangeDetailsResource).not.toHaveBeenCalled();
    });

    it("should alert the user if the password was not changed", function () {
        scope.changePasswordValid = true;
        scope.changePassword();
        deferred.reject('reject');
        scope.$digest();
        expect(scope.changePasswordMessage).toEqual("Password change failed");
    });

    describe("product privileges get description for key", function () {
        var privileges = [];
        beforeEach(function () {

            privileges.push({"name": "A", "description": "Aaaaaa"});
            privileges.push({"name": "B", "description": "Bbbbbb"});
        });

        it("should return a description for a given key if the key exists", function () {
            var result;

            result = scope.getKeyDescription("B", privileges);
            expect(result).toEqual("Bbbbbb");
        });

        it("should return the original key if the key does not exist in the list of privileges", function () {
            var result;
            result = scope.getKeyDescription("ZKEY", privileges);
            expect(result).toEqual("ZKEY");
        });
    });

    describe("on product privileges", function () {

        var productPrivileges = {
            "productPrivileges": [
                {
                    "id": 1,
                    "name": "order_Amend_Kill",
                    "description": "Enter market price assessments",
                    "defaultRoleGroup": null,
                    "organisationType": "Argus",
                    "marketMustBeOpen": false
                }
            ],
            "systemPrivileges": [
                {
                    "id": 10,
                    "name": "CPMatrix_Update",
                    "description": "Access to the AOM System Administration Tool",
                    "argusUseOnly": false,
                    "defaultRoleGroup": null,
                    "organisationType": 2
                }
            ]
        };

        it("should call getProductPrivileges", function () {

            expect(mockUserApi.getProductPrivileges).toHaveBeenCalled();
        });

        it("should set a scope variable containing system privileges", function () {

            getResolvedData = function () {
                return productPrivileges;
            };

            deferred.resolve(getResolvedData());
            scope.$digest();
            expect(scope.systemPrivileges).not.toBe(null);
            expect(scope.systemPrivileges[0].productTitle).toEqual("System Privileges");
        });

        it("should set a scope variable containing user product privileges", function () {

            getResolvedData = function () {
                return productPrivileges;
            };

            deferred.resolve(getResolvedData());
            scope.$digest();
            expect(scope.userProductPrivileges).not.toBe(null);
            expect(scope.userProductPrivileges[0].productTitle).toEqual("Test");
        });
    });
});
