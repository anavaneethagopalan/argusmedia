﻿angular.module('argusOpenMarkets.shared').service("brokerPermissionsService",
    function brokerPermissionsService(userService,
                                      productConfigurationService,
                                      userApi) {
        "use strict";

        var bpmProducts = [];

        var getProducts = function (callback) {
            bpmProducts = [];

            var products = productConfigurationService.getOrganisationSubscribedProducts();

            for (var product in products) {
                if (products.hasOwnProperty(product)) {
                    var p = products[product];
                    if (userService.hasProductPrivilege(p.productId, ["View_BidAsk_Widget"])) {
                        bpmProducts.push({
                            id: p.productId,
                            name: p.name,
                            filtered: false
                        });
                    }
                }
            }

            if (callback) {
                callback(bpmProducts);
            }
            return bpmProducts;
        };

        var getBrokerPermissionsByMarketId = function (marketId, organisationId, callback) {

            if (marketId) {
                userApi.getBrokerPermissionsByMarketId(marketId, organisationId, callback);
            }
        };

        var saveBrokerPermissions = function (matrixPermissions, organisationId, callback) {

            if (matrixPermissions) {
                userApi.saveBrokerPermissions(matrixPermissions, organisationId, callback);
            }
        };

        return {
            getProducts: getProducts,
            getBrokerPermissionsByMarketId: getBrokerPermissionsByMarketId,
            saveBrokerPermissions: saveBrokerPermissions
        };
    });