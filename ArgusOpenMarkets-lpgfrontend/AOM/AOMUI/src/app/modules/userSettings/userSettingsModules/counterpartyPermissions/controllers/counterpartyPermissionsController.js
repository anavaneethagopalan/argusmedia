angular.module('argusOpenMarkets.userSettings')
    .controller('counterpartyPermissionsController',
        function ($scope,
                  userService,
                  counterpartyPermissionsService,
                  messageSubscriptionService2,
                  permissionsService,
                  marketService,
                  messagePopupsService) {

            $scope.user = userService.getUser();
            $scope.counterparties = [];
            $scope.updateFailed = false;
            $scope.organisationId = $scope.user.organisationId;
            $scope.listChanges = [];
            $scope.updateAllowed = $scope.user.systemPrivileges.privileges.indexOf("CPMatrix_Update") > -1;
            $scope.userHasMadeAChange = false;
            $scope.updateRequested = false;
            $scope.updateInProgress = false;
            $scope.updateWasSuccessful = false;
            $scope.errorMessage = "";

            var matrixByMarket = [];

            $scope.filters = {
                counterparty: "",
                product: "",
                actionRequired: false
            };

            // var storeMatrixByMarketId = function (marketId, matrix, organisations, lastUpdatedInfo) {
            //     var existingMatrix = _.find(matrixByMarket, function (matrix) {
            //         return matrix.marketId === marketId;
            //     });
            //
            //     if (existingMatrix) {
            //         existingMatrix.counterparties = angular.copy(matrix);
            //         existingMatrix.organisations = angular.copy(organisations);
            //         existingMatrix.lastUpdatedInfo = angular.copy(lastUpdatedInfo);
            //     } else {
            //         matrixByMarket.push({
            //             'marketId': marketId,
            //             'counterparties': matrix,
            //             'organisations': organisations,
            //             'lastUpdatedInfo': lastUpdatedInfo
            //         });
            //     }
            // };

            // var getMatrixByMarket = function (marketId) {
            //     var existingMatrix = _.find(matrixByMarket, function (matrix) {
            //         return matrix.marketId === marketId;
            //     });
            //
            //     if (existingMatrix) {
            //         return existingMatrix;
            //     }
            //
            //     return null;
            // };

            var getBrokerTabs = function () {
                var userMarkets = marketService.filterMarketsForProducts($scope.markets, $scope.products);
                $scope.brokerTabs = permissionsService.makePermissionsTabs(userMarkets);
            };

            var onGetMarkets = function (marketData) {
                $scope.markets = marketData;
                if ($scope.products && $scope.products.length) {
                    // We have products and markets.
                    getBrokerTabs();
                }
            };

            var getProducts = function () {
                return counterpartyPermissionsService.getProducts();
            };

            marketService.getMarkets(onGetMarkets);

            $scope.products = getProducts();
            $scope.lastUpdatedInfo = {"lastUpdated": "-", "lastUpdatedBy": "System"};
            $scope.waiting = true;

            $scope.translateTriStateToPermission = function (triState) {
                return permissionsService.translateTriStateToPermission(triState);
            };


            $scope.buildPermissionHover = function (productAction, usThem, ourPermission) {
                return permissionsService.buildHoverContent(productAction, usThem, ourPermission);
            };

            var onCpmMessage = function (message) {
                var counter,
                    ourProductId,
                    dataResponse,
                    responseCounter,
                    currentEntity,
                    matrixPerm;

                if (message) {
                    for (responseCounter = 0; responseCounter < message.length; responseCounter++) {
                        dataResponse = message[responseCounter];
                        ourProductId = dataResponse.productId;
                        $scope.organisations = permissionsService.addOrganisationsToList($scope.organisations, dataResponse.permittedTradingOrgs);
                        for (counter = 0; counter < dataResponse.bps.length; counter++) {
                            currentEntity = dataResponse.bps[counter];
                            matrixPerm = permissionsService.calculateMatrixPerm(currentEntity, dataResponse.userDetails, false);

                            permissionsService.updateLastUpdatedInfo(matrixPerm.buyLastUpdatedDate, matrixPerm.lastUpdatedUsername, $scope.lastUpdatedInfo);
                            permissionsService.updateLastUpdatedInfo(matrixPerm.sellLastUpdatedDate, matrixPerm.lastUpdatedUsername, $scope.lastUpdatedInfo);

                            // Create their counterparty decision....
                            var cpm = permissionsService.findOrCreateCounterparty(matrixPerm.theirOrgId, $scope.counterparties, $scope.products, dataResponse.permittedTradingOrgs);

                            for (var pi = 0; pi < cpm.products.length; pi++) {
                                var pr = cpm.products[pi];

                                if (pr.id === ourProductId) {
                                    pr.buy.us.originalPermission = permissionsService.translatePermissionToTriState(matrixPerm.buyOurDecision);
                                    pr.buy.us.permission = pr.buy.us.originalPermission;

                                    pr.buy.us.lastUpdated.by = matrixPerm.lastUpdatedUsername;
                                    pr.buy.us.lastUpdated.at = matrixPerm.buyLastUpdatedDate;

                                    pr.cpSubscribed = true;
                                    pr.buy.them.permission = permissionsService.translatePermissionToTriState(matrixPerm.sellTheirDecision);
                                    pr.buy.overallStatus = permissionsService.determineStatus(permissionsService.translatePermissionToTriState(matrixPerm.buyOurDecision), permissionsService.translatePermissionToTriState(matrixPerm.sellTheirDecision));

                                    pr.sell.us.lastUpdated.by = matrixPerm.lastUpdatedUsername;
                                    pr.sell.us.lastUpdated.at = matrixPerm.sellLastUpdatedDate;
                                    pr.sell.us.originalPermission = permissionsService.translatePermissionToTriState(matrixPerm.sellOurDecision);
                                    pr.sell.us.permission = permissionsService.translatePermissionToTriState(matrixPerm.sellOurDecision);
                                    pr.sell.them.permission = permissionsService.translatePermissionToTriState(matrixPerm.buyTheirDecision);
                                    pr.sell.overallStatus = permissionsService.determineStatus(permissionsService.translatePermissionToTriState(matrixPerm.sellOurDecision), permissionsService.translatePermissionToTriState(matrixPerm.buyTheirDecision));
                                }
                            }
                        }
                    }
                }

                // storeMatrixByMarketId($scope.selectedTab.marketId, $scope.counterparties, $scope.organisations, $scope.lastUpdatedInfo);
                $scope.waiting = false;
                // $scope.updateInProgress = false;
                // $scope.updateWasSuccessful = true;
            };

            // Counterparty filter updated
            $scope.counterpartyFilterChange = function () {

                var filterString = $scope.filters.counterparty.toLowerCase();

                var setFilter = function (co) {
                    co.filtered = !(filterString === "" || co.nameAndCode.toLowerCase().indexOf(filterString) > -1);
                    if ($scope.filters.actionRequired === true && !co.filtered) {
                        for (var pi = 0; pi < co.products.length; pi++) {
                            var pr = co.products[pi];

                            if (pr.cpSubscribed && (pr.buy.overallStatus === permissionsService.PermissionStatus.conflict || pr.sell.overallStatus === permissionsService.PermissionStatus.conflict)) {
                                return;
                            }
                        }
                        co.filtered = true;
                    }
                };

                permissionsService.iterateOverMatrixCounterparties(setFilter, $scope.counterparties);
            };

            // Product filter updated
            $scope.productFilterChange = function () {

                var filterString = $scope.filters.product.toLowerCase();

                for (var pi = 0; pi < $scope.products.length; pi++) {
                    var pr = $scope.products[pi];
                    pr.filtered = !($scope.filters.product === "" || pr.name.toLowerCase().indexOf(filterString) > -1);
                }
            };

            // Action Required filter updated
            $scope.actionRequiredFilterChange = function () {
                $scope.counterpartyFilterChange();
            };

            // Permission clicked on
            $scope.permissionChangeClick = function (productOperation) {

                if ($scope.updateAllowed) {

                    switch (productOperation.us.permission) {
                        case true:
                            permissionsService.updatePermission(productOperation, false);
                            break;
                        case false:
                            permissionsService.updatePermission(productOperation, null);
                            break;
                        default:
                            permissionsService.updatePermission(productOperation, true);
                    }

                    $scope.userHasMadeAChange = true;
                }
            };

            $scope.onTabSelected = function (selectedTab) {

                $scope.selectedTab = selectedTab;
                // $scope.filters.productIds = selectedTab.productIds;
                $scope.showSelectedTab(selectedTab);
            };

            var getCounterparties = function () {
                var marketId;

                if ($scope.selectedTab) {
                    marketId = $scope.selectedTab.marketId;
                    // This should be a list of organisation(s) that are going to appear in our Matrix
                    $scope.organisations = [];


                    counterpartyPermissionsService.getCounterpartyPermissionsByMarketId(marketId, $scope.organisationId).then(function (data) {
                        if (data) {
                            onCpmMessage(data);
                        }
                    });

                }
            };

            $scope.showSelectedTab = function () {
                var productIds;

                if ($scope.selectedTab) {
                    productIds = $scope.selectedTab.productIds;

                    if (productIds) {
                        for (var pi = 0; pi < $scope.products.length; pi++) {
                            if (permissionsService.productShouldBeVisible($scope.products[pi].id, productIds)) {
                                $scope.products[pi].filtered = false;
                            } else {
                                $scope.products[pi].filtered = true;
                            }
                        }
                    }

                    $scope.waiting = true;
                    $scope.organisations = [];
                    $scope.counterparties = [];
                    $scope.lastUpdatedInfo = {"lastUpdated": "-", "lastUpdatedBy": "System"};
                    // Clear the filter.
                    $scope.filters.counterparty = "";

                    getCounterparties();
                }
            };

            // Counterparty ban or allow button pressed
            $scope.changeAllCPPermissions = function (counterparty, newPermission) {
                var productIds,
                    pi,
                    product,
                    matchingProduct;

                if ($scope.updateAllowed) {
                    productIds = $scope.selectedTab.productIds;

                    for (pi = 0; pi < counterparty.products.length; pi++) {
                        product = counterparty.products[pi];
                        matchingProduct = _.find(productIds, {productId: product.id});

                        if (matchingProduct && product.cpSubscribed) {
                            permissionsService.updatePermission(product.buy, newPermission);
                            permissionsService.updatePermission(product.sell, newPermission);
                            if (product.buy.us.permission !== product.buy.us.originalPermission || product.sell.us.permission !== product.sell.us.originalPermission) {
                                $scope.userHasMadeAChange = true;
                            }
                        }
                    }
                }
            };

            // Commit button pressed
            $scope.commitChanges = function () {
                // find all where us != originalUs and commit

                var listOfChanges = [];

                permissionsService.iterateOverMatrixProducts(
                    function (co, pr) {
                        co.changesToCommit = false;
                        pr.changesToCommit = false;
                    }, $scope.counterparties
                );
                permissionsService.iterateOverMatrixProducts(
                    function (co, pr) {

                        if (pr.buy.us.permission !== pr.buy.us.originalPermission) {
                            co.changesToCommit = true;
                            pr.changesToCommit = true;
                            listOfChanges.push({'action': "buy", 'change': pr.buy, 'co': co, 'pr': pr});
                        }
                        if (pr.sell.us.permission !== pr.sell.us.originalPermission) {
                            co.changesToCommit = true;
                            pr.changesToCommit = true;
                            listOfChanges.push({'action': "sell", 'change': pr.sell, 'co': co, 'pr': pr});
                        }
                    }, $scope.counterparties
                );

                if (listOfChanges.length > 0) {
                    $scope.errorMessage = "";
                    $scope.updateRequested = true;
                    $scope.listChanges = listOfChanges;
                }
            };

            // Cancel button pressed
            $scope.cancelChanges = function () {
                // find all where us != originalUs and revert

                var revert = function (co, pr) {

                    if (pr.buy.us.permission !== pr.buy.us.originalPermission) {
                        permissionsService.updatePermission(pr.buy, pr.buy.us.originalPermission);
                    }
                    if (pr.sell.us.permission !== pr.sell.us.originalPermission) {
                        permissionsService.updatePermission(pr.sell, pr.sell.us.originalPermission);
                    }
                };

                permissionsService.iterateOverMatrixProducts(revert, $scope.counterparties);
                $scope.userHasMadeAChange = false;
            };

            $scope.hasChangesFilter = function (c) {
                return c.changesToCommit === true;
            };

            $scope.updateConfirmReject = function () {
                $scope.updateRequested = false;
            };

            var matrixPermissionUpdated = function (buySellPermission, newUpdatedPermission) {

                buySellPermission.us.lastUpdated.at = newUpdatedPermission.lastUpdated;
                buySellPermission.us.lastUpdated.by = $scope.user.name;
                buySellPermission.us.originalPermission = buySellPermission.us.permission;
                permissionsService.updatePermission(buySellPermission, buySellPermission.us.permission);
                permissionsService.updateLastUpdatedInfo(newUpdatedPermission.lastUpdated, $scope.user.name, $scope.lastUpdatedInfo);
            };

            var onSaveCounterpartyPermissions = function (data) {
                var counter,
                    counterPartyCounter,
                    productCounter,
                    updatedMatrixPerm,
                    updatedPermissions,
                    updateSuccessful = false;

                if (data) {
                    updatedPermissions = data.counterpartyPermissions;
                    updateSuccessful = data.updateSuccessful;
                }

                if (updateSuccessful) {
                    for (counter = 0; counter < updatedPermissions.length; counter++) {
                        updatedMatrixPerm = updatedPermissions[counter];

                        // Ok - we need to update this permissions in the list of counterparties.
                        for (counterPartyCounter = 0; counterPartyCounter < $scope.counterparties.length; counterPartyCounter++) {
                            if ($scope.counterparties[counterPartyCounter].id === updatedMatrixPerm.theirOrganisation) {
                                // We have found a match - this is the Organisation we have just updated.

                                for (productCounter = 0; productCounter < $scope.counterparties[counterPartyCounter].products.length; productCounter++) {
                                    if ($scope.counterparties[counterPartyCounter].products[productCounter].id === updatedMatrixPerm.productId) {
                                        // We have found our product
                                        if (updatedMatrixPerm.buyOrSell.toLowerCase() === "buy") {
                                            // This is a buy permission
                                            matrixPermissionUpdated($scope.counterparties[counterPartyCounter].products[productCounter].buy, updatedMatrixPerm);
                                            $scope.counterparties[counterPartyCounter].products[productCounter].changesToCommit = false;
                                        } else {
                                            // This is a sell permission.
                                            matrixPermissionUpdated($scope.counterparties[counterPartyCounter].products[productCounter].sell, updatedMatrixPerm);
                                            $scope.counterparties[counterPartyCounter].products[productCounter].changesToCommit = false;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $scope.updateWasSuccessful = true;
                    $scope.userHasMadeAChange = false;
                    $scope.updateInProgress = false;
                    $scope.errorMessage = "";
                } else {
                    // The update was NOT successful - we need to display the results - showing the row(s) that updated
                    // and the rows that did NOT update.
                    $scope.listChanges = [];
                    $scope.updateInProgress = true;
                    $scope.updateWasSuccessful = false;
                    $scope.updateRequested = false;
                    $scope.failedUpdates = _.where(updatedPermissions, {updateSuccessful: false});
                    _.each($scope.failedUpdates, function (failedUpdate) {
                        failedUpdate.organisation = permissionsService.getOrganisationName(failedUpdate.theirOrganisation, $scope.organisations);
                        failedUpdate.product = permissionsService.getProductName(failedUpdate.productId, $scope.products);
                    });
                    $scope.showUpdateFailed = true;
                }
            };

            $scope.updateConfirmAccept = function () {
                // User has confirmed that he/she wants to update the changes made.
                $scope.updateInProgress = true;
                $scope.updateWasSuccessful = false;
                $scope.errorMessage = "";

                // *** NEW CODE SAVE MULTIPLE CHANGES ***
                var matrixPermissions = [];

                // Get all the changes for a single product.
                for (var i = 0; i < $scope.listChanges.length; i++) {

                    var pr = $scope.listChanges[i].pr;
                    var co = $scope.listChanges[i].co;

                    var matrixPermission;
                    if ($scope.listChanges[i].action === 'buy') {
                        if (pr.buy.us.permission !== pr.buy.us.originalPermission) {
                            matrixPermission = {
                                ourOrganisation: userService.getUser().organisationId,
                                theirOrganisation: co.id,
                                productId: pr.id,
                                buyOrSell: "Buy",
                                allowOrDeny: permissionsService.translateTriStateToPermission(pr.buy.us.permission),
                                lastUpdated: pr.buy.us.lastUpdated.at,
                                lastUpdatedUserId: -1
                            };
                            matrixPermissions.push(matrixPermission);
                        }
                    } else {
                        if (pr.sell.us.permission !== pr.sell.us.originalPermission) {
                            matrixPermission = {
                                ourOrganisation: userService.getUser().organisationId,
                                theirOrganisation: co.id,
                                productId: pr.id,
                                buyOrSell: "Sell",
                                allowOrDeny: permissionsService.translateTriStateToPermission(pr.sell.us.permission),
                                lastUpdated: pr.sell.us.lastUpdated.at,
                                lastUpdatedUserId: -1
                            };
                            matrixPermissions.push(matrixPermission);
                        }
                    }
                }

                counterpartyPermissionsService.saveCounterpartyPermissions(matrixPermissions, $scope.organisationId, onSaveCounterpartyPermissions);
            };

            $scope.closeSuccessConfirmation = function () {

                $scope.updateInProgress = false;
                $scope.updateWasSuccessful = false;
                $scope.updateRequested = false;
            };

            $scope.updateFailedReloadTab = function () {
                $scope.listChanges = [];
                $scope.userHasMadeAChange = false;
                $scope.updateInProgress = false;
                $scope.showUpdateFailed = false;
                $scope.errorMessage = "";
                $scope.updateRequested = false;
                $scope.showSelectedTab();
            };

            $scope.onTabChangeDataLosss = function () {
                // TODO: We need to display a data loss warning to the user...
                messagePopupsService.displayDataLossWarning("Change Tab - Data Loss", "Please save changes or cancel changes before selecting new tab");
            };
        }
    );