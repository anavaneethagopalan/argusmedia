﻿angular.module('argusOpenMarkets.shared').service("mockCounterpartyPermissionsService", function mockCounterpartyPermissionsService(){
    "use strict";

    var PermissionStatus = {
        allow: 'allow',
        forbid:'forbid',
        conflict:'conflict'
    };

    var updatePermission = function( productOperation, permission ){
        productOperation.us = permission;
        productOperation.overallStatus = determineStatus( productOperation.us, productOperation.them);
    };

    var determineStatus = function( us, them ) {
        switch (us) {
            case true:
                switch (them) {
                    case true:
                        return PermissionStatus.allow;
                    case false:
                        return PermissionStatus.conflict;
                    default:
                        return PermissionStatus.conflict;
                }
                break;
            case false:
                switch (them) {
                    case true:
                        return PermissionStatus.conflict;
                    case false:
                        return PermissionStatus.forbid;
                    default:
                        return PermissionStatus.forbid;
                }
                break;
            default:
                switch (them) {
                    case true:
                        return PermissionStatus.conflict;
                    case false:
                        return PermissionStatus.forbid;
                    default:
                        return PermissionStatus.forbid;
                }
        }
    };

    var getProducts = function( ){
        return [
            {
                id: 1,
                name: "Naphtha CIF NWE - Cargoes",
                filtered: false
            },
            {
                id: 2,
                name: "Naphtha FOB NWE - Barges",
                filtered: false
            },
            {
                id: 3,
                name: "Jet Fuel",
                filtered: false
            }
        ];
    };

    var getMockCounterparties = function( ){
        return [
            {
                id:0,
                name:"BASF",
                nameAndCode:"BASF (BASF)"
            },
            {
                id:2,
                name:"BP",
                nameAndCode:"BP (BP)"
            },
            {
                id:3,
                name:"DOW CHEMICAL",
                nameAndCode:"DOW CHEMICAL (DWC)"
            },
            {
                id:4,
                name:"INDIAN OIL CORP",
                nameAndCode:"INDIAN OIL CORP (IDO)"
            },
            {
                id:5,
                name:"MERCURIA",
                nameAndCode:"MERCURIA (MER)"
            }
        ];
    };


    var getCounterpartyPermissionsByMarketId = function(marketId, organisationId){


    };

    return {
        getProducts: getProducts,
        updatePermission:updatePermission,
        determineStatus:determineStatus,
        PermissionStatus:PermissionStatus,
        getCounterpartyPermissionsByMarketId: getCounterpartyPermissionsByMarketId
    };
});