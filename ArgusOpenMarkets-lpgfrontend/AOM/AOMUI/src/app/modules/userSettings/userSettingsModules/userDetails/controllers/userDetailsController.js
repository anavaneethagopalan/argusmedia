angular.module('argusOpenMarkets.userSettings')
    .controller('userDetailsController', function(
        $scope,
        userService,
        productConfigurationService,
        userApi)
    {
        var user = userService.getUser();
        $scope.userName = user.username;
        $scope.email = user.email;
        $scope.telephone =  user.telephone;
        $scope.loading = false;

        $scope.getKeyDescription = function(key, privileges){

            var counter = 0,
                privilegeDescription = key;

            if(privileges && privileges.length) {
                for (counter = 0; counter < privileges.length; counter++) {
                    if(privileges[counter].name){
                        if (privileges[counter].name.toLowerCase() === key.toLowerCase()) {
                            privilegeDescription = privileges[counter].description;
                        }
                    }
                }
            }

            return privilegeDescription;
        };

        var onProductPrivileges = function(data){
            var userProductPrivileges = user.productPrivileges,
                userSystemPrivileges = user.systemPrivileges,
                systemPrivs = [];

            $scope.userProductPrivileges = processPrivileges(data.productPrivileges, userProductPrivileges, false);

            systemPrivs.push(userSystemPrivileges);
            $scope.systemPrivileges = processPrivileges(data.systemPrivileges, systemPrivs, true);
        };

        var processPrivileges = function(privilegeDescriptions, userProductPrivileges, systemPrivs){
            var descriptivePrivileges = [],
                marketMustBeOpen,
                marketOpenText = "",
                marketMustBeOpenText = " [market must be open]",
                counter;

            if(userProductPrivileges && userProductPrivileges.length) {
                angular.forEach(userProductPrivileges, function (productPrivileges) {

                    if(systemPrivs){
                        descriptivePrivileges = [];
                        productPrivileges.productTitle = "System Privileges";
                        productPrivileges.productId = -100;

                        for(counter = 0; counter < productPrivileges.privileges.length; counter++) {
                            descriptivePrivileges.push({
                                "id": "sys" + counter + productPrivileges.productId,
                                "desc": $scope.getKeyDescription(productPrivileges.privileges[counter], privilegeDescriptions),
                                "marketMustBeOpen": ""
                            });
                        }
                    }else {
                        descriptivePrivileges = [];
                        productPrivileges.productTitle = productConfigurationService.getProductTitleById(productPrivileges.productId);

                        counter = 0;
                        for (var property in productPrivileges.privileges) {
                            if (productPrivileges.privileges.hasOwnProperty(property)) {
                                marketMustBeOpen = productPrivileges.privileges[property];
                                if(marketMustBeOpen){
                                    marketOpenText = marketMustBeOpenText = " [market must be open]";
                                }else{
                                    marketOpenText = "";
                                }
                                descriptivePrivileges.push(
                                    {
                                        "id": "pp" + counter + productPrivileges.productId,
                                        "desc": $scope.getKeyDescription(property, privilegeDescriptions),
                                        "marketMustBeOpen": marketOpenText});
                                counter++;
                            }
                        }
                    }
                    productPrivileges.descriptivePrivileges = descriptivePrivileges;
                });
            }

            return userProductPrivileges;
        };

        $scope.changePassword = function(){
            if($scope.changePasswordValid){
                var credentials = {
                    username: user.username,
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.password
                };
                $scope.loading = true;
                userApi.getChangeDetailsResource().post(credentials).$promise.then(function (data) {
                    $scope.loading = false;
                    $scope.changePasswordMessage = "Password changed";
                },
                function(){
                    $scope.loading = false;
                    $scope.changePasswordMessage = "Password change failed";
                });
            }
        };

        $scope.isCollapsedPrivileges = true;
        $scope.isCollapsedPassword = false;

        userApi.getProductPrivileges().then(onProductPrivileges);
    });
