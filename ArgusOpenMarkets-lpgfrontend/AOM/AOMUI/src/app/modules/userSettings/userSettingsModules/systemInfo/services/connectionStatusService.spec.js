describe('User Settings, System Info, Connection Status Service tests', function() {
    "use strict";

    var _connectionStatusService;

    var mockSocketProvider = {
        registerConnectCallback: function(callback, callOnReconnect) {
            this.connectCallback = callback;
        },
        registerLostConnectionCallback: function(callback) {
            this.lostConnectCallback = callback;
        },
        isConnected: function() { return true },
        getClient: function() {
            return {
                isReconnected: function() { return true; }
            }
        },
        connectCallback: null,
        lostConnectCallback: null,
        sendFakeDisconnect: function() {
            if (this.lostConnectCallback) {
                this.lostConnectCallback();
            }
        },
        sendFakeConnect: function(isConnected, isReconnect) {
            if (this.connectCallback) {
                this.connectCallback(isConnected, isReconnect);
            }
        }
    };

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
    });

    beforeEach(function(){
        module(function($provide) {
            $provide.value('socketProvider', mockSocketProvider);
        });

    });

    beforeEach(function () {
        inject(function (connectionStatusService) {
            _connectionStatusService = connectionStatusService;
        });
    });

    it('service should be defined', function() {
        expect(_connectionStatusService).toBeDefined();
    });

    it('should get connection status from socket provider', function() {
        expect(_connectionStatusService.isConnected()).toBeTruthy();
    });

    it('should get reconnection status from socket provider', function() {
        expect(_connectionStatusService.isReconnected()).toBeTruthy();
    });

    it('should register a connect callback to socket provider', function() {
        expect(mockSocketProvider.connectCallback).not.toBeNull();
    });

    it('should register a lost connect callback to socket provider', function() {
        expect(mockSocketProvider.lostConnectCallback).not.toBeNull();
    });

    it('should call provided callback when connection lost', function() {
        var called = false;
        _connectionStatusService.setDisconnectCallback(function() {
            called = true;
        });
        mockSocketProvider.sendFakeDisconnect();
        expect(called).toBeTruthy();
    });

    it('should call provided callback when connection regained', function() {
        var connected = false, isReconnect = false;
        _connectionStatusService.setConnectCallback(function(c, r) {
            connected = c;
            isReconnect = r;
        });
        mockSocketProvider.sendFakeConnect(true, true);
        expect(connected).toBeTruthy();
        expect(isReconnect).toBeTruthy();
    });

    it('should clear connection callback when asked to', function() {
        var called = false;
        _connectionStatusService.setConnectCallback(function(connected, isReconnect) {
            called = true;
        });
        _connectionStatusService.clearConnectCallback();
        mockSocketProvider.sendFakeConnect(true, true);
        expect(called).toBeFalsy();
    });

    it('should clear lost connection callback when asked to', function() {
        var called = false;
        _connectionStatusService.setDisconnectCallback(function() {
            called = true;
        });
        _connectionStatusService.clearDisconnectCallback();
        mockSocketProvider.sendFakeDisconnect();
        expect(called).toBeFalsy();
    });
});
