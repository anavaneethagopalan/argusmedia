describe('counterpartyPermissionsService tests', function () {
    "use strict";

    var counterpartyPermissionsServiceTest;
    var mockUserService;

    var create_mock_promise = function (call_through, do_reject, mock_data) {
        return {
            then: function (resolve, reject) {
                if (!call_through) {
                    return this;
                } else {
                    if (do_reject) {
                        return _(reject).isFunction() ? reject(mock_data) : undefined;
                    } else {
                        return resolve(mock_data);
                    }

                }
            }
        };
    };

    var mockTradingOrganisations = [
        {
            id: 1,
            name: "BASF",
            nameAndCode: "BASF (BASF)"
        },
        {
            id: 2,
            name: "BP",
            nameAndCode: "BP (BP)"
        },
        {
            id: 3,
            name: "DOW CHEMICAL",
            nameAndCode: "DOW CHEMICAL (DWC)"
        }
    ];

    var mockUserApi = {
        getPrincipalsForBroker: function () {
        },
        getPrincipals: function (productId) {
            return create_mock_promise(true, false, mockTradingOrganisations);
        }
    };

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(function () {
        module(function ($provide) {
            var $injector = angular.injector(['argusOpenMarkets.shared']);

            mockUserService = $injector.get('mockUserService');
            var mockProductConfigurationService = $injector.get('mockProductConfigurationService');

            mockUserService.setUser(mockUserService.MOCK_USER_T1);

            $provide.value('productConfigurationService', mockProductConfigurationService);
            $provide.value('userService', mockUserService);
            $provide.value('userApi', mockUserApi);
        });

        inject(function (counterpartyPermissionsService) {
            counterpartyPermissionsServiceTest = counterpartyPermissionsService;
        });
    });


    // it("UpdatePermission correctly updates the overall status", function () {
    //
    //     var testPermission = {overallStatus: null, us: {permission: null}, them: {permission: null}};
    //     var permissionStatus = counterpartyPermissionsServiceTest.PermissionStatus;
    //
    //     testPermission.them.permission = null;
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, true);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.conflict);
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, false);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.forbid);
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, null);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.forbid);
    //
    //     testPermission.them.permission = false;
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, true);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.conflict);
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, false);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.forbid);
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, null);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.forbid);
    //
    //     testPermission.them.permission = true;
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, true);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.allow);
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, false);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.conflict);
    //
    //     counterpartyPermissionsServiceTest.updatePermission(testPermission, null);
    //     expect(testPermission.overallStatus).toEqual(permissionStatus.conflict);
    // });

    it("getProducts returns CPM product construct", function () {

        var rv = counterpartyPermissionsServiceTest.getProducts();
        expect(rv.length).toEqual(3);

    });

    // it('should return correct details for counterparties for all products', function() {
    //     // is in Org ID 5 so all trading orgs should be returned
    //     mockUserService.setUser(mockUserService.MOCK_USER_B1);
    //     // need to call getProducts first to set up products internally
    //     var rv = counterpartyPermissionsServiceTest.getProducts();
    //     expect(rv.length).toEqual(3);
    //     var traders = counterpartyPermissionsServiceTest.getCounterpartiesForAllProducts();
    //     var orgDetails = _.map(traders, function(t) {
    //         return {id:t.id, name: t.name, nameAndCode: t.nameAndCode};
    //     });
    //     expect(orgDetails).toEqual(mockTradingOrganisations);
    //
    // });

    // it('should not return own company from list of counterparties for all products', function() {
    //     mockUserService.setUser(mockUserService.MOCK_USER_T1); // is in Org ID 3
    //     // need to call getProducts first to set up products internally
    //     var rv = counterpartyPermissionsServiceTest.getProducts();
    //     expect(rv.length).toEqual(3);
    //     var traders = counterpartyPermissionsServiceTest.getCounterpartiesForAllProducts("Broker");
    //     expect(traders.length).toEqual(2);
    //     expect(_.filter(traders, function(t) { return t.id == mockTradingOrganisations[0].id}).length).toEqual(1);
    //     expect(_.filter(traders, function(t) { return t.id == mockTradingOrganisations[1].id}).length).toEqual(1);
    // });

    // it("findOrCreateCounterparty builds CPMCounterparties correctly", function(){
    //
    //     var cpmProducts = counterpartyPermissionsServiceTest.getProducts();
    //     var cpmCounterparties = [];
    //
    //     var rv = counterpartyPermissionsServiceTest.findOrCreateCounterparty( 1, cpmCounterparties, cpmProducts );  //new
    //     expect(cpmCounterparties.length).toEqual(1);
    //     expect(rv.products.length).toEqual(cpmProducts.length);
    //
    //     var rv = counterpartyPermissionsServiceTest.findOrCreateCounterparty( 1, cpmCounterparties, cpmProducts );  //existing
    //     expect(cpmCounterparties.length).toEqual(1);
    //
    //     var rv = counterpartyPermissionsServiceTest.findOrCreateCounterparty( 99, cpmCounterparties, cpmProducts );  //new
    //     expect(cpmCounterparties.length).toEqual(2);
    //
    //     var rv = counterpartyPermissionsServiceTest.findOrCreateCounterparty( 1, cpmCounterparties, cpmProducts );  //existing
    //     expect(cpmCounterparties.length).toEqual(2);
    //
    //     var rv = counterpartyPermissionsServiceTest.findOrCreateCounterparty( 99, cpmCounterparties, cpmProducts );  //existing
    //     expect(cpmCounterparties.length).toEqual(2);
    //
    //     var rv = counterpartyPermissionsServiceTest.findOrCreateCounterparty( 22, cpmCounterparties, cpmProducts );  //new
    //     expect(cpmCounterparties.length).toEqual(3);
    //
    // });
});