describe('brokerPermissionsService tests', function() {
    "use strict";

    var brokerPermissionsServiceTest;
    var mockUserService;

    var create_mock_promise = function (call_through, do_reject, mock_data) {
        return {
            then: function (resolve, reject) {
                if (!call_through) {
                    return this;
                } else {
                    if (do_reject) {
                        return _(reject).isFunction() ? reject(mock_data) : undefined;
                    } else {
                        return resolve(mock_data);
                    }

                }
            }
        };
    };

    var mockTradingOrganisations = [
        {
            id:1,
            name:"BASF",
            nameAndCode:"BASF (BASF)"
        },
        {
            id:2,
            name:"BP",
            nameAndCode:"BP (BP)"
        },
        {
            id:3,
            name:"DOW CHEMICAL",
            nameAndCode:"DOW CHEMICAL (DWC)"
        }
    ];

    var mockBrokerOrganisations = [
        {
            id:4,
            name:"GFI",
            nameAndCode: "GFI (GFIG)"
        },
        {
            id:5,
            name:"Spectron",
            nameAndCode: "Spectron (SPEC)"
        }
    ];

    var mockUserApi = {
        getPrincipalsForBroker: function(){},
        getBrokers: function(productId) {
            return create_mock_promise(true, false, mockBrokerOrganisations);
        },
        getPrincipals: function(productId) {
            return create_mock_promise(true, false, mockTradingOrganisations);
        }
    };

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(function(){
        module(function($provide) {
            var $injector = angular.injector( [ 'argusOpenMarkets.shared' ]);

            mockUserService = $injector.get( 'mockUserService' );
            var mockProductConfigurationService = $injector.get( 'mockProductConfigurationService' );

            mockUserService.setUser(mockUserService.MOCK_USER_T1);

            $provide.value('productConfigurationService', mockProductConfigurationService );
            $provide.value('userService', mockUserService );
            $provide.value('userApi', mockUserApi );
        });

        inject(function(brokerPermissionsService){
            brokerPermissionsServiceTest = brokerPermissionsService;
        });
    });


    // it("UpdatePermission correctly updates the overall status", function(){
    //
    //     var testPermission = {overallStatus:null,us:{permission:null}, them:{permission:null}};
    //     var permissionStatus = brokerPermissionsServiceTest.PermissionStatus;
    //
    //     testPermission.them.permission = null;
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, true );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.conflict);
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, false );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.forbid);
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, null );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.forbid);
    //
    //     testPermission.them.permission = false;
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, true );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.conflict);
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, false );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.forbid);
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, null );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.forbid);
    //
    //     testPermission.them.permission = true;
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, true );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.allow);
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, false );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.conflict);
    //
    //     brokerPermissionsServiceTest.updatePermission( testPermission, null );
    //     expect(testPermission.overallStatus).toEqual( permissionStatus.conflict);
    // });

    it("getProducts returns CPM product construct", function(){
        var rv = brokerPermissionsServiceTest.getProducts();
        expect(rv.length).toEqual(3);
    });

    it("getProducts returns CPM product construct and includes internal products", function(){
        var products = brokerPermissionsServiceTest.getProducts();
        expect(products.length).toEqual(3);
    });
    //
    // it('should return correct details for counterparties for all products', function() {
    //     // is in Org ID 5 so all trading orgs should be returned
    //     mockUserService.setUser(mockUserService.MOCK_USER_B1);
    //     // need to call getProducts first to set up products internally
    //     var rv = brokerPermissionsServiceTest.getProducts();
    //     expect(rv.length).toEqual(3);
    //     var traders = brokerPermissionsServiceTest.getCounterpartiesForAllProducts("Broker");
    //     var orgDetails = _.map(traders, function(t) {
    //         return {id:t.id, name: t.name, nameAndCode: t.nameAndCode};
    //     });
    //     expect(orgDetails).toEqual(mockTradingOrganisations);
    //
    // });

    // it('should not return own company from list of counterparties for all products', function() {
    //     mockUserService.setUser(mockUserService.MOCK_USER_T1); // is in Org ID 3
    //     // need to call getProducts first to set up products internally
    //     var rv = brokerPermissionsServiceTest.getProducts();
    //     expect(rv.length).toEqual(3);
    //     var traders = brokerPermissionsServiceTest.getCounterpartiesForAllProducts("Broker");
    //     expect(traders.length).toEqual(2);
    //     expect(_.filter(traders, function(t) { return t.id == mockTradingOrganisations[0].id}).length).toEqual(1);
    //     expect(_.filter(traders, function(t) { return t.id == mockTradingOrganisations[1].id}).length).toEqual(1);
    // });

    // it('should return correct details for brokers for all products', function() {
    //     // is in Org ID 3 so all broker orgs should be returned
    //     mockUserService.setUser(mockUserService.MOCK_USER_T1);
    //     // need to call getProducts first to set up products internally
    //     var rv = brokerPermissionsServiceTest.getProducts();
    //     expect(rv.length).toEqual(3);
    //     var brokers = brokerPermissionsServiceTest.getCounterpartiesForAllProducts("Trading");
    //     var orgDetails = _.map(brokers, function(t) {
    //         return {id:t.id, name: t.name, nameAndCode: t.nameAndCode};
    //     });
    //     expect(orgDetails).toEqual(mockBrokerOrganisations);
    // });

    // it('should not return own company from list of brokers for all products', function() {
    //     mockUserService.setUser(mockUserService.MOCK_USER_B1); // is in Org ID 5
    //     // need to call getProducts first to set up products internally
    //     var rv = brokerPermissionsServiceTest.getProducts();
    //     expect(rv.length).toEqual(3);
    //     var brokers = brokerPermissionsServiceTest.getCounterpartiesForAllProducts("Trading");
    //     expect(brokers.length).toEqual(1);
    //     expect(brokers[0].id).toEqual(mockBrokerOrganisations[0].id);
    // });
});
