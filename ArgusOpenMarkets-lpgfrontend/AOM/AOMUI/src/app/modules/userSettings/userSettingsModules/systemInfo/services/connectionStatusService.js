angular.module('argusOpenMarkets.userSettings')
    .service("connectionStatusService", function connectionStatusService(socketProvider) {
        var isConnected = socketProvider.isConnected();
        var isReconnected = socketProvider.getClient().isReconnected();

        var isConnectedFn = function() {
            return isConnected;
        };

        var isReconnectedFn = function() {
            return isReconnected;
        };

        var disconnectCallback = null;
        var connectCallback = null;

        var setConnectCallback = function(callback) {
            connectCallback = callback;
        };

        var setDisconnectCallback = function(callback) {
            disconnectCallback = callback;
        };

        var clearConnectCallback = function() {
            connectCallback = null;
        };

        var clearDisconnectCallback = function() {
            disconnectCallback = null;
        };

        socketProvider.registerConnectCallback(function(connected, isReconnect) {
            isConnected = connected;
            isReconnected = isReconnect;
            if (connectCallback) {
                connectCallback(connected, isReconnect);
            }
        }, true);

        socketProvider.registerLostConnectionCallback(function() {
           isConnected = false;
            if (disconnectCallback) {
                disconnectCallback();
            }
        });

        return {
            isConnected: isConnectedFn,
            isReconnected: isReconnectedFn,
            setConnectCallback: setConnectCallback,
            setDisconnectCallback: setDisconnectCallback,
            clearConnectCallback: clearConnectCallback,
            clearDisconnectCallback: clearDisconnectCallback
        };
    });
