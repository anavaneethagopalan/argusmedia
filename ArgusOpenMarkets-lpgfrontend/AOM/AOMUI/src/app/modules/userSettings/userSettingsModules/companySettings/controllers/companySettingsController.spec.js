describe('Company settings controller tests', function() {
    "use strict";
    var scope,
        companySettingsController,
        userService,
        mockModalInstance;

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
    });

    beforeEach(function () {
        inject(function ($controller, $rootScope, mockUserService, mockOrganisationDetailsService) {
            scope = $rootScope.$new();
            userService = mockUserService;
            mockModalInstance = {
                dismiss: function(exitReason) {}
            };
            mockUserService.setUser(mockUserService.MOCK_USER_T1);

            companySettingsController =
                $controller('companySettingsController',
                    {
                        $scope: scope,
                        $modal: mockModalInstance,
                        userService: mockUserService,
                        organisationDetailsService: mockOrganisationDetailsService
                    });
        });
    });

    it("should be defined", function(){
       expect( companySettingsController ).toBeDefined();
    });

    it("should have the expected data", function(){
        expect(scope.selection.length).toEqual(0);

        expect(scope.userOrganisationName).toEqual(userService.MOCK_USER_T1.userOrganisation.name);
        expect(scope.userOrganisationLegalName).toEqual(userService.MOCK_USER_T1.userOrganisation.legalName);
        expect(scope.userOrganisationShortCode).toEqual(userService.MOCK_USER_T1.userOrganisation.shortCode);
        expect(scope.userOrganisationAddress).toEqual(userService.MOCK_USER_T1.userOrganisation.address);
    });

    it("should be able to toggle a selection on and off", function(){
        expect(scope.selection.length).toEqual(0);

        scope.toggleSelection(1);
        expect(scope.selection.length).toEqual(1);

        scope.toggleSelection(2);
        expect(scope.selection.length).toEqual(2);

        scope.toggleSelection(2);
        expect(scope.selection.length).toEqual(1);

        scope.toggleSelection(1);
        expect(scope.selection.length).toEqual(0);

    });
});
