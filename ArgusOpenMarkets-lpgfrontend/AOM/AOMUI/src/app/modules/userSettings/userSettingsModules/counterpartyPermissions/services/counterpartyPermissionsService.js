﻿angular.module('argusOpenMarkets.shared').service("counterpartyPermissionsService",
    function counterpartyPermissionsService(userService,
                                            productConfigurationService,
                                            userApi) {

        "use strict";

        var cpmProducts = [];

        var getProducts = function () {
            cpmProducts = [];

            var products = productConfigurationService.getOrganisationSubscribedProducts();

            for (var product in products) {
                if (products.hasOwnProperty(product)) {
                    var p = products[product];
                    if (userService.hasProductPrivilege(p.productId, ["View_BidAsk_Widget"])) {
                        cpmProducts.push({
                            id: p.productId,
                            name: p.name,
                            filtered: false
                        });
                    }
                }
            }
            return cpmProducts;
        };

        var getCounterpartyPermissionsByMarketId = function (marketId, organisationId) {
            return userApi.getCounterpartyPermissionsByMarketId(marketId, organisationId).$promise;
        };

        var saveCounterpartyPermissions = function (matrixPermissions, organisationId, callback) {
            if (matrixPermissions) {
                userApi.saveCounterpartyPermissions(matrixPermissions, organisationId, callback);
            }
        };

        return {
            getProducts: getProducts,
            getCounterpartyPermissionsByMarketId: getCounterpartyPermissionsByMarketId,
            saveCounterpartyPermissions: saveCounterpartyPermissions
        };
    });