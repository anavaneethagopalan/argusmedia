/*global platform: false */
angular.module('argusOpenMarkets.userSettings')
    .service("platformInfoService", function platformInfoService() {

        var osVersion = platform.os.toString();

        // This is based on hasFlash() from diffusion.uncompressed.js
        var getFlashVersion = function() {
            if (window.ActiveXObject) {
                try {
                    var obj = new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash");
                    var ver = obj.GetVariable("$version");
                    return ver;
                } catch (ignore) {
                    return null;
                }
            }
            if (navigator.plugins && navigator.mimeTypes.length) {
                try {
                    var x = navigator.plugins["Shockwave Flash"];
                    return x ? x.description : null;
                } catch (ig) {
                    return null;
                }
            }
        };

        var flashVersion = getFlashVersion();

        var browserInfo = {
            name: platform.name,
            version: platform.version,
            userAgentString: platform.ua,
            flashVersion: flashVersion ? flashVersion : "None"
        };

        var isBrowserSupported = function(){
            return platform.name === "Chrome" || platform.name === "Firefox" || (platform.name === "IE" && parseInt(platform.version)>=9);
        };

        var isBrowserBlocked = function(){
            return platform.name === "IE" ? parseInt(platform.version)<9 : false;
        };

        var getOSDescription = function() {
            return osVersion;
        };

        var getBrowserInfo = function () {
            return browserInfo;
        };

        var getBrowserName = function(){
            return platform.name +" version: "+ platform.version;
        };

        return {
            getOSDescription: getOSDescription,
            getBrowserInfo: getBrowserInfo,
            isBrowserSupported: isBrowserSupported,
            isBrowserBlocked:isBrowserBlocked,
            getBrowserName:getBrowserName
        };
    });
