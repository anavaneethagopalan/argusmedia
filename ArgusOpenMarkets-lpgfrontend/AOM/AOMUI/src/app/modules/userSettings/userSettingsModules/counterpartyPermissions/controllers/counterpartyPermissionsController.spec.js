describe('Counterparty Permissions tests', function () {
    describe('counterpartyPermissions tests', function () {

        var testController,
            mockUser,
            testMSS,
            mockMessagePopupsService = {
                displaySuccess: function (a, b) {
                    return null;
                },
                displayInfo: function (a, b) {
                    return null;
                },
                displayWarning: function (a, b) {
                    return null;
                },
                displayError: function (a, b) {
                    return null;
                }
            },
            mockMarketService = {
                getMarkets: function (cb) {
                    var data = [{
                        "marketId": 1,
                        "marketName": "NWE",
                        "productId": 1,
                        "productName": "Naphtha CIF NWE - Cargoes"
                    }, {
                        "marketId": 3,
                        "marketName": "NWE Other",
                        "productId": 2,
                        "productName": "Naphtha CIF NWE - Barges"
                    }, {"marketId": 2, "marketName": "Other", "productId": 3, "productName": "Other"}, {
                        "marketId": 1,
                        "marketName": "NWE",
                        "productId": 621,
                        "productName": "TEST DAF BREST"
                    }];

                    cb(data);
                },
                filterMarketsForProducts: function(markets, products){
                    return products;
                }
            },
            mockPermissionsService = {};

        beforeEach(module('ui.router', function ($locationProvider) {
            locationProvider = $locationProvider;
            $locationProvider.html5Mode(false);
        }));

        beforeEach(function () {
            module('argusOpenMarkets.shared');
            module('argusOpenMarkets.userSettings');
        });

        beforeEach(
            inject(function ($controller,
                             $rootScope,
                             mockUserService,
                             mockCounterpartyPermissionsService, mockMessageSubscriptionService2) {

                    mockUser = mockUserService.MOCK_USER_T1;

                    mockUserService.setUser(mockUserService.MOCK_USER_T1);
                    testMSS = mockMessageSubscriptionService2;

                    $scope = $rootScope.$new();

                    testController = $controller('counterpartyPermissionsController',
                        {
                            $scope: $scope,
                            counterpartyPermissionsService: mockCounterpartyPermissionsService,
                            userService: mockUserService,
                            messageSubscriptionService2: mockMessageSubscriptionService2,
                            permissionsService: mockPermissionsService,
                            marketService: mockMarketService,
                            messagePopupsService: mockMessagePopupsService
                        });
                }
            ));

        it('should be defined', inject(function () {
            expect(testController).toBeDefined();
        }));

        // it('should contain a populated matrix', function () {
        //     TODO - Need to setup test that display counterparty permissions under test.
        //     expect($scope.counterparties.length).toEqual(5);
        //     expect($scope.counterparties[0].products.length).toEqual($scope.products.length);
        // });
    });
});