angular.module('argusOpenMarkets.userSettings')
    .controller('productSettingsController', function($scope, userApi, productConfigurationService)
    {
        $scope.isHeaderCollapsed = false;

        var onProductSettings = function(data){
            var subscribedProducts = [],
                productsUserSubscribedTo = productConfigurationService.getAllSubscribedProductIds();

            if(data){

                angular.forEach(data, function(product){
                    product.collapsed = false;
                    if(userSubscribedToProduct(product, productsUserSubscribedTo)){
                        subscribedProducts.push(product);
                    }
                });

                $scope.products = subscribedProducts;
            }
        };

        var userSubscribedToProduct = function(product, subscribedProducts) {

            for(var i = 0; i < subscribedProducts.length; i++){
                if(subscribedProducts[i] === product.product.productId){
                    return true;
                }
            }
            return false;
        };

        $scope.expandCollapseProduct = function(product){

            angular.forEach($scope.products, function(p){
                if(p.product.productId === product.product.productId){
                    p.collapsed = !p.collapsed;
                }
            });
        };

        $scope.isCollapsedProduct = function(product){
            var counter;

            for(counter = 0; counter < $scope.products.length; counter++){
                if($scope.products[counter].product.productId === product.product.productId){
                    return $scope.products[counter].collapsed;
                }
            }
        };

        userApi.getProductSettings().then(onProductSettings);

        $scope.controller = "Product Settings";
    });
