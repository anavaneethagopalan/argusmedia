angular.module('argusOpenMarkets.userSettings').config(function ($provide) {
    $provide.decorator('uiSrefDirective', function ($delegate, $log, $injector) {
        var directive = $delegate[0];
        directive.compile = function () {
            return function (scope, $element, $attrs) {
                var stateName = $attrs.uiSref.replace(/\(.+\)$/g, '');
                var state = $injector && $injector.get('$state').get(stateName);
                var userService = $injector.get('userService');
                if (!state) {
                    $log.warn('Could not find state:', $attrs.uiSref);
                } else if (state.permissionsToView && state.permissionsToView.length !== 0) {
                    if (!userService.hasSystemPrivileges(state.permissionsToView)) {
                        $element.remove();
                    }
                }

                // NTB - Added a new check - ensure only Trading organisations can see Counterparty Permissions.
                if (state.restrictOrgType && state.restrictOrgType.length !== 0) {
                    var user = userService.getUser();
                    if (user.userOrganisation.organisationType !== state.restrictOrgType) {
                        $element.remove();
                    }
                }
                // Otherwise pass through and let uiSref handle the rest
                directive.link.apply(this, arguments);
            };
        }
        ;

        return $delegate;
    })
    ;
})
;