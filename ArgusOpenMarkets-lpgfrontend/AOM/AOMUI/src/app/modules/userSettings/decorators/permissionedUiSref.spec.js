describe('permissionedUiSrefTests', function () {

    var scope, $state, $injector, element;
    var mockUserService = {
        hasSystemPrivileges:function(){return false},
        getUser: function(){
            return { 'userOrganisation': {
                'organisationType': 'T'
            }};
        }
    };

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
        module(function($provide) {
            $provide.value('userService', mockUserService);
        });
    });

    beforeEach(function () {
        inject(function ($rootScope, _$state_, _$injector_, $compile) {
            scope = $rootScope.$new();
            $state = _$state_;
            $injector = _$injector_;
            element = angular.element('<div class="settings-link top" ui-sref="UserSettings.CounterpartyPermissions" ui-sref-active="active">My Settings</div>');
            spyOn(mockUserService,'hasSystemPrivileges').and.callThrough();
            $compile(element)(scope);
            scope.$digest();
        })
    });

    it("creates the ui-sref element", function(){
        expect(element).toBeTruthy();
    });

    it("checks if permissioned to see element", function(){
        expect(mockUserService.hasSystemPrivileges).toHaveBeenCalled();
    });
});