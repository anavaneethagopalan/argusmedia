angular.module('argusOpenMarkets.userSettings', [
    'argusOpenMarkets.shared',
    'gridster'
]).config(function config($stateProvider) {
    $stateProvider.state('UserSettings', {
        url: '/UserSettings',
        views: {
            "": {
                templateUrl: 'src/app/modules/userSettings/views/userSettings.part.html',
                controller: 'userSettingsController'
            },
            'header': {
                templateUrl: 'src/app/modules/dashboard/dashboardModules/header/views/header.part.html',
                controller: 'headerController'
            },
            "footer": {
                templateUrl: 'src/app/modules/dashboard/dashboardModules/footer/views/footer.part.html',
                controller: 'footerController'
            },
            resolve: {}
        }
    }).state('UserSettings.UserDetails', {
        url: '/MySettings',
        parent: 'UserSettings',
        templateUrl: 'src/app/modules/userSettings/userSettingsModules/userDetails/views/userDetails.part.html',
        controller: 'userDetailsController',
        permissionsToView: []
    }).state('UserSettings.CompanySettings', {
        url: 'CompanySettings',
        parent: 'UserSettings',
        templateUrl: 'src/app/modules/userSettings/userSettingsModules/companySettings/views/companySettings.part.html',
        controller: 'companySettingsController',
        permissionsToView: ['CompanySettings_View']
    }).state('UserSettings.CounterpartyPermissions', {
        url: '/CounterpartyPermissions',
        parent: 'UserSettings',
        templateUrl: 'src/app/modules/userSettings/userSettingsModules/counterpartyPermissions/views/counterpartyPermissions.part.html',
        controller: 'counterpartyPermissionsController',
        permissionsToView: ['CPMatrix_View'],
        restrictOrgType: 'Trading',
    }).state('UserSettings.BrokerPermissions', {
        url: '/BrokerPermissions',
        parent: 'UserSettings',
        templateUrl: 'src/app/modules/userSettings/userSettingsModules/brokerPermissions/views/brokerPermissions.part.html',
        controller: 'brokerPermissionsController',
        permissionsToView: ['BPMatrix_View']
    }).state('UserSettings.SystemInfo', {
        url: '/SystemInfo',
        parent: 'UserSettings',
	    controller: 'systemInfoController',
        templateUrl: 'src/app/modules/userSettings/userSettingsModules/systemInfo/views/systemInfo.part.html',
        permissionsToView: []
    }).state('UserSettings.ProductSettings', {
        url: '/ProductSettings',
        parent: 'UserSettings',
        controller: 'productSettingsController',
        templateUrl: 'src/app/modules/userSettings/userSettingsModules/productSettings/views/productSettings.part.html',
        permissionsToView: []
    }).state('UserSettings.DashboardLayout', {
        url: '/DashboardLayout',
        parent: 'UserSettings',
        controller: 'dashboardLayoutController',
        templateUrl: 'src/app/modules/userSettings/userSettingsModules/dashboardLayout/views/dashboardLayout.part.html',
        permissionsToView: []
    });
});