angular.module('argusOpenMarkets.userSettings')
    .controller('userSettingsController',
    function userSettingsController(
        $scope,
        $state,
        $window,
        userSettingsStateService)
    {
        $state.transitionTo(userSettingsStateService.getUserSettingsState());

        $window.onbeforeunload = function(e){
            return "Are you sure you want to leave your AOM Session?";
        };
    });