describe('User settings controller tests', function() {
    "use strict";
    var scope,
        userSettingsController;
    var mockstate = {transitionTo:function(){}};

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.userSettings');
    });

    beforeEach(function () {
        inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            spyOn(mockstate,'transitionTo');
            userSettingsController = $controller('userSettingsController',{$scope:scope,$state:mockstate});
        });
    });

    it("should transition to the user settings view", function(){
        expect(mockstate.transitionTo).toHaveBeenCalledWith("UserSettings.UserDetails");
    });
});
