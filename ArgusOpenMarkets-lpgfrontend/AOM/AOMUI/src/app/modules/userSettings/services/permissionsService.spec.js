describe('permissions service tests', function () {

    var scope,
        testService,
        stateProvider;

    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.dashboard', function ($stateProvider, $provide) {
        stateProvider = $stateProvider;
        //$provide.value('$window',mockWindow);
    }));

    beforeEach(module('argusOpenMarkets.userSettings'));

    beforeEach(inject(function (permissionsService) {
        testService = permissionsService;
    }));

    it('should translate an allow permission to tristate', function () {
        expect(testService.translatePermissionToTriState('a')).toBeTruthy();
    });

    it('should translate an deny permission to tristate', function () {
        expect(testService.translatePermissionToTriState('b')).toBeFalsy();
    });

    it('should translate an unknown permission to tristate', function () {
        expect(testService.translatePermissionToTriState('u')).toBeNull();
    });

    it('should translate an unknown tristate to permission', function () {
        expect(testService.translateTriStateToPermission(null)).toEqual('NotSet');
    });

    it('should translate an deny tristate to permission', function () {
        expect(testService.translateTriStateToPermission(false)).toEqual('Deny');
    });

    it('should translate an allow tristate to permission', function () {
        expect(testService.translateTriStateToPermission(true)).toEqual('Allow');
    });

    it('should return a product name if exists', function () {
        var products = [{'id': 1, 'name': 'Another Great Product'}, {'id': 2, 'name': 'Not Found'}];
        expect(testService.getProductName(1, products)).toEqual('Another Great Product');
    });

    it('should return an empty string if product does not exist', function () {
        var products = [{'id': 1, 'name': 'Another Great Product'}, {'id': 2, 'name': 'Not Found'}];
        expect(testService.getProductName(3, products)).toEqual('');
    });

    it('should return an organisation name if exists', function () {
        var organisations = [{'id': 1, 'name': 'Org 1'}, {'id': 2, 'name': 'Org 2'}];
        expect(testService.getOrganisationName(1, organisations)).toEqual('Org 1');
    });

    it('should return unknown if organisation does not exist', function () {
        var organisations = [{'id': 1, 'name': 'Another Great Product'}, {'id': 2, 'name': 'Not Found'}];
        expect(testService.getOrganisationName(3, organisations)).toEqual('Unknown');
    });

    describe('determine status', function () {
        it('should determine the status to be tradeable true if our decision is true and their decision is true', function () {
            var result = testService.determineStatus(true, true);
            expect(result).toEqual('allow');
        });

        it('should determine the status to be conflict if our decision is true and their decision is false', function () {
            var result = testService.determineStatus(true, false);
            expect(result).toEqual('conflict');
        });

        it('should determine the status to be conflict if our decision is true and their decision is null', function () {
            var result = testService.determineStatus(true, null);
            expect(result).toEqual('conflict');
        });

        it('should determine the status to be conflict if our decision is false and their decision is true', function () {
            var result = testService.determineStatus(false, true);
            expect(result).toEqual('conflict');
        });

        it('should determine the status to be forbid if our decision is false and their decision is false', function () {
            var result = testService.determineStatus(false, false);
            expect(result).toEqual('forbid');
        });

        it('should determine the status to be forbid if our decision is false and their decision is null', function () {
            var result = testService.determineStatus(false, false);
            expect(result).toEqual('forbid');
        });
    });

    describe('permissions status', function () {

        it('should return the Permission Status object', function () {
            expect(testService.PermissionStatus).toEqual({
                allow: 'allow',
                forbid: 'forbid',
                conflict: 'conflict'
            });
        });
    });

    describe('update Permission', function () {
        var existingPerm = {
            us: {
                permission: false
            },
            them: {
                permission: true
            }
        };

        it('should update the permission with our new decision and set the overall status to allow', function () {
            testService.updatePermission(existingPerm, true);
            expect(existingPerm.overallStatus).toEqual('allow');
            expect(existingPerm.us.permission).toEqual(true);
        });

        it('should update the permission with our new decision and set the overall status to conflict', function () {
            existingPerm.them.permission = false;
            testService.updatePermission(existingPerm, true);
            expect(existingPerm.overallStatus).toEqual('conflict');
            expect(existingPerm.us.permission).toEqual(true);
        });
    });

    describe('adding organisations should de-dupe organisation', function () {

        it('should return an empty list of organisations if two empty lists passed in', function () {
            var orgList = [];
            var orgsToAdd = [];

            var result = testService.addOrganisationsToList(orgList, orgsToAdd);
            expect(result).toEqual([]);
        });

        it('should return the original list if no organisations to add', function () {
            var orgList = [{'id': 1, name: 'Org1'}];
            var orgsToAdd = [];

            var result = testService.addOrganisationsToList(orgList, orgsToAdd);
            expect(result).toEqual([{'id': 1, name: 'Org1'}]);
        });

        it('should return the original list plus the items in the new list', function () {
            var orgList = [{'id': 1, name: 'Org1'}];
            var orgsToAdd = [{'id': 2, name: 'Org2'}];

            var result = testService.addOrganisationsToList(orgList, orgsToAdd);
            expect(result).toEqual([{'id': 1, name: 'Org1'}, {'id': 2, name: 'Org2'}]);
        });

        it('should return the original list ommitting items in the second list as they are duplicaates', function () {
            var orgList = [{'id': 1, name: 'Org1'}];
            var orgsToAdd = [{'id': 1, name: 'Org1'}];

            var result = testService.addOrganisationsToList(orgList, orgsToAdd);
            expect(result).toEqual([{'id': 1, name: 'Org1'}]);
        });
    });

    describe('calculate matrix permission', function () {

        var buyOd = true,
            sellTd = true,
            perm = {
                'buyOd': buyOd,
                'sellTd': sellTd,
                'buyUpdateUser': 'UTC1'
            },
            userDetails = [{'id': 'UTC1', username: 'Fred Flintstone'}];

        it('if a broker permission should set sell od to be buy od and buytd to be selltd', function () {
            var brokerPermissions = true,
                permission;

            permission = testService.calculateMatrixPerm(perm, userDetails, brokerPermissions);
            expect(perm.sellOd).toEqual(buyOd);
            expect(perm.buyTd).toEqual(sellTd);
        });

        it('should set the last updated user name to be unknown if user lookup does not exist', function () {

            userDetails[0].id = 'UTC2';
            var permission = testService.calculateMatrixPerm(perm, userDetails, true);
            expect(permission.lastUpdatedUsername).toEqual('Unknown');
        });

        it('should set the last updated user name to be the lookup value', function () {
            userDetails[0].id = 'UTC1';
            var permission = testService.calculateMatrixPerm(perm, userDetails, true);
            expect(permission.lastUpdatedUsername).toEqual('Fred Flintstone');
        });
    });

    describe('update last updated information', function () {

        it('should update the last updated username and date', function () {

            var lastUpdatedInfo = {
                    lastUpdated: new Date(2017, 1, 1),
                    lastUpdatedBy: 'Fred Flintstone'
                },
                lastUpdatedDate = new Date(2017, 1, 10);

            result = testService.updateLastUpdatedInfo(lastUpdatedDate, 'Nathan Bellamore', lastUpdatedInfo);
            expect(lastUpdatedInfo.lastUpdatedBy).toEqual('Nathan Bellamore');
        });

        it('should not update the last updated username and if the new date is earlier than last info', function () {

            var lastUpdatedInfo = {
                    lastUpdated: new Date(2017, 1, 10),
                    lastUpdatedBy: 'Fred Flintstone'
                },
                lastUpdatedDate = new Date(2017, 1, 1);

            result = testService.updateLastUpdatedInfo(lastUpdatedDate, 'Nathan Bellamore', lastUpdatedInfo);
            expect(lastUpdatedInfo.lastUpdatedBy).toEqual('Fred Flintstone');
        });
    });

    describe('build hover content', function () {

        var productAction = {
            'us': {
                'permission': true,
                'originalPermission': true,
                'lastUpdated': {
                    'by': 'Nathan Bellamore',
                    'at': '12:10'
                }
            },
            'them': {
                'permission': true,
                'originalPermission': true,
                'lastUpdated':{
                    'by': 'Fred Dinage',
                    'at': '10:10'
                }
            }
        };

        it('template should only display Trading Allowed if not our permission', function () {
            var template;

            productAction.us.orginalPermission = true;
            template = testService.buildHoverContent(productAction, "Us", false);
            expect(template.length).toEqual(1);
            expect(template[0].title).toEqual('Trading Allowed');
        });

        it('template should add Last Update By and when information into array if our permission', function () {
            var template;

            productAction.us.orginalPermission = true;
            template = testService.buildHoverContent(productAction, "Us", true);
            expect(template.length).toEqual(3);
            expect(template[0].title).toEqual('Trading Allowed');
            expect(template[1].title).toEqual('Last Update By');
            expect(template[2].title).toEqual('Last Update Date');
        });

        it('template should add Last Update By value and when information into array if our permission', function () {
            var template;

            productAction.us.orginalPermission = true;
            template = testService.buildHoverContent(productAction, "Us", true);
            expect(template.length).toEqual(3);
            expect(template[0].content).toEqual('Yes');
            expect(template[1].content).toEqual('Nathan Bellamore');
            expect(template[2].content).toEqual('12:10');
        });
    });

    describe('find or create counterparty', function(){

        organisations = [{
            "id": 1,
            "shortCode": "NTB",
            "name": "Nathan Trading Company",
            "legalName": "NTC LTD"
        }, {
            "id": 4,
            "shortCode": "TR2",
            "name": "TradeCo #2",
            "legalName": "TradeCo #2 Ltd"
        }, {
            "id": 61,
            "shortCode": "BRA",
            "name": "Bravo Trading",
            "legalName": "Bravo Trading"
        }, {
            "id": 62,
            "shortCode": "DLT",
            "name": "Delta Trading",
            "legalName": "Delta Trading"
        }, {
            "id": 63,
            "shortCode": "ECH",
            "name": "Echo Trading",
            "legalName": "Echo Trading"
        }];

        var counterparties = [{
            "id": 3,
            "name": "TradeCo #1",
            "nameAndCode": "TradeCo #1(TR1)",
            "legalName": "TradeCo #1 Ltd",
            "shortCode": "TR1",
            "hoverContent": [{
                "title": "Name",
                "content": "TradeCo #1"
            }, {
                "title": "Legal Name",
                "content": "TradeCo #1 Ltd"
            }, {
                "title": "Short Code",
                "content": "TR1"
            }],
            "filtered": false,
            "products": [{
                "id": 1,
                "cpSubscribed": true,
                "buy": {
                    "us": {
                        "permission": false,
                        "originalPermission": false,
                        "lastUpdated": {
                            "by": "Adam Smith",
                            "at": "2017-02-10T17:07:46.926Z"
                        }
                    },
                    "them": {
                        "permission": true,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "overallStatus": "conflict"
                },
                "sell": {
                    "us": {
                        "permission": true,
                        "originalPermission": true,
                        "lastUpdated": {
                            "by": "Adam Smith",
                            "at": "2017-02-10T15:40:12.94Z"
                        }
                    },
                    "them": {
                        "permission": true,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "overallStatus": "allow"
                }
            }, {
                "id": 2,
                "cpSubscribed": true,
                "buy": {
                    "us": {
                        "permission": null,
                        "originalPermission": null,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "them": {
                        "permission": null,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "overallStatus": "forbid"
                },
                "sell": {
                    "us": {
                        "permission": null,
                        "originalPermission": null,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "them": {
                        "permission": null,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "overallStatus": "forbid"
                }
            }]
        }, {
            "id": 4,
            "name": "TradeCo #2",
            "nameAndCode": "TradeCo #2(TR2)",
            "legalName": "TradeCo #2 Ltd",
            "shortCode": "TR2",
            "hoverContent": [{
                "title": "Name",
                "content": "TradeCo #2"
            }, {
                "title": "Legal Name",
                "content": "TradeCo #2 Ltd"
            }, {
                "title": "Short Code",
                "content": "TR2"
            }],
            "filtered": false,
            "products": [{
                "id": 1,
                "cpSubscribed": true,
                "buy": {
                    "us": {
                        "permission": true,
                        "originalPermission": true,
                        "lastUpdated": {
                            "by": "Adam Smith",
                            "at": "2017-02-13T11:12:30.537Z"
                        }
                    },
                    "them": {
                        "permission": true,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "overallStatus": "allow"
                },
                "sell": {
                    "us": {
                        "permission": true,
                        "originalPermission": true,
                        "lastUpdated": {
                            "by": "Adam Smith",
                            "at": "2017-02-10T13:26:44.583Z"
                        }
                    },
                    "them": {
                        "permission": true,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "overallStatus": "allow"
                }
            }, {
                "id": 2,
                "cpSubscribed": true,
                "buy": {
                    "us": {
                        "permission": null,
                        "originalPermission": null,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "them": {
                        "permission": null,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "overallStatus": "forbid"
                },
                "sell": {
                    "us": {
                        "permission": null,
                        "originalPermission": null,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "them": {
                        "permission": null,
                        "lastUpdated": {
                            "by": null,
                            "at": null
                        }
                    },
                    "overallStatus": "forbid"
                }
            }]
        }];

        it('should return the counterparty if it already exists and not add into the list of counterparties', function(){
            var products = [],
                organisations = [],
                counterparty;

            expect(counterparties.length).toEqual(2);
            var counterparty = testService.findOrCreateCounterparty(3, counterparties, products, organisations);
            expect(counterparties.length).toEqual(2);
        });

        it('should return add a new counterparty and add into the list of counterparties', function(){
            var products = [],
                counterparty;

            expect(counterparties.length).toEqual(2);
            var counterparty = testService.findOrCreateCounterparty(1, counterparties, products, organisations);
            expect(counterparties.length).toEqual(3);
        });

        it('should return add a new counterparty if it doesnt exist with organisation details filled out', function(){
            var products = [],
                counterparty,
                nameAndCode;

            var counterparty = testService.findOrCreateCounterparty(1, counterparties, products, organisations);


            nameAndCode = 'Nathan Trading Company' + '(NTB)'
            expect(counterparty.name).toEqual('Nathan Trading Company');
            expect(counterparty.legalName).toEqual('NTC LTD');
            expect(counterparty.shortCode).toEqual('NTB');
            expect(counterparty.nameAndCode).toEqual(nameAndCode);
        });
    })
});