angular.module('argusOpenMarkets.userSettings').service("permissionsService",
    function permissionsService($filter) {
        "use strict";

        var translateTriStateToPermission = function (perm) {
            switch (perm) {
                case true:
                    return "Allow";
                case false:
                    return "Deny";
                default: // null
                    return "NotSet";
            }
        };

        var translatePermissionToTriState = function (permission) {
            switch (permission) {
                case "a":
                    return true;
                case "d":
                    return false;
                default: // "NotSet"
                    return null;
            }
        };

        var getLastUpdatedUsername = function (listUsers, userLookupId) {

            var user = _.find(listUsers, {id: userLookupId});

            if (user) {
                return user.username;
            } else {
                return 'Unknown';
            }
        };

        var buildHoverContent = function (productAction, usThem, ourPermission) {

            var template = [];
            var party = usThem === "Us" ? productAction.us : productAction.them;
            template.push({
                title: 'Trading Allowed',
                content: party.permission === true ? "Yes" : party.permission === false ? "No" : "No (Has not been set)"
            });

            if (usThem === "Us" && party.permission !== party.originalPermission) {
                if (ourPermission) {
                    template.push({title: 'Last Update By', content: 'You in this session'}, {
                        title: 'Last Update Date',
                        content: 'Pending, not committed'
                    });
                    template.push({
                        title: 'Previous Update By',
                        content: party.lastUpdated.by
                    }, {
                        title: 'Previous Update Date',
                        content: $filter('date')(party.lastUpdated.at, 'dd-MMM-y HH:mm')
                    });
                }
            } else {
                if (ourPermission) {
                    template.push({title: 'Last Update By', content: party.lastUpdated.by}, {
                        title: 'Last Update Date',
                        content: $filter('date')(party.lastUpdated.at, 'dd-MMM-y HH:mm')
                    });
                }
            }
            return template;
        };

        var makePermissionsTabs = function (userMarkets) {
            var counter,
                tabSelected,
                tabs = [];

            for (counter = 0; counter < userMarkets.length; counter++) {
                if (counter === 0) {
                    tabSelected = true;
                } else {
                    tabSelected = false;
                }
                tabs.push({
                    'tabName': userMarkets[counter].marketName,
                    'selected': tabSelected,
                    'marketId': userMarkets[counter].marketId,
                    'productIds': userMarkets[counter].products
                });
            }

            return tabs;
        };

        var productShouldBeVisible = function (productId, productIds) {
            if (productIds) {
                for (var i = 0; i < productIds.length; i++) {
                    if (productIds[i].productId === productId) {
                        // Product should be visible.
                        return true;
                    }
                }
            }
            return false;
        };

        var updateLastUpdatedInfo = function (lastUpdatedDate, lastUpdatedUsername, lastUpdatedInfo) {

            if (lastUpdatedDate) {
                if (lastUpdatedDate > lastUpdatedInfo.lastUpdated) {
                    lastUpdatedInfo.lastUpdated = lastUpdatedDate;

                    if (lastUpdatedUsername) {
                        lastUpdatedInfo.lastUpdatedBy = lastUpdatedUsername;
                    }
                }
            }
        };

        var calculateMatrixPerm = function (perm, userDetails, brokerPermissions) {
            var permission;

            if (brokerPermissions) {
                if (perm) {
                    // NOTE: Broker perms store one value for buy and sell.
                    perm.sellOd = perm.buyOd;
                    perm.buyTd = perm.sellTd;
                }
            }

            permission = {
                'theirOrgId': perm.orgId,
                'buyOurDecision': perm.buyOd,
                'buyTheirDecision': perm.buyTd,
                'sellOurDecision': perm.sellOd,
                'sellTheirDecision': perm.sellTd,
                'lastUpdatedUsername': getLastUpdatedUsername(userDetails, perm.buyUpdateUser),
                'buyLastUpdatedDate': perm.buyOdLastUpdated,
                'sellLastUpdatedDate': perm.sellOdLastUpdated
            };

            return permission;
        };

        var getOrganisationName = function (orgId, organisations) {
            var matchingOrg = _.find(organisations, function (org) {
                return org.id === orgId;
            });

            if (matchingOrg) {
                return matchingOrg.name;
            }

            return 'Unknown';
        };

        var getProductName = function (productId, products) {
            var matchingProduct = _.find(products, function (product) {
                return product.id === productId;
            });

            if (matchingProduct) {
                return matchingProduct.name;
            }

            return '';
        };

        var addOrganisationsToList = function (organisationList, organisationsToAdd) {

            return _.uniq(_.union(organisationList, organisationsToAdd), function (x) {
                return x.id;
            });
        };

        var PermissionStatus = {
            allow: 'allow',
            forbid: 'forbid',
            conflict: 'conflict'
        };

        var determineStatus = function (ourPermission, theirPermission) {
            switch (ourPermission) {
                case true:
                    switch (theirPermission) {
                        case true:
                            return PermissionStatus.allow;
                        case false:
                            return PermissionStatus.conflict;
                        default:
                            return PermissionStatus.conflict;
                    }
                    break;
                case false:
                    switch (theirPermission) {
                        case true:
                            return PermissionStatus.conflict;
                        case false:
                            return PermissionStatus.forbid;
                        default:
                            return PermissionStatus.forbid;
                    }
                    break;
                default:
                    switch (theirPermission) {
                        case true:
                            return PermissionStatus.conflict;
                        case false:
                            return PermissionStatus.forbid;
                        default:
                            return PermissionStatus.forbid;
                    }
            }
        };

        var updatePermission = function (productOperation, permission) {
            productOperation.us.permission = permission;
            productOperation.overallStatus = determineStatus(productOperation.us.permission, productOperation.them.permission);
        };

        var findOrganisation = function (organisationId, organisations) {

            return _.find(organisations, {id: organisationId}) || null;
        };

        var findOrCreateCounterparty = function (theirOrganisationId, counterparties, products, organisations) {
            var counterparty,
                orgName,
                nameAndCode,
                legalName,
                shortCode,
                org,
                newCounterpartyPermission;

            counterparty = _.find(counterparties, {id: theirOrganisationId});
            if (counterparty) {
                return counterparty;
            }

            org = findOrganisation(theirOrganisationId, organisations);
            if (org) {
                orgName = org.name;
                nameAndCode = org.name + '(' + org.shortCode + ')';
                legalName = org.legalName;
                shortCode = org.shortCode;
            } else {
                orgName = "<Unknown Id:" + theirOrganisationId + ">";
                nameAndCode = "<Unknown Id:" + theirOrganisationId + ">";
                legalName = '';
                shortCode = '';
            }

            newCounterpartyPermission = {
                id: theirOrganisationId,
                name: orgName,
                nameAndCode: nameAndCode,
                legalName: legalName,
                shortCode: shortCode,
                hoverContent: [
                    {title: "Name", content: orgName},
                    {title: "Legal Name", content: legalName},
                    {title: "Short Code", content: shortCode}],
                filtered: false,
                products: []
            };

            for (var pi = 0; pi < products.length; pi++) {
                newCounterpartyPermission.products.push(
                    {
                        id: products[pi].id,
                        cpSubscribed: false,
                        buy: {
                            us: {permission: null, originalPermission: null, lastUpdated: {by: null, at: null}},
                            them: {permission: null, lastUpdated: {by: null, at: null}},
                            overallStatus: "forbid"
                        },
                        sell: {
                            us: {permission: null, originalPermission: null, lastUpdated: {by: null, at: null}},
                            them: {permission: null, lastUpdated: {by: null, at: null}},
                            overallStatus: "forbid"
                        }
                    }
                );
            }
            counterparties.push(newCounterpartyPermission);
            return newCounterpartyPermission;
        };

        var iterateOverMatrixCounterparties = function (callback, counterparties) {
            for (var ci = 0; ci < counterparties.length; ci++) {
                var co = counterparties[ci];
                callback(co);
            }
        };

        var iterateOverMatrixProducts = function (callback, counterparties) {
            iterateOverMatrixCounterparties(
                function (co) {
                    for (var pi = 0; pi < co.products.length; pi++) {
                        var pr = co.products[pi];
                        callback(co, pr);
                    }
                }, counterparties);
        };

        return {
            translateTriStateToPermission: translateTriStateToPermission,
            translatePermissionToTriState: translatePermissionToTriState,
            buildHoverContent: buildHoverContent,
            getLastUpdatedUsername: getLastUpdatedUsername,
            makePermissionsTabs: makePermissionsTabs,
            updateLastUpdatedInfo: updateLastUpdatedInfo,
            productShouldBeVisible: productShouldBeVisible,
            calculateMatrixPerm: calculateMatrixPerm,
            getOrganisationName: getOrganisationName,
            getProductName: getProductName,
            addOrganisationsToList: addOrganisationsToList,
            updatePermission: updatePermission,
            determineStatus: determineStatus,
            PermissionStatus: PermissionStatus,
            findOrCreateCounterparty: findOrCreateCounterparty,
            iterateOverMatrixCounterparties: iterateOverMatrixCounterparties,
            iterateOverMatrixProducts: iterateOverMatrixProducts
        };
    });