angular.module('argusOpenMarkets.userSettings').service("userSettingsStateService", function userSettingsStateService($rootScope){
    "use strict";

    var state =  'UserSettings.UserDetails';

    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            if(toState.name.indexOf('UserSettings.')!==-1){
                state = toState.name;
            }
        });

    return{
        getUserSettingsState: function(){return state;}
    };
});