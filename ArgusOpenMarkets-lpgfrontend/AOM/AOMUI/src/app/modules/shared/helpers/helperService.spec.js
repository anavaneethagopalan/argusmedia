﻿describe('helperService Tests', function() {

    beforeEach(module('argusOpenMarkets.shared'));

    it('should encode string', inject(function (helpers){
        var encoded = helpers.encodeString("testString");
        expect(result).not.toBe("testString");
        var result = helpers.decodeString(encoded);
        expect(result).toEqual("testString");
    }));


    it('should display number with commas',inject(function(helpers){
        var number = 123456789;

        var result = helpers.formatNumberWithCommas(number,0);

        expect(result).toBe('123,456,789');
    }));

    it('should display number to 2 decimal places when has more than 2',inject(function(helpers){
        var number = 123456789.1234;

        var result = helpers.formatNumberWithCommas(number,2);

        expect(result).toBe('123,456,789.12');
    }));

    it('should remove the first match from an array', inject(function(helpers) {
        var xs = [2, 1, 3, 1, 4];
        xs.removeFirstMatch(function(i) {return i === 1;});
        expect(xs).toEqual([2, 3, 1, 4]);
    }));

    it('should return an empty array when asked to removeFirstMatch from an empty array', function() {
        var xs = [];
        xs.removeFirstMatch(function(i) {return i === 1;});
        expect(xs).toEqual([]);
    });

    it('should add organisation short code to organisation name for nameAndCode', inject(function (helpers){
        var fakeOrg = {
            name: "Fake Company",
            shortCode: "FAK"
        };
        expect(helpers.getOrganisationNameAndCode(fakeOrg)).toEqual("Fake Company (FAK)");
    }));

    it('should give nameAndCode as just the organisation name if it has no short code', inject(function (helpers){
        var fakeOrg = {
            name: "Fake Company"
        };
        expect(helpers.getOrganisationNameAndCode(fakeOrg)).toEqual("Fake Company");
    }));

    it('should return an empty string when calling formatNumberWithCommas when a null number is passed in', inject(function (helpers){

        var result = helpers.formatNumberWithCommas(null, 2);
        expect(result).toEqual("");
    }));

    it('should return an the number with no decimal places if no decimal places supplied', inject(function (helpers){

        var result = helpers.formatNumberWithCommas(123456, null);
        expect(result).toEqual("123,456");
    }));

    it('should return an the number with no decimal places if no decimal places supplied', inject(function (helpers){

        var result = helpers.formatNumberWithCommas(123456.765, 2);
        expect(result).toEqual("123,456.76");
    }));
});