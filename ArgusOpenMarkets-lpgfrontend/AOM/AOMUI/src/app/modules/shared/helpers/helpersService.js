﻿angular.module('argusOpenMarkets.shared').service("helpers", function helpers($log) {
    //this.isEmpty = isEmpty;
    function isEmpty(value) {
        return (typeof value === "undefined" || value === null);
    }

    function areEqualAndNotEmpty(a,b){
        if(isEmpty(a)){
            return false;
        }
        if(isEmpty(b)){
            return false;
        }
        if(a === b){
            return true;
        }
        return false;
    }

    var isBlank = function (value) {
        return (typeof value === "undefined" || value === null || value==="");
    };

    Array.prototype.check = function() {
        var arr = this, i, max_i;
        for (i = 0, max_i = arguments.length; i < max_i; i++) {
            arr = arr[arguments[i]];
            if (arr === undefined) {
                return false;
            }
        }
        return true;
    };


    Array.prototype.compare = function(testArr) {
        if (this.length !== testArr.length) {
            return false;
        }
        for (var i = 0; i < testArr.length; i++) {
            if (this[i].compare) {
                if (!this[i].compare(testArr[i])){ return false;}
            }
            if (this[i] !== testArr[i]) {return false;}
        }
        return true;
    };

    Array.prototype.removeFirstMatch = function(predicate) {
        for (var i = 0; i < this.length; ++i) {
            if (predicate(this[i])) {
                this.splice(i, 1);
                return;
            }
        }
    };

    Array.prototype.firstOrDefault = function (predicate) {
        for (var i = 0; i < this.length; ++i) {
            if (predicate(this[i])) {
                return this[i];
            }
        }

        return null;
    };

    var formatNumberWithCommas = function (number, decimalPlaces){

        if(number) {
            if (decimalPlaces === undefined) {
                decimalPlaces = 0;
            }

            try {
                var rounded = number.toFixed(decimalPlaces);
                var parts = rounded.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }
            catch(err){
                $log.debug("formatNum" + err);
            }

        }

        return "";
    };

    var getOrganisationNameAndCode = function(org) {
        if (org && org.shortCode) {
            return org.name + " (" + org.shortCode + ")";
        } else {
            if(org) {
                return org.name;
            }
        }

        return "";
    };

    /************ Encoding and decoding    *********************/
    var Base64 = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        encode: function (e) {
            var t = "";
            var n, r, i, s, o, u, a;
            var f = 0;
            e = Base64._utf8_encode(e);
            while (f < e.length) {
                n = e.charCodeAt(f++);
                r = e.charCodeAt(f++);
                i = e.charCodeAt(f++);
                s = n >> 2;
                o = (n & 3) << 4 | r >> 4;
                u = (r & 15) << 2 | i >> 6;
                a = i & 63;
                if (isNaN(r)) {
                    u = a = 64;
                } else if (isNaN(i)) {
                    a = 64;
                }
                t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a);
            }
            return t;
        }, decode: function (e) {
            var t = "";
            var n, r, i;
            var s, o, u, a;
            var f = 0;
            e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (f < e.length) {
                s = this._keyStr.indexOf(e.charAt(f++));
                o = this._keyStr.indexOf(e.charAt(f++));
                u = this._keyStr.indexOf(e.charAt(f++));
                a = this._keyStr.indexOf(e.charAt(f++));
                n = s << 2 | o >> 4;
                r = (o & 15) << 4 | u >> 2;
                i = (u & 3) << 6 | a;
                t = t + String.fromCharCode(n);
                if (u !== 64) {
                    t = t + String.fromCharCode(r);
                }
                if (a !== 64) {
                    t = t + String.fromCharCode(i);
                }
            }
            t = Base64._utf8_decode(t);
            return t;
        }, _utf8_encode: function (e) {
            e = e.replace(/\r\n/g, "\n");
            var t = "";
            for (var n = 0; n < e.length; n++) {
                var r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                } else if (r > 127 && r < 2048) {
                    t += String.fromCharCode(r >> 6 | 192);
                    t += String.fromCharCode(r & 63 | 128);
                } else {
                    t += String.fromCharCode(r >> 12 | 224);
                    t += String.fromCharCode(r >> 6 & 63 | 128);
                    t += String.fromCharCode(r & 63 | 128);
                }
            }
            return t;
        }, _utf8_decode: function (e) {
            var t = "";
            var n = 0;
            //var r = c1 = c2 = 0;
            //var r = c2 = 0;
            var c2 = 0;
            var r = c2;
            var c3 = 0;
            while (n < e.length) {
                r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                    n++;
                } else if (r > 191 && r < 224) {
                    c2 = e.charCodeAt(n + 1);
                    t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                    n += 2;
                } else {
                    c2 = e.charCodeAt(n + 1);
                    c3 = e.charCodeAt(n + 2);
                    t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                    n += 3;
                }
            }
            return t;
        }};

    var encodeString = function (string){
        return Base64.encode(string);
    };

    var decodeString = function (string) {
       return Base64.decode(string);
    };

    var getUrlParameter = function(sParam) {
        var sPageURL = window.location.hash.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            //if (sParameterName[0] === sParam) {
            if (sParameterName[0].indexOf(sParam) >0){
                return sParameterName[1];
            }
        }
    };

    Date.now = Date.now || function() { return +new Date; };

    Date.prototype.addDays = function (days) {
        this.setDate(this.getDate() + days);
        return this;
    };

    Date.prototype.subDays = function (days) {
        this.setDate(this.getDate() - days);
        return this;
    };

    Array.prototype.getIndexBy = function (name, value) {
        for (var i = 0; i < this.length; i++) {
            if (this[i][name] === value) {
                return i;
            }
        }
    };

    return {
        isEmpty : isEmpty,
        isBlank : isBlank,
        encodeString : encodeString,
        decodeString : decodeString,
        getUrlParameter : getUrlParameter,
        formatNumberWithCommas : formatNumberWithCommas,
        areEqualAndNotEmpty:areEqualAndNotEmpty,
        getOrganisationNameAndCode: getOrganisationNameAndCode
    };
});