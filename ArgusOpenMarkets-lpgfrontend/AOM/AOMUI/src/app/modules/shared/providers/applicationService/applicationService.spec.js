﻿describe('Version Service', function () {
    'use strict';

    var _applicationService,
        httpBackend,
        http;

    beforeEach(function() {
        module('argusOpenMarkets.shared');
        inject(function ($httpBackend, $http) {
            httpBackend = $httpBackend;
            http = $http;
        });
    });

    beforeEach(inject(function(applicationService){
        _applicationService = applicationService;
    }));


    it('Gets Application Version Configuration', function(){
        var versionConfig;

        _applicationService.promise().then(function(){
           var versionConfig = _applicationService.getVersionConfiguration();

           expect(versionConfig.version).toEqual('1');
       });

    });

    it('Gets Application Debug Mode', function(){
        var versionConfig;

        _applicationService.promise().then(function(){
            var versionConfig = _applicationService.getApplicationDebugMode();

            expect(versionConfig.debug).toBeTruthy();
        });

    });

    it('Gets Application Background Class', function(){
        var backgroundClass;

        _applicationService.promise().then(function(){
            var versionConfig = _applicationService.getApplicationBackgroundClass();

            expect(versionConfig.backgroundClass).toEqual("pre-live-background-class");
        });

    });
});