﻿angular.module('argusOpenMarkets.shared').factory("socketProvider", function socketProvider(
    $q,
    $timeout,
    $interval,
    $window,
    $modal,
    diffusionProvider,
    messagePopupsService,
    diffusionConfigurationService,
    keepAlivePingTime,
    maxReconnectRetryAttempts,
    timeBetweenConnectionAttempts,
    $location,
    $log)
{
    'use strict';

    var diffusionClient = diffusionProvider.getDiffusionClient(),
        diffusionConfiguration;

    var onConnect = $q.defer();

    var getClient = function(){
        return diffusionClient;
    };

    var socketEndpoint = function(){
        return diffusionConfiguration.endpoint;
    };

    var onTopicDeletedCallbacks = [];

    var registerCallbackOnTopicDeleted = function(callback){
        onTopicDeletedCallbacks.push(callback);
    };

    var onTopicDeleted = function(topic){
        for (var i = 0; i < onTopicDeletedCallbacks.length; i++){
            var callback = onTopicDeletedCallbacks[i];
            callback(topic);
        }
    };

    var keepAlive = function(){
        $interval(function(){
            diffusionClient.ping();
        },keepAlivePingTime);
    };

    var disconnect = function() {
        if(diffusionClient.isConnected()) {
            diffusionClient.close();
        }
    };

    var isConnected = function() {
        return diffusionClient.isConnected();
    };

    var subscribe = function(topic) {
        // DIFFUSION 5.1.4 CODE
        // diffusionClient.subscribe(topic);

        // NEW CODE _ INSTEAD OF SUBSCRIBING CLIENT SIDE _ SEND A MESSAGE STATING WE'D LIKE TO SUBSCRIBE
        var topicToSubscribeTo = JSON.stringify({ 'messageAction': 'CREATE',  'messageType': 'TopicSubscription', 'messageBody': { 'topicSubscribe': topic, 'topicUnsubscribe': '' }});
        sendMessage('AOM/Users', topicToSubscribeTo);
    };

    var unsubscribe = function(topic) {
        $log.debug("unsubscribed from ", topic);
        // diffusionClient.unsubscribe(topic);

        var topicToUnSubscribeTo = JSON.stringify({ 'messageAction': 'CREATE',  'messageType': 'TopicSubscription', 'messageBody': { 'topicSubscribe': '', 'topicUnsubscribe': topic }});
        sendMessage('AOM/Users', topicToUnSubscribeTo);
    };

    var removeTopicListener = function(handle){
        $log.debug("removeTopicListener from ", handle);
        diffusionClient.unsubscribe(handle);
    };

    var fetch = function(topic){
        diffusionClient.fetch(topic);
    };

    var sendMessage = function(topic, message) {
        diffusionClient.send(topic, message);
    };

    var addTopicCallback = function(topicName, onMessageReceivedCallback){
        diffusionClient.addTopicListener(topicName, onMessageReceivedCallback);
    };

    var openLostConnectionModalInstance = function(){
        lostConnectionModalInstance = $modal.open({
            templateUrl: 'src/app/modules/shared/providers/socket/views/lostConnection.part.html',
            controller: 'lostConnectionController',
            backdrop: 'static'
        });
    };
    var reconnectAttempts=0;
    var reconnect = function(){
        if(reconnectAttempts <= maxReconnectRetryAttempts) {
            $log.debug('Attempting Diffusion reconnect');

            if (reconnectAttempts === 0){
                openLostConnectionModalInstance();
            }

            diffusionClient.reconnect();
            reconnectAttempts++;

            if(reconnectAttempts === maxReconnectRetryAttempts) {
                logout();
            }
        }
    };

    var logout = function(){

        messagePopupsService.displayError("AOM Connection Lost", "Please try to log in again");
        $timeout(function () {
            if(angular.isDefined(lostConnectionModalInstance)) {
                lostConnectionModalInstance.close();
            }
            disconnect();
            $window.onbeforeunload = null;
            $location.url('?l=1');
            $window.location.reload();
        }, 3000);
    };

    var connectCallbacks = [];
    var registerConnectCallback = function(callback,callOnReconnect){
        connectCallbacks.push({callback:callback,callOnReconnect:callOnReconnect});
    };

    var lostConnectionCallbacks = [];
    var registerLostConnectionCallback = function(callback){
        lostConnectionCallbacks.push(callback);
    };

    var lostConnectionModalInstance;

    var isReconnectCycle = false;

    var connect = function(onData, credentials, onConnectionRejected) {
        var endpoint,
            port,
            debug,
            wsURL,
            xHRURL,
            timeoutMS,
            connectionDetails;

        diffusionConfigurationService.promise().then(function(){
            diffusionConfiguration = diffusionConfigurationService.getDiffusionConfiguration();
            if (diffusionConfiguration.wsURL){
                wsURL = diffusionConfiguration.wsURL;
            }
            if (diffusionConfiguration.wsURL){
                xHRURL = diffusionConfiguration.XHRURL;
            }
            port = diffusionConfiguration.port;


            endpoint = diffusionConfiguration.endpoint;
            timeoutMS = 100000;
            if((typeof(diffusionConfiguration.debug) ==="boolean" && diffusionConfiguration.debug)||
                (typeof(diffusionConfiguration.debug) ==="string" && diffusionConfiguration.debug ==="true")) {
                $log.info(new Array(100).join("_"));
                $log.info("Diffusion logging is enabled...");
                $log.info(new Array(100).join("_"));
                debug  = true;
            }
            connectionDetails = {
                wsURL:wsURL ,
                XHRURL: xHRURL,
                debug: debug,
                disableSilverlight : true,
                disableIframe: true,
                timeoutMS:timeoutMS,
                wsTimeout:timeoutMS,
                transportTimeout:timeoutMS,

                onLostConnectionFunction: function(){
                    if(diffusionConfiguration.logoutUserOnFailover) {
                        $log.debug('Lost connection to diffusion, logging user out...');
                        logout();
                    }else{
                        $log.debug('Lost connection to diffusion, attempting reconnect...');
                        reconnect();
                        for (var i = 0; i < lostConnectionCallbacks.length; i++) {
                            lostConnectionCallbacks[i]();
                        }
                    }
                },

                onCallbackFunction: function(isConnected, isReconnect){

                    $log.debug('isConnected: ' + isConnected + '|isReconnect: ' + isReconnect + '|isReconnectCycle: ' + isReconnectCycle);

                    if (!isConnected){
                        if (isReconnect){
                            $timeout(reconnect, timeBetweenConnectionAttempts);
                            isReconnectCycle = true;
                        }
                        else{
                            logout();
                        }
                    }

                    if (isConnected){

                        reconnectAttempts = 0;
                        if (lostConnectionModalInstance) { lostConnectionModalInstance.close(); }

                        //do the callbacks
                        for (var i = 0; i < connectCallbacks.length; i++) {
                            var container = connectCallbacks[i];

                            if (isReconnectCycle && container.callOnReconnect){
                                container.callback(isConnected,isReconnect);
                            }
                            else if (!isReconnectCycle && !container.callOnReconnect){
                                container.callback(isConnected,isReconnect);
                            }
                        }
                        if (isReconnectCycle){
                            isReconnectCycle = false;
                        }
                        onConnect.resolve();
                    }
                    else{
                        logout();
                    }

                    keepAlive();
                }
            };

            if(onData) {
                connectionDetails.onDataFunction = onData;
            }
            if(onConnectionRejected)
            {
                connectionDetails.onConnectionRejectFunction = onConnectionRejected;
            }

            connectionDetails.onTopicStatusFunction = function(message){
                if (message.status === "R"){
                    onTopicDeleted(message.topic);
                }
            };

            try {
                diffusionClient.connect(connectionDetails, credentials);
            }
            catch(ex){
                $log.error(ex);
            }
        });
    };

    return{
        unsubscribe:unsubscribe,
        removeTopicListener:removeTopicListener,
        addTopicCallback:addTopicCallback,
        onConnect:onConnect,
        subscribe: subscribe,
        connect:connect,
        disconnect:disconnect,
        getClient:getClient,
        socketEndpoint:socketEndpoint,
        isConnected:isConnected,
        sendMessage:sendMessage,
        fetch:fetch,
        registerCallbackOnTopicDeleted:registerCallbackOnTopicDeleted,
        registerConnectCallback:registerConnectCallback,
        reconnect:reconnect,
        registerLostConnectionCallback:registerLostConnectionCallback
    };
});
