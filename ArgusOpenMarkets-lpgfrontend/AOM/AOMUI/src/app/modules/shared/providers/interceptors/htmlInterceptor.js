angular.module('argusOpenMarkets.shared')
    .service("htmlInterceptor", function htmlInterceptor() {
        "use strict";

        // NOTE: Leave string below exactly as is - Team City will replace the VERSION_NUM with the correct version number.
        var version = "VERSION_NUM";

        var htmlUrl = function(configUrl){
            var htmlUrl = false,
                urlToLower;

            if(configUrl !== null) {
                urlToLower = configUrl.toLowerCase();
                if(urlToLower) {
                    if (urlToLower.indexOf('src') > -1) {
                        htmlUrl = (configUrl.toLowerCase().indexOf('.html') > -1);
                    }
                }
            }

            return htmlUrl;
        };

        var htmlInterceptRequest = function (config) {

            if(config) {
                if (htmlUrl(config.url)) {
                    var separator = config.url.indexOf('?') === -1 ? '?' : '&';
                    config.url = config.url + separator + 'v=' + version;
                }
            }

            return config;
        };

        var htmlInterceptResponse =  function(response){

            if(response.config.url.toLowerCase().indexOf('applicationconfig.json') > -1){
                // This is response of application config.
                version = response.data.version;
            }
            return response;
        };

        return {
            request: htmlInterceptRequest,
            response: htmlInterceptResponse,
            htmlUrl: htmlUrl
        };
    })
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('htmlInterceptor');
    }]);