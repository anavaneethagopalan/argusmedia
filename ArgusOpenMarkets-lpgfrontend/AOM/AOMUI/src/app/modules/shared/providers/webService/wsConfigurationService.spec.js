﻿describe('Web Service Configuration service', function () {
    'use strict';

    var wsConfigurationServiceTest,
        httpBackend,
        http;

    beforeEach(function() {
        module('argusOpenMarkets.shared');
        inject(function ($httpBackend, $http) {
            httpBackend = $httpBackend;
            http = $http;
        });
    });

    beforeEach(inject(function(wsConfigurationService){
        wsConfigurationServiceTest = wsConfigurationService;
    }));


    it('Gets Web Service Configuration', function(){
        httpBackend.expectGET('assets/webServiceConfig.json');
        wsConfigurationServiceTest.promise();
        wsConfigurationServiceTest.getWsConfiguration();

    });

    it('Gets Web Service Configuration and sets wsConfig', function(){
        httpBackend.when('GET', 'assets/webServiceConfig.json').respond({test: 'test'});
        wsConfigurationServiceTest.promise();
        wsConfigurationServiceTest.getWsConfiguration()
        httpBackend.flush();
    });
});