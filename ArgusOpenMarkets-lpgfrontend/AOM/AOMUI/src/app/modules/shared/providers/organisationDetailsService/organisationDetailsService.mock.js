angular.module('argusOpenMarkets.shared')
    .service("mockOrganisationDetailsService",
    function organisationDetailsService() {
        "use strict";

        var organisationDetails = [];

        var findOrCreateMappingCategory = function(categoryName){
            for (var i=0;i<organisationDetails.length;i++){
                var c = organisationDetails[i];
                if (c.name === categoryName ){
                    return c;
                }
            }

            var newname = {
                name: categoryName,
                mappings: []};

            organisationDetails.push(newname);
            return newname;
        };

        var connectToOrganisationDetailsTopic = function(orgId, callback){

            findOrCreateMappingCategory("CounterPartyPermissionChanges").mappings.push({
                    "id": 1,
                    "name": "Naphtha CIF NWE - Outright",
                    "emailAddress": "travis@goldhammer.com",
                    "eventType": "CounterPartyPermissionChanges"
                });

            callback();

            findOrCreateMappingCategory("UserSuspension").mappings.push({
                    "id": 2,
                    "name": "Naphtha CIF NWE - Diff",
                    "emailAddress": "dave@beefchest.com",
                    "eventType": "UserSuspension"

                });

            callback();

            findOrCreateMappingCategory("UserSuspension").mappings.push({
                "id": 3,
                "name": "Naphtha CIF NWE - Outright",
                "emailAddress": "max@fightmaster.com",
                "eventType": "UserSuspension"
            });

            callback();

        };

        var getMappings = function(){
            return organisationDetails;
        };
        var getMappingCategories = function(){return [];};
        return {
            connectToOrganisationDetailsTopic:connectToOrganisationDetailsTopic,
            getMappings:getMappings,
            getMappingCategories:getMappingCategories
        };
    });