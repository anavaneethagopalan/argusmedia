angular.module('argusOpenMarkets.shared')
    .service("tradePermissionsService", function tradePermissionsService(
        userService,
        messageSubscriptionService2,
        $q,
        $rootScope)
    {
        "use strict";

        var tradePermissionsSummaryPath  = "AOM/Organisations/" + userService.getUsersOrganisation().id+"/PermissionsMatrix/Summary/Products/";
        var tradePermissions={products:[]};

        function TradePermission(productId,permissionType,canTrade,organisationId,buyOrSellPermission){
            this.productId = productId;
            this.permissionType = permissionType;
            this.buyOrSellPermission = buyOrSellPermission.toLowerCase();
            this.canTrade = canTrade;
            this.organisationId = organisationId;
        }

        function ProductPermissions() {
            this.counterpartyPermissions = {buy:[],sell:[]};
            this.brokerPermissions = {buy:[],sell:[]};
        }

        var notifySubscribers = function(){
            $rootScope.$broadcast("TradePermissionsService-Change");
        };

        var createPermissionFromResponse = function(response){
            var splitPath = response.topicPath.split("/");
            var productId = splitPath[splitPath.indexOf("Products") +1];
            var buyOrSell = splitPath[splitPath.indexOf("Buy")] ? "buy" : "sell";
            var permissionType = response.message.messageType;

            return new TradePermission(productId,
                permissionType,
                response.canTrade,
                response.organisationId,
                buyOrSell);
        };

        var addOrUpdatePermission = function(response){

            var permission = createPermissionFromResponse(response);
            if(typeof tradePermissions.products[permission.productId] === 'undefined'){
                tradePermissions.products[permission.productId] = new ProductPermissions();
            }
            var counterpartyOrBroker  = permission.permissionType === "CounterpartyPermission" ? "counterpartyPermissions" : "brokerPermissions";
            tradePermissions.products[permission.productId][counterpartyOrBroker][permission.buyOrSellPermission][permission.organisationId] = permission;
            notifySubscribers();
        };

        var deletePermission = function(deletedPath){
            var splitPath = deletedPath.split("/");
            var productId = splitPath[splitPath.indexOf("Products") +1];
            var buyOrSell = splitPath[splitPath.indexOf("Buy")] ? "buy" : "sell";
            var permissionType = splitPath[splitPath.indexOf("Principals")] ? "counterpartyPermissions" : "brokerPermissions";
            var orgId = splitPath[splitPath.length - 1];
            delete tradePermissions.products[productId][permissionType][buyOrSell][orgId];

            notifySubscribers();
        };

        var checkTradePermissions = function(productId, brokerPermissions, buyOrSell, brokerId){

            if(tradePermissions && tradePermissions.products && tradePermissions.products.check) {

                return tradePermissions.products.check(productId,
                    brokerPermissions,
                    buyOrSell,
                    brokerId);
            }

            return false;
        };

        var canTrade = function(productId, buyOrSell, counterpartyId, brokerId, coBrokeredOrder){
            // invert as you are doing the opposite to what is on the order -> if they are selling you are buying
            buyOrSell = buyOrSell === "sell" ? "buy" : "sell";

            var canTradeWithBroker;
            var canTradeWithCounterparty;

            if(userService.isBroker()){
                if(coBrokeredOrder){
                    canTradeWithCounterparty = checkTradePermissions(productId, "brokerPermissions", buyOrSell, counterpartyId);
                    return canTradeWithCounterparty;
                }else {
                    if((brokerId === null || typeof brokerId === 'undefined')){
                        canTradeWithCounterparty = checkTradePermissions(productId,  "brokerPermissions", buyOrSell, counterpartyId);
                        return canTradeWithCounterparty;
                    }
                    return false;//a broker cannot trade an order if order has named broker (and it isn't cobrokered)
                }
            } else {
                canTradeWithBroker = (brokerId === null || typeof brokerId === 'undefined') ? true : checkTradePermissions(productId, "brokerPermissions", buyOrSell, brokerId);
                canTradeWithCounterparty = checkTradePermissions(productId, "counterpartyPermissions", buyOrSell, counterpartyId);
                return canTradeWithCounterparty && canTradeWithBroker;
            }
        };

        messageSubscriptionService2.subscribeToTopicDeletedUsingPath("PermissionsMatrix/Summary",deletePermission);
        messageSubscriptionService2.subscribeToTopicUsingPath(tradePermissionsSummaryPath,addOrUpdatePermission);

        return{
            canTrade:canTrade
        };
    });