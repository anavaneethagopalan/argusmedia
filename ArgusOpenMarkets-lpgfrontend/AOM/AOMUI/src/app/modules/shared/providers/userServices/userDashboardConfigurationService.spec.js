﻿describe('Dashboard Configuration service', function () {
    'use strict';

    var userDashboardConfigurationServiceTest,
        $httpBackend,
        mockSocketProvider,
        mockUserService = {
            getSubscribedProductIds: function(){return [1];},
            getSubscribedProductIdsByDisplayOrder: function(){ return [1];},
            hasProductPrivilege: function(){},
            hasProductPrivilegeOnAnyProduct: function(){},
            getUserId: function(){return 1},
            userMessagePromise: { promise: { then: function(resolve) {return resolve();} } }
        },
        mockCookieService = {
            readCookie: function () {
                return null
            }
        },
        mockProductConfigurationService = {
            getAllProducts: function(){
                return [1,2,3,4,5];
            }
        };

    beforeEach(
        module('argusOpenMarkets.shared')
    );

    beforeEach(
        module(function ($provide) {
            var $injector = angular.injector(['argusOpenMarkets.shared']);
            mockSocketProvider = $injector.get('mockSocketProvider');
            $provide.value('cookieService', mockCookieService);
            $provide.value('userService', mockUserService);
            $provide.value('socketProvider', mockSocketProvider);
            $provide.value('productConfigurationService', mockProductConfigurationService);
        })
    );

    beforeEach(inject(function(userDashboardConfigurationService, $injector){
        $httpBackend = $injector.get('$httpBackend');
        userDashboardConfigurationServiceTest = userDashboardConfigurationService;
    }));

    it('can view widget filter should return false for bid ask widget if the user is not entitled to the bid ask stack for that product', function(){

        var canViewWidget,
            widget;
        spyOn(mockUserService,'hasProductPrivilege').and.returnValue(false);
        widget = {"view": "bidAskStack", "productIds": [1]};
        canViewWidget = userDashboardConfigurationServiceTest.canViewWidget(widget);

        expect(canViewWidget).toBeFalsy();
    });

    it('can view widget filter should return true for bid ask widget if the user is entitled to the bid ask stack for that product', function(){

        var canViewWidget,
            widget;

        spyOn(mockUserService,'hasProductPrivilege').and.returnValue(true);

        widget = {"view": "bidAskStack", "productIds": [1], displayOrder: 1};
        canViewWidget = userDashboardConfigurationServiceTest.canViewWidget(widget);

        expect(canViewWidget).toBeTruthy();
    });

    it('can view widget filter should return false for assessments widget if the user is not entitled to the assessments widget for any product', function(){

        var canViewWidget,
            widget;

        spyOn(mockUserService,'hasProductPrivilegeOnAnyProduct').and.returnValue(false);


        widget = {"view": "assessments", "productIds": [1,2]};
        canViewWidget = userDashboardConfigurationServiceTest.canViewWidget(widget);

        expect(canViewWidget).toBeFalsy();
    });

    it('can view widget filter should return true for assessments widget if the user is entitiled to assessments widget for a single product', function(){

        var canViewWidget,
            widget;

        spyOn(mockUserService,'hasProductPrivilegeOnAnyProduct').and.returnValue(true);

        widget = {"view": "assessments", "productIds": [1,2]};
        canViewWidget = userDashboardConfigurationServiceTest.canViewWidget(widget);

        expect(canViewWidget).toBeTruthy();
    });

    it('can view widget filter should return true for assessments widget if the user is entitiled to assessments widget for all products', function(){

        var canViewWidget,
            widget;

        spyOn(mockUserService,'hasProductPrivilegeOnAnyProduct').and.returnValue(true);

        widget = {"view": "assessments", "productIds": [1,2]};
        canViewWidget = userDashboardConfigurationServiceTest.canViewWidget(widget);

        expect(canViewWidget).toBeTruthy();
    });

    it('can view widget filter should return true for market ticker widget if the user is entitiled to market ticker widget for all products', function(){

        var canViewWidget,
            widget;

        spyOn(mockUserService,'hasProductPrivilegeOnAnyProduct').and.returnValue(true);

        widget = {"view": "marketTicker", "productIds": [1,2]};
        canViewWidget = userDashboardConfigurationServiceTest.canViewWidget(widget);

        expect(canViewWidget).toBeTruthy();
    });


    it('Gets Web Service Configuration', function () {
        $httpBackend.expectGET('assets/userDashboardConfig.json');
        userDashboardConfigurationServiceTest.getUserConfiguration();
    });

    it('Gets Web Service Configuration and sets wsConfig', function () {
        $httpBackend.when('GET', 'assets/userDashboardConfig.json').respond({test: 'test'});
        userDashboardConfigurationServiceTest.getUserConfiguration();
        $httpBackend.flush();
    });

    // it('get user dashboard configuration returns a dashboard layout if the user is subscribed to a single product', function(){
    //     var expectedDashboardConfig = [{"productIds":[1],"tile":{"tileType":"assessments","position":{"row":"default","col":"default"},"size":{"x":"default","y":"default"}}},{"productIds":[1],"tile":{"tileType":"marketTicker","position":{"row":"default","col":"default"},"size":{"x":"default","y":"default"}}},{"productIds":[1],"tile":{"tileType":"newsFeed","position":{"row":"default","col":"default"},"size":{"x":"default","y":"default"}}},{"productIds":[1],"tile":{"tileType":"bidAskStack","position":{"row":"default","col":"default"},"size":{"x":"default","y":"default"}}}];
    //     var dashboardConfig = userDashboardConfigurationServiceTest.getUserDashboardConfig();
    //     expect(dashboardConfig).toEqual(expectedDashboardConfig);
    // });
});