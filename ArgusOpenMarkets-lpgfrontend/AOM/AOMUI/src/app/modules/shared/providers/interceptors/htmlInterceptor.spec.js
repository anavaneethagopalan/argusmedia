describe('htmlInterceptor', function () {
    "use strict";

    var htmlInterceptor = htmlInterceptor,
        httpProvider,
        $httpBackend,
        $http;

    describe("html Interceptor", function () {

        beforeEach(function () {
            module('argusOpenMarkets.shared', function ($httpProvider) {
                //save our interceptor
                httpProvider = $httpProvider;

                httpProvider.interceptors = [];
                httpProvider.interceptors.push('htmlInterceptor');

            });
        });

        beforeEach(inject(function(_htmlInterceptor_, _$httpBackend_, _$http_) {
            htmlInterceptor = _htmlInterceptor_;
            $httpBackend = _$httpBackend_;
            $http = _$http_;
        }));


        var $httpBackend;
        var htmlInterceptor;
        var token = 'someToken';

        describe('html interceptor tests', function () {

            it('should be defined', function () {
                expect(htmlInterceptor).toBeDefined();
            });

            it('html url should return true if the url starts with src and the extension is of type html', function(){

                $httpBackend.expectGET('http://src/blah/blah/nathan.html?v=VERSION_NUM').respond(200, {});
                $http.get('http://src/blah/blah/nathan.html');
                $httpBackend.flush();
            });

            it('html url should return true if the url starts with src and the extension is of type html', function(){

                $httpBackend.expectGET('http://src/blah/blah/nathan.html?v=VERSION_NUM').respond(200, {});
                $http.get('http://src/blah/blah/nathan.html');
                $httpBackend.flush();
            });

            it('html url should return true if the original url if it is not one of our urls', function(){

                $httpBackend.expectGET('http://taostr/blah/blah/nathan.html').respond(200, {});
                $http.get('http://taostr/blah/blah/nathan.html');
                $httpBackend.flush();
            });

            it('html url should return true if the original url is not a html part', function(){

                $httpBackend.expectGET('http://src/blah/blah/nathan.js').respond(200, {});
                $http.get('http://src/blah/blah/nathan.js');
                $httpBackend.flush();
            });

        });

    });
});