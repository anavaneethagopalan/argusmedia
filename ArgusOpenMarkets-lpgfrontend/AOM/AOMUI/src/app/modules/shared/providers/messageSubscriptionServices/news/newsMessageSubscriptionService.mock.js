﻿angular.module('argusOpenMarkets.shared')
    .service("mockNewsMessageSubscriptionService", function mockNewsMessageSubscriptionService() {
        "use strict";

        var subscribedNews = {News:function(){}};

        this.subscribeToNews = function (action){
            subscribedNews = {News:action};
         };

    });
