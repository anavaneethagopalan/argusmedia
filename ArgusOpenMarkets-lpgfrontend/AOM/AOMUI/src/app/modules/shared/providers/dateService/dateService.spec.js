describe('Date Service', function () {
    'use strict';

    var _dateService;

    beforeEach(function() {
        module('argusOpenMarkets.shared');
    });

    beforeEach(inject(function(dateService){
        _dateService = dateService;
    }));


    it('Returns the Date Time Now', function(){

        var dateTimeNow = new Date(),
            dateServiceDateTimeNow = _dateService.getDateTimeNow();

        expect(dateTimeNow.getYear()).toEqual(dateServiceDateTimeNow.getYear());
        expect(dateTimeNow.getMonth()).toEqual(dateServiceDateTimeNow.getMonth());
        expect(dateTimeNow.getDay()).toEqual(dateServiceDateTimeNow.getDay());
        expect(dateTimeNow.getHours()).toEqual(dateServiceDateTimeNow.getHours());
        expect(dateTimeNow.getMinutes()).toEqual(dateServiceDateTimeNow.getMinutes());
    });

    it('should remove a utc identifier from a date', function(){
        var utcDate = "2017-03-22T00:00:00Z",
            nonUtcDate;

        nonUtcDate = _dateService.removeUtcIdentifierFromDate(utcDate);
        expect(nonUtcDate).toEqual('2017-03-22T00:00:00');
    });

    it('should not remove a utc identifier from a date that has no utc identifer', function(){
        var utcDate = "2017-03-22T00:00:00",
            nonUtcDate;

        nonUtcDate = _dateService.removeUtcIdentifierFromDate(utcDate);
        expect(nonUtcDate).toEqual(utcDate);
    });
});