﻿describe('MarketTickerMessageSubscrService that for some reason test ProductMessageSubscriptionService', function () {
    'use strict';

    var mockMessageSubscriptionService2;

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(function(){
        module(function($provide) {
            var $injector = angular.injector([ 'argusOpenMarkets.shared' ]);
            mockMessageSubscriptionService2 = $injector.get( 'mockMessageSubscriptionService2' );
            spyOn(mockMessageSubscriptionService2,'subscribeToTopicUsingPath').and.callThrough();
            $provide.value('messageSubscriptionService2', mockMessageSubscriptionService2);
        });
    });

    it('should subscribe new to products', inject(function(productMessageSubscriptionService){
	    var rootPath = "AOM/Orders/Products/" + 1 + "/SEC-Privileges/";
        var topicPaths = [
            "AOM/Orders/Products/1/"
        ];
        productMessageSubscriptionService.subscribeToProduct(1);
        topicPaths.forEach(function(topicPath) {
            expect(mockMessageSubscriptionService2.subscribeToTopicUsingPath).toHaveBeenCalledWith(
                topicPath, jasmine.any(Function) );
        });
    }));

    it('should not subscribe multiple times to products', inject(function(productMessageSubscriptionService){
        productMessageSubscriptionService.subscribeToProduct(1);
        productMessageSubscriptionService.subscribeToProduct(2);
        productMessageSubscriptionService.subscribeToProduct(1);
        expect(mockMessageSubscriptionService2.subscribeToTopicUsingPath.calls.count()).toEqual(3);
    }));

    it('should allow you to register actions to product messages Bid', inject(function(productMessageSubscriptionService){
        productMessageSubscriptionService.subscribeToProduct(1);
        var testMessageAction = { action:function(){}};
        spyOn(testMessageAction,'action');
        productMessageSubscriptionService.registerActionOnProductMessageType("TestSubscriber",1,"onBid", testMessageAction.action);
        mockMessageSubscriptionService2.mockOnMessage(
            "AOM/Orders/Products/1/SEC-Privileges/View_BidAsk_Widget/NotHeld/",
            {messageType:"Order",order:{orderType:"Bid",productId :1}}
        );
        expect(testMessageAction.action).toHaveBeenCalled();
    }));

    it('should allow you to register actions to product messages Ask', inject(function(productMessageSubscriptionService){
        productMessageSubscriptionService.subscribeToProduct(1);
        var testMessageAction = {action:function(){}};
        spyOn(testMessageAction,'action');
        productMessageSubscriptionService.registerActionOnProductMessageType("TestSubscriber",1,"onAsk", testMessageAction.action);
        mockMessageSubscriptionService2.mockOnMessage(
            "AOM/Orders/Products/1/SEC-Privileges/View_BidAsk_Widget/NotHeld/",
            {messageType:"Order",order:{orderType:"Ask",productId :1}}
        );
        expect(testMessageAction.action).toHaveBeenCalled();
    }));

    it('should not allow you to register actions to non subscribed products', inject(function(productMessageSubscriptionService){
        var testMessageAction = {action:function(){}};
        spyOn(testMessageAction,'action');
        productMessageSubscriptionService.registerActionOnProductMessageType("TestSubscriber",1,"onBid", testMessageAction.action);
        mockMessageSubscriptionService2.mockOnMessage(
            "AOM/Orders/Products/1/SEC-Privileges/View_BidAsk_Widget/NotHeld/",
            {messageType:"Order",order:{orderType:"Bid",productId :1}}
        );
        expect(testMessageAction.action).not.toHaveBeenCalled();
    }));

});