﻿angular.module('argusOpenMarkets.shared').factory("mockSocketProvider", function mockSocketProvider() {
    "use strict";

    var bConnected = false;

    var connect = function(){ bConnected=true; };
    var disconnect = function() { bConnected=false; };
    var isConnected = function() { return bConnected; };
    var callbackConnect;
    var getClient = function(){ return "diffusionClient"; };
    var socketEndpoint = function(){ return "diffusionConfiguration.endpoint"; };

    var registerCallbackOnTopicDeleted = function(callback){};
    var subscribe = function(topic) {};
    var fetch = function(topic){};
    var sendMessage = function(topic, message) {};
    var addTopicCallback = function(topicName, onMessageReceivedCallback){};
    var onConnect = function(){callbackConnect(true);};

    var registerLostConnectionCallback = function(callback) {};
    var registerConnectCallback = function(callback, reconnect){callbackConnect =callback;};

    return{
        addTopicCallback:addTopicCallback,
        onConnect:onConnect,
        subscribe: subscribe,
        connect:connect,
        disconnect:disconnect,
        getClient:getClient,
        socketEndpoint:socketEndpoint,
        isConnected:isConnected,
        sendMessage:sendMessage,
        fetch:fetch,
        registerCallbackOnTopicDeleted:registerCallbackOnTopicDeleted,
        registerConnectCallback:registerConnectCallback,
        registerLostConnectionCallback:registerLostConnectionCallback
    };

});
