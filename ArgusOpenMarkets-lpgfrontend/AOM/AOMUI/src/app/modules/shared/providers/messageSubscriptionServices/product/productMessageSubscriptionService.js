﻿angular.module('argusOpenMarkets.shared')
    .service("productMessageSubscriptionService", function productMessageSubscriptionService(messageSubscriptionService2, helpers) {
        "use strict";

        var getProductAction = function(orderType){
            switch(orderType) {
                case "Bid" : return "onBid";
                case "Ask" : return "onAsk";
                case "Deal" : return "onDeal";
            }
        };

        var processMessage = function (response) {
            var messageType = getProductAction(response.order.orderType);
            response.order.topicKey = response.topicPath;
            var productId = response.order.productId;
            var product = _.find(subscribedProducts, function (subscribedProduct) {
                return subscribedProduct.productId === productId;
            });
            if(!helpers.isEmpty(product)) {
                angular.forEach(product.productMessageActions[messageType], function (subscriber) {
                    subscriber.action(response);
                });
            }
        };

        var handleTopicDeleted = function(topic){
            var product;
            for (var i = 0; i < subscribedProducts.length; i++){
                product = subscribedProducts[i];

                for (var j = 0; j < product.productMessageActions.onOrderDeleted.length; j++){
                    product.productMessageActions.onOrderDeleted[j].action(topic);
                }
            }
        };

        var subscribedProducts = [];

        var isSubscribedToProduct = function(productId){
            var found = false;
            angular.forEach(subscribedProducts,function(subscribedProduct){
                if(subscribedProduct.productId === productId){
                     found = true;
                }
            });
            return found;
        };

        var isSubscribedToAnyProduct = function(){
            if(subscribedProducts.length > 0){
                return true;
            }

            return false;
        };

        var createOrdersTopicPaths = function(productId) {
            var rootPath = "AOM/Orders/Products/" + productId + "/";
            return {
                "updates": rootPath,
                "delete": rootPath
            };
        };

        this.unsubscribeFromProduct = function(productId) {
            var topicPaths = createOrdersTopicPaths(productId);
            messageSubscriptionService2.unsubscribeFromTopicUsingPath(topicPaths.updatesh);
            var subscribedProductIndex = subscribedProducts.getIndexBy('productId', productId);
            subscribedProducts.splice(subscribedProductIndex, 1);
            messageSubscriptionService2.unsubscribeFromTopicDeletedUsingPath(topicPaths.delete);
        };

        this.subscribeToProduct = function (productId) {
            // DIFFUSION 5.1.4 - OLD CODE - USER IS SUBSCRIBING TO ORDER TOPIC PATHS CLIENT SIDE
            // WE WILL NOW SUBSCRIBE USER TO ORDER TOPICS SERVER SIDE
            var topicPaths = createOrdersTopicPaths(productId);
            messageSubscriptionService2.subscribeToTopicUsingPath(topicPaths.updates, processMessage);
            if(!isSubscribedToProduct(productId)){
                subscribedProducts.push({
                    productId: productId,
                    productMessageActions: {onBid: [], onAsk: [], onOrderDeleted: [], onDeal: [], onInfo: []}
                });
                messageSubscriptionService2.subscribeToTopicDeletedUsingPath(topicPaths.delete, handleTopicDeleted);
            }
        };

        this.registerActionOnProductMessageType = function (subscriberId, productId, messageType, action) {
            var productToRegisterAgainst = _.find(subscribedProducts, function (subscribedProduct) {
                return subscribedProduct.productId === productId;
            });
            if(!helpers.isEmpty(productToRegisterAgainst)) {
                var subscriberIndex = productToRegisterAgainst.productMessageActions[messageType].getIndexBy("subscriberId",subscriberId);
                if(!helpers.isEmpty(subscriberIndex)){
                    productToRegisterAgainst.productMessageActions[messageType].splice(subscriberIndex,1);
                }
                productToRegisterAgainst.productMessageActions[messageType].push({subscriberId:subscriberId,action:action});
            }
        };

        this.unregisterActionOnProductMessageType = function (subscriberId, productId, messageType) {
            var productToUnregisterActionAgainst = _.find(subscribedProducts, function (subscribedProduct) {
                return subscribedProduct.productId === productId;
            });
            if(!helpers.isEmpty(productToUnregisterActionAgainst)) {
                var subscriberIndex = productToUnregisterActionAgainst.productMessageActions[messageType].getIndexBy("subscriberId",subscriberId);
                if(!helpers.isEmpty(subscriberIndex)){
                    productToUnregisterActionAgainst.productMessageActions[messageType].splice(subscriberIndex,1);
                }
            }
        };

    });
