angular.module('argusOpenMarkets.dashboard').service("mockClientNotificationMessageService",
    function mockClientNotificationMessageService() {
        "use strict";

        this.onErrorEventHandler = function(userMessage){
            window.console.debug("onErrorEventHandler: " + userMessage.messageBody);
        };

        this.onMessageEventHandler = function(userMessage){
            window.console.debug("onMessageEventHandler: " + userMessage.messageBody);
        };

        this.onUserMessageEventHandler = function(userMessage){
            window.console.debug("onUserMessageEventHandler: " + userMessage.messageBody);
        };

        this.onLogoffEventHandler = function(userMessage){
            window.console.debug("onLogoffEventHandler: " + userMessage.messageBody);
        };

        this.unsubscribe = function(){
        };

        this.logout= function(userMessage){
        };
    }
);


