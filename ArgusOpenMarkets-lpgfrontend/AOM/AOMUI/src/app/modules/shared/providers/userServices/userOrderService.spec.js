﻿describe('user order service tests', function() {
    "use strict";

    var scope,
        userOrderServiceTest;
    var mockUserService = {
        getUsersOrganisation : function(){return {organisationName:"XYZ"};},
        hasProductPrivilege: function(productId, privs){return true;},
        getUserId: function() {return 1;},
        sendStandardFormatMessageToUsersTopic:function(a,b){sendMsgResult = b;}
    };

    var create_mock_promise = function (call_through, do_reject, mock_data) {
        return {
            then: function (resolve, reject) {
                if (!call_through) {
                    return this;
                } else {
                    if (do_reject) {
                        return _(reject).isFunction() ? reject(mock_data) : undefined;
                    } else {
                        return resolve(mock_data);
                    }
                }
            }
        };
    };

    var sendMsgResult;
    var mockSocketProvider ={
        processMessage: null,
        subscribe:function(){return true;},
        connect:function(){},
        onConnect:{promise: create_mock_promise(true,false,null)},
        addTopicCallback:function(topicName,onMessage){ this.processMessage =onMessage },
        onMessage:function(message){this.processMessage(message)},
        getClient:function(){return {getClientID: function(){return "XXXX";}}}
    };

    beforeEach(function () {
        module('argusOpenMarkets.shared');
    });

    beforeEach(function(){
        spyOn(mockSocketProvider ,"subscribe");
        spyOn(mockUserService ,"getUsersOrganisation").and.returnValue({id:1});
        spyOn(mockUserService,"sendStandardFormatMessageToUsersTopic");
        module(function($provide) {
            $provide.value('socketProvider', mockSocketProvider);
            $provide.value('userService', mockUserService);
        });
    });

    beforeEach(inject(function(userOrderService){
        userOrderServiceTest = userOrderService;
    }));

    it('isownorder should return true if userid is the same as the principal id', inject(function(){
        var order = {principalOrganisationId:1,brokerOrganisationId:2};
        expect(userOrderServiceTest.isOrderMine(order)).toEqual(true);
    }));

    it('isownorder should return false if userid is not same as the principal id', inject(function(){
        var order = {principalOrganisationId:2};
        expect(userOrderServiceTest.isOrderMine(order)).toEqual(false);
    }));

    it('isownorder should return true if userid is the same as the broker id', inject(function(){
        var order = {principalOrganisationId:1,brokerOrganisationId:1};
        expect(userOrderServiceTest.isOrderMine(order)).toEqual(true);
    }));

    it('isownorder should return false if userid is not same as the broker id', inject(function(){
        var order = {principalOrganisationId:2,brokerOrganisationId:3};
        expect(userOrderServiceTest.isOrderMine(order)).toEqual(false);
    }));
    it('should send new order formatted message', inject(function(){
        var order = {principalOrganisationId:1,brokerOrganisationId:2};
        userOrderServiceTest.sendNewOrderMessage(order);
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith
        ("Order","Create", {"principalOrganisationId":1,"brokerOrganisationId":2});
    }));

    it('should send edit order formatted message', inject(function(){
        var order = {principalOrganisationId:1, brokerOrganisationId:2};
        userOrderServiceTest.sendEditOrderMessage(order);
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith
        ("Order","Update", {"principalOrganisationId":1,"brokerOrganisationId":2});
    }));
    it('should send hold order formatted message', inject(function(){
        var order = {principalOrganisationId:1, brokerOrganisationId:2};
        userOrderServiceTest.sendHoldOrderMessage(order);
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith
        ("Order","Hold", {"principalOrganisationId":1,"brokerOrganisationId":2});
    }));
    it('should send reinstate order formatted message', inject(function(){
        var order = {principalOrganisationId:1, brokerOrganisationId:2};
        userOrderServiceTest.sendReinstateOrderMessage(order);
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith
        ("Order","Reinstate", {"principalOrganisationId":1,"brokerOrganisationId":2});
    }));
    it('should send execute order formatted message', inject(function(){
        var order = {principalOrganisationId:1, brokerOrganisationId:2};
        userOrderServiceTest.sendExecuteOrderMessage(order);
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith
        ("Order","Execute", {"principalOrganisationId":1,"brokerOrganisationId":2});
    }));
    it('should send kill order formatted message', inject(function(){
        var order = {principalOrganisationId:1, brokerOrganisationId:2};
        userOrderServiceTest.sendKillOrderMessage(order);
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith
        ("Order","Kill", {"principalOrganisationId":1,"brokerOrganisationId":2});
    }));
});
