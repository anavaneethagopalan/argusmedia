describe('midnightEventService tests', function(helpers) {

    var midnightEventService, $timeout, $rootScope;

    var mockDateService, fakeMidnightCallback, fakeScope;

    beforeEach(function() {
        module('argusOpenMarkets.shared');
    });

    beforeEach(function() {
        fakeMidnightCallback = jasmine.createSpy("fakeMidnightCallback");
        fakeScope = jasmine.createSpyObj("fakeScope", ["$on"]);

        var getNowCounter = 0;
        var fakeNow = new Date(2015, 0, 1, 23, 59, 59);
        var fakeNextNow = new Date(2015, 0, 2, 0, 0, 1);
        mockDateService = jasmine.createSpyObj("mockDateService", ["getDateTimeNow"]);
        mockDateService.getDateTimeNow.and.callFake(function() {
            ++getNowCounter;
            if (getNowCounter == 1) {
                return fakeNow;
            }
            return fakeNextNow.addDays(getNowCounter - 1);
        });
    });

    beforeEach(function() {
        module(function($provide) {
            $provide.value('dateService', mockDateService);
        });
    });

    beforeEach(inject(function(_$rootScope_, _$timeout_, _midnightEventService_) {
        $rootScope = _$rootScope_;
        spyOn($rootScope, '$broadcast').and.callThrough();
        spyOn($rootScope, '$on').and.callThrough();
        $timeout = _$timeout_;
        midnightEventService = _midnightEventService_;
    }));

    it('should be defined', function() {
        expect(midnightEventService).toBeDefined();
    });

    it('should not trigger the onMidnight event when created', function() {
        expect($rootScope.$broadcast).not.toHaveBeenCalledWith('midnightService-midnight-event');
    });

    it('should not trigger the onMidnight event if we have not reached midnight yet', function() {
        $timeout.flush(500);
        expect($rootScope.$broadcast).not.toHaveBeenCalledWith('midnightService-midnight-event');
    });

    it('should trigger the onMidnight event if the time to midnight has elapsed', function() {
        $timeout.flush(2500);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('midnightService-midnight-event');
    });

    it('should report the onMidnight event name as the one broadcast uses', function() {
        $timeout.flush(2500);
        expect($rootScope.$broadcast).toHaveBeenCalledWith(midnightEventService.getMidnightEventName());
    });

    it('should not have triggered the onMidnight event again if another midnight has not happened yet', function() {
        $timeout.flush(2500);
        expect($rootScope.$broadcast).toHaveBeenCalledWith(midnightEventService.getMidnightEventName());
        $rootScope.$broadcast.calls.reset();
        $timeout.flush(60*60*1000);
        expect($rootScope.$broadcast).not.toHaveBeenCalledWith(midnightEventService.getMidnightEventName());
    });

    it('should trigger the onMidnight event again for the next midnight', function() {
        $timeout.flush(2500);
        expect($rootScope.$broadcast).toHaveBeenCalledWith(midnightEventService.getMidnightEventName());
        $rootScope.$broadcast.calls.reset();
        $timeout.flush(24*60*61*1000); // give it 1s more than 1 day
        expect($rootScope.$broadcast).toHaveBeenCalledWith(midnightEventService.getMidnightEventName());
    });

    it('should trigger the onMidnight event again for the midnight after next', function() {
        $timeout.flush(2500);
        expect($rootScope.$broadcast).toHaveBeenCalledWith(midnightEventService.getMidnightEventName());
        $rootScope.$broadcast.calls.reset();
        $timeout.flush(24*60*61*1000);
        expect($rootScope.$broadcast).toHaveBeenCalledWith(midnightEventService.getMidnightEventName());
        $rootScope.$broadcast.calls.reset();
        $timeout.flush(24*60*61*1000);
        expect($rootScope.$broadcast).toHaveBeenCalledWith(midnightEventService.getMidnightEventName());
    });

    it('should cancel the timeout if the rootScope is destroyed', function() {
        $rootScope.$emit('$destroy');
        $timeout.verifyNoPendingTasks();
    });
});
