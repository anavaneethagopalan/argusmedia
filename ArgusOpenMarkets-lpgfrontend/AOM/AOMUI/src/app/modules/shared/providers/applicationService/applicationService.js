﻿angular.module('argusOpenMarkets.shared').service("applicationService", function applicationService($http) {

    var appConfiguration = null;

    var promise = function () {
        return $http.get('assets/applicationConfig.json').success(function (data) {
            appConfiguration = data;
        });
    };

    var getVersionConfiguration = function () {
        return trimSuffix("0.", appConfiguration.version);
    };

    var getCssVersionConfiguration = function () {
        return appConfiguration.cssVersion;
    };

    var getApplicationDebugMode = function () {
        return appConfiguration.debug;
    };

    var getApplicationBackgroundClass = function () {
        return appConfiguration.backgroundClass;
    };

    var trimSuffix = function (suffix, input) {
        if (input.indexOf(suffix) === 0) {
            return trimSuffix(suffix, input.substring(suffix.length, input.length));
        } else {
            return input;
        }
    };

    return {
        promise: promise,
        getVersionConfiguration: getVersionConfiguration,
        getCssVersionConfiguration: getCssVersionConfiguration,
        getApplicationDebugMode: getApplicationDebugMode,
        getApplicationBackgroundClass: getApplicationBackgroundClass
    };
});