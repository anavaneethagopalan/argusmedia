angular.module('argusOpenMarkets.shared').service("dateService", function dateService(){

    var getDateTimeNow = function(){
        return new Date();
    };

    var removeUtcIdentifierFromDate = function(dateToRemoveUtcIdentifier){
        if(dateToRemoveUtcIdentifier){
            if(dateToRemoveUtcIdentifier[dateToRemoveUtcIdentifier.length-1] === 'Z'){
                dateToRemoveUtcIdentifier = dateToRemoveUtcIdentifier.substring(0, dateToRemoveUtcIdentifier.length - 1);
            }
        }

        return dateToRemoveUtcIdentifier;
    };

    return {
        getDateTimeNow: getDateTimeNow,
        removeUtcIdentifierFromDate: removeUtcIdentifierFromDate
    };
});