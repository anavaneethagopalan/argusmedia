﻿describe('Diffusion Configuration service', function () {
    'use strict';
    var diffusionConfigurationServiceTest,httpBackend,http;
    var create_mock_promise = function (call_through, do_reject, mock_data) {
        return {
            then: function (resolve, reject) {
                if (!call_through) {
                    return this;
                } else {
                    if (do_reject) {
                        return _(reject).isFunction() ? reject(mock_data) : undefined;
                    } else {
                        return resolve(mock_data);
                    }
                }
            }
        };
    };

    beforeEach(function() {
        module('argusOpenMarkets.shared');
        inject(function ($httpBackend, $http) {
            httpBackend = $httpBackend;
            http = $http;
        });
    });


    beforeEach(inject(function(diffusionConfigurationService){
        diffusionConfigurationServiceTest = diffusionConfigurationService;
    }));

    it('Gets Diffusion Configuration', function(){
        httpBackend.expectGET('assets/diffusionConfig.json');

        diffusionConfigurationServiceTest.promise();
        diffusionConfigurationServiceTest.getDiffusionConfiguration();

    });

});