﻿describe('messageSubscriptionService2', function () {
    "use strict";

    var mockSocketProvider ={
        processMessage: null,
        deleteCallback:null,
        subscribe:function(){return true;},
        unsubscribe:function(){return true;},
        removeTopicListener:function(){return true;},
        connect:function(){},
        addTopicCallback:function(topicName,onMessage){
            this.processMessage =onMessage
        },
        registerCallbackOnTopicDeleted:function(topicName,onMessage){
            this.deleteCallback = onMessage
        },
        onMessage:function(message){this.processMessage(message)},
        onDeletion:function(topic){this.deleteCallback(topic)},
        createSocketMessage:function(topic, message){
            var socketMessage = {
                records:[
                    {fields:[message]}
                ],
                displayFormat: function(){ return this.records;},
                topic: topic
            };
            mockSocketProvider.onMessage( socketMessage );
        }
    };
    beforeEach(module('toastr'));

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(function(){
        spyOn(mockSocketProvider, 'connect');
        module(function($provide) {
            $provide.value('socketProvider', mockSocketProvider);

        });
    });

    it('should send to registered callback for defined Topic and Message type', inject(function(messageSubscriptionService2){
        var productService ={processMessage: function(){return true;}};
        spyOn(productService,'processMessage');
        messageSubscriptionService2.subscribeToTopicAndMessageType("OrderTopic","Order",productService.processMessage);
        mockSocketProvider.createSocketMessage("AOM/OrderTopic",'{"message" : {"messageType":"Order" ,"messageBody":{"productId":"1"}}}');
        expect(productService.processMessage).toHaveBeenCalled();
    }));

    it('should send to registered callback for defined Topic and Message type, and also callback for defined Topic only', inject(function(messageSubscriptionService2){
        var productService ={processMessage: function(){return true;}};
        spyOn(productService,'processMessage');
        var otherService ={processMessage: function(){return true;}};
        spyOn(otherService,'processMessage');
        messageSubscriptionService2.subscribeToTopicAndMessageType("OrderTopic","Order",productService.processMessage);
        messageSubscriptionService2.subscribeToTopicUsingPath("OrderTopic",otherService.processMessage);
        mockSocketProvider.createSocketMessage("AOM/OrderTopic",'{"message" : {"messageType":"Order" ,"messageBody":{"productId":"1"}}}');
        expect(productService.processMessage).toHaveBeenCalled();
        expect(otherService.processMessage).toHaveBeenCalled();
    }));

    it('should send to registered callback for defined Topic but not Message type, when message type differs', inject(function(messageSubscriptionService2){
        var productService ={processMessage: function(){return true;}};
        spyOn(productService,'processMessage');
        var otherService ={processMessage: function(){return true;}};
        spyOn(otherService,'processMessage');
        messageSubscriptionService2.subscribeToTopicAndMessageType("OrderTopic","Order",productService.processMessage);
        messageSubscriptionService2.subscribeToTopicUsingPath("OrderTopic",otherService.processMessage);
        mockSocketProvider.createSocketMessage("AOM/OrderTopic",'{"message" : {"messageType":"NotAnOrder" ,"messageBody":{"productId":"1"}}}');
        expect(productService.processMessage).not.toHaveBeenCalled();
        expect(otherService.processMessage).toHaveBeenCalled();
    }));


    it('should not send to registered callback for defined Topic or Message type, when topic differs', inject(function(messageSubscriptionService2){
        var productService ={processMessage: function(){return true;}};
        spyOn(productService,'processMessage');
        var otherService ={processMessage: function(){return true;}};
        spyOn(otherService,'processMessage');
        messageSubscriptionService2.subscribeToTopicAndMessageType("OrderTopic","Order",productService.processMessage);
        messageSubscriptionService2.subscribeToTopicUsingPath("OrderTopic",otherService.processMessage);
        mockSocketProvider.createSocketMessage("AOM/DifferentTopic",'{"message" : {"messageType":"NotAnOrder" ,"messageBody":{"productId":"1"}}}');
        expect(productService.processMessage).not.toHaveBeenCalled();
        expect(otherService.processMessage).not.toHaveBeenCalled();
    }));


    it('should allow subscriptions to/unsubscriptions from a topic', inject(function(messageSubscriptionService2){
        spyOn(mockSocketProvider,'subscribe');
        spyOn(mockSocketProvider,'unsubscribe');
        spyOn(mockSocketProvider,'addTopicCallback');
        messageSubscriptionService2.subscribeToTopicAndMessageType("TestTopic","MessageType1",null);
        messageSubscriptionService2.subscribeToTopicAndMessageType("TestTopic","MessageType2",null);
        messageSubscriptionService2.subscribeToTopicUsingPath("TestTopic",null);
        expect(mockSocketProvider.subscribe.calls.count()).toBe(1);
        expect(mockSocketProvider.addTopicCallback.calls.count()).toBe(1);
        messageSubscriptionService2.subscribeToTopicAndMessageType("TestTopic2","MessageType1",null);
        expect(mockSocketProvider.subscribe.calls.count()).toBe(2);
        expect(mockSocketProvider.addTopicCallback.calls.count()).toBe(2);

        expect(mockSocketProvider.unsubscribe.calls.count()).toBe(0);
        messageSubscriptionService2.unsubscribeFromTopicAndMessageType("TestTopic2","MessageType1");
        expect(mockSocketProvider.unsubscribe.calls.count()).toBe(1);
        messageSubscriptionService2.unsubscribeFromTopicAndMessageType("TestTopic","MessageType1");
        expect(mockSocketProvider.unsubscribe.calls.count()).toBe(1);
        messageSubscriptionService2.unsubscribeFromTopicAndMessageType("TestTopic","MessageType2");
        expect(mockSocketProvider.unsubscribe.calls.count()).toBe(1);
        messageSubscriptionService2.unsubscribeFromTopicUsingPath("TestTopic");
        expect(mockSocketProvider.unsubscribe.calls.count()).toBe(2);
    }));

});