﻿angular.module('argusOpenMarkets.shared').service("diffusionConfigurationService", function diffusionConfigurationService($http){
    "use strict";

    var diffusionConfiguration = null;

    var promise = function() {
        return $http.get('assets/diffusionConfig.json').success(function (data) {
            diffusionConfiguration = data;
        });
    };

    return {
        promise: promise,
        getDiffusionConfiguration:function(){ return diffusionConfiguration; }
    };
});