﻿angular.module('argusOpenMarkets.shared')
    .service("messagePopupsService", function messagePopupsService(toastr, toastrConfig, popupMessageConfig, helpers,$log) {
        "use strict";

        // Adding the preventOpenDuplicates: true option below will prevent *all* duplicate toasts
        // from being shown and currently we only want duplicate errors to be prevented.
        // This is to prevent market open then close then open (say) from looking like the market
        // is closed (as the duplicate close message will not be shown).
        // If that changes then the whole ToastDetails, toastsShown and so forth can be replaced
        // with preventOpenDuplicates: true
        angular.extend(toastrConfig, {
            positionClass: "toast-bottom-full-width"
        });

        // N.B.  All of the duration and timeout values are supposed to be specified as NUMBERS, but have been specified in this file as strings - they will not work and the default values will be used.
        // If I correct them it will change the system behaviour (specifically the warnings will remain for 50sec), so am leaving them for now.  TH 7/11/2014.

        var useToastr = popupMessageConfig.useToastr;

        var toastrTypes = {
            error: "error"
        };

        var ToastDetails = function(type, title, message) {
            this.type = type;
            this.title = title;
            this.message = message;
        };
        ToastDetails.prototype.matches = function(type, title, message) {
            return this.type === type && this.title === title && this.message === message;
        };

        var toastsShown = [];

        var alreadyShowingToast = function(type, title, message) {
            for (var i = 0; i < toastsShown.length; ++i) {
                if (toastsShown[i].matches(type, title, message)) {
                    return true;
                }
            }
            return false;
        };
        var addToShownToasts = function(type, title, message) {
            toastsShown.push(new ToastDetails(type, title, message));
        };
        var onToastHidden = function(type, title, message) {
            toastsShown.removeFirstMatch(function(td) {
                return td.matches(type, title, message);
            });
        };


        var displaySuccess = function (title, message,duration) {
            if (useToastr) {
                var timeToShowPopup = angular.isDefined(duration)  ? duration : popupMessageConfig.success.duration ;
                toastr.success(message, title,
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "onclick": null,
                        "showDuration": timeToShowPopup,
                        "hideDuration": 1000,
                        "timeOut": timeToShowPopup,
                        "extendedTimeOut": 1000,
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
            } else {
                $log.log( title + "\n\n" + message );
            }
        };

        var displayInfo = function (title, message,duration) {
            if (useToastr) {
                var timeToShowPopup = angular.isDefined(duration)  ? duration : popupMessageConfig.info.duration ;
                toastr.info(message, title,
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "onclick": null,
                        "showDuration":timeToShowPopup,
                        "hideDuration": 1000,
                        "timeOut": timeToShowPopup,
                        "extendedTimeOut": 1000,
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
            } else {
                window.alert( title + "\n\n" + message );
            }
        };

        var displayWarning = function (title, message,duration) {
            if (useToastr) {
                var timeToShowPopup = angular.isDefined(duration)  ? duration : popupMessageConfig.warning.duration ;
                toastr.warning(message, title,
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "onclick": null,
                        "showDuration": timeToShowPopup,
                        "hideDuration": 0,
                        "timeOut": timeToShowPopup,
                        "extendedTimeOut": 10000,
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
            } else {
                window.alert( title + "\n\n" + message);
            }
        };

        var displayError = function (title, message, duration) {
            if (useToastr) {
                if (alreadyShowingToast(toastrTypes.error, title, message)) {
                    return;
                } else {
                    addToShownToasts(toastrTypes.error, title, message);
                }

                var timeToShowPopup = angular.isDefined(duration)  ? duration : popupMessageConfig.error.duration ;
                toastr.error(message, title,
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "onclick": null,
                        "showDuration": timeToShowPopup,
                        "hideDuration": 1000,
                        "timeOut": 0,
                        "extendedTimeOut": 0,
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut",
                        "onHidden": function() { onToastHidden("error", title, message); }
                    });
            } else {
                window.alert( title + "\n\n" + message);
            }
        };

        var clear = function(){
            toastr.clear();
            toastsShown = [];
        };

        var displayDataLossWarning = function (title, message, duration) {
            if (useToastr) {
                var timeToShowPopup = angular.isDefined(duration) ? duration : popupMessageConfig.warning.duration;
                toastr.warning(message, title,
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": 3000,
                        "hideDuration": 1000,
                        "timeOut": 5000,
                        "extendedTimeOut": 1000,
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
            } else {
                window.alert(title + "\n\n" + message);
            }
        };

        return{
            displayInfo: displayInfo,
            displayWarning: displayWarning,
            displayError: displayError,
            displaySuccess: displaySuccess,
            clear : clear,
            displayDataLossWarning: displayDataLossWarning
        };
    });

