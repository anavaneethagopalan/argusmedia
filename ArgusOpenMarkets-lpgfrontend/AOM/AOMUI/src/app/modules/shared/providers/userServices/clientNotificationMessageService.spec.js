﻿describe("User Message Handling service tests", function () {

    var mockUserService, mockMessageSubscriptionService2, mockSocketProvider;

    var mockWindow = {
        location: {
            href: '',
            reload: function(){}
        }
    };

    var mockMessagePopupsService = {
        displaySuccess: function (a,b) { return null; },
        displayInfo: function (a,b) { return null; },
        displayWarning: function (a,b) { return null; },
        displayError: function (a,b) { return null; }
    };

    var testService;

    beforeEach(
        module('argusOpenMarkets.shared')
    );

    beforeEach(
        module(function ($provide) {

        var $injector = angular.injector(['argusOpenMarkets.shared']);

        mockUserService = $injector.get('mockUserService');
        mockMessageSubscriptionService2 = $injector.get('mockMessageSubscriptionService2');
        mockSocketProvider = $injector.get('mockSocketProvider');

        $provide.value('userService', mockUserService);
        $provide.value('messagePopupsService', mockMessagePopupsService);
        $provide.value('messageSubscriptionService2', mockMessageSubscriptionService2);
        $provide.value('socketProvider', mockSocketProvider);
        $provide.value('$window', mockWindow);
    }));

    beforeEach(inject(function(clientNotificationMessageService){
        testService = clientNotificationMessageService;
    }));

    it('should call window reload when logout called',function(){

        var userMessage = {
            messageBody: ''
        };

        spyOn(mockWindow.location, 'reload');

        testService.logout(userMessage);

        expect(mockWindow.location.reload).toHaveBeenCalled();
    });

    it('should call socket provider disconnect when logout called',function(){

        var userMessage = {
            messageBody: ''
        };

        spyOn(mockSocketProvider, 'disconnect');

        testService.logout(userMessage);

        expect(mockSocketProvider.disconnect).toHaveBeenCalled();
    });

    it('should call message popup service display error on error event',function(){

        var userMessage = {message:{
            messageBody: 'messageBody',
            messageType: 'messageType'
        }};
        spyOn(mockMessagePopupsService,'displayError');

        testService.onErrorEventHandler(userMessage);

        expect(mockMessagePopupsService.displayError).toHaveBeenCalledWith(
            userMessage.message.messageType.toUpperCase(),userMessage.message.messageBody);
    });

    it('should call message popup service display info on message event',function(){

        var userMessage = {message:{
            messageBody: 'messageBody',
            messageType: 'messageType'
        }};
        spyOn(mockMessagePopupsService,'displayInfo');

        testService.onMessageEventHandler(userMessage);

        expect(mockMessagePopupsService.displayInfo).toHaveBeenCalledWith(
            "User Notification",userMessage.message.messageBody);
    });

    it('should call logout when log off event',function(){

        var userMessage = {
            messageBody: 'messageBody',
            messageType: 'messageType'
        };
        spyOn(mockWindow.location, 'reload');

        testService.onLogoffEventHandler(userMessage);

        expect(mockWindow.location.reload).toHaveBeenCalled();
        //expect(testService.logout).toHaveBeenCalledWith(userMessage);
    });

});
