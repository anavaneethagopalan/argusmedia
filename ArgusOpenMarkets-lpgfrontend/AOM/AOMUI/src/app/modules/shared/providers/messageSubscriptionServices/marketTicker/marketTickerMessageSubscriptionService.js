﻿angular.module('argusOpenMarkets.shared')
    .service("marketTickerMessageSubscriptionService",
        function marketTickerMessageSubscriptionService(messageSubscriptionService2,
                                                        userService,
                                                        $log)
        {
        "use strict";

        var actions = {};
        var onTickerItemDeleted;

        this.registerMarketTickerItemDeleted = function(onMarketTickerItemDeleted){
            onTickerItemDeleted = onMarketTickerItemDeleted;
        };

        var processMessage = function (response) {
            if(!response.message){return;}
            else {
                var action = actions[response.message.messageType];
                if (action) {
                    action(response);
                }
            }
        };


        this.registerActionOnMarketTickerMessageType = function (messageType, action) {
            actions[messageType] = action;

        };

        //When a topic is deleted -use for when the Pending Filter is active for AE
        var handleTopicDeletedInMT = function (topic) {
            var marketTickerId,
                topicArray = topic.split("/");

            if(topicArray){
                marketTickerId = topicArray[topicArray.length - 1];
            }
            if(typeof onTickerItemDeleted !== "undefined"){
                onTickerItemDeleted(marketTickerId);
            }
        };

        var subscribeUserToMarketTickerTopics = function(){

            var user = userService.getUser();
            if(user !== null) {
                messageSubscriptionService2.subscribeToTopicUsingPath("AOM/MarketTicker/", processMessage);
                // messageSubscriptionService2.subscribeToTopicUsingPath("AOM/MarketTicker/SEC-ViewableBy/", processMessage);
                messageSubscriptionService2.subscribeToTopicDeletedUsingPath("AOM/MarketTicker/", handleTopicDeletedInMT);
                // // AomMarketInfoResponse message are published to the user topic so subscribe to that as well here.
                messageSubscriptionService2.subscribeToTopicUsingPath('AOM/Users/' + userService.getUserId(), processMessage);
            }
        };

        subscribeUserToMarketTickerTopics();
    });
