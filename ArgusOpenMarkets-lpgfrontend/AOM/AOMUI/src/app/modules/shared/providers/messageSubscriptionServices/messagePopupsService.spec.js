﻿describe('MessagePopupsService', function () {
    'use strict';

    var mockToastr = {
        success: function (m, t, o) {
        },
        error: function (m, t, o) {
            if (o.onShown) {
                o.onShown(t, m);
            }
        },
        info: function (m, t, o) {
        },
        warning: function (m, t, o) {
        },
        clear: function() {
        }
    };

    var mockToastrConfig = {};

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(function () {
        module(function ($provide) {
            $provide.value('toastr', mockToastr);
            $provide.value('toastrConfig', mockToastrConfig);
        });
    });

    it("should call toastr success", inject(function (messagePopupsService) {
        spyOn(mockToastr, 'success');
        messagePopupsService.displaySuccess("title", "message");
        expect(mockToastr.success).toHaveBeenCalled();
    }));
    it("should call toastr info", inject(function (messagePopupsService) {
        spyOn(mockToastr, 'info');
        messagePopupsService.displayInfo("title", "message");
        expect(mockToastr.info).toHaveBeenCalled();
    }));
    it("should call toastr warning", inject(function (messagePopupsService) {
        spyOn(mockToastr, 'warning');
        messagePopupsService.displayWarning("title", "message");
        expect(mockToastr.warning).toHaveBeenCalled();
    }));
    it("should call toastr error", inject(function (messagePopupsService) {
        spyOn(mockToastr, 'error');
        messagePopupsService.displayError("title", "message");
        expect(mockToastr.error).toHaveBeenCalled();
    }));
    it("should allow you to set the timeout to 0", inject(function(messagePopupsService) {
        messagePopupsService.displayWarning("title", "message", 0);
        expect(mockToastr.options.timeOut).toEqual(0);
    }));
    it("should allow you to set the timeout to a number", inject(function(messagePopupsService) {
        messagePopupsService.displayWarning("title", "message", 100);
        expect(mockToastr.options.timeOut).toEqual(100);
    }));
    it("should allow you to set the timeout to a negative", inject(function(messagePopupsService) {
        spyOn(mockToastr, 'success');
        messagePopupsService.displayWarning("title", "message", -1);
        expect(mockToastr.options.timeOut).toEqual(-1);
    }));
    it("should register callback for onHidden when creating an error message", inject(function(messagePopupsService) {
        messagePopupsService.displayError("title", "message");
        expect(mockToastr.options.onHidden).toBeDefined();
    }));
    it("should not display a duplicate error toastr", inject(function(messagePopupsService) {
        spyOn(mockToastr, 'error').and.callThrough();
        messagePopupsService.displayError("title", "message");
        messagePopupsService.displayError("title", "message");
        expect(mockToastr.error.calls.count()).toEqual(1);
    }));
    it("should display multiple errors if the titles or messages are not the same", inject(function(messagePopupsService) {
        spyOn(mockToastr, 'error').and.callThrough();
        messagePopupsService.displayError("title", "message");
        messagePopupsService.displayError("title2", "message");
        messagePopupsService.displayError("title", "message2");
        expect(mockToastr.error.calls.count()).toEqual(3);
    }));
    it("should display second same error message if first is closed", inject(function(messagePopupsService) {
        var hideCallback = null;
        mockToastr.error = function(m, t, o) {
            hideCallback = function() { o.onHidden(t, m); };
            if (o.onShown) {
                o.onShown(t, m);
            }
        };
        spyOn(mockToastr, 'error').and.callThrough();
        messagePopupsService.displayError("title", "message");
        expect(hideCallback).not.toBeNull();
        hideCallback();
        messagePopupsService.displayError("title", "message");
        expect(mockToastr.error.calls.count()).toEqual(2);
    }));
    it("should display second same error message if clear called after first", inject(function(messagePopupsService) {
        spyOn(mockToastr, 'clear');
        spyOn(mockToastr, 'error').and.callThrough();
        messagePopupsService.displayError("title", "message");
        messagePopupsService.clear();
        expect(mockToastr.clear).toHaveBeenCalled();
        messagePopupsService.displayError("title", "message");
        expect(mockToastr.error.calls.count()).toEqual(2);
    }));
});

