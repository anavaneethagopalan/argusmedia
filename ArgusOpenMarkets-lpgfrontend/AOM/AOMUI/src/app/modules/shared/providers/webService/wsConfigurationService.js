﻿angular.module('argusOpenMarkets.shared').service("wsConfigurationService", function wsConfigurationService($http){
    "use strict";

    var wsConfiguration = null;

    var promise = function() {
        return $http.get('assets/webServiceConfig.json').success(function (data) {
            wsConfiguration = data;
        });
    };

    var getWsConfiguration = function() {
        return wsConfiguration; };

    return {
        promise: promise,
        getWsConfiguration:getWsConfiguration
   };
});