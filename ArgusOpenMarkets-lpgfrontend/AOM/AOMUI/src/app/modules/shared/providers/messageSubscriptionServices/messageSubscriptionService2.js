﻿angular.module('argusOpenMarkets.shared')
    .service("messageSubscriptionService2", function messageSubscriptionService2(socketProvider,
                                                                                 $log)
    {
        "use strict";

        var ANYORNONE = "ANYORNONE";

        var messageTypeSubscriptions = {};
        var pathDeleteSubscriptions = [];

        var fetchData = function(topic){
            socketProvider.fetch(topic);
        };

        // -- TOPIC UPDATES HANDLING --

        // Message received - may contain a messageType - route by path and messageType/ANYORNONE
        var onMessage = function(socketMessage)
        {
            if(socketMessage.displayFormat()>"") {
                var response;
                var messageType;
                var bHandled = false;

                try{
                    response = JSON.parse(socketMessage.records[0].fields[0]);
                }catch(ex){
                    $log.error(ex);
                    return false;
                }
                response.topicPath = socketMessage.topic;

                try{
                    messageType = response.message.messageType;
                }catch(ex){
                    messageType = ANYORNONE;
                }

                for (var subscribedTopic in messageTypeSubscriptions) {
                    if( messageTypeSubscriptions.hasOwnProperty( subscribedTopic ) ) {

                        if(socketMessage.topic.indexOf(subscribedTopic)>-1){
                            var subscription = messageTypeSubscriptions[subscribedTopic][messageType];

                            if (subscription !== undefined && subscription.callback !== undefined)
                            {
                                bHandled = true;
                                try{
                                    subscription.callback(response);
                                }catch(ex){
                                    $log.error(ex);
                                }
                            }

                            if (messageType !== ANYORNONE) {
                                subscription = messageTypeSubscriptions[subscribedTopic][ANYORNONE];

                                if (subscription !== undefined && subscription.callback !== undefined) {
                                    bHandled = true;
                                    try {
                                        subscription.callback(response);
                                    } catch (ex) {
                                        $log.error(ex);
                                    }
                                }
                            }
                        }
                    }
                }
                if (!bHandled){
                    $log.debug("MSS2 unhandled message being discarded: " + socketMessage.topic + " - " + messageType);
                }
            }
        };

        var subscribeToTopic = function(topic){

            socketProvider.subscribe(topic);
        };

        var subscribeToTopicAndMessageType = function( topic, messageType, callback, listenOnly){

            $log.debug("MSS2 registering callback for message type '" + messageType + "' on: " + topic);
            var topicSubscription = messageTypeSubscriptions[topic];
            if (topicSubscription === undefined){
                if (listenOnly!==true) {
                    $log.debug("  MSS2 subscribing to: " + topic);
                    socketProvider.subscribe(topic);
                }
                socketProvider.addTopicCallback(topic,onMessage);
                messageTypeSubscriptions[topic] = {};
            }
            messageTypeSubscriptions[topic][messageType]={"callback":callback};
        };

        var unsubscribeFromTopicAndMessageType = function(topic,messageType){
            var subscription = messageTypeSubscriptions[topic][messageType];
            if (subscription !== undefined) {
                $log.debug("MSS2 unregistering callback for message type '" + messageType + "' on: " + topic);
                delete messageTypeSubscriptions[topic][messageType];
                if (Object.keys(messageTypeSubscriptions[topic]).length === 0) {
                    $log.debug("  MSS2 unsubscribing from: " + topic);
                    unsubscribeAndRemoveListener(topic);
                    delete messageTypeSubscriptions[topic];
                }
            }
        };

        var unsubscribeAndRemoveListener = function(topic){
            socketProvider.unsubscribe(topic);
            socketProvider.removeTopicListener(topic);
        };

        var subscribeToTopicUsingPath = function( topic, callback, listenOnly ){
            subscribeToTopicAndMessageType( topic, ANYORNONE, callback, listenOnly );
        };

        var unsubscribeFromTopicUsingPath = function(topic){
            unsubscribeFromTopicAndMessageType( topic, ANYORNONE );
        };

        // -- TOPIC DELETION HANDLING --

        // Topic deletion received - route by path
        var onTopicDeletion = function( deletedPath ) {
            for(var i=pathDeleteSubscriptions.length-1;i>=0;i--)
            {
                if(deletedPath.indexOf(pathDeleteSubscriptions[i].topic)>-1){
                    var subscription = pathDeleteSubscriptions[i];

                    if (subscription.callback !== undefined)
                    {
                        subscription.callback(deletedPath);
                    }
                }
            }
        };

        socketProvider.registerCallbackOnTopicDeleted(onTopicDeletion);

        var subscribeToTopicDeletedUsingPath = function( topic, callback ){
            $log.debug("MSS2 registering callback for deletions on: " + topic);
            pathDeleteSubscriptions.push({"topic":topic,"callback":callback});
        };

        var unsubscribeFromTopicDeletedUsingPath = function(topic){
            for(var i=pathDeleteSubscriptions.length-1;i>=0;i--)
            {
                if(topic===pathDeleteSubscriptions[i].topic){
                    $log.debug("MSS2 unregistering callback for deletions on: " + topic);
                    pathDeleteSubscriptions.splice(i,1);
                }
            }
        };

        return{
            fetchData:fetchData,
            subscribeToTopicAndMessageType:subscribeToTopicAndMessageType,
            unsubscribeFromTopicAndMessageType:unsubscribeFromTopicAndMessageType,
            subscribeToTopicUsingPath:subscribeToTopicUsingPath,
            subscribeToTopic: subscribeToTopic,
            unsubscribeFromTopicUsingPath:unsubscribeFromTopicUsingPath,
            subscribeToTopicDeletedUsingPath:subscribeToTopicDeletedUsingPath,
            unsubscribeFromTopicDeletedUsingPath:unsubscribeFromTopicDeletedUsingPath
        };
    });


