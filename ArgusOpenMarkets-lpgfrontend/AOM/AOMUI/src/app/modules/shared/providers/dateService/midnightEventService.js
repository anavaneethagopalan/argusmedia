angular.module('argusOpenMarkets.shared').service("midnightEventService", function midnightEventService($rootScope,
                                                                                                        $timeout,
                                                                                                        dateService,
                                                                                                        $log,
                                                                                                        helpers) {
    var timer;
    var midnightEventName = 'midnightService-midnight-event';

    var getTimeUntilNextMidnight = function() {
        var now = dateService.getDateTimeNow();
        var nextMidnight = new Date(now).addDays(1).setHours(0, 0, 0, 0);
        return nextMidnight - now;
    };

    var getTimeoutForNextMidnightEvent = function() {
        var timeToMidnight = getTimeUntilNextMidnight();
        $log.debug("midnightEventService: calculating time to midnight as " + timeToMidnight + "ms at " + dateService.getDateTimeNow());
        return timeToMidnight + 1000; // Add 1s to ensure we're triggering in the next day not a few ms early
    };

    var onMidnight = function() {
        $log.debug("midnightEventService: triggering midnight event at " + dateService.getDateTimeNow());
        $rootScope.$broadcast(midnightEventName);
        timer = $timeout(onMidnight, getTimeoutForNextMidnightEvent());
    };

    timer = $timeout(onMidnight, getTimeoutForNextMidnightEvent());

    $rootScope.$on('$destroy', function() {
        $timeout.cancel(timer);
    });

    return {
        getMidnightEventName: function() {
            return midnightEventName;
        }
    };
});
