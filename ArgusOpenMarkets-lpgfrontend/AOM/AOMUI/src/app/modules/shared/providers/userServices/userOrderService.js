﻿angular.module('argusOpenMarkets.shared').service("userOrderService", function userOrderService(socketProvider,userService,helpers){
    "use strict";

    var sendEditOrderMessage = function(order){
        userService.sendStandardFormatMessageToUsersTopic( 'Order', 'Update', order );
    };

    var sendKillOrderMessage = function(order){
        userService.sendStandardFormatMessageToUsersTopic( 'Order', 'Kill', order );
    };

    var sendHoldOrderMessage = function(order){
        userService.sendStandardFormatMessageToUsersTopic( 'Order', 'Hold', order );
    };

    var sendReinstateOrderMessage = function(order){
        userService.sendStandardFormatMessageToUsersTopic( 'Order', 'Reinstate', order );
    };

    var sendExecuteOrderMessage = function(order){
        userService.sendStandardFormatMessageToUsersTopic( 'Order', 'Execute', order );
    };
    var sendNewOrderMessage = function(order){
        userService.sendStandardFormatMessageToUsersTopic( 'Order', 'Create', order );
    };

    var isOrderMine = function(order){
        var principalOrganisationId,
            brokerOrganisationId;
        if(order) {
            principalOrganisationId = order.principalOrganisationId;
            brokerOrganisationId = order.brokerOrganisationId;
            return userService.getUsersOrganisation().id === principalOrganisationId || userService.getUsersOrganisation().id === brokerOrganisationId;
        }
        return false;
    };

    var isOrderFromAnotherBroker = function(order){
        if (helpers.isEmpty(order.id)){
          return false;
        }
        else
        {
            return !!(userService.isBroker() && (!helpers.isEmpty(order.brokerOrganisationId)) && (order.brokerOrganisationId !== userService.getUsersOrganisation().id));
        }
    };

    return {
        sendKillOrderMessage:sendKillOrderMessage,
        isOrderMine:isOrderMine,
        isOrderFromAnotherBroker:isOrderFromAnotherBroker,
        sendNewOrderMessage:sendNewOrderMessage,
        sendExecuteOrderMessage : sendExecuteOrderMessage,
        sendEditOrderMessage:sendEditOrderMessage,
        sendHoldOrderMessage:sendHoldOrderMessage,
        sendReinstateOrderMessage:sendReinstateOrderMessage
    };

});