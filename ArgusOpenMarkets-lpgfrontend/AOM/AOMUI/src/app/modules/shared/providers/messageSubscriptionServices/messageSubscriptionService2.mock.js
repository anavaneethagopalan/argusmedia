﻿angular.module('argusOpenMarkets.shared')
    .service("mockMessageSubscriptionService2", function mockMessageSubscriptionService2() {
        "use strict";

        var ANYORNONE = "ANYORNONE";

        var messageTypeSubscriptions = {};
        var pathDeleteSubscriptions = [];

        var fetchData = function(topic){

        };

        // -- TOPIC UPDATES HANDLING --

        // Message received - may contain a messageType - route by path and messageType/ANYORNONE
        var mockOnMessage = function(topic,response)
        {
            if(true) {
                var messageType;
                var bHandled = false;

                response.topicPath = topic;

                try{
                    messageType = response.message.messageType;
                }catch(ex){
                    messageType = ANYORNONE;
                }

                for (var subscribedTopic in messageTypeSubscriptions) {
                    if( messageTypeSubscriptions.hasOwnProperty( subscribedTopic ) ) {

                        if(topic.indexOf(subscribedTopic)>-1){
                            var subscription = messageTypeSubscriptions[subscribedTopic][messageType];

                            if (subscription !== undefined && subscription.callback !== undefined)
                            {
                                bHandled = true;
                                try{
                                    subscription.callback(response);
                                }catch(ex){
                                    window.console.error(ex);
                                }
                            }

                            if (messageType !== ANYORNONE) {
                                subscription = messageTypeSubscriptions[subscribedTopic][ANYORNONE];

                                if (subscription !== undefined && subscription.callback !== undefined) {
                                    bHandled = true;
                                    try {
                                        subscription.callback(response);
                                    } catch (ex) {
                                        window.console.error(ex);
                                    }
                                }
                            }
                        }
                    }
                }
                if (!bHandled){
                    window.console.debug("MSS2 unhandled message being discarded: " + topic + " - " + messageType);
                }
            }
        };

        var subscribeToTopicAndMessageType = function( topic, messageType, callback, listenOnly){
            window.console.debug("MSS2 registering callback for message type '" + messageType + "' on: " + topic);
            var topicSubscription = messageTypeSubscriptions[topic];
            if (topicSubscription === undefined){
                if (listenOnly!==true) {
                    window.console.debug("  MSS2 subscribing to: " + topic);
                    //socketProvider.subscribe(topic);
                }
                //socketProvider.addTopicCallback(topic,onMessage);
                messageTypeSubscriptions[topic] = {};
            }
            messageTypeSubscriptions[topic][messageType]={"callback":callback};
        };

        var unsubscribeFromTopicAndMessageType = function(topic,messageType){
            var subscription = messageTypeSubscriptions[topic][messageType];
            if (subscription !== undefined) {
                window.console.debug("MSS2 unregistering callback for message type '" + messageType + "' on: " + topic);
                delete messageTypeSubscriptions[topic][messageType];
                if (Object.keys(messageTypeSubscriptions[topic]).length === 0) {
                    window.console.debug("  MSS2 unsubscribing from: " + topic);
                    delete messageTypeSubscriptions[topic];
                    unsubscribeAndRemoveListener(subscription.topic);
                }
            }
        };

        var unsubscribeAndRemoveListener = function(topic){
            // socketProvider.unsubscribe(topic);
            // socketProvider.removeTopicListener(topic);
        };

        var subscribeToTopicUsingPath = function( topic, callback, listenOnly ){
            subscribeToTopicAndMessageType( topic, ANYORNONE, callback, listenOnly );
        };

        var unsubscribeFromTopicUsingPath = function(topic){
            unsubscribeFromTopicAndMessageType( topic, ANYORNONE );
        };

        // -- TOPIC DELETION HANDLING --

        // Topic deletion received - route by path
        var onTopicDeletion = function( deletedPath ) {
            for(var i=pathDeleteSubscriptions.length-1;i>=0;i--)
            {
                if(deletedPath.indexOf(pathDeleteSubscriptions[i].topic)>-1){
                    var subscription = pathDeleteSubscriptions[i];

                    if (subscription.callback !== undefined)
                    {
                        subscription.callback(deletedPath);
                    }
                }
            }
        };

        //socketProvider.registerCallbackOnTopicDeleted(onTopicDeletion);

        var subscribeToTopicDeletedUsingPath = function( topic, callback ){
            window.console.debug("MSS2 registering callback for deletions on: " + topic);
            pathDeleteSubscriptions.push({"topic":topic,"callback":callback});
        };

        var unsubscribeFromTopicDeletedUsingPath = function(topic){
            for(var i=pathDeleteSubscriptions.length-1;i>=0;i--)
            {
                if(topic===pathDeleteSubscriptions[i].topic){
                    window.console.debug("MSS2 unregistering callback for deletions on: " + topic);
                    delete pathDeleteSubscriptions[i];
                }
            }
        };

        return{
            fetchData:fetchData,
            subscribeToTopicAndMessageType:subscribeToTopicAndMessageType,
            unsubscribeFromTopicAndMessageType:unsubscribeFromTopicAndMessageType,
            subscribeToTopicUsingPath:subscribeToTopicUsingPath,
            unsubscribeFromTopicUsingPath:unsubscribeFromTopicUsingPath,
            subscribeToTopicDeletedUsingPath:subscribeToTopicDeletedUsingPath,
            unsubscribeFromTopicDeletedUsingPath:unsubscribeFromTopicDeletedUsingPath,
            mockOnMessage:mockOnMessage
        };
    });


