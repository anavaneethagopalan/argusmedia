﻿describe('user service tests', function(){
    "use strict";

    var scope,
        userServiceTest;
    var mockMessageSubscriptionService2;

    var create_mock_promise = function (call_through, do_reject, mock_data) {
        return {
            then: function (resolve, reject) {
                if (!call_through) {
                    return this;
                } else {
                    if (do_reject) {
                        return _(reject).isFunction() ? reject(mock_data) : undefined;
                    } else {
                        return resolve(mock_data);
                    }

                }
            }
        };
    };

    var mockSocketProvider ={
        processMessage: null,
        subscribe:function(){return true;},
        connect:function(){},
        onConnect:{promise: create_mock_promise(true,false,null)},
        addTopicCallback:function(topicName,onMessage){ this.processMessage =onMessage },
        onMessage:function(message){this.processMessage(message)}
    };

    beforeEach(function () {
        module('argusOpenMarkets.shared');
    });

    beforeEach(function(){
        spyOn(mockSocketProvider ,"subscribe");
        var $injector = angular.injector([ 'argusOpenMarkets.shared' ]);
        mockMessageSubscriptionService2 = $injector.get( 'mockMessageSubscriptionService2' );
        spyOn(mockMessageSubscriptionService2 ,"subscribeToTopicUsingPath").and.callThrough();
        module(function($provide) {
            $provide.value('socketProvider', mockSocketProvider);
            $provide.value('messageSubscriptionService2', mockMessageSubscriptionService2);
        });
    });

    beforeEach(inject(function(userService){
        userServiceTest = userService;
        var user = {
            id: 11,
            fullName: "John Adams",
            role: "Trader",
            subscribedProducts:[{productId:1, userProductPrivileges:{Order_Create_Any:false}}],
            systemPrivileges:["TestFake"],
            userOrganisation:{organisationName:"ABC"},
            productPrivileges : [{productId:1, privileges:{Order_Create_Any:false,Order_Create_Test:false}},
                {productId:2, privileges:{Order_Create_Any:false}}]

        };
        userServiceTest.setUser(user);
    }));

    it('should have set the user', inject(function(){
        expect(userServiceTest.getUser().id).toEqual(11);
    }));

    it('should get the users role', inject(function(){
        expect(userServiceTest.getRole()).toEqual("Trader");
    }));

    it('should get the users id', inject(function(){
        expect(userServiceTest.getUserId()).toEqual(11);
    }));

    it('should get the users org', inject(function(){
        expect(userServiceTest.getUsersOrganisation().organisationName).toEqual("ABC");
    }));

    it('should get the user privileges', inject(function(){
        expect(userServiceTest.getSystemPrivileges()).toEqual(["TestFake"]);
    }));

    it('should get determine if the user has a role', inject(function(){
        expect(userServiceTest.hasProductPrivilege(1,["Order_Create_Any","Fake", "Fake"])).toEqual(true);
    }));

    it('should get determine if the user does not have a role', inject(function(){
        expect(userServiceTest.hasProductPrivilege(1,["Fake", "Fake"])).toEqual(false);
    }));

    it('should get determine if the user does not have a role', inject(function(userService){
        expect(userServiceTest.hasProductPrivilege(2,["Fake", "Fake"])).toEqual(false);
    }));

    it('should get true if the user does have a role on any product', inject(function(userService){
        expect(userServiceTest.hasProductPrivilegeOnAnyProduct(["Order_Create_Any", "Fake"])).toEqual(true);
    }));

    it('should get a list of all products with a privilege', inject(function(userService){
        expect(userServiceTest.getProductsWithPrivileges(["Order_Create_Any"])).toEqual([1,2]);
    }));

    it('should get a list of all products with a privilege', inject(function(userService){
        expect(userServiceTest.getProductsWithPrivileges(["Order_Create_Test"])).toEqual([1]);
    }));

    it('should get an empty list if no products with a privilege', inject(function(userService){
        expect(userServiceTest.getProductsWithPrivileges(["NONE"])).toEqual([]);
    }));

    it('should get false if the user does not have a role on any product', inject(function(userService){
        expect(userServiceTest.hasProductPrivilegeOnAnyProduct(["Fake", "Fake"])).toEqual(false);
    }));

    it('should get the subscribed products for a user', inject(function(userService){
        expect(userServiceTest.getSubscribedProductIds ()).toEqual([1,2]);
    }));

    it('should attempt to connect to user topic', inject(function(userService){
        expect(userServiceTest.connectToUserTopic());
        expect(mockMessageSubscriptionService2.subscribeToTopicUsingPath).toHaveBeenCalledWith('AOM/Users/11',jasmine.any(Function));
    }));

    it('should recieve data on user topic', inject(function(userService){
        expect(userServiceTest.connectToUserTopic());
        expect(mockMessageSubscriptionService2.subscribeToTopicUsingPath).toHaveBeenCalledWith('AOM/Users/11',jasmine.any(Function));
        var message = {records:[{fields:[JSON.stringify({a:"A"}).toString()]}]};
        message.displayFormat = function(){return "message"};
        mockMessageSubscriptionService2.mockOnMessage('AOM/Users/11',message);
    }));

    describe('Has Any Product Privilege', function(){

        var user;

        beforeEach(function() {

            user = {
                "id":3,
                "username":"t1",
                "name":"Trader One",
                "email":"user_1@tradeco_1.com",
                "productPrivileges":[
                    {
                        "productId":1,
                        "privileges":{
                            "priv1":true
                        }
                    },
                    {
                        "productId":2,
                        "privileges":{
                            "priv2":true,
                            "priv3":true
                        }
                    },
                    {
                        "productId":3,
                        "privileges":{
                            "priv4":true
                        }
                    },
                    {
                        "productId":4,
                        "privileges":{
                            "priv5":true,
                            "priv4":true
                        }
                    }
                ],
                "systemPrivileges":{
                    "privileges":[
                        "syspriv1",
                        "syspriv2"
                    ],
                    "defaultRoleGroup":null,
                    "organisationType":0
                }
            };
        });
        it('should return false for has product privilege if no product privileges exist', function(){

            var hasProductPrivilege,
                productPrivileges = ['priv1'];

            hasProductPrivilege = userServiceTest.hasProductPrivilege(1, productPrivileges);
            expect(hasProductPrivilege).toBeFalsy();
        });

        it('should return true for has product privilege if the privilege to check exists for the product', function(){
            var hasProductPrivilege,
                productPrivileges = ['priv1'];

            userServiceTest.setUser(user);

            hasProductPrivilege = userServiceTest.hasProductPrivilege(1, productPrivileges);
            expect(hasProductPrivilege).toBeTruthy();
        });

        it('should return true for has product privilege if testing multiple privileges', function(){
            var hasProductPrivilege,
                productPrivileges = ['priv2', 'priv3'];

            userServiceTest.setUser(user);

            hasProductPrivilege = userServiceTest.hasProductPrivilege(2, productPrivileges);
            expect(hasProductPrivilege).toBeTruthy();
        });

        it('should return true for has product privilege if testing multiple privileges but the user does not have all privileges requested', function(){

            var hasProductPrivilege,
                productPrivileges = ['priv2', 'priv3', 'priv5'];

            userServiceTest.setUser(user);

            hasProductPrivilege = userServiceTest.hasProductPrivilege(2, productPrivileges)
            expect(hasProductPrivilege).toBeTruthy();
        });

        it('should return false for has any any product privilege if none of the products for the user has the required privileges', function(){

            var hasProductPrivilege,
                productPrivileges = ['priv10', 'priv11'];

            userServiceTest.setUser(user);

            hasProductPrivilege = userServiceTest.hasAnyProductPrivileges([1, 2], productPrivileges);
            expect(hasProductPrivilege).toBeFalsy();
        });

        it('should return true for has any any product privilege if only one of the products has the required privilege', function(){

            var hasProductPrivilege;

            userServiceTest.setUser(user);

            hasProductPrivilege = userServiceTest.hasAnyProductPrivileges([1, 2], ['priv2']);
            expect(hasProductPrivilege).toBeTruthy();
        });

        it('should return true for has any any product privilege if both productgs have the required privilege', function(){

            var hasProductPrivilege,
                productPrivileges = ['priv10', 'priv11'];

            userServiceTest.setUser(user);

            hasProductPrivilege = userServiceTest.hasAnyProductPrivileges([3, 4], ['priv4']);
            expect(hasProductPrivilege).toBeTruthy();
        });

        it('should return false for has any product privileges if no products passed into method', function(){

            var hasProductPrivilege,
                productPrivileges = ['priv10'];

            userServiceTest.setUser(user);

            hasProductPrivilege = userServiceTest.hasAnyProductPrivileges([], ['priv4']);
            expect(hasProductPrivilege).toBeFalsy();
        });

        it('should return false for has any product privileges if no privileges passed into method', function(){

            var hasProductPrivilege,
                productPrivileges = ['priv10'];

            userServiceTest.setUser(user);

            hasProductPrivilege = userServiceTest.hasAnyProductPrivileges([1], []);
            expect(hasProductPrivilege).toBeFalsy();
        });
    });
});