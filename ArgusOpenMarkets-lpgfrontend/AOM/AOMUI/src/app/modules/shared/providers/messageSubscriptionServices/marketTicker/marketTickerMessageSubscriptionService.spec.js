describe('marketTickerMessageSubscriptionService', function() {
    'use strict';

    var mockSubsService = jasmine.createSpyObj(
        'messageSubscriptionService2',
        ['subscribeToTopicDeletedUsingPath', 'subscribeToTopicUsingPath', 'fetchData', 'subscribeToTopic']);

    var mockUserService;

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(function () {
        module(function ($provide) {
            var $injector = angular.injector(['argusOpenMarkets.shared']);
            mockUserService = $injector.get('mockUserService');
            $provide.value('messageSubscriptionService2', mockSubsService);
            $provide.value('userService', mockUserService);
        });
    });

    it('should have a defined service', inject(function(marketTickerMessageSubscriptionService) {
        expect(marketTickerMessageSubscriptionService).toBeDefined();
    }));

    it('should subscribe to market ticker topic deleted', inject(function(marketTickerMessageSubscriptionService) {
        expect(mockSubsService.subscribeToTopicDeletedUsingPath).toHaveBeenCalledWith(
            'AOM/MarketTicker/', jasmine.any(Function));
    }));
});
