﻿angular.module('argusOpenMarkets.shared')
    .service("newsMessageSubscriptionService", function newsMessageSubscriptionService(
        messageSubscriptionService2,
        marketTickerService,
        productConfigurationService) {
        "use strict";

        var newNewsAction,
            onNewsItemDeleted;

        this.registerNewsItemDeleted = function(onNewsDeleted){
            onNewsItemDeleted = onNewsDeleted;
        };

        var processFreeMessage = function (response) {
            newNewsAction(response);
        };

        var processMessage = function (response) {
            newNewsAction(response);
        };

        this.registerActionOnNews = function (action) {
            newNewsAction = action;
        };

        var handleNewsDeleted = function(topic) {

            var newsId,
                topicArray = topic.split("/");

            if (topicArray) {
                newsId = topicArray[topicArray.length - 1];
            }
            if (typeof onNewsItemDeleted !== "undefined") {
                onNewsItemDeleted(newsId);
            }
        };

        var subscribeToNewsTopics = function () {
            var contentStreams = [],
                commodityIds = [],
                commodityId;

            var allProductConfigs = productConfigurationService.getAllProductConfigurations();
            angular.forEach(allProductConfigs, function (productConfig) {

                commodityId = productConfig.commodity.id;
                if (commodityIds.indexOf(commodityId) < 0) {
                    commodityIds.push(commodityId);
                }

                angular.forEach(productConfig.contentStreamIds, function (contentStreamId) {
                    if (contentStreams.indexOf(contentStreamId) < 0) {
                        contentStreams.push(contentStreamId);
                    }
                });
            });

            angular.forEach(commodityIds, function (commodityId) {
                messageSubscriptionService2.subscribeToTopicAndMessageType("AOM/News/Free/" + commodityId + "/", "News", processFreeMessage);
                // messageSubscriptionService2.subscribeToTopicAndMessageType("AOM/News/Free/","News",processMessage);
            });

            angular.forEach(contentStreams, function (contentStream) {
                messageSubscriptionService2.subscribeToTopicAndMessageType("AOM/News/ContentStreams/" + contentStream + "/", "News", processMessage);
            });


            messageSubscriptionService2.subscribeToTopicDeletedUsingPath("AOM/News/", handleNewsDeleted);
        };

        marketTickerService.hasData.then(function () {
            subscribeToNewsTopics();
        });

        return {
            subscribeToNewsTopics: subscribeToNewsTopics,
            registerActionOnNews: this.registerActionOnNews,
            registerNewsItemDeleted: this.registerNewsItemDeleted
        };
    });
