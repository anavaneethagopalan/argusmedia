angular.module('argusOpenMarkets.shared').service("clientNotificationMessageService",
    function clientNotificationMessageService(userService,messagePopupsService,messageSubscriptionService2,socketProvider,$window) {
        "use strict";

        var onErrorEventHandler = function(userMessage){
            messagePopupsService.displayError(userMessage.message.messageType.toUpperCase(), userMessage.message.messageBody);
        };

        var onMessageEventHandler = function(userMessage){
            messagePopupsService.displayInfo("User Notification", userMessage.message.messageBody);
        };

        var onUserMessageEventHandler = function(userMessage){
            messagePopupsService.displayInfo("User Notification", userMessage.message.messageBody);
        };

        var onLogoffEventHandler = function(userMessage){
            $window.onbeforeunload = undefined;
            logout(userMessage);
        };

        socketProvider.registerConnectCallback(function(){
            messagePopupsService.displayInfo("User Notification", "Connection with server regained");
        },true);

        var topic = "AOM/Users/" + userService.getUserId();
        messageSubscriptionService2.subscribeToTopicAndMessageType(topic,"Error",onErrorEventHandler);
        messageSubscriptionService2.subscribeToTopicAndMessageType(topic,"Message",onMessageEventHandler);
        messageSubscriptionService2.subscribeToTopicAndMessageType(topic,"Logoff",onLogoffEventHandler);
        messageSubscriptionService2.subscribeToTopicAndMessageType(topic,"UserMessage",onUserMessageEventHandler);

        var unsubscribe = function(){
            messageSubscriptionService2.unsubscribeFromTopicAndMessageType(topic,"Error",onErrorEventHandler);
            messageSubscriptionService2.unsubscribeFromTopicAndMessageType(topic,"Message",onMessageEventHandler);
            messageSubscriptionService2.unsubscribeFromTopicAndMessageType(topic,"Logoff",onLogoffEventHandler);
            messageSubscriptionService2.unsubscribeFromTopicAndMessageType(topic,"UserMessage",onUserMessageEventHandler);
        };

        var logout= function(userMessage){
            unsubscribe();
            socketProvider.disconnect();
            userService.setUser(null);
            $window.location.href = $window.location.href + "?logoffMessage=loggedOutErrorMessagesConstant";
            $window.location.reload();
        };

        return {
            onErrorEventHandler:onErrorEventHandler,
            onMessageEventHandler:onMessageEventHandler,
            onUserMessageEventHandler:onUserMessageEventHandler,
            onLogoffEventHandler:onLogoffEventHandler,
            unsubscribe:unsubscribe,
            logout:logout
        };
    }
);


