describe( 'argusOpenMarkets.shared.productMetadataService', function() {
    'use strict';

    var productId = 1;
    var metadataDefinitions, productMetadataServiceToTest, mockProductConfigurationService;

    beforeEach(function(){
        metadataDefinitions = [{
            "metadataList": {
                "id": 3,
                "fieldList": [
                    {
                        "id": 1,
                        "displayName": "Port A",
                        "itemValue": 100
                    },
                    {
                        "id": 2,
                        "displayName": "Port B",
                        "itemValue": 101
                    },
                    {
                        "id": 3,
                        "displayName": "Port c",
                        "itemValue": 102
                    }
                ]
            },
            "id": 1,
            "productId": productId,
            "displayName": "Delivery Port",
            "fieldType": "IntegerEnum"
        }, {
            "valueMinimum": 0,
            "valueMaximum": 20,
            "id": 3,
            "productId": productId,
            "displayName": "Delivery Description",
            "fieldType": "String"
        }];
    });

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(function() {
        mockProductConfigurationService = {
            getProductMetaDataById: function(id){
                return { "productId": productId, "fields": metadataDefinitions };
            }
        };

        module(function ($provide) {
            $provide.value('productConfigurationService', mockProductConfigurationService);
        });
        inject(function (productMetadataService, helpers) {
            productMetadataServiceToTest = productMetadataService;
        });
    });

    it('Should generate default metadata values', function(){
        var expectedMetadataValues = [{
            productMetaDataId: 1,
            displayName: "Delivery Port",
            displayValue: "Port A",
            itemValue: 100,
            metaDataListItemId : 1,
            itemType: "IntegerEnum"
        }, {
            productMetaDataId: 3,
            displayName: "Delivery Description",
            displayValue: "",
            itemValue: null,
            metaDataListItemId : null,
            itemType: "String"
        }];

        var defaultValues = productMetadataServiceToTest.synchronizeMetadata([], productId);
        expect(defaultValues).toEqual(expectedMetadataValues);
    })

    it('Should generate metadata collection', function(){
        var order = {
            id: 100,
            price: 10,
            metaData: [{
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId: 1,
                itemType:"IntegerEnum"
            },{
                productMetaDataId:3,
                displayName:"Delivery Description",
                displayValue:"123",
                itemValue:null,
                metaDataListItemId:null,
                itemType:"String"
            }]
        };

        var metadataCollection = productMetadataServiceToTest.getMetadataCollection(order.metaData, productId);

        var expectedMetadataCollection = [
            { definition: metadataDefinitions[0], metadataitem: order.metaData[0]},
            { definition: metadataDefinitions[1], metadataitem: order.metaData[1] }
        ];

        expect(metadataCollection).toEqual(expectedMetadataCollection);
    });

    it('Should generate newly added metadata items with default values', function(){
        var existingMetadataValue = {
            productMetaDataId: 1,
            displayName: "Delivery Port",
            displayValue: "Port A",
            itemValue: 100,
            metaDataListItemId: 1,
            itemType:"IntegerEnum"
        };

        var order = {
            id: 100,
            price: 10,
            metaData: [ existingMetadataValue ]
        };

        order.metaData = productMetadataServiceToTest.synchronizeMetadata(order.metaData, productId);
        var defaultValue = {
            productMetaDataId:3,
            displayName:"Delivery Description",
            displayValue:"",
            itemValue:null,
            metaDataListItemId:null,
            itemType:"String"
        };
        expect(order.metaData).toEqual([ existingMetadataValue, defaultValue ]);
    });

    it('Should not synchronize removed metadata items', function(){
        var order = {
            id: 100,
            price: 10,
            metaData: [{
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId: 1,
                itemType:"IntegerEnum"
            },{
                productMetaDataId:3,
                displayName:"Delivery Description",
                displayValue:"123",
                itemValue:null,
                metaDataListItemId:null,
                itemType:"String"
            }]
        };

        mockProductConfigurationService.getProductMetaDataById = function(id){
            return { "productId": productId, "fields": [ metadataDefinitions[1] ] };
        };

        var syncMetadata = productMetadataServiceToTest.synchronizeMetadata(order.metaData, productId);
        expect(syncMetadata).toEqual([order.metaData[1]]);
    });

    it('Should synchronize metadata items in correct display order', function(){
        var order = {
            id: 100,
            price: 10,
            metaData: [{
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId: 1,
                itemType:"IntegerEnum"
            },{
                productMetaDataId:3,
                displayName:"Delivery Description",
                displayValue:"123",
                itemValue:null,
                metaDataListItemId:null,
                itemType:"String"
            }]
        };

        metadataDefinitions = metadataDefinitions.reverse();
        var syncMetadata = productMetadataServiceToTest.synchronizeMetadata(order.metaData, productId);
        expect(syncMetadata).toEqual([order.metaData[1], order.metaData[0]]);
    })

    it('Should generate default metadata values if items type was changed', function(){
        var order = {
            id: 100,
            price: 10,
            metaData: [{
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId: 1,
                itemType:"IntegerEnum"
            },{
                productMetaDataId:3,
                displayName:"Delivery Description",
                displayValue:"123",
                itemValue:null,
                metaDataListItemId:null,
                itemType:"String"
            }]
        };

        metadataDefinitions = metadataDefinitions.reverse();
        var syncMetadata = productMetadataServiceToTest.synchronizeMetadata(order.metaData, productId);
        expect(syncMetadata).toEqual([order.metaData[1], order.metaData[0]]);
    })

    it('Should synchronize metadata item display name', function () {

        var existingMetadataValue = {
            productMetaDataId: 1,
            displayName: "Obsolete display name",
            displayValue: "Port A",
            itemValue: 100,
            metaDataListItemId: 1,
            itemType:"IntegerEnum"
        };

        var order = {
            id: 100,
            price: 10,
            metaData: [ existingMetadataValue ]
        };

        order.metaData = productMetadataServiceToTest.synchronizeMetadata(order.metaData, productId);
        expect(order.metaData[0].displayName).toBe("Delivery Port");
    });

    it('Should synchronize metadata item display value and item value', function () {

        var existingMetadataValue = {
            productMetaDataId: 1,
            displayName: "Delivery Port",
            displayValue: "Obsolete display value",
            itemValue: 123,
            metaDataListItemId: 1,
            itemType:"IntegerEnum"
        };

        var order = {
            id: 100,
            price: 10,
            metaData: [ existingMetadataValue ]
        };

        order.metaData = productMetadataServiceToTest.synchronizeMetadata(order.metaData, productId);
        expect(order.metaData[0].displayValue).toBe("Port A");
        expect(order.metaData[0].itemValue).toBe(100);
    });

    it('Should set first metadata list item if appropriate item was not found', function () {

        var existingMetadataValue = {
            productMetaDataId: 1,
            displayName: "Delivery Port",
            displayValue: "Unknown list item",
            itemValue: 123,
            metaDataListItemId: 123,
            itemType:"IntegerEnum"
        };

        var order = {
            id: 100,
            price: 10,
            metaData: [ existingMetadataValue ]
        };

        var expectedMetadataValue = {
            productMetaDataId: 1,
            displayName: "Delivery Port",
            displayValue: "Port A",
            itemValue: 100,
            metaDataListItemId: 1,
            itemType:"IntegerEnum"
        };

        order.metaData = productMetadataServiceToTest.synchronizeMetadata(order.metaData, productId);
        expect(order.metaData[0]).toEqual(expectedMetadataValue);
    });
});
