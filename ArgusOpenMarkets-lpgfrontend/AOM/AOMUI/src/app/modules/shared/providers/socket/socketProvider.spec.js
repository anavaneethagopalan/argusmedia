﻿describe('SocketProviderUnitTests', function () {
    "use strict";

    var create_mock_promise = function (call_through, do_reject, mock_data) {
        return {
            then: function (resolve, reject) {
                if (!call_through) {
                    return this;
                } else {
                    if (do_reject) {
                        return _(reject).isFunction() ? reject(mock_data) : undefined;
                    } else {
                        return resolve(mock_data);
                    }
                }
            }
        };
    };

	var provider,
        _connectionDetails,
        mockDiffusionClient = {
            isConnected: function(connectionDetails, credentials) { return false; },
            subscribe:function(topic){return true;},
            send:function(topic,message){return true;},
            close:function(){return true;},
            onData:null,
            connect: function(onData,connectionDetails){
                onData = onData;
                _connectionDetails = connectionDetails;
            }
        },
        mockDiffusionProvider = {
            getDiffusionClient : function(){
            return mockDiffusionClient;
            }
        },
        mockDiffusionConfigurationService = {
            getDiffusionConfiguration: function () {
                return {
                    "endpoint": "localhost",
                    "port": "8080",
                    "wsURL": 'ws://localhost:8080',
                    "debug": false
                };
            },
            promise:function() {
                return create_mock_promise(true,false,true);
            }
        },
        mockUserCredentials= {
                    username: 'nathan',
                    password: 'password'
        };


    beforeEach(module('toastr'));
    beforeEach(module('argusOpenMarkets.shared'));




    beforeEach(function() {
        module(function ($provide) {
            // Tell angular to inject my mock instead of the read diffusion provider.
            spyOn(mockDiffusionClient, 'connect');
            $provide.value('diffusionProvider', mockDiffusionProvider);
            $provide.value('diffusionConfigurationService', mockDiffusionConfigurationService);
            $provide.value('$modal', null);


        });

    });

    beforeEach(inject(function(socketProvider){
        provider = socketProvider;
    }));

    it('should return an endpoint for the socket provider', function(){

        provider.onConnect.promise.then(function() {
            var socketEndpoint = provider.socketEndpoint(),
                expectedSocketEndpoint = 'localhost';

            expect(socketEndpoint).toEqual(expectedSocketEndpoint);
        });
    });

    it('should contain a socket provider instance', function(){
        var client = provider.getClient();
        expect(client).not.toBeNull();
    });

    it('should call socket provider isConnected', function(){
        var isConnected;
        spyOn(mockDiffusionClient, 'isConnected');
        isConnected = provider.isConnected();
        expect(mockDiffusionClient.isConnected).toHaveBeenCalled();
    });


    it('should call socket provider send Message with topic and a message containing the topic you want to subscribe to', function(){
        spyOn(mockDiffusionClient, 'send');
        provider.subscribe("Test");
        expect(mockDiffusionClient.send).toHaveBeenCalledWith('AOM/Users','{"messageAction":"CREATE","messageType":"TopicSubscription","messageBody":{"topicSubscribe":"Test","topicUnsubscribe":""}}');
    });

    it('should call send on the diffusion client on send', function(){
        spyOn(mockDiffusionClient, 'send');
        provider.sendMessage("Topic","Test");
        expect(mockDiffusionClient.send).toHaveBeenCalledWith("Topic","Test");
    });

    it('should call close on the diffusion client on disconnect', function(){
        spyOn(mockDiffusionClient,'isConnected').and.returnValue(true);
        spyOn(mockDiffusionClient, 'close');
        provider.disconnect();
        expect(mockDiffusionClient.close).toHaveBeenCalled();
    });

    it('should default false for isConnected', inject(function(socketProvider){
        expect(socketProvider.isConnected()).toEqual(false);
    }));

    it('should pass connection details to diffusion client using the default topicRoot', function(){
        var expectedDiffusionClientConnection = {
            serverHost: 'localhost:8080',
            wsURL: 'ws://lcoalhost:8080',
            flashURL: 'localhost:8080',
            serverPort: '8080',
            flashPort: '8080',
            debug: false
        };

        provider.connect(null, null, null);
        expect(mockDiffusionClient.connect).toHaveBeenCalled();

    });

    it('should set the onData function on connect', function(){
        var expectedDiffusionClientConnection = {
            serverHost: 'localhost:8080',
            wsURL: 'ws://lcoalhost:8080',
            flashURL: 'localhost:8080',
            serverPort: '8080',
            flashPort: '8080',
            debug: false
        };

        var testdatafunc = function(){return "TestData"};
        provider.connect(testdatafunc, null, null);
        expect(mockDiffusionClient.connect).toHaveBeenCalled();
    });
});