angular.module('argusOpenMarkets.shared')
    .service("organisationDetailsService", function organisationDetailsService(
        $q,
        messageSubscriptionService2,
        userService,
        $rootScope)
    {
        "use strict";

        var companyEventNotifications = [];
        var notificationTypes = [];
        var orgId = userService.getUsersOrganisation().id;
        var systemNotificationTopic = "AOM/Organisations/" + orgId + "/SystemNotifications/";
        var systemNotificationTypeTopic = "AOM/Organisations/" + orgId + "/SystemNotificationTypes/";

        var alertSubscribers = function(){
            $rootScope.$broadcast("CompanyEventNotifications");
        };

        var EventNotifications = function(_eventType,mappings) {
            this.mappings = mappings;
            this.eventType = _eventType;
        };

        messageSubscriptionService2.subscribeToTopicUsingPath( systemNotificationTopic, onNewSystemNotification, false);
        messageSubscriptionService2.subscribeToTopicUsingPath( systemNotificationTypeTopic, onNewNotificationType, false);
        messageSubscriptionService2.subscribeToTopicDeletedUsingPath(systemNotificationTopic, onSystemNotificationDeleted);

        function onSystemNotificationDeleted(deletedTopic){
            var topicDetails = deletedTopic.split('/');
            var notificationType = topicDetails[topicDetails.indexOf("SystemNotifications") + 1];
            var notificationId = topicDetails[topicDetails.indexOf("SystemNotifications") + 2];
            if(companyEventNotifications.check(notificationType)){
                var eventTypeIndex = companyEventNotifications.getIndexBy("eventType",parseInt(notificationType));
                var indexOfNotification = companyEventNotifications[eventTypeIndex].mappings.getIndexBy("id",parseInt(notificationId));
                companyEventNotifications[eventTypeIndex].mappings.splice(indexOfNotification,1);
            }
            alertSubscribers();
        }

        function onNewSystemNotification(notification){
            var notificationId = notification.id;
            var eventTypeIndex = companyEventNotifications.getIndexBy("eventType",notification.eventType);
            if(typeof(eventTypeIndex)==='undefined'){
                companyEventNotifications.push(new EventNotifications(notification.eventType,[notification]));
            }
            else {
                var indexOfNotification = companyEventNotifications[eventTypeIndex].mappings.getIndexBy('id', notificationId);
                if (indexOfNotification){
                    companyEventNotifications[eventTypeIndex].mappings[indexOfNotification] = notification;
                }
                else{
                    companyEventNotifications[eventTypeIndex].mappings.push(notification);
                }
            }
            alertSubscribers();
        }


        var getMappings = function(){
            for (var i=0;i<companyEventNotifications.length;i++){
                var index = notificationTypes.getIndexBy("id",companyEventNotifications[i].eventType);
                if(index > -1) {
                    companyEventNotifications[i].name = notificationTypes[index].description;
                }
            }
            return companyEventNotifications;
        };

        function onNewNotificationType(message){
            var notificationType = message;
            var eventTypeIndex = companyEventNotifications.getIndexBy("eventType",notificationType.id);
            if(typeof(eventTypeIndex)==='undefined'){
                companyEventNotifications.push(new EventNotifications(notificationType.id,[]));
            }
            notificationTypes.push({
                id: notificationType.id,
                description: notificationType.description
            });
            alertSubscribers();
        }

        var getMappingCategories = function(){
            return notificationTypes;
        };

        var sendNewMapping = function(mapping){
            userService.sendStandardFormatMessageToUsersTopic(
                'OrganisationNotificationDetail',
                'Create',
                mapping
            );
        };

        return {
            getMappings:getMappings,
            getMappingCategories: getMappingCategories,
            sendNewMapping:sendNewMapping
        };
    });