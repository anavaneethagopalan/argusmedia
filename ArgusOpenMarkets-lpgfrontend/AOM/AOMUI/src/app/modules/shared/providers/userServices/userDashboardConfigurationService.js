﻿angular.module('argusOpenMarkets.shared').service("userDashboardConfigurationService",
    function userDashboardConfigurationService($http,
                                               cookieService,
                                               userService,
                                               $q) {

        var userConfig = null;
        var cookieName = "aom.argusmedia.com.usertiles";
        var deferred = $q.defer();
        var promise = deferred.promise;

        $http.get('assets/userDashboardConfig.json').success(function (data) {
            var userConfigCookieValue = cookieService.readCookie(cookieName);
            if (userConfigCookieValue !== null) {
                userConfig = JSON.parse(userConfigCookieValue);
                if(userConfigForLoggedInUser(userConfig)) {
                    deferred.resolve();
                } else{
                    userService.userMessagePromise.promise.then(function () {
                        userConfig = getUserDashboardConfig();
                        deferred.resolve();
                    });
                }
            } else {
                userService.userMessagePromise.promise.then(function () {
                    // WE ARE MAKING THE CONFIG FOR THE USER.....
                    userConfig = getUserDashboardConfig();
                    deferred.resolve();
                    // if (!userConfigForLoggedInUser(userConfig)) {
                    //     // This is not this users dashboard configuration - use the default.
                    //     userService.userMessagePromise.promise.then(function () {
                    //         userConfig = getUserDashboardConfig();
                    //         deferred.resolve();
                    //     });
                    // } else {
                    //     deferred.resolve();
                    // }
                });
            }
        });

        var userConfigForLoggedInUser = function (userConfig) {
            var configForLoggedInUser = false,
                loggedInUserId = userService.getUserId();

            if (userConfig) {
                if (userConfig.userId && userConfig.userId === loggedInUserId) {
                    configForLoggedInUser = true;
                } else {
                    for (var i = 0; i < userConfig.length; i++) {
                        if (userConfig[i] && userConfig[i].userId && userConfig[i].userId === loggedInUserId) {
                            configForLoggedInUser = true;
                            break;
                        }
                    }
                }
            }

            return configForLoggedInUser;
        };


        var getUserDashboardConfig = function () {

            var subscribedProductIds = userService.getSubscribedProductIdsByDisplayOrder(),
                productIds = [],
                userConfig = [];

            if (subscribedProductIds && subscribedProductIds.length) {
                userConfig.push(makeDashboardConfig("assessments", subscribedProductIds, "default"));
                userConfig.push(makeDashboardConfig("marketTicker", subscribedProductIds, "default"));
                userConfig.push(makeDashboardConfig("newsFeed", subscribedProductIds, "default"));

                angular.forEach(subscribedProductIds, function (productId) {

                    productIds = [];
                    productIds.push(productId);
                    userConfig.push(makeDashboardConfig("bidAskStack", productIds, "default"));
                });
            }

            userConfig.userId = userService.getUserId();
            return userConfig;
        };

        var makeDashboardConfig = function (tileType, subscribedProductIds, colValue) {
            return {
                "productIds": subscribedProductIds,
                "displayOrder": 0,
                "tile": {
                    "tileType": tileType,
                    "position": {"row": "default", "col": colValue},
                    "size": {"x": "default", "y": "default"}
                }
            };
        };

        var viewWidgetPermissions = {
                "BidAskStack": "View_BidAsk_Widget",
                "Assessments": "View_Assessment_Widget",
                "MarketTicker": "View_MarketTicker_Widget",
                "NewsFeed": "View_NewsAnalysis_Widget"
            },
            widgetNames = {
                "BidAskStack": "bidAskStack",
                "Assessments": "assessments",
                "MarketTicker": "marketTicker",
                "NewsFeed": "newsFeed"
            };


        var getUserConfiguration = function () {
            return userConfig;
        };

        var getWidgetPermissionName = function (widgetName) {

            switch (widgetName) {

                case widgetNames.Assessments:
                    return viewWidgetPermissions.Assessments;

                case widgetNames.MarketTicker:
                    return viewWidgetPermissions.MarketTicker;

                case widgetNames.NewsFeed:
                    return viewWidgetPermissions.NewsFeed;
            }
        };

        var canViewWidget = function (item) {

            var canViewWidget = false,
                counter,
                widgetPermissionName;

            switch (item.view) {
                case widgetNames.BidAskStack:
                    if (item.productIds) {
                        var productId = item.productIds[0];
                        canViewWidget = userService.hasProductPrivilege(productId, [viewWidgetPermissions.BidAskStack]);
                    }
                    break;

                case widgetNames.Assessments:
                case widgetNames.MarketTicker:
                case widgetNames.NewsFeed:
                    if (item.productIds) {

                        widgetPermissionName = getWidgetPermissionName(item.view);
                        for (counter = 0; counter < item.productIds.length; counter++) {
                            canViewWidget = userService.hasProductPrivilegeOnAnyProduct([widgetPermissionName]);
                            if (canViewWidget) {
                                break;
                            }
                        }
                    }
                    break;
            }

            return canViewWidget;
        };

        var saveUserConfiguration = function (value) {

            if(value !== null){
                if(value[0] !== null){
                    value[0].userId = userService.getUserId();
                }
            }
            userConfig = value;
            cookieService.createCookie(JSON.stringify(value), cookieName);
        };

        return {
            promise: promise,
            getUserDashboardConfig: getUserDashboardConfig,
            getUserConfiguration: getUserConfiguration,
            saveUserConfiguration: saveUserConfiguration,
            canViewWidget: canViewWidget
        };
    });