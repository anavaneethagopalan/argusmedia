angular.module('argusOpenMarkets.shared').service("mockUserService", function mockUserService() {
    "use strict";

    var mock_user_ae1 = {
        "id": 7777,
        "username": "ae1",
        "name": "Argus Editor One",
        "email": "ae1@argusmedia.com",
        "organisationId": 1,
        "isBlocked": false,
        "isActive": true,
        "isDeleted": false,
        "token": null,
        "telephone": null,
        "title": null,
        "argusCrmUsername": null,
        "userOrganisation": {
            "id": 1,
            "name": "Argus Media",
            "organisationType": "Argus",
            "shortCode": null,
            "legalName": "Argus Media Ltd",
            "address": "Argus House, 175 St John Street, London EC1V 4LW",
            "dateCreated": "2014-01-01T00:00:00Z",
            "description": "Argus Media Company",
            "isDeleted": false
        },
        "systemPrivileges": {
            "privileges": {}
        },
        "productPrivileges": [
            {
                "productId": 1,
                "privileges": {
                    "deal_Amend_All": false,
                    "deal_Create_All": false,
                    "view_BidAsk_Widget": false,
                    "assessment_Create": false,
                    "marketTicker_Authenticate": false,
                    "view_MarketTicker_Widget": false,
                    "view_NewsAnalysis_Widget": false,
                    "view_Assessment_Widget": false,
                    "product_Change_Market_Status": false,
                    "deal_Authenticate": false,
                    "marketTicker_Create": false
                }
            },
            {
                "productId": 2,
                "privileges": {
                    "deal_Create_All": false,
                    "assessment_Create": false,
                    "view_NewsAnalysis_Widget": false,
                    "marketTicker_Authenticate": false,
                    "deal_Authenticate": false,
                    "marketTicker_Create": false,
                    "deal_Amend_All": false,
                    "view_BidAsk_Widget": false,
                    "product_Change_Market_Status": false,
                    "view_Assessment_Widget": false,
                    "view_MarketTicker_Widget": false
                }
            },
            {
                "productId": 3,
                "privileges": {
                    "marketTicker_Authenticate": false,
                    "assessment_Create": false,
                    "deal_Create_All": false,
                    "product_Change_Market_Status": false,
                    "view_Assessment_Widget": true
                }
            }
        ],
        "userCredentials": null
    };
    var mock_user_t1 = {
        "id": 8888,
        "username": "t1",
        "name": "Trader One",
        "email": "user_1@tradeco_1.com",
        "organisationId": 3,
        "isBlocked": false,
        "isActive": true,
        "isDeleted": false,
        "token": null,
        "telephone": null,
        "title": null,
        "argusCrmUsername": null,
        "userOrganisation": {
            "id": 3,
            "name": "TradeCo #1",
            "organisationType": "Trading",
            "shortCode": "TR1",
            "legalName": "TradeCo #1 Ltd",
            "address": "",
            "dateCreated": "2014-01-01T00:00:00Z",
            "description": "",
            "isDeleted": false
        },
        "systemPrivileges": {
            "privileges": ["CPMatrix_Update","BPMatrix_Update"]
        },
        "productPrivileges": [
            {
                "productId": 1,
                "privileges": {
                    "order_Amend_Kill": false,
                    "view_Assessment_Widget": false,
                    "marketTicker_Create": false,
                    "view_BidAsk_Widget": false,
                    "view_MarketTicker_Widget": false,
                    "view_NewsAnalysis_Widget": false,
                    "order_Amend_Own": false,
                    "order_Create_Principal": false,
                    "order_Aggress_Principal": false,
                    "order_Amend_OwnOrg": false,
                    "deal_Create_Principal": false
                }
            },
            {
                "productId": 2,
                "privileges": {
                    "order_Create_Principal": false,
                    "order_Amend_Kill": false,
                    "order_Aggress_Principal": false,
                    "view_NewsAnalysis_Widget": false,
                    "marketTicker_Create": false,
                    "view_MarketTicker_Widget": false,
                    "view_BidAsk_Widget": false,
                    "order_Amend_OwnOrg": false,
                    "view_Assessment_Widget": false,
                    "order_Amend_Own": false
                }
            }
        ],
        "userCredentials": null
    };

    var mock_user_b1 = {
        "id": 9999,
        "username": "b1",
        "name": "Broker One",
        "email": "broker.one@broker_1.com",
        "organisationId": 5,
        "isBlocked": false,
        "isActive": true,
        "isDeleted": false,
        "token": null,
        "telephone": null,
        "title": null,
        "argusCrmUsername": null,
        "userOrganisation": {
            "id": 5,
            "name": "BrokerCo #1",
            "organisationType": "Brokerage",
            "shortCode": "BR1",
            "legalName": "BrokerCo #1 Ltd",
            "address": "",
            "dateCreated": "2014-01-01T00:00:00Z",
            "description": "",
            "isDeleted": false
        },
        "systemPrivileges": {
            "privileges": ["CPMatrix_Update","BPMatrix_Update"]
        },
        "productPrivileges": [
            {
                "productId": 1,
                "privileges": {
                    "deal_Create_Broker": false,
                    "order_Amend_Own": false,
                    "view_NewsAnalysis_Widget": false,
                    "order_Create_Broker": false,
                    "order_Amend_OwnOrg": false,
                    "order_Aggress_Broker": false,
                    "view_MarketTicker_Widget": false,
                    "marketTicker_Create": false,
                    "view_BidAsk_Widget": false,
                    "order_Amend_Kill": false,
                    "view_Assessment_Widget": false
                }
            },
            {
                "productId": 2,
                "privileges": {
                    "order_Amend_Own": false,
                    "view_Assessment_Widget": false,
                    "order_Create_Broker": false,
                    "view_BidAsk_Widget": false,
                    "order_Amend_OwnOrg": false,
                    "deal_Create_Broker": false,
                    "order_Aggress_Broker": false,
                    "view_MarketTicker_Widget": false,
                    "marketTicker_Create": false,
                    "order_Amend_Kill": false,
                    "view_NewsAnalysis_Widget": false
                }
            },
            {
                "productId": 3,
                "privileges": {
                    "order_Amend_Own": false,
                    "view_Assessment_Widget": true,
                    "order_Create_Broker": false,
                    "view_BidAsk_Widget": false,
                    "order_Amend_OwnOrg": false,
                    "deal_Create_Broker": false,
                    "order_Aggress_Broker": false,
                    "view_MarketTicker_Widget": false,
                    "marketTicker_Create": false,
                    "order_Amend_Kill": false,
                    "view_NewsAnalysis_Widget": false
                }
            }
        ]
    };

    var user = null;

    var getUser = function () {
        return user;
    };

    var setUser = function (usr) {
        user = usr;
    };

    var getSystemPrivileges = function () {
        return  user.systemPrivileges;
    };

    var getUserId = function () {
        return user.id;
    };

    var getRole = function () {
        return user.role;
    };

    var getUsersOrganisation = function () {
        return user.userOrganisation;
    };

    var isBroker = function () {
        return getUsersOrganisation().organisationType === "Brokerage";
    };

    var getSubscribedProducts = function () {
        return user.subscribedProducts;
    };

    var getSubscribedProductIds = function () {
        var productIds = [];

        angular.forEach(user.productPrivileges, function (productPrivilege) {
            productIds.push(productPrivilege.productId);
        });

        return productIds;
    };

    var getProductPrivileges = function (productId) {
        var productPrivileges = {};
        angular.forEach(user.productPrivileges, function (product) {
            if (product.productId === productId) {
                productPrivileges = product.privileges;
            }
        });
        return productPrivileges;
    };

    var hasProductPrivilege = function (productId, privilegeNameArray) {
        var productPrivileges = getProductPrivileges(productId);
        for (var k in productPrivileges) {
            if (k) {
                for (var i = 0; i < privilegeNameArray.length; i++) {
                    if (k.toUpperCase() === privilegeNameArray[i].toUpperCase()) {
                        return true;
                    }
                }
            }
        }
        return false;
    };

    var hasAnyProductPrivileges = function(productIds, privilegeNameArray){

        var counter,
            hasPrivilege;

        if(!productIds){
            return false;
        }

        if(!privilegeNameArray){
            return false;
        }

        for(counter = 0; counter <= productIds.length; counter++) {
            hasPrivilege = hasProductPrivilege(productIds[counter], privilegeNameArray);
            if(hasPrivilege) {
                return true;
            }
        }
        return false;
    };

    var hasProductPrivilegeOnAnyProduct = function (privilegeNameArray) {
        var hasPriv = false;
        angular.forEach(getSubscribedProductIds(), function (productId) {
            if (hasProductPrivilege(productId, privilegeNameArray)) {
                hasPriv = true;
            }
        });
        return hasPriv;
    };

    var getProductsWithPrivileges = function (privileges) {
        var result = [];
        angular.forEach(getSubscribedProductIds(), function (productId) {
            if (hasProductPrivilege(productId, privileges)) {
                result.push(productId);
            }
        });
        return result;
    };

    var userMessagePromise = "userMessagePromise";

    var sendStandardFormatMessageToUsersTopic = function (messageType, messageAction, bodyObject) {
        window.console.log('mock send message: ' + messageType + '\n' + messageAction + '\n' + bodyObject);
    };

    var connectToUserTopic = function () {
        window.console.log('Subcribing to User topic');
    };

    var sessionToken;

    var getSessionToken = function () {
        return this.sessionToken;
    };

    var setSessionToken = function (sessionToken) {
        this.sessionToken = sessionToken;
    };

    // set user to T1 by default;
    setUser(mock_user_t1);

    return{
        MOCK_USER_T1: mock_user_t1,
        MOCK_USER_B1: mock_user_b1,
        MOCK_USER_AE1: mock_user_ae1,
        getUser: getUser,
        getSystemPrivileges: getSystemPrivileges,
        getUserId: getUserId,
        getSubscribedProducts: getSubscribedProducts,
        getRole: getRole,
        getUsersOrganisation: getUsersOrganisation,
        setUser: setUser,
        hasProductPrivilege: hasProductPrivilege,
        getSubscribedProductIds: getSubscribedProductIds,
        userMessagePromise: userMessagePromise,
        connectToUserTopic: connectToUserTopic,
        hasProductPrivilegeOnAnyProduct: hasProductPrivilegeOnAnyProduct,
        hasAnyProductPrivileges: hasAnyProductPrivileges,
        isBroker: isBroker,
        getProductsWithPrivileges: getProductsWithPrivileges,
        sendStandardFormatMessageToUsersTopic: sendStandardFormatMessageToUsersTopic,
        getSessionToken: getSessionToken,
        setSessionToken: setSessionToken
    };
});