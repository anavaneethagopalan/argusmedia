describe('News Message Subscription service tests', function () {
    "use strict";;

    var mockMessageSubscriptionService2,
        mockMarketTickerService = { hasData: function( ){ return { then: function(){}}}},
        mockProductConfigurationService = { getAllProductConfigurations: function(){
            return [
                {
                    "productId": 1,
                    "productName": "Naphtha CIF NWE - Cargoes",
                    "productTitle": "Naphtha CIF NWE - Cargoes",
                    "isInternal": false,
                    "hasAssessment": true,
                    "status": 1,
                    "commodity": {
                        "id": 1,
                        "name": "Naphtha",
                        "enteredByUserId": 0,
                        "lastUpdatedUserId": 0,
                        "dateCreated": "0001-01-01T00:00:00Z",
                        "lastUpdated": "0001-01-01T00:00:00Z"
                    },
                    "contractSpecification": {
                        "volume": {
                            "default": 17500,
                            "minimum": 12500,
                            "maximum": 55000,
                            "increment": 500,
                            "decimalPlaces": 0,
                            "volumeUnitCode": "t",
                            "commonQuantities": [
                                30000,
                                12500,
                                35000,
                                17500,
                                25000
                            ]
                        },
                        "pricing": {
                            "minimum": 1,
                            "maximum": 99999,
                            "increment": 0.25,
                            "decimalPlaces": 2,
                            "pricingUnitCode": "t",
                            "pricingCurrencyCode": "$"
                        },
                        "tenors": [
                            {
                                "deliveryLocation": {
                                    "id": 1,
                                    "name": "Rotterdam",
                                    "dateCreated": "2014-01-01T00:00:00Z"
                                },
                                "deliveryStartDate": "+1d",
                                "deliveryEndDate": "+20d",
                                "rollDate": "DAILY",
                                "id": 1,
                                "minimumDeliveryRange": 3
                            }
                        ],
                        "contractType": "Physical"
                    },
                    "bidAskStackNumberRows": 6,
                    "allowNegativePriceForExternalDeals": false,
                    "displayOptionalPriceDetail": false,
                    "contentStreamIds": [
                        95028,
                        95508,
                        95525
                    ]
                }];
        }},
        newsMessageSubscriptionServiceTest,
        deferred;

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
    });

    beforeEach(function () {
        module(function ($provide) {
            var $injector = angular.injector(['argusOpenMarkets.shared']);
            mockMessageSubscriptionService2 = $injector.get('mockMessageSubscriptionService2');
            spyOn(mockMessageSubscriptionService2,'subscribeToTopicAndMessageType').and.callThrough();

            $provide.value('messageSubscriptionService2', mockMessageSubscriptionService2);
            $provide.value('marketTickerService', mockMarketTickerService);
            $provide.value('productConfigurationService', mockProductConfigurationService);
        });
    });

    beforeEach(inject(function($q) {
        deferred = $q.defer();
        spyOn(mockMarketTickerService, 'hasData').and.returnValue(deferred);
        mockMarketTickerService.hasData = deferred.promise;
    }));

    it('to be defined', inject(function (newsMessageSubscriptionService) {
        expect(newsMessageSubscriptionService).toBeDefined();
    }));

    it('should call the message subscription service to subscribe for free news with a commodity id', inject(function (newsMessageSubscriptionService){

        deferred.resolve();
        newsMessageSubscriptionService.subscribeToNewsTopics();
        expect(mockMessageSubscriptionService2.subscribeToTopicAndMessageType).toHaveBeenCalledWith('AOM/News/Free/1/', 'News', jasmine.any(Function) );
    }));


});



