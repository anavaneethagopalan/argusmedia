angular.module('argusOpenMarkets.shared').
service("productMetadataService", function productMetadataService(productConfigurationService) {
    "use strict";

    var createDefaultMetadataValue = function(definition){
        var metadataValue = {
            productMetaDataId: definition.id,
            displayName: definition.displayName,
            displayValue: null,
            itemValue: null,
            metaDataListItemId : null,
            itemType : null
        };

        if (definition.fieldType === "String"){
            metadataValue.displayValue = "";
            metadataValue.itemType = definition.fieldType;

        } else if (definition.fieldType === "IntegerEnum"){
            if (definition.metadataList !== null && definition.metadataList.fieldList.length > 0){
                var firstField = definition.metadataList.fieldList[0];
                metadataValue.metaDataListItemId = firstField.id;
                metadataValue.displayValue = firstField.displayName;
                metadataValue.itemValue = firstField.itemValue;
                metadataValue.itemType = definition.fieldType;
            }
        }

        return metadataValue;
    };

    this.getMetadataCollection = function(metadataValues, productId){
        var metadataCollection = [];
        var metadataDefinitions = angular.copy(productConfigurationService.getProductMetaDataById(productId));
        var metadataPredicate = function(x){ return x.productMetaDataId === definition.id; };
        if (metadataDefinitions && metadataValues) {
            for (var i = 0; i < metadataDefinitions.fields.length; i++) {
                var definition = metadataDefinitions.fields[i];
                var metaDataItem = metadataValues.firstOrDefault(metadataPredicate);
                if (metaDataItem){
                    metadataCollection.push({definition: definition, metadataitem: metaDataItem});
                }
            }
        }

        return metadataCollection;
    };
    
    this.synchronizeMetadata = function (metadataValues, productId) {
        if (typeof metadataValues === "undefined" || metadataValues === null){
            metadataValues = [];
        }

        var newMetadataValues = [];
        var metadataDefinitions = angular.copy(productConfigurationService.getProductMetaDataById(productId));
        var metadataPredicate = function(x){ return x.productMetaDataId === definition.id; };
        if (metadataDefinitions) {
            for (var i = 0; i < metadataDefinitions.fields.length; i++) {
                var definition = metadataDefinitions.fields[i];
                var metadataValue = metadataValues.firstOrDefault(metadataPredicate);
                if (metadataValue){
                    if (definition.fieldType !== metadataValue.itemType){
                        metadataValue = createDefaultMetadataValue(definition);
                    }
                    else {
                        metadataValue.displayName = definition.displayName;
                        if (metadataValue.itemType === "IntegerEnum"){
                            var metadataField = null;
                            for (var x = 0; x < definition.metadataList.fieldList.length; x++) {
                                if (definition.metadataList.fieldList[x].id === metadataValue.metaDataListItemId){
                                    metadataField = definition.metadataList.fieldList[x];
                                    break;
                                }
                            }

                            if (!metadataField){
                                if (definition.metadataList.fieldList.length > 0) {
                                    metadataField = definition.metadataList.fieldList[0];
                                }
                            }

                            if (metadataField){
                                metadataValue.displayValue = metadataField.displayName;
                                metadataValue.itemValue = metadataField.itemValue;
                                metadataValue.metaDataListItemId = metadataField.id;
                            }
                        }
                    }

                } else {
                    metadataValue = createDefaultMetadataValue(definition);
                }
                newMetadataValues.push(metadataValue);
            }
        }

        return newMetadataValues;
    };

    this.getMetadataValuesFromPool = function(metadataPool, productId){
        if (metadataPool.hasOwnProperty(productId)){
            return metadataPool[productId];
        } else{
            var defaultMetadata = this.synchronizeMetadata([], productId);
            metadataPool[productId] = defaultMetadata;
            return defaultMetadata;
        }
    };
});