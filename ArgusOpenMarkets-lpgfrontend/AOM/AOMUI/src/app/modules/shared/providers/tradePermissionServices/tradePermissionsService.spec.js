describe('trade permission service tests', function() {
    "use strict";

    var testTradePermissionsService,
        mockMessageSubscriptionService2 ;

    beforeEach(function () {
        module('argusOpenMarkets.shared');
    });

    beforeEach(function () {
        module(function($provide) {
            var $injector = angular.injector([ 'argusOpenMarkets.shared' ]);
            var mockUserService = $injector.get( 'mockUserService' );
            mockMessageSubscriptionService2 = $injector.get( 'mockMessageSubscriptionService2' );
            spyOn(mockMessageSubscriptionService2,"subscribeToTopicUsingPath").and.callThrough();
            spyOn(mockMessageSubscriptionService2,"subscribeToTopicDeletedUsingPath").and.callThrough();

            $provide.value('userService', mockUserService);
            $provide.value('messageSubscriptionService2', mockMessageSubscriptionService2);
        });
    });

    beforeEach(inject(function (tradePermissionsService) {
        testTradePermissionsService = tradePermissionsService;
    }));

    it('should subscribe to the summary in the permissions matrix messages', function(){
        expect(mockMessageSubscriptionService2.subscribeToTopicUsingPath)
            .toHaveBeenCalledWith("AOM/Organisations/3/PermissionsMatrix/Summary/Products/", jasmine.any(Function));
    });

    it('should subscribe to the summary in the permissions matrix topic deletions', function(){
        expect(mockMessageSubscriptionService2.subscribeToTopicDeletedUsingPath)
            .toHaveBeenCalledWith("PermissionsMatrix/Summary", jasmine.any(Function));
    });

    it('should expose a canTrade method', function(){
        expect(testTradePermissionsService.canTrade).toBeDefined();
    });

    it('can trade should return false if no permissions', function(){
        expect(testTradePermissionsService.canTrade(1,"buy",1,2)).toEqual(false);
        expect(testTradePermissionsService.canTrade(1,"sell",1,2)).toEqual(false);
    });

    it('can trade should return true with counterparty permission and any broker', function(){
        var topic = "AOM/Organisations/3/PermissionsMatrix/Summary/Products/1/Principals/Buy/2";
        var message = {
            "message": {
                "messageType": "CounterpartyPermission",
                "messageAction": "Create",
                "messageBody": null
            },
            "canTrade": true,
            "organisationId": 2
        };

        mockMessageSubscriptionService2.mockOnMessage(topic,message);
        expect(testTradePermissionsService.canTrade(1,"sell",2)).toEqual(true);
    });

    it('can trade should return false with counterparty permission and no broker permission', function(){
        var topic = "AOM/Organisations/3/PermissionsMatrix/Summary/Products/1/Principals/Buy/2";
        var message = {
            "message": {
                "messageType": "CounterpartyPermission",
                "messageAction": "Create",
                "messageBody": null
            },
            "canTrade": true,
            "organisationId": 2
        };

        mockMessageSubscriptionService2.mockOnMessage(topic,message);
        expect(testTradePermissionsService.canTrade(1,"sell",2,8)).toEqual(false);
    });


    it('can trade should return true with counterparty permission and broker permission', function(){
        var topic = "AOM/Organisations/3/PermissionsMatrix/Summary/Products/1/Principals/Buy/2";
        var message = {
            "message": {
                "messageType": "CounterpartyPermission",
                "messageAction": "Create",
                "messageBody": null
            },
            "canTrade": true,
            "organisationId": 2
        };
        mockMessageSubscriptionService2.mockOnMessage(topic,message);
        topic = "AOM/Organisations/3/PermissionsMatrix/Summary/Products/1/Brokers/Buy/8";
        message = {
            "message": {
                "messageType": "BrokerPermission",
                "messageAction": "Create",
                "messageBody": null
            },
            "canTrade": true,
            "organisationId": 8
        };
        mockMessageSubscriptionService2.mockOnMessage(topic,message);
        expect(testTradePermissionsService.canTrade(1,"sell",2,8)).toEqual(true);
    });

});