﻿angular.module('argusOpenMarkets.shared').service("userService",
    function userService($q,
                         socketProvider,
                         messageSubscriptionService2,
                         cookieService) {
        "use strict";

        var user = null;
        var clientLog = {};
        var runJob = "";

        var getUser = function () {
            return user;
        };

        var setUser = function (usr) {
            user = usr;
        };

        var getSystemPrivileges = function () {
            return user.systemPrivileges;
        };

        var hasSystemPrivileges = function (privilegeNameArray) {
            var systemPrivileges = getSystemPrivileges();
            for (var k = 0; k < systemPrivileges.privileges.length; k++) {
                if (systemPrivileges.privileges[k]) {
                    for (var i = 0; i < privilegeNameArray.length; i++) {
                        if (systemPrivileges.privileges[k].toUpperCase() === privilegeNameArray[i].toUpperCase()) {
                            return true;
                        }
                    }
                }
            }
            return false;
        };

        var getUserId = function () {
            return user.id;
        };

        var getRole = function () {
            return user.role;
        };

        var getUsersOrganisation = function () {
            return user.userOrganisation;
        };

        var isBroker = function () {
            return getUsersOrganisation().organisationType === "Brokerage";
        };

        var getSubscribedProductIds = function () {
            var productIds = [];

            angular.forEach(user.productPrivileges, function (productPrivilege) {
                productIds.push(productPrivilege.productId);
            });

            return productIds;
        };

        var getSubscribedProductIdsByDisplayOrder = function(){
            var productIds = [],
                userProductPrivileges = _.sortBy(user.productPrivileges, 'displayOrder');

            angular.forEach(userProductPrivileges, function (productPrivilege) {
                productIds.push(productPrivilege.productId);
            });

            return productIds;
        };

        var getProductPrivileges = function (productId) {
            var productPrivileges = {};
            angular.forEach(user.productPrivileges, function (product) {
                if (product.productId === productId) {
                    productPrivileges = product.privileges;
                }
            });
            return productPrivileges;
        };

        var hasProductPrivilege = function (productId, privilegeNameArray) {

            var productPrivileges = getProductPrivileges(productId);
            for (var k in productPrivileges) {
                if (k) {
                    for (var i = 0; i < privilegeNameArray.length; i++) {
                        if (k.toUpperCase() === privilegeNameArray[i].toUpperCase()) {
                            return true;
                        }
                    }
                }
            }
            return false;
        };

        var hasAnyProductPrivileges = function (productIds, privilegeNameArray) {

            var counter,
                hasPrivilege;

            if (!productIds) {
                return false;
            }

            if (!privilegeNameArray) {
                return false;
            }

            for (counter = 0; counter <= productIds.length; counter++) {
                hasPrivilege = hasProductPrivilege(productIds[counter], privilegeNameArray);
                if (hasPrivilege) {
                    return true;
                }
            }
            return false;
        };

        var hasProductPrivilegeOnAnyProduct = function (privilegeNameArray) {
            var hasPriv = false;
            angular.forEach(getSubscribedProductIds(), function (productId) {
                if (hasProductPrivilege(productId, privilegeNameArray)) {
                    hasPriv = true;
                }
            });
            return hasPriv;
        };

        var getProductsWithPrivileges = function (privileges) {
            var result = [];
            angular.forEach(getSubscribedProductIds(), function (productId) {
                if (hasProductPrivilege(productId, privileges)) {
                    result.push(productId);
                }
            });
            return result;
        };

        var userMessagePromise = $q.defer();

        var clearClientLog = function(){
            clientLog = {};
        };

        var setRunJob = function(rj){
            runJob = rj;
        };

        var addClientLog = function(message){

            switch(message) {
                case "LOGIN_PAGE_LOADED":
                    clientLog.loginPageLoadedClient = new Date();
                    break;
                case "LOGIN_BUTTON_CLICKED":
                    clientLog.loginButtonClickedClient = new Date();
                    break;
                case "USER_AUTHENTICATED_WS":
                    clientLog.webSvcAuthenticatedClient = new Date();
                    break;
                case "USER_AUTHENTICATED_DIFFUSION":
                    clientLog.diffusionAuthenticatedClient = new Date();
                    break;
                case "USER_TRANSISTION_DASHBOARD":
                    clientLog.transitionToDashboardClient = new Date();
                    break;
                case "PRODUCT_CONFIG_LOADED":
                    clientLog.webSktProdDefJsonReceivedClient = new Date();
                    break;
                case "DASHBOARD_PAGE_LOADED":
                    clientLog.dashboardRenderedClient = new Date();
                    break;
                case "USER_JSON_RECEIVED":
                    clientLog.webSktUserJsonReceivedClient = new Date();
                    break;
                case "ALL_JSON_LOADED":
                    clientLog.webSktAllJsonReceivedClient = new Date();
                    break;
            }

            if(message === "DASHBOARD_PAGE_LOADED"){
                // Ok - we can now push ALL client log messages.
                clientLog.userId = getUserId();
                clientLog.runJob = runJob;
                sendStandardFormatMessageToUsersTopic("CLIENTLOG", "CREATE", clientLog);
                clientLog = [];
            }
        };

        var sendStandardFormatMessageToUsersTopic = function (messageType, messageAction, bodyObject) {
            var userId = getUserId();
            var topicName = 'AOM/Users/' + userId;
            var sessionId = socketProvider.getClient().getClientID();

            if (messageType === 'Order' || messageType === 'ExternalDeal' || messageType === 'Deal') {
                try {
                    adjustOffset(bodyObject);
                } catch (err) {
                    //window.log()
                }
            }

            var message = '{' +
                '"MessageType": "' + messageType + '",' +
                '"MessageAction": "' + messageAction + '",' +
                '"MessageBody": ' + JSON.stringify(bodyObject) +
                '}';

            socketProvider.sendMessage(topicName, message);
        };
        var firstConnect = true;

        var connectToUserTopic = function () {
            socketProvider.onConnect.promise.then(function () {
                if (firstConnect) {
                    var topicPath = "AOM/Users/" + getUserId();

                    messageSubscriptionService2.subscribeToTopicUsingPath(topicPath, function (message) {
                        if (firstConnect) {
                            firstConnect = false;
                            setUser(message);
                            userMessagePromise.resolve();
                        }
                    });
                }
            });
        };

        var getSessionToken = function () {
            return this.sessionToken;
        };

        var setSessionToken = function (sessionToken) {
            this.sessionToken = sessionToken;
        };

        var setLoginType = function(loginType){
            loginType = loginType.toLowerCase();
            cookieService.createCookie(loginType, 'userType');
        };

        var isCmeUser = function(){
            var loginType = cookieService.readCookie('userType');

            if(loginType === 'cme'){
                return true;
            }

            return false;
        };

        //compensated for GMT+1
        var adjustOffset = function (orderOrDeal) {
            var offset = orderOrDeal.deliveryStartDate.getTimezoneOffset();
            if (offset !== 0) {
                orderOrDeal.deliveryStartDate.setMinutes(offset *-1);
            }
            offset = orderOrDeal.deliveryEndDate.getTimezoneOffset();
            if (offset !== 0) {
                orderOrDeal.deliveryEndDate.setMinutes(offset *-1);
            }
        };

        return {
            getUser: getUser,
            getSystemPrivileges: getSystemPrivileges,
            getUserId: getUserId,
            getRole: getRole,
            getUsersOrganisation: getUsersOrganisation,
            setUser: setUser,
            hasProductPrivilege: hasProductPrivilege,
            hasAnyProductPrivileges: hasAnyProductPrivileges,
            getSubscribedProductIds: getSubscribedProductIds,
            getSubscribedProductIdsByDisplayOrder: getSubscribedProductIdsByDisplayOrder,
            userMessagePromise: userMessagePromise,
            connectToUserTopic: connectToUserTopic,
            hasProductPrivilegeOnAnyProduct: hasProductPrivilegeOnAnyProduct,
            isBroker: isBroker,
            getProductsWithPrivileges: getProductsWithPrivileges,
            sendStandardFormatMessageToUsersTopic: sendStandardFormatMessageToUsersTopic,
            getSessionToken: getSessionToken,
            setSessionToken: setSessionToken,
            setLoginType: setLoginType,
            isCmeUser : isCmeUser,
            hasSystemPrivileges: hasSystemPrivileges,
            clearClientLog: clearClientLog,
            addClientLog: addClientLog,
            setRunJob: setRunJob
        };
    });