﻿angular.module('argusOpenMarkets.shared').service("productConfigurationService",
    function productConfigurationService($rootScope,
                                         socketProvider,
                                         userService,
                                         helpers,
                                         $q,
                                         messagePopupsService,
                                         userApi) {
        var allProducts = [];
        var productConfigs = [];
        var metaDataItems = [];
        var marketStatus = {};
        var organisationSubscribedProducts = {};

        var deferredPromise = $q.defer();
        var deferredMarketStatusPromise = $q.defer();
        var deferredMetaDataPromise = $q.defer();

        var configReceived = deferredPromise.promise;
        var marketStatusReceived = deferredMarketStatusPromise.promise;
        var metaDataReceived = deferredMetaDataPromise.promise;

        socketProvider.registerConnectCallback(
            function () {
                deferredPromise = $q.defer();
                configReceived = deferredPromise.promise;
            }, true);

        var productConfigChangeCallbacks = [];
        var registerForProductConfigurationChanges = function (callback) {
            productConfigChangeCallbacks.push(callback);
        };

        configReceived.then(function () {
            angular.forEach(productConfigChangeCallbacks, function (callback) {
                callback(productConfigs);
            });
        });

        socketProvider.registerConnectCallback(
            function () {
                deferredMetaDataPromise = $q.defer();
                metaDataReceived = deferredMetaDataPromise.promise;
            }, true);

        var productMetaDataChangeCallbacks = [];
        var registerForProductMetaDataChanges = function (callback) {
            productMetaDataChangeCallbacks.push(callback);
        };

        metaDataReceived.then(function () {
            angular.forEach(productMetaDataChangeCallbacks, function (callback) {
                callback(metaDataItems);
            });
        });

        var getProductConfiguration = function (productIds) {
            var promises = [];

            var topicSP = "AOM/Organisations/" + userService.getUsersOrganisation().id + "/SubscribedProducts/";
            socketProvider.subscribe(topicSP);
            socketProvider.addTopicCallback(topicSP, function (message) {
                var shortDef = JSON.parse(message.records[0].fields[0]);
                organisationSubscribedProducts[shortDef.productId] = shortDef;
            });

            angular.forEach(productIds, function (productId) {
                var deferred = $q.defer();
                var promise = deferred.promise;

                promises.push(promise);
                var topicPath = "AOM/Products/" + productId;
                socketProvider.addTopicCallback(topicPath + "/Definition", function (message) {
                    var productId = JSON.parse(message.records[0].fields[0]).productId;
                    productConfigs[productId] = JSON.parse(message.records[0].fields[0]);
                    deferred.resolve(productConfigs[productId]);
                    $rootScope.$broadcast("ProductConfigurationService-Change", {productId: productId});
                });
                socketProvider.subscribe(topicPath + "/Definition");
            });

            $q.all(promises).then(function (configItemsReceived) {
                productConfigs = configItemsReceived;
                userService.addClientLog("PRODUCT_CONFIG_LOADED");
                deferredPromise.resolve();
            });

        };

        var getProductMetaData = function (productIds) {
            var promises = [];

            angular.forEach(productIds, function (productId) {
                var deferred = $q.defer();
                var promise = deferred.promise;

                promises.push(promise);
                var topicPath = "AOM/Products/" + productId;
                socketProvider.addTopicCallback(topicPath + "/MetaData", function (message) {
                    var mDataItem = JSON.parse(message.records[0].fields[0]);
                    metaDataItems[productId] = mDataItem;
                    //messagePopupsService.displayInfo("MetaData DisplayName: " + mDataItem.fields[0].displayName);
                    deferred.resolve(metaDataItems[productId]);
                    $rootScope.$broadcast("ProductConfigurationService-MetaDataChange", {productId: productId});
                });
                socketProvider.subscribe(topicPath + "/MetaData");
            });

            $q.all(promises).then(function (metaDataItemsReceived) {
                metaDataItems = metaDataItemsReceived;
                deferredMetaDataPromise.resolve();
            });
        };

        var getMarketsClosedStatus = function (productIds) {
            var promises = [];

            angular.forEach(productIds, function (productId) {
                var deferred = $q.defer();
                var promise = deferred.promise;

                promises.push(promise);
                var topicPath = "AOM/Products/" + productId;
                socketProvider.subscribe(topicPath + "/MarketStatus");
                socketProvider.addTopicCallback(topicPath + "/MarketStatus", function (message) {
                    var ms = JSON.parse(message.records[0].fields[0]);
                    if (marketStatus[productId] && !getProductConfigurationById(productId).isInternal) {
                        ReportMarketStatus(ms);
                    }
                    marketStatus[productId] = ms;
                    deferred.resolve(marketStatus[productId]);
                });
            });

            $q.all(promises).then(function () {
                deferredMarketStatusPromise.resolve();
            });
        };

        var ReportMarketStatus = function (marketStatus) {
            if (marketStatus.marketStatus === 0) {
                messagePopupsService.displayWarning("User Notification", "The " + marketStatus.productName + " market is now closed.");
            }
            else {
                messagePopupsService.displayInfo("User Notification", "The " + marketStatus.productName + " market is now open.");
            }
        };

        var getClosedMarketsMessage = function () {
            var closedMarkets = "",
                message;

            if (marketStatus !== null) {
                angular.forEach(marketStatus, function (ms, key) {
                    if (ms.marketStatus === 0 && !getProductConfigurationById(parseInt(key)).isInternal) {
                        // Market is closed.
                        closedMarkets += "[" + ms.productName + "], ";
                    }
                });

                if (closedMarkets) {
                    closedMarkets = closedMarkets.trim();
                    message = "The following market(s): " + closedMarkets.substring(0, closedMarkets.length - 1) + " are currently closed.  Any bids or asks created whilst the market is closed will be placed into a held state.  You will need to manually reinstate these bids/asks when the market reopens.";
                    return message;
                }
            }
            return "";
        };

        var getAllProducts = function () {
            userApi.getProductSettings().then(function (data) {
                allProducts = _.map(data, function (s) {
                    return {
                        productId: s.product.productId,
                        productName: s.product.name
                    };
                });
            });
        };

        userService.userMessagePromise.promise.then(function () {
            var productIds = userService.getSubscribedProductIds();

            userService.addClientLog("USER_JSON_RECEIVED");
            getAllProducts();
            getProductConfiguration(productIds);
            getProductMetaData(productIds);
            getMarketsClosedStatus(productIds);
        });


        var getProductConfigurationById = function (productConfigId) {
            var matchedConfig;
            angular.forEach(productConfigs, function (productConfig) {
                if (productConfig.productId === productConfigId) {
                    matchedConfig = productConfig;
                }
            });
            return matchedConfig;
        };

        var getProductTitleById = function (productConfigId) {
            var config = getProductConfigurationById(productConfigId);
            return config ? config.productTitle : '';
        };

        var getProductDisplayOrder = function (productConfigId) {
            var config = getProductConfigurationById(productConfigId);
            return config ? config.displayOrder : 0;
        };

        var getProductConfigurationLabels = function (productConfigId) {
            var config = getProductConfigurationById(productConfigId),
                locationLabel = "Location",
                deliveryPeriodLabel = "Delivery Period";

            if (config && config.locationLabel) {
                locationLabel = config.locationLabel;
            }

            if (config && config.deliveryPeriodLabel) {
                deliveryPeriodLabel = config.deliveryPeriodLabel;
            }

            return {
                "locationLabel": locationLabel,
                "deliveryPeriodLabel": deliveryPeriodLabel
            };
        };

        var getAllSubscribedProductIds = function () {
            var ids = [];
            angular.forEach(productConfigs, function (productConfig) {
                ids.push(productConfig.productId);
            });
            return ids;
        };

        var getCommonProductQuantities = function (id) {
            var config = getProductConfigurationById(id);
            return config ? config.contractSpecification.volume.commonQuantities : null;
        };

        var getOrganisationSubscribedProducts = function () {
            return organisationSubscribedProducts;
        };

        //var getAllProductMetaData = function (){
        //  var productIds = userService.getSubscribedProductIds();
        //getProductMetaData(productIds);
        // return metaDataItems;
        //};

        var getProductMetaDataById = function (productId) {
            var matchedDataItem = null;
            angular.forEach(metaDataItems, function (metaDataItem) {
                if (metaDataItem.productId === productId) {
                    matchedDataItem = metaDataItem;
                }
            });
            return matchedDataItem;
        };

        return {
            configReceived: configReceived,
            marketStatusReceived: marketStatusReceived,
            metaDataReceived: metaDataReceived,
            getAllSubscribedProductIds: getAllSubscribedProductIds,
            getAllProductConfigurations: function () {
                return productConfigs;
            },
            getAllProducts: function () {
                return allProducts;
            },
            getAllProductMetaData: function () {
                return metaDataItems;
            },
            getOrganisationSubscribedProducts: getOrganisationSubscribedProducts,
            getProductConfigurationById: getProductConfigurationById,
            getCommonProductQuantities: getCommonProductQuantities,
            marketsClosedMessage: getMarketsClosedStatus,
            getClosedMarketsMessage: getClosedMarketsMessage,
            registerForProductConfigurationChanges: registerForProductConfigurationChanges,
            registerForProductMetaDataChanges: registerForProductMetaDataChanges,
            getProductTitleById: getProductTitleById,
            getProductDisplayOrder: getProductDisplayOrder,
            getProductConfigurationLabels: getProductConfigurationLabels,
            getProductMetaDataById: getProductMetaDataById
        };
    });