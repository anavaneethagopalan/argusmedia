describe('market service tests', function () {

    var scope,
        testService,
        mockUserService = {},
        mockUserApi;

    mockUserApi = {
        getMarkets: function(callback) {
            var data = [{
                "marketId": 1,
                "marketName": "NWE",
                "productId": 1,
                "productName": "Naphtha CIF NWE - Cargoes"
            }];

            callback(data)
        }
    }

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(function () {

        module(function ($provide) {
            $provide.value('userApi', mockUserApi);
        });
    });

    var makeDefaultProducts = function () {
        return [{
            "id": 1,
            "name": "Naphtha CIF NWE - Cargoes",
            "filtered": false
        }, {
            "id": 2,
            "name": "Naphtha CIF NWE - Barges",
            "filtered": false
        }];
    };

    var makeDefaultMarkets = function () {
        return [{
            "marketId": 1,
            "marketName": "NWE",
            "productId": 1,
            "productName": "Naphtha CIF NWE - Cargoes"
        }, {
            "marketId": 1,
            "marketName": "NWE",
            "productId": 2,
            "productName": "Naphtha CIF NWE - Barges"
        }, {
            "marketId": 2,
            "marketName": "Other",
            "productId": 3,
            "productName": "Other"
        }]
    };

    beforeEach(inject(function (marketService) {
        testService = marketService;
    }));

    it('should return a list of markets', function () {
        var callBackData,
            cb = function (data) {
                callBackData = data;
            };

        testService.getMarkets(cb);

        expect(callBackData).not.toBeNull();
        expect(callBackData.length).toEqual(1);
    });
    it('filter markets by products should return a single market with two products', function () {

        var markets = makeDefaultMarkets(),
            products = makeDefaultProducts();

        var marketsFilteredByProducts = testService.filterMarketsForProducts(markets, products);
        expect(marketsFilteredByProducts).not.toEqual(null);
        expect(marketsFilteredByProducts.length).toEqual(1);
        expect(marketsFilteredByProducts[0].products.length).toEqual(2);
    });

    it('filter markets by products should return a single market with one product if user only has access to single product', function () {

        var markets = makeDefaultMarkets(),
            products = makeDefaultProducts(),
            singleProduct = products.splice(1, 1);

        var marketsFilteredByProducts = testService.filterMarketsForProducts(markets, singleProduct);
        expect(marketsFilteredByProducts).not.toEqual(null);
        expect(marketsFilteredByProducts.length).toEqual(1);
        expect(marketsFilteredByProducts[0].products.length).toEqual(1);
    });

    it('filter markets by products should return a multiple markets with the relevant products', function () {

        var markets = makeDefaultMarkets(),
            products = makeDefaultProducts();

        products.push({
            "id": 3,
            "name": "Other",
            "filtered": false
        });

        var marketsFilteredByProducts = testService.filterMarketsForProducts(markets, products);
        expect(marketsFilteredByProducts).not.toEqual(null);
        expect(marketsFilteredByProducts.length).toEqual(2);
        expect(marketsFilteredByProducts[0].products.length).toEqual(2);
        expect(marketsFilteredByProducts[1].products.length).toEqual(1);
    });
});