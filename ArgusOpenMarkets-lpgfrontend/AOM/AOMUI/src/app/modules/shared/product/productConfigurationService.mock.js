angular.module('argusOpenMarkets.shared').service("mockProductConfigurationService",
    function mockProductConfigurationService() {

        var productConfigChangeCallback = null;

        var products = [
            {   "productId": 1,
                "productName": "Argus Naphtha CIF NWE - Outright",
                "productTitle": "Argus Naphtha CIF NWE - Outright",
                "commodity": {
                    "id": 1,
                    "name": "Naphtha"
                },
                "hasAssessment": false,
                "isInternal": false,
                "contractSpecification": {
                    "volume": {
                        "minimum": 12500,
                        "maximum": 35000,
                        "increment": 500,
                        "decimalPlaces": 0,
                        "volumeUnitCode": "MT"
                    },
                    "pricing": {
                        "minimum": 1,
                        "maximum": 9999,
                        "increment": 0.25,
                        "decimalPlaces": 2,
                        "pricingUnitCode": "MT",
                        "pricingCurrencyCode": "USD"
                    },
                    "tenors": [
                        {
                            "deliveryLocation": {
                                "id": 1,
                                "name": "Rotterdam",
                                "dateCreated": "2014-01-01T00:00:00Z"
                            },
                            "deliveryStartDate": "+10d",
                            "deliveryEndDate": "+25d",
                            "rollDate": "DAILY",
                            "id": 0
                        }
                    ],
                    "contractType": "Physical"
                }
            },
            {
                "productId": 2,
                "productName": "Argus Naphtha CIF NWE - Diff",
                "productTitle": "Argus Naphtha CIF NWE - Diff",
                "commodity": {
                    "id": 1,
                    "name": "Naphtha"
                },
                "hasAssessment": true,
                "isInternal": false,
                "contractSpecification": {
                    "volume": {
                        "minimum": 12500,
                        "maximum": 35000,
                        "increment": 500,
                        "decimalPlaces": 0,
                        "volumeUnitCode": "MT"
                    },
                    "pricing": {
                        "minimum": -999,
                        "maximum": 999,
                        "increment": 0.01,
                        "decimalPlaces": 2,
                        "pricingUnitCode": "BBL",
                        "pricingCurrencyCode": "USD"
                    },
                    "tenors": [
                        {"deliveryLocation": {
                            "id": -1,
                            "name": "-",
                            "dateCreated": "2014-01-01T00:00:00Z"},
                            "deliveryStartDate": "+10d",
                            "deliveryEndDate": "+25d",
                            "rollDate": "DAILY",
                            "id": 0
                        }
                    ],
                    "contractType": "Spread"
                }
            },
            {   "productId": 3,
                "productName": "Biofuels - Outright",
                "productTitle": "Biofuels - Outright",
                "commodity": {
                    "id": 1,
                    "name": "Naphtha"
                },
                "hasAssessment": true,
                "isInternal": false,
                "contractSpecification": {
                    "volume": {
                        "minimum": 12500,
                        "maximum": 35000,
                        "increment": 500,
                        "decimalPlaces": 0,
                        "volumeUnitCode": "MT"
                    },
                    "pricing": {
                        "minimum": 1,
                        "maximum": 9999,
                        "increment": 0.25,
                        "decimalPlaces": 2,
                        "pricingUnitCode": "MT",
                        "pricingCurrencyCode": "USD"
                    },
                    "tenors": [
                        {"deliveryLocation": {
                            "id": 1,
                            "name": "Rotterdam",
                            "dateCreated": "2014-01-01T00:00:00Z"
                        },
                            "deliveryStartDate": "+10d",
                            "deliveryEndDate": "+25d",
                            "rollDate": "DAILY",
                            "id": 0
                        }
                    ],
                    "contractType": "Physical"
                }
            }
        ];

        var productMetaData = [
            {
                "productId": 1,
                "fields": [
                    {
                        "fieldList": [
                            {
                                "key": 102,
                                "value": "Port C"
                            },
                            {
                                "key": 101,
                                "value": "Port B"
                            },
                            {
                                "key": 100,
                                "value": "Port A"
                            }
                        ],
                        "id": 1,
                        "productId": 1,
                        "displayName": "Delivery Port",
                        "fieldType": "IntegerEnum"
                    },
                    {
                        "valueMinimum": 0,
                        "valuePrecision": 4,
                        "valueScale": 2,
                        "valueMaximum": 100,
                        "id": 2,
                        "productId": 1,
                        "displayName": "Delivery Qty",
                        "fieldType": "Decimal"
                    }
                ],
                "json": null
            },
            {
                "productId": 2,
                "fields": [
                    {
                        "fieldList": [
                            {
                                "key": 102,
                                "value": "Port F"
                            },
                            {
                                "key": 101,
                                "value": "Port E"
                            },
                            {
                                "key": 100,
                                "value": "Port D"
                            }
                        ],
                        "id": 3,
                        "productId": 2,
                        "displayName": "Delivery Port 2",
                        "fieldType": "IntegerEnum"
                    },
                    {
                        "valueMinimum": 100,
                        "valuePrecision": 4,
                        "valueScale": 2,
                        "valueMaximum": 1000,
                        "id": 4,
                        "productId": 2,
                        "displayName": "Delivery Qty",
                        "fieldType": "Decimal"
                    }
                ],
                "json": null
            }
        ];

        return {
            getProductConfigurationById: function (id) {
                var returnProduct = products[0];

                for(var i = 0; i < products.length; i++){
                    if(products[i].productId === id){
                        returnProduct = products[i];
                    }
                }

                return returnProduct;
            },
            getAllProductConfigurations: function () {
                return products;
            },
            getAllSubscribedProductIds: function(){
                return [1,2];
            },
            getAllProducts: function(){
                return _.map(products, function(p) {
                    return {productId:p.productId, productName:p.productName};
                });
            },
            getOrganisationSubscribedProducts: function () {
                return [{productId:1,isInternal:false,name:"Argus Naphtha CIF NWE - Outright"},{productId:2,isInternal:false,name:"Argus Naphtha CIF NWE - Diff"}, {productId:2,isInternal:true,name:"Argus Naphtha CIF NWE - Spread"}];
            },
            getClosedMarketsMessage: function(){
                return "";
            },
            getProductTitleById:function(){
                return "Test";
            },

            getAllProductMetaData: function() {
                return productMetaData;
            },
            getProductMetaDataById:function (id) {
                var dataItem = productMetaData[0];

                for(var i = 0; i < productMetaData.length; i++){
                    if(productMetaData[i].productId === id){
                        dataItem = productMetaData[i];
                    }
                }
                return dataItem;
            },

            configReceived:{then:function(){}},
            registerForProductConfigurationChanges: function(callback) {
                productConfigChangeCallback = callback;
            },
            productConfigChangeCallback: productConfigChangeCallback
        };
    }
);
