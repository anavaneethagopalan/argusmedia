angular.module('argusOpenMarkets.shared').service("marketService",
    function marketService(userApi)
    {
        var getMarkets = function (callback) {

            userApi.getMarkets(callback);
        };

        var marketProductIdExists = function (marketProductId, products) {
            var counter;

            if (products) {
                for (counter = 0; counter < products.length; counter++) {
                    if (products[counter].id === marketProductId) {
                        return products[counter];
                    }
                }
            }

            return null;
        };

        var addMarketWithProduct = function (marketProducts, market, product) {
            var counter,
                found = false;

            for (counter = 0; counter < marketProducts.length; counter++) {
                if (marketProducts[counter].marketId === market.marketId) {
                    found = true;
                    marketProducts[counter].products.push({'productId': product.id, 'productName': product.name});
                }
            }

            if (!found) {
                // Add in the market.
                marketProducts.push({
                    'marketId': market.marketId,
                    'marketName': market.marketName,
                    products: [{'productId': product.id, 'productName': product.name}]
                });
            }
        };

        var filterMarketsForProducts = function (markets, products) {
            var marketsWithProducts = [],
                counter,
                matchingProduct;

            for (counter = 0; counter < markets.length; counter++) {
                matchingProduct = marketProductIdExists(markets[counter].productId, products);

                if (matchingProduct !== null) {
                    // The user does have this product - so they have this market. Add the market into the array
                    addMarketWithProduct(marketsWithProducts, markets[counter], matchingProduct);
                }
            }

            return marketsWithProducts;
        };

        return {
            getMarkets: getMarkets,
            filterMarketsForProducts: filterMarketsForProducts
        };
    });