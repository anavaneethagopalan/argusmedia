﻿describe('product configuration service tests', function () {

    var scope, productConfigurationServiceTest, mockProductConfigurationServiceTest, mockMessageSubscriptionService2Test, userServiceMock;

    var create_mock_promise = function (call_through, do_reject, mock_data) {
        return {
            then: function (resolve, reject) {
                if (!call_through) {
                    return this;
                } else {
                    if (do_reject) {
                        return _(reject).isFunction() ? reject(mock_data) : undefined;
                    } else {
                        return resolve(mock_data);
                    }

                }
            }
        };
    };

    userServiceMock = {
        getSubscribedProductIds: function () {
            return [1, 2];
        },
        userMessagePromise: {promise: create_mock_promise(true, false, null)},
        getUsersOrganisation: function () {
            return {id: 1};
        }
    };

    var productConfig = {
        "productConfig": [
            {
                "productId": 1,
                "productName": "Argus-Naphtha-CIF-NWE-Outright",
                "productTitle": "Argus-Naphtha-CIF-NWE-Outright",
                "contractSpecification": {
                    "volumeUnits": "Metric Tonne",
                    "pricingUnit": "$/MT",
                    "market": "North West Europe",
                    "deliveryLocation": "Rotterdam",
                    "contractType": "Physical",
                    "deliveryStartDate": "+10",
                    "deliveryEndDate": "+25",
                    "rollDate": "Daily",
                    "volume": {
                        "minimum": 12500,
                        "maximum": 35000,
                        "increment": 500,
                        "decimalPlaces": 0,
                        "volumeUnitCode": "MT",
                        "commonQuantities": [15000, 20000, 25000, 30000]
                    }
                }
            },
            {
                "productId": 2,
                "productName": "Naphtha-CIF-NWE-Brent-Diff",
                "productTitle": "Naphtha-CIF-NWE-Diff",
                "contractSpecification": {
                    "volumeUnits": "Metric Tonne",
                    "pricingUnit": "$/BBL",
                    "market": "North West Europe",
                    "deliveryLocation": "Rotterdam",
                    "contractType": "Financial",
                    "deliveryStartDate": "+10",
                    "deliveryEndDate": "+25",
                    "rollDate": "Daily"
                }
            }
        ]
    };

    var mockMessagePopupsService = {
        displayInfo: function (title, message, duration) {
        },
        displayWarning: function (title, message, duration) {
        }
    };

    var mockSocketProvider = {
        topicCallbacks: {},
        subscribe: function () {
            return true;
        },
        connect: function () {
        },
        onConnect: {promise: create_mock_promise(true, false, null)},
        addTopicCallback: function (topicName, onMessage) {
            this.topicCallbacks[topicName] = onMessage
        },
        onMessage: function (topicName, message) {
            this.topicCallbacks[topicName](message)
        },
        registerConnectCallback: function () {
        }
    };

    var fakeProductSettings = [
        {product: {productId: 1, name: 'name1'}},
        {product: {productId: 2, name: 'name2'}},
        {product: {productId: 3, name: 'name3'}}
    ];

    var mockUserApi = {
        getProductSettings: function () {
            return create_mock_promise(true, false, fakeProductSettings);
        }
    };

    var createSingleFieldMessage = function (field) {
        return {
            records: [
                {fields: [JSON.stringify(field)]}
            ]
        }
    };

    var createMarketStatusMessage = function (status, productName) {
        return createSingleFieldMessage({
            marketStatus: status,
            productName: productName
        });
    };

    var topicPaths = {
        product1: {
            definition: 'AOM/Products/1/Definition',
            marketStatus: 'AOM/Products/1/MarketStatus',
            metaData: 'AOM/Products/1/MetaData'
        },
        product2: {
            definition: 'AOM/Products/2/Definition',
            marketStatus: 'AOM/Products/2/MarketStatus',
            metaData: 'AOM/Products/2/MetaData'
        }
    };

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(function () {
        module(function ($provide) {
            $provide.value('socketProvider', mockSocketProvider);
            $provide.value('userService', userServiceMock);
            $provide.value('messagePopupsService', mockMessagePopupsService);
            $provide.value('userApi', mockUserApi);
        });

        spyOn(mockSocketProvider, 'connect');
        spyOn(mockSocketProvider, "subscribe");
        spyOn(mockSocketProvider, "addTopicCallback").and.callThrough();
        spyOn(mockUserApi, "getProductSettings").and.callThrough();

        inject(function (productConfigurationService, mockProductConfigurationService, mockMessageSubscriptionService2) {
            productConfigurationServiceTest = productConfigurationService;
            mockProductConfigurationServiceTest = mockProductConfigurationService;
            mockMessageSubscriptionService2Test = mockMessageSubscriptionService2;
        });
    });

    it('attempts to subscribe to products from config', function () {
        expect(mockSocketProvider.subscribe).toHaveBeenCalledWith(topicPaths.product1.definition);
        expect(mockSocketProvider.subscribe).toHaveBeenCalledWith(topicPaths.product2.definition);
    });

    it('should register a topic callback to products from config', function () {
        expect(mockSocketProvider.addTopicCallback).toHaveBeenCalledWith(topicPaths.product1.definition, jasmine.any(Function));
        expect(mockSocketProvider.addTopicCallback).toHaveBeenCalledWith(topicPaths.product2.definition, jasmine.any(Function));
    });

    it('returns nothing on wrong ID', function () {
        mockSocketProvider.onMessage(topicPaths.product2.definition, createSingleFieldMessage(productConfig.productConfig[1]));
        expect(productConfigurationServiceTest.getProductConfigurationById(1)).toEqual(undefined);
    });


    it('should return the product config for a correct product ID', function () {
        var config = productConfig.productConfig[0];
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(config));
        expect(productConfigurationServiceTest.getProductConfigurationById(config.productId)).toEqual(config);
    });


    it('should return the correct product metadata for a product ID from all metadataitems', function () {
        //will need mock message to match the exact json from the DB
        var productId = 1;
        var mockMetaDataItem = mockProductConfigurationServiceTest.getProductMetaDataById(productId);
        mockSocketProvider.onMessage(topicPaths.product1.metaData, createSingleFieldMessage(mockMetaDataItem));
        var metaDataItems = productConfigurationServiceTest.getAllProductMetaData();
        expect(metaDataItems[1]).toEqual(mockMetaDataItem);
    });

    it('should return the correct product metadata for a specific product ID', function () {
        //will need mock message to match the exact json from the DB
        var productId = 1;
        var mockMetaDataItem = mockProductConfigurationServiceTest.getProductMetaDataById(productId);
        mockSocketProvider.onMessage(topicPaths.product1.metaData, createSingleFieldMessage(mockMetaDataItem));
        var metaDataItem = productConfigurationServiceTest.getProductMetaDataById(productId);
        expect(metaDataItem).toEqual(mockMetaDataItem);
    });

    it('should return an empty metadata item for a non-existent product ID', function () {
        //will need mock message to match the exact json from the DB
        var productId = -1;
        var metaDataItem = productConfigurationServiceTest.getProductMetaDataById(productId);
        expect(metaDataItem).toBeFalsy();
    });
    
    it('should return an empty title for a non-existent product ID', function () {
        mockSocketProvider.onMessage(topicPaths.product2.definition, createSingleFieldMessage(productConfig.productConfig[1]));
        var otherProductId = productConfig.productConfig[0].productId;
        expect(productConfigurationServiceTest.getProductTitleById(otherProductId)).toBeFalsy();
    });

    it('should return the correct common quantities for a product', function () {
        var config = productConfig.productConfig[0];
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(config));
        var commonQuantities = productConfigurationServiceTest.getCommonProductQuantities(config.productId);
        expect(commonQuantities).toEqual(config.contractSpecification.volume.commonQuantities);
    });

    it('should return null common quantities for a non-existent ID', function () {
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(productConfig.productConfig[0]));
        expect(productConfigurationServiceTest.getCommonProductQuantities(productConfig.productConfig[1].productId)).toBeFalsy();
    });

    it('should subscribe and add a topic callback for market status per product for getMarketsClosedStatus', function () {
        productConfigurationServiceTest.marketsClosedMessage([1, 2]);
        expect(mockSocketProvider.subscribe).toHaveBeenCalledWith(topicPaths.product1.marketStatus);
        expect(mockSocketProvider.subscribe).toHaveBeenCalledWith(topicPaths.product2.marketStatus);
        expect(mockSocketProvider.addTopicCallback).toHaveBeenCalledWith(topicPaths.product1.marketStatus, jasmine.any(Function));
        expect(mockSocketProvider.addTopicCallback).toHaveBeenCalledWith(topicPaths.product2.marketStatus, jasmine.any(Function));
    });

    it('should display warning message with correct text when an open market is closed', function () {
        spyOn(mockMessagePopupsService, "displayWarning");
        var product = productConfig.productConfig[0];
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(product));
        productConfigurationServiceTest.marketsClosedMessage([product.productId]);
        mockSocketProvider.onMessage(topicPaths.product1.marketStatus, createMarketStatusMessage(1, product.productName));
        expect(mockMessagePopupsService.displayWarning).not.toHaveBeenCalled();
        mockSocketProvider.onMessage(topicPaths.product1.marketStatus, createMarketStatusMessage(0, product.productName));
        expect(mockMessagePopupsService.displayWarning).toHaveBeenCalledWith(jasmine.any(String), 'The ' + product.productName + ' market is now closed.');
    });

    it('should display info message with correct text when a closed market is opened', function () {
        spyOn(mockMessagePopupsService, "displayInfo");
        var product = productConfig.productConfig[0];
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(product));
        productConfigurationServiceTest.marketsClosedMessage([product.productId]);
        mockSocketProvider.onMessage(topicPaths.product1.marketStatus, createMarketStatusMessage(0, product.productName));
        expect(mockMessagePopupsService.displayInfo).not.toHaveBeenCalled();
        mockSocketProvider.onMessage(topicPaths.product1.marketStatus, createMarketStatusMessage(1, product.productName));
        expect(mockMessagePopupsService.displayInfo).toHaveBeenCalledWith(jasmine.any(String), 'The ' + product.productName + ' market is now open.');
    });

    it('should return data for all product settings from the user api in getAllProducts', function () {
        expect(mockUserApi.getProductSettings).toHaveBeenCalled();
        var products = productConfigurationServiceTest.getAllProducts();
        expect(products.length).toEqual(fakeProductSettings.length);
        _.each(fakeProductSettings, function (s) {
            var toFind = {productId: s.product.productId, productName: s.product.name};
            expect(_.where(products, toFind).length).toEqual(1);
        });
    });

    it('should return the default value for location label if the product definition does not contain a value', function () {
        var config = productConfig.productConfig[0];
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(config));
        var labels = productConfigurationServiceTest.getProductConfigurationLabels(config.productId);
        expect(labels.locationLabel).toEqual("Location");
    });

    it('should return the default value for delivery period label if the product definition does not contain a value', function () {
        var config = productConfig.productConfig[0];
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(config));
        var labels = productConfigurationServiceTest.getProductConfigurationLabels(config.productId);
        expect(labels.deliveryPeriodLabel).toEqual("Delivery Period");
    });

    it('should return the product definition value for delivery period label if the product definition contains a value', function () {
        var config = productConfig.productConfig[0];
        config.deliveryPeriodLabel = "Dev Period";
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(config));
        var labels = productConfigurationServiceTest.getProductConfigurationLabels(config.productId);
        expect(labels.deliveryPeriodLabel).toEqual("Dev Period");
    });

    it('should return the product definition value for location label if the product definition contains a value', function () {
        var config = productConfig.productConfig[0];
        config.locationLabel = "Geographic Co-Ordinates";
        mockSocketProvider.onMessage(topicPaths.product1.definition, createSingleFieldMessage(config));
        var labels = productConfigurationServiceTest.getProductConfigurationLabels(config.productId);
        expect(labels.locationLabel).toEqual("Geographic Co-Ordinates");
    });
});