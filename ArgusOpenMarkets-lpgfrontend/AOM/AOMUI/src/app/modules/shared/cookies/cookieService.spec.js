﻿describe('cookieService Tests', function() {

    var helpers = {
        encodeString : function(string){ return string;},
        decodeString : function(string) {return string;}
    };
    var cookieStore;

    beforeEach(module('ngCookies','argusOpenMarkets.shared'));

    it('should set cookie', inject(function (cookieService){
        cookieService.createCookie("testCookie");
        var result = cookieService.readCookie();
        expect(result).toEqual("testCookie");
    }));

    it('should erase cookie', inject(function (cookieService){
        cookieService.createCookie("testCookie");
        cookieService.eraseCookie();
        var result = cookieService.getCookie;
        expect(result).toBe(undefined);
    }));

    it('should read cookie', inject(function (cookieService){
        cookieService.createCookie("eee","testCookie");
        var result = cookieService.readCookie("testCookie");
        expect(result).toBe("eee");
    }));


})
