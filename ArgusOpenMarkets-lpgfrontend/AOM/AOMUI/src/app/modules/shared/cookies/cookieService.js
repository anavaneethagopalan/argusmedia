﻿angular.module('argusOpenMarkets.shared').service("cookieService", function cookieService($cookieStore, helpers){
    var aomCookieName = "aom.argusmedia.com";
    var aomCookieExpiration = 180; //days

    var createCookie =  function createCookie(value,cookieName) {
        if(helpers.isEmpty(cookieName))
        {
            cookieName = aomCookieName;
        }
        var date = new Date();
        date.setTime(date.getTime()+(aomCookieExpiration*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
        var decodedValue = helpers.encodeString(value);
        document.cookie = cookieName+"="+decodedValue+expires+"; path=/";
    };

    var readCookie =  function readCookie(key) {
        if(helpers.isEmpty(key))
        {
            key = aomCookieName;
        }
        var nameEQ = key + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)===' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) === 0) {
                return helpers.decodeString(c.substring(nameEQ.length, c.length));
            }
        }
        return null;
    };

    var eraseCookie = function eraseCookie(name) {
        if(!name){
            name = aomCookieName;
        }
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    };

    return {
        createCookie : createCookie,
        readCookie : readCookie,
        eraseCookie : eraseCookie
    };
});
