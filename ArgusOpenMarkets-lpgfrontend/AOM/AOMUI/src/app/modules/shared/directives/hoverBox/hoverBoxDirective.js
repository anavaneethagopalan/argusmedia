﻿angular.module('argusOpenMarkets.shared').directive('hover',function($compile){

    var template = '<div data-titlebar="true" ng-style="titlerowstyle" data-style="qtip-dark" class="qtip-dark" >' ;
    template+='<div class="qtip-titlebar {{titlerowclass}}">{{titletemplate}}</div>';
    template+='<div class="qtip-content" ng-style="contentstyle">';
    template+='<table><tr class="hoverContentRow" ng-repeat=\'contentItem in parsedContent \'>';
    template+='<td class="hoverContentRowHeader"><b>{{contentItem.title | capitalizeFirstLetter}}:</b></td><td>{{contentItem.content}}</td></tr></table>';
    template+= '</div></div>';

    var linker = function(scope, element, attr){

        scope.hover = element.qtip({
            position: {
                at: scope.position,
                my: "top " + scope.pointing
            },
            content: {
                text: function() {
                    return scope.$apply(function() {
                        scope.parsedContent = JSON.parse(scope.content);
                        var text = $compile(template);
                        return text(scope);
                    });
                }
            },
            show: {
                delay: scope.delay ? scope.delay :300
            },
            events:{
                show: function(event, api) {
                    if(scope.show() === null || typeof scope.show() === 'undefined'){return;}
                    if(!scope.show()){
                        if(event) {
                            event.preventDefault();
                        }
                    }
                }
            }
        });
    };

    return{
        restrict: 'A',
        link: linker,
        scope:{
            titletemplate:'@',
            position:'@',
            content:'@',
            show:'&',
            delay:'=',
            contentstyle:'=',
            titlerowstyle:'=',
            titlerowclass:'=',
            pointing:'@'
        }
    };
});