angular.module('argusOpenMarkets.shared').directive("metadataElement", function() {
    return {
        restrict: 'E',
        scope: {
            metadataProperty: '=',
            readonly: '='
        },
        replace: true,
        template: '<ng-include src="templateUrl"/>',
        controller: function($scope){
            if ($scope.readonly === true) {
                $scope.templateUrl = 'src/app/modules/shared/directives/metadata/metadataReadOnly.part.html';
            } else if ($scope.metadataProperty.definition.fieldType === "String") {
                $scope.templateUrl = 'src/app/modules/shared/directives/metadata/metadataString.part.html';
                $scope.validityChanged = function(isValid){
                    $scope.metadataProperty.validation = {
                        isValid: isValid,
                        errorMessage: isValid ? '' :
                            'String should have length between ' +
                            $scope.metadataProperty.definition.valueMinimum +
                            ' and ' +
                            $scope.metadataProperty.definition.valueMaximum
                    };
                };
            } else if ($scope.metadataProperty.definition.fieldType === "IntegerEnum"){
                var itemId = $scope.metadataProperty.metadataitem.metaDataListItemId;
                if ($scope.metadataProperty.definition.metadataList !== null) {
                    if ($scope.metadataProperty.definition.metadataList.fieldList.length > 0) {
                        $scope.selectedValue = $scope.metadataProperty.definition.metadataList.fieldList[0];
                    }

                    for (var i = 0; i < $scope.metadataProperty.definition.metadataList.fieldList.length; i++) {
                        var field = $scope.metadataProperty.definition.metadataList.fieldList[i];
                        if (field.id === itemId) {
                            $scope.selectedValue = field;
                            break;
                        }
                    }
                }
                $scope.templateUrl = 'src/app/modules/shared/directives/metadata/metadataSelect.part.html';
                $scope.itemChanged = function(){
                    this.metadataProperty.metadataitem.displayValue = this.selectedValue.displayName;
                    this.metadataProperty.metadataitem.itemValue = this.selectedValue.itemValue;
                    this.metadataProperty.metadataitem.metaDataListItemId = this.selectedValue.id;
                };
            }
        }
    };
});
