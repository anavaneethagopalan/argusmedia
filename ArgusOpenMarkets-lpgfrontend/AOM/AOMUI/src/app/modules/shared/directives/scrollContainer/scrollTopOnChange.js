angular.module('argusOpenMarkets.shared').directive('scrollTopOnChange', function() {
    return{
        replace: false,
        restrict: 'A',
        scope:{
            scrollTopOnChange:'='
        },
        link: function (scope, element, attr) {
            scope.$watch('scrollTopOnChange', function(){
                element.scrollTop(0);
            },true);
        }
    };
});