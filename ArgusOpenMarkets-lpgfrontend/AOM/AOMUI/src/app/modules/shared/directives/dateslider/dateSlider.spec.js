﻿describe('date slider directive tests', function(){
    var  $scope,
        $compile,
        el;

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(inject(function(_$rootScope_, _$compile_){
        $scope = _$rootScope_.$new();
        $compile = _$compile_;
        $scope.minRange = 5;
        $scope.minDate = new Date(2014,1,1);
        $scope.maxDate = new Date(2014,1,21);
        $scope.deliveryStartDate =new Date(2014,1,1);
        $scope.deliveryEndDate = new Date(2014,1,11);
        el = angular.element('<div class="slider" date-slider minstartdate="minDate" minrange="minRange" maxenddate="maxDate" startdate="deliveryStartDate" daysselected="daysSelected" enddate="deliveryEndDate" ></div>')
        $compile(el)($scope);
        $scope.$digest();

    }));

    it("sets the minDate", function(){
        expect(el.scope().$$childHead.minstartdate).toEqual(new Date(2014,1,1));
    });

    it("sets the daysselected", function(){
        expect($scope.daysSelected).toEqual(11);
    });

    it("sets the start date offset for the slider when updating the cal", function(){
        expect(el.scope().$$childHead.startdateoffset).toEqual(0);
        el.scope().$$childHead.setstartdateoffset(new Date(2014,1,3));
        expect(el.scope().$$childHead.startdateoffset).toEqual(2);
    });

    it("sets the end offset for the slider when updating the cal", function(){
        expect(el.scope().$$childHead.startdateoffset).toEqual(0);
        el.scope().$$childHead.setenddateoffset(new Date(2014,1,8));
        expect(el.scope().$$childHead.enddateoffset).toEqual(7);
    });

    it("moves both sliders when range is too low when updating the end date cal", function(){
        expect(el.scope().$$childHead.startdateoffset).toEqual(0);
        el.scope().$$childHead.setstartdateoffset(new Date(2014,1,9));
        el.scope().$$childHead.setenddateoffset (new Date(2014,1,10));
        expect(el.scope().$$childHead.startdateoffset).toEqual(5);
    });

});