angular.module('argusOpenMarkets.shared').directive('stringValidation', function() {
    return {
        require: 'ngModel',
        scope: {
            minLength: '=',
            maxLength: '=',
            validityChanged: '&'
        },
        link: function(scope, element, attr, mCtrl) {
            var validateString = function(value){
                return value.length >= scope.minLength && value.length <= scope.maxLength;
            };

            mCtrl.$formatters.push(function(modelValue){
                var isValid = validateString(modelValue);
                mCtrl.$setValidity('', isValid);
                return modelValue;
            });

            mCtrl.$parsers.push(function(viewValue) {
                var isValid = validateString(viewValue);
                mCtrl.$setValidity('', isValid);
                scope.validityChanged({ isValid: isValid});
                return viewValue;
            });
        }
    };
});