angular.module('argusOpenMarkets.shared').directive('modalWindow', function($timeout, $parse) {
    return {
        restrict:"A",
        link: function(scope, element, attrs) {
            element.draggable({ containment: 'window', scroll: false });
        }
    };
});