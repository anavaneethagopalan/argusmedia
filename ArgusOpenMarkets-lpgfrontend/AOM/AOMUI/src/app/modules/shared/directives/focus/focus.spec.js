/**
 * Created by tushara.fernando on 17/11/2014.
 */
describe('focus directive tests', function(){
    var  $scope,
        $compile,
        $timeout,
        el;

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(inject(function(_$rootScope_, _$compile_,_$timeout_){
        $timeout =_$timeout_;
        $scope = _$rootScope_.$new();
        $compile = _$compile_;
        el = angular.element('<input focus="content">')
        $scope.content ="";
        $compile(el)($scope);
        $scope.$digest();

    }));

    it("focuses the element", function(){
        spyOn(el[0],'focus');
        $timeout.flush();
        expect(el[0].focus).toHaveBeenCalled();
    });

});