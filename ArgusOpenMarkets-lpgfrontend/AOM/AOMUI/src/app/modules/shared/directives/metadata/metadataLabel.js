angular.module('argusOpenMarkets.shared').directive("metadataLabel", function() {
    return {
        restrict: 'E',
        scope: {
            metadataProperty: '='
        },
        replace: true,
        template: '<label>{{metadataProperty.definition.displayName}}</label>'
    };
});
