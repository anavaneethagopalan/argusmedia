angular.module('argusOpenMarkets.shared').directive('highlighter', function($timeout, $parse) {
    return {
        restrict: 'A',
        scope: {
            model: '=highlighter'
        },
        link: function(scope, element) {
            scope.$watch('model', function (newValue, oldValue) {

                if(!newValue){
                    newValue = 0;
                }

                if(!oldValue){
                    oldValue = 0;
                }

                if(oldValue === 0){
                    oldValue = newValue;
                }

                if (newValue !== oldValue) {
                    // apply class
                    element.addClass('highlight');

                    // auto remove after some delay
                    $timeout(function () {
                        element.removeClass('highlight');
                    }, 2000);
                }
            });
        }
    };
});
