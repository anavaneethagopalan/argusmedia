﻿angular.module('argusOpenMarkets.shared').directive('focus', function($timeout, $parse) {
        return {
            link: function(scope, element, attrs) {
                var model = $parse(attrs.focus);
                scope.$watch(model, function(value) {
                    if(value !== null && typeof value !== 'undefined' && document.activeElement !==element[0]) {
                        $timeout(function() {
                            element[0].focus();
                        },700);
                    }
                });
            }
        };
    });
