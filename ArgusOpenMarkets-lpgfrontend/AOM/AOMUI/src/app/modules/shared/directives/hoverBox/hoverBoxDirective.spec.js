﻿describe('hover box directive tests', function(){
    var  $scope,
        $compile,
        el,
        showCalled;

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(inject(function(_$rootScope_, _$compile_){
        $scope = _$rootScope_.$new();
        $compile = _$compile_;
        $scope.content = {title:"testTitle",content:"testContent"};
        $scope.showTest = function(){showCalled = true;};
        el = angular.element('<div hover titleTemplate="TestHeader" show="showTest"  delay ="100" content="[]" ></div>')
        $compile(el)($scope);
        $scope.$digest();

    }));

    it("sets the delay of hover box", function(){
        expect(el.scope().$$childHead.delay).toEqual(100);
    });

    it("sets the show function of hover box", function(){
        var showFunction = el.qtip().options.events.show;
        expect(showFunction()).toEqual($scope.showTest());
    });

    it("sets the title to be the passed in title", function(){
       var content = el.qtip().options.content.text();
       expect(content.text()).toContain("TestHeader");
    });

});