﻿angular.module('argusOpenMarkets.shared').directive('scrollContainer', function() {
    return{
        replace: false,
        restrict: 'A',
        link: function (scope, element, attr) {
            var scrollBy = attr.mousewheeldistance || 96;
            element.bind("mousewheel DOMMouseScroll",function(ev) {
                var scrollTop = $(this).scrollTop();
                var delta = Math.max(-1, Math.min(1, (ev.originalEvent.wheelDelta || -ev.originalEvent.detail)));
                $(this).scrollTop(scrollTop-(delta * scrollBy));
                ev.preventDefault();
            });
        }
    };
});