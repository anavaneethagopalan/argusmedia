﻿angular.module('argusOpenMarkets.shared').directive('dateSlider', function () {
    return {
        transclude: true,
        scope: {
            minstartdate: '=',
            maxenddate: '=',
            startdate: '=',
            enddate: '=',
            daysselected: '=',
            minrange: '=',
            disabled: '=',
            candrag: '=',
            itemdisabled: '&'
        },
        template: '<div>' +
        '<div id="slider" min="{{minval}}" max="{{maxval}} ng-disabled="isDisabled()"></div>' +
        '<div><input type="text" class="datepicker-input" id="fromDate" readonly="false" ng-disabled="isDisabled()" style="padding-bottom: 0px">' +
        '<input type="text" class="datepicker-input" id="toDate" ng-disabled="isDisabled()" style="padding-bottom: 0px"></div></div>',
        replace: true,
        restrict: 'A',
        controller: function ($scope) {
            $scope.minstartdate.setHours(0, 0, 0, 0,0);
            $scope.maxenddate.setHours(0, 0, 0, 0,0);
            $scope.enddate.setHours(0, 0, 0, 0,0);
            $scope.startdate.setHours(0, 0, 0, 0,0);

            $scope.minval = 0;
            $scope.maxval = ($scope.maxenddate - $scope.minstartdate) / (1000 * 60 * 60 * 24);
            $scope.startdateoffset = ($scope.startdate - $scope.minstartdate) / (1000 * 60 * 60 * 24);
            $scope.enddateoffset = ($scope.enddate - $scope.minstartdate) / (1000 * 60 * 60 * 24);
            $scope.daysselected = Math.round($scope.enddateoffset - $scope.startdateoffset + 1);

            $scope.disabled = $scope.disabled;

            var setSliderOptions = function(start, end){
                if($("#slider").slider("option", "values")[0] !== start || $("#slider").slider("option", "values")[0] !== end) {
                    $("#slider").slider("option", "values", [start, end]);
                }
                $scope.daysselected = Math.round($scope.enddateoffset - $scope.startdateoffset + 1);
            };

            var setDatePickerOptions = function(){

                if($('#fromDate').datepicker('option', 'minDate') !== $scope.minstartdate) {
                    $('#fromDate').datepicker('option', 'minDate', $scope.minstartdate);
                }

                if($('#fromDate').datepicker('option', 'maxDate') !== $scope.maxenddate) {
                    $('#fromDate').datepicker('option', 'maxDate', $scope.maxenddate);
                }

                if($('#toDate').datepicker('option', 'minDate') !== $scope.minstartdate) {
                    $('#toDate').datepicker('option', 'minDate', $scope.minstartdate);
                }

                if($('#toDate').datepicker('option', 'maxDate') !== $scope.maxenddate) {
                    $('#toDate').datepicker('option', 'maxDate', $scope.maxenddate);
                }

                $('#fromDate').datepicker('refresh');
                $('#toDate').datepicker('refresh');

                $scope.minval = 0;
                $scope.maxenddate.setHours(0, 0, 0, 0,0);
                $scope.minstartdate.setHours(0, 0, 0, 0,0);
                $scope.maxval = ($scope.maxenddate - $scope.minstartdate) / (1000 * 60 * 60 * 24) - 1;

                if($('#slider').slider('option', 'min') !== $scope.minval) {
                    $('#slider').slider('option', 'min', $scope.minval);
                }

                if($('#slider').slider('option', 'max') !== $scope.maxval) {
                    $('#slider').slider('option', 'max', $scope.maxval);
                }

                setSliderOptions($scope.startdateoffset, $scope.enddateoffset);
            };

            $scope.$watch('startdate', function (newValue, oldValue) {

                var minRange = $scope.minrange - 1;
                newValue.setHours(0, 0, 0, 0,0);
                $scope.setstartdateoffset(newValue, true);

                $scope.enddate.setHours(0, 0, 0, 0,0);
                $scope.startdate.setHours(0, 0, 0, 0,0);
                var interval = ($scope.enddate - $scope.startdate) / (1000 * 60 * 60 * 24);
                if( interval < (minRange)){
                    // The interval is too low.  The user has just changed the start date.  Make the enddate the start date + 2;
                    $scope.enddate = newValue.addDays(minRange);
                }

                setDatePickerOptions();
                $('#fromDate').datepicker('setDate', new Date(newValue));
            });

            $scope.$watch('enddate', function (newValue, oldValue) {

                var minRange = $scope.minrange - 1;
                newValue.setHours(0, 0, 0, 0,0);
                $scope.setenddateoffset(newValue, true);

                var interval = ($scope.enddate - $scope.startdate) / (1000 * 60 * 60 * 24);
                if( interval < (minRange)){
                    // The interval is too low.  The user has just changed the start date.  Make the enddate the start date + 2;
                    $scope.startdate = newValue.addDays(minRange * -1);
                }

                setDatePickerOptions();
                $('#toDate').datepicker('setDate', new Date(newValue));
            });

            $scope.isDisabled = function(){
                var controllerDisabledControler = false;

                if($scope.itemdisabled()){
                    controllerDisabledControler = $scope.itemdisabled()();
                }

                return $scope.disabled || controllerDisabledControler;
            };

            $scope.setstartdateoffset = function(date, updateScope) {

                $scope.minstartdate.setHours(0, 0, 0, 0,0);
                date.setHours(0, 0, 0, 0,0);
                $scope.startdateoffset = (date - $scope.minstartdate) / (1000 * 60 * 60 * 24);
                if ($scope.enddateoffset - $scope.startdateoffset < $scope.minrange) {
                    $scope.enddateoffset += ($scope.minrange - ($scope.enddateoffset - $scope.startdateoffset) - 1);
                    if (updateScope === 'undefined') {
                        $scope.updatedates($scope.startdateoffset, $scope.enddateoffset, $scope.disabled);
                    }
                }

                setSliderOptions($scope.startdateoffset, $scope.enddateoffset);
            };

            $scope.setenddateoffset = function(date, updateScope) {

                $scope.minstartdate.setHours(0, 0, 0, 0,0);
                date.setHours(0, 0, 0, 0,0);
                $scope.enddateoffset = (date - $scope.minstartdate) / (1000 * 60 * 60 * 24);

                if ($scope.enddateoffset - $scope.startdateoffset < $scope.minrange) {
                    $scope.startdateoffset -= ($scope.minrange - ($scope.enddateoffset - $scope.startdateoffset) - 1);
                    if (updateScope === 'undefined') {
                        $scope.updatedates($scope.startdateoffset, $scope.enddateoffset, $scope.disabled);
                    }
                }

                setSliderOptions($scope.startdateoffset, $scope.enddateoffset);
            };

            $scope.updatedates= function(startdateoffset, enddateoffset, disabled){
                if($scope.isDisabled()){
                    return;
                }

                var startdate = new Date($scope.minstartdate);
                startdate.setHours(0, 0, 0, 0,0);
                startdate.addDays(startdateoffset);

                var enddate = new Date($scope.minstartdate);
                enddate.setHours(0, 0, 0, 0,0);
                enddate.addDays(enddateoffset);

                $scope.startdate = startdate;
                $scope.enddate = enddate;
                $('#fromDate').datepicker('setDate', new Date(startdate));
                $('#toDate').datepicker('setDate', new Date(enddate));
                $scope.daysselected = Math.round(enddateoffset - startdateoffset + 1);
            };
        },

        link: function (scope, elem, attrs) {
            var slider = $("#slider").slider({
                range: true,
                min: scope.minval,
                max: scope.maxval,
                values: [scope.startdateoffset, scope.enddateoffset],
                create: function( event, ui ) {
                    $(function () {
                        var todate = $.datepicker.formatDate('d M y', new Date(scope.enddate));
                        var mindateto = new Date(scope.minstartdate);
                        mindateto.addDays(scope.minrange-1);
                        var dates = $("#toDate").datepicker({
                            fixFocusIE: true,
                            changeMonth: true,
                            numberOfMonths: 1,
                            dateFormat: 'd M y',
                            minDate: mindateto,
                            maxDate: scope.maxenddate,
                            onSelect: function (selectedDate) {
                                var option = this.id === "toDate" ? "minDate" : "maxDate",
                                    instance = $(this).data("datepicker"),
                                    date = $.datepicker.parseDate(
                                        instance.settings.dateFormat ||
                                        $.datepicker._defaults.dateFormat,
                                        selectedDate, instance.settings);
                                scope.setenddateoffset(date);
                                scope.enddate = date;
                                scope.$apply();
                                dates.not(this).datepicker("option", option, date);
                            }
                        }).val(todate);
                    });
                    $('#toDate').click(function(){
                        $('#toDate').datepicker('show');
                    });

                    $(function () {
                        var  sliderDiv = $("#slider");

                        var minPos = 0, maxPos = 0, deltaPos = 0, currentPos = 0;

                        scope.canDrag = false;

                        sliderDiv.mousedown(function(e) {
                            var val1 = deltaPos === 1 ? minPos : minPos + deltaPos / 2;
                            var val2 = deltaPos === 1 ? maxPos : maxPos - deltaPos / 2;
                            if (currentPos > val1 && currentPos < val2){
                                scope.canDrag = true;
                                $('html,body').css('cursor','move');
                            }
                            else {
                                scope.canDrag = false;
                                $('html,body').css('cursor','auto');
                            }

                            if(scope.canDrag){
                                if(scope.itemdisabled()()){
                                    scope.canDrag = false;
                                }
                            }
                        });

                        $(this).mouseup(function(e) {
                            scope.canDrag = false;
                            $('html,body').css('cursor', 'auto');
                        });

                        $(this).mousemove(function(e) {
                            var sliderWidth = sliderDiv.width();
                            var tickSize = sliderWidth / (scope.maxval - scope.minval);
                            currentPos = parseInt((e.pageX - sliderDiv.offset().left) / tickSize);

                            if(scope.canDrag){
                                if(scope.itemdisabled()()){
                                    scope.canDrag = false;
                                }
                            }

                            if (scope.canDrag){
                                var val1 = currentPos - deltaPos;
                                var val2 = currentPos + deltaPos;
                                if (val1 <= scope.minval) {
                                    val1 = scope.minval;
                                    val2 = val1 + deltaPos * 2;
                                }
                                if (val2 >= scope.maxval) {
                                    val2 = scope.maxval;
                                    val1 = val2 - deltaPos * 2;
                                }
                                minPos = val1;
                                maxPos = val2;
                                sliderDiv.slider("values", [minPos, maxPos ]);

                                scope.startdateoffset = minPos;
                                scope.enddateoffset = maxPos;
                                scope.updatedates(scope.startdateoffset, scope.enddateoffset);
                                scope.$apply();
                            }
                        });

                        sliderDiv.mousemove(function(e) {

                            if (!scope.canDrag){
                                var value0 = sliderDiv.slider("values", 0);
                                var value1 = sliderDiv.slider("values", 1);
                                minPos = Math.min(value0, value1);
                                maxPos = Math.max(value0, value1);
                                deltaPos = (maxPos - minPos) / 2;
                            }
                        });
                    });

                    $(function () {
                        var fromdate = $.datepicker.formatDate('d M y', new Date(scope.startdate));
                        var maxdate = new Date(scope.maxenddate);

                        maxdate.subDays(scope.minrange - 1);
                        var dates = $("#fromDate").datepicker({
                            changeMonth: true,
                            numberOfMonths: 1,
                            dateFormat: 'd M y',
                            minDate: scope.minstartdate,
                            maxDate: maxdate,
                            onSelect: function (selectedDate) {
                                var option = this.id === "fromDate" ? "minDate" : "maxDate",
                                    instance = $(this).data("datepicker"),
                                    date = $.datepicker.parseDate(
                                        instance.settings.dateFormat ||
                                        $.datepicker._defaults.dateFormat,
                                        selectedDate, instance.settings);
                                scope.setstartdateoffset(date);
                                scope.startdate = date;
                                scope.$apply();
                                dates.not(this).datepicker("option", option, date);
                            }
                        }).val(fromdate);
                    });
                    $('#fromDate').click(function(){
                        $('#fromDate').datepicker('show');
                    });
                },
                slide: function (event, ui) {

                    var startValue = ui.values[0];
                    var endValue = ui.values[1];
                    var uiValue = ui.value;

                    if (scope.disabled || scope.canDrag) {
                        return false;
                    }

                    if(scope.itemdisabled()()){
                        return false;
                    }

                    if ((endValue - startValue) < 1) {
                        return false;
                    }

                    if ((endValue - startValue) < scope.minrange-1) {

                        if (uiValue === startValue) {
                            if (endValue < scope.maxval) {
                                scope.startdateoffset = startValue;
                                scope.enddateoffset = scope.startdateoffset + scope.minrange - 1;
                                $("#slider").slider("option", "values", [scope.startdateoffset, scope.startdateoffset + scope.minrange-1]);
                                scope.updatedates(scope.startdateoffset, scope.enddateoffset);
                                scope.$apply();
                                return;
                            } else {
                                return false;
                            }
                        } else {
                            if (uiValue === endValue) {

                                if (scope.minval < startValue) {
                                    scope.enddateoffset = scope.startdateoffset + (scope.minrange-1)-1;
                                    scope.startdateoffset = scope.enddateoffset - (scope.minrange-1);
                                    $("#slider").slider("option", "values", [scope.startdateoffset, scope.startdateoffset + scope.minrange]);
                                    scope.updatedates(scope.startdateoffset, scope.enddateoffset);
                                    scope.$apply();
                                    return;
                                }
                            }
                        }
                    }

                    if (endValue - startValue < scope.minrange-1) {
                        return false;
                    }

                    scope.startdateoffset = startValue;
                    scope.enddateoffset = endValue;
                    scope.updatedates(scope.startdateoffset, scope.enddateoffset);
                    scope.$apply();
                }
            });
        }
    };
});