/**
 * Created by tushara.fernando on 17/11/2014.
 */
describe('draggable modal directive tests', function(){
    var  $scope,
        $compile,
        el;

    beforeEach(module('argusOpenMarkets.shared'));

    beforeEach(inject(function(_$rootScope_, _$compile_){
        $scope = _$rootScope_.$new();
        $compile = _$compile_;
        el = angular.element('<div modal-window></div>')
        $compile(el)($scope);
        $scope.$digest();

    }));

    it("sets draggable on the element", function(){
        expect(el.hasClass("ui-draggable")).toEqual(true);
    });

});