angular.module('argusOpenMarkets.shared').directive("metadataErrorMessage", function() {
    return {
        restrict: 'E',
        scope: {
            metadataProperty: '='
        },
        replace: true,
        template: '<div class="errorMessageUnderInput" ng-show="{{!metadataProperty.validation.isValid}}">{{metadataProperty.validation.errorMessage}}</div>'
    };
});
