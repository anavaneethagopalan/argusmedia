describe('argusOpenMarkets.shared.metadataElement', function() {
    'use strict';

    var compileMetadataElement = function(metadataProperty, readOnly){
        var scope;

        inject(function ($compile, $rootScope) {
            scope = $rootScope.$new();
            scope.metadataProperty = metadataProperty;
            var element = angular.element('<metadata-element metadata-property="metadataProperty" readonly="' + readOnly + '"/>');
            element = $compile(element)(scope);
            scope = scope.$$childHead;
        });

        return scope;
    };

    describe('enum metadata property', function(){
        var metadataProperty = {
            definition: {
                "metadataList" :{
                    "fieldList": [
                        {
                            "id": 1,
                            "displayName": "Port A",
                            "itemValue": 100
                        },
                        {
                            "id": 2,
                            "displayName": "Port B",
                            "itemValue": 101
                        },
                        {
                            "id": 3,
                            "displayName": "Port C",
                            "itemValue": 102
                        }
                    ]
                },
                "id": 1,
                "productId": 1,
                "displayName": "Delivery Port",
                "fieldType": "IntegerEnum"
            },
            metadataitem: {
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port B",
                itemValue: 101,
                metaDataListItemId : 2,
                itemType: 10
            }
        };

        var scope;

        beforeEach(function(){
            module('argusOpenMarkets.shared');
            scope = compileMetadataElement(metadataProperty);
        });

        it("Should set correct template", function(){
            expect(scope.templateUrl).toBe('src/app/modules/shared/directives/metadata/metadataSelect.part.html');
        });

        it("Should set correct selected value", function(){
            expect(scope.selectedValue).toEqual(metadataProperty.definition.metadataList.fieldList[1]);
        });

        it("Should update model when selected value changed", function(){
            scope.selectedValue = metadataProperty.definition.metadataList.fieldList[2];
            scope.itemChanged();
            expect(scope.metadataProperty.metadataitem).toEqual({
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port C",
                itemValue: 102,
                metaDataListItemId : 3,
                itemType: 10
            });
        });

        it("Should set first value from list if no value found", function(){
            var scopeWithWrongValue = compileMetadataElement({
                definition: metadataProperty.definition,
                metadataitem: {
                productMetaDataId: 1,
                    displayName: "Delivery Port",
                    displayValue: "Port B",
                    itemValue: 101,
                    metaDataListItemId : 123,
                    itemType: 10
                }});

            expect(scopeWithWrongValue.selectedValue).toEqual(metadataProperty.definition.metadataList.fieldList[0]);
        });
    });

    describe('string metadata property', function(){
        var metadataProperty = {
            definition: {
                "valueMinimum": 1,
                "valueMaximum": 100,
                "id": 3,
                "productId": 1,
                "displayName": "Delivery Description",
                "fieldType": "String"
            },
            metadataitem: {
            "productMetaDataId": 3,
            "displayName": "Delivery Description",
            "displayValue": "asdfer",
            "itemValue": null,
            "metaDataListItemId": null,
            "itemType": 20
            }
        };

        var scope;

        beforeEach(function(){
            module('argusOpenMarkets.shared');
            scope = compileMetadataElement(metadataProperty);
        });

        it("Should set correct template", function(){
            expect(scope.templateUrl).toBe('src/app/modules/shared/directives/metadata/metadataString.part.html');
        });

        it("Should set correct error message when wrong string was inputted", function(){
            scope.validityChanged(false);
            expect(scope.metadataProperty.validation.isValid).toBe(false);
            expect(scope.metadataProperty.validation.errorMessage).toBe('String should have length between 1 and 100');
        });

        it("Should clear error message when correct string was inputted", function(){
            scope.validityChanged(false);
            scope.validityChanged(true);
            expect(scope.metadataProperty.validation.isValid).toBe(true);
            expect(scope.metadataProperty.validation.errorMessage).toBe('');
        });
    });

    describe('readonly metadata property', function() {
        var metadataProperty = {
            definition: {
                "valueMinimum": 1,
                "valueMaximum": 100,
                "id": 3,
                "productId": 1,
                "displayName": "Delivery Description",
                "fieldType": "String"
            },
            metadataitem: {
                "productMetaDataId": 3,
                "displayName": "Delivery Description",
                "displayValue": "asdfer",
                "itemValue": null,
                "metaDataListItemId": null,
                "itemType": 20
            }
        };

        var scope;

        beforeEach(function(){
            module('argusOpenMarkets.shared');
            scope = compileMetadataElement(metadataProperty, true);
        });

        it("Should set correct template", function(){
            expect(scope.templateUrl).toBe('src/app/modules/shared/directives/metadata/metadataReadOnly.part.html');
        });
    });
});
