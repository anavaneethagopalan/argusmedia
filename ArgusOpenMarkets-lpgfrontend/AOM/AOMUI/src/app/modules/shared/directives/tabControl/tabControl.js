angular.module('argusOpenMarkets.shared').directive('tabControl', function () {
    return {
        transclude: false,
        scope: {
            tabsdisplay: '=',
            usermadechange: '=',
            ondatalosschangetab: '&',
            ontabselected: '&'
        },
        templateUrl: 'src/app/modules/shared/directives/tabControl/tabControl.part.html',
        replace: true,
        restrict: 'A',
        controller: function ($scope) {

            $scope.selectedTabMarketId = -1;

            var getSelectedTab = function () {
                var counter;

                if ($scope.tabsdisplay) {
                    for (counter = 0; counter < $scope.tabsdisplay.length; counter++) {
                        if ($scope.tabsdisplay[counter].selected) {
                            return $scope.tabsdisplay[counter];
                        }
                    }
                }
                return null;
            };

            $scope.$watchCollection('tabsdisplay', function(oldVal, newValue){
                var selectedTab;

                if($scope.tabsdisplay) {
                    selectedTab = getSelectedTab();
                    if (selectedTab.marketId !== $scope.selectedTabMarketId) {
                        $scope.selectedTabMarketId = selectedTab.marketId;
                        $scope.ontabselected()(selectedTab);
                    }
                }
            });

            $scope.selectTab = function(selectedTab){

                if (selectedTab) {
                    if(!selectedTab.selected){
                        if($scope.usermadechange){
                            // User has made a change - stop the user from moving onto new tab
                            if($scope.ondatalosschangetab){
                                $scope.ondatalosschangetab()();
                            }
                        }else {
                            // Only allow the click if the selected tab is NOT already selected.
                            for (var i = 0; i < $scope.tabsdisplay.length; i++) {
                                $scope.tabsdisplay[i].selected = false;
                            }

                            selectedTab.selected = true;

                            // Call back to the function that the tab has ben selected...
                            $scope.ontabselected()(selectedTab);
                        }
                    }
                }
            };

            // When we initialize - fire the onTabSelected event for the first tab.
            var selectedTab = getSelectedTab();
            if(selectedTab){
                $scope.ontabselected()(selectedTab);
            }
        },
        link: function (scope, elem, attrs) {
        }
    };
});