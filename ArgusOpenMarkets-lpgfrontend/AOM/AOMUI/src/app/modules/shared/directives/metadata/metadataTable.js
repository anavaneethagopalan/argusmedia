angular.module('argusOpenMarkets.shared').directive("metadataTable", function() {
    return {
        restrict: 'E',
        scope: {
            metadataCollection: '=',
            hide: '=',
            readonly: '='
        },
        replace: true,
        templateUrl: 'src/app/modules/shared/directives/metadata/metadataTable.part.html',
    };
});
