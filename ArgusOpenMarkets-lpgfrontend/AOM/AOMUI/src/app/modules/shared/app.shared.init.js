﻿angular.module('argusOpenMarkets.shared', ['ngResource'])
    .constant("keepAlivePingTime", 15000)
    .constant("maxReconnectRetryAttempts", 9)
    .constant("timeBetweenConnectionAttempts", 6000)
    .constant("popupMessageConfig",{
        useToastr:true,
        error:{
            duration:0
        },
        info:{
            duration:30000
        },
        warning:{
            duration: 15000
        },
        success:{
            duration:5000
        }
    })
    .constant("loggedOutErrorMessagesConstant",{
        "1": "You have been logged out due to a connection problem, please try to log in again"
    });