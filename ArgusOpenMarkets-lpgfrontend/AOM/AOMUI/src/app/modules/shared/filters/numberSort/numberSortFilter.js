﻿angular.module('argusOpenMarkets.shared')
    .filter('numberSort', function() {
        return function(input) {
            return input.sort(function(a, b){
                return a - b;
            });
        };
    });

