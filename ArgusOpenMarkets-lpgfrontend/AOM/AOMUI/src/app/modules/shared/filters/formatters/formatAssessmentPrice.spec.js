describe('Test format assessment price', function()
{
    'use strict'
    var $filter;

    beforeEach(function(){
       module('argusOpenMarkets.shared');

       inject(function(_$filter_){
            $filter = _$filter_;
       });
    });

    it('should get formatted price-remove digits after decimal', function () {
        var result = $filter('formatAssessmentPrice')(100.9800235, 'formatAssessmentPrice');
        expect(result).toEqual('100.98');
    });

    it('should get formatted price - add digits to deciamal', function () {
        var result = $filter('formatAssessmentPrice')(100, 'formatAssessmentPrice');
        expect(result).toEqual('100.00');
    });

    it('should get formatted price-format zeros', function () {
        var result = $filter('formatAssessmentPrice')(0, 'formatAssessmentPrice');
        expect(result).toEqual('-');
    });

});