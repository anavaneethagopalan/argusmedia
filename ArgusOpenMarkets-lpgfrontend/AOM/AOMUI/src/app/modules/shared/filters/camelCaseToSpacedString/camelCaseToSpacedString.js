/**
 * Created by tushara.fernando on 17/12/2014.
 */
angular.module('argusOpenMarkets.shared')
    .filter('camelCaseToSpacedString', function() {
        return function(input) {
            if ( typeof input !== 'undefined' && input!==null) {
                return input
                    // insert a space before all caps
                    .replace(/([A-Z])/g, ' $1')
                    // uppercase the first character
                    .replace(/^./, function(str){ return str.toUpperCase();
                    });
            }
        };
    });
