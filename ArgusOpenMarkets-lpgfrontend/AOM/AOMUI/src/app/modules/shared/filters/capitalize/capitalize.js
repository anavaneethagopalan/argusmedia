﻿angular.module('argusOpenMarkets.shared')
.filter('capitalize', function() {
    return function(input) {
        if ( typeof input !== 'undefined' && input!==null) {
            return input.toUpperCase();
        }
    };
});