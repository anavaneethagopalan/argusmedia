﻿describe('Test sort filter', function () {
    'use strict';

    var $filter;

    beforeEach(function () {
        module('argusOpenMarkets.shared');

        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('should sort array low to high', function () {
        var array = [9,2,7,4,5,10,1], result;

        result = $filter('numberSort')(array, 'numberSort');

        expect(result).toEqual([1,2,4,5,7,9,10]);
    });

    it('empty should throw error', function () {
        var array, result;



        expect(function(){
            $filter('numberSort')([], 'numberSort');
        }).not.toThrow();
    });
});