﻿angular.module('argusOpenMarkets.shared')
.filter('capitalizeFirstLetter', function() {
        return function (input) {
            if ( typeof input !== 'undefined' && input!==null) {
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
    };
});

