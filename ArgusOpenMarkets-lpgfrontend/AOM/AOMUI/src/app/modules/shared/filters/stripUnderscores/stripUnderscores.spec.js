describe('strip underscores from text', function()
{
    'use strict'
    var $filter;

    beforeEach(function(){
        module('argusOpenMarkets.shared');

        inject(function(_$filter_){
            $filter = _$filter_;
        });
    });

    it('should get formatted price-remove digits after decimal', function () {
        var result = $filter('stripUnderscores')("aba", 'stripUnderscores');
        expect(result).toEqual('aba');
    });

    it('should get formatted price-format zeros', function () {
        var result = $filter('stripUnderscores')("bbb_aaa__pp", 'stripUnderscores');
        expect(result).toEqual('bbb aaa  pp');
    });

});