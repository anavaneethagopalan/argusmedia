angular.module('argusOpenMarkets.shared').filter('formatAssessmentPrice',function(){
return function (price) {
        if (angular.isDefined(price) && price !== null) {
            if (price === 0) {
                return "-";
            }
            else {
                return price.toFixed(2);
            }
        }
        else {
            return "-";
        }
    };
});