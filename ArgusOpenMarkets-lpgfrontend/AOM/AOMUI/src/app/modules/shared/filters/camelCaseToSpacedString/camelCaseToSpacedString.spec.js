describe('Test camelCaseToSpacedString filter', function () {
    'use strict';

    var $filter;

    beforeEach(function () {
        module('argusOpenMarkets.shared');

        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('should split camel case and add spaces', function () {
        var string = 'helloWorld', result;

        result = $filter('camelCaseToSpacedString')(string, 'camelCaseToSpacedString');

        expect(result).toEqual('Hello World');
    });

    it('should not throw if null string', function () {
        var string;

        expect(function(){
            $filter('camelCaseToSpacedString')(string, 'camelCaseToSpacedString');
        }).not.toThrow();
    });
});