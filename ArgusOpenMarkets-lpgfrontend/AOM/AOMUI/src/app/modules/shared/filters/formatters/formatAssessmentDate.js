angular.module('argusOpenMarkets.shared').filter('formatAssessmentDate',function(){
    return function (assessmentDate) {
        if (angular.isDefined(assessmentDate) && assessmentDate !== null) {
            var d = new Date(assessmentDate);

            var month = new Array("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");

            return d.getDate() + "-" + month[d.getMonth()];
        }
        else {
            return "";
        }
    };
});