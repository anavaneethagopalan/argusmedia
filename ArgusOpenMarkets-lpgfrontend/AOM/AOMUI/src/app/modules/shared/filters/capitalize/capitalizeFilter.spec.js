﻿describe('Test capitalize filter', function () {
    'use strict';

    var $filter;

    beforeEach(function () {
        module('argusOpenMarkets.shared');

        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('should capitalize the first letter of a string', function () {
        var string = 'hello world', result;

        result = $filter('capitalize')(string, 'capitalize');

        expect(result).toEqual('HELLO WORLD');
    });

    it('should not throw if null string', function () {
        var string;

        expect(function(){
            $filter('capitalize')(string, 'capitalize');
        }).not.toThrow();
    });
});