﻿describe('Test capitalize filter', function () {
    'use strict';

    var $filter;

    beforeEach(function () {
        module('argusOpenMarkets.shared');

        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('should capitalize the first letter of a string', function () {
        var string = 'hello world', result;

        result = $filter('capitalizeFirstLetter')(string, 'capitalizeFirstLetter');

        expect(result).toEqual('Hello world');
    });

    it('should not throw if null string', function () {
        var string;

        expect(function(){
            $filter('capitalizeFirstLetter')(string, 'capitalizeFirstLetter');
        }).not.toThrow();
    });
});