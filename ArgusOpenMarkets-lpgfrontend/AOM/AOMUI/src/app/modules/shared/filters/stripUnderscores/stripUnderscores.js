angular.module('argusOpenMarkets.shared')
    .filter('stripUnderscores', function() {
        return function(input) {
            if ( typeof input !== 'undefined' && input!==null) {
                return input.replace(/_/g," ");
            }
        };
    });
