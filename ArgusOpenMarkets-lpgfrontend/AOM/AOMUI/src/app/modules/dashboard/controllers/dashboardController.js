﻿angular
    .module('argusOpenMarkets.dashboard')
    .controller('dashboardController',
    function dashboardController($scope,
                                 productConfigurationService,
                                 dashboardConfigurationService,
                                 userService,
                                 messagePopupsService,
                                 clientNotificationMessageService,
                                 userDashboardConfigurationService,
                                 $window) {

        $scope.title = "Dashboard";

        var width = 1527;
        var columns = 3;

        if (!$scope.userDashboardConfig) {
            // Only reload if we have to.
            $scope.userDashboardConfig = dashboardConfigurationService.getDashboardLayout();
        }
        var colUsage = (function (colCount) {
            var colArr = [];
            for (var i = 0; i < colCount; i++) {
                colArr[i] = false;
            }
            angular.forEach($scope.userDashboardConfig, function (item) {
                var size = item.tile.sizeX;
                var col = item.tile.col;
                for (var y = col; y <= (col + (size - 1)); y++) {
                    colArr[y] = true;
                }
            });
            return colArr;
        }(3));

        var decrementItemTileCol = function (item) {
            item.tile.col--;
        };
        for (var i = 0; i < colUsage.length; i++) {
            if (!colUsage[colUsage.length - columns]) {
                columns--;
                width = width - 518;
                angular.forEach($scope.userDashboardConfig, decrementItemTileCol);
            }
            if (!colUsage[columns - 1]) {
                columns--;
                width = width - 518;
            }
        }

        $scope.gridsterOptions = {
            columns: columns, // the width of the grid, in columns
            pushing: true, // whether to push other items out of the way on move or resize
            floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
            swapping: true, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
            width: width, // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
            colWidth: 504, // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 15, // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [10, 5], // the pixel distance between each widget
            outerMargin: true, // whether margins apply to outer edges of the grid
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: false, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 2, // the minimum height of the grid, in rows
            maxRows: 200,
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            resizable: {
                enabled: false,
                handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw']
            },
            draggable: {
                enabled: false
            }
        };

        var leavingPageText = "Are you sure you want to leave your AOM Session?";

        userService.sendStandardFormatMessageToUsersTopic('User', 'Register', "creating a user session");

        $scope.getProductConfigItems = function (productIds) {
            var productConfigItems = [],
                prodConfig;

            angular.forEach(productIds, function (productId) {
                prodConfig = productConfigurationService.getProductConfigurationById(productId);
                if (prodConfig) {
                    if (prodConfig.bidAskStackNumberRows) {
                        $scope.gridRowCount = prodConfig.bidAskStackNumberRows;
                    } else {
                        $scope.gridRowCount = 6;
                    }
                    productConfigItems.push(prodConfig);
                }
            });

            return productConfigItems;
        };


        $scope.$on('$locationChangeStart', function (event, next, current) {

            confirmUrlChange(event, next, current);
        });

        var displayClosedMarkets = function () {
            userService.addClientLog("DASHBOARD_PAGE_LOADED");

            var marketsClosedMessage = productConfigurationService.getClosedMarketsMessage();

            if (marketsClosedMessage) {
                messagePopupsService.displayInfo("User Notification", marketsClosedMessage);
            }
        };

        var confirmUrlChange = function (event, next, current) {
            if (next.toLowerCase().indexOf("dashboard?l=1") > -1) {
                return;
            }
            if (next.toLowerCase().indexOf("usersettings") > -1) {
                return;
            }
            if (next.toLowerCase().indexOf("logoff") > -1) {
                return;
            }
            if (next.toLowerCase().indexOf('dashboard') > -1) {
                return;
            }
            if (current.toLowerCase().indexOf("logoff") > -1) {
                return;
            }
            if (current.toLowerCase().indexOf("dashboard") > -1) {
                var answer = window.confirm(leavingPageText);
                if (!answer) {
                    event.preventDefault();
                } else {
                    $window.location.reload();
                }
            }
        };

        $window.sendingEmail = false;

        $window.onbeforeunload = function (e) {
            if ($window.sendingEmail) {
                $window.sendingEmail = false;
            } else {
                return leavingPageText;
            }
        };

        $scope.gridRowCount = 6;

        $scope.viewWidgetFilter = function (item) {
            return userDashboardConfigurationService.canViewWidget(item);
        };

        $scope.$on('$destroy', function () {
            window.onbeforeunload = null;
        });

        $scope.logout = function (userMessage) {
            clientNotificationMessageService.logout(userMessage);
        };

        $scope.$on('decreaseBidAskStackWidgetSizeRequest', function (event, gridRowCount) {

            $scope.gridRowCount = gridRowCount;
            $scope.gridRowCount -= 6;
            if ($scope.gridRowCount < 3) {
                $scope.gridRowCount = 3;
            }
            // var bidAskStackWidgets = getBidAskStacksInUserDashboardConfig($scope.userDashboardConfig);
            updateBidAskStackWidgetTileSize($scope.gridRowCount, $scope.userDashboardConfig);
            dashboardConfigurationService.updateUserTileConfig($scope.userDashboardConfig);
            dashboardConfigurationService.saveUserTileConfig();
            $scope.$broadcast('decreaseBidAskStackSize', $scope.gridRowCount);
        });

        var resizeWidget = function (widgetName, size) {
            var rowValue,
                sizeY,
                totalSizeY = 0,
                columnReset = true;

            for (var j = 0; j < $scope.userDashboardConfig.length; j++) {
                if ($scope.userDashboardConfig[j].view === widgetName) {
                    $scope.userDashboardConfig[j].tile.sizeY = size;
                }
            }

            dashboardConfigurationService.updateUserTileConfig($scope.userDashboardConfig);
            dashboardConfigurationService.saveUserTileConfig();
        };

        $scope.$on('setAssessmentsWidgetSize', function (event, size) {
            resizeWidget("assessments", size);
        });

        $scope.$on('setMarketTickerSize', function (event, size) {

            resizeWidget("marketTicker", size);
            $scope.$broadcast('resizedMarketTicker', size);
        });

        $scope.onIncreaseBidAskStackWidgetSizeRequest = function (event, gridRowCount) {

            $scope.gridRowCount = gridRowCount;
            if ($scope.gridRowCount === 3) {
                $scope.gridRowCount = 6;
            }
            else {
                $scope.gridRowCount += 6;
            }
            // var bidAskStackWidgets = getBidAskStacksInUserDashboardConfig($scope.userDashboardConfig);
            updateBidAskStackWidgetTileSize($scope.gridRowCount, $scope.userDashboardConfig);
            dashboardConfigurationService.updateUserTileConfig($scope.userDashboardConfig);
            dashboardConfigurationService.saveUserTileConfig();
            $scope.$broadcast('increaseBidAskStackSize', $scope.gridRowCount);
        };

        $scope.$on('increaseBidAskStackWidgetSizeRequest', $scope.onIncreaseBidAskStackWidgetSizeRequest);

        var getBidAskStacksInUserDashboardConfig = function (userDashboardConfig) {

            var bidAskStacks = [];

            for (var i = 0; i < userDashboardConfig.length; i++) {
                if (userDashboardConfig[i].view === "bidAskStack") {
                    bidAskStacks.push(userDashboardConfig[i]);
                }
            }

            return bidAskStacks;
        };

        var updateBidAskStackWidgetTileSize = function (gridRowCount, userDashboardConfig) {
            var newTileSize = dashboardConfigurationService.determineBidAskTileSizePixels(gridRowCount);

            for (var i = 0; i < userDashboardConfig.length; i++) {
                if (userDashboardConfig[i].view === "bidAskStack") {
                    if (newTileSize === 18) {
                        newTileSize = 17.9;
                    }
                    else if (newTileSize === 30) {
                        newTileSize = 28.5;
                    }
                    else if (newTileSize === 42) {
                        newTileSize = 39;
                    }
                    else if (newTileSize === 54) {
                        newTileSize = 51;
                    }
                    userDashboardConfig[i].tile.sizeY = newTileSize;
                }
            }
        };

        displayClosedMarkets();
    });