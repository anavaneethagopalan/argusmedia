﻿describe('dashboardControllerTests', function () {

    var dashboardController,
        scope,
        rootScope,
        controller,
        stateProvider,
        toastr,
        mockUserDashboardConfigurationService = {
            getUserConfiguration: function () {
                return { userTiles: {marketIds: [1], tile: {"tileType": "bidAskStack",
                    position: {row: "default", col: "default"},
                    size: {x: "default", y: "default"}}},
                    marketConfig: null, userDetails: null };
            },
            canViewWidget: function(){return true;}
        }
        ;

    var mockDashboardConfigurationService = {
        getDashboardLayout: function () {
            return null;
        },
        defaultTileConfiguration: function () {
            return{get: function () {
                return null;
            }}
        },
        saveUserTileConfig: function () {
            return true;
        },
        determineBidAskTileSizePixels: function(gridRowSize){
            return 6;
        },
        updateUserTileConfig: function(userDashboardConfig){

        }
    };

    var mockWindow = {
        console: {
            log: function(message){}
        },
        location: {
            href: {},
            reload: function(){}
        }
    };

    var mockMessagePopupsService = {
        displaySuccess: function (a,b) { return null; },
        displayInfo: function (a,b) { return null; },
        displayWarning: function (a,b) { return null; },
        displayError: function (a,b) { return null; }
    };

    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.dashboard', function ($stateProvider, $provide) {
        stateProvider = $stateProvider;
        //$provide.value('$window',mockWindow);
    }));

    beforeEach(inject(function ($controller, $rootScope, mockSocketProvider, mockUserService, mockProductConfigurationService,mockClientNotificationMessageService) {

        rootScope = $rootScope;
        scope = $rootScope.$new();
        scope.userDashboardConfig = null;
        controller = $controller;
        toastr = $rootScope.toastr;
        scope.userService=mockUserService;
        scope.mockSocketProvider=mockSocketProvider;

        dashboardController = $controller('dashboardController',
        {
            $scope: scope,
            userDashboardConfigurationService: mockUserDashboardConfigurationService,
            productConfigurationService:mockProductConfigurationService,
            dashboardConfigurationService: mockDashboardConfigurationService,
            userService: mockUserService,
            clientNotificationMessageService:mockClientNotificationMessageService,
            messagePopupsService: mockMessagePopupsService,
            socketProvider: mockSocketProvider,
            toastr: toastr,
            $window:mockWindow
        });
    }));

    it('dashboard controller should set the title to Dashboard', function(){
        expect(scope.title).toEqual('Dashboard');
    });

    it('default the gridster options', function(){

        var expectedGridsterOptions = {
            minRows: 2, // the minimum height of the grid, in rows
                maxRows: 40,
            columns: 12, // the width of the grid, in columns
            colWidth: '83', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: '84', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [10, 5], // the pixel distance between each widget
            defaultSizeX: 2, // the default width of a gridster item, if not specified
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            resizable: {
            enabled: true
        },
            draggable: {
                enabled: false,
                    stop: function(event, uiWidget, $element) {
                    dashboardConfigurationService.saveUserTileConfig($scope.userDashboardConfig);
                }
            }
        };

        expect(scope.gridsterOptions.minRows).toEqual(expectedGridsterOptions.minRows);
    });

    it('should get product config items', function(){

        var items = scope.getProductConfigItems([1]);

        expect(items.length).toBe(1);
    });

    it('should set gridRowCount to 3 when was 6 and decreaseBidAskStackWidgetSizeRequest called', function(){

        scope.gridRowCount = 6;
        scope.userDashboardConfig = [{tile: {sizeY: 10}, view: "bidAskStack"}];

        scope.$broadcast('decreaseBidAskStackWidgetSizeRequest', 6);
        expect(scope.gridRowCount).toBe(3);
    });

    it('should set grid row count to 12 when 6 onIncreaseBidAskStackWidgetSizeRequest',function(){

        scope.gridRowCount = 6;
        scope.userDashboardConfig = [{tile: {sizeY: 10}, view: "bidAskStack"}];

        scope.onIncreaseBidAskStackWidgetSizeRequest({}, 6);

        expect(scope.gridRowCount).toBe(12);
    });

    it('should set grid row count to 6 when 3 onIncreaseBidAskStackWidgetSizeRequest',function(){

        scope.gridRowCount = 3;
        scope.userDashboardConfig = [{tile: {sizeY: 10}, view: "bidAskStack"}];

        scope.onIncreaseBidAskStackWidgetSizeRequest({}, 3);

        expect(scope.gridRowCount).toBe(6);
    });

});