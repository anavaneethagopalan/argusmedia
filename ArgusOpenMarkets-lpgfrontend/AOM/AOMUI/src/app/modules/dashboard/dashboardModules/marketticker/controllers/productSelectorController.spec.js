describe('argusOpenMarkets.dashboard.productSelectorController', function() {
    "use strict";

    describe('productSelectorController tests', function() {

        var scope, testController;
        var mockModal, mockProductSelectorService;

        var allMockProductConfigs, mockUserProductConfigs;

        var orderExecutionMode = "External";
        var getExecutionMode = function() { return orderExecutionMode; };

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(module('argusOpenMarkets.dashboard'));

        beforeEach(inject(function ($controller,
                                    $rootScope,
                                    mockProductConfigurationService) {
            scope = $rootScope.$new();
            allMockProductConfigs = mockProductConfigurationService.getAllProductConfigurations();
            mockUserProductConfigs = allMockProductConfigs.slice(0, 2);
            mockModal = jasmine.createSpyObj('mockModal', ['dismiss', 'close']);
            mockProductSelectorService = jasmine.createSpyObj('mockProductSelectorService', [
                'getUsersTradableProductConfigs',
                'onProductSelected'
            ]);
            mockProductSelectorService.getUsersTradableProductConfigs.and.returnValue(mockUserProductConfigs);

            testController = $controller('productSelectorController', {
                $scope: scope,
                $modalInstance: mockModal,
                productSelectorService: mockProductSelectorService,
                executionMode: getExecutionMode()
            });
        }));

        it('should have a defined controller', function() {
            expect(testController).toBeDefined();
        });

        it('should not enable OK button when first shown', function() {
            expect(scope.enableOk).toBeDefined();
            expect(scope.enableOk()).toBeFalsy();
        });

        it('should call modal dismiss when cancelled', function() {
            expect(mockModal.dismiss).not.toHaveBeenCalled();
            scope.exit();
            expect(mockModal.dismiss).toHaveBeenCalled();
        });

        it('should call modal close with the result on OK', function() {
            var fakeResult = {productId: 1, tenorId:1};
            scope.result = fakeResult;
            scope.ok();
            expect(mockModal.close).toHaveBeenCalledWith(fakeResult);
        });

        it('should ask the userProductDataService for the users products', function() {
            expect(mockProductSelectorService.getUsersTradableProductConfigs).toHaveBeenCalledWith(getExecutionMode());
        });

        it('should have the products the userProductDataService returns', function() {
            expect(scope.products).toBeDefined();
            expect(scope.products).toEqual(mockUserProductConfigs);
        });

        it('should have no products if the user has no subscribed products', inject(function($controller, $rootScope) {
            scope = $rootScope.$new();
            mockModal = jasmine.createSpyObj('mockModal', ['dismiss', 'close']);
            mockProductSelectorService = jasmine.createSpyObj(
                'mockProductSelectorService', ['getUsersTradableProductConfigs']);
            mockProductSelectorService.getUsersTradableProductConfigs.and.returnValue([]);

            testController = $controller('productSelectorController', {
                $scope: scope,
                $modalInstance: mockModal,
                productSelectorService: mockProductSelectorService,
                executionMode: getExecutionMode()
            });

            expect(scope.products).toEqual([]);
        }));

        it('should call the productSelectorService when a product is selected', function() {
            var fakeReply = {"result": {}, "tenors": {}, "enableTenorSelection": false};
            mockProductSelectorService.onProductSelected.and.returnValue(fakeReply);
            scope.productSelected(mockUserProductConfigs[0]);
            expect(mockProductSelectorService.onProductSelected).toHaveBeenCalled();
        });

        it('should set result based on the reply from productSelectorService when a product is selected', function() {
            var fakeReply = {"result": "I am a test", "tenors": {}, "enableTenorSelection": false};
            mockProductSelectorService.onProductSelected.and.returnValue(fakeReply);
            scope.productSelected(mockUserProductConfigs[0]);
            expect(scope.result).toEqual(fakeReply.result);
        });

        it('should set tenors based on the reply from productSelectorService when a product is selected', function() {
            var fakeReply = {"result": {}, "tenors": "I am a test", "enableTenorSelection": false};
            mockProductSelectorService.onProductSelected.and.returnValue(fakeReply);
            scope.productSelected(mockUserProductConfigs[0]);
            expect(scope.tenors).toEqual(fakeReply.tenors);
        });

        it('should set enableTenorSelection based on the reply from productSelectorService when a product is selected', function() {
            var fakeReply = {"result": {}, "tenors": {}, "enableTenorSelection": true};
            mockProductSelectorService.onProductSelected.and.returnValue(fakeReply);
            scope.productSelected(mockUserProductConfigs[0]);
            expect(scope.enableTenorSelection).toEqual(fakeReply.enableTenorSelection);
        });

        it('should set the result tenorId when a tenor is selected', function() {
            var fakeTenor = {"id":5150};
            scope.tenorSelected(fakeTenor);
            expect(scope.result.tenorId).toEqual(fakeTenor.id);
        });

        it('should enable OK if both productId and tenorId are set in the reply result', function() {
            var fakeReply = {"result": {"productId": 1, "tenorId": 2}, "tenors": {}, "enableTenorSelection": false};
            mockProductSelectorService.onProductSelected.and.returnValue(fakeReply);
            scope.productSelected(mockUserProductConfigs[0]);
            expect(scope.enableOk()).toBeTruthy();
        });

        it('should not enable OK until tenor is selected if only product ID is set in the reply result', function() {
            var fakeReply = {"result": {"productId": 1, "tenorId": null}, "tenors": {}, "enableTenorSelection": true};
            var fakeTenor = {"id":5150};
            mockProductSelectorService.onProductSelected.and.returnValue(fakeReply);
            scope.productSelected(mockUserProductConfigs[0]);
            expect(scope.enableOk()).toBeFalsy();
            scope.tenorSelected(fakeTenor);
            expect(scope.enableOk()).toBeTruthy();
        });
    })
});
