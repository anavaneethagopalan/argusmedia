﻿describe("Bid Ask Stack Class", function() {

    var stateProvider;

    var locationLabel = "Location";

    var deliveryPeriodLabel = "Delivery Period";

    var orderMessage;

    var isOrderMine = true;

    var bidAskStack;

    var productIsBrokerable=true;

    var mockProductConfigurationService = {

        getProductConfigurationLabels: function(productId){
            return {
                "locationLabel": locationLabel,
                "deliveryPeriodLabel": deliveryPeriodLabel
            };
        },

        getProductConfigurationById : function(productId){
            return {
                "coBrokering" : productIsBrokerable
            };
        }
    };

    var findNumberMatchesInTitle = function(hoverContent, searchTitle){
        var countLocation = 0;

        for(var i = 0; i < hoverContent.length; i++){
            if(hoverContent[i].title === searchTitle){
                countLocation++;
            }
        }

        return countLocation;
    }

    var findNumberMatchesInValue = function(hoverContent, searchValue){
        var countLocation = 0;

        for(var i = 0; i < hoverContent.length; i++){
            if(hoverContent[i].content === searchValue){
                countLocation++;
            }
        }

        return countLocation;
    }

    var mockUserOrderService = {
        isOrderMine: function(order){
            return isOrderMine;
        },
        isOrderFromAnotherBroker: function(order){
            return true;
        }
    };

    var mockUser = {
        userId: 12,
        fullName: "Mock User",
        subscribedProducts: [
            {productId: 1, userProductPrivileges: ["Order_Create_Principal", "Order_Aggress_Principal"]},
            {productId: 2, userProductPrivileges: ["Order_Create_Broker", "Order_Aggress_Broker"]}
        ],
        userPrivileges: {},
        organisation: {organisationName: "XYZ",  organisationType: "Trading", id: 1},
        userOrganisation : {organisationName: "XYZ",  organisationType: "Trading", id: 1},
        username: "mockUser"

    };

    var mockUserService = {

        hasProductPrivilege: function (productId, arrayKeys) {
            return true;
        },
        hasProductPrivilege: function () {
            return true;
        },
        getUsersOrganisation: function () {
            return mockUser.organisation;
        },
        getUser: function(){
            return mockUser;
        },
        getUserId: function(){
            return mockUser.userId;
        },
        isBroker: function(){
            return false;
        }
    };

    var mockMessagePopupsService = {

        displaySuccess: function(title, message){

        }
    };

    var mockMessageSubscriptionService2;

    beforeEach(module('toastr'));

    beforeEach(function () {
        module('diffusion');
    });

    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
    });

    beforeEach(function () {
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(module('argusOpenMarkets.dashboard', function ($stateProvider, $provide) {
        var $injector = angular.injector([ 'argusOpenMarkets.shared' ]);
        stateProvider = $stateProvider;
        mockMessageSubscriptionService2 = $injector.get( 'mockMessageSubscriptionService2' );
        $provide.value('userService', mockUserService);
        $provide.value('userOrderService', mockUserOrderService);
        $provide.value('productConfigurationService', mockProductConfigurationService);
        $provide.value('messagePopupsService',mockMessagePopupsService);
        $provide.value('$modal',null);
        $provide.value('messageSubscriptionService2', mockMessageSubscriptionService2);

    }));

    beforeEach(inject(function(BidAskStack) {
        bidAskStack = new BidAskStack();

        mockUser.organisation.id = 1;

        orderMessage =
        {
            "message": {
                "messageType": "Order",
                "messageAction": "Create"
            },
            "order": {
                "id": 133,
                "price": 900.0,
                "quantity": 17500.0,
                "tenor": {
                    "deliveryLocation": {
                        "id": 1,
                        "name": null,
                        "dateCreated": null
                    },
                    "deliveryStartDate": null,
                    "deliveryEndDate": null,
                    "rollDate": null,
                    "id": 1
                },
                "orderType": "Bid",
                "lastUpdated": "2014-07-03T16:40:06.8325744Z",
                "lastUpdatedUserId": 2,
                "lastUpdatedUserName": null,
                "userId": 3,
                "userName": null,
                "deliveryEndDate": "2009-04-12T19:44:55Z",
                "deliveryStartDate": "2009-04-12T19:44:55Z",
                "brokerId": null,
                "brokerName": null,
                "brokerOrganisationId": 1,
                "principalOrganisationId": 1,
                "brokerOrganisationName": null,
                "organisationId": 3,
                "organisationName": null,
                "orderStatus": "Active",
                "notes": null,
                "executionMode": "Internal",
                "productId": 1,
                "productName": null,
                "metaData": [{"displayName": "MetaDataLabel", "metaDataListItemId": 1, "productMetaDataId": 1, "displayValue": "PortC", "itemValue": "MetaDataValue"}]
            },
            "sessionId": null,
            "userId": 0
        };
    }));


    it('should contain a BidAskStack class',
    inject(function(BidAskStack){
        expect(BidAskStack).toEqual(BidAskStack);
    }));

    it('should be able to instantiate a new BidAskStack',
        inject(function(BidAskStack){
            var bas = new BidAskStack();
            expect(bas).not.toBe(null);
    }));

    it('should be instantiated with empty bids and asks',
        inject(function(BidAskStack){
            var bas = new BidAskStack();
            expect(bas.bids).not.toBe(null);
            expect(bas.asks).not.toBe(null);
            expect(bas.bids.length).toBe(0);
            expect(bas.asks.length).toBe(0);
        }));

    it('should create empty bid via static method',
        inject(function(BidAskStack,helpers){
            var emptyBid = BidAskStack.createEmptyBid();
            expect(emptyBid).not.toBe(null);
            expect(helpers.isEmpty(emptyBid.order.id)).toBe(true);
        }));

    it('should create empty ask via static method',
        inject(function(BidAskStack,helpers){
            var emptyAsk = BidAskStack.createEmptyAsk();
            expect(emptyAsk).not.toBe(null);
            expect(helpers.isEmpty(emptyAsk.order.id)).toBe(true);
        }));

    it('should not add bid when unknown message action',
        function() {

            orderMessage.message.messageAction = "N/A";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";

            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.bids.length).toEqual(0);
        });

    it('should add bid order to stack for create message',
        function() {
            orderMessage.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";

            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.bids.length).toEqual(1);
        });

    it('should remove bid order from stack for kill message',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);
            var bidsCountAfterCreate = bidAskStack.bids.length;

            orderMessage.message.messageAction = "Kill";
            orderMessage.order = {};
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);
            var bidsCountAfterKill = bidAskStack.bids.length;

            expect(bidsCountAfterCreate).toEqual(1);
            expect(bidsCountAfterKill).toEqual(0);
    });

    it('should add 2 bids',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Create";
            bidMessage2.order.id = 2;
            bidMessage2.order.price = 901;
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage2);

            expect(bidAskStack.bids.length).toEqual(2);
        });

    it('should sort 2 bids',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Create";
            bidMessage2.order.id = 2;
            bidMessage2.order.price = 901;
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage2);

            expect(bidAskStack.bids[0].order.price).toEqual(901);
            expect(bidAskStack.bids[1].order.price).toEqual(900);
        });

    it('should sort 2 asks on price',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.orderType = "Ask";
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onAskMessageEventHandler(orderMessage);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Create";
            bidMessage2.order.id = 2;
            bidMessage2.order.orderType = "Ask";
            bidMessage2.order.price = 901;
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onAskMessageEventHandler(bidMessage2);

            expect(bidAskStack.asks[0].order.price).toEqual(900);
            expect(bidAskStack.asks[1].order.price).toEqual(901);
        });

    it('should sort 2 asks on date',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.orderType = "Ask";
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onAskMessageEventHandler(orderMessage);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Create";
            bidMessage2.order.id = 2;
            bidMessage2.order.orderType = "Ask";
            bidMessage2.order.price = 900;
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onAskMessageEventHandler(bidMessage2);

            expect(bidAskStack.asks[0].order.id).toEqual(1);
            expect(bidAskStack.asks[1].order.id).toEqual(2);
        });

    it('should update existing bid',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.bids[0].order.price).toEqual(900);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Update";
            bidMessage2.order.id = 1;
            bidMessage2.order.price = 905;
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage2);

            expect(bidAskStack.bids.length).toEqual(1);
            expect(bidAskStack.bids[0].order.price).toEqual(905);
        });

    it('should remove bid when execute called',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.bids[0].order.price).toEqual(900);
            expect(bidAskStack.bids.length).toEqual(1);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Execute";
            bidMessage2.order.id = 1;
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage2);

            expect(bidAskStack.bids.length).toEqual(0);
        });

    it('should replace bid when hold is called and order is mine',
        function() {

            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.bids[0].order.price).toEqual(900);
            expect(bidAskStack.bids[0].order.orderStatus).toEqual('Active');
            expect(bidAskStack.bids.length).toEqual(1);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Hold";
            bidMessage2.order.id = 1;
            bidMessage2.order.orderStatus = 'Held';
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage2);

            expect(bidAskStack.bids.length).toEqual(1);
            expect(bidAskStack.bids[0].order.orderStatus).toEqual('Held');
        });

    it('should remove bid when hold is called and order is not mine',
        function() {

            isOrderMine = false;
            mockUser.organisation.id = 2;

            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.bids[0].order.price).toEqual(900);
            expect(bidAskStack.bids[0].order.orderStatus).toEqual('Active');
            expect(bidAskStack.bids.length).toEqual(1);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Hold";
            bidMessage2.order.id = 1;
            bidMessage2.order.orderStatus = 'Held';
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage2);

            expect(bidAskStack.bids.length).toEqual(0);
        });

    it('should replace bid when held bid is reinstated and order is mine',
        function() {

            isOrderMine = true;

            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.bids[0].order.price).toEqual(900);
            expect(bidAskStack.bids[0].order.orderStatus).toEqual('Active');
            expect(bidAskStack.bids.length).toEqual(1);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Hold";
            bidMessage2.order.id = 1;
            bidMessage2.order.orderStatus = 'Held';
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage2);

            expect(bidAskStack.bids.length).toEqual(1);
            expect(bidAskStack.bids[0].order.orderStatus).toEqual('Held');

            var bidMessage3 = angular.copy(bidMessage2);
            bidMessage3.message.messageAction = "Reinstate";
            bidMessage3.order.id = 1;
            bidMessage3.order.orderStatus = 'Active';
            bidMessage3.order.lastUpdatedDate = "2014-07-03T18:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage3);

            expect(bidAskStack.bids.length).toEqual(1);
            expect(bidAskStack.bids[0].order.orderStatus).toEqual('Active');
        });

    it('should add bid when held bid is reinstated and order is not mine',
        function() {

            isOrderMine = false;
            mockUser.organisation.id = 3;

            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.bids[0].order.price).toEqual(900);
            expect(bidAskStack.bids[0].order.orderStatus).toEqual('Active');
            expect(bidAskStack.bids.length).toEqual(1);

            var bidMessage2 = angular.copy(orderMessage);
            bidMessage2.message.messageAction = "Hold";
            bidMessage2.order.id = 1;
            bidMessage2.order.orderStatus = 'Held';
            bidMessage2.order.lastUpdatedDate = "2014-07-03T17:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage2);

            expect(bidAskStack.bids.length).toEqual(0);

            var bidMessage3 = angular.copy(bidMessage2);
            bidMessage3.message.messageAction = "Reinstate";
            bidMessage3.order.id = 1;
            bidMessage3.order.orderStatus = 'Active';
            bidMessage3.order.lastUpdatedDate = "2014-07-03T18:40:06.8325744Z";
            bidAskStack.onBidMessageEventHandler(bidMessage3);

            expect(bidAskStack.bids.length).toEqual(1);
            expect(bidAskStack.bids[0].order.orderStatus).toEqual('Active');
        });

    it('should not call display popup when user is last updated',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";
            orderMessage.order.lastUpdatedUserId = mockUserService.getUserId();

            spyOn(mockMessagePopupsService, 'displaySuccess');

            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(mockMessagePopupsService.displaySuccess).not.toHaveBeenCalled();
        });

    it('should calculate correct market depth',
        function() {
            orderMessage.message.messageAction = "Create";
            orderMessage.order.id = 1;
            orderMessage.order.price = 900;
            orderMessage.order.lastUpdatedDate = "2014-07-03T16:40:06.8325744Z";

            bidAskStack.onBidMessageEventHandler(orderMessage);

            expect(bidAskStack.getMarketDepth()).toEqual(1);
        });

    it('should setup the hover content with the location label set to default if it has NOT been overriden', inject(function(BidAskStack){

        bidAskStack.onBidMessageEventHandler(orderMessage);

        var hoverContent = bidAskStack.bids[0].hoverContent;
        findNumberMatchesInTitle(hoverContent, "Location");
        expect(findNumberMatchesInTitle(hoverContent, "Location")).toEqual(1);
    }));

    it('should setup the hover content with a single meta data item if one exists displaying the label correctly', inject(function(BidAskStack){

        bidAskStack.onBidMessageEventHandler(orderMessage);

        var hoverContent = bidAskStack.bids[0].hoverContent;
        expect(findNumberMatchesInTitle(hoverContent, "MetaDataLabel")).toEqual(1);
    }));

    it('should setup the hover content with a single meta data item if one exists displaying the value correctly', inject(function(BidAskStack){

        bidAskStack.onBidMessageEventHandler(orderMessage);

        var hoverContent = bidAskStack.bids[0].hoverContent;
        expect(findNumberMatchesInValue(hoverContent, "PortC")).toEqual(1);
    }));


    it('should setup the hover content with the location label set to an overriden value if its overriden', inject(function(BidAskStack){

        locationLabel = "Location Label";
        bidAskStack.onBidMessageEventHandler(orderMessage);
        var hoverContent = bidAskStack.bids[0].hoverContent;

        expect(findNumberMatchesInTitle(hoverContent, "Location")).toEqual(0);
        expect(findNumberMatchesInTitle(hoverContent, "Location Label")).toEqual(1);
    }));

    it('should setup the hover content with the delivery period label set to default if it has NOT been overriden', inject(function(BidAskStack){

        bidAskStack.onBidMessageEventHandler(orderMessage);
        var hoverContent = bidAskStack.bids[0].hoverContent;

        expect(findNumberMatchesInTitle(hoverContent, "Delivery Period")).toEqual(1);
    }));

    it('should setup the hover content with the location label set to an overriden value if its overriden', inject(function(BidAskStack){

        deliveryPeriodLabel = "Delivery Period Label";
        bidAskStack.onBidMessageEventHandler(orderMessage);

        var hoverContent = bidAskStack.bids[0].hoverContent;

        expect(findNumberMatchesInTitle(hoverContent, "Delivery Period")).toEqual(0);
        expect(findNumberMatchesInTitle(hoverContent, "Delivery Period Label")).toEqual(1);
    }));

    it('hover text should have CoBrokerable message if product is CoBrokerable',
        function() {
            productIsBrokerable=true;
            bidAskStack.onBidMessageEventHandler(orderMessage);

            var hoverContent = bidAskStack.bids[0].hoverContent;
            expect(findNumberMatchesInTitle(hoverContent, "Co-brokering enabled")).toEqual(1);

        });

    it('hover text should not have CoBrokerable message if product is not CoBrokerable',
        function() {
            productIsBrokerable=false;
            bidAskStack.onBidMessageEventHandler(orderMessage);

            var hoverContent = bidAskStack.bids[0].hoverContent;
            expect(findNumberMatchesInTitle(hoverContent, "Co-brokering enabled")).toEqual(0);

        });
});
