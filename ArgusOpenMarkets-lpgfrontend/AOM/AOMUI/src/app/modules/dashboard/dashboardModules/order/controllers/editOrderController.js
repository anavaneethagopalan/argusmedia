﻿angular.module('argusOpenMarkets.dashboard')
    .controller('editOrderController',
        function editOrderController($scope,
                                     userOrderService,
                                     productConfigurationService,
                                     tenorId,
                                     $modalInstance,
                                     userService,
                                     $state,
                                     order,
                                     orderType,
                                     productId,
                                     orderValidationService,
                                     userApi,
                                     productMessageSubscriptionService,
                                     orderDialogService,
                                     productMetadataService) {
            "use strict";
            $scope.orderType = orderType;
            $scope.orderChanged = false;
            $scope.actionButtonClicked = false;

            $scope.isExternalOrder = order.executionMode === "External";

            $scope.$on('$stateChangeSuccess', function () {
                if ($state.current.name !== 'Dashboard') {
                    $modalInstance.dismiss('cancel');
                }
            });

            // principals and brokers
            $scope.IsBroker = function () {
                return userService.isBroker();
            };

            var setOrder = function (order) {
                $scope.order = angular.copy(order);

                $scope.orderIsHeld = order.orderStatus === "Held";
                $scope.organisation = userService.getUsersOrganisation().name;

                $scope.orderChanged = false;
                $scope.orderDeleted = false;

                $scope.originalOrderStr = null;
                $scope.modifiedOrderStr = null;

                $scope.validPrinicipalSelected = !$scope.IsBroker();

                if(!productConfigurationService.getProductConfigurationById(productId).coBrokering)
                {
                    //edge case for product config changing
                    $scope.order.coBrokering = false;
                }
            };

            $scope.refreshOrder = function () {
                if ($scope.updatedOrder) {
                    setOrder($scope.updatedOrder);
                    $scope.order.deliveryStartDate = new Date(moment($scope.order.deliveryStartDate));
                    $scope.order.deliveryEndDate = new Date(moment($scope.order.deliveryEndDate));
                    refreshMetadataCollection();
                }
            };

            var handleBidAskUpdated = function (order) {
                if ($scope.order) {
                    if ($scope.order.id === order.id) {
                        $scope.updatedOrder = order;
                        $scope.orderChanged = true;
                    }
                }
            };

            $scope.itemdisabled = function(){
                return $scope.orderChanged;
            };

            var handleBidAskDeleted = function (orderDeletedTopic) {
                var id = 0;

                var parts = orderDeletedTopic.split('/');
                id = parseInt(parts[parts.length - 1]);

                if ($scope.order) {
                    if ($scope.order.id === id) {
                        $scope.orderDeleted = true;
                    }
                }
            };

            var onBidEventHandler = function (orderMessage) {
                handleBidAskUpdated(orderMessage.order);
            };

            var onAskEventHandler = function (orderMessage) {
                handleBidAskUpdated(orderMessage.order);
            };

            var onOrderDeletedEventHandler = function (orderDeleted) {
                handleBidAskDeleted(orderDeleted);
            };

            $scope.$on('$destroy', function () {
                productMessageSubscriptionService.unregisterActionOnProductMessageType("Edit Order Controller " + $scope.order.productId, $scope.order.productId, "onBid");
                productMessageSubscriptionService.unregisterActionOnProductMessageType("Edit Order Controller " + $scope.order.productId, $scope.order.productId, "onAsk");
                productMessageSubscriptionService.unregisterActionOnProductMessageType("Edit Order Controller " + $scope.order.productId, $scope.order.productId, "onOrderDeleted");
            });


            // We might want to consider a price formatting directive in the future:
            // http://stackoverflow.com/questions/23505322/angular-ng-pattern-preventing-value-from-binding
            // - PT
            if (order.price) {
                if (typeof(order.price) === "number") {
                    order.price = order.price.toFixed(2);
                }
                if (typeof(order.price) === "string") {
                    order.price = parseFloat(order.price).toFixed(2);
                }
            }

            $scope.setOrderType = function (orderType) {
                $scope.orderType = orderType;
            };

            setOrder(order);
            productMessageSubscriptionService.registerActionOnProductMessageType("Edit Order Controller " + $scope.order.productId, $scope.order.productId, "onBid", onBidEventHandler);
            productMessageSubscriptionService.registerActionOnProductMessageType("Edit Order Controller " + $scope.order.productId, $scope.order.productId, "onAsk", onAskEventHandler);
            productMessageSubscriptionService.registerActionOnProductMessageType("Edit Order Controller " + $scope.order.productId, $scope.order.productId, "onOrderDeleted", onOrderDeletedEventHandler);

            // Required for IE7.
            /// $scope.order.price = parseFloat($scope.order.price);
            $scope.productLabels = productConfigurationService.getProductConfigurationLabels(order.productId);

            var deregisterSelectedBrokerWatch;
            var registerSelectedBrokerWatch = function () {
                deregisterSelectedBrokerWatch = $scope.$watch('selectedBroker', function (a, b) {
                    var selectionChange = (a !== b);
                    updateCoBrokeringUIElements(selectionChange);
                });
            };
            registerSelectedBrokerWatch();

            // Get the information for the modal dropdowns
            var getBrokersOrPrincipals = function () {
                var buyOrSell = $scope.orderType === "Bid" ? "buy" : "sell";
                // Get the information for the modal dropdowns
                if ($scope.IsBroker()) {
                    $scope.executeEnabled = false; //need to select a principal
                    userApi.getPrincipals(productId, true, buyOrSell).then(
                        function (data) {
                            $scope.principals = data;
                        },
                        function (error) {
                            userApi.formatErrorResponse(error);
                            $scope.exit();
                        }
                    );
                }
                else {
                    userApi.getBrokers(productId, true, buyOrSell).then(
                        function (data) {
                            deregisterSelectedBrokerWatch();
                            var noBroker = {
                                "id": -100,
                                "name": "Bilateral Only (No Broker)",
                                "isDeleted": false,
                                "shortCode": "BIL",
                                "userId": -1,
                                nameAndCode: "Bilateral Only (No Broker)"
                            };
                            data.unshift(noBroker);
                            $scope.brokers = data;
                            if ($scope.order.brokerOrganisationId !== null) {

                                for (var i = 0; i < $scope.brokers.length; i++) {
                                    if ($scope.order.brokerOrganisationId === $scope.brokers[i].id) {
                                        $scope.selectedBroker = $scope.brokers[i];
                                    }
                                }
                            }
                            if ($scope.order.brokerRestriction === "None") {
                                $scope.selectedBroker = noBroker;
                            }
                            registerSelectedBrokerWatch();
                        },
                        function (error) {
                            userApi.formatErrorResponse(error);
                            $scope.exit();
                        }
                    );
                }
            };

            $scope.$watch('orderType', function (a, b) {
                getBrokersOrPrincipals();
            });

            $scope.onBrokerChange = function (opt, brokerSelect) {
                $scope.selectedBroker = opt;
                updateBroker();
            };

            $scope.orderHasPrincipal = function () {
                return order.principalUserId !== null;
            };

            $scope.orderHasBroker = function () {
                return order.brokerOrganisationId !== null;
            };

            $scope.exit = function () {
                $modalInstance.dismiss('exit');
            };

            $scope.holdOrder = function () {
                if ($scope.actionButtonClicked) {
                    return;
                }
                $scope.actionButtonClicked = true;

                userOrderService.sendHoldOrderMessage($scope.order);
                $modalInstance.dismiss('exit');
            };

            $scope.killOrder = function () {

                if ($scope.actionButtonClicked) {
                    return;
                }
                $scope.actionButtonClicked = true;

                userOrderService.sendKillOrderMessage($scope.order);
                $modalInstance.dismiss('exit');
            };

            $scope.reinstateOrder = function () {

                if ($scope.actionButtonClicked) {
                    return;
                }
                $scope.actionButtonClicked = true;

                userOrderService.sendReinstateOrderMessage($scope.order);
                $modalInstance.dismiss('exit');
            };

            $scope.switchToExecuteMode = function () {
                $modalInstance.dismiss('execute');
            };

            $scope.editOrder = function () {

                if ($scope.actionButtonClicked) {
                    return;
                }
                $scope.actionButtonClicked = true;

                if ($scope.order && $scope.order.notes) {
                    $scope.order.notes = $scope.order.notes.trim();
                }
                userOrderService.sendEditOrderMessage($scope.order);

                $modalInstance.dismiss('exit');
            };

            var updateBroker = function () {
                if ($scope.IsBroker() === true) {
                    return false;
                }
                if ($scope.selectedBroker) {
                    $scope.order.brokerRestriction = $scope.getBrokerRestriction($scope.selectedBroker.id);

                    if ($scope.selectedBroker.id === -100) {
                        $scope.order.brokerOrganisationId = null;
                    } else {
                        $scope.order.brokerOrganisationId = $scope.selectedBroker.id;
                    }
                    //$scope.order.brokerId = $scope.selectedBroker.userId;
                    $scope.order.brokerName = $scope.selectedBroker.name;
                }
                else {
                    $scope.order.brokerOrganisationId = null;
                    $scope.order.brokerId = null;
                    $scope.order.brokerName = null;
                    $scope.order.brokerRestriction = $scope.getBrokerRestriction($scope.order.brokerOrganisationId);
                }
            };

            $scope.getBrokerRestriction = function (brokerOrganisationId) {

                if (brokerOrganisationId === -100) {
                    return "None";
                }

                if (brokerOrganisationId > 0) {
                    return "Specific";
                }

                return "Any";
            };


            $scope.showPrincipal = function () {
                return orderDialogService.hasNeededOrderCreatePrivilegesAsPrincipal(
                    productId, $scope.order.executionMode);
            };

            $scope.showBroker = function () {
                return orderDialogService.hasNeededOrderCreatePrivilegesAsBroker(
                    productId, $scope.order.executionMode);
            };

            $scope.showExecuteMode = function () {
                if ($scope.orderIsHeld) {
                    return false;
                }
                if (userService.hasProductPrivilege(productId, ["Order_Aggress_Broker"]) && userOrderService.isOrderMine($scope.order)) {
                    return true;
                }
                return false;
            };

            $scope.productConfig = angular.copy(productConfigurationService.getProductConfigurationById(productId));
            angular.forEach($scope.productConfig.contractSpecification.tenors, function (tenor) {
                if (tenor.id === tenorId) {
                    $scope.tenor = tenor;
                }
            });

            $scope.quantities = $scope.productConfig.contractSpecification.volume.commonQuantities;
            $scope.setQuantity = function (volume) {
                $scope.order.quantity = volume;
            };

            var startDateOffset = parseInt($scope.tenor.deliveryStartDate);
            var today = new Date();
            $scope.minDate = today.addDays(startDateOffset);
            var maxdate = new Date();
            $scope.maxDate = maxdate.addDays(parseInt($scope.tenor.deliveryEndDate));
            $scope.order.deliveryStartDate = new Date(moment($scope.order.deliveryStartDate));
            $scope.order.deliveryEndDate = new Date(moment($scope.order.deliveryEndDate));
            $scope.minRange = $scope.tenor.minimumDeliveryRange;

            var productConfigPrice = $scope.productConfig.contractSpecification.pricing;
            $scope.invalidPriceReason = "";
            $scope.priceSpecification = {
                decimalPlaces: productConfigPrice.decimalPlaces,
                isValid: function (price) {

                    if ($scope.order.price === undefined) {
                        if (typeof(price) === 'object') {
                            $scope.order.price = price.$viewValue;
                        }
                    }

                    var validity = orderValidationService.validateOrderPrice($scope.order.price, productConfigPrice);

                    if (typeof(price) !== 'object') {
                        if ($scope) {
                            if ($scope.editOrderForm) {
                                price = $scope.editOrderForm.price;
                            }
                        }
                    }

                    if (price && typeof(price) === 'object') {
                        price.$setValidity("price", validity.isValid);
                        $scope.invalidPriceReason = validity.invalidReason;
                    }
                }
            };

            var productConfigQuantity = $scope.productConfig.contractSpecification.volume;
            $scope.invalidQuantityReason = "";
            $scope.quantitySpecification = {
                decimalPlaces: productConfigQuantity.decimalPlaces,
                isValid: function (quantity) {
                    if ($scope.order.quantity === undefined) {
                        if (typeof(quantity) === 'object') {
                            $scope.order.quantity = quantity.$viewValue;
                        }
                    }

                    var validity = orderValidationService.validateOrderQuantity($scope.order.quantity, productConfigQuantity);
                    $scope.invalidQuantityReason = validity.invalidReason;

                    if (typeof(quantity) !== 'object') {
                        if ($scope) {
                            if ($scope.editOrderForm) {
                                quantity = $scope.editOrderForm.quantity;
                            }
                        }
                    }

                    if (quantity && typeof(quantity) === 'object') {
                        quantity.$setValidity("quantity", validity.isValid);
                    }
                }
            };

            var refreshMetadataCollection = function(){
                $scope.order.metaData = productMetadataService.synchronizeMetadata($scope.order.metaData, productId);
                $scope.metaDataCollection = productMetadataService.getMetadataCollection($scope.order.metaData, productId);
            };

            refreshMetadataCollection();

            $scope.notesCharacterLimit = 250;
            $scope.notesCharactersLeft = $scope.notesCharacterLimit;
            $scope.onNotesChange = function () {

                if ($scope.order.notes.length > $scope.notesCharacterLimit) {
                    $scope.order.notes = $scope.order.notes.slice(0, $scope.notesCharacterLimit);
                }
                $scope.notesCharactersLeft = $scope.notesCharacterLimit - $scope.order.notes.length;
            };
            $scope.onNotesChange();

            $scope.submitForm = function (isValid) {
                if ($scope.order && $scope.order.notes) {
                    $scope.order.notes = $scope.order.notes.trim();
                }
            };

            $scope.$watch('order.price', function (price) {

                if (price === undefined) {
                    if ($scope) {
                        if ($scope.editOrderForm) {
                            if ($scope.editOrderForm.price) {
                                price = $scope.editOrderForm.price.$viewValue;
                            }
                        }
                    }
                }
                $scope.priceSpecification.isValid(price);
            });

            $scope.$watch('order.quantity', function (quantity) {

                if (quantity === undefined) {
                    if ($scope) {
                        if ($scope.editOrderForm) {
                            if ($scope.editOrderForm.quantity) {
                                quantity = $scope.editOrderForm.quantity.$viewValue;
                            }
                        }
                    }
                }
                $scope.quantitySpecification.isValid(quantity);
            });

            $scope.$watch('order', function (newOrder, oldOrder) {

                $scope.orderEdited = $scope.hasOrderChanged(newOrder, oldOrder);

            }, true);

            $scope.hasOrderChanged = function (newOrder, oldOrder) {

                var newOrderNotesValue,
                    notesStored = false;

                if (angular.equals($scope.originalOrderStr, null)) {

                    if ($scope.order && $scope.order.notes) {
                        $scope.order.notes = $scope.order.notes.trim();
                    }
                    $scope.originalOrderStr = getOrderJsonStr(angular.copy($scope.order));
                    $scope.modifiedOrderStr = $scope.originalOrderStr;
                }

                if (!angular.equals(newOrder, oldOrder)) {
                    if (newOrder && newOrder.notes) {
                        newOrderNotesValue = newOrder.notes;
                        newOrder.notes = newOrder.notes.trim();
                        notesStored = true;
                    }
                    $scope.modifiedOrderStr = getOrderJsonStr(newOrder);

                    if (notesStored) {
                        newOrder.notes = newOrderNotesValue;
                    }
                }

                return $scope.originalOrderStr !== $scope.modifiedOrderStr;
            };

            var isCoBrokeringDisabledForProductOrSelection = function(){
                var isDisabled = true;

                if (isCoBrokeringVisibleForProduct()) {
                    if($scope.IsBroker()) {
                        isDisabled = false;
                    }
                    else {
                        if ($scope.selectedBroker) {
                            if ($scope.selectedBroker.id > 0) {
                                isDisabled = false;
                            }
                        }
                    }
                }
                return isDisabled;
            };

            var isCoBrokeringVisibleForProduct = function(){

                var isVisible = false;

                if ($scope.productConfig) {
                    if ($scope.productConfig.coBrokering) {
                        isVisible = true;
                    }
                }
                return isVisible;
            };

            var updateCoBrokeringUIElements = function(changeMade){
                var previousCoBrokeringDisabled = $scope.coBrokeringDisabled;
                $scope.coBrokeringVisible = isCoBrokeringVisibleForProduct();
                $scope.coBrokeringDisabled = isCoBrokeringDisabledForProductOrSelection();

                if ($scope.coBrokeringVisible===true && changeMade &&
                    $scope.coBrokeringDisabled===false && previousCoBrokeringDisabled===true) {
                    $scope.order.coBrokering = true;
                }

                if($scope.coBrokeringVisible===false || $scope.coBrokeringDisabled===true) {
                    if(changeMade) {
                        $scope.order.coBrokering = false;
                    }
                }
            };

            var getOrderJsonStr = function (order) {
                var tempOrder = angular.copy(order);

                if (!tempOrder.deliveryStartDate.getFullYear) {
                    tempOrder.deliveryStartDate = new Date(tempOrder.deliveryStartDate);
                }

                if (!tempOrder.deliveryEndDate.getFullYear) {
                    tempOrder.deliveryEndDate = new Date(tempOrder.deliveryEndDate);
                }

                tempOrder.deliveryStartDate = new Date(tempOrder.deliveryStartDate.getFullYear(),
                    tempOrder.deliveryStartDate.getMonth(), tempOrder.deliveryStartDate.getDate());
                tempOrder.deliveryEndDate = new Date(tempOrder.deliveryEndDate.getFullYear(),
                    tempOrder.deliveryEndDate.getMonth(), tempOrder.deliveryEndDate.getDate());
                if (tempOrder.brokerName === null) {
                    tempOrder.brokerName = tempOrder.brokerOrganisationName;
                }

                return JSON.stringify(tempOrder).replace(/["']/g, "");
            };
        });