﻿angular.module('argusOpenMarkets.dashboard').service("assessmentService", function assessmentService(
    userService,
    helpers,
    productConfigurationService,
    messageSubscriptionService2)
{
    var assessments = [];
    var subscribersToAssessments = [];

    var notifySubscribersOfAssessmentChanges = function (assessment) {
        var assessmentInArray = assessments.getIndexBy("productId", assessment.productId);
        if (helpers.isEmpty(assessmentInArray)) {
            assessments.push(assessment);
        }
        else {
            assessments[assessmentInArray] = assessment;
        }
        angular.forEach(subscribersToAssessments, function (subscriber) {
            subscriber.action();
        });
    };

    var unregisterSubscriberForAssessments = function (subscriberId) {
        var subscriberInArray = subscribersToAssessments.getIndexBy("subscriberId", subscriberId);
        if (!helpers.isEmpty(subscriberInArray)) {
            subscribersToAssessments.splice(subscriberInArray, 1);
        }
    };

    var registerSubscriberForAssessments = function (subscriberId, action) {
        var subscriberInArray = subscribersToAssessments.getIndexBy("subscriberId", subscriberId);
        if (!helpers.isEmpty(subscriberInArray)) {
            subscribersToAssessments.splice(subscriberInArray, 1);
        }
        subscribersToAssessments.push({subscriberId: subscriberId, action: action});
    };

    var getAssessments = function () {
        return assessments;
    };

    var sendNewAssessment = function (assessment) {
        userService.sendStandardFormatMessageToUsersTopic('Assessment', 'Create', assessment);
    };

    var sendClearAssessment = function (assessment) {
        userService.sendStandardFormatMessageToUsersTopic('Assessment', 'Clear', assessment);
    };

    var sendRefreshAssessment = function (message) {
        userService.sendStandardFormatMessageToUsersTopic('Assessment', 'Refresh', message);
    };

    var getAssessmentStatusDisplayName = function (status) {
        if (status === "Final") {
            return "FINAL";
        }
        else if (status === "Corrected") {
            return "CORRECTED";
        }
        else if (status === "RunningVwa") {
            return "RUNNING VWA";
        }
        else if (status === "None") {
            return "N/A";
        }
        else {
            return "N/A";
        }
    };

    var getSubscribersToAssessmentsCount = function () {
        return subscribersToAssessments.length;
    };


    // messageSubscriptionService2.subscribeToTopicAndMessageType("AOM/Assessment", "Assessment", notifySubscribersOfAssessmentChanges);

    var assessmentProducts = productConfigurationService.getAllSubscribedProductIds();
    angular.forEach(assessmentProducts, function (productId) {
        var topic = "AOM/Products/" + productId + "/Assessment";
        messageSubscriptionService2.subscribeToTopicAndMessageType(topic, "Assessment", notifySubscribersOfAssessmentChanges);
    });

    var getPriceClass = function (ass) {
        if (ass) {
            if (ass.trendFromPreviousAssesment) {
                if (ass.trendFromPreviousAssesment.toLowerCase() === "upward") {
                    return 'assessment-item-price-up';
                } else if (ass.trendFromPreviousAssesment.toLowerCase() === "downward") {
                    return 'assessment-item-price-down';
                }
                else {
                    return 'assessment-item-price-level';
                }
            }
        }

        return 'assessment-item-price-level';
    };

    return {
        sendNewAssessment: sendNewAssessment,
        sendClearAssessment: sendClearAssessment,
        sendRefreshAssessment: sendRefreshAssessment,
        registerSubscriberForAssessments: registerSubscriberForAssessments,
        unregisterSubscriberForAssessments: unregisterSubscriberForAssessments,
        getAssessments: getAssessments,
        getAssessmentStatusDisplayName: getAssessmentStatusDisplayName,
        getSubscribersToAssessmentsCount: getSubscribersToAssessmentsCount,
        getPriceClass: getPriceClass
    };
});