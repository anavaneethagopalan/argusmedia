﻿describe( 'argusOpenMarkets.dashboard.newExternalDealController', function() {
    "use strict";
    describe( 'newExternalDealController', function() {

        var AppCtrl,
            $testController,
            $scope,
            $rootScope,
            $controller,
            newDealCtrl,
            mockUserApi = {
                getPrincipals: function () {
                    return {
                        then: function () {
                        }
                    };
                },
                getBrokers: function () {
                    return {
                        then: function () {
                        }
                    };
                }
            },
            mockUserService = {
                getUser: function(){
                    return {
                        "id":7,
                        "username":"b1",
                        "name":"Broker One",
                        "password":"5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8",
                        "email":"broker.one@broker_1.com",
                        "organisationId":5,
                        "isBlocked":false,
                        "isActive":true,
                        "isDeleted":false,"token":null,
                        "userRole":null,
                        "userOrganisation":
                            {
                                "id":5,
                                "shortCode":"BR1",
                                "name":null,
                                "legalName":"BrokerCo #1 Ltd",
                                "address":null,
                                "dateCreated":null,
                                "isBroker":false,
                                "description":null,
                                "organisationType": "Argus",
                                "isDeleted":false},
                        "userPrivileges":null,
                        "productPrivileges":[
                            {"productId":1,"privileges":["Order_Create_Broker","Order_Aggress_Broker","Order_Amend_Own","Order_Amend_OwnOrg","Deal_Create_Broker","MarketInfo_Create"]},
                            {"productId":2,"privileges":["Order_Create_Broker","Order_Aggress_Broker","Order_Amend_Own","Order_Amend_OwnOrg","Deal_Create_Broker","MarketInfo_Create"]}]};
                },

                hasProductPrivilege : function(productfId, roles){
                    return false;
                },
                hasProductPrivilegeOnAnyProduct : function() {return true;},
                getProductsWithPrivileges : function(createDealPermissions){
                    return [1, 2];
                }
            },
            metadataDefinitions = [{
                "metadataList": {
                    "fieldList": [
                        {
                            "id": 1,
                            "displayName": "Port A",
                            "itemValue": 100
                        },
                        {
                            "id": 2,
                            "displayName": "Port B",
                            "itemValue": 101
                        },
                        {
                            "id": 3,
                            "displayName": "Port c",
                            "itemValue": 102
                        }
                    ]
                },
                "id": 1,
                "productId": 1,
                "displayName": "Delivery Port",
                "fieldType": "IntegerEnum"
            }, {
                "valueMinimum": 0,
                "valueMaximum": 20,
                "id": 1,
                "productId": 2,
                "displayName": "Delivery Description",
                "fieldType": "String"
            }],
            products = [{"productId":1,"productName":"Argus Naphtha CIF NWE - Outright","productTitle":"Argus Naphtha CIF NWE - Outright","commodity":{"id":1,"name":"Naphtha"},"contractSpecification":{"volume":{"minimum":12500,"maximum":35000,"increment":500,"decimalPlaces":0,"volumeUnitCode":"MT"},"pricing":{"minimum":1,"maximum":9999,"increment":0.01,"decimalPlaces":2,"pricingUnitCode":"MT","pricingCurrencyCode":"USD"},"tenors":[{"deliveryLocation":{"id":1,"name":"Rotterdam","dateCreated":"2014-01-01T00:00:00Z"},"deliveryStartDate":"+10d","deliveryEndDate":"+25d","defaultStartDate":"+12d","defaultEndDate":"+23d","minimumDeliveryRange":3,"rollDate":"DAILY","id":0}],"contractType":"Physical"}},{"productId":2,"productName":"Argus Naphtha CIF NWE - Diff","productTitle":"Argus Naphtha CIF NWE - Diff","commodity":{"id":1,"name":"Naphtha"},"contractSpecification":{"volume":{"minimum":12500,"maximum":35000,"increment":500,"decimalPlaces":0,"volumeUnitCode":"MT"},"pricing":{"minimum":-999,"maximum":999,"increment":0.01,"decimalPlaces":2,"pricingUnitCode":"BBL","pricingCurrencyCode":"USD"},"tenors":[{"deliveryLocation":{"id":-1,"name":"-","dateCreated":"2014-01-01T00:00:00Z"},"deliveryStartDate":"+5d","deliveryEndDate":"+20d","defaultStartDate":null,"defaultEndDate":null,"minimumDeliveryRange":2, "rollDate":"DAILY","id":0}],"contractType":"Spread"}}],
            mockProductConfigurationService = {
                getAllProductConfigurations: function () {
                    return products;
                },
                getProductConfigurationById: function (productId) {
                    return products[productId - 1];
                },
                getProductConfigurationLabels: function () {
                    return "product";
                },
                getProductMetaDataById: function (productId) {
                    return {"productId": productId, "fields": [metadataDefinitions[productId - 1]]};
                }
            },
            mockModal = {open: function (){
                return true;
            }},
            mockUserApi = {
                getPrincipals: function(productId){
                    return { then: function(){ return null;} };
                },
                getBrokers: function(productId){
                    return { then: function(){ return null;} };
                }
            },
            mockDateService = {
                getDateTimeNow: function () {
                    return new Date(2016, 5, 20)
                }
            };

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );

        beforeEach(module(function($provide) {
            $provide.value('productConfigurationService', mockProductConfigurationService);
        }));

        beforeEach( inject( function( $controller, $rootScope ) {

            $scope = $rootScope.$new();
            $testController = $controller;
        }));

        it( 'should be defined', inject( function() {

            newDealCtrl = $testController
            ( 'newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {},
                    useApi: mockUserApi
                });

            expect( newDealCtrl ).toBeDefined();
        }));

        it('should return true for canSeeFreeFormDeal if the logged in user is an Argus Editor', function(){

            newDealCtrl = $testController
            ( 'newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {}
                });

            expect($scope.canSeeFreeFormDeal()).toBeTruthy();
        });

        it('should return false for canSeeFreeFormDeal if the logged in user is a Broker', function(){

            mockUserService.getUser = function() {
                return {
                    "id":7,
                    "username":"b1",
                    "name":"Broker One",
                    "password":"5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8",
                    "email":"broker.one@broker_1.com",
                    "organisationId":5,
                    "isBlocked":false,
                    "isActive":true,
                    "isDeleted":false,"token":null,
                    "userRole":null,
                    "userOrganisation":
                    {
                        "id":5,
                        "shortCode":"BR1",
                        "name":null,
                        "legalName":"BrokerCo #1 Ltd",
                        "address":null,
                        "dateCreated":null,
                        "isBroker":false,
                        "description":null,
                        "organisationType": "Brokerage",
                        "isDeleted":false}
                };
            };

            newDealCtrl = $testController
            ( 'newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {}
                });

            expect($scope.canSeeFreeFormDeal()).toBeFalsy();
        });

        it('should return false for canSeeFreeFormDeal if the logged in user is a Trader', function(){

            mockUserService.getUser = function() {
                return {
                    "id":7,
                    "username":"b1",
                    "name":"Broker One",
                    "password":"5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8",
                    "email":"broker.one@broker_1.com",
                    "organisationId":5,
                    "isBlocked":false,
                    "isActive":true,
                    "isDeleted":false,"token":null,
                    "userRole":null,
                    "userOrganisation":
                    {
                        "id":5,
                        "shortCode":"BR1",
                        "name":null,
                        "legalName":"BrokerCo #1 Ltd",
                        "address":null,
                        "dateCreated":null,
                        "isBroker":false,
                        "description":null,
                        "organisationType": "Trading",
                        "isDeleted":false}
                };
            };

            newDealCtrl = $testController
            ( 'newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {}
                });

            expect($scope.canSeeFreeFormDeal()).toBeFalsy();
        });

        it('should mark a deal as not using custom free form notes if the free form deal message is empty', function() {
            newDealCtrl = $testController('newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {}
                });
            expect($scope.deal.freeFormDealMessage).toBeFalsy();
            $scope.overrideFreeForm();
            expect($scope.deal.useCustomFreeFormNotes).toBeFalsy();
        });

        it('should mark a deal as using custom free form notes if the free form deal message is not empty', function() {
            newDealCtrl = $testController('newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {}
                });
            $scope.deal.freeFormDealMessage = "Not an empty string";
            $scope.overrideFreeForm();
            expect($scope.deal.useCustomFreeFormNotes).toBeTruthy();
        });

        it('should create metadata default values and set metadata collection at first', function(){
            newDealCtrl = $testController
            ( 'newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {}
                });

            var expectedMetadataValue = {
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId: 1,
                itemType:"IntegerEnum"
            };

            expect($scope.deal.metaData).toEqual([expectedMetadataValue]);
            expect($scope.metaDataCollection).toEqual([{
                definition: metadataDefinitions[0],
                metadataitem: expectedMetadataValue
            }]);
        });

        it('should update metadata collection when selected product was changed', function(){
            newDealCtrl = $testController
            ( 'newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {}
                });

            $scope.selectContract(products[1], { $setValidity: function(property, isValidPrice){}});

            var expectedMetadataValue = {
                productMetaDataId:1,
                displayName:"Delivery Description",
                displayValue:"",
                itemValue:null,
                metaDataListItemId:null,
                itemType:"String"
            };

            expect($scope.deal.metaData).toEqual([expectedMetadataValue]);
            expect($scope.metaDataCollection).toEqual([{
                definition: metadataDefinitions[1],
                metadataitem: expectedMetadataValue
            }]);
        });

        it('should restore metadata values when selected product was changed back', function(){
            newDealCtrl = $testController
            ( 'newExternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {}
                });

            var expectedMetadataValue = {
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port B",
                itemValue: 101,
                metaDataListItemId: 1,
                itemType:"IntegerEnum"
            };

            // Set metadata value
            $scope.deal.metaData[0] = $scope.metaDataCollection[0].metadataitem = expectedMetadataValue;
            var priceMock = { $setValidity: function(property, isValidPrice){}};
            // Switch to the second product
            $scope.selectContract(products[1], priceMock);
            // Switch back to the first product
            $scope.selectContract(products[0], priceMock);

            expect($scope.deal.metaData).toEqual([expectedMetadataValue]);
            expect($scope.metaDataCollection).toEqual([{
                definition: metadataDefinitions[0],
                metadataitem: expectedMetadataValue
            }]);
        });

        it('should set delivery date range regarding product tenor', function(){
            newDealCtrl = $testController('newExternalDealController', {
                $scope: $scope,
                $modalInstance: {},
                userApi: mockUserApi,
                $state: {},
                userService: mockUserService,
                dealService: {},
                productConfigurationService:mockProductConfigurationService,
                $filter: {},
                dateService: mockDateService
            });

            expect($scope.minDate.getTime()).toBe(new Date(2016, 5, 30).getTime());
            expect($scope.maxDate.getTime()).toBe(new Date(2016, 6, 15).getTime());
            expect($scope.deal.deliveryStartDate.getTime()).toBe(new Date(2016, 6, 2).getTime());
            expect($scope.deal.deliveryEndDate.getTime()).toBe(new Date(2016, 6, 13).getTime());
            expect($scope.minRange).toBe(3);
        });

        it('should set delivery date range after product selected', function(){
            newDealCtrl = $testController('newExternalDealController', {
                $scope: $scope,
                $modalInstance: {},
                userApi: mockUserApi,
                $state: {},
                userService: mockUserService,
                dealService: {},
                productConfigurationService:mockProductConfigurationService,
                $filter: {},
                dateService: mockDateService
            });

            $scope.selectContract($scope.contracts[1], { $setValidity: function(){}});

            expect($scope.minDate.getTime()).toBe(new Date(2016, 5, 25).getTime());
            expect($scope.maxDate.getTime()).toBe(new Date(2016, 6, 10).getTime());
            expect($scope.deal.deliveryStartDate.getTime()).toBe(new Date(2016, 5, 25).getTime());
            expect($scope.deal.deliveryEndDate.getTime()).toBe(new Date(2016, 6, 10).getTime());
            expect($scope.minRange).toBe(2);
        });
    });
});