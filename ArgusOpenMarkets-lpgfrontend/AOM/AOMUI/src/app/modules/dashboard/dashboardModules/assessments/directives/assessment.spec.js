describe('assessment directive tests', function () {
    var $scope,
        $compile,
        el,
        priceClass = '',
        mockAssessmentService = {
            getPriceClass: function(ass){
                return priceClass;
            }
        };

    beforeEach(module('src/app/modules/dashboard/dashboardModules/assessments/views/assessment.part.html'));

    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(function(){
        module(function($provide) {
            $provide.value('assessmentService', mockAssessmentService);

            $provide.provider('htmlInterceptor', function() {
                this.$get = function () {
                    return {
                        request: function (config) {
                            return config;
                        }
                    };
                }
            });
        });
    });

    beforeEach(inject(function (_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.test = {
            id: 10,
            enteredByUserId: 0,
            lastUpdatedUserId: 5062,
            dateCreated: "2014-11-24T09:27:09.702Z",
            lastUpdated: "2014-11-24T10:15:41.262Z",
            priceHigh: 788.00,
            priceLow: 788.00,
            productId: 1,
            product: null,
            businessDate: "24.11.2014",
            trendFromPreviousAssesment: "Downward"
        };
        $compile = _$compile_;
        el = angular.element('<ar-assessment assessment="test" assessmentlabel="Today"></ar-assessment>')
        $compile(el)($scope);
        $scope.$digest();

    }));

    it('should get correct price class', function () {
        priceClass = 'assessment-item-price-up';
        expect(el.scope().$$childHead.getPriceClass({trendFromPreviousAssesment: "Upward"})).toEqual('assessment-item-price-up');
    });

    it('should set the assessment label to be today', function(){

        var assessmentLabel = el.find('.assessment-label');
        expect(assessmentLabel.text()).toEqual('Today');
    });

});