﻿describe('dealServiceTests', function() {
    "use strict";

    var dealService;
    var mockDateFilter, mockNumberFilter;
    var fakeDeal;
    var fakeContract;

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.dashboard'));

    beforeEach(function () {
        module(function ($provide) {
            var $injector = angular.injector(['argusOpenMarkets.shared']);
            var mockUserService = $injector.get('mockUserService');
            $provide.value('userService', mockUserService);
        });
    });

    beforeEach(function() {
        mockDateFilter = jasmine.createSpy("mockDateFilter");
        mockNumberFilter = jasmine.createSpy("mockNumberFilter");
        fakeDeal = {};
        fakeContract = {
            contractSpecification: {
                volume: {},
                pricing: {}
            }
        }
    });

    beforeEach(inject(function (_dealService_) {
        dealService = _dealService_;
    }));

    describe('free form deal text generation', function() {

        it('should have a defined dealService', function() {
            expect(dealService).toBeDefined();
        });

        it('should format the start and end date with the dateFilter', function() {
            dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(mockDateFilter.calls.count()).toEqual(2);
        });

        it('should format the price and quantity with the numberFilter', function() {
            dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(mockNumberFilter.calls.count()).toEqual(2);
        });

        it('should include optional price detail if specified on the deal', function() {
            var priceDetail = "optional price detail";
            fakeDeal.optionalPriceDetail = priceDetail;
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(priceDetail)).not.toEqual(-1);
        });

        it('should contain the broker name if a broker is specified on the deal', function() {
            var brokerName = "a broker";
            fakeDeal.brokerName = brokerName;
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(brokerName)).not.toEqual(-1);
        });

        it('should contain the price with currency and unit', function() {
            fakeContract.contractSpecification.pricing.pricingCurrencyCode = "currencyCode";
            fakeContract.contractSpecification.pricing.pricingUnitCode = "currencyUnit";
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(fakeContract.contractSpecification.pricing.pricingCurrencyCode)).not.toEqual(-1);
            expect(text.indexOf(fakeContract.contractSpecification.pricing.pricingUnitCode)).not.toEqual(-1);
        });

        it('should contain the volume unit code', function() {
            fakeContract.contractSpecification.volume.volumeUnitCode = "volumeUnit";
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(fakeContract.contractSpecification.pricing.volumeUnitCode)).not.toEqual(-1);
        });

        it ('should contain the buyer name', function() {
            fakeDeal.buyerName = "a buyer";
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(fakeDeal.buyerName)).not.toEqual(-1);
        });

        it ('should contain the seller name', function() {
            fakeDeal.sellerName = "a seller";
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(fakeDeal.sellerName)).not.toEqual(-1);
        });

        it('should contain the contract name for a regular contract', function() {
            fakeContract.productTitle = "a product";
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(fakeContract.productTitle)).not.toEqual(-1);            
        });

        it('should contain the input contract name for the Other contract', function() {
            fakeContract.productTitle = "Other";
            fakeDeal.contractInput = "Specified Product";
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(fakeDeal.contractInput)).not.toEqual(-1);
        });

        it('should contain the input contract name for the Other - Biofuels contract', function() {
            fakeContract.productTitle = "Other - Biofuels";
            fakeDeal.contractInput = "Specified Product";
            var text = dealService.createFreeFormNotes(fakeDeal, fakeContract, mockDateFilter, mockNumberFilter);
            expect(text.indexOf(fakeDeal.contractInput)).not.toEqual(-1);
        });
    });

});