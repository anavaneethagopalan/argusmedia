﻿describe('argusOpenMarkets.dashboard.marketTickerController', function () {
    "use strict";
    describe('marketTickerController', function () {

        var $testController,
            $scope,
            $q,
            rootScope,
            mockMarketTickerService = {},
            mockUserService,
            mockDashboardConfigurationService = {},
            mockMessagePopupService = {},
            mockMarketTickerMessageSubscriptionService = {},
            mockModal,
            mockOrderRepositoryService,
            controller;

        var getSingleMarketTickerItem = function () {
            return [
                {
                    "marketTickerType": "newBid",
                    "principal": "ABC Trading",
                    "marketTickerItemStatus": "Pending",
                    "marketTickerItemType": "Info",
                    "productId": 1,
                    "id": 1,
                    "dateCreated": "2014-05-14T08:54:09.815756",
                    "ProductID": 1, "Type": "BID",
                    "ActionState": "WITHDRAWN",
                    "FormattedDate": "10:06",
                    "message": "Fire at BOC Premises in Ireland.  More to come...",
                    "ownerOrganisations": [1, 2, 3]
                },
                {
                    "marketTickerType": "newOffer",
                    "principal": "Argus Media Ltd",
                    "id": 2,
                    "productId": 1,
                    "marketTickerItemType": "Dal",
                    "orderStatus": "Active",
                    "marketTickerItemStatus": "Active",
                    "ownerOrganisations": [1, 2, 3],
                    "order": {},
                    "deal": {
                        "dealStatus": "Pending",
                        "id": 1,
                        "bid": {
                            "organisationId": 1,
                            "organisationName": "Argus Media Ltd",
                            "brokerOrganisationId": 0,
                            "brokerOrganisationName": "",
                            "price": 1000.345,
                            "deliveryStartDate": "1 Jan 2014",
                            "deliveryEndDate": "15 Jan 2014",
                            "quantity": 12000,
                            "tenor": {
                                "deliveryLocation": {
                                    "name": "Prompt"
                                }
                            }
                        },
                        "ask": {
                            "organisationId": 2,
                            "organisationName": "John Doe Oil Refinery",
                            "brokerOrganisationId": 0,
                            "brokerOrganisationName": "",
                            "price": 1000.345,
                            "deliveryStartDate": "1 Jan 2014",
                            "deliveryEndDate": "15 Jan 2014",
                            "quantity": 12000,
                            "tenor": {
                                "deliveryLocation": {
                                    "name": "Prompt"
                                }
                            }
                        }
                    }
                }
            ];
        };
        mockMarketTickerService = {
            subscribeToMarketTickerChunk: sinon.spy(),
            getMarketData: getSingleMarketTickerItem(),
            onMarketTickRegister: function () {
            }
        };

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(module('argusOpenMarkets.dashboard'));

        beforeEach(inject(function ($controller, $rootScope, $timeout, $log, _$q_, helpers) {
            mockMarketTickerService = {
                registerProductStatusChange: function (getMarketTickerStatus) {
                    var result = getMarketTickerStatus;
                },
                getMarketTickerProducts: function () {

                },
                getMarketTickerTypes: function () {

                },
                hasProductPrivilegeOnAnyProduct: function (arrayPrivileges) {
                    return true;
                },
                onMarketTickRegister: function (cb) {

                },
                onMarketTickRegisterPushedItems: function (cb) {

                },
                onMarketTickRegisterDeletedItem: function (cb) {

                },
                checkPrivilegesSendAndWait: function() {}
            };

            mockUserService = jasmine.createSpyObj('mockUserService', [
                'getUser',
                'hasProductPrivilegeOnAnyProduct',
                'hasProductPrivilege']);
            mockUserService.getUser.and.returnValue({});
            mockUserService.hasProductPrivilegeOnAnyProduct.and.returnValue(true);
            mockUserService.hasProductPrivilege.and.returnValue(true);

            mockMessagePopupService = {};
            mockMarketTickerMessageSubscriptionService = {
                registerActionOnMarketTickerMessageType: function (type, obj) {

                }
            };

            mockDashboardConfigurationService = {
                getDashboardLayout: function () {
                    return [{"view": "marketticker", tile: {"sizeY": 14}}];
                }
            };

            mockModal = jasmine.createSpyObj('mockModal', ['dismiss', 'open']);

            mockOrderRepositoryService = jasmine.createSpyObj('mockOrderRepositoryService', ['getOrderById']);

            rootScope = $rootScope;
            $q = _$q_;
            $scope = $rootScope.$new();
            $testController = $controller;

            controller = $controller('marketTickerController', {
                $scope: $scope,
                $rootScope: $rootScope,
                $modal: mockModal,
                $timeout: $timeout,
                $log: $log,
                helpers: helpers,
                marketTickerService: mockMarketTickerService,
                userService: mockUserService,
                marketTickerMessageSubscriptionService: mockMarketTickerMessageSubscriptionService,
                messagePopupsService: mockMessagePopupService,
                dashboardConfigurationService: mockDashboardConfigurationService,
                orderRepositoryService: mockOrderRepositoryService
            });
        }));

        var makeTickerItems = function (numberItems) {
            var tickerItems = [],
                tickerItem;

            for (var i = 0; i < numberItems; i++) {
                tickerItem = {};
                tickerItem.id = i + 1;
                tickerItem.updateFormattedDate = function() {};
                tickerItems.push(tickerItem);
            }

            return tickerItems;
        };

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

        it('should return 6 for the maximum number of market ticker items to display if there are only 2 market ticker items', function () {

            var numberMarketTickerItemsToDisplay = 0;

            $scope.marketTickerItems = makeTickerItems(2);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            expect(numberMarketTickerItemsToDisplay).toEqual(6);
        });

        it('should return 9 for the maximum number of market ticker items to display if there are  7 market ticker items', function () {

            var numberMarketTickerItemsToDisplay = 0;

            $scope.marketTickerItems = makeTickerItems(7);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            expect(numberMarketTickerItemsToDisplay).toEqual(9);
        });

        it('should return 12 for the maximum number of market ticker items to display if there are  11 market ticker items', function () {

            var numberMarketTickerItemsToDisplay = 0;

            $scope.marketTickerItems = makeTickerItems(11);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            expect(numberMarketTickerItemsToDisplay).toEqual(12);
        });

        it('should return 12 for the maximum number of market ticker items to display if there are  12 market ticker items', function () {

            var numberMarketTickerItemsToDisplay = 0;

            $scope.marketTickerItems = makeTickerItems(12);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            expect(numberMarketTickerItemsToDisplay).toEqual(12);
        });

        it('should return 21 for the maximum number of market ticker items to display if there are  22 market ticker items', function () {

            var numberMarketTickerItemsToDisplay = 0;

            $scope.marketTickerItems = makeTickerItems(22);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            expect(numberMarketTickerItemsToDisplay).toEqual(21);
        });

        it('should return 21 for the maximum number of market ticker items to display if there are 50 market ticker items', function () {

            var numberMarketTickerItemsToDisplay = 0;

            $scope.marketTickerItems = makeTickerItems(50);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            expect(numberMarketTickerItemsToDisplay).toEqual(21);
        });

        it('should return 6 for the maximum number of market ticker items to display if there are 0 market ticker items', function () {

            var numberMarketTickerItemsToDisplay = 0;

            $scope.marketTickerItems = makeTickerItems(0);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            expect(numberMarketTickerItemsToDisplay).toEqual(6);
        });

        it('should return true for can click down if we are currently displaying 3 market ticker items and there are 6 items in total', function () {

            var numberMarketTickerItemsToDisplay = 0,
                canClickDown = false;

            $scope.marketTickerItemsDisplayed = 3;
            $scope.marketTickerItems = makeTickerItems(6);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            canClickDown = $scope.canClickDown();
            expect(canClickDown).toBeTruthy();
        });

        it('should return false for can click down if we are currently displaying 6 market ticker items and there are 6 items in total', function () {

            var numberMarketTickerItemsToDisplay = 0,
                canClickDown = false;

            $scope.marketTickerItemsDisplayed = 6;
            $scope.marketTickerItems = makeTickerItems(6);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            canClickDown = $scope.canClickDown();
            expect(canClickDown).toBeFalsy();
        });

        it('should return true for can click up if we are currently displaying 6 market ticker items and there are 6 items in total', function () {

            var numberMarketTickerItemsToDisplay = 0,
                canClickDown = false;

            $scope.marketTickerItemsDisplayed = 6;
            $scope.marketTickerItems = makeTickerItems(6);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            canClickDown = $scope.canClickUp();
            expect(canClickDown).toBeTruthy();
        });

        it('should return false for can click up if we are currently displaying 6 market ticker items and there are 6 items in total and we are displaying 3', function () {

            var numberMarketTickerItemsToDisplay = 0,
                canClickDown = false;

            $scope.marketTickerItemsDisplayed = 3;
            $scope.marketTickerItems = makeTickerItems(6);
            numberMarketTickerItemsToDisplay = $scope.maxNumberTickerItemsToDisplay();
            canClickDown = $scope.canClickUp();
            expect(canClickDown).toBeFalsy();
        });

        it('should initialize the market ticker number of rows to 3 if the size of the widget is 15', function () {

            expect($scope.marketTickerItemsDisplayed).toEqual(3);
        });

        describe('show AOM Lite buttons', function() {
            it('should ask the user service if the user has the correct privilege for +Info', function() {
                mockUserService.hasProductPrivilegeOnAnyProduct.calls.reset();
                $scope.showExternalInfo();
                expect(mockUserService.hasProductPrivilegeOnAnyProduct).toHaveBeenCalled();
                expect(mockUserService.hasProductPrivilegeOnAnyProduct.calls.count()).toBe(1);
                var args = mockUserService.hasProductPrivilegeOnAnyProduct.calls.mostRecent().args;
                expect(args.length).toBe(1);
                expect(args[0]).toContain("MarketTicker_PlusInfo_Create");
            });
            it('should show +Info button if user has correct privilege for any product', function() {
                mockUserService.hasProductPrivilegeOnAnyProduct.and.returnValue(true);
                expect($scope.showExternalInfo()).toBeTruthy();
            });
            it('should not show +Info button if user does not have correct privilege for any product', function() {
                mockUserService.hasProductPrivilegeOnAnyProduct.and.returnValue(false);
                expect($scope.showExternalInfo()).toBeFalsy();
            });
            it('should ask the user service if the user has correct privileges for +Bid/+Ask', function() {
                mockUserService.hasProductPrivilegeOnAnyProduct.calls.reset();
                $scope.showExternalOrder();
                expect(mockUserService.hasProductPrivilegeOnAnyProduct).toHaveBeenCalled();
                expect(mockUserService.hasProductPrivilegeOnAnyProduct.calls.count()).toBe(1);
                var args = mockUserService.hasProductPrivilegeOnAnyProduct.calls.mostRecent().args;
                expect(args.length).toBe(1);
                expect(args[0]).toContain("ExternalOrder_Create_Principal");
                expect(args[0]).toContain("ExternalOrder_Create_Broker");
            });
            it('should show +Bid/+Ask if the user has correct privileges for any product', function() {
                mockUserService.hasProductPrivilegeOnAnyProduct.and.returnValue(true);
                expect($scope.showExternalOrder()).toBeTruthy();
            });
            it('should not show +Bid/+Ask if the user does not have correct privileges for any product', function() {
                mockUserService.hasProductPrivilegeOnAnyProduct.and.returnValue(false);
                expect($scope.showExternalOrder()).toBeFalsy();
            });
        });

        describe('marketTickerItemClicked', function() {
            beforeEach(function() {
                spyOn(mockMarketTickerService, "checkPrivilegesSendAndWait");
            });
            it('should not call marketTickerService when clicking on a pending item if openDialog is true', function() {
                $scope.openDialog = true;
                var fakeItem = {marketTickerItemStatus:"Pending"};
                $scope.marketTickerItemClicked(fakeItem);
                expect(mockMarketTickerService.checkPrivilegesSendAndWait).not.toHaveBeenCalled();
            });
            it('should call marketTickerService when clicking on a pending item', function(){
                $scope.openDialog = false;
                var fakeItem = {marketTickerItemStatus:"Pending"};
                $scope.marketTickerItemClicked(fakeItem);
                expect(mockMarketTickerService.checkPrivilegesSendAndWait).toHaveBeenCalled();
            });
            it('should call marketTickerService when clicking on active Deal', function(){
                $scope.openDialog = false;
                var fakeItem = {marketTickerItemStatus:"Active", marketTickerItemType:"Deal"};
                $scope.marketTickerItemClicked(fakeItem);
                expect(mockMarketTickerService.checkPrivilegesSendAndWait).toHaveBeenCalled();
            });
            it('should call marketTickerService when clicking on active Info', function(){
                $scope.openDialog = false;
                var fakeItem = {marketTickerItemStatus:"Active", marketTickerItemType:"Info"};
                $scope.marketTickerItemClicked(fakeItem);
                expect(mockMarketTickerService.checkPrivilegesSendAndWait).toHaveBeenCalled();
            });
            it('should not call marketTickerService when clicking on void Info', function(){
                $scope.openDialog = false;
                var fakeItem = {marketTickerItemStatus:"Void", marketTickerItemType:"Info"};
                $scope.marketTickerItemClicked(fakeItem);
                expect(mockMarketTickerService.checkPrivilegesSendAndWait).not.toHaveBeenCalled();
            });
            it('should not call marketTickerService when clicking on active Bid', function(){
                $scope.openDialog = false;
                var fakeItem = {marketTickerItemStatus:"Active", marketTickerItemType:"Bid"};
                $scope.marketTickerItemClicked(fakeItem);
                expect(mockMarketTickerService.checkPrivilegesSendAndWait).not.toHaveBeenCalled();
            });
            it('should not call marketTickerService when clicking on active Ask', function(){
                $scope.openDialog = false;
                var fakeItem = {marketTickerItemStatus:"Active", marketTickerItemType:"Ask"};
                $scope.marketTickerItemClicked(fakeItem);
                expect(mockMarketTickerService.checkPrivilegesSendAndWait).not.toHaveBeenCalled();
            });

            describe('external order', function() {
                var fakeMti, fakeOrder, mockModalInstance, deferred;
                beforeEach(function() {
                    fakeMti = {
                        "isMine": true,
                        "marketTickerItemType": "Bid",
                        "marketTickerItemStatus": "Active",
                        "orderId": 123
                    };
                    fakeOrder = {
                        "id": 123,
                        "productId": 234,
                        "orderType": "Bid",
                        "executionMode": "External",
                        "tenor": {"id": 345}
                    };
                    deferred = $q.defer();
                    mockModalInstance = {
                        "result": deferred.promise
                    };
                    mockOrderRepositoryService.getOrderById.and.returnValue(fakeOrder);
                    mockModal.open.and.returnValue(mockModalInstance);
                });

                it("should request order from order repository when order in ticker is clicked", function() {
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockOrderRepositoryService.getOrderById).toHaveBeenCalledWith(fakeMti.orderId);
                });
                it("should not show the edit order modal if the order is not in the repository", function() {
                    mockOrderRepositoryService.getOrderById.and.returnValue(null);
                    $scope.marketTickerItemClicked(fakeMti);
                    expect($scope.openDialog).toBeFalsy();
                });
                it("should not show the edit order modal if order is not external", function() {
                    fakeOrder.executionMode = "Internal";
                    $scope.marketTickerItemClicked(fakeMti);
                    expect($scope.openDialog).toBeFalsy();
                });
                it("should ask for amend external order privileges for order if it is external", function() {
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                        fakeOrder.productId, ["ExternalOrder_Amend"]);
                });
                it('should open edit order dialog when clicking on permissioned active order', function() {
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).toHaveBeenCalled();
                });
                it('should open edit order dialog when clicking on permissioned updated order', function() {
                    fakeMti.marketTickerItemStatus = "Updated";
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).toHaveBeenCalled();
                });
                it('should open edit order dialog when clicking on permissioned held order', function() {
                    fakeMti.marketTickerItemStatus = "Held";
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).toHaveBeenCalled();
                });
                it('should not open edit order dialog when clicking on a withdrawn order', function() {
                    // A deleted (withdrawn) order is show in the Market Ticker as an Active item with a message
                    // that says "WITHDRAWN: -". The fact that the order is deleted means that it is not returned
                    // from the order repository
                    var undefinedOrder;
                    mockOrderRepositoryService.getOrderById.and.returnValue(undefinedOrder);
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).not.toHaveBeenCalled();
                });
                it('should not open edit order dialog when clicking other users order', function() {
                    fakeMti.isMine = false;
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).not.toHaveBeenCalled();
                });
                it('should not open edit order dialog when clicking with no order permission', function() {
                    mockUserService.hasProductPrivilege.and.returnValue(false);
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).not.toHaveBeenCalled();
                });
                it('should not open edit order dialog when clicking internal order', function() {
                    fakeOrder.executionMode = "Internal";
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).not.toHaveBeenCalled();
                });
                it('should set the scope openDialog to true when modal opened', function() {
                    $scope.marketTickerItemClicked(fakeMti);
                    expect($scope.openDialog).toBeTruthy();
                });
                it('should set scope openDialog to false when modal is closed resolved', function() {
                    $scope.marketTickerItemClicked(fakeMti);
                    deferred.resolve();
                    $scope.$apply();
                    expect($scope.openDialog).toBeFalsy();
                });
                it('should set scope openDialog to false when modal is closed rejected', function() {
                    $scope.marketTickerItemClicked(fakeMti);
                    deferred.reject();
                    $scope.$apply();
                    expect($scope.openDialog).toBeFalsy();
                });
                it('should supply the order details from the modal instance resolve', function() {
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).toHaveBeenCalled();
                    expect(mockModal.open.calls.count()).toEqual(1);
                    var modalArgs = mockModal.open.calls.argsFor(0);
                    expect(modalArgs).toBeDefined();
                    expect(modalArgs.length).toEqual(1);
                    expect(modalArgs[0].resolve).toBeDefined();
                    expect(modalArgs[0].resolve.productId()).toEqual(fakeOrder.productId);
                    expect(modalArgs[0].resolve.order()).toEqual(fakeOrder);
                    expect(modalArgs[0].resolve.orderType()).toEqual(fakeOrder.orderType);
                    expect(modalArgs[0].resolve.tenorId()).toEqual(fakeOrder.tenor.id);
                });
                it('should not open the modal if scope.openDialog is true', function() {
                    $scope.openDialog = true;
                    $scope.marketTickerItemClicked(fakeMti);
                    expect(mockModal.open).not.toHaveBeenCalled();
                });
            });
        });

        describe('midnight event handling', function() {
            it('should call updateFormattedDate on each market ticker item on midnight event', function() {
                $scope.marketTickerItems = makeTickerItems(3);
                $scope.marketTickerItems.forEach(function(mti) {
                    spyOn(mti, "updateFormattedDate").and.callThrough();
                });
                rootScope.$broadcast('midnightService-midnight-event');
                $scope.marketTickerItems.forEach(function(mti) {
                    expect(mti.updateFormattedDate).toHaveBeenCalled();
                });
            });
        });

    });
});