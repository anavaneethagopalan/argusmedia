﻿angular.module('argusOpenMarkets.dashboard')
    .controller('authInternalDealController', function authInternalDealController(
        $scope,
        $modalInstance,
        userApi,
        $state,
        userService,
        dealService,
        productConfigurationService,
        $filter,
        deal,
        userContactDetails,
        productMetadataService) {
        "use strict";

        $scope.deal = deal;
        $scope.userContactDetails = userContactDetails;
        $scope.deal.deliveryStartDate = new Date(Date.parse($scope.deal.initial.deliveryStartDate));
        $scope.deal.deliveryEndDate = new Date(Date.parse($scope.deal.initial.deliveryEndDate));
        $scope.deal.buyerName =  deal.initial.orderType==='Ask' ? deal.matching.principalOrganisationName : deal.initial.principalOrganisationName ;
        $scope.deal.sellerName = deal.initial.orderType==='Ask' ? deal.initial.principalOrganisationName : deal.matching.principalOrganisationName ;
        $scope.deal.brokerName = deal.initial.brokerOrganisationName === null ? deal.matching.brokerOrganisationName === null ? "" : deal.matching.brokerOrganisationName : deal.initial.brokerOrganisationName;

        var product = productConfigurationService.getProductConfigurationById(deal.initial.productId);
        $scope.quantityLabel = "(" + product.contractSpecification.volume.volumeUnitCode + ")";
        $scope.priceLabel = "(" + product.contractSpecification.pricing.pricingCurrencyCode + "/" + product.contractSpecification.pricing.pricingUnitCode + ")";

        $scope.deal.initial.metaData = productMetadataService.synchronizeMetadata(
            $scope.deal.initial.metaData,
            $scope.deal.initial.productId);
        $scope.metaDataCollection = productMetadataService.getMetadataCollection(
            $scope.deal.initial.metaData,
            $scope.deal.initial.productId);

        $scope.$on('$stateChangeSuccess', function () {
            if ($state.current.name !== 'Dashboard') {
                $modalInstance.dismiss('cancel');
            }
        });

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.verifyDeal = function () {

            if ($scope.deal.notes) {
                // Will only be set if the user is an Argus Editor.
                $scope.deal.freeFormDealMessage = $scope.deal.freeFormDealMessage + ">>" + $scope.deal.notes;
            }

            dealService.sendVerifyInternalDealMessage($scope.deal);
            $modalInstance.dismiss('submitted');
        };
        $scope.updateDeal = function (asVerified) {

            $scope.deal.dealStatus = 'Executed'; //Effectively verifies the update.
            dealService.sendUpdateInternalDealMessage($scope.deal);
            $modalInstance.dismiss('submitted');
        };

        $scope.sendMail = function(){
            var link = "mailto:" + userContactDetails.email +
                       "?subject=Argus Open Markets - Deal Query" +
                       "&body=To " + userContactDetails.name + "," +
                              "%0D%0A%0D%0ARegarding your report of execution of a deal in Argus Open Markets, (deal reference: " + deal.id + ")." +
                              "%0D%0A%0D%0AThe reported details were: '" + $scope.deal.freeFormDealMessage + "'." +
                              "%0D%0A%0D%0AQUERYHERE" +
                              "%0D%0A%0D%0ARegards" +
                              "%0D%0A%0D%0A" + userService.getUser().name
                ;
            window.sendingEmail = true;
            window.location.href = link;
        };

        $scope.voidingMode = false;
        $scope.voidReason =
            {
                reasonText: "",
                reasonSelection: ""
            };
        $scope.setVoidReason = function( reason ){
            $scope.voidReason.reasonSelection = reason;  // value was not being updated automatically.
        };

        $scope.canVoid = function(){
            if ($scope.voidReason.reasonSelection > ""){
                if ($scope.voidReason.reasonSelection === "other"){
                    if ($scope.voidReason.reasonText==="" || $scope.voidReason.reasonText === "Enter reason"){
                        return false;
                    }
                }
                return true;
            }
            return false;
        };

        $scope.voidDeal = function () {

            $scope.voidingMode=true;
        };
        $scope.voidConfirmation = function (confirm) {

            if(confirm){
                var vr = $scope.voidReason.reasonSelection === "other" ? "other: " + $scope.voidReason.reasonText : $scope.voidReason.reasonSelection;
                deal.message = deal.message + ". \nVOID reason was '" + vr + "'";

                dealService.sendVoidInternalDealMessage(deal);
                $modalInstance.dismiss('submitted');
            } else {
                $scope.voidingMode = false;
            }

        };
    });


