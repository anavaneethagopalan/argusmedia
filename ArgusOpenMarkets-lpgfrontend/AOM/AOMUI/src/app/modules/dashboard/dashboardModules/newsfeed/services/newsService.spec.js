describe('news service tests', function() {
    "use strict";

    var newsServiceTest;
    var mockUserApi = {
        getNewsStory : sinon.stub()
    };
    var subscriber,
        onNewsDeleted;
    var mockNewsMessageSubscriptionService = {
        registerActionOnNews : function(callback){subscriber = callback},
        registerNewsItemDeleted: function(callback){ onNewsDeleted = callback},
        newNews : function (data) {
            subscriber(data);
        }
    };
    var fakeDateTimeNow, mockDateService;

    var toLocalHours = function(utcHours){
        return Math.floor(utcHours - (new Date().getTimezoneOffset() / 60));
    };

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(function () {
        fakeDateTimeNow = new Date(2015, 0, 2);
        mockDateService = jasmine.createSpyObj("mockDateService", ["getDateTimeNow"]);
        mockDateService.getDateTimeNow.and.returnValue(fakeDateTimeNow);
    });

    beforeEach(function(){
        module(function($provide) {
            $provide.value('newsMessageSubscriptionService', mockNewsMessageSubscriptionService);
            $provide.value('userApi', mockUserApi);
            $provide.value('dateService', mockDateService);
        });
    });

    beforeEach(inject(function(newsService){
        newsServiceTest = newsService;
    }));

    it('news service should subscribe to news', inject(function(){
        expect(subscriber).not.toBeNull();
    }));

    it('transform news sets publication date', inject(function(){
        var news = {story:"Test",publicationDate:"2014-11-18T18:09:31Z"};
        var result = newsServiceTest.transform(news).publicationDate.getFullYear();
        expect(result).toEqual(2014);
    }));

    it('should call news item received on new news', inject(function(){
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews",publicationDate:"2014-11-18T18:09:31Z", cmsId:2}});
        var result = newsServiceTest.getNews().length;
        expect(result).toEqual(1);
    }));

    it('should not add multiple items of the same id', inject(function(){
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews",publicationDate:"2014-11-18T18:09:31Z", cmsId:2}});
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews",publicationDate:"2014-11-18T18:09:31Z", cmsId:2}});
        var result = newsServiceTest.getNews().length;
        expect(result).toEqual(1);
    }));

    it('news multiple', inject(function(){
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews",publicationDate:"2014-11-18T18:09:31Z", cmsId:2}});
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews",publicationDate:"2014-11-18T18:09:31Z", cmsId:3}});
        var result = newsServiceTest.getNews().length;
        expect(result).toEqual(2);
    }));

    it('should put the most recent news item first in the list of news', function() {
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews2",publicationDate:"2015-01-01T18:09:31Z", cmsId:2}});
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews3",publicationDate:"2015-01-02T18:09:31Z", cmsId:3}});
        var news = newsServiceTest.getNews();
        expect(news.length).toBe(2);
        expect(news[0].cmsId).toBe(3);
        expect(news[1].cmsId).toBe(2);
    });

    it('should format a news item date to be the time if the item date is for current day', function() {
        var fakeItemDate = new Date(2015, 0, 2, 12, 23, 34);
        var formattedDate = newsServiceTest.formatDate(fakeItemDate);
        expect(formattedDate).toEqual("12:23");
    });

    it('should format a news items date to be the date if the item date is for a previous day', function() {
        var fakeItemDate = new Date(2015, 0, 1, 12, 23, 34);
        var formattedDate = newsServiceTest.formatDate(fakeItemDate);
        expect(formattedDate).toEqual("1-Jan-2015");
    });

    it('should format a news item date to be the date if the current date changes', function() {
        var fakeItemDate = new Date(2015, 0, 2, 12, 23, 34);
        var formattedDate = newsServiceTest.formatDate(fakeItemDate);
        expect(formattedDate).toEqual("12:23");
        mockDateService.getDateTimeNow.and.returnValue(new Date(2015, 1, 3, 10, 0, 0));
        formattedDate = newsServiceTest.formatDate(fakeItemDate);
        expect(formattedDate).toEqual("2-Jan-2015");
    });

    it('should leave formatted dates unchanged if updateAllFormattedDates called on same day', function() {
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews2",publicationDate:"2015-01-01T18:09:31Z", cmsId:2}});
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews3",publicationDate:"2015-01-02T18:09:31Z", cmsId:3}});
        expect(newsServiceTest.getNews().length).toBe(2);
        expect(newsServiceTest.getNews()[0].formattedDate).toBe(18 + ":09");
        expect(newsServiceTest.getNews()[1].formattedDate).toBe("1-Jan-2015");
        mockDateService.getDateTimeNow.and.returnValue(new Date(2015, 0, 2, 19, 0, 0));
        newsServiceTest.updateAllFormattedDates();
        expect(newsServiceTest.getNews()[0].formattedDate).toBe(18 + ":09");
        expect(newsServiceTest.getNews()[1].formattedDate).toBe("1-Jan-2015");
    });

    it('should update formatted dates if updateAllFormattedDates called on different day', function() {
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews2",publicationDate:"2015-01-01T18:09:31Z", cmsId:2}});
        mockNewsMessageSubscriptionService.newNews({news:{story:"TestNews3",publicationDate:"2015-01-02T18:09:31Z", cmsId:3}});
        expect(newsServiceTest.getNews().length).toBe(2);
        expect(newsServiceTest.getNews()[0].formattedDate).toBe(18 + ":09");
        expect(newsServiceTest.getNews()[1].formattedDate).toBe("1-Jan-2015");
        mockDateService.getDateTimeNow.and.returnValue(new Date(2015, 0, 3, 1, 0, 0));
        newsServiceTest.updateAllFormattedDates();
        expect(newsServiceTest.getNews()[0].formattedDate).toBe("2-Jan-2015");
        expect(newsServiceTest.getNews()[1].formattedDate).toBe("1-Jan-2015");
    });
});
