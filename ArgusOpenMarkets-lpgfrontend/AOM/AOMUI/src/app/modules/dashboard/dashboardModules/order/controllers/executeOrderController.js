﻿angular.module('argusOpenMarkets.dashboard')
    .controller('executeOrderController',
    function executeOrderController($scope,
                                    $state,
                                    productConfigurationService,
                                    $modalInstance,
                                    order,
                                    orderType,
                                    productId,
                                    userService,
                                    userOrderService,
                                    userApi,
                                    productMessageSubscriptionService,
                                    helpers,
                                    productMetadataService)
    {
        "use strict";
        $scope.$on('$stateChangeSuccess', $scope.onStateChangeSuccess);

        var handleBidAskDeleted = function(orderDeletedTopic){
            var id = 0;

            var parts = orderDeletedTopic.split('/');
            id = parseInt(parts[parts.length - 1]);

            if($scope.order){
                if($scope.order.id === id){
                    $scope.orderDeleted = true;
                }
            }
        };

        var handleBidAskUpdated = function(order){
            if($scope.order){
                if($scope.order.id === order.id){
                    $scope.updatedOrder = order;
                    $scope.orderChanged = true;
                }
            }
        };

        var onBidEventHandler = function(orderMessage){
            handleBidAskUpdated(orderMessage.order);
        };

        var onAskEventHandler = function(orderMessage){
            // TODO: Handle updates to Asks.
            handleBidAskUpdated(orderMessage.order);
        };

        var onOrderDeletedEventHandler = function(orderDeleted){
            handleBidAskDeleted(orderDeleted);
        };

        var setOrder = function(orderToSet){

            order = orderToSet;
            $scope.order = angular.copy(orderToSet);
            $scope.orderType = orderType;

            $scope.orderChanged = false;
            $scope.orderDeleted = false;

            $scope.order.metaData = productMetadataService.synchronizeMetadata($scope.order.metaData, productId);
            $scope.metaDataCollection = productMetadataService.getMetadataCollection($scope.order.metaData, productId);
        };

        $scope.refreshOrder = function(){
            if($scope.updatedOrder){
                setOrder($scope.updatedOrder);
            }
        };

        $scope.onStateChangeSuccess = function () {
            if($state.current.name !== 'Dashboard'){
                $modalInstance.dismiss('cancel');
            }
        };

        $scope.$on('$destroy', function() {
            productMessageSubscriptionService.unregisterActionOnProductMessageType("Execute Order Controller " + $scope.order.productId,  $scope.order.productId, "onBid");
            productMessageSubscriptionService.unregisterActionOnProductMessageType("Execute Order Controller " + $scope.order.productId, $scope.order.productId, "onAsk");
            productMessageSubscriptionService.unregisterActionOnProductMessageType("Execute Order Controller " + $scope.order.productId, $scope.order.productId, "onOrderDeleted");
        });


        $scope.scope = $scope;
        $scope.executeEnabled = true;

        $scope.IsBroker = function(){
            return userService.isBroker();
        };

        $scope.CanSelectAPrincipal = function(){

            if($scope.order.nonTradeable) {
                return false;
            }

            if(!$scope.IsBroker() ) {
                return false;
            }

            if(order.coBrokering){
                return true;
            }else {
                return (order.brokerDisplayName === 'Any Broker' || iAmTheBroker());
            }
        };

        $scope.UserOrganisationName = function(){
            return helpers.getOrganisationNameAndCode(userService.getUsersOrganisation());
        };

        $scope.OrderHasBrokerRestriction = function() {
            return (order.brokerOrganisationId !== null || order.brokerDisplayName === 'No Broker');
        };

        $scope.ShowBrokerOnInitialOrder = function(){
          return $scope.order.brokerOrganisationId > 0 ;
        };

        $scope.WillBeFourWayDeal= function(){
            return $scope.coBrokeredOrder && (
                ($scope.IsBroker() && order.brokerOrganisationId !== userService.getUsersOrganisation().id) ||
                (!$scope.IsBroker() && order.principalOrganisationId !== userService.getUsersOrganisation().id)
            );
        };

        $scope.ShowExecutingBroker = function(){

            if($scope.order.nonTradeable) {
                return false;
            }

            if(iAmTheBroker()) {
                return false; //don't show twice
            }

            if($scope.OrderHasBrokerRestriction()) {
                return $scope.coBrokeredOrder;
            }
            return true;
        };

        $scope.CanSelectExecutingBroker = function(){
            return $scope.ShowExecutingBroker() && !$scope.IsBroker();
        };

        var iAmTheBroker = function(){
            return (order.brokerOrganisationId === userService.getUsersOrganisation().id);
        };

        $scope.orderHasPrincipal = function(){
            return order.principalOrganisationId !== null;
        };

        $scope.princLabel1 = function(){
            if($scope.orderType === "Bid"){
                return "Buyer";
            }
            else{
                return "Seller";
            }
        };

        $scope.princLabel2 = function(){
            if($scope.orderType === "Bid"){
                return "Seller";
            }
            else{
                return "Buyer";
            }
        };

        $scope.removeCoBrokeredBrokerOnOriginalOrder = function(brokers, existingBrokerOnOrderId) {

            var indexToDelete = -1;

            for (var i = 0; i < brokers.length; i++) {
                if (brokers[i].id === existingBrokerOnOrderId) {
                    // This broker is in the list of brokers - but exist(s) on the exist order - we need to remove from the list.
                    indexToDelete = i;
                    break;
                }
            }

            if (indexToDelete > -1) {
                brokers.splice(indexToDelete, 1);
            }

            return brokers;
        };

        setOrder(order);
        productMessageSubscriptionService.registerActionOnProductMessageType("Execute Order Controller " + $scope.order.productId,  $scope.order.productId, "onBid", onBidEventHandler);
        productMessageSubscriptionService.registerActionOnProductMessageType("Execute Order Controller " + $scope.order.productId, $scope.order.productId, "onAsk", onAskEventHandler);
        productMessageSubscriptionService.registerActionOnProductMessageType("Execute Order Controller " + $scope.order.productId, $scope.order.productId, "onOrderDeleted", onOrderDeletedEventHandler);
        $scope.productLabels = productConfigurationService.getProductConfigurationLabels(order.productId);


        // Get the information for the modal dropdowns
        var buyOrSell = $scope.order.orderType === "Bid" ? "Sell":"Buy";
        if ($scope.IsBroker()) {
            $scope.executeEnabled = false; //need to select a principal
            userApi.getCommonPermissionedOrganisations(productId, $scope.order.principalOrganisationId, $scope.order.brokerOrganisationId, buyOrSell).then(
                function (data) {
                    $scope.principals = data;
                },
                function (error) {
                    userApi.formatErrorResponse(error);
                    $scope.exit();
                }
            );
        }
        else {
            userApi.getCommonPermissionedOrganisations(productId, $scope.order.principalOrganisationId, $scope.order.brokerOrganisationId, buyOrSell).then(
                function(brokers) {
                    if($scope.order.coBrokering) {
                        brokers = $scope.removeCoBrokeredBrokerOnOriginalOrder(brokers, $scope.order.brokerOrganisationId);
                    }

                    $scope.brokers = brokers;
                },
                function (error) {
                    userApi.formatErrorResponse(error);
                    $scope.exit();
                }
            );
        }

        $scope.onPrincipalChange = function(opt, principalsSelector){
            if(opt === null){
                $scope.executeEnabled = false;
            }else{
                $scope.executeEnabled = true;
            }
            $scope.selectedPrincipal = opt;
        };

        $scope.onBrokerChange = function(opt){
            $scope.selectedBroker = opt;
        };

        //location
        if(order.tenor.deliveryLocation.name) {
            $scope.deliveryLocation = order.tenor.deliveryLocation.name;
        }else{
            $scope.deliveryLocation = "Rotterdam";
        }

        //order Type
        $scope.setOrderType = function(orderType){
            $scope.orderType = orderType;
        };

        //date slider
        var startDateOffset = parseInt($scope.order.tenor.deliveryStartDate);
        var today = new Date();
        $scope.minDate = today.addDays(startDateOffset);
        var maxdate = new Date();
        $scope.maxDate = maxdate.addDays(parseInt($scope.order.tenor.deliveryEndDate));
        var startDay = new Date(moment($scope.order.deliveryStartDate));
        var endDay = new Date(moment($scope.order.deliveryEndDate));
        $scope.order.deliveryStartDate = startDay;
        $scope.order.deliveryEndDate = endDay;
        $scope.minRange = $scope.order.tenor.minimumDeliveryRange;

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.executeButtonDisabled = function(){

            if($scope.order){
                if($scope.order.nonTradeable){
                    // The execute button will always be disabled.
                    return true;
                }
            }

            if($scope.orderChanged || $scope.orderDeleted){
                // Cannot execute order as the order has changed or been deleted (killed).
                return true;
            }

            return !$scope.executeEnabled;
        };

        $scope.showEditMode =function(){
            if(userService.hasProductPrivilege(productId,["Order_Create_Any","Order_Create_Broker"]) && userOrderService.isOrderMine($scope.order)){
                return true;
            }
            return false;
        };

        $scope.switchToEditMode = function (){
            $modalInstance.dismiss('edit');
        };

        $scope.executeOrder = function(principalsSelector) {

            if($scope.IsBroker()) {
                if($scope.selectedPrincipal) {
                    order.executionInfo = { principalOrganisationId: $scope.selectedPrincipal.id, brokerOrganisationId:userService.getUsersOrganisation().id};
                }
                else {
                    order.executionInfo = { brokerOrganisationId:userService.getUsersOrganisation().id};
                }

            }else {
                if($scope.selectedBroker) {
                    order.executionInfo = { principalOrganisationId: userService.getUsersOrganisation().id, brokerOrganisationId:$scope.selectedBroker.id};
                }
                else {
                    order.executionInfo = { principalOrganisationId: userService.getUsersOrganisation().id};
                }
            }

            order.OrderType = orderType;
            order.productId = productId;

            userOrderService.sendExecuteOrderMessage(order);
            $modalInstance.dismiss('exit');
        };

        var coBrokeredOrder = function(){

            var productConfig = productConfigurationService.getProductConfigurationById(order.productId);
            if(productConfig.coBrokering){
                if(order.coBrokering){
                    return true;
                }
            }

            return false;
        };

        $scope.coBrokeredOrder = coBrokeredOrder();


        $scope.productConfig = angular.copy(productConfigurationService.getProductConfigurationById(productId));
    });