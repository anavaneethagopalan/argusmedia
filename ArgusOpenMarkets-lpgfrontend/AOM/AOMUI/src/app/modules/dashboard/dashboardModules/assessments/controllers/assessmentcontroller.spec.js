﻿describe( 'Assessments tests', function() {
    describe( 'assessmentController tests', function() {
        var $scope,productConfigurationService;
        var mockModalInstance = {result: {then: function(a,b){}}};
        var mockModal = {
            open: function (a) {return mockModalInstance;}
        };
        var assessmentService = {
            getAssessments: function(){return []},
            registerSubscriberForAssessments:sinon.spy()
        };

        beforeEach(module('ui.router', function ($locationProvider) {
            locationProvider = $locationProvider;
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );

        beforeEach( inject( function( $controller, $rootScope, helpers, mockProductConfigurationService ) {
            var $injector = angular.injector([ 'argusOpenMarkets.shared' ]);
            var mockUserService = $injector.get( 'mockUserService' );
            mockUserService.setUser(mockUserService.MOCK_USER_AE1);
            $scope = $rootScope.$new();
            productConfigurationService = mockProductConfigurationService;
            $scope.assCtrl = $controller( 'assessmentController', { $scope: $scope, $modal: mockModal, helpers: helpers, userService: mockUserService, assessmentService:assessmentService, productConfigurationService: mockProductConfigurationService});
        }));

        it( 'should be defined', inject( function() {
            expect( $scope.assCtrl ).toBeDefined();
        }));

        it('should contain a title of Assessments', function(){
            expect($scope.Title).toEqual("Assessments");
        });

        it( 'should get the name of the product', inject( function() {
            spyOn(productConfigurationService,'getProductConfigurationById').and.returnValue({productName : "Apples"});
            expect($scope.getProductName(1) ).toEqual("Apples");
        }));


        describe('assessment items', function(){
            it('should contain no assessment items', function(){
                expect($scope.assessments).toBeDefined();
                expect($scope.assessments.length).toEqual(0);
            });

            var assessments = [
                {
                    "productId": 1,
                    "assessment": {
                        "today": {
                            "id": 3,
                            "enteredByUserId": -1,
                            "lastUpdatedUserId": -1,
                            "dateCreated": "2015-05-05T09:58:11.308Z",
                            "lastUpdated": "2015-05-05T10:58:11.309Z",
                            "priceHigh": 0,
                            "priceLow": 0,
                            "productId": 1,
                            "product": null,
                            "businessDate": "2015-05-04T23:00:00Z",
                            "trendFromPreviousAssesment": "Downward",
                            "assessmentStatus": "None"
                        },
                        "previous": {
                            "id": 2,
                            "enteredByUserId": 1,
                            "lastUpdatedUserId": 5062,
                            "dateCreated": "2000-01-01T13:29:08Z",
                            "lastUpdated": "2000-01-01T14:37:48Z",
                            "priceHigh": 789.5,
                            "priceLow": 680,
                            "productId": 1,
                            "product": null,
                            "businessDate": "2000-01-02T00:00:00Z",
                            "trendFromPreviousAssesment": "Upward",
                            "assessmentStatus": "RunningVwa"
                        },
                        "productId": 1
                    },
                    "eventName": "AomNewAssessmentResponse",
                    "message": {
                        "messageType": "Assessment",
                        "messageAction": "Update",
                        "messageBody": null
                    },
                    "topicPath": "AOM/Products/1/Assessment"
                }
            ];

            var assessmentsWithEntitlements = [
                {
                    "productId": 3,
                    "assessment": {
                        "today": {
                            "id": 3,
                            "enteredByUserId": -1,
                            "lastUpdatedUserId": -1,
                            "dateCreated": "2015-05-05T09:58:11.308Z",
                            "lastUpdated": "2015-05-05T10:58:11.309Z",
                            "priceHigh": 0,
                            "priceLow": 0,
                            "productId": 1,
                            "product": null,
                            "businessDate": "2015-05-04T23:00:00Z",
                            "trendFromPreviousAssesment": "Downward",
                            "assessmentStatus": "None"
                        },
                        "previous": {
                            "id": 2,
                            "enteredByUserId": 1,
                            "lastUpdatedUserId": 5062,
                            "dateCreated": "2000-01-01T13:29:08Z",
                            "lastUpdated": "2000-01-01T14:37:48Z",
                            "priceHigh": 789.5,
                            "priceLow": 680,
                            "productId": 1,
                            "product": null,
                            "businessDate": "2000-01-02T00:00:00Z",
                            "trendFromPreviousAssesment": "Upward",
                            "assessmentStatus": "RunningVwa"
                        },
                        "productId": 3
                    },
                    "eventName": "AomNewAssessmentResponse",
                    "message": {
                        "messageType": "Assessment",
                        "messageAction": "Update",
                        "messageBody": null
                    },
                    "topicPath": "AOM/Products/1/Assessment"
                }
            ];

            it('should return no assessment items', function(){
                var assessments = $scope.filterAssessmentsForProductHasAssessment(assessments);

                expect($scope.assessments).toBeDefined();
                expect($scope.assessments.length).toEqual(0);
            });

            it('should return a single assessment item', function(){
                var result = $scope.filterAssessmentsForProductHasAssessment(assessmentsWithEntitlements);

                expect(result).toBeDefined();
                expect(result.length).toEqual(1);
            });
        });

        describe('assessment logic', function(){
            it("new assessment opens modal", inject(function () {
                spyOn(mockModal, 'open').and.callThrough();
                $scope.openAssessmentDialog();
                expect(mockModal.open).toHaveBeenCalled();
            }));

        });
    });
});