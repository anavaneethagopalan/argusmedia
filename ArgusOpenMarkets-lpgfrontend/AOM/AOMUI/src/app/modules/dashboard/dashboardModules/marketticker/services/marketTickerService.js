﻿angular.module('argusOpenMarkets.dashboard').factory("marketTickerService",
    function marketTickerService($q,
                                 $timeout,
                                 userService,
                                 helpers,
                                 marketTickerMessageSubscriptionService,
                                 productConfigurationService) {
        "use strict";

        var marketTickerItems = [];
        var marketTickerTypes = [
            {itemType: "Info", name: "INFO", ticked: true},
            {itemType: "Deal", name: "DEAL", ticked: true},
            {itemType: "Bid", name: "BID", ticked: true},
            {itemType: "Ask", name: "ASK", ticked: true}
        ];
        var onMarketTickAction;
        var onMarketTickPushedItemsAction;
        var onMarketTickerItemDeletedAction;
        var productIdsAndRoles = [];
        var marketTickerProducts = angular.copy(productConfigurationService.getAllProductConfigurations());

        var onMarketTickerItemDeleted = function (marketTickerId) {
            var mtId = parseInt(marketTickerId),
                mtItemIndex;

            mtItemIndex = marketTickerItems.getIndexBy("id", mtId);
            if (typeof mtItemIndex !== "undefined") {
                marketTickerItems.splice(mtItemIndex, 1);
                // We have deleted a market ticker item - only happens when state changes from pending to Approved / Void etc.
                onMarketTickerItemDeletedAction(marketTickerId);
            }
        };

        marketTickerMessageSubscriptionService.registerMarketTickerItemDeleted(onMarketTickerItemDeleted);


        angular.forEach(marketTickerProducts, function (p) {
            if (helpers.isEmpty(p.ticked)) {
                p.ticked = true;
            }
        });

        angular.forEach(marketTickerProducts, function (p, k) {
            productIdsAndRoles.push({
                id: p.productId,
                isEditor: userService.hasProductPrivilege(p.productId, ["MarketTicker_Authenticate"])
            });
        });

        var onProductStatusChange = [];
        var registerProductStatusChange = function (action) {
            onProductStatusChange.push(action);
        };

        var onProductConfigChange = function (configItems) {

            marketTickerProducts = configItems;
            productIdsAndRoles = [];

            angular.forEach(marketTickerProducts, function (p) {
                if (helpers.isEmpty(p.ticked)) {
                    p.ticked = true;
                }
            });
            angular.forEach(marketTickerProducts, function (p, k) {
                productIdsAndRoles.push({
                    id: p.productId,
                    isEditor: userService.hasProductPrivilege(p.productId, ["MarketTicker_Authenticate"])
                });
            });

            angular.forEach(onProductStatusChange, function (action) {
                action();
            });
        };
        productConfigurationService.registerForProductConfigurationChanges(onProductConfigChange);


        var addItem = function (newTickerItem) {
            var pushNewItem = true,
                marketTickerCounter,
                lengthMarketTickerItems,
                callMarketTickAction = true;

            for (marketTickerCounter = 0, lengthMarketTickerItems = marketTickerItems.length; marketTickerCounter < lengthMarketTickerItems; ++marketTickerCounter) {
                var mti = marketTickerItems[marketTickerCounter];

                // lastUpdated is stored as a string not a Date, so it's OK to compare with ===
                if (newTickerItem.id === mti.id) {
                    callMarketTickAction = false;
                    pushNewItem = false;  // don't duplicate
                    break;
                } else if (newTickerItem.id > mti.id || newTickerItem.lastUpdated > mti.lastUpdated) {
                    marketTickerItems.splice(marketTickerCounter, 0, newTickerItem); // add new item so newest is at front
                    pushNewItem = false;
                    break;
                }
            }

            if (pushNewItem) {
                // Page Down Event - captures adding to bottom of array.
                marketTickerItems.push(newTickerItem);
            }

            if (callMarketTickAction) {
                onMarketTickPushedItemsAction(newTickerItem, marketTickerCounter, marketTickerItems);
            }
        };

        var processTickerMessage = function (newItem) {
            deferedHasData.resolve();
            var newTickerItem = newItem.marketTickerItem;
            addItem(newTickerItem);
        };


        var onMarketTickRegister = function (onMarketTick) {
            onMarketTickAction = onMarketTick;
            marketTickerMessageSubscriptionService.registerActionOnMarketTickerMessageType('MarketTickerItem', processTickerMessage);
            onMarketTick(marketTickerItems);
        };

        var onMarketTickRegisterDeletedItem = function (onMarketTickDeletedItem) {
            onMarketTickerItemDeletedAction = onMarketTickDeletedItem;
        };

        var onMarketTickRegisterPushedItems = function (onMarketTickPushedItems) {
            onMarketTickPushedItemsAction = onMarketTickPushedItems;
        };

        var checkPrivilegesSendAndWait = function (item) {
            var checkPrivileges = function (productIds, privRequired) {

                if (productIds === null || productIds.length === 0) {
                    return (userService.hasProductPrivilegeOnAnyProduct(privRequired));
                } else {
                    return userService.hasAnyProductPrivileges(productIds, privRequired);
                }
            };

            switch (item.marketTickerItemType) {
                case "Deal":
                    if (checkPrivileges(item.productIds, ["Deal_Authenticate"])) {
                        if (item.dealId === null) {
                            userService.sendStandardFormatMessageToUsersTopic('ExternalDeal', 'Request', {
                                "id": item.externalDealId,
                                "productId": item.productIds[0]
                            });
                        } else {
                            userService.sendStandardFormatMessageToUsersTopic('Deal', 'Request', {
                                "id": item.dealId,
                                "initial": {"productId": item.productIds[0]}
                            });
                        }
                    }
                    break;
                case "Info":
                    if (item.marketInfoId && checkPrivileges(item.productIds, ["MarketTicker_Authenticate"])) {
                        userService.sendStandardFormatMessageToUsersTopic('Info', 'Request', {"id": item.marketInfoId});
                    }
                    break;
                default:
                    return false;
            }
        };


        var deferedHasData = $q.defer();
        var hasData = deferedHasData.promise;


        return {
            onMarketTickRegister: onMarketTickRegister,
            onMarketTickRegisterPushedItems: onMarketTickRegisterPushedItems,
            onMarketTickRegisterDeletedItem: onMarketTickRegisterDeletedItem,
            hasData: hasData,
            getMarketTickerProducts: function () {
                return marketTickerProducts;
            },
            getMarketTickerTypes: function () {
                return marketTickerTypes;
            },
            registerProductStatusChange: registerProductStatusChange,
            checkPrivilegesSendAndWait: checkPrivilegesSendAndWait
        };
    });
