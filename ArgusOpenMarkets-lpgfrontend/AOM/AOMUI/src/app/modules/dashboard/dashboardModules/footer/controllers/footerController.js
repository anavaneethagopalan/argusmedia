angular.module('argusOpenMarkets.dashboard')
    .controller('footerController', function footerController(
        $scope)
    {
        $scope.copyrightYear = new Date().getFullYear();
    });