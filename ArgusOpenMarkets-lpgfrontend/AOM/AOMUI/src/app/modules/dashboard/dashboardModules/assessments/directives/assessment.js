angular.module('argusOpenMarkets.dashboard').directive('arAssessment', function (assessmentService) {
    return {
        scope: {
            assessment: '=',
            assessmentlabel: '@'
        },
        templateUrl: 'src/app/modules/dashboard/dashboardModules/assessments/views/assessment.part.html',
        replace: true,
        restrict: 'E',
        controller: function ($scope) {
            $scope.getPriceClass = function (ass) {
                return assessmentService.getPriceClass(ass);
            };

        },
        link: function (scope, elem, attrs) {
        }
    };
});
