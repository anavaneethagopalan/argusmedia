/**
 * Created by nathanb on 23/04/2015.
 */
describe('argusOpenMarkets.dashboard.newExternalMarketInfoController', function() {
    "use strict";
    describe('newExternalMarketInfoController tests', function() {

        var scope, testController;
        var mockModal, mockMarketInfoService, mockProductConfigurationService;

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );

        beforeEach( inject( function( $controller,
                                      $rootScope,
                                      helpers,
                                      _mockProductConfigurationService_) {
            mockModal = jasmine.createSpyObj('mockModal', ['dismiss']);
            mockMarketInfoService = jasmine.createSpyObj('mockMarketInfoService', [
                'sendNewMarketInfoMessage',
                'processMarketInfoText',
                'canCreateVerified',
                'getTickedProductIds']);
            scope = $rootScope.$new();
            mockProductConfigurationService = _mockProductConfigurationService_;
            testController = $controller('newExternalMarketInfoController', {
                $scope: scope,
                $modalInstance: mockModal,
                helpers: helpers,
                marketInfoService: mockMarketInfoService,
                productConfigurationService: mockProductConfigurationService
            });
        }));

        it('should have a defined controller', function() {
            expect(testController).toBeDefined();
        });
        it('should call modal dismiss on exit', function() {
            expect(scope.exit).toBeDefined();
            scope.exit();
            expect(mockModal.dismiss).toHaveBeenCalledWith('exit');
        });
        it('should call market info service for canCreateVerified', function() {
            mockMarketInfoService.canCreateVerified.and.returnValue(false);
            expect(scope.canCreateVerified()).toEqual(false);
            expect(mockMarketInfoService.canCreateVerified).toHaveBeenCalled();
            mockMarketInfoService.canCreateVerified.and.returnValue(true);
            expect(scope.canCreateVerified()).toEqual(true);
        });

        describe('product ticking tests', function() {
            it('calling infoChanged should return infoValid of false if just the market info text is specified and all the products have been deselected', function() {
                var testText = 'Hello';
                mockMarketInfoService.processMarketInfoText.and.returnValue({
                    infoValid: true,
                    text: testText,
                    charactersLeft: scope.characterLimit-testText.length
                });
                mockMarketInfoService.getTickedProductIds.and.returnValue([]);
                scope.info = {marketInfoText: testText};
                scope.infoChanged();
                expect(scope.infoValid).toBeFalsy();
            });
            it('should default all products in the products drop down to ticked', function() {
                var allProducts = mockProductConfigurationService.getAllProductConfigurations();
                expect(scope.marketTickerInfoProducts.length).toEqual(allProducts.length);
                for (var i = 0; i < scope.marketTickerInfoProducts.length; ++i) {
                    expect(scope.marketTickerInfoProducts[i].ticked).toBeTruthy();
                }
            });
        });

        describe('Info text tests', function() {
            beforeEach(function() {
                mockMarketInfoService.getTickedProductIds.and.returnValue([1]);
            });
            it('should call the marketInfoService when the market info text changes', function() {
                mockMarketInfoService.processMarketInfoText.and.returnValue({
                    infoValid: false,
                    text: '',
                    charactersLeft: scope.characterLimit
                });
                scope.info.marketInfoText = '';
                scope.infoChanged();
                expect(mockMarketInfoService.processMarketInfoText).toHaveBeenCalledWith(
                    scope.info.marketInfoText, scope.characterLimit);
            });
            it('should set info text values to the returned values from the service', function() {
                mockMarketInfoService.processMarketInfoText.and.returnValue({
                    infoValid: false,
                    text: '',
                    charactersLeft: scope.characterLimit
                });
                scope.infoChanged();
                expect(scope.infoValid).toEqual(false);
                expect(scope.charactersLeft).toEqual(scope.characterLimit);
                expect(scope.info.marketInfoText).toEqual('');
                mockMarketInfoService.processMarketInfoText.and.returnValue({
                    infoValid: true,
                    text: 'abc',
                    charactersLeft: scope.characterLimit-3
                });
                scope.infoChanged();
                expect(scope.infoValid).toEqual(true);
                expect(scope.charactersLeft).toEqual(scope.characterLimit-3);
                expect(scope.info.marketInfoText).toEqual('abc');
            });
        });

        describe('Create new Info tests', function() {
            it('should call the market info service when newInfo called', function() {
                scope.newInfo('');
                expect(mockMarketInfoService.sendNewMarketInfoMessage).toHaveBeenCalled();
            });
            it('should send the correct status and info when calling market info service', function() {
                scope.info.marketInfoText = 'abc';
                scope.newInfo('status');
                expect(mockMarketInfoService.sendNewMarketInfoMessage).toHaveBeenCalled();
                var callArgs = mockMarketInfoService.sendNewMarketInfoMessage.calls.argsFor(0);
                expect(callArgs.length).toBeGreaterThan(0);
                var callArg = callArgs[0];
                expect(callArg).toBeDefined();
                expect(callArg).not.toBeNull();
                expect(callArg.marketInfoStatus).toEqual('status');
                expect(callArg.info).toEqual('abc');
            });
            it('should send the ticked product ids only when calling market info service', function() {
                var testProductIds = [2, 4, 6];
                mockMarketInfoService.getTickedProductIds.and.returnValue(testProductIds);
                scope.newInfo('status3');
                expect(mockMarketInfoService.sendNewMarketInfoMessage).toHaveBeenCalled();
                var callArgs = mockMarketInfoService.sendNewMarketInfoMessage.calls.argsFor(0);
                expect(callArgs.length).toBeGreaterThan(0);
                var callArg = callArgs[0];
                expect(callArg).toBeDefined();
                expect(callArg).not.toBeNull();
                expect(callArg.productIds).toEqual(testProductIds);
            });
            it('should dismiss the modal after sending the message', function() {
                scope.newInfo('');
                expect(mockModal.dismiss).toHaveBeenCalledWith('submitted');
            });
        });

        describe('market info type of bid ask or deal should', function(){

            it('should set drop down select mode to single if market info is of type bid', function(){
                scope.setMarketInfoType('bid');
                expect(scope.dropDownSelectMode).toEqual('single');
            });

            it('should set drop down button label to productName if market info is of type bid', function(){
                scope.setMarketInfoType('bid');
                expect(scope.dropDownButtonLabel).toEqual('productName');
            });

            it('should set the max DropDown Labels Display to 1 if the market info type is bid', function(){
                scope.setMarketInfoType('bid');
                expect(scope.maxDropDownLabelsDisplay).toEqual(1);
            });

            it('should set drop down button label to an empty string if market info is of type info', function(){
                scope.setMarketInfoType('info');
                expect(scope.dropDownButtonLabel).toEqual('');
            });

            it('should set the max DropDown Labels Display to 0 if the market info type is info', function(){
                scope.setMarketInfoType('info');
                expect(scope.maxDropDownLabelsDisplay).toEqual(0);
            });

            it('should set drop down select mode to multiple if market info is of type info', function(){
                scope.setMarketInfoType('info');
                expect(scope.dropDownSelectMode).toEqual('multiple');
            });

            it('should default all products to be ticked when controller initialised', function(){

                for (var i = 0; i < scope.marketTickerInfoProducts.length; ++i) {
                    expect(scope.marketTickerInfoProducts[i].ticked).toBeTruthy();
                }

                expect(i).toBeGreaterThan(0);
            });
        });
    });
});

