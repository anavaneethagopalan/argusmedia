﻿angular.module('argusOpenMarkets.dashboard')
    .controller('authExternalMarketInfoController', function authExternalMarketInfoController(
            $scope,
            $modalInstance,
            marketInfoService,
            helpers ,
            $state,
            userService,
            info,
            userContactDetails,
            productConfigurationService,
            externalMarketInfoService){

        "use strict";

        $scope.info = info;
        $scope.marketInfoType = info.marketInfoType;
        $scope.userContactDetails = userContactDetails;

        var productsChanged = function(originalProducts, selectedProducts){
            var counter,
                outerCounter,
                originalItemsSelected = 0,
                selectedProductsLength = 0;

            if (!originalProducts && !selectedProducts) {
                return false;
            }

            if (originalProducts) {
                originalItemsSelected = _.reduce(originalProducts, function(memo, p) { return memo + !!p.ticked; }, 0);
            }
            if(selectedProducts){
                selectedProductsLength = selectedProducts.length;
            }
            if(originalItemsSelected !== selectedProductsLength) {
                return true;
            }

            for(outerCounter = 0;  outerCounter < selectedProducts.length; outerCounter++) {

                for (counter = 0; counter < originalProducts.length; counter++) {
                    if (originalProducts[counter].productId === selectedProducts[outerCounter]){
                        if (originalProducts[counter].ticked === false) {
                            return true;
                        }
                    }
                }
            }

            return false;
        };

        var hasInfoBeenUpdated = function() {
            var infoTextChanged = infoCopy && ($scope.info.info !== infoCopy.info);
            var selectedProducts = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            var productsUpdated = productsChanged($scope.originalProducts, selectedProducts);
            var infoTypeChanged = $scope.originalInfoType &&
                                  ($scope.originalInfoType.toLowerCase() !== $scope.marketInfoType.toLowerCase());
            return infoTextChanged || productsUpdated || infoTypeChanged;
        };

        $scope.infoButtonClass = function(marketInfoType){

            if($scope.marketInfoType.toLowerCase() === marketInfoType.toLowerCase()){
                return marketInfoType + "-selected";
            }

            return false;
        };

        $scope.setMarketInfoType = function(marketInfoType){
            externalMarketInfoService.setMarketInfoType($scope, marketInfoType);
            if (!$scope.originalInfoType) {
                $scope.originalInfoType = marketInfoType;
            }
            $scope.updated = hasInfoBeenUpdated();
        };

        $scope.marketInfoType = info.marketInfoType.toLowerCase();
        $scope.setMarketInfoType(info.marketInfoType);

        $scope.$on('$stateChangeSuccess', function () {
            if ($state.current.name !== 'Dashboard') {
                $modalInstance.dismiss('cancel');
            }
        });

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        var products = angular.copy(productConfigurationService.getAllProducts());
        var subsProdIds = productConfigurationService.getAllSubscribedProductIds();
        $scope.marketTickerInfoProducts = products;
        for(var i=0;i<products.length;i++) {
            products[i].shouldDisable = !(_.contains(subsProdIds, products[i].productId));
            products[i].ticked = _.contains(info.productIds, products[i].productId);
        }
        $scope.originalProducts = angular.copy(products);

        $scope.canCreateVerified = function(){
            return marketInfoService.canCreateVerified();
        };
        $scope.infoValid = false;
        $scope.characterLimit = 500;
        $scope.charactersLeft = $scope.characterLimit;
        $scope.updated = false;
        $scope.pending = $scope.info.marketInfoStatus === "Pending";
        $scope.voidable = $scope.info.marketInfoStatus === "Active" || $scope.info.marketInfoStatus === "Updated" || $scope.pending ;
        var infoCopy = angular.copy($scope.info);

        $scope.infoChanged = function(){
            var processedText, selectedProducts;
            processedText = marketInfoService.processMarketInfoText($scope.info.info, $scope.characterLimit);

            $scope.info.info = processedText.text;
            $scope.charactersLeft = processedText.charactersLeft;
            $scope.infoValid = processedText.infoValid;

            selectedProducts = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            if($scope.infoValid) {
                if (selectedProducts.length === 0) {
                    $scope.infoValid = false;
                }
            }
            $scope.updated = hasInfoBeenUpdated();
        };

        $scope.verifyMarketInfo = function () {
            $scope.info.marketInfoType = $scope.marketInfoType;
            $scope.info.productIds = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            marketInfoService.sendVerifyMarketInfoMessage($scope.info);
            $modalInstance.dismiss('submitted');
        };
        $scope.updateMarketInfo = function () {
            $scope.info.marketInfoType = $scope.marketInfoType;
            $scope.info.productIds = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            marketInfoService.sendUpdateMarketInfoMessage($scope.info);
            $modalInstance.dismiss('submitted');
        };
        $scope.voidMarketInfo = function () {
            // Ignore changes to marketInfoType here as can't change type when voiding
            $scope.info.productIds = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            marketInfoService.sendVoidMarketInfoMessage($scope.info);
            $modalInstance.dismiss('submitted');
        };

        $scope.sendMail = function(){
            var link = "mailto:" + userContactDetails.email +
                    "?subject=Argus Open Markets - Market Information Query" +
                    "&body=To " + userContactDetails.name + "," +
                    "%0D%0A%0D%0ARegarding your submission of market information to Argus Open Markets, (our reference: " + info.id + ")." +
                    "%0D%0A%0D%0AThe reported information was: '" + $scope.info.info + "'." +
                    "%0D%0A%0D%0AQUERYHERE" +
                    "%0D%0A%0D%0ARegards" +
                    "%0D%0A%0D%0A" + userService.getUser().name
                ;
            window.sendingEmail=true;
            window.location.href = link;
        };
        $scope.infoChanged();

    });


