﻿angular.module('argusOpenMarkets.dashboard')
    .filter('marketTickerProductFilter', function() {
        return function (marketTickerItems, marketTickerProducts) {
            var result = [];
            angular.forEach(marketTickerItems, function (marketTickerItem) {
                angular.forEach(marketTickerProducts, function (marketTickerProduct) {
                    var productIds = marketTickerItem.productIds;
                    for(var i=0; productIds && i < productIds.length; ++i){
                        if (productIds[i] === marketTickerProduct.productId && marketTickerProduct.ticked === true) {
                            if(result.indexOf(marketTickerItem) === -1){
                                result.push(marketTickerItem);
                            }
                        }
                    }
                });
            });
            return result;

        };
    })
    .filter('marketTickerItemTypeFilter', function() {
        return function (marketTickerItems, marketTickerItemTypes) {
            var tickedItemTypes =
                _.chain(marketTickerItemTypes).filter(function(mtiType) {return mtiType.ticked;}).pluck('itemType').value();

            var getItemFilterType = function(mti) {
                if (mti.marketTickerItemType !== 'Info') {
                    return mti.marketTickerItemType;
                }
                return mti.marketInfoType ? mti.marketInfoType : mti.marketTickerItemType;
            };

            return _.filter(marketTickerItems, function(mti) {
                return _.contains(tickedItemTypes, getItemFilterType(mti));
            });
        };
    })
    .filter('pendingFilter', function() {
        return function (marketTickerItems, showPendingOnly) {
            var result = [];
            angular.forEach(marketTickerItems, function (marketTickerItem) {
                if(!showPendingOnly){
                    result.push(marketTickerItem);
                }
                else if (marketTickerItem.marketTickerItemStatus === "Pending") {
                    result.push(marketTickerItem);
                }
            });
            return result;
        };
    });
