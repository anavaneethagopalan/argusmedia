describe('argusOpenMarkets.dashboard.newMarketInfoController', function() {
    "use strict";
    describe('authMarketInfoController tests', function() {

        var scope, testController;
        var mockModal, mockMarketInfoService;
        var testInfo = {
            info: 'abc',
            productIds: [1]
        };
        var fakeUserContactDetails = {
            name: 'fake contact name',
            email: 'fake email'
        }

        var fakeUser = {
            name: "fake user"
        };
        var fakeUserService = {
            getUser: function() {
                return fakeUser;
            }
        };

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );

        beforeEach( inject( function( $controller,
                                      $rootScope,
                                      helpers ,
                                      mockProductConfigurationService ) {
            mockModal = jasmine.createSpyObj('mockModal', ['dismiss']);
            mockMarketInfoService = jasmine.createSpyObj('mockMarketInfoService', [
                'sendVerifyMarketInfoMessage',
                'sendUpdateMarketInfoMessage',
                'sendVoidMarketInfoMessage',
                'processMarketInfoText',
                'canCreateVerified',
                'getTickedProductIds']);
            mockMarketInfoService.processMarketInfoText.and.callFake(function(text, characterLimit) {
                return {
                    text: text,
                    infoValid: true,
                    charactersLeft: characterLimit-text.length
                };
            });
            mockMarketInfoService.getTickedProductIds.and.returnValue([1]);
            scope = $rootScope.$new();
            testController = $controller('authMarketInfoController', {
                $scope: scope,
                $modalInstance: mockModal,
                helpers: helpers,
                marketInfoService: mockMarketInfoService,
                productConfigurationService: mockProductConfigurationService,
                userService : fakeUserService,
                info: angular.copy(testInfo),
                userContactDetails: fakeUserContactDetails
            });
        }));

        it('should have a defined controller', function() {
            expect(testController).toBeDefined();
        });
        it('should set info to supplied info', function() {
            expect(scope.info).toEqual(testInfo);
        });
        it('should set info validity and characters left based on supplied info', function(){
            expect(scope.infoValid).toEqual(true);
            expect(scope.charactersLeft).toEqual(scope.characterLimit - testInfo.info.length);
        });
        it('should call market info service for canCreateVerified', function() {
            mockMarketInfoService.canCreateVerified.and.returnValue(false);
            expect(scope.canCreateVerified()).toEqual(false);
            expect(mockMarketInfoService.canCreateVerified).toHaveBeenCalled();
            mockMarketInfoService.canCreateVerified.and.returnValue(true);
            expect(scope.canCreateVerified()).toEqual(true);
        });
        it('should not be marked as updated if the info is not changed', function() {
            scope.infoChanged();
            expect(scope.updated).toEqual(false);
        });
        it('should be marked as updated if the info is changed', function() {
            scope.info.info = 'updated value';
            scope.infoChanged();
            expect(scope.updated).toEqual(true);
        });
        it('should set info text values to the returned values from the service', function() {
            var updatedText = 'updated';
            mockMarketInfoService.processMarketInfoText.and.returnValue({
                infoValid: true,
                text: updatedText,
                charactersLeft: scope.characterLimit-updatedText.length
            });
            scope.infoChanged();
            expect(scope.infoValid).toEqual(true);
            expect(scope.charactersLeft).toEqual(scope.characterLimit-updatedText.length);
            expect(scope.info.info).toEqual('updated');
        });
        it('should mark the products in the market info productId list as ticked', function() {
            for (var i = 0; i < scope.marketTickerInfoProducts.length; ++i) {
                var p = scope.marketTickerInfoProducts[i];
                expect(p).toBeDefined();
                expect(p).not.toBeNull();
                var shouldBeTicked = (testInfo.productIds.indexOf(p.productId) !== -1);
                expect(p.ticked).toEqual(shouldBeTicked);
            }
        });


        describe("User Updates Products Selected For Market Info", function(){
            var originalProducts,
                selectedProducts;

            beforeEach(function(){

                originalProducts = [];
                originalProducts.push({ "ticked" : true, "productId" : 1});
                originalProducts.push({ "ticked" : true, "productId" : 2});

                selectedProducts = [];
                selectedProducts.push(1);
                selectedProducts.push(2);
            });

            it("Should return false for products changed if the original products exactly match the selected product", function(){

                var productsChanged = scope.productsChanged(originalProducts, selectedProducts);
                expect(productsChanged).toBeFalsy();
            });

            it("Should return true for products changed if the selected products does not match the original products the user unselects one of the products", function(){

                var productsChanged,
                    userRemovesOneProduct = [1];

                productsChanged = scope.productsChanged(originalProducts, userRemovesOneProduct)
                expect(productsChanged).toBeTruthy();
            });

            it("Should track the state of original products against selected products and return appropriate values as to whether the products have changed", function(){

                var productsChanged;

                productsChanged = scope.productsChanged(originalProducts, selectedProducts);
                expect(productsChanged).toBeFalsy();

                originalProducts[0].ticked = false;
                productsChanged = scope.productsChanged(originalProducts, selectedProducts);
                expect(productsChanged).toBeTruthy();

                selectedProducts.splice(1, 1);
                productsChanged = scope.productsChanged(originalProducts, selectedProducts);
                expect(productsChanged).toBeTruthy();
            });
        });


        describe('send message tests', function() {
            beforeEach(function() {
                mockMarketInfoService.getTickedProductIds.and.returnValue(angular.copy(testInfo.productIds));
            });
            it('should call sendVerifyMarketInfoMessage when verifying', function(){
                scope.verifyMarketInfo();
                expect(mockMarketInfoService.sendVerifyMarketInfoMessage).toHaveBeenCalledWith(testInfo);
            });
            it('should call sendUpdateMarketInfoMessage when updating', function(){
                scope.updateMarketInfo();
                expect(mockMarketInfoService.sendUpdateMarketInfoMessage).toHaveBeenCalledWith(testInfo);
            });
            it('should call sendVoidMarketInfoMessage when voiding', function(){
                scope.voidMarketInfo();
                expect(mockMarketInfoService.sendVoidMarketInfoMessage).toHaveBeenCalledWith(testInfo);
            });
        });
    });
});
