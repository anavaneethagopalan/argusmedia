describe('argusOpenMarkets.dashboard.orderDialogService', function() {
    "use strict";
    describe('orderDialogService tests', function () {

        var service;
        var mockUserService;

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(function () {
            module('argusOpenMarkets.dashboard');
        });

        beforeEach(function() {
            mockUserService = jasmine.createSpyObj('mockUserService', [
                'hasProductPrivilege',
                'getSubscribedProductIds'
            ]);
            mockUserService.hasProductPrivilege.and.returnValue(true);
        });

        beforeEach(function(){
            module(function($provide) {
                $provide.value('userService', mockUserService );
            });

            inject(function(orderDialogService){
                service = orderDialogService;
            });
        });

        it("should be defined", function() {
            expect(service).toBeDefined();
        });

        describe("check principal needed privileges", function() {
            it("should ask for internal trader privileges for correct product for internal order", function() {
                service.hasNeededOrderCreatePrivilegesAsPrincipal(1234, "Internal");
                expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                    1234, ["Order_Create_Principal", "Order_Create_Any"]);
            });
            it("should ask for external trader privileges for correct product for external order", function() {
                service.hasNeededOrderCreatePrivilegesAsPrincipal(2345, "External");
                expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                    2345, ["ExternalOrder_Create_Principal"]);
            });
            it("should return truthy if userService call returns true", function() {
                mockUserService.hasProductPrivilege.and.returnValue(true);
                expect(service.hasNeededOrderCreatePrivilegesAsPrincipal(2345, "External")).toBeTruthy();
            });
            it("should return falsy if userService call returns false", function() {
                mockUserService.hasProductPrivilege.and.returnValue(false);
                expect(service.hasNeededOrderCreatePrivilegesAsPrincipal(2345, "External")).toBeFalsy();
            });
        });

        describe("check broker needed privileges", function() {
            it("should ask for internal trader privileges for correct product for internal order", function() {
                service.hasNeededOrderCreatePrivilegesAsBroker(1234, "Internal");
                expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                    1234, ["Order_Create_Broker", "Order_Create_Any"]);
            });
            it("should ask for external trader privileges for correct product for external order", function() {
                service.hasNeededOrderCreatePrivilegesAsBroker(2345, "External");
                expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                    2345, ["ExternalOrder_Create_Broker"]);
            });
            it("should return truthy if userService call returns true", function() {
                mockUserService.hasProductPrivilege.and.returnValue(true);
                expect(service.hasNeededOrderCreatePrivilegesAsBroker(2345, "External")).toBeTruthy();
            });
            it("should return falsy if userService call returns false", function() {
                mockUserService.hasProductPrivilege.and.returnValue(false);
                expect(service.hasNeededOrderCreatePrivilegesAsBroker(2345, "External")).toBeFalsy();
            });
        });
    });
});

