﻿angular.module('argusOpenMarkets.dashboard').service("marketInfoService", function marketInfoService(userService,
                                                                                                     helpers){
    "use strict";

    var sendNewMarketInfoMessage = function(info){
        userService.sendStandardFormatMessageToUsersTopic( 'Info', 'Create', info );
    };

    var sendUpdateMarketInfoMessage = function(info){
        userService.sendStandardFormatMessageToUsersTopic( 'Info', 'Update', info );
    };

    var sendVerifyMarketInfoMessage = function(info){
        userService.sendStandardFormatMessageToUsersTopic( 'Info', 'Verify', info );
    };

    var sendVoidMarketInfoMessage = function(info){
        userService.sendStandardFormatMessageToUsersTopic( 'Info', 'Void', info );
    };

    var canCreateVerified = function(){
        return userService.hasProductPrivilegeOnAnyProduct(["MarketTicker_Authenticate"]);
    };

    var processMarketInfoText = function(text, characterLimit){
        var infoValid = false;
        var charactersLeft = characterLimit;
        if(!helpers.isEmpty(text)) {
            if (text.trim().length > 0) {
                infoValid = true;
            }
            else{
                infoValid = false;
            }
            if (text.length > characterLimit){
                text = text.slice(0, characterLimit);
            }
            charactersLeft = characterLimit - text.length;
        }
        else{
            charactersLeft = characterLimit;
            infoValid = false;
        }
        return {
            text: text,
            infoValid: infoValid,
            charactersLeft: charactersLeft
        };
    };

    var getTickedProductIds = function(products) {
        if (!products) {
            return [];
        }
        return _.chain(products).filter(function(p) {return p.ticked;}).pluck("productId").value();
    };

    return {
        sendNewMarketInfoMessage:sendNewMarketInfoMessage,
        sendUpdateMarketInfoMessage:sendUpdateMarketInfoMessage,
        sendVerifyMarketInfoMessage:sendVerifyMarketInfoMessage,
        sendVoidMarketInfoMessage:sendVoidMarketInfoMessage,
        processMarketInfoText: processMarketInfoText,
        canCreateVerified: canCreateVerified,
        getTickedProductIds: getTickedProductIds
    };
});