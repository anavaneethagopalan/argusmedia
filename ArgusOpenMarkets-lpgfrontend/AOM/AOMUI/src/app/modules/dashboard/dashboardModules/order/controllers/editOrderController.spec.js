﻿describe('argusOpenMarkets.dashboard.editOrder', function () {
    "use strict";
    describe('editOrderController', function () {
        var $scope, mockProductConfigurationService, mockCoBrokerableProductConfigurationService, mockModalInstance, mockUserOrderService, testOrder, testCoBrokerableOrder, thisController;
        var mockUserApi, mockUserService, mockProductMessageSubscriptionService;
        var editOrderCallbackOrderUpdated, editOrderCallbackAskUpdated, editOrderCallbackOrderDeleted;
        var mockOrderDialogService, provide;

        mockUserOrderService = {
            sendHoldOrderMessage: sinon.spy(),
            sendKillOrderMessage: sinon.spy(),
            sendReinstateOrderMessage: sinon.spy(),
            sendEditOrderMessage: sinon.spy(),
            isOrderMine: function (order) {
                return true;
            }
        };

        testOrder = {
            orderStatus: 'Active',
            notes: "",
            productId: 1,
            id: 100,
            price: 10,
            metaData: [{
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId: 1,
                itemType:"IntegerEnum"
            },{
                productMetaDataId:3,
                displayName:"Delivery Description",
                displayValue:"123",
                itemValue:null,
                metaDataListItemId:null,
                itemType:"String"
            }]
        };

        var metadataDefinitions = [{
            "metadataList": {
                "id": 3,
                "fieldList": [
                    {
                        "id": 1,
                        "displayName": "Port A",
                        "itemValue": 100
                    },
                    {
                        "id": 2,
                        "displayName": "Port B",
                        "itemValue": 101
                    },
                    {
                        "id": 3,
                        "displayName": "Port C",
                        "itemValue": 102
                    }
                ]},
            "id": 1,
            "productId": 1,
            "displayName": "Delivery Port",
            "fieldType": "IntegerEnum"
        }, {
            "valueMinimum": 0,
            "valueMaximum": 20,
            "id": 3,
            "productId": 1,
            "displayName": "Delivery Description",
            "fieldType": "String"
        }];

        testCoBrokerableOrder = angular.copy(testOrder);
        testCoBrokerableOrder.coBrokering=true;

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(module('argusOpenMarkets.dashboard'));
        beforeEach(module(function($provide) { provide = $provide; }));
        beforeEach(inject(function ($controller, $rootScope) {
            $scope = $rootScope.$new();

            var userOrganisation = {
                organisationType: "Brokerage"
            };

            var user = {
                userOrganisation: userOrganisation
            };

            $scope.selectedPrincipal = {
                userId: {},
                organisationId: {}
            };

            mockUserService = {
                getUsersOrganisation: function () {
                    return {organisationName: "XYZ"};
                },
                hasProductPrivilege: function (productId, privs) {
                    return true;
                },
                getUser: function () {
                    return user;
                },
                isBroker: function () {
                    return false;
                }
            };

            mockProductConfigurationService = {
                getProductConfigurationById: function (id) {
                    return {
                        "productId": 1,
                        "coBrokering": false ,
                        "productName": "Argus-Naphtha-CIF-NWE-Outright",
                        "productTitle": "Argus-Naphtha-CIF-NWE-Outright",
                        "contractSpecification": {
                            "contractType": "Physical",
                            "volume": {
                                "volumeUnitCode": "MT",
                                "minimum": 12500,
                                "maximum": 35000,
                                "increment": 500,
                                "decimalPlaces": 0
                            },
                            "pricing": {
                                "pricingUnitCode": "MT",
                                "pricingCurrencyCode": "$",
                                "minimum": 1,
                                "maximum": 9999,
                                "increment": 0.1,
                                "decimalPlaces": 2
                            },
                            "tenors": [
                                {
                                    "deliveryLocation": {
                                        "id": 1,
                                        "name": "Rotterdam",
                                        "dateCreated": "2014-01-01T00:00:00Z"
                                    },
                                    "deliveryStartDate": "+1d",
                                    "deliveryEndDate": "+20d",
                                    "rollDate": "DAILY",
                                    "id": 1,
                                    "minimumDeliveryRange": 4
                                }
                            ]
                        }
                    };
                },
                getCommonProductQuantities: function (id) {
                    return [10000, 20000, 30000];
                },
                getProductConfigurationLabels: function (id) {
                    return {
                        "locationLabel": "Location",
                        "deliveryPeriodLabel": "Delivery Period"
                    };
                },
                getProductMetaDataById: function(productId) {
                    return { "productId": 1, "fields": metadataDefinitions };
                }
            };

            mockCoBrokerableProductConfigurationService = {
                getProductConfigurationById: function (id) {
                    return {
                        "productId": 1,
                        "coBrokering": true ,
                        "productName": "Argus-Naphtha-CIF-NWE-Outright",
                        "productTitle": "Argus-Naphtha-CIF-NWE-Outright",
                        "contractSpecification": {
                            "contractType": "Physical",
                            "volume": {
                                "volumeUnitCode": "MT",
                                "minimum": 12500,
                                "maximum": 35000,
                                "increment": 500,
                                "decimalPlaces": 0
                            },
                            "pricing": {
                                "pricingUnitCode": "MT",
                                "pricingCurrencyCode": "$",
                                "minimum": 1,
                                "maximum": 9999,
                                "increment": 0.1,
                                "decimalPlaces": 2
                            },
                            "tenors": [
                                {
                                    "deliveryLocation": {
                                        "id": 1,
                                        "name": "Rotterdam",
                                        "dateCreated": "2014-01-01T00:00:00Z"
                                    },
                                    "deliveryStartDate": "+1d",
                                    "deliveryEndDate": "+20d",
                                    "rollDate": "DAILY",
                                    "id": 1,
                                    "minimumDeliveryRange": 4
                                }
                            ]
                        }
                    };
                },
                getCommonProductQuantities: function (id) {
                    return [10000, 20000, 30000];
                },
                getProductConfigurationLabels: function (id) {
                    return {
                        "locationLabel": "Location",
                        "deliveryPeriodLabel": "Delivery Period"
                    };
                },
                getProductMetaDataById: function(productId) {
                    return {
                        "productId": 1,
                        "fields": []
                    }
                }
            };

            mockUserApi = {
                getPrincipals: function (productId, canTrade, buysell) {
                    throw error();
                },
                getBrokers: function (productId, canTrade, buysell) {
                    return {then: function(callback) { callback([{"id": 1, "name": "brok1"}, {"id": 2, "name": "brok2"}]); }};
                },
                formatErrorResponse: function (error) {
                    this.error = error;
                },
                error: ""
            };

            mockProductMessageSubscriptionService = {
                registerActionOnProductMessageType: function (registerId, uniqueId, event, callback) {
                    // "Edit Order Controller " + $scope.order.productId,  $scope.order.productId, "onBid", onBidEventHandler
                    if (event === "onBid") {
                        editOrderCallbackOrderUpdated = callback;
                    }

                    if (event === "onAsk") {
                        editOrderCallbackAskUpdated = callback;
                    }

                    if (event === "onOrderDeleted") {
                        editOrderCallbackOrderDeleted = callback;
                    }
                }
            };

            mockModalInstance = {
                dismiss: function () {
                    return true;
                }
            };

            mockOrderDialogService = jasmine.createSpyObj('mockOrderDialogService',
                [
                    'hasNeededOrderCreatePrivilegesAsPrincipal',
                    'hasNeededOrderCreatePrivilegesAsBroker'
                ]);

            provide.value('productConfigurationService', mockProductConfigurationService);

            thisController = $controller('editOrderController',
                {
                    $scope: $scope,
                    productConfigurationService: mockProductConfigurationService,
                    $modalInstance: mockModalInstance,
                    userOrderService: mockUserOrderService,
                    orderType: 'bid',
                    productId: 1,
                    userService: mockUserService,
                    tenorId: 1,
                    order: testOrder,
                    userApi: mockUserApi,
                    productMessageSubscriptionService: mockProductMessageSubscriptionService,
                    orderDialogService: mockOrderDialogService
                });
        }));

        it('should be defined', inject(function () {
            expect(thisController).toBeDefined();
        }));

        it('should get the product config', inject(function () {
            var prodConfig = mockProductConfigurationService.getProductConfigurationById(1);
            expect(prodConfig).not.toBe(null);
        }));

        it('co-brokerable option is not visible as product is not co-brokerable', function () {
            expect($scope.productConfig.coBrokering).toBeFalsy();
            expect($scope.coBrokeringVisible).toBeFalsy();
        });

        it('should set order type to the passed in order type', inject(function () {
            expect($scope.orderType).toEqual('bid');
        }));

        it('should set the order type on setOrderType', inject(function () {
            $scope.setOrderType('offer');
            expect($scope.orderType).toEqual('offer');
        }));

        it('exit should call close on modal instance', inject(function () {
            spyOn(mockModalInstance, 'dismiss');
            $scope.exit();
            expect(mockModalInstance.dismiss).toHaveBeenCalled();
        }));

        it('should not allow prices less than the min price', function () {
            var mockFormPrice = {
                $setValidity: function () {
                }
            };
            $scope.order.price = 0.1;
            $scope.priceSpecification.isValid(mockFormPrice);
            expect($scope.invalidPriceReason).toEqual("Price must be at least 1")
        });

        it('should not allow prices more than the max price', function () {
            var mockFormPrice = {
                $setValidity: function () {
                }
            };
            $scope.order.price = 99999;
            $scope.priceSpecification.isValid(mockFormPrice);
            expect($scope.invalidPriceReason).toEqual("Price must not exceed 9999")
        });

        it('should not allow prices that are not multiples of the price increment', function () {
            var mockFormPrice = {
                $setValidity: function () {
                }
            };
            $scope.order.price = 1.05;
            $scope.priceSpecification.isValid(mockFormPrice);
            expect($scope.invalidPriceReason).toEqual("Price must be in increments of 0.1");
        });

        it('should set the order quantity on set quantity called', function () {
            $scope.setQuantity(12500);
            expect($scope.order.quantity).toBe(12500);
        });

        it('should allow prices between the min and max price', function () {
            var mockFormPrice = {
                $setValidity: function () {
                }
            };
            $scope.order.price = 9998;
            $scope.priceSpecification.isValid(mockFormPrice);
            expect($scope.invalidPriceReason).toEqual("")
        });

        it('should not allow quantities less than the min quantity', function () {
            var mockFormQuantity = {
                $setValidity: function () {
                }
            };
            $scope.order.quantity = 0.1;
            $scope.quantitySpecification.isValid($scope.order.quantity, mockFormQuantity);
            expect($scope.invalidQuantityReason).toEqual("Quantity must be at least 12500")
        });

        it('should not allow quantities more than the max quantity', function () {
            var mockFormQuantity = {
                $setValidity: function () {
                }
            };
            $scope.order.quantity = 35001;
            $scope.quantitySpecification.isValid($scope.order.quantity, mockFormQuantity);
            expect($scope.invalidQuantityReason).toEqual("Quantity must not exceed 35000")
        });

        it('should not allow quantities at the incorrect increment', function () {
            var mockFormQuantity = {
                $setValidity: function () {
                }
            };
            $scope.order.quantity = 12501;
            $scope.quantitySpecification.isValid($scope.order.quantity, mockFormQuantity);
            expect($scope.invalidQuantityReason).toEqual("Quantity must be in lots of 500")
        });

        it('should  allow quantities between the min and max price at the correct increment', function () {
            var mockFormQuantity = {
                $setValidity: function () {
                }
            };
            $scope.order.quantity = 12500;
            $scope.quantitySpecification.isValid($scope.order.quantity, mockFormQuantity);
            expect($scope.invalidQuantityReason).toEqual("")
        });

        it("should allow you to hold an order", function () {
            $scope.holdOrder();
            expect(mockUserOrderService.sendHoldOrderMessage.called).toEqual(true);
        });

        it('should set characters left as 0 when notes field too large', function () {
            $scope.notesCharacterLimit = 5;
            $scope.order.notes = "123456789";

            $scope.onNotesChange();

            expect($scope.notesCharactersLeft).toBe(0);
        });

        it('should restrict order notes field to max characters allowed', function () {
            $scope.notesCharacterLimit = 5;
            $scope.order.notes = "123456789";

            $scope.onNotesChange();

            expect($scope.order.notes.length).toBe($scope.notesCharacterLimit);
        });

        it("should allow you to reinstate an order", function () {
            $scope.reinstateOrder();
            expect(mockUserOrderService.sendReinstateOrderMessage.called).toEqual(true);
        });

        it("should allow you to kill an order", function () {
            $scope.killOrder();
            expect(mockUserOrderService.sendKillOrderMessage.called).toEqual(true);
        });

        it("should allow you to edit an order no brokerid", function () {
            $scope.editOrder();
            expect(mockUserOrderService.sendEditOrderMessage.called).toEqual(true);
        });

        it("should allow you to edit an order with brokerid", function () {
            $scope.order.brokerId = 1;
            $scope.editOrder();
            expect(mockUserOrderService.sendEditOrderMessage.called).toEqual(true);
        });

        it("should allow you to edit an order with brokerid", function () {
            $scope.order.brokerId = 1;
            $scope.editOrder();
            expect(mockUserOrderService.sendEditOrderMessage.called).toEqual(true);
        });

        it("should set null for null broker", function () {
            $scope.onBrokerChange(null);
            $scope.$digest();
            expect($scope.order.brokerOrganisationId).toBe(null);
            //expect($scope.order.brokerId).not.toBeDefined();
            expect($scope.order.brokerName).toBe(null);
        });

        it("should set broker fields on updated", function () {
            $scope.order.brokerOrganisationId = 3;
            $scope.order.brokerName = "old name";

            var mockBroker = {id: 1234};

            $scope.onBrokerChange(mockBroker);
            $scope.$digest();
            expect($scope.selectedBroker).toEqual(mockBroker);
            expect($scope.order.brokerOrganisationId).toEqual($scope.selectedBroker.id);
        });

        it("update button should be enabled if user is principal", function () {
            expect($scope.validPrinicipalSelected).toBe(true);
        });

        it("should not show execute for held order", function () {
            var showExecute;
            $scope.orderIsHeld = false;
            showExecute = $scope.showExecuteMode();
            expect(showExecute).toBe(true);
            $scope.orderIsHeld = true;
            showExecute = $scope.showExecuteMode();
            expect(showExecute).toBe(false);
        });

        it('hasOrderChanged should return false if the new order is the same as the old order', function () {

            expect($scope.hasOrderChanged($scope.order, $scope.order)).toBe(false);
        });

        it('hasOlocarderChanged should return false if the new order is different to the old order with just whitespace in the notes field', function () {
            var newOrder = $scope.order;

            newOrder.notes = "   ";

            expect($scope.hasOrderChanged($scope.order, newOrder)).toBe(false);
        });

        it('hasOrderChanged should return true if the new order is different to the old order', function () {

            var newOrder = $scope.order;
            $scope.hasOrderChanged($scope.order, newOrder);
            newOrder.notes = "xyz";
            expect($scope.hasOrderChanged($scope.order, newOrder)).toBe(false);
        });


        it('should return Any for the Broker Restriction when the broker organisation id is null', function () {

            var brokerRestriction = $scope.getBrokerRestriction(null);
            expect(brokerRestriction).toEqual("Any");
        });

        it('should return None for the Broker Restriction when the broker organisation id is -100', function () {

            var brokerRestriction = $scope.getBrokerRestriction(-100);
            expect(brokerRestriction).toEqual("None");
        });

        it('should return Specific for the Broker Restriction when the broker organisation id is 200', function () {

            var brokerRestriction = $scope.getBrokerRestriction(200);
            expect(brokerRestriction).toEqual("Specific");
        });

        it('should set order to updated when the order that is being edited is updated by someone else', function () {

            expect($scope.orderChanged).toEqual(false);

            var orderMessage = {
                order: {
                    id: 100
                }
            };

            editOrderCallbackOrderUpdated(orderMessage);
            expect($scope.orderChanged).toEqual(true);
        });

        it('should not set order to updated when a different order is updated by someone', function () {

            expect($scope.orderChanged).toEqual(false);

            var orderMessageNotMatching = {
                order: {
                    id: 101
                }
            };

            editOrderCallbackOrderUpdated(orderMessageNotMatching);
            expect($scope.orderChanged).toEqual(false);
        });

        it('should set order to updated when the ask that is being edited is updated by someone else', function () {

            expect($scope.orderChanged).toEqual(false);

            var orderMessage = {
                order: {
                    id: 100
                }
            };

            editOrderCallbackAskUpdated(orderMessage);
            expect($scope.orderChanged).toEqual(true);
        });

        it('should set order to deleted when the order that is being edited is killed', function () {

            expect($scope.orderDeleted).toEqual(false);
            editOrderCallbackOrderDeleted("diffusion/topic/path/last/number/ismatching/id/100");
            expect($scope.orderDeleted).toEqual(true);
        });

        it('should not set order to deleted when the order that is being edited is not killed', function () {

            expect($scope.orderDeleted).toEqual(false);
            editOrderCallbackOrderDeleted("diffusion/topic/path/last/number/ismatching/id/900");
            expect($scope.orderDeleted).toEqual(false);
        });

        it('should reset the order updated flag to false if the user chooses to refresh the order', function () {

            expect($scope.orderChanged).toEqual(false);

            var orderMessage = {
                order: {
                    id: 100,
                    price: 5,
                    metaData: []
                }
            };

            editOrderCallbackAskUpdated(orderMessage);
            expect($scope.orderChanged).toEqual(true);

            $scope.refreshOrder();
            expect($scope.orderChanged).toEqual(false);
        });

        it('refreshing the order after someone has changed to be reflected in the scope', function () {

            expect($scope.orderChanged).toEqual(false);

            var orderMessage = {
                order: {
                    id: 100,
                    price: 5,
                    metaData: []
                }
            };

            editOrderCallbackAskUpdated(orderMessage);
            expect($scope.orderChanged).toEqual(true);
            expect(parseInt($scope.order.price)).toEqual(10);

            $scope.refreshOrder();
            expect($scope.orderChanged).toEqual(false);
            expect(parseInt($scope.order.price)).toEqual(parseInt(orderMessage.order.price));
        });

        it('refreshing the order after someone has changed it should reflect metadata values', function () {

            var orderMessage = {
                order: {
                    id: 100,
                    price: 10,
                    metaData: [{
                        productMetaDataId: 1,
                        displayName: "Delivery Port",
                        displayValue: "Port C",
                        itemValue: 102,
                        metaDataListItemId: 3,
                        itemType:"IntegerEnum"
                    },{
                        productMetaDataId:3,
                        displayName:"Delivery Description",
                        displayValue:"abcdefg",
                        itemValue:null,
                        metaDataListItemId:null,
                        itemType:"String"
                    }]
                }
            };

            editOrderCallbackAskUpdated(orderMessage);

            $scope.refreshOrder();
            var expectedMetadataCollection = [{
                definition: metadataDefinitions[0],
                metadataitem: orderMessage.order.metaData[0]
            }, {
                definition: metadataDefinitions[1],
                metadataitem: orderMessage.order.metaData[1]
            }];
            expect($scope.order.metaData).toEqual(orderMessage.order.metaData);
            expect($scope.metaDataCollection).toEqual(expectedMetadataCollection);
        });

        it('should display metadata values from order metadata', function () {

            var metadataValues = [{
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId: 1,
                itemType:"IntegerEnum"
            },{
                productMetaDataId:3,
                displayName:"Delivery Description",
                displayValue:"123",
                itemValue:null,
                metaDataListItemId:null,
                itemType:"String"
            }];

            expect($scope.metaDataCollection.length).toBe(2);
            expect($scope.metaDataCollection[0].definition).toEqual(metadataDefinitions[0]);
            expect($scope.metaDataCollection[1].definition).toEqual(metadataDefinitions[1]);
            expect($scope.metaDataCollection[0].metadataitem).toEqual(metadataValues[0]);
            expect($scope.metaDataCollection[1].metadataitem).toEqual(metadataValues[1]);
        });

        it('clicking the edit order button should set the actionButtonClicked to true', function () {
            var order = {},
                spy = spyOn(mockUserOrderService, 'sendEditOrderMessage');

            $scope.editOrder(order);
            expect($scope.actionButtonClicked).toBeTruthy();
        });

        it('clicking the edit order button twice should result in the send order message only being sent once', function () {
            var order = {},
                spy = spyOn(mockUserOrderService, 'sendEditOrderMessage');

            $scope.editOrder(order);
            expect($scope.actionButtonClicked).toBeTruthy();
            expect(spy.calls.count()).toEqual(1);
            $scope.editOrder(order);
            expect(spy.calls.count()).toEqual(1);
        });

        describe('with a co-brokerable order and non brokerable product', function() {
            beforeEach(inject(function ($controller, $rootScope) {
                $scope = $rootScope.$new();

                thisController = $controller('editOrderController',
                    {
                        $scope: $scope,
                        productConfigurationService: mockProductConfigurationService,
                        $modalInstance: mockModalInstance,
                        userOrderService: mockUserOrderService,
                        orderType: 'bid',
                        productId: 1,
                        userService: mockUserService,
                        tenorId: 1,
                        order: testCoBrokerableOrder,
                        userApi: mockUserApi,
                        productMessageSubscriptionService: mockProductMessageSubscriptionService,
                        orderDialogService: mockOrderDialogService
                    });
            }));

            it('expect product to not be co-brokerable', function () {
                expect($scope.productConfig.coBrokering).toBeFalsy();
            });

            it('expect co-brokerable order to be reset as not co-brokerable (edge case)', function () {
                expect(testCoBrokerableOrder.coBrokering).toBeTruthy();
                expect($scope.order.coBrokering).toBeFalsy();
            });

        });

        describe('with a CoBrokerable Product and not selected broker', function() {
            beforeEach(inject(function ($controller, $rootScope) {
                $scope = $rootScope.$new();
                thisController = $controller('editOrderController',
                    {
                        $scope: $scope,
                        productConfigurationService: mockCoBrokerableProductConfigurationService,
                        $modalInstance: mockModalInstance,
                        userOrderService: mockUserOrderService,
                        orderType: 'bid',
                        productId: 1,
                        userService: mockUserService,
                        tenorId: 1,
                        order: testOrder,
                        userApi: mockUserApi,
                        productMessageSubscriptionService: mockProductMessageSubscriptionService,
                        orderDialogService: mockOrderDialogService
                    });
                
                $scope.$digest();
            }));

            it('expect product to be co-brokerable', function () {
                expect($scope.productConfig.coBrokering).toBeTruthy();
            });
            it('co-brokerable option is visible', function () {
                expect($scope.coBrokeringVisible).toBeTruthy();
            });

            it('co-brokerable option is disabled', function () {
                expect($scope.coBrokeringDisabled).toBeTruthy();
            });

            // describe('And when there is NO selected broker', function() {
            //     beforeEach(inject(function ($controller, $rootScope) {
            //         var fakeBroker = {id:-100, name:"Any"};
            //         $scope.onBrokerChange(fakeBroker);
            //         $scope.$digest();
            //     }));
            //
            //     it('co-brokerable option is visible', function () {
            //         expect($scope.coBrokeringVisible).toBeTruthy();
            //     });
            //
            //     it('co-brokerable option is disabled as no broker selected', function () {
            //         expect($scope.coBrokeringDisabled).toBeTruthy();
            //     });
            //
            // });

            describe('And when there is a selected broker', function () {
                it('should not set co-brokering as true when "no broker" option is selected', function(){
                    $scope.$apply(function() {
                        $scope.selectedBroker = { id:-100, name:"No Broker" };
                    });
                    expect($scope.order.coBrokering).toBeFalsy();
                });
                it('should set co-brokering as true when particular broker is selected', function(){
                    $scope.$apply(function() {
                        $scope.selectedBroker = { id:5, name:"Broker 1" };
                    });
                    expect($scope.order.coBrokering).toBeTruthy();
                });
                it('should not set co-brokering as true when second broker is selected', function(){
                    $scope.$apply(function() {
                        $scope.selectedBroker = { id:5, name:"Broker 1" };
                    });
                    expect($scope.order.coBrokering).toBeTruthy();
                    $scope.$apply(function() {
                        $scope.order.coBrokering = false;
                    });
                    expect($scope.order.coBrokering).toBeFalsy();
                    $scope.$apply(function() {
                        $scope.selectedBroker = { id:6, name:"Broker 2" };
                    });
                    expect($scope.order.coBrokering).toBeFalsy();
                });
            });
        });

        describe('with a CoBrokerable Product and selected particular broker and with cobrokering disabled', function() {
            var orderWithSelectedBroker = angular.copy(testOrder);
            orderWithSelectedBroker.brokerDisplayName = "BrokerCo #1 (BR1)";
            orderWithSelectedBroker.brokerId = null;
            orderWithSelectedBroker.brokerOrganisationId = 5;
            orderWithSelectedBroker.brokerOrganisationName = "BrokerCo #1";
            orderWithSelectedBroker.brokerRestriction = "Specific";
            orderWithSelectedBroker.brokerShortCode = "BR1"
            orderWithSelectedBroker.coBrokering = false;

            beforeEach(inject(function ($controller, $rootScope) {
                $scope = $rootScope.$new();
                thisController = $controller('editOrderController',
                    {
                        $scope: $scope,
                        productConfigurationService: mockCoBrokerableProductConfigurationService,
                        $modalInstance: mockModalInstance,
                        userOrderService: mockUserOrderService,
                        orderType: 'bid',
                        productId: 1,
                        userService: mockUserService,
                        tenorId: 1,
                        order: orderWithSelectedBroker,
                        userApi: mockUserApi,
                        productMessageSubscriptionService: mockProductMessageSubscriptionService,
                        orderDialogService: mockOrderDialogService
                    });

                $scope.$apply(function() {
                    $scope.selectedBroker = { id:10, name:"BrokerCo #1" };
                });
            }));

            it('expect product to be co-brokerable', function () {
                expect($scope.productConfig.coBrokering).toBeTruthy();
            });

            it('co-brokerable option is visible', function () {
                expect($scope.coBrokeringVisible).toBeTruthy();
            });

            it('co-brokerable option is disabled', function () {
                expect($scope.coBrokeringDisabled).toBeFalsy();
            });

            it('co-brokerable option is disabled', function () {
                expect($scope.coBrokering).toBeFalsy();
            });
        });
    });
});