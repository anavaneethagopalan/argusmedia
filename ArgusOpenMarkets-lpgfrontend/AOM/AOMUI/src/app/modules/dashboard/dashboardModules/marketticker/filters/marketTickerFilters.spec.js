/**
 * Created by tushara.fernando on 17/11/2014.
 */
describe('Test marketTickerProductFilter', function () {
    'use strict';

    var $filter;
    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.dashboard');
        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('marketTickerProductFilter filters out items for non ticked products', function () {
        var marketTickerItems = [{productIds:[1], item:"A"}, {productIds:[2], item:"B"}];
        var marketTickerProducts = [{productId:1, ticked:true}];
        var result = $filter('marketTickerProductFilter')(marketTickerItems,marketTickerProducts, 'marketTickerProductFilter');
        expect(result).toEqual([{productIds:[1], item:"A"}]);
    });

    it('marketTickerItemTypeFilter filters out items for non ticked product types', function () {
        var marketTickerItems = [{productIds:[1], item:"A", marketTickerItemType:"Ask"}, {productIds:[2], item:"B", marketTickerItemType :"Bid"}];
        var marketTickerTypes = [{itemType :"Bid", ticked:false}];
        var result = $filter('marketTickerItemTypeFilter')(marketTickerItems,marketTickerTypes, 'marketTickerItemTypeFilter');
        expect(result).toEqual([]);
    });

    it('marketTickerItemTypeFilter filters out items for non ticked product types', function () {
        var marketTickerItems = [{productIds:[1], item:"A", marketTickerItemType:"Ask"}, {productIds:[2], item:"B", marketTickerItemType :"Bid"}];
        var marketTickerTypes = [{itemType :"Bid", ticked:true}];
        var result = $filter('marketTickerItemTypeFilter')(marketTickerItems,marketTickerTypes, 'marketTickerItemTypeFilter');
        expect(result).toEqual([{productIds:[2], item:"B", marketTickerItemType :"Bid"}]);
    });

    it('marketTickerItemTypeFilter should filter out regular market info', function() {
        var marketTickerItems = [
            {productIds:[1], item:"A", marketTickerItemType:"Ask"},
            {productIds:[2], item:"B", marketTickerItemType :"Info"}
        ];
        var marketTickerTypes = [{itemType :"Bid", ticked:true}];
        var result = $filter('marketTickerItemTypeFilter')(marketTickerItems,marketTickerTypes, 'marketTickerItemTypeFilter');
        expect(result).toEqual([]);
    });

    it('marketTickerItemTypeFilter should not filter out +Info version of item', function() {
        var marketTickerItems = [
            {productIds:[1], item:"A", marketTickerItemType:"Ask"},
            {productIds:[2], item:"B", marketTickerItemType :"Info", marketInfoType:"Bid"}
        ];
        var marketTickerTypes = [{itemType :"Bid", ticked:true}];
        var result = $filter('marketTickerItemTypeFilter')(marketTickerItems,marketTickerTypes, 'marketTickerItemTypeFilter');
        expect(result).toEqual([{productIds:[2], item:"B", marketTickerItemType :"Info", marketInfoType:"Bid"}]);
    });

    it('marketTickerItemTypeFilter should filter out +Info of a different type', function() {
        var marketTickerItems = [
            {productIds:[1], item:"A", marketTickerItemType:"Ask"},
            {productIds:[2], item:"B", marketTickerItemType :"Info", marketInfoType:"Ask"}
        ];
        var marketTickerTypes = [{itemType :"Bid", ticked:true}];
        var result = $filter('marketTickerItemTypeFilter')(marketTickerItems,marketTickerTypes, 'marketTickerItemTypeFilter');
        expect(result).toEqual([]);
    });

    it('marketTickerItemTypeFilter should not filter out +Info of a different type and an item of that type', function() {
        var marketTickerItems = [
            {productIds:[1], item:"A", marketTickerItemType:"Ask"},
            {productIds:[2], item:"B", marketTickerItemType :"Info", marketInfoType:"Ask"}
        ];
        var marketTickerTypes = [{itemType :"Ask", ticked:true}];
        var result = $filter('marketTickerItemTypeFilter')(marketTickerItems,marketTickerTypes, 'marketTickerItemTypeFilter');
        expect(result).toEqual(marketTickerItems);
    });

    it('pendingFilter filters out non pending items', function () {
        var marketTickerItems = [{productIds:[1], item:"A", marketTickerItemType:"Ask", marketTickerItemStatus:"Pending"}, {productIds:[2], item:"B", marketTickerItemType :"Bid", marketTickerItemStatus:"Active"}];
        var pendingOnly = true;
        var result = $filter('pendingFilter')(marketTickerItems,pendingOnly, 'pendingFilter');
        expect(result).toEqual([{productIds:[1], item:"A", marketTickerItemType:"Ask", marketTickerItemStatus:"Pending"}]);
    });

    it('pendingFilter should only filter pending items when showpendingonly is true', function () {
        var marketTickerItems = [{productIds:[1], item:"A", marketTickerItemType:"Ask", marketTickerItemStatus:"Pending"}, {productIds:[2], item:"B", marketTickerItemType :"Bid", marketTickerItemStatus:"Active"}];
        var pendingOnly = false;
        var result = $filter('pendingFilter')(marketTickerItems,pendingOnly, 'pendingFilter');
        expect(result).toEqual(marketTickerItems);
    });

});