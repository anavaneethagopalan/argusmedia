describe( 'Assessments tests', function() {
    describe('inputAssessmentController tests', function () {

        var $scope;
        var productConfigurationService;
        var mockUserService, mockForm, mockModalInstance, mockAssessmentService;

        var fakeAssessment = {
            productId: 1,
            message: {
            },
            assessment: {
                enteredByUserId: 55,
                today: {
                    businessDate: moment()
                },
                previous: {

                }
            }
        };

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );

        beforeEach( inject( function( $controller, $rootScope, mockProductConfigurationService ) {
            var $injector = angular.injector([ 'argusOpenMarkets.shared' ]);
            mockUserService = $injector.get( 'mockUserService' );
            mockUserService.setUser(mockUserService.MOCK_USER_AE1);
            productConfigurationService = mockProductConfigurationService;
            mockModalInstance = {
                dismiss: function(exitReason) {}
            };
            mockForm = {
                lowPrice : {
                    $setValidity: function (validationErrorKey, isValid) {}
                },
                highPrice : {
                    $setValidity: function (validationErrorKey, isValid) {}
                }
            };
            mockAssessmentService = {
                sendNewAssessment: function(assessment) {},
                sendRefreshAssessment: function(assessment) {}
            };
            $scope = $rootScope.$new();
            $scope.thisController = $controller( 'inputAssessmentController',
                {
                    $scope: $scope,
                    $modalInstance: mockModalInstance,
                    assessmentService: mockAssessmentService,
                    userService: mockUserService,
                    productConfigurationService: mockProductConfigurationService,
                    assessment: fakeAssessment
                });
        }));

        it ('controller should be defined', function () {
           expect($scope.thisController).toBeDefined();
        });

        it ('should get the name of the product', inject( function() {
            spyOn(productConfigurationService,'getProductConfigurationById').and.returnValue(
                {productName : "Apples"});
            expect($scope.getProductName(1) ).toEqual("Apples");
        }));

        describe('newAssessment tests', function() {
            it('scope should have low input defined', function() {
                expect($scope.lowInput).toBeDefined();
            });

            it('scope should have high input defined', function() {
                expect($scope.highInput).toBeDefined();
            });

            it('should create correct assessment when creating new assessment', function(){
                var low = 500, high = 600;
                var spy = sinon.spy(mockAssessmentService, 'sendNewAssessment');
                $scope.lowInput.low = low;
                $scope.highInput.high = high;
                $scope.newAssesment();
                var callArg = spy.getCall(0).args[0];
                expect(callArg).toBeDefined();
                expect(callArg.today.priceLow).toBeCloseTo(low);
                expect(callArg.today.priceHigh).toBeCloseTo(high);
                expect(callArg.today.enteredByUserId).toBeCloseTo(55);
                expect(callArg.today.lastUpdatedUserId).toEqual(mockUserService.getUser().id);
            });

            it('should close the modal as submitted', function() {
                var low = 500, high = 600;
                var spy = sinon.spy(mockModalInstance, 'dismiss');
                $scope.lowInput.low = low;
                $scope.highInput.high = high;
                $scope.newAssesment();
                expect(spy.calledWith('submitted')).toBeTruthy();
            });
        });

        describe('rollAssessment tests', function() {
            // it('should send refresh assessment for correct product', function() {
            //     var spy = sinon.spy(mockAssessmentService, 'sendRefreshAssessment');
            //     $scope.rollAssesment();
            //     expect(spy.calledWith({productId: 1})).toBeTruthy();
            // });

            it('should close modal as submitted', function() {
                var spy = sinon.spy(mockModalInstance, 'dismiss');
                $scope.rollAssesment();
                expect(spy.calledWith('submitted')).toBeTruthy();
            });

            it('is already rolled if today-assessment-date is today', function () {
                expect($scope.alreadyRolled).toBeTruthy();
            });
        });

        describe('price validation tests', function() {
            it('valid high assessment price should validate', function() {
                $scope.highInput.high = 600;
                var spyHigh = sinon.spy(mockForm.highPrice, '$setValidity');
                var spyLow = sinon.spy(mockForm.lowPrice, '$setValidity');
                $scope.priceSpecification.isValid(mockForm, 'high');
                expect($scope.invalidPriceReason).toBe('');
                expect($scope.disabled).toBeFalsy();
                expect(spyHigh.calledWith("pricerange", true)).toBeTruthy();
                expect(spyLow.calledWith("pricerange", true)).toBeTruthy();
                expect(spyHigh.calledWith("price", true)).toBeTruthy();
                expect(spyLow.calledWith("price", true)).toBeFalsy();
                expect(spyLow.calledWith("price", false)).toBeFalsy();
            });

            it('valid low assessment price should validate', function() {
                $scope.lowInput.low = 500;
                var spyHigh = sinon.spy(mockForm.highPrice, '$setValidity');
                var spyLow = sinon.spy(mockForm.lowPrice, '$setValidity');
                $scope.priceSpecification.isValid(mockForm, 'low');
                expect($scope.invalidPriceReason).toBe('');
                expect($scope.disabled).toBeFalsy();
                expect(spyHigh.calledWith("pricerange", true)).toBeTruthy();
                expect(spyLow.calledWith("pricerange", true)).toBeTruthy();
                expect(spyLow.calledWith("price", true)).toBeTruthy();
                expect(spyHigh.calledWith("price", true)).toBeFalsy();
                expect(spyHigh.calledWith("price", false)).toBeFalsy();
            });

            it('valid high and low assessment prices should validate', function() {
                $scope.lowInput.low = 500;
                $scope.highInput.high = 600;
                var spyHigh = sinon.spy(mockForm.highPrice, '$setValidity');
                var spyLow = sinon.spy(mockForm.lowPrice, '$setValidity');
                $scope.priceSpecification.isValid(mockForm, 'low');
                $scope.priceSpecification.isValid(mockForm, 'high');
                expect($scope.invalidPriceReason).toBe('');
                expect($scope.disabled).toBeFalsy();
                expect(spyHigh.calledWith("pricerange", true)).toBeTruthy();
                expect(spyLow.calledWith("pricerange", true)).toBeTruthy();
                expect(spyLow.calledWith("price", true)).toBeTruthy();
                expect(spyHigh.calledWith("price", true)).toBeTruthy();
            });

            it('high price lower than low fails validation', function() {
                $scope.lowInput.low = 600;
                $scope.highInput.high = 500;
                var spyHigh = sinon.spy(mockForm.highPrice, '$setValidity');
                var spyLow = sinon.spy(mockForm.lowPrice, '$setValidity');
                $scope.priceSpecification.isValid(mockForm, 'low');
                expect($scope.invalidPriceReason).toBe('Price high cannot be less than price low!');
                expect($scope.disabled).toBeTruthy();
                expect(spyHigh.calledWith("pricerange", false)).toBeTruthy();
                expect(spyLow.calledWith("pricerange", false)).toBeTruthy();
            });

            it('high price lower than low then corrected should clear error', function() {
                $scope.lowInput.low = 600;
                $scope.highInput.high = 500;
                $scope.priceSpecification.isValid(mockForm, 'low');
                expect($scope.invalidPriceReason).not.toBe('');
                expect($scope.disabled).toBeTruthy();

                var spyHigh = sinon.spy(mockForm.highPrice, '$setValidity');
                var spyLow = sinon.spy(mockForm.lowPrice, '$setValidity');
                $scope.lowInput.low = 400;
                $scope.priceSpecification.isValid(mockForm, 'low');
                expect(spyHigh.calledWith("pricerange", true)).toBeTruthy();
                expect(spyLow.calledWith("pricerange", true)).toBeTruthy();
                expect($scope.disabled).toBeFalsy();
            });

            it('high price above product max fails validation', function (){
                $scope.highInput.high = 10000;
                var spyHigh = sinon.spy(mockForm.highPrice, '$setValidity');
                $scope.priceSpecification.isValid(mockForm, 'high');
                expect(spyHigh.calledWith("price", false)).toBeTruthy();
                expect($scope.invalidPriceReason).not.toBe('');
                expect($scope.disabled).toBeTruthy();
            });

            it('high price not on stepping fails validation', function (){
                $scope.highInput.high = 500.1;
                var spyHigh = sinon.spy(mockForm.highPrice, '$setValidity');
                $scope.priceSpecification.isValid(mockForm, 'high');
                expect(spyHigh.calledWith("price", false)).toBeTruthy();
                expect($scope.invalidPriceReason).not.toBe('');
                expect($scope.disabled).toBeTruthy();
            });

            it('low price below product min fails validation', function (){
                $scope.lowInput.low = 0.5;
                var spyLow = sinon.spy(mockForm.lowPrice, '$setValidity');
                $scope.priceSpecification.isValid(mockForm, 'low');
                expect(spyLow.calledWith("price", false)).toBeTruthy();
                expect($scope.invalidPriceReason).not.toBe('');
                expect($scope.disabled).toBeTruthy();
            });

            it('low price not on stepping fails validation', function (){
                $scope.lowInput.low = 500.1;
                var spyLow = sinon.spy(mockForm.lowPrice, '$setValidity');
                $scope.priceSpecification.isValid(mockForm, 'low');
                expect(spyLow.calledWith("price", false)).toBeTruthy();
                expect($scope.invalidPriceReason).not.toBe('');
                expect($scope.disabled).toBeTruthy();
            });
        });
    });
});
