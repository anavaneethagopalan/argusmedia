﻿angular.module('argusOpenMarkets.dashboard.bidaskstack.tpls',[]).run(function($templateCache){
    $templateCache.put('headerBidsRowTemplate.html',
        '<div ng-class="col.colIndex()" class="ngHeaderCell bidAskStackHeader" ng-repeat="col in renderedColumns" >\r' +
        '\n' +
        '\t<div class="bidAskStackHeaderCell" style="height: 100%;"><div ng-header-cell><button id="newBidButton" ng-class="canCreateOrder ? \'ae-cursor\' : \'normal-cursor\'" ng-show="col.field==\'order.price\'" ng-click="newBidAsk(\'Bid\')" class = "pull-left btn btn-sm btn-bidaskstack btn-bidstack">+ BID</button></div></div>\r' +
        '\n' +
        '</div>\r'
    );

    $templateCache.put('headerAsksRowTemplate.html',
        '<div ng-class="col.colIndex()" class="ngHeaderCell bidAskStackHeader" ng-repeat="col in renderedColumns" >\r' +
        '\n' +
        '\t<div class="bidAskStackHeaderCell" style="height: 100%;"><div ng-header-cell><button id="newAskButton" ng-class="canCreateOrder ? \'ae-cursor\' : \'normal-cursor\'" ng-show="col.field==\'order.price\'" ng-click="newBidAsk(\'Ask\')" class = "pull-right btn btn-bidaskstack btn-askstack btn-sm">+ ASK</button></div></div>\r' +
        '\n' +
        '</div>\r'
    );

    $templateCache.put('cellNotesIconTemplate.html',
        '<div id="iconNotes" class="ngCellNotesIconOverride" ng-class="col.colIndex()">' +
        '<i class="fa fa-list-alt fa-1" aria-hidden="true" ng-show="row.entity.notesPresent"></i>' +
        '</div>'
    );

    $templateCache.put('rowBidsTemplate.html',
        '<div class="bidAskStackGridRowHover" ng-class="{bidCellEven: row.rowIndex % 2 == 0,bidCellOdd: row.rowIndex % 2 != 0, ' +
        'nonTradeable: row.entity.nonTradeable, isTradeable: !row.entity.nonTradeable, noBroker: row.entity.noBroker, cellIsMine: row.entity.orderIsMine, coBrokering: row.entity.coBrokering }" >' +
        '<div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" ' +
        'hover pointing = "left" position="bottom right" show="showBidAsk(row.entity)" contentstyle="bidContentStyle" titlerowstyle="bidTitleRowStyle" ' +
        'titletemplate="Bid" content="{{row.entity.hoverContent}}" ng-click="bidAskRowClicked(row.entity, \'Bid\')" class="ngCell {{col.cellClass}}">\r\n' +
        '\t<div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div>\r\n' +
        '\t<div class="ngCellBidGridOverride" ng-class="{\'heldOrder\': row.entity.orderIsHeld}"  ng-cell></div>\r\n</div></div>'
    );

    $templateCache.put('rowAsksTemplate.html',
        '<div class="bidAskStackGridRowHover" ng-class="{askCellEven: row.rowIndex % 2 == 0,askCellOdd: row.rowIndex % 2 != 0, ' +
        'nonTradeable: row.entity.nonTradeable, isTradeable: !row.entity.nonTradeable, noBroker: row.entity.noBroker, cellIsMine: row.entity.orderIsMine, coBrokering: row.entity.coBrokering }" >' +
        '<div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" ' +
        'hover pointing="right" position="bottom right" show="showBidAsk(row.entity)" contentstyle="askContentStyle" titlerowstyle="askTitleRowStyle" ' +
        'titletemplate="Ask" content="{{row.entity.hoverContent}}" ng-click="bidAskRowClicked(row.entity, \'Ask\')" class="ngCell {{col.cellClass}}">\r\n' +
        '\t<div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div>\r\n' +
        '\t<div class="ngCellAskGridOverride" ng-class="{\'heldOrder\': row.entity.orderIsHeld}" ng-cell></div>\r\n</div></div>'
    );

    $templateCache.put('cellBidsPriceTemplate.html',
        '<div id="btnOpenBid" class="ngCellText bidAskPriceBorderRight" ng-class="col.colIndex()"><span class="bidPrice" ng-cell-text>{{row.getProperty(col.field) | number:2}}</span></div>'
    );

    $templateCache.put('cellBidsQuantityTemplate.html',
        '<div id="btnOpenBid" class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number:0}}</span></div>'
    );

    $templateCache.put('cellAsksPriceTemplate.html',
        '<div id="btnOpenAsk" class="ngCellText" ng-class="col.colIndex()"><span class="askPrice" ng-cell-text>{{row.getProperty(col.field) | number:2}}</span></div>'
    );

    $templateCache.put('cellAsksQuantityTemplate.html',
        '<div id="btnOpenAsk" class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number:0}}</span></div>'
    );

    $templateCache.put('bidsFooterTemplate.html',
        '<div class="bidAskGridBidsFooter bidAskGridFooter" class="ng-scope">'+
        '<button id="bidAskGridUpButton" class="bidAskGridUpButton" ng-disabled="!canClickUp()" ng-click="upClicked()" type="button">'+
        '<i ng-if="canClickUp()" class="fa fa-chevron-up fa-3 collapse-arrow" aria-hidden="true"></i>'+
        '<i  ng-if="!canClickUp()" class="fa fa-chevron-up fa-3 collapse-arrow-disabled" aria-hidden="true"></i>'+
        '</button>'+
        '</div>'
    );

    $templateCache.put('asksFooterTemplate.html',
        '<div class="bidAskGridAsksFooter bidAskGridFooter" class="ng-scope">'+
        '<button id="bidAskGridDownButton" class="bidAskGridDownButton" ng-disabled="!canClickDown()" ng-click="downClicked()" type="button"> ' +
        '<i ng-if="canClickDown()" class="fa fa-chevron-down fa-3 collapse-arrow" aria-hidden="true"></i>'+
        '<i ng-if="!canClickDown()" class="fa fa-chevron-down fa-3 collapse-arrow-disabled" aria-hidden="true"></i>' +
        '{{getMarketDepthDisplay()}}'+
        '</button>'+
        '</div>'
    );
});