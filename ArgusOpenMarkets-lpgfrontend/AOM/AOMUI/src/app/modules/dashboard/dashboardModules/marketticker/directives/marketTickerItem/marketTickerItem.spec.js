describe('market ticker item test', function () {
    var $scope,
        $compile,
        mockUserService,
        el;

    var mockTradePermissionsService = {canTrade:function(){}},
        dateTimeNow = new Date(),
        mockDateService = { getDateTimeNow: function(){ return dateTimeNow }};

    var toDateString = function(dt) {
        var yyyy = dt.getFullYear().toString();
        var mm = (dt.getMonth()+1).toString();
        var dd  = dt.getDate().toString();

        var mmChars = mm.split('');
        var ddChars = dd.split('');

        return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
    };

    var toLocalHours = function(utcHours){
        return Math.floor(utcHours - (new Date().getTimezoneOffset() / 60));
    };

    beforeEach(module('src/app/modules/dashboard/dashboardModules/marketticker/directives/marketTickerItem/marketTickerItem.part.html'));

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(function(){
        module(function($provide) {
            var $injector = angular.injector([ 'argusOpenMarkets.shared' ]);
            mockUserService = $injector.get( 'mockUserService' );
            $provide.value('userService', mockUserService);
            $provide.value('tradePermissionsService', mockTradePermissionsService);
            $provide.value('dateService', mockDateService);

            $provide.provider('htmlInterceptor', function() {
                this.$get = function () {
                    return {
                        request: function (config) {
                            return config;
                        }
                    };
                }
            });
        });
    });


    beforeEach(inject(function (_$compile_, _$rootScope_) {
        mockUserService.setUser(mockUserService.MOCK_USER_B1);
        $scope = _$rootScope_.$new();
        $scope.test = {
            "id": 1,
            "lastUpdated": "2014-12-09T11:49:11.049Z",
            "lastUpdatedUserId": 4,
            "lastUpdatedUser": null,
            "createdUserId": 0,
            "createdDate": "2014-12-09T11:49:11.049Z",
            "createdCompanyId": 0,
            "name": "CreateOrderMarketTick",
            "message": "NEW:  TradeCo #1 bids 30,000t Naphtha CIF NWE - Outright  10-Dec-2014 - 12-Dec-2014 at $900.00/t",
            "marketTickerItemStatus": "Pending",
            "marketTickerItemType": "Bid",
            "productIds": [1],
            "orderId": 129,
            "dealId": null,
            "externalDealId": null,
            "marketInfoId": null,
            "ownerOrganisations": [
                3,5
            ],
            "notes": "testNotes"
        };
        $compile = _$compile_;
        el = angular.element('<mt-item item="test" ></mt-item>');


    }));

    it('should determine if the mt item is yours', function () {
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.isMine).toEqual(true);
    });

    it('checks user cannot auth a BID', function () {
        mockUserService.setUser(mockUserService.MOCK_USER_AE1);
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.isPending ).toBeFalsy();
    });

    it('checks if user can auth info', function () {

        mockUserService.setUser(mockUserService.MOCK_USER_AE1);
        $scope.test.marketTickerItemType = "Info";
        $scope.test.marketInfoType = "";
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.isPending ).toEqual(true);
    });

    it('checks if user can auth deal', function () {
        mockUserService.setUser(mockUserService.MOCK_USER_AE1);
        $scope.test.marketTickerItemType = "Deal";
        $scope.test.marketInfoType = "";
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.isPending ).toEqual(true);
    });

    it('checks if item is void', function () {
        mockUserService.setUser(mockUserService.MOCK_USER_AE1);
        $scope.test.marketTickerItemStatus = "Void";
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.isVoid  ).toEqual(true);
    });

    it('checks if item is tradeable', function () {
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.notTradeable).toEqual(false);
    });

    it('returns the rounded down time when the ticker item is for an item today', function() {
        var now = new Date();
        var timeString = toDateString(now) + 'T16:00:29.983Z';
        $scope.test.lastUpdated = timeString;
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.formattedDate).toEqual(toLocalHours(16) + ":00");
    });

    it('returns the truncated time when the ticker item is for an item today', function() {
        var now = new Date();
        var timeString = toDateString(now) + 'T15:59:59.983Z';
        $scope.test.lastUpdated = timeString;
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.formattedDate).toEqual(toLocalHours(15) + ":59");
    });

    it('returns the rounded time and the date when the ticker item is for an item before today', function() {
        $scope.test.lastUpdated = "2014-12-09T11:49:29.049Z";
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.formattedDate).toEqual(11 + ':49 | 09-Dec-2014');
    });

    it('Rolling GetDate back to the same date as the article should return just the time element', function() {
        dateTimeNow = new Date();
        $scope.test.lastUpdated =  toDateString(dateTimeNow) + "T11:49:29.049Z";
        $compile(el)($scope);
        $scope.$digest();
        expect(el.scope().$$childHead.item.formattedDate).toEqual(toLocalHours(11) + ':49');
    });

    it('updateFormattedDate should set formatted date to time if still the day of publication', function() {
        $scope.test.lastUpdated = "2014-12-09T11:49:29.049Z";
        spyOn(mockDateService, "getDateTimeNow").and.returnValue(new Date(2014, 11, 9, 14, 0, 0));
        $compile(el)($scope);
        $scope.$digest();
        var item = el.scope().$$childHead.item;
        expect(item.formattedDate).toEqual(11 + ':49');
        mockDateService.getDateTimeNow.and.returnValue(new Date(2014, 11, 9, 18, 0, 0));
        item.updateFormattedDate();
        expect(item.formattedDate).toEqual(11 + ':49');
    });

    it('updateFormattedDate should set formatted date to date and time if after publication date', function() {
        $scope.test.lastUpdated = "2014-12-09T11:49:29.049Z";
        spyOn(mockDateService, "getDateTimeNow").and.returnValue(new Date(2014, 11, 09, 14, 0, 0));
        $compile(el)($scope);
        $scope.$digest();
        var item = el.scope().$$childHead.item;
        expect(item.formattedDate).toEqual(11 + ':49');
        mockDateService.getDateTimeNow.and.returnValue(new Date(2014, 11, 10, 09, 0, 0));
        item.updateFormattedDate();
        expect(item.formattedDate).toEqual(11 + ':49 | 09-Dec-2014');
    });

    describe('ticker type', function(){

        var defaultTickerItem = {
            "id": 1,
            "lastUpdated": "2014-12-09T11:49:11.049Z",
            "lastUpdatedUserId": 4,
            "lastUpdatedUser": null,
            "createdUserId": 0,
            "createdDate": "2014-12-09T11:49:11.049Z",
            "createdCompanyId": 0,
            "name": "CreateOrderMarketTick",
            "message": "NEW:  TradeCo #1 bids 30,000t Naphtha CIF NWE - Outright  10-Dec-2014 - 12-Dec-2014 at $900.00/t",
            "marketTickerItemStatus": "Pending",
            "marketTickerItemType": "Info",
            "marketInfoType": "",
            "productIds": [1],
            "orderId": 129,
            "dealId": null,
            "externalDealId": null,
            "marketInfoId": null,
            "ownerOrganisations": [
                3,5
            ],
            "notes": "testNotes"
        };

        it('returns the correct Ticker Type when the ticker item is internal info', function(){

            $compile(el)($scope);
            $scope.$digest();
            var tickerType = el.scope().$$childHead.getMarketTickerType(defaultTickerItem);
            expect(tickerType.tickerText).toEqual("INFO");
            expect(tickerType.tickerClass).toEqual("info");
            expect(tickerType.tickerPrefix).toEqual("");
            expect(tickerType.tickerExternal).toBe(false);
        });

        it('returns the correct Ticker Type when the ticker item is an external info', function(){

            defaultTickerItem.marketInfoType = "Info";

            $compile(el)($scope);
            $scope.$digest();
            var tickerType = el.scope().$$childHead.getMarketTickerType(defaultTickerItem);
            expect(tickerType.tickerText).toEqual("INFO");
            expect(tickerType.tickerClass).toEqual("info");
            expect(tickerType.tickerPrefix).toEqual("+");
            expect(tickerType.tickerExternal).toBe(true);
        });

        it('returns the correct Ticker Type when the ticker item is an external bid', function(){

            defaultTickerItem.marketTickerItemType = "Info";
            defaultTickerItem.marketInfoType = "Bid";

            $compile(el)($scope);
            $scope.$digest();
            var tickerType = el.scope().$$childHead.getMarketTickerType(defaultTickerItem);
            expect(tickerType.tickerText).toEqual("BID");
            expect(tickerType.tickerClass).toEqual("bid");
            expect(tickerType.tickerPrefix).toEqual("+");
            expect(tickerType.tickerExternal).toBe(true);
        });

        it('returns the correct Ticker Type when the ticker item is an external ask', function(){

            defaultTickerItem.marketTickerItemType = "Info";
            defaultTickerItem.marketInfoType = "Ask";

            $compile(el)($scope);
            $scope.$digest();
            var tickerType = el.scope().$$childHead.getMarketTickerType(defaultTickerItem);
            expect(tickerType.tickerText).toEqual("ASK");
            expect(tickerType.tickerClass).toEqual("ask");
            expect(tickerType.tickerPrefix).toEqual("+");
            expect(tickerType.tickerExternal).toBe(true);
        });

        it('returns the correct Ticker Type when the ticker item is an external deal from +Info', function(){

            defaultTickerItem.marketTickerItemType = "Info";
            defaultTickerItem.marketInfoType = "Deal";

            $compile(el)($scope);
            $scope.$digest();
            var tickerType = el.scope().$$childHead.getMarketTickerType(defaultTickerItem);
            expect(tickerType.tickerText).toEqual("DEAL");
            expect(tickerType.tickerClass).toEqual("deal");
            expect(tickerType.tickerPrefix).toEqual("+");
            expect(tickerType.tickerExternal).toBe(true);
        });

        it('returns the correct Ticker Type when the ticker item is an external deal from +Deal', function() {
            var tickerItem = angular.copy(defaultTickerItem);
            tickerItem.marketTickerItemType = "Deal";
            tickerItem.externalDealId = "123";
            expect(tickerItem.dealId).toBeFalsy();
            $compile(el)($scope);
            $scope.$digest();
            var tickerType = el.scope().$$childHead.getMarketTickerType(tickerItem);
            expect(tickerType.tickerText).toEqual("DEAL");
            expect(tickerType.tickerClass).toEqual("deal");
            expect(tickerType.tickerPrefix).toEqual("+");
            expect(tickerType.tickerExternal).toBe(true);
        });

        it('returns the correct Ticker Type when the ticker item is an internal deal', function() {
            var tickerItem = angular.copy(defaultTickerItem);
            tickerItem.marketTickerItemType = "Deal";
            tickerItem.dealId = "123";
            expect(tickerItem.externalDealId).toBeFalsy();
            $compile(el)($scope);
            $scope.$digest();
            var tickerType = el.scope().$$childHead.getMarketTickerType(tickerItem);
            expect(tickerType.tickerText).toEqual("DEAL");
            expect(tickerType.tickerClass).toEqual("deal");
            expect(tickerType.tickerPrefix).toEqual("");
            expect(tickerType.tickerExternal).toBe(false);
        });
    });
});