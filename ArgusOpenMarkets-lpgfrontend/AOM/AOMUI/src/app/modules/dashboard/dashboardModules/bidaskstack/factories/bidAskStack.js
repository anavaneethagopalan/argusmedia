﻿angular.module('argusOpenMarkets.dashboard.bidaskstack').factory("BidAskStack",
    function (userOrderService,
              userService,
              messagePopupsService,
              tradePermissionsService,
              helpers,
              $filter,
              productConfigurationService) {

        function BidAskStack() {
            this.bids = [];
            this.asks = [];
        }

        function Bid(order) {
            return new Order(order, userService);
        }

        function Ask(order) {
            return new Order(order, userService);
        }

        function Order(order, userService) {
            var notesText = order.notes;
            this.notesPresent = (!helpers.isBlank(notesText));
            this.checkNonTradeable = function () {
            };
            this.checkCoBrokering = function () {
            };
            this.refreshOnPermissionChange = function () {
            };

            if (helpers.isEmpty(order.id)) {
                this.order = {
                    price: null,
                    quantity: null,
                    principal: null,
                    broker: null,
                    deliveryLocation: null,
                    deliveryDate: null,
                    notes: null,
                    userId: null,
                    orderStatus: null,
                    id: null
                };
                this.hoverContent = [];
            }
            else {
                this.order = order;

                if (this.notesPresent && notesText.length > 200) {
                    notesText = notesText.substring(0, 200) + ' ...';
                }
                if (this.order !== null && this.order.id !== null) {

                    this.checkNonTradeable = function () {

                        if (userService.hasProductPrivilege(this.order.productId, ["Order_Aggress_Broker"]) && userOrderService.isOrderMine(this.order)) {
                            return false;
                        }
                        else {
                            var productConfig = productConfigurationService.getProductConfigurationById(this.order.productId);
                            var productIsCoBrokerable = ((productConfig === null || typeof productConfig === 'undefined') ? false : productConfig.coBrokering);

                            return !tradePermissionsService.canTrade(this.order.productId,
                                    this.orderBuySell,
                                    this.order.principalOrganisationId,
                                    this.order.brokerOrganisationId,
                                    this.order.coBrokering && productIsCoBrokerable) ||
                                (this.order.brokerRestriction === "None" && userService.isBroker());

                            //return (userOrderService.isOrderFromAnotherBroker(this.order) ||
                            //!tradePermissionsService.canTrade(this.order.productId, this.orderBuySell, this.order.principalOrganisationId, this.order.brokerOrganisationId) ||
                            //(this.order.brokerRestriction === "None" && userService.isBroker()));
                        }
                    };

                    this.checkCoBrokering = function (orderIsMine, orderNonTradeable, userIsBroker) {

                        if (orderIsMine) {
                            return false;
                        }

                        if (orderNonTradeable) {
                            return false;
                        }

                        if (userIsBroker) {
                            return this.order.coBrokering;
                        } else {
                            return false;
                        }
                    };

                    this.refreshOnPermissionChange = function () {
                        this.nonTradeable = this.checkNonTradeable();
                        this.coBrokering = this.checkCoBrokering(this.orderIsMine, this.nonTradeable, userService.isBroker());
                        this.hoverContent = this.buildHoverText();
                    };

                    this.buildHoverText = function () {

                        if (helpers.isEmpty(order.id)) {
                            return [];
                        }

                        var coBrokerableText = "Co-brokering enabled";
                        var productConfig = productConfigurationService.getProductConfigurationById(this.order.productId);

                        var hoverItems = [
                            {title: "Order Id", content: this.order.id},
                            {
                                title: "Status",
                                content: (userOrderService.isOrderFromAnotherBroker(this.order) && !this.order.coBrokering) ? "Not tradeable" : this.order.orderStatus
                            },
                            {title: "Tradeable", content: this.nonTradeable ? "No" : "Yes"},
                            {title: coBrokerableText, content: this.order.coBrokering ? "Yes" : "No"},
                            {title: "Price", content: helpers.formatNumberWithCommas(this.order.price, 2)},
                            {title: "Quantity", content: helpers.formatNumberWithCommas(this.order.quantity, 0)},
                            {title: "Principal", content: this.order.principalDisplayName},
                            {title: "Broker", content: this.order.brokerDisplayName},
                            {title: configLabels.locationLabel, content: this.order.tenor.deliveryLocation.name},
                            {
                                title: configLabels.deliveryPeriodLabel,
                                content: $filter("date")(this.order.deliveryStartDate, "d-MMM") + " - " + $filter("date")(this.order.deliveryEndDate, "d-MMM")
                            }
                        ];

                        if (this.order.metaData) {
                            for (var i = 0; i < this.order.metaData.length; i++) {
                                hoverItems.push({
                                    'title': this.order.metaData[i].displayName,
                                    'content': this.order.metaData[i].displayValue
                                });
                            }
                        }

                        hoverItems.push({title: "Notes", content: notesText});

                        // Add last updated at the end of the hover box.
                        hoverItems.push({
                            title: "Last Updated",
                            content: $filter("date")(this.order.lastUpdated, "d-MMM H:mm")
                        });


                        if ((productConfig === null || typeof productConfig === 'undefined') ? true : !productConfig.coBrokering) {
                            hoverItems.removeFirstMatch(function (x) {
                                return x.title === coBrokerableText;
                            });
                        }

                        return hoverItems;
                    };

                    this.orderIsHeld = this.order.orderStatus === "Held";
                    this.orderIsMine = userOrderService.isOrderMine(this.order);
                    this.orderBuySell = this.order.orderType === "Ask" ? "sell" : "buy";

                    this.nonTradeable = this.checkNonTradeable();
                    this.coBrokering = this.checkCoBrokering(this.orderIsMine, this.nonTradeable, userService.isBroker());
                    this.noBroker = (this.order.brokerRestriction === "None" && userService.isBroker());
                }

                var configLabels = productConfigurationService.getProductConfigurationLabels(this.order.productId);
                this.hoverContent = this.buildHoverText();

                // NOTE Added in a strip out of the Z from dates returned from the Server - as this is adjusting the date
                // to the users locale.  So for example - if the date coming back from the server is: 10 April 2017 00:00:00
                // Then when we call the angular filter for date - with D parameter - then we are getting
                // the original date adjusted to the users locale.  So if in California - then the date is taken back
                // -8 hours - which results in it appearing as if the date range was a day earlier.  Same for the end date
                this.orderShortDeliveryPeriod = $filter('date')(this.order.deliveryStartDate, 'd') + '-' + $filter('date')(this.order.deliveryEndDate, 'd');
            }

            // this.startBlink = function () {
            //     // var self = this;
            //     // self.blink = true;
            //     // setTimeout(function () {
            //     //     self.blink = false;
            //     // }, 1000);
            // };
        }

        function bidsSortFunction(left, right) {
            /*
             Highest price on top, if price match then oldest order on top
             */
            var p1 = left.order.price;
            var p2 = right.order.price;

            if (p1 !== p2) {
                return p2 - p1;
            }

            return (new Date(left.order.lastUpdated) - new Date(right.order.lastUpdated));
        }

        function asksSortFunction(left, right) {
            /*
             Lowest price on top, if price match then oldest order on top
             */
            var p1 = left.order.price;
            var p2 = right.order.price;

            if (p1 === null && p2 !== null) {
                return 1;
            }
            if (p2 === null && p1 !== null) {
                return -1;
            }

            if (p1 !== p2) {
                return p1 - p2;
            }
            return (new Date(left.order.lastUpdated) - new Date(right.order.lastUpdated));
        }

        function findIndexOfOrderInArrayById(items, id) {
            var length = items.length;
            var currentOrder = null;
            var index = -1;
            for (var i = 0; i < length; i++) {
                currentOrder = items[i];
                if (!helpers.isEmpty(currentOrder.order.id)) {
                    if (currentOrder.order.id === id) {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        function findIndexOfOrderInArrayByTopicKey(items, topicKey) {
            var length = items.length;
            var currentOrder = null;
            var index = -1;
            for (var i = 0; i < length; i++) {
                currentOrder = items[i];
                if (!helpers.isEmpty(currentOrder.order.topicKey)) {
                    if (currentOrder.order.topicKey === topicKey) {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        function addOrderToStack(order, items, constructor) {

            var item = constructor(order);
            items.push(item);
            return item;
        }

        function removeOrderFromStack(order, items) {
            var indexToRemove = findIndexOfOrderInArrayById(items, order.id);
            if (indexToRemove > -1) {
                items.splice(indexToRemove, 1);
            }
        }

        function replaceOrderInStackWithUpdatedOrder(order, items, constructor) {
            var indexToRemove = findIndexOfOrderInArrayById(items, order.id);
            if (indexToRemove > -1) {
                items[indexToRemove] = new constructor(order);
                return items[indexToRemove];
            }
        }

        function handleOrderMessage(orderMessage, constructorForNewItem, existingItems, sortFunction) {

            var messageAction = orderMessage.message.messageAction,
                item;

            if (orderMessage.messageType === "ERROR") {
                window.alert("ERROR\n\nError message received in BASC: " + orderMessage.message.messageBody);
            }
            else {

                var order = orderMessage.order;

                var orderAlreadyExists = (findIndexOfOrderInArrayById(existingItems, order.id) > -1);
                switch (messageAction) {
                    case "Create":

                        //if(order.brokerRestriction === "Any"){
                        //    order.brokerShortCode = "*";
                        //}

                        if (orderAlreadyExists) {
                            // replaceOrderInStackWithUpdatedOrder(order, existingItems, constructorForNewItem);
                            item = replaceOrderInStackWithUpdatedOrder(order, existingItems, constructorForNewItem);
                            //item.startBlink();
                        }
                        else {
                            // addOrderToStack(order, existingItems, constructorForNewItem);
                            item = addOrderToStack(order, existingItems, constructorForNewItem);
                            //item.startBlink();
                        }
                        break;
                    case "Kill":
                        if (orderAlreadyExists) {
                            removeOrderFromStack(order, existingItems);
                        }
                        break;
                    case "Update":
                        if (orderAlreadyExists) {
                            // replaceOrderInStackWithUpdatedOrder(order, existingItems, constructorForNewItem);
                            item = replaceOrderInStackWithUpdatedOrder(order, existingItems, constructorForNewItem);
                            //item.startBlink();
                        }
                        else {
                            // addOrderToStack(order, existingItems, constructorForNewItem);
                            item = addOrderToStack(order, existingItems, constructorForNewItem);
                            //item.startBlink();
                        }
                        break;
                    case "Execute":
                        if (orderAlreadyExists) {
                            removeOrderFromStack(order, existingItems);
                        }
                        break;
                    case "Hold":
                        if (userOrderService.isOrderMine(order)) {
                            if (orderAlreadyExists) {
                                // replaceOrderInStackWithUpdatedOrder(order, existingItems, constructorForNewItem);
                                item = replaceOrderInStackWithUpdatedOrder(order, existingItems, constructorForNewItem);
                                // item.startBlink();
                            }
                            else {
                                // addOrderToStack(order, existingItems, constructorForNewItem);
                                item = addOrderToStack(order, existingItems, constructorForNewItem);
                                // item.startBlink();
                            }
                        }
                        else {
                            if (orderAlreadyExists) {
                                removeOrderFromStack(order, existingItems);
                            }
                        }
                        break;
                    case "Reinstate":
                        if (orderAlreadyExists) {
                            // replaceOrderInStackWithUpdatedOrder(order, existingItems, constructorForNewItem);
                            item = replaceOrderInStackWithUpdatedOrder(order, existingItems, constructorForNewItem);
                            // item.startBlink();
                        }
                        else {
                            // addOrderToStack(order, existingItems, constructorForNewItem);
                            item = addOrderToStack(order, existingItems, constructorForNewItem);
                            // item.startBlink();
                        }
                        break;
                    default:
                        break;
                }

                existingItems.sort(sortFunction);
            }
        }

        /*
         Instance methods
         */

        BidAskStack.prototype.getMarketDepth = function () {
            return Math.max(this.asks.length, this.bids.length);
        };

        BidAskStack.prototype.onBidMessageEventHandler = function (orderMessage) {

            handleOrderMessage(orderMessage, Bid, this.bids, bidsSortFunction);
        };

        BidAskStack.prototype.onAskMessageEventHandler = function (orderMessage) {

            handleOrderMessage(orderMessage, Ask, this.asks, asksSortFunction);
        };

        BidAskStack.prototype.deleteOrderIfExists = function (orderKey, callbackOnBidDeleted, callbackOnAskDeleted) {

            var indexOfBid = findIndexOfOrderInArrayByTopicKey(this.bids, orderKey);
            if (indexOfBid > -1) {
                this.bids.splice(indexOfBid, 1);
                callbackOnBidDeleted();
                return true;
            }

            var indexOfAsk = findIndexOfOrderInArrayByTopicKey(this.asks, orderKey);
            if (indexOfAsk > -1) {
                this.asks.splice(indexOfAsk, 1);
                callbackOnAskDeleted();
                return true;
            }

            return false;
        };

        /*
         Static methods
         */
        BidAskStack.createEmptyBid = function () {
            return new Bid({});
        };

        BidAskStack.createEmptyAsk = function () {
            return new Ask({});
        };

        BidAskStack.processOrder = function (order) {
            return new Order(order);
        };

        return ( BidAskStack );

    });
