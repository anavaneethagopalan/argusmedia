describe('marketTickerService', function() {

    var mockUserService, mockProductConfigurationService, marketTickerService;

    var mockSubsService = jasmine.createSpyObj('marketTickerMessageSubscriptionService',
        [
            'subscribeToMarketTickerConfig',
            'registerActionOnMarketTickerConfigChange',
            'registerActionOnMarketTickerMessageType',
            'getMarketTickerItems',
            'registerMarketTickerItemDeleted',
            'subscribeToTopic'
        ]);

    var createFakeMarketTickerItem = function(itemType, id, externalId, lastUpdated, productIds, message) {
        var fakeItem = {
            marketTickerItem: {
                id: id,
                marketTickerItemType: itemType,
                lastUpdated: lastUpdated.toISOString(),
                productIds: productIds,
                ownerOrganisations: [],
                message: message
            }
        };
        if (itemType === 'Info') {
            fakeItem.marketTickerItem.marketInfoId = externalId;
        } else if (itemType === 'Deal') {
            fakeItem.marketTickerItem.dealId = externalId;
        } else if (itemType === 'ExternalDeal') {
            fakeItem.marketTickerItem.marketTickerItemType = 'Deal';
            fakeItem.marketTickerItem.externalDealId = externalId;
        } else if (itemType === 'Bid' || itemType === 'Ask') {
            fakeItem.marketTickerItem.orderId = externalId;
        }
        return fakeItem;
    };

    var processTickerMessage = function(marketTickerItem) {
        var processTickerMessageCallback = null;
        mockSubsService.registerActionOnMarketTickerMessageType.and.callFake(function(itemType, callback) {
            processTickerMessageCallback = callback;
        });
        var marketTickerItems = [];
        var numberCallbackCalls = 0;
        marketTickerService.onMarketTickRegister(function(items){marketTickerItems = items; numberCallbackCalls++;});
        marketTickerService.onMarketTickRegisterPushedItems(function(marketTickerItem, positionToSplice, items){marketTickerItems = items; numberCallbackCalls++;});
        expect(processTickerMessageCallback).not.toBeNull();
        processTickerMessageCallback(marketTickerItem);
        return marketTickerItems;
    };

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.dashboard'));

    beforeEach(function () {
        module(function ($provide) {
            var $injector = angular.injector(['argusOpenMarkets.shared']);
            mockUserService = $injector.get('mockUserService');
            mockProductConfigurationService = $injector.get('mockProductConfigurationService');
            spyOn(mockProductConfigurationService, 'registerForProductConfigurationChanges').and.callThrough();
            $provide.value('userService', mockUserService);
            $provide.value('marketTickerMessageSubscriptionService', mockSubsService);
            $provide.value('productConfigurationService', mockProductConfigurationService);
        });
    });

    beforeEach(inject(function(_marketTickerService_){
        marketTickerService = _marketTickerService_;
    }));


    it('should have a defined service', function() {
        expect(marketTickerService).toBeDefined();
    });
    it('should register for product configuration changes', function(){
        expect(mockProductConfigurationService.registerForProductConfigurationChanges).toHaveBeenCalledWith(
            jasmine.any(Function));
    });
    it('should tick all products when created', function() {
        var productConfigs = marketTickerService.getMarketTickerProducts();
        productConfigs.forEach(function(p) { expect(p.ticked).toBeTruthy(); });
    });
    it('should have the market ticker products supplied by the product configuration service', function(){
        var productConfigs = marketTickerService.getMarketTickerProducts();
        var expectedConfigs = mockProductConfigurationService.getAllProductConfigurations();
        expectedConfigs.forEach(function (p) {p.ticked = true;});
        expect(productConfigs).toEqual(expectedConfigs);
    });
    it('should add a new market ticker item if it currently has no items', function() {
        var fakeItem = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1), [1]);
        var marketTickerItems = processTickerMessage(fakeItem);
        expect(marketTickerItems.length).toEqual(1);
        expect(marketTickerItems[0]).toEqual(fakeItem.marketTickerItem);
    });
    it('should add a second item with higher id to front of list', function() {
        var fakeItem1 = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1), [1]);
        var marketTickerItems = processTickerMessage(fakeItem1);
        expect(marketTickerItems.length).toEqual(1);
        var fakeItem2 = createFakeMarketTickerItem('Info', 2, 124, new Date(2015, 0, 1), [1]);
        marketTickerItems = processTickerMessage(fakeItem2);
        expect(marketTickerItems.length).toEqual(2);
        expect(marketTickerItems[0]).toEqual(fakeItem2.marketTickerItem);
        expect(marketTickerItems[1]).toEqual(fakeItem1.marketTickerItem);
    });
    it('should add a second item that has a lower id to the end of the list', function() {
        var fakeItem1 = createFakeMarketTickerItem('Info', 2, 124, new Date(2015, 0, 1), [1]);
        var marketTickerItems = processTickerMessage(fakeItem1);
        expect(marketTickerItems.length).toEqual(1);
        var fakeItem2 = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1), [1]);
        marketTickerItems = processTickerMessage(fakeItem2);
        expect(marketTickerItems.length).toEqual(2);
        expect(marketTickerItems[0]).toEqual(fakeItem1.marketTickerItem);
        expect(marketTickerItems[1]).toEqual(fakeItem2.marketTickerItem);
    });

    it('should add a new info item when the status changes and remove the original info item', function() {
        var fakeItem = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1), [1]);
        fakeItem.marketTickerItem.ownerOrganisations.push(mockUserService.getUsersOrganisation().id);
        var marketTickerItems = processTickerMessage(fakeItem);
        expect(marketTickerItems.length).toEqual(1);
        var fakeItem2 = angular.copy(fakeItem);
        fakeItem2.marketTickerItem.id = 2;
        fakeItem2.marketTickerItem.lastUpdated = new Date(2015, 0, 1, 10, 1).toISOString();
        marketTickerItems = processTickerMessage(fakeItem2);
        expect(marketTickerItems.length).toEqual(2);
        expect(marketTickerItems[0].id).toEqual(2);
    });
    it('should replace an info item if the you are an editor and an update for that item is received', function() {
        spyOn(mockUserService, 'hasAnyProductPrivileges').and.callFake(function() { return true; });
        var fakeItem = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1, 10, 0), [1]);
        var marketTickerItems = processTickerMessage(fakeItem);
        expect(marketTickerItems.length).toEqual(1);
        var fakeItem2 = angular.copy(fakeItem);
        fakeItem2.marketTickerItem.id = 2;
        fakeItem2.marketTickerItem.lastUpdated = new Date(2015, 0, 1, 10, 1).toISOString();
        marketTickerItems = processTickerMessage(fakeItem2);
        expect(marketTickerItems.length).toEqual(2);
        expect(marketTickerItems[0].id).toEqual(2);
    });
    it('should add a second bid if a new bid is added', function() {
        var fakeItem1 = createFakeMarketTickerItem('Bid', 1, 123, new Date(2015, 0, 1, 10, 0), [1]);
        var fakeItem2 = createFakeMarketTickerItem('Bid', 2, 234, new Date(2015, 0, 1, 10, 1), [1]);
        var marketTickerItems = processTickerMessage(fakeItem1);
        expect(marketTickerItems.length).toEqual(1);
        marketTickerItems = processTickerMessage(fakeItem2);
        expect(marketTickerItems.length).toEqual(2);
        expect(marketTickerItems[0]).toEqual(fakeItem2.marketTickerItem);
        expect(marketTickerItems[1]).toEqual(fakeItem1.marketTickerItem);
    });
    it('should replace an existing bid if an update for that bid is received', function() {
        var fakeItem = createFakeMarketTickerItem('Bid', 1, 123, new Date(2015, 0, 1, 10, 0), [1]);
        fakeItem.marketTickerItem.name = 'CreateOrderMarketTick';
        var marketTickerItems = processTickerMessage(fakeItem);
        expect(marketTickerItems.length).toEqual(1);
        var fakeItem2 = angular.copy(fakeItem);
        fakeItem2.marketTickerItem.id = 2;
        fakeItem2.marketTickerItem.name = 'UpdateOrderMarketTick';
        fakeItem2.marketTickerItem.lastUpdated = new Date(2015, 0, 1, 10, 1).toISOString();
        marketTickerItems = processTickerMessage(fakeItem2);
        expect(marketTickerItems.length).toEqual(2);
        expect(marketTickerItems[0]).toEqual(fakeItem2.marketTickerItem);
    });
    it('should use the highest id update for a deal if two are received at the same time', function() {
        // This happens when a pending deal is voided
        var fakeItem1 = createFakeMarketTickerItem('Deal', 1, 123, new Date(2015, 0, 1, 10, 0), [1], "update1");
        var fakeItem2 = createFakeMarketTickerItem('Deal', 2, 123, new Date(2015, 0, 1, 10, 0), [1], "update2");
        var marketTickerItems = processTickerMessage(fakeItem1);
        expect(marketTickerItems.length).toEqual(1);
        marketTickerItems = processTickerMessage(fakeItem2);
        expect(marketTickerItems.length).toEqual(2);
        expect(marketTickerItems[0].message).toEqual(fakeItem2.marketTickerItem.message);
    });

    describe('checkPrivilegeSendAndWait', function() {
        beforeEach(function() {
            spyOn(mockUserService, "sendStandardFormatMessageToUsersTopic");
        });

        describe('checking product privileges', function() {

            describe('for Market Info items', function() {
                it('should check for MarketTicker_Authenticate for any product for an item with null productIds', function() {
                    var fakeItem = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1, 10, 0), null, "info1");
                    spyOn(mockUserService, "hasProductPrivilegeOnAnyProduct");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.hasProductPrivilegeOnAnyProduct).toHaveBeenCalledWith(["MarketTicker_Authenticate"]);
                });
                it('should check for MarketTicker_Authenticate for any product for an item with empty productIds', function() {
                    var fakeItem = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1, 10, 0), [], "info1");
                    spyOn(mockUserService, "hasProductPrivilegeOnAnyProduct");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.hasProductPrivilegeOnAnyProduct).toHaveBeenCalledWith(["MarketTicker_Authenticate"]);
                });
                it('should check for MarketTicker_Authenticate for specified products for an item with productIds', function() {
                    var fakeItem = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1, 10, 0), [1,2,3,4], "info1");
                    spyOn(mockUserService, "hasAnyProductPrivileges");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.hasAnyProductPrivileges).toHaveBeenCalledWith([1,2,3,4], ["MarketTicker_Authenticate"]);
                });
            });

            describe('for Deal items', function() {
                it('should check for Deal_Authenticate for any product for an item with null productIds', function() {
                    var fakeItem = createFakeMarketTickerItem('Deal', 1, 123, new Date(2015, 0, 1, 10, 0), null, "deal1");
                    spyOn(mockUserService, "hasProductPrivilegeOnAnyProduct");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.hasProductPrivilegeOnAnyProduct).toHaveBeenCalledWith(["Deal_Authenticate"]);
                });
                it('should check for Deal_Authenticate for any product for an item with empty productIds', function() {
                    var fakeItem = createFakeMarketTickerItem('Deal', 1, 123, new Date(2015, 0, 1, 10, 0), null, "deal1");
                    spyOn(mockUserService, "hasProductPrivilegeOnAnyProduct");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.hasProductPrivilegeOnAnyProduct).toHaveBeenCalledWith(["Deal_Authenticate"]);
                });
                it('should check for Deal_Authenticate for specified products for an item with productIds', function() {
                    var fakeItem = createFakeMarketTickerItem('Deal', 1, 123, new Date(2015, 0, 1, 10, 0), [1,2,3,4], "deal1");
                    spyOn(mockUserService, "hasAnyProductPrivileges");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.hasAnyProductPrivileges).toHaveBeenCalledWith([1,2,3,4], ["Deal_Authenticate"]);
                });
            });
        });

        describe('user service message sending', function() {

            beforeEach(function() {
                spyOn(mockUserService, "hasAnyProductPrivileges").and.callFake(function () {
                    return true;
                });
            });

            describe('for Market Info items', function() {
                it('should send a message for an Info item with a non-null market info ID', function () {
                    var fakeItem = createFakeMarketTickerItem('Info', 1, 123, new Date(2015, 0, 1, 10, 0), [1], "info1");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith(
                        'Info', 'Request', {"id":123});
                });
                it('should not send a message for an Info item with a null market info ID', function () {
                    var fakeItem = createFakeMarketTickerItem('Info', 1, null, new Date(2015, 0, 1, 10, 0), [1], "info1");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.sendStandardFormatMessageToUsersTopic).not.toHaveBeenCalled();
                });
            });

            describe('for Deal items', function() {
                it('should send a message for an internal Deal item with a non-null deal ID', function () {
                    var fakeItem = createFakeMarketTickerItem('Deal', 1, 123, new Date(2015, 0, 1, 10, 0), [1], "deal1");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith(
                        'Deal', 'Request', jasmine.any(Object));
                });
                it('should send a message for an External Deal item with a null market deal ID', function () {
                    var fakeItem = createFakeMarketTickerItem('Deal', 1, null, new Date(2015, 0, 1, 10, 0), [1], "deal1");
                    marketTickerService.checkPrivilegesSendAndWait(fakeItem.marketTickerItem);
                    expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith(
                        'ExternalDeal', 'Request', jasmine.any(Object));
                });
            });

        });
    });

    // TODO Test productIdsAndRoles and registerActionOnMarketTickerConfigChange callback function
});
