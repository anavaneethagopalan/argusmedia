﻿describe( 'argusOpenMarkets.dashboard.executeOrder', function() {
    "use strict";
    describe( 'executeOrderController', function() {
        var $scope,mockProductConfigurationService,mockModalInstance, mockUserOrderService, mockUserService, mockBrokerUserService, mockState, mockOrderType,thisController;
        var mockUserApi, mockBrokerUserApi, mockOrder, mockProductMessageSubscriptionService;
        var editOrderCallbackOrderUpdated, editOrderCallbackAskUpdated, editOrderCallbackOrderDeleted, provide;

        var userOrganisationTrading = {
            id : 100,
            name : "a trading org",
            shortCode : "XYZ",
            organisationType: "Trading"
        };

        var userOrganisationBrokerage = {
            id : 200,
            name : "a brokerage org",
            shortCode : "XYZ",
            organisationType: "Brokerage"
        };

        var metadataDefinitions = [{
            "metadataList" : {
                "id": 3,
                "fieldList": [
                    {
                        "id": 1,
                        "displayName": "Port A",
                        "itemValue": 100
                    },
                    {
                        "id": 2,
                        "displayName": "Port B",
                        "itemValue": 101
                    },
                    {
                        "id": 3,
                        "displayName": "Port C",
                        "itemValue": 102
                    }
                ]},
            "id": 1,
            "productId": 1,
            "displayName": "Delivery Port",
            "fieldType": "IntegerEnum"
        }, {
            "valueMinimum": 0,
            "valueMaximum": 20,
            "id": 3,
            "productId": 1,
            "displayName": "Delivery Description",
            "fieldType": "String"
        }];

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module(function($provide) { provide = $provide; }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );

        beforeEach( inject( function( $controller, $rootScope ) {
            $scope = $rootScope.$new();

            Date.prototype.addDays = function (days) {
                this.setDate(this.getDate() + days);
                return this;
            };

            mockState = {
                current: {
                    name: 'dashboard'
                }
            };

            mockOrderType = 'bid';

            mockProductConfigurationService = {
                getProductConfigurationById:function(productId)
                {
                    return {
                        "productId": 1,
                        "productName": "Naphtha CIF NWE - Cargoes",
                        "productTitle": "Naphtha CIF NWE - Cargoes",
                        "isInternal": false,
                        "hasAssessment": true,
                        "status": 1,
                        "commodity": {
                            "id": 1,
                            "name": "Naphtha",
                            "enteredByUserId": 0,
                            "lastUpdatedUserId": 0,
                            "dateCreated": "0001-01-01T00:00:00Z",
                            "lastUpdated": "0001-01-01T00:00:00Z"
                        },
                        "contractSpecification": {
                            "volume": {
                                "default": 27000,
                                "minimum": 12500,
                                "maximum": 55000,
                                "increment": 500,
                                "decimalPlaces": 0,
                                "volumeUnitCode": "t",
                                "commonQuantities": [30000, 17500, 25000, 27000, 12500]
                            },
                            "pricing": {
                                "minimum": -100,
                                "maximum": 99999,
                                "increment": 0.25,
                                "decimalPlaces": 2,
                                "pricingUnitCode": "t",
                                "pricingCurrencyCode": "$"
                            },
                            "tenors": [{
                                "deliveryLocation": {
                                    "id": 1,
                                    "name": "Rotterdam",
                                    "dateCreated": "2014-01-01T00:00:00Z"
                                },
                                "deliveryStartDate": "+1d",
                                "deliveryEndDate": "+20d",
                                "rollDate": "DAILY",
                                "id": 1,
                                "minimumDeliveryRange": 3
                            }],
                            "contractType": "Physical"
                        },
                        "bidAskStackNumberRows": 6,
                        "allowNegativePriceForExternalDeals": false,
                        "displayOptionalPriceDetail": false,
                        "contentStreamIds": [95028, 95508, 95525],
                        "locationLabel": null,
                        "deliveryPeriodLabel": null,
                        "coBrokering": true
                    }
                },
                getProductConfigurationLabels: function(id){
                    return {
                        "locationLabel": "Location",
                        "deliveryPeriodLabel": "Delivery Period"
                    };
                },
                getProductMetaDataById: function(id){
                    return { "productId": 1, "fields": metadataDefinitions };
                }
            };

            provide.value('productConfigurationService', mockProductConfigurationService);

            mockModalInstance={
                dismiss: function(){
                    return true;
                }
            };

            var user  = {
                organisation : userOrganisationTrading
            };

            var userInBrokerage  = {
                organisation : userOrganisationBrokerage
            };

            mockOrder = {
                "id": 100,
                "price": 10,
                "nonTradeable": true,
                "principalOrganisationId" : 666,

                tenor: {"deliveryStartDate": 2, "deliveryEndDate": 10, "deliveryLocation": "Amsterdam"}};

            $scope.selectedPrincipal = {
                userId: {},
                organisationId: {}
            };

            mockUserService = {
                getUser: function(){ return user;},
                getUsersOrganisation : function(){return user.organisation;},
                hasProductPrivilege: function(productId, privs){return true;},
                isBroker: function(){ return false;}
            };

            mockBrokerUserService = {
                getUser: function(){ return userInBrokerage;},
                getUsersOrganisation : function(){return userInBrokerage.organisation;},
                hasProductPrivilege: function(productId, privs){return true;},
                isBroker: function(){ return true;}
            };

            mockUserOrderService = {
                sendExecuteOrderMessage:sinon.spy()
            };

            mockUserApi = {
                getPrincipals: function (productId, dataCallback, errorCallback) {
                    dataCallback([{"userId": 1}, {"userId": 1}, {"Meee": 3}]);
                },
                getBrokers: function (productId, dataCallback, errorCallback) {
                    dataCallback([{"userId": 1}, {"userId": 2}]);
                },
                getCommonPermissionedOrganisations: function(){
                    return {then:function(){}};
                },
                formatErrorResponse: function (error) {
                    this.error = error;
                },
                error: ""
            };

            mockBrokerUserApi = mockUserApi;

            mockProductMessageSubscriptionService = {
                registerActionOnProductMessageType: function(registerId, uniqueId, event, callback){
                    // "Edit Order Controller " + $scope.order.productId,  $scope.order.productId, "onBid", onBidEventHandler
                    if(event === "onBid") {
                        editOrderCallbackOrderUpdated = callback;
                    }

                    if(event === "onAsk"){
                        editOrderCallbackAskUpdated = callback;
                    }

                    if(event === "onOrderDeleted"){
                        editOrderCallbackOrderDeleted = callback;
                    }
                }
            };
            thisController = $controller( 'executeOrderController',
                {
                    $scope: $scope,
                    $state:mockState,
                    userOrderService:mockUserOrderService,
                    productConfigurationService: mockProductConfigurationService,
                    $modalInstance:mockModalInstance,
                    orderType:mockOrderType,
                    productId:1,
                    order:mockOrder,
                    userService: mockUserService,
                    userApi: mockUserApi,
                    productMessageSubscriptionService: mockProductMessageSubscriptionService
                });
        }));

        it( 'should be defined', inject( function() {
            expect( thisController ).toBeDefined();
        }));

        it('should set order type to the passed in order type', inject(function(){
            expect($scope.orderType).toEqual('bid');
        }));

        it('should set the order type on setOrderType', inject(function(){
            $scope.setOrderType('offer');
            expect($scope.orderType).toEqual('offer');
        }));

        it('exit should call close on modal instance', inject(function(){
            spyOn(mockModalInstance, 'dismiss');
            $scope.exit();
            expect(mockModalInstance.dismiss).toHaveBeenCalled();
        }));

        it("should allow you to execute an order", function(){
            $scope.executeOrder();
            expect(mockUserOrderService.sendExecuteOrderMessage.called).toEqual(true);
        });

        it("should set execute enabled to be false", function(){
            $scope.onPrincipalChange(null);
            expect($scope.executeEnabled).toBe(false);
            expect($scope.selectedPrincipal).toBe(null);
        });

        it("should set null for null broker", function(){
            $scope.onBrokerChange(null);
            $scope.executeOrder();
            expect($scope.order.brokerOrganisationId).not.toBeDefined();
            //expect($scope.order.brokerId).not.toBeDefined();
            expect($scope.order.brokerName).not.toBeDefined();
        });

        it("should set null for null principal", function(){
            $scope.onPrincipalChange(null);
            expect($scope.order.brokerOrganisationId).not.toBeDefined();
            //expect($scope.order.brokerId).not.toBeDefined();
            expect($scope.order.brokerName).not.toBeDefined();
        });

        it("should not be able to change the broker", function(){
            var result = $scope.ShowExecutingBroker();
            expect(result).toBe(false);
        });

        it('Executing broker is not selectable', function () {
            var result = $scope.CanSelectExecutingBroker();
            expect(result).toEqual(false);
        });

        it("should return orderHasPrincipal as true", function(){

            var result = $scope.orderHasPrincipal();

            expect(result).toBe(true);
        });

        it("should call modal dismiss on state change to cancel", function(){

            spyOn(mockModalInstance,'dismiss');

            $scope.onStateChangeSuccess();

            expect(mockModalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

        it('should return princLabel1 as Buyer when bid', function(){

            $scope.setOrderType('Bid');

            var result = $scope.princLabel1();

            expect(result).toBe('Buyer');
        });

        it('should return princLabel1 as Seller when offer', function(){

            $scope.setOrderType('Offer');

            var result = $scope.princLabel1();

            expect(result).toBe('Seller');
        });

        it('should return princLabel2 as Seller when bid', function(){

            $scope.setOrderType('Bid');

            var result = $scope.princLabel2();

            expect(result).toBe('Seller');
        });

        it('should return princLabel2 as Buyer when bid', function(){

            $scope.setOrderType('Offer');

            var result = $scope.princLabel2();

            expect(result).toBe('Buyer');
        });

        it('should return true for execute button disabled if the order is nontradeable', function(){

            var executeButtonDisabled;

            executeButtonDisabled = $scope.executeButtonDisabled();
            expect(executeButtonDisabled).toBeTruthy();
        });

        it('should return true for execute button disabled if the order is tradeable and the execute enabled is false', function(){

            var executeButtonDisabled;

            $scope.executeEnabled = false;
            executeButtonDisabled = $scope.executeButtonDisabled();
            expect(executeButtonDisabled).toBeTruthy();
        });

        it('should return false for execute button disabled if the order is tradeable and the execute enabled is true', function(){

            var executeButtonDisabled;

            $scope.executeEnabled = true;
            executeButtonDisabled = $scope.executeButtonDisabled();
            expect(executeButtonDisabled).toBeTruthy();
        });

        it('should set order to updated when the order that is being edited is updated by someone else', function(){

            expect($scope.orderChanged).toEqual(false);

            var orderMessage = {
                order: {
                    id: 100
                }
            };

            editOrderCallbackOrderUpdated(orderMessage);
            expect($scope.orderChanged).toEqual(true);
        });

        it('should not set order to updated when a different order is updated by someone', function(){

            expect($scope.orderChanged).toEqual(false);

            var orderMessageNotMatching = {
                order: {
                    id: 101
                }
            };

            editOrderCallbackOrderUpdated(orderMessageNotMatching);
            expect($scope.orderChanged).toEqual(false);
        });

        it('should set order deleted to true if the order is killed by someone else', function(){

            expect($scope.orderDeleted).toEqual(false);
            editOrderCallbackOrderDeleted("anything/for/diffusion/topic/last/id/100");
            expect($scope.orderDeleted).toEqual(true);
        });

        it('refreshing the execute order dialog should reset order Changed to false as weve pulled in the updated order', function(){

            expect($scope.orderChanged).toEqual(false);

            var orderMessageMatching = {
                order: {
                    id: 100
                }
            };

            editOrderCallbackOrderUpdated(orderMessageMatching);
            expect($scope.orderChanged).toEqual(true);

            $scope.refreshOrder();
            expect($scope.orderChanged).toEqual(false);
        });

        it('refreshing the order after someone has changed it should reflect metadata values', function () {

            var orderMessage = {
                order: {
                    id: 100,
                    price: 10,
                    metaData: [{
                        productMetaDataId: 1,
                        displayName: "Delivery Port",
                        displayValue: "Port C",
                        itemValue: 102,
                        metaDataListItemId: 3,
                        itemType:"IntegerEnum"
                    },{
                        productMetaDataId:3,
                        displayName:"Delivery Description",
                        displayValue:"abcdefg",
                        itemValue:null,
                        metaDataListItemId:null,
                        itemType:"String"
                    }]
                }
            };

            editOrderCallbackAskUpdated(orderMessage);

            $scope.refreshOrder();
            var expectedMetadataCollection = [{
                definition: metadataDefinitions[0],
                metadataitem: orderMessage.order.metaData[0]
            }, {
                definition: metadataDefinitions[1],
                metadataitem: orderMessage.order.metaData[1]
            }];

            expect($scope.order.metaData).toEqual(orderMessage.order.metaData);
            expect($scope.metaDataCollection).toEqual(expectedMetadataCollection);
        });

        it('calling removeCoBrokeredBrokerOnOriginalOrder when the order has no creation broker id should return the same set of brokers', function(){

            var brokers = [{"id":5,"name":"BrokerCo #1","organisationType":"Brokerage","shortCode":"BR1","legalName":"BrokerCo #1 Ltd","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-01-01T00:00:00Z","description":"","isDeleted":false,"nameAndCode":"BrokerCo #1 (BR1)"},{"id":6,"name":"BrokerCo #2","organisationType":"Brokerage","shortCode":"BR2","legalName":"BrokerCo #2 Ltd","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-01-01T00:00:00Z","description":"","isDeleted":false,"nameAndCode":"BrokerCo #2 (BR2)"},{"id":64,"name":"Zulu Brokerage","organisationType":"Brokerage","shortCode":"ZUL","legalName":"Zulu Brokerage","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-12-12T14:48:06.816Z","description":"","isDeleted":false,"nameAndCode":"Zulu Brokerage (ZUL)"},{"id":65,"name":"Victor Brokerage","organisationType":"Brokerage","shortCode":"VIC","legalName":"Victor Brokerage","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-12-12T14:49:30.832Z","description":"","isDeleted":false,"nameAndCode":"Victor Brokerage (VIC)"},{"id":999,"name":"Whisky Trading","organisationType":"Brokerage","shortCode":"WHI","legalName":"Whisky Trading","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-12-12T14:17:30.589Z","description":"","isDeleted":false,"nameAndCode":"Whisky Trading (WHI)"}],
                originalBrokersLength = brokers.length;

            var newBrokers = $scope.removeCoBrokeredBrokerOnOriginalOrder(brokers, -1);

            expect(newBrokers.length).toEqual(originalBrokersLength);
        });

        it('calling removeCoBrokeredBrokerOnOriginalOrder when the order has a broker id should return the list of brokers with the original broker id removed from the list', function(){

            var brokers = [{"id":5,"name":"BrokerCo #1","organisationType":"Brokerage","shortCode":"BR1","legalName":"BrokerCo #1 Ltd","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-01-01T00:00:00Z","description":"","isDeleted":false,"nameAndCode":"BrokerCo #1 (BR1)"},{"id":6,"name":"BrokerCo #2","organisationType":"Brokerage","shortCode":"BR2","legalName":"BrokerCo #2 Ltd","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-01-01T00:00:00Z","description":"","isDeleted":false,"nameAndCode":"BrokerCo #2 (BR2)"},{"id":64,"name":"Zulu Brokerage","organisationType":"Brokerage","shortCode":"ZUL","legalName":"Zulu Brokerage","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-12-12T14:48:06.816Z","description":"","isDeleted":false,"nameAndCode":"Zulu Brokerage (ZUL)"},{"id":65,"name":"Victor Brokerage","organisationType":"Brokerage","shortCode":"VIC","legalName":"Victor Brokerage","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-12-12T14:49:30.832Z","description":"","isDeleted":false,"nameAndCode":"Victor Brokerage (VIC)"},{"id":999,"name":"Whisky Trading","organisationType":"Brokerage","shortCode":"WHI","legalName":"Whisky Trading","address":"","email":"catchall@argus-labs.com","dateCreated":"2014-12-12T14:17:30.589Z","description":"","isDeleted":false,"nameAndCode":"Whisky Trading (WHI)"}],
                originalBrokersLength = brokers.length;

            var newBrokers = $scope.removeCoBrokeredBrokerOnOriginalOrder(brokers, 5);

            expect(newBrokers.length).not.toEqual(originalBrokersLength);
        });

        describe('As a principal executing order', function() {
            describe('with just a principal on order with no-broker bilateral restriction', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock2WayOnlyOrder = angular.copy(mockOrder);
                    mock2WayOnlyOrder.brokerDisplayName = 'No Broker';
                    mock2WayOnlyOrder.brokerOrganisationId = null;
                    mock2WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock2WayOnlyOrder,
                            userService: mockUserService,
                            userApi: mockUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });
                }));

                it('Cannot be 4way', function () {

                    expect($scope.IsBroker()).toEqual(false);
                    expect($scope.coBrokeredOrder).toEqual(false);

                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(false);
                });

                it('Broker on initial order is not shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(false);
                });

                it('Executing broker is not shown', function () {
                    expect($scope.IsBroker()).toEqual(false);//make sure we are a principal
                    var result = $scope.ShowExecutingBroker();
                    expect(result).toEqual(false);
                });

                it('Cannot select principal (as I am the principal executing)', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(false);
                });

                it('Can Execute order', function () {
                    expect($scope.executeEnabled).toBe(true);
                });

            });

            describe('with just a principal on order with Any Broker permitted', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock2WayOnlyOrder = angular.copy(mockOrder);
                    mock2WayOnlyOrder.brokerDisplayName = 'Any Broker';
                    mock2WayOnlyOrder.brokerOrganisationId = null;
                    mock2WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock2WayOnlyOrder,
                            userService: mockUserService,
                            userApi: mockUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });
                }));

                it('Cannot be 4way', function () {

                    expect($scope.IsBroker()).toEqual(false);
                    expect($scope.coBrokeredOrder).toEqual(false);

                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(false);
                });

                it('Broker on initial order not shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(false);
                });

                it('Executing broker is shown', function () {
                    expect($scope.IsBroker()).toEqual(false);//make sure we are a principal
                    var result = $scope.ShowExecutingBroker();
                    expect(result).toEqual(true);
                });

                it('Executing broker is selectable', function () {
                    var result = $scope.CanSelectExecutingBroker();
                    expect(result).toEqual(true);
                });

                it('Cannot select principal (as I am the principal executing)', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(false);
                });

                it('Can Execute order with no broker selected', function () {
                    expect($scope.selectedBroker).toBeUndefined();
                    expect($scope.executeEnabled).toBe(true);
                });

                describe('with broker selected', function () {
                    beforeEach(inject(function ($controller, $rootScope) {

                        var mockBroker = {id: 1234};
                        $scope.onBrokerChange(mockBroker);
                        $scope.$digest();
                    }));

                    it('Broker is selected', function () {
                        expect($scope.selectedBroker).toBeDefined();
                        expect($scope.selectedBroker.id).toBe(1234);
                    });

                    it('Can Execute order', function () {
                        expect($scope.executeEnabled).toBe(true);
                    });
                });
            });

            describe('with principal and broker named on order', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock3WayOnlyOrder = angular.copy(mockOrder);
                    mock3WayOnlyOrder.brokerDisplayName = 'Broker Name';
                    mock3WayOnlyOrder.brokerOrganisationId = 2;
                    mock3WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock3WayOnlyOrder,
                            userService: mockUserService,
                            userApi: mockUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });
                }));

                it('Cannot be 4way', function () {
                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(false);
                });

                it('Broker on initial order is shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(true);
                });

                it('Executing broker is not shown', function () {
                    expect($scope.IsBroker()).toEqual(false);//make sure we are a principal
                    var result = $scope.ShowExecutingBroker();
                    expect(result).toEqual(false);
                });

                it('Cannot select principal (as I am the principal executing)', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(false);
                });

                it('Can Execute order', function () {
                    expect($scope.executeEnabled).toBe(true);
                });

            });

            describe('with principal and broker named on co-brokerable order', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock3WayOnlyOrder = angular.copy(mockOrder);
                    mock3WayOnlyOrder.brokerDisplayName = 'Broker Name';
                    mock3WayOnlyOrder.brokerOrganisationId = 2;
                    mock3WayOnlyOrder.coBrokering = true;
                    mock3WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock3WayOnlyOrder,
                            userService: mockUserService,
                            userApi: mockUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });
                }));

                it('Can be a 4way', function () {
                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(true);
                });

                it('Broker on initial order is shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(true);
                });

                it('Executing broker is shown', function () {
                    expect($scope.IsBroker()).toEqual(false);//make sure we are a principal
                    var result = $scope.ShowExecutingBroker();
                    expect(result).toEqual(true);
                });

                it('Executing broker is selectable', function () {
                    var result = $scope.CanSelectExecutingBroker();
                    expect(result).toEqual(true);
                });

                it('Cannot select principal (as I am the principal executing)', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(false);
                });

                it('Can Execute order', function () {
                    expect($scope.executeEnabled).toBe(true);
                });
            });
        });

        describe('As a broker executing order', function() {
            describe('with just a principal on order with no-broker bilateral restriction', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock2WayOnlyOrder = angular.copy(mockOrder);
                    mock2WayOnlyOrder.brokerDisplayName = 'No Broker';
                    mock2WayOnlyOrder.brokerOrganisationId = -1;
                    mock2WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock2WayOnlyOrder,
                            userService: mockBrokerUserService,
                            userApi: mockBrokerUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });
                }));

                it('Cannot be 4way', function () {

                    expect($scope.IsBroker()).toEqual(true);
                    expect($scope.coBrokeredOrder).toEqual(false);

                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(false);
                });

                it('Broker on initial order is not shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(false);
                });

                it('Executing broker is not shown', function () {
                    expect($scope.IsBroker()).toEqual(true);
                    var result = $scope.ShowExecutingBroker();
                    expect(result).toEqual(false);
                });

                it('Cannot select principal', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(false);
                });

                it('Cannot Execute order as order say no brokers', function () {
                    expect($scope.executeEnabled).toBe(false);
                });
            });

            describe('with just a principal on order with Any Broker permitted', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock2WayOnlyOrder = angular.copy(mockOrder);
                    mock2WayOnlyOrder.brokerDisplayName = 'Any Broker';
                    mock2WayOnlyOrder.brokerOrganisationId = null;
                    mock2WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock2WayOnlyOrder,
                            userService: mockBrokerUserService,
                            userApi: mockBrokerUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });
                }));

                it('Cannot be 4way', function () {

                    expect($scope.IsBroker()).toEqual(true);
                    expect($scope.coBrokeredOrder).toEqual(false);

                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(false);
                });

                it('Broker on initial order is not shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(false);
                });

                it('Executing broker shown', function () {
                    expect($scope.IsBroker()).toEqual(true);
                    var result = $scope.ShowExecutingBroker();
                    expect(result).toEqual(true);
                });

                it('Executing broker is not selectable (as you are broker)', function () {
                    var result = $scope.CanSelectExecutingBroker();
                    expect(result).toEqual(false);
                });

                it('Can select principal', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(true);
                });

                it('Cannot Execute order (no principal selected yet)', function () {
                    expect($scope.executeEnabled).toBe(false);
                });

                describe('with principal selected', function () {
                    beforeEach(inject(function ($controller, $rootScope) {
                        expect($scope.CanSelectAPrincipal()).toEqual(true);

                        //select a principal
                        var mockPrincipal = {id: 1234};
                        $scope.onPrincipalChange(mockPrincipal);
                        $scope.$digest();
                    }));

                    it('Can Execute order', function () {
                        expect($scope.executeEnabled).toBe(true);
                    });
                });
            });


            describe('with principal and broker named on order', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock3WayOnlyOrder = angular.copy(mockOrder);
                    mock3WayOnlyOrder.brokerDisplayName = 'Broker Name';
                    mock3WayOnlyOrder.brokerOrganisationId = 2;
                    mock3WayOnlyOrder.coBrokering = false;
                    mock3WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock3WayOnlyOrder,
                            userService: mockBrokerUserService,
                            userApi: mockBrokerUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });
                }));

                it('Cannot be 4way', function () {
                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(false);
                });

                it('Broker on initial order is shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(true);
                });

                it('Executing broker is not shown (as you are a different broker and it is not cobrokerable)', function () {
                    expect($scope.IsBroker()).toEqual(true);
                    var result = $scope.ShowExecutingBroker();
                    expect(result).toEqual(false);
                });

                it('Cannot select principal', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(false);
                });

                it('Cannot Execute order as I am a broker, broker on order and cobroker=false', function () {
                        expect($scope.executeEnabled).toBe(false);
                    });
            });

            describe('with principal and other broker named on co-brokerable order', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock3WayOnlyOrder = angular.copy(mockOrder);
                    mock3WayOnlyOrder.brokerDisplayName = 'Broker Name';
                    mock3WayOnlyOrder.brokerOrganisationId = 2;
                    mock3WayOnlyOrder.coBrokering = true;
                    mock3WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock3WayOnlyOrder,
                            userService: mockBrokerUserService,
                            userApi: mockBrokerUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });

                }));

                it('Can be a 4way', function () {
                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(true);
                });

                it('Broker on initial order is shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(true);
                });

                it('Executing broker shown', function () {

                    var result = $scope.ShowExecutingBroker();
                    expect(result).toEqual(true);
                });

                it('Cant change executing broker (as we are the broker)', function () {
                    var result = $scope.CanSelectExecutingBroker();
                    expect(result).toEqual(false);
                });

                it('Can select principal', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(true);
                });

                it('Cannot Execute order (no principal selected)', function () {
                        expect($scope.executeEnabled).toBe(false);
                    });

                describe('with principal selected', function () {
                    beforeEach(inject(function ($controller, $rootScope) {
                        expect($scope.CanSelectAPrincipal()).toEqual(true);

                        //select a principal
                        var mockPrincipal = {id: 1234};
                        $scope.onPrincipalChange(mockPrincipal);
                        $scope.$digest();
                    }));

                    it('Can Execute order', function () {
                        expect($scope.executeEnabled).toBe(true);
                    });
                });
            });

            describe('with principal and MY brokerage named on order', function () {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();

                    var mock3WayOnlyOrder = angular.copy(mockOrder);
                    mock3WayOnlyOrder.brokerDisplayName = userOrganisationBrokerage.name;
                    mock3WayOnlyOrder.brokerOrganisationId = userOrganisationBrokerage.id ;
                    mock3WayOnlyOrder.coBrokering = false;
                    mock3WayOnlyOrder.nonTradeable=false;

                    thisController = $controller('executeOrderController',
                        {
                            $scope: $scope,
                            $state: mockState,
                            userOrderService: mockUserOrderService,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            orderType: mockOrderType,
                            productId: 1,
                            order: mock3WayOnlyOrder,
                            userService: mockBrokerUserService,
                            userApi: mockBrokerUserApi,
                            productMessageSubscriptionService: mockProductMessageSubscriptionService
                        });
                }));

                it('Cannot be 4way', function () {
                    var result = $scope.WillBeFourWayDeal();
                    expect(result).toEqual(false);
                });

                it('Broker on initial order is shown', function () {
                    var result = $scope.ShowBrokerOnInitialOrder();
                    expect(result).toEqual(true);
                });

                it('Executing broker is not shown (as you are already named broker)', function () {
                    expect($scope.IsBroker()).toEqual(true);
                    
                    expect($scope.ShowExecutingBroker()).toEqual(false);
                });

                it('Can select principal', function () {
                    expect($scope.CanSelectAPrincipal()).toEqual(true);
                });

                it('Cannot Execute order (no principal selected)', function () {
                    expect($scope.executeEnabled).toBe(false);
                });

                describe('with principal selected', function () {
                    beforeEach(inject(function ($controller, $rootScope) {
                        expect($scope.CanSelectAPrincipal()).toEqual(true);

                        //select a principal
                        var mockPrincipal = {id: 1234};
                        $scope.onPrincipalChange(mockPrincipal);
                        $scope.$digest();
                    }));

                    it('Can Execute order', function () {
                        expect($scope.executeEnabled).toBe(true);
                    });
                });
            });

        });
    });
});