﻿angular.module('argusOpenMarkets.dashboard')
    .controller('authExternalDealController', function authExternalDealController(
        $scope,
        $modalInstance,
        userApi,
        $state,
        userService,
        dealService,
        productConfigurationService,
        $filter,
        deal,
        userContactDetails,
        productMetadataService) {
        "use strict";

        $scope.formChanged = false;
        $scope.deal = deal;
        $scope.userContactDetails = userContactDetails;
        $scope.deal.deliveryStartDate = new Date(Date.parse($scope.deal.deliveryStartDate));
        $scope.deal.deliveryEndDate = new Date(Date.parse($scope.deal.deliveryEndDate));

        $scope.dealUsesCustomFreeFormNotes = deal.useCustomFreeFormNotes;
        $scope.deal.metaData = productMetadataService.synchronizeMetadata($scope.deal.metaData, $scope.deal.productId);
        var metadataPool = {};
        metadataPool[$scope.deal.productId] = $scope.deal.metaData;
        $scope.metaDataCollection = productMetadataService.getMetadataCollection($scope.deal.metaData, $scope.deal.productId);

        $scope.$on('$stateChangeSuccess', function () {
            if ($state.current.name !== 'Dashboard') {
                $modalInstance.dismiss('cancel');
            }
        });

        $scope.itemdisabled = function(){
            return false;
        };

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.canCreateVerified = function () {
            if (!$scope.selectedContract) {
                return false;
            }
            var createVerifiedDealPermissions = ["Deal_Authenticate"];
            return userService.hasProductPrivilege($scope.selectedContract.productId, createVerifiedDealPermissions);
        };

        $scope.canViewFreeFormNotes = function(){
            var createVerifiedDealPermissions = ["Deal_Authenticate"];
            return userService.hasProductPrivilegeOnAnyProduct(createVerifiedDealPermissions);
        };

        $scope.getPrincipals = function (name) {
            return userApi.getPrincipals(name);
        };

        $scope.getBrokers = function (name) {
            return userApi.getBrokers(name);
        };


        $scope.selectContract = function (contract) {

            $scope.quantityLabel = "";
            $scope.priceLabel = "";
            if (contract) {
                $scope.selectedContract = contract;
                $scope.deal.productId = contract.productId;
                $scope.quantityLabel = "(" + $scope.selectedContract.contractSpecification.volume.volumeUnitCode + ")";
                $scope.priceLabel = "(" + $scope.selectedContract.contractSpecification.pricing.pricingCurrencyCode + "/" + $scope.selectedContract.contractSpecification.pricing.pricingUnitCode + ")";
                $scope.deal.metaData = productMetadataService.getMetadataValuesFromPool(metadataPool, $scope.deal.productId);
                $scope.metaDataCollection = productMetadataService.getMetadataCollection($scope.deal.metaData, $scope.deal.productId);
            }
        };

        $scope.overrideFreeForm = function () {
            $scope.deal.useCustomFreeFormNotes = !!$scope.deal.freeFormDealMessage;
        };

        $scope.contractNameCharacterLimit = 100;
        $scope.contractNameCharactersLeft = $scope.contractNameCharacterLimit -  ($scope.deal.contractInput ? $scope.deal.contractInput.length : 0);
        $scope.onContractNameChange = function(){
            if ($scope.deal.contractInput) {
                if ($scope.deal.contractInput.length > $scope.contractNameCharacterLimit){
                    $scope.deal.contractInput = $scope.deal.contractInput.slice(0, $scope.contractNameCharacterLimit);
                }
                $scope.contractNameCharactersLeft = $scope.contractNameCharacterLimit - $scope.deal.contractInput.length;
            }
            else {
                $scope.contractNameCharactersLeft = $scope.contractNameCharacterLimit;
            }
        };

        $scope.showOptionalPriceDetail = function(){
            if ($scope.selectedContract && $scope.selectedContract.displayOptionalPriceDetail){
                return true;
            }
            else{
                return false;
            }
        };

        $scope.notesCharacterLimit = 250;
        $scope.notesCharactersLeft = $scope.notesCharacterLimit;
        $scope.onNotesChange = function(){
            if (!$scope.deal.notes) {
                $scope.deal.notes = "";
            }
            if ($scope.deal.notes && $scope.deal.notes.length > $scope.notesCharacterLimit){
                $scope.deal.notes = $scope.deal.notes.slice(0, $scope.notesCharacterLimit);
            }
            $scope.notesCharactersLeft = $scope.notesCharacterLimit - $scope.deal.notes.length;
        };
        $scope.onNotesChange();

        $scope.createMessage = function (form) {
            var valid = !form.$error.required && !form.$invalid;
            if (valid) {
                if (!$scope.deal.useCustomFreeFormNotes) {
                    var deal = $scope.deal;
                    var freeFormDeal =
                        dealService.createFreeFormNotes(deal, $scope.selectedContract, $filter('date'), $filter('number'));

                    if(freeFormDeal !== $scope.deal.freeFormDealMessage){
                        $scope.formChanged = true;
                    }

                    $scope.deal.freeFormDealMessage = freeFormDeal;
                }
            }
        };

        $scope.setBroker = function (broker) {
            $scope.deal.brokerId = broker.id;
        };

        $scope.setSeller = function (seller) {
            $scope.deal.sellerId = seller.id;
        };

        $scope.setBuyer = function (buyer) {
            $scope.deal.buyerId = buyer.id;
        };

        var contracts = [];
        angular.forEach(userService.getProductsWithPrivileges(["Deal_Authenticate"]), function (id) {
            var contract = productConfigurationService.getProductConfigurationById(id);
            contracts.push(contract);
            if (deal.productId === id) {
                $scope.selectedContract = contract;
                $scope.deal.productId = contract.productId;
                $scope.quantityLabel = "(" + $scope.selectedContract.contractSpecification.volume.volumeUnitCode + ")";
                $scope.priceLabel = "(" + $scope.selectedContract.contractSpecification.pricing.pricingCurrencyCode + "/" + $scope.selectedContract.contractSpecification.pricing.pricingUnitCode + ")";
            }
        });

        $scope.contracts = contracts;

        var today = new Date();
        $scope.minDate = new Date(Math.min( today.addDays(1), $scope.deal.deliveryStartDate));
        var maxdate = new Date();
        $scope.maxDate = maxdate.addDays(30);
        var startDay = $scope.deal.deliveryStartDate;
        var endDay = $scope.deal.deliveryStartDate;
        $scope.minRange = 5;

        $scope.verifyDeal = function () {
            dealService.sendVerifyExternalDealMessage($scope.deal);
            $modalInstance.dismiss('submitted');
        };
        $scope.updateDeal = function (asVerified) {
            $scope.deal.dealStatus = 'Pending'; //Effectively verifies the update.
            dealService.sendUpdateExternalDealMessage($scope.deal);
            $modalInstance.dismiss('submitted');
        };

        $scope.updateButtonDisabled = function(){

            var buttonDisabled = true;

            if($scope.formChanged){
                return false;
            }

            return (($scope.authExternalDealForm.$error.required || $scope.authExternalDealForm.$invalid) && !deal.useCustomFreeFormNotes) || $scope.authExternalDealForm.$pristine;
        };

        $scope.sendMail = function(){
            var link = "mailto:" + userContactDetails.email +
                    "?subject=Argus Open Markets - External Deal Query" +
                    "&body=To " + userContactDetails.name + "," +
                    "%0D%0A%0D%0ARegarding your report of an external deal to Argus Open Markets, (deal reference: " + deal.id + ")." +
                    "%0D%0A%0D%0AThe reported details were: '" + $scope.deal.freeFormDealMessage + "'." +
                    "%0D%0A%0D%0AQUERYHERE" +
                    "%0D%0A%0D%0ARegards" +
                    "%0D%0A%0D%0A" + userService.getUser().name
                ;
            window.sendingEmail = true;
            window.location.href = link;
        };

        $scope.voidingMode = false;
        $scope.voidReason =
        {
            reasonText: "",
            reasonSelection: ""
        };
        $scope.setVoidReason = function( reason ){
            $scope.voidReason.reasonSelection = reason;  // value was not being updated automatically.
        };

        $scope.canVoid = function(){
            if ($scope.voidReason.reasonSelection > ""){
                if ($scope.voidReason.reasonSelection === "other"){
                    if ($scope.voidReason.reasonText==="" || $scope.voidReason.reasonText === "Enter reason"){
                        return false;
                    }
                }
                return true;
            }
            return false;
        };

        $scope.voidDeal = function () {

            $scope.voidingMode=true;
        };
        $scope.voidConfirmation = function (confirm) {

            if(confirm){
                var vr = $scope.voidReason.reasonSelection === "other" ? "other: " + $scope.voidReason.reasonText : $scope.voidReason.reasonSelection;
                $scope.deal.freeFormDealMessage = $scope.deal.freeFormDealMessage + ". \nVOID reason was '" + vr + "'";

                dealService.sendVoidExternalDealMessage($scope.deal);
                $modalInstance.dismiss('submitted');
            } else {
                $scope.voidingMode = false;
            }

        };
    });