describe('marketInfo service tests', function() {
    "use strict";

    var marketInfoServiceTest;
    var mockUserService;

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(function(){
        mockUserService = jasmine.createSpyObj('mockUserService',
            ['sendStandardFormatMessageToUsersTopic', 'hasProductPrivilegeOnAnyProduct']);
    });

    beforeEach(function(){
        module(function($provide) {
            $provide.value('userService', mockUserService);
        });
    });

    beforeEach(inject(function(marketInfoService){
        marketInfoServiceTest = marketInfoService;
    }));

    it('calls sendStandardFormatMessageToUsersTopic for new info', function(){
        marketInfoServiceTest.sendNewMarketInfoMessage("blah");
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith( 'Info', 'Create', "blah");
    });

    it('calls sendStandardFormatMessageToUsersTopic for update info', function(){
        marketInfoServiceTest.sendUpdateMarketInfoMessage("blah");
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith( 'Info', 'Update', "blah");
    });

    it('calls sendStandardFormatMessageToUsersTopic for verify info', function(){
        marketInfoServiceTest.sendVerifyMarketInfoMessage("blah");
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith( 'Info', 'Verify', "blah");
    });

    it('calls sendStandardFormatMessageToUsersTopic for void info', function(){
        marketInfoServiceTest.sendVoidMarketInfoMessage("blah");
        expect(mockUserService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith( 'Info', 'Void', "blah");
    });

    it('should call user service for canCreateVerified', function() {
        mockUserService.hasProductPrivilegeOnAnyProduct.and.returnValue(false);
        expect(marketInfoServiceTest.canCreateVerified()).toBe(false);
        expect(mockUserService.hasProductPrivilegeOnAnyProduct).toHaveBeenCalled();
        mockUserService.hasProductPrivilegeOnAnyProduct.and.returnValue(true);
        expect(marketInfoServiceTest.canCreateVerified()).toBe(true);
    });

    it('should return only the products that have been ticked from getTickedProductIds', function() {
        var fakeProducts = [
            {productId:1, ticked:true},
            {productId:2, ticked:false},
            {productId:3},
            {productId:4, ticked:true}
        ];
        expect(marketInfoServiceTest.getTickedProductIds(fakeProducts)).toEqual([1, 4]);
    });

    it('should return an empty array for ticked products if products argument is falsey', function() {
        expect(marketInfoServiceTest.getTickedProductIds(null)).toEqual([]);
    });

    describe('processMarketInfoText tests', function() {
        var characterLimit = 20;

        it('should have infoValid as false if info text is empty', function() {
            var text ='';
            var res = marketInfoServiceTest.processMarketInfoText(text, characterLimit);
            expect(res.infoValid).toBe(false);
        });
        it('should have characters left equal to character limit for empty text', function() {
            var text ='';
            var res = marketInfoServiceTest.processMarketInfoText(text, characterLimit);
            expect(res.charactersLeft).toEqual(characterLimit);
        });
        it('should have infoValid==false and charactersLeft==limit if info text is null or undefined', function() {
            var testValues = ['', null, undefined];
            for (var i = 0; i < testValues.length; ++i) {
                var text =testValues[i];
                var res = marketInfoServiceTest.processMarketInfoText(text, characterLimit);
                expect(res.infoValid).toBe(false);
                expect(res.charactersLeft).toEqual(characterLimit);
            };
        });
        it('should reduce characters left as the info text gets longer', function() {
            var text ='a';
            var res = marketInfoServiceTest.processMarketInfoText(text, characterLimit);
            expect(res.charactersLeft).toEqual(characterLimit-1);
            expect(res.text).toEqual(text);
            text ='aa';
            res = marketInfoServiceTest.processMarketInfoText(text, characterLimit);
            expect(res.charactersLeft).toEqual(characterLimit-2);
            expect(res.text).toEqual(text);
        });
        it('should say a non-empty info text is valid', function() {
            var text ='a';
            var res = marketInfoServiceTest.processMarketInfoText(text, characterLimit);
            expect(res.infoValid).toBe(true);
        });
        it('should say an info text of just spaces is not valid', function() {
            var text ='      ';
            var res = marketInfoServiceTest.processMarketInfoText(text, characterLimit);
            expect(res.infoValid).toBe(false);
        });
        it('should truncate an overlong info text to the character limit', function() {
            var longString = "123456789";
            while (longString.length <= characterLimit) {
                longString += longString;
            }
            var text =longString;
            var res = marketInfoServiceTest.processMarketInfoText(text, characterLimit);
            expect(res.text.length).toBe(characterLimit);
            expect(res.charactersLeft).toBe(0);
            expect(res.infoValid).toBe(true);
        });
    });

});



