﻿angular.module('argusOpenMarkets.dashboard').service("mockNewsService", function mockNewsService(){
    "use strict";

    var getNewsStory = function(id){
        var newsItem = {
            "cmsId":12345,
            "headline":"Falling naphtha prices return profitability to heavy-feed crackers globally",
            "publicationDate":"2014-05-14T08:54:09.815756",
            "itemUrl":"https://direct.argusmedia.com/newsandanalysis/article/897182",
            "story":"Houston, 31 April (Argus) — Heavy and full range N+A naphtha differentials to Gulf coast waterborne unleaded (GC WBM UNL) 9 RVP M2 gasoline were steady to weaker in sporadic trading interest Monday. Heavy and full range N+A naphtha differentials to Gulf coast waterborne unleaded (GC WBM UNL) 9 RVP M2 gasoline were steady to weaker in sporadic trading interest Monday. Heavy and full range N+A naphtha differentials to Gulf coast waterborne unleaded (GC WBM UNL) 9 RVP M2 gasoline were steady to weaker in sporadic trading interest Monday. Heavy and full range N+A naphtha differentials to Gulf coast waterborne unleaded (GC WBM UNL) 9 RVP M2 gasoline were steady to weaker in sporadic trading interest Monday.",
            "commodityId":1,
            "newsType":"News",
            "sectorId":-1,
            "sector":"sector",
            "isFree":false,
            "contentStreams": [],
            "aomPrimaryContentStream": "aomPrimaryContentStream"};

        return newsItem;
    };

    var transform = function(news) {
        news.expanded = false;
        news.publicationDate = new Date(Date.parse(news.publicationDate));
        return news;
    };

    var subscribeToNewsAlerts = function(){};

    var getNews = function(){
        return getNewsStory();
    };

    var updateAllFormattedDates = function() {};

    return {
        getNewsStory:getNewsStory,
        transform:transform,
        getNews:getNews,
        subscribeToNewsAlerts:subscribeToNewsAlerts,
        updateAllFormattedDates:updateAllFormattedDates
    };
});