﻿angular.module('argusOpenMarkets.dashboard')
    .controller('inputAssessmentController', function newInputAssessmentController($scope,
                                                                                   $modalInstance,
                                                                                   assessmentService,
                                                                                   $state,
                                                                                   userService,
                                                                                   productConfigurationService,
                                                                                   assessment,
                                                                                   productValidationService) {
        $scope.assessmentPeriods = ["Today", "Previous"];
        $scope.selectedAssessmentPeriod = "Today";

        $scope.$on('$stateChangeSuccess', function () {
            if ($state.current.name !== 'Dashboard') {
                $modalInstance.dismiss('cancel');
            }
        });

        $scope.clearAssessmentValue = function () {

            var clearAssessment = {};
            clearAssessment.Period = $scope.selectedAssessmentPeriod;
            clearAssessment.productId = $scope.assessment.assessment.productId;
            clearAssessment.enteredByUserId = $scope.assessment.assessment.enteredByUserId;
            clearAssessment.lastUpdatedUserId = userService.getUser().id;

            assessmentService.sendClearAssessment(clearAssessment);

            $modalInstance.dismiss('submitted');
        };

        $scope.showClearAssessmentButton = function () {

            if ($scope.selectedAssessmentPeriod === "") {
                return false;
            }

            return true;
        };

        $scope.$watch("selectedAssessmentPeriod", function () {
            $scope.clearAssessmentValueButtonLabel = "Clear " + $scope.selectedAssessmentPeriod + " Assessment";
        });

        $scope.clearAssessmentValueButtonLabel = "Clear Assessment";
        $scope.assessment = assessment;
        $scope.disabled = true;

        $scope.lowInput = {low: ""};
        $scope.highInput = {high: ""};

        $scope.selectedAssessmentStatus = "";
        $scope.selectAssessmentStatus = function (status) {
            $scope.selectedAssessmentStatus = status;
        };

        $scope.assessmentStatusDisplayName = function (status) {
            return assessmentService.getAssessmentStatusDisplayName(status);
        };

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.getProductName = function (productId) {
            return productConfigurationService.getProductConfigurationById(productId).productName;
        };



        if ($scope.assessment.assessment.today === null || moment($scope.assessment.assessment.today.businessDate).date() === moment().date()) {
            $scope.alreadyRolled = true;
        }



        $scope.newAssesment = function () {
            // now
            var d = new Date();

            if ($scope.selectedAssessmentPeriod === "Previous") {
                d.setDate(d.getDate() - 1); // Set date to yesterday.
            }

            var curr_month = d.getMonth() + 1;
            var newBusinessDate = d.getFullYear() + "." + curr_month + "." + d.getDate();

            var newAssessment = angular.copy($scope.assessment);
            //old today is now previous
            if(newAssessment.assessment.today === null){
                if(newAssessment.assessment.previous !== null) {
                    newAssessment.assessment.today = newAssessment.assessment.previous;
                }else{
                    newAssessment.assessment.today = {"id": 0,
                        "enteredByUserId": null,
                        "lastUpdatedUserId": null,
                        "dateCreated": null,
                        "lastUpdated": null,
                        "priceHigh": null,
                        "priceLow": null,
                        "productId": newAssessment.assessment.productId,
                        "product": null,
                        "businessDate": null,
                        "trendFromPreviousAssesment": null,
                        "assessmentStatus": null};
                }
            }

            newAssessment.assessment.previous = null;
            //today
            newAssessment.assessment.today.priceLow = $scope.lowInput.low;
            newAssessment.assessment.today.priceHigh = $scope.highInput.high;
            newAssessment.assessment.today.businessDate = newBusinessDate;
            newAssessment.assessment.today.enteredByUserId = $scope.assessment.assessment.enteredByUserId;
            newAssessment.assessment.today.lastUpdatedUserId = userService.getUser().id;
            newAssessment.assessment.today.assessmentStatus = $scope.selectedAssessmentStatus;
            newAssessment.assessment.today.lastUpdated = new Date();
            newAssessment.assessment.today.dateCreated = new Date();

            newAssessment.message.messageAction = "Create";
            assessmentService.sendNewAssessment(newAssessment.assessment);

            $modalInstance.dismiss('submitted');
        };

        $scope.rollAssesment = function () {
            assessment = {
                'productId': $scope.assessment.productId,
                'lastUpdatedUserId': userService.getUser().id

            };
            assessmentService.sendRefreshAssessment(assessment);
            $modalInstance.dismiss('submitted');
        };

        $scope.productConfig = productConfigurationService.getProductConfigurationById($scope.assessment.productId);
        var productConfigPrice = $scope.productConfig.contractSpecification.pricing;

        var priceViolatesStepping = function (floatPrice) {
            return productValidationService.violatesSteppingIncrement(
                floatPrice, productConfigPrice.increment);
        };

        $scope.invalidPriceReason = "";
        $scope.priceSpecification = {
            decimalPlaces: productConfigPrice.decimalPlaces,
            isValid: function (form, updatedPrice) {
                $scope.disabled = true;

                var floatPriceHigh = parseFloat($scope.highInput.high);
                var floatPriceLow = parseFloat($scope.lowInput.low);

                if (floatPriceHigh < floatPriceLow) {
                    $scope.invalidPriceReason = "Price high cannot be less than price low!";
                    form.lowPrice.$setValidity("pricerange", false);
                    form.highPrice.$setValidity("pricerange", false);
                    return;
                }
                else {
                    $scope.invalidPriceReason = "";
                    form.lowPrice.$setValidity("pricerange", true);
                    form.highPrice.$setValidity("pricerange", true);
                }

                var lowUpdated = updatedPrice === 'low';
                var highUpdated = !lowUpdated;

                if (lowUpdated && productConfigPrice.minimum > floatPriceLow) {
                    $scope.invalidPriceReason = "Price low must be greater than " + productConfigPrice.minimum;
                    form.lowPrice.$setValidity("price", false);
                }
                else if (highUpdated && floatPriceHigh > productConfigPrice.maximum) {
                    $scope.invalidPriceReason = "Price high must be less than " + productConfigPrice.maximum;
                    form.highPrice.$setValidity("price", false);
                }
                else if (highUpdated && priceViolatesStepping(floatPriceHigh)) {
                    $scope.invalidPriceReason = "Price high must be in increments of " + productConfigPrice.increment;
                    form.highPrice.$setValidity("price", false);
                }
                else if (lowUpdated && priceViolatesStepping(floatPriceLow)) {
                    $scope.invalidPriceReason = "Price low must be in increments of " + productConfigPrice.increment;
                    form.lowPrice.$setValidity("price", false);
                }
                else {
                    var price = lowUpdated ? form.lowPrice : form.highPrice;
                    price.$setValidity("price", true);
                    $scope.disabled = false;
                    $scope.invalidPriceReason = "";
                }
            }
        };

        var statuses = [];
        statuses.push("Final");
        statuses.push("RunningVwa");
        statuses.push("Corrected");
        $scope.statuses = statuses;

        $scope.selectAssessmentPeriod = function (period) {

            $scope.selectedAssessmentPeriod = period;
        };


    });
