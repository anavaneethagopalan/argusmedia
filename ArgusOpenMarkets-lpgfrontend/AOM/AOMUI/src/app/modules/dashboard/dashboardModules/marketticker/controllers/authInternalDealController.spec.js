﻿describe( 'argusOpenMarkets.dashboard.authInternalDealController', function() {
    "use strict";
    describe( 'authInternalDealController', function() {

        var $testController,
            $scope,
            authDealCntrl,
            mockUserApi = {},
            mockModal = {open: function ()
            {
                return true;
            }},
            metadataDefinitions = [{
                "valueMinimum": 0,
                "valueMaximum": 20,
                "id": 1,
                "productId": 2,
                "displayName": "Delivery Description",
                "fieldType": "String"
            }],
            mockProductConfigurationService = {
                getProductMetaDataById: function(productId) {
                    return { "productId": productId, "fields": metadataDefinitions };
                }
            },
            deal = {
                initial:{
                    deliveryStartDate:"",
                    deliveryEndDate:"",
                    productId:1,
                    metaData:[{
                        productMetaDataId:1,
                        displayName:"Initial order description",
                        displayValue:"",
                        itemValue:null,
                        metaDataListItemId:null,
                        itemType:"String"
                    }]
                },
                matching:{
                    metaData:[{
                        productMetaDataId:1,
                        displayName:"Matching order description",
                        displayValue:"",
                        itemValue:null,
                        metaDataListItemId:null,
                        itemType:"String"
                    }]
                }
            };

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(module( 'argusOpenMarkets.dashboard' ) );
        beforeEach(module(function($provide){
            $provide.value("productConfigurationService", mockProductConfigurationService);
        }));

        beforeEach(inject( function( $controller, $rootScope, mockProductConfigurationService,mockUserService, helpers ) {
            $scope = $rootScope.$new();
            $testController = $controller;

            authDealCntrl = $testController
            ( 'authInternalDealController',
                {
                    $scope: $scope,
                    $modalInstance: {},
                    userApi: mockUserApi,
                    $state: {},
                    userService: mockUserService,
                    dealService: {},
                    productConfigurationService:mockProductConfigurationService,
                    $filter: {},
                    deal: deal,
                    userContactDetails: {}
                });
        }));

        it('should be defined', function() {
            expect( authDealCntrl ).toBeDefined();
        });

        it('should init metadata collection from initial order', function() {
            expect($scope.metaDataCollection).toEqual([{
                definition: metadataDefinitions[0],
                metadataitem: deal.initial.metaData[0]
            }]);
        });
    });
});