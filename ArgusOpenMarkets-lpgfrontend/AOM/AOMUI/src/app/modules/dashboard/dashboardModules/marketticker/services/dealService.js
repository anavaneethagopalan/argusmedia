﻿angular.module('argusOpenMarkets.dashboard').service("dealService", function dealService(userService){
    "use strict";

    var sendNewExternalDealMessage = function(deal){
        userService.sendStandardFormatMessageToUsersTopic( 'ExternalDeal', 'Create', deal );
    };

    var sendUpdateExternalDealMessage = function(deal){
        userService.sendStandardFormatMessageToUsersTopic( 'ExternalDeal', 'Update', deal );
    };

    var sendVerifyExternalDealMessage = function(deal){
        userService.sendStandardFormatMessageToUsersTopic( 'ExternalDeal', 'Verify', deal );
    };

    var sendVoidExternalDealMessage = function(deal){
        userService.sendStandardFormatMessageToUsersTopic( 'ExternalDeal', 'Void', deal );
    };

    var sendUpdateInternalDealMessage = function(deal){
        userService.sendStandardFormatMessageToUsersTopic( 'Deal', 'Update', deal );
    };

    var sendVerifyInternalDealMessage = function(deal){
        userService.sendStandardFormatMessageToUsersTopic( 'Deal', 'Verify', deal );
    };

    var sendVoidInternalDealMessage = function(deal){
        userService.sendStandardFormatMessageToUsersTopic( 'Deal', 'Void', deal );
    };

    var isOtherContract = function(contractSpecification) {
        if (!contractSpecification) {
            return false;
        }
        // TODO MS - This should really check an isOther flag on the contract, only there isn't one ...
        return contractSpecification.productTitle &&
               contractSpecification.productTitle.toLowerCase().indexOf('other') > -1;
    };

    var createFreeFormNotes = function(deal, selectedContract, dateFilter, numberFilter) {

        var optionalPriceDetail = (deal.optionalPriceDetail === "") ? "" : " " + deal.optionalPriceDetail;
        var startDate = dateFilter(deal.deliveryStartDate, "dd/MMM/yyyy");
        var endDate = dateFilter(deal.deliveryEndDate, "dd/MMM/yyyy");
        var contractLabel = isOtherContract(selectedContract) ? deal.contractInput : selectedContract.productTitle;
        var brokerString = "";
        if (deal.brokerName) {
            brokerString = "/" + deal.brokerName;
        }

        var metaDataString = "";
        if(deal.metaData) {
            for (var i = 0; i < deal.metaData.length; i++) {
                metaDataString += ", " + deal.metaData[i].displayValue;
            }
        }

        var freeFormDeal = deal.buyerName + brokerString + " buys " + numberFilter(deal.quantity) + selectedContract.contractSpecification.volume.volumeUnitCode +
            " of " + contractLabel + " " + startDate + " - " + endDate + " from " + deal.sellerName + brokerString +
            " at " + selectedContract.contractSpecification.pricing.pricingCurrencyCode + numberFilter(deal.price, 2) + "/" + selectedContract.contractSpecification.pricing.pricingUnitCode +
            optionalPriceDetail + metaDataString;
        return freeFormDeal;
    };

    return {
        sendNewExternalDealMessage:sendNewExternalDealMessage,
        sendUpdateExternalDealMessage:sendUpdateExternalDealMessage,
        sendVerifyExternalDealMessage:sendVerifyExternalDealMessage,
        sendVoidExternalDealMessage:sendVoidExternalDealMessage,
        sendUpdateInternalDealMessage:sendUpdateInternalDealMessage,
        sendVerifyInternalDealMessage:sendVerifyInternalDealMessage,
        sendVoidInternalDealMessage:sendVoidInternalDealMessage,
        createFreeFormNotes:createFreeFormNotes
    };

});