﻿angular.module('argusOpenMarkets.dashboard')
    .controller('marketTickerController', function marketTickerController($scope,
                                                                          $rootScope,
                                                                          $modal,
                                                                          $timeout,
                                                                          $log,
                                                                          helpers,
                                                                          marketTickerService,
                                                                          userService,
                                                                          marketTickerMessageSubscriptionService,
                                                                          messagePopupsService,
                                                                          dashboardConfigurationService,
                                                                          midnightEventService,
                                                                          orderRepositoryService) {
        "use strict";
        $scope.openDialog = false;
        $scope.showPendingTitle = "ALL";
        $scope.showDeal = false;
        $scope.showPendingOnly = false;
        $scope.loading = false;
        $scope.marketTickerItems = [];

        var marketTickerRowsAndHeight = [
            {"tickerItems": 3, "numberRows": 14, "pixels": 129},
            {"tickerItems": 6, "numberRows": 22, "pixels": 249},
            {"tickerItems": 9, "numberRows": 30, "pixels": 369},
            {"tickerItems": 12, "numberRows": 38, "pixels": 488},
            {"tickerItems": 15, "numberRows": 46, "pixels": 609},
            {"tickerItems": 18, "numberRows": 53, "pixels": 716},
            {"tickerItems": 21, "numberRows": 63, "pixels": 837}
        ];

        $scope.setMtContainerHeight = function (numberMarketTickerItemsDisplayed) {

            var height = 248;

            angular.forEach(marketTickerRowsAndHeight, function (rowDimensions) {
                if (rowDimensions.tickerItems === numberMarketTickerItemsDisplayed) {
                    height = rowDimensions.pixels;
                }
            });

            setTimeout(function () {
                $('#mTContainer').css('height', height + "px");
            }, 600);
        };

        var getMarketTickerNumberOfRowsBasedOnSize = function (numberItemsDisplayed) {

            var newSize = 6;

            angular.forEach(marketTickerRowsAndHeight, function (rowDimensions) {
                if (rowDimensions.numberRows === numberItemsDisplayed) {
                    newSize = rowDimensions.tickerItems;
                }
            });

            return newSize;
        };

        var initializeMarketTickerHeight = function () {

            $scope.marketTickerItemsDisplayed = 6;
            var userDashboardConfig = dashboardConfigurationService.getDashboardLayout();
            angular.forEach(userDashboardConfig, function (dbConfig) {
                if (dbConfig.view.toLowerCase() === "marketticker") {
                    $scope.marketTickerItemsDisplayed = getMarketTickerNumberOfRowsBasedOnSize(dbConfig.tile.sizeY);
                }
            });

            $scope.setMtContainerHeight($scope.marketTickerItemsDisplayed);
        };

        initializeMarketTickerHeight();

        var getMarketTickerSizeBasedOnNumberRows = function (numberItemsDisplayed) {

            var newSize = 22;

            angular.forEach(marketTickerRowsAndHeight, function (rowDimensions) {
                if (numberItemsDisplayed === rowDimensions.tickerItems) {
                    newSize = rowDimensions.numberRows;
                }
            });

            return newSize;
        };

        $scope.maxNumberTickerItemsToDisplay = function () {

            var numberItemsToDisplay = 0,
                numberMarketTickerItems = $scope.marketTickerItems.length;

            if(numberMarketTickerItems < 7){
                numberItemsToDisplay = 6;
            } else if(numberMarketTickerItems < 10){
                numberItemsToDisplay = 9;
            } else if(numberMarketTickerItems < 13) {
                numberItemsToDisplay = 12;
            } else if(numberMarketTickerItems < 16){
                numberItemsToDisplay = 15;
            } else if(numberMarketTickerItems < 19){
                numberItemsToDisplay = 18;
            } else{
                numberItemsToDisplay = 21;
            }

            return numberItemsToDisplay;
        };

        var getMarketTickerStatus = function () {
            $scope.marketTickerProducts = marketTickerService.getMarketTickerProducts();
            $scope.typeList = marketTickerService.getMarketTickerTypes();
        };
        marketTickerService.registerProductStatusChange(getMarketTickerStatus);
        getMarketTickerStatus();

        $scope.user = userService.getUser();
        $scope.showDeal = userService.hasProductPrivilegeOnAnyProduct(["Deal_Create_Broker", "Deal_Create_Principal", "Deal_Create_All"]);
        $scope.enablePendingFilter = userService.hasProductPrivilegeOnAnyProduct(["Deal_Authenticate", "MarketTicker_Authenticate"]);


        var scrollTop = function () {
            $("#mTContainer").scrollTop = 0;
            $("#mTContainer").perfectScrollbar('update');
        };


        $scope.broadcastMarketTickerResized = function (event, size) {

            $scope.marketTickerItemsDisplayed = getMarketTickerNumberOfRowsBasedOnSize(size);
            $scope.setMtContainerHeight($scope.marketTickerItemsDisplayed);
        };

        $scope.$on('resizedMarketTicker', $scope.broadcastMarketTickerResized);

        $scope.$watchGroup(['marketTickerProducts', 'showPendingOnly', 'typeList'], function () {
            scrollTop();
        }, true);

        $scope.$on('TradePermissionsService-Change', function () {
            angular.forEach($scope.marketTickerItems, function (marketTickerItem) {
                // NOTE Can Trade will be null if we've failed over.
                if(marketTickerItem.canTrade) {
                    marketTickerItem.notTradeable = !marketTickerItem.canTrade();
                }
            });
        });

        $scope.$on('midnightService-midnight-event', function() {
            $log.debug("marketTickerController midnight callback to reformat market ticker item formatted dates");
            angular.forEach($scope.marketTickerItems, function (mti) {
                mti.updateFormattedDate();
            });
        });

        var refresh = function (marketTickerItems) {
            $scope.marketTickerItems = marketTickerItems;
            $scope.$apply();
            $("#mTContainer").perfectScrollbar('update');
        };

        var onTickEventHandler = function (marketTickerItems) {
            $timeout(function () {
                refresh(marketTickerItems);
            }, 0, true);

        };

        var onMarketTickerItemDeletedEventHandler = function (marketTickerId) {
            var mtId = parseInt(marketTickerId),
                mtItemIndex;

            mtItemIndex = $scope.marketTickerItems.getIndexBy("id", mtId);
            if (typeof mtItemIndex !== "undefined") {
                $scope.marketTickerItems.splice(mtItemIndex, 1);
                $scope.$apply();
            }

        };

        var onTickPushedItemEventHandler = function (marketTickerItem, positionToSplice, marketTickerItems) {

            // $scope.marketTickerItems.push(marketTickerItem);
            $log.debug("Current - " + _.pluck($scope.marketTickerItems, 'id').join(","));
            $log.debug("New - " + _.pluck(marketTickerItems, 'id').join(","));

            $timeout(function () {
                var addItem = true,
                    controllerNumTicks = $scope.marketTickerItems.length,
                    serviceNumTicks = 0;

                if (marketTickerItems) {
                    serviceNumTicks = marketTickerItems.length;
                }

                if (controllerNumTicks !== serviceNumTicks) {
                    if ((controllerNumTicks + 1) === serviceNumTicks) {
                        angular.forEach($scope.marketTickerItems, function (mtItem) {

                            if (addItem) {
                                if (mtItem.id === marketTickerItem.id) {
                                    addItem = false;
                                }
                            }
                        });
                        if (addItem) {
                            if (typeof positionToSplice === 'undefined') {
                                $scope.marketTickerItems.unshift(marketTickerItem);
                            } else {
                                $scope.marketTickerItems.splice(positionToSplice, 0, marketTickerItem);
                            }
                            $scope.$apply();
                        }
                    } else {
                        $scope.marketTickerItems = marketTickerItems;
                        $scope.$apply();
                    }
                }
            }, 0, true);
        };

        $scope.canClickDown = function () {
            if ($scope.marketTickerItemsDisplayed < $scope.maxNumberTickerItemsToDisplay()) {
                return true;
            }

            return false;
        };

        $scope.canClickUp = function () {
            if ($scope.marketTickerItemsDisplayed > 3) {
                return true;
            }

            return false;
        };

        $scope.increaseMarketTickerSize = function () {

            if ($scope.marketTickerItemsDisplayed < $scope.maxNumberTickerItemsToDisplay()) {
                $scope.marketTickerItemsDisplayed = $scope.marketTickerItemsDisplayed + 3;
                $scope.$emit('setMarketTickerSize', getMarketTickerSizeBasedOnNumberRows($scope.marketTickerItemsDisplayed));
            }
        };

        $scope.decreaseMarketTickerSize = function () {

            if ($scope.marketTickerItemsDisplayed > 3) {
                $scope.marketTickerItemsDisplayed = $scope.marketTickerItemsDisplayed - 3;
                $scope.$emit('setMarketTickerSize', getMarketTickerSizeBasedOnNumberRows($scope.marketTickerItemsDisplayed));
            }
        };

        $scope.mtItemDisabled = function () {
            return $scope.openDialog;
        };

        $scope.togglePending = function () {
            $scope.showPendingOnly = !$scope.showPendingOnly;
        };

        var itemHasAnyStatus = function(mti, statuses) {
            return _.contains(statuses, mti.marketTickerItemStatus);
        };

        var isPending = function(mti) {
            return itemHasAnyStatus(mti, ["Pending"]);
        };

        var isEditableDeal = function(mti) {
            return itemHasAnyStatus(mti, ["Active"]) && mti.marketTickerItemType === "Deal";
        };

        var isEditableInfo = function(mti) {
            return itemHasAnyStatus(mti, ["Active", "Updated"]) && mti.marketTickerItemType === "Info";
        };

        var isEditableOrder = function(mti) {
            return mti.isMine &&
                itemHasAnyStatus(mti, ["Active", "Updated", "Held"]) &&
                (mti.marketTickerItemType === "Bid" || mti.marketTickerItemType === "Ask");
        };

        var userHasPrivilegesToEditExternalOrder = function(order) {
            return userService.hasProductPrivilege(order.productId, ["ExternalOrder_Amend"]);
        };

        var editOrder = function(marketTickerItem) {
            if ($scope.openDialog === false) {
                var order = orderRepositoryService.getOrderById(marketTickerItem.orderId);
                if (!order || order.executionMode !== "External" || !userHasPrivilegesToEditExternalOrder(order)) {
                    return;
                }

                $scope.openDialog = true;
                var modalInstance = $modal.open({
                        templateUrl: 'src/app/modules/dashboard/dashboardModules/order/views/editOrder.part.html',
                        controller: 'editOrderController',
                        windowClass: 'orderModal',
                        backdrop: 'static',
                        resolve: {
                            productId: function () {
                                return order.productId;
                            },
                            order: function () {
                                return order;
                            },
                            orderType: function () {
                                return order.orderType;
                            },
                            tenorId: function () {
                                return order.tenor.id;
                            }
                        }
                    }
                );
                modalInstance.result.then(
                    function () {
                        $scope.openDialog = false;
                    },
                    function () {
                        $scope.openDialog = false;
                    }
                );
            }
        };

        $scope.marketTickerItemClicked = function (marketTickerItem) {
            if ($scope.openDialog === false) {
                if (isPending(marketTickerItem) ||
                    isEditableDeal(marketTickerItem) ||
                    isEditableInfo(marketTickerItem)) {
                    $scope.openDialog = true;
                    marketTickerService.checkPrivilegesSendAndWait(marketTickerItem);
                    $scope.openDialog = false;
                } else if (isEditableOrder(marketTickerItem)) {
                    editOrder(marketTickerItem);
                }
            }
        };

        var hasExternalMarketInfo = function () {
            return userService.hasProductPrivilegeOnAnyProduct(["MarketTicker_PlusInfo_Create"]);
        };

        var hasExternalOrder = function() {
            return userService.hasProductPrivilegeOnAnyProduct(
                ["ExternalOrder_Create_Principal", "ExternalOrder_Create_Broker"]);
        };

        $scope.openNewDeal = function () {
            if (userService.hasProductPrivilegeOnAnyProduct(["Deal_Create_Broker", "Deal_Create_Principal", "Deal_Create_All"]) && $scope.openDialog === false) {
                $scope.openDialog = true;
                var modalInstance = $modal.open({
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/newExternalDeal.part.html',
                    controller: 'newExternalDealController',
                    backdrop: 'static',
                    resolve: {}
                });

                modalInstance.result.then(function (reason) {
                    },
                    function () {
                        $scope.openDialog = false;
                    });
            }
        };

        $scope.showExternalInfo = function () {
            return hasExternalMarketInfo();
        };

        $scope.showExternalOrder = function() {
            return hasExternalOrder();
        };

        $scope.openNewInfo = function () {
            if (userService.hasProductPrivilegeOnAnyProduct(["MarketTicker_Create", "MarketTicker_Authenticate"]) && $scope.openDialog === false) {
                $scope.openDialog = true;
                var modalInstance = $modal.open({
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/newMarketInfo.part.html',
                    controller: 'newMarketInfoController',
                    backdrop: 'static',
                    resolve: {}
                });

                modalInstance.result.then(function (reason) {
                    },
                    function () {
                        $scope.openDialog = false;
                    });
            }
        };

        $scope.openNewExternalInfo = function () {
            if (hasExternalMarketInfo() && $scope.openDialog === false) {
                $scope.openDialog = true;
                var modalInstance = $modal.open({
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/newExternalMarketInfo.part.html',
                    controller: 'newExternalMarketInfoController',
                    backdrop: 'static',
                    resolve: {}
                });

                modalInstance.result.then(function (reason) {
                    },
                    function () {
                        $scope.openDialog = false;
                    });
            }
        };

        var openNewOrder = function(orderType, productId, productTenorId) {
            var modalInstance = $modal.open({
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/order/views/newOrder.part.html',
                    controller: 'newOrderController',
                    windowClass: 'orderModal',
                    backdrop: 'static',
                    resolve: {
                        productId: function () {
                            return productId;
                        },
                        orderType: function () {
                            return orderType;
                        },
                        tenorId:function(){
                            return productTenorId;
                        },
                        executionMode: function() {
                            return "External";
                        }
                    }
                });
            modalInstance.result.then(function(reason){},
                function(){
                    $scope.openDialog = false;
                });
        };

        $scope.openProductSelectorThenNewOrder = function(orderType) {
            if ($scope.openDialog === false && hasExternalOrder()) {
                $scope.openDialog = true;
                var modalProductSelect = $modal.open({
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/productSelector.part.html',
                    controller: 'productSelectorController',
                    backdrop: 'static',
                    resolve: {
                        executionMode: function() { return "External"; }
                    }
                });
                modalProductSelect.result.then(
                    function (result) {
                        openNewOrder(orderType, result.productId, result.tenorId);
                    },
                    function () {
                        $scope.openDialog = false;
                    }
                );
            }
        };

        $scope.externalDealReturned = function (msg) {
            if (msg.message.messageAction === 'Request' && $scope.openDialog === false) {
                if (msg.externalDeal.dealStatus === "Void") {
                    messagePopupsService.displayError("This deal was already voided");
                    return;
                }
                $scope.openDialog = true;
                var modalInstance = $modal.open({
                        templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/authExternalDeal.part.html',
                        controller: 'authExternalDealController',
                        windowClass: 'orderModal',
                        backdrop: 'static',
                        resolve: {
                            deal: function () {
                                return msg.externalDeal;
                            },
                            userContactDetails: function () {
                                return msg.userContactDetails;
                            }
                        }
                    }
                );

                modalInstance.result.then(function (reason) {
                    },
                    function (reason) {
                        $scope.openDialog = false;
                    });
            }
        };

        $scope.internalDealReturned = function (msg) {
            if (msg.message.messageAction === 'Request' && $scope.openDialog === false) {
                if (msg.deal.dealStatus === "Void") {
                    messagePopupsService.displayError("This deal was already voided");
                    return;
                }
                $scope.openDialog = true;
                var modalInstance = $modal.open({
                        templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/authInternalDeal.part.html',
                        controller: 'authInternalDealController',
                        windowClass: 'orderModal',
                        backdrop: 'static',
                        resolve: {
                            deal: function () {
                                return msg.deal;
                            },
                            userContactDetails: function () {
                                return msg.userContactDetails;
                            }
                        }
                    }
                );

                modalInstance.result.then(function (reason) {
                    },
                    function () {
                        $scope.openDialog = false;
                    });
            }
        };

        $scope.infoReturned = function (msg) {
            var modalInstance;

            if (msg.message.messageAction === 'Request' && $scope.openDialog === false) {
                $scope.openDialog = true;

                if (msg.marketInfo && msg.marketInfo.marketInfoType && msg.marketInfo.marketInfoType.toLowerCase() === "unspecified") {
                    modalInstance = $modal.open({
                        templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/authMarketInfo.part.html',
                        controller: 'authMarketInfoController',
                        windowClass: 'orderModal',
                        modal: true,
                        backdrop: 'static',
                        resolve: {
                            info: function () {
                                return msg.marketInfo;
                            },
                            userContactDetails: function () {
                                return msg.userContactDetails;
                            }
                        }
                    });

                    modalInstance.result.then(function (reason) {
                        },
                        function () {
                            $scope.openDialog = false;
                        });
                } else {
                    // This is an external Info we are authorising.
                    modalInstance = $modal.open({
                        templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/authExternalMarketInfo.part.html',
                        controller: 'authExternalMarketInfoController',
                        windowClass: 'orderModal',
                        modal: true,
                        backdrop: 'static',
                        resolve: {
                            info: function () {
                                return msg.marketInfo;
                            },
                            userContactDetails: function () {
                                return msg.userContactDetails;
                            }
                        }
                    });

                    modalInstance.result.then(function (reason) {
                        },
                        function () {
                            $scope.openDialog = false;
                        });
                }
            }
        };

        marketTickerService.onMarketTickRegister(onTickEventHandler);
        marketTickerService.onMarketTickRegisterPushedItems(onTickPushedItemEventHandler);
        marketTickerService.onMarketTickRegisterDeletedItem(onMarketTickerItemDeletedEventHandler);
        marketTickerMessageSubscriptionService.registerActionOnMarketTickerMessageType('Deal', $scope.internalDealReturned);
        marketTickerMessageSubscriptionService.registerActionOnMarketTickerMessageType('ExternalDeal', $scope.externalDealReturned);
        marketTickerMessageSubscriptionService.registerActionOnMarketTickerMessageType('Info', $scope.infoReturned);
    });