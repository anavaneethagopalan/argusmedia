angular.module('argusOpenMarkets.dashboard').
service("orderDialogService", function orderDialogService(userService) {
    "use strict";

    var isInternalOrder = function(executionMode) {
        return executionMode === "Internal";
    };

    var hasNeededOrderCreatePrivilegesAsPrincipal = function(productId, executionMode) {
        var neededPrivileges;
        if (isInternalOrder(executionMode)) {
            neededPrivileges = ["Order_Create_Principal", "Order_Create_Any"];
        } else {
            neededPrivileges = ["ExternalOrder_Create_Principal"];
        }
        return userService.hasProductPrivilege(productId, neededPrivileges);
    };

    var hasNeededOrderCreatePrivilegesAsBroker = function(productId, executionMode) {
        var neededPrivileges;
        if (isInternalOrder(executionMode)) {
            neededPrivileges = ["Order_Create_Broker", "Order_Create_Any"];
        } else {
            neededPrivileges = ["ExternalOrder_Create_Broker"];
        }
        return userService.hasProductPrivilege(productId, neededPrivileges);
    };

    return {
        hasNeededOrderCreatePrivilegesAsPrincipal: hasNeededOrderCreatePrivilegesAsPrincipal,
        hasNeededOrderCreatePrivilegesAsBroker: hasNeededOrderCreatePrivilegesAsBroker
    };
});