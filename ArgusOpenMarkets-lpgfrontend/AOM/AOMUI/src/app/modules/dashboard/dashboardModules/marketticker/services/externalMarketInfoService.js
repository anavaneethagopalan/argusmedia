angular.module('argusOpenMarkets.dashboard').service("externalMarketInfoService", function exterMarketInfoService() {

    "use strict";

    var selectSingleProductForMarketInfo = function(marketTickerInfoProducts){
        var numProductsSelected = _.reduce(
            marketTickerInfoProducts,
            function(memo, p) { return memo + Number(p.ticked); },
            0);

        if (numProductsSelected !== 1) {
            angular.forEach(marketTickerInfoProducts, function(p){ p.ticked = false; });
        }
    };

    var canOnlySelectSingleProduct = function(scope){
        return scope.marketInfoType !== "info";
    };

    var setMarketInfoType = function(scope, marketInfoType){

        scope.marketInfoType = marketInfoType;
        if(canOnlySelectSingleProduct(scope)){
            // For bids / asks / deals - only one product can be selected.
            selectSingleProductForMarketInfo(scope.marketTickerInfoProducts);
            scope.maxDropDownLabelsDisplay = 1;
            scope.dropDownSelectMode = "single";
            scope.dropDownButtonLabel = "productName";
        } else {
            scope.maxDropDownLabelsDisplay = 0;
            scope.dropDownSelectMode = "multiple";
            scope.dropDownButtonLabel = "";
        }

        scope.marketTickerInfoProducts = angular.copy(scope.marketTickerInfoProducts);
    };

    return {
        "setMarketInfoType": setMarketInfoType
    };

});
