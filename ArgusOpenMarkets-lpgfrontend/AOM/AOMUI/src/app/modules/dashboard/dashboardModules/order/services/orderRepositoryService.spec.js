describe('argusOpenMarkets.dashboard.orderRepositoryService', function() {
    "use strict";
    describe('orderRepositoryService tests', function () {

        var service;
        var mockUserService;
        var mockProductMessageSubscriptionService;
        var $rootScope;
        var fakeUserSubscriptionsProductIds = [3, 5, 7];

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(function() {
            module('argusOpenMarkets.dashboard');
        });

        beforeEach(function() {
            mockUserService = jasmine.createSpyObj('mockUserService', [
                'hasProductPrivilege',
                'getSubscribedProductIds'
            ]);
            mockUserService.hasProductPrivilege.and.returnValue(true);
            mockUserService.getSubscribedProductIds.and.callFake(function() { return fakeUserSubscriptionsProductIds; });

            mockProductMessageSubscriptionService = jasmine.createSpyObj('', [
                'subscribeToProduct',
                'registerActionOnProductMessageType',
                'unsubscribeFromProduct',
                'unregisterActionOnProductMessageType'
            ]);
        });

        beforeEach(function() {
            module(function($provide) {
                $provide.value('userService', mockUserService);
                $provide.value('productMessageSubscriptionService', mockProductMessageSubscriptionService);
            });
        });

        beforeEach(inject(function(_$rootScope_, orderRepositoryService){
            service = orderRepositoryService;
            $rootScope = _$rootScope_;
        }));

        var defaultPrice = 123;
        var defaultQuantity = 3456;

        function getHandler(productId, handlerString) {
            var argsArray = mockProductMessageSubscriptionService.registerActionOnProductMessageType.calls.allArgs();
            var handler = null;
            argsArray.forEach(function(callArgs) {
                expect(callArgs).not.toBeNull();
                expect(callArgs.length).toBe(4);
                if (!handler && callArgs[1] == productId && callArgs[2] == handlerString) {
                    handler = callArgs[3];
                }
            });
            expect(handler).not.toBeNull();
            return handler;
        }

        function createTopicKey(orderId) { return "topicKey" + orderId; }

        function addOrder(id, productId, bidAsk, price, quantity, executionMode) {
            var orderMessage = {
                "order": {
                    "id": id,
                    "price": price || defaultPrice,
                    "quantity": quantity || defaultQuantity,
                    "orderType": bidAsk,
                    "productId": productId,
                    "topicKey" : createTopicKey(id),
                    "executionMode": executionMode || "Internal"
                }
            };
            var handlerString = bidAsk == "Bid" ? "onBid" : "onAsk";
            var eventHandler = getHandler(productId, handlerString);
            eventHandler(orderMessage);
        }

        function addBid(id, productId, price, quantity) {
            addOrder(id, productId, "Bid", price, quantity);
        }

        function addAsk(id, productId, price, quantity) {
            addOrder(id, productId, "Ask", price, quantity);
        }

        function deleteOrder(id, productId) {
            var eventHandler = getHandler(productId, "onOrderDeleted");
            eventHandler(createTopicKey(id));
        }

        it('should have a defined service', function() {
            expect(service).toBeDefined();
        });

        describe('internal subscriptions', function() {

            it('should subscribe to all products', function() {
                fakeUserSubscriptionsProductIds.forEach(function(productId) {
                    expect(mockProductMessageSubscriptionService.subscribeToProduct).toHaveBeenCalledWith(productId);
                });
            });

            it('should subscribe to onBid actions for all subscribed products', function() {
                fakeUserSubscriptionsProductIds.forEach(function (productId) {
                    expect(mockProductMessageSubscriptionService.registerActionOnProductMessageType).toHaveBeenCalledWith(
                        jasmine.any(String), productId, "onBid", jasmine.any(Function));
                });
            });

            it('should subscribe to onAsk actions for all subscribed products', function() {
                fakeUserSubscriptionsProductIds.forEach(function (productId) {
                    expect(mockProductMessageSubscriptionService.registerActionOnProductMessageType).toHaveBeenCalledWith(
                        jasmine.any(String), productId, "onAsk", jasmine.any(Function));
                });
            });

            it('should subscribe to onDelete actions for all subscribed products', function() {
                fakeUserSubscriptionsProductIds.forEach(function (productId) {
                    expect(mockProductMessageSubscriptionService.registerActionOnProductMessageType).toHaveBeenCalledWith(
                        jasmine.any(String), productId, "onOrderDeleted", jasmine.any(Function));
                });
            });
        });

        describe('adding orders', function() {

            it('should find a bid that has been added', function() {
                addBid(1, 3);
                var o = service.getOrderById(1);
                expect(o).toBeDefined();
                expect(o).not.toBeNull();
                expect(o.productId).toEqual(3);
                expect(o.id).toEqual(1);
                expect(o.orderType).toEqual("Bid");
            });

            it('should find an ask that has been added', function() {
                addAsk(1, 3);
                var o = service.getOrderById(1);
                expect(o).toBeDefined();
                expect(o).not.toBeNull();
                expect(o.productId).toEqual(3);
                expect(o.id).toEqual(1);
                expect(o.orderType).toEqual("Ask");
            });

            it('should not find an order that has not been added', function() {
                addBid(1, 3);
                var o = service.getOrderById(999);
                expect(o).not.toBeDefined();
            });

            it('should find order by id if more than one order has been added', function() {
                addBid(1, 3);
                addBid(2, 5);
                var o = service.getOrderById(2);
                expect(o).toBeDefined();
                expect(o).not.toBeNull();
                expect(o.productId).toEqual(5);
                expect(o.id).toEqual(2);
                expect(o.orderType).toEqual("Bid");
            });
        });

        describe('updating orders', function() {
            it('should find the latest version of an updated order', function() {
                addBid(1, 3, 100);
                addBid(1, 3, 200);
                var o = service.getOrderById(1);
                expect(o.id).toEqual(1);
                expect(o.price).toEqual(200);
            })
        });

        describe('deleting orders', function() {
            it('should not find a deleted order', function() {
                addBid(1, 3);
                deleteOrder(1, 3);
                var o = service.getOrderById(1);
                expect(o).not.toBeDefined();
            });
            it('should still find existing orders when a different order is deleted', function() {
                addBid(1, 3);
                addBid(2, 5);
                deleteOrder(1, 3);
                var o2 = service.getOrderById(2);
                expect(o2).toBeDefined();
                var o1 = service.getOrderById(1);
                expect(o1).not.toBeDefined();
            });
        });

        describe('external subscriptions', function() {
            var createSubscriber = function(id, products, orderTypes) {
                var subsId = "testSubscriber" + id;
                return {
                    "id": subsId,
                    "products": products,
                    "callbacks": jasmine.createSpyObj(subsId + 'Callbacks', ['onBid', 'onAsk', 'onDelete']),
                    "orderTypes": orderTypes
                };
            };
            var s1;
            var s2;

            beforeEach(function() {
                s1 = createSubscriber(1, [3,5], "*");
                s2 = createSubscriber(2, [5,7], "Internal");
                service.subscribeToOrderUpdates(s1.id, s1.products, s1.callbacks, s1.orderTypes);
                service.subscribeToOrderUpdates(s2.id, s2.products, s2.callbacks, s2.orderTypes);
            });

            it('should notify a subscriber to all order types when a new bid event occurs', function() {
                expect(s1.callbacks.onBid).not.toHaveBeenCalled();
                addOrder(1, 3, "Bid");
                expect(s1.callbacks.onBid).toHaveBeenCalled();
            });

            it('should notify a subscriber to all order types when a new ask event occurs', function() {
                expect(s1.callbacks.onAsk).not.toHaveBeenCalled();
                addOrder(1, 3, "Ask");
                expect(s1.callbacks.onAsk).toHaveBeenCalled();
            });

            it('should notify a subscriber to all order types when a new delete event occurs', function() {
                addOrder(1, 3, "Bid");
                expect(s1.callbacks.onBid).toHaveBeenCalled();
                deleteOrder(1, 3);
                expect(s1.callbacks.onDelete).toHaveBeenCalled();
            });

            it('should notify a subscriber to internal orders when a new bid event for an internal order occurs', function() {
                expect(s2.callbacks.onBid).not.toHaveBeenCalled();
                addOrder(1, 5, "Bid");
                expect(s2.callbacks.onBid).toHaveBeenCalled();
            });

            it('should notify a subscriber to all orders when a new bid event for an external order occurs', function() {
                expect(s1.callbacks.onBid).not.toHaveBeenCalled();
                addOrder(1, 5, "Bid", 123, 456, "External");
                expect(s1.callbacks.onBid).toHaveBeenCalled();
            });

            it('should not notify a subscriber to internal orders when a new bid event for an external order occurs', function() {
                addOrder(1, 5, "Bid", 123, 456, "External");
                expect(s2.callbacks.onBid).not.toHaveBeenCalled();
            });

            it('should not notify a subscriber about a delete action for different product', function() {
                addOrder(1, 7, "Bid");
                deleteOrder(1, 7);
                expect(s1.callbacks.onDelete).not.toHaveBeenCalled();
            });

            it('should not notify a subscriber about a delete action for a different order type', function() {
                addOrder(1, 5, "Bid", 123, 456, "External");
                deleteOrder(1, 5);
                expect(s2.callbacks.onDelete).not.toHaveBeenCalled();
            });

            it('should not notify a subscriber when an event for an order for a non-subscribed product occurs', function() {
                addOrder(1, 7, "Bid");
                expect(s1.callbacks.onBid).not.toHaveBeenCalled();
            });

            it('should not notify a subscriber that has unsubscribed', function() {
                service.unsubscribeFromOrderUpdates(s1.id);
                addOrder(1, 3, "Bid");
                expect(s1.callbacks.onBid).not.toHaveBeenCalled();
            });

            it('should notify all subscribers for a product', function() {
                expect(s1.callbacks.onBid).not.toHaveBeenCalled();
                expect(s2.callbacks.onBid).not.toHaveBeenCalled();
                addOrder(1, 5, "Bid");
                expect(s1.callbacks.onBid).toHaveBeenCalled();
                expect(s2.callbacks.onBid).toHaveBeenCalled();
            });
        });

        describe('unsubscribe on rootScope destroy', function() {

            it('should unsubscribe for all products', function() {
                $rootScope.$emit('$destroy');
                fakeUserSubscriptionsProductIds.forEach(function(productId) {
                    expect(mockProductMessageSubscriptionService.unsubscribeFromProduct).toHaveBeenCalledWith(productId);
                });
            });

            it('should unsubscribe from onBid actions for all products', function() {
                $rootScope.$emit('$destroy');
                fakeUserSubscriptionsProductIds.forEach(function (productId) {
                    expect(mockProductMessageSubscriptionService.unregisterActionOnProductMessageType).toHaveBeenCalledWith(
                        jasmine.any(String), productId, "onBid");
                });
            });

            it('should unsubscribe from onAsk actions for all products', function() {
                $rootScope.$emit('$destroy');
                fakeUserSubscriptionsProductIds.forEach(function (productId) {
                    expect(mockProductMessageSubscriptionService.unregisterActionOnProductMessageType).toHaveBeenCalledWith(
                        jasmine.any(String), productId, "onAsk");
                });
            });

            it('should unsubscribe from onDelete actions for all products', function() {
                $rootScope.$emit('$destroy');
                $rootScope.$emit('$destroy');
                fakeUserSubscriptionsProductIds.forEach(function (productId) {
                    expect(mockProductMessageSubscriptionService.unregisterActionOnProductMessageType).toHaveBeenCalledWith(
                        jasmine.any(String), productId, "onOrderDeleted");
                });
            });
        });
    });
});
