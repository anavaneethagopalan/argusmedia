﻿angular.module('argusOpenMarkets.dashboard').directive('mtItem', function ($filter, userService, helpers) {
    return {
        transclude: true,
        scope: {
            item: '='
        },
        templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/directives/marketTickerItem/marketTickerItem.part.html',
        replace: true,
        restrict: 'AE',
        controller: function ($scope, userService, tradePermissionsService, dateService) {

            var user = userService.getUser();

            $scope.getMarketTickerType = function(item){

                var tickerText = "",
                    tickerClass = "",
                    externalType = false,
                    tickerPrefix = "",
                    enlargeTickerPrefix = false;

                if(item.marketTickerItemType.toLowerCase() === "info")
                {
                    if(item.marketInfoType === null || item.marketInfoType === "" || typeof(item.marketInfoType) === "undefined"){
                        item.marketInfoType = "Info";
                        tickerText = "INFO";
                        tickerClass = "info";
                    } else {
                        tickerClass = item.marketInfoType.toLowerCase();
                        tickerText = item.marketInfoType.toUpperCase();
                        externalType = true;
                    }
                } else {
                    tickerText = item.marketTickerItemType.toUpperCase();
                    tickerClass = item.marketTickerItemType.toLowerCase();
                    if (tickerClass === "deal" && !helpers.isEmpty(item.externalDealId)) {
                        externalType = true;
                        enlargeTickerPrefix = true;
                    }
                }

                if(externalType){
                    tickerPrefix = "+";
                }

                return {
                    "tickerPrefix": tickerPrefix,
                    "enlargeTickerPrefix": enlargeTickerPrefix,
                    "tickerText": tickerText,
                    "tickerClass": tickerClass,
                    "tickerExternal": externalType
                };
            };


            $scope.doesLoggedOnUserOwnMarketTickerItem = function (marketTickerItem) {

                if (marketTickerItem) {
                    if (marketTickerItem.createdCompanyId === user.organisationId) {
                        return true;
                    }

                    var counter;
                    for (counter = 0; counter < marketTickerItem.ownerOrganisations.length; counter++) {
                        if (marketTickerItem.ownerOrganisations[counter] === user.organisationId) {
                            return true;
                        }
                    }
                }

                return false;
            };

            $scope.item.canTrade = function () {
                var counter;
                var buyOrSell;
                if (this.marketTickerItemType === "Ask" || this.marketTickerItemType === "Bid") {
                    for (counter = 0; counter < this.ownerOrganisations.length; counter++) {
                        if (this.ownerOrganisations[counter] === user.organisationId) {
                            return true;
                        }
                    }

                    buyOrSell = this.marketTickerItemType === "Ask" ? "sell" :"buy";

                    // There are no brokers listed on this && I am a broker.
                    if(user.userOrganisation.organisationType === "Brokerage" && this.ownerOrganisations.length < 2){
                        // Does broker company have permission to trade with owner company?
                        // 1 = Any Broker

                        // TODO: COBROKERING - NEED TO PASS IN WHETHER OR NOT THIS ORDER HAS BEEN CO-BROKERED
                        return this.brokerRestriction === 1 &&
                            tradePermissionsService.canTrade(this.productIds[0], buyOrSell, this.ownerOrganisations[0], null, false);
                    }

                    // TODO: COBROKERING - NEED TO PASS IN WHETHER THIS HAS CO-BROKERING
                    return tradePermissionsService.canTrade(this.productIds[0], buyOrSell, this.ownerOrganisations[0], this.ownerOrganisations[1], false);
                }
                return true;
            };


            var getFormattedDate = function (date) {
                var today = dateService.getDateTimeNow(),
                    testDate = new Date(date);

                today.setHours(0, 0, 0, 0);
                testDate.setHours(0, 0, 0, 0);

                if (testDate.getTime() === today.getTime()) {
                    return $filter('date')(date, 'HH:mm');
                }
                return $filter('date')(date, 'HH:mm | dd-MMM-y');
            };

            $scope.item.updateFormattedDate = function() {
                this.formattedDate = getFormattedDate(this.lastUpdated);
            };

            $scope.item.updateFormattedDate();
        },
        link: function (scope, elem, attrs) {

            var tickerType = scope.getMarketTickerType(scope.item);
            scope.item.marketTickerItemTypePrefix = tickerType.tickerPrefix;
            scope.item.marketTickerEnlargeItemPrefix = tickerType.enlargeTickerPrefix;
            scope.item.marketTickerItemTypeUpper = tickerType.tickerText;
            scope.item.itemDisplayClass = tickerType.tickerClass;

            if (scope.item.notes !== null && scope.item.notes.trim().length === 0) {
                scope.item.notes = null;
            }
            if (scope.item.marketTickerItemStatus === "Void") {
                scope.item.isVoid = true;
            }
            else if (scope.item.marketTickerItemStatus === "Pending") {

                var hasProductIds = scope.item.productIds && scope.item.productIds.length !== 0;

                if (scope.item.marketTickerItemType === "Info") {
                    var shouldBeMarkedPending = false;
                    if (!hasProductIds) {
                        shouldBeMarkedPending = userService.hasProductPrivilegeOnAnyProduct(["marketTicker_Authenticate"]);
                    } else {
                        shouldBeMarkedPending = userService.hasAnyProductPrivileges(scope.item.productIds, ["marketTicker_Authenticate"]);
                    }

                    if (shouldBeMarkedPending) {
                        scope.item.isPending = true;
                        return;
                    }
                } else if (scope.item.marketTickerItemType === "Deal") {

                    if (hasProductIds && userService.hasProductPrivilege(scope.item.productIds[0], ["deal_Authenticate"])) {
                        scope.item.isPending = true;
                        return;
                    }
                }
            }


            scope.item.notTradeable = !scope.item.canTrade();

            scope.item.isMine = scope.doesLoggedOnUserOwnMarketTickerItem(scope.item);
        }
    };
});