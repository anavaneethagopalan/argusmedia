﻿describe( 'argusOpenMarkets.dashboard.viewOrder', function() {
    "use strict";
    describe( 'viewOrderController', function() {
        var $scope,mockProductConfigurationService,mockModalInstance,mockUserOrderService,testOrder,thisController, provide;

        mockUserOrderService = {
            sendHoldOrderMessage:sinon.spy(),
            sendKillOrderMessage:sinon.spy(),
            sendReinstateOrderMessage:sinon.spy(),
            sendEditOrderMessage:sinon.spy()

        };

        testOrder = {
            orderStatus: 'Active',
            notes: ""
        };


        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );
        beforeEach(module(function($provide) { provide = $provide; }));
        beforeEach( inject( function( $controller, $rootScope, helpers ) {
            $scope = $rootScope.$new();

            var userOrganisation = {
                organisationType: "Brokerage"
            };

            var user  = {
                userOrganisation : userOrganisation
            };

            $scope.selectedPrincipal = {
                userId: {},
                organisationId: {}
            };

            var mockUserService = {
                getUsersOrganisation : function(){return {organisationName:"XYZ"};},
                hasProductPrivilege: function(productId, privs){return true;},
                getUser: function(){ return user;}
            };

            mockProductConfigurationService = {
                getProductConfigurationById:function(id)
                {
                    return {
                        "productId":1 ,
                        "productName":"Argus-Naphtha-CIF-NWE-Outright" ,
                        "productTitle":"Argus-Naphtha-CIF-NWE-Outright",
                        "contractSpecification":
                        {
                            "contractType":"Physical",
                            "volume":{"volumeUnitCode": "MT", "minimum":12500 ,"maximum":35000 ,"increment":500 ,"decimalPlaces":0},
                            "pricing":{"pricingUnitCode":"MT", "pricingCurrencyCode":"$", "minimum":1, "maximum":9999, "increment":0.01, "decimalPlaces":2},
                            "tenors": [
                                {
                                    "deliveryLocation": {
                                        "id": 1,
                                        "name": "Rotterdam",
                                        "dateCreated": "2014-01-01T00:00:00Z"
                                    },
                                    "deliveryStartDate": "+1d",
                                    "deliveryEndDate": "+20d",
                                    "rollDate": "DAILY",
                                    "id": 1,
                                    "minimumDeliveryRange": 4
                                }
                            ]
                        }
                    };
                },
                getCommonProductQuantities: function(id){
                    return [10000,20000,30000];
                },
                getProductConfigurationLabels: function(id){
                    return {
                        "locationLabel": "Location",
                        "deliveryPeriodLabel": "Delivery Period"
                    };
                },
                getProductMetaDataById: function(productId){
                    return null;
                }
            };
            provide.value('productConfigurationService', mockProductConfigurationService);

            mockModalInstance={
                dismiss: function(){
                    return true;
                }
            };


            thisController = $controller( 'viewOrderController',
                { $scope: $scope,
                    productConfigurationService: mockProductConfigurationService,
                    $modalInstance:mockModalInstance,
                    userOrderService:mockUserOrderService,
                    orderType:'bid',
                    productId:1,
                    userService: mockUserService,
                    tenorId:1,
                    order:testOrder,
                    principalsFromService: [{"userId": 1},{"userId": 1},{"Meee":3}],
                    brokersFromService : [{"userId": 1},{"userId": 2}]
                });
        }));

        it( 'should be defined', inject( function() {
            expect( thisController ).toBeDefined();
        }));

        it('should get the product config', inject(function(){
            var prodConfig = mockProductConfigurationService.getProductConfigurationById(1);
            expect(prodConfig).not.toBe(null);
        }));

        it('should set order type to the passed in order type', inject(function(){
            expect($scope.orderType).toEqual('bid');
        }));

        it('should set the order type on setOrderType', inject(function(){
            $scope.setOrderType('offer');
            expect($scope.orderType).toEqual('offer');
        }));

        it('should call dismiss on exit', function(){
            spyOn(mockModalInstance,'dismiss');

            $scope.exit();

            expect(mockModalInstance.dismiss).toHaveBeenCalledWith('exit');
        });
    });
});