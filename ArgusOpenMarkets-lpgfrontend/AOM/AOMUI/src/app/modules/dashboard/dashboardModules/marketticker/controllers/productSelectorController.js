angular.module('argusOpenMarkets.dashboard')
    .controller('productSelectorController', function productSelectorController($scope,
                                                                                $modalInstance,
                                                                                productSelectorService,
                                                                                executionMode) {

        $scope.result = {
            "productId": null,
            "tenorId": null
        };

        $scope.productSelected = function(product) {
            var reply = productSelectorService.onProductSelected(product);
            $scope.result = reply.result;
            $scope.tenors = reply.tenors;
            $scope.enableTenorSelection = reply.enableTenorSelection;
        };

        $scope.tenorSelected = function(tenor) {
            $scope.result.tenorId = tenor.id;
        };

        $scope.enableOk = function() {
            return $scope.result.productId !== null && $scope.result.tenorId !== null;
        };

        $scope.ok = function() {
            $modalInstance.close($scope.result);
        };

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.products = productSelectorService.getUsersTradableProductConfigs(executionMode);
    });
