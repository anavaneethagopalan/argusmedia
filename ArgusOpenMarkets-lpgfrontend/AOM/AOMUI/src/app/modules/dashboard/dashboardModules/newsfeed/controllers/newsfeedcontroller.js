﻿angular.module('argusOpenMarkets.dashboard')
    .controller('newsFeedController', function newsFeedController($scope, $timeout, $http, $sce, $filter, newsService, userService, $log) {
        "use strict";

        $scope.Title = "News & Analysis";
        $scope.newsFeedItems = [];
        $scope.expandedItem = null;
        $scope.newsFeedItems = newsService.getNews();

        var newsItemReceived = function (news) {
            $scope.newsFeedItems = newsService.getNews();
            $('#news-data-scroller').animate({scrollTop: 0}, 1000);
        };

        $scope.expandItem = function (item) {
            var checkItem,
                userId = userService.getUser().id;

            for (var i = 0; i < $scope.newsFeedItems.length; i++) {
                checkItem = $scope.newsFeedItems[i];
                if (checkItem.expanded) {
                    checkItem.expanded = false;
                }
            }
            $scope.expandedItem = angular.copy(item);
            $scope.expandedItem.story = "Fetching article ...";
            $scope.expandedItem.expanded = true;
            item.expanded = true;
            newsService.getNewsStory(item.cmsId, fullArticleReceived, errorResponseReceived);
        };

        var fullArticleReceived = function (response) {
            $scope.expandedItem.story = response.story;
            $scope.expandedItem.error = response.error;
            $('#news-data-scroller').animate({scrollTop: $('#article' + $scope.expandedItem.cmsId).position().top + $('#news-data-scroller').scrollTop()}, 1000);
        };

        var errorResponseReceived = function (error) {
            $scope.expandedItem.story = error;
            $scope.expandedItem.error = true;
            $('#news-data-scroller').animate({scrollTop: $('#article' + $scope.expandedItem.cmsId).position().top + $('#news-data-scroller').scrollTop()}, 1000);
        };

        $scope.closeExpandedItem = function (item) {
            $scope.expandedItem.expanded = false;
            item.expanded = false;
        };

        newsService.subscribeToNewsAlerts($scope, newsItemReceived);

        $scope.$on('midnightService-midnight-event', function() {
            $log.debug("Midnight callback in News Feed Controller, asking News Service to update all formatted dates");
            newsService.updateAllFormattedDates();
        });

    });

