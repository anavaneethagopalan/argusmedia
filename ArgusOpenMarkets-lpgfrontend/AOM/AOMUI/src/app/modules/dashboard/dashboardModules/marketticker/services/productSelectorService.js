angular.module('argusOpenMarkets.dashboard').service("productSelectorService",
    function productSelectorService(userService,
                                    productConfigurationService,
                                    $log) {

        var isTradableProduct = function(p) {
            return p && !p.isInternal && p.contractSpecification &&
                p.contractSpecification.tenors && (p.contractSpecification.tenors.length > 0);
        };

        var hasAddOrderPermissionForProduct = function(productId) {
            return userService.hasProductPrivilege(
                productId, ["Order_Create_Broker", "Order_Create_Principal", "Order_Create_All"]);
        };

        var hasAddExternalOrderPermissionForProduct = function(productId) {
            return userService.hasProductPrivilege(
                productId, ["ExternalOrder_Create_Principal", "ExternalOrder_Create_Broker"]);
        };

        var getUsersTradableProductConfigs = function(executionMode) {
            var subsProdIds = userService.getSubscribedProductIds();
            var hasRequiredProductPrivileges =
                executionMode === "External" ? hasAddExternalOrderPermissionForProduct : hasAddOrderPermissionForProduct;
            return _.chain(subsProdIds)
                    .filter(hasRequiredProductPrivileges)
                    .map(productConfigurationService.getProductConfigurationById)
                    .filter(isTradableProduct)
                    .value();
        };

        var addTenorDisplayName = function(t) {
            t.displayName = t.deliveryLocation.name + " (" + t.deliveryStartDate + ":" + t.deliveryEndDate + ")";
        };

        var onProductSelected = function(product) {
            var reply = {
                "result": {
                    "productId": product.productId,
                    "tenorId": null
                },
                "tenors": angular.copy(product.contractSpecification.tenors),
                "enableTenorSelection": false
            };
            if (!reply.tenors || reply.tenors.length === 0) {
                $log.warn("Selected product has no tenors");
                return reply;
            }
            if (reply.tenors.length > 1) {
                _.each(reply.tenors, addTenorDisplayName);
                reply.enableTenorSelection = true;
            } else {
                reply.result.tenorId = reply.tenors[0].id;
            }
            return reply;
        };

        return {
            "getUsersTradableProductConfigs": getUsersTradableProductConfigs,
            "onProductSelected": onProductSelected
        };
    });
