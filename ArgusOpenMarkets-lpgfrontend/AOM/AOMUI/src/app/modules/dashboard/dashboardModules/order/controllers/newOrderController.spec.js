﻿describe( 'argusOpenMarkets.dashboard.newOrder', function() {
    "use strict";
    describe( 'newOrderController', function() {
        var $scope, mockProductConfigurationService,mockBrokerableProductConfigurationService, mockModalInstance, mockUserOrderService, thisController;
        var mockUserApi, provide;

        var userOrganisation = {
            organisationType: "Brokerage",
            name: "FakeBrokerage"
        };
        var user  = {
            userOrganisation : userOrganisation
        };
        var mockUserService;
        var executionMode;
        var mockOrderDialogService;

        var metadataDefinitions = [{
            "metadataList": {
                "fieldList": [
                    {
                        "id": 1,
                        "displayName": "Port A",
                        "itemValue": 100
                    },
                    {
                        "id": 2,
                        "displayName": "Port B",
                        "itemValue": 101
                    },
                    {
                        "id": 3,
                        "displayName": "Port c",
                        "itemValue": 102
                    }
                ]
            },
            "id": 1,
            "productId": 1,
            "displayName": "Delivery Port",
            "fieldType": "IntegerEnum"
        }, {
            "valueMinimum": 0,
            "valueMaximum": 20,
            "id": 3,
            "productId": 1,
            "displayName": "Delivery Description",
            "fieldType": "String"
        }];

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );
        beforeEach( module(function($provide) { provide = $provide; }));
        beforeEach( inject( function( $controller, $rootScope ) {
            $scope = $rootScope.$new();

            mockUserService = {
                getUsersOrganisation : function(){return {organisationName:"XYZ",organisationType: "Trading"};},
                getUser: function(){ return user;},
                hasProductPrivilege: function(productId, privs){return true;},
                isBroker: function(){ return false;}
            };

            mockProductConfigurationService = {

                getProductConfigurationById:function(id)
                {
                    return {
                        "productId":1 ,
                        "coBrokering": false ,
                        "productName":"Argus-Naphtha-CIF-NWE-Outright" ,
                        "productTitle":"Argus-Naphtha-CIF-NWE-Outright",
                        "contractSpecification":
                        {
                            "contractType":"Physical",
                            "volume":{"volumeUnitCode": "MT", "minimum":12500 ,"maximum":35000 ,"increment":500 ,"decimalPlaces":0},
                            "pricing":{"pricingUnitCode":"MT", "pricingCurrencyCode":"$", "minimum":1, "maximum":9999, "increment":0.10, "decimalPlaces":2},
                            "tenors": [
                                {
                                    "deliveryLocation": {
                                        "id": 1,
                                        "name": "Rotterdam",
                                        "dateCreated": "2014-01-01T00:00:00Z"
                                    },
                                    "deliveryStartDate": "+1d",
                                    "deliveryEndDate": "+20d",
                                    "rollDate": "DAILY",
                                    "id": 1,
                                    "minimumDeliveryRange": 4
                                }
                            ]
                        }
                    };
                },
                getCommonProductQuantities: function(id){
                    return [10000,20000,30000];
                },
                getProductConfigurationLabels: function(id){
                    return {
                        "locationLabel": "Location",
                        "deliveryPeriodLabel": "Delivery Period"
                    };
                },
                getProductMetaDataById: function(id){
                    return { "productId": 1, "fields": metadataDefinitions };
                }
            };

            mockBrokerableProductConfigurationService = {

                getProductConfigurationById:function(id)
                {
                    return {
                        "productId":1 ,
                        "coBrokering": true ,
                        "productName":"Argus-Naphtha-CIF-NWE-Outright" ,
                        "productTitle":"Argus-Naphtha-CIF-NWE-Outright",
                        "contractSpecification":
                        {
                            "contractType":"Physical",
                            "volume":{"volumeUnitCode": "MT", "minimum":12500 ,"maximum":35000 ,"increment":500 ,"decimalPlaces":0},
                            "pricing":{"pricingUnitCode":"MT", "pricingCurrencyCode":"$", "minimum":1, "maximum":9999, "increment":0.10, "decimalPlaces":2},
                            "tenors": [
                                {
                                    "deliveryLocation": {
                                        "id": 1,
                                        "name": "Rotterdam",
                                        "dateCreated": "2014-01-01T00:00:00Z"
                                    },
                                    "deliveryStartDate": "+1d",
                                    "deliveryEndDate": "+20d",
                                    "rollDate": "DAILY",
                                    "id": 1,
                                    "minimumDeliveryRange": 4
                                }
                            ]
                        }
                    };
                },
                getCommonProductQuantities: function(id){
                    return [10000,20000,30000];
                },
                getProductConfigurationLabels: function(id){
                    return {
                        "locationLabel": "Location",
                        "deliveryPeriodLabel": "Delivery Period"
                    };
                },
                getProductMetaDataById: function(id){
                    return null;
                }
            };

            mockUserApi = {
                getPrincipals: function (productId, canTradeOnly, buyOrSell) {
                    return {then: function(callback) { callback([{"userId": 1}, {"userId": 2}, {"Meee": 3}]); }};
                },
                getBrokers: function (productId, canTradeOnly, buyOrSell) {
                    return {then: function(callback) { callback([{"userId": 1}, {"userId": 2}]); }};
                },
                formatErrorResponse: function (error) {
                    this.error = error;
                },
                error: ""
            };

            mockModalInstance={
                dismiss: function(){
                    return true;
                }
            };

            mockUserOrderService = jasmine.createSpyObj('mockUserOrderService', ['sendNewOrderMessage']);

            mockOrderDialogService = jasmine.createSpyObj('mockOrderDialogService',
                [
                    'hasNeededOrderCreatePrivilegesAsPrincipal',
                    'hasNeededOrderCreatePrivilegesAsBroker'
                ]);

            provide.value('productConfigurationService', mockProductConfigurationService);

            thisController = $controller('newOrderController',
                {
                    $scope: $scope,
                    productConfigurationService: mockProductConfigurationService,
                    $modalInstance: mockModalInstance,
                    userOrderService: mockUserOrderService,
                    orderType: 'bid',
                    productId: 1,
                    userService: mockUserService,
                    tenorId: 1,
                    userApi: mockUserApi,
                    executionMode: "Internal",
                    orderDialogService: mockOrderDialogService
                });
        }));

        it( 'should be defined', inject( function() {
            expect( thisController ).toBeDefined();
        }));

        it('should get the product config', inject(function(){
            var prodConfig = mockProductConfigurationService.getProductConfigurationById(1);
            expect(prodConfig).not.toBe(null);
        }));

        it('should set order type to the passed in order type', inject(function(){
            expect($scope.orderType).toEqual('bid');
        }));

        it('should set the order type on setOrderType', inject(function(){
            $scope.setOrderType('offer');
            expect($scope.orderType).toEqual('offer');
        }));

        it('exit should call close on modal instance', inject(function(){
            spyOn(mockModalInstance, 'dismiss');
            $scope.exit();
            expect(mockModalInstance.dismiss).toHaveBeenCalled();
        }));

        it('should not allow prices less than the min price', function(){
            var mockFormPrice = {$setValidity:function(){}};
            $scope.order.price = 0.1;
            $scope.priceSpecification.isValid(mockFormPrice);
            expect($scope.invalidPriceReason).toEqual("Price must be at least 1")
        });

        it('should not allow prices more than the max price', function(){
            var mockFormPrice = {$setValidity:function(){}};
            $scope.order.price = 99999;
            $scope.priceSpecification.isValid(mockFormPrice);
            expect($scope.invalidPriceReason).toEqual("Price must not exceed 9999")
        });

        it('should  allow prices between the min and max price', function(){
            var mockFormPrice = {$setValidity:function(){}};
            $scope.order.price = 9998;
            $scope.priceSpecification.isValid(mockFormPrice);
            expect($scope.invalidPriceReason).toEqual("")
        });

        it('should not allow prices that are not multiples of the price increment', function(){
            var mockFormPrice = {$setValidity:function(){}};
            $scope.order.price = 1.05;
            $scope.priceSpecification.isValid(mockFormPrice);
            expect($scope.invalidPriceReason).toEqual("Price must be in increments of 0.1");
        });

        it('should not allow quantities less than the min quantity', function(){
            var mockFormQuantity = {$setValidity:function(){}};
            $scope.order.quantity = 0.1;
            $scope.quantitySpecification.isValid($scope.order.quantity,mockFormQuantity);
            expect($scope.invalidQuantityReason).toEqual("Quantity must be at least 12500")
        });

        it('should not allow quantities more than the max quantity', function(){
            var mockFormQuantity = {$setValidity:function(){}};
            $scope.order.quantity = 35001;
            $scope.quantitySpecification.isValid($scope.order.quantity,mockFormQuantity);
            expect($scope.invalidQuantityReason).toEqual("Quantity must not exceed 35000")
        });

        it('should not allow quantities at the incorrect increment', function(){
            var mockFormQuantity = {$setValidity:function(){}};
            $scope.order.quantity = 12501;
            $scope.quantitySpecification.isValid($scope.order.quantity,mockFormQuantity);
            expect($scope.invalidQuantityReason).toEqual("Quantity must be in lots of 500")
        });

        it('should  allow quantities between the min and max price at the correct increment', function(){
            var mockFormQuantity = {$setValidity:function(){}};
            $scope.order.quantity = 12500;
            $scope.quantitySpecification.isValid($scope.order.quantity,mockFormQuantity);
            expect($scope.invalidQuantityReason).toEqual("")
        });

        it('should set characters left as 0 when notes field too large',function(){
            $scope.notesCharacterLimit = 5;
            $scope.order.notes = "123456789";

            $scope.onNotesChange();

            expect($scope.notesCharactersLeft).toBe(0);
        });

        it('should set the order quantity on set quantity called', function(){
            $scope.setQuantity(12500);
            expect($scope.order.quantity).toBe(12500);
        });

        it('should restrict order notes field to max characters allowed',function() {
            $scope.notesCharacterLimit = 5;
            $scope.order.notes = "123456789";

            $scope.onNotesChange();

            expect($scope.order.notes.length).toBe($scope.notesCharacterLimit);
        });

        it('should ask the userApi for buy brokers when adding a Bid as a trader', function() {
            spyOn(mockUserApi, 'getBrokers').and.callThrough();
            $scope.setOrderType("Bid");
            $scope.$digest();
            expect(mockUserApi.getBrokers).toHaveBeenCalledWith(1, true, "buy");
        });

        it('should ask the userApi for sell brokers when adding an Ask as a trader', function() {
            spyOn(mockUserApi, 'getBrokers').and.callThrough();
            $scope.setOrderType("Ask");
            $scope.$digest();
            expect(mockUserApi.getBrokers).toHaveBeenCalledWith(1, true, "sell");
        });

        it('should ask the userApi for buy principals when adding a Bid as a broker', function() {
            spyOn(mockUserApi, 'getPrincipals').and.callThrough();
            spyOn(mockUserService, 'isBroker').and.returnValue(true);
            $scope.setOrderType("Bid");
            $scope.$digest();
            expect(mockUserApi.getPrincipals).toHaveBeenCalledWith(1, true, "buy");
        });

        it('should ask the userApi for sell principals when adding an Ask as a broker', function() {
            spyOn(mockUserApi, 'getPrincipals').and.callThrough();
            spyOn(mockUserService, 'isBroker').and.returnValue(true);
            $scope.setOrderType("Ask");
            $scope.$digest();
            expect(mockUserApi.getPrincipals).toHaveBeenCalledWith(1, true, "sell");
        });

        it('should call userOrderService.sendNewOrderMessage with the order when newOrder called', function() {
            expect(mockUserOrderService.sendNewOrderMessage).not.toHaveBeenCalled();
            $scope.newOrder();
            expect(mockUserOrderService.sendNewOrderMessage).toHaveBeenCalled();
        });

        it('should ask for create order permissions when creating a new internal order', function() {
            spyOn(mockUserService, 'hasProductPrivilege');
            $scope.newOrder();
            expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                1, ["Order_Create_Principal", "Order_Create_Any", "Order_Create_Broker"]);
        });

        it('should generate default metadata values', function() {
            var metadataValues = [{
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId : 1,
                itemType: "IntegerEnum"
            }, {
                productMetaDataId: 3,
                displayName: "Delivery Description",
                displayValue: "",
                itemValue: null,
                metaDataListItemId : null,
                itemType: "String"
            }];

            expect($scope.metaDataCollection.length).toBe(2);
            expect($scope.metaDataCollection[0].definition).toEqual(metadataDefinitions[0]);
            expect($scope.metaDataCollection[1].definition).toEqual(metadataDefinitions[1]);
            expect($scope.metaDataCollection[0].metadataitem).toEqual(metadataValues[0]);
            expect($scope.metaDataCollection[1].metadataitem).toEqual(metadataValues[1]);
        });

        it ('should propagate metadata values to order model', function(){
            var metadataValues = [{
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port A",
                itemValue: 100,
                metaDataListItemId : 1,
                itemType: "IntegerEnum"
            }, {
                productMetaDataId: 3,
                displayName: "Delivery Description",
                displayValue: "",
                itemValue: null,
                metaDataListItemId : null,
                itemType: "String"
            }];

            $scope.newOrder();
            expect($scope.order.metaData).toEqual(metadataValues);
        });

        describe('as a trader', function() {
            it('should say valid principal is selected', function() {
                expect($scope.validPrinicipalSelected).toBeTruthy();
            });
            it('should set the principal details on the order', function() {;
                $scope.newOrder();
                var o = $scope.order;
                expect(o.principalUserId).toEqual(mockUserService.getUser().id);
                expect(o.principalOrganisationId).toEqual(mockUserService.getUsersOrganisation().id);
            });
            it('should set the broker restriction to Any if no broker selected', function() {
                $scope.newOrder();
                expect($scope.order.brokerRestriction).toEqual("Any");
            });
            it('should set the broker details on the order if one selected', function() {
                var fakeBroker = {userId:123, id:234, name:"A Broker"};
                $scope.onBrokerChange(fakeBroker);
                $scope.newOrder();
                expect($scope.order.brokerOrganisationId).toEqual(fakeBroker.id);
                expect($scope.order.brokerId).toEqual(fakeBroker.userId);
                expect($scope.order.brokerName).toEqual(fakeBroker.name);
            });
            it('should set the broker id to null on the order if the No Broker option selected', function() {
                var noBrokerOption = {id:-100}; // Magic number for No Broker
                $scope.onBrokerChange(noBrokerOption);
                $scope.newOrder();
                expect($scope.order.brokerOrganisationId).toBeNull();
            });

            describe('with a CoBrokerable Product and selected broker', function() {

                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();
                    thisController = $controller('newOrderController',
                        {
                            $scope: $scope,
                            productConfigurationService: mockBrokerableProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            userOrderService: mockUserOrderService,
                            orderType: 'bid',
                            productId: 1,
                            userService: mockUserService,
                            tenorId: 1,
                            userApi: mockUserApi,
                            executionMode: "Internal",
                            orderDialogService: mockOrderDialogService
                        });
                }));

                it('expect product to be co-brokerable', function () {
                   expect($scope.productConfig.coBrokering).toBeTruthy();
                });
                it('co-brokerable option is visible', function () {
                   expect($scope.coBrokeringVisible).toBeTruthy();
                });

                it('co-brokerable option is disabled', function () {
                   expect($scope.coBrokeringDisabled).toBeTruthy();
                });
                it('should set cobrokering as false for trading org regardless of product being co-brokerable', function() {
                   $scope.newOrder();
                   var o = $scope.order;
                   expect(o.coBrokering).toBeFalsy();
                });
                it('should not set co-brokering as true when "no broker" option is selected', function(){
                   $scope.$apply(function() {
                        $scope.selectedBroker = { id:-100, name:"No Broker" };
                   });
                   expect($scope.coBrokering).toBeFalsy();
                });
                it('should set co-brokering as true when particular broker is selected', function(){
                    $scope.$apply(function() {
                        $scope.selectedBroker = { id:5, name:"Broker 1" };
                    });
                    expect($scope.coBrokering).toBeTruthy();
                });
                it('should not set co-brokering as true when second broker is selected', function(){
                   $scope.$apply(function() {
                        $scope.selectedBroker = { id:5, name:"Broker 1" };
                        $scope.onBrokerChange({ id:5, name:"Broker 1" });
                   });
                   expect($scope.coBrokering).toBeTruthy();
                   $scope.$apply(function() {
                        $scope.coBrokering = false;
                   });
                   expect($scope.coBrokering).toBeFalsy();
                   $scope.$apply(function() {
                        $scope.selectedBroker = { id:6, name:"Broker 2" };
                        $scope.onBrokerChange({ id:6, name:"Broker 2" });
                   });
                   expect($scope.coBrokering).toBeFalsy();
                });
            });
        });

        describe('as a broker', function() {
            beforeEach(inject( function( $controller, $rootScope ) {
                $scope = $rootScope.$new();
                mockUserService.isBroker = function() { return true; };
                thisController = $controller('newOrderController',
                    {
                        $scope: $scope,
                        productConfigurationService: mockProductConfigurationService,
                        $modalInstance: mockModalInstance,
                        userOrderService: mockUserOrderService,
                        orderType: 'bid',
                        productId: 1,
                        userService: mockUserService,
                        tenorId: 1,
                        userApi: mockUserApi,
                        executionMode: "Internal",
                        orderDialogService: mockOrderDialogService
                    });
                $scope.$digest();
            }));
            it('should not say valid principal selected if one is not selected', function() {
                expect($scope.validPrinicipalSelected).toBeFalsy();
            });
            it('should say valid principal selected if one is selected', function() {
                var fakePrincipal = {userId:123, id:234};
                $scope.onPrincipalChange(fakePrincipal);
                expect($scope.validPrinicipalSelected).toBeTruthy();
            });
            it('should set the principal and broker details on the order', function() {
                var fakePrincipal = {userId:123, id:234};
                $scope.onPrincipalChange(fakePrincipal);
                $scope.newOrder();
                var o = $scope.order;
                var org = mockUserService.getUsersOrganisation();
                var user = mockUserService.getUser();
                expect(o.principalUserId).toEqual(fakePrincipal.userId);
                expect(o.principalOrganisationId).toEqual(fakePrincipal.id);
                expect(o.brokerOrganisationId).toEqual(org.id);
                expect(o.brokerOrganisationName).toEqual(org.name);
                expect(o.brokerId).toEqual(user.id);
                expect(o.brokerName).toEqual(user.userOrganisation.name);
            });
            it('expect product to not be co-brokerable', function () {
                expect($scope.productConfig.coBrokering).toBeFalsy()
            });
            it('co-brokerable option is hidden when product is not co-brokerable', function () {
                expect($scope.coBrokeringVisible).toBeFalsy();
            });
            it('should set co-brokerable as false when product is not co-brokerable', function() {
                $scope.newOrder();
                var o = $scope.order;
                expect(o.coBrokering).toBeFalsy();
            });

            describe('with a CoBrokerable Product', function() {
                beforeEach(inject(function ($controller, $rootScope) {
                    $scope = $rootScope.$new();
                    thisController = $controller('newOrderController',
                        {
                            $scope: $scope,
                            productConfigurationService: mockBrokerableProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            userOrderService: mockUserOrderService,
                            orderType: 'bid',
                            productId: 1,
                            userService: mockUserService,
                            tenorId: 1,
                            userApi: mockUserApi,
                            executionMode: "Internal",
                            orderDialogService: mockOrderDialogService
                        });
                    $scope.$digest();
                }));
                it('expect product to be co-brokerable', function () {
                    expect($scope.productConfig.coBrokering).toBeTruthy()
                });
                it('co-brokerable option is visible', function () {
                    expect($scope.coBrokeringVisible).toBeTruthy();
                });
                it('co-brokerable option is enabled', function () {
                    expect($scope.coBrokeringDisabled).toBeFalsy();
                });
                it('order defaults to co-brokerable', function () {
                    $scope.newOrder();
                    var o = $scope.order;
                    expect(o.coBrokering).toBeTruthy();
                });
            })
        });

        describe('external orders', function() {
            describe('as a trader', function() {
                beforeEach(inject( function( $controller, $rootScope ) {
                    $scope = $rootScope.$new();
                    thisController = $controller('newOrderController',
                        {
                            $scope: $scope,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            userOrderService: mockUserOrderService,
                            orderType: 'bid',
                            productId: 1,
                            userService: mockUserService,
                            tenorId: 1,
                            userApi: mockUserApi,
                            executionMode: "External",
                            orderDialogService: mockOrderDialogService
                        });
                }));
                it('should think user is not a broker', function() {
                    expect($scope.IsBroker()).toBeFalsy();
                });
                it('should ask for create external order permission when creating a new order', function() {
                    spyOn(mockUserService, 'hasProductPrivilege');
                    $scope.newOrder();
                    expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                        1, ['ExternalOrder_Create_Principal', 'ExternalOrder_Create_Broker']);
                });
                it('should set the executionMode on the order', function() {
                    $scope.newOrder();
                    expect($scope.order.executionMode).toBe("External");
                });
                it('should set validPrinicipalSelected to true when opened', function() {
                    expect($scope.validPrinicipalSelected).toBeTruthy();
                });
                it('should leave validPrinicipalSelected as true on newOrder', function() {
                    $scope.newOrder();
                    expect($scope.validPrinicipalSelected).toBeTruthy();
                });
            });
            describe('as a broker', function() {
                beforeEach(inject( function( $controller, $rootScope ) {
                    $scope = $rootScope.$new();
                    mockUserService.isBroker = function() { return true; };
                    thisController = $controller('newOrderController',
                        {
                            $scope: $scope,
                            productConfigurationService: mockProductConfigurationService,
                            $modalInstance: mockModalInstance,
                            userOrderService: mockUserOrderService,
                            orderType: 'bid',
                            productId: 1,
                            userService: mockUserService,
                            tenorId: 1,
                            userApi: mockUserApi,
                            executionMode: "External",
                            orderDialogService: mockOrderDialogService
                        });
                    $scope.$digest();
                }));
                it('should think user is a broker', function() {
                    expect($scope.IsBroker()).toBeTruthy();
                });
                it('should ask for create external order permission when creating a new order', function() {
                    spyOn(mockUserService, 'hasProductPrivilege');
                    $scope.newOrder();
                    expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                        1, ['ExternalOrder_Create_Principal', 'ExternalOrder_Create_Broker']);
                });
                it('should set the executionMode on the order', function() {
                    $scope.newOrder();
                    expect($scope.order.executionMode).toBe("External");
                });
                it('should set validPrinicipalSelected to true when opened', function() {
                    expect($scope.validPrinicipalSelected).toBeTruthy();
                });
                it('should leave validPrinicipalSelected as true on newOrder', function() {
                    $scope.newOrder();
                    expect($scope.validPrinicipalSelected).toBeTruthy();
                });
            });
        });

    });
});
