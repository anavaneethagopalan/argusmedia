﻿angular.module('argusOpenMarkets.dashboard')
    .controller('viewOrderController', function viewOrderController(
        $scope,
        userOrderService,
        productConfigurationService,
        tenorId,
        $modalInstance,
        userService,
        $state,
        order,
        orderType,
        productId,
        productMetadataService)
    {
        "use strict";

        $scope.productLabels = productConfigurationService.getProductConfigurationLabels(productId);
        $scope.orderType = orderType;

        $scope.setOrderType = function(orderType){
            $scope.orderType = orderType;
        };

        $scope.order = angular.copy(order);

        $scope.order.metaData = productMetadataService.synchronizeMetadata($scope.order.metaData, productId);
        $scope.metaDataCollection = productMetadataService.getMetadataCollection($scope.order.metaData, productId);

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.productConfig = angular.copy(productConfigurationService.getProductConfigurationById(productId));
        angular.forEach($scope.productConfig.contractSpecification.tenors, function(tenor){
            if(tenor.id === tenorId){
                $scope.tenor = tenor;
            }
        });

        var startDateOffset = parseInt($scope.tenor.deliveryStartDate);
        var today = new Date();
        $scope.minDate = today.addDays(startDateOffset);
        var maxdate = new Date();
        $scope.maxDate = maxdate.addDays(parseInt($scope.tenor.deliveryEndDate));
        var startDay = new Date(moment($scope.order.deliveryStartDate));
        $scope.order.deliveryStartDate = startDay;
        var endDay = new Date(moment($scope.order.deliveryEndDate));
        $scope.order.deliveryEndDate = endDay;
        $scope.minRange = $scope.tenor.minimumDeliveryRange;
    });