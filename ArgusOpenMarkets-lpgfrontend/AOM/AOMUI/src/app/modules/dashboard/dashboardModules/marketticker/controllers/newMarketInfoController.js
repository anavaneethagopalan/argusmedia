﻿angular.module('argusOpenMarkets.dashboard')
    .controller('newMarketInfoController', function newMarketInfoController(
            $scope,
            helpers,
            $modalInstance,
            marketInfoService ,
            $state,
            productConfigurationService){
        "use strict";
        $scope.$on('$stateChangeSuccess', function () {
            if ($state.current.name !== 'Dashboard') {
                $modalInstance.dismiss('cancel');
            }
        });

        $scope.infoValid = false;

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.info ={
            marketInfoText :""
        };

        $scope.canCreateVerified = function(){
             return marketInfoService.canCreateVerified();
        };

        var products = angular.copy(productConfigurationService.getAllProductConfigurations());

        angular.forEach(products, function(product){
            product.ticked = true;
        });

        $scope.marketTickerInfoProducts = products;

        $scope.characterLimit = 500;
        $scope.charactersLeft = $scope.characterLimit;

        $scope.infoChanged = function(){
            var processedText =
                marketInfoService.processMarketInfoText($scope.info.marketInfoText, $scope.characterLimit);

            $scope.info.marketInfoText = processedText.text;
            $scope.charactersLeft = processedText.charactersLeft;
            $scope.infoValid = processedText.infoValid;
            if($scope.infoValid) {
                var selectedProducts = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
                if (selectedProducts.length === 0) {
                    $scope.infoValid = false;
                }
            }
        };

        $scope.newInfo = function(status) {
            var tickedProductIds = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            var info = {
                marketInfoStatus:status,
                info: $scope.info.marketInfoText,
                productIds: tickedProductIds
            };
            marketInfoService.sendNewMarketInfoMessage(info);
            $modalInstance.dismiss('submitted');
        };
    });