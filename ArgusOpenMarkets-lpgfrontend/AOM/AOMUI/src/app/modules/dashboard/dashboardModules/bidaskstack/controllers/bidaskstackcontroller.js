﻿angular.module('argusOpenMarkets.dashboard.bidaskstack')
    .controller('bidAskStackController', function bidAskStackController(
        $scope,
        $modal,
        $templateCache,
        userService,
        orderRepositoryService,
        helpers,
        BidAskStack,
        $timeout,
        dashboardConfigurationService,
        productConfigurationService //Note: this is required for subscription to product config to track changes
    )
    {

        $scope.bidAskStackInitialised = false;
        $scope.openDialog = false;
        $scope.GRID_ROW_HEIGHT = 28;
        $scope.GRID_HEADER_HEIGHT = 28;
        $scope.GRID_FOOTER_HEIGHT = 14;
        $scope.bidAskStack = new BidAskStack();


        var getGridRowCount = function(){
            return dashboardConfigurationService.determineNumberBidsInStack();
        };

        var setBidAskStackSize = function(gridRowCount) {

            var newHeightFooter = 0,
                newHeight = 0;

            if(typeof(gridRowCount) === "undefined" || gridRowCount < 3){
                gridRowCount = getGridRowCount();
            }

            $scope.gridRowCount = gridRowCount;

            newHeight = $scope.gridRowCount * $scope.GRID_ROW_HEIGHT;
            newHeightFooter = newHeight + $scope.GRID_FOOTER_HEIGHT + $scope.GRID_HEADER_HEIGHT + 2;

            if($scope.bidGridOptions.ngGrid.$viewport) {
                $scope.renderAsksGrid();
                $scope.renderBidsGrid();
                $scope.bidGridOptions.ngGrid.$viewport.css('height', newHeight + 'px');
                $scope.askGridOptions.ngGrid.$viewport.css('height', newHeight + 'px');
                $scope.bidGridOptions.ngGrid.$root.css('height', newHeightFooter + 'px');
                $scope.askGridOptions.ngGrid.$root.css('height', newHeightFooter + 'px');
            }
        };

        $scope.$on('ngGridEventData', function(event) {

            var gridRowCount;

            if(!$scope.bidAskStackInitialised) {
                gridRowCount = getGridRowCount();

                setBidAskStackSize(gridRowCount);
                $scope.bidAskStackInitialised = true;
            }
        });

        $scope.onDecreaseBidAskStackSize = function(event,gridRowCount){

            setBidAskStackSize(gridRowCount);
        };
        $scope.$on('decreaseBidAskStackSize', $scope.onDecreaseBidAskStackSize);

        $scope.onIncreaseBidAskStackSize = function(event,gridRowCount){

            setBidAskStackSize(gridRowCount);
        };

        $scope.$on('increaseBidAskStackSize',$scope.onIncreaseBidAskStackSize );
        $scope.upClicked = function(){
            $scope.$emit('decreaseBidAskStackWidgetSizeRequest', $scope.gridRowCount);
        };

        $scope.downClicked = function(){
            $scope.$emit('increaseBidAskStackWidgetSizeRequest', $scope.gridRowCount);
        };
        $scope.getMarketDepthDisplay = function(){
            var marketDepth = $scope.bidAskStack.getMarketDepth(),
                visibleMarketDepth = Math.max(marketDepth-$scope.gridRowCount,0);
            return (visibleMarketDepth === 0) ? '' : visibleMarketDepth;
        };

        $scope.canClickUp = function(){
            return $scope.gridRowCount > 3;
        };
        $scope.canClickDown = function(){
            var hasHiddenMarketDepth = $scope.bidAskStack.getMarketDepth() > $scope.gridRowCount;
            return hasHiddenMarketDepth || $scope.gridRowCount === 3;
        };

        if (helpers.isEmpty($scope.productConfigItems))
        {
            throw "No productConfig attached to scope";
        }

        // TODO: Should we not evaluate all products?
        var productConfig = $scope.productConfigItems[0];
        var productId = productConfig.productId;
        $scope.productId = productId;
        $scope.gridRowCount = productConfig.bidAskStackNumberRows;

        $scope.IsBroker = userService.isBroker();

        $scope.productTitle =productConfig.productTitle;
        var contractSpecification = productConfig.contractSpecification;
        var productTenor = contractSpecification.tenors[0];
        var principals, brokers;

        $scope.hoverBidAskContent =[
            {title:"Volume Units", content: contractSpecification.volume.volumeUnitCode},
            {title:"Pricing Unit", content: contractSpecification.pricing.pricingCurrencyCode +"/"+ contractSpecification.pricing.pricingUnitCode},
            {title:"Delivery Location", content: productTenor ? (productTenor.deliveryLocation ? productTenor.deliveryLocation.name : "") : ""},
            {title:"Contract Type", content: contractSpecification.contractType},
            {title:"Delivery Start Date", content: productTenor ? productTenor.deliveryStartDate : ""},
            {title:"Delivery End Date", content: productTenor ? productTenor.deliveryEndDate : ""},
            {title:"Roll Date", content: productTenor ? productTenor.rollDate: ""}];

        $scope.bids = [];
        $scope.asks = [];

        var setDisplayNames = function(orderMessage) {
            var brokerShortCode = "",
                brokerOrganisationName = "",
                principalShortCode = "",
                principalOrganisationName = "";

            if(orderMessage && orderMessage.order){
                brokerShortCode = orderMessage.order.brokerShortCode;
                brokerOrganisationName = orderMessage.order.brokerOrganisationName;
                principalShortCode = orderMessage.order.principalOrganisationShortCode;
                principalOrganisationName = orderMessage.order.principalOrganisationName;
                orderMessage.order.brokerDisplayName =
                    $scope.getBrokerDisplayName(brokerShortCode, brokerOrganisationName);
                orderMessage.order.principalDisplayName =
                    $scope.getPrincipalDisplayName(principalShortCode, principalOrganisationName);
            }
        };

        var onAskEventHandler = function(orderMessage){
            if(orderMessage.order.productId === $scope.productId) {
                setDisplayNames(orderMessage);
                $scope.bidAskStack.onAskMessageEventHandler(orderMessage);
                $scope.renderAsksGrid();

                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        };

        $scope.renderAsksGrid = function(){
            var asks = $scope.bidAskStack.asks.slice(0, $scope.gridRowCount);
            if ($scope.bidAskStack.asks.length < $scope.gridRowCount){
                var rowFillCount = $scope.gridRowCount - $scope.bidAskStack.asks.length;
                for (var i = 0; i < rowFillCount; i++){
                    asks.push(BidAskStack.createEmptyAsk());
                }
            }
            $scope.asks = asks;
        };

        var onBidEventHandler = function(orderMessage){
            if(orderMessage.order.productId === $scope.productId) {
                setDisplayNames(orderMessage);
                $scope.bidAskStack.onBidMessageEventHandler(orderMessage);
                $scope.renderBidsGrid();

                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        };

        $scope.renderBidsGrid = function(){
            var bids = $scope.bidAskStack.bids.slice(0, $scope.gridRowCount);
            if ($scope.bidAskStack.bids.length < $scope.gridRowCount){
                var rowFillCount = $scope.gridRowCount - $scope.bidAskStack.bids.length;
                for (var i = 0; i < rowFillCount; i++){
                    bids.push(BidAskStack.createEmptyBid());
                }
            }

            $scope.bids = bids;
        };

        $scope.getBrokerDisplayName = function(brokerShortCode, brokerOrganisationName) {
            switch (brokerShortCode) {
                case null:case "":
                    return "No Broker";
                case "*":
                    return "Any Broker";
                default:
                    return helpers.getOrganisationNameAndCode({name:brokerOrganisationName, shortCode:brokerShortCode});
            }
        };

        $scope.getPrincipalDisplayName = function(shortCode, organisationName) {
            return helpers.getOrganisationNameAndCode({name: organisationName, shortCode:shortCode});
        };

        var onOrderDeletedEventHandler = function(orderKey){
            var result = $scope.bidAskStack.deleteOrderIfExists(
                orderKey,
                function(){
                    $scope.renderBidsGrid();

                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                },
                function() {
                    $scope.renderAsksGrid();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                });
        };

        var subscribeToOrderUpdates = function() {
            var subscriberId = "Bid Ask Stack Controller " + productId;
            orderRepositoryService.subscribeToOrderUpdates(
                subscriberId,
                productId,
                {"onBid": onBidEventHandler, "onAsk": onAskEventHandler, "onDelete": onOrderDeletedEventHandler},
                "Internal");

            $scope.$on('$destroy', function() {
                orderRepositoryService.unsubscribeFromOrderUpdates(subscriberId);
            });
        };

        var refreshBidAskStack = function() {
            angular.forEach($scope.asks, function (ask) {
                ask.refreshOnPermissionChange();
            });
            angular.forEach($scope.bids, function (bid) {
                bid.refreshOnPermissionChange();
            });
        };

        $scope.$on('ProductConfigurationService-Change', function(ev, args){
            if(args.productId===$scope.productId) {
                refreshBidAskStack();
            }
        });

        $scope.$on('TradePermissionsService-Change', function(){
            refreshBidAskStack();
        });

        $scope.canCreateOrder = userService.hasProductPrivilege(productId, ["Order_Create_Any","Order_Create_Broker","Order_Create_Principal"]);

        $scope.$watch('openDialog', function(){

            if($scope.openDialog === false){
            $('#ui-datepicker-div').hide();
            }
        });

        $scope.newBidAsk = function(type){
            if($scope.canCreateOrder && $scope.openDialog === false) {
                $scope.openDialog = true;
                var modalInstance = $modal.open({
                        templateUrl: 'src/app/modules/dashboard/dashboardModules/order/views/newOrder.part.html',
                        controller: 'newOrderController',
                        windowClass: 'orderModal',
                        backdrop: 'static',
                        resolve: {
                            productId: function () {
                                return productId;
                            },
                            orderType: function () {
                                return type;
                            },
                            tenorId:function(){
                                if(productTenor) {
                                    return productTenor.id;
                                }

                                return 0;
                            },
                            executionMode: function() {
                                return "Internal";
                            }
                        }}
                );
                modalInstance.result.then(function(reason){},
                    function(reason){
                        $scope.openDialog = false;
                    });
            }
        };

        $scope.bidAskRowClicked = function(priceRequest, type){
            if (priceRequest.orderIsMine){
                return $scope.editBidAsk(priceRequest, type);
            }
            else if($scope.canExecuteBidAsk()){
                // kanban-51 - Slight hack - pass through the nonTradeable flag through to the Dialogue for Execute Bid.  If Non-Tradeable
                // The Execute bid will always have the Execute Button Disabled.
                priceRequest.order.nonTradeable = priceRequest.nonTradeable;
                return $scope.executeBidAsk(priceRequest, type);
            }
            else{
                //otherwise readonly
                return $scope.viewBidAsk(priceRequest,type);
            }
        };

        $scope.canExecuteBidAsk = function(){
            return userService.hasProductPrivilege(productId, ["Order_Aggress_Principal","Order_Aggress_Broker","Order_Aggress_Principal_OwnOrg"]);
        };

        $scope.viewBidAsk = function(priceRequest,type){
            if($scope.showBidAsk(priceRequest) && $scope.openDialog === false) {
                $scope.openDialog = true;
                var modalInstance = $modal.open({
                        templateUrl: 'src/app/modules/dashboard/dashboardModules/order/views/viewOrder.part.html',
                        controller: 'viewOrderController',
                        windowClass: 'orderModal',
                        backdrop: 'static',
                        resolve: {
                            productId: function () {
                                return productId;
                            },
                            order: function () {
                                return priceRequest.order;
                            },
                            orderType: function () {
                                return type;
                            },
                            tenorId: function () {
                                if(productTenor) {
                                    return productTenor.id;
                                }
                                return 0;
                            }
                        }
                });

                modalInstance.result.then(function(reason){},
                    function(reason){
                        $scope.openDialog = false;
                });


            }
        };

        $scope.executeBidAsk = function(priceRequest,type){
            if($scope.canExecuteBidAsk()) {
                if($scope.showBidAsk(priceRequest) && $scope.openDialog === false) {
                    $scope.openDialog = true;
                    var modalInstance = $modal.open({
                            templateUrl: 'src/app/modules/dashboard/dashboardModules/order/views/executeOrder.part.html',
                            controller: 'executeOrderController',
                            windowClass: 'orderModal',
                            backdrop: 'static',
                            resolve: {
                                productId: function () {
                                    return productId;
                                },
                                order: function () {
                                    return priceRequest.order;
                                },
                                orderType: function () {
                                    return type;
                                }
                            }
                        }
                    );
                    modalInstance.result.then(
                        function (reason) {},
                        function (reason) {
                            $scope.openDialog = false;
                            if (reason === "edit"){
                                $timeout(function(){$scope.editBidAsk(priceRequest,type);},500);
                            }
                        }
                    );
                }
            }
        };

        $scope.editBidAsk = function(priceRequest,type){
            if(userService.hasProductPrivilege(productId, ["Order_Amend_Own","Order_Amend_Any","Order_Amend_OwnOrg"])) {
                if ($scope.showBidAsk(priceRequest) && $scope.openDialog === false) {
                    $scope.openDialog = true;
                    var modalInstance = $modal.open({
                            templateUrl: 'src/app/modules/dashboard/dashboardModules/order/views/editOrder.part.html',
                            controller: 'editOrderController',
                            windowClass: 'orderModal',
                            backdrop: 'static',
                            resolve: {
                                productId: function () {
                                    return productId;
                                },
                                order: function () {
                                    return priceRequest.order;
                                },
                                orderType: function () {
                                    return type;
                                },
                                tenorId: function () {

                                    if(productTenor) {
                                        return productTenor.id;
                                    }
                                    return 0;
                                }
                            }}
                    );
                    modalInstance.result.then(
                        function (reason) {},
                        function (reason) {
                            $scope.openDialog = false;
                            if (reason === "execute"){
                                $timeout(function(){$scope.executeBidAsk(priceRequest,type);},500);
                            }
                        }
                    );
                }
            }
        };

        $scope.contractTitleRowStyle='marketContractHoverHeader';
        $scope.contractContentStyle = {"background-color":"#424A51"};

        //setup initial empty bid/ask stack grid
        for (var i = 0; i < $scope.gridRowCount; i++){
            $scope.bids.push(BidAskStack.createEmptyBid());
            $scope.asks.push(BidAskStack.createEmptyAsk());
        }

        $scope.bidGridOptions = {
            data: "bids",
            enableColumnReordering: false,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResize: false,
            jqueryUIDraggable: true,
            enableSorting: false,
            headerRowHeight: $scope.GRID_HEADER_HEIGHT,
            rowHeight: $scope.GRID_ROW_HEIGHT,
            virtualizationThreshold: 100,
            headerRowTemplate: $templateCache.get('headerBidsRowTemplate.html'),
            rowTemplate: $templateCache.get('rowBidsTemplate.html'),
            columnDefs : [
                {field:'notesPresent',displayName:'',width:'16px',cellTemplate: $templateCache.get('cellNotesIconTemplate.html')},
                {field:'orderShortDeliveryPeriod',displayName:'DATE',width:'40px'},
                {field:'order.brokerShortCode',displayName:'BRO',width:'32px'},
                {field:'order.principalOrganisationShortCode',displayName:'PRI',width:'32px'},
                {field:'order.quantity',displayName:'QTY',width:'56px',cellTemplate: $templateCache.get('cellBidsQuantityTemplate.html')},
                {field:'order.price',displayName:'',width:'71px',cellTemplate: $templateCache.get('cellBidsPriceTemplate.html')}
            ],
            showFooter: true,
            footerRowHeight: $scope.GRID_FOOTER_HEIGHT,
            footerTemplate: $templateCache.get('bidsFooterTemplate.html')
        };
        $scope.bidTitleRowStyle={"background-color":"#061C25"};
        $scope.bidContentStyle = {"background-color":"#162F3A"};


        $scope.askGridOptions = {
            data: "asks",
            enableColumnReordering: false,
            enableColumnResize: false,
            jqueryUIDraggable: true,
            multiSelect:false,
            enableSorting: false,
            headerRowHeight: $scope.GRID_HEADER_HEIGHT,
            rowHeight: $scope.GRID_ROW_HEIGHT,
            virtualizationThreshold: 100,
            headerRowTemplate: $templateCache.get('headerAsksRowTemplate.html'),
            rowTemplate: $templateCache.get('rowAsksTemplate.html'),
            columnDefs : [
                {field:'order.price',displayName:'',width:'71px',cellTemplate: $templateCache.get('cellAsksPriceTemplate.html')},
                {field:'order.quantity',displayName:'QTY',width:'56px',cellTemplate: $templateCache.get('cellAsksQuantityTemplate.html')},
                {field:'order.principalOrganisationShortCode',displayName:'PRI',width:'32px'},
                {field:'order.brokerShortCode',displayName:'BRO',width:'32px'},
                {field:'orderShortDeliveryPeriod',displayName:'DATE',width:'40px'},
                {field:'notesPresent',displayName:'',width:'16px',cellTemplate: $templateCache.get('cellNotesIconTemplate.html')}
            ],
            showFooter: true,
            footerRowHeight: $scope.GRID_FOOTER_HEIGHT,
            footerTemplate: $templateCache.get('asksFooterTemplate.html')
        };
        $scope.askTitleRowStyle={"background-color":"#1E0504"};
        $scope.askContentStyle = {"background-color":"#311B1C"};

        $scope.showBidAsk = function(priceRequest){
            if(!helpers.isEmpty(priceRequest.order)) {
                if (!helpers.isEmpty(priceRequest.order.price)) {
                    return true;
                }
            }
            return false;
        };

        $scope.getGridHeight = function(){
            return {
                height: ($scope.GRID_HEADER_HEIGHT + ($scope.gridRowCount * $scope.GRID_ROW_HEIGHT) + $scope.GRID_FOOTER_HEIGHT + 2) + "px"
            };
        };

        subscribeToOrderUpdates();
    });

