﻿angular.module('argusOpenMarkets.dashboard')
    .controller('newExternalDealController',
        function newExternalDealController(
            $scope,
            $modalInstance,
            userApi,
            $state,
            userService,
            dealService,
            productConfigurationService,
            $filter,
            helpers,
            productMetadataService,
            dateService)
        {
            "use strict";
            $scope.canViewFFNotes = null;
            var loggedOnUser;

            $scope.viewValue = "";
            $scope.brokers =[];
            $scope.principals =[];
            $scope.$on('$stateChangeSuccess', function () {
                if ($state.current.name !== 'Dashboard') {
                    $modalInstance.dismiss('cancel');
                }
            });

            var metadataPool = {};
            var updateDealMetadata = function(){
                $scope.deal.metaData = productMetadataService.getMetadataValuesFromPool(metadataPool, $scope.deal.productId);
                $scope.metaDataCollection = productMetadataService.getMetadataCollection($scope.deal.metaData, $scope.deal.productId);
            };

            $scope.itemdisabled = function(){
                return false;
            };

            $scope.exit = function () {
                $modalInstance.dismiss('exit');
            };

            $scope.canSeeFreeFormDeal = function () {
                var canSee = false;

                if (!loggedOnUser) {
                    loggedOnUser = userService.getUser();
                }

                switch (loggedOnUser.userOrganisation.organisationType) {
                    case "Brokerage":
                        canSee = false;
                        break;
                    case "Trading":
                        canSee = false;
                        break;
                    case "Argus":
                        canSee = true;
                        break;
                }

                return canSee;
            };


            $scope.canCreateVerified = function () {
                if (!$scope.selectedContract) {
                    return false;
                }
                var createVerifiedDealPermissions = ["Deal_Authenticate"];
                return userService.hasProductPrivilege($scope.selectedContract.productId, createVerifiedDealPermissions);
            };

            $scope.canViewFreeFormNotes = function() {
                if($scope.canViewFFNotes === null) {
                    var createVerifiedDealPermissions = ["Deal_Authenticate"];
                    $scope.canViewFFNotes = userService.hasProductPrivilegeOnAnyProduct(createVerifiedDealPermissions);
                }

                return $scope.canViewFFNotes;
            };


            $scope.selectContract = function (contract, price) {
                $scope.quantityLabel = "";
                $scope.priceLabel = "";
                if (contract) {
                    $scope.selectedContract = contract;
                    $scope.deal.productId = contract.productId;
                    $scope.quantityLabel = "(" + $scope.selectedContract.contractSpecification.volume.volumeUnitCode + ")";
                    $scope.priceLabel = "(" + $scope.selectedContract.contractSpecification.pricing.pricingCurrencyCode + "/" + $scope.selectedContract.contractSpecification.pricing.pricingUnitCode + ")";

                    $scope.priceSpecification.isValid(price);
                    $scope.productLabels = productConfigurationService.getProductConfigurationLabels(contract.productId);
                }
                userApi.getPrincipals($scope.selectedContract.productId).then(function(data) {
                    $scope.principals = data;
                });

                userApi.getBrokers($scope.selectedContract.productId).then(function(data) {
                    $scope.brokers = data;
                });

                updateDealMetadata();
                setDefaultDeliveryDateRange();
            };

            $scope.showOptionalPriceDetail = function(){
                if ($scope.selectedContract && $scope.selectedContract.displayOptionalPriceDetail){
                    return true;
                }
                else{
                    return false;
                }
            };

            $scope.priceSpecification = {
                isValid: function (price) {
                    var isValidPrice = true;
                    if (helpers.isEmpty($scope.deal.price)) {
                        isValidPrice = false;
                    } else if ($scope.selectedContract && !$scope.selectedContract.allowNegativePriceForExternalDeals && $scope.deal.price < 0){
                        isValidPrice = false;
                    }
                    price.$setValidity("price", isValidPrice);
                }
            };

            $scope.overrideFreeForm = function () {
                $scope.deal.useCustomFreeFormNotes = !!$scope.deal.freeFormDealMessage;
            };

            $scope.contractNameCharacterLimit = 100;
            $scope.contractNameCharactersLeft = $scope.contractNameCharacterLimit;

            $scope.onContractNameChange = function(){
                if ($scope.deal.contractInput) {
                    if ($scope.deal.contractInput.length > $scope.contractNameCharacterLimit){
                        $scope.deal.contractInput = $scope.deal.contractInput.slice(0, $scope.contractNameCharacterLimit);
                    }
                    $scope.contractNameCharactersLeft = $scope.contractNameCharacterLimit - $scope.deal.contractInput.length;
                }
                else {
                    $scope.contractNameCharactersLeft = $scope.contractNameCharacterLimit;
                }
            };

            $scope.notesCharacterLimit = 250;

            $scope.notesCharactersLeft = $scope.notesCharacterLimit;
            $scope.onNotesChange = function(){
                if ($scope.deal.notes.length > $scope.notesCharacterLimit){
                    $scope.deal.notes = $scope.deal.notes.slice(0, $scope.notesCharacterLimit);
                }
                $scope.notesCharactersLeft = $scope.notesCharacterLimit - $scope.deal.notes.length;
            };

            $scope.freeFormNotesCharacterLimit = 250;
            $scope.freeFormNotesCharactersLeft = $scope.freeFormNotesCharacterLimit;
            $scope.onFreeFormNotesChange = function(){
                // $scope.overrideFreeForm();
                if($scope.deal.freeFormDealMessage) {
                    if ($scope.deal.freeFormDealMessage.length > $scope.freeFormNotesCharacterLimit) {
                        $scope.deal.freeFormDealMessage = $scope.deal.freeFormDealMessage.slice(0, $scope.freeFormNotesCharacterLimit);
                    }
                    $scope.freeFormNotesCharactersLeft = $scope.freeFormNotesCharacterLimit - $scope.deal.freeFormDealMessage.length;
                }
            };

            $scope.createMessage = function (form) {
                var valid = !form.$error.required && !form.$invalid;
                if (valid) {
                    if (!$scope.deal.useCustomFreeFormNotes) {
                        var deal = $scope.deal;
                        var freeFormDeal =
                            dealService.createFreeFormNotes(deal, $scope.selectedContract, $filter('date'), $filter('number'));
                        $scope.deal.freeFormDealMessage = freeFormDeal;
                        $scope.onFreeFormNotesChange();
                    }
                }
            };

            $scope.setBroker = function (broker) {
                $scope.deal.brokerId = broker.id;
            };

            $scope.setSeller = function (seller) {
                $scope.deal.sellerId = seller.id;
            };

            $scope.setBuyer = function (buyer) {
                $scope.deal.buyerId = buyer.id;
            };

            var createDealPermissions = ["Deal_Create_Broker", "Deal_Create_Principal", "Deal_Create_All"];
            var productsUserCanCreateDealsFor = userService.getProductsWithPrivileges(createDealPermissions);

            var contracts = [];
            angular.forEach(productsUserCanCreateDealsFor, function (id) {
                contracts.push(productConfigurationService.getProductConfigurationById(id));
            });
            $scope.contracts = contracts;

            $scope.deal = {};
            // var today = new Date();
            // $scope.minDate = today.addDays(1);
            // var maxdate = new Date();
            // $scope.maxDate = maxdate.addDays(30);
            // var startDay = new Date();
            // $scope.deal.deliveryStartDate = startDay.addDays(1);
            // var endDay = new Date();
            // $scope.deal.deliveryEndDate = endDay.addDays(3);
            // $scope.minRange = 3;
            $scope.deal.notes = "";
            $scope.deal.optionalPriceDetail = "";

            $scope.newDeal = function (asVerified) {
                if ($scope.deal.brokerName==='') {
                    $scope.deal.brokerId = null;
                }
                $scope.deal.asVerified = asVerified;

                if ($scope.deal.useCustomFreeFormNotes) {
                    var deal = $scope.deal;
                    var freeFormDeal =
                        dealService.createFreeFormNotes(deal, $scope.selectedContract, $filter('date'), $filter('number'));
                    $scope.deal.freeFormDealMessage = freeFormDeal;
                }

                dealService.sendNewExternalDealMessage($scope.deal);
                $modalInstance.dismiss('submitted');
            };

            if ($scope.contracts.length > 0) {
                $scope.selectedContract = $scope.contracts[0];
                $scope.productLabels = productConfigurationService.getProductConfigurationLabels($scope.contracts[0].productId);
                $scope.deal.productId = $scope.selectedContract.productId;
                $scope.quantityLabel = "(" + $scope.selectedContract.contractSpecification.volume.volumeUnitCode + ")";
                $scope.priceLabel = "(" + $scope.selectedContract.contractSpecification.pricing.pricingCurrencyCode + "/" + $scope.selectedContract.contractSpecification.pricing.pricingUnitCode + ")";
                userApi.getPrincipals($scope.selectedContract.productId).then(function (data) {
                    $scope.principals = data;
                });
                userApi.getBrokers($scope.selectedContract.productId).then(function (data) {
                    $scope.brokers = data;
                });

                updateDealMetadata();
            }else {
                $scope.productLabels = productConfigurationService.getProductConfigurationLabels(-1);
            }

            var setDefaultDeliveryDateRange = function() {
                if ($scope.selectedContract && $scope.selectedContract.contractSpecification.tenors.length > 0) {
                    var tenor = $scope.selectedContract.contractSpecification.tenors[0];
                    var startDateOffset = parseInt(tenor.deliveryStartDate);
                    var endDateOffset = parseInt(tenor.deliveryEndDate);
                    var defaultStartDate = parseInt(tenor.defaultStartDate || tenor.deliveryStartDate);
                    var defaultEndDate = parseInt(tenor.defaultEndDate || tenor.deliveryEndDate);

                    $scope.minDate = dateService.getDateTimeNow().addDays(startDateOffset);
                    $scope.maxDate = dateService.getDateTimeNow().addDays(endDateOffset);
                    $scope.minRange = tenor.minimumDeliveryRange;
                    $scope.deal.deliveryStartDate = dateService.getDateTimeNow().addDays(Math.max(startDateOffset, defaultStartDate));
                    $scope.deal.deliveryEndDate = dateService.getDateTimeNow().addDays(Math.min(endDateOffset, defaultEndDate));
                } else {
                    $scope.minDate = dateService.getDateTimeNow().addDays(1);
                    $scope.maxDate = dateService.getDateTimeNow().addDays(30);
                    $scope.minRange = 3;
                    $scope.deal.deliveryStartDate = dateService.getDateTimeNow().addDays(1);
                    $scope.deal.deliveryEndDate = dateService.getDateTimeNow().addDays(3);
                }
            };

            setDefaultDeliveryDateRange();
        });