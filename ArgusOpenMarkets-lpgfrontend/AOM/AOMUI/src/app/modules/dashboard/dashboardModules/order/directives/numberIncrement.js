angular.module('argusOpenMarkets.dashboard').directive('numberIncrement', function (productValidationService) {
    return {
        scope: {
            nivalue: '=',
            nistep: '@',
            niminimum: '@',
            nimaximum: '@',
            nidecimalplaces: '@',
            itemdisabled: '&'
        },
        templateUrl: 'src/app/modules/dashboard/dashboardModules/order/directives/numberIncrement.part.html',
        // require: 'ngModel',
        replace: true,
        transclude: true,
        restrict: 'AE',
        link: function (scope, elem, attrs) {
            // var inputElem = elem.find('input');
            // .attr('msd-wheel', 'scroll($event, delta, deltax, deltay)');
            //elem.attachEvent("onmousewheel", scroll);

            elem.bind('wheel', function($event, delta, deltax, deltay){
                scope.mouseWheel($event);
            });
        },
        controller: function($scope){
            var applyScope = function(){
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            };

            $scope.spinnaClass = function(){
                if(isNaN(this.nivalue)){
                    return "spinna-disabled";
                }
                return "";
            };

            $scope.disableSpinna = function(){
                return isNaN(this.nivalue);
            };

            $scope.mouseWheel = function($event) {
                if($event.originalEvent.wheelDelta > 0 || $event.originalEvent.deltaY < 0) {
                    $scope.incrementValue();
                }else {
                    $scope.decrementValue();
                }
                $event.preventDefault();
            };

            $scope.incrementValue = function(){
                if(!$scope.itemdisabled()) {
                    var valueToIncrement = getIncrement(this.nistep);
                    if (!isNaN(this.nivalue)) {
                        this.nivalue = incrementValue(this.nivalue, this.niminimum, this.nimaximum, valueToIncrement, this.nidecimalplaces);
                        applyScope();
                    }
                }
            };

            $scope.decrementValue = function(){
                if(!$scope.itemdisabled()) {
                    var valueToIncrement = getIncrement(this.nistep);
                    if (!isNaN(this.nivalue)) {
                        this.nivalue = incrementValue(this.nivalue, this.niminimum, this.nimaximum, (valueToIncrement * -1), this.nidecimalplaces);
                        applyScope();
                    }
                }
            };

            var getIncrement = function(increment){
                var valueToIncrement;
                if(typeof(increment) === "undefined"){
                    valueToIncrement = 1;
                }else{
                    valueToIncrement = parseFloat(increment);
                }
                return valueToIncrement;
            };

            var validateIncrementValue = function(value, increment){
                var remainder;
                if(productValidationService.violatesSteppingIncrement(value, increment)){
                    if(increment > 0){
                        increment = increment - (value % increment);
                    }else{
                        increment = (value % increment) * -1;
                    }
                }
                return parseFloat(increment.toFixed(2));
            };

            var incrementValue = function(value, minimumValue, maximumValue, valueToIncrement, decimalPlaces){
                var actualValue,
                    newValue;

                if(typeof(value) === "undefined"){
                    if(typeof(minimumValue !== "undefined")){
                        value = minimumValue;
                    }else{
                        return value;
                    }
                }

                actualValue = parseFloat(value);
                valueToIncrement = validateIncrementValue(value, valueToIncrement);

                if(!isNaN(valueToIncrement)) {
                    newValue = actualValue + valueToIncrement;
                    if (newValue < parseFloat(minimumValue)) {
                        newValue = minimumValue;
                    }
                    if (newValue > parseFloat(maximumValue)) {
                        newValue = maximumValue;
                    }
                    return parseFloat(newValue).toFixed(decimalPlaces);
                }
                return 0;
            };
        }
    };
});