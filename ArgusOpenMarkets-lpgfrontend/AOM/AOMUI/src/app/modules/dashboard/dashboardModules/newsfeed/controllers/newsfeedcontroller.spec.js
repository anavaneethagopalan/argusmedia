﻿describe( 'argusOpenMarkets.dashboard.newsFeedController', function() {
    "use strict";
    describe( 'newsFeedController', function() {
        var $scope, $rootScope, thisController,newsService;

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(module('argusOpenMarkets.dashboard'));

        beforeEach(inject(function ($controller, _$rootScope_, mockNewsService, mockNewsMessageSubscriptionService) {
            newsService = mockNewsService;
            var mockUserService = {
                getUser: function () {
                    return {"userId": 3};
                }
            };

            $rootScope = _$rootScope_;
            $scope = $rootScope.$new();
            thisController = $controller('newsFeedController', {
                $scope: $scope,
                newsService: mockNewsService,
                newsMessageSubscriptionService: mockNewsMessageSubscriptionService,
                userService: mockUserService
            });
        }));

        it('should be defined', inject(function () {
            expect(thisController).toBeDefined();
        }));

        it('should contain a title of News and Analysis', function () {
            expect($scope.Title).toEqual("News & Analysis");
        });

        it('close item should set expanded to false', function(){
            var item = {};
            $scope.expandedItem = {};
            $scope.expandedItem.expanded = true;
            $scope.closeExpandedItem(item);
            expect($scope.expandedItem.expanded).toEqual(false);
            expect(item.expanded).toEqual(false);
        });

        it('should call the news service to get the full article when we try to expand', function(){
            var itemToGet = {cmsId :1};
            spyOn(newsService,'getNewsStory')
            $scope.expandItem(itemToGet);
            expect($scope.expandedItem.expanded).toEqual(true);
            expect(newsService.getNewsStory).toHaveBeenCalled();
        });

        it('should tell the news service to updateAllFormattedDates when the midnight event is fired', function() {
            spyOn(newsService, "updateAllFormattedDates");
            expect(newsService.updateAllFormattedDates).not.toHaveBeenCalled();
            $rootScope.$broadcast('midnightService-midnight-event');
            expect(newsService.updateAllFormattedDates).toHaveBeenCalled();
        });
    })
});