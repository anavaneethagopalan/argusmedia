describe('number increment directive tests', function () {
    var $scope,
        $compile,
        el;

    beforeEach(module('src/app/modules/dashboard/dashboardModules/order/directives/numberIncrement.part.html'));

    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(module(function($provide){
        $provide.provider('htmlInterceptor', function() {
            this.$get = function () {
                return {
                    request: function (config) {
                        return config;
                    }
                };
            }
        });
    }));

    beforeEach(inject(function (_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.test = {
            id: 10,
            enteredByUserId: 0,
            lastUpdatedUserId: 5062,
            dateCreated: "2014-11-24T09:27:09.702Z",
            lastUpdated: "2014-11-24T10:15:41.262Z",
            priceHigh: 788.00,
            priceLow: 788.00,
            productId: 1,
            product: null,
            businessDate: "24.11.2014",
            trendFromPreviousAssesment: "Downward"
        };
        $compile = _$compile_;
        el = angular.element('<div><span number-increment nivalue="100" nistep="10" niminimum="50" nimaximum="150"><input name="quantity" class="pull-left order-input" required type="text" pattern=""/></span></div>');
        $compile(el)($scope);
        $scope.$digest();

    }));

    it('should return an empty string for the spinna class as we have a valid number displayed', function () {
        expect(el.scope().$$childHead.spinnaClass()).toEqual("");
    });

    it('incrementing the value should increase it by 10.', function () {
        el.scope().$$childHead.incrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('110');
    });

    it('incrementing the value six times should only actually increment 5 times as it will have reached the maximum value', function () {
        el.scope().$$childHead.incrementValue();
        el.scope().$$childHead.incrementValue();
        el.scope().$$childHead.incrementValue();
        el.scope().$$childHead.incrementValue();
        el.scope().$$childHead.incrementValue();
        el.scope().$$childHead.incrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('150');
    });

    it('incrementing the value once with an invalid step should increase the value to the next valid step', function () {

        el.scope().$$childHead.nivalue = 11;
        el.scope().$$childHead.nistep = 5;
        el.scope().$$childHead.niminimum = 5;
        el.scope().$$childHead.nimaximum = 100;

        el.scope().$$childHead.incrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('15');
    });

    it('decrementing the value once with an invalid step value should decrease the value to the next valid step', function () {

        el.scope().$$childHead.nivalue = 12;
        el.scope().$$childHead.nistep = 5;
        el.scope().$$childHead.niminimum = 5;
        el.scope().$$childHead.nimaximum = 100;

        el.scope().$$childHead.decrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('10');
    });

    it('incrementing the decimal value once with an invalid step should increase the value to the next valid step', function () {

        el.scope().$$childHead.nivalue = 12.3;
        el.scope().$$childHead.nistep = 0.25;
        el.scope().$$childHead.niminimum = 5;
        el.scope().$$childHead.nimaximum = 100;
        el.scope().$$childHead.nidecimalplaces = 2;
        el.scope().$$childHead.incrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('12.50');
    });

    it('decrementing the decimal value once with an invalid step value should decrease the value to the next valid step', function () {

        el.scope().$$childHead.nivalue = 12.3;
        el.scope().$$childHead.nistep = 0.25;
        el.scope().$$childHead.niminimum = 5;
        el.scope().$$childHead.nimaximum = 100;
        el.scope().$$childHead.nidecimalplaces = 2;

        el.scope().$$childHead.decrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('12.25');
    });

    it('incrementing the value 12.5 should display 12.50 if displaying 2 decimal places', function () {

        el.scope().$$childHead.nivalue = 12.25;
        el.scope().$$childHead.nistep = 0.25;
        el.scope().$$childHead.niminimum = 5;
        el.scope().$$childHead.nimaximum = 100;
        el.scope().$$childHead.nidecimalplaces = 2;

        el.scope().$$childHead.incrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('12.50');
    });

    it('incrementing the value 10 should display 20 if displaying 0 decimal places and the step is 10', function () {

        el.scope().$$childHead.nivalue = 10;
        el.scope().$$childHead.nistep = 10;
        el.scope().$$childHead.niminimum = 5;
        el.scope().$$childHead.nimaximum = 100;
        el.scope().$$childHead.nidecimalplaces = 0;

        el.scope().$$childHead.incrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('20');
    });

    it('incrementing the value 10 should display 20.00 if displaying 2 decimal places and the step is 10', function () {

        el.scope().$$childHead.nivalue = 10;
        el.scope().$$childHead.nistep = 10;
        el.scope().$$childHead.niminimum = 5;
        el.scope().$$childHead.nimaximum = 100;
        el.scope().$$childHead.nidecimalplaces = 2;

        el.scope().$$childHead.incrementValue();
        expect(el.scope().$$childHead.nivalue).toEqual('20.00');
    });
});