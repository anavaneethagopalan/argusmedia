﻿angular.module('argusOpenMarkets.dashboard').service("newsService", function newsService($rootScope,
                                                                                         $filter,
                                                                                         userApi,
                                                                                         newsMessageSubscriptionService,
                                                                                         dateService){
    "use strict";

    var newNewsEventName = "newService-new-news";
    var newsFeedItems =[];

    var getNewsStory = function (id, callback, errorCallback) {
        userApi.getNewsStory(id, callback, errorCallback);
    };

    var subscribeToNewsAlerts = function(scope, action) {
        var handler = $rootScope.$on(newNewsEventName, action);
        scope.$on('$destroy', handler);
    };

    var notifySubscribersOfNewNews = function(){
        $rootScope.$emit(newNewsEventName);
    };

    var formatDate = function (itemDate) {
        if (angular.isUndefined(itemDate)) {
            return "";
        }
        var todaysDate = dateService.getDateTimeNow();
        var tempDate = angular.copy(itemDate);

        if (tempDate.setHours(0, 0, 0, 0) === todaysDate.setHours(0, 0, 0, 0)) {
            return $filter('date')(itemDate, 'HH:mm');
        } else {
            return $filter('date')(itemDate, 'd-MMM-y');
        }
    };

    var transform = function(news) {
        news.expanded = false;
        news.publicationDate = new Date(moment(news.publicationDate));
        news.formattedDate = formatDate(news.publicationDate);
        return news;
    };

    var getNews = function(){
        return newsFeedItems;
    };

    var updateAllFormattedDates = function() {
        newsFeedItems.forEach(function(n) {
            n.formattedDate = formatDate(n.publicationDate);
        });
    };

    var newsItemReceived = function(response){
        var item = transform(response.news);
        for (var i = 0, len = newsFeedItems.length; i < len; i++) {
            if (item.publicationDate > newsFeedItems[i].publicationDate) {
                newsFeedItems.splice(i, 0, item);
                return;
            } else {
                if (item.publicationDate - newsFeedItems[i].publicationDate ===0) {
                    if (item.cmsId !== newsFeedItems[i].cmsId) // don't duplicate
                    {
                        newsFeedItems.splice(i, 0, item);
                    }
                    return;
                }
            }
        }
        newsFeedItems.push(item);
        notifySubscribersOfNewNews();
    };

    var onNewsItemDeleted = function (newsId) {

        // Ok - we need to remove this from the list of newsFeedItems.
        if(newsId){

            for(var i = 0; i < newsFeedItems.length; i++){
                if(newsFeedItems[i].cmsId === newsId){
                    newsFeedItems.splice(i, 1);
                    break;
                }
            }
        }
    };

    newsMessageSubscriptionService.registerActionOnNews(newsItemReceived);
    newsMessageSubscriptionService.registerNewsItemDeleted(onNewsItemDeleted);

    return {
        getNewsStory:getNewsStory,
        transform:transform,
        getNews:getNews,
        subscribeToNewsAlerts:subscribeToNewsAlerts,
        formatDate:formatDate,
        updateAllFormattedDates:updateAllFormattedDates
    };
});