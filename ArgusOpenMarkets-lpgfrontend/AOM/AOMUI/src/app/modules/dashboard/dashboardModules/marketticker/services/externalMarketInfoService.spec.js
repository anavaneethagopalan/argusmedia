describe("externalMarketInfoService tests", function() {

    var service, scope;

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(function() {
        scope = {
            "marketTickerInfoProducts": [{id:1}, {id:2}, {id:3}]
        };
    });

    beforeEach(inject(function(externalMarketInfoService){
        service = externalMarketInfoService;
    }));

    var getNumberOfTickedProducts = function() {
        return _.reduce(scope.marketTickerInfoProducts, function(memo, p) { return memo + Number(p.ticked); }, 0);
    };

    it('should ensure no products selected if market info type is changing from type to bid and more than one product already selected', function(){
        _.each(scope.marketTickerInfoProducts, function(p) {p.ticked = true;});
        expect(getNumberOfTickedProducts()).toEqual(3);
        service.setMarketInfoType(scope, 'bid');
        expect(getNumberOfTickedProducts()).toEqual(0);
    });

    it('should ensure one product selected if market info type is changing from type to bid and one product already selected', function(){
        _.each(scope.marketTickerInfoProducts, function(p, i) {p.ticked = (i === 0);});
        expect(getNumberOfTickedProducts()).toEqual(1);
        service.setMarketInfoType(scope, 'bid');
        expect(getNumberOfTickedProducts()).toEqual(1);
    });

    it('should ensure no products selected if market info type is changing from type to bid and no product selected', function(){
        _.each(scope.marketTickerInfoProducts, function(p) {p.ticked = false;});
        expect(getNumberOfTickedProducts()).toEqual(0);
        service.setMarketInfoType(scope, 'bid');
        expect(getNumberOfTickedProducts()).toEqual(0);
    });

    it('should ensure product still selected if market info type is changing from bid to info and a product already selected', function(){
        service.setMarketInfoType(scope, 'bid');
        _.each(scope.marketTickerInfoProducts, function(p, i) {p.ticked = (i === 0);});
        expect(getNumberOfTickedProducts()).toEqual(1);
        service.setMarketInfoType(scope, 'info');
        expect(getNumberOfTickedProducts()).toEqual(1);
    });

    it('should ensure no products selected if market info type is changing from bid to info and no product selected', function(){
        service.setMarketInfoType(scope, 'bid');
        _.each(scope.marketTickerInfoProducts, function(p) {p.ticked = false;});
        expect(getNumberOfTickedProducts()).toEqual(0);
        service.setMarketInfoType(scope, 'info');
        expect(getNumberOfTickedProducts()).toEqual(0);
    });
});
