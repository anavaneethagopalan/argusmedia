﻿describe( 'argusOpenMarkets.dashboard.authExternalDealController', function() {
    "use strict";
    describe( 'authExternalDealController', function() {

        var controller,
            testScope,
            mockUserApi = {
                getPrincipals: function(name){},
                getBrokers: function(name){}
            },
            mockModalInstance = { dismiss: function(reason){} },
            metadataDefinitions = [{
                "metadataList": {
                    "id": 2,
                    "fieldList": [
                        {
                            "id": 1,
                            "displayName": "Port A",
                            "itemValue": 100
                        },
                        {
                            "id": 2,
                            "displayName": "Port B",
                            "itemValue": 101
                        },
                        {
                            "id": 3,
                            "displayName": "Port c",
                            "itemValue": 102
                        }
                    ]},
                "id": 1,
                "productId": 1,
                "displayName": "Delivery Port",
                "fieldType": "IntegerEnum"
            }, {
                "valueMinimum": 0,
                "valueMaximum": 20,
                "id": 1,
                "productId": 2,
                "displayName": "Delivery Description",
                "fieldType": "String"
            }],
            products = [
                {"productId":1,"productName":"Argus Naphtha CIF NWE - Outright","productTitle":"Argus Naphtha CIF NWE - Outright","commodity":{"id":1,"name":"Naphtha"},"contractSpecification":{"volume":{"minimum":12500,"maximum":35000,"increment":500,"decimalPlaces":0,"volumeUnitCode":"MT"},"pricing":{"minimum":1,"maximum":9999,"increment":0.01,"decimalPlaces":2,"pricingUnitCode":"MT","pricingCurrencyCode":"USD"},"tenors":[{"deliveryLocation":{"id":1,"name":"Rotterdam","dateCreated":"2014-01-01T00:00:00Z"},"deliveryStartDate":"+10d","deliveryEndDate":"+25d","rollDate":"DAILY","id":0}],"contractType":"Physical"}},
                {"productId":2,"productName":"Argus Naphtha CIF NWE - Diff","productTitle":"Argus Naphtha CIF NWE - Diff","commodity":{"id":1,"name":"Naphtha"},"contractSpecification":{"volume":{"minimum":12500,"maximum":35000,"increment":500,"decimalPlaces":0,"volumeUnitCode":"MT"},"pricing":{"minimum":-999,"maximum":999,"increment":0.01,"decimalPlaces":2,"pricingUnitCode":"BBL","pricingCurrencyCode":"USD"},"tenors":[{"deliveryLocation":{"id":-1,"name":"-","dateCreated":"2014-01-01T00:00:00Z"},"deliveryStartDate":"+10d","deliveryEndDate":"+25d","rollDate":"DAILY","id":0}],"contractType":"Spread"}}
            ],
            mockProductConfigurationService = {
                getAllProductConfigurations: function(){
                    return products;
                },
                getProductConfigurationById: function(productId){
                    return products[productId - 1];
                },
                getProductMetaDataById: function(productId) {
                    return { "productId": productId, "fields": [ metadataDefinitions[productId - 1] ] };
                }
            },
            initialMetadataValue = {
                productMetaDataId: 1,
                displayName: "Delivery Port",
                displayValue: "Port B",
                itemValue: 101,
                metaDataListItemId: 2,
                itemType:"IntegerEnum"
            };

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach( module( 'argusOpenMarkets.dashboard' ) );

        var provide;
        beforeEach(module(function($provide) { provide = $provide; }));

        beforeEach(inject( function( $controller, $rootScope, mockUserService, helpers  ) {
            provide.value('productConfigurationService', mockProductConfigurationService);
            testScope = $rootScope.$new();

            controller = $controller('authExternalDealController', {
                $scope: testScope,
                $modalInstance: mockModalInstance,
                userApi: mockUserApi,
                $state: {},
                userService: mockUserService,
                dealService: {},
                productConfigurationService:mockProductConfigurationService,
                $filter: {},
                deal: {
                    notes: { length: 0 },
                    metaData: [initialMetadataValue],
                    productId: products[0].productId
                },
                userContactDetails: {}
            });
        }));

        it('should have defined the controller', function() {
            expect(controller).toBeDefined();
        });

        it('should call dismiss on modal instance when exit',function (){
            spyOn(mockModalInstance,'dismiss');
            testScope.exit();
            expect(mockModalInstance.dismiss).toHaveBeenCalled();
        });

        it('should return false on canCreateVerified when no selected contract', function(){
            testScope.selectedContract = undefined;
            var result = testScope.canCreateVerified();
            expect(result).toEqual(false);
        });

        it('should return false for canViewFreeFormNotes ', function(){
            var result = testScope.canCreateVerified();
            expect(result).toEqual(false);
        });

        it('should call get principals from userapi', function(){
            spyOn(mockUserApi,'getPrincipals');
            testScope.getPrincipals('');
            expect(mockUserApi.getPrincipals).toHaveBeenCalled();
        });

        it('should call get brokers from userapi', function(){
            spyOn(mockUserApi,'getBrokers');
            testScope.getBrokers('');
            expect(mockUserApi.getBrokers).toHaveBeenCalled();
        });

        it('should mark a deal as not using custom free form notes if the free form deal message is empty', function() {
            expect(testScope.deal.freeFormDealMessage).toBeFalsy();
            testScope.overrideFreeForm();
            expect(testScope.deal.useCustomFreeFormNotes).toBeFalsy();
        });

        it('should mark a deal as using custom free form notes if the free form deal message is not empty', function() {
            testScope.deal.freeFormDealMessage = "Not an empty string";
            testScope.overrideFreeForm();
            expect(testScope.deal.useCustomFreeFormNotes).toBeTruthy();
        });

        it('should set metadata collection when loaded', function(){
            expect(testScope.metaDataCollection).toEqual([{
                definition: metadataDefinitions[0],
                metadataitem: initialMetadataValue
            }]);
        });

        it('should update metadata collection when selected product was changed', function(){

            testScope.selectContract(products[1]);

            var expectedMetadataValue = {
                productMetaDataId:1,
                displayName:"Delivery Description",
                displayValue:"",
                itemValue:null,
                metaDataListItemId:null,
                itemType:"String"
            };

            expect(testScope.deal.metaData).toEqual([expectedMetadataValue]);
            expect(testScope.metaDataCollection).toEqual([{
                definition: metadataDefinitions[1],
                metadataitem: expectedMetadataValue
            }]);
        });

        it('should restore metadata values when selected product was changed back', function(){

            // Switch to the second product
            testScope.selectContract(products[1]);
            // Switch back to the first product
            testScope.selectContract(products[0]);

            expect(testScope.deal.metaData).toEqual([initialMetadataValue]);
            expect(testScope.metaDataCollection).toEqual([{
                definition: metadataDefinitions[0],
                metadataitem: initialMetadataValue
            }]);
        });
    });
});