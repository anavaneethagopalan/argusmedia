﻿describe('bidAskStack tests', function () {

    var mockUser = {
        userId: 12,
        fullName: "Mock User",
        subscribedProducts: [
            {productId: 1, userProductPrivileges: ["Order_Create_Principal", "Order_Aggress_Principal"]},
            {productId: 2, userProductPrivileges: ["Order_Create_Broker", "Order_Aggress_Broker"]}
        ],
        userPrivileges: {},
        organisation: {organisationName: "XYZ",  organisationType: "Trading"},
        userOrganisation : {organisationName: "XYZ",  organisationType: "Trading"},
        username: "mockUser"
    };

    var mockGridOptions = {
        ngGrid: {
            $viewport: {
                css: function(name,value){

                }
            },
            $root: {
                css: function(name,value){

                }
            }
        }
    };

    var mockDashboardConfigurationService = {};
    var mockproductConfigurationService = {};

    var mockUserService = {

        hasProductPrivilege: function () {
            return true;
        },
        getUsersOrganisation: function () {
            return mockUser.organisation;
        },
        isBroker: function(){ return false;},

        getUser: function(){
            return mockUser;
        }
    };

    var mockUserOrderService = {
        isOrderMine: function (order) {
            return mockUserService.getUsersOrganisation().organisationName === order.principal || mockUser.organisation.organisationName === order.broker;
        },
        isBroker: function(){ return false;}
    };

    var onBidEventHandler;
    var onAskEventHandler;
    var onDeleteEventHandler;

    var mockOrderRepositoryService;

    function mockBidAskStack () {
        this.bids = [];
        this.asks = [];
    }

    mockBidAskStack.createEmptyBid = function(){

    };

    mockBidAskStack.createEmptyAsk = function(){

    };

    mockBidAskStack.prototype.onBidMessageEventHandler = function (orderMessage) {
        this.bids.push(orderMessage.messageBody);
    };

    mockBidAskStack.prototype.onAskMessageEventHandler = function(orderMessage){
        this.asks.push(orderMessage.messageBody);
    };

    mockBidAskStack.prototype.deleteOrderIfExists = function(orderKey) {
        this.bids = _.omit(this.bids, function(o) { return o.topicKey === orderKey; });
        this.asks = _.omit(this.asks, function(o) { return o.topicKey === orderKey; });
    };

    describe('bidAskStackControllerTestsNoConfig', function () {

        var scope, templateCache, helpers, BidAskStack;
        var mockModal = {open: function () {
            return true
        }, result: function() {return 'exit'}};

        var httpBackend,http;


        beforeEach(function() {
            mockOrderRepositoryService = jasmine.createSpyObj('mockOrderRepositoryService', [
                'subscribeToOrderUpdates', 'unsubscribeFromOrderUpdates'
            ]);
            mockOrderRepositoryService.subscribeToOrderUpdates.and.callFake(
                function(subscriberId, productIds, callbacks, orderTypes) {
                    onBidEventHandler = callbacks["onBid"];
                    onAskEventHandler = callbacks["onAsk"];
                    onDeleteEventHandler = callbacks["onDelete"];
                });
        });

        beforeEach(function() {
            module('argusOpenMarkets.shared');
            module('argusOpenMarkets.dashboard.bidaskstack');
            inject(function ($httpBackend, $http) {
                httpBackend = $httpBackend;
                http = $http;
            });
        });

        beforeEach((inject(function ($rootScope, $templateCache, helpers) {
            scope = $rootScope.$new();
            templateCache = $templateCache;
            helpers = helpers;
        })));

        it("should throw an error if there is no product config", inject(function ($controller, helpers) {
            expect(function () {
                $controller('bidAskStackController', {
                    $scope: scope,
                    $modal: mockModal,
                    $templateCache: templateCache,
                    helpers: helpers,
                    userService: mockUserService,
                    userOrderService: mockUserOrderService,
                    orderRepositoryService: mockOrderRepositoryService,
                    BidAskStack: mockBidAskStack,
                    dashboardConfigurationService: mockDashboardConfigurationService,
                    productConfigurationService: mockproductConfigurationService
                })
            }).toThrow("No productConfig attached to scope");
        }));
    });

    describe('bidAskStackControllerTests', function () {

        var bidMessage =
        {
            "messageType": "Order",
            "messageAction": "CREATE",
            "messageBody": {
                "id": 133,
                "price": 900.0,
                "quantity": 17500.0,
                "tenor": {
                    "deliveryLocation": {
                        "id": 1,
                        "name": null,
                        "dateCreated": null
                    },
                    "deliveryStartDate": null,
                    "deliveryEndDate": null,
                    "rollDate": null,
                    "id": 1
                },
                "orderType": "Bid",
                "lastUpdated": "2014-07-03T16:40:06.8325744Z",
                "lastUpdatedUserId": 2,
                "lastUpdatedUserName": null,
                "userId": 3,
                "userName": null,
                "deliveryEndDate": "2009-04-12T19:44:55Z",
                "deliveryStartDate": "2009-04-12T19:44:55Z",
                "brokerId": null,
                "brokerName": null,
                "brokerOrganisationName": null,
                "brokerShortCode": "*",
                "organisationId": 3,
                "organisationName": null,
                "orderStatus": "Active",
                "notes": null,
                "executionMode": "Internal",
                "productId": 1,
                "productName": null,
                "order":{
                    "brokerShortCode": "*"
                },
                "topicKey": "FakeTopicKeyForBidId1"
            },
            "sessionId": null,
            "order":{
                "productId": 1
            },
            "userId": 0
        };

        var askMessage =
        {
            "messageType": "Order",
            "messageAction": "CREATE",
            "messageBody": {
                "id": 133,
                "price": 900.0,
                "quantity": 17500.0,
                "tenor": {
                    "deliveryLocation": {
                        "id": 1,
                        "name": null,
                        "dateCreated": null
                    },
                    "deliveryStartDate": null,
                    "deliveryEndDate": null,
                    "rollDate": null,
                    "id": 1
                },
                "orderType": "Ask",
                "lastUpdated": "2014-07-03T16:40:06.8325744Z",
                "lastUpdatedUserId": 2,
                "lastUpdatedUserName": null,
                "userId": 3,
                "userName": null,
                "deliveryEndDate": "2009-04-12T19:44:55Z",
                "deliveryStartDate": "2009-04-12T19:44:55Z",
                "brokerId": null,
                "brokerName": null,
                "brokerOrganisationName": null,
                "brokerShortCode": "*",
                "organisationId": 3,
                "organisationName": null,
                "orderStatus": "Active",
                "notes": null,
                "executionMode": "Internal",
                "productId": 1,
                "productName": null,
                "topicKey": "FakeTopicKeyForAskId1"
            },
            "order": {
                "productId": 1
            },
            "sessionId": null,
            "userId": 0
        };

        var scope, templateCache, helpers;
        var mockModalInstance = {result: {then: function(a,b){}}};
        var mockModal = {open: function (a) {return mockModalInstance;}};

        var httpBackend,http;

        beforeEach(function() {
            module('argusOpenMarkets.shared');
            module('argusOpenMarkets.dashboard.bidaskstack');
            inject(function ($httpBackend, $http) {
                httpBackend = $httpBackend;
                http = $http;
            });
        });

        beforeEach(inject(function ($rootScope, $templateCache, helpers, $controller) {
            scope = $rootScope.$new();
            templateCache = $templateCache;
            helpers = helpers;
            scope.productConfigItems = [
                {
                    "productId": 1,
                    "productName": "Argus-Naphtha-CIF-NWE-Outright",
                    "productTitle": "Argus-Naphtha-CIF-NWE-Outright",
                    "contractSpecification": {
                        "contractType": "Physical",
                        "volume": {"volumeUnitCode": "MT", "minimum": 12500, "maximum": 35000, "increment": 500, "decimalPlaces": 0},
                        "pricing": {"pricingUnitCode": "MT", "pricingCurrencyCode": "$", "minimum": 1, "maximum": 9999, "increment": 0.01, "decimalPlaces": 2},
                        "tenors": [
                            {"deliveryLocation": "Rotterdam", "deliveryStartDate": "+10", "deliveryEndDate": "+25", "rollDate": "Daily"}
                        ]
                    }
                }
            ];
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            })
        }));

        it('gets the grid height',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            scope.gridRowCount = 5;
            scope.GRID_ROW_HEIGHT = 10;
            scope.GRID_HEADER_HEIGHT = 10;
            scope.GRID_FOOTER_HEIGHT = 10;

            var gridRowHeight = scope.getGridHeight();

            expect(gridRowHeight.height).toBe('72px');
        }));

        it('opens edit modal after row is clicked and order is mine',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            spyOn(scope,'editBidAsk');

            var priceRequest = {
                orderIsMine: true,
                "nonTradeable": false
            };
            var emptyType = {};

            scope.bidAskRowClicked(priceRequest,emptyType);

            expect(scope.editBidAsk).toHaveBeenCalled;
        }));

        it('opens execute modal after row is clicked and order is not mine',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            spyOn(scope,'executeBidAsk');

            var priceRequest = {
                orderIsMine: false,
                nonTradeable: false,
                order: {}
            };
            var emptyType = {};

            scope.bidAskRowClicked(priceRequest,emptyType);

            expect(scope.executeBidAsk).toHaveBeenCalled;
        }));

        it('should send event when up button clicked',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            spyOn(scope,'$emit');

            scope.upClicked();

            expect(scope.$emit).toHaveBeenCalled;
        }));

        it('should send event when down button clicked',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $rootScope : scope,
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            spyOn(scope,'$emit');

            scope.downClicked();

            expect(scope.$emit).toHaveBeenCalled;
        }));

        it('onDecreaseBidAskStackSize sets grid row count',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            var emptyEvent = {};
            var gridRowCount = 10;

            var bidGridOptions = angular.copy(mockGridOptions);
            var askGridOptions = angular.copy(mockGridOptions);

            scope.bidGridOptions = bidGridOptions;
            scope.askGridOptions = askGridOptions;

            scope.onDecreaseBidAskStackSize(emptyEvent,gridRowCount);

            expect(scope.gridRowCount).toBe(gridRowCount);
        }));

        it('onIncreaseBidAskStackSize sets grid row count',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            var emptyEvent = {};
            var gridRowCount = 10;

            var bidGridOptions = angular.copy(mockGridOptions);
            var askGridOptions = angular.copy(mockGridOptions);

            scope.bidGridOptions = bidGridOptions;
            scope.askGridOptions = askGridOptions;

            scope.onIncreaseBidAskStackSize(emptyEvent,gridRowCount);

            expect(scope.gridRowCount).toBe(gridRowCount);
        }));

        it('should show correct getMarketDepthDisplay ',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            scope.bidAskStack = { getMarketDepth: function(){return 10;}};
            scope.gridRowCount = 6;

            var marketDepthDisplay = scope.getMarketDepthDisplay();

            expect(marketDepthDisplay).toBe(4);
        }));

        it('should be able to click up',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            scope.bidAskStack = { getMarketDepth: function(){return 10;}};
            scope.gridRowCount = 6;

            var canClickUp = scope.canClickUp();

            expect(canClickUp).toBe(true);
        }));

        it('should be able to click down',inject(function ($controller, helpers){
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            scope.bidAskStack = { getMarketDepth: function(){return 3;}};
            scope.gridRowCount = 6;

            var canClickDown = scope.canClickDown();

            expect(canClickDown).toBe(false);
        }));

        it("new bid ask action opens modal", inject(function () {
            spyOn(mockModal, 'open').and.callThrough();
            scope.openDialog = false;
            scope.newBidAsk('bid');
            expect(mockModal.open).toHaveBeenCalled();
        }));

        it("on bid message received displays on grid", inject(function ($controller, helpers) {

            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi: mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            spyOn(mockBidAskStack.prototype,'onBidMessageEventHandler');
            onBidEventHandler(bidMessage);
            expect(mockBidAskStack.prototype.onBidMessageEventHandler).toHaveBeenCalled();

            expect(scope.bids.length == 1);
        }));

        it("on ask message received displays on grid", inject(function ($controller, helpers) {

            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi: mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            spyOn(mockBidAskStack.prototype,'onAskMessageEventHandler');
            onAskEventHandler(askMessage);
            expect(mockBidAskStack.prototype.onAskMessageEventHandler).toHaveBeenCalled();

            expect(scope.asks.length == 1);
        }));

        it("on delete message received displays delete order is called", inject(function ($controller, helpers) {

            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi: mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            onAskEventHandler(askMessage);
            expect(scope.asks.length == 1);

            spyOn(mockBidAskStack.prototype,'deleteOrderIfExists');
            onDeleteEventHandler(askMessage.messageBody.topicKey);
            expect(mockBidAskStack.prototype.deleteOrderIfExists).toHaveBeenCalled();
            expect(scope.asks.length === 0);
        }));

        it("on own bid message received displays on grid", inject(function ($controller, helpers) {

            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            var m = mockUser;
            mockUser.organisation.organisationName = bidMessage.messageBody.principal;

            onBidEventHandler(bidMessage);

            mockUser = m;

            spyOn(mockBidAskStack.prototype,'onBidMessageEventHandler');
            onBidEventHandler(bidMessage);
            expect(mockBidAskStack.prototype.onBidMessageEventHandler).toHaveBeenCalled();

            expect(scope.bids.length == 1);
        }));

        it("on own ask message received displays on grid", inject(function ($controller, helpers) {

            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi:  mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            });

            var m = mockUser;
            mockUser.organisation.organisationName = bidMessage.messageBody.principal;

            mockUser = m;

            spyOn(mockBidAskStack.prototype,'onAskMessageEventHandler');
            onAskEventHandler(askMessage);
            expect(mockBidAskStack.prototype.onAskMessageEventHandler).toHaveBeenCalled();

            expect(scope.asks.length == 1);
        }));

        it("execute bid ask action opens modal", inject(function () {
            var priceRequest = {order: {price: 12}}
            spyOn(mockModal, 'open').and.callThrough();
            scope.executeBidAsk(priceRequest, 'bid');
            expect(mockModal.open).toHaveBeenCalled();
        }));

        it("execute bid ask does not open model if no price", inject(function () {
            var priceRequest = {order: {price: null}}
            spyOn(mockModal, 'open');
            scope.executeBidAsk(priceRequest, 'bid');
            expect(mockModal.open).not.toHaveBeenCalled;
        }));

        it("execute bid ask does not open model if no order", inject(function () {
            var priceRequest = {};
            spyOn(mockModal, 'open');
            scope.executeBidAsk(priceRequest, 'bid');
            expect(mockModal.open).not.toHaveBeenCalled;
        }));

    });

    describe('bid ask stack controller will set the grid row height to the product config items bidAskStackNumberRows passed in', function () {

        var scope, templateCache, helpers;
        var mockModal = {open: function () {
            return true
        }};

        var httpBackend,http;

        mockUserApi = {
            //getPrincipalsForBrokerResource: function(){return {post:function(){return {"Id":1};}};}
            getPrincipalsForBroker : function(){ return {"Id":1};},
            getBrokersForPrincipal : function(){ return {"Id":1};}
        };

        beforeEach(function() {
            module('argusOpenMarkets.shared');
            module('argusOpenMarkets.dashboard.bidaskstack');
            inject(function ($httpBackend, $http) {
                httpBackend = $httpBackend;
                http = $http;
            });
        });

        beforeEach((inject(function ($rootScope, $templateCache, helpers, $controller) {
            scope = $rootScope.$new();
            templateCache = $templateCache;
            helpers = helpers;
            scope.productConfigItems = [
                {
                    "productId": 1,
                    "productName": "Argus-Naphtha-CIF-NWE-Outright",
                    "productTitle": "Argus-Naphtha-CIF-NWE-Outright",
                    "bidAskStackNumberRows": 3,
                    "contractSpecification": {
                        "contractType": "Physical",
                        "volume": {"volumeUnitCode": "MT", "minimum": 12500, "maximum": 35000, "increment": 500, "decimalPlaces": 0},
                        "pricing": {"pricingUnitCode": "MT", "pricingCurrencyCode": "$", "minimum": 1, "maximum": 9999, "increment": 0.01, "decimalPlaces": 2},
                        "tenors": [
                            {"deliveryLocation": "Rotterdam", "deliveryStartDate": "+10", "deliveryEndDate": "+25", "rollDate": "Daily"}
                        ]
                    }
                }
            ];
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi: mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            })
        })));

        it("should set the grid row count to 3", inject(function () {

            expect(scope.gridRowCount).toEqual(3);
        }));

        it("should return Any Broker for the Broker Display Name if the Broker Short Code is *", function(){
            var brokerDisplayName = scope.getBrokerDisplayName("*", "");

            expect(brokerDisplayName).toEqual("Any Broker");
        });

        it("should return No Broker for the Broker Display Name if the Broker Short Code is null", function(){
            var brokerDisplayName = scope.getBrokerDisplayName(null, "");

            expect(brokerDisplayName).toEqual("No Broker");
        });

        it("should return No Broker for the Broker Display Name if the Broker Short Code is an empty string", function(){
            var brokerDisplayName = scope.getBrokerDisplayName("", "");

            expect(brokerDisplayName).toEqual("No Broker");
        });

        it("should return the broker organisation name and code if the broker short code is not null space or *", function(){
            var brokerDisplayName = scope.getBrokerDisplayName("ZUL", "Zulu Entertainment England");

            expect(brokerDisplayName).toEqual("Zulu Entertainment England (ZUL)");
        });

    });

    describe('bidAskStackControllerTestsNoPermission', function () {

        var scope, templateCache, helpers;
        var mockModalInstance = {result: {then: function(a,b){}}};
        var mockModal = {open: function (a) {return mockModalInstance;}};
        var mockDashboardConfigurationService = {};

        var httpBackend,http;

        mockUserApi = {
            //getPrincipalsForBrokerResource: function(){return {post:function(){return {"Id":1};}};}
            getPrincipalsForBroker : function(){ return {"Id":1};},
            getBrokersForPrincipal : function(){ return {"Id":1};}
        };

        beforeEach(function() {
            module('argusOpenMarkets.shared');
            module('argusOpenMarkets.dashboard.bidaskstack');
            inject(function ($httpBackend, $http) {
                httpBackend = $httpBackend;
                http = $http;
            });
        });

        beforeEach((inject(function ($rootScope, $templateCache, helpers, $controller) {
            scope = $rootScope.$new();
            templateCache = $templateCache;
            helpers = helpers;
            scope.productConfigItems = [
                {
                    "productId": 1,
                    "productName": "Argus-Naphtha-CIF-NWE-Outright",
                    "productTitle": "Argus-Naphtha-CIF-NWE-Outright",
                    "contractSpecification": {
                        "contractType": "Physical",
                        "volume": {"volumeUnitCode": "MT", "minimum": 12500, "maximum": 35000, "increment": 500, "decimalPlaces": 0},
                        "pricing": {"pricingUnitCode": "MT", "pricingCurrencyCode": "$", "minimum": 1, "maximum": 9999, "increment": 0.01, "decimalPlaces": 2},
                        "tenors": [
                            {"deliveryLocation": "Rotterdam", "deliveryStartDate": "+10", "deliveryEndDate": "+25", "rollDate": "Daily"}
                        ]
                    }
                }
            ];
            $controller('bidAskStackController', {
                $scope: scope,
                $modal: mockModal,
                $templateCache: templateCache,
                helpers: helpers,
                userService: mockUserService,
                userOrderService: mockUserOrderService,
                userApi: mockUserApi,
                orderRepositoryService: mockOrderRepositoryService,
                BidAskStack: mockBidAskStack,
                dashboardConfigurationService: mockDashboardConfigurationService,
                productConfigurationService: mockproductConfigurationService
            })
        })));

        it("execute bid ask does not open model if permission denied", inject(function () {
            var priceRequest = {};
            spyOn(mockModal, 'open');
            scope.executeBidAsk(priceRequest, 'bid');
            expect(mockModal.open).not.toHaveBeenCalled;
        }));

        it("create bid ask does not open model if permission denied", inject(function () {
            var priceRequest = {};
            spyOn(mockModal, 'open').and.callThrough();
            scope.newBidAsk(priceRequest, 'bid');
            expect(mockModal.open).not.toHaveBeenCalled;
        }));
    });
});