﻿angular.module('argusOpenMarkets.dashboard')
    .controller('authMarketInfoController', function authMarketInfoController(
            $scope,
            $modalInstance,
            marketInfoService,
            helpers ,
            $state,
            userService,
            info,
            userContactDetails,
            productConfigurationService){
        "use strict";
        $scope.$on('$stateChangeSuccess', function () {
            if ($state.current.name !== 'Dashboard') {
                $modalInstance.dismiss('cancel');
            }
        });

        $scope.exit = function () {
            $modalInstance.dismiss('exit');
        };

        $scope.info = info;
        $scope.userContactDetails = userContactDetails;

        var products = angular.copy(productConfigurationService.getAllProducts());
        var subsProdIds = productConfigurationService.getAllSubscribedProductIds();
        $scope.marketTickerInfoProducts = products;
        for(var i=0;i<products.length;i++) {
            products[i].shouldDisable = !(_.contains(subsProdIds, products[i].productId));
            products[i].ticked = _.contains(info.productIds, products[i].productId);
        }
        $scope.originalProducts = angular.copy(products);

        $scope.canCreateVerified = function(){
            return marketInfoService.canCreateVerified();
        };
        $scope.infoValid = false;
        $scope.characterLimit = 500;
        $scope.charactersLeft = $scope.characterLimit;
        $scope.updated = false;
        $scope.pending = $scope.info.marketInfoStatus === "Pending";
        $scope.voidable = $scope.info.marketInfoStatus === "Active" || $scope.info.marketInfoStatus === "Updated" || $scope.pending ;
        var infoCopy = angular.copy($scope.info);

        $scope.infoChanged = function(){
            var processedText, selectedProducts;
            processedText = marketInfoService.processMarketInfoText($scope.info.info, $scope.characterLimit);

            $scope.info.info = processedText.text;
            $scope.charactersLeft = processedText.charactersLeft;
            $scope.infoValid = processedText.infoValid;

            selectedProducts = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            if($scope.infoValid) {
                if (selectedProducts.length === 0) {
                    $scope.infoValid = false;
                }
            }
            $scope.updated = ($scope.info.info !== infoCopy.info) || $scope.productsChanged($scope.originalProducts, selectedProducts);
        };

        $scope.productsChanged = function(originalProducts, selectedProducts){
            var counter,
                outerCounter,
                productsChanged = false,
                originalItemsSelected = 0,
                selectedProductsLength = 0;

            angular.forEach(originalProducts, function(originalProduct){
                if(originalProduct.ticked){
                    originalItemsSelected ++;
                }
            });

            if(selectedProducts){
                selectedProductsLength = selectedProducts.length;
            }
            if(originalItemsSelected !== selectedProductsLength) {
                return true;
            }

            for(outerCounter = 0;  outerCounter < selectedProducts.length; outerCounter++) {

                for (counter = 0; counter < originalProducts.length; counter++) {
                    if (originalProducts[counter].productId === selectedProducts[outerCounter]){
                        if (originalProducts[counter].ticked === false) {
                            return true;
                        }
                    }
                }
            }

            return productsChanged;
        };

        $scope.verifyMarketInfo = function () {
            $scope.info.productIds = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            marketInfoService.sendVerifyMarketInfoMessage($scope.info);
            $modalInstance.dismiss('submitted');
        };
        $scope.updateMarketInfo = function () {
            $scope.info.productIds = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            marketInfoService.sendUpdateMarketInfoMessage($scope.info);
            $modalInstance.dismiss('submitted');
        };
        $scope.voidMarketInfo = function () {
            $scope.info.productIds = marketInfoService.getTickedProductIds($scope.marketTickerInfoProducts);
            marketInfoService.sendVoidMarketInfoMessage($scope.info);
            $modalInstance.dismiss('submitted');
        };

        $scope.sendMail = function(){
            var link = "mailto:" + userContactDetails.email +
                    "?subject=Argus Open Markets - Market Information Query" +
                    "&body=To " + userContactDetails.name + "," +
                    "%0D%0A%0D%0ARegarding your submission of market information to Argus Open Markets, (our reference: " + info.id + ")." +
                    "%0D%0A%0D%0AThe reported information was: '" + $scope.info.info + "'." +
                    "%0D%0A%0D%0AQUERYHERE" +
                    "%0D%0A%0D%0ARegards" +
                    "%0D%0A%0D%0A" + userService.getUser().name
                ;
            window.sendingEmail=true;
            window.location.href = link;
        };
        $scope.infoChanged();

    });


