angular.module('argusOpenMarkets.dashboard').service("orderRepositoryService",
    function orderRepositoryService($rootScope,
                                    userService,
                                    productMessageSubscriptionService,
                                    dateService) {
        "use strict";

        var orders = {};
        var subscribers = [];
        var subscribedProductIds;
        var allOrderTypes = "*";

        ///////////////////////////////////////////////////////////////////////////
        // Public API
        ///////////////////////////////////////////////////////////////////////////

        /**
         * Get the order with the given order ID
         * @param orderId The Order ID of the order to get
         * @returns The order, can be undefined (if not present)
         */
        function getOrderById(orderId) {
            if (orders) {
                if (orders[orderId]) {
                    return orders[orderId].order;
                }
            }
        }

        /**
         * Subscriber to updates for orders for the specified products and, optionally, of specific types (execution modes)
         * @param subscriberId An ID to identify this subscriber
         * @param productIds Either a single product ID or an array of product IDs to be notified for
         * @param callbacks
         * An object with a callback for each event type you are interested in e.g.
         * {
     *      "onBid": bidCallback,
     *      "onAsk": askCallback,
     *      "onDelete": deleteOrderCallback
     * }
         * All callbacks are of the form function(orderMessage), except deleteOrderEvent which is function(orderTopicKey)
         * @param orderTypes
         * Optional single type or array of orderTypes to subscribe to, default is all orderTypes
         * This is based on the order's executionMode e.g. "Internal", "External". Use "*" for all types (or omit argument)
         */
        function subscribeToOrderUpdates(subscriberId, productIds, callbacks, orderTypes) {
            orderTypes = orderTypes || allOrderTypes;
            if (orderTypes !== allOrderTypes && !Array.isArray(orderTypes)) {
                orderTypes = [orderTypes];
            }
            var subscriber = {
                "id": subscriberId,
                "productIds": Array.isArray(productIds) ? productIds : [productIds],
                "orderTypes": orderTypes,
                "callbacks": callbacks
            };

            if (subscribers) {
                subscribers.push(subscriber);
            } else {
                subscribers = [];
            }

            replayOrders(productIds);
        }

        function replayOrders(productId) {

            var actualOrder;

            for (var orderId in orders) {
                if (orderId) {

                    actualOrder = orders[orderId];

                    if (actualOrder) {
                        if (actualOrder.order.productId === productId) {
                            if (actualOrder.order.orderType.toLowerCase() === 'bid') {

                                for (var countBids = 0; countBids < subscribers.length; countBids++) {
                                    // TODO: Knock out if its an external order
                                    if(isSubscribedOrderType(subscribers[countBids], actualOrder.order)) {
                                        subscribers[countBids].callbacks.onBid(actualOrder);
                                    }
                                }
                            }
                            else {
                                // It's an ask
                                for (var countAsks = 0; countAsks < subscribers.length; countAsks++) {
                                    // TODO: Knock out if its an external order
                                    if(isSubscribedOrderType(subscribers[countAsks], actualOrder.order)) {
                                        subscribers[countAsks].callbacks.onAsk(actualOrder);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /**
         * Unsubscribe from updates on orders
         * @param subscriberId The ID of the subscriber to unsubscribe
         */
        function unsubscribeFromOrderUpdates(subscriberId) {

            var match = -1;
            for (var i = 0; i < subscribers.length; i++) {
                if (subscribers[i].id === subscriberId) {
                    match = i;
                    break;
                }
            }
            if (match > -1) {
                subscribers.splice(match, 1);
            }
        }

        ///////////////////////////////////////////////////////////////////////////
        // Implementation
        ///////////////////////////////////////////////////////////////////////////

        function notifySubscribers(order, callbackKey, payload) {
            if (!order) {
                return;
            }
            _.each(subscribers, function (s) {
                if (_.contains(s.productIds, order.productId) && isSubscribedOrderType(s, order)) {
                    var callback = s.callbacks[callbackKey];
                    if (callback) {
                        callback(payload);
                    }
                }
            });
        }

        function onOrderEvent(orderMessage, callbackKey) {
            var order = orderMessage.order;
            orders[order.id] = orderMessage;
            order.deliveryStartDate = dateService.removeUtcIdentifierFromDate(order.deliveryStartDate);
            order.deliveryEndDate = dateService.removeUtcIdentifierFromDate(order.deliveryEndDate);
            notifySubscribers(order, callbackKey, orderMessage);
        }

        function onBidEvent(orderMessage) {
            onOrderEvent(orderMessage, "onBid");
        }

        function onAskEvent(orderMessage) {
            onOrderEvent(orderMessage, "onAsk");
        }

        function isSubscribedOrderType(subscriber, order) {
            return subscriber.orderTypes === allOrderTypes || _.contains(subscriber.orderTypes, order.executionMode);
        }

        function onOrderDeleted(topicKey) {
            var predicate = function (o) {
                return o.order.topicKey === topicKey;
            };
            var order = _.find(orders, predicate);
            if(order && order.order) {
                notifySubscribers(order.order, "onDelete", topicKey);
            }
            orders = _.omit(orders, predicate);
        }

        function subscribeToProduct(productId) {
            var subscriberId = "Order Repository Service " + productId;
            productMessageSubscriptionService.subscribeToProduct(productId);
            productMessageSubscriptionService.registerActionOnProductMessageType(subscriberId, productId, "onBid", onBidEvent);
            productMessageSubscriptionService.registerActionOnProductMessageType(subscriberId, productId, "onAsk", onAskEvent);
            productMessageSubscriptionService.registerActionOnProductMessageType(subscriberId, productId, "onOrderDeleted", onOrderDeleted);
        }

        function unsubscribeFromProduct(productId) {
            var subscriberId = "Order Repository Service " + productId;
            productMessageSubscriptionService.unsubscribeFromProduct(productId);
            productMessageSubscriptionService.unregisterActionOnProductMessageType(subscriberId, productId, "onBid");
            productMessageSubscriptionService.unregisterActionOnProductMessageType(subscriberId, productId, "onAsk");
            productMessageSubscriptionService.unregisterActionOnProductMessageType(subscriberId, productId, "onOrderDeleted");
        }

        subscribedProductIds = userService.getSubscribedProductIds();

        _.each(subscribedProductIds, subscribeToProduct);

        $rootScope.$on('$destroy', function () {
            _.each(subscribedProductIds, unsubscribeFromProduct);
        });

        return {
            "subscribeToOrderUpdates": subscribeToOrderUpdates,
            "unsubscribeFromOrderUpdates": unsubscribeFromOrderUpdates,
            "getOrderById": getOrderById
        };
    });
