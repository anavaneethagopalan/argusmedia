﻿angular.module('argusOpenMarkets.dashboard')
    .controller('assessmentController',
    function assessmentController($scope,
                                  $modal,
                                  helpers,
                                  userService,
                                  assessmentService,
                                  productConfigurationService)
    {
        $scope.Title = "Assessments";
        $scope.filterAssessmentsForProductHasAssessment = function(assessments){
            var filteredAssessments = [],
                productConfig;

            angular.forEach(assessments, function(assessment){
                var productId;

                if(assessment && assessment.assessment) {
                    productConfig = productConfigurationService.getProductConfigurationById(assessment.assessment.productId);
;
                    if (productConfig) {
                        if (productConfig.hasAssessment && userService.hasProductPrivilege(assessment.assessment.productId, ["View_Assessment_Widget"])) {
                            filteredAssessments.push(assessment);
                        }
                    }
                }
            });

            return filteredAssessments;
        };

        $scope.assessments = $scope.filterAssessmentsForProductHasAssessment(assessmentService.getAssessments());

        $scope.isAE = userService.hasProductPrivilegeOnAnyProduct(["Assessment_Create"]);
        var openDialog = false;

        $scope.getProductName = function (productId) {
            var product = productConfigurationService.getProductConfigurationById(productId);
            return product ? product.productName : "";
        };

        $scope.assessmentStatusDisplayName = function (status) {
            return assessmentService.getAssessmentStatusDisplayName(status);
        };

        var getAssessmentWidgetHeight = function(numberAssessments){

            var numberRows = numberAssessments / 4 >> 0,
                reamainingAssessments = numberAssessments % 4,
                remainingRows;

            if(reamainingAssessments > 0){
                numberRows ++;
            }

            // More Magic numbers.
            switch(numberRows) {
                case 1:
                    return 10;
                case 2:
                    return 15;
                case 3:
                    return 20.5;
                case 4:
                    return 26;
                default:
                    remainingRows = numberRows - 4;
                    return 26 + (remainingRows * 5);
            }
        };

        var onNewAssessmentEventMessageHandler = function () {
            var assessments = assessmentService.getAssessments();
            assessments = $scope.filterAssessmentsForProductHasAssessment(assessments);
            $scope.assessments = assessments;

            // Now we must resize the height of the assessments container. 3 rows for each assessment + 1 row for borders.
            // !! Don't need to redraw assessments on receipt of assessment prices
            //$scope.$emit('setAssessmentsWidgetSize', getAssessmentWidgetHeight($scope.assessments.length));
            //$scope.$emit('random', 0);   // but might need to fire something to refresh senders page?

            $scope.$apply();
        };


        assessmentService.registerSubscriberForAssessments("Assessments Controller", onNewAssessmentEventMessageHandler);

        $scope.$on('$destroy', function () {
            assessmentService.unregisterSubscriberForAssessments("Assessments Controller");
        });

        $scope.getPriceClass = function (ass) {
            return assessmentService.getPriceClass(ass);
        };

        $scope.openAssessmentDialog = function (assessment) {
            if (userService.hasProductPrivilegeOnAnyProduct(["Assessment_Create"]) && openDialog === false) {
                openDialog = true;
                var modalInstance = $modal.open({
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/assessments/views/inputAssessment.part.html',
                    controller: 'inputAssessmentController',
                    backdrop: 'static',
                    resolve: {
                        assessment: function () {
                            return assessment;
                        }
                    }
                });

                modalInstance.result.then(function (reason) {
                    },
                    function (reason) {
                        openDialog = false;
                    });
            }

        };
    });