describe('argusOpenMarkets.dashboard.authExternalMarketInfoController', function() {
    "use strict";
    describe('authExternalMarketInfoController tests', function () {

        var scope, testController;
        var mockModal, mockProductConfigurationService, mockUserService;
        var fakeInitialInfo, fakeUserContactDetails;

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(module('argusOpenMarkets.dashboard'));

        beforeEach(function() {
            fakeInitialInfo = {
                "marketInfoType": "info",
                "info": "This is a test +info item"
            };

            fakeUserContactDetails = {
                name: "test",
                email: "test@test"
            };
        });

        beforeEach(function(){
            module(function($provide) {
                mockUserService = jasmine.createSpyObj('mockUserService', ['getUser']);
                $provide.value('userService', mockUserService);
            });
        });

        beforeEach(inject(function ($controller,
                                    $rootScope,
                                    helpers,
                                    _mockProductConfigurationService_) {
            mockModal = jasmine.createSpyObj('mockModal', ['dismiss']);
            scope = $rootScope.$new();
            mockProductConfigurationService = _mockProductConfigurationService_;
            testController = $controller('authExternalMarketInfoController', {
                $scope: scope,
                $modalInstance: mockModal,
                helpers: helpers,
                productConfigurationService: mockProductConfigurationService,
                userService: mockUserService,
                info: fakeInitialInfo,
                userContactDetails: fakeUserContactDetails
            });
        }));

        it('should have a defined controller', function () {
            expect(testController).toBeDefined();
        });

        it('should set updated flag if info type is changed', function() {
            expect(scope.updated).toBeFalsy();
            scope.setMarketInfoType('bid');
            expect(scope.updated).toBeTruthy();
        });

        it('should set updated flag back to false if info type changed back to original type', function() {
            expect(scope.updated).toBeFalsy();
            scope.setMarketInfoType('bid');
            expect(scope.updated).toBeTruthy();
            scope.setMarketInfoType('info');
            expect(scope.updated).toBeFalsy();
        });
    });
});
