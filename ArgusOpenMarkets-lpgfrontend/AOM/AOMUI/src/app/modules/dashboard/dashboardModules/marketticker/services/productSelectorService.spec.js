describe('argusOpenMarkets.dashboard.productSelectorService', function() {
    "use strict";
    describe('productSelectorService tests', function () {

        var service;
        var mockUserService;
        var fakeProductConfigs;
        var mockProductConfigurationService;
        var thisTestProductConfigurationService;
        var log;

        var fakeProductWith2Tenors = {
            id:123,
            contractSpecification: {
                tenors: [
                    {id:12, deliveryLocation: {}},
                    {id:13, deliveryLocation: {}}
                ]
            }
        };

        beforeEach(module('ui.router', function ($locationProvider) {
            $locationProvider.html5Mode(false);
        }));

        beforeEach(function () {
            module('argusOpenMarkets.dashboard');
        });

        beforeEach(function(){
            mockUserService = jasmine.createSpyObj('mockUserService', [
                'hasProductPrivilege',
                'getSubscribedProductIds'
            ]);
            mockUserService.hasProductPrivilege.and.returnValue(true);

            thisTestProductConfigurationService = jasmine.createSpyObj('thisTestProductConfigurationService', [
                'getProductConfigurationById'
            ]);
            thisTestProductConfigurationService.getProductConfigurationById.and.callFake(function(id) {
                return mockProductConfigurationService.getProductConfigurationById(id);
            });
        });

        beforeEach(function(){
            module(function($provide) {
                var $injector = angular.injector(['argusOpenMarkets.shared']);
                mockProductConfigurationService = $injector.get('mockProductConfigurationService');
                fakeProductConfigs = mockProductConfigurationService.getAllProductConfigurations();
                $provide.value('userService', mockUserService);
                $provide.value('productConfigurationService', thisTestProductConfigurationService);
            });
        });

        beforeEach(inject(function(productSelectorService, _$log_){
            service = productSelectorService;
            log = _$log_;
        }));

        it("should have a defined service", function() {
            expect(service).toBeDefined();
        });

        describe("getUsersTradableProductConfigs", function() {

            it("should be a defined method", function() {
                expect(service.getUsersTradableProductConfigs).toBeDefined();
            });

            it("should return all products the user subscribes to if they have tenors and the user can create orders for them", function() {
                var fakeIds = [1, 2];
                mockUserService.getSubscribedProductIds.and.returnValue(fakeIds);
                var configs = service.getUsersTradableProductConfigs();
                expect(configs.length).toEqual(fakeIds.length);
                var expectedConfigs = _.map(fakeIds, function(id) {
                    return mockProductConfigurationService.getProductConfigurationById(id);
                });
                expect(configs).toEqual(expectedConfigs);
            });

            it("should filter out products with no tenors", function() {
                var fakeIds = [1, 2];
                mockUserService.getSubscribedProductIds.and.returnValue(fakeIds);
                thisTestProductConfigurationService.getProductConfigurationById.and.callFake(function(id) {
                    var cfg = mockProductConfigurationService.getProductConfigurationById(id);
                    if (id === 1) {
                        return cfg;
                    } else {
                        var c = angular.copy(cfg);
                        c.contractSpecification.tenors = [];
                        return c;
                    }
                });
                var configs = service.getUsersTradableProductConfigs();
                expect(configs.length).toEqual(1);
                expect(configs[0].productId).toEqual(1);
            });

            it("should filter out internal products", function() {
                var fakeIds = [1, 2, 3];
                mockUserService.getSubscribedProductIds.and.returnValue(fakeIds);
                thisTestProductConfigurationService.getProductConfigurationById.and.callFake(function(id) {
                    var cfg = mockProductConfigurationService.getProductConfigurationById(id);
                    if (id !== 3) {
                        return cfg;
                    } else {
                        var c = angular.copy(cfg);
                        c.isInternal = true;
                        return c;
                    }
                });
                var configs = service.getUsersTradableProductConfigs();
                expect(configs.length).toEqual(2);
                expect(configs[0].productId).toEqual(1);
                expect(configs[1].productId).toEqual(2);
            });

            it('should ask for order creation privileges when getting product configs for Internal Orders', function() {
                var fakeIds = [1, 2, 3];
                mockUserService.getSubscribedProductIds.and.returnValue(fakeIds);
                service.getUsersTradableProductConfigs("Internal");
                fakeIds.forEach(function(productId) {
                    expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                        productId, ["Order_Create_Broker", "Order_Create_Principal", "Order_Create_All"]);
                })
            });

            it('should ask for external order creation privilege when getting product configs for External Orders', function() {
                var fakeIds = [1, 2, 3];
                mockUserService.getSubscribedProductIds.and.returnValue(fakeIds);
                service.getUsersTradableProductConfigs("External");
                fakeIds.forEach(function(productId) {
                    expect(mockUserService.hasProductPrivilege).toHaveBeenCalledWith(
                        productId, ["ExternalOrder_Create_Principal", "ExternalOrder_Create_Broker"]);
                })
            });

            it("should filter out products the user cannot create orders for Internal Orders", function() {
                var fakeIds = [1, 2, 3];
                mockUserService.getSubscribedProductIds.and.returnValue(fakeIds);
                mockUserService.hasProductPrivilege.and.callFake(function(id) {
                    return id !== 2;
                });
                var configs = service.getUsersTradableProductConfigs("Internal");
                expect(configs.length).toEqual(2);
                expect(configs[0].productId).toEqual(1);
                expect(configs[1].productId).toEqual(3);
            });

            it("should filter out products the user cannot create external orders for External Orders", function() {
                var fakeIds = [1, 2, 3];
                mockUserService.getSubscribedProductIds.and.returnValue(fakeIds);
                mockUserService.hasProductPrivilege.and.callFake(function(id) {
                    return id !== 2;
                });
                var configs = service.getUsersTradableProductConfigs("External");
                expect(configs.length).toEqual(2);
                expect(configs[0].productId).toEqual(1);
                expect(configs[1].productId).toEqual(3);
            });
        });

        describe("onProductSelected", function(){
            it("should be a defined method", function() {
                expect(service.onProductSelected).toBeDefined();
            });

            it('should set the result product ID when a product is selected', function() {
                var p = fakeProductConfigs[0];
                var reply = service.onProductSelected(p);
                expect(reply.result.productId).toEqual(p.productId);
            });

            it('should not enable tenor selection if a product with only 1 tenor is selected', function() {
                var p = fakeProductConfigs[0];
                expect(p.contractSpecification.tenors.length).toEqual(1);
                var reply = service.onProductSelected(p);
                expect(reply.enableTenorSelection).toBeFalsy();
            });

            it('should set the result tenor ID to that tenor if a product with only 1 tenor is selected', function() {
                var p = fakeProductConfigs[0];
                expect(p.contractSpecification.tenors.length).toEqual(1);
                var reply = service.onProductSelected(p);
                expect(reply.result.tenorId).toEqual(p.contractSpecification.tenors[0].id);
            });

            it('should enable tenor selection if a product with more than 1 tenor is selected', function() {
                var reply = service.onProductSelected(fakeProductWith2Tenors);
                expect(reply.enableTenorSelection).toBeTruthy();
            });

            it('should set the result tenor ID to null if a product with more than 1 tenor is selected', function() {
                var reply = service.onProductSelected(fakeProductWith2Tenors);
                expect(reply.result.tenorId).toBeNull();
            });

            it('should add a display name to each tenor when a product with more than 1 tenor is selected', function() {
                var reply = service.onProductSelected(fakeProductWith2Tenors);
                reply.tenors.forEach(function(t) {
                    expect(t.displayName).toBeDefined();
                });
            });

            it('should not modify the selected products tenors directly with the display name', function() {
                service.onProductSelected(fakeProductWith2Tenors);
                fakeProductWith2Tenors.contractSpecification.tenors.forEach(function(t) {
                    expect(t.displayName).not.toBeDefined();
                });
            });

            describe("if product selected has no tenors", function() {
                // These products should be filtered out with getUsersTradableProductConfigs for the real service

                var fakeProduct1 = {"id": 123, "contractSpecification": {"tenors": []}};
                var fakeProduct2 = {"id": 123, "contractSpecification": {}};

                it('should return null tenorId and false for enable tenor selection', function() {
                    var res = service.onProductSelected(fakeProduct1);
                    expect(res.result.tenorId).toBeNull();
                    expect(res.enableTenorSelection).toBeFalsy();
                    res = service.onProductSelected(fakeProduct2);
                    expect(res.result.tenorId).toBeNull();
                    expect(res.enableTenorSelection).toBeFalsy();
                });

                it('should log a warning if tenors empty', function() {
                    service.onProductSelected(fakeProduct1);
                    expect(log.warn.logs.length).toBe(1);
                    expect(log.warn.logs[0]).toContain("Selected product has no tenors");
                });

                it('should log a warning if tenors undefined', function() {
                    service.onProductSelected(fakeProduct2);
                    expect(log.warn.logs.length).toBe(1);
                    expect(log.warn.logs[0]).toContain("Selected product has no tenors");
                });
            });
        });
    })
});