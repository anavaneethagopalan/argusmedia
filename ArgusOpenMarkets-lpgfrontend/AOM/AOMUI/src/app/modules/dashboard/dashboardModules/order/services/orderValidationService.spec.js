describe('orderValidationService Tests', function () {
    "use strict";

    var priceConfig = {
        "pricingUnitCode": "MT",
        "pricingCurrencyCode": "$",
        "minimum": 10,
        "maximum": 100,
        "increment": 0.10,
        "decimalPlaces": 2
    };
    var quantityConfig = {
        "volumeUnitCode": "MT",
        "minimum": 12500,
        "maximum": 35000,
        "increment": 500,
        "decimalPlaces": 0
    };
    var quantityConfigFractionalIncrement = {
        "volumeUnitCode": "MT",
        "minimum": 12500,
        "maximum": 35000,
        "increment": 0.5,
        "decimalPlaces": 2
    };

    beforeEach(module('ui.router'));
    beforeEach(module('argusOpenMarkets.dashboard'));

    it('price greater than max price results fails validation', inject(function (orderValidationService) {
        var price = 999;
        var res = orderValidationService.validateOrderPrice(price, priceConfig);
        expect(res.isValid).toBeFalsy();
        expect(res.invalidReason).toEqual('Price must not exceed 100');
    }));

    it('price less than min price results fails validation', inject(function (orderValidationService) {
        var price = 5;
        var res = orderValidationService.validateOrderPrice(price, priceConfig);
        expect(res.isValid).toBeFalsy();
        expect(res.invalidReason).toEqual('Price must be at least 10');
    }));

    it('price that violates price increment fails validation', inject(function (orderValidationService) {
        var price = 50.05;
        var res = orderValidationService.validateOrderPrice(price, priceConfig);
        expect(res.isValid).toBeFalsy();
        expect(res.invalidReason).toEqual('Price must be in increments of 0.1');
    }));

    it('price that is a multiple of price stepping and between min and max values passes validation',
        inject(function (orderValidationService) {
            var price = 50.5;
            var res = orderValidationService.validateOrderPrice(price, priceConfig);
            expect(res.isValid).toBeTruthy();
            expect(res.invalidReason).toEqual('');
        }));

    it('quantity greater than max quantity results fails validation', inject(function (orderValidationService) {
        var quantity = 50000;
        var res = orderValidationService.validateOrderQuantity(quantity, quantityConfig);
        expect(res.isValid).toBeFalsy();
        expect(res.invalidReason).toEqual('Quantity must not exceed 35000');
    }));

    it('quantity less than min quantity results fails validation', inject(function (orderValidationService) {
        var quantity = 1000;
        var res = orderValidationService.validateOrderQuantity(quantity, quantityConfig);
        expect(res.isValid).toBeFalsy();
        expect(res.invalidReason).toEqual('Quantity must be at least 12500');
    }));

    it('quantity that violates quantity increment fails validation', inject(function (orderValidationService) {
        var quantity = 15100;
        var res = orderValidationService.validateOrderQuantity(quantity, quantityConfig);
        expect(res.isValid).toBeFalsy();
        expect(res.invalidReason).toEqual('Quantity must be in lots of 500');
    }));

    it('quantity that violates quantity increment due to decimal point precision', inject(function (orderValidationService) {
        var quantity = 12500.1;
        var res = orderValidationService.validateOrderQuantity(quantity, quantityConfig);
        expect(res.isValid).toBeFalsy();
        expect(res.invalidReason).toEqual('Quantity must be in lots of 500');
    }));

    it('quantity that is a multiple of quantity stepping and between min and max values passes validation',
        inject(function (orderValidationService) {
            var quantity = 15500;
            var res = orderValidationService.validateOrderQuantity(quantity, quantityConfig);
            expect(res.isValid).toBeTruthy();
            expect(res.invalidReason).toEqual('');
        }));

    it('quantity with fractional stepping is rejected if it violates stepping',
        inject(function (orderValidationService) {
            var quantity = 15000.1;
            var res = orderValidationService.validateOrderQuantity(quantity, quantityConfigFractionalIncrement);
            expect(res.isValid).toBeFalsy();
            expect(res.invalidReason).toEqual('Quantity must be in lots of 0.5');
        }));

    it('quantity with fractional stepping is accepted if it does not violate stepping',
        inject(function (orderValidationService) {
            var quantity = 15000.5;
            var res = orderValidationService.validateOrderQuantity(quantity, quantityConfigFractionalIncrement);
            expect(res.isValid).toBeTruthy();
            expect(res.invalidReason).toEqual('');
        }));

    it('quantity with more decimal places than allowed should fail validation',
        inject(function (orderValidationService) {
            var quantity = '15000.000';
            var res = orderValidationService.validateOrderQuantity(quantity, quantityConfigFractionalIncrement);
            expect(res.isValid).toBeFalsy();
            expect(res.invalidReason).toEqual('Quantity must be to 2 decimal places');
        }));
});
