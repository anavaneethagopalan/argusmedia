﻿angular.module('argusOpenMarkets.dashboard')
    .controller('newOrderController',
        function newOrderController($scope,
                                    productConfigurationService,
                                    tenorId,
                                    userOrderService,
                                    $modalInstance,
                                    userService,
                                    $state,
                                    orderType,
                                    productId,
                                    orderValidationService,
                                    userApi,
                                    executionMode,
                                    orderDialogService,
                                    productMetadataService) {
            "use strict";

            $scope.coBrokering = false;
            $scope.orderType = orderType;
            $scope.actionButtonClicked = false;
            $scope.productLabels = productConfigurationService.getProductConfigurationLabels(productId);
            $scope.executionMode = executionMode;
            $scope.isExternalOrder = executionMode === "External";

            var getBrokersOrPrincipals = function () {
                var buyOrSell = $scope.orderType === "Bid" ? "buy" : "sell";
                // Get the information for the modal dropdowns
                if ($scope.IsBroker()) {
                    $scope.executeEnabled = false; //need to select a principal
                    userApi.getPrincipals(productId, true, buyOrSell).then(
                        function (data) {
                            $scope.principals = data;
                            $('#principals').trigger("chosen:updated");
                        },
                        function (error) {
                            userApi.formatErrorResponse(error);
                            $scope.exit();
                        }
                    );
                }
                else {
                    userApi.getBrokers(productId, true, buyOrSell).then(
                        function (data) {
                            var noBroker = {
                                "id": -100,
                                "name": "Bilateral Only (No Broker)",
                                "isDeleted": false,
                                "shortCode": "BIL",
                                "userId": -1,
                                "nameAndCode": "Bilateral Only (No Broker)"
                            };
                            data.unshift(noBroker);
                            $scope.brokers = data;
                        },
                        function (error) {
                            userApi.formatErrorResponse(error);
                            $scope.exit();
                        }
                    );
                }
            };

            $scope.$watch('selectedBroker', function (a, b) {
                var selectionChange = (a !== b);
                updateCoBrokeringUIElements(selectionChange);
            });

            $scope.$watch('order.price', function (price) {

                if (price === undefined) {
                    if ($scope) {
                        if ($scope.newOrderForm) {
                            if ($scope.newOrderForm.price) {
                                price = $scope.newOrderForm.price.$viewValue;
                            }
                        }
                    }
                }
                $scope.priceSpecification.isValid(price);
            });

            $scope.$watch('order.quantity', function (quantity) {

                if (quantity === undefined) {
                    if ($scope) {
                        if ($scope.newOrderForm) {
                            if ($scope.newOrderForm.quantity) {
                                quantity = $scope.newOrderForm.quantity.$viewValue;
                            }
                        }
                    }
                }
                $scope.quantitySpecification.isValid(quantity);
            });

            $scope.$watch('orderType', function (a, b) {
                getBrokersOrPrincipals();
            });


            $scope.validPrinicipalSelected = false;
            $scope.invalidPrincipalReason = "You must select a principal";

            $scope.showInvalidPrincipal = {
                show: false
            };

            $scope.itemdisabled = function () {
                return false;
            };

            $scope.$on('$stateChangeSuccess', function () {
                if ($state.current.name !== 'Dashboard') {
                    $modalInstance.dismiss('cancel');
                }
            });

            $scope.setOrderType = function (orderType) {
                $scope.orderType = orderType;
            };

            $scope.productConfig = angular.copy(productConfigurationService.getProductConfigurationById(productId));
            angular.forEach($scope.productConfig.contractSpecification.tenors, function (tenor) {
                if (tenor.id === tenorId) {
                    $scope.tenor = tenor;
                }
            });

            $scope.quantities = angular.copy(productConfigurationService.getCommonProductQuantities(productId));
            $scope.setQuantity = function (volume) {
                $scope.order.quantity = volume;
            };

            $scope.order = {
                tenor: {id: tenorId},
                productId: productId,
                notes: "",
                quantity: $scope.productConfig.contractSpecification.volume.default
            };

            $scope.order.metaData = productMetadataService.synchronizeMetadata([], productId);
            $scope.metaDataCollection = productMetadataService.getMetadataCollection($scope.order.metaData, productId);

            $scope.exit = function () {
                $modalInstance.dismiss('exit');
            };

            $scope.organisation = userService.getUsersOrganisation().legalName;

            $scope.IsBroker = function () {
                return userService.isBroker();
            };

            if (!$scope.IsBroker() || $scope.isExternalOrder) {
                $scope.validPrinicipalSelected = true;
            }

            $scope.onPrincipalChange = function (opt) {
                if (opt === null) {
                    $scope.validPrinicipalSelected = false;
                    $scope.showInvalidPrincipal.show = true;
                } else {
                    $scope.validPrinicipalSelected = true;
                    $scope.showInvalidPrincipal.show = false;
                }
                $scope.selectedPrincipal = opt;
            };

            $scope.onBrokerChange = function (opt) {
                $scope.selectedBroker = opt;
            };


            var hasRequiredCreateNewOrderPrivileges = function () {
                var requiredPrivileges = $scope.isExternalOrder ?
                    ["ExternalOrder_Create_Principal", "ExternalOrder_Create_Broker"] :
                    ["Order_Create_Principal", "Order_Create_Any", "Order_Create_Broker"];
                return userService.hasProductPrivilege(productId, requiredPrivileges);
            };

            var isCoBrokeringDisabledForProductOrSelection = function () {
                var isDisabled = true;

                if (isCoBrokeringVisibleForProduct()) {
                    if ($scope.IsBroker()) {
                        isDisabled = false;
                    }
                    else {
                        if ($scope.selectedBroker) {
                            if ($scope.selectedBroker.id > 0) {
                                isDisabled = false;
                            }
                        }
                    }
                }
                return isDisabled;
            };

            var isCoBrokeringVisibleForProduct = function () {

                var isVisible = false;

                if ($scope.productConfig) {
                    if ($scope.productConfig.coBrokering) {
                        isVisible = true;
                    }
                }
                return isVisible;
            };

            $scope.coBrokering = $scope.IsBroker() && isCoBrokeringVisibleForProduct() ? true : false;//default initial value

            var updateCoBrokeringUIElements = function (changeMade) {
                var previousCoBrokeringDisabled = $scope.coBrokeringDisabled;
                $scope.coBrokeringVisible = isCoBrokeringVisibleForProduct();
                $scope.coBrokeringDisabled = isCoBrokeringDisabledForProductOrSelection();

                if ($scope.coBrokeringVisible === true && $scope.coBrokeringDisabled === false &&
                    previousCoBrokeringDisabled === true) {
                    $scope.coBrokering = true;
                }

                if ($scope.coBrokeringVisible === false || $scope.coBrokeringDisabled === true) {
                    if (changeMade) {
                        $scope.coBrokering = false;
                    }
                }
            };

            updateCoBrokeringUIElements();

            $scope.newOrder = function () {

                if ($scope.actionButtonClicked) {
                    return;
                }

                $scope.actionButtonClicked = true;

                if (!hasRequiredCreateNewOrderPrivileges()) {
                    return;
                }

                $scope.order.orderType = $scope.orderType;
                $scope.order.executionMode = $scope.executionMode;
                if ($scope.IsBroker()) {
                    if (!$scope.isExternalOrder && $scope.selectedPrincipal === null) {
                        $scope.validPrinicipalSelected = false;
                    }
                    $scope.order.brokerOrganisationId = userService.getUsersOrganisation().id;
                    $scope.order.brokerOrganisationName = userService.getUsersOrganisation().name;
                    $scope.order.brokerName = userService.getUser().userOrganisation.name;
                    $scope.order.brokerId = userService.getUser().id;
                    $scope.order.brokerRestriction = "Specific";

                    if ($scope.selectedPrincipal) {
                        $scope.order.principalUserId = $scope.selectedPrincipal.userId;
                        $scope.order.principalOrganisationId = $scope.selectedPrincipal.id;
                    }
                } else {
                    $scope.order.principalUserId = userService.getUser().id;
                    $scope.order.principalOrganisationId = userService.getUsersOrganisation().id;
                    if ($scope.selectedBroker) {
                        $scope.order.brokerRestriction = $scope.getBrokerRestriction($scope.selectedBroker.id);

                        if ($scope.selectedBroker.id === -100) {
                            $scope.order.brokerOrganisationId = null;
                        } else {
                            $scope.order.brokerOrganisationId = $scope.selectedBroker.id;
                            $scope.order.brokerId = $scope.selectedBroker.userId;
                            $scope.order.brokerName = $scope.selectedBroker.name;
                        }
                    } else {
                        $scope.order.brokerRestriction = "Any";
                    }
                }

                $scope.order.coBrokering = $scope.coBrokering;

                $scope.order.notes = $scope.order.notes.trim();
                userOrderService.sendNewOrderMessage($scope.order);
                $modalInstance.dismiss('exit');
            };

            $scope.getBrokerRestriction = function (brokerOrganisationId) {

                if (brokerOrganisationId === -100) {
                    return "None";
                }

                if (brokerOrganisationId > 0) {
                    return "Specific";
                }

                return "Any";
            };

            $scope.showPrincipal = function () {
                if (orderDialogService.hasNeededOrderCreatePrivilegesAsPrincipal(productId, $scope.executionMode)) {
                    $scope.order.principalOrganisationId = userService.getUsersOrganisation().id;
                    $scope.order.principalUserId = userService.getUserId();
                    return true;
                }
                return false;
            };

            $scope.showBroker = function () {
                if (orderDialogService.hasNeededOrderCreatePrivilegesAsBroker(productId, $scope.executionMode)) {
                    $scope.order.brokerId = userService.getUserId();
                    $scope.order.brokerOrganisationId = userService.getUsersOrganisation().id;
                    return true;
                }
                return false;
            };

            var parseInteger = function(value){

                if(value){
                    return parseInt(value);
                }

                return 0;
            };

            var startDateOffset = parseInteger($scope.tenor.deliveryStartDate);
            var endDateOffset = parseInteger($scope.tenor.deliveryEndDate);
            var defaultStartDate = parseInteger($scope.tenor.defaultStartDate);
            var defaultEndDate = parseInteger($scope.tenor.defaultEndDate);

            var today = new Date();
            $scope.minDate = today.addDays(startDateOffset);
            var maxdate = new Date();

            $scope.maxDate = maxdate.addDays(endDateOffset);
            var startDay = new Date();
            $scope.order.deliveryStartDate = startDay.addDays(defaultStartDate);
            var endDay = new Date();
            $scope.order.deliveryEndDate = endDay.addDays(defaultEndDate);
            $scope.minRange = $scope.tenor.minimumDeliveryRange;

            var productConfigPrice = $scope.productConfig.contractSpecification.pricing;
            $scope.invalidPriceReason = "";
            $scope.priceSpecification = {
                decimalPlaces: productConfigPrice.decimalPlaces,
                isValid: function (price) {

                    if ($scope.order.price === undefined) {
                        if (typeof(price) === 'object') {
                            $scope.order.price = price.$viewValue;
                        }
                    }

                    var validity = orderValidationService.validateOrderPrice($scope.order.price, productConfigPrice);

                    if (typeof(price) !== 'object') {
                        if ($scope) {
                            if ($scope.newOrderForm) {
                                price = $scope.newOrderForm.price;
                            }
                        }
                    }

                    if (price && typeof(price) === 'object') {
                        price.$setValidity("price", validity.isValid);
                        $scope.invalidPriceReason = validity.invalidReason;
                    }
                }
            };

            $scope.notesCharacterLimit = 250;
            $scope.notesCharactersLeft = $scope.notesCharacterLimit;
            $scope.onNotesChange = function () {
                if ($scope.order.notes.length > $scope.notesCharacterLimit) {
                    $scope.order.notes = $scope.order.notes.slice(0, $scope.notesCharacterLimit);
                }
                $scope.notesCharactersLeft = $scope.notesCharacterLimit - $scope.order.notes.length;
            };

            var productConfigQuantity = $scope.productConfig.contractSpecification.volume;
            $scope.invalidQuantityReason = "";
            $scope.quantitySpecification = {
                decimalPlaces: productConfigQuantity.decimalPlaces,
                isValid: function (quantity) {
                    var validity;

                    if ($scope.order.quantity === undefined) {
                        if (typeof(quantity) === 'object') {
                            $scope.order.quantity = quantity.$viewValue;
                        }
                    }

                    validity = orderValidationService.validateOrderQuantity($scope.order.quantity, productConfigQuantity);
                    $scope.invalidQuantityReason = validity.invalidReason;

                    if (typeof(quantity) !== 'object') {
                        if ($scope) {
                            if ($scope.newOrderForm) {
                                quantity = $scope.newOrderForm.quantity;
                            }
                        }
                    }

                    if (quantity && typeof(quantity) === 'object') {
                        quantity.$setValidity("quantity", validity.isValid);
                    }
                }
            };

            $scope.submitForm = function (isValid) {

            };
        });