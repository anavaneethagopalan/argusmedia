﻿mockUserConfig = {
    'fullName': 'Nathan Bellamore',
    role: 'Trader'
};

describe('header Controller', function(){
    var name = {};
    var scope,
        controller,
        headerController,
        mockWindow = {
            location: {
                reload: function(){}
            }
        };

    beforeEach(function () {
        module('argusOpenMarkets.shared');
    });

    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.dashboard', function ($stateProvider, $provide) {
        stateProvider = $stateProvider;
        //$provide.value('$window',mockWindow);
    }));

    beforeEach(inject(function($controller, $rootScope, mockSocketProvider,mockUserService){
        controller = $controller;

        scope = $rootScope;
        scope.mockSocketProvider=mockSocketProvider;
        headerController = controller('headerController', {
            '$scope': scope,
            'userService': mockUserService,
            'socketProvider':mockSocketProvider,
            $window:mockWindow
        });

    }));

    it('header controller should contain a user object with the users fullname', function(){
        expect(scope.user.name).toEqual('Trader One');
    });

    it('should call disconnect on socket provider when log out is called',function(){

        spyOn(scope.mockSocketProvider,'disconnect');

        scope.logout();

        expect(scope.mockSocketProvider.disconnect).toHaveBeenCalled();
    });
});