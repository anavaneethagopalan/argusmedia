﻿describe('Header Controller e2e tests', function() {

    xit('Displays users detals on the header bar', function() {
        browser.get('http://localhost/#/Dashboard');
        var fullNameTag = element(by.id('header-logged-on-user'))
        expect(fullNameTag.getText()).toEqual('John Adams');
    });

});