﻿angular.module('argusOpenMarkets.dashboard')
    .controller('headerController', function headerController(
        $scope,
        $http,
        $state,
        userService,
        socketProvider,
        $window,
        wsConfigurationService
        )
    {
        var endpoint,
            url,
            port,
            api,
            protocol,
            wsConfiguration,
            fullGetHelpFileUrl;

        $scope.user = userService.getUser();
        $scope.state= $state.current.name;




        wsConfigurationService.promise().then(function() {
            wsConfiguration = wsConfigurationService.getWsConfiguration();
            endpoint = wsConfiguration.endpoint;
            api = wsConfiguration.api;

            protocol = wsConfiguration.protocol;
            if(wsConfiguration.port !== ""){
                port =":" + wsConfiguration.port;
                url = protocol + "://" + endpoint + port + "/" + api + "/" ;
            }
            else{
                url = protocol + "://" + endpoint + "/" + api + "/";
            }
            //url = protocol + "://" + endpoint + port + "/";

            fullGetHelpFileUrl = url + wsConfiguration.getHelpFile;
        });

        if(userService.getUser().name){
            $scope.username = userService.getUser().name;
        }
        if(userService.getUsersOrganisation().name){
            $scope.userOrganisationName = userService.getUsersOrganisation().name;
        }

        $scope.showSettings = function(){

            return !userService.isCmeUser();
        };

        $scope.logout= function(){
            socketProvider.disconnect();
            $window.onbeforeunload = undefined;
            $window.location.reload();
        };

        $scope.downloadHelpFile = function(){

            var url = fullGetHelpFileUrl + '?token=' + userService.getSessionToken() + '&uid=' + userService.getUserId();

            window.open(url, '_blank');
        };
    });