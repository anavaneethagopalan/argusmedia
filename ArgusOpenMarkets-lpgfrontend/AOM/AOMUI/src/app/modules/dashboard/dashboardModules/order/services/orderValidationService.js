angular.module('argusOpenMarkets.dashboard').
    service("orderValidationService", function orderValidationService(productValidationService){
        "use strict";

        var validateOrderValueImpl = function(val, config, description, incrementName) {
            var res = {
                isValid: true,
                invalidReason: ""
            };
            if (config.minimum > val) {
                res.invalidReason = description + " must be at least " + config.minimum;
                res.isValid = false;
            } else if (val > config.maximum) {
                res.invalidReason = description + " must not exceed " + config.maximum;
                res.isValid = false;
            } else if (productValidationService.violatesSteppingIncrement(val, config.increment)) {
                res.invalidReason = description + " must be in " + incrementName + " of " + config.increment;
                res.isValid = false;
            } else if(productValidationService.violatesNumberDecimalPlaces(val, config.decimalPlaces)){
                res.invalidReason = description + " must be to " + config.decimalPlaces + " decimal places";
                res.isValid = false;
            }
            return res;
        };

        var validateOrderPrice = function(price, productConfigPrice) {
            return validateOrderValueImpl(price, productConfigPrice, "Price", "increments");
        };

        var validateOrderQuantity = function(quantity, productConfigQuantity) {
            return validateOrderValueImpl(quantity, productConfigQuantity, "Quantity", "lots");
        };

        return{
            validateOrderPrice: validateOrderPrice,
            validateOrderQuantity: validateOrderQuantity
        };
    });
