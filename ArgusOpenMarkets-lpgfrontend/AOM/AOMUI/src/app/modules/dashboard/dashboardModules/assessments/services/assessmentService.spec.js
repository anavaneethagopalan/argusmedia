describe('Assessment service tests', function () {
    "use strict";

    var assessmentServiceTest;
    var subscriber;
    var mockMessageSubscriptionService2;
    var mockModal = {
        open: sinon.spy()
    };
    var mockProductConfigurationService = {
        getAllSubscribedProductIds: function () {
            return [1, 2]
        }
    };

    beforeEach(module('ui.router', function ($locationProvider) {
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('argusOpenMarkets.dashboard');
    });

    beforeEach(function () {
        module(function ($provide) {
            var $injector = angular.injector(['argusOpenMarkets.shared']);
            var mockUserService = $injector.get('mockUserService');
            mockMessageSubscriptionService2 = $injector.get('mockMessageSubscriptionService2');
            $provide.value('$modal', mockModal);
            $provide.value('userService', mockUserService);
            $provide.value('productConfigurationService', mockProductConfigurationService);
            $provide.value('messageSubscriptionService2', mockMessageSubscriptionService2);
        });
    });


    beforeEach(inject(function (assessmentService) {
        assessmentServiceTest = assessmentService;
    }));

    it('should add a new assessment', function () {

        var assessment = {"productId": 1, "assessment": {"productId": 1}, "message": {"messageType": "Assessment"}};

        expect(assessmentServiceTest.getAssessments().length).toEqual(0);
        assessmentServiceTest.registerSubscriberForAssessments(1, function () {
        });
        mockMessageSubscriptionService2.mockOnMessage("AOM/Products/" + 1 + "/Assessment", assessment);
        expect(assessmentServiceTest.getAssessments().length).toEqual(1);
    });

    it('should replace or add new assessment', function () {
        var assessment1 = {"productId": 1, "assessment": {"productId": 1}, "message": {"messageType": "Assessment"}};
        var assessment2 = {"productId": 2, "assessment": {"productId": 2}, "message": {"messageType": "Assessment"}};

        expect(assessmentServiceTest.getAssessments().length).toEqual(0);
        assessmentServiceTest.registerSubscriberForAssessments(1, function () {
        });
        mockMessageSubscriptionService2.mockOnMessage("AOM/Products/" + 1 + "/Assessment", assessment1);
        mockMessageSubscriptionService2.mockOnMessage("AOM/Products/" + 1 + "/Assessment", assessment1);
        mockMessageSubscriptionService2.mockOnMessage("AOM/Products/" + 2 + "/Assessment", assessment2);
        expect(assessmentServiceTest.getAssessments().length).toEqual(2);
    });

    it('should get correct assessment status display name', function () {
        expect(assessmentServiceTest.getAssessmentStatusDisplayName("Final")).toEqual("FINAL");
        expect(assessmentServiceTest.getAssessmentStatusDisplayName("Corrected")).toEqual("CORRECTED");
        expect(assessmentServiceTest.getAssessmentStatusDisplayName("RunningVwa")).toEqual("RUNNING VWA");
        expect(assessmentServiceTest.getAssessmentStatusDisplayName("")).toEqual("N/A");
    });

    it('should send assessment through userservice to user topic on new assessment', inject(function (userService) {
        var assessment = {};
        spyOn(userService, 'sendStandardFormatMessageToUsersTopic');
        assessmentServiceTest.sendNewAssessment(assessment);
        expect(userService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith('Assessment', 'Create', assessment);
    }));

    it('should send assessment through userservice to user topic on refresh assessment', inject(function (userService) {
        var assessment = {};
        spyOn(userService, 'sendStandardFormatMessageToUsersTopic');
        assessmentServiceTest.sendRefreshAssessment(assessment);
        expect(userService.sendStandardFormatMessageToUsersTopic).toHaveBeenCalledWith('Assessment', 'Refresh', assessment);
    }));

    it('should register subscriber for assessments', function () {
        var subscriberId = 1;
        expect(assessmentServiceTest.getSubscribersToAssessmentsCount()).toEqual(0);
        assessmentServiceTest.registerSubscriberForAssessments(subscriberId, (function () {
        }));
        expect(assessmentServiceTest.getSubscribersToAssessmentsCount()).toEqual(1);
    });

    it('should remove subscriber if already in array', function () {
        var subscriberId = 1;
        expect(assessmentServiceTest.getSubscribersToAssessmentsCount()).toEqual(0);
        assessmentServiceTest.registerSubscriberForAssessments(subscriberId, (function () {
        }));
        expect(assessmentServiceTest.getSubscribersToAssessmentsCount()).toEqual(1);
        assessmentServiceTest.registerSubscriberForAssessments(subscriberId, (function () {
        }));
        expect(assessmentServiceTest.getSubscribersToAssessmentsCount()).toEqual(1);
    });

    it('should unRegister subscriber for assessments', function () {
        var subscriberId = 1;
        expect(assessmentServiceTest.getSubscribersToAssessmentsCount()).toEqual(0);
        assessmentServiceTest.registerSubscriberForAssessments(subscriberId, (function () {
        }));
        expect(assessmentServiceTest.getSubscribersToAssessmentsCount()).toEqual(1);
        assessmentServiceTest.unregisterSubscriberForAssessments(subscriberId);
        expect(assessmentServiceTest.getSubscribersToAssessmentsCount()).toEqual(0);
    });

    it('should notify subscriber of assessment changes', function () {
        var subscriberId = 1;
        var actionHasBeenCalled = false;
        var assessment = {"productId": 1, "assessment": {"productId": 1}, "message": {"messageType": "Assessment"}};
        var subscriberAction = function () {
            actionHasBeenCalled = true
        };
        assessmentServiceTest.registerSubscriberForAssessments(subscriberId, subscriberAction);
        mockMessageSubscriptionService2.mockOnMessage("AOM/Products/" + 1 + "/Assessment", assessment);
        expect(actionHasBeenCalled).toBe(true);
    });

    it('should return the default price class when calling getPriceClass with a null assessment', function () {
        var result = assessmentServiceTest.getPriceClass(null);
        expect(result).toEqual('assessment-item-price-level');
    });

    it('should return the price moved up class when calling getPriceClass for an assessment when the trend is upwards', function () {

        var assessment = {trendFromPreviousAssesment: 'Upward'},
            result = assessmentServiceTest.getPriceClass(assessment);

        expect(result).toEqual('assessment-item-price-up');
    });

    it('should return the price moved down class when calling getPriceClass for an assessment when the trend is downward', function () {

        var assessment = {trendFromPreviousAssesment: 'Downward'},
            result = assessmentServiceTest.getPriceClass(assessment);

        expect(result).toEqual('assessment-item-price-down');
    });

    it('should return the price moved down class when calling getPriceClass for an assessment when the trend is downward lower case', function () {

        var assessment = {trendFromPreviousAssesment: 'downward'},
            result = assessmentServiceTest.getPriceClass(assessment);

        expect(result).toEqual('assessment-item-price-down');
    });

    it('should return the price level class when calling getPriceClass for an assessment when the trend is neither upwards or downwards', function () {

        var assessment = {trendFromPreviousAssesment: 'Level'},
            result = assessmentServiceTest.getPriceClass(assessment);

        expect(result).toEqual('assessment-item-price-level');
    });

    it('should return n/a when a null status is passed into getAssessmentStatusDisplayName', function () {
        var status = assessmentServiceTest.getAssessmentStatusDisplayName(null);
        expect(status).toEqual('N/A');
    });
});



