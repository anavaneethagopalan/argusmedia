﻿angular.module('argusOpenMarkets.dashboard').service("dashboardConfigurationService",
    function dashboardConfigurationService($http, helpers, userDashboardConfigurationService, productConfigurationService) {

        var defaultTileConfig = null;
        var currentUserTileConfig = null;
        var productIds = null;
        userDashboardConfigurationService.promise.then(function () {
            currentUserTileConfig = userDashboardConfigurationService.getUserConfiguration();
        }).then(function () {
            productConfigurationService.configReceived.then(function () {
                productIds = productConfigurationService.getAllSubscribedProductIds();
                angular.forEach(currentUserTileConfig, addProductTitlesToTile);
            });
        });


        var addProductTitlesToTile = function (userConfigItem) {
            var title,
                displayOrder;

            userConfigItem.productTitles = [];
            if (userConfigItem.tile.tileType === "assessments") {
                userConfigItem.productTitles.push("All");
            }
            else {
                for (var x = 0; x < userConfigItem.productIds.length; x++) {
                    title = productConfigurationService.getProductTitleById(userConfigItem.productIds[x]);
                    if (title) {
                        userConfigItem.productTitles.push(title);
                    }

                    if (userConfigItem.tile.tileType === 'bidAskStack') {
                        userConfigItem.displayOrder = productConfigurationService.getProductDisplayOrder(userConfigItem.productIds[x]);
                    } else {
                        // Set to a high value.
                        userConfigItem.displayOrder = 100000;
                    }
                }
            }
        };


        var promise = $http.get('assets/defaultTileConfig.json').success(function (data) {
            defaultTileConfig = data;
        });

        var isNewTile = function (userTileConfigItem) {
            if (userTileConfigItem.tile.position.row === "default" || userTileConfigItem.tile.position.column === "default") {
                if (userTileConfigItem.tile.size.x === "default" || userTileConfigItem.tile.size.y === "default") {
                    return true;
                }
            }
            return false;
        };

        var addExistingTiles = function (userTileConfig, dashboardLayout) {

            var lowestDownTile = _.max(dashboardLayout, function (c) {
                return c.tile.row;
            });
            var offset = lowestDownTile.tile ? lowestDownTile.tile.row : 0;
            angular.forEach(userTileConfig, function (userTileConfigItem) {
                if (!isNewTile(userTileConfigItem)) {
                    dashboardLayout.push({
                        tile: {
                            sizeX: userTileConfigItem.tile.size.x,
                            sizeY: userTileConfigItem.tile.size.y,
                            row: userTileConfigItem.tile.position.row,
                            col: (Number(userTileConfigItem.tile.position.col) + offset )
                        }, view: userTileConfigItem.tile.tileType,
                        productIds: userTileConfigItem.productIds,
                        productTitles: userTileConfigItem.productTitles,
                        displayOrder: userTileConfigItem.displayOrder
                    });
                }
            });
            return dashboardLayout;
        };

        var addSingleTile = function (tileType, productIds) {
            var newTile = {
                "productIds": productIds,
                "tile": {
                    "tileType": tileType,
                    "position": {"row": "default", "col": "default"},
                    "size": {"x": "default", "y": "default"}
                }
            };
            addProductTitlesToTile(newTile);
            currentUserTileConfig.push(newTile);
            return this.getDashboardLayout();
        };

        var addNewTiles = function (userTileConfig) {

            var dashboardLayout = [],
                row = 0,
                column = 0,
                nextFreeRow = 0;

            angular.forEach(defaultTileConfig.tileTypes, function (tileType) {
                    angular.forEach(userTileConfig, function (userTileConfigItem) {
                        if (isNewTile(userTileConfigItem)) {
                            if (userTileConfigItem.tile.tileType === tileType.name) {
                                if (column + tileType.sizeX > defaultTileConfig.columns) {
                                    row = nextFreeRow;
                                    column = 0;
                                }
                                dashboardLayout.push({
                                    tile: {
                                        sizeX: tileType.sizeX,
                                        sizeY: tileType.sizeY,
                                        row: row,
                                        col: column
                                    }, view: tileType.view,
                                    productIds: userTileConfigItem.productIds,
                                    productTitles: userTileConfigItem.productTitles,
                                    displayOrder: userTileConfigItem.displayOrder
                                });
                                column += tileType.sizeX;
                                nextFreeRow = Math.max(nextFreeRow, row + tileType.sizeY);
                            }
                        }
                    });
                }
            );

            return dashboardLayout;
        };

        var updateUserTileConfig = function (dashboardLayout) {

            dashboardLayout = recalculateRowIndexes(dashboardLayout);
            var userTileConfig = [];
            try {
                angular.forEach(dashboardLayout, function (tileConfig) {
                    userTileConfig.push({
                        productIds: tileConfig.productIds,
                        tile: {
                            tileType: tileConfig.view,
                            position: {row: tileConfig.tile.row, col: tileConfig.tile.col},
                            size: {x: tileConfig.tile.sizeX, y: tileConfig.tile.sizeY}
                        },
                        productTitles: tileConfig.productTitles
                    });
                });
                currentUserTileConfig = userTileConfig;
            } catch (err) {
                throw "error saving user tile config " + err;
            }
        };

        var saveUserTileConfig = function () {
            userDashboardConfigurationService.saveUserConfiguration(currentUserTileConfig);
        };


        var getDashboardLayout = function () {

            if (helpers.isEmpty(defaultTileConfig)) {
                throw "Default tile type configuration is empty";
            }
            if (helpers.isEmpty(currentUserTileConfig)) {
                throw "User tile configuration is empty";
            }
            var dashboardLayout = addNewTiles(currentUserTileConfig);
            dashboardLayout = addExistingTiles(currentUserTileConfig, dashboardLayout);
            dashboardLayout =  _.sortBy(dashboardLayout, 'displayOrder');
            updateUserTileConfig(dashboardLayout);
            return dashboardLayout;
        };

        var recalculateRowIndexes = function (userDashboardConfig) {
            var totalSizeY = 0,
                sizeY,
                rowValue,
                inRows = {},
                sortedByRow,
                configItems;

            if (!userDashboardConfig || userDashboardConfig.length < 2) {
                return userDashboardConfig;
            }

            // Sort into rows and then add to an array of all the items in that row
            angular.forEach(userDashboardConfig, function (configItem) {
                var row = configItem.tile.row;
                if (!inRows[row]) {
                    inRows[row] = [];
                }
                inRows[row].push(configItem);
            });
            sortedByRow = _.chain(inRows).pairs().sortBy(function (rowItems) {
                return Number(rowItems[0]);
            }).value();

            angular.forEach(sortedByRow, function (rowItems) {
                rowValue = Number(rowItems[0]);
                configItems = rowItems[1];
                sizeY = _.max(_.map(configItems, function (item) {
                    return item.tile.sizeY;
                }));
                if (rowValue !== 0 && rowValue < totalSizeY) {
                    _.each(configItems, function (configItem) {
                        configItem.tile.row = (totalSizeY + 1);
                    });
                }
                totalSizeY += sizeY;
            });

            return userDashboardConfig;
        };


        var BID_ASK_STACK_WIDGET_HEADER_UNITS = 3;
        var BID_ASK_STACK_GRID_HEADER_UNITS = 2;
        var BID_ASK_STACK_GRID_ROW_UNITS = 2;
        var BID_ASK_STACK_GRID_FOOTER_UNITS = 1;

        var determineBidAskTileSizePixels = function (gridRowCount) {
            return (BID_ASK_STACK_WIDGET_HEADER_UNITS +
            BID_ASK_STACK_GRID_HEADER_UNITS +
            (gridRowCount * BID_ASK_STACK_GRID_ROW_UNITS) +
            BID_ASK_STACK_GRID_FOOTER_UNITS);
        };

        var calculateNumberBidsInStack = function (userDashboardConfig) {

            var gridRowCount = 0,
                gridRowSize = 0,
                divideResult;

            for (var i = 0; i < userDashboardConfig.length; i++) {
                if (userDashboardConfig[i].view === "bidAskStack") {
                    gridRowSize = userDashboardConfig[i].tile.sizeY;
                    break;
                }
            }

            gridRowSize = Math.round(gridRowSize);

            switch (gridRowSize) {
                case 12:
                    gridRowCount = 3;
                    break;
                case 17:
                case 18:
                    gridRowCount = 6;
                    break;
                case 29:
                case 28:
                    gridRowCount = 12;
                    break;
                default:
                    gridRowCount = (gridRowSize - BID_ASK_STACK_WIDGET_HEADER_UNITS - BID_ASK_STACK_GRID_HEADER_UNITS - BID_ASK_STACK_GRID_FOOTER_UNITS);
                    gridRowCount = Math.round(gridRowCount / BID_ASK_STACK_GRID_ROW_UNITS);
                    divideResult = (gridRowCount % 6);
                    if (divideResult !== 0) {
                        gridRowCount = gridRowCount + (6 - divideResult);
                    }
                    break;
            }

            return gridRowCount;
        };

        var determineNumberBidsInStack = function () {

            var userDashboardConfig = getDashboardLayout();
            return calculateNumberBidsInStack(userDashboardConfig);
        };

        return {
            promise: promise,
            getDashboardLayout: getDashboardLayout,
            updateUserTileConfig: updateUserTileConfig,
            saveUserTileConfig: saveUserTileConfig,
            addSingleTile: addSingleTile,
            recalculateRowIndexes: recalculateRowIndexes,
            determineBidAskTileSizePixels: determineBidAskTileSizePixels,
            calculateNumberBidsInStack: calculateNumberBidsInStack,
            determineNumberBidsInStack: determineNumberBidsInStack
        };
    });