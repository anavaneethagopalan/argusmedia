﻿beforeEach(function () {
    // When beforeEach is called outside of a `describe` scope, the matchers are
    // available globally. See http://stackoverflow.com/a/11942151/508355
    jasmine.addMatchers({
        toBeAtLeast: function () {
            return {
                compare: function (actual, expected) {
                    var result = {};
                    result.pass = actual >= expected;
                    if (result.pass) {
                        result.message = "Expected " + actual + " to be less than " + expected;
                    } else {
                        result.message = "Expected " + actual + " to be at least " + expected;
                    }
                    return result;
                }
            };
        },
        toBeAtMost: function () {
            return {
                compare: function (actual, expected) {
                    var result = {};
                    result.pass = actual <= expected;
                    if (result.pass) {
                        result.message = "Expected " + actual + " to be greater than " + expected;
                    } else {
                        result.message = "Expected " + actual + " to be at most " + expected;
                    }
                    return result;
                }
            };
        }
    });
});

describe("Dashboard configuration service", function () {
    var $httpBackend, dashboardConfigurationService;
    var mockProductConfigurationService;
    var tileConfig = {
        tileTypes: [
            {name: "bidAskStack", sizeX: 4, sizeY: 2, view: "bidAskStack"},
            {name: "marketTicker", sizeX: 8, sizeY: 3, view: "marketTicker"},
            {name: "newsFeed", sizeX: 8, sizeY: 3, view: "newsFeed"}
        ],
        columns: 12
    };


    var Promise = function () {
        this.then = function (callback) {
            return callback();
        }
    };

    Promise.prototype.then = function (callback) {
        return callback();
    };

    var mockUserDashboardConfigurationService = {
        getUserConfiguration: function () {
            return userDashcoardConfig
        },
        saveUserConfiguration: function () {
        },
        promise: {
            then: function (callback) {
                callback();
                return new Promise();
            }
        }
    };

    var userDashcoardConfig = [
        {
            "productIds": [1],
            "tile": {
                "tileType": "bidAskStack",
                "position": {"row": "default", "col": "0"},
                "size": {"x": "default", "y": "default"}
            }
        },
        {
            "productIds": [2],
            "tile": {
                "tileType": "bidAskStack",
                "position": {"row": "default", "col": "6"},
                "size": {"x": "default", "y": "default"}
            }
        },
        {
            "productIds": [1, 2],
            "tile": {
                "tileType": "marketTicker",
                "position": {"row": "default", "col": "default"},
                "size": {"x": "default", "y": "default"}
            }
        }
    ];


    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);

    }));

    beforeEach(module('argusOpenMarkets.dashboard'));


    beforeEach(module(function ($provide) {
        var $injector = angular.injector(['argusOpenMarkets.shared']);
        mockProductConfigurationService = $injector.get('mockProductConfigurationService');
        mockProductConfigurationService.configReceived = {
            then: function () {
            }
        };
        mockProductConfigurationService.getProductDisplayOrder = function (productId) {
            return 1;
        };
        $provide.value('productConfigurationService', mockProductConfigurationService);
        $provide.value('userDashboardConfigurationService', mockUserDashboardConfigurationService);
    }));


    beforeEach(inject(function (_dashboardConfigurationService_, _$httpBackend_, $q) {
        $httpBackend = _$httpBackend_;
        dashboardConfigurationService = _dashboardConfigurationService_;
        var deferred = $q.defer();
        spyOn(mockProductConfigurationService.configReceived, 'then').and.returnValue({$promise: deferred.promise});
        deferred.resolve();
        spyOn(mockUserDashboardConfigurationService, 'getUserConfiguration').and.callFake(function () {
            return userDashcoardConfig
        });
        spyOn(mockUserDashboardConfigurationService.promise, 'then').and.callFake(function () {
            return {
                then: function (callback) {
                    callback();
                    return new Promise();
                }
            };
        });
    }));

    it('no default tile config throws error', function () {
        expect(function () {
            dashboardConfigurationService.getDashboardLayout(null)
        }).toThrow("Default tile type configuration is empty");
    });

    it('no user tile config throws error', function () {
        $httpBackend.whenGET('assets/defaultTileConfig.json').respond(null);
        dashboardConfigurationService.promise.then(function () {
            expect(function () {
                dashboardConfigurationService.getDashboardLayout(null)
            }).toThrow();
        });
        $httpBackend.flush();
    });

    it('loads new tiles for user', function () {
        var expectedLayout = [{
            tile: {sizeX: 4, sizeY: 2, row: 0, col: 0},
            view: 'bidAskStack',
            productIds: [1],
            productTitles: undefined,
            displayOrder: undefined
        }, {
            tile: {sizeX: 4, sizeY: 2, row: 0, col: 4},
            view: 'bidAskStack',
            productIds: [2],
            productTitles: undefined,
            displayOrder: undefined
        }, {
            tile: {sizeX: 8, sizeY: 3, row: 2, col: 0},
            view: 'marketTicker',
            productIds: [1, 2],
            productTitles: undefined,
            displayOrder: undefined
        }];
        $httpBackend.whenGET('assets/defaultTileConfig.json').respond(tileConfig);
        dashboardConfigurationService.promise.then(function () {
            var dashboardLayout = dashboardConfigurationService.getDashboardLayout();
            expect(dashboardLayout).toEqual(expectedLayout);
        });
        $httpBackend.flush();

    });

    it('loads new tiles on top and old tiles with offset', function () {
        var expectedLayout = [{
            tile: {sizeX: 4, sizeY: 2, row: 0, col: 0},
            view: 'bidAskStack',
            productIds: [1],
            productTitles: undefined,
            displayOrder: undefined
        }, {
            tile: {sizeX: 4, sizeY: 2, row: 0, col: 4},
            view: 'bidAskStack',
            productIds: [2],
            productTitles: undefined,
            displayOrder: undefined
        }, {
            tile: {sizeX: 8, sizeY: 3, row: 2, col: 0},
            view: 'marketTicker',
            productIds: [1, 2],
            productTitles: undefined,
            displayOrder: undefined
        }];

        $httpBackend.whenGET('assets/defaultTileConfig.json').respond(tileConfig);
        dashboardConfigurationService.promise.then(function () {
            var dashboardLayout = dashboardConfigurationService.getDashboardLayout();
            expect(dashboardLayout).toEqual(expectedLayout);
        });
        $httpBackend.flush();

    });

    it('saves', function () {
        var expectedSavedConfig =
            [{
                tile: {sizeX: 4, sizeY: 2, row: 0, col: 0},
                view: 'bidAskStack',
                productIds: [1],
                productTitles: undefined,
                displayOrder: undefined
            }, {
                tile: {sizeX: 4, sizeY: 2, row: 0, col: 4},
                view: 'bidAskStack',
                productIds: [2],
                productTitles: undefined,
                displayOrder: undefined
            }, {
                tile: {sizeX: 8, sizeY: 3, row: 2, col: 0},
                view: 'marketTicker',
                productIds: [1, 2],
                productTitles: undefined,
                displayOrder: undefined
            }];


        var dashboardLayout = [{tile: {sizeX: 4, sizeY: 2, row: 0, col: 0}, view: "bidAskStack", productIds: [1]},
            {tile: {sizeX: 4, sizeY: 2, row: 0, col: 4}, view: "bidAskStack", productIds: [2]},
            {tile: {sizeX: 8, sizeY: 3, row: 3, col: 0}, view: "marketTicker", productIds: [1, 2]}];

        $httpBackend.whenGET('assets/defaultTileConfig.json').respond(tileConfig);
        dashboardConfigurationService.promise.then(function () {
            dashboardConfigurationService.saveUserTileConfig(dashboardLayout);
            var savedLayout = dashboardConfigurationService.getDashboardLayout();
            expect(savedLayout).toEqual(expectedSavedConfig);

        });
        $httpBackend.flush();

    });

    it('should recalculateRowIndexes so that tiles do not overlap when the config items are in row order', function () {
        var i, currentTile, previousTile;
        var userDashboardConfig = [
            {
                "tile": {
                    "sizeY": 17,
                    "row": 0,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 7,
                    "row": 5,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 4,
                    "row": 10,
                    "col": 0
                }
            }];

        userDashboardConfig = dashboardConfigurationService.recalculateRowIndexes(userDashboardConfig);
        for (i = 1; i < userDashboardConfig.length; ++i) {
            currentTile = userDashboardConfig[i].tile;
            previousTile = userDashboardConfig[i - 1].tile;
            expect(currentTile.row).toBeAtLeast(previousTile.row + previousTile.sizeY);
        }
    });

    it('should recalculateRowIndexes so that tiles do not overlap when the config items are not in row order', function () {
        var i, currentTile, previousTile;
        var userDashboardConfig = [
            {
                "tile": {
                    "sizeY": 7,
                    "row": 5,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 17,
                    "row": 0,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 4,
                    "row": 10,
                    "col": 0
                }
            }];

        userDashboardConfig = dashboardConfigurationService.recalculateRowIndexes(userDashboardConfig);
        userDashboardConfig.sort(function (a, b) {
            return a.tile.row - b.tile.row;
        });
        for (i = 1; i < userDashboardConfig.length; ++i) {
            currentTile = userDashboardConfig[i].tile;
            previousTile = userDashboardConfig[i - 1].tile;
            expect(currentTile.row).toBeAtLeast(previousTile.row + previousTile.sizeY);
        }
    });

    it('should recalculateRowIndexes so that tiles do not overlap when the config items are in different columns', function () {
        var i, currentTile, previousTile;
        var userDashboardConfig = [
            {
                "tile": {
                    "sizeY": 7,
                    "row": 0,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 17,
                    "row": 5,
                    "col": 1
                }
            },
            {
                "tile": {
                    "sizeY": 4,
                    "row": 10,
                    "col": 2
                }
            }];

        userDashboardConfig = dashboardConfigurationService.recalculateRowIndexes(userDashboardConfig);
        for (i = 1; i < userDashboardConfig.length; ++i) {
            currentTile = userDashboardConfig[i].tile;
            previousTile = userDashboardConfig[i - 1].tile;
            expect(currentTile.row).toBeAtLeast(previousTile.row + previousTile.sizeY);
        }
    });

    it('should recalculateRowIndexes so that tiles do not overlap when the first row item is not in the first column', function () {
        var i, currentTile, previousTile;
        var userDashboardConfig = [
            {
                "tile": {
                    "sizeY": 7,
                    "row": 0,
                    "col": 1
                }
            },
            {
                "tile": {
                    "sizeY": 17,
                    "row": 5,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 4,
                    "row": 10,
                    "col": 0
                }
            }];

        userDashboardConfig = dashboardConfigurationService.recalculateRowIndexes(userDashboardConfig);
        for (i = 1; i < userDashboardConfig.length; ++i) {
            currentTile = userDashboardConfig[i].tile;
            previousTile = userDashboardConfig[i - 1].tile;
            expect(currentTile.row).toBeAtLeast(previousTile.row + previousTile.sizeY);
        }
    });

    it('should recalculateRowIndexes so that tiles in the same column in the first row are on the same row', function () {
        var userDashboardConfig = [
            {
                "tile": {
                    "sizeY": 7,
                    "row": 0,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 7,
                    "row": 0,
                    "col": 1
                }
            },
            {
                "tile": {
                    "sizeY": 4,
                    "row": 5,
                    "col": 0
                }
            }];

        userDashboardConfig = dashboardConfigurationService.recalculateRowIndexes(userDashboardConfig);
        expect(userDashboardConfig[0].tile.row).toEqual(0);
        expect(userDashboardConfig[1].tile.row).toEqual(0);
        expect(userDashboardConfig[2].tile.row).toBeAtLeast(userDashboardConfig[0].tile.row + userDashboardConfig[0].tile.sizeY);
    });

    it('should recalculateRowIndexes so that tiles in the same column in the second row are on the same row', function () {
        var userDashboardConfig = [
            {
                "tile": {
                    "sizeY": 10,
                    "row": 0,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 7,
                    "row": 5,
                    "col": 0
                }
            },
            {
                "tile": {
                    "sizeY": 7,
                    "row": 5,
                    "col": 1
                }
            }];

        userDashboardConfig = dashboardConfigurationService.recalculateRowIndexes(userDashboardConfig);
        expect(userDashboardConfig[0].tile.row).toEqual(0);
        expect(userDashboardConfig[1].tile.row).toEqual(userDashboardConfig[2].tile.row);
        expect(userDashboardConfig[1].tile.row).toBeAtLeast(userDashboardConfig[0].tile.row + userDashboardConfig[0].tile.sizeY);
    });

    var makeDashboardConfigWithSize = function (size) {

        var userDashboardConfig = [
            {
                "tile": {
                    "sizeX": 1,
                    "sizeY": size,
                    "row": 0,
                    "col": 0
                },
                "view": "bidAskStack",
                "productIds": [
                    1
                ],
                "productTitles": [
                    "Naphtha CIF NWE - Cargoes"
                ],
                "$$hashKey": "object:11"
            }];

        return userDashboardConfig;
    };

    it('calculates the number of bids in the stack based upon the size of the widget being 18', function () {

        var userDashboardConfig = makeDashboardConfigWithSize(18),
            numberBids;

        numberBids = dashboardConfigurationService.calculateNumberBidsInStack(userDashboardConfig);
        expect(numberBids).toEqual(6);
    });

    it('calculates the number of bids in the stack based upon the size of the widget being 12', function () {

        var userDashboardConfig = makeDashboardConfigWithSize(12),
            numberBids;

        numberBids = dashboardConfigurationService.calculateNumberBidsInStack(userDashboardConfig);
        expect(numberBids).toEqual(3);
    });

    it('calculates the number of bids in the stack based upon the size of the widget being 28', function () {

        var userDashboardConfig = makeDashboardConfigWithSize(28),
            numberBids;

        numberBids = dashboardConfigurationService.calculateNumberBidsInStack(userDashboardConfig);
        expect(numberBids).toEqual(12);
    });

    it('calculates the number of bids in the stack based upon the size of the widget being 39', function () {

        var userDashboardConfig = makeDashboardConfigWithSize(39),
            numberBids;

        numberBids = dashboardConfigurationService.calculateNumberBidsInStack(userDashboardConfig);
        expect(numberBids).toEqual(18);
    });
});
