describe('productValidationService Tests', function() {
    "use strict";

    beforeEach(module('ui.router'));
    beforeEach(module('argusOpenMarkets.dashboard'));

    var productValidationService;

    beforeEach(inject(function (_productValidationService_) {
        productValidationService = _productValidationService_;
    }));

    it('value on stepping does not violate stepping', function () {
        var val = 1.11;
        var stepping = 0.01;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeFalsy();
    });

    it('2dp value on 1dp stepping does not violate stepping', function () {
        var val = 1.10;
        var stepping = 0.1;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeFalsy();
    });

    it('undefined value should not violate stepping', function () {
        var val;
        var stepping = 0.1;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeFalsy();
    });

    it('null value should not violate stepping', function () {
        var val = null;
        var stepping = 0.1;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeFalsy();
    });

    it('value just above stepping violates stepping', function () {
        var val = 1.11;
        var stepping = 0.1;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeTruthy();
    });

    it('value just below stepping violates stepping', function () {
        var val = 1.09;
        var stepping = 0.1;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeTruthy();
    });

    it('value with fewer decimal places but on stepping does not violate stepping', function() {
        var val = 1.2;
        var stepping = 0.01;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeFalsy();
    });

    it('value with fewer decimal places but not on stepping violates stepping', function() {
        var val = 1.3;
        var stepping = 0.08;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeTruthy();
    });

    it('value with too many significant figures violates stepping', function () {
        var val = 1.001;
        var stepping = 0.1;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeTruthy();
    });

    it('value with too many significant figures violates integer stepping', function () {
        // See AR-976
        var val = 17500.5;
        var stepping = 500;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeTruthy();
    });

    it('value with too many decimal places violates number decimal places', function () {
        // See AR-976
        var val = 17500.5001;
        var dp = 0;
        expect(productValidationService.violatesNumberDecimalPlaces(val, dp)).toBeTruthy();
    });

    it('value with too few decimal places does not violate the number decimal places', function () {
        // See AR-976
        var val = 17500.50;
        var dp = 4;
        expect(productValidationService.violatesNumberDecimalPlaces(val, dp)).toBeFalsy();
    });

    it('null value does not violate the number decimal places', function () {
        // See AR-976
        var val = null;
        var dp = 4;
        expect(productValidationService.violatesNumberDecimalPlaces(val, dp)).toBeFalsy();
    });

    it('negative value on a stepping does not violate stepping', function() {
        // See AOMK-132
        var val = -1.25;
        var stepping = 0.25;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeFalsy();
    });

    it('negative value not on a stepping violates stepping', function() {
        // See AOMK-132
        var val = -1.21;
        var stepping = 0.25;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeTruthy();
    });

    it('increment of one thousandth does not violate stepping if value divisible by one thounsandth', function() {
        var val = 900.31;
        var stepping = 0.01;
        expect(productValidationService.violatesSteppingIncrement(val, stepping)).toBeFalsy();
    });
});