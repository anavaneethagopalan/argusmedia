angular.module('argusOpenMarkets.dashboard').service("productValidationService", function productValidationService(helpers){

    var floatTolerance = 1e-8;

    var violatesSteppingIncrement = function(value, step) {

        if (helpers.isEmpty(value)) {
            return false;
        }

        var bigValue = Math.abs(value * 10000).toFixed(0);
        var bigStep = Math.abs(step * 10000).toFixed(0);
        return (bigValue % bigStep) !== 0;
    };

    var violatesNumberDecimalPlaces = function(val, maxNumberDecimalPlaces){
        var valPart,
            actualDecimalPlaces = 0;

        if(val) {
            valPart = val.toString().split('.');
            if (valPart[1]) {
                actualDecimalPlaces = valPart[1].length;
            }
        }

        return (actualDecimalPlaces > maxNumberDecimalPlaces);
    };

    return {
        violatesSteppingIncrement: violatesSteppingIncrement,
        violatesNumberDecimalPlaces: violatesNumberDecimalPlaces
    };
});
