﻿angular.module('argusOpenMarkets.dashboard',[
	'argusOpenMarkets.dashboard.bidaskstack',
    'argusOpenMarkets.shared',
    'ngResource',
    'multi-select',
    'ngSanitize'

]).config(function config($stateProvider) {

	$stateProvider.state('Dashboard', {
		url: '/Dashboard',
		views: {
			"": {
				templateUrl: 'src/app/modules/dashboard/views/dashboard.part.html',
				controller: 'dashboardController'
			},
            'header': {
                templateUrl: 'src/app/modules/dashboard/dashboardModules/header/views/header.part.html',
                controller: 'headerController'
            },
            'footer': {
                templateUrl: 'src/app/modules/dashboard/dashboardModules/footer/views/footer.part.html',
                controller: 'footerController'
            },
			"bidAskStack@Dashboard": {
				templateUrl: 'src/app/modules/dashboard/dashboardModules/bidaskstack/views/bidAskStack.part.html',
				controller: 'bidAskStackController'
			},
			"ladder@Dashboard": {
				templateUrl: 'src/app/modules/dashboard/dashboardModules/ladder/views/ladder.part.html'
			},
			"marketTicker@Dashboard": {
                controller: 'marketTickerController',
				templateUrl: 'src/app/modules/dashboard/dashboardModules/marketticker/views/marketTicker.part.html'
            },
            "assessments@Dashboard": {
                controller: 'assessmentController',
                templateUrl: 'src/app/modules/dashboard/dashboardModules/assessments/views/assessmentsContainer.part.html'
			},
			"newsFeed@Dashboard": {
                controller: 'newsFeedController',
				templateUrl: 'src/app/modules/dashboard/dashboardModules/newsfeed/views/newsFeed.part.html'
			}
		},
        resolve:{
            socketProviderConnected:function(socketProvider){
                return socketProvider.onConnect.promise;
            },
            userConnectedToTopic:function(userService){
                return userService.userMessagePromise.promise;
            },
            productConfigReceived:function(productConfigurationService){
                return productConfigurationService.configReceived;
            },
            marketsClosedReceived: function(productConfigurationService){
                return productConfigurationService.marketStatusReceived;
            },
            userDashboardConfigurationServiceResolve: function(userDashboardConfigurationService){
                return userDashboardConfigurationService.promise;
            },
            dashboardConfigurationServiceResolve:function(dashboardConfigurationService){
                return dashboardConfigurationService.promise;
            }
        }
	});
});