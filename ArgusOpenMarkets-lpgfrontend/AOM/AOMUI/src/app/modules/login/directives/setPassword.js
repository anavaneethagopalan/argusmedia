/**
 * Created by tushara.fernando on 03/10/2014.
 */
angular.module('argusOpenMarkets.login').directive('passwordChecker', function($compile) {
    return {
        restrict: 'E',
        scope: {
            password: '=',
            minchars:'@',
            minnumorspecial:'@',
            mincasediff:'@',
            changepasswordvalid:'=',
            temppassword:'='
        },
        templateUrl:'src/app/modules/login/directives/setPassword.part.html',
        replace: true,
        controller: function($scope){
            $scope.password ='';
            $scope.lengthCheck = function(){
                if($scope.password === ''){
                    return false;
                }
                return $scope.password.length >=$scope.minchars;
            };

            $scope.caseCheck = function(){
                if( $scope.password === ''){
                    return false;
                }
                var upper = $scope.password.replace(/[0-9a-z]/g, '').replace(/\W/g, '').length;
                var lower = $scope.password.replace(/[0-9A-Z]/g, '').replace(/\W/g, '').length;
                return (upper>=$scope.mincasediff)&&(lower>=$scope.mincasediff);
            };

            $scope.checkNumberOrSpecial = function(){
                if( $scope.password === ''){
                    return false;
                }
                var number = $scope.password.replace(/[a-zA-Z]/g, '').replace(/\W/g, '').length;
                var special = $scope.password.replace(/[0-9a-zA-Z]/g, '').length;
                return (special>=$scope.minnumorspecial)||(number>=$scope.minnumorspecial);
            };
            $scope.passwordsMatch = function(){
                if($scope.password === ''){
                    return false;
                }
                if($scope.passwordConfirm === ''){
                    return false;
                }
                if($scope.passwordConfirm === $scope.password){
                    return true;
                }

            };

            $scope.passwordsNotTemp = function(){
                if($scope.password === $scope.temppassword ){
                    return false;
                }
                return true;
            };

            $scope.checkPassword = function(){
                $scope.lengthCheckValid = $scope.lengthCheck();
                $scope.caseCheckValid= $scope.caseCheck();
                $scope.checkNumberOrSpecialValid = $scope.checkNumberOrSpecial();
                $scope.passwordsMatchCheckValid = $scope.passwordsMatch();
                $scope.notTempCheckValid = $scope.passwordsNotTemp();

                $scope.changepasswordvalid = $scope.lengthCheckValid && $scope.caseCheckValid && $scope.checkNumberOrSpecialValid && $scope.passwordsMatchCheckValid&&$scope.notTempCheckValid;
            };
        },
        link: function(scope, elem, attr, ctrl) {
            scope.$watch('password',scope.checkPassword);
            scope.$watch('passwordConfirm',scope.checkPassword);
        }
    };
});