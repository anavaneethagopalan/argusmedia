describe('setPasswordTests', function(){
    var $scope,
        $compile,
        el;

    beforeEach(module('src/app/modules/login/directives/setPassword.part.html'));

    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(function () {
        module('toastr');
        module('diffusion');
        module('argusOpenMarkets.shared');
        module('argusOpenMarkets.dashboard');
    });


    beforeEach(module(function($provide){
        $provide.provider('htmlInterceptor', function() {
            this.$get = function () {
                return {
                    request: function (config) {
                        return config;
                    }
                };
            }
        });
    }));

    beforeEach(module('argusOpenMarkets.login', function ($stateProvider) {
    }));


    beforeEach(inject(function(_$compile_,_$rootScope_){
        $scope = _$rootScope_.$new();
        $compile = _$compile_;
        el = angular.element('<password-checker password="password" temppassword="oldPassword" minChars=8 minNumOrSpecial=1 changePasswordValid="changePasswordValid" ></password-checker>')
        $compile(el)($scope);
        $scope.$digest();

    }));



    it('password must be more than x chars - empty', inject(function(){
        var result = (el.scope().$$childHead.lengthCheck() == true);
        expect(result).toEqual(false);
    }));

    it('password must be more than x chars', inject(function(){
        el.scope().$$childHead.password ="aaaaaaaaa";
        expect(el.scope().$$childHead.lengthCheck()).toEqual(true);
    }));


});