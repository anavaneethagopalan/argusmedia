﻿angular.module('argusOpenMarkets.login').controller('resetPasswordController', function resetPasswordController($scope,
                                                                                                                $state,
                                                                                                                $location,
                                                                                                                userService,
                                                                                                                userApi,
                                                                                                                messagePopupsService,
                                                                                                                $timeout) {
    $scope.isSubmitted = false;
    $scope.oldPassword = $location.search().t;
    var user = userService.getUser();
    if (user && user.firstLogin) {
        $scope.oldPassword = user.oldPassword;
    }else{
        // Why are we on the Reset password page - with NO user  We must have an old password
        if(!$scope.oldPassword) {
            $state.transitionTo('Login');
        }
    }
    $scope.timedOut = false;
    $scope.password = '';
    $scope.loginMessage = '';
    $scope.loginFailed = false;
    $scope.loading = false;
    $scope.changePasswordValid = false;

    $scope.reset = function () {
        var data,
            timeoutFunction;

        if (!$scope.loading && $scope.changePasswordValid) {
            var credentials;

            if (validPassword()) {
                $scope.loading = true;
                $scope.loginMessage = "";
                $scope.loginFailed = false;

                if (user && user.firstLogin) {
                    credentials = {
                        username: user.username,
                        oldPassword: angular.copy(user.oldPassword),
                        newPassword: $scope.password
                    };
                    timeoutFunction = $timeout($scope.requestTimedOut, 10000);

                    userApi.getChangeDetailsResource().post(credentials).$promise.then(function (data) {
                        // Cancel the timeout function - we've got the data
                        $timeout.cancel(timeoutFunction);
                        // User has been authenticated with the web service.
                        $scope.onChangedPassword(data);
                        $scope.loading = false;
                    });
                }
                else {
                    credentials = {oldPassword: $scope.oldPassword, newPassword: $scope.password};
                    timeoutFunction = $timeout($scope.requestTimedOut, 10000);

                    userApi.getResetPasswordResource().post(credentials).$promise.then(function (data) {
                        $scope.onChangedPassword(data);
                        $scope.loading = false;

                    });
                }
            }
        }
        messagePopupsService.clear();
    };

    $scope.requestTimedOut = function(){
        // The request has timed out
        $scope.loading = false;
        $scope.isSubmitted = true;
        $scope.timedOut = true;
        loginFailed('An unknown error has occurred.\n\nPlease contact Aom Support on aomsupport@argusmedia.com or call +44(0)2071999430 if the problem persists..');
    };

    $scope.onChangedPassword = function (userAuthResult) {
        if(!$scope.timedOut) {
            if (userAuthResult.isAuthenticated) {
                $scope.isSubmitted = true;
                $scope.loginMessage = "Your password has been changed successfully.\n\nPlease click the following link to Login.";
            }
            else {
                $scope.isSubmitted = false;
                loginFailed(userAuthResult.message.replace("[[EMAILADDRESS]]", "AOMSupport@argusmedia.com"));
            }

            $scope.loading = false;
        }
    };

    function validPassword() {
        return !!$scope.password.length;

    }

    function loginFailed(message) {
        $scope.loginFailed = true;
        $scope.loading = false;
        $scope.loginMessage = message;
    }

    $scope.submitDisabled = function () {
        if ($scope.loading) {
            return true;
        }
        return !$scope.changePasswordValid;
    };

    $scope.submitButtonClass = function () {
        if (!$scope.changePasswordValid) {
            return "disabled";
        }

        return "";
    };
});