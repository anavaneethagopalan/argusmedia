describe('resetPasswordController', function () {
    var resetPasswordController,
        scope,
        deferred,
        mockSocketProvider,
        mockApplicationService = {
            promise: function () {
                return deferred.promise;
            },
            getVersionConfiguration: function () {
                return "12"
            },
            getApplicationDebugMode: function () {
                return true;
            }
        };

    var cookie = "tjf";
    var mockUserApi = {
        getResetPasswordResource: function () {
        },
        formatErrorResponse: function (error) {
            return error;
        }
    };
    var mockCookieService = {
        createCookie: function (c) {
            cookie = c;
        },
        readCookie: function () {
            return cookie;
        },
        eraseCookie: function () {
        }
    };
    var stateProvider = {
        transitionTo: function () {
        }
    };

    var mockMessagePopupsService = {
        clear: function () {
            return null;
        }
    };
    var mockModal = {
        open: function () {
            return true
        }, result: function () {
            return 'exit'
        }
    };

    beforeEach(function () {
        module('toastr');
        module('diffusion');
        module('argusOpenMarkets.shared');
    });


    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.login', function ($provide) {
        $provide.value('$modal', mockModal);
    }));

    beforeEach(inject(function ($controller, $rootScope, $q, helpers, _mockSocketProvider_) {
        scope = $rootScope;
        mockSocketProvider = _mockSocketProvider_;
        deferred = $q.defer();
        spyOn(stateProvider, 'transitionTo');
        spyOn(mockSocketProvider, 'connect');

        spyOn(mockUserApi, 'getResetPasswordResource').and.returnValue({
            post: function () {
                return {$promise: deferred.promise}
            }
        });
        $controller('resetPasswordController', {
            '$scope': scope,
            $state: stateProvider,
            socketProvider: mockSocketProvider,
            userApi: mockUserApi,
            cookieService: mockCookieService,
            helpers: helpers,
            messagePopupsService: mockMessagePopupsService,
            applicationService: mockApplicationService
        });
    }));

    it('should call the reset password rescource', function () {
        scope.changePasswordValid = true;
        scope.password = "p1";
        scope.username = "tjf";
        scope.reset();
        deferred.resolve({isAuthenticated: true, firstLogin: false});
        scope.$digest();
        expect(scope.isSubmitted).toBe(true);
    });

    it('should call set login failed to true if not authenticated', function () {
        scope.changePasswordValid = true;
        scope.password = "p1";
        scope.username = "tjf";
        scope.reset();
        deferred.resolve({isAuthenticated: false, firstLogin: false, message: "error"});
        scope.$digest();
        expect(scope.isSubmitted).toBe(false);
    });
});