﻿
describe('loginScreenControllerTests', function(){
    var loginScreenController,
        scope,
        deferred,
        mockSocketProvider,
        mockPlatformInfoService = {isBrowserSupported:function(){return true;} };
        mockApplicationService = {
            promise:function(){return deferred.promise;},
            getVersionConfiguration:function(){return "12"},
            getApplicationDebugMode: function(){ return true;}
        };
        var cookie = "tjf";
        var mockUserApi={
            getLoginResource:function(){},
            formatErrorResponse:function(error){return error;}
        };
        var mockCookieService = {
            createCookie : function(c){cookie = c;},
            readCookie : function(){return cookie;},
            eraseCookie:function(){}
        };
        var stateProvider = {
            transitionTo: function () {}
        };

    var mockMessagePopupsService = {
        clear: function () { return null; }
    };
    var mockModal = {open: function () {
        return true
    }, result: function() {return 'exit'}};

    beforeEach(function () {
        module('toastr');
        module('diffusion');
        module('argusOpenMarkets.shared');
    });


    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.login', function ( $provide) {
        $provide.value('$modal', mockModal);
    }));

    beforeEach(inject(function($controller, $rootScope,$q,helpers,_mockSocketProvider_){
        scope = $rootScope;
        mockSocketProvider=_mockSocketProvider_;
        deferred = $q.defer();
        spyOn(stateProvider,'transitionTo');
        spyOn(mockSocketProvider,'connect');

        spyOn(mockUserApi,'getLoginResource').and.returnValue({post:function(){return{$promise:deferred.promise}}});
        $controller('loginScreenController', {
            '$scope': scope,
            $state:stateProvider,
            socketProvider:mockSocketProvider,
            userApi: mockUserApi,
            cookieService: mockCookieService,
            helpers: helpers,
            messagePopupsService: mockMessagePopupsService,
            applicationService: mockApplicationService,
            platformInfoService:mockPlatformInfoService
        });
    }));

    it('should set the username from the cookie if there is one', function(){
        mockCookieService.createCookie("tjf");
        expect(scope.username).toBe('tjf');
    });

    it('should set loading to true on login', function(){
        scope.username ="tjf";
        scope.password = "password";
        scope.login();
        expect(scope.loading).toBe(true);
    });

    it('login should call out to the web service', function(){
        scope.username ="tjf";
        scope.password = "password";
        scope.login();
        expect(mockUserApi.getLoginResource).toHaveBeenCalled();
    });

    it('login display the error if login api failed', function(){
        scope.username ="tjf";
        scope.password = "password";
        scope.login();
        deferred.reject('login rejected');
        scope.$digest();
        expect(scope.loginMessage).toEqual('login rejected');
    });

    it('first time login should send you to reset password', function(){
        scope.username ="tjf";
        scope.password = "password";
        scope.login();
        deferred.resolve({isAuthenticated:true,firstLogin:true});
        scope.$digest();
        expect(stateProvider.transitionTo).toHaveBeenCalledWith("ResetPassword");
    });

    it('should set login to failed if authentiaction fails', function(){
        scope.username ="tjf";
        scope.password = "password";
        scope.login();
        deferred.resolve({isAuthenticated:false,firstLogin:false, message:"contact [[EMAILADDRESS]]"});
        scope.$digest();
        expect(scope.loginMessage).toEqual("contact AOMSupport@argusmedia.com");
    });

    it('should try to connect to diffusion if authenticated', function(){
        scope.username ="tjf";
        scope.password = "password";
        scope.login();
        deferred.resolve({isAuthenticated:true,firstLogin:false});
        scope.$digest();
        expect(mockSocketProvider.connect).toHaveBeenCalled();
    });

    it('if connected to diffusion transition to dashboard', function(){
        scope.username ="tjf";
        scope.password = "password";
        scope.login();
        deferred.resolve({isAuthenticated:true,firstLogin:false});
        scope.$digest();
        mockSocketProvider.onConnect();
        expect(stateProvider.transitionTo).toHaveBeenCalledWith("Dashboard");
    });
});