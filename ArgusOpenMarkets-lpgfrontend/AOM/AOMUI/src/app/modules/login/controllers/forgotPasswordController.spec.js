
describe('loginScreenControllerTests', function(){
    var forgotPasswordController,
        scope,
        deferred,
        mockSocketProvider,
        mockApplicationService = {
            promise:function(){return deferred.promise;},
            getVersionConfiguration:function(){return "12"},
            getApplicationDebugMode: function(){return true;}
        };

    var mockUserApi={
        getForgotPasswordResource:function(){},
        formatErrorResponse:function(error){return error;}
    };
    var stateProvider = {
        transitionTo: function () {}
    };

    var mockMessagePopupsService = {
        clear: function () { return null; }
    };
    var mockModal = {open: function () {
        return true
    }, result: function() {return 'exit'}};

    beforeEach(function () {
        module('toastr');
        module('diffusion');
        module('argusOpenMarkets.shared');
    });


    beforeEach(module('ui.router', function ($locationProvider) {
        locationProvider = $locationProvider;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.login', function ( $provide) {
        $provide.value('$modal', mockModal);
    }));

    beforeEach(inject(function($controller, $rootScope,$q,helpers,_mockSocketProvider_){
        scope = $rootScope;
        mockSocketProvider=_mockSocketProvider_;
        deferred = $q.defer();
        spyOn(stateProvider,'transitionTo');
        spyOn(mockSocketProvider,'connect');

        spyOn(mockUserApi,'getForgotPasswordResource').and.returnValue({post:function(){return{$promise:deferred.promise}}});
        forgotPasswordController=  $controller('forgotPasswordController', {
            '$scope': scope,
            $state:stateProvider,
            socketProvider:mockSocketProvider,
            userApi: mockUserApi,
            helpers: helpers,
            messagePopupsService: mockMessagePopupsService,
            applicationService: mockApplicationService
        });
    }));

    it('should call the forgotten password rescource', function(){
        scope.username = "tjf";
        scope.forgotPassword();
        deferred.resolve({isAuthenticated:true,firstLogin:false});
        scope.$digest();
        expect(scope.isSubmitted  ).toBe(true);
    });

    it('should call set login failed to true if not authenticated', function(){
        scope.username = "tjf";
        scope.forgotPassword();
        deferred.resolve({isAuthenticated:false,firstLogin:false, message:"error"});
        scope.$digest();
        expect(scope.loginFailed).toBe(true);
    });

    it('should call set login failed  if timeout', function(){
        scope.username = "tjf";
        scope.forgotPassword();
        deferred.reject({isAuthenticated:false,firstLogin:false, message:"error"});
        scope.$digest();
        expect(scope.loginMessage ).toBe("Thank you for your request.\n\nAn email has been sent to you with a link to a page where you can change your password.");
    });

});