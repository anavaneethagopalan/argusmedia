﻿angular.module('argusOpenMarkets.login').controller('forgotPasswordController', function forgotPasswordController(
    $scope,
    $state,
    $location,
    userApi,
    helpers,
    messagePopupsService)
{
    $scope.isSubmitted = false;
    $scope.username = "";
    $scope.loginMessage = "";
    $scope.loginFailed = false;
    $scope.loading = false;
    $scope.url = $location.absUrl().replace($location.url(), "");

    $scope.forgotPassword = function () {
        if(!$scope.loading) {
            var credentials,
                loginMessage = "Thank you for your request.\n\nAn email has been sent to you with a link to a page where you can change your password.";

            if (validUsername()) {
                $scope.loading = true;
                $scope.loginMessage = "";
                $scope.loginFailed = false;
                credentials = {username: $scope.username, url: $scope.url};
                userApi.getForgotPasswordResource().post(credentials).$promise.then(function (userAuthResult) {
                    if(userAuthResult.isAuthenticated) {
                        $scope.isSubmitted = true;
                        $scope.loading = false;
                        $scope.loginMessage = loginMessage;
                    }
                    else{
                        $scope.isSubmitted = false;
                        $scope.loginFailed = true;
                        $scope.loading = false;
                        $scope.loginMessage = loginMessage;
                    }

                },function(){
                    $scope.loginFailed = true;
                    $scope.loading = false;
                    $scope.loginMessage = loginMessage;
                });
            }
        }
        messagePopupsService.clear();
    };

    function validUsername(){
        return !!$scope.username.length;
    }
});
