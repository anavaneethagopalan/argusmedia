﻿angular.module('argusOpenMarkets.login')
    .controller('loginScreenController', function loginScreenController
        ($scope,
         $state,
         userService,
         socketProvider,
         cookieService,
         userApi,
         helpers,
         messagePopupsService,
         applicationService,
         $window,
         $location,
         loggedOutErrorMessagesConstant,
         $log,
         platformInfoService)
    {
        $scope.username = '';
        $scope.password = '';
        $scope.oldPassword = '';
        $scope.loginMessage = '';
        $scope.loginFailed = false;
        $scope.loading = false;
        $scope.changePasswordValid = false;
        $scope.browserNotSupported = !platformInfoService.isBrowserSupported();
        var usernameHolder = "";
        var config;
        var message;
        var LOGIN_PAGE_LOADED = "LOGIN_PAGE_LOADED";
        var LOGIN_BUTTON_CLICKED = "LOGIN_BUTTON_CLICKED";
        var USER_AUTHENTICATED_WEB_SERVICE = "USER_AUTHENTICATED_WS";
        var USER_AUTHENTICATED_DIFFUSION = "USER_AUTHENTICATED_DIFFUSION";
        var USER_TRANSISTION_DASHBOARD = "USER_TRANSISTION_DASHBOARD";

        userService.clearClientLog();

        var getUrlRunJob = function(){
            var runJob = $location.search().runjob;

            if(runJob){
                userService.setRunJob(runJob);
            }
        };
        getUrlRunJob();

        userService.addClientLog(LOGIN_PAGE_LOADED);
        userService.setLoginType('normal');
        var cookie = cookieService.readCookie();
        if (cookie) {
            $scope.username = cookie;
            $scope.rememberMe = true;
        }

        applicationService.promise().then(function () {
            $scope.version = applicationService.getVersionConfiguration();
        });

        var urlParameter = helpers.getUrlParameter('l');
        if (urlParameter){
            if (urlParameter.toLowerCase() === "loggedouterrormessagesconstant") {
                message = "You have been signed out by another session";
            }else {
                message = loggedOutErrorMessagesConstant[urlParameter];
            }

            messagePopupsService.displayWarning("Session Termination Notification", message);
        }

        var onDiffusionAuthenticated = function (isConnected, isReconnect) {
            $log.debug('isConnected: ' + isConnected + '|isReconnect: ' + isReconnect + '|loginFailed: ' + $scope.loginFailed);

            if (!$scope.loginFailed) {
                if (isConnected && !isReconnect) {
                    userService.addClientLog(USER_AUTHENTICATED_DIFFUSION);
                    $log.debug('Connected - now connecting to user topic');
                    var loggedOnUser = {
                        "username": $scope.username,
                        "id": $scope.userId
                    };
                    userService.setUser(loggedOnUser);
                    userService.addClientLog(USER_TRANSISTION_DASHBOARD);
                    userService.connectToUserTopic();

                    $state.transitionTo('Dashboard');
                }
            }
        };

        var onUserAuthenticated = function (userAuthResult) {
            var credentials;
            if (userAuthResult.isAuthenticated) {

                $scope.userId = userAuthResult.userId;
                userService.addClientLog(USER_AUTHENTICATED_WEB_SERVICE);

                if (userAuthResult.credentialType === 'T') {
                    userService.setSessionToken(userAuthResult.sessionToken);
                    resetPassword();
                    return true;
                } else {
                    if ($scope.rememberMe) {
                        cookieService.createCookie($scope.username);
                    }
                    else {
                        cookieService.eraseCookie();
                    }
                    credentials = {
                        "username": 'AOM-' + $scope.username,
                        "password": userAuthResult.token
                    };
                    socketProvider.registerConnectCallback(onDiffusionAuthenticated, false);
                    socketProvider.connect(function () {
                    }, credentials, onConnectionRejected);
                    userService.setSessionToken(userAuthResult.sessionToken);
                }
            }
            else {
                // User has not been authenticated via web api service.
                loginFailed(userAuthResult.message.replace("[[EMAILADDRESS]]", "AOMSupport@argusmedia.com"));
            }
        };

        var loginFailed = function (message) {
            $scope.loginFailed = true;
            $scope.loading = false;
            $scope.loginMessage = message;
            $scope.password = "";
        };


        var onConnectionRejected = function () {
            loginFailed("Error Authenticating User");
        };

        $scope.login = function () {
            userService.addClientLog(LOGIN_BUTTON_CLICKED);
            if(!$scope.loading) {
                var credentials;
                $scope.loading = true;
                $scope.loginMessage = "";
                $scope.loginFailed = false;
                credentials = {username: $scope.username, password: $scope.password, loginSource: "aom"};
                userApi.getLoginResource().post(credentials).$promise.then(function (data) {
                    onUserAuthenticated(data);
                }, function (error) {
                    $scope.loginFailed = true;
                    $scope.loading = false;
                    error = userApi.formatErrorResponse(error, false);
                    $scope.loginMessage = error || 'Login timed out';
                });
                messagePopupsService.clear();
            }
        };


        var resetPassword = function () {
            usernameHolder = angular.copy($scope.username);
            var user = userService.getUser();
            if (helpers.isEmpty(user)) {
                user = {username: $scope.username, id: $scope.userId};
            }
            user.oldPassword = angular.copy($scope.password);
            user.firstLogin = true;
            userService.setUser(user);
            cookieService.eraseCookie();
            $state.transitionTo("ResetPassword");
        };
    });
