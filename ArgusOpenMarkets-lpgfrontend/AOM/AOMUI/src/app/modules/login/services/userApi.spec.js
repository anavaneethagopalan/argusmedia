describe('userApi', function(){

    var $provider,
        $resource,
        $http,
        $modal,
        $locationProvider,
        messageDisplayed,
        mockUserService = {};

    var mockMessagePopupsService = {
        displayError : function(header, message){return }
    };

    beforeEach(module('ui.router', function (_$locationProvider_) {
        $locationProvider = _$locationProvider_;
        $locationProvider.html5Mode(false);
    }));

    beforeEach(module('argusOpenMarkets.shared'));
    beforeEach(module('argusOpenMarkets.dashboard'));
    beforeEach(module('argusOpenMarkets.login'));

    beforeEach(module(function($provide) {
        $provide.value('$modalProvider', {});
        $provide.value('userService', mockUserService);
        $provide.value('toastrProvider', {});
        $provide.value('messagePopupsService', {displayMessage: function(message){
            messageDisplayed = message;
        },
        displayError: function(error){
            messageDisplayed = error;
        }});
    }));

    describe('userApi factory', function(){
        var userApiFactory = null,
            $modalProvider = {};

        beforeEach(inject(function(userApi){
            userApiFactory = userApi;
        }));

        it('should define methods', function(){
            expect(userApiFactory.getLoginResource).toBeDefined();
            expect(userApiFactory.getForgotPasswordResource).toBeDefined();
            expect(userApiFactory.getResetPasswordResource).toBeDefined();
            expect(userApiFactory.getChangeDetailsResource).toBeDefined();
            expect(userApiFactory.getCommonPermissionedOrganisations).toBeDefined();
            expect(userApiFactory.getPrincipals).toBeDefined();
            expect(userApiFactory.getNewsStory).toBeDefined();
            expect(userApiFactory.getBrokers).toBeDefined();
            expect(userApiFactory.formatErrorResponse).toBeDefined();
        });

        it('should define getProductPrivileges method', function(){

            expect(userApiFactory.getProductPrivileges).toBeDefined();
        });

        it('should format an error response', function(){
            var error = {"status": 0, "statusText": ""},
                callbackMessage = "",
                callbackFunction = function(message){
                    callbackMessage = message;
                };

            userApiFactory.formatErrorResponse(error, callbackFunction);
            expect(messageDisplayed).toEqual("Unhandled Web Service Exception");
        });
    });
});