﻿angular.module('argusOpenMarkets.login').factory("userApi", function ($resource,
                                                                      $http,
                                                                      wsConfigurationService,
                                                                      userService,
                                                                      messagePopupsService,
                                                                      helpers) {
    "use strict";
    var wsConfiguration,
        endpoint,
        port,
        api,
        protocol,
        loginPath,
        url,
        fullForgotPasswordPath,
        fullResetPasswordPath,
        fullAllPrincipalsPath,
        fullAllBrokersPath,
        fullChangeDetailsPath,
        fullNewsStoryPath,
        fullGetBrokerPermissionsByProductIdPath,
        fullGetBrokerPermissionsByMarketIdPath,
        fullGetCounterpartyPermissionsByMarketIdPath,
        getProductPrivilegesUrl,
        getMarketsUrl,
        getProductSettingsUrl,
        getCommonPermissionedOrganisationsPath,
        fullLoginUrl = "/login",
        saveBrokerPermissionsPath,
        saveCounterpartyPermissionsPath;

    wsConfigurationService.promise().then(function () {

        wsConfiguration = wsConfigurationService.getWsConfiguration();
        endpoint = wsConfiguration.endpoint;
        port = wsConfiguration.port;
        api = wsConfiguration.api;
        loginPath = wsConfiguration.loginPath;
        protocol = wsConfiguration.protocol;
        if (wsConfiguration.port !== "") {
            url = protocol + "://" + endpoint + ":" + port + "/" + api + "/";
        }
        else {
            url = protocol + "://" + endpoint + "/" + api + "/";
        }
        fullLoginUrl = url + loginPath;
        fullForgotPasswordPath = url + wsConfiguration.forgotPasswordPath;
        fullResetPasswordPath = url + wsConfiguration.resetPasswordPath;
        fullAllPrincipalsPath = url + wsConfiguration.getAllPrincipalsPath;
        fullAllBrokersPath = url + wsConfiguration.getAllBrokersPath;
        fullChangeDetailsPath = url + wsConfiguration.getChangeDetailsPath;
        fullNewsStoryPath = url + wsConfiguration.getNewsStoryPath;
        fullGetBrokerPermissionsByProductIdPath = url + wsConfiguration.getBrokerPermissionsByProductIdPath;
        fullGetBrokerPermissionsByMarketIdPath = url + wsConfiguration.getBrokerPermissionsByMarketIdPath;
        fullGetCounterpartyPermissionsByMarketIdPath = url + wsConfiguration.getCounterpartyPermissionsByMarketIdPath;
        getCommonPermissionedOrganisationsPath = url + wsConfiguration.getCommonPermissionedOrganisations;
        getProductPrivilegesUrl = url + wsConfiguration.getProductPrivilegesPath;
        getMarketsUrl = url + wsConfiguration.getMarketsPath;
        getProductSettingsUrl = url + wsConfiguration.getProductSettingsPath;
        saveBrokerPermissionsPath = url + wsConfiguration.saveBrokerPermissionsPath;
        saveCounterpartyPermissionsPath = url + wsConfiguration.saveCounterpartyPermissionsPath;
    });

    var getCustomHeaders = function () {

        return {
            'token': userService.getSessionToken(),
            'userid': userService.getUserId()
        };
    };

    function getLoginResource() {

        return $resource(fullLoginUrl, {}, {
            post: {
                method: 'POST',
                params: {},
                isArray: false
            }
        });
    }

    function getForgotPasswordResource() {

        return $resource(fullForgotPasswordPath, {}, {
            post: {
                method: 'POST',
                params: {},
                isArray: false
            }
        });
    }

    function getResetPasswordResource() {

        return $resource(fullResetPasswordPath, {}, {
            post: {
                method: 'POST',
                params: {},
                isArray: false
            }
        });
    }

    function getChangeDetailsResource() {

        return $resource(fullChangeDetailsPath, {}, {
            post: {
                method: 'POST',
                params: {},
                isArray: false,
                headers: getCustomHeaders()
            }
        });
    }


    var getPrincipals = function (productId, canTradeOnly, buyOrSell) {
        canTradeOnly = (typeof canTradeOnly === "undefined") ? false : canTradeOnly;
        buyOrSell = (typeof buyOrSell === "undefined") ? false : buyOrSell;
        return getPrincipalsResource().get({
            userId: userService.getUserId(),
            productId: productId
        }).$promise.then(function (data) {
            data = buyOrSell === "buy" ? data.buyOrganisations : data.sellOrganisations;
            if (canTradeOnly) {
                data = _.filter(data, function (dataItem) {
                    return dataItem.canTrade;
                });
            }
            return addNameAndCodeToOrganisations(_.pluck(data, "organisation"));
        });
    };

    function getPrincipalsResource() {

        return $resource(fullAllPrincipalsPath, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: false,
                headers: getCustomHeaders()
            }
        });
    }


    function getCommonPermissionedOrganisationsResource() {
        return $resource(getCommonPermissionedOrganisationsPath, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: true,
                headers: getCustomHeaders()
            }
        });
    }

    function addNameAndCodeToOrganisations(organisations) {
        return _.map(organisations, function (o) {
            o.nameAndCode = helpers.getOrganisationNameAndCode(o);
            return o;
        });
    }

    var getCommonPermissionedOrganisations = function (productId, organisationId, brokerId, buyOrSell) {
        return getCommonPermissionedOrganisationsResource().get({
            productId: productId,
            organisationId: organisationId,
            brokerId: brokerId,
            buyOrSell: buyOrSell
        }).$promise.then(function (data) {
            return addNameAndCodeToOrganisations(data);
        });
    };

    var getBrokers = function (productId, canTradeOnly, buyOrSell) {

        canTradeOnly = (typeof canTradeOnly === "undefined") ? false : canTradeOnly;
        buyOrSell = (typeof buyOrSell === "undefined") ? "buy" : buyOrSell;
        return getBrokersResource().get({
            userId: userService.getUserId(),
            productId: productId
        }).$promise.then(function (data) {
            data = buyOrSell === "buy" ? data.buyOrganisations : data.sellOrganisations;
            if (canTradeOnly) {
                data = _.filter(data, function (dataItem) {
                    return dataItem.canTrade;
                });
            }
            return addNameAndCodeToOrganisations(_.pluck(data, "organisation"));
        });
    };

    function getBrokersResource() {

        return $resource(fullAllBrokersPath, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: false,
                headers: getCustomHeaders()
            }
        });
    }

    function getNewsStoryResource() {

        return $resource(fullNewsStoryPath, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: false,
                headers: getCustomHeaders()
            }
        });
    }

    function getBrokerPermissionsResource() {
        return $resource(fullGetBrokerPermissionsByProductIdPath, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: true,
                headers: getCustomHeaders()
            }
        });
    }

    function getBrokerPermissionsByMarketResource() {
        return $resource(fullGetBrokerPermissionsByMarketIdPath, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: true,
                headers: getCustomHeaders()
            }
        });
    }

    function getCounterpartyPermissionsByMarketResource() {
        return $resource(fullGetCounterpartyPermissionsByMarketIdPath, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: true,
                headers: getCustomHeaders()
            }
        });
    }

    function saveCounterpartyPermissionsResource(){
        return $resource(saveCounterpartyPermissionsPath, {}, {
            post: {
                method: 'POST',
                params: {},
                isArray: false,
                headers: getCustomHeaders()
            }
        });
    }

    function saveBrokerPermissionsResource() {

        return $resource(saveBrokerPermissionsPath, {}, {
            post: {
                method: 'POST',
                params: {},
                isArray: false,
                headers: getCustomHeaders()
            }
        });
    }

    function formatErrorResponse(error, popup) {
        if (error.status === 0 && error.statusText === '') {
            error.statusText = "SYSTEM UNAVAILABLE";
        }
        var errorMessage = "An internal system error has occurred (Webservice returned " + error.status + " - " + error.statusText + ").  Please contact support on if the problem persists. " +
            "aomsupport@argusmedia.com  +44(0)2071999430";
        if (popup) {
            messagePopupsService.displayError("Unhandled Web Service Exception", errorMessage);
        }
        return errorMessage;
    }


    function getNewsStory(id, callback, errorCallback) {

        getNewsStoryResource().get({id: id, userId: userService.getUserId()}).$promise.then(function (data) {
            callback(data);
        }, function (error) {
            formatErrorResponse(error, errorCallback);
        });
    }

    function getProductSettingsResource() {

        return $resource(getProductSettingsUrl, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: true,
                headers: getCustomHeaders()
            }
        });
    }

    function getProductSettings() {
        return getProductSettingsResource().get().$promise.then(function (data) {
            return data;
        });
    }

    function getMarketsResource() {
        return $resource(getMarketsUrl, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: true,
                headers: getCustomHeaders()
            }
        });
    }

    function getProductPrivilegesResource() {

        return $resource(getProductPrivilegesUrl, {}, {
            get: {
                method: 'GET',
                params: {},
                isArray: false,
                headers: getCustomHeaders()
            }
        });
    }

    function getProductPrivileges() {
        return getProductPrivilegesResource().get().$promise.then(function (data) {
            return data;
        });
    }

    function getBrokerPermissionsByProductId(productId, organisationId, callback, errorCallback) {

        getBrokerPermissionsResource().get({
            productId: productId,
            organisationId: organisationId
        }).$promise.then(function (data) {
            if (callback) {
                callback(data);
            }
        }, function (error) {
            // formatErrorResponse(error, errorCallback);
        });
    }

    function getBrokerPermissionsByMarketId(marketId, organisationId, callback, errorCallback) {

        getBrokerPermissionsByMarketResource().get({
            marketId: marketId,
            organisationId: organisationId
        }).$promise.then(function (data) {
            if (callback) {
                callback(data);
            }
        }, function (error) {
            // formatErrorResponse(error, errorCallback);
        });
    }

    function getCounterpartyPermissionsByMarketId(marketId, organisationId) {
        return getCounterpartyPermissionsByMarketResource().get({marketId: marketId, organisationId: organisationId});
    }

    function saveCounterpartyPermissions(counterpartyPermissions, organisationId, callback){
        var inData = {'permissions': counterpartyPermissions, 'organisationId': organisationId};
        saveCounterpartyPermissionsResource().post(inData).$promise.then(function (data) {
            if (callback) {
                callback(data);
            }
        }, function (error) {
            //formatErrorResponse(error, errorCallback);
        });
    }

    function saveBrokerPermissions(brokerPermissions, organisationId, callback) {
        var inData = {'permissions': brokerPermissions, 'organisationId': organisationId};
        saveBrokerPermissionsResource().post(inData).$promise.then(function (data) {
            if (callback) {
                callback(data);
            }
        }, function (error) {
            //formatErrorResponse(error, errorCallback);
        });
    }

    function getMarkets(callback) {
        getMarketsResource().get().$promise.then(function (data) {
            if (callback) {
                callback(data);
            }
        });
    }

    return {
        getLoginResource: getLoginResource,
        getForgotPasswordResource: getForgotPasswordResource,
        getResetPasswordResource: getResetPasswordResource,
        getChangeDetailsResource: getChangeDetailsResource,
        getPrincipals: getPrincipals,
        getNewsStory: getNewsStory,
        getBrokers: getBrokers,
        formatErrorResponse: formatErrorResponse,
        getCommonPermissionedOrganisations: getCommonPermissionedOrganisations,
        getProductPrivileges: getProductPrivileges,
        getProductSettings: getProductSettings,
        getBrokerPermissionsByProductId: getBrokerPermissionsByProductId,
        getBrokerPermissionsByMarketId: getBrokerPermissionsByMarketId,
        getCounterpartyPermissionsByMarketId: getCounterpartyPermissionsByMarketId,
        saveBrokerPermissions: saveBrokerPermissions,
        saveCounterpartyPermissions: saveCounterpartyPermissions,
        getMarkets: getMarkets
    };
});