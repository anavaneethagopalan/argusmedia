angular.module('argusOpenMarkets', [
    'ui.utils',
    'ui.route',
    'ui.router.state',
    'ui.bootstrap',
    'argusOpenMarkets.login',
    'argusOpenMarkets.cmeLogin',
    'argusOpenMarkets.dashboard',
    'argusOpenMarkets.shared',
    'argusOpenMarkets.userSettings',
    'toastr',
    'diffusion',
    'ngCookies',
    'localytics.directives',
    'perfect_scrollbar',
    'satellizer'
])
    .config(function myAppConfig
        ($stateProvider,
         $urlRouterProvider,
         $logProvider) {
            $urlRouterProvider.when('cme', '/Cme');
            $urlRouterProvider.otherwise('/');
            $logProvider.debugEnabled(false);
    })
    .config(function ($authProvider) {

        $authProvider.facebook({
            clientId: '1849075298686936',
            responseType: 'token'
        });
    })
    .config(function config($stateProvider) {
        $stateProvider.state('Logout', {
                url: '/Logout',
                views: {
                    "": {
                        templateUrl: 'src/app/modules/login/views/login.part.html'
                    },
                    "main@Login": {
                        templateUrl: 'src/app/modules/login/views/loginScreen.part.html',
                        controller: 'loginScreenController'
                    },
                    'footer': {
                        templateUrl: 'src/app/modules/dashboard/dashboardModules/footer/views/footer.part.html',
                        controller: 'footerController'
                    }
                }
            }
        ).state('Cme', {
            url: '/Cme',
            views: {
                "": {
                    templateUrl: 'src/app/modules/cmeLogin/views/login.part.html'
                },
                "main@Cme": {
                    templateUrl: 'src/app/modules/cmeLogin/views/loginScreen.part.html',
                    controller: 'cmeLoginScreenController'
                },
                'footer': {
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/footer/views/footer.part.html',
                    controller: 'footerController'
                }
            }
        }).state('Login', {
            url: '/',
            views: {
                "": {
                    templateUrl: 'src/app/modules/login/views/login.part.html'
                },
                "main@Login": {
                    templateUrl: 'src/app/modules/login/views/loginScreen.part.html',
                    controller: 'loginScreenController'
                },
                'footer': {
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/footer/views/footer.part.html',
                    controller: 'footerController'
                },
                resolve: {
                    configurationLoaded: function (wsConfigurationService) {
                        return wsConfigurationService.promise;
                    },
                    principalsForBroker: function (wsConfigurationService) {
                        return wsConfigurationService.promise;
                    }
                }
            }
        }).state('ForgottenPassword', {
            url: '/ForgottenPassword',
            views: {
                "": {
                    templateUrl: 'src/app/modules/login/views/login.part.html'
                },
                "main@ForgottenPassword": {
                    templateUrl: 'src/app/modules/login/views/forgotPassword.part.html',
                    controller: 'forgotPasswordController'
                },
                'footer': {
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/footer/views/footer.part.html',
                    controller: 'footerController'
                },
                resolve: {
                    configurationLoaded: function (wsConfigurationService) {
                        return wsConfigurationService.promise;
                    },
                    principalsForBroker: function (wsConfigurationService) {
                        return wsConfigurationService.promise;
                    }
                }
            }
        }).state('ResetPassword', {
            url: '/ResetPassword?t',
            views: {
                "": {
                    templateUrl: 'src/app/modules/login/views/login.part.html'
                },
                "main@ResetPassword": {
                    templateUrl: 'src/app/modules/login/views/resetPassword.part.html',
                    controller: 'resetPasswordController'
                },
                'footer': {
                    templateUrl: 'src/app/modules/dashboard/dashboardModules/footer/views/footer.part.html',
                    controller: 'footerController'
                }
            }
        });
    })
    .run(function run($state,
                      userService,
                      $rootScope,
                      $timeout) {


        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                var user = userService.getUser();
                if (typeof(user) === 'undefined' ||
                    user === null && toState.name !== 'Login' && toState.name !== 'ForgottenPassword' && toState.name !== 'ResetPassword' && toState.name !== 'Cme') {
                    event.preventDefault();
                    $timeout(function () {
                        event.targetScope.$apply(function () {
                            if(userService.isCmeUser()) {
                                $state.transitionTo("Cme");
                            }else{
                                $state.transitionTo("Login");
                            }
                        });
                    }, 30);
                }
            }
        );
    })    
    .controller('applicationController', function AppCtrl($scope, toastr, $log, applicationService) {
        applicationService.promise().then(function () {
            $scope.preLiveClass = applicationService.getApplicationBackgroundClass();
        });

        $scope.$on("$locationChangeStart", function (event, next, current) {
            var cssVersion = "4.0.0";
            applicationService.promise().then(function () {
                cssVersion = applicationService.getCssVersionConfiguration();

                if (next.toLowerCase().indexOf('dashboard') === -1) {
                    // We are on the Login Page.

                    if (next.indexOf('/#/Cme/') > -1 || next.indexOf('/#/Cme') > -1) {
                        $scope.cssPage1 = "assets/ArgusOpenMarkets-" + cssVersion + "Page1.css";
                        $scope.cssPage2 = "assets/ArgusOpenMarkets-" + cssVersion + "Page2.css";
                    } else {
                        $scope.cssPage1 = "assets/ArgusOpenMarkets-" + cssVersion + "defaultPage1.css";
                        $scope.cssPage2 = "assets/ArgusOpenMarkets-" + cssVersion + "defaultPage2.css";
                    }
                }

                if (!$scope.cssPage1) {
                    $scope.cssPage1 = "assets/ArgusOpenMarkets-" + cssVersion + "defaultPage1.css";
                }

                if (!$scope.cssPage2) {
                    $scope.cssPage2 = "assets/ArgusOpenMarkets-" + cssVersion + "defaultPage2.css";
                }

            });
        });

        $log.debug(new Array(100).join("_"));
        $log.debug("Console logging is enabled...");
        $log.debug(new Array(100).join("_"));
    });
