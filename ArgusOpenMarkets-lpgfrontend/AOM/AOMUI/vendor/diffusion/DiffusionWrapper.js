/*
 * Plugins do not know when they are being loaded.
 * This simple bit of Javascript can be called from Flash or Silverlight to drop the
 * existing connection, when the page is unloaded.
 * 
 * Once the plugin has made the connection to Diffusion, the client should call setClientDetails
 */

DiffusionWrapper = new function() {
	this.clientID = null;
	this.serverUrl = null;
	this.iframe = null;
	this.url = null;

	if (navigator.appVersion.match('MSIE') == 'MSIE') {
		this.isIE = true;
		if (navigator.appVersion.match('MSIE 10.0') == 'MSIE 10.0') {
			this.isModernIE = true;
		} else if (navigator.appVersion.match('MSIE 9.0') == 'MSIE 9.0') {
			this.isModernIE = true;
		} else {
			this.isModernIE = false;
		}
	} else {
		this.isIE = false;
	}

	this.closeClient = function() {
		var props = ['width', 'height', 'border'];
		var iframe, url = this.serverUrl + "/diffusion?m=29&c=" + this.clientID;

		if (this.isIE && !this.isModernIE) {
			iframe = document.createElement('<iframe name="closeFrame" src="' + url + '">');
		} else {
			iframe = document.createElement("iframe");
			iframe.setAttribute("src", url);
		}

		for (var i = 0; i < props.length; ++i) {
			iframe[props[i]] = "0px";
		}

		document.body.appendChild(iframe);
	}

	this.setClientID = function(clientID) {
		this.clientID = clientID;
	}

	this.getServerUrl = function() {
		return this.serverUrl;
	}

	this.setServerUrl = function(serverUrl) {
		this.serverUrl = serverUrl.replace("httpc", "http");
	}
}

function closeIt() {
	DiffusionWrapper.closeClient();
}

function setClientDetails(clientId, serverUrl) {
	DiffusionWrapper.setClientID(clientId);
	DiffusionWrapper.setServerUrl(serverUrl);
	window.onbeforeunload = closeIt;
}
