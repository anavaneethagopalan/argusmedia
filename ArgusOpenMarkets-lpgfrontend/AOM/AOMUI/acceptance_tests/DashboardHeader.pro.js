fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var DBFunctions = require("./DatabaseFunctions.js");

var automation = new Automation();

var myObj = {value: "none"};
var gNameOfUser = 'nothing';


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Dashboard header', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;

    var saveBeforeSignin = false;

    var specDescription = this.description;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    console.log('\r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        //console.log(sBrowserName)
    });

    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

    login = new LoginPage();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs in for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "succeeds for user";
        login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.waitForLoad(20);
        dashboard.productTitles().then(function(ProductTitles){
            expect(ProductTitles.length).not.toEqual(0);
        });
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("shows user in header as: '" + username + "'", function () {
        var specIt = "shows user in header";
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.getHeaderUser().then(function(HeaderUsers){
            expect(HeaderUsers[0].getText()).toEqual(username);
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("gets name of user from db for user : '" + username + "'", function (done) {
        var specIt = "gets name of user from db for user";

        function getNameOfUserCB(nameOfUser) {
            gNameOfUser = nameOfUser;
            done();
        }

        var DBF = new DBFunctions();
        DBF.getNameOfUser(username, getNameOfUserCB);
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("checks name of user from db for user : '" + username + "'", function () {
        var specIt = "checks name of user from db for user";
        expect(gNameOfUser).toEqual(username);
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("shows organisation in header as: 'Argus Media''", function () {
        var specIt = "shows organisation in header";
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.getOrganisation().then(function(Organisations){
            expect(Organisations[0].getText()).toEqual('Argus Media');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs out", function () {
        var specIt = "logs out";
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.logout(hostURL);
        login.waitForLoad(10);
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + '.png');
    });

}, 60000);
