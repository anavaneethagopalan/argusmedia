fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var MarketTickerPage = require("./pages/markettickerpage");
var dashboard;

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Market Ticker', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;

    var saveBeforeSignin = false;
    var specDescription = this.description;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
    });

    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("displays login window", function () {
        var specIt = "displays login window";

        var automation = new Automation();
        var login = new LoginPage();
        login.waitForLoad(20).then(function (SignInButtons) {
            expect(SignInButtons.length).toEqual(1);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + '.png');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs in for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "logs in for user";

        var automation = new Automation();
        var login = new LoginPage();
        var dashboard = new DashboardPage(); // get the dashboard page.

        for (var i = 0; i <= 5; i++) {
            login.isDisplayed().then(function (SignInButtons) {
                if (SignInButtons.length === 1){
                    login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
                    dashboard.waitForLoad(20).then(function (BidButtons) {
                        if (BidButtons.length = 0) {
                            i = 6;
                        }
                    });
                }
            });
        }

        dashboard.getProductTitles().then(function(ProductTitles){
            expect(ProductTitles.length).not.toEqual(0);
            //expect(ProductTitles.length).toEqual('ProductTitles.length');
        });

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("has items", function () {
        var specIt = "has items";
        var mtRows;

        var marketTicker = new MarketTickerPage();

        marketTicker.Rows().then(function (Rows) {
            expect(Rows.length).not.toEqual(0);
            //expect(Rows.length).toEqual("Rows.length");
            expect(Rows[0].getText()).not.toEqual('');
            expect(Rows[Rows.length-1].getText()).not.toEqual('');
        });

    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("has products filter", function () {
        var specIt = "has products filter";
        var marketTicker = new MarketTickerPage();

        marketTicker.products().then(function (Products) {
            expect(Products[0].getText()).toContain('PRODUCTS');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("products filter defaults to All", function () {
        var specIt = "products filter defaults to All";
        var marketTicker = new MarketTickerPage();

        marketTicker.products().then(function (Products) {
            expect(Products[0].getText()).toContain("All");
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("can filter products", function () {
        var specIt = "can filter products";
        var automation = new Automation();
        var marketTicker = new MarketTickerPage();

        marketTicker.products().then(function (Products){
            Products[0].click();

            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + '.png');

            marketTicker.filters().then(function(Filters) {
                for (var i = 4; i < Filters.length; i++) {    //  first product filter is at 4 (0-3 are types)
                    expect(Filters[i].getAttribute('checked')).toEqual('true'); // filter should be on first
                    Filters[i].click();
                    expect(Filters[i].getAttribute('checked')).toEqual(null);   // Off now
                    if (i < (Filters.length-1)) expect(Products[0].getText()).toContain(Filters.length-5);
                }

                marketTicker.Rows().then(function (Rows) {
                    expect(Rows.length).toEqual(0);
                });

                // Turn all product filters on again
                for (var i = 4; i < Filters.length; i++) {    //  first product filter is at 4 (0-3 are types)
                    expect(Filters[i].getAttribute('checked')).toEqual(null);
                    Filters[i].click();
                    expect(Filters[i].getAttribute('checked')).toEqual('true');   // Off now
                }

                expect(Products[0].getText()).toContain("All");
                marketTicker.Rows().then(function (Rows) {
                    expect(Rows.length).not.toEqual(0);
                });

            });
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("has types filter", function () {
        var specIt = "has types filter";
        var marketTicker = new MarketTickerPage();

        marketTicker.types().then(function (Types) {
            expect(Types[0].getText()).toContain('TYPES');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("types filter defaults to All", function () {
        var specIt = "types filter defaults to All";
        var marketTicker = new MarketTickerPage();

        marketTicker.types().then(function (Types) {
            expect(Types[0].getText()).toContain('All');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("can filter types", function () {
        var specIt = "can filter types";
        var automation = new Automation();
        var marketTicker = new MarketTickerPage();

        marketTicker.types().then(function (Types){
            Types[0].click();

            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + '.png');

            marketTicker.filters().then(function(Filters) {
                for (var i = 0; i < 4; i++) {    //  filters 0-3 are types
                    expect(Filters[i].getAttribute('checked')).toEqual('true'); // filter should be on first
                    Filters[i].click();
                    expect(Filters[i].getAttribute('checked')).toEqual(null);   // Off now
                    if (i<3) expect(Types[0].getText()).toContain(3-i);
                }

                marketTicker.Rows().then(function (Rows) {
                    expect(Rows.length).toEqual(0);
                });

                // Turn all product filters on again
                for (var i = 0; i < 4; i++) {    //  filters 0-3 are types
                    expect(Filters[i].getAttribute('checked')).toEqual(null);
                    Filters[i].click();
                    expect(Filters[i].getAttribute('checked')).toEqual('true');   // Off now
                }

                expect(Types[0].getText()).toContain("All");
                marketTicker.Rows().then(function (Rows) {
                    expect(Rows.length).not.toEqual(0);
                });

            });
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs out", function () {
        var specIt = "logs out";
        var automation = new Automation();
        var dashboard = new DashboardPage(); // get the dashboard page.
        var login = new LoginPage();

        dashboard.logout(hostURL);
        login.waitForLoad(10);
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + '.png');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('pauses', function () {
        var stream;
        var newOrder;
        console.log('pause'); //Newline
    });
}, 60000);
