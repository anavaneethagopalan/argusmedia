fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
//var dashboard;

var automation = new Automation();

// Require keyword library
var key = require('keyword');
key(require('./AOMKeywordsSignin'));
key(require('./AOMKeywordsOther'));

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('AOM Signin Behaviour', function () {
    var sBrowserName;
    var login;
    var hostURL;
    var iStep = 1;


    var username;
    var password;

    var saveBeforeSignin = false;

    var specDescription = this.description;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    //console.log('\r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        //console.log(sBrowserName)
    });


    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    //browser.driver.quit();
    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("SigninFailMessages",
        [   ['VerifySigninFailMessage',['NoSuchUser','password','Invalid login details, please try again']],
            ['VerifySigninFailMessage',[username,'incorrect','Invalid login details, please try again']],
            ['VerifySigninFailMessage',[username + 'blocked','password','This account is not authorised from AOM. Please contact the Argus AOM administration team. Email: AOMSupport@argusmedia.com. Tel +44 020X XXX XXXX']]],
        function(keyword, data) {
            it("Step " + iStep++ + ":" + keyword + " with data: " + data, function () {
                var specIt = "run: " + keyword ;
                key.run(keyword, data).then(function() {
                    //expect(keyword).not.toEqual(''); // non-blank is enough
                });
           },120000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("SigninSuspension",
//        [   ['VerifySigninFailedAttemptsAllowed',[username + 'forblocking', 'incorrect', 'password', 5]],
            [['SuspendAfterFailedSigningAttempts',[username + 'forblocking', 'incorrect', 'password', 5]]],
        function(keyword, data) {
            it("Step " + iStep++ + ":" + keyword + " with data: " + data, function () {
                var specIt = "run: " + keyword ;
                key.run(keyword, data).then(function() {
                    //expect(keyword).not.toEqual(''); // non-blank is enough
                });
            },120000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
//*
    automation.using("run AOMkeywords",
        [   ['Signin', ['john', 'password', 'false']],
            ['NewOrder', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['OrderInBidAskStack', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['OrderInMarketTicker', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['OrderInDatabase', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['Signout', ['none']]],
        function(keyword, data) {
            xit("Step " + iStep++ + ":" + keyword + " with data: " + data, function () {
                var specIt = "run: " + keyword ;
                key.run(keyword, data).then(function() {
                    //expect(keyword).not.toEqual(''); // non-blank is enough
                });
            },120000);
        });
//*/

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('Ends', function () {
    });

}, 60000);