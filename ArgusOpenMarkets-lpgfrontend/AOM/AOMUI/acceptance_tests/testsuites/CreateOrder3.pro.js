var Automation = require("../helpers/automation.js");
var automation = new Automation();
var key = require('keyword'); // keyword functionality

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('CreateOrder3', function () {
    var hostURL;
    var iStep = 1;
    var specDescription = this.description;

    var username = browser.params.credentials.username;
    var password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        case 'LIVE':
            hostURL = browser.params.hostURL.live;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
    xit("Does nothing", function () {
    }, 120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("running CreateOrder3 test script", require('../testscripts/CreateOrder3.script.js'),
        function (step) {
            var sObject = step.action.substring(0, step.action.indexOf('.'));
            //Load keyword file (name must be in the form 'object.keywords.js'). "try" handles exception when loading same object.keywords.js file
            try { key(require('../keywords/' + sObject + '.keywords.js')); } catch(err) { }
            it("Step " + iStep++ + ": Keyword=" + step.action + ", Data=[" + step.params + "]", function () {
                key.run(step.action, step.params).then(function () {
                });
            }, 120000);
        });

}, 60000);
