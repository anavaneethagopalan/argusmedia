// Node packages that are required for the automated testing:
// npm install jasmine-spec-reporter
// npm install jasmine-reporters@1.0.0
// npm install replace
// npm install mysql
// npm install keyword

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',

    allScriptsTimeout: 30000,

    onPrepare: function () {


        require('jasmine-spec-reporter');
        jasmine.getEnv().addReporter(new jasmine.SpecReporter({displayStacktrace: true}));
        //browser.driver.manage().window().maximize(); //browser.driver.manage().window().setSize(1366, 768);

        var thisRunId = Math.floor(Math.random() * 10000);
        var username = browser.params.credentials.username;
        var password = browser.params.credentials.password;

        var DBFunctions = require("./DatabaseFunctions.js");
        var DBF = new DBFunctions();
        //DBF.testUsersSetup(username, '5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8'); //password);

/*
// Turn off toastr messages by modifying the messagePopupsService.js using the replace function
        var replace = require("replace");
        replace({
            regex: "var useToastr = true",
            replacement: "var useToastr = false",
            paths: ['../build/src/app/modules/shared/providers/messageSubscriptionServices/messagePopupsService.js'],
            recursive: true,
            silent: true
        });
*/
        var browserName;
        browser.getCapabilities().then(function (cap) {
            browserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        });

        var d = new Date();
        fs = require('fs');
        browser.params.saveFolder = browser.params.saveFolder + "/" + d.getFullYear() + "." + ('0' + (d.getMonth()+1)).slice(-2) + "." + ('0' + d.getDate()).slice(-2) + "." + browserName;
        if (!fs.existsSync(browser.params.saveFolder)) {
            fs.mkdir(browser.params.saveFolder);
        }

        // *******   jasmine-reporters v1.0.0  **** npm install jasmine-reporters@1.0.0 ****
        require('jasmine-reporters');
        jasmine.getEnv().addReporter(new jasmine.JUnitXmlReporter(browser.params.saveFolder, true, true, browserName + ".")); // + "/" + browserName + "."));

        //jasmine-reporters v2  ****** DOES NOT WORK ****
        //var jasmineReporters = require('jasmine-reporters');
        //jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({savePath: browser.params.saveFolder, consolidate: false, consolidateAll: false, filePrefix: browserName + ".", useDotNotation: true}));
    },

    onComplete: function () {
        var username = browser.params.credentials.username;

        var DBFunctions = require("./DatabaseFunctions.js");
        var DBF = new DBFunctions();
        //DBF.testUsersCleanUp(username);

// Turn toastr messages back
/*
        var replace = require("replace");
        replace({
            regex: "var useToastr = false",
            replacement: "var useToastr = true",
            paths: ['../build/src/app/modules/shared/providers/messageSubscriptionServices/messagePopupsService.js'],
            recursive: true,
            silent: true
        });
*/
    },

    params: {

        //testEnv: 'dev',
        testEnv: 'local',       // <-- change this to change the test environment eg URL

        name1: 'value1',
        saveFolder: '/temp',

        hostURL: {
            local: 'http://web.aom.com', ///#/Dashboard',
            dev: 'http://web.dev.aom.dev.argusmedia.com/', //#/Dashboard',
            live: 'http://aomweb.argusmedia.com/', //#/Dashboard',
            systest: 'http://web.aom.com/' //#/Dashboard'
        },

        local: {
            url: 'http://web.aom.com/' //#/Dashboard'
        },

        credentials: {
            username: 'ta1',
            //username: 'john',
            //username: 'IntTest' + ('0000' + Math.floor(Math.random() * 10000)).slice(-4),
            password: 'password'
        }

    },
   /*
     multiCapabilities: [
     {
     'browserName': 'chrome'
     },
     {
     'browserName': 'firefox'
     }//,
 //    {
 //    'browserName': 'internet explorer'//,
     //'platform': 'ANY',
     //'version': '10'
//     }
     ],
     */

    jasmineNodeOpts: {
        DefaultTimeoutInterval: 120000
    },


    /*
     capabilities: {
        'browserName': 'chrome',
         shardTestFiles: true,
         maxInstances: 1
         //count: 2
     },
     */

    /*
     capabilities: {
     'browserName': 'internet explorer'
     },
     */

    /*
     capabilities: {
     'browserName': 'firefox'
     },
     */

    'loggingPrefs': {
        'browser': 'ALL'
    },

    specs: [
        //    '*.pro.js',
        //'HitLiftOrders.pro.js',
        //'NewOrders.pro.js',
        //'NewOrders2.pro.js'
        //'DashboardHeader.pro.js'
        //'LoginPositiveAndNegative.pro.js'
        //,'LoginFailMessages.pro.js'
        //,'LoginBlocking.pro.js'
        //,'LoginRememberMe.pro.js'
        //'MarketTicker.pro.js'
        //,'Assessments.pro.js'
        //,'KeywordDriven.pro.js'
        //'SigninBehaviour.pro.js'
        //'OrderCreate.pro.js'
        //'TemplateTestUsingKeywords.pro.js'
        //,'TemplateTestWithoutUsingKeywords.pro.js'
        //'OrderOpen.pro.js'
        //'OrderRead.pro.js'
        //'MarketTickerRead.pro.js'
        //'./testsuites/CreateOrder.pro.js'
        './testsuites/*.pro.js'
    ],

    reporters: ['spec', 'coverage']
};

