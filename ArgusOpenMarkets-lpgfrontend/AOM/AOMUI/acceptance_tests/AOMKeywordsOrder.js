// AOMKeywordsOrder.js
var DashboardPage = require("./pages/dashboardpage");
var NewOrderPage = require("./pages/neworderpage");


var AOMKeywordsOrder = {

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "NewOrder": function(next, product, bidask, quantity, price, notes ) {
        var dashboard = new DashboardPage(); // get the dashboard page.

        if (bidask.substr(0, 1).toUpperCase() === 'B')  {
            dashboard.newBid(product);
        } else {
            dashboard.newAsk(product);
        }
        var newOrder = new NewOrderPage();
        newOrder.setPrice(price);
        newOrder.create();

        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "NewOrders": function(next, number, product, bidAsk, quantity, startPrice, priceIncrement) {
        var dashboard = new DashboardPage(); // get the dashboard page.

        for (var i = 0; i < number; i++) {
            if (bidAsk.substr(0, 1).toUpperCase() === 'B')  {
                dashboard.newBid(product);
            } else {
                dashboard.newAsk(product);
            }

            newOrder = new NewOrderPage();
            newOrder.setPrice((startPrice+priceIncrement*i).toFixed(2));
            newOrder.create();
            browser.sleep(250);
        }

        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInBidAskStack": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInMarketTicker": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInDatabase": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    }

};

module.exports = AOMKeywordsOrder;