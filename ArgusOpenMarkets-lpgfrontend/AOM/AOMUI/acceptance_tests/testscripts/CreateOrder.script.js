var CreateOrder = [
	[{action:'AOM.Start',params:[{browser:'chrome',url:'http://aomweb.argusmedia.com/#/Dashboard'}]}],
	[{action:'AOM.Signin',params:[{username:'t1',password:'password'}]}],
	[{action:'Order.Create',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'12500',price:'2801',deliverystart:'01/01/2015',deliveryend:'05/01/2015'}]}],
	[{action:'Order.CheckInStack',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'12500',price:'2802',deliverystart:'01/01/2015',deliveryend:'05/01/2015'}]}],
	[{action:'Order.CheckInTicker',params:[{product:'NAPHTHA CIF NWE - OUTRIGHT',bidask:'BID',quantity:'12500',price:'2802',deliverystart:'01/01/2015',deliveryend:'05/01/2015'}]}],
	[{action:'AOM.Signout',params:[{none:''}]}],
	[{action:'AOM.End',params:[{none:''}]}]
];

module.exports = CreateOrder;
