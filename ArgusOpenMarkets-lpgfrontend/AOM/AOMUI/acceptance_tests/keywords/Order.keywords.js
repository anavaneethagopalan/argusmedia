// AOMKeywordsOrder.js
var DashboardPage = require("../pages/dashboardpage");
var NewOrderPage = require("../pages/neworderpage");


var OrderKeywords = {

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "NewOrder": function(next, product, bidask, quantity, price, notes ) {
        var dashboard = new DashboardPage(); // get the dashboard page.

        if (bidask.substr(0, 1).toUpperCase() === 'B')  {
            dashboard.newBid(product);
        } else {
            dashboard.newAsk(product);
        }
        var newOrder = new NewOrderPage();
        newOrder.setPrice(price);
        newOrder.create();

        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "NewOrders": function(next, number, product, bidAsk, quantity, startPrice, priceIncrement) {
        var dashboard = new DashboardPage(); // get the dashboard page.

        for (var i = 0; i < number; i++) {
            if (bidAsk.substr(0, 1).toUpperCase() === 'B')  {
                dashboard.newBid(product);
            } else {
                dashboard.newAsk(product);
            }

            newOrder = new NewOrderPage();
            newOrder.setPrice((startPrice+priceIncrement*i).toFixed(2));
            newOrder.create();
            browser.sleep(250);
        }

        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInBidAskStack": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInMarketTicker": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInDatabase": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "Order.Create": function(next, params ) {
        var iProduct;

        console.log("Order.Create: product:" + params.product);
        var dashboard = new DashboardPage(); // get the dashboard page.

        if (params.product.search('OUTRIGHT') > -1){
            iProduct = 0;
        } else {
            iProduct = 1;
        }

        if (params.bidask.substr(0, 1).toUpperCase() === 'B')  {
            dashboard.newBid(iProduct);
        } else {
            dashboard.newAsk(iProduct);
        }
        var newOrder = new NewOrderPage();
        newOrder.setPrice(params.price);
        newOrder.create();

        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "Order.CheckInStack": [
        "Order.CheckInStack1", ["$1"], "=> $retVal",
        "Order.CheckInStack2", ["$retVal"]
    ],
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "Order.CheckInStack2": function(next, retVal ) {
        expect(retVal).toEqual(true);
    },
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "Order.CheckInStack1": function(next, params ) {
        console.log("Order.CheckInStack: product:" + params.product + "," + params.bidask + " " + params.quantity + "@" + params.price);
        var iRows = 0;
        var iColumns = 0;
        var iProducts = 0;
        var iProduct = 0;
        var arrOrderElements;
        var sQuantity;
        var sPrice;
        var iStartCell;
        var bFoundEntry = false;

        var DashboardPage = require("../pages/dashboardpage");
        var dashboard = new DashboardPage(); // get the dashboard page.

        dashboard.getProductTitles().then(function (ProductTitles) {
            console.log('ProductTitles.length = ' + ProductTitles.length);
            iProducts = ProductTitles.length;

            dashboard.allBidAskRows().then(function (BidAskRows) {
                console.log('BidAskRows.length = ' + BidAskRows.length);
                iRows = BidAskRows.length/(iProducts*2);
                iColumns = BidAskRows.length/iRows;

                // Which product index is it?
                for (var i=0; i < iProducts; i++) {
                    ProductTitles[i].getText().then(function (ProductText) {
                        if (ProductText === params.product) {
                            if (params.bidask === 'BID') {
                                iStartCell = iProduct * iProducts * iRows;
                            } else {
                                iStartCell = iProduct * iProducts * iRows + iRows;
                            }
                            for (var iRow=iStartCell; iRow < (iStartCell + iRows); iRow++) {
                                BidAskRows[iRow].getText().then(function (RowText) {
                                    if (RowText.length > 0) {
                                        console.log(RowText.replace(/\n/g, '~'));
                                        arrOrderElements = RowText.split(/\n/g);

                                        if (arrOrderElements.length === 5) {
                                            sQuantity = arrOrderElements[3];
                                            sPrice = arrOrderElements[4];
                                        } else {
                                            sQuantity = arrOrderElements[2];
                                            sPrice = arrOrderElements[3];
                                        }

                                        if (Number(sQuantity.replace(',','')) === Number(params.quantity)) {
                                            if (Number(sPrice.replace(',','')) === Number(params.price)) {
                                                bFoundEntry = true;
                                                next(bFoundEntry);
                                            }
                                        }
                                    } else { // Blank means no more rows
                                        next(bFoundEntry);
                                    } // if non-blank row
                                }); // BidAskRows[iRow].getText().then(function (RowText) {
                            } // for iRow
                        } // if params.product
                        iProduct++;
                    }); // ProductTitles[i].getText().then(function (ProductText) {
                } //for iProduct
            }); // dashboard.allBidAskRows().then(function (BidAskRows) {
        }); // dashboard.getProductTitles().then(function (ProductTitles) {
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "Order.CheckInTicker": function(next, params ) {
        //console.log(this.name);
        next();
    }

};

module.exports = OrderKeywords;