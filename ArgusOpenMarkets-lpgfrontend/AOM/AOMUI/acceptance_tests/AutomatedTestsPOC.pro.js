fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var NewOrderPage = require("./pages/neworderpage");
var dashboard;

var automation = new Automation();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('New order window', function () {
    var newOrder;
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;


    console.log('r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        console.log(sBrowserName)
    });


    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

//    console.log(browser.params.testEnv.toUpperCase() + ',' + hostURL); //Newline

//    browser.get('http://10.8.0.228/#/Dashboard');
    browser.get(hostURL);


    login = new LoginPage();

    //console.log('\r\n'); //Newline

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('checks the parameters', function () {
        expect(browser.params.name1).toEqual('browser.params.name1');
        expect(browser.params.general.url).toEqual('browser.params.general.url');
        expect(browser.params.credentials.username).toEqual('browser.params.credentials.username');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('shows the parameters', function () {
        console.log('browser.params.name1: ' + browser.params.name1);
        console.log('browser.params.testEnv: ' + browser.params.testEnv);
        console.log('browser.params.saveFolder: ' + browser.params.saveFolder);
        console.log('browser.params.credentials.username: ' + browser.params.credentials.username);
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('logs in', function () {

//        login.login("Hello", "world", "/temp/screenshot-" + sBrowserName + "-LoginPage.png");
        login.login(username, password, "/temp/screenshot-" + sBrowserName + "-LoginPage.png");
        //login.login("Hello", "world", "");

        // get the dashboard page.  Do it here  and then verify with expect() in another it()
        dashboard = new DashboardPage();
        dashboard.waitForLoad(20);
        //console.log("Log in as: " + "Hello")

    });//}, 60000);

    // -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('transitions to dashboard', function () {
        //dashboard.waitForLoad(20);
        //console.log("Number of grids: " + dashboard.gridCount());
        expect(dashboard.gridCount()).not.toEqual(0);

        automation.screenshot('/temp/screenshot-' + sBrowserName + '.Dashboard.png');
        //console.log("Logged in")
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('gets number of bid/ask grids', function () {
        //How many grids are there?
        //console.log("Number of bid/ask grids: " + dashboard.gridCount());
        expect(dashboard.gridCount() + 0).not.toEqual(0);
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('each grid has heading', function () {

        for (var i = 0; i < dashboard.gridCount(); i++) {
            //console.log("Heading of grid " + i + ": " + dashboard.headingText(i));
            expect(dashboard.headingText(i) + "").not.toEqual("");
        }
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('gets total number of bid/ask grid rows', function () {
        //How many grids are there?
        //console.log("Number of bid ask rows: " + dashboard.bidAskRowCount());
        expect(dashboard.bidAskRowCount() + 0).not.toEqual(0);
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('opens a new order modal', function () {
        dashboard.newBid(0);
        newOrder = new NewOrderPage();
        expect(newOrder.displayed()).toEqual(true);
       // console.log("New Order displayed: " + newOrder.displayed());
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('price and quantity are blank', function () {
        expect(newOrder.getPrice() ).toEqual('');
        expect(newOrder.getQuantity()).toEqual('');
        //console.log("Price and quantity blank");
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('CREATE button is disabled', function () {

        expect(newOrder.createDisabled()).toEqual('true');
        //console.log("create button is disabled");
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('CREATE button is enabled', function () {
        //console.log('verifies the new order windows'); //Newline

// Bid --------------------------

        expect(newOrder.getPrice()).toEqual('');
        expect(newOrder.getQuantity()).toEqual('');
        automation.screenshot('/temp/screenshot-' + sBrowserName + '-' + newOrder.headingText() + '-bid1.png');

        newOrder.setPrice('100');
        newOrder.setQuantity('12500');

        expect(newOrder.getPrice()).toEqual('100');
        expect(newOrder.getQuantity()).toEqual('12500');
        automation.screenshot('/temp/screenshot-' + sBrowserName + '-' + newOrder.headingText() + '-bid2.png');

        expect(newOrder.createDisabled()).not.toEqual(true);
        newOrder.hoverCreate();
        automation.screenshot('/temp/screenshot-' + sBrowserName + '-' + newOrder.headingText() + '-bid3.png');

        newOrder.exit(); //Close the new order form
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('pauses', function () {
        var stream;
        var newOrder;

        console.log('pause'); //Newline
    });
}, 60000);
