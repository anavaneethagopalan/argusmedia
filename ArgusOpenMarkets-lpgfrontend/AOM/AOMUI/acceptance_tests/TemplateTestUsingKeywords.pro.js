var Automation = require("./automation");
var automation = new Automation();

// Require keyword library
var key = require('keyword');
key(require('./TemplateKeywords'));

var testScript1 =  [
    ['DoSomething A', ['DataItem', 'DataItem', 'DataItem', 'DataItem']],
    ['DoSomething B', ['DataItem', 'DataItem', 'DataItem']],
    ['DoSomething C', ['DataItem', 'DataItem', 'DataItem', 'DataItem', 'DataItem']],
    ['DoSomething D', ['DataItem', 'DataItem']],
    ['DoSomething E', ['none']]
];

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('template test scenario', function () {
    var hostURL;
    var iStep = 1;
    var specDescription = this.description;

    var username = browser.params.credentials.username;
    var password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("running a test using the keywords below",
        [   ['DoSomething A', ['DataItem', 'DataItem', 'DataItem', 'DataItem']],
            ['DoSomething B', ['DataItem', 'DataItem', 'DataItem']],
            ['DoSomething C', ['DataItem', 'DataItem', 'DataItem', 'DataItem', 'DataItem']],
            ['DoSomething D', ['DataItem', 'DataItem']],
            ['DoSomething E', ['none']]],
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("running a test using the keywords below", testScript1,
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("running a test using the keywords below", require('./TemplateTestScript'),
        function(keyword, data) {
            xit("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("running a test using the keywords below", require('./TemplateTestScriptWithActionAndParams'),
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword.action + ", Data=[" + data + "]", function () {
                key.run(keyword.action, data).then(function() {
                });
            },120000);
        });


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("running a test using the keywords below", require('./TemplateTestScriptWithObjects'),
        function(step) {
            it("Step " + iStep++ + ": Keyword=" + step.action + ", Data=[" + step.params + "]", function () {
                key.run(step.action, step.params).then(function() {
                });
            },120000);
        });

    describe('nested describe', function () {

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
        for (var i=0; i<5; i++) {
            automation.using("running a test using the keywords below", require('./TemplateTestScriptWithObjects'),
                function (step) {
                    it("Iteration:" + i + ", Step " + iStep++ + ": Keyword=" + step.action + ", Data=[" + step.params + "]", function () {
                        key.run(step.action, step.params).then(function () {
                        });
                    }, 120000);
                });
        }

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
   //require('./keywords/aomkeywords');
   key(require('./keywords/aomkeywords'));
        automation.using("running a test using the keywords below", require('./testscripts/createorder.script.js'),
        function (step) {
            //console.log("Step " + iStep++ + ": Keyword=" + step.action + ", Data=[" + step.params + "]");
            it("Step " + iStep++ + ": Keyword=" + step.action + ", Data=[" + step.params + "]", function () {
                key.run(step.action, step.params).then(function () {
                });
            }, 120000);
        });
    }, 60000);
}, 60000);
