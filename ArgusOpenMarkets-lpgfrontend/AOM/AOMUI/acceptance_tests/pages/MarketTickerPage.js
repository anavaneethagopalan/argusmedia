var MarketTickerPage = (function () {

    var ItemsCount;

    function MarketTickerPage() {

        //this.MTItems = element.all(by.repeater('marketTickerItem in marketTickerItems'));
        this.MTItems = element.all(by.repeater('item in marketTickerItems'));
        this.productsBtn = element.all(by.className('marketTickerProducts'));
        this.typesBtn = element.all(by.className('marketTickerType'));
        this.filtersList = element.all(by.repeater('item in (filteredModel = (inputModel))'));
    }

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    MarketTickerPage.prototype.Rows = function () {
            return this.MTItems;
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    MarketTickerPage.prototype.products = function () {
        return this.productsBtn;
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    MarketTickerPage.prototype.types = function () {
        return this.typesBtn;
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    MarketTickerPage.prototype.filters = function () {
        return this.filtersList;
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    MarketTickerPage.prototype.getSelectedTypes = function () {
        return this.selectedTypes.getText();
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    MarketTickerPage.prototype.setSelectedTypes = function (optionNumber) {
        element(by.model('selectedType')).element.all(by.tagName("option")).then(function(options) {
            options[optionNumber].click();
        });
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    MarketTickerPage.prototype.getSelectedProducts = function () {
        return this.selectedProducts.getText();
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    MarketTickerPage.prototype.setSelectedProducts = function (optionNumber) {
        element(by.model('selectedProduct')).element.all(by.tagName("option")).then(function(options) {
            options[optionNumber].click();
        });
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    return MarketTickerPage;
})();

module.exports = MarketTickerPage;
