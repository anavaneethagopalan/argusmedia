var NewOrderPage = (function () {
    var bDisplayed;
    var sFormHeading;
    var priceText;
    var quantityText;

    function NewOrderPage() {
        var timeout = 5;  //Seconds to wait

        for (var i = 0; i <= timeout; i++) {
            element.all(by.id('btnNewOrderExit')).then(function (buttons) {
                if (buttons.length > 0) {
                    i = timeout;
                } else {
                    browser.sleep(1000);
                }
            });
        }

        element(by.name('newOrderForm')).getText().then(function (FormHeading) {
           sFormHeading = FormHeading.substr(0, FormHeading.indexOf('\n'));
        }); //FormHeading
/*
        element(by.model('order.price')).getText().then(function (Price) {
            priceText = Price;
            console.log('priceText = Price : ' + Price);
        }); //

        element(by.model('order.quantity')).getText().then(function (Quantity) {
            quantityText = Quantity;
            console.log('quantityText = Quantity : ' + Quantity);
        }); //
*/
        this.allFormText = element(by.name('newOrderForm')).getText();
        this.priceField = element(by.model('order.price'));
        this.quantityField = element(by.model('order.quantity'));

        this.exitButton = element(by.id('btnNewOrderExit'));
        this.createButton = element(by.id('btnNewOrderCreate'));

        bDisplayed = element(by.name('newOrderForm')).isDisplayed();
    }

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.displayed = function () {
        return bDisplayed;
    };


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.formText = function () {
        return this.allFormText;
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.price = priceText;

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.getPrice = function () {
        return this.priceField.getAttribute('value');
    };

// -------------------------------------------------------------------------// -------------------------------------------------------------------------
    NewOrderPage.prototype.setPrice = function (sPrice) {
        this.priceField.sendKeys(sPrice);
        return this.priceField.getAttribute('value');
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.getQuantity = function () {
        return this.quantityField.getAttribute('value');
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.setQuantity = function (sQuantity) {
        this.quantityField.sendKeys(sQuantity);
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.headingText = function () {
        return sFormHeading;
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.createEnabled = function () {
        var bEnabled = false;

        if (this.createButton.getAttribute('disabled') != true){
            bEnabled = true;
        }
        return  bEnabled;
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.createDisabled = function () {
        return this.createButton.getAttribute('disabled');
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.create = function () {
        this.createButton.click();
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.hoverCreate = function () {
        this.createButton.click();
//        browser.actions().moveToElement(this.createButton).perform();
        browser.actions().mouseMove(this.createButton).perform();
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    NewOrderPage.prototype.exit = function () {
        this.exitButton.click();
    };

    return NewOrderPage;
})();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
module.exports = NewOrderPage;