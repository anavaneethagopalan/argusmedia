fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var dashboard;

var automation = new Automation();
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Login messages', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;
    var specDescription = this.description;
    var saveBeforeSignin = false;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    //console.log('\r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        //console.log(sBrowserName)
    });


    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("displays login window", function () {
        var specIt = "displays login window";

        var login = new LoginPage();
        login.waitForLoad(20).then(function (SignInButtons) {
            expect(SignInButtons.length).toEqual(1);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + '.png');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("failing credentials",
        //[['','', 'No User found with that username!','Username must be entered.'],
        //[username,'','No User found with that username!','Password must be entered.'],
        //['','password','No User found with that username!','Username must be entered.'],
        [['NoSuchUser','password','Invalid login details, please try again'],
        [username,'incorrect','Invalid login details, please try again'],
        [username + 'blocked','password','This account is not authorised from AOM. Please contact the Argus AOM administration team. Email: AOMSupport@argusmedia.com. Tel +44 020X XXX XXXX']],
            function(user, pwd, expMsg) {
            it("fails for user: '" + user + "'/'" + pwd + "'", function () {
                var specIt = "fails for user";
                var login = new LoginPage();
                login.login(user, pwd, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + user + " pwd=" +  pwd + ".png");
                expect(login.failText()).toEqual(expMsg);
            });
        });


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('pauses', function () {
        var stream;
        var newOrder;
        console.log('pause'); //Newline
    });
}, 60000);
