fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
//var dashboard;

var automation = new Automation();


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
//describe(temp +'-Login functionality', function () {
describe('Login functionality', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;

    var saveBeforeSignin = false;

    var specDescription = this.description;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    //console.log('\r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        //console.log(sBrowserName)
    });


    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("displays login window", function () {
        var specIt = "displays login window";

        var login = new LoginPage();
        login.waitForLoad(20).then(function (SignInButtons) {
            expect(SignInButtons.length).toEqual(1);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + '.png');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("failing credentials",
        //[['',''],
        //[username,''],
        //['','password'],
        [['NoSuchUser','password'],
        [username,'incorrect'],
        [username + 'blocked','password']],
            function(user, pwd) {
            it("fails for user: '" + user + "'/'" + pwd + "'", function () {
                var specIt = "fails for user";
                var login = new LoginPage();
                login.login(user, pwd, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + user + " pwd=" +  pwd + ".png");
                expect(login.failText()).not.toEqual(''); // non-blank is enough
            });
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("succeeds for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "succeeds for user";
        var login = new LoginPage();

        login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.waitForLoad(30).then(function (BidButtons) {
           expect(BidButtons.length).not.toEqual(0);
        });
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');

    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs out", function () {
        var specIt = "logs out";
        var dashboard = new DashboardPage(); // get the dashboard page.
        var login = new LoginPage();

        dashboard.logout(hostURL);
        login.waitForLoad(10);
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + '.png');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('pauses', function () {
        var stream;
        var newOrder;
        console.log('pause'); //Newline
    });
}, 60000);
