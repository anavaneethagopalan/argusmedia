// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('template test scenario', function () {
    var hostURL;
    var iStep = 1;
    var specDescription = this.description;

    var username = browser.params.credentials.username;
    var password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("DoSomething A", function () {
        var dataItem1='DataItem';

        // code for functionality goes here
        // can use expect().toEqual() for assertions
    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("DoSomething B", function () {
        var dataItem1='DataItem';

        // code for functionality goes here
        // can use expect().toEqual() for assertions
    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("DoSomething C", function () {
        var dataItem1='DataItem';

        // code for functionality goes here
        // can use expect().toEqual() for assertions
    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("DoSomething D", function () {
        var dataItem1='DataItem';

        // code for functionality goes here
        // can use expect().toEqual() for assertions
    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("DoSomething E", function () {
        // code for functionality goes here
        // can use expect().toEqual() for assertions
    },120000);


}, 60000);