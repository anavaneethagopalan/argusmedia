fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var NewOrderPage = require("./pages/neworderpage");

var automation = new Automation();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('New order', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;

    var saveBeforeSignin = false;
    var specDescription = this.description;

    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
    });

    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

    login = new LoginPage();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("displays login window", function () {
        var specIt = "displays login window";

        login.waitForLoad(20).then(function (SignInButtons) {
            expect(SignInButtons.length).toEqual(1);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + '.png');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs in for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "logs in for user";

        var dashboard = new DashboardPage(); // get the dashboard page.

        for (var i = 0; i <= 5; i++) {
            login.isDisplayed().then(function (SignInButtons) {
                if (SignInButtons.length === 1){
                    login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
                    dashboard.waitForLoad(20).then(function (BidButtons) {
                        if (BidButtons.length = 0) {
                            i = 6;
                        }
                    });
                }
            });
        }

        dashboard.getProductTitles().then(function(ProductTitles){
            expect(ProductTitles.length).not.toEqual(0);
            //expect(ProductTitles.length).toEqual('ProductTitles.length');
        });

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('opens the new order modals', function () {
        var specIt = "opens the new order modals";
        var newOrder;

        var dashboard = new DashboardPage(); // get the dashboard page.

        dashboard.getProductTitles().then(function(ProductTitles){
            expect(ProductTitles.length).not.toEqual(0);

            for (var i = 0; i < ProductTitles.length; i++) {
// Bid --------------------------
                dashboard.newBid(i);
                newOrder = new NewOrderPage(); //Is it displayed?
                expect(newOrder.displayed()).toEqual(true);
                automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + ".BID " + (i+1) +  '.png');
                newOrder.exit(); //Close the new order form
// Ask ---------------------------
                dashboard.newAsk(i);
                newOrder = new NewOrderPage(); //Is it displayed?
                expect(newOrder.displayed()).toEqual(true);
                automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + ".ASK " + (i+1) +  '.png');
                newOrder.exit(); //Close the new order form
            } //For each grid
        });

    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs out", function () {
        var specIt = "logs out";
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.logout(hostURL);
        login.waitForLoad(10);
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + '.png');
    });


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("opens the new order modal", function () {
        var specIt = "opens the new order modal";
        var dashboard = new DashboardPage(); // get the dashboard page.


        element.all(by.id('newBidButton')).then(function(newBidButtons) {
            //expect(newBidButtons.length).toEqual("newBidButtons");
            expect(newBidButtons.length).not.toEqual(0);
            newBidButtons[5].click();
            //Is it displayed?
            newOrder = new NewOrderPage();
            expect(newOrder.displayed()).toEqual(true);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + ".BID " + '.png');
            newOrder.exit(); //Close the new order form
        });

        element.all(by.id('newAskButton')).then(function(newAskButtons) {
            //expect(newAskButtons.length).toEqual("newAskButtons");
            newAskButtons[0].click();
            //Is it displayed?
            newOrder = new NewOrderPage();
            expect(newOrder.displayed()).toEqual(true);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + ".ASK " +  '.png');
            newOrder.exit(); //Close the new order form
        });

    });

}, 60000);
