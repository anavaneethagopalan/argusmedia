var DatabaseFunctions = (function () {

    function DatabaseFunctions() {
        //this.retObj = {name: "name", value: "default"}
    }

// ----------------------------------------------------------------
// ----------------------------------------------------------------
    DatabaseFunctions.prototype.getNameOfUserBAK = function (username, retObj) {
        var mysql = require('mysql');

        function what(callback) {

            var connection = mysql.createConnection({
                //debug: true,
                host: 'aominstancedev.co46fwzab7bh.eu-west-1.rds.amazonaws.com',
                user: 'aom',
                password: 'Argu5Med1a',
                database: 'crm'
            });

            connection.connect();

            connection.query("SELECT Name AS name FROM UserInfo WHERE Username = '" + username + "'", function (err, rows) {
                if (err) {
                    console.log(err);
                    throw err;
                }
                console.log ("1. inside getNameOfUser: " + retObj.value);
                retObj.value = rows[0].name;
                console.log ("2. inside getNameOfUser: " + retObj.value);
                callback();
            });

            connection.end();
        }

        function setObj() {
            console.log ("3. inside setOBj: " + retObj.value);
        }

        what(setObj);

    };

// ----------------------------------------------------------------
// ----------------------------------------------------------------
    DatabaseFunctions.prototype.getNameOfUser = function (username, callback) {
        var mysql = require('mysql');

        var connection = mysql.createConnection({
            //debug: true,
            host: 'aominstancedev.co46fwzab7bh.eu-west-1.rds.amazonaws.com',
            user: 'aom',
            password: 'Argu5Med1a',
            database: 'crm'
        });

        connection.connect();
        connection.query("SELECT Name AS name FROM UserInfo WHERE Username = '" + username + "'", function (err, rows) {
            if (err) {
                console.log(err);
                throw err;
            }
            //console.log ("1. inside getNameOfUser: " + rows[0].name);
            callback(rows[0].name);
        });
        connection.end();
    };

// ----------------------------------------------------------------
// ----------------------------------------------------------------
    DatabaseFunctions.prototype.testUsersSetup = function (username, password){
        var mysql = require('mysql');

        var connection = mysql.createConnection({
            //debug: true,
            host: 'aominstancedev.co46fwzab7bh.eu-west-1.rds.amazonaws.com',
            user: 'aom',
            password: 'Argu5Med1a',
            database: 'crm'
        });

        connection.connect();
// ----------------------------------------------------------------
// This is the integration test user that will be used by login and other GUI tests
// ----------------------------------------------------------------
        connection.query("DELETE FROM UserToken WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserSession WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfoRole WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfo WHERE Name='" + username + "'", function (err, result) {
            if (err) throw err;
        });

        connection.query("INSERT INTO UserInfo SET Organisation_Id_fk=1, Email='" + username + "', Name='" + username + "', Title='Mr.', Telephone='+44 1234567890', Username='" + username + "', Password='" + password + "', IsActive=1, Token='1234567890ABCDEF',IsDeleted=0,IsBlocked=0", function (err, result) {
            if (err) throw err;
        });

        connection.query("INSERT INTO UserInfoRole SELECT Id, 2 FROM UserInfo WHERE Name = '" + username + "'", function (err, result) {
            if (err) throw err;
        });
// ----------------------------------------------------------------
// This is the integration test user that will be used by login and other GUI tests
// ----------------------------------------------------------------
        connection.query("DELETE FROM UserToken WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "forblocking')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserSession WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "forblocking')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfoRole WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "forblocking')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfo WHERE Name='" + username + "forblocking'", function (err, result) {
            if (err) throw err;
        });

        connection.query("INSERT INTO UserInfo SET Organisation_Id_fk=1, Email='" + username + "forblocking', Name='" + username + "forblocking', Title='Mr.', Telephone='+44 1234567890', Username='" + username + "forblocking', Password='" + password + "', IsActive=1, Token='1234567890ABCDEF',IsDeleted=0,IsBlocked=0", function (err, result) {
            if (err) throw err;
        });

        connection.query("INSERT INTO UserInfoRole SELECT Id, 2 FROM UserInfo WHERE Name = '" + username + "forblocking'", function (err, result) {
            if (err) throw err;
        });

// ----------------------------------------------------------------
// This is the blocked test user that will be used by login tests
// ----------------------------------------------------------------
        connection.query("DELETE FROM UserToken WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "blocked')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserSession WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "blocked')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfoRole WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "blocked')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfo WHERE Name='" + username + "blocked'", function (err, result) {
            if (err) throw err;
        });

        connection.query("INSERT INTO UserInfo SET Organisation_Id_fk=1, Email='" + username + "blocked', Name='" + username + "blocked', Title='Mr.', Telephone='+44 1234567890', Username='" + username + "blocked', Password='" + password + "', IsActive=1, Token='1234567890ABCDEF',IsDeleted=0,IsBlocked=1", function (err, result) {
            if (err) throw err;
        });

        connection.query("INSERT INTO UserInfoRole SELECT Id, 2 FROM UserInfo WHERE Name = '" + username + "blocked'", function (err, result) {
            if (err) throw err;
        });

        connection.end();

    };

// ----------------------------------------------------------------
// ----------------------------------------------------------------
    DatabaseFunctions.prototype.testUsersUnblock = function (username){
        var mysql = require('mysql');

        var connection = mysql.createConnection({
            debug: true,
            host: 'aominstancedev.co46fwzab7bh.eu-west-1.rds.amazonaws.com',
            user: 'aom',
            password: 'Argu5Med1a',
            database: 'crm'
        });

        connection.connect();
// ----------------------------------------------------------------
// This is the integration test user that will be used by login and other GUI tests
// ----------------------------------------------------------------
        connection.query("UPDATE crm.UserInfo SET IsBlocked=0 WHERE Name = '" + username + "')", function (err, result) {
            if (err) throw err;
        });

        connection.end();

    };

// ----------------------------------------------------------------
// ----------------------------------------------------------------
    DatabaseFunctions.prototype.testUsersCleanUp = function (username) {
        var mysql = require('mysql');

        var connection = mysql.createConnection({
            //debug    : true,
            host: 'aominstancedev.co46fwzab7bh.eu-west-1.rds.amazonaws.com',
            user: 'aom',
            password: 'Argu5Med1a',
            database: 'crm'
        });

        connection.connect();
    // ----------------------------------------------------------------
    // This is the integration test user that will be used by login and other GUI tests
    // ----------------------------------------------------------------
        connection.query("DELETE FROM UserToken WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserSession WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfoRole WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfo WHERE Name='" + username + "'", function (err, result) {
            if (err) throw err;
        });

    // ----------------------------------------------------------------
    // This is the forblocking test user that will be used by login tests
    // ----------------------------------------------------------------
        connection.query("DELETE FROM UserToken WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "forblocking')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserSession WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "forblocking')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfoRole WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "forblocking')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfo WHERE Name='" + username + "forblocking'", function (err, result) {
            if (err) throw err;
        });

        // ----------------------------------------------------------------
        // This is the blocked test user that will be used by login tests
        // ----------------------------------------------------------------
        connection.query("DELETE FROM UserToken WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "blocked')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserSession WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "blocked')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfoRole WHERE UserInfo_Id_fk IN (SELECT Id FROM UserInfo WHERE Name = '" + username + "blocked')", function (err, result) {
            if (err) throw err;
        });

        connection.query("DELETE FROM UserInfo WHERE Name='" + username + "blocked'", function (err, result) {
            if (err) throw err;
        });

        connection.end();
    };

    return DatabaseFunctions;

})();

module.exports = DatabaseFunctions;