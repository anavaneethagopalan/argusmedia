fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var dashboard;

var automation = new Automation();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
//describe(temp +'-Login functionality', function () {
describe('Login remember me', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;

    var saveBeforeSignin = false;

    var specDescription = this.description;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    //console.log('\r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        //console.log(sBrowserName)
    });


    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

    login = new LoginPage();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("initial state should be unchecked for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "starts with unchecked for user";

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.OnStart.png');

        login.getRememberMe().then(function (Rem) {
            if (Rem === 'true') {
                login.setRememberMe();
                expect(login.getRememberMe()).toEqual(null);

                //Changed it so have to login and logout to make it stick
                login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
                dashboard = new DashboardPage(); // get the dashboard page.
                dashboard.waitForLoad(20);
                dashboard.productTitles().then(function(ProductTitles){
                    expect(ProductTitles.length).not.toEqual(0);
                });
                automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
                dashboard.logout(hostURL);
                expect(login.getUsername()).toEqual('');
                //expect(login.getUsername()).toEqual('PEEK');
            }
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("tick it and log in for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "tick it and log in for user";

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.OnStart.png');

        login.getRememberMe().then(function (Rem) {
            if (Rem === null) {
                login.setRememberMe();
                expect(login.getRememberMe()).toEqual('true');

                //Changed it so have to login and logout to make it stick
                login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
                dashboard = new DashboardPage(); // get the dashboard page.
                dashboard.waitForLoad(20);
                dashboard.productTitles().then(function(ProductTitles){
                    expect(ProductTitles.length).not.toEqual(0);
                });
                automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
                dashboard.logout(hostURL);
                expect(login.getUsername()).toEqual(username);
                //expect(login.getUsername()).toEqual('PEEK');
             }
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("untick it and log in for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "untick it and log in for user";

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.OnStart.png');

        login.getRememberMe().then(function (Rem) {
            if (Rem === 'true') {
                login.setRememberMe();
                expect(login.getRememberMe()).toEqual(null);

                //Changed it so have to login and logout to make it stick
                login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
                dashboard = new DashboardPage(); // get the dashboard page.
                dashboard.waitForLoad(20);
                dashboard.productTitles().then(function(ProductTitles){
                    expect(ProductTitles.length).not.toEqual(0);
                });
                automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
                dashboard.logout(hostURL);
                expect(login.getUsername()).toEqual('');
                //expect(login.getUsername()).toEqual('PEEK');
            }
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("tick it and fail to log in for user: '" + username + "'/''", function () {
        var specIt = "tick it and fail to log in for user";

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + '.OnStart.png');

        login.getRememberMe().then(function (Rem) {
            if (Rem === 'true') {
                login.setRememberMe();
                expect(login.getRememberMe()).toEqual(null);

                //Changed it so have to login and logout to make it stick
                login.login(username, "", saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + ".png");

                browser.reload();
                expect(login.getRememberMe()).toEqual('true');
                expect(login.getUsername()).toEqual(username);
            }
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("tick it and log in again for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "tick it and log in again for user";

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.OnStart.png');

        login.getRememberMe().then(function (Rem) {
            if (Rem === null) {
                login.setRememberMe();
                expect(login.getRememberMe()).toEqual('true');

                //Changed it so have to login and logout to make it stick
                login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
                dashboard = new DashboardPage(); // get the dashboard page.
                dashboard.waitForLoad(20);
                dashboard.productTitles().then(function(ProductTitles){
                    expect(ProductTitles.length).not.toEqual(0);
                });
                automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
                dashboard.logout(hostURL);
                expect(login.getUsername()).toEqual(username);
                //expect(login.getUsername()).toEqual('PEEK');
            }
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("untick it and fail to log in for user: '" + username + "'/''", function () {
        var specIt = "untick it and fail to log in for user";

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + '.OnStart.png');

        login.getRememberMe().then(function (Rem) {
            if (Rem === null) {
                login.setRememberMe();
                expect(login.getRememberMe()).toEqual('true');

                //Changed it so have to login and logout to make it stick
                login.login(username, "", saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + ".png");

                browser.reload();
                expect(login.getRememberMe()).toEqual(null);
                expect(login.getUsername()).toEqual('');
            }
        });
    });


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('pauses', function () {
        var stream;
        var newOrder;
        console.log('pause'); //Newline
    });
}, 60000);
