fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var NewOrderPage = require("./pages/neworderpage");

var automation = new Automation();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('New orders', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;

    var saveBeforeSignin = false;
    var specDescription = this.description;

    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
    });

    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

    login = new LoginPage();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("displays login window", function () {
        var specIt = "displays login window";

        login.waitForLoad(20).then(function (SignInButtons) {
            expect(SignInButtons.length).toEqual(1);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + '.png');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs in for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "logs in for user";

        var dashboard = new DashboardPage(); // get the dashboard page.

        username = 't1';
        for (var i = 0; i <= 5; i++) {
            login.isDisplayed().then(function (SignInButtons) {
                if (SignInButtons.length === 1){
                    login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
                    dashboard.waitForLoad(20).then(function (BidButtons) {
                        if (BidButtons.length = 0) {
                            i = 6;
                        }
                    });
                }
            });
        }

        dashboard.getProductTitles().then(function(ProductTitles){
            expect(ProductTitles.length).not.toEqual(0);
            //expect(ProductTitles.length).toEqual('ProductTitles.length');
        });

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('opens the new order modals', function () {
        var specIt = "opens the new order modals";
        var newOrder;

        var dashboard = new DashboardPage(); // get the dashboard page.

        dashboard.getProductTitles().then(function(ProductTitles){
            expect(ProductTitles.length).not.toEqual(0);

            for (var i = 0; i < ProductTitles.length; i++) {
// Bid --------------------------
                dashboard.newBid(i);
                newOrder = new NewOrderPage(); //Is it displayed?
                expect(newOrder.displayed()).toEqual(true);
                automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + ".BID " + (i+1) +  '.png');
                newOrder.exit(); //Close the new order form
// Ask ---------------------------
                dashboard.newAsk(i);
                newOrder = new NewOrderPage(); //Is it displayed?
                expect(newOrder.displayed()).toEqual(true);
                automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + ".ASK " + (i+1) +  '.png');
                newOrder.exit(); //Close the new order form
            } //For each grid
        });

    });
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("place orders",
        [[0, 40, 'BID', 965, 1]],
        function(product, number, bidask, startprice, priceincrement) {
            xit("places " + number +  " " + bidask + " order(s) for product " + product, function () {
                var specIt = "places " + number +  " " + bidask + " order(s) for product " + product;
                var newOrder; // = new NewOrderPage();
                var dashboard = new DashboardPage(); // get the dashboard page.
                var i;

                if (bidask.substr(0, 1).toUpperCase() === 'B')  {
                    for (i = 0; i < number; i++) {
                        dashboard.newBid(product);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice+i);
                        newOrder.create();
                    }
                } else {
                    for (i = 0; i < number; i++) {
                        dashboard.newAsk(product);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice+i);
                        newOrder.create();
                    }
                }
            }, 1200000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("place orders",
        [[0, 10, 'BID', 500, 1]],
 //       [0, 50, 'ASK', 500, -1]],
/*
        [[0, 2, 'ASK', 140, -1],
         [0, 2, 'BID', 1210, 1],
         [1, 2, 'ASK', 1, -1],
         [1, 2, 'BID', 1, 1],
         [0, 2, 'ASK', 138, -1],
         [0, 2, 'BID', 1212, 1]],
*/
        function(product, number, bidask, startprice, priceincrement) {
            xit("places " + number +  " " + bidask + " order(s) for product " + product, function () {
                var specIt = "places " + number +  " " + bidask + " order(s) for product " + product;
                var newOrder; // = new NewOrderPage();
                var dashboard = new DashboardPage(); // get the dashboard page.
                var i;

                if (bidask.substr(0, 1).toUpperCase() === 'B')  {
                    for (i = 0; i < number; i++) {
                        dashboard.newBid(product);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice+priceincrement*i);
                        newOrder.create();
                    }
                } else {
                    for (i = 0; i < number; i++) {
                        dashboard.newAsk(product);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice+priceincrement*i);
                        newOrder.create();
                    }
                }
            }, 1200000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("place orders 2",
        [[10, 0, 'BID', 500, 1, 0, 'ASK', 500, -1 ]],
//        [[10, 1, 'BID', 1, 0.25, 1, 'ASK', 3, -0.25 ]],
        function(number, product1, bidask1, startprice1, priceincrement1, product2, bidask2, startprice2, priceincrement2) {
            it("places " + number +  " " + bidask1 + " order(s) for product " + product1, function () {
                var specIt = "places " + number +  " " + bidask1 + " order(s) for product " + product1;
                var newOrder; // = new NewOrderPage();
                var dashboard = new DashboardPage(); // get the dashboard page.
                var i;

                for (i = 0; i < number; i++) {
                    if (bidask1.substr(0, 1).toUpperCase() === 'B')  {
                        dashboard.newBid(product1);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice1+priceincrement1*i);
                        newOrder.create();
                    } else {
                        dashboard.newAsk(product1);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice1+priceincrement1*i);
                        newOrder.create();
                    }
                    browser.sleep(500);
                    if (bidask2.substr(0, 1).toUpperCase() === 'B')  {
                        dashboard.newBid(product2);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice2+priceincrement2*i);
                        newOrder.create();
                    } else {
                        dashboard.newAsk(product2);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice2+priceincrement2*i);
                        newOrder.create();
                    }
                    browser.sleep(500);
                }
            }, 1200000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("can be paused", function () {
        var specIt = "can be paused";
        expect(true).toEqual(true);
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs out", function () {
        var specIt = "logs out";
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.logout(hostURL);
        login.waitForLoad(10);
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + '.png');
    });

}, 1200000);
