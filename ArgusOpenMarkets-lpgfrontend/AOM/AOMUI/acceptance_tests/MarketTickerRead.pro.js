var Automation = require("./automation");
var automation = new Automation();
var MarketTickerPage = require("./pages/markettickerpage");

// Require keyword library
var key = require('keyword');
key(require('./AOMKeywordsSignin'));

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Market ticker read', function () {
    var hostURL;
    var iStep = 1;
    var specDescription = this.description;

    var username = browser.params.credentials.username;
    var password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);
    //browser.executeScript("window.onbeforeunload = function(){};");

    xit("sets things up", function () {
        browser.executeScript("window.onbeforeunload = function(){};");
    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("Market ticker read",
        [   ['Signin', ['t1', 'password', 'false']]],
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("has items", function () {
        var specIt = "has items";
        var iRow = 0;

        var marketTicker = new MarketTickerPage();

        marketTicker.Rows().then(function (Rows) {
            expect(Rows.length).not.toEqual(0);
            //expect(Rows.length).toEqual("Rows.length");
            expect(Rows[0].getText()).not.toEqual('');
            expect(Rows[Rows.length-1].getText()).not.toEqual('');
            console.log("Market Ticker Rows: " + Rows.length);
            //console.log (Rows[0].getText());

            element.all(by.className('panel-body-info-content-container')).then(function (Infos) {
                Infos[0].getCssValue("color").then(function (Color) {
                    console.log("Color[0]:" + Color);
                });
                Infos[0].getText().then(function (Text) {
                    console.log("Text[0]:" + Text);
                });
            });


            for (var iItems = 0; iItems < Rows.length; iItems++) {
                Rows[iItems].getText().then(function (Item) {
                    console.log("Row[" + (iRow++) + "]: " + Item.replace(/\n/g, "~"));
                });
            }

        });

    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("has products filter", function () {
        var specIt = "has products filter";
        var marketTicker = new MarketTickerPage();

        marketTicker.products().then(function (Products) {
            expect(Products[0].getText()).toContain('PRODUCTS');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("products filter defaults to ALL", function () {
        var specIt = "products filter defaults to All";
        var marketTicker = new MarketTickerPage();

        marketTicker.products().then(function (Products) {
            expect(Products[0].getText()).toContain("ALL");
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("can filter products", function () {
        var specIt = "can filter products";
        var automation = new Automation();
        var marketTicker = new MarketTickerPage();
        var iRows = 0;

        marketTicker.Rows().then(function (Rows) {
            iRows = Rows.length;
        });

        marketTicker.products().then(function (Products){
            Products[0].click();

            marketTicker.filters().then(function(Filters) {
                for (var i = 4; i < Filters.length; i++) {    //  first product filter is at 4 (0-3 are types)
                    expect(Filters[i].getAttribute('checked')).toEqual('true'); // filter should be on first
                    Filters[i].click();
                    expect(Filters[i].getAttribute('checked')).toEqual(null);   // Off now
                    if (i < (Filters.length-1)) expect(Products[0].getText()).toContain(Filters.length-5);
                }

                marketTicker.Rows().then(function (Rows) {
                    expect(Rows.length).toBeLessThan(iRows);
                });

                // Turn all product filters on again
                for (var i = 4; i < Filters.length; i++) {    //  first product filter is at 4 (0-3 are types)
                    expect(Filters[i].getAttribute('checked')).toEqual(null);
                    Filters[i].click();
                    expect(Filters[i].getAttribute('checked')).toEqual('true');   // Off now
                }

                expect(Products[0].getText()).toContain("ALL");
                marketTicker.Rows().then(function (Rows) {
                    expect(Rows.length).not.toEqual(0);
                });

            });
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("has types filter", function () {
        var specIt = "has types filter";
        var marketTicker = new MarketTickerPage();

        marketTicker.types().then(function (Types) {
            expect(Types[0].getText()).toContain('TYPES');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("types filter defaults to ALL", function () {
        var specIt = "types filter defaults to All";
        var marketTicker = new MarketTickerPage();

        marketTicker.types().then(function (Types) {
            expect(Types[0].getText()).toContain('ALL');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
   xit("can filter types", function () {
        var specIt = "can filter types";
        var automation = new Automation();
        var marketTicker = new MarketTickerPage();

        marketTicker.types().then(function (Types){
            Types[0].click();

            marketTicker.filters().then(function(Filters) {
                for (var i = 0; i < 4; i++) {    //  filters 0-3 are types
                    expect(Filters[i].getAttribute('checked')).toEqual('true'); // filter should be on first
                    Filters[i].click();
                    expect(Filters[i].getAttribute('checked')).toEqual(null);   // Off now
                    if (i<3) expect(Types[0].getText()).toContain(3-i);
                }

                marketTicker.Rows().then(function (Rows) {
                    expect(Rows.length).toEqual(0);
                });

                // Turn all product filters on again
                for (var i = 0; i < 4; i++) {    //  filters 0-3 are types
                    expect(Filters[i].getAttribute('checked')).toEqual(null);
                    Filters[i].click();
                    expect(Filters[i].getAttribute('checked')).toEqual('true');   // Off now
                }

                expect(Types[0].getText()).toContain("ALL");
                marketTicker.Rows().then(function (Rows) {
                    expect(Rows.length).not.toEqual(0);
                });

            });
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("Market ticker read",
        [   ['Signout', ['none']]],
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });
}, 60000);