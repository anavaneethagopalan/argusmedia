// AOMKeywords.js
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");

var AOMKeywordsOther = {

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "NewOrder": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInBidAskStack": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInMarketTicker": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "OrderInDatabase": function(next, product, bidask, quantity, price, notes ) {
        //console.log("product=" + product + ", bidask=" + bidask + ', quantity=' + quantity);
        next();
    }

};

module.exports = AOMKeywordsOther;