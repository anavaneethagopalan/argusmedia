﻿@AR-98
Feature: Permitted users can connect and log in to the database
	In order to access the database
	As a permitted database user
	I want to be able to connect and login to the database

@ConnectViaApi
Scenario: Connect and login to the database using an API
	Given I pass the connection string databaseConnection
	And I pass the user credentials userCredentials
	When I execute the connect method
	Then the connect method will return success

@ConnectViaCommandLine
Scenario: Connect and login to the database using an command line interface
	Given I type the database command line program name databaseProg at the command line prompt
	And I pass the correct database connection information databaseConnection as an argument
	And I pass valid user credentials userCredentials as arguments
	When I press enter to run the database command line program
	Then the database command line program will run
	And a prompt will be displayed at which database commands may be entered
	But it will not display an error about a connection failure 
