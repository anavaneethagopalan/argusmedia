# AR-46 Bid/Ask Stack - New Order - Cancel

# Acceptance Criteria
# 1. User can cancel at any point during the enter new order process:
# - The new order will not be stored
# - The modal will be closed
# 2. If the new order has been partially entered, i.e. any piece of information has been set such as price, quantity, dates, etc., then the user will be prompted to confirm the cancellation.

# Description
# "As a CT/CB I want to be able to exit the new order modal without committing the order to the stack. - Done so via the exit button."
# Create and Exit button functionality on modal
----
@AR-46
Feature: New order can be abandoned 
	The exit button is always active
	The exit button will abandon the order in progress
	Display a prompt for acceptance that entered data will be lost
	The order will not be created 
	
	@CanAbandonNewOrder
	Scenario Outline: The Exit button will abandon the order 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And I have the permission required to place new orders 
		And I have clicked the +<OrderType> button on the grid headed <GridHeader>
		And the new order window is displayed
		When I click the exit button
		Then the new order window will close 
		
		Examples:
		| Browser | OrderType  | GridHeader          | 
		| IE 11   | Bid        | ArgusNaphtha-CIF... | 
		| IE 10   | Bid        | ArgusNaphtha-CIF... | 
		| IE 9    | Bid        | ArgusNaphtha-CIF... | 
		| IE 8    | Bid        | ArgusNaphtha-CIF... | 
		| Chrome  | Bid        | ArgusNaphtha-CIF... | 
		| Firefox | Bid        | ArgusNaphtha-CIF... | 
		| IE 11   | Ask        | ArgusNaphtha-CIF... | 
		| IE 10   | Ask        | ArgusNaphtha-CIF... | 
		| IE 9    | Ask        | ArgusNaphtha-CIF... | 
		| IE 8    | Ask        | ArgusNaphtha-CIF... | 
		| Chrome  | Ask        | ArgusNaphtha-CIF... | 
		| Firefox | Ask        | ArgusNaphtha-CIF... | 
		
	@ConfirmAbandonNewOrder
	Scenario Outline: The confirmation prompt is displayed 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And I have the permission required to place new orders 
		And I have clicked the +<OrderType> button on the grid headed <GridHeader>
		And the new order window is displayed
		And I have entered some order data  
		When I click the exit button
		Then a confirmation prompt will be displayed
		
		Examples:
		| Browser | OrderType  | GridHeader          | Type | Price | Quantity | Broker |
		| IE 11   | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| IE 10   | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| IE 9    | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| IE 8    | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| Chrome  | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| Firefox | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| IE 11   | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| IE 10   | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| IE 9    | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| IE 8    | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| Chrome  | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| Firefox | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
