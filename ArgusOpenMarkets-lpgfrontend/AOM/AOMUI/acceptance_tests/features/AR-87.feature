# AR-87 Screen - General/Static - Footer appearance

# Acceptance Criteria
# - Ensure links are clickable and active
# - test different AOM log in profiles to ensure the links take the user to correct locations, as per the AD footer.

# Description
# As an AOM user, I want to see the AOM footer in the same format as the Argus Direct website footer, so my user experience is seamless across all Argus products
----
@AR-87
Feature: AOM web page footer conforms to the Argus Direct style and contains clickable links   
	AOM footer is displayed 
	It matches the format of Argus Direct website footer
	It contains clickable linksPopup
	
	@AOMWebPageHasFooter @Manual
	Scenario Outline: AOM web page has a footer
		Given I am using browser <Browser> 
		When I navigate to the AOM web site
		Then the web page contains a footer 

		Examples:
		| Browser |  
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 

	@AOMWebPageFooterInADFormat @Manual
	Scenario Outline: AOM web page footer is in the Argus Direct format
		Given I am using browser <Browser> 
		When I have loaded the AOM web site
		Then the AOM footer format is as the Argus Direct website

		Examples:
		| Browser |  
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 

	@AOMWebPageFooterHasClickableLinks  @Manual
	Scenario Outline: AOM web page footer has clickable links
		Given I am using browser <Browser> 
		And I have loaded the AOM web site
		When I click on a link in the footer
		Then the link target page opens in the browser 

		Examples:
		| Browser |  
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 
