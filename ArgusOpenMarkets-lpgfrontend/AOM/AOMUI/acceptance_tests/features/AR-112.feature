@AR-112 Push Data - Set up Messaging Servers - deploy Amazon image to 2 regions for Dev environments
----
Feature: EU and US-based Diffusion services are available
	Servers to host push service 
	Servers are reachable
	
@ConnectToTheDevelopmentEnvironmentService @Manual
Scenario Outline: Can connect to the Diffusion push service
	Given the server is located in the <location>
	When I initiate a connection to the address <address>
	Then I will be connected

	Examples:
		| location | address                  | 
		| EU       | http://10.27.4.133:8080/ |
		| US       | http://10.27.4.153:8080/ |
