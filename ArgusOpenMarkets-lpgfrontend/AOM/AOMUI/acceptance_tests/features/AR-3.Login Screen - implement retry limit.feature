# AR-3 Login Screen - implement retry limit

# Acceptance Criteria
# 1. If a user attempts to login with an incorrect password more than a system-wide configurable number of times in a row, then that user will be suspended and it will be impossible to login even with the correct password.
# 2. If a suspended user is re-activated, it will then be possible to login.
# 3. If a user attempts to login with an incorrect password less than the limit number of times, then successfully logs in, then attempts to login with an incorrect password less than the limit number of times again, they will not be suspended.

# Description
# "As an AOM user I can only log in to one session at any one time. Additional sessions will not be processed."
# Lock users out after configured number of failed login attempts.


----
@AR-3
Feature: Login Suspension
	A user's account can be suspended (blocked)
	A blocked user cannot log in until the user's account is unblocked
	Exceeding the number of allowed failed login attempts within a set time frame will cause the user account to be blocked
	The number of allowable sequential failed login attempts is configurable.
	
	@AlreadyBlockedLogin
	Scenario Outline: Login is already blocked
	Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have entered a valid username <Username>
		And I have entered the correct password <Password>
		When I click the sign in button
		Then a message <Message> is  displayed
		
		Examples:
		| Browser | Username  | Password | Message                             | 
		| IE 11   | Suspended | Correct  | User is blocked. Please contact ... |
		
	@RepeatedFailingLogin
	Scenario Outline: User is suspended following too many sequential failed login attempts
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have entered a valid username <Username> 
		And I have entered an incorrect password <Password>
		When I click the sign in button X times
		Then the login will fail
		And the error message <Message> is displayed
		
		Examples:
		| Browser | Username      | Password   | Message                                                                      | 
		| IE 11   | Non-suspended | Incorrect  | You have been blocked for too many unsuccessful attempts. Please contact ... |

	@BlockingCountResetAfterSuccessfulLogin
	Scenario Outline: Blocking count is reset after a successful login
		Given I am using browser <Browser> 
		And I have logged in with username <Username> after failing to login X-1 times
		And I have am displaying the AOM login screen
		When I log out 
		And I enter a valid username <Username>
		And I enter an incorrect password <Password>
		And I click the sign in button X-1 times
		Then the login will fail
		And the error message <Message> is displayed
		
		Examples:
		| Browser | Username      | Password   | Message                         | 
		| IE 11   | Non-suspended | Incorrect  | Incorrect user details entered. |