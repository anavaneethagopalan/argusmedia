# AR-110 Push Data - Test messaging scenarios

# Acceptance Criteria (AR-106)
# Test "Hello world" server application and web client created.
# The server will create named data channels (at least 2) and publish data onto theses channels.
# The client will subscribe to multiple data channel and receive push data (time or other variable data) and display it, updating continuously on five clients simultaneously. 
# The client should demonstrate functionality on all supported browsers (Chrome, IE8+, Firefox and Safari).

# Description (AR-106)
# "As an AOM Server I need a mechanism to push information out to all connected clients in order to distribute prices, deals and news to the clients"
# Push new data to clients who are subscribed to a channel or push to a group or single client.
----
@AR-110
Feature: Place message and get acknowledgement from topic
	Place message data on to a topic
	Get acknowledgement message from topic 
	
	
	@GetAcknowledgement @Manual
	Scenario Outline: Get an acknowledgement to a message onto placed onto a topic
		Given Diffusion is running
		And I have opened the "AOM.Services.Client.Tests" project in Visual Studio
		And I put a break point on the line with "Assert.AreEqual(expectedValue,returnValue)" in A_Update_Topic_Value_Test_0
		And I put a break point on the line with "Assert.AreEqual(expectedAcknowledgement,returnAckValue)" in Start_Round_Trip_Continously_Topic_Test_1
		When I run the DiffusionTests module
		And hover the mouse pointer over "returnValue" on the breakpoint line 
		And hover the mouse pointer over "returnAckValue" on the breakpoint line 
		Then variable returnValue contains the text <ReturnValue>
		And variable returnAckValue contains the text <ReturnAckValue>
		
		Examples:
			| ReturnValue | ReturnAckValue      |
			| 1.6789      | Acknowledged:1.6789 |

