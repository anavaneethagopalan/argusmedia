# AR-122 Setup DEV server(s)

# Acceptance Criteria
# 1) 

----
@AR-122-1
Feature: Provide a development environment consisting of servers and associated services for use during the development effort
	In order develop and configure the application software solution 
	As a developer
	I want to be able to access the servers and services that will support my development activities
	
@ConnectToTheDevelopmentEnvironmentService @Manual
Scenario Outline: Can connect to a development environment service
	Given the service address is <address>
	And the username is <username>
	And the password is <password>
	When I initiate a connection to the service 
	Then I have access to the development environment <environment>

	Examples:
		| environment  | address                               | username | password   |
		| Web Server   | http://web.dev.aom.dev.argusmedia.com | Admin    | Argu5Med1a |
		| Database     |                                       |          |            |
		| Diffusion EU | http://10.23.1.129:8080/              |          |            |
		| Diffusion US | http://10.23.1.148:8080/              |          |            |
		| Rabbit MQ    | http://10.23.1.16:15672/#/            |  Admin   | Argu5Med1a |
		
