# AR-111 Push Data - Set up authentication - transport only

# Acceptance Criteria

----
@AR-111
Feature: User Credentials Service
	Client sends user credentials to Diffusion
	Diffusion sends the user credentials in an authentication request message to the authentication service
	Authentication service returns allow/deny access and an identifier token 
	Diffusion passes the token to the client

