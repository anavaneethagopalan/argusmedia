# AR-9 AOM Header - username and company

# Acceptance Criteria
# AOM user name and role displayed in AOM header: "John Adams - XYZ Corp"


# Description
# "As an AOM user I want to see my login name and company in the header, so that I am sure I am logged into AOM as the correct user, with my privileges and permissions."
# Display user name and company in header, as retrieved from DB.

----
@AR-9
Feature: Login name and organisation in the header
	User's "Name" is displayed in the dashboard header
	The user's organisation name is displayed in the header
	
	@LoginNameAndOrganisationDisplayedInHeader
	Scenario Outline: Login name and organisation displayed in the header
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I enter username <Username> and password <Password>
		When I click the signing button
		Then the AOM dashboard is displayed
		And my name is displayed as <Name> in the header
		And my organisation is displayed as <Organisation> in the header
		
		Examples:
		| Browser | Username    | Password | Name        | Organisation |
		| IE 11   | IntTestnnnn | password | IntTestnnnn | Argus Media  |
