﻿Feature: Execute queries on the database and return data
	In order to get data from the database
	As a permitted database user
	I want to run queries that return data from the database

@QueryViaCommandLine
Scenario: Run a query using the database command line inteface and return data
	Given I have connected and logged in to the database
	And the database command line prompt is displayed
	And I enter the query SQLQuery at the prompt
	When I press enter
	Then the query will be executed 
	And the results will be displayed
