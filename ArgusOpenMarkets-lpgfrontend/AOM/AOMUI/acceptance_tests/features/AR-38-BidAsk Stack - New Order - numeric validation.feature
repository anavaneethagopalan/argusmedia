# AR-38 Bid/Ask Stack - New Order - numeric validation

# Acceptance Criteria
# Price and Quantity fields must be validated and limited to certain ranges, dependent upon the product. 
# Attempts to enter a new order where the fields break any of the following validation rules must fail with an appropriate error message and highlighting.
# 1. Quantity validation:
#  - Decimal Places
#  - Significant Figures
#  - Minimum
#  - Maximum
#  - Increment
# 2. Price validation:
#  - Decimal Places
#  - Significant Figures
#  - Minimum
#  - Maximum
#  - Increment
# Current products rules are as follows:
# 1. Naphtha Outright:
#  - Quantity: Min 12,500, Max 35,000, Increment 500
#  - Price: DP 2, SF 8, Min 0, Max 9,999
# 2. Naphtha Diff:
#  - Quantity: Min 12,500, Max 35,000, Increment 500
#  - Price: DP 2, SF 8, Min -9,999, Max 9,999

# Description
# "As a CT/CB I need to be able to enter in numerical digits into the price and quantity fields of the order modals. If the index is an outright, then positive numerals only, up to eight including decimal (to 2 dp). 
# If the Index is a Dif, eight numeric fields, including decimal (to 2 dp) in either positive or negative of zero."
# Price fields. Must be positive and numerical. Limit to eight significant figures
# NB - the validations are defined in the database on the product table.

----
@AR-38
Feature: New order numeric validation 
	
	@InvalidPrice
	Scenario Outline: Error message for invalid price 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And I have the permission required to place new orders 
		And I have clicked the +<OrderType> button on the grid headed <GridHeader>
		And the new order window is displayed
		When I click order type button <Type> 
		And I enter price <Price>
		Then the message <Message> is displayed
		And Price field turns red
		
		Examples:
		| Browser | OrderType  | GridHeader          | Type | Price | Message |
		| IE 11   | Bid        | ArgusNaphtha-CIF... | BID  |       |         |
		| IE 10   | Bid        | ArgusNaphtha-CIF... | BID  |       |         |
		| IE 9    | Bid        | ArgusNaphtha-CIF... | BID  |       |         |
		| IE 8    | Bid        | ArgusNaphtha-CIF... | BID  |       |         |
		| Chrome  | Bid        | ArgusNaphtha-CIF... | BID  |       |         |
		| Firefox | Bid        | ArgusNaphtha-CIF... | BID  |       |         |
		| IE 11   | Ask        | ArgusNaphtha-CIF... | ASK  |       |         |
		| IE 10   | Ask        | ArgusNaphtha-CIF... | ASK  |       |         |
		| IE 9    | Ask        | ArgusNaphtha-CIF... | ASK  |       |         |
		| IE 8    | Ask        | ArgusNaphtha-CIF... | ASK  |       |         |
		| Chrome  | Ask        | ArgusNaphtha-CIF... | ASK  |       |         |
		| Firefox | Ask        | ArgusNaphtha-CIF... | ASK  |       |         |
		
	@InvalidQuantity
	Scenario Outline: Error message for invalid quantity 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And I have the permission required to place new orders 
		And I have clicked the +<OrderType> button on the grid headed <GridHeader>
		And the new order window is displayed
		When I click order type button <Type> 
		And I enter quantity <Quantity>
		Then the message <Message> is displayed
		And quantity field turns red
		
		Examples:
		| Browser | OrderType  | GridHeader          | Type | Quantity | Message |
		| IE 11   | Bid        | ArgusNaphtha-CIF... | BID  |          |         |
		| IE 10   | Bid        | ArgusNaphtha-CIF... | BID  |          |         |
		| IE 9    | Bid        | ArgusNaphtha-CIF... | BID  |          |         |
		| IE 8    | Bid        | ArgusNaphtha-CIF... | BID  |          |         |
		| Chrome  | Bid        | ArgusNaphtha-CIF... | BID  |          |         |
		| Firefox | Bid        | ArgusNaphtha-CIF... | BID  |          |         |
		| IE 11   | Ask        | ArgusNaphtha-CIF... | ASK  |          |         |
		| IE 10   | Ask        | ArgusNaphtha-CIF... | ASK  |          |         |
		| IE 9    | Ask        | ArgusNaphtha-CIF... | ASK  |          |         |
		| IE 8    | Ask        | ArgusNaphtha-CIF... | ASK  |          |         |
		| Chrome  | Ask        | ArgusNaphtha-CIF... | ASK  |          |         |
		| Firefox | Ask        | ArgusNaphtha-CIF... | ASK  |          |         |
