# AR-36 Bid/Ask Stack - New Order - General appearance

# Acceptance Criteria
# 1) Modal only accessible given appropriate permissions.
# 2) Heading matches market.
# 3) Bid Ask colours toggle correctly.

# Description
# As an AOM client, I need the new order entry modal to have fields that are permissioned to my user profile (CT/CB), so that I can easily enter new orders to the bid ask stack.
# Elements and criteria of modal:
# - Modal is headlined with the traded index (e.g. - "Argus Naphtha CIF NWE - New Order"
# - Order type selectable by coloured buttons (Blue = Bid / Sell = Red)
# NB. Refer to signed-off UX documentation to see final agreed layout, contents and styles. These will have changed since the PoC images were attached.
----
@AR-36
Feature: New order window
	The new order is available only to permitted users
	It is opened when the +bid or +ask buttons are clicked 
	It is modal
	Its heading text matches the bid/ask grid heading text
	The order type is indicated by blue (for bid) or red (for ask) highlighting
	The overall style matches the signed-off specification at http://ketpatel.com/demos/aom/argus_open_markets.html
	
	@OpenNewOrderWindow
	Scenario Outline: Open a new order window for bid 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboardbid/ask
		And I have the permission required to place new orders 
		When I click the +<OrderType> button on the grid headed <GridHeader>
		Then new order window for <OrderType> opens
		And the window is modal
		And the order type <OrderType> is highlighted
		And the window has a heading that matches <GridHeader>
		And the window style matches the style shown at <Specification>
		
		Examples:
		| Browser | OrderType  | GridHeader          | Specification                                         | 
		| IE 10   | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 9    | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 8    | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Chrome  | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Firefox | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 10   | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 9    | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 8    | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Chrome  | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Firefox | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		
	@NoPermissionForNewOrder
	Scenario Outline: Open a new order window for bid 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboardbid/ask
		And I do not have the permission required to place new orders 
		When I click the +<OrderType> button on the grid headed <GridHeader>
		Then a new order window does not open
		
		Examples:
		| Browser | OrderType  | GridHeader          | 
		| IE 10   | Bid        | ArgusNaphtha-CIF... | 
		| IE 9    | Bid        | ArgusNaphtha-CIF... | 
		| IE 8    | Bid        | ArgusNaphtha-CIF... | 
		| Chrome  | Bid        | ArgusNaphtha-CIF... | 
		| Firefox | Bid        | ArgusNaphtha-CIF... | 
		| IE 10   | Ask        | ArgusNaphtha-CIF... | 
		| IE 9    | Ask        | ArgusNaphtha-CIF... | 
		| IE 8    | Ask        | ArgusNaphtha-CIF... | 
		| Chrome  | Ask        | ArgusNaphtha-CIF... | 
		| Firefox | Ask        | ArgusNaphtha-CIF... | 
		
		
	@OpenNewOrderBidWindow
	@NewOrderBidWindowIsModal
	@NewOrderBidWindowMatchingHeading
	@NewOrderBidWindowStyleAsSpecified
	