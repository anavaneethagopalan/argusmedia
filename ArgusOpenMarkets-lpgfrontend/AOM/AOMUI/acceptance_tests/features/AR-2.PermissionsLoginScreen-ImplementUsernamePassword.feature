# AR-2 Permissions - Login Screen - implement username and password

# Acceptance Criteria
# 1. Any attempt to login with a non-existent username will fail.
# 2. Any attempt to login with an incorrect password will fail.
# 3. Any attempt to login when a user is suspended will fail.
# 4. Any attempt to login with a correct username and password and an active user will succeed.

# Description
# "As an AOM user I need to log into the system securely, through a dedicated secure login screen with 2 stage authentication and have access to the system according to my permissioning"
# User name and secure password functionality entry screen with user based permissioning

----
@AR-2
Feature: Login
	Users' login credentials are stored in the database in an encrypted form 
	A user is identified by username
	Valid credentials are a username that exists in the database and its corresponding password
	Invalid credentials are:
		a username that is not in the database
		or a username that exists in the database and an incorrect password
	A user can be suspended 
	A non-suspended user can login with valid credentials
	A suspended user cannot login with valid credentials
	
	@Succesfullogin
	Scenario Outline: Login as a non-suspended user
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have entered username <Username>
		And I have entered password <Password>
		When I click the sign in button
		Then the dashboard is displayed
		
		Examples:
		| Browser | Username      | Password | 
		| IE 11   | Non-suspended | Correct  | 
		
	@FailedLogin
	Scenario Outline: Login that will fail
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have entered username <Username>
		And I have entered password <Password>
		When I click the sign in button
		Then the login will fail
		And the error message <Expected Message> will be displayed
		
		Examples:
		| Browser | Username      | Password      | Reason             | Expected Message |
		| IE 11   | Blank         | Blank         | Mandatory field    |                  |
		| IE 11   | Any Non-Blank | Blank         | Mandatory field    |                  | 
		| IE 11   | Blank         | Any Non-Blank | Mandatory field    |                  |
		| IE 11   | Non-Existent  | Any Non-Blank | Non-existent user  |                  | 
		| IE 11   | Existing      | Incorrect     | Incorrect password |                  | 
		| IE 11   | Suspended     | Correct       | User not allowed   |                  | 
