# AR-102 Database - Create DBL - construct basic tables

# Acceptance Criteria
# 1) Entity Relationship model defined and documented.
# 2) Executable script to create the tables created and put under source control.
# 3) Executable script to create the minimum population of data required to allow the system to function created and put under source control.

# Description
# Create DBL - construct basic tables and enter the minimum set of static data that is needed for the system to function.
----
@AR-102
Feature: Design database model and produce scripts to create tables and populate with data   
	There is an entity relationship model as a MySQL Workbench file
	There is a database build script as a SQL script file
	There is a data population script as a SQL script file
	
	@DatabaseERModel @Manual
	Scenario: Database ER model created
		Given I have access to the AOM GitHub repository 
		When I open folder ArgusOpenMarkets\AOM\AOMDatabase\DataModel
		Then I see the mwb files 

	@DatabaseBuidlScript @Manual
	Scenario: Database build script created
		Given I have access to the AOM GitHub repository 
		When I open folder ArgusOpenMarkets\AOM\AOMDatabase\DatabaseDumps
		Then I see the sql file 

	@DatabaseDataPopulationScript @Manual
	Scenario: Database build script created
		Given I have access to the AOM GitHub repository 
		When I open folder ArgusOpenMarkets\AOM\AOMDatabase\DatabaseDumps
		Then I see the sql file 
