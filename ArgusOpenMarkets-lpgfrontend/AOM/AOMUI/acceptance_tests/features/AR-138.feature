# AR-138 Security - Client IP tracking and storing

# Acceptance Criteria
# Phase 1:
# - Google Analytics: Ensure 3 AOM user log in sessions (for AE, CT and CB) are persisted to Google Analytics and the returned evidence of each three of the user logins via attaching a screen shot to this Jira.
# - Database: Ensure user log in, all actions and user log out are persisted to database via SQL query. Attach screenshot to Jira.
#
# To be replicated to an additional story in a later sprint, once the frame work is established:
# - Log in as each of the three user types and ensure the following events are being tracked in Google Analytics
# - Create New Order in Bid Ask Stack
# - Edit existing order in bid ask stack
# - hold order
# - kill order
# - Report deal to Market Ticker (if logged in as AE, approve the deal)
# - Create a message in market ticker using the +Market Info function

----
@AR-138
Feature: Track and store IP addresses of all AOM
	Log all user logins and logouts to Google Analytics
	Log all user activity to Google Analytics

	@LogToGoogleAnalytics @Manual
	Scenario Outline: Page activity is logged to Google Analytics
		Given I am using browser <Browser>
		And I am using Developer Tools mode (press F12)
		And I have selected the Network tab
		And I have loaded page http://web.dev.aom.dev.argusmedia.com/ 
		When I click the "Post to Google Analytics" button
		Then I see records appearing in the on-screen log

		Examples:
			| Browser  |
			| IE       | 
			| Chrome   | 
			| Firefox  | 
			
