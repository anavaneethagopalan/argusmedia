# AR-119 Architecture - Automated testing and continuous deployment

# Acceptance Criteria
# 1) Load "Hello World" project and unit test with control pass and fail into the build environment
# 2) Initiate a build and test
# 3) Receive a successful build report
# 4) Receive a unit test report showing control test pass and fail
# 5) Load bad project into the environment
# 6) Initiate a build and test
# 7) Receive a failed build report with full details of issue
----
@AR-119
Feature: Run an automated build and test of a "Hello world!" project 
	In order to verify that the CI tool is correctly configured to run jobs to build and test
	As a developer
	I want to be able to run a job to build a "Hello world!" project using the CI tool

	@BuildSuccessfulTestPass @Manual
	Scenario: Can build and test a "Hello world!" project that results in a successful build and test pass
		Given I have configured the CI tool with the job to build the "Hello world!" project and run the test
		And the "Hello world!" project is configured to build successfully
		And the "Hello world!" project is configured to pass the test
		When I initiate the job 
		Then the "Hello world!" project is built
		And the "Hello world!" project test is run
		And the CI tool reports success in its log for the build
		And the CI tool reports success in its log for the test

	@BuildSuccessfulTestFails @Manual
	Scenario: Can build and test a "Hello world!" project that results in a successful build and test failure
		Given I have configured the CI tool with the job to build the "Hello world!" project and run the test
		And the "Hello world!" project is configured to build successfully
		And the "Hello world!" project is configured to fail the test
		When I initiate the job 
		Then the "Hello world!" project is built
		And the "Hello world!" project test is run
		And the CI tool reports success in its log for the build
		And the CI tool reports a failure in its log for the test

	@BuildFailures @Manual
	Scenario: Can build and test a "Hello world!" project that results in a failed build
		Given I have configured the CI tool with the job to build the "Hello world!" project and run the test
		And the "Hello world!" project is configured to fail the build
		When I initiate the job 
		Then the "Hello world!" project is built
		And the CI tool reports a failure in its log for the build
