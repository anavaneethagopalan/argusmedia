# AR-6 Login Screen - permissioning for bid/ask stacks

# Acceptance Criteria
# Log in as 3 user profiles (Broker, Trader, Editor)
# 1. Place bid order to the stack from all three.
# 2. Ensure Only CT and CB can place orders.


# Description
# "As an AOM user I want to see a configurable view of all the Bid/Ask stacks (per product) my user permissioning allows me to see."
# Allow the viewing of multi instances of bid ask stacks across Argus product range running concurrently - dependent on user permissions.
# Retrieve the user's permissions from the AOM server and make them available to the various UI components in order to allow permissions-driven behaviour.

----
@AR-6
Feature: Roles that can create orders
	Brokers and Traders can create orders
	An Editor cannot create orders
	
	@CanCreateOrder
	Scenario Outline: Can create orders
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have logged in with username <Username>
		When I click the <BidAsk> button of the the first bid ask stack
		Then new order window is displayed
		And I can create a new order
		
		Examples:
		| Browser | Username   | BidAsk | 
		| IE 11   | BrokerUser | Bid    | 
		| IE 11   | BrokerUser | Ask    | 
		| IE 11   | TraderUser | Bid    | 
		| IE 11   | TraderUser | Ask    | 
		
	@CannotCreateOrder
	Scenario Outline: Cannot create orders
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have logged in with username <Username>
		When I click the <BidAsk> button of the the first bid ask stack
		Then new order window is not displayed
		
		Examples:
		| Browser | Username   | BidAsk | 
		| IE 11   | EditorUser | Bid    | 
		| IE 11   | EditorUser | Ask    | 
