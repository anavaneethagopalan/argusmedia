# AR-66 Market Ticker - General Appearance and Behaviour - content

# Acceptance Criteria
# 1. Market Ticker (MT) window painted as UX market ticker (graduated grey and black panels) Awaiting UX (Ket) to provide colour palate with CYMK codes)
# 2. MT widget is static (not closable) within the AOM layout for phase 1 (orientated horizontal between bid ask stacks (above) and News and Analysis (below)).
# 3. Header of the MT contains 2 drop down (product filter and status filters) that allow multiple selection of for all filterable items.
#    1. Product Filter for Phase 1 = 1. ARGUS NAPHTHA CIF NWE - OUTRIGHT and 2: ARGUS NAPHTHA CIF NWE - DIFF
#    2. Status Filter for Phase 1 = BID, ASK, INFO, DEAL, WITHDRAWN, VOID and PENDING
# 4. Header of the MT contains 2 modal buttons, +DEAL and + MARKET INFO (relevant modals open when permissioned user clicks on respective button.
#    1. only permissioned AOM users can see the +DEAL and + MARKET INFO buttons (users = Client Broker, Client Principal and Argus Editor) - see AOM Permission Matrix.xlsx file attached to this Jira
# 5. MT window has scroll bar orientated on right side of widget.
# 6. Each market action in MT is timestamped (note same convention as in News & Analysis Window);
#    1. If market event is less than 24 hours from current system clock time, use 24hr stamp - (HH:MM) (eg. 14:06)
#    2. If market event is greater than 24 hours old from current system clock time, used 24hr stamp + day (HH:MM DD-MMM-YYYY) (eg. 14:06 21-MAY-2014)

# Description
# "As an AOM user I want to see the results of all "market activity" for markets I am permissioned to see, displayed in the Market Ticker, so I can keep informed of trading events in the market.
# Note to Developer:
# - Market Activity is any action/event that occurs to an order or a deal. (includes state changes of an order - e.g. new, edit, hold, reinstate). All actions are reported to the market ticker (and the Database). It also includes + MARKET INFO
# - Create the widget, container and the display headers and grid.

----
@AR-66
Feature: Market Ticker Filters
	There is a filter dropdown list for PRODUCTS
	There is a filter dropdown list for STATUSES
	Each filter dropdown list has selectable entries   
	Each entry has a checkbox 
	The filters allow multiple selection of entries using the checkboxes
	The title of the filter always shows the filter name
	The title of the filter also indicates how many entries have been selected by showing:
	- "(All)" if all the entries have been selected, or
	- (number) if only some entries have been selected, or
	- no indication if no entries are selected.
	The market ticker filters are set to All by default
	The market ticker shows all rows by default
	The market ticker rows are automatically updated to reflect a change to a filter entry checkbox  
	
	The PRODUCTS filter has an entry for each subscribed product
	The STATUSES filter has entries of BID, ASK, INFO, and DEAL
	
	Each market ticker event row shows the STATUS and the time of the event and, if before today, the date.
	The market ticker has a scroll bar if there are more rows than will fit in the window.
	Only the Editor user can see the "SHOW/HIDE PENDING", "+DEAL" and "+MARKET INFO" buttons.


#Scenarios

	
	@SomeScenario
	Scenario Outline: Some Scenario
		Given 
		When 
		Then 
	
		Examples:
		| Browser |
