# AR-20 Bid/Ask Stack - Hover box for index specifications

# Acceptance Criteria
# 1) Populated hover box appears after the specified delay when hovering over the header. Values match those in the DB.
----
@AR-20
Feature: Popup hover box over index name in bid/ask stack header displaying the full name of the product, units of trade, terms of trade, delivery locations etc
	Popup is displayed for the bid/ask grid header
	Popup disappears when mouse is moved away from from the bid/ask grid header
	Popup is displayed for a bid/ask grid row
	The information in the popup box is derived from the database

	@ShowBidAskHeaderPopup @Manual
	Scenario Outline: Show bid/ask header popup 
		Given I am using browser <Browser> 
		And I have loaded an AOM product configuration
		And I am displaying bid/ask grid for a product 
		When I hover the mouse over the grid product header row
		Then a popup box is shown
		And the popup box shows the product information

		Examples:
		| Browser |  
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 

	@HideBidAskHeaderPopup @Manual
	Scenario Outline: Hide bid/ask header popup 
		Given I am using browser <Browser> 
		And I have loaded an AOM product configuration
		And I am displaying bid/ask grid for a product 
		And I have hovered the mouse over the grid product header row
		And a popup box is shown
		When I move the mouse pointer away from the  grid product header row
		Then the popup box disappears

		Examples:
		| Browser |  
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 
		
	@ShowBidRowPopup @Manual
	Scenario Outline: Show bid row popup 
		Given I am using browser <Browser> 
		And I have loaded an AOM product configuration
		And I am displaying bid/ask grid for a product 
		When I hover the mouse over a bid row
		Then a popup box is shown
		And the popup box shows the bid information

		Examples:
		| Browser |  
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 
