# AR-21 Bid/Ask Stack - Glow/highlite row selector

# Acceptance Criteria
# - Glow precedes the hover box when traversing bid ask stack
# - Glow appears as per http://www.ketpatel.com/demos/openmarkets/argus_open_markets.html
----
@AR-21
Feature: Highlight the bid/ask grid row
	The row in the bid/ask grid is highlighted as the mouse pointer is moved over it
	The highlighted row remains highlighted when the popup box opens when the mouse pointer is hovered over the row
	The highlighted row loses it's highlighting when the mouse pointer is moved off it

	@HighlightBidAskRow @Manual
	Scenario Outline: Highlight bid/ask row 
		Given I am using browser <Browser> 
		And I have loaded an AOM product configuration
		And I am displaying bid/ask grid for a product 
		When I move the mouse pointer over a row
		Then the row is highlighted 

		Examples:
		| Browser |  
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 

	@UnhighlightBidAskRow @Manual
	Scenario Outline: Hide bid/ask header popup 
		Given I am using browser <Browser> 
		And I have loaded an AOM product configuration
		And I am displaying bid/ask grid for a product 
		And I have moved the mouse over a grid product row
		And the row is highlighted 
		When I move the mouse pointer off the highlighted row 
		Then the row loses its highlighting 

		Examples:
		| Browser |  
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 
		
