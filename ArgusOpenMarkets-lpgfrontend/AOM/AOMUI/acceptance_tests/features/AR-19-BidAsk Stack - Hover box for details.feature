# AR-19 Bid/Ask Stack - Hover box for details

# Acceptance Criteria
# 1) Populated hover box appears after the specified delay.
# 2) Items included in hover box match the stored configuration for the market. Demonstrate different configurations.
----
@AR-19
Feature: Popup hover box over a bid/ask grid order row displaying bid offer attributes
	Popup is displayed for a bid/ask grid row
	Popup disappears when mouse is moved away from from the bid/ask row

	@ShowBidRowPopup @Manual
	Scenario Outline: Show bid row popup 
		Given I am using browser <Browser> 
		And I have loaded an AOM product configuration
		And I am displaying bid/ask grid for a product 
		When I hover the mouse over a bid row
		Then a popup box is shown
		And the popup box shows the bid information

		Examples:
		| Browser |  
		| IE 11   | 
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 

	@HideBidAskHeaderPopup @Manual
	Scenario Outline: Hide bid/ask row popup 
		Given I am using browser <Browser> 
		And I have loaded an AOM product configuration
		And I am displaying bid/ask grid for a product 
		And I have hovered the mouse over a row
		And a popup box is shown
		When I move the mouse pointer from over the row 
		Then the popup box disappears

		Examples:
		| Browser |  
		| IE 11   | 
		| IE 10   | 
		| IE 9    | 
		| IE 8    | 
		| Chrome  | 
		| Firefox | 
