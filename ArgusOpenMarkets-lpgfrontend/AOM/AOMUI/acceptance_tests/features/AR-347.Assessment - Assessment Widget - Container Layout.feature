# AR-347 Assessment - Assessment Widget - Container Layout

# Acceptance Criteria
# - Assessment widget is a fixed size window, positioned between the bid ask stack (above) and the market ticker (below) - non movable for phase 1
# - Phase 1: Assessment is contained within the header. Only 1 Assessment (Naphtha Outright) is required for phase 1.
# - Assessment widget is titled in the same manner as the other AOM headers - three dots and the word "Assessment" - (font = Ariel) confirm size from Ket - http://ketpatel.com/demos/aomv14/new.html
# - The Naphtha Outright assessment contains the following price assessment fields:
#  - (a) (Current) DD - MMM - (b) Low Price | (c) High Price - {Note today might not be viewable if assessment is priced at 17:00 - input from business needed.}
#  - (a) (Previous) DD-MMM - (b) Low Price | (c) High Price

# Nice to Have
# - When only one assessment is contained in the widget, the widget resizes so that the assessment data moves to reside within the header, creating more space within AOM.

# FOR PHASE 1.5 AND BEYOND:
# - Products are added to the container via a drop down menu in the header
# - Assessment container is dynamic and resizes to contain all the assessments for which the AOM user is permissioned to see (Max assessments for phase 1 = 2 (Naphtha CIF NWE Outright and Dif)
# - drop down is titled " All Products" -
# - Unknown macro: {include Ket's UX size specs - to attach once completed}
# - Widget is responsive. If a product is deselected/selected
# - Assessments for Products can be added or removed via the drop down check box (clicking on drop down produces a selection list with check boxes).
# - Assessments for Products can also be removed by clicking on the "x" icon in the header of each individual assessment pane.
# - When an assessment product is included/excluded, the remaining fields automatically reside to populate the space.

# Description
# "As a permissioned AOM user, I want to see the previous and current product assessments prices for the products I am permissioned for. The product list should be selectable. The assessment information should all be displayed a within a self contained user interface widget."

----
@AR-347
Feature: Assessment Widget
	Has a Title "ASSESSMENTS"
	Shows at least one assessment item.
	The item has a product title like "NAPHTHA"
	Shows the date corresponding to to today
	Shows the date corresponding to to the previous day
	Shows the price range for today
	Shows the price range for the previous day
	The price range shows the lowest and highest prices
	The price range is coloured red if lower than the previous day's or green if higher than the previous day's
	
	@AssessmentForProduct
	Scenario Outline: Assessment for product
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		When I login to AOM
		Then	the AOM dashboard is displayed
		And the Assessments panel is displayed
		And the assessments panel is titled "ASSESSMENTS"
		And the assessments panel shows one item for product "NAPHTHA"
		And the assessment product shows today's date and the previous day's date
		And the assessment product shows the price range for today and the previous day

		Examples:
		| Browser |
		| IE11    |