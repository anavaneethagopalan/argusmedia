# AR-76 Market Ticker - Deal text formatting

# Acceptance Criteria
# Format of messages generated in the deal ticker must match the following structure for each given action - See attachment titled "Market Ticker Syntax.xlsx"

# Description
# "As an AOM user I need to see the deal text in the following format (see messaging to market ticker tab)."
# Text format Principle aggressor/broker trade xxxxxx principle initiator/broker

----
@AR-76
Feature: Market Ticker Formatting


#Scenarios

	
	@SomeScenario
	Scenario Outline: Some Scenario
		Given 
		When 
		Then 
	
		Examples:
		| Browser |
