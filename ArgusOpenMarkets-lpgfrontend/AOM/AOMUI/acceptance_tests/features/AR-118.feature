# AR-118 Architecture - UI framework for dynamic multiple panes and navigation

# Acceptance Criteria
# A skeleton AOM page demonstrating the following behaviours:
# 1) Dynamic configuration - 2 different configurations loadable to give different layouts.
# 2) Multiple panels, user resizable.
# 3) Demonstrate functionality in all supported browsers.
----
@AR-118
Feature: Provide a presentation framework for the client application that supports dynamic configuration for layouts and multiple user-resizable panels on all supported browsers  
	In order to develop the application with the required UI functionality that will run on on all supported browsers  
	As a developer
	I want a presentation framework in place that supports dynamic configuration for layouts and multiple user-resizable panels on all supported browsers

	@LoadLayoutConfiguration @Manual
	Scenario Outline: Load a configuration that changes the on-screen presentation layout
		Given I am using browser <Browser> 
		And I have loaded configuration <InitialConfiguration> 
		And I am displaying a page 
		When I load configuration <LoadConfiguration>
		And I refresh the display of the page
		Then the page layout changes as specified by the <FinalConfiguration> 

		Examples:
		| Browser | InitialConfiguration | LoadConfiguration | FinalConfiguration | 
		| A       | 1                    | 2                 | 2                  |
		| A       | 2                    | 1                 | 1                  |
		| B       | 1                    | 2                 | 2                  |
		| B       | 2                    | 1                 | 1                  |

		
	@MultipleResizablePanels @Manual
	Scenario Outline: Display Multiple resizable panels
		Given I am using browser <Browser> 
		And I am displaying a page containing multiple panels
		And Panel <Panel> has size <InitialSize>
		When I change the size of panel <Panel> by <ReSize>
		Then the size of panel <Panel> changes to <FinalSize>

		Examples:
		| Browser | Panel | InitialSize | ReSize | FinalSize | 
		| IE8     | 1     | 4x4         | 2x2    | 2x2       |
		| IE9     | 1     | 4x4         | 2x2    | 2x2       |
		| IE10    | 1     | 4x4         | 2x2    | 2x2       |
		| Chrome  | 1     | 4x4         | 2x2    | 2x2       |
		| Firefox | 1     | 4x4         | 2x2    | 2x2       |
		