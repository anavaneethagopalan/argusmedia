# AR-150 Permissions - Allows AOM user to log in and view app according to their predefined permissions.

# Acceptance Criteria
# CI testing - ongoing
# 1. Refer to attached user permissioning matrix

# Description
# Allows AOM user to log in and see his predefined permissions.

----
@AR-150
Feature: Login
	The login window is displayed
	Enter valid user credentials and proceed to AOM dashboard
	Enter invalid user credentials and receive an error message
	Application functionality is available as per the user role in the permissions matrix 
	
	@LoginValiduser
	Scenario Outline: Login as an authorised user
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have entered username <Username>
		And I have entered password <Password>
		When I click the sign in button
		Then the dashboard is displayed
		
		Examples:
		| Browser | Username  | Password  | 
		| IE 11   |           |           | 
		| IE 10   |           |           | 
		| IE 9    |           |           | 
		| IE 8    |           |           | 
		| Chrome  |           |           | 
		| Firefox |           |           | 
		
	@UnauthorisedUserLogin
	Scenario Outline: Attempt to login with unauthorised user credentials
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have entered username <Username>
		And I have entered password <Password>
		When I click the sign in button
		Then the error message <Message> will be displayed
		
		Examples:
		| Browser | Username  | Password  | Message |
		| IE 11   |           |           |         |
		| IE 10   |           |           |         |
		| IE 9    |           |           |         |
		| IE 8    |           |           |         |
		| Chrome  |           |           |         |
		| Firefox |           |           |         |
	