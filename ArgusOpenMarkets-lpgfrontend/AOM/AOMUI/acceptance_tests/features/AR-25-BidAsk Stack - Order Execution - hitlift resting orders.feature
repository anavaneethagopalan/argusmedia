# AR-25 Bid/Ask Stack - Order Execution - hit/lift resting orders

# Acceptance Criteria
# User can not execute own order - ensure that modal opens in execute mode only for orders (white text) that the user is not related to and is permissioned to trade.
# Log in as user and click a sell order from stack placed by another counterparty
# Ensure modal opens up as follows:
# - Order type selected to ASK (red) and bot editable by aggressor
# - Contract is representative of the stack from which it was opened
# - Location field is defaulted to Rotterdam#
# - Price is in correct units for contract and to 2 dp
#   - Outright = 5 significant figure (inclusive of 2 dp) and must be positive integer (ie 904.25). (Price units = $/mt)
#   - Diff = 4 significant figures (inclusive of 2 dp) and can be a positive or negative integer (ie -12.75). (Price units = $/Bbl)
# Quantity field is selected and not editable.
# Delivery period is populated and not editable
# Broker field may be selected - not editable
# Free form text notes may be populated, however this fields is not editable
# Active buttons in Execute Mode:
# - Exit - located to right side of modal and is linked to the enter key buy default.
# - Execute button - (left of exit button).
 
# Note: Once order executed the modal closes, the order drops off the stack and is reported as a deal in the Market Ticker. Confirms to user follow.

# Market convention - Bids are hit, asks are lifted

# Description
# As a trade permissioned AOM user, I need to be able to transact (hit and lift) resting orders in the bid ask stack. When aggressing an existing order, the modal opens with the economic fields locked down and non-editable.

# Note to Developer:
# - If broker is aggressing the order, they must select the principal aggressor they are acting on behalf of, within the order modal.
# - The resulting order produces a pendIng message to the ticker - to be verified by Argus Editor.
# - Once verified, DEAL message is publishes in ticker to all AOM users.
# - Permissioning - a user can't aggress his own trade

# Functionality to build:

# UI
# - Validate data and/or permissions
# - Send Command

# Server side
# - Order Handler
#   - Validate data and permissions
#   - Persist action to DB
#   - Publish action to queue

----
@AR-358
Feature: Hit/lifted dealPersist deal to the database

@AR-359
Feature: Hit/lifted deal removed from the bid/ask stack

#-----------------------------------------------------------------------------------------------------------------------
 order window
	The new order is available only to permitted users
	It is opened when the +bid or +ask buttons are clicked 
	It is modal
	Its heading text matches the bid/ask grid heading text
	The order type is indicated by blue (for bid) or red (for ask) highlighting
	The overall style matches the signed-off specification at http://ketpatel.com/demos/aom/argus_open_markets.html
	
	@OpenNewOrderWindow
	Scenario Outline: Open a new order window for bid 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboardbid/ask
		And I have the permission required to place new orders 
		When I click the +<OrderType> button on the grid headed <GridHeader>
		Then new order window for <OrderType> opens
		And the window is modal
		And the order type <OrderType> is highlighted
		And the window has a heading that matches <GridHeader>
		And the window style matches the style shown at <Specification>
		
		Examples:
		| Browser | OrderType  | GridHeader          | Specification                                         | 
		| IE 10   | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 9    | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 8    | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Chrome  | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Firefox | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 10   | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 9    | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 8    | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Chrome  | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Firefox | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		
	@NoPermissionForNewOrder
	Scenario Outline: Open a new order window for bid 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboardbid/ask
		And I do not have the permission required to place new orders 
		When I click the +<OrderType> button on the grid headed <GridHeader>
		Then a new order window does not open
		
		Examples:
		| Browser | OrderType  | GridHeader          | 
		| IE 10   | Bid        | ArgusNaphtha-CIF... | 
		| IE 9    | Bid        | ArgusNaphtha-CIF... | 
		| IE 8    | Bid        | ArgusNaphtha-CIF... | 
		| Chrome  | Bid        | ArgusNaphtha-CIF... | 
		| Firefox | Bid        | ArgusNaphtha-CIF... | 
		| IE 10   | Ask        | ArgusNaphtha-CIF... | 
		| IE 9    | Ask        | ArgusNaphtha-CIF... | 
		| IE 8    | Ask        | ArgusNaphtha-CIF... | 
		| Chrome  | Ask        | ArgusNaphtha-CIF... | 
		| Firefox | Ask        | ArgusNaphtha-CIF... | 
		
		
	@OpenNewOrderBidWindow
	@NewOrderBidWindowIsModal
	@NewOrderBidWindowMatchingHeading
	@NewOrderBidWindowStyleAsSpecified
	