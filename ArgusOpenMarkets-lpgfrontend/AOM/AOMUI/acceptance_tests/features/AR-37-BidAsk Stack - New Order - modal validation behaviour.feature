# AR-37 Bid/Ask Stack - New Order - modal validation behaviour

# Acceptance Criteria
# General validation behaviour:
# 1. Any attempt to commit a new order with invalid data must fail.
# 2. All fields in error must be highlighted.
# 3. A descriptive message must be displayed for the first field in error in the modal.
#
# The specific validations required are:
# 1. Order Type: one must be selected
# 2. Price: must be entered. Detailed validations for numerics are covered in AR-38.
# 3. Quantity: must be entered (manually or via quick selection button). Detailed validations for numerics are covered in AR-38.
# 4. Principal: (Present when in a broker role only). Must be selected.

# Description
# "If I haven't completed all the required fields in the modal for the order to be processed, the modal won't submit (see PoC). 
# If several fields need attention, the first from the top of the modal will appear with the red halo and error message and proceed downwards until all fields have been completed satisfactorily."
# Input fields in window behaviours - Error message appears and edge of windows glow red if info is insufficient/incorrect. Modal won't submit to stack. If OK window glows blue
----
@AR-37
Feature: New order window mandatory fields 
	The CREATE functionality is available if:
	BID or ASK is specified
	A valid price is entered
	A valid quantity is entered
	Principal is entered (for broker roles).
	
	@CanCreateNewOrder
	Scenario Outline: The CREATE button is clickable 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And I have the permission required to place new orders 
		And I have clicked the +<OrderType> button on the grid headed <GridHeader>
		And the new order window is displayed
		When I click order type button <Type> 
		And I enter price <Price>
		And I enter quantity <Quantity>
		And I enter broker <Broker>
		Then the CREATE button becomes activated
		And it turns green when I hover the mouse pointer over it
		
		Examples:
		| Browser | OrderType  | GridHeader          | Type | Price | Quantity | Broker |
		| IE 11   | Bid        | ArgusNaphtha-CIF... | BID  | 100   | 12500    |        |
		| IE 10   | Bid        | ArgusNaphtha-CIF... | BID  | 100   | 12500    |        |
		| IE 9    | Bid        | ArgusNaphtha-CIF... | BID  | 100   | 12500    |        |
		| IE 8    | Bid        | ArgusNaphtha-CIF... | BID  | 100   | 12500    |        |
		| Chrome  | Bid        | ArgusNaphtha-CIF... | BID  | 100   | 12500    |        |
		| Firefox | Bid        | ArgusNaphtha-CIF... | BID  | 100   | 12500    |        |
		| IE 11   | Ask        | ArgusNaphtha-CIF... | ASK  | 100   | 12500    |        |
		| IE 10   | Ask        | ArgusNaphtha-CIF... | ASK  | 100   | 12500    |        |
		| IE 9    | Ask        | ArgusNaphtha-CIF... | ASK  | 100   | 12500    |        |
		| IE 8    | Ask        | ArgusNaphtha-CIF... | ASK  | 100   | 12500    |        |
		| Chrome  | Ask        | ArgusNaphtha-CIF... | ASK  | 100   | 12500    |        |
		| Firefox | Ask        | ArgusNaphtha-CIF... | ASK  | 100   | 12500    |        |
		
	@CreateNotActivated
	Scenario Outline: The CREATE button is not activated
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And I have the permission required to place new orders 
		And I have clicked the +<OrderType> button on the grid headed <GridHeader>
		And the new order window is displayed
		When I click order type button <Type> 
		And I enter price <Price>
		And I enter quantity <Quantity>
		And I enter broker <Broker>
		Then the CREATE button does not become activated
		And it does not turn green when I hover the mouse pointer over it
		
		Examples:
		| Browser | OrderType  | GridHeader          | Type | Price | Quantity | Broker |
		| IE 11   | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| IE 10   | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| IE 9    | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| IE 8    | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| Chrome  | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| Firefox | Bid        | ArgusNaphtha-CIF... | BID  |       |          |        |
		| IE 11   | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| IE 10   | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| IE 9    | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| IE 8    | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| Chrome  | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		| Firefox | Ask        | ArgusNaphtha-CIF... | ASK  |       |          |        |
		
	