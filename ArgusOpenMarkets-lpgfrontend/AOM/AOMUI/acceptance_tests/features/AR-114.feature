# AR-114 Architecture - Create server service framework

# Acceptance Criteria
# Implement two generic / test business services:
# 1) Time service (AR-106)
# 2) Service that returns high-level details of business services implemented. Name of service and description of its purpose will suffice.

# Framework must provide:
# 1) Ability to register business service.
# 2) Ability to call service via gateway
----
@AR-114
Feature: Register and list services 
	The framework should allow a service to be registered
	The framework should show all the registered services

	@RegisterNewService @Manual
	Scenario Outline: Register a new Service
		Given I logged into the RabbitMQ Management console at http://localhost:15672/#/ as user guest/guest
		And I have clicked the Queues tab to navigate to the "Queues" page at http://localhost:15672/#/queues
		And there are <InitialQs> queues in the list
		When I run the services dotnet project
		Then there are <InitialQs>+<AddedQs> in the list
		
		Examples:
			| InitialQs | AddedQs |
			| 0         | 2       |

	@Re-registerService @Manual
	Scenario: Re-register a Service
		Given I logged into the RabbitMQ Management console at http://localhost:15672/#/ as user guest/guest
		And I have clicked the Queues tab to navigate to the "Queues" page at http://localhost:15672/#/queues
		And there are <InitialQs> queues in the list
		When I run the services dotnet project
		Then there are <FinalQs> in the list

		Examples:
			| InitialQs | FinalQs |
			| 2         | 2       |
		
	@ListServices @Manual
	Scenario: List Services
		Given I logged into the RabbitMQ Management console at http://localhost:15672/#/ as user guest/guest
		When I click the Queues tab to navigate to the "Queues" page at http://localhost:15672/#/queues
		Then I will see the queues (services) that have been created (registered)
