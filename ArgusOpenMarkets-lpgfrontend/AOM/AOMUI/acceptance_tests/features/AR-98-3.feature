﻿Feature: A table created on one database is automatically replicated on the other database
	In order to ensure that database structure and data is replicated for data integrity and system availability 
	As someone who can't bear to lose data or availability
	I want automatic replication of database tables and data between two databases

@VerifyAutomaticReplicationOfTable
Scenario: verify that a table created on one database is automatically created on the other database
	Given I have connected and logged in to the first database
	And I have connected and logged in to the second database
	And I have the permissions needed to create tables on the first database
	When I enter the command SQLCommand to create a table on the first database
	And I wait for Seconds seconds
	Then the table Table will be created on the first database
	And the table Table will be automatically created on the second database 

@VerifyAutomaticReplicationOfData
Scenario: verify that data inserted into a table created on one database is automatically inserted into the corresponding table on the other database
	Given I have connected and logged in to the first database
	And I have connected and logged in to the second database
	And the table Table exists on the first database
	And the table Table exists on the second database
	When I enter the command SQLCommand to insert some data into table on the first database
	And I wait for Seconds seconds
	Then the data Data inserted will be present in table Table on the first database
	And  the data Data will automatically be inserted into table Table on the second database
