# AR-18 Bid/Ask Stack - moveable windows

# Acceptance Criteria
# 1. Windows repeatably moveable by user without loss of functionality, both horizontally and vertically.
# 2. windows remain within the horizontal boundary of the screen at all times

# Description
# "As an AOM user I need to be able to move and configure my screen layout and view by being able to select and deselect modules and windows"
# Implement movable window functionality.
----
@AR-18
Feature: Bid/Ask stack panels are moveable 
	The user can move panels by drag and drop
	One or more resting panels are automatically repositioned to accommodate a panel being moved
	Panels are automatically repositioned when the browser window's width is changed
	Panels positions remain the same when the browser window's height is changed 
 
	@MovePanel @Manual
	Scenario Outline: Move a panel to new position
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard 
		When I click on a bid ask panel
		And I drag the panel to new position
		And I drop the panel at the new position
		Then the panel will snap into a position approximately where it was dropped

		Examples:
		| Browser |  
		| IE 11   |  
		| IE 10   |  
		| IE 9    |  
		| IE 8    |  
		| Chrome  |  
		| Firefox |  

	@ChangeBrowserWidth @Manual
	Scenario Outline: Change the width of the browser
		Given I am using browser <Browser> 
		And the browser window is maximised
		And I am displaying the AOM dashboard 
		When I reduce the width of the browser window
		Then the panels reposition themselves
		And 	the panels remain in view
 
 		Examples:
		| Browser |  
		| IE 11   |  
		| IE 10   |  
		| IE 9    |  
		| IE 8    |  
		| Chrome  |  
		| Firefox |  