# AR-58 Bid/Ask Stack - Order Execution - General appearance

# Acceptance Criteria
# 1. Modal only accessible given appropriate permissions.
# 2. Heading matches market.
# 3. All fields populated from existing order.

# Description
# "As a CT/CB when I click on a counterparties order, the window opens in Execute mode:
# - Allowing me to hit or lift (depending on the order type) from the bid ask stack. I need the trading modal to appear in the correct permissioned default status;
# - Modal must open in execute mode, with an execute button to transact on the order.
# - An exit button, if selected, aborts the transaction, close window and return to the main screen
# - All economic fields locked down and not editable "
# Screen painted as per Market ticker in PoC, and filled with the details of the selected order.
# NB. Refer to signed-off UX documentation to see final agreed layout, contents and styles. These will have changed since the PoC images were attached.

----
@AR-58
Feature: Execute order window
	The order execution function is available only to permitted users
	The window is opened when a bid or ask entry is clicked 
	It is modal
	Its heading text matches the bid/ask grid heading text
	Its data fields are display-only
	The order execution can be abandoned
	The order can be executed
	The overall style matches the signed-off specification at http://ketpatel.com/demos/aom/argus_open_markets.html
	
	@HitABid
	Scenario Outline: Hit a bid 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And there is at least one bid row 
		And I have the permission required to hit a bid order 
		When I click on the bid row 
		Then execute bid window opens
		And the window is modal
		And the window has a heading that matches <GridHeader>
		And the window data is display-only
		And the window data corresponds to the data in the bid row
		And the window style matches the style shown at <Specification>
		
		Examples:
		| Browser | OrderType  | GridHeader          | Specification                                         | 
		| IE 11   | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 10   | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 9    | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 8    | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Chrome  | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Firefox | Bid        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		
	@HitABid
	Scenario Outline: Lift an Ask 
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And there is at least one ask row 
		And I have the permission required to lift an ask order 
		When I click on the ask row 
		Then execute ask window opens
		And the window is modal
		And the window has a heading that matches <GridHeader>
		And the window data is display-only
		And the window data corresponds to the data in the ask row
		And the window style matches the style shown at <Specification>
		
		Examples:
		| Browser | OrderType  | GridHeader          | Specification                                         | 
		| IE 11   | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 10   | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 9    | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| IE 8    | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Chrome  | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |
		| Firefox | Ask        | ArgusNaphtha-CIF... | http://ketpatel.com/demos/aom/argus_open_markets.html |

	@NoPermissionForHit
	Scenario Outline: No permission to hit a bid
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And there is at least one bid row 
		And I do not have the permission required to hit bid or lift ask orders 
		When I click on the bid row 
		Then execute bid window does not open
		
		Examples:
		| Browser | OrderType  | 
		| IE 11   | Bid        | 
		| IE 10   | Bid        | 
		| IE 9    | Bid        | 
		| IE 8    | Bid        | 
		| Chrome  | Bid        | 
		| Firefox | Bid        | 
		
	@NoPermissionForLift
	Scenario Outline: No permission to lift an ask
		Given I am using browser <Browser> 
		And I am displaying the AOM dashboard
		And there is at least one ask row 
		And I do not have the permission required to hit bid or lift ask orders 
		When I click on the ask row 
		Then execute ask window does not open
		
		Examples:
		| Browser | OrderType  | 
		| IE 11   | Ask        | 
		| IE 10   | Ask        | 
		| IE 9    | Ask        | 
		| IE 8    | Ask        | 
		| Chrome  | Ask        | 
		| Firefox | Ask        | 
		
