# AR-109 Push Data - Server side configuration

# Acceptance Criteria (AR-106)
# Test "Hello world" server application and web client created.
# The server will create named data channels (at least 2) and publish data onto theses channels.
# The client will subscribe to multiple data channel and receive push data (time or other variable data) and display it, updating continuously on five clients simultaneously. 
# The client should demonstrate functionality on all supported browsers (Chrome, IE8+, Firefox and Safari).

# Description (AR-106)
# "As an AOM Server I need a mechanism to push information out to all connected clients in order to distribute prices, deals and news to the clients"
# Push new data to clients who are subscribed to a channel or push to a group or single client.
----
@AR-109
Feature: Manage Topics and Messages
	Add a topic
	Remove a topic
	Put a message on to a topic
	Get a message from a topic 

	@AddNewTopic @Manual
	Scenario Outline: Add a new topic
		Given I have started Diffusion
		And I am looking at the Diffusion Topics on the Diffusion console at http://localhost:8080/console/
		And the topic <Topic> is not displayed 
		And I have opened the ControlClient project in Visual Studio
		When I run the AomControlClientTests module
		Then The <Topic> topic appears on the Diffusion console
		And The sub topics <SubTopics> are accessible in <Topic>
		
		Examples:
			| Topic | SubTopics                         |
			| AOM   | Naphta/NWE/FronMonth/1M/Bids      |

	@AddExistingTopic @Manual
	Scenario: Add a topic that already exists
		Given I have started Diffusion
		And I am looking at the Diffusion Topics on the Diffusion console at http://localhost:8080/console/
		And the topic <Topic> is already displayed 
		And I have opened the ControlClient project in Visual Studio
		And I comment out the line with "...RemoveTopic(...)"
		When I run the AomControlClientTests module
		Then The <Topic> topic still appears on the Diffusion console

		Examples:
			| Topic |
			| AOM   |
		
	@PutAndGetMessageOnTopic @Manual
	Scenario Outline: Put a message on to a topic and read it back
		Given I have started Diffusion
		And I am looking at the Diffusion Topics on the Diffusion console at http://localhost:8080/console/
		And the topic <Topic> is displayed 
		And I have opened the ControlClient project in Visual Studio
		And I put a break point on the line with "Assert.IsNotNull(msg1)"
		When I run the AomControlClientTests module
		And hover the mouse pointer over "msg1" on the breakpoint line 
		Then variable msg1 contains the text <Message>
		
		Examples:
			| Topic | Message                                     |
			| AOM   | hello AOM/Naptha/NWE/FronMonth/1M/Bids      |

	@RemoveExistingTopic @Manual
	Scenario Outline: Remove an existing topic
		Given I have started Diffusion
		And I am looking at the Diffusion Topics on the Diffusion console at http://localhost:8080/console/
		And the topic <Topic> is displayed 
		And I have opened the ControlClient project in Visual Studio
		And I put a break point on the line with "...AddTopic(...)"
		When I run the AomControlClientTests module
		Then The <Topic> topic disappears from the on the Diffusion console

		Examples:
			| Topic |
			| AOM   |
			
	@RemoveTopicThatDoesNotExist @Manual
	Scenario Outline: Remove a topic that does not exist
		Given I have started Diffusion
		And I am looking at the Diffusion Topics on the Diffusion console at http://localhost:8080/console/
		And the topic <Topic> is not displayed 
		And I have opened the ControlClient project in Visual Studio
		And I put a break point on the line with "...AddTopic(...)"
		When I run the AomControlClientTests module
		Then The <Topic> topic is still not displayed on the Diffusion console

		Examples:
			| Topic |
			| AOM   |			