# AR-104 Database - Design Initial data model

# Acceptance Criteria
# 1) 
----
@AR-104
Feature: Design a database data model that supports the functionality of the AOM application
	In order to provide the end-user functionality of the AOM application
	As a developer
	I want a data model that supports all of the functionality that needs to be developed for the AOM application

	@DesignERModel @Manual
	Scenario: Design the entity-relationship model for the AOM application
		Given I have defined the functional requirements of the AOM application 
		When I design the entity-relationship model
		Then the entity-relationship model supports all of the functionality of the AOM application

	@DocumentERModel @Manual
	Scenario: Document the entity-relationship model for the AOM application
		Given I have designed the entity-relationship model for the AOM application 
		When I document the entity-relationship model
		Then the documentation accurately describes the entity-relationship model
