\# AR-116 Component for the display of the bid/ask stack

# Acceptance Criteria
# 1) Ability to specify required columns including name, data type and data driven format (administrator function)
# 2) Ability for a tester to load test column configuration and test data into grid
# 3) Tester is able to quiz grid via programming language to determine its current display
# 4) Grid displays test columns and data as intended
# 5) API supports ability to select columns, rows and cells; user is able to execute

\# It has been assumed that the implementation of the grid will allow it to be configured using an user administration function like a configuration file or page
----
@AR-116-1
Feature: Ability to specify required columns including name, data type and data driven format (administrator function)
	In order to display a grid of information in a specified format on the screen 
	As an administrator
	I want to be able to specify the column names, data types and data driven format for a grid

	@DesignGridLayoutAndDataSource
	Scenario: Define the grid layout and content by specifying the grid configuration data like columns and headings
		Given I specify the grid columns and layout
		When I save the grid configuration 
		And I refresh the page that shows the grid
		Then the grid is displayed with the columns and layout specified by the configuration
----	
@AR-116-2
Feature: Ability for a tester to load test column configuration and test data into grid
	In order to display a grid with data in a specified column configuration on the screen 
	As a tester
	I want to be able to specify a column configuration and the data to be loaded

	@DisplaySpecifiedLayoutAndData
	Scenario: Display a grid showing a specified set of data using a defined grid configuration
		Given I specify the grid columns and layout
		And I have loaded a set of data into the database
		When I refresh the page that shows the grid
		Then the grid is displayed with the columns and layout specified by the configuration
		And it contains the data from the database
----
@AR-116-3
Feature: Tester is able to quiz grid via programming language to determine its current display
	In order to determine the current display of a grid via an API 
	As a Developer
	I want to be able to execute an API call that returns information about the grid's current configuration

	@GetGridConfiguration
	Scenario: Query the grid to get its current configuration
		Given the grid is displayed
		And I specify the grid API function and its arguments 
		When I execute the API function
		Then it will return information about the grid's current configuration
----	
@AR-116-4
Feature: Grid displays test columns and data as intended
	In order to verify that a grid displays columns with data correctly
	As a tester
	I want to be able display a grid containing specified columns and data
	
	@DisplayColumnsAndData
	Scenario: Grid displays columns and data
		Given the grid has been configured
		And the database contains data
		When I open the page to display the grid 
		Then the grid is displayed with the columns and data expected
----	
@AR-116-5
Feature: API supports ability to select columns, rows and cells; user is able to execute
	In order to perform an action on selected columns, rows or cells
	As a user
	I want to be able to highlight a set of columns, rows or cells

	@SelectColumns
	Scenario: Select grid columns
		Given the grid is displayed
		When I click on column(s)
		Then the column(s) will be selected (highlighted)

	@SelectRows
	Scenario: Select grid rows
		Given the grid is displayed
		When I click on row(s)
		Then the row(s) will be selected (highlighted)

	@SelectCells
	Scenario: Select grid cells
		Given the grid is displayed
		When I click on cells(s)
		Then the cell(s) will be selected (highlighted)
