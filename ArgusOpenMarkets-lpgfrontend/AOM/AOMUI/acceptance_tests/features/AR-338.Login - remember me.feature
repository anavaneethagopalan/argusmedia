# AR-338 Login - remember me

# Acceptance Criteria
# 1. There is a check box  on the login screen to allow the user to select this option.
# 2. When checked (enabled), the system will automatically fill in the username of the last logged in user when the login screen is opened.
# 3. When unchecked, the system will leave the username blank when the login screen is opened.
# 4. Unchecking the option will make the system forget the username.  Subsequently checking the option will not cause the username to be automatically filled in again until a successful login has occurred.

# Description
# As an AOM user I want the option to have the system remember my username so I do not have to re-enter it each time I login.
----
@AR-338
Feature: Remember me on login
	The checkbox is unchecked by default
	Once checked the checkbox will remain checked until explicitly unchecked
	Once unchecked the checkbox will remain unchecked until explicitly checked
	- The change of state of the checkbox will be persisted after a refresh only if there was a successful login before the refresh
	The last successful username will be remembered if the checkbox's persisted state is checked
	The username will be blanked if the checkbox's persisted state is unchecked
	
	@InitialStateIsUnchecked
	Scenario Outline: The initial state of the checkbox is unchecked
		Given I am using browser <Browser> 
		When I navigate to the AOM web site for the first time 
		Then I the AOM login screen is displayed 
		And the username and password are balnk
		And the checkbox is unchecked
		
		Examples:
		| Browser |
		| IE 11   |
		
	@CheckTheCheckboxAndLogin
	Scenario Outline: Check the checkbox and login and logout
		Given I am using browser <Browser> 
		And I have just logged out of AOM
		And I am displaying the AOM login screen
		And the checkbox is unchecked
		And the username is blank
		When I check the checkbox 
		And I enter username <Username> and password <Password>
		And I login
		And I log out
		And I reload the AOM website
		Then the AOM login screen is displayed
		And the checkbox is checked
		And the username is set to <Username>
		
		Examples:
		| Browser | Username      | Password  | 
		| IE 11   | tim           | password  | 

	@UncheckTheCheckboxAndLogin
	Scenario Outline: Uncheck the checkbox and login and logout
		Given I am using browser <Browser> 
		And I am displaying the AOM login screen
		And I have just logged out of AOM
		And the checkbox is checked
		And the username is <Username>
		When I uncheck the checkbox 
		And I enter username <Username> and password <Password>
		And I log in
		And I log out
		Then the AOM login screen is displayed
		And the checkbox is unchecked
		And the username is blank
		
		Examples:
		| Browser | Username      | Password  | 
		| IE 11   | tim           | password  |  

	@CheckTheCheckboxAndFailToLogin
	Scenario Outline: Check the checkbox and fail to login
		Given I am using browser <Browser> 
		And I have just logged out of AOM 
		And I am displaying the AOM login screen
		And the checkbox is unchecked
		And I have checked the checkbox 
		And I have entered a username <Username> and password <Password>
		And I have failed to login
		When reload the AOM web site 
		Then the checkbox is unchecked
		And the username is blank
		
		Examples:
		| Browser | Username      | Password  | 
		| IE 11   | tim           | blank     | 
		
	@UncheckTheCheckboxAndFailToLogin
	Scenario Outline: Uncheck the checkbox and fail to login
		Given I am using browser <Browser> 
		And I have just logged out of AOM 
		And I am displaying the AOM login screen
		And the checkbox is checked
		And the username is set to <Username>
		And I have unchecked the checkbox 
		And I have entered a blank username and password 
		And I have failed to login
		When reload the AOM web site 
		Then the checkbox is checked
		And the username is <Username>
		
		Examples:
		| Browser | Username      | Password  | 
		| IE 11   | tim           | blank     | 
		