// TemplateKeywords.js


var TemplateKeywords = {

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "DoSomething A": function(next, dataItem1, dataItem2, dataItem3, dataItem4 ) {

        // code for keyword functionality goes here
        // can use expect().toEqual() for assertions
        console.log(dataItem1 + "," + dataItem2 + "," + dataItem3);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "DoSomething B": function(next, dataItem1, dataItem2, dataItem3 ) {

        // code for keyword functionality goes here
        // can use expect().toEqual() for assertions
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "DoSomething C": function(next, dataItem1, dataItem2, dataItem3, dataItem4, dataItem5) {

        // code for keyword functionality goes here
        // can use expect().toEqual() for assertions
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "DoSomething D": function(next, dataItem1, dataItem2) {

        // code for keyword functionality goes here
        // can use expect().toEqual() for assertions
        next();
    },
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "DoSomething E": function(next, none) {

        // code for keyword functionality goes here
        // can use expect().toEqual() for assertions
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "ActionAndParams" : function(next, params) {

        // code for keyword functionality goes here
        // can use expect().toEqual() for assertions

        console.log(params.param3 + "," + params.param1 + "," + params.param2);
        next();
    },

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    "objects" : function(next, params) {

        // code for keyword functionality goes here
        // can use expect().toEqual() for assertions

        console.log("in objects:" + params.param3 + "," + params.param1 + "," + params.param2);
//        console.log(params.param3);
        next();
    }

};

module.exports = TemplateKeywords;