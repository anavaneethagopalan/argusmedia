fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
//var dashboard;

var automation = new Automation();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Login blocking', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;
    var specDescription = this.description;
    var saveBeforeSignin = false;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    //console.log('\r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        //console.log(sBrowserName)
    });


    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()) {
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("displays login window", function () {
        var specIt = "displays login window";

        var login = new LoginPage();
        login.waitForLoad(20).then(function (SignInButtons) {
            expect(SignInButtons.length).toEqual(1);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + '.png');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("fails for a blocked user: '" + username + 'blocked' + "'/'" + password + "'", function () {
        var specIt = "fails for a blocked user";
        login = new LoginPage();

        login.login(username + 'blocked', password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + 'blocked' + " pwd=" + password + ".png");
        expect(login.failText()).toContain('This account is not authorised from AOM.');
    });


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("resets the blocking count after a successful login for user: '" + username + 'forblocking' + "'/'" + password + "'", function () {
        var specIt = "resets the blocking count after a successful login for user";

        //Login successfully to reset the count
        login.login(username + 'forblocking', password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + 'forblocking' + " pwd=" + password + ".Initial" + ".png");
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.waitForLoad(30).then(function (BidButtons) {
            expect(BidButtons.length).not.toEqual(0);
        });
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
        dashboard.logout(hostURL);

        //Repeat failed attempt X times to get to the limit
        for (var j = 1; j < 5; j++) {
            login.login(username + 'forblocking', 'incorrect', saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + 'forblocking' + " pwd=incorrect." + j + ".png");
            //expect(login.failText()).toEqual('Incorrect user details entered.');
            expect(login.failText()).toContain('Invalid login details, please try again');
        }

        //Login successfully to show the count was reset
        login.login(username + 'forblocking', password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + 'forblocking' + " pwd=" + password + ".Final" + ".png");
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.waitForLoad(30).then(function (BidButtons) {
            expect(BidButtons.length).not.toEqual(0);
        });
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
        dashboard.logout(hostURL);

    }, 60000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("suspends after X failed login attempts for user: '" + username + 'forblocking' + "'/'" + 'incorrect' + "'", function () {
        var specIt = "suspends after X failed login attempts for user";

        //Login successfully to reset the count
        login.login(username + 'forblocking', password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + 'forblocking' + " pwd=" + password + ".Initial" + ".png");

        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.waitForLoad(30).then(function (BidButtons) {
            expect(BidButtons.length).not.toEqual(0);
        });
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
        dashboard.logout(hostURL);

        //Repeat X times to get to the limit
        for (var j = 1; j < 5; j++) {
            login.login(username + 'forblocking', 'incorrect', saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + 'forblocking' + " pwd=incorrect." + j + ".png");
            //expect(login.failText()).toEqual('Incorrect user details entered.');
            expect(login.failText()).toContain('Invalid login details, please try again');
        }

        //This time it will block
        login.login(username + 'forblocking', 'incorrect', saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + 'forblocking' + " pwd=incorrect." + j + ".png");
        //expect(login.failText()).toEqual('You have been blocked for too many unsuccessful attempts. Please contact ...');
        expect(login.failText()).toContain('This account is currently inaccessible.');
    }, 60000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('pauses', function () {
        var stream;
        var newOrder;
        console.log('pause'); //Newline
    });
}, 60000);
