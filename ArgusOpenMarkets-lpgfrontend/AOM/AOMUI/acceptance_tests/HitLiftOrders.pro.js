fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var NewOrderPage = require("./pages/neworderpage");
var ExecuteOrderPage = require("./pages/executeorderpage");

var automation = new Automation();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Hit and lift orders', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;

    var saveBeforeSignin = false;
    var specDescription = this.description;

    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
    });

    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

    login = new LoginPage();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("displays login window", function () {
        var specIt = "displays login window";

        login.waitForLoad(20).then(function (SignInButtons) {
            expect(SignInButtons.length).toEqual(1);
            automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + '.png');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs in for user: '" + username + "'/'" + password + "'", function () {
        var specIt = "logs in for user";

        var dashboard = new DashboardPage(); // get the dashboard page.

        username = 't3';
        for (var i = 0; i <= 5; i++) {
            login.isDisplayed().then(function (SignInButtons) {
                if (SignInButtons.length === 1){
                    login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + ".png");
                    dashboard.waitForLoad(20).then(function (BidButtons) {
                        if (BidButtons.length = 0) {
                            i = 6;
                        }
                    });
                }
            });
        }

        dashboard.getProductTitles().then(function(ProductTitles){
            expect(ProductTitles.length).not.toEqual(0);
            //expect(ProductTitles.length).toEqual('ProductTitles.length');
        });

        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "="  + username + " pwd=" + password + '.Dashboard.png');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("place orders",
        [[0, 50, 'BID', 500, 1],
        [0, 50, 'ASK', 500, -1]],
/*
        [[0, 2, 'ASK', 140, -1],
         [0, 2, 'BID', 1210, 1],
         [1, 2, 'ASK', 1, -1],
         [1, 2, 'BID', 1, 1],
         [0, 2, 'ASK', 138, -1],
         [0, 2, 'BID', 1212, 1]],
*/
        function(product, number, bidask, startprice, priceincrement) {
            xit("places " + number +  " " + bidask + " order(s) for product " + product, function () {
                var specIt = "places " + number +  " " + bidask + " order(s) for product " + product;
                var newOrder; // = new NewOrderPage();
                var dashboard = new DashboardPage(); // get the dashboard page.
                var i;

                if (bidask.substr(0, 1).toUpperCase() === 'B')  {
                    for (i = 0; i < number; i++) {
                        dashboard.newBid(product);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice+priceincrement*i);
                        newOrder.create();
                    }
                } else {
                    for (i = 0; i < number; i++) {
                        dashboard.newAsk(product);
                        newOrder = new NewOrderPage();
                        newOrder.setPrice(startprice+priceincrement*i);
                        newOrder.create();
                    }
                }
            }, 1200000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("hits a bid", function () {
        var specIt = "hits a bid";
        var executeOrder;
        var dashboard = new DashboardPage(); // get the dashboard page.
        var i;

        /*
         dashboard.allBidAskRows().then(function (BidAskRows) {
         expect(BidAskRows.length).toEqual('BidAskRows.length');
         });

         dashboard.Bids().then(function (Bids) {
         expect(Bids.length).toEqual('Bids.length');
         });

         dashboard.Asks().then(function (Asks) {
         expect(Asks.length).toEqual('Asks.length');
         });

         //expect(dashboard.bidAskRowCount()).toEqual('dashboard.bidAskRowCount()');
         */
        browser.sleep(20000);
        for (i = 0; i < 10; i++){
            dashboard.openBid(0);
            executeOrder = new ExecuteOrderPage();
            executeOrder.execute();
            browser.sleep(1000);
            dashboard.openAsk(0);
            executeOrder = new ExecuteOrderPage();
            executeOrder.execute();
            browser.sleep(1000);
         }
    }, 1200000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("can be paused", function () {
        var specIt = "can be paused";
        expect(true).toEqual(true);
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs out", function () {
        var specIt = "logs out";
        var dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.logout(hostURL);
        login.waitForLoad(10);
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." +  specIt + '.png');
    });

}, 1200000);
