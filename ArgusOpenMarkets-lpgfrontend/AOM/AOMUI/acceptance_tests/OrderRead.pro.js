var Automation = require("./automation");
var automation = new Automation();

// Require keyword library
var key = require('keyword');
key(require('./AOMKeywordsSignin'));
key(require('./AOMKeywordsOrder'));

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Order read', function () {
    var hostURL;
    var iStep = 1;
    var specDescription = this.description;

    var username = browser.params.credentials.username;
    var password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);
    //browser.executeScript("window.onbeforeunload = function(){};");

    xit("sets things up", function () {
        browser.executeScript("window.onbeforeunload = function(){};");
    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("order read",
        [   ['Signin', ['t1', 'password', 'false']]],
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("show bids and asks", function () {

        var iIndex = 0;
        var iRows = 0;
        var iColumns = 0;
        var iProducts = 0;
        var iCol =0;
        var iRow;
        var sBidAsk = '';

        var DashboardPage = require("./pages/dashboardpage");
        var dashboard = new DashboardPage(); // get the dashboard page.

        dashboard.getProductTitles().then(function (ProductTitles) {
            console.log('ProductTitles.length = ' + ProductTitles.length);
            iProducts = ProductTitles.length;
        });

        dashboard.allBidAskRows().then(function (BidAskRows) {
            console.log('BidAskRows.length = ' + BidAskRows.length);
            iRows = BidAskRows.length/(iProducts*2);
            iColumns = BidAskRows.length/iRows;

            for (var i = 0; i <= BidAskRows.length - 1; i++) {
                 BidAskRows[i].getText().then(function (RowText) {
                    //console.log('Row:' + parseInt((iIndex)/(iProducts*2)));
                    iCol = parseInt(iIndex/iRows+1);
                    iRow = parseInt((iIndex+1)%iRows);
                    if (RowText.length > 0) {
                        sBidAsk = 'BID';
                        if (iCol%2===0) {sBidAsk = 'ASK'}
                        console.log('Index[' + iIndex + '] Coord[' + iCol +',' + iRow + '] [' + sBidAsk + ']=' + RowText.replace(/\n/g, '~'));
                    }
                    iIndex++;
                 });
            }
        });

    },120000);

    // -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("show bids and asks grid", function () {
        var iRows = 0;
        var iColumns = 0;
        var iProducts = 0;
        var sLine = '';

        var DashboardPage = require("./pages/dashboardpage");
        var dashboard = new DashboardPage(); // get the dashboard page.

        dashboard.getProductTitles().then(function (ProductTitles) {
            console.log('ProductTitles.length = ' + ProductTitles.length);
            iProducts = ProductTitles.length;

            dashboard.allBidAskRows().then(function (BidAskRows) {
                console.log('BidAskRows.length = ' + BidAskRows.length);
                iRows = BidAskRows.length/(iProducts*2);
                iColumns = BidAskRows.length/iRows;

                var iProduct=0;
                for (var i=0; i < iProducts; i++) {
                    ProductTitles[i].getText().then(function (ProductText) {
                        sLine = sLine + '                            ' + ProductText + '            ';
                        if (iProduct%iProducts === 1) {
                            console.log(sLine);
                            console.log('       |              BID               |              ASK               |              BID               |              ASK               |');
                            console.log('-------+--------------------------------+--------------------------------+--------------------------------+--------------------------------|');
                        }
                        iProduct++;
                    });
                }

                var iCell = 0;
                for (var iRow=0; iRow < iRows; iRow++) {
                    for (var iCol=0; iCol < iColumns; iCol++){
                        BidAskRows[iCol*iRows+iRow].getText().then(function (RowText) {
                            if (iCell%iColumns === 0) {
                                sLine = '| ';
                            }

                            if (RowText.length > 0) {
                                sLine = sLine + (RowText.replace(/\n/g, '~') + '               ').slice(0, 30) + ' | ';
                            } else {
                                sLine = sLine + 'NONE                          ' + ' | ';
                            }

                            if (iCell%iColumns === 3) {
                                console.log("Row[" + parseInt(iCell/iColumns) + "]:" + sLine);
                            }
                            iCell++;
                        });
                    }
                }

            });
        });

    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("order read",
        [   ['Signout', ['none']]],
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });
}, 60000);