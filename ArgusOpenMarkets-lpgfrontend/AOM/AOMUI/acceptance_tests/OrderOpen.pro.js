var Automation = require("./automation");
var automation = new Automation();

// Require keyword library
var key = require('keyword');
key(require('./AOMKeywordsSignin'));
key(require('./AOMKeywordsOrder'));

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Order open', function () {
    var hostURL;
    var iStep = 1;
    var specDescription = this.description;

    var username = browser.params.credentials.username;
    var password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);
    //browser.executeScript("window.onbeforeunload = function(){};");

    xit("sets things up", function () {
        browser.executeScript("window.onbeforeunload = function(){};");
    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("create order",
        [   ['Signin', ['t1', 'password', 'false']]],
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("show bids and asks", function () {

        var DashboardPage = require("./pages/dashboardpage");
        var dashboard = new DashboardPage(); // get the dashboard page.

        /*
        dashboard.allBidAskRows().then(function (BidAskRows) {
            expect(BidAskRows.length).toEqual('BidAskRows.length');
            //expect(BidAskRows[0].getText().toString().replace('\n','+')).toEqual('BidAskRows[0].getText()');
//            expect(BidAskRows[0].getText()).toEqual('BidAskRows[1].getText()');
//            expect(BidAskRows[1].getText()).toEqual('BidAskRows[1].getText()');
//            expect(BidAskRows[2].getText()).toEqual('BidAskRows[2].getText()');
//            expect(BidAskRows[6].getText()).toEqual('BidAskRows[6].getText()');
//            expect(BidAskRows[12].getText()).toEqual('BidAskRows[12].getText()');
            for (var i = 0; i <= BidAskRows.length; i++) {
                console.log('BidAskRows[' + i + '].getText() = ' + BidAskRows[i].getText());
            }
        });
        */
 /*
         dashboard.Bids().then(function (Bids) {
            console.log('Bids.length = ' + Bids.length);
//            expect(Bids[0].getText()).toEqual('Bids[0].getText()');
//            expect(Bids[1].getText()).toEqual('Bids[1].getText()');
//            expect(Bids[2].getText()).toEqual('Bids[2].getText()');
//            expect(Bids[3].getText()).toEqual('Bids[3].getText()');
//            expect(Bids[4].getText()).toEqual('Bids[4].getText()');
            for (var i = 0; i <= Bids.length -1; i++) {
                console.log('Bids[' + i + '].getText() = ' + Bids[i].getText());
                console.log('Bids[' + i + '].getText() = ' + Bids.getItem(i).getText());
            }
        });
*/
    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("show bids and asks", function () {

        var iRow = 0;

        element.all(by.repeater('row in renderedRows')).then(function (BidAskRows) {
            console.log('BidAskRows.length = ' + BidAskRows.length);
            for (var i = 0; i <= BidAskRows.length - 1; i++) {
                //console.log('Row[' + i + ']=' +  BidAskRows[i].getText().replace(/\n/g, '~'));
                //console.log('Row[' + i + ']=');
                BidAskRows[i].getText().then(function (RowText) {
                    console.log('Row[' + iRow + ']=' + RowText.replace(/\n/g, '~'));
                    //console.log('Row: ' + RowText.replace(/\n/g, '~'));
                    iRow++;
                });
            }
        });


    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit("show bids and asks", function () {

        var iIndex = 0;
        var iRows = 0;
        var iColumns = 0;
        var iProducts = 0;
        var iCol =0;
        var iRow;
        var sBidAsk = '';

        var DashboardPage = require("./pages/dashboardpage");
        var dashboard = new DashboardPage(); // get the dashboard page.

        dashboard.getProductTitles().then(function (ProductTitles) {
            console.log('ProductTitles.length = ' + ProductTitles.length);
            iProducts = ProductTitles.length;
        });

        dashboard.allBidAskRows().then(function (BidAskRows) {
            console.log('BidAskRows.length = ' + BidAskRows.length);
            iRows = BidAskRows.length/(iProducts*2);
            iColumns = BidAskRows.length/iRows;

            for (var i = 0; i <= BidAskRows.length - 1; i++) {
                 BidAskRows[i].getText().then(function (RowText) {
                    //console.log('Row:' + parseInt((iIndex)/(iProducts*2)));
                    iCol = parseInt(iIndex/iRows+1);
                    iRow = parseInt((iIndex+1)%iRows);
                    if (RowText.length > 0) {
                        sBidAsk = 'BID';
                        if (iCol%2===0) {sBidAsk = 'ASK'}
                        console.log('Index[' + iIndex + '] Coord[' + iCol +',' + iRow + '] [' + sBidAsk + ']=' + RowText.replace(/\n/g, '~'));
                    }
                    iIndex++;
                 });
            }
        });

    },120000);

    // -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("show bids and asks grid", function () {

        var iIndex = 0;
        var iRows = 0;
        var iColumns = 0;
        var iProducts = 0;
        var sBidAsk = '';
        var iIndex = 0;
        var sLine = '';

        var DashboardPage = require("./pages/dashboardpage");
        var dashboard = new DashboardPage(); // get the dashboard page.

        dashboard.getProductTitles().then(function (ProductTitles) {
            console.log('ProductTitles.length = ' + ProductTitles.length);
            iProducts = ProductTitles.length;

            dashboard.allBidAskRows().then(function (BidAskRows) {
                console.log('BidAskRows.length = ' + BidAskRows.length);
                iRows = BidAskRows.length/(iProducts*2);
                iColumns = BidAskRows.length/iRows;

                var iProduct=0;
                for (var i=0; i < iProducts; i++) {
                    ProductTitles[i].getText().then(function (ProductText) {
                        sLine = sLine + '                            ' + ProductText + '            ';
                        if (iProduct%iProducts === 1) {
                            console.log(sLine);
                            console.log('       |              BID               |              ASK               |              BID               |              ASK               |');
                            console.log('-------+--------------------------------+--------------------------------+--------------------------------+--------------------------------|');
                        }
                        iProduct++;
                    });
                }

                for (var iRow=0; iRow < iRows; iRow++) {
                    for (var iCol=0; iCol < iColumns; iCol++){
                        BidAskRows[iCol*iRows+iRow].getText().then(function (RowText) {
                            if (iIndex%iColumns === 0) {
                                sLine = '| ';
                            }

                            if (RowText.length > 0) {
                                sLine = sLine + (RowText.replace(/\n/g, '~') + '               ').slice(0, 30) + ' | ';
                            } else {
                                sLine = sLine + 'NONE                          ' + ' | ';
                            }

                            if (iIndex%iColumns === 3) {
                                console.log("Row[" + parseInt(iIndex/iColumns) + "]:" + sLine);
                            }
                            iIndex++;
                        });
                    }
                }

            });
        });

    },120000);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("create order",
        [   ['Signout', ['none']]],
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },120000);
        });
}, 60000);