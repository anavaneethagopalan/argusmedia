fs = require('fs');  //Need this for saving screenshot file

var Automation = (function () {

    function Automation() {

    }

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    Automation.prototype.hhmmssm = function () {
        var d = new Date();
        var t = ('0' + d.getHours()).slice(-2) + ('0' + d.getMinutes()).slice(-2) + ('0' + d.getSeconds()).slice(-2) +  ('00' + d.getMilliseconds()).slice(-3);

        return t;
    };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    Automation.prototype.screenshot = function (filename) {
        // Capture screenshot
        browser.takeScreenshot().then(function (png) {
            var stream = fs.createWriteStream(filename);
            stream.write(new Buffer(png, 'base64'));
            stream.end();
        }); //png
    };

// -------------------------------------------------------------------------
// A "using" function for data-driven tests
// -------------------------------------------------------------------------
    Automation.prototype.using = function (name, values, func){
        for (var i = 0, count = values.length; i < count; i++) {
            if (Object.prototype.toString.call(values[i]) !== '[object Array]') {
                values[i] = [values[i]];
            }
            func.apply(this, values[i]);
            jasmine.currentEnv_.currentSpec.description += ' (with "' + name + '" using ' + values[i].join(', ') + ')';
        }
    };

    return Automation;

})();

module.exports = Automation;
