fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
var AssessmentsPage = require("./pages/AssessmentsPage");
var dashboard;

var automation = new Automation();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Assessments', function () {
    var sBrowserName;
    var login;
    var hostURL;

    var username;
    var password;

    var saveBeforeSignin = false;

    var specDescription = this.description;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    //console.log('\r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        //console.log(sBrowserName)
    });


    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()) {
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

    login = new LoginPage();

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs in as user: '" + username + "'/'" + password + "'", function () {
        var specIt = "logs in as user";
        login.login(username, password, saveBeforeSignin, saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + " pwd=" + password + ".png");
        dashboard = new DashboardPage(); // get the dashboard page.
        dashboard.waitForLoad(20);
        dashboard.productTitles().then(function (ProductTitles) {
            expect(ProductTitles.length).not.toEqual(0);
        });
//        expect(dashboard.gridCount()).not.toEqual(0);
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + "=" + username + " pwd=" + password + '.Dashboard.png');
        //dashboard.logout();
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("has title", function () {
        var specIt = "has title";
        var assessments = new AssessmentsPage();

        assessments.getTitle().getText().then(function (Title) {
            expect(Title).toEqual('ASSESSMENTS');
        });

    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("has items", function () {
        var specIt = "has items";
        var assessments = new AssessmentsPage();
        var foo = jasmine.createSpy('foo');

        assessments.getAssessments().then(function (Assessments) {
            foo(Assessments.length, function () {
                return true;
            });
            expect(foo).toHaveBeenCalledWith(jasmine.any(Number), jasmine.any(Function));
            expect(Assessments.length).toBeGreaterThan(0);
            //expect(Assessments[0].getText()).toEqual('txtPeek');
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("has product title", function () {
        var specIt = "has product title";
        var assessments = new AssessmentsPage();

        assessments.getProducts().then(function (Products) {
            expect(Products[0].getText()).toContain("NAPHTHA")
        });
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("has price ranges", function () {
        var specIt = "has price ranges";
        var assessments = new AssessmentsPage();

        assessments.getPriceRange1().then(function (Range) {
            expect(Range[0].getText()).not.toEqual("")
        });

        assessments.getPriceRange2().then(function (Range) {
            expect(Range[0].getText()).not.toEqual("")
        });

    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it("logs out", function () {
        var specIt = "logs out";
        dashboard.logout(hostURL);
        login.waitForLoad(10);
        automation.screenshot(saveFolder + "/screenshot." + automation.hhmmssm() + sBrowserName + "." + specDescription + "." + specIt + '.png');
    });

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    xit('pauses', function () {
        var stream;
        var newOrder;
        console.log('pause'); //Newline
    });
}, 60000);
