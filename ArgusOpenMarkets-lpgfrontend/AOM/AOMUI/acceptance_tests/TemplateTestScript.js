var TestScript = [
    ['DoSomething A', ['DataItem', 'DataItem', 'DataItem', 'DataItem']],
    ['DoSomething B', ['DataItem', 'DataItem', 'DataItem']],
    ['DoSomething C', ['DataItem', 'DataItem', 'DataItem', 'DataItem', 'DataItem']],
    ['DoSomething D', ['DataItem', 'DataItem']],
    ['DoSomething E', ['none']]
];

module.exports = TestScript;

