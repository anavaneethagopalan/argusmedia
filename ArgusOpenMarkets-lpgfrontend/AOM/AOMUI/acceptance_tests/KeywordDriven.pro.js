fs = require('fs');  //Need this for saving screenshot file

var Automation = require("./automation");
var LoginPage = require("./pages/loginpage");
var DashboardPage = require("./pages/dashboardpage");
//var dashboard;

var automation = new Automation();

// Require keyword library
var key = require('keyword');
key(require('./AOMKeywords'));

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
//describe(temp +'-Login functionality', function () {
describe('Keyword Driven Test', function () {
    var sBrowserName;
    var login;
    var hostURL;
    var iStep = 1;


    var username;
    var password;

    var saveBeforeSignin = false;

    var specDescription = this.description;

    var d = new Date();
    var saveFolder = browser.params.saveFolder + "/" + specDescription;
    if (!fs.existsSync(saveFolder)) {
        fs.mkdir(saveFolder);
    }

    //console.log('\r\n');

    browser.getCapabilities().then(function (cap) {
        sBrowserName = cap.caps_.browserName + cap.caps_.version.substr(0, 2);
        //console.log(sBrowserName)
    });


    username = browser.params.credentials.username;
    password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    //browser.driver.quit();
    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
/*
    automation.using("run keywords",
        [['Step 1 - keyword', 'some data a'],
         ['Step 2 - keyword', 'some data b'],
         ['Step 3 - keyword', 'some data c'],
         ['Step 4 - keyword', 'some data d'],
         ['Step 5 - keyword', 'some data e'],
         ['Step 6 - keyword', 'some data f']],
            function(keyword, data) {
            it("run: " + keyword + " with data: " + data, function () {
                var specIt = "run: " + keyword ;
                expect(keyword).not.toEqual(''); // non-blank is enough
            });
        });
*/
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
//*
    automation.using("run AOMkeywords",
        [   ['Signin', ['john', 'password', 'false']],
            ['NewOrder', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['OrderInBidAskStack', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['OrderInMarketTicker', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['OrderInDatabase', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['Signout', ['none']]],
        function(keyword, data) {
            it("Step " + iStep++ + ":" + keyword + " with data: " + data, function () {
                var specIt = "run: " + keyword ;
                key.run(keyword, data).then(function() {
                    //expect(keyword).not.toEqual(''); // non-blank is enough
                });
           },120000);
        });
//*/
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    it('Ends', function () {
    });

}, 60000);