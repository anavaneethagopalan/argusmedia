var Automation = require("./automation");
var automation = new Automation();

// Require keyword library
var key = require('keyword');
key(require('./AOMKeywordsSignin'));
key(require('./AOMKeywordsOrder'));

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
describe('Order create', function () {
    var hostURL;
    var iStep = 1;
    var specDescription = this.description;

    var username = browser.params.credentials.username;
    var password = browser.params.credentials.password;

    switch (browser.params.testEnv.toUpperCase()){
        case 'LOCAL':
            hostURL = browser.params.hostURL.local;
            break;
        case 'DEV':
            hostURL = browser.params.hostURL.dev;
            break;
        case 'SYSTEST':
            hostURL = browser.params.hostURL.systest;
            break;
        default:
    }

    browser.get(hostURL);

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
    automation.using("create order",
        [   ['Signin', ['ta1', 'password', 'false']],
//            ['NewOrder', [0, 'Bid', '12500', '800', 'notes']],
//            ['NewOrder', [0, 'Ask', '12500', '500', 'notes']],
//            ['NewOrders', [2, 0, 'Bid', '12500', 801, 1]],
            ['NewOrders', [100, 0, 'Bid', '12500', 5, 0.1]],
            ['NewOrders', [100, 0, 'Ask', '12500', 100, -0.1]],
            ['NewOrders', [100, 0, 'Bid', '12500', 5, 0.1]],
            ['NewOrders', [100, 0, 'Ask', '12500', 100, -0.1]],
            ['NewOrders', [100, 0, 'Bid', '12500', 5, 0.1]],
            ['NewOrders', [100, 0, 'Ask', '12500', 100, -0.1]],
//            ['OrderInBidAskStack', ['Outright', 'Bid', '800', '12500', 'notes']],
//            ['OrderInMarketTicker', ['Outright', 'Bid', '800', '12500', 'notes']],
//            ['OrderInDatabase', ['Outright', 'Bid', '800', '12500', 'notes']],
            ['Signout', ['none']]],
        function(keyword, data) {
            it("Step " + iStep++ + ": Keyword=" + keyword + ", Data=[" + data + "]", function () {
                key.run(keyword, data).then(function() {
                });
            },1200000);
        });

}, 1200000);