module.exports = function (grunt) {

    "use strict";

    /**
     * Load required Grunt tasks. These are installed based on the versions listed
     * in `package.json` when you do `npm install` in this directory.
     */
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-conventional-changelog");
    grunt.loadNpmTasks("grunt-bump");
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-karma");
    grunt.loadNpmTasks("grunt-ng-annotate");
    grunt.loadNpmTasks('grunt-protractor-webdriver');
    grunt.loadNpmTasks('grunt-protractor-runner');
    grunt.loadNpmTasks('grunt-express');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-csssplit');
    grunt.loadNpmTasks('grunt-string-replace');

    /**
     * Load in our build configuration file.
     */
    var userConfig = require("./build.config.js");

    var path = require('path'),
        ptorDir = 'node_modules/.bin/';

    /**
     * This is the configuration object Grunt uses to give each plugin its
     * instructions.
     */
    var taskConfig = {
        /**
         * We read in our `package.json` file so we can access the package name and
         * version. It's already there, so we don't repeat ourselves here.
         */
        pkg: grunt.file.readJSON("package.json"),

        /**
         * The banner is the comment that is placed at the top of our compiled
         * source files. It is first processed as a Grunt template, where the `<%=`
         * pairs are evaluated based on this very configuration object.
         */
        meta: {
            banner: "/**\n" +
                " * <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today('yyyy-mm-dd') %>\n" +
                " * <%= pkg.homepage %>\n" +
                " *\n" +
                " * Copyright (c) <%= grunt.template.today('yyyy') %> <%= pkg.author %>\n" +
                " * Licensed <%= pkg.licenses.type %> <<%= pkg.licenses.url %>>\n" +
                " */\n"
        },

        /**
         * Creates a changelog on a new version.
         */
        changelog: {
            options: {
                dest: "CHANGELOG.md",
                template: "changelog.tpl"
            }
        },

        /**
         * Increments the version number, etc.
         */
        bump: {
            options: {
                files: [
                    "package.json",
                    "bower.json"
                ],
                commit: false,
                commitMessage: "chore(release): v%VERSION%",
                commitFiles: [
                    "package.json",
                    "client/bower.json"
                ],
                createTag: false,
                tagName: "v%VERSION%",
                tagMessage: "Version %VERSION%",
                push: false,
                pushTo: "origin"
            }
        },

        /**
         * The directories to delete when `grunt clean` is executed.
         */
        clean: {
            options:{
                force:true

            },
            build:["<%= build_dir %>"],
            postcompile:{src:["<%= compile_dir %>/src/app/**/*.js"]},
            compile:["<%= compile_dir %>"]

        },

        /**
         * The `copy` task just copies files from A to B. We use it here to copy
         * our project assets (images, fonts, etc.) and javascripts into
         * `build_dir`, and then to copy the assets to `compile_dir`.
         */
        copy: {
            favico_compile:{
                src: [ "**/*.ico" ],
                dest: "<%= compile_dir %>",
                cwd: "src",
                expand: true
            },
            font_awesome:{
                expand: true,
                dot: true,
                cwd: 'vendor/font-awesome',
                src: ['fonts/*.*'],
                dest: "<%= build_dir %>/vendor/font-awesome/"
            },
            font_awesome_compile:{
                expand: true,
                dot: true,
                cwd: 'vendor/font-awesome',
                src: ['fonts/*.*'],
                dest: "<%= compile_dir %>/vendor/font-awesome/"
            },
            favico_build:{
                src: [ "**/*.ico" ],
                dest: "<%= build_dir %>",
                cwd: "src",
                expand: true
            },
            build_app_assets: {
                files: [
                    {
                        src: [ "**" ],
                        dest: "<%= build_dir %>/assets/",
                        cwd: "src/assets",
                        expand: true
                    }
                ]
            },
            build_vendor_assets: {
                files: [
                    {
                        src: [ "<%= vendor_files.assets %>" ],
                        dest: "<%= build_dir %>/assets/",
                        cwd: ".",
                        expand: true,
                        flatten: true
                    }
                ]
            },
            build_vendor_assetimages: {
                files: [
                    {
                        src: [ "<%= vendor_files.assetimages %>" ],
                        dest: "<%= build_dir %>/assets/images/",
                        cwd: ".",
                        expand: true,
                        flatten: true
                    }
                ]
            },
            build_appjs: {
                files: [
                    {
                        src: [ "<%= app_files.js %>" ],
                        dest: "<%= build_dir %>/",
                        cwd: ".",
                        expand: true
                    }
                ]
            },
            build_appviews: {
                files: [
                    {
                        src: [ "<%= app_files.html %>" ],
                        dest: "<%= build_dir %>/",
                        cwd: ".",
                        expand: true
                    }
                ]
            },
            compile_html:{
                files: [
                    {
                        src: [ "<%= app_files.html %>" ],
                        dest: "<%= compile_dir %>/",
                        cwd: ".",
                        expand: true
                    }
                ]
            },
            build_vendorjs: {
                files: [
                    {
                        src: [ "<%= vendor_files.js %>" ],
                        dest: "<%= build_dir %>/",
                        cwd: ".",
                        expand: true
                    }
                ]
            },
            compile_vendorjs: {
                files: [
                    {
                        src: [ "<%= vendor_files.js %>" ],
                        dest: "<%= compile_dir %>/",
                        cwd: ".",
                        expand: true
                    }
                ]
            },
            compile_vendorcss: {
                files: [
                    {
                        src: [ "<%= vendor_files.css %>" ],
                        dest: "<%= compile_dir %>/",
                        cwd: ".",
                        expand: true
                    }
                ]
            },
            build_vendorcss: {
                files: [
                    {
                        src: [ "<%= vendor_files.css %>" ],
                        dest: "<%= build_dir %>/",
                        cwd: ".",
                        expand: true
                    }
                ]
            },
            compile_assets: {
                files: [
                    {
                        src: [ "**" ],
                        dest: "<%= compile_dir %>/assets",
                        cwd: "<%= build_dir %>/assets",
                        expand: true
                    }
                ]
            }
        },


        /**
         * String replace - app.js - need to switch debug mode from true to false when compiling
         */
        'string-replace': {
            single_file: {
                files: {
                    'src/app.js': 'src/app.js'
                },
                options: {
                    replacements: [{
                        pattern: '$logProvider.debugEnabled(true);',
                        replacement: '$logProvider.debugEnabled(false);'
                    }]
                }
            }
        },

        /**
         * `grunt concat` concatenates multiple source files into a single file.
         */
        concat: {
            /**
             * The `build_css` target concatenates compiled CSS and vendor CSS together.
             */
            build_AomCss: {
                src: ['<%= vendor_files.css %>', '<%= build_dir %>/src/<%= pkg.name %>-<%= pkg.version %>.css'],
                dest: '<%= build_dir %>/src/<%= pkg.name %>-<%= pkg.version %>.css'
            },
            build_CmeCss: {
                src: ['<%= vendor_files.css %>', '<%= build_dir %>/src/<%= pkg.name %>-<%= pkg.version %>default.css'],
                dest: '<%= build_dir %>/src/<%= pkg.name %>-<%= pkg.version %>default.css'
            },
            /**
             * The `compile_js` target is the concatenation of our application source
             * code and all specified vendor source code into a single file.
             */
            compile_js: {
                options: {
                    banner: "<%= meta.banner %>"
                },
                src: [
                    "<%= vendor_files.js %>",
                    "module.prefix",
                    "<%= build_dir %>/src/**/*.js",
                    "module.suffix"
                ],
                dest: "<%= compile_dir %>/<%= pkg.name %>-<%= pkg.version %>.js"
            }
        },

        /**
         * `ng-min` annotates the sources before minifying. That is, it allows us
         * to code without the array syntax.
         */
        ngAnnotate: {
            compile: {
                options:{
                    add: true
                },
                files: [
                    {
                        src: [ "<%= compile_dir %>/<%= pkg.name %>-<%= pkg.version %>.js" ],
                        extDot: 'last',
                        ext: '.js',
                        expand: true
                    }
                ]
            }
        },

        /**
         * Minify the sources!
         */
        uglify: {
            compile: {
                options: {
                    banner: "<%= meta.banner %>"
                },
                files: {
                    "<%= concat.compile_js.dest %>": "<%= concat.compile_js.dest %>"
                }
            }
        },

        less: {
            build: {
                files: [
                    {'<%= build_dir %>/src/<%= pkg.name %>-<%= pkg.version %>.css': '<%= app_files.less %>'},
                    {'<%= build_dir %>/src/<%= pkg.name %>-<%= pkg.version %>default.css': '<%= app_files.defaultless %>'}
                ]
            },

            compile: {
                files: {
                    '<%= build_dir %>/src/<%= pkg.name %>-<%= pkg.version %>.css': '<%= app_files.less %>'
                },
                options: {
                    cleancss: true,
                    compress: true
                }
            }
        },

        csssplit: {
            build: {
                src: ['<%= build_dir %>/src/*.css'],
                dest: '<%= build_dir %>/assets',
                options: {
                        maxRules: 2000,
                        maxPages: 10,
                        suffix: 'Page'
                }
            },
            compile: {
                src: ['<%= compile_dir %>/src/*.css'],
                dest: '<%= compile_dir %>/assets',
                options: {
                    maxRules: 2000,
                    maxPages: 10,
                    suffix: 'Page'
                }
            }
        },


        /**
         * `jshint` defines `the rules of our linter as well as which files we
         * should check. This file, all javascript sources, and all our unit tests
         * are linted based on the policies listed in `options`. But we can also
         * specify exclusionary patterns by prefixing them with an exclamation
         * point (!); this is useful when code comes from a third party but is
         * nonetheless inside `src/`.
         */
        jshint: {
            src: [
                "<%= app_files.js %>",
                "**/*.spec.js"
            ],
            test: [
                "<%= app_files.jsunit %>"
            ],
            gruntfile: [
                "Gruntfile.js"
            ],
            options: {
                jshintrc: ".jshintrc"
            }
        },

        /**
         * The Karma configurations.
         */
        karma: {
            options: {
                configFile: "<%= build_dir %>/karma-unit.js"
            },
            unit: {
                runnerPort: 9101,
                background: true
            },
            continuous: {
                runnerPort: 9101,
                singleRun: true
            }
        },

        protractor: {
            options: {
                configFile: 'protractorCIConfig.js',
                keepAlive: true,  // ,false
                browser: 'chrome'
            },
            local: {},
            inttest: {
                options: {
                    configFile: './automated_tests/protractorCIConfig.js',
                    keepAlive: true, // false,
                    args: {
                        browser: (grunt.option("browser") === undefined ? "chrome" : (grunt.option("browser") === 0 ? "chrome" : grunt.option("browser"))),
                        params: {
                            testEnv: (grunt.option("testenv") === undefined ? "systest" : grunt.option("testenv"))
                        }
                    }
                }
            },

            serialtest: {
                options: {
                    configFile: './automated_tests/protractorCIConfig.js',
                    keepAlive: true,
                    args: {
                        browser: (grunt.option("browser") === undefined ? "chrome" : (grunt.option("browser") === 0 ? "chrome" : grunt.option("browser"))),
                        params: {
                            testEnv: (grunt.option("testenv") === undefined ? "systest" : grunt.option("testenv"))
                        }
                    }
                }
            },

            paralleltest: {
                options: {
                    configFile: './automated_tests/protractorCIConfig.js',
                    keepAlive: true,
                    args: {
                        browser: (grunt.option("browser") === undefined ? "chrome" : (grunt.option("browser") === 0 ? "chrome" : grunt.option("browser"))),
                        capabilities:{
                            //browserName: (grunt.option("browser") === undefined ? "chrome" : (grunt.option("browser") === 0 ? "chrome" : grunt.option("browser"))),
                            shardTestFiles : true,
                            maxInstances : (grunt.option("maxinstances") === undefined ? 99 : (grunt.option("maxinstances") <= 1 ? 99 : grunt.option("maxinstances")))
                        },
                        params: {
                            testEnv: (grunt.option("testenv") === undefined ? "systest" : grunt.option("testenv"))
                        }
                    }
                }
            }
        },

        shell: {
            protractor: {
                options: {
                    stdout: true
                },
                command: path.resolve(ptorDir + 'webdriver-manager') + ' update --standalone --chrome' // --ie --force'
            }
        },

        protractor_webdriver: {
            e2e: {
                options: {
                    path: ptorDir
                }
            }
        },

        /**
         * The `index` task compiles the `index.html` file as a Grunt template. CSS
         * and JS files co-exist here but they get split apart later.
         */
        index: {

            /**
             * During development, we don't want to have wait for compilation,
             * concatenation, minification, etc. So to avoid these steps, we simply
             * add all script files directly to the `<head>` of `index.html`. The
             * `src` property contains the list of included files.
             */
            build: {
                dir: "<%= build_dir %>",
                src: [
                    "<%= vendor_files.js %>",
                    "<%= build_dir %>/src/**/*.js",
                    "<%= vendor_files.css %>",
                    '<%= build_dir %>/assets/*.css'
                ]
            },

            /**
             * When it is time to have a completely compiled application, we can
             * alter the above to include only a single JavaScript and a single CSS
             * file. Now we're back!
             */
            compile: {
                dir: "<%= compile_dir %>",
                src: [
                    "<%= concat.compile_js.dest %>",
                    "<%= vendor_files.css %>",
                    '<%= build_dir %>/assets/*.css'
                ]
            }
        },

        /**
         * This task compiles the karma template so that changes to its file array
         * don't have to be managed manually.
         */
        karmaconfig: {
            unit: {
                dir: "<%= build_dir %>",
                src: [
                    "<%= vendor_files.js %>",
                    "<%= test_files.js %>"
                ]
            }
        },

        /**
         * And for rapid development, we have a watch set up that checks to see if
         * any of the files listed below change, and then to execute the listed
         * tasks when they do. This just saves us from having to type "grunt" into
         * the command-line every time we want to see what we're working on; we can
         * instead just leave "grunt watch" running in a background terminal. Set it
         * and forget it, as Ron Popeil used to tell us.
         *
         * But we don't need the same thing to happen for all the files.
         */
        delta: {
            /**
             * By default, we want the Live Reload to work for all tasks; this is
             * overridden in some tasks (like this file) where browser resources are
             * unaffected. It runs by default on port 35729, which your browser
             * plugin should auto-detect.
             */
            options: {
                livereload: true
            },

            /**
             * When the Gruntfile changes, we just want to lint it. In fact, when
             * your Gruntfile changes, it will automatically be reloaded!
             */
            gruntfile: {
                files: "Gruntfile.js",
                tasks: [ "jshint:gruntfile" ],
                options: {
                    livereload: false
                }
            },

            /**
             * When our JavaScript source files change, we want to run lint them and
             * run our unit tests.
             */
            jssrc: {
                files: [
                    "<%= app_files.js %>"
                ],
                tasks: [ "jshint:src", "karma:unit:run", "copy:build_appjs" ]
            },

            /**
             * When assets are changed, copy them. Note that this will *not* copy new
             * files, so this is probably not very useful.
             */
            assets: {
                files: [
                    "src/assets/**/*"
                ],
                tasks: [ "copy:build_app_assets" ]
            },

            /**
             * When index.html changes, we need to compile it.
             */
            html: {
                files: [ "<%= app_files.html %>" ],
                tasks: [ "index:build", "copy:build_appviews" ]
            },

            /**
             * When the CSS files change, we need to compile and minify them.
             */
            less: {
                files: [ "src/**/*.less" ],
                tasks: [ "less:build" ]
            },

            /**
             * When a JavaScript unit test file changes, we only want to lint it and
             * run the unit tests. We don't want to do any live reloading.
             */
            jsunit: {
                files: [
                    "<%= app_files.jsunit %>"
                ],
                tasks: [ "jshint:test", "karma:unit:run" ],
                options: {
                    livereload: false
                }
            }
        }
    };

    grunt.initConfig(grunt.util._.extend(taskConfig, userConfig));

    var previous_force_state = grunt.option("force");
    grunt.registerTask("force",function(set){
        if (set === "on") {
            grunt.option("force",true);
        }
        else if (set === "off") {
            grunt.option("force",false);
        }
        else if (set === "restore") {
            grunt.option("force",previous_force_state);
        }
    });
    /**
     * In order to make it safe to just compile or copy *only* what was changed,
     * we need to ensure we are starting from a clean, fresh build. So we rename
     * the `watch` task to `delta` (that's why the configuration var above is
     * `delta`) and then add a new task called `watch` that does a clean build
     * before watching for changes.
     */
    grunt.renameTask("watch", "delta");
    grunt.registerTask("watch", ["jshint", "build", "karmaconfig", "karma:unit", "delta"]);

    /**
     * The default task is to build and compile.
     */
    grunt.registerTask("default", ["build", "compile"]);

    /**
     * The `build` task gets your app ready to run for development and testing.
     */
    grunt.registerTask("build", [
        "force:on", "clean", "jshint", "less:build", 
        "concat:build_AomCss", "concat:build_CmeCss", "copy:build_app_assets", "copy:build_vendor_assets", "copy:favico_build", "copy:font_awesome",
        "copy:build_appjs", "copy:build_appviews", "copy:build_vendorjs", "copy:build_vendorcss", "csssplit:build", "index:build", "UpdateAppConfigCssVersion"
    ]);

    grunt.registerTask("test", ["karmaconfig", "karma:continuous"]);

    grunt.registerTask("intTest", ["shell:protractor", "protractor_webdriver:e2e", 'protractor:inttest']);

    grunt.registerTask("serialTest", ["shell:protractor", "protractor_webdriver:e2e", 'protractor:serialtest']);

    grunt.registerTask("parallelTest", ["shell:protractor", "protractor_webdriver:e2e", 'protractor:paralleltest']);

    grunt.registerTask("UpdateAppConfigCssVersion", function () {
        var appConfigFilePath = grunt.config("build_dir") + "/assets/applicationConfig.json";
        var appConfigFile = grunt.file.readJSON(appConfigFilePath);
        appConfigFile.cssVersion = taskConfig.pkg.version;
        grunt.file.write(appConfigFilePath, JSON.stringify(appConfigFile, null, 2));
    });

    /**
     * The `compile` task gets your app ready for deployment by concatenating and minifying your code.
     */
    grunt.registerTask("compile", [
        "string-replace:single_file", "less:compile", "copy:compile_vendorjs", "copy:compile_assets", "copy:favico_compile", "copy:font_awesome_compile",
        "copy:compile_vendorcss", "copy:compile_html", "concat:compile_js", "ngAnnotate:compile", "uglify:compile", "csssplit:compile", "index:compile"
    ]);

    /**
     * A utility function to get all app JavaScript sources.
     */
    function filterForJS(files) {
        return files.filter(function (file) {
            return file.match(/\.js$/);
        });
    }

    /**
     * A utility function to get all app CSS sources.
     */
    function filterForCSS(files) {
        return files.filter(function (file) {
            return file.match(/\.css$/);
        });
    }

    /**
     * The index.html template includes the stylesheet and javascript sources
     * based on dynamic names calculated in this Gruntfile. This task assembles
     * the list into variables for the template to use and then runs the
     * compilation.
     */
    grunt.registerMultiTask("index", "Process index.html template", function () {
        var dirRE = new RegExp("^(" + grunt.config("build_dir") + "|" + grunt.config("compile_dir") + ")\/", "g");
        var jsFiles = filterForJS(this.filesSrc).map(function (file) {
            return file.replace(dirRE, "");
        });
        var cssFiles = filterForCSS(this.filesSrc).map(function (file) {

            return file.replace(dirRE, "");
        });

        grunt.file.copy("src/index.html", this.data.dir + "/index.html", {
            process: function (contents) {
                return grunt.template.process(contents, {
                    data: {
                        scripts: jsFiles,
                        styles: cssFiles,
                        version: grunt.config("pkg.version")
                    }
                });
            }
        });
    });

    /**
     * In order to avoid having to specify manually the files needed for karma to
     * run, we use grunt to manage the list for us. The `karma/*` files are
     * compiled as grunt templates for use by Karma. Yay!
     */
    grunt.registerMultiTask("karmaconfig", "Process karma config templates", function () {
        var jsFiles = filterForJS(this.filesSrc);

        grunt.file.copy("karma/karma-unit.tpl.js", grunt.config("build_dir") + "/karma-unit.js", {
            process: function (contents) {
                return grunt.template.process(contents, {
                    data: {
                        scripts: jsFiles
                    }
                });
            }
        });
    });

};