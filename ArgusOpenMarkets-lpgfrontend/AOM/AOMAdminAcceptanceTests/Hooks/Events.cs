﻿using AOMAdminAcceptanceTests.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace AOMAdminAcceptanceTests.Hooks
{
    [Binding]
    public class Events
    {
        public string ScreenshotFolder = ConfigurationManager.AppSettings["WebDriverScreenshotsFolder"];

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            Console.WriteLine("Initializing Test URL: " + ConfigurationManager.AppSettings["TestUrl"]);
            Console.WriteLine("Initializing Driver: " + ConfigurationManager.AppSettings["Environment"]);
            Console.WriteLine("Browser: " + ConfigurationManager.AppSettings["Browser"]);
            Console.WriteLine("Deleting Screenshots from previous Run");
            Console.WriteLine("Application Path " + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            var filepath = ConfigurationManager.AppSettings["WebDriverScreenshotsFolder"];
            // ReSharper disable once AssignNullToNotNullAttribute
            //var filepath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), screenShotsFolderPath);

            if (!Directory.Exists(filepath))
            {
                Console.WriteLine("File Does not exist....Creating File");
                Directory.CreateDirectory(filepath);
                Console.WriteLine("File Created");
            }
            new DriverManager().SetDriver();
        }

        [BeforeScenario()]
        public void BeforeScenario()
        {
            new DriverManager().SetDriver();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            var testcontext = ScenarioContext.Current["TestContext"] as TestContext;
            var screenshotfilename =
                $@"{ScreenshotFolder}\{testcontext.TestName}.jpeg";
            if (ScenarioContext.Current.TestError != null)
            {
                Console.WriteLine("Taking Screenshots");
                var image = TakeScreenShot();
                Console.WriteLine($"Screenshot file {screenshotfilename}");
                image.Save(screenshotfilename, ImageFormat.Jpeg);
                Console.WriteLine(testcontext.TestRunDirectory);
                Console.WriteLine(testcontext.TestRunResultsDirectory);
                // Console.WriteLine(testcontext.);
                testcontext.AddResultFile(screenshotfilename);
                new DriverManager().ExitDriver();
            }
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            new DriverManager().ExitDriver();
        }

        public static void Empty(DirectoryInfo directory)
        {
            if (directory.Exists)
                foreach (var file in directory.GetFiles())
                    file.Delete();
        }

        public static void GrantAccess(string fullPath)
        {
            var dInfo = new DirectoryInfo(fullPath);
            var dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
            //return true;
        }

        private Bitmap TakeScreenShot()
        {
            Bitmap stitchedImage;
            try
            {
                var totalwidth1 = (long)((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript("return document.body.offsetWidth");//documentElement.scrollWidth");

                var totalHeight1 = (long)((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript("return  document.body.scrollHeight");

                var totalWidth = (int)totalwidth1;
                var totalHeight = (int)totalHeight1;

                // Get the Size of the Viewport
                var viewportWidth1 = (long)((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript("return document.body.clientWidth");//documentElement.scrollWidth");
                var viewportHeight1 = (long)((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript("return window.innerHeight");//documentElement.scrollWidth");

                var viewportWidth = (int)viewportWidth1;
                var viewportHeight = (int)viewportHeight1;

                // Split the Screen in multiple Rectangles
                var rectangles = new List<Rectangle>();
                // Loop until the Total Height is reached
                for (var i = 0; i < totalHeight; i += viewportHeight)
                {
                    var newHeight = viewportHeight;
                    // Fix if the Height of the Element is too big
                    if (i + viewportHeight > totalHeight)
                    {
                        newHeight = totalHeight - i;
                    }
                    // Loop until the Total Width is reached
                    for (var ii = 0; ii < totalWidth; ii += viewportWidth)
                    {
                        var newWidth = viewportWidth;
                        // Fix if the Width of the Element is too big
                        if (ii + viewportWidth > totalWidth)
                        {
                            newWidth = totalWidth - ii;
                        }

                        // Create and add the Rectangle
                        var currRect = new Rectangle(ii, i, newWidth, newHeight);
                        rectangles.Add(currRect);
                    }
                }

                // Build the Image
                stitchedImage = new Bitmap(totalWidth, totalHeight);
                // Get all Screenshots and stitch them together
                var previous = Rectangle.Empty;
                foreach (var rectangle in rectangles)
                {
                    // Calculate the Scrolling (if needed)
                    if (previous != Rectangle.Empty)
                    {
                        var xDiff = rectangle.Right - previous.Right;
                        var yDiff = rectangle.Bottom - previous.Bottom;
                        // Scroll
                        //selenium.RunScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        ((IJavaScriptExecutor)DriverManager.Driver).ExecuteScript($"window.scrollBy({xDiff}, {yDiff})");
                        Thread.Sleep(200);
                    }

                    // Take Screenshot
                    var screenshot = ((ITakesScreenshot)DriverManager.Driver).GetScreenshot();

                    // Build an Image out of the Screenshot
                    Image screenshotImage;
                    using (var memStream = new MemoryStream(screenshot.AsByteArray))
                    {
                        screenshotImage = Image.FromStream(memStream);
                    }

                    // Calculate the Source Rectangle
                    var sourceRectangle = new Rectangle(viewportWidth - rectangle.Width, viewportHeight - rectangle.Height, rectangle.Width, rectangle.Height);

                    // Copy the Image
                    using (var g = Graphics.FromImage(stitchedImage))
                    {
                        g.DrawImage(screenshotImage, rectangle, sourceRectangle, GraphicsUnit.Pixel);
                    }

                    // Set the Previous Rectangle
                    previous = rectangle;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error on taking screenshots: " + e);
            }
            return stitchedImage;
        }
    }
}