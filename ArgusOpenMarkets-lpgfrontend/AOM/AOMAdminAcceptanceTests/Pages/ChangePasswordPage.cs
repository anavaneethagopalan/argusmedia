﻿using AOMAdminAcceptanceTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AOMAdminAcceptanceTests.Pages
{
    public class ChangePasswordPage : BasePage
    {
        public ChangePasswordPage()
        {
            new SeleniumHelpers().WaitUntilElementIsDisplayed(pgheader, 10);
            Assert.AreEqual("Change Password", pgheader.Text, "You are not on change password page");
        }
#pragma warning disable
        [FindsBy(How = How.TagName, Using = "h4")] private IWebElement pgheader;

        [FindsBy(How = How.Id, Using = "NewPassword")] private IWebElement newpassword;

        [FindsBy(How = How.Id, Using = "ConfirmPassword")] private IWebElement confirmpassword;

        [FindsBy(How = How.Id, Using = "aGeneratePwd")] private IWebElement generatePassword;

        [FindsBy(How = How.Id, Using = "TemporaryPassword")] private IWebElement temporarycheckbox;

        [FindsBy(How = How.CssSelector, Using = ".btn.btn-default[value=\"Set password\"]")]
        private IWebElement setPassword;
#pragma warning restore
        public HomePage SetTempPassword(string password, bool isPerm)
        {
            newpassword.SendKeys(password);
            confirmpassword.SendKeys(password);
            if (!temporarycheckbox.Selected)
                temporarycheckbox.Click();
            setPassword.Click();
            Driver.SwitchTo().Alert().Accept();
            return new HomePage();
        }

        public HomePage SetPermanentPassword(bool isPerm) 
        {
            generatePassword.Click();
            if (temporarycheckbox.Selected == isPerm)
            {
                temporarycheckbox.Click();
            }
            setPassword.Click();
            Driver.SwitchTo().Alert().Accept();
            return new HomePage();
        }

    /*    public HomePage SetPassword(string password, bool isPerm)
        {
            newpassword.SendKeys(password);
            confirmpassword.SendKeys(password);
            if (temporarycheckbox.Selected == isPerm)
            {
                temporarycheckbox.Click();
            }
            setPassword.Click();
            Driver.SwitchTo().Alert().Accept();
            return new HomePage();
        } */
    }
}