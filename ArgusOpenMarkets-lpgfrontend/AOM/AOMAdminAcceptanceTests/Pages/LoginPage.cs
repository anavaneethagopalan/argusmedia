﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Linq;
using AOMAdminAcceptanceTests.Helpers.Context;

// ReSharper disable CollectionNeverUpdated.Local

namespace AOMAdminAcceptanceTests.Pages
{
    public class LoginPage : BasePage
    {
        public LoginPage()
        {
            Assert.AreEqual("Log in", Driver.Title, "You are not on Login Page");
        }

#pragma warning disable 649

        [FindsBy(How = How.Id, Using = "UserName")]
        private IWebElement _username;

        [FindsBy(How = How.Id, Using = "Password")]
        private IWebElement _password;

        [FindsBy(How = How.CssSelector, Using = ".btn.btn-default")]
        private IWebElement _loginButton;

        [FindsBy(How = How.CssSelector, Using = ".field-validation-error>span")]
        private IWebElement _errormsg;

        [FindsBy(How = How.CssSelector, Using = " .validation-summary-errors>ul>li")]
        private IList<IWebElement> _loginerrmsg;

#pragma warning restore 649

        public HomePage EnterLoginDetails(string username, string password)
        {
            UserContext.Username = username;
            UserContext.Password = password;
            _username.SendKeys(username);
            _password.SendKeys(password);
            _loginButton.Click();
            return new HomePage();
        }

        public string ErrorMsg()
        {
            return _errormsg.Text;
        }

        public void EnterDetails(string username, string password)
        {
            _username.SendKeys(username);
            _password.SendKeys(password);
        }

        public LoginPage ClickLogin()
        {
            _loginButton.Click();
            return this;
        }

        public List<string> GetLoginErrorMsg()
        {
            return _loginerrmsg.Select(x => x.Text).ToList();
        }
    }
}