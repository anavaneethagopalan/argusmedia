﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using AOMAdminAcceptanceTests.Helpers;

namespace AOMAdminAcceptanceTests.Pages
{
    public class HomePage : BasePage
    {
        public HomePage()
        {
            
            Assert.IsTrue(Driver.Title.Contains("Home Page"), "You are not on Home Page");
        }

        [FindsBy(How = How.CssSelector, Using = ".nav.navbar-nav.navbar-right>li>a[title='Manage']")]
#pragma warning disable 649
        private IWebElement _usertext;

        [FindsBy(How = How.CssSelector, Using = ".list-group-item>a")]
        private IList<IWebElement> _userlinklist;

        [FindsBy(How = How.LinkText, Using = "Authentication History")]
        private IWebElement _authhistorylink;

        [FindsBy(How = How.LinkText, Using = "User Management")]
        private IWebElement _usermgmtlink;

#pragma warning restore 649

        public string GetUserText()
        {
            return _usertext.Text;
        }

        public List<String> GetLinks()
        {
            var linklist = _userlinklist.Select(x => x.Text).ToList();
            return linklist;
        }

        public AuthenticationHistoryPage ClickAutheHist()
        {
            _authhistorylink.Click();
            return new AuthenticationHistoryPage();
        }

        public UserManagementPage ClickUserMgmt()
        {
            _usermgmtlink.Click();
            return new UserManagementPage();
        }
    }
}