﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace AOMAdminAcceptanceTests.Pages
{
    public class EditUserPage : BasePage
    {
#pragma warning disable 649
        [FindsBy(How = How.Id, Using = "detailsTable")] private IWebElement _mainTable;
        private ReadOnlyCollection<IWebElement> _allinputs => _mainTable.FindElements(By.TagName("input"));
        private IWebElement _blockedCheckBox => _allinputs.First(x => x.GetAttribute("id").Contains("IsBlocked_CheckBox1"));

        [FindsBy(How = How.LinkText, Using = "Update")] private IWebElement _update;
#pragma warning restore 649

        public EditUserPage()
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(
                ExpectedConditions.ElementExists(By.LinkText("Update")));
        }

        public EditUserPage BlockUser()
        {
            if (!_blockedCheckBox.Selected)
            {
                _blockedCheckBox.Click();
                Driver.SwitchTo().Alert().Accept();
                _update.Click();
                Driver.SwitchTo().Alert().Accept();
            }
            return new EditUserPage();
        }
    }
}