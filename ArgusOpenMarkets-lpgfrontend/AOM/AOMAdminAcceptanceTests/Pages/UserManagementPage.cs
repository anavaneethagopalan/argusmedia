﻿using AOMAdminAcceptanceTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;

namespace AOMAdminAcceptanceTests.Pages
{
    public class UserManagementPage : BasePage
    {
        public UserManagementPage()
        {
            Assert.AreEqual("User Management", Driver.Title, "You are not on Usermanagement Page");
        }

#pragma warning disable 649

        [FindsBy(How = How.ClassName, Using = "DDSubHeader")]
        private IWebElement _header;

        [FindsBy(How = How.Id, Using = "GridView1")]
        private IWebElement _noitemstext;

        [FindsBy(How = How.ClassName, Using = "DDFilters")]
        private IWebElement _filterContainer;

        private ReadOnlyCollection<IWebElement> _allSelects => _filterContainer.FindElements(By.TagName("select"));

        private SelectElement _orgdropdown => new SelectElement(_allSelects.First(x => x.GetAttribute("id").Contains("ddlOrganisationDto")));

        private SelectElement _activedropdown => new SelectElement(_allSelects.First(x => x.GetAttribute("id").Contains("ddlIsActive")));

        private SelectElement _blockeddropdown => new SelectElement(_allSelects.First(x => x.GetAttribute("id").Contains("ddlIsBlocked")));

        private SelectElement _deleteddropdown => new SelectElement(_allSelects.First(x => x.GetAttribute("id").Contains("ddlIsDeleted")));

        private IWebElement _searchbox => _filterContainer.FindElements(By.TagName("input")).Single(x => x.GetAttribute("id").Contains("txbUserSearch"));

        [FindsBy(How = How.Id, Using = "GridView1")]
        private IWebElement _table;

        [FindsBy(How = How.CssSelector, Using = "#GridView1 .th>th")]
        private IList<IWebElement> _tableheaders;

        [FindsBy(How = How.CssSelector, Using = ".th>th")]
        private IWebElement _tabheader;

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_InsertHyperLink")]
        private IWebElement _insertNewItem;

#pragma warning restore 649

        public string GetHeaderText()
        {
            return _header.Text;
        }

        public string GetNoRowsFound()
        {
            return _noitemstext.Text;
        }

        public UserManagementPage SelectOrg(string orgname)
        {
            _orgdropdown.SelectByText(orgname);
            return this;
        }

        public UserManagementPage SelectActive(string active)
        {
            _activedropdown.SelectByText(active);
            return this;
        }

        public UserManagementPage SelectBlocked(string blocked)
        {
            _blockeddropdown.SelectByText(blocked);
            return this;
        }

        public UserManagementPage SelectDeleted(string deleted)
        {
            _deleteddropdown.SelectByText(deleted);
            return this;
        }

        public UserManagementPage SearchDetails(string searchText)
        {
            _searchbox.SendKeys(searchText);
            _searchbox.SendKeys(Keys.Enter);
            return new UserManagementPage();
        }

        public CreateUserPage ClickonInsertUserLink()
        {
            _insertNewItem.Click();
            return new CreateUserPage();
        }

        public DataTable GetOrgList()
        {
            var orgTable = new DataTable();

            if (new SeleniumHelpers().IsElementPresent(_tabheader))
            {
                //Get all headers from the table
                foreach (var headers in _tableheaders)
                {
                    orgTable.Columns.Add(headers.Text);
                }

                var rows = _table.FindElements(By.TagName("tr"));

                for (int i = 1; i <= rows.Count - 1; i++)
                {
                    var orgTableRow = orgTable.NewRow();
                    int j = 0;
                    var elementinrow = rows[i].FindElements(By.TagName("td"));

                    foreach (DataColumn column in orgTable.Columns)
                    {
                        orgTableRow[column] = elementinrow[j].Text;
                        j++;
                    }
                    var activecheckbox = rows[i].FindElements(By.TagName("input")).Single(x => x.GetAttribute("name").Contains("IsActive$CheckBox"));
                    var blockedcheckbox = rows[i].FindElements(By.TagName("input")).Single(x => x.GetAttribute("name").Contains("IsBlocked$CheckBox"));
                    var deletedcheckbox = rows[i].FindElements(By.TagName("input")).Single(x => x.GetAttribute("name").Contains("IsDeleted$CheckBox"));

                    if (activecheckbox.GetAttribute("checked") == "true")
                        orgTableRow["Active (CRM)"] = 1;
                    else
                        orgTableRow["Active (CRM)"] = 0;

                    if (blockedcheckbox.GetAttribute("checked") == "true")
                        orgTableRow["Blocked"] = 1;
                    else
                        orgTableRow["Blocked"] = 0;

                    if (deletedcheckbox.GetAttribute("checked") == "true")
                        orgTableRow["Deleted"] = 1;
                    else
                        orgTableRow["Deleted"] = 0;

                    orgTable.Rows.Add(orgTableRow);
                }
            }

            return orgTable;
        }

        public DataTable GetUserManagementFromDb()
        {
            var sqlhelper = new SqlHelper();
            var sql = string.Format(@"
            SELECT  ui.Name, ui.Title, ui.Email, ui.Telephone, ui.Username, date_format(uc.Expiration,'%c/%e/%Y %l:%i:%s %p'), ui.ArgusCrmUsername,
            o.Name as Organisation, group_concat(Distinct r.Name SEPARATOR ' ') as OrganisationRole,
            ui.IsActive as Active, ui.IsBlocked as Blocked, ui.IsDeleted as Deleted
            FROM `crm.{0}`.UserInfo as ui
            LEFT Join  `crm.{0}`.UserInfoRole as uir on ui.id = uir.UserInfo_Id_fk
            LEFT JOIN `crm.{0}`.OrganisationRole as r on uir.Role_Id_fk = r.id
            JOIN `crm.{0}`.Organisation as o on ui.organisation_id_fk = o.id
            LEFT JOIN `crm.{0}`.Organisation on o.id = r.organisation_id_fk
            LEFT JOIN `crm.{0}`.UserCredentials as uc on ui.id = uc.UserInfo_Id_fk
            GROUP BY ui.id
            ORDER BY ui.id", sqlhelper._tablePrefix);

            return sqlhelper.ExecuteSql(sql);
        }
    }
}