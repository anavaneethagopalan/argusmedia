﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using AOMAdminAcceptanceTests.Helpers.Context;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace AOMAdminAcceptanceTests.Pages
{
    public class CreateUserPage : BasePage
    {
#pragma warning disable 649

        [FindsBy(How = How.LinkText, Using = "Create")]
        private IWebElement _createlink;

        [FindsBy(How = How.Id, Using = "detailsTable")] private IWebElement MainTable;

        private ReadOnlyCollection<IWebElement> _allinputs => MainTable.FindElements(By.TagName("input"));
        private IWebElement _nameTextBox => _allinputs.First(x => x.GetAttribute("id").Contains("Name_TextBox1"));

        private IWebElement _titleTextBox => _allinputs.First(x => x.GetAttribute("id").Contains("Title_TextBox1"));

        private IWebElement _emailTextBox => _allinputs.First(x => x.GetAttribute("id").Contains("Email_TextBox1"));

        private IWebElement _telephoneTextBox => _allinputs.First(x => x.GetAttribute("id").Contains("Telephone_TextBox1"));

        private IWebElement _usernameTextBox => _allinputs.First(x => x.GetAttribute("id").Contains("Username_TextBox1"));

        private IWebElement _argusCrmUsernameTextBox => _allinputs.First(x => x.GetAttribute("id").Contains("ArgusCrmUsername_TextBox1"));

        private SelectElement _organisationElement => new SelectElement(MainTable.FindElements(By.TagName("select")).
            First(x => x.GetAttribute("id").Contains("OrganisationDto_DropDownList1")));

        private IWebElement _activeCheckBox => _allinputs.First(x => x.GetAttribute("id").Contains("IsActive_CheckBox1"));

        private IWebElement _blockedCheckBox => _allinputs.First(x => x.GetAttribute("id").Contains("IsBlocked_CheckBox1"));

        private IWebElement _deletedCheckBox => _allinputs.First(x => x.GetAttribute("id").Contains("IsBlocked_CheckBox1"));

        [FindsBy(How = How.ClassName, Using = "DDSuccess")] private IWebElement _result;

#pragma warning restore 649

        public CreateUserPage()
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementExists(By.LinkText("Create")));
        }

        public CreateUserPage InsertNewUser(InsertUpdateUserContext insertUserTable)
        {
            _nameTextBox.SendKeys(insertUserTable.Name);
            _titleTextBox.SendKeys(insertUserTable.Title);
            _emailTextBox.SendKeys(insertUserTable.Email);
            _telephoneTextBox.SendKeys(insertUserTable.Telephone);
            _usernameTextBox.SendKeys(insertUserTable.UserName);
            _argusCrmUsernameTextBox.SendKeys(insertUserTable.ArgusCrmUserName);

            _organisationElement.SelectByText(insertUserTable.Organisation);

            if (insertUserTable.Active)
            {
                _activeCheckBox.Click();
            }

            if (insertUserTable.Blocked)
            {
                _blockedCheckBox.Click();
            }

            if (insertUserTable.Deleted)
            {
                _deletedCheckBox.Click();
            }

            _createlink.Click();
            Driver.SwitchTo().Alert().Accept();
            new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until(
                ExpectedConditions.TextToBePresentInElement(_result, "was created successfully"));
            return this;
        }
    }
}