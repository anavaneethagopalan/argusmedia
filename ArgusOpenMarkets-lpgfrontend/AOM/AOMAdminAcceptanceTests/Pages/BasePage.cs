﻿using AOMAdminAcceptanceTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AOMAdminAcceptanceTests.Pages
{
    public abstract class BasePage
    {
        protected IWebDriver Driver = DriverManager.Driver;
#pragma warning disable 649
        [FindsBy(How = How.Id, Using = "logoutForm")] private IWebElement _logout;

        [FindsBy(How = How.LinkText, Using = "Home")]
        private IWebElement _HomePage;

#pragma warning restore 649

        protected BasePage()
        {
            PageFactory.InitElements(Driver, this);
            new SeleniumHelpers().WaitForPageToLoad(30);
        }

        public HomePage ClickOnHomePage()
        {
            _HomePage.Click();
            return new HomePage();
        }

        public LoginPage LogOff()
        {
            _logout.Submit();
            return new LoginPage();
        }
    }
}