﻿using AOMAdminAcceptanceTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;

namespace AOMAdminAcceptanceTests.Pages
{
    public class AuthenticationHistoryPage : BasePage
    {
        public AuthenticationHistoryPage()
        {
            Assert.AreEqual("Authentication History", Driver.Title, "You are not on Authentication History Page");
            new SeleniumHelpers().WaitUntilElementIsDisplayed(_table, 30);
        }

#pragma warning disable 649

        [FindsBy(How = How.ClassName, Using = "DDFilters")]
        private IWebElement _filterContainer;

        private ReadOnlyCollection<IWebElement> _allSelects => _filterContainer.FindElements(By.TagName("select"));

        [FindsBy(How = How.ClassName, Using = "DDSubHeader")]
        private IWebElement _header;

        [FindsBy(How = How.Id, Using = "GridView1")]
        private IWebElement _noitemstext;

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_txbAuthHistorySearch")]
        private IWebElement _search;

        private SelectElement _successfullogin => new SelectElement(_allSelects.First(x => x.GetAttribute("id").Contains("ddlSuccessfulLogin")));

        [FindsBy(How = How.CssSelector, Using = "#GridView1 .th>th")]
        private IList<IWebElement> _authHistoryTableHeader;

        [FindsBy(How = How.Id, Using = "GridView1")]
        private IWebElement _table;

        [FindsBy(How = How.CssSelector, Using = ".th>th")]
        private IWebElement _tabheader;

#pragma warning restore 649

        public string GetHeaderText()
        {
            return _header.Text;
        }

        public string GetNoRowsFoundText()
        {
            return _noitemstext.Text;
        }

        public AuthenticationHistoryPage SelectSuccessfulLoginOption(string text)
        {
            _successfullogin.SelectByText(text);
            return this;
        }

        public AuthenticationHistoryPage EnterSearch(string searchText)
        {
            _search.SendKeys(searchText);
            _search.SendKeys(Keys.Enter);
            return new AuthenticationHistoryPage();
        }

        public DataTable GetAuthHistoryTable()
        {
            var historyTable = new DataTable();
            if (new SeleniumHelpers().IsElementPresent(_tabheader))
            {
                // Get allTable headers
                foreach (var tableHeaders in _authHistoryTableHeader)
                {
                    historyTable.Columns.Add(tableHeaders.Text);
                }

                // Get all rows and copy into data table
                var rows = Driver.FindElement(By.Id("GridView1"));
                var authrows = rows.FindElements(By.TagName("tr"));

                var maxrows = authrows.Count - 1;
                //Only check first 20 rows
                if (maxrows > 20)
                {
                    maxrows = 20;
                }

                for (int i = 1; i <= maxrows; i++)
                {
                    var historyTableRow = historyTable.NewRow();
                    int j = 0;
                    var elementsinrow = authrows[i].FindElements(By.TagName("td"));

                    if (authrows[i].FindElement(By.TagName("input")).GetAttribute("checked") == "true")
                        historyTableRow["SuccessfulLogin"] = 1;
                    else
                        historyTableRow["SuccessfulLogin"] = 0;

                    foreach (DataColumn column in historyTable.Columns)
                    {
                        if (!column.ColumnName.Equals("SuccessfulLogin"))
                        {
                            historyTableRow[column] = elementsinrow[j].Text.Trim();
                        }
                        j++;
                    }
                    historyTable.Rows.Add(historyTableRow);
                }
            }

            return historyTable;
        }

        public DataTable GetLoginsforUserFromDb(string user)
        {
            var sqlhelper = new SqlHelper();

            var sql = string.Format(@"SELECT Username, trim(LoginFailureReason),SuccessfulLogin,IpAddress,date_format(DateCreated,'%c/%e/%Y %l:%i:%s %p')
            FROM `crm.{0}`.AuthenticationHistory
            Where username = '{1}'
            Order by DateCreated desc;", sqlhelper._tablePrefix, user);
            return sqlhelper.ExecuteSql(sql);
        }
    }
}