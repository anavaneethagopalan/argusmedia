﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMAdminAcceptanceTests.Helpers.Context
{
    public class InsertUpdateUserContext
    {
        public string Name;
        public string Title;
        public string Email;
        public string Telephone;
        public string UserName;
        public string ArgusCrmUserName;
        public string Organisation;
        public bool Active;
        public bool Blocked;
        public bool Deleted;
    }
}