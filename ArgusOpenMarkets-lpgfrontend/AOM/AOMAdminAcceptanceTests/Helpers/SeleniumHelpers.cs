﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace AOMAdminAcceptanceTests.Helpers
{
    public class SeleniumHelpers
    {
        public static readonly TimeSpan NoWait = new TimeSpan(0, 0, 0, 0);
        private readonly IWebDriver _driver = DriverManager.Driver;

        public void WaitForPageToLoad(int timespan)
        {
            var wait = new WebDriverWait(_driver, new TimeSpan(0, 0, timespan));
            try
            {
                wait.Until(
                    d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").ToString()
                        .Equals("complete"));
            }
            catch (UnhandledAlertException)
            {
                _driver.SwitchTo().Alert().Accept();
                wait.Until(
                    d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").ToString()
                        .Equals("complete"));
            }
        }

        public void WaitUntilElementIsDisplayed(IWebElement element, int timeinsec)
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(timeinsec));
            wait.Until(d => element.Displayed);
        }

        public void SelectElementByText(IWebElement element, string text)
        {
            var selectelement = new SelectElement(element);
            selectelement.SelectByText(text);
        }

        public string GetCurrentUrl()
        {
            return _driver.Url;
        }

        public bool IsElementPresent(IWebElement element)
        {
            _driver.Manage().Timeouts().ImplicitWait = NoWait;
            try
            {
                return element.Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            finally
            {
                _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            }
        }
    }
}