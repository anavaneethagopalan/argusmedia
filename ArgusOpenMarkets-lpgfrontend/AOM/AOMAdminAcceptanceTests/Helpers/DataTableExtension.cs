﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;

namespace AOMAdminAcceptanceTests.Helpers
{
    public static class DataTableExtension
    {
        public static void CompareRows(this DataTable actualDataTable, DataTable expectedDataTable)
        {
            int i = 0;
            foreach (DataRow row in actualDataTable.Rows)
            {
                int j = 0;
                foreach (DataColumn column in actualDataTable.Columns)
                {
                    Assert.AreEqual(row[column].ToString().Trim(), expectedDataTable.Rows[i][j].ToString().Trim());
                    j++;
                }
                i++;
            }
        }
    }
}