﻿using AOMAdminAcceptanceTests.Helpers.Context;
using AOMAdminAcceptanceTests.Pages;
using OpenQA.Selenium;
using System.Configuration;
using System.Linq;
using TechTalk.SpecFlow;

namespace AOMAdminAcceptanceTests.Helpers
{
    public class NavigateTo
    {
#pragma warning disable 169
        private readonly string _basepage = ConfigurationManager.AppSettings["TestUrl"];
        private readonly string _loginpage = "/Account/Login";
        private readonly string _authenticationhistorypage = "/Admin/AuthenticationHistories/List.aspx";
        private readonly string _usermanagementpage = "/Admin/UserInfoes/List.aspx";
        private readonly string _insertnewuserpage = "/Admin/UserInfoes/Insert.aspx";
        private readonly string _logoutForm = "logoutForm";
        private readonly string _home = "/home";
        private string _adminusername = ConfigurationManager.AppSettings["Adminuser"];
        private string _adminpassword = ConfigurationManager.AppSettings["Adminpassword"];
        private string _helpdeskusername = ConfigurationManager.AppSettings["helpdeskuser"];
        private string _helpdeskpassword = ConfigurationManager.AppSettings["helpdeskpassword"];
        private string _editUserurl = "/Admin/UserInfoes/Edit.aspx?Id=";

        private string _changepasswordurl = "/Account/ChangeUserPassword?userId=";

#pragma warning restore 169

        public NavigateTo(bool logmein = true)
        {
            if (UserContext.Username != null && !DriverManager.Driver.Url.Contains(_loginpage) && DriverManager.Driver.Url.Contains(_basepage))
            {
                var logout = DriverManager.Driver.FindElement(By.Id(_logoutForm));
                logout.Submit();
            }
            else
            {
                NavigateToLoginPage();
            }

            if (logmein)
            {
                if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("helpdeskUser"))
                {
                    NavigateToLoginPage();
                    new LoginPage().EnterLoginDetails(_helpdeskusername, _helpdeskpassword);
                }
                else
                {
                    new LoginPage().EnterLoginDetails(_adminusername, _adminpassword);
                }
            }
        }

        public void NavigateToLoginPage()
        {
            DriverManager.Driver.Navigate().GoToUrl(_basepage + _loginpage);
        }

        public void NavigateToAuthenticationHistoryPage()
        {
            DriverManager.Driver.Navigate().GoToUrl(_basepage + _authenticationhistorypage);
        }

        public void NavigateToUserManagementPage()
        {
            DriverManager.Driver.Navigate().GoToUrl(_basepage + _usermanagementpage);
        }

        public void NavigateToInsertNewUser()
        {
            DriverManager.Driver.Navigate().GoToUrl(_basepage + _insertnewuserpage);
        }

        public void NavigateToHomePage()
        {
            DriverManager.Driver.Navigate().GoToUrl(_basepage + _home);
        }

        public void NavigateToEditUserPage(string username)
        {
            DriverManager.Driver.Navigate().GoToUrl(_basepage + _editUserurl + new SqlHelper().GetUserIdFromUserName(username));
        }

        public void NavigateToChangePasswordPage(string username)
        {
            DriverManager.Driver.Navigate().GoToUrl(_basepage+_changepasswordurl+new SqlHelper().GetUserIdFromUserName(username));
        }
    }
}