﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Configuration;

namespace AOMAdminAcceptanceTests.Helpers
{
    public class DriverManager
    {
        public static IWebDriver Driver;

        public void SetDriver()
        {
            if (Driver == null)
            {
                switch (ConfigurationManager.AppSettings["Browser"])
                {
                    case "IE":
                        var ieoptions = new InternetExplorerOptions();
                        ieoptions.EnsureCleanSession = true;
                        ieoptions.IgnoreZoomLevel = true;
                        ieoptions.EnablePersistentHover = false;
                        ieoptions.EnableNativeEvents = false;
                        Driver = new InternetExplorerDriver(ieoptions);
                        Console.WriteLine("STARTED ON -- IE");
                        break;

                    case "Firefox":
                        Driver = new FirefoxDriver();
                        Console.WriteLine("STARTED ON -- FIREFOX");
                        break;

                    case "Chrome":
                        var chromeoptions = new ChromeOptions();
                        Driver = new ChromeDriver(chromeoptions);
                        Console.WriteLine("STARTED ON -- CHROME");
                        break;

                    case "Edge":
                        Driver = new EdgeDriver();
                        break;

                    default:
                        throw new Exception("Driver Not set Properly");
                }
                Driver.Manage().Window.Maximize();
                Driver.Manage().Cookies.DeleteAllCookies();
                Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            }
        }

        public void ExitDriver()
        {
            if (Driver != null)
            {
                Console.WriteLine("Exiting Driver ......");
                Driver.Quit();
                Driver.Dispose();
                Driver = null;
            }
        }
    }
}