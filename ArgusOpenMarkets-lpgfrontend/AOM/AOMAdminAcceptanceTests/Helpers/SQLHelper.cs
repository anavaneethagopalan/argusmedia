﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;

namespace AOMAdminAcceptanceTests.Helpers
{
    public class SqlHelper
    {
        private readonly string _environmentAppSetting = ConfigurationManager.AppSettings["Environment"];

        public string _tablePrefix = ConfigurationManager.AppSettings["TablePrefix"];
        private readonly MySqlConnection _con;

        public MySqlConnection aomsqlConnection =
            new MySqlConnection(ConfigurationManager.ConnectionStrings["aom"].ToString());

        public SqlHelper()
        {
            _con = aomsqlConnection;
            _con.Open();
        }

        private object RunScalarSql(string sqlquery)
        {
            MySqlCommand command = new MySqlCommand(sqlquery, _con);
            return command.ExecuteScalar();
        }

        public void ExecuteNonQuery(string sqlquery)
        {
            MySqlCommand command = new MySqlCommand(sqlquery, _con);
            command.ExecuteNonQuery();
        }

        public DataTable ExecuteSql(string sqlquery)
        {
            var results = new DataTable();
            using (MySqlCommand command = new MySqlCommand(sqlquery, _con))
            {
                using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command))
                {
                    dataAdapter.Fill(results);
                }
            }

            return results;
        }

        public void DeleteUser(string username)
        {
            //Doing this in stages as want to be safe deleting
            var userinfoId = GetUserIdFromUserName(username);
            if (userinfoId == null)
            {
                return;
            }
            var sqlquery = $@"
                             DELETE ut.* FROM `crm.{_tablePrefix}`.UserToken ut WHERE ut.UserInfo_Id_fk = {userinfoId};
                             DELETE uc.* FROM `crm.{_tablePrefix}`.UserCredentials uc WHERE uc.UserInfo_Id_fk = {userinfoId};
                             DELETE ula.* FROM `crm.{_tablePrefix}`.UserLoginAttempts ula WHERE ula.UserInfo_Id_fk = {userinfoId};
                             DELETE uir.* FROM `crm.{_tablePrefix}`.UserInfoRole uir WHERE uir.UserInfo_Id_fk = {userinfoId};
                             DELETE ui.* FROM `crm.{_tablePrefix}`.UserInfo ui WHERE ui.id = {userinfoId};
                             ";

            ExecuteNonQuery(sqlquery);
        }

        public int? GetUserIdFromUserName(string username)
        {
            var sqlquery = $@"SELECT id
                            FROM `crm.{_tablePrefix}`.UserInfo ui
                            WHERE ui.Username = '{username}'; ";
            return (int?)Convert.ToInt32(RunScalarSql(sqlquery));
        }

        public bool GetCurrentUserBlockedStatus(string username)
        {
            var sqlquery = $@"SELECT isBlocked
                            FROM `crm.{_tablePrefix}`.UserInfo ui
                            WHERE ui.Username = '{username}'; ";
            return (bool)Convert.ToBoolean(RunScalarSql(sqlquery));
        }

        public void UpdateCurrentUserBlockedStatus(string username, bool isBlocked)
        {
            var sqlquery = $@"Update `crm.{_tablePrefix}`.UserInfo
                            Set isBlocked = {isBlocked}
                            WHERE Username = '{username}'; ";
            ExecuteNonQuery(sqlquery);
        }

        public string GetUserCredentials(string username, string TempOrPerm)
        {
            var sqlquery = $@"SELECT CredentialType FROM `crm.{_tablePrefix}`.UserCredentials
                            where userinfo_id_fk = (SELECT id FROM `crm.{_tablePrefix}`.UserInfo
                                                  where Username = '{username}')
                            and CredentialType = '{TempOrPerm}';";

            return (string)(RunScalarSql(sqlquery));
        }
    }
}