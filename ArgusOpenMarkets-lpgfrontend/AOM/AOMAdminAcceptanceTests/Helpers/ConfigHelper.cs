﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMAdminAcceptanceTests.Helpers
{
    public class ConfigHelper
    {
        public string TestUrl => GetVariable("TestUrl");

        public string Browser => GetVariable("Browser");

        private static string GetVariable(string name)
        {
            var environmentValue = Environment.GetEnvironmentVariable(name);
            if (environmentValue != null)
            {
                return environmentValue;
            }
            return ConfigurationManager.AppSettings[name];
        }
    }
}