﻿Feature: Login


Scenario Outline: 01.Login to AOM Admin with correct credentials
	Given I navigate to the loginPage
	When I  enter "<username>" and "<password>" and click login
	Then i should be see my "<username>" on home page	
Examples:
 | username | password   |
 | helpdesk | Password12 |
 | AE1      | password   |


Scenario Outline: 02.Login to AOM Admin with blank username
	Given I navigate to the loginPage
	When I  enter "<username>" and "<password>" 
	And I click login
	Then i should see "The User name field is required." on the screen	
Examples:
| username | password   |
|          | Password12 |


Scenario Outline: 03.Login to AOM Admin with blank password
	Given I navigate to the loginPage
	When I  enter "<username>" and "<password>" 
	And I click login
	Then i should see "The Password field is required." on the screen	
Examples:
| username | password |
| helpdesk |          |



Scenario Outline: 04.Login to AOM Admin with blank password
	Given I navigate to the loginPage
	When I  enter "<username>" and "<password>" 
	And I click login
	Then i should see login error message on the screen	
Examples:
| username | password |
| sdf      | xyz      |
