﻿Feature: UserManagement


@helpdeskUser
Scenario Outline: 01.An Helpdesk user should be able to retrieve a list of active users in an Organisation
Given I am on User Management Page
When I search for an active "<Active>" organisation "<Organisation>" 
Then I check the database matches the "<Active>" active results returned for organisation "<Organisation>" for Helpdesk User on the User Management page
Examples:
|  Organisation | Active |
|  Glencore     | True   |
|  Load Test 56 | False  |
|  Shell        | False  |

@helpdeskUser
Scenario Outline: 02.An Helpdesk user should be able to retrieve a list of blocked users in an Organisation
Given I am on User Management Page
When I search for an blocked "<Blocked>" organisation "<Organisation>" 
Then I check the database matches the "<Blocked>" blocked results returned for organisation "<Organisation>" for Helpdesk User on the User Management page
Examples:
 | Organisation | Blocked |
 | Glencore     | False   |
 | Shell        | True    |
 | Vitol        | False   |

@helpdeskUser
Scenario Outline: 03.An Helpdesk user should be able to retrieve a list of deleted users in an Organisations
Given I am on User Management Page
When I search for an deleted "<Deleted>" organisation "<Organisation>" 
Then I check the database matches the "<Deleted>" Deleted results returned for organisation "<Organisation>" for Helpdesk User on the User Management page
Examples:
| Deleted | Organisation |
| False   | Glencore     |
| True    | Shell        |

@helpdeskUser
Scenario Outline: 04.An Helpdesk user should be able to retrieve a list of users based on their username
Given I am on User Management Page
When I search for an user "<searchuser>"
Then I check the database matches the "<searchuser>" username results returned for User Management for Helpdesk User
Examples:
| searchuser |
| UA14       |

@helpdeskUser
Scenario Outline: 05.An Helpdesk user should be able to retrieve a list of users based on their name
Given I am on User Management Page
When I search for an user "<searchuser>"
Then I check the database matches the "<searchuser>" name results returned for User Management for Helpdesk User
Examples:
| searchuser        |
| UAT Trader User14 |

@helpdeskUser
Scenario Outline: 06.An Helpdesk user should be able to retrieve a list of users based on their email
Given I am on User Management Page
When I search for an user "<searchuser>"
Then I check the database matches the "<searchuser>" email results returned for User Management for Helpdesk User
Examples:
| searchuser          |
| la04@argusmedia.com |


Scenario: 07. An Admin User, the insert new user link on User Management page takes me to the Insert User Page
Given I am on User Management Page
When I click on Insert new Item
Then I expect to be on the Insert User Page

Scenario: 08. An Admin User, can insert a new user
Given I am on Insert new User Page
When I insert a new user
| Name     | Title | Email                  | Telephone   | User Name | ArgusCrmUserName | Organisation | Active | Blocked | Deleted |
| New User | Mr    | newuser@argusmedia.com | 01245631247 | NU1       | New User         | Argus Media  | True   | False   | False   |
Then I expect the new user to be created in the database 

@helpdeskUser
Scenario Outline: 09. A helpdesk user can block and unblock a user
Given the user "<UserName>" is unblocked
And I am on Edit user page for user "<UserName>"
When I Block the user
Then I expect the user to be blocked in the database
#And I _log table is update
Examples: 
| UserName |
| UA14     |

@helpdeskUser
Scenario Outline: 10. A helpdesk user can change temporary password for an user
Given I am on change password page for an user "<username>"
When I set the temporary password '<password>' for an user
Then the user credentials should be updated in the database as T
Examples: 
| username | password |
| ae1_old  | Uo46VNe0 |


@helpdeskUser
Scenario Outline: 11. A helpdesk user can change permanent password for an user
Given I am on change password page for an user "<username>"
When I set the permanent password for an user
Then the user credentials should be updated in the database as P
Examples: 
| username        |
| aomdevdashboard |

