﻿Feature: Home

@helpdeskUser
Scenario: 01.Helpdesk user should be able to see user management pages
Given I am on home page as a helpdesk user
Then I should be able to see Authentication History and User Management links


Scenario: 02.Admin user should be able to see all the user management pages
Given I am on home page as a admin user
Then I should be able to see all the user management links


@helpdeskUser
Scenario: 03.Helpdesk user should be able to browse to Authentication history
Given I am on home page as a helpdesk user
When i click on Authentication History link
Then I should be on Authentication History Page

@helpdeskUser
Scenario: 04.Helpdesk user should be able to browse to User Management
Given I am on home page as a helpdesk user
When i click on User Management link
Then I should be on User Management Page



