﻿Feature: AuthenticationHistory

@helpdeskUser
Scenario Outline: 01.An Helpdesk user should be able to view Authentication History for an user
Given I am on Authentication History Page
When I search history for "<searchuser>" for "<successfullogin>" successful Login
Then I check the database matches the "<successfullogin>" results returned for user "<searchuser>"

Examples:
| successfullogin | searchuser |
| All             | UA14       |
| True            | UA14       |
| False           | UA14       |


@helpdeskUser
Scenario: 02.Verify Back to home page works on Authentication History
Given I am on Authentication History Page
When I Click on Back to Home Page
Then I should be on home Page