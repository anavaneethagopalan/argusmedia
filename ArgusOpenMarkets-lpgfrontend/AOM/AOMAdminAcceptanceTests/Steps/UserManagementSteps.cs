﻿using AOMAdminAcceptanceTests.Helpers;
using AOMAdminAcceptanceTests.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Linq;
using AOMAdminAcceptanceTests.Helpers.Context;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace AOMAdminAcceptanceTests.Steps
{
    [Binding]
    public class UserManagementSteps
    {
        private InsertUpdateUserContext _insertuserContext;

        public UserManagementSteps(InsertUpdateUserContext InsertuserContext)
        {
            _insertuserContext = InsertuserContext;
        }

        [Given(@"I am on User Management Page")]
        public void GivenIAmOnUserManagementPage()
        {
            new NavigateTo().NavigateToUserManagementPage();
        }

        [When(@"I search for an active ""(.*)"" organisation ""(.*)""")]
        public void WhenISearchForAnActiveOrganisation(string active, string orgname)
        {
            var usrmgtpg = new UserManagementPage();
            usrmgtpg.SelectOrg(orgname);
            usrmgtpg.SelectActive(active);
        }

        [When(@"I search for an blocked ""(.*)"" organisation ""(.*)""")]
        public void WhenISearchForAnBlockedOrganisation(string blocked, string orgname)
        {
            var usrmgtpg = new UserManagementPage();
            usrmgtpg.SelectOrg(orgname);
            usrmgtpg.SelectBlocked(blocked);
        }

        [When(@"I search for an deleted ""(.*)"" organisation ""(.*)""")]
        public void WhenISearchForAnDeletedOrganisation(string deleted, string orgname)
        {
            var usrmgtpg = new UserManagementPage();
            usrmgtpg.SelectOrg(orgname);
            usrmgtpg.SelectDeleted(deleted);
        }

        [Then(@"I check the database matches the ""(.*)"" active results returned for organisation ""(.*)"" for Helpdesk User on the User Management page")]
        public void ThenICheckTheDatabaseMatchesTheResultsReturnedForOrganisationForUserManagement(string active, string orgname)
        {
            var rowFilter = $"Active = {Convert.ToBoolean(active)}";
            var rowFilter2 = $"Organisation = '{orgname}'";
            FilterOrgAndCompareTables(rowFilter, rowFilter2);
        }

        [Then(@"I check the database matches the ""(.*)"" blocked results returned for organisation ""(.*)"" for Helpdesk User on the User Management page")]
        public void ThenICheckTheDatabaseMatchesTheBlockedResultsReturnedForOrganisationForUserManagementForHelpdeskUser(string blocked, string orgname)
        {
            var rowFilter = $"Blocked = {Convert.ToBoolean(blocked)}";
            var rowFilter2 = $"Organisation = '{orgname}'";
            FilterOrgAndCompareTables(rowFilter, rowFilter2);
        }

        [Then(@"I check the database matches the ""(.*)"" Deleted results returned for organisation ""(.*)"" for Helpdesk User on the User Management page")]
        public void ThenICheckTheDatabaseMatchesTheDeletedResultsReturnedForOrganisationForUserManagementForHelpdeskUser(string deleted, string orgname)
        {
            var rowFilter = $"Deleted = {Convert.ToBoolean(deleted)}";
            var rowFilter2 = $"Organisation = '{orgname}'";
            FilterOrgAndCompareTables(rowFilter, rowFilter2);
        }

        private static void FilterOrgAndCompareTables(params string[] rowFilters)
        {
            var expecteddDataTable = new UserManagementPage().GetUserManagementFromDb();

            var rowfiltersjoined = string.Join(" AND ", rowFilters);
            DataView dv = new DataView(expecteddDataTable)
            {
                RowFilter = rowfiltersjoined
            };

            expecteddDataTable = dv.ToTable();

            var userManagementPage = new UserManagementPage();
            var actualDataTable = userManagementPage.GetOrgList();

            if (expecteddDataTable.Rows.Count == 0)
            {
                Assert.AreEqual("There are currently no items in this table.", userManagementPage.GetNoRowsFound());
                return;
            }

            //Remove the first column as is a link to another record
            actualDataTable.Columns.RemoveAt(0);

            actualDataTable.CompareRows(expecteddDataTable);
        }

        [Then(@"I check the database matches the ""(.*)"" username results returned for User Management for Helpdesk User")]
        public void ThenICheckTheDatabaseMatchesTheUsernameResultsReturnedForUserManagementForHelpdeskUser(string username)
        {
            var rowFilter = $"Username = '{username}'";
            FilterOrgAndCompareTables(rowFilter);
        }

        [Then(@"I check the database matches the ""(.*)"" name results returned for User Management for Helpdesk User")]
        public void ThenICheckTheDatabaseMatchesTheNameResultsReturnedForUserManagementForHelpdeskUser(string name)
        {
            var rowFilter = $"Name = '{name}'";
            FilterOrgAndCompareTables(rowFilter);
        }

        [Then(@"I check the database matches the ""(.*)"" email results returned for User Management for Helpdesk User")]
        public void ThenICheckTheDatabaseMatchesTheEmailResultsReturnedForUserManagementForHelpdeskUser(string email)
        {
            var rowFilter = $"Email = '{email}'";
            FilterOrgAndCompareTables(rowFilter);
        }

        [When(@"I search for an user ""(.*)""")]
        public void WhenISearchForAnUserTest(string searchText)
        {
            var page = new UserManagementPage();
            page.SearchDetails(searchText);
        }

        [When(@"I click on Insert new Item")]
        public void WhenIClickOnInsertNewItem()
        {
            var page = new UserManagementPage();
            page.ClickonInsertUserLink();
        }

        [Then(@"I expect to be on the Insert User Page")]
        public void ThenIExpectToBeOnTheInsertUserPage()
        {
            new CreateUserPage();
        }

        [Given(@"I am on Insert new User Page")]
        public void GivenIAmOnInsertNewUserPage()
        {
            new NavigateTo().NavigateToInsertNewUser();
        }

        [Given(@"I don't have user ""(.*)"" created")]
        public void GivenIDonTHaveUserCreated(string username)
        {
            var deleteExistingUser = new SqlHelper();
            deleteExistingUser.DeleteUser(username);
        }

        [When(@"I insert a new user")]
        public void WhenIInsertANewUserCalled(Table userTable)
        {
            _insertuserContext = userTable.CreateInstance<InsertUpdateUserContext>();
            // Delete user if it exists in databae
            var deleteExistingUser = new SqlHelper();
            deleteExistingUser.DeleteUser(_insertuserContext.UserName);

            var createUserPage = new CreateUserPage();
            createUserPage.InsertNewUser(_insertuserContext);
        }

        [Then(@"I expect the new user to be created in the database")]
        public void ThenIExpectTheNewUserToBeCreatedInTheDatabase()
        {
            var sqlHelper = new SqlHelper();
            var id = sqlHelper.GetUserIdFromUserName(_insertuserContext.UserName);
            Assert.AreNotEqual(id, 0, $" Username {_insertuserContext.UserName} not found in database");
        }

        [Given(@"the user ""(.*)"" is unblocked")]
        public void GivenTheUserIsUnblocked(string username)
        {
            var sqlhelper = new SqlHelper();
            sqlhelper.UpdateCurrentUserBlockedStatus(username, false);
        }

        [Given(@"I am on Edit user page for user ""(.*)""")]
        public void GivenIAmOnEditUserPageForUser(string username)
        {
            _insertuserContext.UserName = username;
            new NavigateTo().NavigateToEditUserPage(username);
        }

        [When(@"I Block the user")]
        public void WhenIBlockTheUser()
        {
            new EditUserPage().BlockUser();
        }

        [Then(@"I expect the user to be blocked in the database")]
        public void ThenIExpectTheUserToBeBlockedInTheDatabase()
        {
            var sqlhelper = new SqlHelper();
            var blocked = sqlhelper.GetCurrentUserBlockedStatus(_insertuserContext.UserName);
            Assert.IsTrue(blocked);
        }

        [Given(@"I am on change password page for an user ""(.*)""")]
        public void GivenIAmOnChangePasswordPageForAnUser(string username)
        {
            _insertuserContext.UserName = username;
            new NavigateTo().NavigateToChangePasswordPage(username);
        }

        [When(@"I set the temporary password '(.*)' for an user")]
        public void WhenISetTheTemporaryPasswordForAnUser(string password)
        {
            var page = new ChangePasswordPage();
            page.SetTempPassword(password, false);
        }

        [Then(@"the user credentials should be updated in the database as (.*)")]
        public void ThenTheUserCredentialsShouldBeUpdatedInTheDatabase(string type)
        {
            var sqlhelper = new SqlHelper();
            var expected = type;

            var actual = sqlhelper.GetUserCredentials(_insertuserContext.UserName, type);
            Assert.AreEqual(expected, actual, "Password not set");
        }

        [When(@"I set the permanent password for an user")]
        public void WhenISetThePermanentPasswordForAnUser()
        {
            var page = new ChangePasswordPage();
            page.SetPermanentPassword(true);
        }
    }
}