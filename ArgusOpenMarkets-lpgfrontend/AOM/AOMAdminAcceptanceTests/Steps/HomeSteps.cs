﻿using System;
using AOMAdminAcceptanceTests.Helpers.Context;
using AOMAdminAcceptanceTests.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using System.Linq;
using AOMAdminAcceptanceTests.Helpers;
using TechTalk.SpecFlow;

namespace AOMAdminAcceptanceTests.Steps
{
    [Binding]
    public sealed class HomeSteps
    {
        [Given(@"I am on home page as a helpdesk user")]
        public void GivenIAmOnHomePageAsAHelpdeskUser()
        {
            new NavigateTo().NavigateToHomePage();
        }

        [Given(@"I am on home page as a admin user")]
        public void GivenIAmOnHomePageAsAAdminUser()
        {
            new NavigateTo().NavigateToHomePage();
        }

        [Then(@"I should be able to see Authentication History and User Management links")]
        public void ThenIShouldBeAbleToSeeAuthenticationHistoryAndUserManagementLinks()
        {
            var linktext = "Authentication History,User Management";
            var homepg = new HomePage();
            var expected = linktext.Split(',').ToList();
            var actual = homepg.GetLinks();
            CollectionAssert.AreEqual(expected, actual, "User cannot see the links: " + expected);
        }

        [Then(@"I should be able to see all the user management links")]
        public void ThenIShouldBeAbleToSeeAllTheUserManagementLinks()
        {
            var linktext = "Authentication History,Organisation Management,Organisation Role Management,System Privilege Management,User Management";
            var homepg = new HomePage();
            var expected = linktext.Split(',').ToList();
            var actual = homepg.GetLinks();
            CollectionAssert.AreEqual(expected, actual, "User cannot see the links: " + expected);
        }

        [When(@"i click on Authentication History link")]
        public void WhenIClickOnAuthenticationHistoryLink()
        {
            var homepg = new HomePage();
            homepg.ClickAutheHist();
        }

        [When(@"i click on User Management link")]
        public void WhenIClickOnUserManagementLink()
        {
            var homepg = new HomePage();
            homepg.ClickUserMgmt();
        }

        [Then(@"I should be on Authentication History Page")]
        public void ThenIShouldBeOnAuthenticationHistoryPage()
        {
            var authpg = new AuthenticationHistoryPage();
            var expected = "Authentication History";
            var actual = authpg.GetHeaderText();
            Assert.AreEqual(expected, actual, "You are not on " + expected + " page");
        }

        [Then(@"I should be on User Management Page")]
        public void ThenIShouldBeOnUserManagementPage()
        {
            var usrmgmtpg = new UserManagementPage();
            var expected = "User Management";
            var actual = usrmgmtpg.GetHeaderText();
            Assert.AreEqual(expected, actual, "You are not on " + expected + " page");
        }
    }
}