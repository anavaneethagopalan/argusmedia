﻿using AOMAdminAcceptanceTests.Helpers;
using AOMAdminAcceptanceTests.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using TechTalk.SpecFlow;

namespace AOMAdminAcceptanceTests.Steps
{
    [Binding]
    public class AuthenticationHistorySteps
    {
        [Given(@"I am on Authentication History Page")]
        [When(@"I navigate to Authentication History Page")]
        public void WhenINavigateToAuthenticationHistoryPage()
        {
            new NavigateTo().NavigateToAuthenticationHistoryPage();
        }

        [When(@"I search history for ""(.*)"" for ""(.*)"" successful Login")]
        public void WhenISearchHistoryForForSuccessfulLogin(string searchText, string successvalue)
        {
            var authpg = new AuthenticationHistoryPage();
            authpg.SelectSuccessfulLoginOption(successvalue);
            authpg.EnterSearch(searchText);
        }

        [Then(@"I should see all ""(.*)"" logins for user ""(.*)""")]
        public void ThenIShouldSeeAllLoginsForUser(string successvalue, string searchText)
        {
            var page = new AuthenticationHistoryPage();
            var table = page.GetAuthHistoryTable();
            var text = "There are currently no items in this table.";
            int i = 0;

            Console.WriteLine("Count:" + table.Rows.Count);
            if (table.Rows.Count != 0)
            {
                foreach (var unused in table.Rows)
                {
                    Assert.AreEqual(successvalue.ToUpper(), table.Rows[i]["SuccessfulLogin"].ToString().ToUpper(), "Successful login list is not correct");
                    Assert.AreEqual(searchText.ToUpper(), table.Rows[i]["Username"].ToString().ToUpper(), "Successful login list is not correct");
                    i++;
                }
            }
            else
            {
                Assert.AreEqual(text, page.GetNoRowsFoundText(), "There are items in the table");
            }
        }

        [When(@"I Click on Back to Home Page")]
        public void WhenIClickOnBackToHomePage()
        {
            var page = new AuthenticationHistoryPage();
            page.ClickOnHomePage();
        }

        [Then(@"I check the database matches the ""(.*)"" results returned for user ""(.*)""")]
        public void ThenICheckTheDatabaseMatchesTheResultsReturnedForUser(string successfulLogin, string user)
        {
            var authhistpage = new AuthenticationHistoryPage();
            var expecteddDataTable = authhistpage.GetLoginsforUserFromDb(user);
            DataView dv = new DataView(expecteddDataTable);
            var actualDataTable = authhistpage.GetAuthHistoryTable();

            if (!string.Equals(successfulLogin, "all", StringComparison.CurrentCultureIgnoreCase))
            {
                dv.RowFilter = $"SuccessfulLogin = {Convert.ToBoolean(successfulLogin)}";
                expecteddDataTable = dv.ToTable();
            }

            if (expecteddDataTable.Rows.Count == 0)
            {
                Assert.AreEqual("There are currently no items in this table.", authhistpage.GetNoRowsFoundText());
                return;
            }

            //Remove the first column as is a link to another record
            actualDataTable.Columns.RemoveAt(0);
            actualDataTable.CompareRows(expecteddDataTable);
        }

        [Then(@"I should be on home Page")]
        public void ThenIShouldBeOnHomePage()
        {
            new HomePage();
            /* var expected = ConfigurationManager.AppSettings["TestUrl"].ToString() +"/";
             var helper = new SeleniumHelpers();
             var actual = helper.GetCurrentUrl();
             Assert.AreEqual(expected, actual, "Back to Home page does not return to Home page");
             */
        }
    }
}