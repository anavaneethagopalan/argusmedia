﻿using System.Configuration;
using AOMAdminAcceptanceTests.Helpers;
using AOMAdminAcceptanceTests.Helpers.Context;
using AOMAdminAcceptanceTests.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TechTalk.SpecFlow;

namespace AOMAdminAcceptanceTests.Steps
{
    [Binding]
    public class LoginSteps
    {
        private readonly string _basepage = ConfigurationManager.AppSettings["TestUrl"];

        [Given(@"I navigate to the loginPage")]
        public void GivenINavigateToTheLoginPage()
        {
            new NavigateTo(false).NavigateToLoginPage();
        }

        [When(@"I  enter ""(.*)"" and ""(.*)"" and click login")]
        public void WhenIEnterAndAndClickLogin(string username, string password)
        {
            var loginpg = new LoginPage();
            loginpg.EnterLoginDetails(username, password);// returns homepage object
        }

        [Then(@"i should be see my ""(.*)"" on home page")]
        public void ThenIShouldBeSeeMyOnHomePage(string username)
        {
            var homepg = new HomePage();
            var expected = "Hello " + username + "!";
            var actual = homepg.GetUserText();
            Assert.AreEqual(expected, actual, "Logged in user is not correct");
        }

        [Then(@"i should see ""(.*)"" on the screen")]
        public void ThenIShouldBeSeeOnTheScreen(string errormsg)
        {
            var loginpg = new LoginPage();
            var expected = errormsg;
            var actual = loginpg.ErrorMsg();
            Assert.AreEqual(expected, actual, "Incorrect error message for invalid login");
        }

        [When(@"I  enter ""(.*)"" and ""(.*)""")]
        public void WhenIEnterPassword(string username, string password)
        {
            var loginpg = new LoginPage();
            loginpg.EnterDetails(username, password);
        }

        [When(@"I click login")]
        public void WhenIClickLogin()
        {
            var loginpg = new LoginPage();
            loginpg.ClickLogin();
        }

        [Then(@"i should see login error message on the screen")]
        public void ThenIShouldSeeLoginErrorMessageOnTheScreen()
        {
            var loginpg = new LoginPage();
            var errormsg = "Username/password combination not found,Please contact support aomdevteam@argusmedia.com if the problem persists.";
            var expected = errormsg.Split(',').ToList();
            var actual = loginpg.GetLoginErrorMsg();
            CollectionAssert.AreEqual(expected, actual, "Incorrect login error message");
        }
    }
}