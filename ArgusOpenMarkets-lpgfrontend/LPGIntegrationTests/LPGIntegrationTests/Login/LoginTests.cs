﻿using System.Configuration;
using NUnit.Framework;
using RestSharp;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace LPGIntegrationTests.Login
{
    [TestFixture]
    public class LoginTests
    {
        //TODO: This should go into app.config file
        private readonly string _baseUrl = ConfigurationManager.AppSettings["BaseUrl"];

        private readonly string _testUser = ConfigurationManager.AppSettings["IntegrationTestUserName"];
        private readonly string _testPassword = ConfigurationManager.AppSettings["IntegrationTestPassword"];

        private readonly string _loginUrl = "Login";

        [Test]
        public void LoginSuccessful()
        {
            var loginresponse = LoginRequest(_testUser, _testPassword, "aom");
            Assert.IsTrue(loginresponse.IsAuthenticated, $"Authentication failed, response from service: {loginresponse.Message}");
        }

        [Test]
        public void LoginFailNoUserName()
        {
            var loginresponse = LoginRequest("", _testPassword, "aom");
            Assert.IsFalse(loginresponse.IsAuthenticated, $"Authentication failed, response from service: {loginresponse.Message}");
        }

        [Test]
        public void LoginFailNoPassword()
        {
            var loginresponse = LoginRequest(_testUser, "", "aom");
            Assert.IsFalse(loginresponse.IsAuthenticated, $"Authentication failed, response from service: {loginresponse.Message}");
        }

        [Test]
        public void LoginFailIncorrectPassword()
        {
            var loginresponse = LoginRequest(_testUser, "Incorrect", "aom");
            Assert.IsFalse(loginresponse.IsAuthenticated, $"Authentication failed, response from service: {loginresponse.Message}");
        }

        [Test]
        public void LoginFailBlankSource()
        {
            var loginresponse = LoginRequest(_testUser, _testPassword, "");
            Assert.IsFalse(loginresponse.IsAuthenticated, $"Authentication failed, response from service: {loginresponse.Message}");
        }

        [Test]
        public void SuccessfulLoginCreatesAuthHistory()
        {
            var authHistorySql = new SQLQueries.AuthenticationHistory();
            var previousauthcount = authHistorySql.GetAuthHistoryCount(_testUser);
            LoginSuccessful();
            var currentauthcount = authHistorySql.GetAuthHistoryCount(_testUser);
            Assert.IsTrue(previousauthcount < currentauthcount);
            var results = authHistorySql.GetLatestAuthHistory(_testUser);

            Assert.AreEqual(results.Rows[0]["UserName"].ToString(), _testUser);
            Assert.AreEqual(results.Rows[0]["SuccessfulLogin"].ToString(), "1");
            Assert.AreNotEqual(results.Rows[0]["IpAddress"].ToString(), "");
        }

        [Test]
        public void UnSuccessFulLoginCreatesAuthHistory()
        {
            var authHistorySql = new SQLQueries.AuthenticationHistory();
            var previousauthcount = authHistorySql.GetAuthHistoryCount(_testUser);
            LoginFailIncorrectPassword();
            var currentauthcount = authHistorySql.GetAuthHistoryCount(_testUser);
            Assert.IsTrue(previousauthcount < currentauthcount);
            var results = authHistorySql.GetLatestAuthHistory(_testUser);

            Assert.AreEqual(results.Rows[0]["UserName"].ToString(), _testUser);
            Assert.AreEqual(results.Rows[0]["SuccessfulLogin"].ToString(), "0");
            Assert.AreNotEqual(results.Rows[0]["IpAddress"].ToString(), "");
            Assert.AreEqual(results.Rows[0]["LoginFailureReason"].ToString(), $"Password incorrect for user {_testUser}. Denying ...");
        }

        private LoginResponse LoginRequest(string username, string password, string loginSource)
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest(_loginUrl, Method.POST);

            var loginrequest = new LoginRequest { Username = username, Password = password, LoginSource = loginSource };
            request.AddJsonBody(loginrequest);

            var response = client.Execute<LoginResponse>(request);
            return response.Data;
        }
    }
}