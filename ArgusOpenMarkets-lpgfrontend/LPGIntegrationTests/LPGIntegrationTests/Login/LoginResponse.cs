﻿namespace LPGIntegrationTests.Login
{
    public class LoginResponse
    {
        public string Token { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }
        public bool IsAuthenticated { get; set; }
        public string SessionToken { get; set; }
    }
}