﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace LPGIntegrationTests.Helpers
{
    public class SqlHelper
    {
        private readonly string _aomConnectionString = ConfigurationManager.ConnectionStrings["aom"].ToString();
        private readonly string _crmConnectionString = ConfigurationManager.ConnectionStrings["crm"].ToString();

        public DataTable ExecuteSqlAom(string sqlQuery, IEnumerable<MySqlParameter> parameters)
        {
            return ExecuteSql(_aomConnectionString, sqlQuery, parameters);
        }

        public DataTable ExecuteSqlCrm(string sqlQuery, IEnumerable<MySqlParameter> parameters)
        {
            return ExecuteSql(_crmConnectionString, sqlQuery, parameters);
        }

        public object RunScalarSqlAom(string sqlQuery, IEnumerable<MySqlParameter> parameters)
        {
            return RunScalarSql(_aomConnectionString, sqlQuery, parameters);
        }

        public object RunScalarSqlCrm(string sqlQuery, IEnumerable<MySqlParameter> parameters)
        {
            return RunScalarSql(_crmConnectionString, sqlQuery, parameters);
        }

        private DataTable ExecuteSql(string connectionString, string sqlQuery, IEnumerable<MySqlParameter> parameters)
        {
            var results = new DataTable();
            using (var con = new MySqlConnection(connectionString))
            {
                con.Open();
                using (var command = new MySqlCommand(sqlQuery, con))
                {
                    foreach (MySqlParameter param in parameters)
                    {
                        command.Parameters.Add(param);
                    }
                    using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command))
                    {
                        dataAdapter.Fill(results);
                    }
                }
            }
            return results;
        }

        private object RunScalarSql(string connectionString, string sqlQuery, IEnumerable<MySqlParameter> parameters)
        {
            using (var con = new MySqlConnection(connectionString))
            {
                con.Open();
                using (var command = new MySqlCommand(sqlQuery, con))
                {
                    foreach (MySqlParameter param in parameters)
                    {
                        command.Parameters.Add(param);
                    }
                    return command.ExecuteScalar();
                }
            }
        }      
    }
}