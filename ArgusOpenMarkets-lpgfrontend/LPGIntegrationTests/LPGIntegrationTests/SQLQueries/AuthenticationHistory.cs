﻿using System;
using System.Collections.Generic;
using System.Data;
using LPGIntegrationTests.Helpers;
using MySql.Data.MySqlClient;

namespace LPGIntegrationTests.SQLQueries
{
    public class AuthenticationHistory
    {
        public int GetAuthHistoryCount(string user)
        {
            const string sql = @"SELECT COUNT(*)
                                FROM AuthenticationHistory
                                WHERE username = @user";
            var parameters = new List<MySqlParameter>
            {
                new MySqlParameter("@user", user)
            };
            var sqlHelper = new SqlHelper();
            return Convert.ToInt32(sqlHelper.RunScalarSqlCrm(sql, parameters));
        }

        public DataTable GetLatestAuthHistory(string user)
        {
            const string sql = @"SELECT * FROM AuthenticationHistory
                                WHERE username = @user
                                ORDER BY id DESC
                                LIMIT 1;";
            var parameters = new List<MySqlParameter>
            {
                new MySqlParameter("@user", user)
            };
            var sqlHelper = new SqlHelper();
            return sqlHelper.ExecuteSqlCrm(sql, parameters);
        }
    }
}
