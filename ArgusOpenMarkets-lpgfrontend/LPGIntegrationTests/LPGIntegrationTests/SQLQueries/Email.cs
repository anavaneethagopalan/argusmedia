﻿using System;
using System.Collections.Generic;
using System.Data;
using LPGIntegrationTests.Helpers;
using MySql.Data.MySqlClient;

namespace LPGIntegrationTests.SQLQueries
{
    public class Email
    {
        public int GetEmailCount()
        {
            const string sql = @"SELECT COUNT(*)
                                FROM Email";
            var sqlHelper = new SqlHelper();
            return Convert.ToInt32(sqlHelper.RunScalarSqlAom(sql, new List<MySqlParameter>()));
        }

        public DataTable GetLatestEmail()
        {
            const string sql = @"SELECT * FROM Email
                                ORDER BY id DESC
                                LIMIT 1;";
            var sqlHelper = new SqlHelper();
            return sqlHelper.ExecuteSqlAom(sql, new List<MySqlParameter>());
        }
    }
}
