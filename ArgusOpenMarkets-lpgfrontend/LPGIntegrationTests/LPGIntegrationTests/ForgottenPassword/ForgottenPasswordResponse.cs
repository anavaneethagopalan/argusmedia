﻿namespace LPGIntegrationTests.ForgottenPassword
{
    public class ForgottenPasswordResponse
    {
        public bool IsAuthenticated { get; set; }
        public string Message { get; set; }
    }
}
