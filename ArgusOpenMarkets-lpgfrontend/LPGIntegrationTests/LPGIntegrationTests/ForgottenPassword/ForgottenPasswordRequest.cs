﻿namespace LPGIntegrationTests.ForgottenPassword
{
    public class ForgottenPasswordRequest
    {
        public string Username;
        public string Url;
    }
}
