﻿using System.Configuration;
using NUnit.Framework;
using RestSharp;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using StringAssert = Microsoft.VisualStudio.TestTools.UnitTesting.StringAssert;

namespace LPGIntegrationTests.ForgottenPassword
{
    [TestFixture]
    public class ForgottenPasswordTests
    {
        //TODO: This should go into app.config file
        private readonly string _baseUrl = ConfigurationManager.AppSettings["BaseUrl"];

        private readonly string _testUser = ConfigurationManager.AppSettings["IntegrationTestUserName"];

        private readonly string _fogottenPasswordUrl = "forgotPassword";

        [Test]
        public void ForgottenPasswordSucessful()
        {
            var forgottenPasswordResponse = ForgottenPasswordRequest(_testUser, _baseUrl);
            Assert.IsTrue(forgottenPasswordResponse.IsAuthenticated, $"Request failed, response from service: {forgottenPasswordResponse.Message}");
        }

        [Test]
        public void ForgottenPasswordNoUsername()
        {
            var forgottenPasswordResponse = ForgottenPasswordRequest("" , _baseUrl);
            Assert.IsFalse(forgottenPasswordResponse.IsAuthenticated, $"Request failed, response from service: {forgottenPasswordResponse.Message}");
        }

        [Test]
        public void ForgottenPasswordIncorrectUsername()
        {
            var forgottenPasswordResponse = ForgottenPasswordRequest("UsernameThatDoesNotExist", _baseUrl);
            Assert.IsFalse(forgottenPasswordResponse.IsAuthenticated, $"Request failed, response from service: {forgottenPasswordResponse.Message}");
        }

        [Test]
        public void ForgottenPasswordSuccessCreatesEmailRecord()
        {
            var emailSql = new SQLQueries.Email();
            var previousEmailCount = emailSql.GetEmailCount();
            ForgottenPasswordSucessful();
            var currentEmailCount = emailSql.GetEmailCount();
            Assert.IsTrue(previousEmailCount < currentEmailCount);
            var results = emailSql.GetLatestEmail();

            Assert.AreEqual(results.Rows[0]["Subject"].ToString(), "Reset Password - AOM");
            StringAssert.Contains(results.Rows[0]["Body"].ToString(), _baseUrl + "#/ResetPassword");            
        }

        private ForgottenPasswordResponse ForgottenPasswordRequest(string username, string url)
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest(_fogottenPasswordUrl, Method.POST);

            var forgottenPasswordRequest = new ForgottenPasswordRequest { Username = username, Url = url};
            request.AddJsonBody(forgottenPasswordRequest);

            var response = client.Execute<ForgottenPasswordResponse>(request);
            return response.Data;
        }
    }
}
