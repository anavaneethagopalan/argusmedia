function GetCredentials ($environment,$username,$password)
{    
    if ($environment -eq "local")
	{
		$username = [Environment]::UserDomainName + "\" + [Environment]::UserName
		$storedPassFile = "C:\securestring.txt"
		if (Test-Path $storedPassFile)
		{
			$pass = cat $storedPassFile | convertto-securestring 
			$credentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $pass
		}
		else
		{
        	$credentials = get-credential $username
		}
	}
	else
	{
	    if ($password -eq $null)
	    {
            $credentials = get-credential $username
	    }
	    else
	    {
	        $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force
	        $credentials = New-Object System.Management.Automation.PSCredential ($username, $secpasswd)
	    }
	}
    
    return $credentials
}

function ConnectToMachine ($environment,$apptype,$username,$password)
{    	
    $machineAddress = GetMachineAddress -environment $environment -apptype $apptype
    $credentials = GetCredentials -username $username -password $password
    return new-pssession -computername $machineAddress -port 5985 -credential $credentials
}

function DisconnectFromMachine ([System.Management.Automation.Runspaces.PSSession]$session)
{
    write-output "Disconnecting"
    remove-pssession -session $session
}

function GetMachineAddress ($environment,$apptype)
{
    if ($environment -eq "local")
    {
        $machineAddress = "local.directalerts.dev.argusmedia.com"
    }
    else
    {
        if ($environment -eq "live")
        {
			$machineAddress = $target
        }
        else
        {
            $machineAddress = "$environment.netsuite.dev.argusmedia.com"
        }
    }
    return $machineAddress
}

function CreateShare ([System.Management.Automation.Runspaces.PSSession]$session,$localFolder,$shareName)
{
    invoke-command{
    param([Parameter(Position=0)]$localFolder,[Parameter(Position=1)]$shareName)
    function Create-WMIAce{ 
        param([string]$account,[System.Security.AccessControl.FileSystemRights]$rights)
        $trustee = Create-WMITrustee $account 
        $ace = ([WMIClass] "Win32_ace").CreateInstance()  
        $ace.AccessMask = $rights  
        $ace.AceFlags = 0 # set inheritances and propagation flags 
        $ace.AceType = 0 # set SystemAudit  
        $ace.Trustee = $trustee  
        $ace 
    }

    function Create-WMITrustee([string]$NTAccount){ 
        $user = New-Object System.Security.Principal.NTAccount($NTAccount) 
        $strSID = $user.Translate([System.Security.Principal.SecurityIdentifier]) 
        $sid = New-Object security.principal.securityidentifier($strSID)  
        [byte[]]$ba = ,0 * $sid.BinaryLength      
        [void]$sid.GetBinaryForm($ba,0)  
     
        $Trustee = ([WMIClass] "Win32_Trustee").CreateInstance()  
        $Trustee.SID = $ba 
        $Trustee    
    }


    if (!(Test-Path $localFolder)) 
    { 
        Write-Output "Creating local directory $localFolder"
        New-Item $localFolder -type Directory 
    }  

    #$Shares=[WMICLASS]”WIN32_Share”


    if(!(GET-WMIOBJECT Win32_Share -filter “name='$shareName'” -ErrorAction Continue)){
       [System.Security.AccessControl.FileSystemRights]$rights='FullControl'
       [System.Security.Principal.NTAccount]$account="Admin"
       $sd = ([WMIClass] "Win32_SecurityDescriptor").CreateInstance()
       $ace=Create-WMIAce $account $rights
       $sd.DACL += @($ace.psobject.baseobject)
       $sd.ControlFlags="0x4"

        Write-Output "Sharing $localFolder as $shareName"
        $share = $Shares.Create($localFolder,$shareName,0,$null,"",$null,$sd)
        write-output $share
    }

    } -ErrorAction Stop -Verbose -session $session -ArgumentList $localFolder,$shareName

}

function DeleteLocalPrivateQueues([System.Management.Automation.Runspaces.PSSession]$session)
{
    invoke-command{

        $hostName = get-content env:computername

         Write-Output ("found host of {0}" -f $hostName)

        $machineIp = [System.Net.Dns]::GetHostAddresses($hostName)[0].IPAddressToString

        Write-Output ("found host IP of {0}" -f $machineIp)

        Write-Output "Deleting existing message queues"
        [Reflection.Assembly]::LoadWithPartialName("System.Messaging")

        $privateQueues = [System.Messaging.MessageQueue]::GetPrivateQueuesByMachine(".")

        foreach($queue in $privateQueues)
        {
            $queuePath=("FormatName:DIRECT=TCP:{0}\{1}" -f $machineIp, $queue.QueueName)
            Write-Output ("Deleting queue {0}" -f $queuePath)
            [System.Messaging.MessageQueue]::Delete($queuePath) 
        }
    }  -ErrorAction Stop -Verbose -session $session
}

function CreateDrive ($credentials,$driveLetter,$rootFolder)
{
    if (!(Get-PSDrive $driveLetter -ErrorAction SilentlyContinue))
    {
        New-PSDrive -Name $driveLetter -Credential $credentials -Root $rootFolder -PSProvider FileSystem -Scope Global
    }
}

function Decrypt-SecureString
{
    param([Parameter(ValueFromPipeline=$true,Mandatory=$true,Position=0)]
        [System.Security.SecureString]
        $sstr)

    $marshal = [System.Runtime.InteropServices.Marshal]
    $ptr = $marshal::SecureStringToBSTR( $sstr )
    $str = $marshal::PtrToStringBSTR( $ptr )
    $marshal::ZeroFreeBSTR( $ptr )
    return $str
}

function RemoteCopyFolderContents($source, $dest, $session, $noClean)
{
	If (!$noClean) {
	RemoteDirectoryClean $session $dest
	}
    Get-ChildItem -Recurse -Path $source | % {

        if ($_.PSIsContainer -eq $false)
        {
            $from = $_.FullName
            $to = $dest + "\" + $_.FullName.SubString($source.Length + 1)
			
			Send-Files $from $to $session
            Write-Output "Copying $from to $to"
        } else {
			$to = $dest + "\" + $_.FullName.SubString($source.Length + 1)
			RemoteDirectoryCreate $session $to
			Write-Output "Creating Folder Called $to"
		}
    
    }
}
function Send-Files
{
	param(
	    ## The path on the local computer
	    [Parameter(Mandatory = $true)]
	    $Source,

	    ## The target path on the remote computer
	    [Parameter(Mandatory = $true)]
	    $Destination,

	    ## The session that represents the remote computer
	    [Parameter(Mandatory = $true)]
	    [System.Management.Automation.Runspaces.PSSession] $Session
	)

	Set-StrictMode -Version Latest

	## Get the source file, and then get its content
	$sourcePath = (Resolve-Path $source).Path
	$sourceBytes = [IO.File]::ReadAllBytes($sourcePath)
	$streamChunks = @()

	## Now break it into chunks to stream
	$streamSize = 1MB
	for($position = 0; $position -lt $sourceBytes.Length;
	    $position += $streamSize)
	{
	    $remaining = $sourceBytes.Length - $position
	    $remaining = [Math]::Min($remaining, $streamSize)

	    $nextChunk = New-Object byte[] $remaining
	    [Array]::Copy($sourcebytes, $position, $nextChunk, 0, $remaining)
	    $streamChunks += ,$nextChunk
	}

	$remoteScript = {
	    param($destination, $length)

	    ## Convert the destination path to a full filesytem path (to support
	    ## relative paths)
	    $Destination = $executionContext.SessionState.`
	        Path.GetUnresolvedProviderPathFromPSPath($Destination)

	    ## Create a new array to hold the file content
	    $destBytes = New-Object byte[] $length
	    $position = 0

	    ## Go through the input, and fill in the new array of file content
	    foreach($chunk in $input)
	    {
	        [GC]::Collect()
	        [Array]::Copy($chunk, 0, $destBytes, $position, $chunk.Length)
	        $position += $chunk.Length
	    }

	    ## Write the content to the new file
	    [IO.File]::WriteAllBytes($destination, $destBytes)

	    ## Show the result
	    #Get-Item $destination
	    [GC]::Collect()
	}

	## Stream the chunks into the remote script
	$streamChunks | Invoke-Command -Session $session $remoteScript `
	    -ArgumentList $destination,$sourceBytes.Length
}

function RemoteDirectoryCreate($session, $path)
{
    $script = {
        param($path)
        $result = new-item $path -type directory -force
    }
    invoke-command -session $session -scriptblock $script -ArgumentList $path
}

function RemoteDirectoryClean($session, $path)
{
    $script = {
        param($path)
        $dir = $path + "\*"
        remove-item $dir -recurse -force
    }
    invoke-command -session $session -scriptblock $script -ArgumentList $path
}

function WebConfigConnectionStringUpdate($workingDir, $connectionString)
{
	$webConfigPath = "$workingDir\Src\Alerts.Service\Alerts.Service\web.config"
	# Get the content of the config file
	$xml = [xml](get-content $webConfigPath)
	#this was the trick I had been looking for
	$root = $xml.get_DocumentElement();
	 
	#Change the Connection String. Add really means "replace"
	$root.connectionStrings.add.connectionString = $connectionString
	 
	# Save it
	$xml.Save($webConfigPath)
}

function RemoteCreateAppPool($session,$siteName)
{
    invoke-command{
        param($siteName)
        import-module webadministration
        
        $appPoolExists = test-path iis:\AppPools\$siteName
        if ($appPoolExists -eq $True)
        {
            remove-item iis:\AppPools\$siteName -recurse
        }
        $appPool = new-item iis:\AppPools\$siteName
        $appPool | set-itemproperty -Name "managedRuntimeVersion" -Value "v4.0"
        #$appPool | set-itemproperty -Name "managedPipelineMode" -Value 1
        $appPool | set-itemproperty -Name "enable32BitAppOnWin64" -Value "false"
    } -session $session -ArgumentList $siteName -ErrorAction Stop
}

function RemoteCreateSite($environment ,$siteName, $appPoolName, $physicalPath, $siteAddress)
{
    invoke-command{
        param($environment, $siteName, $appPoolName, $physicalPath, $siteAddress)
        import-module webadministration
        
        Write-Output "Deleting the existing site $siteName"
		$siteExists = test-path iis:\Sites\$siteName
        if ($siteExists)
        {
            remove-item iis:\sites\$siteName -recurse
        }
		
		if ($environment -eq "live")
		{
			$siteAddress = ""
		}
		
		Write-Output "Creating the new site $siteName"
        $site = new-item iis:\Sites\$siteName -bindings @{protocol="http";bindingInformation=":80:$siteAddress"} -physicalPath $physicalPath
        $site | set-itemproperty -Name "ApplicationPool" -Value "$appPoolName"

    } -session $session -ArgumentList $environment, $siteName, $appPoolName, $physicalPath, $siteAddress
}

function GetPhysicalPath ($environment)
{
	
    if ($environment -eq "local")
	{
    	$physicalPath = "C:\Projects\NetSuite.CRM\Src\MarketingLeadGen\MarketingLeadGen"
	}
	else
	{
		$physicalPath = "D:\WebApps\NetSuite.CRM\$environment\MarketingLeadGen"
	}

    return $physicalPath
}

function MsDeploy($workingDir, $package, $machineName, $siteName, $physicalPath, $credentials)
{
	$msdeploy = "C:\Program Files\IIS\Microsoft Web Deploy V3\msdeploy.exe"

    $username = $credentials.Username
	$password = Decrypt-SecureString $credentials.Password
	$serviceUrl = "https://" + $machineName + ":8172/MsDeploy.axd"
	C:\Windows\System32\cmd.exe /C $(" `"$msdeploy`" -verb:sync -source:package=`"{0}`" -dest:auto,computerName=`"{1}`",authType=Basic,username=`"{2}`",password=`"{3}`",includeAcls=False -setParam:name=`"IIS Web Application Name`",value=`"{4}`" -disableLink:AppPoolExtension -disableLink:ContentExtension -disableLink:CertificateExtension -allowUntrusted -debug -verbose" -f $package, $serviceUrl, $username, $password, $siteName)
}
