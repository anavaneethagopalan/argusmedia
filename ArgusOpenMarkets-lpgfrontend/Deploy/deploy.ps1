param([ValidateSet('live','systest','uat','ci','local')] $environment, $target, $username, $password, $workingDir, $packageFile)
. '.\tools.ps1'

trap
{
    write-error $_
    exit 1
}

function Deploy
{
	# Sanity check target params
	
	# Deploy
	ArgusOpenMarketsWebSite
}

function ArgusOpenMarketsWebSite {
$siteName = "LeadGenWebServ ($environment)"
    $appPoolName = $siteName
	$apptype = "website"

    ##########  CONNECT #####################

    Write-Output "Connecting to $environment with $username"

    $global:session = ConnectToMachine -environment $environment -apptype $apptype -username $username -password $password

    Write-Output "Connected"

    ##########################################

    ############# CREATE WEB APP ##################

    Write-Output "Creating App Pool"

    RemoteCreateAppPool -session $global:session -siteName $siteName

    Write-Output "App Pool Created"

    $physicalPath = GetPhysicalPath -environment $environment
    if ($target -eq $null){
        $machineAddress = GetMachineAddress -environment $environment
    } else {
        $machineAddress = $target
    }
    $credentials = GetCredentials -environment $environment -username $username -password $password

    Write-Output "Creating Web App called $siteName at $physicalPath on $machineAddress"

    RemoteCreateSite -environment $environment -siteName $siteName -appPoolName $appPoolName -physicalPath $physicalPath -siteAddress $machineAddress

    Write-Output "Web App Created"

    Write-Output "Deploying using MS Deploy"

    MsDeploy -workingDir $workingDir -package $packageFile -machineName $machineAddress -siteName $siteName -physicalPath $physicalPath -credentials $credentials

    Write-Output "App deployed"


    ################################################


    ######## DISCONNECT #######################

    Write-Output "Disconnecting"
    
    DisconnectFromMachine -session $global:session
    
    Write-Output "Disconnected"

    ###########################################
}


Deploy