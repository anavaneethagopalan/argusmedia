param($MSBUILD,$workingDir)

function Build
{
if($MSBUILD -eq $null){
        $MSBUILD = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe"
    }

    if($workingDir -eq $null){
        $workingDir = "C:\projects\ArgusOpenMarkets"
    }
	$name = "ArgusOpenMarkets"
	$packageFile = "$workingDir\Artefacts\ArgusOpenMarkets.zip"
	$projectFile = "$workingDir\Src\ArgusOpenMarkets.Web.App\ArgusOpenMarkets.Web.App.csproj"
    
    Write-Output "Packaging using MSBuild located at $MSBUILD"

	$result = & $MSBUILD $projectFile /T:Package /p:Configuration=Release /p:PackageLocation=$packageFile /p:Platform=AnyCPU

     Write-Output "Done"
}
Build