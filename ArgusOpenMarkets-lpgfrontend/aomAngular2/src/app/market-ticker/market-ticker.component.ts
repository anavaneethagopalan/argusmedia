import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MarketTickerService } from './market-ticker.service';

@Component({
    selector: 'market-ticker',
    styleUrls: [
        './market-ticker.component.scss'
    ],
    templateUrl: './market-ticker.component.html',
    providers: [ MarketTickerService ]
})
export class MarketTickerComponent implements OnInit, OnDestroy {

    public marketTickerItems: string[];
    private subscription: Subscription;

    constructor(public marketTickerService: MarketTickerService) {
    }

    public ngOnInit(): void {
        this.subscription = this.marketTickerService.marketTicker$
            .subscribe((result) => this.marketTickerItems = result);
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
