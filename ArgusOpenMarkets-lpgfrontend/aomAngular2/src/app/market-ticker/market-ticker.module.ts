import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MarketTickerComponent } from './market-ticker.component';

import '../../styles/themes.scss';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        MarketTickerComponent
    ],
    exports: [
        MarketTickerComponent
    ]
})
export class MarketTickerModule {}
