import { TestBed, async } from '@angular/core/testing';

import { MarketTickerComponent } from './market-ticker.component';

describe('MarketTickerComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MarketTickerComponent
      ],
    }).compileComponents();
  }));

  it('should create MarketTickerComponent', async(() => {
    const fixture = TestBed.createComponent(MarketTickerComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
