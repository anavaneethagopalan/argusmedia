import { Injectable } from '@angular/core';
import { User } from '../login/user';
import diffusion from 'diffusion';
// import * as DiffusionClient from '../../assets/diffusion/diffusion-js-classic-5.9.8';
//var diff = require('assets/diffusion/diffusion-js-classic-5.9.8.js');

@Injectable()
export class SocketProvider  {
  private DiffusionClient: any;
  private _isAuthenticated: boolean  = false;

  constructor() {

  }

  public isAuthenticated(){
    console.log('User is authenticated: ' + this._isAuthenticated);
    return this._isAuthenticated;
  }

  public connect(email, password) {

    // let result = diffusion.connect({host : 'localhost',
    //   port : 8080,
    //   secure : false,
    //   principal : email,
    //   credentials : password});

    console.log('email: ' + email);
    if(email && email.toLowerCase() === 'nathan') {
      this._isAuthenticated = true;
      let user = new User('nathanbellamore', password, true);
      return new Promise(resolve => resolve(user));
    }else {
      this._isAuthenticated = false;
      let failedLogin = new User(email, password, false);
      return new Promise(resolve => resolve(failedLogin));
    }
  }
};
