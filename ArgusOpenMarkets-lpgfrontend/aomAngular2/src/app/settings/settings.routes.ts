import { SettingsComponent } from './settings.component';
import { AboutComponent } from '../about';

export const routes = [
  { path: '', component: SettingsComponent }
];
