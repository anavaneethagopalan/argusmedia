import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { routes } from './settings.routes';
import { SettingsComponent } from './settings.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        SettingsComponent
    ]
})
export class SettingsModule {
    public static routes = routes;
}
