import { TestBed, async } from '@angular/core/testing';

import { NewsComponent } from './news.component';

describe('NewsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NewsComponent
      ],
    }).compileComponents();
  }));

  it('should create NewsComponent', async(() => {
    const fixture = TestBed.createComponent(NewsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
