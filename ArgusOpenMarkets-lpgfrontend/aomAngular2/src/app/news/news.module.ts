import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NewsComponent } from './news.component';

import '../../styles/themes.scss';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        NewsComponent
    ],
    exports: [
        NewsComponent
    ]
})
export class NewsModule {}
