import { Component } from '@angular/core';

@Component({
    selector: 'news',
    styleUrls: [
        './news.component.scss'
    ],
    templateUrl: './news.component.html'
})
export class NewsComponent {
    public newsItems = [
        'News Item 1',
        'News Item 2',
        'News Item 3',
        'News Item 4'
    ];
}
