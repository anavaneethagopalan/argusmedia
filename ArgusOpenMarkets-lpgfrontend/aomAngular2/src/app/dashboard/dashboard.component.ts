import { Component } from '@angular/core';
import { HealthcheckService } from '../shared/healthcheck.service';

@Component({
    selector: 'dashboard',
    styleUrls: [
        './dashboard.component.scss'
    ],
    templateUrl: './dashboard.component.html',
    providers: [ HealthcheckService ]
})
export class DashboardComponent {
    public status: number;

    constructor(private healthcheckService: HealthcheckService) {}

    public healthcheck() {
        this.healthcheckService.checkHealth().subscribe((response) => this.status = response);
    }
}
