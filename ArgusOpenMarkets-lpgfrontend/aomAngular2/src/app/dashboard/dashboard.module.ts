import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { routes } from './dashboard.routes';
import { DashboardComponent } from './dashboard.component';

import { BidAskModule } from '../bidask';
import { AssessmentsModule } from '../assessments';
import { MarketTickerModule } from '../market-ticker';
import { NewsModule } from '../news';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        BidAskModule,
        AssessmentsModule,
        MarketTickerModule,
        NewsModule
    ],
    declarations: [
        DashboardComponent
    ]
})
export class DashboardModule {
    public static routes = routes;
}
