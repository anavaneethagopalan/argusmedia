import { Component } from '@angular/core';

@Component({
    selector: 'assessments',
    styleUrls: [
        './assessments.component.scss'
    ],
    templateUrl: './assessments.component.html'
})
export class AssessmentsComponent {}
