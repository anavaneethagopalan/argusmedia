import { TestBed, async } from '@angular/core/testing';

import { AssessmentsComponent } from './assessments.component';

describe('AssessmentsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AssessmentsComponent
      ],
    }).compileComponents();
  }));

  it('should create AssessmentsComponent', async(() => {
    const fixture = TestBed.createComponent(AssessmentsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
