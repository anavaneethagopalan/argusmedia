import { Component, Input } from '@angular/core';

@Component({
    selector: 'assessments-product',
    styleUrls: [
        './assessments-product.component.scss'
    ],
    templateUrl: './assessments-product.component.html'
})
export class AssessmentsProductComponent {
    @Input() public productName: string;
}
