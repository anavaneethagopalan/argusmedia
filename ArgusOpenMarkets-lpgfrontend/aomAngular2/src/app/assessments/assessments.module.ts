import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AssessmentsComponent } from './assessments.component';
import { AssessmentsProductComponent } from './assessments-product.component';

import '../../styles/themes.scss';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        AssessmentsComponent,
        AssessmentsProductComponent
    ],
    exports: [
        AssessmentsComponent
    ]
})
export class AssessmentsModule {}
