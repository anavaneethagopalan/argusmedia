import { WEB_SERVICE_CONFIG } from './web-service-config';

export class WebServiceResources {
  public static get healthcheckResource(): string {
    return this.api + '/' + WEB_SERVICE_CONFIG.healthcheck;
  }

  public static get loginResource(): string {
    return this.api + '/' + WEB_SERVICE_CONFIG.login;
  }

  private static get api(): string {
    return WEB_SERVICE_CONFIG.protocol + '://'
      + WEB_SERVICE_CONFIG.endpoint + ':'
      + WEB_SERVICE_CONFIG.port + '/'
      + WEB_SERVICE_CONFIG.api;
  }
}