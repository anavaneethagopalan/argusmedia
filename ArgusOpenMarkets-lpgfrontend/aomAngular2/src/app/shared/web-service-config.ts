export const WEB_SERVICE_CONFIG = {
    protocol: 'http',
    endpoint: 'localhost',
    port: '57166',
    api: 'api',
    healthcheck: 'healthcheck',
    login: 'login'
};
