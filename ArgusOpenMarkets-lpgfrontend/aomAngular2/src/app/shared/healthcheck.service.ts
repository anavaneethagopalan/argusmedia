import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { WebServiceResources } from './web-service-resources';

@Injectable()
export class HealthcheckService {
    constructor(private http: Http) { }
    public checkHealth(): Observable<number> {
        return this.http
            .get(WebServiceResources.healthcheckResource)
            .timeout(10000)
            .map((response) => response.status)
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
