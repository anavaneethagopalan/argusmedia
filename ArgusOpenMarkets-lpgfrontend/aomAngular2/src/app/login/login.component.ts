import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './user';
import { SocketProvider } from '../diffusion/socket-provider';

import { LoginService } from './login.service';

@Component({
  selector: 'login',
  styles: [`
  `],
  templateUrl: './login.html',
  providers: [LoginService]
})

export class LoginComponent implements OnInit {
  public user: User = new User('', '', false);
  public webServiceUser: User = new User('', '', false);
  public errorMsg: string = '';
  public num: number = 0;

  constructor(
    public socketProvider: SocketProvider,
    public router: Router,
    private loginService: LoginService) { }

  public ngOnInit() {
    console.log('Login Component Initialised');
  }

  public login() {
    this.num++;
    console.log('this.user=' + this.user);
    console.log(this.user.email);
    this.socketProvider.connect(this.user.email, this.user.password).then((data) => {
      let user = <User>data;
      console.log(user);
      if (user && user.isAuthenticated) {
        console.log('user is authenticated');
        this.router.navigate(['/dashboard']);
      } else {
        this.errorMsg = 'We are unable to log you in';
      }
    });
  }

  public webServiceLogin() {
    console.log('this.webServiceUser=', this.webServiceUser);
    this.loginService.login(this.webServiceUser.email, this.webServiceUser.password)
      .subscribe((response) => this.onUserAuthenticated(response),
        (error) => this.onLoginError(error));
  }

  private onUserAuthenticated(response) {
    console.log('login response: ', response);
  }

  private onLoginError(error) {
    console.log('login error: ', error);
  }
}
