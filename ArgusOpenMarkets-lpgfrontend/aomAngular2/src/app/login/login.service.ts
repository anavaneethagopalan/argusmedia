import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { WebServiceResources } from '../shared/web-service-resources';

@Injectable()
export class LoginService {
    constructor(private http: Http) { }

    public login(username: string, password: string): Observable<Response> {
        let credentials = {
            username,
            password
        };
        return this.http
            .post(WebServiceResources.loginResource, credentials)
            .timeout(10000)
            .map((response) => response);
    }

}
