import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { BidAskComponent } from './bidask.component';
import { BidAskProductComponent } from './bidask-product.component';

import { DragulaModule } from 'ng2-dragula';

import '../../styles/themes.scss';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DragulaModule
    ],
    declarations: [
        BidAskComponent,
        BidAskProductComponent
    ],
    exports: [
        BidAskComponent
    ]
})
export class BidAskModule {}
