export class Bid {
    public date: string;
    public broker: string;
    public price: number;
    public quantity: number;
}
