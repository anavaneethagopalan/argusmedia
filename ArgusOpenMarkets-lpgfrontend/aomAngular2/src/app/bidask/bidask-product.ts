import { Bid } from './bid';
import { Ask } from './ask';

export class BidAskProduct {
  public productId: number;
  public productName: string;
  public bids: Bid[];
  public asks: Ask[];
}
