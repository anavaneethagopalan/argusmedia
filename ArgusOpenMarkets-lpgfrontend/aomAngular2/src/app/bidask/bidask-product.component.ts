import {
    Component,
    Input
} from '@angular/core';

import { BidAskProduct } from './bidask-product';
import { Bid } from './bid';
import { Ask } from './ask';
import { OrdersService } from './orders.service';

import { DragulaService } from 'ng2-dragula';

@Component({
    selector: 'bidask-product',
    styleUrls: ['./bidask-product.component.scss'],
    templateUrl: './bidask-product.component.html'
})
export class BidAskProductComponent {
    @Input() public bidAskProduct: BidAskProduct;
    public componentSize = 'medium';
    public componentHeight = 'standard';

    constructor(public ordersService: OrdersService) {
    }

    public resizeSmall(): void {
        this.componentSize = 'small';
        this.printComponentSize();
    }

    public resizeMedium(): void {
        this.componentSize = 'medium';
        this.printComponentSize();
    }

    public resizeLarge(): void {
        this.componentSize = 'large';
        this.printComponentSize();
    }

    public resizeShrink(): void {
        this.componentHeight = 'standard';
    }

    public resizeExpand(): void {
        this.componentHeight = 'large';
    }

    public addBid(): void {
        this.ordersService.addBid(this.bidAskProduct.productId, this.generateBid());
    }

    public addAsk(): void {
        this.ordersService.addAsk(this.bidAskProduct.productId, this.generateAsk());
    }

    private printComponentSize(): void {
        console.log('component size changed to: ', this.componentSize);
    }

    private generateBid(): Bid {
        return {
            date: '16-4',
            broker: '*',
            price: 111,
            quantity: 10300
        };
    }

    private generateAsk(): Ask {
        return {
            date: '16-4',
            broker: '*',
            price: 111,
            quantity: 10300
        };
    }
}
