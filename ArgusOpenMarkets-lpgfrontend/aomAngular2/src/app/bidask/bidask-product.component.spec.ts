import { TestBed, async } from '@angular/core/testing';

import { BidAskProductComponent } from './bidask-product.component';

describe('BidAskProductComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BidAskProductComponent
      ],
    }).compileComponents();
  }));

  it('should create BidAskProductComponent', async(() => {
    const fixture = TestBed.createComponent(BidAskProductComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
