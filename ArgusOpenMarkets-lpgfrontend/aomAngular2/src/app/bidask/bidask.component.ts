import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { BidAskProduct } from './bidask-product';
import { Bid } from './bid';
import { Ask } from './ask';
import { DragulaService } from 'ng2-dragula';
import { OrdersService } from './orders.service';

@Component({
  selector: 'bidask',
  styles: [`
  `],
  templateUrl: './bidAsk.component.html',
  viewProviders: [DragulaService],
  providers: [OrdersService]
})
export class BidAskComponent implements OnInit, OnDestroy {
  public bids: [{
    date: string,
    broker: string,
    price: number,
    quantity: number,

  }];

  public localState: any;
  public bidaskProducts: BidAskProduct[];
  private subscription: Subscription;

  constructor(public route: ActivatedRoute, public ordersService: OrdersService) {
  }

  public ngOnInit() {
    this.route
      .data
      .subscribe((data: any) => {
        // your resolved data from route
        this.localState = data.yourData;
      });

    console.log('hello `BidAsk` component');
    // static data that is bundled
    // var mockData = require('assets/mock-data/mock-data.json');
    // console.log('mockData', mockData);
    // if you're working with mock data you can also use http.get('assets/mock-data/mock-data.json')
    this.asyncDataWithWebpack();

    this.loadBids();
    this.subscription = this.ordersService.bidAskProduct$
      .subscribe((result) => this.bidaskProducts = result);
    console.log('bid ask products:', this.bidaskProducts);
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private loadBids() {
    // TODO: Needs to be populated by Diffusion - out of scope
    this.bids = [{
      date: '10-10',
      broker: '*',
      price: 123.99,
      quantity: 10000
    }, {
      date: '11-10',
      broker: '*',
      price: 125.99,
      quantity: 12500
    }];

  }

  private asyncDataWithWebpack() {
    // you can also async load mock data with 'es6-promise-loader'
    // you would do this if you don't want the mock-data bundled
    // remember that 'es6-promise-loader' is a promise
    setTimeout(() => {

      System.import('../../assets/mock-data/mock-data.json')
        .then((json) => {
          console.log('async mockData', json);
          this.localState = json;
        });

    });
  }

}
