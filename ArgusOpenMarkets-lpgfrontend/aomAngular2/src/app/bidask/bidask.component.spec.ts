import { TestBed, async } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { BidAskComponent } from './bidask.component';
import { DragulaModule } from 'ng2-dragula';

describe('BidAskComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BidAskComponent
      ],
      imports: [
        DragulaModule,
        RouterTestingModule
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  }));

  it('should create BidAskComponent', async(() => {
    const fixture = TestBed.createComponent(BidAskComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
