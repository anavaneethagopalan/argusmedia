import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BidAskProduct } from './bidask-product';
import { Bid } from './bid';
import { Ask } from './ask';

@Injectable()
export class OrdersService {
    // TODO: initialise orders from diffusion and listen for updates
    private bidaskProducts: BidAskProduct[] = [
        {
            productId: 1,
            productName: 'NAPHTHA CIF ARA - CARGOES',
            bids: [{
                date: '10-10',
                broker: '*',
                price: 123.99,
                quantity: 10000
            }, {
                date: '11-10',
                broker: '*',
                price: 125.99,
                quantity: 12500
            }],
            asks: []
        },
        {
            productId: 2,
            productName: 'NAPHTHA CIF ARA - BARGES',
            bids: [{
                date: '10-10',
                broker: '*',
                price: 123.99,
                quantity: 10000
            }, {
                date: '11-10',
                broker: '*',
                price: 125.99,
                quantity: 12500
            }],
            asks: [{
                date: '10-10',
                broker: '*',
                price: 123.99,
                quantity: 10000
            }, {
                date: '11-10',
                broker: '*',
                price: 125.99,
                quantity: 12500
            }]
        },
        {
            productId: 3,
            productName: 'NEW PRODUCT 1',
            bids: [],
            asks: []
        }
    ];
    private bidAskProductSource = new BehaviorSubject<BidAskProduct[]>(this.bidaskProducts);

    public get bidAskProduct$() {
        return this.bidAskProductSource.asObservable();
    }

    public addBid(productId: number, bid: Bid) {
        // TODO: send create order message to diffusion
        this.bidaskProducts.find((x) => x.productId === productId).bids.push(bid);
        this.updateSource();
    }

    public addAsk(productId: number, ask: Ask) {
        // TODO: send create order message to diffusion
        this.bidaskProducts.find((x) => x.productId === productId).asks.push(ask);
        this.updateSource();
    }

    private updateSource() {
        this.bidAskProductSource.next(this.bidaskProducts);
    }
}
