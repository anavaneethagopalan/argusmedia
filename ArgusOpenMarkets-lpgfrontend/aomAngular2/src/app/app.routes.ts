import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { LoginComponent } from './login';
import { LoggedInGuard } from './guards/userLoggedInGuard';
import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'dashboard', loadChildren: './dashboard#DashboardModule', canActivate: [LoggedInGuard] },
  { path: 'detail', loadChildren: './+detail#DetailModule'},
  { path: 'barrel', loadChildren: './+barrel#BarrelModule'},
  { path: 'settings', loadChildren: './settings#SettingsModule' },
  { path: '**',    component: NoContentComponent }
];
