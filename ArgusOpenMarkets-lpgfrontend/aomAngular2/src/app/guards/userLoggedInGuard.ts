
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SocketProvider } from '../diffusion/socket-provider';

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor(
    private router: Router,
    private socketProvider: SocketProvider
  ) { }

  public canActivate() {
    if(this.socketProvider.isAuthenticated() === false) {
      this.router.navigate(['/login']);
      return false;
    }

    return true;
  }
}
