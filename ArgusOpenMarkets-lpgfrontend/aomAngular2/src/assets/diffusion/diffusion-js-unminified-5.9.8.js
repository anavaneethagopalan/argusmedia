(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.diffusion = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

},{}],2:[function(require,module,exports){
(function (global){

'use strict'

var base64 = require('base64-js')
var ieee754 = require('ieee754')
var isArray = require('isarray')

exports.Buffer = Buffer
exports.SlowBuffer = SlowBuffer
exports.INSPECT_MAX_BYTES = 50

Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined
  ? global.TYPED_ARRAY_SUPPORT
  : typedArraySupport()

exports.kMaxLength = kMaxLength()

function typedArraySupport () {
  try {
    var arr = new Uint8Array(1)
    arr.__proto__ = {__proto__: Uint8Array.prototype, foo: function () { return 42 }}
    return arr.foo() === 42 && 
        typeof arr.subarray === 'function' && 
        arr.subarray(1, 1).byteLength === 0 
  } catch (e) {
    return false
  }
}

function kMaxLength () {
  return Buffer.TYPED_ARRAY_SUPPORT
    ? 0x7fffffff
    : 0x3fffffff
}

function createBuffer (that, length) {
  if (kMaxLength() < length) {
    throw new RangeError('Invalid typed array length')
  }
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    that = new Uint8Array(length)
    that.__proto__ = Buffer.prototype
  } else {
    if (that === null) {
      that = new Buffer(length)
    }
    that.length = length
  }

  return that
}


function Buffer (arg, encodingOrOffset, length) {
  if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) {
    return new Buffer(arg, encodingOrOffset, length)
  }

  if (typeof arg === 'number') {
    if (typeof encodingOrOffset === 'string') {
      throw new Error(
        'If encoding is specified then the first argument must be a string'
      )
    }
    return allocUnsafe(this, arg)
  }
  return from(this, arg, encodingOrOffset, length)
}

Buffer.poolSize = 8192 

Buffer._augment = function (arr) {
  arr.__proto__ = Buffer.prototype
  return arr
}

function from (that, value, encodingOrOffset, length) {
  if (typeof value === 'number') {
    throw new TypeError('"value" argument must not be a number')
  }

  if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
    return fromArrayBuffer(that, value, encodingOrOffset, length)
  }

  if (typeof value === 'string') {
    return fromString(that, value, encodingOrOffset)
  }

  return fromObject(that, value)
}

Buffer.from = function (value, encodingOrOffset, length) {
  return from(null, value, encodingOrOffset, length)
}

if (Buffer.TYPED_ARRAY_SUPPORT) {
  Buffer.prototype.__proto__ = Uint8Array.prototype
  Buffer.__proto__ = Uint8Array
  if (typeof Symbol !== 'undefined' && Symbol.species &&
      Buffer[Symbol.species] === Buffer) {
    Object.defineProperty(Buffer, Symbol.species, {
      value: null,
      configurable: true
    })
  }
}

function assertSize (size) {
  if (typeof size !== 'number') {
    throw new TypeError('"size" argument must be a number')
  } else if (size < 0) {
    throw new RangeError('"size" argument must not be negative')
  }
}

function alloc (that, size, fill, encoding) {
  assertSize(size)
  if (size <= 0) {
    return createBuffer(that, size)
  }
  if (fill !== undefined) {
    return typeof encoding === 'string'
      ? createBuffer(that, size).fill(fill, encoding)
      : createBuffer(that, size).fill(fill)
  }
  return createBuffer(that, size)
}

Buffer.alloc = function (size, fill, encoding) {
  return alloc(null, size, fill, encoding)
}

function allocUnsafe (that, size) {
  assertSize(size)
  that = createBuffer(that, size < 0 ? 0 : checked(size) | 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < size; ++i) {
      that[i] = 0
    }
  }
  return that
}

Buffer.allocUnsafe = function (size) {
  return allocUnsafe(null, size)
}
Buffer.allocUnsafeSlow = function (size) {
  return allocUnsafe(null, size)
}

function fromString (that, string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') {
    encoding = 'utf8'
  }

  if (!Buffer.isEncoding(encoding)) {
    throw new TypeError('"encoding" must be a valid string encoding')
  }

  var length = byteLength(string, encoding) | 0
  that = createBuffer(that, length)

  var actual = that.write(string, encoding)

  if (actual !== length) {
    that = that.slice(0, actual)
  }

  return that
}

function fromArrayLike (that, array) {
  var length = array.length < 0 ? 0 : checked(array.length) | 0
  that = createBuffer(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

function fromArrayBuffer (that, array, byteOffset, length) {
  array.byteLength 

  if (byteOffset < 0 || array.byteLength < byteOffset) {
    throw new RangeError('\'offset\' is out of bounds')
  }

  if (array.byteLength < byteOffset + (length || 0)) {
    throw new RangeError('\'length\' is out of bounds')
  }

  if (byteOffset === undefined && length === undefined) {
    array = new Uint8Array(array)
  } else if (length === undefined) {
    array = new Uint8Array(array, byteOffset)
  } else {
    array = new Uint8Array(array, byteOffset, length)
  }

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    that = array
    that.__proto__ = Buffer.prototype
  } else {
    that = fromArrayLike(that, array)
  }
  return that
}

function fromObject (that, obj) {
  if (Buffer.isBuffer(obj)) {
    var len = checked(obj.length) | 0
    that = createBuffer(that, len)

    if (that.length === 0) {
      return that
    }

    obj.copy(that, 0, 0, len)
    return that
  }

  if (obj) {
    if ((typeof ArrayBuffer !== 'undefined' &&
        obj.buffer instanceof ArrayBuffer) || 'length' in obj) {
      if (typeof obj.length !== 'number' || isnan(obj.length)) {
        return createBuffer(that, 0)
      }
      return fromArrayLike(that, obj)
    }

    if (obj.type === 'Buffer' && isArray(obj.data)) {
      return fromArrayLike(that, obj.data)
    }
  }

  throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.')
}

function checked (length) {
  if (length >= kMaxLength()) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                         'size: 0x' + kMaxLength().toString(16) + ' bytes')
  }
  return length | 0
}

function SlowBuffer (length) {
  if (+length != length) { 
    length = 0
  }
  return Buffer.alloc(+length)
}

Buffer.isBuffer = function isBuffer (b) {
  return !!(b != null && b._isBuffer)
}

Buffer.compare = function compare (a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('Arguments must be Buffers')
  }

  if (a === b) return 0

  var x = a.length
  var y = b.length

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i]
      y = b[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function isEncoding (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'latin1':
    case 'binary':
    case 'base64':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function concat (list, length) {
  if (!isArray(list)) {
    throw new TypeError('"list" argument must be an Array of Buffers')
  }

  if (list.length === 0) {
    return Buffer.alloc(0)
  }

  var i
  if (length === undefined) {
    length = 0
    for (i = 0; i < list.length; ++i) {
      length += list[i].length
    }
  }

  var buffer = Buffer.allocUnsafe(length)
  var pos = 0
  for (i = 0; i < list.length; ++i) {
    var buf = list[i]
    if (!Buffer.isBuffer(buf)) {
      throw new TypeError('"list" argument must be an Array of Buffers')
    }
    buf.copy(buffer, pos)
    pos += buf.length
  }
  return buffer
}

function byteLength (string, encoding) {
  if (Buffer.isBuffer(string)) {
    return string.length
  }
  if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' &&
      (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
    return string.byteLength
  }
  if (typeof string !== 'string') {
    string = '' + string
  }

  var len = string.length
  if (len === 0) return 0

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'latin1':
      case 'binary':
        return len
      case 'utf8':
      case 'utf-8':
      case undefined:
        return utf8ToBytes(string).length
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2
      case 'hex':
        return len >>> 1
      case 'base64':
        return base64ToBytes(string).length
      default:
        if (loweredCase) return utf8ToBytes(string).length 
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}
Buffer.byteLength = byteLength

function slowToString (encoding, start, end) {
  var loweredCase = false


  if (start === undefined || start < 0) {
    start = 0
  }
  if (start > this.length) {
    return ''
  }

  if (end === undefined || end > this.length) {
    end = this.length
  }

  if (end <= 0) {
    return ''
  }

  end >>>= 0
  start >>>= 0

  if (end <= start) {
    return ''
  }

  if (!encoding) encoding = 'utf8'

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'latin1':
      case 'binary':
        return latin1Slice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype._isBuffer = true

function swap (b, n, m) {
  var i = b[n]
  b[n] = b[m]
  b[m] = i
}

Buffer.prototype.swap16 = function swap16 () {
  var len = this.length
  if (len % 2 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 16-bits')
  }
  for (var i = 0; i < len; i += 2) {
    swap(this, i, i + 1)
  }
  return this
}

Buffer.prototype.swap32 = function swap32 () {
  var len = this.length
  if (len % 4 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 32-bits')
  }
  for (var i = 0; i < len; i += 4) {
    swap(this, i, i + 3)
    swap(this, i + 1, i + 2)
  }
  return this
}

Buffer.prototype.swap64 = function swap64 () {
  var len = this.length
  if (len % 8 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 64-bits')
  }
  for (var i = 0; i < len; i += 8) {
    swap(this, i, i + 7)
    swap(this, i + 1, i + 6)
    swap(this, i + 2, i + 5)
    swap(this, i + 3, i + 4)
  }
  return this
}

Buffer.prototype.toString = function toString () {
  var length = this.length | 0
  if (length === 0) return ''
  if (arguments.length === 0) return utf8Slice(this, 0, length)
  return slowToString.apply(this, arguments)
}

Buffer.prototype.equals = function equals (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return true
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function inspect () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
    if (this.length > max) str += ' ... '
  }
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function compare (target, start, end, thisStart, thisEnd) {
  if (!Buffer.isBuffer(target)) {
    throw new TypeError('Argument must be a Buffer')
  }

  if (start === undefined) {
    start = 0
  }
  if (end === undefined) {
    end = target ? target.length : 0
  }
  if (thisStart === undefined) {
    thisStart = 0
  }
  if (thisEnd === undefined) {
    thisEnd = this.length
  }

  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
    throw new RangeError('out of range index')
  }

  if (thisStart >= thisEnd && start >= end) {
    return 0
  }
  if (thisStart >= thisEnd) {
    return -1
  }
  if (start >= end) {
    return 1
  }

  start >>>= 0
  end >>>= 0
  thisStart >>>= 0
  thisEnd >>>= 0

  if (this === target) return 0

  var x = thisEnd - thisStart
  var y = end - start
  var len = Math.min(x, y)

  var thisCopy = this.slice(thisStart, thisEnd)
  var targetCopy = target.slice(start, end)

  for (var i = 0; i < len; ++i) {
    if (thisCopy[i] !== targetCopy[i]) {
      x = thisCopy[i]
      y = targetCopy[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

function bidirectionalIndexOf (buffer, val, byteOffset, encoding, dir) {
  if (buffer.length === 0) return -1

  if (typeof byteOffset === 'string') {
    encoding = byteOffset
    byteOffset = 0
  } else if (byteOffset > 0x7fffffff) {
    byteOffset = 0x7fffffff
  } else if (byteOffset < -0x80000000) {
    byteOffset = -0x80000000
  }
  byteOffset = +byteOffset  
  if (isNaN(byteOffset)) {
    byteOffset = dir ? 0 : (buffer.length - 1)
  }

  if (byteOffset < 0) byteOffset = buffer.length + byteOffset
  if (byteOffset >= buffer.length) {
    if (dir) return -1
    else byteOffset = buffer.length - 1
  } else if (byteOffset < 0) {
    if (dir) byteOffset = 0
    else return -1
  }

  if (typeof val === 'string') {
    val = Buffer.from(val, encoding)
  }

  if (Buffer.isBuffer(val)) {
    if (val.length === 0) {
      return -1
    }
    return arrayIndexOf(buffer, val, byteOffset, encoding, dir)
  } else if (typeof val === 'number') {
    val = val & 0xFF 
    if (Buffer.TYPED_ARRAY_SUPPORT &&
        typeof Uint8Array.prototype.indexOf === 'function') {
      if (dir) {
        return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset)
      } else {
        return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset)
      }
    }
    return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir)
  }

  throw new TypeError('val must be string, number or Buffer')
}

function arrayIndexOf (arr, val, byteOffset, encoding, dir) {
  var indexSize = 1
  var arrLength = arr.length
  var valLength = val.length

  if (encoding !== undefined) {
    encoding = String(encoding).toLowerCase()
    if (encoding === 'ucs2' || encoding === 'ucs-2' ||
        encoding === 'utf16le' || encoding === 'utf-16le') {
      if (arr.length < 2 || val.length < 2) {
        return -1
      }
      indexSize = 2
      arrLength /= 2
      valLength /= 2
      byteOffset /= 2
    }
  }

  function read (buf, i) {
    if (indexSize === 1) {
      return buf[i]
    } else {
      return buf.readUInt16BE(i * indexSize)
    }
  }

  var i
  if (dir) {
    var foundIndex = -1
    for (i = byteOffset; i < arrLength; i++) {
      if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
        if (foundIndex === -1) foundIndex = i
        if (i - foundIndex + 1 === valLength) return foundIndex * indexSize
      } else {
        if (foundIndex !== -1) i -= i - foundIndex
        foundIndex = -1
      }
    }
  } else {
    if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength
    for (i = byteOffset; i >= 0; i--) {
      var found = true
      for (var j = 0; j < valLength; j++) {
        if (read(arr, i + j) !== read(val, j)) {
          found = false
          break
        }
      }
      if (found) return i
    }
  }

  return -1
}

Buffer.prototype.includes = function includes (val, byteOffset, encoding) {
  return this.indexOf(val, byteOffset, encoding) !== -1
}

Buffer.prototype.indexOf = function indexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, true)
}

Buffer.prototype.lastIndexOf = function lastIndexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, false)
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  var strLen = string.length
  if (strLen % 2 !== 0) throw new TypeError('Invalid hex string')

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; ++i) {
    var parsed = parseInt(string.substr(i * 2, 2), 16)
    if (isNaN(parsed)) return i
    buf[offset + i] = parsed
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
}

function asciiWrite (buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length)
}

function latin1Write (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length)
}

function ucs2Write (buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
}

Buffer.prototype.write = function write (string, offset, length, encoding) {
  if (offset === undefined) {
    encoding = 'utf8'
    length = this.length
    offset = 0
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset
    length = this.length
    offset = 0
  } else if (isFinite(offset)) {
    offset = offset | 0
    if (isFinite(length)) {
      length = length | 0
      if (encoding === undefined) encoding = 'utf8'
    } else {
      encoding = length
      length = undefined
    }
  } else {
    throw new Error(
      'Buffer.write(string, encoding, offset[, length]) is no longer supported'
    )
  }

  var remaining = this.length - offset
  if (length === undefined || length > remaining) length = remaining

  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
    throw new RangeError('Attempt to write outside buffer bounds')
  }

  if (!encoding) encoding = 'utf8'

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length)

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length)

      case 'ascii':
        return asciiWrite(this, string, offset, length)

      case 'latin1':
      case 'binary':
        return latin1Write(this, string, offset, length)

      case 'base64':
        return base64Write(this, string, offset, length)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toJSON = function toJSON () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  end = Math.min(buf.length, end)
  var res = []

  var i = start
  while (i < end) {
    var firstByte = buf[i]
    var codePoint = null
    var bytesPerSequence = (firstByte > 0xEF) ? 4
      : (firstByte > 0xDF) ? 3
      : (firstByte > 0xBF) ? 2
      : 1

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte
          }
          break
        case 2:
          secondByte = buf[i + 1]
          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint
            }
          }
          break
        case 3:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint
            }
          }
          break
        case 4:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          fourthByte = buf[i + 3]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint
            }
          }
      }
    }

    if (codePoint === null) {
      codePoint = 0xFFFD
      bytesPerSequence = 1
    } else if (codePoint > 0xFFFF) {
      codePoint -= 0x10000
      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
      codePoint = 0xDC00 | codePoint & 0x3FF
    }

    res.push(codePoint)
    i += bytesPerSequence
  }

  return decodeCodePointsArray(res)
}

var MAX_ARGUMENTS_LENGTH = 0x1000

function decodeCodePointsArray (codePoints) {
  var len = codePoints.length
  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints) 
  }

  var res = ''
  var i = 0
  while (i < len) {
    res += String.fromCharCode.apply(
      String,
      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
    )
  }
  return res
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i] & 0x7F)
  }
  return ret
}

function latin1Slice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; ++i) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
  }
  return res
}

Buffer.prototype.slice = function slice (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len
    if (start < 0) start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0) end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start) end = start

  var newBuf
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    newBuf = this.subarray(start, end)
    newBuf.__proto__ = Buffer.prototype
  } else {
    var sliceLen = end - start
    newBuf = new Buffer(sliceLen, undefined)
    for (var i = 0; i < sliceLen; ++i) {
      newBuf[i] = this[i + start]
    }
  }

  return newBuf
}

function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }

  return val
}

Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    checkOffset(offset, byteLength, this.length)
  }

  var val = this[offset + --byteLength]
  var mul = 1
  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul
  }

  return val
}

Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
    ((this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    this[offset + 3])
}

Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var i = byteLength
  var mul = 1
  var val = this[offset + --i]
  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80)) return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset]) |
    (this[offset + 1] << 8) |
    (this[offset + 2] << 16) |
    (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
    (this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    (this[offset + 3])
}

Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance')
  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
}

Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var mul = 1
  var i = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var i = byteLength - 1
  var mul = 1
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  this[offset] = (value & 0xff)
  return offset + 1
}

function objectWriteUInt16 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
      (littleEndian ? i : 1 - i) * 8
  }
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

function objectWriteUInt32 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
  }
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = (value >>> 24)
    this[offset + 2] = (value >>> 16)
    this[offset + 1] = (value >>> 8)
    this[offset] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = 0
  var mul = 1
  var sub = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = byteLength - 1
  var mul = 1
  var sub = 0
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  if (value < 0) value = 0xff + value + 1
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
    this[offset + 2] = (value >>> 16)
    this[offset + 3] = (value >>> 24)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
  if (offset < 0) throw new RangeError('Index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  }
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  }
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

Buffer.prototype.copy = function copy (target, targetStart, start, end) {
  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (targetStart >= target.length) targetStart = target.length
  if (!targetStart) targetStart = 0
  if (end > 0 && end < start) end = start

  if (end === start) return 0
  if (target.length === 0 || this.length === 0) return 0

  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds')
  }
  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
  if (end < 0) throw new RangeError('sourceEnd out of bounds')

  if (end > this.length) end = this.length
  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start
  }

  var len = end - start
  var i

  if (this === target && start < targetStart && targetStart < end) {
    for (i = len - 1; i >= 0; --i) {
      target[i + targetStart] = this[i + start]
    }
  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    for (i = 0; i < len; ++i) {
      target[i + targetStart] = this[i + start]
    }
  } else {
    Uint8Array.prototype.set.call(
      target,
      this.subarray(start, start + len),
      targetStart
    )
  }

  return len
}

Buffer.prototype.fill = function fill (val, start, end, encoding) {
  if (typeof val === 'string') {
    if (typeof start === 'string') {
      encoding = start
      start = 0
      end = this.length
    } else if (typeof end === 'string') {
      encoding = end
      end = this.length
    }
    if (val.length === 1) {
      var code = val.charCodeAt(0)
      if (code < 256) {
        val = code
      }
    }
    if (encoding !== undefined && typeof encoding !== 'string') {
      throw new TypeError('encoding must be a string')
    }
    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
      throw new TypeError('Unknown encoding: ' + encoding)
    }
  } else if (typeof val === 'number') {
    val = val & 255
  }

  if (start < 0 || this.length < start || this.length < end) {
    throw new RangeError('Out of range index')
  }

  if (end <= start) {
    return this
  }

  start = start >>> 0
  end = end === undefined ? this.length : end >>> 0

  if (!val) val = 0

  var i
  if (typeof val === 'number') {
    for (i = start; i < end; ++i) {
      this[i] = val
    }
  } else {
    var bytes = Buffer.isBuffer(val)
      ? val
      : utf8ToBytes(new Buffer(val, encoding).toString())
    var len = bytes.length
    for (i = 0; i < end - start; ++i) {
      this[i + start] = bytes[i % len]
    }
  }

  return this
}


var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g

function base64clean (str) {
  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
  if (str.length < 2) return ''
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function stringtrim (str) {
  if (str.trim) return str.trim()
  return str.replace(/^\s+|\s+$/g, '')
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (string, units) {
  units = units || Infinity
  var codePoint
  var length = string.length
  var leadSurrogate = null
  var bytes = []

  for (var i = 0; i < length; ++i) {
    codePoint = string.charCodeAt(i)

    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      if (!leadSurrogate) {
        if (codePoint > 0xDBFF) {
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        } else if (i + 1 === length) {
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        }

        leadSurrogate = codePoint

        continue
      }

      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
        leadSurrogate = codePoint
        continue
      }

      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000
    } else if (leadSurrogate) {
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
    }

    leadSurrogate = null

    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break
      bytes.push(codePoint)
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break
      bytes.push(
        codePoint >> 0x6 | 0xC0,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break
      bytes.push(
        codePoint >> 0xC | 0xE0,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break
      bytes.push(
        codePoint >> 0x12 | 0xF0,
        codePoint >> 0xC & 0x3F | 0x80,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else {
      throw new Error('Invalid code point')
    }
  }

  return bytes
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str, units) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    if ((units -= 2) < 0) break

    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(base64clean(str))
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; ++i) {
    if ((i + offset >= dst.length) || (i >= src.length)) break
    dst[i + offset] = src[i]
  }
  return i
}

function isnan (val) {
  return val !== val 
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"base64-js":3,"ieee754":4,"isarray":5}],3:[function(require,module,exports){
'use strict'

exports.byteLength = byteLength
exports.toByteArray = toByteArray
exports.fromByteArray = fromByteArray

var lookup = []
var revLookup = []
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array

var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
for (var i = 0, len = code.length; i < len; ++i) {
  lookup[i] = code[i]
  revLookup[code.charCodeAt(i)] = i
}

revLookup['-'.charCodeAt(0)] = 62
revLookup['_'.charCodeAt(0)] = 63

function placeHoldersCount (b64) {
  var len = b64.length
  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4')
  }

  return b64[len - 2] === '=' ? 2 : b64[len - 1] === '=' ? 1 : 0
}

function byteLength (b64) {
  return b64.length * 3 / 4 - placeHoldersCount(b64)
}

function toByteArray (b64) {
  var i, j, l, tmp, placeHolders, arr
  var len = b64.length
  placeHolders = placeHoldersCount(b64)

  arr = new Arr(len * 3 / 4 - placeHolders)

  l = placeHolders > 0 ? len - 4 : len

  var L = 0

  for (i = 0, j = 0; i < l; i += 4, j += 3) {
    tmp = (revLookup[b64.charCodeAt(i)] << 18) | (revLookup[b64.charCodeAt(i + 1)] << 12) | (revLookup[b64.charCodeAt(i + 2)] << 6) | revLookup[b64.charCodeAt(i + 3)]
    arr[L++] = (tmp >> 16) & 0xFF
    arr[L++] = (tmp >> 8) & 0xFF
    arr[L++] = tmp & 0xFF
  }

  if (placeHolders === 2) {
    tmp = (revLookup[b64.charCodeAt(i)] << 2) | (revLookup[b64.charCodeAt(i + 1)] >> 4)
    arr[L++] = tmp & 0xFF
  } else if (placeHolders === 1) {
    tmp = (revLookup[b64.charCodeAt(i)] << 10) | (revLookup[b64.charCodeAt(i + 1)] << 4) | (revLookup[b64.charCodeAt(i + 2)] >> 2)
    arr[L++] = (tmp >> 8) & 0xFF
    arr[L++] = tmp & 0xFF
  }

  return arr
}

function tripletToBase64 (num) {
  return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F]
}

function encodeChunk (uint8, start, end) {
  var tmp
  var output = []
  for (var i = start; i < end; i += 3) {
    tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2])
    output.push(tripletToBase64(tmp))
  }
  return output.join('')
}

function fromByteArray (uint8) {
  var tmp
  var len = uint8.length
  var extraBytes = len % 3 
  var output = ''
  var parts = []
  var maxChunkLength = 16383 

  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)))
  }

  if (extraBytes === 1) {
    tmp = uint8[len - 1]
    output += lookup[tmp >> 2]
    output += lookup[(tmp << 4) & 0x3F]
    output += '=='
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + (uint8[len - 1])
    output += lookup[tmp >> 10]
    output += lookup[(tmp >> 4) & 0x3F]
    output += lookup[(tmp << 2) & 0x3F]
    output += '='
  }

  parts.push(output)

  return parts.join('')
}

},{}],4:[function(require,module,exports){
exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var nBits = -7
  var i = isLE ? (nBytes - 1) : 0
  var d = isLE ? -1 : 1
  var s = buffer[offset + i]

  i += d

  e = s & ((1 << (-nBits)) - 1)
  s >>= (-nBits)
  nBits += eLen
  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & ((1 << (-nBits)) - 1)
  e >>= (-nBits)
  nBits += mLen
  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity)
  } else {
    m = m + Math.pow(2, mLen)
    e = e - eBias
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
}

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
  var i = isLE ? 0 : (nBytes - 1)
  var d = isLE ? 1 : -1
  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0

  value = Math.abs(value)

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0
    e = eMax
  } else {
    e = Math.floor(Math.log(value) / Math.LN2)
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--
      c *= 2
    }
    if (e + eBias >= 1) {
      value += rt / c
    } else {
      value += rt * Math.pow(2, 1 - eBias)
    }
    if (value * c >= 2) {
      e++
      c /= 2
    }

    if (e + eBias >= eMax) {
      m = 0
      e = eMax
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen)
      e = e + eBias
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
      e = 0
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = (e << mLen) | m
  eLen += mLen
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128
}

},{}],5:[function(require,module,exports){
var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};

},{}],6:[function(require,module,exports){

module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}

},{}],7:[function(require,module,exports){
var process = module.exports = {};


var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        return setTimeout(fun, 0);
    }
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        return clearTimeout(marker);
    }
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; 
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],8:[function(require,module,exports){


(function(factory) {
	if (typeof define === 'function' && define.amd) {
		define([], factory);
	} else if (typeof module === 'object') {
		var HashMap = module.exports = factory();
		HashMap.HashMap = HashMap;
	} else {
		this.HashMap = factory();
	}
}(function() {

	function HashMap(other) {
		this.clear();
		switch (arguments.length) {
			case 0: break;
			case 1: this.copy(other); break;
			default: multi(this, arguments); break;
		}
	}

	var proto = HashMap.prototype = {
		constructor:HashMap,

		get:function(key) {
			var data = this._data[this.hash(key)];
			return data && data[1];
		},

		set:function(key, value) {
			this._data[this.hash(key)] = [key, value];
		},

		multi:function() {
			multi(this, arguments);
		},

		copy:function(other) {
			for (var key in other._data) {
				this._data[key] = other._data[key];
			}
		},

		has:function(key) {
			return this.hash(key) in this._data;
		},

		search:function(value) {
			for (var key in this._data) {
				if (this._data[key][1] === value) {
					return this._data[key][0];
				}
			}

			return null;
		},

		remove:function(key) {
			delete this._data[this.hash(key)];
		},

		type:function(key) {
			var str = Object.prototype.toString.call(key);
			var type = str.slice(8, -1).toLowerCase();
			if (type === 'domwindow' && !key) {
				return key + '';
			}
			return type;
		},

		keys:function() {
			var keys = [];
			this.forEach(function(value, key) { keys.push(key); });
			return keys;
		},

		values:function() {
			var values = [];
			this.forEach(function(value) { values.push(value); });
			return values;
		},

		count:function() {
			return this.keys().length;
		},

		clear:function() {
			this._data = {};
		},

		clone:function() {
			return new HashMap(this);
		},

		hash:function(key) {
			switch (this.type(key)) {
				case 'undefined':
				case 'null':
				case 'boolean':
				case 'number':
				case 'regexp':
					return key + '';

				case 'date':
					return '♣' + key.getTime();

				case 'string':
					return '♠' + key;

				case 'array':
					var hashes = [];
					for (var i = 0; i < key.length; i++) {
						hashes[i] = this.hash(key[i]);
					}
					return '♥' + hashes.join('⁞');

				default:
					if (!key._hmuid_) {
						key._hmuid_ = ++HashMap.uid;
						hide(key, '_hmuid_');
					}

					return '♦' + key._hmuid_;
			}
		},

		forEach:function(func, ctx) {
			for (var key in this._data) {
				var data = this._data[key];
				func.call(ctx || this, data[1], data[0]);
			}
		}
	};

	HashMap.uid = 0;


	for (var method in proto) {
		if (method === 'constructor' || !proto.hasOwnProperty(method)) {
			continue;
		}
		var fn = proto[method];
		if (fn.toString().indexOf('return ') === -1) {
			proto[method] = chain(fn);
		}
	}


	function multi(map, args) {
		for (var i = 0; i < args.length; i += 2) {
			map.set(args[i], args[i+1]);
		}
	}

	function chain(fn) {
		return function() {
			fn.apply(this, arguments);
			return this;
		};
	}

	function hide(obj, prop) {
		if (Object.defineProperty) {
			Object.defineProperty(obj, prop, {enumerable:false});
		}
	}

	return HashMap;
}));

},{}],9:[function(require,module,exports){
if (typeof Object.create === 'function') {
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}],10:[function(require,module,exports){

(function (root, definition) {
    "use strict";
    if (typeof module === 'object' && module.exports && typeof require === 'function') {
        module.exports = definition();
    } else if (typeof define === 'function' && typeof define.amd === 'object') {
        define(definition);
    } else {
        root.log = definition();
    }
}(this, function () {
    "use strict";
    var noop = function() {};
    var undefinedType = "undefined";

    function realMethod(methodName) {
        if (typeof console === undefinedType) {
            return false; 
        } else if (console[methodName] !== undefined) {
            return bindMethod(console, methodName);
        } else if (console.log !== undefined) {
            return bindMethod(console, 'log');
        } else {
            return noop;
        }
    }

    function bindMethod(obj, methodName) {
        var method = obj[methodName];
        if (typeof method.bind === 'function') {
            return method.bind(obj);
        } else {
            try {
                return Function.prototype.bind.call(method, obj);
            } catch (e) {
                return function() {
                    return Function.prototype.apply.apply(method, [obj, arguments]);
                };
            }
        }
    }


    function enableLoggingWhenConsoleArrives(methodName, level, loggerName) {
        return function () {
            if (typeof console !== undefinedType) {
                replaceLoggingMethods.call(this, level, loggerName);
                this[methodName].apply(this, arguments);
            }
        };
    }

    function replaceLoggingMethods(level, loggerName) {

        for (var i = 0; i < logMethods.length; i++) {
            var methodName = logMethods[i];
            this[methodName] = (i < level) ?
                noop :
                this.methodFactory(methodName, level, loggerName);
        }
    }

    function defaultMethodFactory(methodName, level, loggerName) {

        return realMethod(methodName) ||
               enableLoggingWhenConsoleArrives.apply(this, arguments);
    }

    var logMethods = [
        "trace",
        "debug",
        "info",
        "warn",
        "error"
    ];

    function Logger(name, defaultLevel, factory) {
      var self = this;
      var currentLevel;
      var storageKey = "loglevel";
      if (name) {
        storageKey += ":" + name;
      }

      function persistLevelIfPossible(levelNum) {
          var levelName = (logMethods[levelNum] || 'silent').toUpperCase();

          try {
              window.localStorage[storageKey] = levelName;
              return;
          } catch (ignore) {}

          try {
              window.document.cookie =
                encodeURIComponent(storageKey) + "=" + levelName + ";";
          } catch (ignore) {}
      }

      function getPersistedLevel() {
          var storedLevel;

          try {
              storedLevel = window.localStorage[storageKey];
          } catch (ignore) {}

          if (typeof storedLevel === undefinedType) {
              try {
                  var cookie = window.document.cookie;
                  var location = cookie.indexOf(
                      encodeURIComponent(storageKey) + "=");
                  if (location) {
                      storedLevel = /^([^;]+)/.exec(cookie.slice(location))[1];
                  }
              } catch (ignore) {}
          }

          if (self.levels[storedLevel] === undefined) {
              storedLevel = undefined;
          }

          return storedLevel;
      }



      self.levels = { "TRACE": 0, "DEBUG": 1, "INFO": 2, "WARN": 3,
          "ERROR": 4, "SILENT": 5};

      self.methodFactory = factory || defaultMethodFactory;

      self.getLevel = function () {
          return currentLevel;
      };

      self.setLevel = function (level, persist) {
          if (typeof level === "string" && self.levels[level.toUpperCase()] !== undefined) {
              level = self.levels[level.toUpperCase()];
          }
          if (typeof level === "number" && level >= 0 && level <= self.levels.SILENT) {
              currentLevel = level;
              if (persist !== false) {  
                  persistLevelIfPossible(level);
              }
              replaceLoggingMethods.call(self, level, name);
              if (typeof console === undefinedType && level < self.levels.SILENT) {
                  return "No console available for logging";
              }
          } else {
              throw "log.setLevel() called with invalid level: " + level;
          }
      };

      self.setDefaultLevel = function (level) {
          if (!getPersistedLevel()) {
              self.setLevel(level, false);
          }
      };

      self.enableAll = function(persist) {
          self.setLevel(self.levels.TRACE, persist);
      };

      self.disableAll = function(persist) {
          self.setLevel(self.levels.SILENT, persist);
      };

      var initialLevel = getPersistedLevel();
      if (initialLevel == null) {
          initialLevel = defaultLevel == null ? "WARN" : defaultLevel;
      }
      self.setLevel(initialLevel, false);
    }



    var defaultLogger = new Logger();

    var _loggersByName = {};
    defaultLogger.getLogger = function getLogger(name) {
        if (typeof name !== "string" || name === "") {
          throw new TypeError("You must supply a name when creating a logger.");
        }

        var logger = _loggersByName[name];
        if (!logger) {
          logger = _loggersByName[name] = new Logger(
            name, defaultLogger.getLevel(), defaultLogger.methodFactory);
        }
        return logger;
    };

    var _log = (typeof window !== undefinedType) ? window.log : undefined;
    defaultLogger.noConflict = function() {
        if (typeof window !== undefinedType &&
               window.log === defaultLogger) {
            window.log = _log;
        }

        return defaultLogger;
    };

    return defaultLogger;
}));

},{}],11:[function(require,module,exports){



(function(global) {
    "use strict";


    var Long = function(low, high, unsigned) {


        this.low = low|0;


        this.high = high|0;


        this.unsigned = !!unsigned;
    };



    Long.isLong = function(obj) {
        return (obj && obj instanceof Long) === true;
    };


    var INT_CACHE = {};


    var UINT_CACHE = {};


    Long.fromInt = function(value, unsigned) {
        var obj, cachedObj;
        if (!unsigned) {
            value = value | 0;
            if (-128 <= value && value < 128) {
                cachedObj = INT_CACHE[value];
                if (cachedObj)
                    return cachedObj;
            }
            obj = new Long(value, value < 0 ? -1 : 0, false);
            if (-128 <= value && value < 128)
                INT_CACHE[value] = obj;
            return obj;
        } else {
            value = value >>> 0;
            if (0 <= value && value < 256) {
                cachedObj = UINT_CACHE[value];
                if (cachedObj)
                    return cachedObj;
            }
            obj = new Long(value, (value | 0) < 0 ? -1 : 0, true);
            if (0 <= value && value < 256)
                UINT_CACHE[value] = obj;
            return obj;
        }
    };


    Long.fromNumber = function(value, unsigned) {
        unsigned = !!unsigned;
        if (isNaN(value) || !isFinite(value))
            return Long.ZERO;
        if (!unsigned && value <= -TWO_PWR_63_DBL)
            return Long.MIN_VALUE;
        if (!unsigned && value + 1 >= TWO_PWR_63_DBL)
            return Long.MAX_VALUE;
        if (unsigned && value >= TWO_PWR_64_DBL)
            return Long.MAX_UNSIGNED_VALUE;
        if (value < 0)
            return Long.fromNumber(-value, unsigned).negate();
        return new Long((value % TWO_PWR_32_DBL) | 0, (value / TWO_PWR_32_DBL) | 0, unsigned);
    };


    Long.fromBits = function(lowBits, highBits, unsigned) {
        return new Long(lowBits, highBits, unsigned);
    };


    Long.fromString = function(str, unsigned, radix) {
        if (str.length === 0)
            throw Error('number format error: empty string');
        if (str === "NaN" || str === "Infinity" || str === "+Infinity" || str === "-Infinity")
            return Long.ZERO;
        if (typeof unsigned === 'number') 
            radix = unsigned,
            unsigned = false;
        radix = radix || 10;
        if (radix < 2 || 36 < radix)
            throw Error('radix out of range: ' + radix);

        var p;
        if ((p = str.indexOf('-')) > 0)
            throw Error('number format error: interior "-" character: ' + str);
        else if (p === 0)
            return Long.fromString(str.substring(1), unsigned, radix).negate();

        var radixToPower = Long.fromNumber(Math.pow(radix, 8));

        var result = Long.ZERO;
        for (var i = 0; i < str.length; i += 8) {
            var size = Math.min(8, str.length - i);
            var value = parseInt(str.substring(i, i + size), radix);
            if (size < 8) {
                var power = Long.fromNumber(Math.pow(radix, size));
                result = result.multiply(power).add(Long.fromNumber(value));
            } else {
                result = result.multiply(radixToPower);
                result = result.add(Long.fromNumber(value));
            }
        }
        result.unsigned = unsigned;
        return result;
    };


    Long.fromValue = function(val) {
        if (typeof val === 'number')
            return Long.fromNumber(val);
        if (typeof val === 'string')
            return Long.fromString(val);
        if (Long.isLong(val))
            return val;
        return new Long(val.low, val.high, val.unsigned);
    };



    var TWO_PWR_16_DBL = 1 << 16;


    var TWO_PWR_24_DBL = 1 << 24;


    var TWO_PWR_32_DBL = TWO_PWR_16_DBL * TWO_PWR_16_DBL;


    var TWO_PWR_64_DBL = TWO_PWR_32_DBL * TWO_PWR_32_DBL;


    var TWO_PWR_63_DBL = TWO_PWR_64_DBL / 2;


    var TWO_PWR_24 = Long.fromInt(TWO_PWR_24_DBL);


    Long.ZERO = Long.fromInt(0);


    Long.UZERO = Long.fromInt(0, true);


    Long.ONE = Long.fromInt(1);


    Long.UONE = Long.fromInt(1, true);


    Long.NEG_ONE = Long.fromInt(-1);


    Long.MAX_VALUE = Long.fromBits(0xFFFFFFFF|0, 0x7FFFFFFF|0, false);


    Long.MAX_UNSIGNED_VALUE = Long.fromBits(0xFFFFFFFF|0, 0xFFFFFFFF|0, true);


    Long.MIN_VALUE = Long.fromBits(0, 0x80000000|0, false);


    Long.prototype.toInt = function() {
        return this.unsigned ? this.low >>> 0 : this.low;
    };


    Long.prototype.toNumber = function() {
        if (this.unsigned) {
            return ((this.high >>> 0) * TWO_PWR_32_DBL) + (this.low >>> 0);
        }
        return this.high * TWO_PWR_32_DBL + (this.low >>> 0);
    };


    Long.prototype.toString = function(radix) {
        radix = radix || 10;
        if (radix < 2 || 36 < radix)
            throw RangeError('radix out of range: ' + radix);
        if (this.isZero())
            return '0';
        var rem;
        if (this.isNegative()) { 
            if (this.equals(Long.MIN_VALUE)) {
                var radixLong = Long.fromNumber(radix);
                var div = this.div(radixLong);
                rem = div.multiply(radixLong).subtract(this);
                return div.toString(radix) + rem.toInt().toString(radix);
            } else
                return '-' + this.negate().toString(radix);
        }

        var radixToPower = Long.fromNumber(Math.pow(radix, 6), this.unsigned);
        rem = this;
        var result = '';
        while (true) {
            var remDiv = rem.div(radixToPower),
                intval = rem.subtract(remDiv.multiply(radixToPower)).toInt() >>> 0,
                digits = intval.toString(radix);
            rem = remDiv;
            if (rem.isZero())
                return digits + result;
            else {
                while (digits.length < 6)
                    digits = '0' + digits;
                result = '' + digits + result;
            }
        }
    };


    Long.prototype.getHighBits = function() {
        return this.high;
    };


    Long.prototype.getHighBitsUnsigned = function() {
        return this.high >>> 0;
    };


    Long.prototype.getLowBits = function() {
        return this.low;
    };


    Long.prototype.getLowBitsUnsigned = function() {
        return this.low >>> 0;
    };


    Long.prototype.getNumBitsAbs = function() {
        if (this.isNegative()) 
            return this.equals(Long.MIN_VALUE) ? 64 : this.negate().getNumBitsAbs();
        var val = this.high != 0 ? this.high : this.low;
        for (var bit = 31; bit > 0; bit--)
            if ((val & (1 << bit)) != 0)
                break;
        return this.high != 0 ? bit + 33 : bit + 1;
    };


    Long.prototype.isZero = function() {
        return this.high === 0 && this.low === 0;
    };


    Long.prototype.isNegative = function() {
        return !this.unsigned && this.high < 0;
    };


    Long.prototype.isPositive = function() {
        return this.unsigned || this.high >= 0;
    };


    Long.prototype.isOdd = function() {
        return (this.low & 1) === 1;
    };


    Long.prototype.isEven = function() {
        return (this.low & 1) === 0;
    };


    Long.prototype.equals = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        if (this.unsigned !== other.unsigned && (this.high >>> 31) === 1 && (other.high >>> 31) === 1)
            return false;
        return this.high === other.high && this.low === other.low;
    };


    Long.prototype.notEquals = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return !this.equals(other);
    };


    Long.prototype.lessThan = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return this.compare(other) < 0;
    };


    Long.prototype.lessThanOrEqual = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return this.compare(other) <= 0;
    };


    Long.prototype.greaterThan = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return this.compare(other) > 0;
    };


    Long.prototype.greaterThanOrEqual = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return this.compare(other) >= 0;
    };


    Long.prototype.compare = function(other) {
        if (this.equals(other))
            return 0;
        var thisNeg = this.isNegative(),
            otherNeg = other.isNegative();
        if (thisNeg && !otherNeg)
            return -1;
        if (!thisNeg && otherNeg)
            return 1;
        if (!this.unsigned)
            return this.subtract(other).isNegative() ? -1 : 1;
        return (other.high >>> 0) > (this.high >>> 0) || (other.high === this.high && (other.low >>> 0) > (this.low >>> 0)) ? -1 : 1;
    };


    Long.prototype.negate = function() {
        if (!this.unsigned && this.equals(Long.MIN_VALUE))
            return Long.MIN_VALUE;
        return this.not().add(Long.ONE);
    };


    Long.prototype.add = function(addend) {
        if (!Long.isLong(addend))
            addend = Long.fromValue(addend);


        var a48 = this.high >>> 16;
        var a32 = this.high & 0xFFFF;
        var a16 = this.low >>> 16;
        var a00 = this.low & 0xFFFF;

        var b48 = addend.high >>> 16;
        var b32 = addend.high & 0xFFFF;
        var b16 = addend.low >>> 16;
        var b00 = addend.low & 0xFFFF;

        var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
        c00 += a00 + b00;
        c16 += c00 >>> 16;
        c00 &= 0xFFFF;
        c16 += a16 + b16;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c32 += a32 + b32;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c48 += a48 + b48;
        c48 &= 0xFFFF;
        return Long.fromBits((c16 << 16) | c00, (c48 << 16) | c32, this.unsigned);
    };


    Long.prototype.subtract = function(subtrahend) {
        if (!Long.isLong(subtrahend))
            subtrahend = Long.fromValue(subtrahend);
        return this.add(subtrahend.negate());
    };


    Long.prototype.multiply = function(multiplier) {
        if (this.isZero())
            return Long.ZERO;
        if (!Long.isLong(multiplier))
            multiplier = Long.fromValue(multiplier);
        if (multiplier.isZero())
            return Long.ZERO;
        if (this.equals(Long.MIN_VALUE))
            return multiplier.isOdd() ? Long.MIN_VALUE : Long.ZERO;
        if (multiplier.equals(Long.MIN_VALUE))
            return this.isOdd() ? Long.MIN_VALUE : Long.ZERO;

        if (this.isNegative()) {
            if (multiplier.isNegative())
                return this.negate().multiply(multiplier.negate());
            else
                return this.negate().multiply(multiplier).negate();
        } else if (multiplier.isNegative())
            return this.multiply(multiplier.negate()).negate();

        if (this.lessThan(TWO_PWR_24) && multiplier.lessThan(TWO_PWR_24))
            return Long.fromNumber(this.toNumber() * multiplier.toNumber(), this.unsigned);


        var a48 = this.high >>> 16;
        var a32 = this.high & 0xFFFF;
        var a16 = this.low >>> 16;
        var a00 = this.low & 0xFFFF;

        var b48 = multiplier.high >>> 16;
        var b32 = multiplier.high & 0xFFFF;
        var b16 = multiplier.low >>> 16;
        var b00 = multiplier.low & 0xFFFF;

        var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
        c00 += a00 * b00;
        c16 += c00 >>> 16;
        c00 &= 0xFFFF;
        c16 += a16 * b00;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c16 += a00 * b16;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c32 += a32 * b00;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c32 += a16 * b16;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c32 += a00 * b32;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c48 += a48 * b00 + a32 * b16 + a16 * b32 + a00 * b48;
        c48 &= 0xFFFF;
        return Long.fromBits((c16 << 16) | c00, (c48 << 16) | c32, this.unsigned);
    };


    Long.prototype.div = function(divisor) {
        if (!Long.isLong(divisor))
            divisor = Long.fromValue(divisor);
        if (divisor.isZero())
            throw(new Error('division by zero'));
        if (this.isZero())
            return this.unsigned ? Long.UZERO : Long.ZERO;
        var approx, rem, res;
        if (this.equals(Long.MIN_VALUE)) {
            if (divisor.equals(Long.ONE) || divisor.equals(Long.NEG_ONE))
                return Long.MIN_VALUE;  
            else if (divisor.equals(Long.MIN_VALUE))
                return Long.ONE;
            else {
                var halfThis = this.shiftRight(1);
                approx = halfThis.div(divisor).shiftLeft(1);
                if (approx.equals(Long.ZERO)) {
                    return divisor.isNegative() ? Long.ONE : Long.NEG_ONE;
                } else {
                    rem = this.subtract(divisor.multiply(approx));
                    res = approx.add(rem.div(divisor));
                    return res;
                }
            }
        } else if (divisor.equals(Long.MIN_VALUE))
            return this.unsigned ? Long.UZERO : Long.ZERO;
        if (this.isNegative()) {
            if (divisor.isNegative())
                return this.negate().div(divisor.negate());
            return this.negate().div(divisor).negate();
        } else if (divisor.isNegative())
            return this.div(divisor.negate()).negate();

        res = Long.ZERO;
        rem = this;
        while (rem.greaterThanOrEqual(divisor)) {
            approx = Math.max(1, Math.floor(rem.toNumber() / divisor.toNumber()));

            var log2 = Math.ceil(Math.log(approx) / Math.LN2),
                delta = (log2 <= 48) ? 1 : Math.pow(2, log2 - 48),

                approxRes = Long.fromNumber(approx),
                approxRem = approxRes.multiply(divisor);
            while (approxRem.isNegative() || approxRem.greaterThan(rem)) {
                approx -= delta;
                approxRes = Long.fromNumber(approx, this.unsigned);
                approxRem = approxRes.multiply(divisor);
            }

            if (approxRes.isZero())
                approxRes = Long.ONE;

            res = res.add(approxRes);
            rem = rem.subtract(approxRem);
        }
        return res;
    };


    Long.prototype.modulo = function(divisor) {
        if (!Long.isLong(divisor))
            divisor = Long.fromValue(divisor);
        return this.subtract(this.div(divisor).multiply(divisor));
    };


    Long.prototype.not = function() {
        return Long.fromBits(~this.low, ~this.high, this.unsigned);
    };


    Long.prototype.and = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return Long.fromBits(this.low & other.low, this.high & other.high, this.unsigned);
    };


    Long.prototype.or = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return Long.fromBits(this.low | other.low, this.high | other.high, this.unsigned);
    };


    Long.prototype.xor = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return Long.fromBits(this.low ^ other.low, this.high ^ other.high, this.unsigned);
    };


    Long.prototype.shiftLeft = function(numBits) {
        if (Long.isLong(numBits))
            numBits = numBits.toInt();
        if ((numBits &= 63) === 0)
            return this;
        else if (numBits < 32)
            return Long.fromBits(this.low << numBits, (this.high << numBits) | (this.low >>> (32 - numBits)), this.unsigned);
        else
            return Long.fromBits(0, this.low << (numBits - 32), this.unsigned);
    };


    Long.prototype.shiftRight = function(numBits) {
        if (Long.isLong(numBits))
            numBits = numBits.toInt();
        if ((numBits &= 63) === 0)
            return this;
        else if (numBits < 32)
            return Long.fromBits((this.low >>> numBits) | (this.high << (32 - numBits)), this.high >> numBits, this.unsigned);
        else
            return Long.fromBits(this.high >> (numBits - 32), this.high >= 0 ? 0 : -1, this.unsigned);
    };


    Long.prototype.shiftRightUnsigned = function(numBits) {
        if (Long.isLong(numBits))
            numBits = numBits.toInt();
        numBits &= 63;
        if (numBits === 0)
            return this;
        else {
            var high = this.high;
            if (numBits < 32) {
                var low = this.low;
                return Long.fromBits((low >>> numBits) | (high << (32 - numBits)), high >>> numBits, this.unsigned);
            } else if (numBits === 32)
                return Long.fromBits(high, 0, this.unsigned);
            else
                return Long.fromBits(high >>> (numBits - 32), 0, this.unsigned);
        }
    };


    Long.prototype.toSigned = function() {
        if (!this.unsigned)
            return this;
        return new Long(this.low, this.high, false);
    };


    Long.prototype.toUnsigned = function() {
        if (this.unsigned)
            return this;
        return new Long(this.low, this.high, true);
    };

 if (typeof require === 'function' && typeof module === 'object' && module && typeof exports === 'object' && exports)
        module["exports"] = Long;
 else if (typeof define === 'function' && define["amd"])
        define(function() { return Long; });
 else
        (global["dcodeIO"] = global["dcodeIO"] || {})["Long"] = Long;

})(this);

},{}],12:[function(require,module,exports){

(function(define) { 'use strict';
define(function (require) {

	var makePromise = require('./makePromise');
	var Scheduler = require('./Scheduler');
	var async = require('./env').asap;

	return makePromise({
		scheduler: new Scheduler(async)
	});

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });

},{"./Scheduler":13,"./env":25,"./makePromise":27}],13:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {


	function Scheduler(async) {
		this._async = async;
		this._running = false;

		this._queue = this;
		this._queueLen = 0;
		this._afterQueue = {};
		this._afterQueueLen = 0;

		var self = this;
		this.drain = function() {
			self._drain();
		};
	}

	Scheduler.prototype.enqueue = function(task) {
		this._queue[this._queueLen++] = task;
		this.run();
	};

	Scheduler.prototype.afterQueue = function(task) {
		this._afterQueue[this._afterQueueLen++] = task;
		this.run();
	};

	Scheduler.prototype.run = function() {
		if (!this._running) {
			this._running = true;
			this._async(this.drain);
		}
	};

	Scheduler.prototype._drain = function() {
		var i = 0;
		for (; i < this._queueLen; ++i) {
			this._queue[i].run();
			this._queue[i] = void 0;
		}

		this._queueLen = 0;
		this._running = false;

		for (i = 0; i < this._afterQueueLen; ++i) {
			this._afterQueue[i].run();
			this._afterQueue[i] = void 0;
		}

		this._afterQueueLen = 0;
	};

	return Scheduler;

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],14:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	function TimeoutError (message) {
		Error.call(this);
		this.message = message;
		this.name = TimeoutError.name;
		if (typeof Error.captureStackTrace === 'function') {
			Error.captureStackTrace(this, TimeoutError);
		}
	}

	TimeoutError.prototype = Object.create(Error.prototype);
	TimeoutError.prototype.constructor = TimeoutError;

	return TimeoutError;
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));
},{}],15:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	makeApply.tryCatchResolve = tryCatchResolve;

	return makeApply;

	function makeApply(Promise, call) {
		if(arguments.length < 2) {
			call = tryCatchResolve;
		}

		return apply;

		function apply(f, thisArg, args) {
			var p = Promise._defer();
			var l = args.length;
			var params = new Array(l);
			callAndResolve({ f:f, thisArg:thisArg, args:args, params:params, i:l-1, call:call }, p._handler);

			return p;
		}

		function callAndResolve(c, h) {
			if(c.i < 0) {
				return call(c.f, c.thisArg, c.params, h);
			}

			var handler = Promise._handler(c.args[c.i]);
			handler.fold(callAndResolveNext, c, void 0, h);
		}

		function callAndResolveNext(c, x, h) {
			c.params[c.i] = x;
			c.i -= 1;
			callAndResolve(c, h);
		}
	}

	function tryCatchResolve(f, thisArg, args, resolver) {
		try {
			resolver.resolve(f.apply(thisArg, args));
		} catch(e) {
			resolver.reject(e);
		}
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));



},{}],16:[function(require,module,exports){

(function(define) { 'use strict';
define(function(require) {

	var state = require('../state');
	var applier = require('../apply');

	return function array(Promise) {

		var applyFold = applier(Promise);
		var toPromise = Promise.resolve;
		var all = Promise.all;

		var ar = Array.prototype.reduce;
		var arr = Array.prototype.reduceRight;
		var slice = Array.prototype.slice;


		Promise.any = any;
		Promise.some = some;
		Promise.settle = settle;

		Promise.map = map;
		Promise.filter = filter;
		Promise.reduce = reduce;
		Promise.reduceRight = reduceRight;

		Promise.prototype.spread = function(onFulfilled) {
			return this.then(all).then(function(array) {
				return onFulfilled.apply(this, array);
			});
		};

		return Promise;

		function any(promises) {
			var p = Promise._defer();
			var resolver = p._handler;
			var l = promises.length>>>0;

			var pending = l;
			var errors = [];

			for (var h, x, i = 0; i < l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					--pending;
					continue;
				}

				h = Promise._handler(x);
				if(h.state() > 0) {
					resolver.become(h);
					Promise._visitRemaining(promises, i, h);
					break;
				} else {
					h.visit(resolver, handleFulfill, handleReject);
				}
			}

			if(pending === 0) {
				resolver.reject(new RangeError('any(): array must not be empty'));
			}

			return p;

			function handleFulfill(x) {
				errors = null;
				this.resolve(x); 
			}

			function handleReject(e) {
				if(this.resolved) { 
					return;
				}

				errors.push(e);
				if(--pending === 0) {
					this.reject(errors);
				}
			}
		}

		function some(promises, n) {
			var p = Promise._defer();
			var resolver = p._handler;

			var results = [];
			var errors = [];

			var l = promises.length>>>0;
			var nFulfill = 0;
			var nReject;
			var x, i; 

			for(i=0; i<l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					continue;
				}
				++nFulfill;
			}

			n = Math.max(n, 0);
			nReject = (nFulfill - n + 1);
			nFulfill = Math.min(n, nFulfill);

			if(n > nFulfill) {
				resolver.reject(new RangeError('some(): array must contain at least '
				+ n + ' item(s), but had ' + nFulfill));
			} else if(nFulfill === 0) {
				resolver.resolve(results);
			}

			for(i=0; i<l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					continue;
				}

				Promise._handler(x).visit(resolver, fulfill, reject, resolver.notify);
			}

			return p;

			function fulfill(x) {
				if(this.resolved) { 
					return;
				}

				results.push(x);
				if(--nFulfill === 0) {
					errors = null;
					this.resolve(results);
				}
			}

			function reject(e) {
				if(this.resolved) { 
					return;
				}

				errors.push(e);
				if(--nReject === 0) {
					results = null;
					this.reject(errors);
				}
			}
		}

		function map(promises, f) {
			return Promise._traverse(f, promises);
		}

		function filter(promises, predicate) {
			var a = slice.call(promises);
			return Promise._traverse(predicate, a).then(function(keep) {
				return filterSync(a, keep);
			});
		}

		function filterSync(promises, keep) {
			var l = keep.length;
			var filtered = new Array(l);
			for(var i=0, j=0; i<l; ++i) {
				if(keep[i]) {
					filtered[j++] = Promise._handler(promises[i]).value;
				}
			}
			filtered.length = j;
			return filtered;

		}

		function settle(promises) {
			return all(promises.map(settleOne));
		}

		function settleOne(p) {
			var h = Promise._handler(p);
			if(h.state() === 0) {
				return toPromise(p).then(state.fulfilled, state.rejected);
			}

			h._unreport();
			return state.inspect(h);
		}

		function reduce(promises, f ) {
			return arguments.length > 2 ? ar.call(promises, liftCombine(f), arguments[2])
					: ar.call(promises, liftCombine(f));
		}

		function reduceRight(promises, f ) {
			return arguments.length > 2 ? arr.call(promises, liftCombine(f), arguments[2])
					: arr.call(promises, liftCombine(f));
		}

		function liftCombine(f) {
			return function(z, x, i) {
				return applyFold(f, void 0, [z,x,i]);
			};
		}
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

},{"../apply":15,"../state":28}],17:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function flow(Promise) {

		var resolve = Promise.resolve;
		var reject = Promise.reject;
		var origCatch = Promise.prototype['catch'];

		Promise.prototype.done = function(onResult, onError) {
			this._handler.visit(this._handler.receiver, onResult, onError);
		};

		Promise.prototype['catch'] = Promise.prototype.otherwise = function(onRejected) {
			if (arguments.length < 2) {
				return origCatch.call(this, onRejected);
			}

			if(typeof onRejected !== 'function') {
				return this.ensure(rejectInvalidPredicate);
			}

			return origCatch.call(this, createCatchFilter(arguments[1], onRejected));
		};

		function createCatchFilter(handler, predicate) {
			return function(e) {
				return evaluatePredicate(e, predicate)
					? handler.call(this, e)
					: reject(e);
			};
		}

		Promise.prototype['finally'] = Promise.prototype.ensure = function(handler) {
			if(typeof handler !== 'function') {
				return this;
			}

			return this.then(function(x) {
				return runSideEffect(handler, this, identity, x);
			}, function(e) {
				return runSideEffect(handler, this, reject, e);
			});
		};

		function runSideEffect (handler, thisArg, propagate, value) {
			var result = handler.call(thisArg);
			return maybeThenable(result)
				? propagateValue(result, propagate, value)
				: propagate(value);
		}

		function propagateValue (result, propagate, x) {
			return resolve(result).then(function () {
				return propagate(x);
			});
		}

		Promise.prototype['else'] = Promise.prototype.orElse = function(defaultValue) {
			return this.then(void 0, function() {
				return defaultValue;
			});
		};

		Promise.prototype['yield'] = function(value) {
			return this.then(function() {
				return value;
			});
		};

		Promise.prototype.tap = function(onFulfilledSideEffect) {
			return this.then(onFulfilledSideEffect)['yield'](this);
		};

		return Promise;
	};

	function rejectInvalidPredicate() {
		throw new TypeError('catch predicate must be a function');
	}

	function evaluatePredicate(e, predicate) {
		return isError(predicate) ? e instanceof predicate : predicate(e);
	}

	function isError(predicate) {
		return predicate === Error
			|| (predicate != null && predicate.prototype instanceof Error);
	}

	function maybeThenable(x) {
		return (typeof x === 'object' || typeof x === 'function') && x !== null;
	}

	function identity(x) {
		return x;
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],18:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function fold(Promise) {

		Promise.prototype.fold = function(f, z) {
			var promise = this._beget();

			this._handler.fold(function(z, x, to) {
				Promise._handler(z).fold(function(x, z, to) {
					to.resolve(f.call(this, z, x));
				}, x, this, to);
			}, z, promise._handler.receiver, promise._handler);

			return promise;
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],19:[function(require,module,exports){

(function(define) { 'use strict';
define(function(require) {

	var inspect = require('../state').inspect;

	return function inspection(Promise) {

		Promise.prototype.inspect = function() {
			return inspect(Promise._handler(this));
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

},{"../state":28}],20:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function generate(Promise) {

		var resolve = Promise.resolve;

		Promise.iterate = iterate;
		Promise.unfold = unfold;

		return Promise;

		function iterate(f, condition, handler, x) {
			return unfold(function(x) {
				return [x, f(x)];
			}, condition, handler, x);
		}

		function unfold(unspool, condition, handler, x) {
			return resolve(x).then(function(seed) {
				return resolve(condition(seed)).then(function(done) {
					return done ? seed : resolve(unspool(seed)).spread(next);
				});
			});

			function next(item, newSeed) {
				return resolve(handler(item)).then(function() {
					return unfold(unspool, condition, handler, newSeed);
				});
			}
		}
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],21:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function progress(Promise) {

		Promise.prototype.progress = function(onProgress) {
			return this.then(void 0, void 0, onProgress);
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],22:[function(require,module,exports){

(function(define) { 'use strict';
define(function(require) {

	var env = require('../env');
	var TimeoutError = require('../TimeoutError');

	function setTimeout(f, ms, x, y) {
		return env.setTimer(function() {
			f(x, y, ms);
		}, ms);
	}

	return function timed(Promise) {
		Promise.prototype.delay = function(ms) {
			var p = this._beget();
			this._handler.fold(handleDelay, ms, void 0, p._handler);
			return p;
		};

		function handleDelay(ms, x, h) {
			setTimeout(resolveDelay, ms, x, h);
		}

		function resolveDelay(x, h) {
			h.resolve(x);
		}

		Promise.prototype.timeout = function(ms, reason) {
			var p = this._beget();
			var h = p._handler;

			var t = setTimeout(onTimeout, ms, reason, p._handler);

			this._handler.visit(h,
				function onFulfill(x) {
					env.clearTimer(t);
					this.resolve(x); 
				},
				function onReject(x) {
					env.clearTimer(t);
					this.reject(x); 
				},
				h.notify);

			return p;
		};

		function onTimeout(reason, h, ms) {
			var e = typeof reason === 'undefined'
				? new TimeoutError('timed out after ' + ms + 'ms')
				: reason;
			h.reject(e);
		}

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

},{"../TimeoutError":14,"../env":25}],23:[function(require,module,exports){

(function(define) { 'use strict';
define(function(require) {

	var setTimer = require('../env').setTimer;
	var format = require('../format');

	return function unhandledRejection(Promise) {

		var logError = noop;
		var logInfo = noop;
		var localConsole;

		if(typeof console !== 'undefined') {
			localConsole = console;
			logError = typeof localConsole.error !== 'undefined'
				? function (e) { localConsole.error(e); }
				: function (e) { localConsole.log(e); };

			logInfo = typeof localConsole.info !== 'undefined'
				? function (e) { localConsole.info(e); }
				: function (e) { localConsole.log(e); };
		}

		Promise.onPotentiallyUnhandledRejection = function(rejection) {
			enqueue(report, rejection);
		};

		Promise.onPotentiallyUnhandledRejectionHandled = function(rejection) {
			enqueue(unreport, rejection);
		};

		Promise.onFatalRejection = function(rejection) {
			enqueue(throwit, rejection.value);
		};

		var tasks = [];
		var reported = [];
		var running = null;

		function report(r) {
			if(!r.handled) {
				reported.push(r);
				logError('Potentially unhandled rejection [' + r.id + '] ' + format.formatError(r.value));
			}
		}

		function unreport(r) {
			var i = reported.indexOf(r);
			if(i >= 0) {
				reported.splice(i, 1);
				logInfo('Handled previous rejection [' + r.id + '] ' + format.formatObject(r.value));
			}
		}

		function enqueue(f, x) {
			tasks.push(f, x);
			if(running === null) {
				running = setTimer(flush, 0);
			}
		}

		function flush() {
			running = null;
			while(tasks.length > 0) {
				tasks.shift()(tasks.shift());
			}
		}

		return Promise;
	};

	function throwit(e) {
		throw e;
	}

	function noop() {}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

},{"../env":25,"../format":26}],24:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function addWith(Promise) {
		Promise.prototype['with'] = Promise.prototype.withThis = function(receiver) {
			var p = this._beget();
			var child = p._handler;
			child.receiver = receiver;
			this._handler.chain(child, receiver);
			return p;
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));


},{}],25:[function(require,module,exports){
(function (process){

(function(define) { 'use strict';
define(function(require) {


	var MutationObs;
	var capturedSetTimeout = typeof setTimeout !== 'undefined' && setTimeout;

	var setTimer = function(f, ms) { return setTimeout(f, ms); };
	var clearTimer = function(t) { return clearTimeout(t); };
	var asap = function (f) { return capturedSetTimeout(f, 0); };

	if (isNode()) { 
		asap = function (f) { return process.nextTick(f); };

	} else if (MutationObs = hasMutationObserver()) { 
		asap = initMutationObserver(MutationObs);

	} else if (!capturedSetTimeout) { 
		var vertxRequire = require;
		var vertx = vertxRequire('vertx');
		setTimer = function (f, ms) { return vertx.setTimer(ms, f); };
		clearTimer = vertx.cancelTimer;
		asap = vertx.runOnLoop || vertx.runOnContext;
	}

	return {
		setTimer: setTimer,
		clearTimer: clearTimer,
		asap: asap
	};

	function isNode () {
		return typeof process !== 'undefined' &&
			Object.prototype.toString.call(process) === '[object process]';
	}

	function hasMutationObserver () {
		return (typeof MutationObserver === 'function' && MutationObserver) ||
			(typeof WebKitMutationObserver === 'function' && WebKitMutationObserver);
	}

	function initMutationObserver(MutationObserver) {
		var scheduled;
		var node = document.createTextNode('');
		var o = new MutationObserver(run);
		o.observe(node, { characterData: true });

		function run() {
			var f = scheduled;
			scheduled = void 0;
			f();
		}

		var i = 0;
		return function (f) {
			scheduled = f;
			node.data = (i ^= 1);
		};
	}
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

}).call(this,require('_process'))
},{"_process":7}],26:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return {
		formatError: formatError,
		formatObject: formatObject,
		tryStringify: tryStringify
	};

	function formatError(e) {
		var s = typeof e === 'object' && e !== null && e.stack ? e.stack : formatObject(e);
		return e instanceof Error ? s : s + ' (WARNING: non-Error used)';
	}

	function formatObject(o) {
		var s = String(o);
		if(s === '[object Object]' && typeof JSON !== 'undefined') {
			s = tryStringify(o, s);
		}
		return s;
	}

	function tryStringify(x, defaultValue) {
		try {
			return JSON.stringify(x);
		} catch(e) {
			return defaultValue;
		}
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],27:[function(require,module,exports){
(function (process){

(function(define) { 'use strict';
define(function() {

	return function makePromise(environment) {

		var tasks = environment.scheduler;
		var emitRejection = initEmitRejection();

		var objectCreate = Object.create ||
			function(proto) {
				function Child() {}
				Child.prototype = proto;
				return new Child();
			};

		function Promise(resolver, handler) {
			this._handler = resolver === Handler ? handler : init(resolver);
		}

		function init(resolver) {
			var handler = new Pending();

			try {
				resolver(promiseResolve, promiseReject, promiseNotify);
			} catch (e) {
				promiseReject(e);
			}

			return handler;

			function promiseResolve (x) {
				handler.resolve(x);
			}
			function promiseReject (reason) {
				handler.reject(reason);
			}

			function promiseNotify (x) {
				handler.notify(x);
			}
		}


		Promise.resolve = resolve;
		Promise.reject = reject;
		Promise.never = never;

		Promise._defer = defer;
		Promise._handler = getHandler;

		function resolve(x) {
			return isPromise(x) ? x
				: new Promise(Handler, new Async(getHandler(x)));
		}

		function reject(x) {
			return new Promise(Handler, new Async(new Rejected(x)));
		}

		function never() {
			return foreverPendingPromise; 
		}

		function defer() {
			return new Promise(Handler, new Pending());
		}


		Promise.prototype.then = function(onFulfilled, onRejected, onProgress) {
			var parent = this._handler;
			var state = parent.join().state();

			if ((typeof onFulfilled !== 'function' && state > 0) ||
				(typeof onRejected !== 'function' && state < 0)) {
				return new this.constructor(Handler, parent);
			}

			var p = this._beget();
			var child = p._handler;

			parent.chain(child, parent.receiver, onFulfilled, onRejected, onProgress);

			return p;
		};

		Promise.prototype['catch'] = function(onRejected) {
			return this.then(void 0, onRejected);
		};

		Promise.prototype._beget = function() {
			return begetFrom(this._handler, this.constructor);
		};

		function begetFrom(parent, Promise) {
			var child = new Pending(parent.receiver, parent.join().context);
			return new Promise(Handler, child);
		}


		Promise.all = all;
		Promise.race = race;
		Promise._traverse = traverse;

		function all(promises) {
			return traverseWith(snd, null, promises);
		}

		function traverse(f, promises) {
			return traverseWith(tryCatch2, f, promises);
		}

		function traverseWith(tryMap, f, promises) {
			var handler = typeof f === 'function' ? mapAt : settleAt;

			var resolver = new Pending();
			var pending = promises.length >>> 0;
			var results = new Array(pending);

			for (var i = 0, x; i < promises.length && !resolver.resolved; ++i) {
				x = promises[i];

				if (x === void 0 && !(i in promises)) {
					--pending;
					continue;
				}

				traverseAt(promises, handler, i, x, resolver);
			}

			if(pending === 0) {
				resolver.become(new Fulfilled(results));
			}

			return new Promise(Handler, resolver);

			function mapAt(i, x, resolver) {
				if(!resolver.resolved) {
					traverseAt(promises, settleAt, i, tryMap(f, x, i), resolver);
				}
			}

			function settleAt(i, x, resolver) {
				results[i] = x;
				if(--pending === 0) {
					resolver.become(new Fulfilled(results));
				}
			}
		}

		function traverseAt(promises, handler, i, x, resolver) {
			if (maybeThenable(x)) {
				var h = getHandlerMaybeThenable(x);
				var s = h.state();

				if (s === 0) {
					h.fold(handler, i, void 0, resolver);
				} else if (s > 0) {
					handler(i, h.value, resolver);
				} else {
					resolver.become(h);
					visitRemaining(promises, i+1, h);
				}
			} else {
				handler(i, x, resolver);
			}
		}

		Promise._visitRemaining = visitRemaining;
		function visitRemaining(promises, start, handler) {
			for(var i=start; i<promises.length; ++i) {
				markAsHandled(getHandler(promises[i]), handler);
			}
		}

		function markAsHandled(h, handler) {
			if(h === handler) {
				return;
			}

			var s = h.state();
			if(s === 0) {
				h.visit(h, void 0, h._unreport);
			} else if(s < 0) {
				h._unreport();
			}
		}

		function race(promises) {
			if(typeof promises !== 'object' || promises === null) {
				return reject(new TypeError('non-iterable passed to race()'));
			}

			return promises.length === 0 ? never()
				 : promises.length === 1 ? resolve(promises[0])
				 : runRace(promises);
		}

		function runRace(promises) {
			var resolver = new Pending();
			var i, x, h;
			for(i=0; i<promises.length; ++i) {
				x = promises[i];
				if (x === void 0 && !(i in promises)) {
					continue;
				}

				h = getHandler(x);
				if(h.state() !== 0) {
					resolver.become(h);
					visitRemaining(promises, i+1, h);
					break;
				} else {
					h.visit(resolver, resolver.resolve, resolver.reject);
				}
			}
			return new Promise(Handler, resolver);
		}


		function getHandler(x) {
			if(isPromise(x)) {
				return x._handler.join();
			}
			return maybeThenable(x) ? getHandlerUntrusted(x) : new Fulfilled(x);
		}

		function getHandlerMaybeThenable(x) {
			return isPromise(x) ? x._handler.join() : getHandlerUntrusted(x);
		}

		function getHandlerUntrusted(x) {
			try {
				var untrustedThen = x.then;
				return typeof untrustedThen === 'function'
					? new Thenable(untrustedThen, x)
					: new Fulfilled(x);
			} catch(e) {
				return new Rejected(e);
			}
		}

		function Handler() {}

		Handler.prototype.when
			= Handler.prototype.become
			= Handler.prototype.notify 
			= Handler.prototype.fail
			= Handler.prototype._unreport
			= Handler.prototype._report
			= noop;

		Handler.prototype._state = 0;

		Handler.prototype.state = function() {
			return this._state;
		};

		Handler.prototype.join = function() {
			var h = this;
			while(h.handler !== void 0) {
				h = h.handler;
			}
			return h;
		};

		Handler.prototype.chain = function(to, receiver, fulfilled, rejected, progress) {
			this.when({
				resolver: to,
				receiver: receiver,
				fulfilled: fulfilled,
				rejected: rejected,
				progress: progress
			});
		};

		Handler.prototype.visit = function(receiver, fulfilled, rejected, progress) {
			this.chain(failIfRejected, receiver, fulfilled, rejected, progress);
		};

		Handler.prototype.fold = function(f, z, c, to) {
			this.when(new Fold(f, z, c, to));
		};

		function FailIfRejected() {}

		inherit(Handler, FailIfRejected);

		FailIfRejected.prototype.become = function(h) {
			h.fail();
		};

		var failIfRejected = new FailIfRejected();

		function Pending(receiver, inheritedContext) {
			Promise.createContext(this, inheritedContext);

			this.consumers = void 0;
			this.receiver = receiver;
			this.handler = void 0;
			this.resolved = false;
		}

		inherit(Handler, Pending);

		Pending.prototype._state = 0;

		Pending.prototype.resolve = function(x) {
			this.become(getHandler(x));
		};

		Pending.prototype.reject = function(x) {
			if(this.resolved) {
				return;
			}

			this.become(new Rejected(x));
		};

		Pending.prototype.join = function() {
			if (!this.resolved) {
				return this;
			}

			var h = this;

			while (h.handler !== void 0) {
				h = h.handler;
				if (h === this) {
					return this.handler = cycle();
				}
			}

			return h;
		};

		Pending.prototype.run = function() {
			var q = this.consumers;
			var handler = this.handler;
			this.handler = this.handler.join();
			this.consumers = void 0;

			for (var i = 0; i < q.length; ++i) {
				handler.when(q[i]);
			}
		};

		Pending.prototype.become = function(handler) {
			if(this.resolved) {
				return;
			}

			this.resolved = true;
			this.handler = handler;
			if(this.consumers !== void 0) {
				tasks.enqueue(this);
			}

			if(this.context !== void 0) {
				handler._report(this.context);
			}
		};

		Pending.prototype.when = function(continuation) {
			if(this.resolved) {
				tasks.enqueue(new ContinuationTask(continuation, this.handler));
			} else {
				if(this.consumers === void 0) {
					this.consumers = [continuation];
				} else {
					this.consumers.push(continuation);
				}
			}
		};

		Pending.prototype.notify = function(x) {
			if(!this.resolved) {
				tasks.enqueue(new ProgressTask(x, this));
			}
		};

		Pending.prototype.fail = function(context) {
			var c = typeof context === 'undefined' ? this.context : context;
			this.resolved && this.handler.join().fail(c);
		};

		Pending.prototype._report = function(context) {
			this.resolved && this.handler.join()._report(context);
		};

		Pending.prototype._unreport = function() {
			this.resolved && this.handler.join()._unreport();
		};

		function Async(handler) {
			this.handler = handler;
		}

		inherit(Handler, Async);

		Async.prototype.when = function(continuation) {
			tasks.enqueue(new ContinuationTask(continuation, this));
		};

		Async.prototype._report = function(context) {
			this.join()._report(context);
		};

		Async.prototype._unreport = function() {
			this.join()._unreport();
		};

		function Thenable(then, thenable) {
			Pending.call(this);
			tasks.enqueue(new AssimilateTask(then, thenable, this));
		}

		inherit(Pending, Thenable);

		function Fulfilled(x) {
			Promise.createContext(this);
			this.value = x;
		}

		inherit(Handler, Fulfilled);

		Fulfilled.prototype._state = 1;

		Fulfilled.prototype.fold = function(f, z, c, to) {
			runContinuation3(f, z, this, c, to);
		};

		Fulfilled.prototype.when = function(cont) {
			runContinuation1(cont.fulfilled, this, cont.receiver, cont.resolver);
		};

		var errorId = 0;

		function Rejected(x) {
			Promise.createContext(this);

			this.id = ++errorId;
			this.value = x;
			this.handled = false;
			this.reported = false;

			this._report();
		}

		inherit(Handler, Rejected);

		Rejected.prototype._state = -1;

		Rejected.prototype.fold = function(f, z, c, to) {
			to.become(this);
		};

		Rejected.prototype.when = function(cont) {
			if(typeof cont.rejected === 'function') {
				this._unreport();
			}
			runContinuation1(cont.rejected, this, cont.receiver, cont.resolver);
		};

		Rejected.prototype._report = function(context) {
			tasks.afterQueue(new ReportTask(this, context));
		};

		Rejected.prototype._unreport = function() {
			if(this.handled) {
				return;
			}
			this.handled = true;
			tasks.afterQueue(new UnreportTask(this));
		};

		Rejected.prototype.fail = function(context) {
			this.reported = true;
			emitRejection('unhandledRejection', this);
			Promise.onFatalRejection(this, context === void 0 ? this.context : context);
		};

		function ReportTask(rejection, context) {
			this.rejection = rejection;
			this.context = context;
		}

		ReportTask.prototype.run = function() {
			if(!this.rejection.handled && !this.rejection.reported) {
				this.rejection.reported = true;
				emitRejection('unhandledRejection', this.rejection) ||
					Promise.onPotentiallyUnhandledRejection(this.rejection, this.context);
			}
		};

		function UnreportTask(rejection) {
			this.rejection = rejection;
		}

		UnreportTask.prototype.run = function() {
			if(this.rejection.reported) {
				emitRejection('rejectionHandled', this.rejection) ||
					Promise.onPotentiallyUnhandledRejectionHandled(this.rejection);
			}
		};


		Promise.createContext
			= Promise.enterContext
			= Promise.exitContext
			= Promise.onPotentiallyUnhandledRejection
			= Promise.onPotentiallyUnhandledRejectionHandled
			= Promise.onFatalRejection
			= noop;


		var foreverPendingHandler = new Handler();
		var foreverPendingPromise = new Promise(Handler, foreverPendingHandler);

		function cycle() {
			return new Rejected(new TypeError('Promise cycle'));
		}


		function ContinuationTask(continuation, handler) {
			this.continuation = continuation;
			this.handler = handler;
		}

		ContinuationTask.prototype.run = function() {
			this.handler.join().when(this.continuation);
		};

		function ProgressTask(value, handler) {
			this.handler = handler;
			this.value = value;
		}

		ProgressTask.prototype.run = function() {
			var q = this.handler.consumers;
			if(q === void 0) {
				return;
			}

			for (var c, i = 0; i < q.length; ++i) {
				c = q[i];
				runNotify(c.progress, this.value, this.handler, c.receiver, c.resolver);
			}
		};

		function AssimilateTask(then, thenable, resolver) {
			this._then = then;
			this.thenable = thenable;
			this.resolver = resolver;
		}

		AssimilateTask.prototype.run = function() {
			var h = this.resolver;
			tryAssimilate(this._then, this.thenable, _resolve, _reject, _notify);

			function _resolve(x) { h.resolve(x); }
			function _reject(x)  { h.reject(x); }
			function _notify(x)  { h.notify(x); }
		};

		function tryAssimilate(then, thenable, resolve, reject, notify) {
			try {
				then.call(thenable, resolve, reject, notify);
			} catch (e) {
				reject(e);
			}
		}

		function Fold(f, z, c, to) {
			this.f = f; this.z = z; this.c = c; this.to = to;
			this.resolver = failIfRejected;
			this.receiver = this;
		}

		Fold.prototype.fulfilled = function(x) {
			this.f.call(this.c, this.z, x, this.to);
		};

		Fold.prototype.rejected = function(x) {
			this.to.reject(x);
		};

		Fold.prototype.progress = function(x) {
			this.to.notify(x);
		};


		function isPromise(x) {
			return x instanceof Promise;
		}

		function maybeThenable(x) {
			return (typeof x === 'object' || typeof x === 'function') && x !== null;
		}

		function runContinuation1(f, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.become(h);
			}

			Promise.enterContext(h);
			tryCatchReject(f, h.value, receiver, next);
			Promise.exitContext();
		}

		function runContinuation3(f, x, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.become(h);
			}

			Promise.enterContext(h);
			tryCatchReject3(f, x, h.value, receiver, next);
			Promise.exitContext();
		}

		function runNotify(f, x, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.notify(x);
			}

			Promise.enterContext(h);
			tryCatchReturn(f, x, receiver, next);
			Promise.exitContext();
		}

		function tryCatch2(f, a, b) {
			try {
				return f(a, b);
			} catch(e) {
				return reject(e);
			}
		}

		function tryCatchReject(f, x, thisArg, next) {
			try {
				next.become(getHandler(f.call(thisArg, x)));
			} catch(e) {
				next.become(new Rejected(e));
			}
		}

		function tryCatchReject3(f, x, y, thisArg, next) {
			try {
				f.call(thisArg, x, y, next);
			} catch(e) {
				next.become(new Rejected(e));
			}
		}

		function tryCatchReturn(f, x, thisArg, next) {
			try {
				next.notify(f.call(thisArg, x));
			} catch(e) {
				next.notify(e);
			}
		}

		function inherit(Parent, Child) {
			Child.prototype = objectCreate(Parent.prototype);
			Child.prototype.constructor = Child;
		}

		function snd(x, y) {
			return y;
		}

		function noop() {}

		function initEmitRejection() {
			if(typeof process !== 'undefined' && process !== null
				&& typeof process.emit === 'function') {
				return function(type, rejection) {
					return type === 'unhandledRejection'
						? process.emit(type, rejection.value, rejection)
						: process.emit(type, rejection);
				};
			} else if(typeof self !== 'undefined' && typeof CustomEvent === 'function') {
				return (function(noop, self, CustomEvent) {
					var hasCustomEvent = false;
					try {
						var ev = new CustomEvent('unhandledRejection');
						hasCustomEvent = ev instanceof CustomEvent;
					} catch (e) {}

					return !hasCustomEvent ? noop : function(type, rejection) {
						var ev = new CustomEvent(type, {
							detail: {
								reason: rejection.value,
								key: rejection
							},
							bubbles: false,
							cancelable: true
						});

						return !self.dispatchEvent(ev);
					};
				}(noop, self, CustomEvent));
			}

			return noop;
		}

		return Promise;
	};
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

}).call(this,require('_process'))
},{"_process":7}],28:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return {
		pending: toPendingState,
		fulfilled: toFulfilledState,
		rejected: toRejectedState,
		inspect: inspect
	};

	function toPendingState() {
		return { state: 'pending' };
	}

	function toRejectedState(e) {
		return { state: 'rejected', reason: e };
	}

	function toFulfilledState(x) {
		return { state: 'fulfilled', value: x };
	}

	function inspect(handler) {
		var state = handler.state();
		return state === 0 ? toPendingState()
			 : state > 0   ? toFulfilledState(handler.value)
			               : toRejectedState(handler.value);
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],29:[function(require,module,exports){

(function(define) { 'use strict';
define(function (require) {

	var timed = require('./lib/decorators/timed');
	var array = require('./lib/decorators/array');
	var flow = require('./lib/decorators/flow');
	var fold = require('./lib/decorators/fold');
	var inspect = require('./lib/decorators/inspect');
	var generate = require('./lib/decorators/iterate');
	var progress = require('./lib/decorators/progress');
	var withThis = require('./lib/decorators/with');
	var unhandledRejection = require('./lib/decorators/unhandledRejection');
	var TimeoutError = require('./lib/TimeoutError');

	var Promise = [array, flow, fold, generate, progress,
		inspect, withThis, timed, unhandledRejection]
		.reduce(function(Promise, feature) {
			return feature(Promise);
		}, require('./lib/Promise'));

	var apply = require('./lib/apply')(Promise);


	when.promise     = promise;              
	when.resolve     = Promise.resolve;      
	when.reject      = Promise.reject;       

	when.lift        = lift;                 
	when['try']      = attempt;              
	when.attempt     = attempt;              

	when.iterate     = Promise.iterate;      
	when.unfold      = Promise.unfold;       

	when.join        = join;                 

	when.all         = all;                  
	when.settle      = settle;               

	when.any         = lift(Promise.any);    
	when.some        = lift(Promise.some);   
	when.race        = lift(Promise.race);   

	when.map         = map;                  
	when.filter      = filter;               
	when.reduce      = lift(Promise.reduce);       
	when.reduceRight = lift(Promise.reduceRight);  

	when.isPromiseLike = isPromiseLike;      

	when.Promise     = Promise;              
	when.defer       = defer;                


	when.TimeoutError = TimeoutError;

	function when(x, onFulfilled, onRejected, onProgress) {
		var p = Promise.resolve(x);
		if (arguments.length < 2) {
			return p;
		}

		return p.then(onFulfilled, onRejected, onProgress);
	}

	function promise(resolver) {
		return new Promise(resolver);
	}

	function lift(f) {
		return function() {
			for(var i=0, l=arguments.length, a=new Array(l); i<l; ++i) {
				a[i] = arguments[i];
			}
			return apply(f, this, a);
		};
	}

	function attempt(f ) {
		for(var i=0, l=arguments.length-1, a=new Array(l); i<l; ++i) {
			a[i] = arguments[i+1];
		}
		return apply(f, this, a);
	}

	function defer() {
		return new Deferred();
	}

	function Deferred() {
		var p = Promise._defer();

		function resolve(x) { p._handler.resolve(x); }
		function reject(x) { p._handler.reject(x); }
		function notify(x) { p._handler.notify(x); }

		this.promise = p;
		this.resolve = resolve;
		this.reject = reject;
		this.notify = notify;
		this.resolver = { resolve: resolve, reject: reject, notify: notify };
	}

	function isPromiseLike(x) {
		return x && typeof x.then === 'function';
	}

	function join() {
		return Promise.all(arguments);
	}

	function all(promises) {
		return when(promises, Promise.all);
	}

	function settle(promises) {
		return when(promises, Promise.settle);
	}

	function map(promises, mapFunc) {
		return when(promises, function(promises) {
			return Promise.map(promises, mapFunc);
		});
	}

	function filter(promises, predicate) {
		return when(promises, function(promises) {
			return Promise.filter(promises, predicate);
		});
	}

	return when;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });

},{"./lib/Promise":12,"./lib/TimeoutError":14,"./lib/apply":15,"./lib/decorators/array":16,"./lib/decorators/flow":17,"./lib/decorators/fold":18,"./lib/decorators/inspect":19,"./lib/decorators/iterate":20,"./lib/decorators/progress":21,"./lib/decorators/timed":22,"./lib/decorators/unhandledRejection":23,"./lib/decorators/with":24}],30:[function(require,module,exports){
var _interface = require('util/interface').interface;

var RecordContent = _interface('RecordContent', [
    'get',

    'records',

    'forEach'
]);

RecordContent.Record = _interface('Record', [
    'get',

    'fields',

    'forEach'
]);

RecordContent.Builder = _interface('RecordContentBuilder', [
    'add',

    'set',

    'build',

    'addAndBuild',

    'setAndBuild'
]);

RecordContent.Builder.Record = _interface('RecordContentRecordBuilder', [
    'add',

    'set'
]);

module.exports = RecordContent;

},{"util/interface":279}],31:[function(require,module,exports){
var _interface = require('util/interface').interface;
var DataType = require('../datatype');

module.exports = _interface('BinaryDataType', DataType, [
    'from'
]);


},{"../datatype":35,"util/interface":279}],32:[function(require,module,exports){
var _interface = require('util/interface').interface;

module.exports = _interface('BinaryDelta', [
    'hasChanges'
]);

},{"util/interface":279}],33:[function(require,module,exports){
var _interface = require('util/interface').interface;
var Bytes = require('../bytes');

module.exports = _interface('Binary', Bytes, [
    'get',

    'diff',

    'apply'
]);

},{"../bytes":34,"util/interface":279}],34:[function(require,module,exports){
var _interface = require('util/interface').interface;

module.exports = _interface('Bytes', [
    'length',

    'asBuffer',

    'copyTo'
]);

},{"util/interface":279}],35:[function(require,module,exports){
var _interface = require('util/interface').interface;

module.exports = _interface('DataType', [
    'name',

    'readValue',

    'writeValue',

    'deltaType'
]);

},{"util/interface":279}],36:[function(require,module,exports){
var _interface = require('util/interface').interface;

module.exports = _interface('DataTypes', [
    'binary',

    'json',

    'get'
]);

},{"util/interface":279}],37:[function(require,module,exports){
var _interface = require('util/interface').interface;

module.exports = _interface('DeltaType', [

    'name',

    'diff',

    'apply',

    'readDelta',

    'writeDelta',

    'noChange',

    'isValueCheaper'
]);

},{"util/interface":279}],38:[function(require,module,exports){
var _interface = require('util/interface').interface;
var DataType = require('../datatype');

module.exports = _interface('JSONDataType', DataType, [

    'from',

    'fromJsonString'
]);


},{"../datatype":35,"util/interface":279}],39:[function(require,module,exports){
var _interface = require('util/interface').interface;
var Bytes = require('../bytes');

module.exports = _interface('JSON', Bytes, [

    'get',

    'diff',

    'jsonDiff',

    'apply'
]);

},{"../bytes":34,"util/interface":279}],40:[function(require,module,exports){
var SessionImpl = require('session/session-impl');
var DataTypes = require('data/datatypes');
var Metadata = require('metadata/metadata');
var Topics = require('./topics/topics');
var TopicSelectors = require('./selectors/topic-selectors');
var ErrorReport = require('services/error-report');
var ClientControlOptions = require('./features/client-control-options');

var Emitter = require('events/emitter');
var Result = require('events/result');

var log = require('util/logger');
log.setLevel('SILENT');

var logger = log.create('Diffusion');

var diffusion = {
    version: '5.9.8',

    build: '8_01#46899',

    log: function (level) {
        log.setLevel(level);
    },

    connect: function (options) {
        var sessionImpl = new SessionImpl(options);
        var session = sessionImpl.get();

        var emitter = new Emitter();
        var r = new Result(emitter);

        function onClose(reason) {
            logger.debug('Session closed', reason);

            session.off({
                connect: onConnect,
                close: onClose
            });

            emitter.error(reason);
        }

        function onConnect(session) {
            logger.info('Session connected', session.toString());

            session.off({
                connect: onConnect,
                close: onClose
            });

            emitter.emit('connect', session);
        }

        session.on({
            connect: onConnect,
            close: onClose
        });

        sessionImpl.connect();

        return r;
    },

    datatypes: DataTypes,

    selectors: TopicSelectors,

    metadata: Metadata,

    topics: Topics,

    errorReport: ErrorReport,

    clients: ClientControlOptions
};

module.exports = diffusion;

},{"./features/client-control-options":48,"./selectors/topic-selectors":296,"./topics/topics":298,"data/datatypes":94,"events/emitter":103,"events/result":104,"metadata/metadata":129,"services/error-report":197,"session/session-impl":255,"util/logger":280}],41:[function(require,module,exports){
var _interface = require('util/interface').interface;

var ErrorReport = _interface('ErrorReport', [
    'message',

    'line',

    'column'
]);

module.exports = ErrorReport;

},{"util/interface":279}],42:[function(require,module,exports){
var _interface = require('util/interface').interface;
var Stream = require('./stream');

module.exports = _interface('FetchStream', Stream, [

]);
},{"./stream":44,"util/interface":279}],43:[function(require,module,exports){
var _interface = require('util/interface').interface;

module.exports = _interface('Result', [
    'then'
]);

},{"util/interface":279}],44:[function(require,module,exports){
var _interface = require('util/interface').interface;

module.exports = _interface('Stream', [
    'on',

    'off',

    'close'


]);

},{"util/interface":279}],45:[function(require,module,exports){
var _interface = require('util/interface').interface;
var Stream = require('./stream');

module.exports = _interface('Subscription', Stream, [
    'selector',

    'asType',

    'view',

    'transform',

    'close'





]);

},{"./stream":44,"util/interface":279}],46:[function(require,module,exports){
var _interface = require('util/interface').interface;
var Stream = require('./stream');

module.exports = _interface('TypedSubscription', Stream, [
    'selector',

    'close'




]);

},{"./stream":44,"util/interface":279}],47:[function(require,module,exports){
var _interface = require('util/interface').interface;
var Stream = require('./stream');

module.exports = _interface('View', Stream, [
    'get'
]);

},{"./stream":44,"util/interface":279}],48:[function(require,module,exports){

var GetSessionPropertiesKeys = {
    ALL_FIXED_PROPERTIES: ['*F'],
    ALL_USER_PROPERTIES: ['*U'],
    ALL_PROPERTIES: ['*F', '*U']
};

module.exports = {
    PropertyKeys: GetSessionPropertiesKeys
};

},{}],49:[function(require,module,exports){
var _interface = require('util/interface').interface;

var ClientControl = _interface('ClientControl', [
    'subscribe',

    'unsubscribe',

    'getSessionProperties',

    'setSessionPropertiesListener'
]);

ClientControl.SessionPropertiesListener = _interface('SessionPropertiesListener', [
    'onActive',

    'onClose',

    'onError',

    'onSessionOpen',

    'onSessionEvent',

    'onSessionClose'
]);



ClientControl.SessionEventType = {
    UPDATED : 0,
    RECONNECTED : 1,
    FAILED_OVER : 2,
    DISCONNECTED : 3
};

module.exports = ClientControl;

},{"util/interface":279}],50:[function(require,module,exports){
var _interface = require('util/interface').interface;
var Messages = _interface('Messaging', [
    'send',

    'listen',

    'addHandler'
]);

Messages.MessageStream = _interface('MessageStream',[
]);

Messages.MessageHandler = _interface('MessageHandler', [
    'onMessage',

    'onActive',

    'onClose'
]);

Messages.Priority = {
    NORMAL : 0,
    HIGH : 1,
    LOW : 2
};
Messages.Message = {};

Messages.SessionMessage = {};



module.exports = Messages;

},{"util/interface":279}],51:[function(require,module,exports){
var _interface = require('util/interface').interface;


var SystemPrincipal = _interface('SystemPrincipal', ['name', 'roles']);



var Role = _interface('Role', ['name', 'global', 'default', 'topic', 'inherits']);

var SecurityConfiguration = _interface('SecurityConfiguration', ['named', 'anonymous', 'roles']);

var SystemAuthenticationConfiguration = _interface('SystemAuthenticationConfiguration', ['principals', 'anonymous']);

var SecurityScriptBuilder = _interface('SecurityScriptBuilder', [
    'build',

    'setRolesForAnonymousSessions',

    'setRolesForNamedSessions',

    'setGlobalPermissions',

    'setDefaultTopicPermissions',

    'removeTopicPermissions',

    'setTopicPermissions',

    'setRoleIncludes'
]);

var SystemAuthenticationScriptBuilder = _interface('SystemAuthenticationScriptBuilder', [
    'build',

    'assignRoles',

    'addPrincipal',

    'setPassword',

    'verifyPassword',

    'removePrincipal',

    'allowAnonymousConnections',

    'denyAnonymousConnections',

    'abstainAnonymousConnections'
]);

var Security = _interface('Security', [
    'getPrincipal',

    'changePrincipal',

    'getSecurityConfiguration',

    'getSystemAuthenticationConfiguration',

    'updateSecurityStore',

    'updateAuthenticationStore',

    'securityScriptBuilder',

    'authenticationScriptBuilder'
]);

Security.SecurityScriptBuilder = SecurityScriptBuilder;
Security.SystemAuthenticationScriptBuilder = SystemAuthenticationScriptBuilder;

Security.SecurityConfiguration = SecurityConfiguration;
Security.SystemAuthenticationConfiguration = SystemAuthenticationConfiguration;

Security.Role = Role;
Security.SystemPrincipal = SystemPrincipal;

module.exports = Security;


},{"util/interface":279}],52:[function(require,module,exports){
var _interface = require('util/interface').interface;


module.exports.TopicControl = _interface('TopicControl', [
    'add',

    'remove',

    'removeSelector',

    'removeWithSession',

    'update',

    'registerUpdateSource',

    'addMissingTopicHandler'
]);

module.exports.MissingTopicHandler = _interface('MissingTopicHandler', [
    'onMissingTopic',

    'onRegister',

    'onClose',

    'onError'
]);

module.exports.MissingTopicNotification = _interface('MissingTopicNotification', [
    'path',

    'selector',

    'sessionID',

    'proceed',

    'cancel'
]);

module.exports.TopicUpdateHandler = _interface('TopicUpdateHandler', [
    'onRegister',

    'onActive',

    'onStandBy',

    'onClose'
]);

module.exports.Updater = _interface('Updater', [
    'update'
]);

module.exports.UpdateFailReason = {
    SUCCESS: 0,
    INCOMPATIBLE_UPDATE: 1,
    UPDATE_FAILED: 2,
    INVALID_UPDATER: 3,
    MISSING_TOPIC: 4,
    INVALID_ADDRESS: 5,
    DUPLICATES: 6,
    EXCLUSIVE_UPDATER_CONFLICT: 7
};



},{"util/interface":279}],53:[function(require,module,exports){
var _interface = require('util/interface').interface;

var Topics = _interface('Topics', [
    'subscribe',

    'unsubscribe',

    'stream',

    'view',

    'fetch'
]);

module.exports = Topics;

},{"util/interface":279}],54:[function(require,module,exports){
var _interface = require('util/interface').interface;

var Metadata = _interface('Metadata', [
    'String',

    'Integer',

    'Decimal',

    'Stateless',

    'RecordContent'
]);


Metadata.Stateless = _interface('Stateless', []);

Metadata.String = _interface('String', [
    'value'
]);

Metadata.Integer = _interface('Integer', [
    'value'
]);

Metadata.Decimal = _interface('Decimal', [
    'value',
    'scale'
]);

Metadata.RecordContent = _interface('RecordContent', [
    'occurs',

    'addRecord',

    'getRecord',

    'getRecords',

    'string',

    'integer',

    'decimal',

    'builder',

    'parse'
]);

Metadata.RecordContent.Record = _interface('Record', [
    'name',

    'occurs',

    'addField',

    'getField',

    'getFields'
]);

Metadata.RecordContent.Field = _interface('MField', [
    'name',
    'type',
    'occurs'
]);

module.exports = Metadata;

},{"util/interface":279}],55:[function(require,module,exports){

var ConnectionActivityMonitor = require('./connection-activity-monitor');

var pingTimeoutFactor = 2;

var noop = {
    onSystemPing: function () {},
    shutdown: function () {}
};

function connectionActivityMonitorFactory(connection, response) {
    if (response.systemPingPeriod > 0) {
        var pingTimeout = response.systemPingPeriod * pingTimeoutFactor;
        return new ConnectionActivityMonitor(pingTimeout, connection);
    }
    else {
        return noop;
    }
}

module.exports = connectionActivityMonitorFactory;

},{"./connection-activity-monitor":56}],56:[function(require,module,exports){

function ConnectionActivityMonitor(pingTimeout, connection) {
    var currentTimeout = null;

    this.onSystemPing = function() {
        if (currentTimeout !== null) {
            clearTimeout(currentTimeout);
            currentTimeout = setTimeout(connection.closeIdleConnection.bind(connection), pingTimeout);
        }
    };

    this.shutdown = function() {
        if (currentTimeout !== null) {
            clearTimeout(currentTimeout);
            currentTimeout = null;
        }
    };

    currentTimeout = setTimeout(connection.closeIdleConnection.bind(connection), pingTimeout);
}

module.exports = ConnectionActivityMonitor;

},{}],57:[function(require,module,exports){

function SessionActivityMonitor(connectionActivityMonitorFactory) {
    var currentConnectionMonitor = null;

    this.onNewConnection = function(connection, response) {
        currentConnectionMonitor = connectionActivityMonitorFactory(connection, response);
    };

    this.onConnectionClosed = function() {
        if (currentConnectionMonitor !== null) {
            currentConnectionMonitor.shutdown();
            currentConnectionMonitor = null;
        }
    };

    this.onSystemPing = function() {
        if (currentConnectionMonitor !== null) {
            currentConnectionMonitor.onSystemPing();
        }
    };
}

module.exports = {
    NOOP : {
        onNewConnection : function() {},
        onConnectionClosed : function() {},
        onSystemPing : function() {}
    },
    create : function(connectionActivityMonitorFactory) {
        return new SessionActivityMonitor(connectionActivityMonitorFactory);
    }
};

},{}],58:[function(require,module,exports){
module.exports.types = {
    UINT : 0,
    INT : 1,
    BYTES : 2,
    STRING : 3,
    ARRAY : 4,
    MAP : 5,
    SEMANTIC : 6,
    SIMPLE : 7,
    FLOAT : 7
};

module.exports.additional = {
    FALSE : 20,
    TRUE : 21,
    NULL : 22,
    UNDEFINED : 23,
    SIMPLE : 24,
    HALF_PRECISION : 25,
    SINGLE_PRECISION : 26,
    DOUBLE_PRECISION : 27,
    BREAK : 31
};

module.exports.tokens = {
    ARRAY_START : 0,
    ARRAY_END : 1,
    MAP_START : 2,
    MAP_END : 3,
    STRING_START : 4,
    STRING_END : 5,
    VALUE : 6
};

module.exports.isStructStart = function(token) {
     switch (token) {
        case module.exports.tokens.ARRAY_START :
        case module.exports.tokens.MAP_START :
        case module.exports.tokens.STRING_START :
            return true;
        default :
            return false;
    }
};

module.exports.isStructEnd = function(token) {
    switch (token) {
        case module.exports.tokens.ARRAY_END :
        case module.exports.tokens.MAP_END :
        case module.exports.tokens.STRING_END :
            return true;
        default :
            return false;
    }
};

},{}],59:[function(require,module,exports){
var ROOT = { length : -1, type : 'root', read : 0 };

function Context() {
    var stack = [];
    var tail = ROOT;

    this.type = function() {
        return tail.type;
    };

    this.read = function() {
        return tail.read;
    };

    this.expected = function() {
        return tail.length;
    };

    this.push = function(type, length) {
        length = length === undefined ? -1 : length;

        tail = { length : length,
                 type : type,
                 read : 0 };

        stack.push(tail);
    };

    this.pop = function() {
        var prev = stack.pop();

        if (stack.length) {
            tail = stack[stack.length - 1];
        } else {
            tail = ROOT;
        }

        return prev;
    };

    this.next = function() {
        if (this.hasRemaining()) {
            tail.read++;
        } else {
            throw new Error('Exceeded expected collection limit');
        }
    };

    this.break = function() {
        if (this.acceptsBreakMarker()) {
            tail.length = 0;
            tail.read = 0;
        }
    };

    this.hasRemaining = function() {
        return tail.length === -1 ? true : tail.length > tail.read;
    };

    this.acceptsBreakMarker = function() {
        return tail !== ROOT && tail.length === -1;
    };
}

module.exports = Context;

},{}],60:[function(require,module,exports){
(function (Buffer){
var Tokeniser = require('cbor/tokeniser');
var consts = require('cbor/consts');

var tokens = consts.tokens,
    types = consts.types;

function decode(token, tokeniser)  {
    switch (token.type) {
        case tokens.VALUE :
            return token.value;
        case tokens.MAP_START :
            var obj = {};

            while (true) {
                var field = tokeniser.nextToken();

                               if (field === null) {
                    throw new Error('Unexpected EOF (reading: map key)');
                }

                if (field.type === tokens.MAP_END) {
                    break;
                }

                var value = tokeniser.nextToken();

                if (value === null || value.type === tokens.MAP_END) {
                    throw new Error('Unexpected EOF (reading: map value)');
                }

                obj[decode(field, tokeniser)] = decode(value, tokeniser);
            }

                        return obj;
        case tokens.ARRAY_START :
            var arr = [];

            while (true) {
                var element = tokeniser.nextToken();

                if (element === null) {
                    throw new Error('Unexpected EOF (reading: array value)');
                }

                if (element.type === tokens.ARRAY_END) {
                    break;
                }

                arr.push(decode(element, tokeniser));
            }

            return arr;
        case tokens.STRING_START :
            var chunks = [];

            while (true) {
                var chunk = tokeniser.nextToken();

                if (chunk === null) {
                    throw new Error('Unexpected EOF (reading: indefinite-length string');
                }

                if (chunk.type === tokens.STRING_END) {
                    break;
                }

                if (chunk.header.type !== token.header.type) {
                    throw new Error('Unexpected chunk type (' + chunk.header.type + ') within string');
                }

                chunks.push(chunk.value);
            }

            var joined;

            if (token.header.type === types.BYTES) {
                joined = Buffer.concat(chunks);
            }

            if (token.header.type === types.STRING) {
                joined = chunks.join('');
            }

            return joined;
        default :
            throw new Error('Unexpected token: ' + JSON.stringify(token));
    }
}

module.exports = function Decoder(initial, offset, length) {
     var tokeniser = (Buffer.isBuffer(initial)) ? new Tokeniser(initial, offset, length) : initial;

     this.hasRemaining = tokeniser.hasRemaining;

     this.nextValue = function() {
         if (tokeniser.hasRemaining()) {
             return decode(tokeniser.nextToken(), tokeniser);
         } else {
             throw new Error('Token stream exhausted');
         }
     };
};


}).call(this,require("buffer").Buffer)
},{"buffer":2,"cbor/consts":58,"cbor/tokeniser":62}],61:[function(require,module,exports){
(function (Buffer){
var BufferOutputStream = require('io/buffer-output-stream');
var consts = require('cbor/consts');

var additional = consts.additional,
    types = consts.types;

function Encoder(initial) {
     var bos = initial||new BufferOutputStream();
     var self = this;

     function writeByte(b) {
         bos.write(b);
     }

      function writeBuffer(buf, offset, length) {
         offset = offset || 0;
         length = length === undefined ? buf.length : length;

         bos.writeMany(buf, offset, length);
     }

     function writeUint16(v) {
         writeByte((v >> 8) & 0xff);
         writeByte(v & 0xff);
     }

     function writeUint32(v) {
         writeUint16((v >> 16) & 0xffff);
         writeUint16(v & 0xffff);
     }

     function writeUint64(v) {
         writeUint32(Math.floor(v / 4294967296));
         writeUint32(v % 4294967296);
     }

    function writeBreakHeader(type) {
        writeByte(type << 5 | additional.BREAK);
    }

     function writeToken(type, val) {
         var first = type << 5;

         if (val < 24) {
             writeByte(first|val);
         } else if (val < 256) {
             writeByte(first|24);
             writeByte(val);
         } else if (val < 65536) {
             writeByte(first|25);
             writeUint16(val);
         } else if (val < 4294967296) {
             writeByte(first|26);
             writeUint32(val);
         } else {
             writeByte(first|27);
             writeUint64(val);
         }
     }

     function writeNumber(val) {
         if (val.toString().indexOf(".") > -1) {
             var buf = new Buffer(8);
             buf.writeDoubleBE(val, 0);

             writeByte((types.SIMPLE << 5) | additional.DOUBLE_PRECISION);
             writeBuffer(buf);
         } else {
             if (val < 0) {
                 writeToken(types.INT, -1 - val);
             } else {
                 writeToken(types.UINT, val);
             }
         }
     }

     function writeString(val) {
         var buf = new Buffer(val, 'utf-8');

                  writeToken(types.STRING, buf.length);
         writeBuffer(buf);
     }

     function writeBinary(val, offset, length) {
         length = length === undefined ? val.length : length;

         writeToken(types.BYTES, length);
         writeBuffer(val, offset, length);
     }

     function writeBoolean(val) {
         if (val) {
             writeToken(types.SIMPLE, additional.TRUE);
         } else {
             writeToken(types.SIMPLE, additional.FALSE);
         }
     } 

     function writeUndefined() {
         writeToken(types.SIMPLE, additional.UNDEFINED);
     }

          function writeNull() {
         writeToken(types.SIMPLE, additional.NULL);
     }

     function writeObject(val) {
         var keys = Object.keys(val);

         self.startObject(keys.length);

         for (var i = 0; i < keys.length; ++i) {
             self.encode(keys[i]);
             self.encode(val[keys[i]]);
         }
     }

     function writeArray(val) {
         self.startArray(val.length);

         for (var i = 0; i < val.length; ++i) {
             self.encode(val[i]);
         }
     }

    this.break = function() {
        writeBreakHeader(types.SIMPLE);

        return self;
    };

    this.startArray = function(length) {
        if (length === undefined) {
            writeBreakHeader(types.ARRAY);
        } else {
            writeToken(types.ARRAY, length);
        }

        return self;
    };

    this.startObject = function(length) {
        if (length === undefined) {
            writeBreakHeader(types.MAP);
        } else {
            writeToken(types.MAP, length);
        }

        return self;
    };

     this.encode = function(value, offset, length) {
         if (value === null) {
             writeNull();
         } else if (value === undefined) {
             writeUndefined();
         } else if (value === true) {
             writeBoolean(true);
         } else if (value === false) {
             writeBoolean(false);
         } else {
             switch (typeof value) {
                 case 'string' :
                     writeString(value);
                     break;
                 case 'number' :
                     writeNumber(value);
                     break;
                 case 'object' :
                     if (Buffer.isBuffer(value)) {
                         writeBinary(value, offset, length);
                     } else if (Array.isArray(value)) {
                         writeArray(value); 
                     } else {
                         writeObject(value);
                     }
             }
         }

         return self;
     };

     this.flush = function() {
         var res = bos.getBuffer(); 
         bos = new BufferOutputStream();
         return res;
     };
}

module.exports = Encoder;

}).call(this,require("buffer").Buffer)
},{"buffer":2,"cbor/consts":58,"io/buffer-output-stream":124}],62:[function(require,module,exports){
(function (Buffer){
var Context = require('./context');
var consts = require('./consts');

var additional = consts.additional,
    tokens = consts.tokens,
    types = consts.types;

var BREAK_FLAG = new Error();

var EMPTY_BUFFER = new Buffer([]);

function Tokeniser(data, offset, length) {
    length = length === undefined ? data.length : length;
    offset = offset || 0;

    var context = new Context();
    var token;
    var type;

    var self = this;
    var pos = offset;

    this.reset = function() {
        context = new Context();
        token = undefined;
        type = undefined;
        pos = offset;
    };

    this.hasRemaining = function() {
        var ctx = context.type();
        var len = ctx === 'root' ? length : length + 1;

        return pos < offset + len;
    };

    this.offset = function() {
        return pos;
    };

    this.getContext = function() {
        return context;
    };

    this.getToken = function() {
        return token;
    };

    this.nextToken = function() {
        if (!self.hasRemaining()) {
            return null;
        }

        var ctx = context.type();
        var previousPos = pos;

        if (ctx !== 'root' && !context.hasRemaining()) {
            switch (ctx) {
                case 'object' :
                    type = tokens.MAP_END;
                    break;
                case 'array' :
                    type = tokens.ARRAY_END;
                    break;
                case 'string' :
                    type = tokens.STRING_END;
                    break;
            }

            context.pop();

            return {
                pos : pos,
                type : type,
                getBuffer : function() {
                    return EMPTY_BUFFER;
                }
            };
        } else {
            context.next();
        }

        var header = readHeader();
        var value;

        switch (header.type) {
            case types.INT :
            case types.UINT :
            case types.FLOAT :
            case types.SIMPLE :
                type = tokens.VALUE;
                value = readValue(header);
                break;
            case types.BYTES :
            case types.STRING :
                if (header.raw === additional.BREAK) {
                    context.push('string', -1);
                    type = tokens.STRING_START;
                } else {
                    type = tokens.VALUE;
                    value = readValue(header);
                }

                break;
            case types.ARRAY :
                context.push('array', readCollectionLength(header));
                type = tokens.ARRAY_START;
                break;
            case types.MAP :
                var len = readCollectionLength(header);

                if (len >= 0) {
                    len = len * 2;
                }

                context.push('object', len);
                type = tokens.MAP_START;
                break;
            case types.SEMANTIC :
                return self.nextToken();
            default :
                throw new Error('Unknown CBOR header type: ' + header.type);
        }

        if (value === BREAK_FLAG) {
            if (context.acceptsBreakMarker()) {
                context.break();
                return self.nextToken();
            } else {
                throw new Error("Unexpected break flag outside of indefinite-length context");
            }
        }

        token = {
            pos : previousPos,
            type : type,
            value : value,
            header : header,
            length : pos,
            getBuffer : function() {
                return data.slice(this.pos, this.length);
            }
        };

        return token;
    };


    function readValue(header) {
        switch (header.type) {
            case types.UINT :
                return readHeaderValue(header);
            case types.INT :
                return -1 - readHeaderValue(header);
            case types.BYTES :
                return readBuffer(readHeaderValue(header)); 
            case types.STRING :
                return readBuffer(readHeaderValue(header)).toString('utf-8');
            case types.SIMPLE :
                return readSimpleValue(header.raw);
        }
    }

    function readSimpleValue(type) {
        switch (type) {
            case additional.TRUE :
                return true;
            case additional.FALSE :
                return false;
            case additional.NULL :
                return null;
            case additional.BREAK :
                return BREAK_FLAG;
            case additional.HALF_PRECISION :
                return readFloat16();
            case additional.SINGLE_PRECISION :
                return readFloat32();
            case additional.DOUBLE_PRECISION :
                return readFloat64();
        }
    }

    function readByte() {
        if (pos < data.length) {
            return data[pos++];
        } else {
            throw new Error('Exhausted token stream');
        }
    }

    function readBuffer(len) {
        return data.slice(pos, pos += len);
    }

    function readUint16() {
        var res = data.readUInt16BE(pos);
        pos += 2;

        return res;
    }

    function readUint32() {
        var res = data.readUInt32BE(pos);
        pos += 4;

        return res;
    }

    function readUint64() {
        return readUint32() * 4294967296 +
               readUint32();
    }

    function readFloat16() {
        var h = readUint16();
        var f = (h & 0x03ff);
        var s = (h & 0x8000) >> 15;
        var e = (h & 0x7c00) >> 10;

        var sign = s ? -1 : 1;

        if (e === 0) {
            return sign * Math.pow(2, -14) * (f / Math.pow(2, 10));
        } else if (e === 0x1f) {
            return f ? NaN : sign * Infinity;
        }

        return sign * Math.pow(2, e - 15) * (1 + (f / Math.pow(2, 10)));
    }

    function readFloat32() {
        var res = data.readFloatBE(pos);
        pos += 4;

        return res;
    }

    function readFloat64() {
        var res = data.readDoubleBE(pos); 
        pos += 8;

        return res;
    }

    function readHeader() {
        var header = readByte();

                return {
            type : header >> 5, 
            raw : header & 0x1F 
        };
    }

    function readHeaderValue(header) {
        var low = header.raw;

        if (low < 24) {
            return low;
        } else if (low === additional.SIMPLE) {
            return readByte();
        } else if (low === additional.HALF_PRECISION) {
            return readUint16();
        } else if (low === additional.SINGLE_PRECISION) {
            return readUint32();
        } else if (low === additional.DOUBLE_PRECISION) {
            return readUint64();
        } else if (low === additional.BREAK) {
            return BREAK_FLAG;
        }
    }

    function readCollectionLength(header) {
        var l = readHeaderValue(header);

        if (l === BREAK_FLAG) {
            return -1;
        }

        return l;
    }
}

module.exports = Tokeniser;

}).call(this,require("buffer").Buffer)
},{"./consts":58,"./context":59,"buffer":2}],63:[function(require,module,exports){
module.exports = {
    COMMUNICATION_FAILURE : {
        id : 100,
        reason : "Communication with server failed"
    },
    SESSION_CLOSED : {
        id : 101,
        reason : "Session is closed"
    },
    REQUEST_TIME_OUT : {
        id : 102,
        reason : "Request time out"
    },
    ACCESS_DENIED : {
        id : 103,
        reason : "Access denied"
    },
    TOPIC_TREE_REGISTRATION_CONFLICT : {
        id : 200,
        reason : "A conflicting, pre-existing registration exists on the same branch of the topic tree"
    }
};
},{}],64:[function(require,module,exports){
var Emitter = require('events/emitter');

var sessionActivityMonitorModule = require('activity/session-activity-monitor');
var ServiceAdapter = require('client/service-adapter');
var ServiceLocator = require('client/service-locator');

var StreamRegistry = require('routing/stream-registry');
var TopicRouting = require('routing/topic-routing');
var TopicCache = require('routing/topic-cache');

var DataTypes = require('data/datatypes');

var serialisers = require('services/serialisers');

var ConnectionRequest = require('protocol/connection-request');
var ResponseCode = require('protocol/response-code');

var CloseReason = require('v4-stack/close-reason');
var Transports = require('transports/transports');
var Aliases = require('v4-stack/aliases');

var log = require('util/logger').create('Internal Session');
var Options = require('../../options');

var curry = require('util/function').curry;
var FSM = require('util/fsm');

var connectionActivityMonitorFactory = require('activity/connection-activity-monitor-factory');

function InternalSession(conversationSet, serviceRegistry, connectionFactory, opts) {
    var emitter = Emitter.assign(this);

        var principal = "";
    var sessionID;
    var token;
    var sessionActivityMonitor;

        var fsm = FSM.create('initialising', {
        initialising    : ['connecting'],
        connecting      : ['connected', 'closing'],
        connected       : ['disconnected', 'closing'],
        disconnected    : ['reconnecting', 'closing'],
        reconnecting    : ['connected', 'closing'],
        closing         : ['closed'],
        closed          : []
    });

        fsm.on('change', function(previous, current) {
        log.info('State changed: ' + previous + ' -> ' + current);
    });

    var self = this;

    this.getState = function() {
        return fsm.state;
    };

    this.getConversationSet = function() {
        return conversationSet;
    };

    this.isConnected = function() { 
        return fsm.state === 'connected';
    };

    this.getServiceRegistry = function() {
        return serviceRegistry;
    };

    this.getSessionId = function() {
        return sessionID;
    };

    this.setPrincipal = function(newPrincipal) {
        principal = newPrincipal;
    };

    this.checkConnected = function(emitter) {
        if (self.isConnected()) {
            return true;
        } else {
            emitter.error(new Error('The session is not connected. Operations are not possible at this time.'));
            return false;
        }
    };

    var transports = Transports.create();

    transports.on({
        'transport-selected' : function(name) {
            emitter.emit('transport-selected', name);
        },
        cascade : function() {
            emitter.emit('cascade');
        }
    });

    var connection = connectionFactory.create(Aliases.create(), transports, opts.reconnect.timeout, 256);

    var serviceAdapter = new ServiceAdapter(this, serialisers, connection.send);
    var serviceLocator = new ServiceLocator(this, serialisers, serviceAdapter);

    var topicCache = new TopicCache(DataTypes);
    var topicStreamRegistry = new StreamRegistry(topicCache);
    var topicRouting = new TopicRouting(this, serviceAdapter, conversationSet, topicCache, topicStreamRegistry);

    serviceRegistry.addListener(serviceAdapter.addService);

    function close(reason) {
        if (fsm.change('closed')) {
            conversationSet.discardAll(reason);
            emitter.close(reason);
        } else {
            log.debug('Unable to handle session close, session state: ', fsm.state);
        }
    }

    var attempts = 0;
    var reconnect = function(opts) {
        if (fsm.change('reconnecting')) {
            log.info('Reconnect attempt (' + (++attempts) + ')');

            var request = ConnectionRequest.reconnect(
                token,
                connection.getAvailableSequence(),
                connection.lastReceivedSequence);

            connection.connect(request, opts, opts.reconnect.timeout);
        } else {
            log.debug('Unable to attempt reconnect, session state: ', fsm.state);
        }
    }; 

    var abort = function(reason) {
        if (fsm.change('closing')) {
            log.debug('Aborting reconnect');
            close(reason || CloseReason.RECONNECT_ABORTED);
        } else {
            log.debug('Unable to abort reconnect, session state: ', fsm.state);
        }
    };

    function replaceConversationSet(err) {
        conversationSet.replace(err);
    }

    this.connect = function() {
        if (fsm.change('connecting')) {
            var reconnectTimeout;

            var request = ConnectionRequest.connect();

            connection.on('data', topicRouting.route);

            connection.on('close', close);

            if (opts.activityMonitor) {
                sessionActivityMonitor = sessionActivityMonitorModule.create(connectionActivityMonitorFactory);
            } else {
                sessionActivityMonitor = sessionActivityMonitorModule.NOOP;
            }

            connection.on('disconnect', function(reason) {
                log.debug('Connection disconnected, reason: ', reason);

                if (fsm.change('disconnected') || fsm.state === 'reconnecting') {
                    sessionActivityMonitor.onConnectionClosed();
                    if (opts.reconnect.timeout > 0 && reason.canReconnect) {
                        opts.token = token;

                        if (fsm.state === 'disconnected') {
                            emitter.emit('disconnect', reason);

                            reconnectTimeout = setTimeout(function() {
                               if (fsm.state !== 'connected') {
                                   abort(CloseReason.CONNECTION_TIMEOUT); 
                               }
                            }, opts.reconnect.timeout);
                        }

                        opts.reconnect.strategy(curry(reconnect, opts), abort, reason);
                    } else {
                        abort(reason);
                    }
                } else if (fsm.change('closing')) {
                    close(reason);
                } else {
                    sessionActivityMonitor.onConnectionClosed();
                    log.debug('Unable to handle session disconnect, session state: ', fsm.state);
                }
            });

            connection.on('connect', function(response) {
                if (fsm.change('connected')) {
                    token = response.token;

                    if (response.response === ResponseCode.OK) {
                        sessionID = response.identity;
                        sessionActivityMonitor.onNewConnection(connection, response);
                        emitter.emit('connect', response.identity);
                    } else if (
                        response.response === ResponseCode.RECONNECTED ||
                        response.response === ResponseCode.RECONNECTED_WITH_MESSAGE_LOSS) {

                        attempts = 0;
                        clearTimeout(reconnectTimeout);

                        if (response.response === ResponseCode.RECONNECTED_WITH_MESSAGE_LOSS) {
                            connection.resetSequences();
                            replaceConversationSet(new Error("Peer is disconnected"));

                            log.info("Reconnected session, but messages may have been lost");
                        } else {
                            log.info('Reconnected session');
                        }

                        sessionActivityMonitor.onNewConnection(connection, response);
                        emitter.emit('reconnect');
                    }
                } else {
                    log.trace('Unknown connection response: ', response);
                }
            });

            log.debug('Connecting with options:', opts);
            log.trace('Connecting with request:', request);

            try {
                connection.connect(request, opts);
            } catch (e) {
                log.warn('Connection error', e);
                emitter.emit('error', e); 

                if (fsm.change('closing')) {
                    close(CloseReason.CONNECTION_ERROR);
                }
            }

            if (opts.principal) {
                principal = opts.principal;
            }
        } else {
            log.warn('Unable to connect, session state: ', fsm.state);
            emitter.emit('error', new Error('Unable to connect, session state: ' + fsm.state));
        }
    };

    this.close = function() {
        if (fsm.change('closing')) {
            sessionActivityMonitor.onConnectionClosed();
            connection.close(CloseReason.CLOSED_BY_CLIENT); 
        } else {
            log.debug('Unable to close, session state: ', fsm.state);
        }
    };

    this.getErrorHandler = function() {
        return function(error) {
            log.error("Session error:", error.message);
        };
    };

    this.getServiceLocator = function() {
        return serviceLocator;
    };

    this.getServiceAdapter = function() {
        return serviceAdapter;
    };

    this.getPrincipal = function() {
        return principal;
    };    

    this.getStreamRegistry = function() {
        return topicStreamRegistry;
    };

    this.getRouting = function() {
        return topicRouting;
    };

    this.onSystemPing = function() {
        sessionActivityMonitor.onSystemPing();
    };
}

module.exports = InternalSession;

},{"../../options":294,"activity/connection-activity-monitor-factory":55,"activity/session-activity-monitor":57,"client/service-adapter":65,"client/service-locator":66,"data/datatypes":94,"events/emitter":103,"protocol/connection-request":135,"protocol/response-code":138,"routing/stream-registry":139,"routing/topic-cache":144,"routing/topic-routing":145,"services/serialisers":219,"transports/transports":271,"util/fsm":277,"util/function":278,"util/logger":280,"v4-stack/aliases":287,"v4-stack/close-reason":288}],65:[function(require,module,exports){
var CommandHeader = require('services/command-header');
var CommandError = require('services/command-error');
var Message = require('v4-stack/message');

var type = CommandError.ErrorType;

var log = require('util/logger').create('Service Adapter');

function ServiceAdapter(internalSession, serialisers, sender) {
    var headerSerialiser = serialisers.get(CommandHeader);
    var errorSerialiser = serialisers.get(CommandError);

    var conversations = internalSession.getConversationSet();
    var listeners = {};

    function sendRequest(header, command, serialiser) {
        var msg = Message.create({
            type : Message.types.SERVICE_REQUEST
        });

        headerSerialiser.write(msg, header);
        serialiser.write(msg, command);

        log.trace('Sending command request: ' + header, command);

        sender(msg);
    }

    function sendResponse(header, command, serialiser) {
        var msg = Message.create({
            type : Message.types.SERVICE_RESPONSE
        });

        headerSerialiser.write(msg, header);
        serialiser.write(msg, command);

        log.trace('Sending command response: ' + header, command);

        sender(msg);
    }

    function sendError(header, command, serialiser) {
        var msg = Message.create({
            type : Message.types.SERVICE_ERROR
        });

        headerSerialiser.write(msg, header);
        serialiser.write(msg, command);

        log.trace('Sending error: ' + header, command);

        sender(msg);
    }

    function handleRequest(header, input) {
        var listener = listeners[header.service];

        if (listener) {
            log.trace('Received command request for service: ' + header);

            listener(header, input);
        } else {
            log.error('Received command request for unknown service: ' + header);

            var error = new CommandError(type.COMMUNICATION_FAILURE, "Unknown client service: " + header.service); 
            sendError(header.createErrorHeader(), error, errorSerialiser);
        }
    }

    function handleResponse(header, input) {
        log.trace('Received command response: ' + header);
        conversations.respond(header.cid, input);
    }

    function handleError(header, input) {
        var error = errorSerialiser.read(input);

        log.warn("Command Error received", error.message);

        conversations.discard(header.cid, new Error(error.message));
    }

    this.sendRequest = sendRequest;
    this.sendResponse = sendResponse;
    this.sendError = sendError;

    this.addService = function addService(definition, service) {
        if (listeners[definition.id] === undefined) {
            var requestSerialiser = serialisers.get(definition.request);
            var responseSerialiser = serialisers.get(definition.response);

            listeners[definition.id] = function(header, input) {
                var request = requestSerialiser.read(input);

                var callback = {
                    respond : function(response) {
                        var rHeader = header.createResponseHeader();
                        sendResponse(rHeader, response, responseSerialiser);
                    },
                    fail : function(error) {
                        var eHeader = header.createErrorHeader();
                        sendError(eHeader, error, errorSerialiser);
                    }
                };

                try {
                    service.onRequest(internalSession, request, callback);
                } catch (e) {
                    throw new Error("Unable to handle request for " + definition.id, e);
                }
            };
        } else {
            throw new Error("Service already exists for " + definition);
        }
    };

    this.onMessage = function onMessage(modes, data) {
        var header = headerSerialiser.read(data);
        switch (modes) {
            case Message.types.SERVICE_REQUEST:
                handleRequest(header, data);
                break;
            case Message.types.SERVICE_RESPONSE:
                handleResponse(header, data);
                break;
            case Message.types.SERVICE_ERROR:
                handleError(header, data);
                break;
            default:
                throw new Error("Unknown Command Service message " + modes);
        }
    };
}

module.exports = ServiceAdapter;

},{"services/command-error":165,"services/command-header":167,"util/logger":280,"v4-stack/message":293}],66:[function(require,module,exports){
var CommandHeader = require('services/command-header');
var Message = require('v4-stack/message');

var log = require('util/logger').create('Service Locator');

var remove = function(arr, e) {
    var i = arr.indexOf(e);
    if (i > -1) {
        arr.splice(i, 1);
    }
};

var defaultCB = function() { };

function ServiceLocator(internalSession, serialisers, serviceAdapter) {
    var conversations = internalSession.getConversationSet();

    this.obtain = function(service) {
        var pending = [],
            requestSerialiser = serialisers.get(service.request),
            responseSerialiser = serialisers.get(service.response);

        var reference = {
            send : function(req, callback) {
                callback = callback || defaultCB;

                var handler = {
                    onOpen : function(cid) {
                        var header = new CommandHeader(service.id, cid);

                        serviceAdapter.sendRequest(header, req, requestSerialiser);
                        pending.push(cid);
                    },
                    onResponse : function(cid, input) {
                        try {
                            var response = responseSerialiser.read(input);

                            callback(null, response);
                            remove(pending, cid);

                            return true;
                        }
                        catch (e) {
                            e.message = internalSession.getSessionId() +
                            ' failed to process response for service \'' +
                            service.name +
                            '\' cid=<' + cid +
                            '> : ' + e.message;
                            throw e;
                        }
                    },
                    onDiscard : function(cid, err) {
                        remove(pending, cid);
                        callback(err);
                    }
                };

                                var cid = conversations.new(handler, callback);

                                return function() {
                    conversations.discard(cid, "cancelled");
                };
            },
            close : function() {
                pending.forEach(conversations.discard);
                pending = [];
            }
        };

        return reference;
    };
}

module.exports = ServiceLocator;

},{"services/command-header":167,"util/logger":280,"v4-stack/message":293}],67:[function(require,module,exports){
var HashMap = require('hashmap');

function ServiceRegistry() {
    var services = new HashMap();
    var listeners = [];

    this.get = function(definition) {
        return services.get(definition);
    };

    this.add = function(definition, service) {
        if (services.has(definition)) {
            throw new Error("Service already exists for " + definition);
        }

        services.set(definition, service);

        listeners.forEach(function(listener) {
            listener(definition, service);
        });
    };

    this.addListener = function(listener) {
        listeners.push(listener);
        services.forEach(function(k, v) {
            listener(v, k);
        });
    };
}

module.exports = ServiceRegistry;

},{"hashmap":8}],68:[function(require,module,exports){
var service = {
    onRequest : function(internal, ping, callback) {
        callback.respond();
        internal.onSystemPing();
    }
};

module.exports = service;

},{}],69:[function(require,module,exports){
var service = {
    onRequest : function(internal, request, callback) {
        callback.respond();

        internal.getRouting().subscribe(request.info);
    }
};

module.exports = service;

},{}],70:[function(require,module,exports){
var service = {
    onRequest : function(internal, notification, callback) {
        callback.respond();

        internal.getRouting().unsubscribe(notification.id, notification.reason);
    }
};

module.exports = service;

},{}],71:[function(require,module,exports){
var service = {
    onRequest : function(internal, ping, callback) {
        callback.respond();
    }
};

module.exports = service;
},{}],72:[function(require,module,exports){
module.exports = {
    RECORD_DELIMITER : 0x01,
    FIELD_DELIMITER : 0x02,
    EMPTY_FIELD : 0x03,
    RECORD_MU : 0x04,
    FIELD_MU : 0x05
};

},{}],73:[function(require,module,exports){
var BufferInputStream = require('io/buffer-input-stream');
var RecordContent = require('./record-content');
var consts = require('content/consts');

module.exports = function RecordContentParser(buffer) {
    var records = [];

    if (buffer.length === 1 && buffer[0] === consts.RECORD_MU) {
        records.push(new RecordContent.Record([]));
    } else if (buffer.length > 0) {
        var bis = new BufferInputStream(buffer);

        while (bis.hasRemaining()) {
            var rbis = new BufferInputStream(bis.readUntil(consts.RECORD_DELIMITER));
            var fields = [];

            while (rbis.hasRemaining()) {
                var field = rbis.readUntil(consts.FIELD_DELIMITER);

                if (field.length === 0 || (field.length === 1 && ((fields.length === 0 && 
                                                                   field[0] === consts.FIELD_MU) || 
                                                                   field[0] === consts.EMPTY_FIELD))) {
                    fields.push(undefined);
                } else {
                    fields.push(field.toString());
                }
            }

            var rbuffer = rbis.buffer;
            if (rbuffer[rbuffer.length - 1] === consts.FIELD_DELIMITER) {
                fields.push(undefined);
            }

                        records.push(new RecordContent.Record(fields));
        }

        if (buffer[buffer.length - 1] === consts.RECORD_DELIMITER) {
            records.push(new RecordContent.Record([]));
        }
    }

    return new RecordContent(records);
};

},{"./record-content":74,"content/consts":72,"io/buffer-input-stream":123}],74:[function(require,module,exports){
var consts = require('content/consts');

var RecordContent = function RecordContentImpl(records) {
    this.length = records.length;

    this.get = function(i) {
        i = i || 0;
        return records[i];
    };

    this.records = function() {
        return records.concat([]);
    };

    this.forEach = function(iterator) {
        records.forEach(iterator);
    };

    this.toString = function() {
        return records.join(consts.RECORD_DELIMITER);
    };
};

RecordContent.Record = function RecordImpl(fields) {
    this.length = fields.length;

    this.get = function(i) {
        i = i || 0;
        return fields[i];
    };

    this.fields = function() {
        return fields.concat([]);
    };

    this.forEach = function(iterator) {
        fields.forEach(iterator);
    };

    this.toString = function() {
        return fields.join(consts.FIELD_DELIMITER);
    };
};

module.exports = RecordContent;
},{"content/consts":72}],75:[function(require,module,exports){
var BEES = require('serialisers/byte-encoded-enum-serialiser');
var Codec = require('io/codec');
var util = require('content/util');

var Encoding = {
    NONE : 0,
    ENCRYPTED : 1,
    COMPRESSED : 2
};

module.exports = {
    read : function(bis) {
        var encoding = BEES.read(bis, Encoding);
        var bytes = Codec.readBytes(bis);

        switch (encoding) {
        case Encoding.NONE :
            return bytes;
        default :
            throw new Error('Unable to handle encoding type ' + encoding);
        }
    },
    write : function(bos, content) {
        BEES.write(bos, Encoding.NONE);
        Codec.writeBytes(bos, util.getBytes(content));
    }
};

},{"content/util":79,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151}],76:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var api = require('../../../content/record-content');

var RecordContent = require('content/structured-record-content/record-content');

function insertFields(record, fn, fields) {
    if (fields) {
        for (var field in fields) {
            var value = fields[field];

            if (value instanceof Array) {
                for (var i = 0; i < value.length; ++i) {
                    fn.call(record, field, value[i], i);
                }
            } else {
                fn.call(record, field, value);
            }
        }
    }
}

var RecordRecordBuilder = _implements(api.Builder.Record,
    function StructuredRecordContentRecordBuilderImpl(meta) {
    var fields = {};

    this.add = function(name, value) {
        var mfield = meta.getField(name);

        if (mfield) {
            if (fields[name] === undefined) {
                fields[name] = [];
            }

            if (mfield.occurs.max === -1 || fields[name].length < mfield.occurs.max) {
                fields[name].push(value);
            } else {
                throw new Error("Field '" + name + "' can only occur up to " + mfield.occurs.max + " times");
            }
        } else {
            throw new Error("Invalid field name  '" + name + "'");
        }

        return this;
    };

    this.set = function(name, value, index) {
        var mfield = meta.getField(name);

        if (mfield) {
            if (fields[name] === undefined) {
                fields[name] = [];
            }

            index = index || 0;

            if (fields[name][index] !== undefined) {
                fields[name][index] = value;
            } else {
                throw new Error("Cannot set nonexistent field '" + name + '" (' + index + ')');
            }
        } else {
            throw new Error("Invalid field name '" + name + "'");
        }

        return this;
    };

    this.build = function() {
        var $fields = {};

        meta.getFields().forEach(function(mfield) {
            var field = mfield.name;
            $fields[field] = [];

            if (fields[field] === undefined) {
                fields[field] = [];
            }

            for (var i = fields[field].length; i < mfield.occurs.min; ++i) {
                fields[field].push(mfield.type.value);
            }

            $fields[field] = fields[field];
        });

        return new RecordContent.Record($fields);
    };
});

var RecordBuilder = _implements(api.Builder,
        function StructuredRecordContentBuilderImpl(meta) {
    var records = {};

    this.add = function(name, fields) {
        var mrecord = meta.getRecord(name);

        if (mrecord) {
            if (records[name] === undefined) {
                records[name] = [];
            }

            if (mrecord.occurs.max === -1 || records[name].length < mrecord.occurs.max) {
                var record = new RecordRecordBuilder(mrecord);
                records[name].push(record);

                insertFields(record, record.add, fields);

                return record;
            } else {
               throw new Error("Record '" + name + "' can only occur up to " + mrecord.occurs.max + " times");
            }
        } else {
            throw new Error("Invalid record name '" + name + "'");
        }
    };

    this.set = function(name, fields, index) {
        var mrecord = meta.getRecord(name);

        if (mrecord) {
            if (records[name] === undefined) {
                records[name] = [];
            }

            index = index || 0;

            if (records[name][index] !== undefined) {
                var record = records[name][index];

                insertFields(record, record.set, fields);

                return record;
            } else {
                throw new Error('Cannot set nonexistent record ' + name + '" (' + index + ')');
            }
        } else {
            throw new Error("Invalid record name '" + name + "'");
        }
    };

    this.build = function() {
        var $records = {};

        meta.getRecords().forEach(function(mrecord) {
            var record = mrecord.name;
            $records[record] = [];

            if (records[record] === undefined) {
                records[record] = [];
            }

            for (var i = records[record].length; i < mrecord.occurs.min; ++i) {
                records[record].push(new RecordRecordBuilder(mrecord));
            }

            for (var j = 0; j < records[record].length; ++j) {
                $records[record].push(records[record][j].build());
            }
        });

        return new RecordContent($records);
    };

    this.addAndBuild = function(name, value) {
        this.add(name, value);
        return this.build();
    };

    this.setAndBuild = function(name, fields, index) {
        this.set(name, fields, index);
        return this.build();
    };

});

module.exports = RecordBuilder;

},{"../../../content/record-content":30,"content/structured-record-content/record-content":78,"util/interface":279}],77:[function(require,module,exports){
var parseAsRecordContent = require('content/record-content/parser');
var RecordContent = require('content/structured-record-content/record-content');

function Reader(set) {
    var pos = 0;

    this.readUpTo = function(occurs, fn) {
        var max = Math.min(pos + (occurs.max === -1 ? set.length : occurs.max), set.length);

        if (set.length === 0) {
            return;
        }

        if (occurs.min > set.length - pos) {
            throw new Error('Data exhaused while parsing');
        }

        for (var i = pos; i < max; ++i) {
            fn(set[i]);
        }

        pos = max;
    };
}

function RecordContentMetadataParser(metadata) {
    this.parse = function(buffer) {
        var content = parseAsRecordContent(buffer);
        var reader = new Reader(content.records());
        var $records = {};

        metadata.getRecords().forEach(function(mrecord) {
            var records = [];

            reader.readUpTo(mrecord.occurs, function(record) {
                var reader = new Reader(record.fields());
                var $fields = {};

                mrecord.getFields().forEach(function(mfield) {
                    var fields = [];

                    reader.readUpTo(mfield.occurs, function(field) {
                        if (field) {
                            fields.push(mfield.type.parse(field));
                        } else {
                            fields.push(field);
                        }
                    });

                                        $fields[mfield.name] = fields;
                });

                records.push(new RecordContent.Record($fields));
            });

            $records[mrecord.name] = records;
        });

        return new RecordContent($records);
    };
}

module.exports = RecordContentMetadataParser;

},{"content/record-content/parser":73,"content/structured-record-content/record-content":78}],78:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var api = require('../../../content/record-content');

var StructuredRecordContent = _implements(api, function StructuredRecordContentImpl(records) {
    var inlined = [];

    for (var k in records) {
        inlined = inlined.concat(records[k]);
    }

    this.get = function(name, index) {
        index = index || 0;

        if (records[name]) {
            return records[name][index];
        }

        return undefined;
    };

    this.records = function(name) {
        if (records[name]) {
            return records[name].concat([]);
        }

        return inlined.concat([]);
    };

    this.forEach = function(iterator) {
        inlined.forEach(iterator);
    };
});

StructuredRecordContent.Record = _implements(api.Record, function StructuredRecordImpl(fields) {
    var inlined = [];

    for (var k in fields) {
        inlined = inlined.concat(fields[k]);
    }

    this.get = function(name, index) {
        index = index || 0;

        if (fields[name]) {
            return fields[name][index];
        }

        return undefined;
    };

    this.fields = function(name) {
        if (fields[name]) {
            return fields[name].concat([]);
        }

        return inlined.concat([]);
    };

    this.forEach = function(iterator) {
        inlined.forEach(iterator);
    };
});

module.exports = StructuredRecordContent;
},{"../../../content/record-content":30,"util/interface":279}],79:[function(require,module,exports){
(function (Buffer){
var Codec = require('io/codec');
var BufferOutputStream = require('io/buffer-output-stream');

var RC = require('content/record-content/record-content');
var SRC = require('content/structured-record-content/record-content');

var consts = require('content/consts');

function getDefaultContentBytes(content) {
    return new Buffer(content.toString());
}

function getRecordContentBytes(content) {
    var bos = new BufferOutputStream();
    var recordDelimiter = false;

    content.forEach(function writeRecord(r) {
        var fieldDelimiter = false;

        if (recordDelimiter) {
            bos.writeInt8(consts.RECORD_DELIMITER);
        } else {
            recordDelimiter = true;
        }

        r.forEach(function writeField(f) {
            if (fieldDelimiter) {
                bos.writeInt8(consts.FIELD_DELIMITER);
            } else {
                fieldDelimiter = true;
            }

            bos.writeString(f.toString());
        });
    });

    return bos.getBuffer();
}

function isRecordContent(content) {
    return RC.isPrototypeOf(content) || SRC.isPrototypeOf(content);
}

function getBytes(content) {
    if (content.$buffer) {
        return content.$buffer.slice(content.$offset, content.$length);
    } else if (isRecordContent(content)) {
        return getRecordContentBytes(content);
    } else {
        return getDefaultContentBytes(content);
    }
}

module.exports = {
    getBytes : getBytes,
    isRecordContent : isRecordContent
};
}).call(this,require("buffer").Buffer)
},{"buffer":2,"content/consts":72,"content/record-content/record-content":74,"content/structured-record-content/record-content":78,"io/buffer-output-stream":124,"io/codec":125}],80:[function(require,module,exports){
var ControlGroup = require('control/control-group');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var name = Codec.readString(input);
        return new ControlGroup(name);
    },
    write : function(output, group) {
        Codec.writeString(output, group.name);
    }
};

module.exports = serialiser;

},{"control/control-group":81,"io/codec":125}],81:[function(require,module,exports){
function ControlGroup(name) {
    this.name = name;
}

ControlGroup.DEFAULT = new ControlGroup("default");

module.exports = ControlGroup;

},{}],82:[function(require,module,exports){
var Services = require('services/services');

var Emitter = require('events/emitter');
var Result = require('events/result');

var CloseReason = require('v4-stack/close-reason');

var ResponseHandlerState = {
    REGISTERING : 0,
    ACTIVE : 1,
    CLOSED : 2
};

function responseHandler(internal, adapter, deregistration) {
    var state = ResponseHandlerState.REGISTERING;
    var close;

    return {
        onDiscard : function(cid, err) {
            if (err instanceof CloseReason) {
                adapter.close();
            } else {
                adapter.close(err);
            }

            state = ResponseHandlerState.CLOSED;
        },
        onOpen : function(cid) {
            close = function close() {
                var emitter = new Emitter();
                var result = new Result(emitter);

                if (state !== ResponseHandlerState.CLOSED) {
                    deregistration(cid, function(err) {
                        if (err) {
                            internal.getConversationSet().discard(cid, err);
                            emitter.error(err);
                        } else {
                            internal.getConversationSet().respond(cid, ResponseHandlerState.CLOSED);
                            emitter.emit('complete');
                        }
                    });
                } else {
                    emitter.error(new Error('Handler already closed'));
                }

                return result;
            };
        },
        onResponse : function(cid, response) {
            switch (response) {
                case ResponseHandlerState.ACTIVE:
                    adapter.active(close);
                    state = response;
                    return false;
                case ResponseHandlerState.CLOSED:
                    adapter.close();
                    state = response;
                    return true;
                default:
                    return adapter.respond(response);
            }
        }
    };
}

function registrationCallback(conversationSet, cid, emitter) {
    return function(err) {
        if (err) {
            conversationSet.discard(cid, err);
            emitter.error(err);
        } else {
            conversationSet.respond(cid, ResponseHandlerState.ACTIVE);
            emitter.emit('complete');
        }
    };
}

function registerHandler(internal, params, adapter, reg, dereg) {
    var conversationSet = internal.getConversationSet();
    var serviceLocator = internal.getServiceLocator();

    var registration = serviceLocator.obtain(reg);
    var deregistration = serviceLocator.obtain(dereg);

    var cid = conversationSet.new(responseHandler(internal, adapter, function(cid, callback) {
        deregistration.send(params, callback);
    }));

    var emitter = new Emitter();
    var result = new Result(emitter);

    registration.send({ params : params, cid : cid }, 
            registrationCallback(conversationSet, cid, emitter));

    return result;
}

module.exports.responseHandler = responseHandler;

module.exports.registrationCallback = registrationCallback;

module.exports.registerMessageHandler = function registerMessageHandler(internal, params, adapter) {
    return registerHandler(internal, params, adapter,
                                  Services.MESSAGE_RECEIVER_CONTROL_REGISTRATION,
                                  Services.MESSAGE_RECEIVER_CONTROL_DEREGISTRATION);
};

module.exports.registerServerHandler = function registerServerHandler(internal, params, adapter) {
    return registerHandler(internal, params, adapter,
            Services.SERVER_CONTROL_REGISTRATION,
            Services.SERVER_CONTROL_DEREGISTRATION);
};

module.exports.registerTopicHandler = function registerTopicHandler(internal, params, adapter) {
    return registerHandler(internal, params, adapter,
        Services.TOPIC_CONTROL_REGISTRATION,
        Services.TOPIC_CONTROL_DEREGISTRATION);
};

module.exports.ResponseHandlerState = ResponseHandlerState;

},{"events/emitter":103,"events/result":104,"services/services":220,"v4-stack/close-reason":288}],83:[function(require,module,exports){
var Codec = require('io/codec');
var ConversationId = require('conversation/conversation-id');
var CIDSerialiser = {
    read : function(input) {
        return new ConversationId(Codec.readInt64(input));
    },
    write : function(out, cid) {
        Codec.writeInt64(out, cid.val);
    }
};

module.exports = CIDSerialiser;

},{"conversation/conversation-id":84,"io/codec":125}],84:[function(require,module,exports){
var Long = require('long');

function ConversationId(val) {
    this.val = val;
}

ConversationId.prototype.toString = function() {
    return this.val.toString(10);
};

ConversationId.prototype.toValue = function() {
    return this.toString();
};

ConversationId.fromString = function(val) {
    return new ConversationId(Long.fromString(val, false));
};

module.exports = ConversationId;

},{"long":11}],85:[function(require,module,exports){
var HashMap = require('hashmap');
var Long = require('long');

var ConversationId = require('conversation/conversation-id');
var Conversation = require('conversation/conversation');
var fn = require('util/function');

var log = require('util/logger').create('Conversation Set');

var nextCID = (function() {
    var id = new Long(0);
    return function() {
        id = id.add(1);
        return new ConversationId(id);
    };
}());

function create(errorHandler) {
    var conversations = new HashMap();

    function discard(cid, err) {
        var conversation = conversations.get(cid.toString());

        if (conversation) {
            conversations.remove(cid.toString());
            conversation.discard(cid, err);   
        }
    }

    var bulkhead = function(set, handler, cb) {
        return function(cid) {
            try {
                return fn.callWithArguments(cb, arguments);
            } catch (e) {
                errorHandler.handlerException(cid, e, arguments[1], arguments[2]);
                discard(cid, e);
                return false;
            }
        };
    };

    return {
        new : function(handler) {
            var cid = nextCID();
            var set = this;

            var conversation = new Conversation({
                onOpen      : bulkhead(set, handler, handler.onOpen),
                onResponse  : bulkhead(set, handler, handler.onResponse),
                onDiscard   : bulkhead(set, handler, handler.onDiscard)
            });

            conversations.set(cid.toString(), conversation);
            conversation.open(cid);

            return cid;
        },
        respond : function(cid, r1, r2) {
            var conversation = conversations.get(cid.toString());

                        if (conversation) {
                var response = conversation.respond(cid, r1, r2);

                switch (response) {
                    case 'ALREADY_FINISHED' :
                        errorHandler.unknownConversation(cid, r1, r2);
                        break;
                    case 'HANDLED_AND_ACTIVE' :
                        break;
                    case 'HANDLED_AND_FINISHED' :
                        conversations.remove(cid.toString());
                        break;
                }
            } else {
                errorHandler.unknownConversation(cid, r1, r2);
            }
        },
        discard : discard,
        discardAll : function(err) {
            conversations.forEach(function(conversation, cid) {
                discard(ConversationId.fromString(cid), err);
            });

            conversations.clear();
        },
        size : function() {
            return conversations.count();
        }
    };
}

module.exports = create;

},{"conversation/conversation":86,"conversation/conversation-id":84,"hashmap":8,"long":11,"util/function":278,"util/logger":280}],86:[function(require,module,exports){
function Conversation(handler) {
    this.handler = handler;
}

Conversation.prototype.open = function(cid) {
    this.handler.onOpen(cid);
};

Conversation.prototype.respond = function(cid, r1, r2) {
    var close = r2 === undefined ? this.handler.onResponse(cid, r1) :
                                   this.handler.onResponse(cid, r1, r2);
    return close ? 'HANDLED_AND_FINISHED' : 'HANDLED_AND_ACTIVE';
};

Conversation.prototype.discard = function(cid, err) {
    this.handler.onDiscard(cid, err);
};

module.exports = Conversation;
},{}],87:[function(require,module,exports){
var conversationSet = require('conversation/conversation-set');

function assign(self, set) {
    for (var m in set) {
        self[m] = set[m];
    }
}

module.exports = function DelegatingConversationSet(globalErrorHandler) {
    var current = conversationSet(globalErrorHandler);
    var self = this;

    assign(self, current);

    this.replace = function(err) {
        var old = current;

        current = conversationSet(globalErrorHandler);

        assign(self, current);

        old.discardAll(err);
    };
};
},{"conversation/conversation-set":85}],88:[function(require,module,exports){
var log = require('util/logger').create('Conversation Error Handler');

module.exports = {
    handlerException : function(cid, e, r1, r2) {
        log.error("Application handler threw an exception when called with a response" +
                  "[cid=" + cid + " " +
                  "response=" + r1 + ", " + r2 + "] ",
                  e);
    },
    unknownConversation : function(cid, r1, r2) {
        log.debug("A response was received from a peer for a finished conversation and ignored" +
                  "[cid=" + cid + " " +
                  "response=" + r1 + ", " + r2 + "]");
    }
};
},{"util/logger":280}],89:[function(require,module,exports){
(function (Buffer){
var _implements = require('util/interface')._implements;
var BinaryDataType = require('../../../data/binary/binary-datatype');

var BinaryDeltaSupportImpl = require('data/binary/binary-delta-support-impl');
var BinaryDeltaImpl = require('data/binary/binary-delta-impl');
var BinaryImpl = require('data/binary/binary-impl');

module.exports = _implements(BinaryDataType, function BinaryDataTypeImpl() {
    var self = this;

    function internalValue(val) {
        if (BinaryImpl.isPrototypeOf(val)) {
            return val;
        } else if (Buffer.isBuffer(val)) {
            return self.readValue(val);
        } else {
            throw new Error("Unable to read Binary value from: " + val);
        }
    }

    var binaryDeltaSupport = new BinaryDeltaSupportImpl(this, internalValue);

    this.name = function() {
        return "binary";
    };

    this.from = function(val) {
        return internalValue(val);
    };

    this.readValue = function(buffer, offset, length) {
        return new BinaryImpl(self, buffer, offset, length);
    };

    this.writeValue = function(binary) {
        binary = internalValue(binary);
        return binary.$buffer.slice(binary.$offset, binary.$offset + binary.$length);
    };

    this.deltaType = function(context) {
        if (!context) {
            return binaryDeltaSupport;
        } else if (typeof context === "string") {
            switch (context.toLowerCase()) {
                case "binary" :
                    return binaryDeltaSupport;
            }
        } else if (BinaryDeltaImpl.isPrototypeOf(context)) {
            return binaryDeltaSupport;
        }

        throw new Error("Unknown delta type: " + context);
    };
});
}).call(this,{"isBuffer":require("../../../../node_modules/browserify/node_modules/insert-module-globals/node_modules/is-buffer/index.js")})
},{"../../../../node_modules/browserify/node_modules/insert-module-globals/node_modules/is-buffer/index.js":6,"../../../data/binary/binary-datatype":31,"data/binary/binary-delta-impl":90,"data/binary/binary-delta-support-impl":91,"data/binary/binary-impl":92,"util/interface":279}],90:[function(require,module,exports){
(function (Buffer){
var _implements = require('util/interface')._implements;
var Tokeniser = require('cbor/tokeniser');

var BinaryDelta = require('../../../data/binary/binary-delta');

var util = require('data/util');

module.exports = _implements(BinaryDelta, function BinaryDeltaImpl(buffer, offset, length) {
    util.assignInternals(this, buffer, offset, length);

    this.visit = function(visitor) {
        if (this.hasChanges()) {
            var tokeniser = new Tokeniser(buffer, offset, length);

            while (tokeniser.hasRemaining()) {
                var token = tokeniser.nextToken();
                var isEnd = false;

                if (Buffer.isBuffer(token.value)) {
                    isEnd = !visitor.insert(token.value);
                } else {
                    var start = token.value;
                    var end = tokeniser.nextToken().value;

                    isEnd = !visitor.match(start, end);
                }

                if (isEnd) {
                    visitor.end();
                    return;
                }
            }

            visitor.end();
        } else {
            visitor.noChange();
        }
    };

    this.hasChanges = function() {
        return length !== 1;
    };
});
}).call(this,{"isBuffer":require("../../../../node_modules/browserify/node_modules/insert-module-globals/node_modules/is-buffer/index.js")})
},{"../../../../node_modules/browserify/node_modules/insert-module-globals/node_modules/is-buffer/index.js":6,"../../../data/binary/binary-delta":32,"cbor/tokeniser":62,"data/util":102,"util/interface":279}],91:[function(require,module,exports){
(function (Buffer){
var _implements = require('util/interface')._implements;
var DeltaType = require('../../../data/delta-type');

var BufferOutputStream = require('io/buffer-output-stream');
var BinaryDeltaImpl = require('data/binary/binary-delta-impl');

var MyersBinaryDiff = require('data/diff/myers-binary-diff');

var Decoder = require('cbor/decoder');
var Encoder = require('cbor/encoder');

var encoder = new Encoder();

var NO_CHANGE = new BinaryDeltaImpl(new Buffer([-10]), 0, 1);

module.exports = _implements(DeltaType, function BinaryDeltaSupportImpl(datatype, internalValue) {
    var binaryDiff = new MyersBinaryDiff();
    var self = this;

    this.name = function() {
        return "binary";
    };

    this.diff = function(oldValue, newValue) {
        oldValue = internalValue(oldValue);
        newValue = internalValue(newValue);

        var buffer = newValue.$buffer;
        var offset = newValue.$offset;
        var length = newValue.$length;

        var budget = length;

        var script = new Script(encoder, buffer, offset, function(cost) {
            return (budget -= cost) <= 0;
        });

        var result = binaryDiff.diff(oldValue.$buffer,
                                oldValue.$offset,
                                oldValue.$length,
                                buffer,
                                offset,
                                length,
                                script);

        var delta = encoder.flush();

        switch (result) {
            case MyersBinaryDiff.REPLACE :
                return replace(newValue);
            case MyersBinaryDiff.NO_CHANGE :
                return NO_CHANGE;
            default :
                return self.readDelta(delta);
        }
    };

    this.apply = function(oldValue, delta) {
        oldValue = internalValue(oldValue);

        if (!delta || !delta.hasChanges()) {
            return oldValue;
        }

        var decoder = new Decoder(delta.$buffer);
        var bos = new BufferOutputStream();

        while (decoder.hasRemaining()) {
            var start = decoder.nextValue();

            if (Buffer.isBuffer(start)) {
                bos.writeMany(start);
            } else if (typeof start === "number") {
                bos.writeMany(oldValue.$buffer, oldValue.$offset + start, decoder.nextValue());
            }
        }

        return datatype.readValue(bos.getBuffer());
    };

    this.readDelta = function(buffer, offset, length) {
        var delta = new BinaryDeltaImpl(buffer, offset, length);

        if (delta.$length === 1 && buffer[delta.$offset] === NO_CHANGE.$buffer[0]) {
            return NO_CHANGE;
        }

        return delta;
    };

    this.writeDelta = function(delta) {
        return delta.$buffer.slice(delta.$offset, delta.$offset + delta.$length);
    };

    this.noChange = function() {
        return NO_CHANGE;
    };

    this.isValueCheaper = function(value, delta) {
        return value.$length <= delta.$length;
    };
});

module.exports.NO_CHANGE = NO_CHANGE;

function replace(value) {
    encoder.encode(value.$buffer, value.$offset, value.$length);
    return new BinaryDeltaImpl(encoder.flush());
}

function cborCost(i) {
    if (i < 24) {
        return 1;
    } else if (i < 0xFF) {
        return 2;
    } else if (i <= 0xFFFF) {
        return 3;
    }

    return 5;
}

function conflatableMatch(matchStart, matchLength, insertLength) {
    var matchCost = cborCost(matchStart) + 1;
    var insertCost = cborCost(matchLength + insertLength) - cborCost(insertLength) + matchLength;

    return insertCost <= matchCost;
}

function Script(encoder, buffer, offset, blowsBudget) {
    var pendingInsert;
    var pendingLength;
    var pendingStart = -1;

    this.insert = function(bStart, length) {
        if (!pendingInsert &&
            pendingStart !== -1 &&
            conflatableMatch(pendingStart, pendingLength, length)) {

            pendingStart = bStart - pendingLength;
            pendingLength += length;
            pendingInsert = true;

            return MyersBinaryDiff.SUCCESS;
        }

        return process(true, bStart, length);
    };

    this.match = function(aStart, length) {
        if (pendingInsert &&
            pendingStart !== -1 &&
            conflatableMatch(aStart, length, pendingLength)) {

            pendingLength += length;
            return MyersBinaryDiff.SUCCESS;
        }

        return process(false, aStart, length);
    };

    this.close = function() {
        return flush();
    };

    function flush() {
        if (pendingStart === -1) {
            return MyersBinaryDiff.SUCCESS;
        } else if (pendingInsert) {
            return writeInsert(pendingStart, pendingLength);
        } else {
            return writeMatch(pendingStart, pendingLength);
        }
    }

    function process(insert, start, length) {
        var r = flush();

        if (r !== MyersBinaryDiff.SUCCESS) {
            return r;
        }

        pendingInsert = insert;
        pendingStart = start;
        pendingLength = length;

        return MyersBinaryDiff.SUCCESS;
    }

    function writeInsert(start, length) {
        if (blowsBudget(length + cborCost(length))) {
            return MyersBinaryDiff.REPLACE;
        }

        encoder.encode(buffer, offset + start, length);

        pendingStart = -1;

        return MyersBinaryDiff.SUCCESS;
    }

    function writeMatch(start, length) {
        if (blowsBudget(cborCost(start) + cborCost(length))) {
            return MyersBinaryDiff.REPLACE;
        }

        encoder.encode(start);
        encoder.encode(length);

        pendingStart = -1;

        return MyersBinaryDiff.SUCCESS;
    }
}
}).call(this,require("buffer").Buffer)
},{"../../../data/delta-type":37,"buffer":2,"cbor/decoder":60,"cbor/encoder":61,"data/binary/binary-delta-impl":90,"data/diff/myers-binary-diff":95,"io/buffer-output-stream":124,"util/interface":279}],92:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var BinaryType = require('../../../data/binary/binary');
var BytesImpl = require('data/bytes-impl');

var util = require('data/util');

module.exports = _implements(BinaryType, function BinaryImpl(datatype, buffer, offset, length) {
    BytesImpl.__constructor.call(this, buffer, offset, length);

    var self = this;

    this.get = function() {
        return buffer.slice(self.$offset, self.$offset + self.$length);
    };

    this.diff = function(original, type) {
        type = type || "binary";

        return datatype.deltaType(type).diff(original, self);
    };

    this.apply = function(delta) {
        return datatype.deltaType(delta).apply(self, delta);
    };

    this.toString = function() {
        return "Binary <" + self.$length + " bytes> " + self.get().toString();
    };
});
},{"../../../data/binary/binary":33,"data/bytes-impl":93,"data/util":102,"util/interface":279}],93:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var Bytes = require('../../data/bytes');
var util = require('data/util');

module.exports = _implements(Bytes, function BytesImpl(buffer, offset, length) {
    util.assignInternals(this, buffer, offset, length);

    var self = this;

    this.length = function() {
        return self.$length;
    };

    this.asBuffer = function() {
        return buffer;
    };

    this.copyTo = function(target, tOffset) {
        tOffset = tOffset || 0;

        buffer.copy(target, tOffset, self.$offset, self.$offset + self.$length);
    };

    this.equalBytes = function(buffer, offset, length) {
        if (this.$length !== length) {
            return false;
        }

        var $buffer = this.$buffer;
        var $offset = this.$offset;

        if ($buffer === buffer && $offset === offset) {
            return true;
        }

        for (var i = 0; i < length; ++i) {
            if ($buffer[$offset + i] !== buffer[offset + i]) {
                return false;
            }
        }

        return true;
    };
});
},{"../../data/bytes":34,"data/util":102,"util/interface":279}],94:[function(require,module,exports){
(function (Buffer){
var _implements = require('util/interface')._implements;
var DataTypes = require('../../data/datatypes');
var TopicType = require('../../topics/topics').TopicType;

var BinaryDataTypeImpl = require('data/binary/binary-datatype-impl');
var BinaryImpl = require('data/binary/binary-impl');

var JSONDataTypeImpl = require('data/json/json-datatype-impl');
var JSONImpl = require('data/json/json-impl');

var DataTypesImpl = _implements(DataTypes, function DataTypesImpl() {
    var binary = new BinaryDataTypeImpl();
    var json = new JSONDataTypeImpl();

    var self = this;

    this.binary = function() {
        return binary;
    };

    this.json = function() {
        return json;
    };

    this.get = function(name) {
        switch (typeof name) {
            case "string" :
                switch (name.toLowerCase()) {
                    case 'json' :
                        return self.json();
                    case 'binary' :
                        return self.binary();
                }
                break;
            case "object" :
                if (name === TopicType.BINARY || BinaryImpl.isPrototypeOf(name) || Buffer.isBuffer(name)) {
                    return self.binary();
                }

                if (name === TopicType.JSON || JSONImpl.isPrototypeOf(name) || name.constructor === Object) {
                    return self.json();
                }
        }

        return null;
    };
});

module.exports = new DataTypesImpl();

}).call(this,{"isBuffer":require("../../../node_modules/browserify/node_modules/insert-module-globals/node_modules/is-buffer/index.js")})
},{"../../../node_modules/browserify/node_modules/insert-module-globals/node_modules/is-buffer/index.js":6,"../../data/datatypes":36,"../../topics/topics":298,"data/binary/binary-datatype-impl":89,"data/binary/binary-impl":92,"data/json/json-datatype-impl":96,"data/json/json-impl":98,"util/interface":279}],95:[function(require,module,exports){
var approximateCubeRoot = require('util/math').approximateCubeRoot;

var BAIL_OUT_FACTOR = 10000;
var MAXIMUM_STORAGE = 0x7fffffff;

var SUCCESS = 0,
    REPLACE = 1,
    NO_CHANGE = 2;

function Storage(max) {
    var maximumD = (max - 3) / 4;
    var vectorLength = 15;
    var vector = [];


    function fill(start) {
        for (var i = start; i < vectorLength; i += 4) {
            vector[i + 0] = -1;
            vector[i + 1] = -1;
            vector[i + 2] = 0x7fffffff;
            vector[i + 3] = 0x7fffffff;
        }
    }

    function ensure(d) {
        var required = 4 * (d + 1) + 3;

        if (vectorLength < required) {
            vectorLength = required;
        }
    }

    this.initialise = function(d) {
        ensure(d);
        fill(3);

        return vector;
    };

    this.extend = function(d) {
        if (d > maximumD) {
            return null;
        }

        var originalLength = vectorLength;

        ensure(d);
        fill(originalLength);

        return vector;
    };
}

function keyF(k) {
    return k < 0 ? -4 * k - 1 : 4 * k;
}

function keyR(k) {
    return keyF(k) + 2;
}

function getF(v, k) {
    var i = v[keyF(k)];
    return i === undefined ? 0 : i;
}

function getR(v, k) {
    var i = v[keyR(k)];
    return i === undefined ? 0 : i;
}

function setF(v, k, i) {
    v[keyF(k)] = i;
}

function setR(v, k, i) {
    var key = keyR(k);
    v[key] = i;
}

function nextF(v, k) {
    var left = getF(v, k + 1);
    var right = getF(v, k - 1);

    return left < right ? right : left + 1;
}

function nextR(v, k) {
    var left = getR(v, k + 1);
    var right = getR(v, k - 1);

    return left < right ? left : right - 1;
}

function corner(d, length) {
    if (d <= length) {
        return d;
    } else {
        return 2 * length - d;
    }
}

function calculateBailOutLimit(l1, l2, bailOutFactor) {
    var total = l1 + l2;
    var cube = approximateCubeRoot(total);
    var mult = bailOutFactor * cube;

    return Math.max(256, mult / 100);
}

function checkBounds(buffer, offset, length) {
    if (offset < 0) {
        throw new Error("offset " + offset + " < 0");
    }

    if (length < 0) {
        throw new Error("length " + length + " < 0");
    }

    if (offset + length > buffer.length || offset + length < 0) {
        throw new Error("offset " + offset + " + " + length + " > " + buffer.length);
    }
}

function Execution(storage, a, b, script, bailOutLimit) {
    var self = this;

    this.diff = function(aOffset, aLength, bOffset, bLength) {
        checkBounds(a, aOffset, aLength);
        checkBounds(b, bOffset, bLength);

        var x = 0;
        var y = 0;

        while (x < aLength && y < bLength && a[aOffset + x] === b[bOffset + y]) {
            ++x;
            ++y;
        }

        var u = aLength;
        var v = bLength;

        while (u > x && v > y && a[aOffset + u - 1] === b[bOffset + v - 1]) {
            --u;
            --v;
        }

        var r1 = script.match(aOffset, x); 
        if (r1 !== SUCCESS) {
            return r1;
        }

        var r2;

        if (x === u) {
            r2 = script.insert(bOffset + y, v - y);
        } else if (y === v) {
            r2 = script.delete(aOffset + x, u - x);
        } else {
            r2 = self.middleSnake(aOffset + x, u - x, bOffset + y, v - y);
        }

        if (r2 !== SUCCESS) {
            return r2;
        }

        return script.match(aOffset + u, aLength - u); 
    };

    this.middleSnake = function(aOffset, aLength, bOffset, bLength) {
        var delta = aLength - bLength;
        var odd = delta & 1;

        var vec = storage.initialise(1);

        setF(vec, -1, 0);
        setR(vec, 1, aLength);

        var d = 0;

        while (true) {
            for (var k1 = -corner(d, aLength); k1 <= corner(d, bLength); k1 +=2) {
                var x1 = nextF(vec, k1);
                var u1 = x1;

                while (u1 < aLength && u1 + k1 < bLength && a[aOffset + u1] === b[bOffset + u1 + k1]) {
                    ++u1;
                }

                setF(vec, k1, u1);

                if (odd && d > 1 && Math.abs(k1 + delta) <= d - 1 && u1 >= getR(vec, k1 + delta)) {
                    return self.recurse(aOffset, aLength, bOffset, bLength, x1, u1, k1);
                }
            }

            for (var k2 = -corner(d, bLength); k2 <= corner(d, aLength); k2 += 2) {
                var u2 = nextR(vec, k2);
                var x2 = u2;

                var kd = k2 - delta;

                while (x2 > 0 && x2 + kd > 0 && a[aOffset + x2 - 1] === b[bOffset + x2 + kd - 1]) {
                    --x2;
                }

                setR(vec, k2, x2);

                if (!odd && d > 0 && Math.abs(kd) <= d && x2 <= getF(vec, kd)) {
                    return self.recurse(aOffset, aLength, bOffset, bLength, x2, u2, kd);
                }
            }

            if (d > bailOutLimit) {
                return bail(vec, d, aOffset, aLength, bOffset, bLength);
            }

            ++d;

            vec = storage.extend(d);

            if (vec === null) {
                return REPLACE;
            }
        }
    };

    this.recurse = function(aOffset, aLength, bOffset, bLength, x, u, k) {
        var r1 = self.diff(aOffset, x, bOffset, x + k);

        if (r1 !== SUCCESS) {
            return r1;
        }

        var r2 = script.match(aOffset + x, u - x);

        if (r2 !== SUCCESS) {
            return r2;
        }

        return self.diff(aOffset + u, aLength - u, bOffset + u + k, bLength - u - k);
    };

    function bail(vec, d, aOffset, aLength, bOffset, bLength) {
        var xbest = 0;
        var ybest = 0;

        var x;
        var y;

        for (var k1 = -corner(d, aLength); k1 <= corner(d, bLength); k1 += 2) {
            var x1 = Math.min(getF(vec, k1), aLength);

            if (x1 + k1 > bLength) {
                x = bLength - k1;
            } else {
                x = x1;
            }

            y = x + k1;

            if (x + y > xbest + ybest) {
                xbest = x;
                ybest = y;
            }
        }

        for (var k2 = -corner(d, bLength); k2 <= corner(d, aLength); k2 += 2) {
            var x2 = Math.max(getR(vec, k2), 0);
            var kd = k2 - (aLength - bLength);

            if (x2 + kd < 0) {
                x = -kd;
            } else {
                x = x2;
            }

            y = x + kd;

            if (aLength + bLength - x - y > xbest + ybest) {
                xbest = x;
                ybest = y;
            }

            var r = boundedDiff(aOffset, xbest, bOffset, ybest, aLength, bLength);

            if (r !== SUCCESS) {
                return r;
            }

            return boundedDiff(
                aOffset + xbest,
                aLength - xbest,
                bOffset + ybest,
                bLength - ybest,
                aLength,
                bLength);
        }
    }

    function boundedDiff(aOffset, aLength, bOffset, bLength, totalN, totalM) {
        var totalSpace = totalN * totalM;
        var nm = aLength * bLength;

        var threshold = (1 << 24) + totalSpace / 2;

        if (nm >= threshold) {
            var x = aLength / 2;
            var y = bLength / 2;

            var r1 = self.diff(aOffset, x, bOffset, y);

            if (r1 !== SUCCESS) {
                return r1;
            }

            return self.diff(aOffset + x, aLength - x, bOffset + y, bLength - y);
        } else {
            return self.diff(aOffset, aLength, bOffset, bLength);
        }
    }
}

var INSERT = function(script, start, length) {
    return script.insert(start, length);
};

var MATCH = function(script, start, length) {
    return script.match(start, length);
};

var NOOP = function() {
    return SUCCESS;
};

function coalesce(delegate, aOffset, bOffset) {
    var neverFlushed = true;

    var pendingLength = 0;
    var pendingStart = 0;
    var pending = NOOP;

    function flushPending() {
        neverFlushed &= pending === NOOP;
        return pending(delegate, pendingStart, pendingLength);
    }

    function process(op, start, length) {
        if (length > 0) {
            if (pending !== op) {
                var r = flushPending();
                if (r !== SUCCESS) {
                    return r;
                }

                pending = op;
                pendingStart = start;
                pendingLength = length;
            } else {
                pendingLength += length;
            }
        }

        return SUCCESS;
    }

    return {
        insert : function(bStart, length) {
            return process(INSERT, bStart - bOffset, length);
        },
        match : function(aStart, length) {
            return process(MATCH, aStart - aOffset, length);
        },
        delete : function(aStart, length) {

            if (pending === INSERT) {
                return SUCCESS;
            }

            var r = flushPending();
            pending = NOOP;
            return r;
        },
        close : function(aLength, bLength) {
            if (neverFlushed) {
                if (pending === INSERT) {
                    return REPLACE;
                } else if (pendingStart === 0 && pendingLength === aLength) {
                    return NO_CHANGE;
                }
            }

            var r = flushPending();

            if (r !== SUCCESS) {
                return r;
            }

            return delegate.close();
        }
    };
}

module.exports = function MyersBinaryDiff(maximumStorage, bailOutFactor) {
    if (maximumStorage === undefined) {
        maximumStorage = MAXIMUM_STORAGE;
    }

    if (bailOutFactor === undefined) {
        bailOutFactor = BAIL_OUT_FACTOR;
    }

    var storage = new Storage(maximumStorage);

    this.diff = function(a, aOffset, aLength, b, bOffset, bLength, editScript) {
        var script = coalesce(editScript, aOffset, bOffset);
        var execution = new Execution(storage, a, b, script, calculateBailOutLimit(aLength, bLength, bailOutFactor));

        var result = execution.diff(aOffset, aLength, bOffset, bLength);

        if (result !== SUCCESS) {
            return result;
        }

        return script.close(aLength, bLength);
    };
};

module.exports.SUCCESS = SUCCESS;
module.exports.REPLACE = REPLACE;
module.exports.NO_CHANGE = NO_CHANGE;


},{"util/math":281}],96:[function(require,module,exports){
(function (Buffer){
var _implements = require('util/interface')._implements;
var JSONDataType = require('../../../data/json/json-datatype');

var BinaryDeltaSupportImpl = require('data/binary/binary-delta-support-impl');
var BinaryDeltaImpl = require('data/binary/binary-delta-impl');
var JSONImpl = require('data/json/json-impl');

var Encoder = require('cbor/encoder');

module.exports = _implements(JSONDataType, function JSONDataTypeImpl() {
    var encoder = new Encoder();
    var self = this;

    function internalValue(val) {
        if (JSONImpl.isPrototypeOf(val)) {
            return val;
        } else if (Buffer.isBuffer(val)) {
            return self.readValue(val);
        } else {
            return self.readValue(encoder.encode(val).flush());
        }
    }

    var binaryDeltaSupport = new BinaryDeltaSupportImpl(this, internalValue);

    this.name = function() {
        return "json";
    };

    this.from = function(val) {
        return internalValue(val);
    };

    this.fromJsonString = function(val) {
        return internalValue(JSON.parse(val));
    };

    this.readValue = function(buffer, offset, length) {
        return new JSONImpl(self, buffer, offset, length);
    };

    this.writeValue = function(json) {
        json = internalValue(json);
        return json.$buffer.slice(json.$offset, json.$offset + json.$length);
    };

    this.deltaType = function(context) {
        if (!context) {
            return binaryDeltaSupport;
        } else if (typeof context === "string") {
            switch (context.toLowerCase()) {
                case "binary" :
                    return binaryDeltaSupport;
            }
        } else if (BinaryDeltaImpl.isPrototypeOf(context)) {
            return binaryDeltaSupport;
        }

        throw new Error("Unknown delta type: " + context);
    };
});
}).call(this,{"isBuffer":require("../../../../node_modules/browserify/node_modules/insert-module-globals/node_modules/is-buffer/index.js")})
},{"../../../../node_modules/browserify/node_modules/insert-module-globals/node_modules/is-buffer/index.js":6,"../../../data/json/json-datatype":38,"cbor/encoder":61,"data/binary/binary-delta-impl":90,"data/binary/binary-delta-support-impl":91,"data/json/json-impl":98,"util/interface":279}],97:[function(require,module,exports){
(function (Buffer){
var JSONPointerMap = require('data/json/json-pointer-map');
var JSONPointer = require('data/json/json-pointer');
var SpanParser = require('data/json/span-parser');

function isPartOf(original, other, start, length) {
    return original.equalBytes(other.$buffer, other.$offset + start, length);
}

module.exports = function JSONDeltaImpl(jsonDataType, original, newValue, binaryDelta) {
    var inserted = new JSONPointerMap();
    var removed = new JSONPointerMap();

    function partOf(value, start, length) {
        return jsonDataType.readValue(value.$buffer, value.$offset + start, length);
    }

    function copyPartOf(value, start, length) {
        var offsetStart = value.$offset + start;
        var buffer = new Buffer(length);

        value.$buffer.copy(buffer, 0, offsetStart, offsetStart + length);

        return jsonDataType.readValue(buffer, 0, length);
    }

    if (binaryDelta !== undefined) {
        binaryDelta.visit(new DeltaVisitor(original, newValue, inserted, removed, partOf, copyPartOf));
    } else {
        inserted.put(JSONPointer.ROOT, original);
        removed.put(JSONPointer.ROOT, newValue);
    }

    this.removed = function() {
        return new ChangeMapImpl(removed);
    };

    this.inserted = function() {
        return new ChangeMapImpl(inserted);
    };

    this.hasChanges = function() {
        return removed.size !== 0 || inserted.size !== 0;
    };

    this.toString = function() {
        return ['REMOVE ', removed, ' INSERT ', inserted].join('');
    };
};

function DeltaVisitor(oldValue, newValue, inserted, removed, partOf, copyPartOf) {
    var removedSplitStructures = new JSONPointerMap();
    var insertedSplitStructures = new JSONPointerMap();

    var oldParser = new SpanParser(oldValue);
    var newParser = new SpanParser(newValue);

    var oldOffset = 0;

    var newOffset = 0;

    var pendingRemove = null;
    var pendingRemoveValue;

    var pendingInsert = null;
    var pendingInsertValue;

    this.match = function(start, length) {
        handleDelete(oldOffset, start - oldOffset);
        handleMatch(start, length);

        return true;
    };

    function handleDelete(start, length) {
        checkInvariants();

        var end = start + length;

        if (oldParser.nextByte() < end &&
            (oldParser.spanTo(end, remover) !== 0 || oldParser.nextByte() > end)) {
            newParser.spanToNext(newOffset + 1, inserter);
        }
    }

    function handleMatch(start, length) {
        checkInvariants();

        var newStart = newOffset;
        var end = start + length;

        newOffset += length;
        oldOffset = end;

        var oldNextByte = oldParser.nextByte();
        var newNextByte = newParser.nextByte();

        if (newNextByte > newStart && oldNextByte === start) {
            oldParser.spanToNext(start + 1, remover);
        } else if (oldNextByte > start && newNextByte === newStart) {
            newParser.spanToNext(newStart + 1, inserter);
        }

        var lastOld = new LastResult(removedSplitStructures);
        var lastNew = new LastResult(insertedSplitStructures);

        oldParser.spanTo(end, lastOld);
        newParser.spanTo(newOffset, lastNew);

        var oldSplit = lastOld.foundLast() && oldParser.nextByte() > end;
        var newSplit = lastNew.foundLast() && newParser.nextByte() > newOffset;

        if (oldSplit && newSplit) {
            lastOld.consumeLast(remover);
            lastNew.consumeLast(inserter);
        }
    }

    this.insert = function(bytes) {
        checkInvariants();

        newOffset += bytes.length;

        if (newParser.nextByte() < newOffset &&
            (newParser.spanTo(newOffset, inserter) !== 0 || newParser.nextByte() > newOffset)) {
            oldParser.spanToNext(oldOffset + 1, remover);
        }

        return true;
    };

    this.end = function() {
        handleDelete(oldOffset, oldValue.$length - oldOffset);

        addInsert(null, null);
        addRemove(null, null);

        replaceFullRemovedStructures();
        replaceFullInsertedStructures();
    };

    function addInsert(nextPointer, nextValue) {
        if (pendingInsert !== null) {
            inserted.put(pendingInsert, pendingInsertValue);
        }

        pendingInsert = nextPointer;
        pendingInsertValue = nextValue;
    }

    function addRemove(nextPointer, nextValue) {
        if (pendingRemove !== null) {
            removed.put(pendingRemove, pendingRemoveValue);
        }

        pendingRemove = nextPointer;
        pendingRemoveValue = nextValue;
    }

    this.noChange = function() {

    };

    function checkInvariants() {
        if (oldParser.nextByte() < oldOffset ||
            newParser.nextByte() < newOffset) {
            throw new Error("Invalid binary delta");
        }
    }

    function replaceFullRemovedStructures() {
        var i = removedSplitStructures.postOrder();

        while (i.hasNext()) {
            var s = i.next();
            var split = s.value;
            var entry = removed.getEntry(s.pointer);

            if (entry !== null && entry.numberOfChildren() === split.elements) {
                entry.setValue(copyPartOf(oldValue, split.start, split.length));
                entry.removeDescendants();
            }
        }
    }

    function replaceFullInsertedStructures() {
        var i = insertedSplitStructures.postOrder();

        while (i.hasNext()) {
            var s = i.next();
            var split = s.value;
            var entry = inserted.getEntry(s.pointer);

            if (entry !== null && entry.numberOfChildren() === split.elements) {
                entry.setValue(partOf(newValue, split.start, split.length));
                entry.removeDescendants();
            }
        }
    }

    var inserter = {
        accept : function(pointer, start, length) {
            if (pendingRemove !== null &&
                pendingRemove.equalIgnoringIndexes(pointer) &&
                isPartOf(pendingRemoveValue, newValue, start, length)) {

                pendingRemove = null;
                pendingRemoveValue = null;
            } else {
                addInsert(pointer, partOf(newValue, start, length));

                addRemove(null, null);
            }
        },
        splitStructureEnd : function(pointer, count, start, length) {
            insertedSplitStructures.put(pointer, {
                start : start,
                length : length,
                elements : count
            });
        }
    };

    var remover = {
        accept : function(pointer, start, length) {
            if (pendingInsert !== null &&
                pendingInsert.equalIgnoringIndexes(pointer) &&
                isPartOf(pendingInsertValue, oldValue, start, length)) {

                pendingInsert = null;
                pendingInsertValue = null;
            } else {
                addRemove(pointer, copyPartOf(oldValue, start, length));

                addInsert(null, null);
            }
        },
        splitStructureEnd : function(pointer, count, start, length) {
            removedSplitStructures.put(pointer, {
                start : start,
                length : length,
                elements : count
            });
        }
    };
}

function LastResult(splitStructures) {
    var last;
    var lastStart;
    var lastLength;

    this.accept = function (pointer, start, length) {
        last = pointer;
        lastStart = start;
        lastLength = length;
    };

    this.foundLast = function () {
        return !!last;
    };

    this.consumeLast = function (delegate) {
        delegate.accept(last, lastStart, lastLength);
    };

    this.splitStructureEnd = function (pointer, count, start, length) {
        splitStructures.put(pointer, {
            start: start,
            length: length,
            elements: count
        });
    };
}

function ChangeMapImpl(parts) {
    var entrySet = [];

    var i = parts.iterator();

    while (i.hasNext()) {
        var n = i.next();

        entrySet.push({
            key : n.pointer.toString(),
            value : n.value.get()
        });
    }

    this.length = entrySet.length;

    this.get = function(key) {
        return parts.get(JSONPointer.parse(key)).get();
    };

    this.entrySet = function() {
        return entrySet;
    };

    this.containsKey = function(key) {
        return parts.contains(JSONPointer.parse(key));
    };

    this.descendants = function(pointer) {
        return new ChangeMapImpl(parts.descendants(JSONPointer.parse(pointer)));
    };

    this.intersection = function(pointer) {
        return new ChangeMapImpl(parts.intersection(JSONPointer.parse(pointer)));
    };
}
}).call(this,require("buffer").Buffer)
},{"buffer":2,"data/json/json-pointer":100,"data/json/json-pointer-map":99,"data/json/span-parser":101}],98:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var BytesImpl = require('data/bytes-impl');
var Decoder = require('cbor/decoder');

var JSONDeltaImpl = require('data/json/json-delta-impl');
var JSONType = require('../../../data/json/json');

module.exports = _implements(JSONType, function JSONImpl(datatype, buffer, offset, length) {
    BytesImpl.__constructor.call(this, buffer, offset, length);

    var self = this;

    this.get = function() {
        var decoder = new Decoder(buffer, offset, length);
        return decoder.nextValue();
    };

    this.diff = function(original, type) {
        var binaryDiff = datatype.deltaType("binary").diff(original, self);

        if (type === "json") {
            return new JSONDeltaImpl(datatype, original, self, binaryDiff);
        } else {
            return binaryDiff;
        }
    };

    this.jsonDiff = function(original) {
        return self.diff(original, "json");
    };

    this.apply = function(delta) {
        return datatype.deltaType(delta).apply(self, delta);
    };

    this.toString = function() {
        return JSON.stringify(self.get());
    };
});
},{"../../../data/json/json":39,"cbor/decoder":60,"data/bytes-impl":93,"data/json/json-delta-impl":97,"util/interface":279}],99:[function(require,module,exports){
var requireNonNull = require('util/require-non-null');
var JSONPointer = require('data/json/json-pointer');
var EMPTY = new JSONPointerMap();

function JSONPointerMap() {
    this.root = new Entry(null, JSONPointer.ROOT);
    this.size = 0;

    var self = this;

    this.put = function(pointer, value) {
        requireNonNull(value, "value");

        var node = self.root;

        var i = 0;

        for (; i < pointer.segments.length;) {
            var s = pointer.segments[i++];
            var c = node.findChild(s);

            if (c === null) {
                node = node.addChild(s);
                break;
            }

            node = c;
        }

        for (; i < pointer.segments.length; i++) {
            node = node.addChild(pointer.segments[i]);
        }

        return node.setValue(value);
    };

    this.contains = function(pointer) {
        return self.get(pointer) !== null;
    };

    this.get = function(pointer) {
        var entry = self.getEntry(pointer);

        if (entry) {
            return entry.value;
        }

        return null;
    };

    this.getEntry = function(pointer) {
        var result = self.root;

        for (var i = 0; i < pointer.segments.length; ++i) {
            result = result.findChild(pointer.segments[i]);

            if (result === null) {
                return null;
            }
        }

        return result;
    };

    this.descendants = function(pointer) {
        var result = new JSONPointerMap();

        var thisNode = self.root;
        var resultNode = result.root;

        var segments = pointer.segments;
        var length = segments.length;

        if (length === 0) {
            return this;
        }

        for (var i = 0; i < length - 1; ++i) {
            var s = segments[i];

            thisNode = thisNode.findChild(s);

            if (thisNode === null) {
                return EMPTY;
            }

            resultNode = resultNode.addChild(s);
        }

        var last = thisNode.findChild(segments[length - 1]);

        if (last === null) {
            return EMPTY;
        }

        resultNode.children.push(last);
        result.size = last.count();

        return result;
    };

    this.intersection = function(pointer) {
        var result = new JSONPointerMap();

        var thisNode = self.root;
        var resultNode = result.root;

        var segments = pointer.segments;
        var length = segments.length;

        if (length === 0) {
            return this;
        }

        if (thisNode.value !== null) {
            resultNode.setValue(thisNode.value);
        }

        for (var i = 0; i < length - 1; ++i) {
            var s = segments[i];

            thisNode = thisNode.findChild(s);

            if (thisNode === null) {
                return result;
            }

            resultNode = resultNode.addChild(s);

            if (thisNode.value !== null) {
                resultNode.setValue(thisNode.value);
            }
        }

        var last = thisNode.findChild(segments[length - 1]);

        if (last === null) {
            return result;
        }

        resultNode.children.push(last);
        result.size += last.count();

        return result;
    };

    this.iterator = function() {
        var stack = [self.root];

        return new EntryIterator(function() {
            while (true) {
                var r = stack.shift();

                if (r === undefined) {
                    return null;
                }

                var n = r.children.length;

                for (; n > 0; --n) {
                    stack.unshift(r.children[n - 1]);
                }

                if (r.value !== null) {
                    return r;
                }
            }
        });
    };

    this.postOrder = function() {
        var stack = [new PostOrderState(self.root)];

        return new EntryIterator(function() {
            while (true) {
                var r = stack[0];

                if (r === undefined) {
                    return null;
                }

                var c = r.nextChild();

                if (c === null) {
                    stack.shift();

                    if (r.node.value !== null) {
                        return r.node;
                    }
                } else if (c.children.length === 0) {
                    return c;
                } else {
                    stack.unshift(new PostOrderState(c));
                }
            }
        });
    };

    this.toString = function() {
        var parts = ['{'];
        var iter = self.iterator();

        while (iter.hasNext()) {
            if (parts.length > 1) {
                parts.push(', ');
            }

            parts.push(iter.next());
        }

        parts.push('}');

        return parts.join('');
    };

    function Entry(segment, pointer) {
        this.segment = segment;
        this.pointer = pointer;
        this.children = [];
        this.value = null;

        this.count = function() {
            var result = this.value ? 1 : 0;

            for (var i = 0; i < this.children.length; ++i) {
                result += this.children[i].count();
            }

            return result;
        };

        this.addChild = function(s) {
            var c = new Entry(s, pointer.withSegment(s));
            this.children.push(c);

            return c;
        };

        this.findChild = function(s) {
            for (var i = 0; i < this.children.length; ++i) {
                if (s.equals(this.children[i].segment)) {
                    return this.children[i];
                }
            }

            return null;
        };

        this.setValue = function(newValue) {
            var previous = this.value;

            if (previous === null) {
                self.size++;
            }

            this.value = newValue;

            return previous;
        };

        this.removeDescendants = function() {
            var removed = 0;

            for (var i = 0; i < this.children.length; ++i) {
                removed += this.children[i].count();
            }

            this.children = [];
            self.size -= removed;

            return removed;
        };

        this.numberOfChildren = function() {
            var result = 0;

            for (var i = 0; i < this.children.length; ++i) {
                result += (this.children[i].value ? 1 : 0);
            }

            return result;
        };

        this.toString = function() {
            return pointer + "=" + this.value;
        };
    }
}

function PostOrderState(node) {
    this.node = node;
    var nextChild = 0;

    this.nextChild = function() {
        var children = node.children;

        if (nextChild >= children.length) {
            return null;
        }

        return children[nextChild++];
    };
}

function EntryIterator(nextWithValue) {
    var next = nextWithValue();

    this.hasNext = function() {
        return next !== null;
    };

    this.next = function() {
        if (next === null) {
            throw new Error("No such element");
        }

        var result = next;
        next = nextWithValue();
        return result;
    };
}

module.exports = JSONPointerMap;
},{"data/json/json-pointer":100,"util/require-non-null":285}],100:[function(require,module,exports){
function JSONPointer(nodes) {
    this.segments = nodes;
}

JSONPointer.prototype.withKey = function(key) {
    return this.withSegment(new KeySegment(key));
};

JSONPointer.prototype.withIndex = function(index) {
    return this.withSegment(new IndexSegment(index));
};

JSONPointer.prototype.withSegment = function(newNode) {
    var newNodes = this.segments.concat(newNode);
    return new JSONPointer(newNodes);
};

JSONPointer.prototype.equalIgnoringIndexes = function(other) {
    if (other === this) {
        return true;
    }

    var length = this.segments.length;
    if (length !== other.segments.length) {
        return false;
    }

    for (var i = 0; i < length; ++i) {
         var s = this.segments[i];
         var o = other.segments[i];

         if (s instanceof IndexSegment) {
             if (o instanceof KeySegment) {
                 return false;
             }
         } else if (!s.equals(o)) {
             return false;
         }
    }

    return true;
};

JSONPointer.prototype.toString = function() {
    var parts = [];

    for (var i = 0; i < this.segments.length; ++i) {
        parts.push(this.segments[i].toString());
    }

    return parts.join('');
};

JSONPointer.prototype.equals = function(other) {
    if (other === this) {
        return true;
    }

    if (!other || !(other instanceof JSONPointer)) {
        return false;
    }

    if (this.segments.length !== other.segments.length) {
        return false;
    }

    var length = this.segments.length;

    for (var i = 0; i < length; ++i) {
        var s1 = this.segments[i];
        var s2 = other.segments[i];

        if (!s1.equals(s2)) {
            return false;
        }
    }

    return true;
};

JSONPointer.ROOT = new JSONPointer([]);

JSONPointer.parse = function(expression) {
    if (expression instanceof JSONPointer) {
        return expression;
    }

    var result = JSONPointer.ROOT;

    var segmentParser = new Parser(expression);

    while (true) {
        var s = segmentParser.next();

        if (s === null) {
            return result;
        }

        result = result.withSegment(s);
    }
};

module.exports = JSONPointer;

function escape(key) {
    for (var i = 0; i < key.length; ++i) {
        var c = key.charAt(i);

        if (c === '/' || c === '~') {
            var length = key.length;
            var parts = [];

            parts.push(key.substring(0, i));

            for (var j = i; j < length; ++j) {
                var c1 = key.charAt(j);

                if (c1 === '/') {
                    parts.push('~1');
                } else if (c1 === '~') {
                    parts.push('~0');
                }  else {
                    parts.push(c1);
                }
            }

            return parts.join('');
        }
    }

    return key;
}

var INDEX0 = new IndexSegment(0);

function IndexSegment(index) {
    this.index = index;
}

IndexSegment.prototype.equals = function(other) {
    if (other === this) {
        return true;
    }

    if (!other || !(other instanceof IndexSegment)) {
        return false;
    }

    return other.index === this.index;
};

IndexSegment.prototype.toString = function() {
    return '/' + this.index;
};

function KeySegment(key) {
    this.expression = '/' + escape(key);
}

KeySegment.prototype.equals = function(other) {
    if (other === this) {
        return true;
    }

    if (!other || !(other instanceof KeySegment)) {
        return false;
    }

    return other.expression === this.expression;
};

KeySegment.prototype.toString = function() {
    return this.expression;
};

function Parser(expression) {
    var p = 0;

    if (expression !== "" && expression.charAt(0) !== '/') {
        throw new Error("JSON Pointer expression must be empty or start with '/' : " + expression);
    }

    function nextSegmentString() {
        var start = p;

        for (; p < expression.length; ++p) {
            var c = expression[p];

            if (c === '/') {
                break;
            } else if (c === '~') {
                var parts = [expression.substring(start, p)];
                var length = expression.length;

                for (; p < length; ++p) {
                    var c1 = expression.charAt(p);

                    if (c1 === '/') {
                        break;
                    } else if (c1 === '~' && p !== length - 1) {
                        ++p;

                        var c2 = expression.charAt(p);

                        if (c2 === '0') {
                            parts.push('~');
                        } else if (c2 === '1') {
                            parts.push('/');
                        } else {
                            parts.push('~');
                            parts.push(c2);
                        }
                    } else {
                        parts.push(c1);
                    }
                }

                return parts.join('');
            }
        }

        return expression.substring(start, p);
    }

    function maybeIndex(s) {
        var digits = s.length;

        if (digits === 0 || digits > 10) {
            return null;
        }

        for (var i = 0; i < digits; ++i) {
            var c = s.charAt(i);

            if (c < '0' || c > '9') {
                return null;
            }

            if (c === '0' && i === 0) {
                return digits === 1 ? INDEX0 : null;
            }
        }

        var index = parseInt(s, 10);

        if (index > 2147483647) {
            return null;
        }

        return new IndexSegment(index);
    }

    this.next = function() {
        if (p === expression.length) {
            return null;
        }

        ++p;

        var s = nextSegmentString();
        var is = maybeIndex(s);

        if (is !== null) {
            return is;
        }

        return new KeySegment(s);
    };
}
},{}],101:[function(require,module,exports){
var JSONPointer = require('data/json/json-pointer');
var Tokeniser = require('cbor/tokeniser');
var consts = require('cbor/consts');

var Long = require('long');

function rangeToLong(start, end) {
    return Long.fromInt(end).shiftLeft(32).add(start);
}

function ArrayAccumulator(tokeniser, base, start, next) {
    AbstractAccumulator.call(this, tokeniser, base, start, next);

    this.take = function(consumer, base, firstAccumulatedIndex, index, start, length) {
        consumer.accept(base.withIndex(index), start, length);
    };

    this.currentPointer = function(base, total) {
        return base.withIndex(total);
    };
}

function ObjectAccumulator(tokeniser, base, start, next) {
    AbstractAccumulator.call(this, tokeniser, base, start, next);

    var _add = this.add;

    var pendingKeyName = "";
    var pendingKey = true;

    var keys = [];

    this.add = function(tokenEnd) {
        if (pendingKey) {
            this.setNextStart(tokenEnd);

            pendingKeyName = tokeniser.getToken().value;
            pendingKey = false;
        } else {
            keys[this.accumulated()] = pendingKeyName;
            pendingKey = true;

            _add(tokenEnd);
        }

        return !pendingKey;
    };

    this.take = function(consumer, base, firstAccumulatedIndex, index, start, length) {
        consumer.accept(base.withKey(keys[index - firstAccumulatedIndex]), start, length);
    };

    this.currentPointer = function(base, total) {
        return base.withKey(pendingKeyName);
    };
}

function RootAccumulator(tokeniser) {
    AbstractAccumulator.call(this, tokeniser, JSONPointer.ROOT, -1, 0);

    this.take = function(consumer, base, firstAccumulatedIndex, index, start, length) {
        if (this.total() > 1) {
            throw new Error("Invalid JSON: multiple values found");
        }

        consumer.accept(base, start, length);
    };

    this.currentPointer = function(base, total) {
        return base;
    };
}

function AbstractAccumulator(tokeniser, base, start, next) {
    var tokenRange = [];

    var accumulated = 0;
    var total = 0;

    var startOffset = start;
    var nextOffset = next;

    var self = this;

    this.total = function() {
        return total;
    };

    this.accumulated = function() {
        return accumulated;
    };

    this.newArray = function(start, next) {
        return new ArrayAccumulator(tokeniser, self.currentPointer(base, total), start, next);
    };

    this.newObject = function(start, next) {
        return new ObjectAccumulator(tokeniser, self.currentPointer(base, total), start, next);
    };

    this.notEmptyAndSplitBy = function(offset) {
        return startOffset < offset && total !== 0;
    };

    this.add = function(next) {
        tokenRange[accumulated] = [nextOffset, next];
        nextOffset = next;

        ++accumulated;
        ++total;

        return false;
    };

    this.setNextStart = function(offset) {
        nextOffset = offset;
    };

    this.skip = function() {
        ++total;
        accumulated = 0;
    };

    this.takeAll = function(consumer) {
        var next = total;
        var begin = next - accumulated;

        for (var i = 0; i < accumulated; ++i) {
            var range = tokenRange[i];
            var start = range[0];
            var end = range[1];

            self.take(consumer, base, begin, begin + i, start, end - start);
        }

        accumulated = 0;
    };

    this.splitStructureEnd = function(consumer) {
        consumer.splitStructureEnd(base, total, startOffset, nextOffset + 1 - startOffset);
    };

    this.toString = function() {
        return base.toString();
    };
}

function SpanParser(json) {
    var tokeniser = new Tokeniser(json.$buffer, json.$offset, json.$length);

    var structure = new RootAccumulator(tokeniser);
    var parentStack = [];

    var startOffset = tokeniser.offset();
    var self = this;

    this.spanToNext = function(offset, result) {
        return self.spanTo(offset, result, true);
    };

    this.spanTo = function(offset, result, atLeastOne) {
        var start = self.nextByte();

        if (start >= offset) {
            return 0;
        }

        var lastHeight = parentStack.length;

        var consumeFirstValue = atLeastOne;
        var next = start;

        var t;

        do {
            t = tokeniser.nextToken();

            var tokenStart = next;
            next = self.nextByte();

            if (t === null) {
                if (parentStack.length !== 0) {
                    throw new Error("Invalid structure");
                }

                break;
            } else if (t.type === consts.tokens.VALUE) {
                consumeFirstValue = structure.add(next);
            } else if (t.type === consts.tokens.ARRAY_START) {
                parentStack.push(structure);
                structure = structure.newArray(tokenStart, next);
            } else if (t.type === consts.tokens.MAP_START) {
                parentStack.push(structure);
                structure = structure.newObject(tokenStart, next);
            } else if (consts.isStructEnd(t.type)) {
                var parent = parentStack.pop();

                if (structure.notEmptyAndSplitBy(start)) {
                    structure.takeAll(result);
                    structure.splitStructureEnd(result);

                    parent.skip();
                    parent.setNextStart(next);
                } else {
                    parent.add(next);
                }

                structure = parent;
                consumeFirstValue = false;
            }
        } while (consumeFirstValue || next < offset || self.nextTokenIsStructEnd(t, next));

        for (var i = 0; i < parentStack.length; ++i) {
            parentStack[i].takeAll(result);
        }

        structure.takeAll(result);

        return parentStack.length - lastHeight;
    };

    this.nextTokenIsStructEnd = function(t, next) {
        var context = tokeniser.getContext();

        if (consts.isStructStart(t.type)) {
            return context.expected() === 0;
        }

        return context.acceptsBreakMarker() &&
               next < json.$length &&
               (json.$buffer[json.$offset + next] & 0x1F) === consts.additional.BREAK ||
               !context.hasRemaining();
    };

    this.nextByte = function() {
        return tokeniser.offset() - startOffset;
    };

    this.toString = function() {
        var parts = ['SpanParser', ' next=', self.nextByte(), ' ['];

        parentStack.forEach(function(x) {
            parts.push(x);
            parts.push(', ');
        });

        parts.push(structure);
        parts.push(']');

        return parts.join('');
    };
}

module.exports = SpanParser;

},{"cbor/consts":58,"cbor/tokeniser":62,"data/json/json-pointer":100,"long":11}],102:[function(require,module,exports){
module.exports.assignInternals = function(self, buffer, offset, length) {
    self.$buffer = buffer;
    self.$offset = offset || 0;
    self.$length = length === undefined ? buffer.length : length;
};
},{}],103:[function(require,module,exports){
(function (process){
var functions = require('util/function');
var array = require('util/array');

var Stream = require('events/stream');

function Emitter(e, fn, events) {
    var listeners = {};

        var listener = function() { };
    var closed = false;

        var emit = function(event, args) {
        args = args || [];

        var dispatch = function(listener) {
            functions.callWithArguments(listener, args);
        };

        process.nextTick(function() {
            if (listeners[event]) {
                listeners[event].forEach(dispatch);
            }
        });

        listener(event, args);
    };

    var publicEmit = function(event) {
        if (closed || !event) {
            return;
        }

                var args = array.argumentsToArray(arguments);
        args.shift();

                emit(event, args);
    };

        var error = function(reason) {
        emit('error', [reason]);
        close();
    };

        var close = function(reason) {
        if (closed) {
            return;
        }

                closed = true;
        emit('close', [reason]);
    };

        var stream = new Stream(listeners, error, close, events);

        if (e !== undefined) {
        stream.on(e, fn);
    }

        this.listen = function(fn) {
        listener = fn;
    };

        this.get = function() {
        return stream;
    };

            this.close = close;
    this.error = error;
    this.emit = publicEmit;

    this.assign = function(instance) {
        for (var k in stream) {
            instance[k] = stream[k];
        }
    };
}

Emitter.assign = function(instance) {
    var emitter = new Emitter();
    var s = emitter.get();

        for (var k in s) {
        instance[k] = s[k];
    }

        return emitter;
};

module.exports = Emitter;

}).call(this,require('_process'))
},{"_process":7,"events/stream":105,"util/array":276,"util/function":278}],104:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var Result = require('../../events/result');

var when = require('when');

function ResultImpl(emitter) {
    var promise = when.promise(function(fulfilled, rejected) {
        emitter.listen(function(e, args) {
            if (e === 'error') {
                rejected(args[0]);
            } else {
                fulfilled(args[0]);
            }
        });
    });

        this.then = function(fulfillment, rejected, partial) {
        promise = promise.then(fulfillment, rejected, partial);
        return promise;
    };
}

module.exports = _implements(Result, ResultImpl);

},{"../../events/result":43,"util/interface":279,"when":29}],105:[function(require,module,exports){
var _implements = require('util/interface')._implements;

var Stream = require('../../events/stream');

function attach(object, key, value, allowedEvents) {
    if (typeof key === 'object') {
        for (var k in key) {
            attach(object, k, key[k], allowedEvents);
        }
    } else if (value) {
        if (object[key] === undefined) {
            if (allowedEvents && allowedEvents.indexOf(key) === -1) {
                throw new Error('Event ' + key + ' not emitted on this stream, allowed events are ' + allowedEvents);
            }
            object[key] = [];
        } 

        object[key].push(value);
    }

        return object;
}

function remove(object, key, value) {
    if (typeof key === 'object') {
        for (var k in key) {
            remove(object, k, key[k]);
        }
    } else if (object[key]) {
        if (value) {
            object[key] = object[key].filter(function(e) {
                return e !== value;
            });
        } else {
            object[key] = [];
        }
    }

    return object;
}

function StreamImpl(listeners, error, close, events) {
    var allowedEvents;

    if (events) {
        allowedEvents = ['close', 'error', 'complete'].concat(events);
    }

    this.on = function(event, fn) {
        attach(listeners, event, fn, allowedEvents);
        return this;
    };

        this.off = function(event, fn) {
        remove(listeners, event, fn);
        return this;
    };

        this.close = function(reason) {
        close(reason);
        return this;
    };

        this.error = function(reason) {
        error(reason);
        return this;
    };
}

module.exports = _implements(Stream, StreamImpl);

},{"../../events/stream":44,"util/interface":279}],106:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var Services = require('services/services');

var Emitter = require('events/emitter');
var Result = require('events/result');

var SessionID = require('session/session-id');

var parseSelector = require('topics/topic-selector-parser');
var responseHandler = require('control/registration').responseHandler;
var registrationCallback = require('control/registration').registrationCallback;

var api = require('../../features/client-control');

var arrays = require('util/array');

var ClientControlOptions = require('../../features/client-control-options');
var SessionPropertiesEventType = require('services/control/session-properties-event-type');

var requireNotNull = require('util/require-non-null');
var logger = require('util/logger').create('Session.Clients');

module.exports = _implements(api, function ClientControlImpl(internal) {
    var conversationSet = internal.getConversationSet();
    var serviceLocator = internal.getServiceLocator();

    var getProperties = serviceLocator.obtain(Services.GET_SESSION_PROPERTIES);
    var registration = serviceLocator.obtain(Services.SESSION_PROPERTIES_REGISTRATION_2);

    internal.getServiceRegistry().add(Services.SESSION_PROPERTIES_EVENT_2, {
        onRequest : function(internal, message, callback) {
            conversationSet.respond(message.cid, message);
            callback.respond();
        }
    });

    this.subscribe = function(session, path) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        try {
            requireNotNull(session, "SessionID or Session Filter");
            requireNotNull(path, "Topic Selector");
        } catch (e) {
            emitter.error(e);
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var selector;

            try {
                selector = parseSelector(path);
            } catch (e) {
                emitter.error(e);
                return result;
            }

            var sessionID = SessionID.validate(session);

            if (sessionID) {
                serviceLocator.obtain(Services.SUBSCRIBE_CLIENT).send({
                    sessionID : sessionID,
                    selector : selector
                }, function(err, result) {
                    if (err) {
                        emitter.error(err);
                    } else {
                        emitter.emit('complete');
                    }
                });
            } else {
                serviceLocator.obtain(Services.FILTER_SUBSCRIBE).send({
                    filter : session,
                    selector : selector
                }, function(err, result) {
                    if (err) {
                        emitter.error(err);
                    } else if (result.isSuccess()) {
                        emitter.emit('complete', result.selected);
                    } else {
                        emitter.error(result.errors);
                    }
                });
            }
        }

        return result;
    };

    this.unsubscribe = function(session, path) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        try {
            requireNotNull(session, "SessionID or Session Filter");
            requireNotNull(path, "Topic Selector");
        } catch (e) {
            emitter.error(e);
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var selector;

            try {
                selector = parseSelector(path);
            } catch (e) {
                emitter.error(e);
                return result;
            }

            var sessionID = SessionID.validate(session);

            if (sessionID) {
                serviceLocator.obtain(Services.UNSUBSCRIBE_CLIENT).send({
                    sessionID : sessionID,
                    selector : selector
                }, function(err, result) {
                    if (err) {
                        emitter.error(err);
                    } else {
                        emitter.emit('complete');
                    }
                });
            } else {
                serviceLocator.obtain(Services.FILTER_UNSUBSCRIBE).send({
                    filter : session,
                    selector : selector
                }, function(err, result) {
                    if (err) {
                        emitter.error(err);
                    } else if (result.isSuccess()) {
                        emitter.emit('complete', result.selected);
                    } else {
                        emitter.error(result.errors);
                    }
                });
            }
        }

        return result;
    };

    this.getSessionProperties = function(sid, propertyKeys) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (typeof sid === 'string') {
            sid = SessionID.fromString(sid);
        }

        if (internal.checkConnected(emitter)) {
            getProperties.send({
                sessionID : sid,
                propertyKeys : propertyKeys
            }, function(err, result) {
                if (err) {
                    emitter.error(err);
                } else {
                    if (result === null) {
                        emitter.error(new Error('Invalid session ID'));
                    } else {
                        emitter.emit('complete', sid, result.properties);
                    }
                }
            });
        }

        return result;
    };

    this.setSessionPropertiesListener = function(requiredProperties, handler) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!handler) {
            emitter.error(new Error('Session Properties listener is null or undefined'));
        }

        if (internal.checkConnected(emitter)) {
            logger.debug('Adding Session Properties Listener');

            var adapter = {
                active : function(close) {
                    logger.debug('Session Properties Listener active');
                    handler.onActive(close);
                },
                respond : function(message) {
                    for (var i = 0; i < message.events.length; ++i) {
                        var event = message.events[i];

                        switch (event.type) {
                            case SessionPropertiesEventType.OPEN:
                                handler.onSessionOpen(event.sessionId, event.oldProperties);
                                break;
                            case SessionPropertiesEventType.UPDATE:
                                handler.onSessionEvent(
                                    event.sessionId,
                                    event.updateType,
                                    event.newProperties,
                                    event.oldProperties);
                                break;
                            case SessionPropertiesEventType.CLOSE:
                                handler.onSessionClose(
                                    event.sessionId,
                                    event.oldProperties,
                                    event.closeReason);
                                break;
                            default :
                                logger.debug('Unknown event type received for session properties listener', event.type);
                        }
                    }
                },
                close : function(err) {
                    logger.debug('Session Properties Listener closed');

                    if (err) {
                        handler.onError(err);
                    } else {
                        handler.onClose();
                    }
                }
            };

            var cid = conversationSet.new(responseHandler(internal, adapter, function(cid, callback) {
                registration.send({ cid : cid }, callback);
            }));

            registration.send(
                    { cid : cid, properties : requiredProperties },
                    registrationCallback(conversationSet, cid, emitter)); 
        }

        return result;
    };
});

},{"../../features/client-control":49,"../../features/client-control-options":48,"control/registration":82,"events/emitter":103,"events/result":104,"services/control/session-properties-event-type":188,"services/services":220,"session/session-id":254,"topics/topic-selector-parser":267,"util/array":276,"util/interface":279,"util/logger":280,"util/require-non-null":285}],107:[function(require,module,exports){
var topicSelectorParser = require('topics/topic-selector-parser');

var _implements = require('util/interface')._implements;

var CommandError = require('services/command-error');
var Services = require('services/services');

var ConversationId = require('conversation/conversation-id');
var SessionId = require('session/session-id');

var ControlGroup = require('control/control-group');
var registration = require('control/registration');

var Emitter = require('events/emitter');
var Result = require('events/result');

var api = require('../../features/messages');
var arrays = require('util/array');

var logger = require('util/logger').create('Session.Messages');

var dummyCID = ConversationId.fromString("0");

function createSendOptions(options) {
    options = options || {};
    options.headers = options.headers || [];
    options.priority = options.priority || 0;

    return options;
}

function MessageStream(e, selector) {
    this.selector = selector;
    e.assign(this);
}

function parseSessionId(str) {
    try {
        var sid = SessionId.fromString(str);
        return sid;
    }
    catch(err) {
        return false;
    }
}

module.exports = _implements(api, function MessagesImpl(internal) {
    var conversationSet = internal.getConversationSet();
    var serviceLocator = internal.getServiceLocator();

        var sender = serviceLocator.obtain(Services.SEND);
    var sessionSender = serviceLocator.obtain(Services.SEND_TO_SESSION); 
    var filterSender = serviceLocator.obtain(Services.SEND_TO_FILTER);

    var streams = [];

    internal.getServiceRegistry().add(Services.SEND, {
        onRequest : function(internal, message, callback) {
            var received = false;

            streams.forEach(function(stream) {
                if (stream.l.selector.selects(message.path)) {
                    stream.e.emit('message', message);
                    received = true;
                }
            });

            if (received) {
                callback.respond();
            } else {
                logger.info("No messaging streams registered for message on path: " + message.path);

                callback.fail(new CommandError(CommandError.ErrorType.COMMUNICATION_FAILURE,
                    "Session " + internal.getSessionId() +
                    " has no registered streams for message sent to: " + message.path));
            }
        } 
    });

    internal.getServiceRegistry().add(Services.SEND_RECEIVER_CLIENT, {
        onRequest : function(internal, message, callback) {
            callback.respond();
            conversationSet.respond(message.cid, message);
        }
    });

    this.addHandler = function(path, handler, keys) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!path) {
            emitter.error(new Error('Message Handler path is null or undefined'));
            return result;
        }

        if (!handler) {
            emitter.error(new Error('Message Handler is null or undefined'));
            return result;
        }

        if (internal.checkConnected(emitter)) {
            logger.debug('Adding message handler', path);

            var params = {
                definition : Services.SEND_RECEIVER_CLIENT,
                group : ControlGroup.DEFAULT,
                path : path,
                keys : keys !== undefined ? keys : {}
            };

            var adapter = {
                active : function(close) {
                    logger.debug('Message handler active', path);

                    handler.onActive(close);
                },
                respond : function(response) {
                    logger.debug('Message handler response', path);

                    var message = {
                        session : response.sender.toString(),
                        content : response.message,
                        options : response.options,
                        path : response.path
                    };
                    var properties = response.sessionProperties;
                    if (properties && Object.keys(properties).length > 0) {
                        message.properties = properties;
                    }

                    handler.onMessage(message);
                    return false;
                },
                close : function() {
                    logger.debug('Message handler closed', path);

                    handler.onClose();
                }
            };

            return registration.registerMessageHandler(internal, params, adapter);
        } else {
            return result;
        }
    };

    this.listen = function(path, cb) {
        var emitter = new Emitter(undefined, undefined, ['message']);
        var selector = topicSelectorParser(path);

        var stream = new MessageStream(emitter, selector);

        var ref = {
            l : stream, 
            e : emitter
        };

        stream.on('close', function() {
            arrays.remove(streams, ref);
        });

        if (cb && cb instanceof Function) {
            stream.on('message', cb);
        }

        streams.push(ref);
        return stream;
    };

    this.send = function(path, message, options, recipient) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        var sendCallback = function(err, result) {
            if (err) {
                logger.debug('Message send failed', path);
                emitter.error(err);
            } else {
                logger.debug('Message send complete', path);

                if (result) {
                    emitter.emit('complete', {
                        path : path,
                        recipient : recipient,
                        sent : result.sent,
                        errors : result.errors
                    });
                } else {
                    emitter.emit('complete', {
                        path : path,
                        recipient : recipient
                    });
                }
            }
        };

        if (options && (typeof options === 'string' || options instanceof SessionId)) {
            recipient = options;
            options = createSendOptions();
        } else {
            options = createSendOptions(options);
        }

        if (internal.checkConnected(emitter)) {
            if (!path) {
                emitter.error(new Error('Message path is null or undefined'));
                return result;
            }

            if (message === undefined || message === null) {
                emitter.error(new Error('Message content is null or undefined'));
                return result;
            }

            var request;

            if (recipient) {
                var session = typeof recipient === 'string' ? parseSessionId(recipient) : recipient;

                                if (session) {
                    request = {
                        path : path,
                        content : message,
                        session : session,
                        options : options,
                        cid : dummyCID
                    };

                    logger.debug('Sending message to session', request);
                    sessionSender.send(request, sendCallback);
                } else { 
                    request = {
                        path : path,
                        content : message,
                        filter : recipient,
                        options : options,
                        cid : dummyCID
                    };

                    logger.debug('Sending message to filter', request);
                    filterSender.send(request, sendCallback);
                }
            } else {
                request = {
                    path : path,
                    content : message, 
                    options : options
                };

                logger.debug('Sending message to server', request);
                sender.send(request, sendCallback);
            }
        }

        return result;
    };
});


},{"../../features/messages":50,"control/control-group":81,"control/registration":82,"conversation/conversation-id":84,"events/emitter":103,"events/result":104,"services/command-error":165,"services/services":220,"session/session-id":254,"topics/topic-selector-parser":267,"util/array":276,"util/interface":279,"util/logger":280}],108:[function(require,module,exports){
var CHANGE_PRINCIPAL = require('services/services').CHANGE_PRINCIPAL;
var GET_SECURITY_CONFIGURATION = require('services/services').GET_SECURITY_CONFIGURATION;
var UPDATE_SECURITY_CONFIGURATION = require('services/services').UPDATE_SECURITY_CONFIGURATION;

var GET_SYSTEM_AUTHENTICATION = require('services/services').GET_SYSTEM_AUTHENTICATION;
var UPDATE_SYSTEM_AUTHENTICATION = require('services/services').UPDATE_SYSTEM_AUTHENTICATION;

var Emitter = require('events/emitter');
var Result = require('events/result');

var SecurityCommandScript = require('services/authentication/security-command-script');
var SecurityCommandScriptResult = require('services/authentication/security-command-script-result');

var SecurityScriptBuilder = require('features/security/security-script-builder');
var AuthenticationScriptBuilder = require('features/security/system-authentication-script-builder');

var _implements = require('util/interface')._implements;
var api = require('../../features/security');

var requireNonNull = require('util/require-non-null');
var log = require('util/logger').create('Session.Security');

var GlobalPermission = {
    AUTHENTICATE : 0,
    VIEW_SESSION : 1,
    MODIFY_SESSION : 2,
    REGISTER_HANDLER : 3,
    VIEW_SERVER : 4,
    CONTROL_SERVER : 5,
    VIEW_SECURITY : 6,
    MODIFY_SECURITY : 7
};

var TopicPermission = {
    READ_TOPIC : 0,
    UPDATE_TOPIC : 1,
    MODIFY_TOPIC : 2,
    SEND_TO_MESSAGE_HANDLER : 3,
    SEND_TO_SESSION : 4,
    SELECT_TOPIC : 5
};

var SecurityConfiguration = _implements(api.SecurityConfiguration, {
    __constructor : function Configuration(anonymous, named, roles) {
        this.anonymous = anonymous;
        this.named = named;
        this.roles = roles;
    },
    toString : function() {
        return "SecurityConfiguration [anonymous=" + this.anonymous + ", named=" + this.named + 
            ", roles=" + this.roles + "]";
    }
});

var SystemAuthentication = _implements(api.SystemAuthenticationConfiguration, {
    __constructor : function Configuration(principals, action, roles) {
        this.principals = principals || [];
        this.anonymous = {
            action : action,
            roles  : roles || []
        };
    },
    toString : function() {
        return "SystemAuthenticationConfiguration [principals=" + 
            this.principals + ", anonymous=" + this.anonymous + "]"; 
    }
});

SystemAuthentication.CONNECTION_ACTION = {
    ALLOW       : { id : 0, value : 'allow' },
    DENY        : { id : 1, value : 'deny' },
    ABSTAIN     : { id : 2, value : 'abstain' },

    fromString  : function(val) {
        return this[val.toUpperCase()];
    }
};

var SystemPrincipal = _implements(api.SystemPrincipal, {
    __constructor : function SystemPrincipal(name, roles) {
        this.name = requireNonNull(name);
        this.roles = roles || [];
    },
    toString : function() {
        return "SystemPrincipal [" + this.name + ", " + this.roles + "]";    
    }
});

var Security = _implements(api, function SecurityImpl(internal) {
    var changePrincipal = internal.getServiceLocator().obtain(CHANGE_PRINCIPAL);
    var getConfiguration = internal.getServiceLocator().obtain(GET_SECURITY_CONFIGURATION);
    var getSystemAuthentication = internal.getServiceLocator().obtain(GET_SYSTEM_AUTHENTICATION);
    var updateSystemAuthentication = internal.getServiceLocator().obtain(UPDATE_SYSTEM_AUTHENTICATION);
    var updateSecurityConfiguration = internal.getServiceLocator().obtain(UPDATE_SECURITY_CONFIGURATION);

        this.getPrincipal = function() {
        return internal.getPrincipal();
    };

    this.changePrincipal = function(principal, credentials) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            log.debug('Changing principal', principal);

            changePrincipal.send({
                principal : principal,
                credentials : credentials
            }, function(err, response) {
                if (err) {
                    emitter.error(err);
                } else {
                    if (response) {
                        internal.setPrincipal(principal);
                        emitter.emit('complete');
                    } else {
                        emitter.error(new Error('Unable to change principal due to authentication failure'));
                    }
                }
            }); 
        }

        return result;
    };

    this.getSecurityConfiguration = function() {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            log.debug('Getting security configuration');

            getConfiguration.send(null, function(err, response) {
                if (err) {
                    emitter.error(err);
                } else {
                    emitter.emit('complete', response);
                }
            }); 
        }

        return result;
    };

    this.getSystemAuthenticationConfiguration = function() {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            log.debug('Getting system authentication');

            getSystemAuthentication.send(null, function(err, response) {
                if (err) {
                    emitter.error(err);
                } else {
                    emitter.emit('complete', response);
                }
            });
        }

        return result;
     };

    var updateStoreCallback = function(err, result, emitter) {
        if (err) {
            emitter.error(err);
        } else if (result.errors.length > 0) {
            emitter.error(result.errors);
        } else {
            emitter.emit('complete');
        }
    };

    var updateStore = function(updater, script) {
        var emitter = new Emitter();
        var result = new Result(emitter);

                if (script === "") {
            emitter.emit('complete');
        } else if (!script || typeof script !== 'string') {
            emitter.error(new Error('Invalid argument for script:' + script));
        } else if (internal.checkConnected(emitter)) {
            updater.send(new SecurityCommandScript(script), function(err, result) {
                 updateStoreCallback(err, result, emitter);
             });
        }

        return result;
    };

        this.updateSecurityStore = function(script) {
        log.debug('Updating security store');
        return updateStore(updateSecurityConfiguration, script);
    };

    this.updateAuthenticationStore = function(script) {
        log.debug('Updating authentication store');
        return updateStore(updateSystemAuthentication, script);
    };

        this.securityScriptBuilder = function(script) {
         return new SecurityScriptBuilder(script);
     };

    this.authenticationScriptBuilder = function(script) {
         return new AuthenticationScriptBuilder(script);
     };
});

module.exports.GlobalPermission = GlobalPermission;
module.exports.TopicPermission = TopicPermission;

module.exports.Security = Security;
module.exports.SystemPrincipal = SystemPrincipal;

module.exports.Configuration = SecurityConfiguration;
module.exports.SystemAuthentication = SystemAuthentication;

},{"../../features/security":51,"events/emitter":103,"events/result":104,"features/security/security-script-builder":109,"features/security/system-authentication-script-builder":110,"services/authentication/security-command-script":159,"services/authentication/security-command-script-result":157,"services/services":220,"util/interface":279,"util/logger":280,"util/require-non-null":285}],109:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var util = require('features/security/util');
var api = require('../../../features/security');

var requireNonNull = require('util/require-non-null');
var permissionsToString = util.permissionsToString;
var quoteAndEscape = util.quoteAndEscape;
var rolesToString = util.rolesToString;

var SecurityScriptBuilder = _implements(api.SecurityScriptBuilder,
        function SecurityScriptBuilderImpl(commands) {

    commands = commands || "";

               var withCommand = function(command) {
        if (commands) {
            return new SecurityScriptBuilder(commands + '\n' + command);
        }

                return new SecurityScriptBuilder(command);
    };

    this.build = function() {
        return commands;
    };

    this.toString = function() {
        return 'ConfigurationScriptBuilder [' + commands + ']';
    };

    this.setRolesForAnonymousSessions = function(roles) {
        return withCommand("set roles for anonymous sessions " +
                rolesToString(requireNonNull(roles, "roles"))); 
    };

    this.setRolesForNamedSessions = function(roles) {
        return withCommand("set roles for named sessions " +
                rolesToString(requireNonNull(roles, "roles"))); 
    };

    this.setGlobalPermissions = function(role, permissions) {
        return withCommand("set " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " permissions " +
                permissionsToString(requireNonNull(permissions, "permissions")));
    };

    this.setDefaultTopicPermissions = function(role, permissions) { 
        return withCommand("set " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " default topic permissions " +
                permissionsToString(requireNonNull(permissions, "permissions")));    
    };

    this.removeTopicPermissions = function(role, path) {
        return withCommand("remove " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " permissions for topic " +
                quoteAndEscape(requireNonNull(path, "path")));
    };

    this.setTopicPermissions = function(role, path, permissions) {
        return withCommand("set " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " topic " +
                quoteAndEscape(requireNonNull(path, "path")) +
                " permissions " +
                permissionsToString(requireNonNull(permissions, "permissions")));
    };

    this.setRoleIncludes = function(role, included) {
        return withCommand("set " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " includes " +
                rolesToString(requireNonNull(included, "included roles")));
    };
});

module.exports = SecurityScriptBuilder;

},{"../../../features/security":51,"features/security/util":111,"util/interface":279,"util/require-non-null":285}],110:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var util = require('features/security/util');
var api = require('../../../features/security');

var requireNonNull = require('util/require-non-null');
var quoteAndEscape = util.quoteAndEscape;
var rolesToString = util.rolesToString; 

var SystemAuthenticationScriptBuilder = _implements(api.SystemAuthenticationScriptBuilder,
        function SystemAuthenticationScriptBuilderImpl(commands) {

         commands = commands || "";

     var withCommand = function(command) {
         if (commands) {
             return new SystemAuthenticationScriptBuilder(commands + '\n' + command);
         }

         return new SystemAuthenticationScriptBuilder(command);
     };

          this.build = function() {
         return commands;
     };

          this.toString = function() {
         return 'SystemAuthenticationScriptBuilder [' + commands + ']';
     };

     this.assignRoles = function(principal, roles) {
         return withCommand("assign roles " +
                 quoteAndEscape(requireNonNull(principal, "principal")) +
                 ' ' +
                 rolesToString(requireNonNull(roles, "roles")));
     };

         this.addPrincipal = function(principal, password, roles) {
         roles = roles || [];

                  return withCommand('add principal ' + 
                 quoteAndEscape(requireNonNull(principal, "principal")) + 
                 ' ' + 
                 quoteAndEscape(requireNonNull(password, "password")) +
                 (roles.length ? ' ' + rolesToString(roles) : ""));
     };

          this.setPassword = function(principal, password) {
         return withCommand('set password ' +
                 quoteAndEscape(requireNonNull(principal, "principal")) +
                 ' ' +
                 quoteAndEscape(requireNonNull(password, "password")));
     };

          this.verifyPassword = function(principal, password) {
         return withCommand("verify password " +
                 quoteAndEscape(requireNonNull(principal, "principal")) +
                 ' ' +
                 quoteAndEscape(requireNonNull(password, "password")));
     };

          this.removePrincipal = function(principal) {
         return withCommand("remove principal" +
                 ' ' +
                 quoteAndEscape(requireNonNull(principal, "principal")));
     };

          this.allowAnonymousConnections = function(roles) {
         roles = roles || [];
         return withCommand("allow anonymous connections" +
                 ' ' +
                 rolesToString(roles));
     };

          this.denyAnonymousConnections = function() {
         return withCommand("deny anonymous connections");
     };

          this.abstainAnonymousConnections = function() {
         return withCommand("abstain anonymous connections");
     };
});

module.exports = SystemAuthenticationScriptBuilder;

},{"../../../features/security":51,"features/security/util":111,"util/interface":279,"util/require-non-null":285}],111:[function(require,module,exports){
function quoteAndEscape(str) {
    var escaped = '\'', i = 0;

        if (str.indexOf('\'') < 0 && str.indexOf('\\') < 0) {
        escaped += str;
    } else {
        for (; i < str.length; ++i) {
            var c = str[i];

                        if (c === '\'' || c === '\\') {
                escaped += '\\';
            }

                        escaped += c;
        }
    }

        escaped += '\'';
    return escaped;
}

function rolesToString(roles) {
    return '[' + roles.map(quoteAndEscape).join(' ') + ']';    
}

function permissionsToString(permissions) {
    return '[' + permissions.join(' ') + ']';
}

module.exports.permissionsToString = permissionsToString;
module.exports.quoteAndEscape = quoteAndEscape;
module.exports.rolesToString = rolesToString;

},{}],112:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var Services = require('services/services');

var Emitter = require('events/emitter');
var Result = require('events/result');

var ControlGroup = require('control/control-group');
var registerTopicHandler = require('control/registration').registerTopicHandler;

var updateResponseHandler = require('features/update-response-handler');

var TopicAddFailReason = require('../../topics/topics').TopicAddFailReason;

var WillResult = require('services/wills/will-registration-result');
var WillParams = require('services/wills/topic-will-parameters');

var RecordContent = require('content/record-content/record-content');
var Update = require('update/update').Update;

var UniversalUpdater = require('features/topic-control/universal-updater');
var ValueCache = require('features/topic-control/value-cache');

var parseSelector = require('topics/topic-selector-parser');
var deriveDetails = require('topics/details/topic-details').deriveDetails;
var util = require('metadata/util');

var api = require('../../features/topic-control');

var logger = require('util/logger').create('Session.Topics');

module.exports = _implements(api.TopicControl, function TopicControlImpl(internal) {
    var conversationSet = internal.getConversationSet();
    var serviceLocator = internal.getServiceLocator();

    var ADD_SERVICE = serviceLocator.obtain(Services.ADD_TOPIC);
    var REMOVE_SERVICE = serviceLocator.obtain(Services.REMOVE_TOPIC);
    var UPDATE_SERVICE = serviceLocator.obtain(Services.UPDATE_TOPIC);
    var TOPIC_REMOVAL = serviceLocator.obtain(Services.TOPIC_REMOVAL);
    var TOPIC_WILL_REGISTRATION = serviceLocator.obtain(Services.TOPIC_SCOPED_WILL_REGISTRATION);
    var TOPIC_WILL_DEREGISTRATION = serviceLocator.obtain(Services.TOPIC_SCOPED_WILL_DEREGISTRATION);
    var UPDATE_SOURCE_REGISTRATION = serviceLocator.obtain(Services.UPDATE_SOURCE_REGISTRATION);

    var valueCache = new ValueCache();
    var universalUpdater = new UniversalUpdater(internal);

    internal.getServiceRegistry().add(Services.UPDATE_SOURCE_STATE, {
        onRequest : function(internal, message, callback) {
            callback.respond();

            conversationSet.respond(message.cid, {
                old : message.old,
                current : message.current
            });
        }
    });

    internal.getServiceRegistry().add(Services.MISSING_TOPIC, {
        onRequest : function(internal, request, callback) {
            conversationSet.respond(request.cid, {
                request : request,
                callback : function(val) {
                    callback.respond(val);
                }
            });
        }
    });

    this.add = function() {
        var path;
        var details;
        var content;

        var emitter = new Emitter();
        var result = new Result(emitter);

        switch (arguments.length) {
        case 1: 
            path = arguments[0];
            details = deriveDetails();
            break;
        case 2: 
            path = arguments[0];
            details = deriveDetails(arguments[1]);
            content = details.content;
            break;
        case 3: 
            path = arguments[0];
            details = deriveDetails(arguments[1]);
            content = arguments[2];
            break;
        default:
            emitter.error({ id : 0, reason : 'Incorrect number of parameters supplied to add() function' });
            return result;
        }

        if (internal.checkConnected(emitter)) {
            logger.debug('Adding topic', path);

            ADD_SERVICE.send({
                path : path, details : details, content : content
            }, function(err, result) {
                if (err) {
                    emitter.error(err);
                } else {
                    switch (result.status) {
                    case 0:
                    case 1:
                        logger.debug("Topic add complete", path);

                        valueCache.put(path, content);

                        emitter.emit('complete', {
                            topic : path, 
                            added : true
                        });
                        break;
                    case 2:
                        if (result.reason === TopicAddFailReason.EXISTS) {
                            logger.debug("Topic add complete (already exists)", path);

                            valueCache.put(path, content);

                            emitter.emit('complete', {
                                topic : path, 
                                added : false
                            });
                        } else {
                            logger.debug("Topic add failed", path);
                            emitter.error(result.reason);
                        }

                        break;
                    case 3:
                        emitter.error({ id : 0, reason : 'Cache failure' });
                        break;
                    }
                }
            });
        }

        return result;
    };

        this.remove = function(path) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            logger.debug('Removing topic', path);

            try {
                REMOVE_SERVICE.send({
                    selector: parseSelector(path)
                }, function (error, result) {
                    if (error) {
                        logger.debug('Topic removal failed', path);

                        emitter.error(error);
                    } else {
                        logger.debug('Topic removal complete', path);

                        emitter.emit('complete');
                    }
                });
            } catch (err) {
                logger.debug('Error parsing selector', path);
                emitter.error(err);
            }
        }

        return result;
    };

    this.removeSelector = function(expression) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            logger.debug('Removing topics', expression);

            try {
                TOPIC_REMOVAL.send({
                    selector : parseSelector(expression)
                }, function(err) {
                    if (err) {
                        logger.debug('Topic removal failed', expression);
                        emitter.error(err);
                    } else {
                        logger.debug('Topic removal complete', expression);
                        emitter.emit('complete');
                    }
                });
            } catch (err) {
                logger.debug('Error parsing selector', expression);
                emitter.error(err);
            }
        }

        return result;
    };

        this.removeWithSession = function(path) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            logger.debug('Registering removeWithSession', path);

            var params = new WillParams(path, WillParams.Will.REMOVE_TOPICS);

            TOPIC_WILL_REGISTRATION.send(params, function(err, result) {
                if (err) {
                    logger.debug('removeWithSession registration failed', path);
                    emitter.error(err);
                } else if (result === WillResult.SUCCESS) {
                    logger.debug('removeWithSession registration complete', path);

                    var registered = true;

                    var dEmitter = new Emitter();
                    var dResult = new Result(dEmitter);

                    var deregister = function() {
                        if (registered && internal.checkConnected(dEmitter)) {
                            logger.debug('Deregistering removeWithSession', path);

                            TOPIC_WILL_DEREGISTRATION.send(params, function(err, result) {
                                if (err) {
                                    dEmitter.error(err);
                                } else {
                                    registered = false;
                                    dEmitter.emit('complete', {
                                        topic : path
                                    });
                                }
                            });
                        }

                        return dResult;
                    };

                    emitter.emit('complete', {
                        deregister : deregister
                    });
                } else {
                    logger.debug('removeWithSession registration failed', path);
                    emitter.error(new Error('REGISTRATION_CONFLICT'));
                }
            });
        }

        return result;
    };

    this.update = function(path, content) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            logger.debug('Updating topic', path);

            var callback = function(error, result) {
                if (error) {
                    logger.debug('Update failed', path);

                    emitter.error(error);
                } else if (result.isError) {
                    logger.debug('Update failed', path);

                    emitter.error(result.reason);
                } else {
                    logger.debug('Update complete', path);

                    emitter.emit('complete', path);
                }
            };

            if (util.isMetadataValue(content)) {
                UPDATE_SERVICE.send({
                    path : path,
                    update : new Update(content)
                }, callback);
            } else {
                universalUpdater.update(path, content, callback);
            }
        }

        return result;
    };

    this.registerUpdateSource = function(path, handler) {
       var emitter = new Emitter();
       var result = new Result(emitter);

       var conversations = internal.getConversationSet();
       var cid = conversations.new(updateResponseHandler(internal, valueCache, path, handler));

       if (internal.checkConnected(emitter)) {
           UPDATE_SOURCE_REGISTRATION.send({
               cid : cid,
               path : path
           }, function(err, state) {
               if (err || state === 'closed') {
                   conversations.discard(cid, err);
                   emitter.error(err);
               } else {
                   conversations.respond(cid, { old : 'init', current : state });
                   emitter.emit('complete');
               }
           });
       }

       return result;
    };

    this.addMissingTopicHandler = function(path, handler) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!handler) {
            emitter.error(new Error('Missing Topic handler is null or undefined'));
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var adapter = {
                active : function(close) {
                    logger.debug('Missing Topic Handler registered for ' + path);
                    handler.onRegister(path, close);
                },
                respond : function(response) {
                    logger.debug('Missing Topic Handler notification for ' +
                        response.request.sessionID +
                        ' using ' +
                        response.request.selector);

                    handler.onMissingTopic({
                        path : response.request.selector.prefix,
                        selector : response.request.selector,
                        sessionID : response.request.sessionID,
                        proceed : function() {
                            response.callback(true);
                        },
                        cancel : function() {
                            response.callback(false);
                        }
                    });
                },
                close : function(err) {
                    logger.debug('Missing Topic Handler closed for ' + path);
                    if (err) {
                        handler.onError(path, err);
                    } else {
                        handler.onClose(path);
                    }
                }
            };

            var params = {
                definition : Services.MISSING_TOPIC,
                group : ControlGroup.DEFAULT,
                path : path
            };

            return registerTopicHandler(internal, params, adapter);
        }

        return result;
    };
});

},{"../../features/topic-control":52,"../../topics/topics":298,"content/record-content/record-content":74,"control/control-group":81,"control/registration":82,"events/emitter":103,"events/result":104,"features/topic-control/universal-updater":113,"features/topic-control/value-cache":114,"features/update-response-handler":122,"metadata/util":134,"services/services":220,"services/wills/topic-will-parameters":250,"services/wills/will-registration-result":252,"topics/details/topic-details":259,"topics/topic-selector-parser":267,"update/update":275,"util/interface":279,"util/logger":280}],113:[function(require,module,exports){
var Services = require('services/services');
var DataTypes = require('data/datatypes');

var UpdateFailReason = require('../../../topics/topics').UpdateFailReason;

module.exports = function UniversalUpdater(internal) {
    var SET_SERVICE = internal.getServiceLocator().obtain(Services.UPDATE_TOPIC_SET);

    function dataToBytes(d) {
        return d.$buffer.slice(d.$offset, d.$length);
    }

    this.update = function(topic, content, callback) {
        var datatype = DataTypes.get(content);

        if (!datatype) {
            callback(UpdateFailReason.INCOMPATIBLE_UPDATE);
            return;
        }

        var value = datatype.from(content);

        SET_SERVICE.send({
            path : topic,
            bytes : dataToBytes(value)
        }, callback);
    };
};
},{"../../../topics/topics":298,"data/datatypes":94,"services/services":220}],114:[function(require,module,exports){
var canonicalise = require('topics/topic-path-utils').canonicalise;

module.exports = function ValueCache() {
    var cache = {};

    this.get = function(path) {
        return cache[canonicalise(path)];
    };

    this.put = function(path, value) {
        cache[canonicalise(path)] = value;
    };

    this.remove = function(selector) {
        for (var k in cache) {
            if (selector.selects(k)) {
                delete cache[k];
            }
        }
    };
};
},{"topics/topic-path-utils":266}],115:[function(require,module,exports){
var _implements = require('util/interface')._implements;

var Emitter = require('events/emitter');
var Result = require('events/result');
var View = require('features/topics/view');

var Services = require('services/services');
var parseSelector = require('topics/topic-selector-parser');

var SubscriptionProxy = require('features/topics/subscription-proxy');
var FetchStream = require('features/topics/fetch-stream');

var Topics = require('../../features/topics');

var CloseReason = require('v4-stack/close-reason');

var requireNonNull = require('util/require-non-null');
var logger = require('util/logger').create('Session');

module.exports = _implements(Topics, function TopicsImpl(internal) {
    var unsubscribe = internal.getServiceLocator().obtain(Services.UNSUBSCRIBE);
    var subscribe = internal.getServiceLocator().obtain(Services.SUBSCRIBE);
    var fetch = internal.getServiceLocator().obtain(Services.FETCH);

    var streamRegistry = internal.getStreamRegistry();

    this.subscribe = function(topic, callback) {
        logger.debug('Subscribing', topic);

        var selector = parseSelector(topic);
        var proxy = new SubscriptionProxy(streamRegistry, selector, false, callback);

                if (internal.checkConnected(proxy.emitter)) {
            subscribe.send(selector);
        }

        return proxy.subscription;
    };

    this.unsubscribe = function(topic) {
        logger.debug('Unsubscribe request for: ', topic);

        var emitter = new Emitter();
        var result = new Result(emitter);
        var selector;

        try {
            selector = parseSelector(topic);
        } catch (err) {
            emitter.error(err);
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var unsubscribeCallback = function(err, result) {
                if (err) {
                    logger.debug('Unsubscribe failed', topic);
                    emitter.error(err);
                } else {
                    logger.debug('Unsubscribe complete', topic);
                    emitter.emit('complete');
                }
            };

            logger.debug('Unubscribing', topic);
            unsubscribe.send(selector, unsubscribeCallback);
        }
        return result;
    };

    this.stream = function(topic, callback) {
        logger.debug('Establishing topic stream', topic);

        var fallback = false;
        var selector = "";

        if (!topic || typeof topic === 'function') {
            callback = topic;
            fallback = true;
        } else {
            selector = parseSelector(topic);
        }

        var proxy = new SubscriptionProxy(streamRegistry, selector, fallback, callback);

        return proxy.subscription;
    };

    this.fetch = function(selector) {
        logger.debug('Fetch request for: ', selector);

        requireNonNull(selector, "Selector");

        selector = parseSelector(selector);

        var emitter = new Emitter();
        var stream = new FetchStream(emitter);

        if (internal.checkConnected(emitter)) {
            var cid = internal.getConversationSet().new({
                onOpen : function(cid) { },
                onResponse : function(cid, path, content) {
                    if (path) {
                        emitter.emit('value', content, path);
                        return false;
                    } else if (content.length === 1 && content[0] === 1) {
                        emitter.close();
                        return true;
                    } else {
                        emitter.error(new Error("Unexpected end of fetch stream"));
                        return true;
                    }
                },
                onDiscard : function(err) {
                    if (err instanceof CloseReason) {
                        emitter.close();
                    } else {
                        emitter.error(err);
                    }
                }
            });

            fetch.send({
                cid : cid,
                selector : selector
            }, function(err) {
                if (err) {
                    emitter.error(err);
                }
            });
        }

        return stream;
    };

    this.view = function(selector, callback) {
        var s = this.subscribe(selector);
        return new View(s, callback);
    };
});

},{"../../features/topics":53,"events/emitter":103,"events/result":104,"features/topics/fetch-stream":116,"features/topics/subscription-proxy":117,"features/topics/view":121,"services/services":220,"topics/topic-selector-parser":267,"util/interface":279,"util/logger":280,"util/require-non-null":285,"v4-stack/close-reason":288}],116:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var FetchStream = require('../../../events/fetch-stream');

module.exports = _implements(FetchStream, function FetchStreamImpl(emitter) {
    emitter.assign(this);
});
},{"../../../events/fetch-stream":42,"util/interface":279}],117:[function(require,module,exports){
var SubscriptionImpl = require('features/topics/subscription');
var Emitter = require('events/emitter');

module.exports = function SubscriptionProxy(registry, selector, fallback, callback) {
    var emitter = new Emitter();
    var stream = emitter.get();
    var self = this;

    var subscription = new SubscriptionImpl(registry, stream, fallback, selector);
    var pending = true;

    stream.on('close', function() {
        registry.remove(self);
    });

    this.subscription = subscription;
    this.emitter = emitter;

    this.selects = function() {
        return true;
    };

    this.onOpen = function() {
        emitter.emit('open', subscription);
    };

    this.onDelta = function(topic, details, value) {
        emitter.emit('update', value, topic);
    };

    this.onValue = function(topic, details, value) {
        emitter.emit('update', value, topic);
    };

    this.onSubscription = function(topic, details) {
        emitter.emit('subscribe', details, topic);
    };

    this.onUnsubscription = function(topic, details, reason) {
        emitter.emit('unsubscribe', reason, topic);
    };

    if (callback) {
        subscription.on('update', callback);

        if (fallback) {
            registry.addFallback(self);
        } else {
            registry.add(selector, self);
        }
    } else {
        subscription.on = function(event, fn) {
            if (pending) {
                if (fallback) {
                    registry.addFallback(self);
                } else {
                    registry.add(selector, self);
                }

                pending = false;
            }

            return stream.on.call(subscription, event, fn);
        };
    }
};
},{"events/emitter":103,"features/topics/subscription":118}],118:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var Subscription = require('../../../events/subscription');

var TypedSubscriptionProxy = require('features/topics/typed-subscription-proxy');
var View = require('features/topics/view');

var Emitter = require('events/emitter');
var util = require('metadata/util');

module.exports = _implements(Subscription, function SubscriptionImpl(registry, stream, fallback, selector) {
    for (var fn in stream) {
        this[fn] = stream[fn];
    }

    this.selector = selector;
    var self = this;

    this.view = function() {
        return new View(self);
    };

    this.asType = function(datatype, callback) {
        var proxy = new TypedSubscriptionProxy(registry, selector, datatype, fallback, callback);
        return proxy.subscription;
    };

    this.transform = function(fn) {
        var e = new Emitter();

        fn = util.isMetadata(fn) ? fn.parse.bind(fn) : fn;

        self.on('open', function(selector, s) {
            e.emit('open', selector, self);
        });

        self.on('close', function() {
            e.emit('close');
        });

        self.on('update', function(update, topic) {
            e.emit('update', fn(update), topic);
        });

        self.on('subscribe', function(details) {
            e.emit('subscribe', details);
        });

        self.on('unsubscribe', function(reason, topic) {
            e.emit('unsubscribe', reason, topic);
        });

        return new SubscriptionImpl(registry, e.get(), selector);
    };
});
},{"../../../events/subscription":45,"events/emitter":103,"features/topics/typed-subscription-proxy":119,"features/topics/view":121,"metadata/util":134,"util/interface":279}],119:[function(require,module,exports){
var TypedSubscriptionImpl = require('features/topics/typed-subscription');
var TopicType = require('../../../topics/topics').TopicType;
var Emitter = require('events/emitter');

module.exports = function TypedSubscriptionProxy(registry, selector, datatype, fallback, callback) {
    var emitter = new Emitter();
    var stream = emitter.get();
    var self = this;

    var subscription = new TypedSubscriptionImpl(registry, stream, selector);
    var pending = true;

    stream.on('close', function() {
        registry.remove(self);
    });

    this.subscription = subscription;
    this.emitter = emitter;

    this.selects = function(details) {
        if (details) {
            return details.type === TopicType[datatype.name().toUpperCase()];
        }

        return false;
    };

    this.onOpen = function() {
        emitter.emit('open', subscription);
    };

    this.onDelta = function(topic, details, received, delta, oldValue, newValue) {
        emitter.emit('value', topic, details, newValue, oldValue);
    };

    this.onValue = function(topic, details, received, oldValue, newValue) {
        emitter.emit('value', topic, details, newValue, oldValue);
    };

    this.onSubscription = function(topic, details) {
        emitter.emit('subscribe', topic, details);
    };

    this.onUnsubscription = function(topic, details, reason) {
        emitter.emit('unsubscribe', topic, details, reason);
    };

    if (callback) {
        subscription.on('value', callback);

        if (fallback) {
            registry.addFallback(self);
        } else {
            registry.add(selector, self);
        }
    } else {
        subscription.on = function(event, fn) {
            if (pending) {
                if (fallback) {
                    registry.addFallback(self);
                } else {
                    registry.add(selector, self);
                }

                pending = false;
            }

            return stream.on.call(subscription, event, fn);
        };
    }
};
},{"../../../topics/topics":298,"events/emitter":103,"features/topics/typed-subscription":120}],120:[function(require,module,exports){
var _implements = require('util/interface')._implements;

var TypedSubscriptionProxy = require('features/topics/typed-subscription-proxy');
var TypedSubscription = require('../../../events/typed-subscription');

module.exports = _implements(TypedSubscription, function TypedSubscriptionImpl(registry, stream, selector) {
    for (var fn in stream) {
        this[fn] = stream[fn];
    }

    this.selector = selector;
});
},{"../../../events/typed-subscription":46,"features/topics/typed-subscription-proxy":119,"util/interface":279}],121:[function(require,module,exports){
var _implements = require('util/interface')._implements;

var Emitter = require('events/emitter');
var View = require('../../../events/view');

function ViewImpl(subscription) {
    var emitter = Emitter.assign(this);
    var data = {};

    subscription.on('update', function(value, topic) {
        var parts = topic.split('/');
        var child = data;
        var i = 0;

                for (; i < parts.length - 1; i++) {
            if (child[parts[i]] === undefined) {
                child[parts[i]] = {};
            }

                        child = child[parts[i]];
        }

                child[parts[i]] = value;

                emitter.emit('update', data);
    });

    subscription.on('unsubscribe', function(reason, topic) {
        if (reason.id === 2) {
            var parts = topic.split('/');
            var child = data;
            var i = 0;

            for (; i < parts.length - 1; i++) {
                if (child[parts[i]] === undefined) {
                    break;
                }

                child = child[parts[i]];
            }

            delete child[parts[i]];

            emitter.emit('update', data);
        }
    });

        subscription.on('close', emitter.close);

        this.get = function() {
        return data;
    };
}

module.exports = _implements(View, ViewImpl);

},{"../../../events/view":47,"events/emitter":103,"util/interface":279}],122:[function(require,module,exports){
var parseSelector = require('topics/topic-selector-parser');
var canonicalise = require('topics/topic-path-utils').canonicalise;

var Services = require('services/services');
var DataTypes = require('data/datatypes');
var Emitter = require('events/emitter');
var Result = require('events/result');
var Update = require('update/update');

var UpdateFailReason = require('../../topics/topics').UpdateFailReason;
var CloseReason = require('v4-stack/close-reason');

var util = require('metadata/util');

function dataToBytes(d) {
    return d.$buffer.slice(d.$offset, d.$length);
}

function clearCache(cache, path) {
    var selector = parseSelector('?' + canonicalise(path) + '//');
    cache.remove(selector);
}

function Updater(cid, dispatch) {
    var self = this;

    this.isClosed = false;

    this.update = function update(topic, value) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (self.isClosed) {
            emitter.error(new Error('Updater is closed'));
        } else if (!topic) {
            emitter.error(new Error('Topic can not be null or empty'));
        } else if (value === undefined || value === null) {
            emitter.error(new Error('Update cannot be null'));
        } else {
            dispatch(emitter, cid, topic, value);
        }

        return result;
    };
}

module.exports = function UpdateResponseHandler(internal, valueCache, topic, handler) {
    var UPDATE_SOURCE_DEREGISTRATION = internal.getServiceLocator().obtain(Services.UPDATE_SOURCE_DEREGISTRATION);
    var UPDATE_SOURCE_UPDATE = internal.getServiceLocator().obtain(Services.UPDATE_SOURCE_UPDATE);
    var UPDATE_SOURCE_DELTA = internal.getServiceLocator().obtain(Services.UPDATE_SOURCE_DELTA);
    var UPDATE_SOURCE_SET = internal.getServiceLocator().obtain(Services.UPDATE_SOURCE_SET);

    var dispatch = function(emitter, cid, path, content) {
        var callback = function(err, result) {
            if (err) {
                emitter.error(err);
            } else if (result.error) {
                emitter.error(new Error("Topic update error for topic " + path + " : " + result.error));
            } else {
                emitter.emit('complete');
            }
        };

        if (internal.checkConnected(emitter)) {
             if (util.isMetadataValue(content)) {
                 UPDATE_SOURCE_UPDATE.send({
                     cid : cid,
                     path : path,
                     update : new Update.Update(content)
                 }, callback);
             } else {
                 var datatype = DataTypes.get(content);

                 if (!datatype) {
                     emitter.error(UpdateFailReason.INCOMPATIBLE_UPDATE);
                     return;
                 }

                 var value = datatype.from(content);
                 var prev = valueCache.get(path);

                 if (prev) {
                     var deltaType = datatype.deltaType("binary");
                     var delta = deltaType.diff(prev, value);

                     if (delta === deltaType.noChange()) {
                         callback(null, {});
                         return;
                     }

                     UPDATE_SOURCE_DELTA.send({
                         id : 0,
                         cid : cid,
                         path : path,
                         bytes : dataToBytes(delta)
                     }, callback);
                 } else {
                     UPDATE_SOURCE_SET.send({
                         cid : cid,
                         path : path,
                         bytes : dataToBytes(value)
                     }, callback);
                 }

                 valueCache.put(path, value);
             }
         }
    };

    var state = 'init';
    var close, updater;

    return {
        onOpen : function(cid) {
            close = function close() {
                var emitter = new Emitter();
                var result = new Result(emitter);

                UPDATE_SOURCE_DEREGISTRATION.send({ cid : cid }, function(err) {
                    if (err) {
                        internal.getConversationSet().discard(cid, err);
                        emitter.error(err);
                    } else {
                        internal.getConversationSet().respond(cid, {
                            old : state,
                            current : 'closed'
                        });

                        emitter.emit('complete');
                    }
                });

                return result;
            };
        },
        onResponse : function(cid, change) {
            if (change.old !== state) {
                throw new Error("Inconsistent server/client update source state");
            }

            if (state === 'init' && change.current !== 'closed') {
                handler.onRegister(topic, close);
            }

            if (updater) {
                updater.isClosed = true;
            }

            state = change.current;

            switch (state) {
                case 'active' :
                    updater = new Updater(cid, dispatch);

                    clearCache(valueCache, topic);

                    handler.onActive(topic, updater);
                    return false;
                case 'standby' :
                    handler.onStandBy(topic);
                    return false;
                default :
                    clearCache(valueCache, topic);

                    handler.onClose(topic);
                    return true;
            }
        },
        onDiscard : function(cid, reason) {
            state = 'closed';

            if (updater) {
                updater.isClosed = true;
            }

            clearCache(valueCache, topic);

            if (reason instanceof CloseReason) {
                handler.onClose(topic);
            } else {
                handler.onClose(topic, reason);
            }
        }
    };
};

},{"../../topics/topics":298,"data/datatypes":94,"events/emitter":103,"events/result":104,"metadata/util":134,"services/services":220,"topics/topic-path-utils":266,"topics/topic-selector-parser":267,"update/update":275,"v4-stack/close-reason":288}],123:[function(require,module,exports){
var Long = require('long');

function BufferInputStream(buffer) {
    if (buffer === null || buffer === undefined) {
        throw new Error("Undefined buffer for InputStream");
    }

        this.buffer = buffer;
    this.count = buffer.length;
    this.mark = 0;
    this.pos = 0;
}

BufferInputStream.prototype.read = function() {
    return (this.pos < this.count) ? (this.buffer[this.pos++] & 0xFF) : -1;
};

BufferInputStream.prototype.readMany = function(length) {
    length = Math.min(length, this.count - this.pos);

        if (length < 0) {
        throw new Error("Length out of bounds");
    }

    var buffer = this.buffer.slice(this.pos, this.pos + length);
    this.pos += length;
    return buffer;
};

BufferInputStream.prototype.readUntil = function(delim) {
    var found = this.count;

        for (var i = this.pos; i < this.count; i++) {
        if (this.buffer[i] === delim) {
            found = i;
            break;
        }
    }

        var buffer = this.buffer.slice(this.pos, found);
    if (found === this.count) {
        this.pos = found;
    } else {
        this.pos = found + 1;
    }

    return buffer;
};

BufferInputStream.prototype.readInt8 = function() {
    return this.buffer.readInt8(this.pos++);
};

BufferInputStream.prototype.readInt32 = function() {
    var i = this.buffer.readInt32BE(this.pos);
    this.pos += 4;
    return i;
};

BufferInputStream.prototype.readInt64 = function() {
    var hi = this.buffer.readInt32BE(this.pos);
    this.pos += 4;
    var lo = this.buffer.readInt32BE(this.pos);
    this.pos += 4;

       return Long.fromBits(lo, hi, false);
};

BufferInputStream.prototype.readUInt64 = function() {
    var hi = this.buffer.readInt32BE(this.pos);
    this.pos += 4;
    var lo = this.buffer.readInt32BE(this.pos);
    this.pos += 4;

       return Long.fromBits(lo, hi, true);
};

BufferInputStream.prototype.hasRemaining = function() {
    return this.pos < this.buffer.length;
};

module.exports = BufferInputStream;

},{"long":11}],124:[function(require,module,exports){
(function (Buffer){
function ensureCapacity(bos, min) {
    if (min - bos.buffer.length > 0) {
        grow(bos, min);
    }
}

function grow(bos, minCapacity) {
    var oldCapacity = bos.buffer.length,
        newCapacity = oldCapacity << 1;

    if (newCapacity - minCapacity < 0) {
        newCapacity = minCapacity;
    }

        try {
        var replacement = new Buffer(newCapacity);
        bos.buffer.copy(replacement);
        bos.buffer = replacement;
    } catch (e) {
        throw new Error("Unable to resize BufferOutputStream to " + newCapacity);
    }
}

function BufferOutputStream(initial) {
    if (Buffer.isBuffer(initial)) {
        this.buffer = initial;
        this.count = initial.length;
    } else {
        this.buffer = new Buffer((initial || 32));
        this.count = 0;
    }
}

BufferOutputStream.prototype.write = function(val) {
    ensureCapacity(this, this.count + 1);
    this.buffer[this.count++] = val;
};

BufferOutputStream.prototype.writeMany = function(buffer, offset, length) {
    offset = offset || 0;
    length = length || buffer.length;

   ensureCapacity(this, this.count + length);

      buffer.copy(this.buffer, this.count, offset, offset + length);
   this.count += length;
};

BufferOutputStream.prototype.writeString = function(val) {
    var length = Buffer.byteLength(val);
    ensureCapacity(this, this.count + length);

    this.buffer.write(val, this.count, length);
    this.count += length;
};

BufferOutputStream.prototype.writeInt8 = function(val) {
    ensureCapacity(this, this.count + 1);

        this.buffer.writeInt8(val, this.count++);
};

BufferOutputStream.prototype.writeInt32 = function(val) {
    ensureCapacity(this, this.count + 4);

        this.buffer.writeInt32BE(val, this.count);
    this.count += 4;
};

BufferOutputStream.prototype.writeInt64 = function(val) {
    ensureCapacity(this, this.count + 8);

    this.buffer.writeInt32BE(val.getHighBits(), this.count);
    this.buffer.writeInt32BE(val.getLowBits(), this.count + 4);

    this.count += 8;
};

BufferOutputStream.prototype.getBuffer = function() {
    return this.buffer.slice(0, this.count);
};

BufferOutputStream.prototype.getBase64 = function() {
    return this.getBuffer().toString('base64');
};

module.exports = BufferOutputStream;

}).call(this,require("buffer").Buffer)
},{"buffer":2}],125:[function(require,module,exports){
(function (Buffer){
var Long = require('long');

var x80 = Long.fromNumber(0x80),
    x7F = Long.fromNumber(0x7F),
    x7FInv = x7F.not(); 

var readOneByte = function(input) {
    var i = input.read();

    if (i === -1) {
        throw new Error("End of stream");
    }

        return i;
};

var codec = {};

codec.readInt64 = function(input) {
    var result = Long.fromNumber(0), shift = 0;

    while (shift < 64) {
        var i = readOneByte(input),
            l = Long.fromNumber(i);

        result = result.or(l.and(x7F).shiftLeft(shift));

        if (l.and(x80).equals(0)) {
            return result;
        }

        shift += 7;
    }

    throw "Malformed int64";
};

codec.writeInt64 = function(bos, value) {
    var int64 = value instanceof Long ? value : Long.fromNumber(value, false);

    while (true) {
        if (int64.and(x7FInv).equals(0)) {
            bos.write(int64.toInt());
            return;
        } else {
            bos.write(int64.and(x7F).or(x80).toInt());
            int64 = int64.shiftRightUnsigned(7);
        }
    }
};

codec.readInt32 = function(bis) {
    var shift = 0,
        result = 0;

    while (shift < 32) {
        var i = readOneByte(bis);

        result |= (i & 0x7F) << shift;

        if ((i & 0x80) === 0) {
            return result;
        }

        shift += 7;
    }

    throw "Malformed int32";
};

codec.writeInt32 = function(bos, value) {
    while (true) {
        if ((value & ~0x7F) === 0) {
            bos.write(value);
            return;
        } else {
            bos.write((value & 0x7F) | 0x80);
            value = value >>> 7;
        }
    }
};

codec.writeByte = function(bos, value) {
    bos.writeInt8(value);

    if ((value & ~0x7F) !== 0) {
        bos.write(1);
    }
};

codec.readByte = function(bis) {
    var b1 = bis.readInt8();

    if ((b1 & ~0x7F) === 0) {
        return b1;
    }

    var b2 = readOneByte(bis);

    if (b2 !== 1) {
        throw "Malformed byte";
    }

    return (b1 | 0x80);
};

codec.readBytes = function(bis) {
    var length = codec.readInt32(bis);
    return bis.readMany(length);
};

codec.writeBytes = function(bos, value) {
    codec.writeInt32(bos, value.length);
    bos.writeMany(value);
};

codec.readString = function(bis) {
    var buffer = codec.readBytes(bis);
    return buffer ? buffer.toString('utf8') : "";
};

codec.writeString = function(bos, value) {
    codec.writeBytes(bos, new Buffer(value, 'utf8'));
};

codec.writeCollection = function(bos, arr, write) {
    codec.writeInt32(bos, arr.length);

        for (var i = 0; i < arr.length; ++i) {
        write(bos, arr[i]);
    }
};

codec.readCollection = function(bis, read) {
    var length = codec.readInt32(bis);
    var arr = [];

        for (var i = 0; i < length; ++i) {
        arr.push(read(bis));
    }

        return arr;
};

codec.readDictionary = function(bis, read) {
    var length = codec.readInt32(bis);
    var dict = {};

    for (var i = 0; i < length; ++i) {
        var k = codec.readString(bis);
        dict[k] = read(bis);
    }

    return dict;
};

codec.writeDictionary = function(bos, dict, write) {
    codec.writeInt32(bos, Object.keys(dict).length);

        for (var k in dict) {
        codec.writeString(bos, k);
        write(bos, dict[k]);
    }
};

codec.readBoolean = function(bis) {
    var b = bis.readInt8();
    if (b === 0) {
        return false;
    }
    return true;
};

codec.writeBoolean = function(bos, value) {
    if (value) {
        bos.writeInt8(1);
    } else {
        bos.writeInt8(0);
    }
};

module.exports = codec;

}).call(this,require("buffer").Buffer)
},{"buffer":2,"long":11}],126:[function(require,module,exports){
var findNextPowerOfTwo = require('util/math').findNextPowerOfTwo;
var curryR = require('util/function').curryR;

var ofSize = require('util/array').ofSize;
var fill = require('util/array').fill;

var TIMESTAMP_ROUNDING = 10;

function roundTimeStamp(timestamp) {
    return timestamp >> TIMESTAMP_ROUNDING;
}

function mask(p, capacity) {
    return p & capacity - 1;
}

module.exports = function RecoveryBuffer(minimumMessages, minimumTimeIndex) {
    var messagesLength = findNextPowerOfTwo(minimumMessages);
    var indexCapacity = findNextPowerOfTwo(minimumTimeIndex);

    var messagesMask = curryR(mask, messagesLength);
    var indexMask = curryR(mask, indexCapacity);

    var messages = ofSize(messagesLength);
    var indices = ofSize(indexCapacity, -1);
    var times = ofSize(indexCapacity);

    var timesHead = 0;
    var timesTail = 0;

    var tail = messagesMask(0);
    var size = 0;

    this.size = function() {
        return size;
    };

    this.put = function(message) {
        messages[tail] = message;
        tail = messagesMask(tail + 1);

        if (size < messagesLength) {
            size = size + 1;
        } else {
            while (indices[timesHead] === tail) {
                indices[timesHead] = -1;
                timesHead = indexMask(timesHead + 1);
            }
        }
    };

    this.recover = function(n, consumer) {
        if (n < 0 || n > size) {
            return false;
        }

        for (var i = n; i >= 1; --i) {
            consumer(messages[messagesMask(tail - i)]);
        }

        return true;
    };

    this.clear = function() {
        fill(messages, null);
        fill(indices, -1);

        timesHead = 0;
        timesTail = 0;
        size = 0;
    };

    function removeElements(newElementsHead) {
        var messagesHead = messagesMask(tail - size);
        var newSize;

        if (messagesHead < newElementsHead) {
            fill(messages, null, messagesHead, newElementsHead);

            newSize = size - newElementsHead + messagesHead;
        } else if (messagesHead > newElementsHead) {
            fill(messages, null, messagesHead, messagesLength);
            fill(messages, null, 0, newElementsHead);

            newSize = size - messagesHead + newElementsHead;
        } else {
            fill(messages, null);
            newSize = 0;
        }

        size = newSize;
    }

    this.markTime = function(timestamp) {
        if (size > 0) {
            var h = timesHead;
            var t = timesTail;
            var firstIndex = indices[h];
            var roundedTimestamp = roundTimeStamp(timestamp);

            if (firstIndex >= 0) {
                var indexLast = indexMask(t - 1);

                if (indices[indexLast] === tail) {
                    return;
                }

                if (times[indexLast] === roundedTimestamp) {
                    indices[indexLast] = tail;
                    return;
                }
            }

            timesTail = indexMask(t + 1);
            times[t] = roundedTimestamp;
            indices[t] = tail;

            if (h === t && firstIndex >= 0) {
                removeElements(firstIndex);
                timesHead = timesTail;
            }
        }
    };

    this.flush = function(timestamp) {

        if (size === 0 || indices[timesHead] < 0) {
            return;
        }

        var i = timesTail;
        var roundedTimestamp = roundTimeStamp(timestamp);

        do {
            var previousI = i;
            i = indexMask(i - 1);

            if (times[i] <= roundedTimestamp) {
                removeElements(indices[i]);

                if (timesHead <= previousI) {
                    fill(indices, -1, timesHead, previousI);
                } else {
                    fill(indices, -1, timesHead, indexCapacity);
                    fill(indices, -1, 0, previousI);
                }

                timesHead = previousI;
                return;
            }
        } while (i !== timesHead);
    };
 };
},{"util/array":276,"util/function":278,"util/math":281}],127:[function(require,module,exports){
module.exports = function MDecimal(val, scale) {
    var self = this;

        if (val !== undefined && typeof val !== "number") {
        throw new Error('Decimal metadata must be given a numeric default value');
    }

        if (scale !== undefined && typeof scale !== "number") {
        throw new Error('Decimal metadata must be given a numeric scale');
    }

        this.value = val || 0.00;

        if (scale === undefined) {
        var str = this.value + '', i = str.indexOf('.');
        this.scale = i > -1 ? str.length - (i + 1) : 2; 
    } else {
        this.scale = scale;
    }

        this.getDetails = function() {
        return {
            type : 'decimalString',
            name : 'decimalString',
            default : self.value,
            scale : self.scale
        };
    };

        this.parse = function(buffer) {
        var str = buffer.toString(), i = str.indexOf('.');
        return +str.substr(0, i + this.scale + 1);
    };
};
},{}],128:[function(require,module,exports){
module.exports = function MInteger(val) {
    var self = this;

        if (val !== undefined && typeof val !== "number") {
        throw new Error('Integer metadata must be given a numeric default value');
    }

        this.value = parseInt(val, 10) || 0;

        this.getDetails = function() {
        return {
            type : 'integerString',
            name : 'integerString',
            default : self.value
        };
    };

        this.parse = function(buffer) {
        return parseInt(buffer.toString(), 10);
    };
};
},{}],129:[function(require,module,exports){
var _implements = require('util/interface')._implements;

var Metadata = require('../../metadata/metadata');

var MRecordContent = require('./record-content');
var MStateless = require('./stateless');
var MDecimal = require('./decimal');
var MInteger = require('./integer');
var MString = require('./string');

   module.exports = {
    RecordContent : MRecordContent,
    Stateless : MStateless,
    Decimal : MDecimal,
    Integer : MInteger,
    String : MString
};


},{"../../metadata/metadata":54,"./decimal":127,"./integer":128,"./record-content":130,"./stateless":131,"./string":132,"util/interface":279}],130:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var Metadata = require('../../metadata/metadata');

var RecordContentParser = require('content/structured-record-content/parser');
var RecordContentBuilder = require('content/structured-record-content/builder');

var MDecimal = require('./decimal');
var MInteger = require('./integer');
var MString = require('./string');

var util = require('./util-single-value');

var count = 0;

function occurs(m, n) {
    var min = 1, max = 1;

    if (m !== undefined) {
        if (typeof(m) === 'string') {
            var i = m.indexOf('.');

            if (i !== -1) {
                var j = m.lastIndexOf('.');

                min = m.substring(0, i);
                max = m.substring(j + 1);

                if (max === '*') {
                    max = -1;
                }
            } else {
                min = m;
                max = m;
            }
        } else if (typeof(m) === 'number') {
            min = m;

            if (n !== undefined && typeof (n) === 'number') {
                max = n;
            } else {
                max = m;
            }
        } else if (m.min !== undefined) {
            min = m.min;
            max = m.max;
        }

        if (max === undefined) {
            max = min;
        }

        min = parseInt(min, 10);
        max = parseInt(max, 10);
    }

    return {
        min : min,
        max : max,
        toString : function() {
            var min = this.min;
            var max = this.max > -1 ? this.max : '*';

            return min === max ? '' + min : min + '..' + max;
        }
    };
}

var MFieldImpl = _implements(Metadata.RecordContent.Field, function MField(name, type, occurs) {
    this.name = name;
    this.occurs = occurs;

    if (type) {
        var t  = util.deriveMetadata(type);

        if (util.getType(t) !== util.Type.SINGLE_VALUE) {
            throw new Error('Record fields can only have single values (string, integer or decimal)');
        }

        this.type = t;
    } else {
        this.type = new MString();
    }

    this.getDetails = function() {
        var details = this.type.getDetails();

        details.name = name;
        details.multiplicity = occurs.toString();

        return details;
    };
});

var MRecordImpl = _implements(Metadata.RecordContent.Record, function MRecord(name, m) {
    var fields = {};
    var order = [];

    this.name = name;
    this.occurs = m;

    this.getField = function(name) {
        switch (typeof name) {
        case 'string' :
            return fields[name];
        case 'number' :
            return fields[order[name]];
        }

        return undefined;
    };

    this.getFields = function() {
        return order.map(function(name) {
            return fields[name];
        });
    };

    this.addField = function(name, type, m) {
        if (fields[name]) {
            throw new Error('Field metadata already exists for: ' + name);
        }

        var previous = fields[order[order.length - 1]];
        if (previous && previous.occurs.min !== previous.occurs.max) {
            throw new Error('Fields cannot be added after a repeating field');
        }

        var field = new MFieldImpl(name, type, occurs(m));

        fields[name] = field;
        order.push(name);

        return field;
    };

    this.getDetails = function() {
        return {
            name : name,
            multiplicity : m.toString()
        };
    };
});

var MRecordContentImpl = _implements(Metadata.RecordContent, function MRecordContent(name) {
    var parser = new RecordContentParser(this);
    var self = this;

    var records = {};
    var order = [];

    name = name || 'MRecordData' + (count++);

    this.addRecord = function(name, m, fields) {
        if (records[name]) {
            throw new Error('Record metadata already exists for: ' + name);
        }

        var previous = records[order[order.length - 1]];
        if (previous && previous.occurs.min !== previous.occurs.max) {
            throw new Error('Records cannot be added after a repeating record');
        }

        var record = new MRecordImpl(name, occurs(m));
        records[name] = record;
        order.push(name);

        if (fields !== undefined) {
            for (var field in fields) {
                record.addField(field, fields[field]);
            }
        }

        return record;
    };

    this.getRecord = function(name) {
        switch (typeof name) {
        case 'string' :
            return records[name];
        case 'number' :
            return records[order[name]];
        }

        return undefined;
    };

    this.getRecords = function() {
        return order.map(function(name) {
            return records[name];
        });
    };

    this.occurs = occurs;
    this.name = name;

    this.string = function(val) {
        return new MString(val);
    };

    this.integer = function(val) {
        return new MInteger(val);
    };

    this.decimal = function(val, scale) {
        return new MDecimal(val, scale);
    };

    this.getDetails = function() {
        return { name : name };
    };

    this.builder = function() {
        return new RecordContentBuilder(self);
    };

    this.parse = function(buffer) {
        return parser.parse(buffer);
    };
});

module.exports = MRecordContentImpl;

},{"../../metadata/metadata":54,"./decimal":127,"./integer":128,"./string":132,"./util-single-value":133,"content/structured-record-content/builder":76,"content/structured-record-content/parser":77,"util/interface":279}],131:[function(require,module,exports){
var details = {
    type : 'stateless',
    name : 'stateless'
};

module.exports = function MStateless() {
    this.getDetails = function() {
        return details;
    };

        this.parse = function(buffer) {
        return buffer;
    };
};
},{}],132:[function(require,module,exports){
module.exports = function MString(val) {
    var self = this;

        this.value = val === undefined ? '' : '' + val;

        this.getDetails = function() {
        return {
            type : 'string',
            name : 'string',
            default : self.value
        };
    };

        this.parse = function(buffer) {
        return buffer.toString();
    };
};
},{}],133:[function(require,module,exports){

var MStateless = require('./stateless');
var MDecimal = require('./decimal');
var MInteger = require('./integer');
var MString = require('./string');
var Type = require('../../topics/topics').TopicType;

function createFromSingleValue(value) {
    switch (typeof value) {
    case 'string' :
        return new MString(value);
    case 'number' :
        var str = '' + value, i = str.indexOf('.');

        if (i > -1) {
            return new MDecimal(value, str.length - (i + 1));
        } else {
            return new MInteger(value);
        }
    }

    throw new Error('Invalid Single Value type', value);
}

function getType(metadata) {
    if (metadata instanceof MDecimal ||
        metadata instanceof MInteger ||
        metadata instanceof MString) {
        return Type.SINGLE_VALUE;
    } else if (metadata instanceof MStateless) {
        return Type.STATELESS;
    }

    throw new Error('Unknown metadata type');
}

function isMetadata(value) {
    return value instanceof MStateless ||
           value instanceof MDecimal ||
           value instanceof MInteger ||
           value instanceof MString;
}

function isMetadataValue(value) {
    return isMetadata(value) ||
        typeof value === "string" ||
        typeof value === "number" ||
        typeof value === "boolean" ||
        value === undefined;
}

function deriveMetadata(value) {
    if (value === undefined) {
        return new MStateless();
    }

    if (isMetadata(value)) {
        return value;
    }

    return createFromSingleValue(value);
}

module.exports = {
    Type : Type,
    getType : getType,
    isMetadata : isMetadata,
    isMetadataValue : isMetadataValue,
    deriveMetadata : deriveMetadata
};
},{"../../topics/topics":298,"./decimal":127,"./integer":128,"./stateless":131,"./string":132}],134:[function(require,module,exports){
var Type = require('../../topics/topics').TopicType;
var util = require('./util-single-value');
var cutil = require('content/util');

var MRecordContent = require('./record-content');

function getType(metadata) {
    if (MRecordContent.isPrototypeOf(metadata)) {
        return Type.RECORD;
    } else {
        return util.getType(metadata);
    }
}

function isMetadata(value) {
    return MRecordContent.isPrototypeOf(value) || util.isMetadata(value);
}

function isMetadataValue(value) {
    return MRecordContent.isPrototypeOf(value) || util.isMetadataValue(value) || cutil.isRecordContent(value);
}

function deriveMetadata(value) {
    if (isMetadata(value)) {
        return value;
    }

    return util.deriveMetadata(value);
}

module.exports = {
    Type : Type,
    getType : getType,
    isMetadata : isMetadata,
    isMetadataValue : isMetadataValue,
    deriveMetadata : deriveMetadata
};

},{"../../topics/topics":298,"./record-content":130,"./util-single-value":133,"content/util":79}],135:[function(require,module,exports){
var consts = require('protocol/consts');

function createConnectionRequest() {
    return {
        type : consts.TYPE,
        version : consts.CURRENT_VERSION,
        capabilities : consts.CAPABILITIES
    };
}

function createReconnectionRequest(token, availableClientSequence, lastServerSequence) {
    var req = createConnectionRequest();

    req.token = token;
    req.availableClientSequence = availableClientSequence;
    req.lastServerSequence = lastServerSequence;

    return req;
}

module.exports = {
    connect : createConnectionRequest,
    reconnect : createReconnectionRequest
};

},{"protocol/consts":137}],136:[function(require,module,exports){
var BufferInputStream = require('io/buffer-input-stream');
var BEES = require('serialisers/byte-encoded-enum-serialiser');

var SessionId = require('session/session-id');
var SessionTokenDeserialiser = require('session/session-token-deserialiser');

var ResponseCode = require('protocol/response-code');
var consts = require('protocol/consts');

function deserialise(buffer) {
    var input = new BufferInputStream(buffer);

    var protocol = input.read();
    if (protocol !== consts.PROTOCOL_BYTE) {
        throw new Error('Unrecognised protocol byte: ' + protocol);
    }

    var version = input.read();
    if (version < consts.CURRENT_VERSION) {
        throw new Error('Unrecognised protocol version: ' + version);
    } else if (version > consts.CURRENT_VERSION) {
        throw new Error('Unsupported protocol version: ' + version);
    }

    var responseCode = BEES.read(input, ResponseCode);

        if (ResponseCode.isSuccess(responseCode)) {
        var sessionID = new SessionId(input.readUInt64(), input.readUInt64());
        var sessionToken = SessionTokenDeserialiser(input);
        var systemPingPeriod = input.readInt64();

        var recoverySequence = 0;

        if (responseCode === ResponseCode.RECONNECTED) {
            recoverySequence = input.readInt32();
        }

        return {
            response : responseCode,
            identity : sessionID,
            token : sessionToken,
            systemPingPeriod : systemPingPeriod,
            version : version,
            success : true,
            sequence : recoverySequence
        };
    } else {
        return {
            response : responseCode,
            version : version,
            identity : null,
            token : null,
            systemPingPeriod : null,
            success : false,
            sequence : 0
        };
    }
}

module.exports = deserialise;

},{"io/buffer-input-stream":123,"protocol/consts":137,"protocol/response-code":138,"serialisers/byte-encoded-enum-serialiser":151,"session/session-id":254,"session/session-token-deserialiser":256}],137:[function(require,module,exports){
module.exports = {
    TYPE : "WB",
    CAPABILITIES : 8,
    PROTOCOL_BYTE : 35,
    CURRENT_VERSION : 9
};

},{}],138:[function(require,module,exports){
function code(id, message) {
    return {
        id : id, message : message
    };
}

var ResponseCode = {
    OK :                                code(100, "Connected successfully"),
    DOWNGRADE :                         code(102, "Server does not support the requested protocol level"),
    RECONNECTED :                       code(105, "Reconnected successfully"),
    RECONNECTED_WITH_MESSAGE_LOSS :     code(106, "Reconnected with message loss"),
    REJECTED :                          code(111, "Connection rejected"),
    CONNECTION_UNSUPPORTED :            code(112, "Connection type not supported by connector"),
    LICENSE_EXCEEDED :                  code(113, "Connection rejected due to license limit"),
    RECONNECTION_UNSUPPORTED :          code(114, "Reconnection not supported by connector"),
    CONNECTION_PROTOCOL_ERROR :         code(115, "Connection failed - protocol error"),
    AUTHENTICATION_FAILED :             code(116, "Connection failed - authentication failed"),
    UNKNOWN_SESSION :                   code(117, "Reconnection failed - the session is unknown"),
    RECONNECTION_FAILED_MESSAGE_LOSS :  code(118, "Reconnection failed due to message loss"),
    ERROR :                             code(127, "Connection failed due to server error")
};

ResponseCode.isSuccess = function isSuccess(code) {
    switch (code) {
        case ResponseCode.OK :
        case ResponseCode.RECONNECTED :
        case ResponseCode.RECONNECTED_WITH_MESSAGE_LOSS :
            return true;
        default :
            return false;
    }
};

module.exports = ResponseCode;

},{}],139:[function(require,module,exports){
var HashMap = require('hashmap');
var Arrays = require('util/array');

module.exports = function StreamRegistry(topicCache) {
    var streams = new HashMap();
    var fallbacks = [];
    var self = this;

    this.add = function(selector, stream) {
        var existing = streams.get(selector);

        if (existing) {
            existing.push(stream);
        } else {
            streams.set(selector, [stream]);
        }

        topicCache.newStream(selector, stream, self);

        stream.onOpen();
    };

    this.addFallback = function(stream) {
        fallbacks.push(stream);
    };

    this.getFallbacks = function(details) {
        return fallbacks.filter(function(fallback) {
            return fallback.selects(details);
        });
    };

    this.remove = function(stream) {
        Arrays.remove(fallbacks, stream);

        streams.forEach(function(existing, selector) {
            if (Arrays.remove(existing, stream)) {
                topicCache.removeStream(stream, self);
            }

            if (existing.length === 0) {
                streams.remove(selector);
            }
        });
    };

    this.streamsFor = function(topic, details) {
        var combined = [];

        if (streams.count() > 0) {
            streams.forEach(function(existing, selector) {
                if (selector.selects(topic)) {
                    combined = combined.concat(existing.filter(function(stream) {
                        return stream.selects(details);
                    }));
                }
            });
        }

        return combined;
    };
};
},{"hashmap":8,"util/array":276}],140:[function(require,module,exports){
var TopicCacheEntry = require('routing/topic-cache-entry');

module.exports = function DatatypeCacheEntry(details, streams, datatype) {
    TopicCacheEntry.call(this, details, streams);

    var deltaType = datatype.deltaType("binary");
    var value;

    this.handleValue = function(received, registry, errorHandler) {
        var oldValue = value;

        try {
            value = datatype.readValue(received);
            this.notifyValue(received, oldValue, value, registry);
        } catch (e) {
            errorHandler(e);
        }
    };

    this.handleDelta = function(received, registry, errorHandler) {
        var oldValue = value;

        try {
            var delta = deltaType.readDelta(received);
            value = deltaType.apply(oldValue, delta);
            this.notifyDelta(received, delta, oldValue, value, registry);
        } catch (e) {
            errorHandler(e);
        }
    };

    this.notifyValueToNewStream = function(path, details, stream) {
        if (value) {
            stream.onValue(path, details, value.$buffer, null, value);
        }
    };
};
},{"routing/topic-cache-entry":141}],141:[function(require,module,exports){
var UnsubscribeReason = require('../../topics/topics').UnsubscribeReason;
var Arrays = require('util/array');

module.exports = function TopicCacheEntryImpl(info, streams) {
    function proxies(details, registry) {
        var p = streams;

        if (p.length === 0) {
            p = registry.getFallbacks(details);
        }

        return p;
    }

    var self = this;

    this.getTopicPath = function() {
        return info.path;
    };

    this.getTopicDetails = function() {
        return info.details;
    };

    this.notifyInitialSubscription = function(registry) {
        proxies(info.details, registry).forEach(function(proxy) {
            proxy.onSubscription(info.path, info.details);
        });
    };

    this.notifySubscription = function(proxy) {
        proxy.onSubscription(info.path, info.details);

        self.notifyValueToNewStream(info.path, info.details, proxy);
    };

    this.notifyUnsubscription = function(reason, registry) {
        proxies(info.details, registry).forEach(function(proxy) {
            proxy.onUnsubscription(info.path, info.details, reason);
        });
    };

    this.notifyValue = function(content, oldValue, newValue, registry) {
        proxies(info.details, registry).forEach(function(proxy) {
            proxy.onValue(info.path, info.details, content, oldValue, newValue);
        });
    };

    this.notifyDelta = function(content, delta, oldValue, newValue, registry) {
        proxies(info.details, registry).forEach(function(proxy) {
            proxy.onDelta(info.path, info.details, content, delta, oldValue, newValue);
        });
    };

    this.addStream = function(stream, registry) {
        if (streams.length === 0) {
            registry.getFallbacks(info.details).forEach(function(fallback) {
                fallback.onUnsubscription(info.path, info.details, UnsubscribeReason.STREAM_CHANGE);
            });
        }

        if (streams.indexOf(stream) < 0) {
            streams.push(stream);

            self.notifySubscription(stream);
        }
    };

    this.removeStream = function(stream, registry) {
        Arrays.remove(streams, stream);

        if (streams.length === 0) {
            registry.getFallbacks(info.details).forEach(function(fallback) {
                self.notifySubscription(fallback);
            });
        }
    };

    this.removeAllStreams = function() {
        streams.length = 0;
    };
};
},{"../../topics/topics":298,"util/array":276}],142:[function(require,module,exports){
var TopicCacheEntry = require('routing/topic-cache-entry');

module.exports = function NoValueEntry(details, streams) {
    TopicCacheEntry.call(this, details, streams);

    this.handleValue = function(received, registry, errorHandler) {
        this.notifyValue(received, null, received, registry);
    };

    this.handleDelta = function(received, registry, errorHandler) {
        this.notifyDelta(received, null, null, received, registry);
    };

    this.notifyValueToNewStream = function(path, stream) {
    };
};
},{"routing/topic-cache-entry":141}],143:[function(require,module,exports){
var TopicCacheEntry = require('routing/topic-cache-entry');

module.exports = function SingleValueEntry(details, streams) {
    TopicCacheEntry.call(this, details, streams);

    var value;

    this.handleValue = function(received, registry, errorHandler) {
        this.notifyValue(received, value, received, registry);
        value = received;
    };

    this.handleDelta = function(received, registry, errorHandler) {
        this.notifyDelta(received, null, value, received, registry);
        value = received;
    };

    this.notifyValueToNewStream = function(path, details, stream) {
        if (value) {
            stream.onValue(path, details, value, null, value);
        }
    };
};
},{"routing/topic-cache-entry":141}],144:[function(require,module,exports){
var NoValueEntry = require('routing/topic-cache-no-value-entry');
var DatatypeEntry = require('routing/topic-cache-datatype-entry');
var SingleEntry = require('routing/topic-cache-single-value-entry');

var UnsubscribeReason = require('../../topics/topics').UnsubscribeReason;
var TopicType = require('../../topics/topics').TopicType;

module.exports = function TopicCache(datatypes) {
    var byPath = {};
    var byId = {};

    this.handleSubscription = function(info, registry) {
        var path = info.path;

        var existing = byPath[path];

        if (existing) {
            existing.notifyUnsubscription(UnsubscribeReason.SUBSCRIPTION_REFRESH, registry);
        }

        var streams = registry.streamsFor(path, info.details);
        var entry;

        switch (info.details.type) {
            case TopicType.JSON :
                entry = new DatatypeEntry(info, streams, datatypes.json());
                break;
            case TopicType.BINARY :
                entry = new DatatypeEntry(info, streams, datatypes.binary());
                break;
            case TopicType.SINGLE_VALUE :
                entry = new SingleEntry(info, streams);
                break;
            default :
                entry = new NoValueEntry(info, streams);
        }

        byId[info.id] = entry;
        byPath[path] = entry;

        entry.notifyInitialSubscription(registry);
    };

    this.handleValue = function(session, path, content, registry) {
        var errorHandler = session.getErrorHandler();
        var entry = byPath[path];

        if (entry) {
            entry.handleValue(content, registry, errorHandler);
        } else {
            errorHandler(new Error("Data loss on topic " + path + " - possibly due to reconnection"));
        }
    };

    this.handleDelta = function(session, path, delta, registry) {
        var errorHandler = session.getErrorHandler();
        var entry = byPath[path];

        if (entry) {
            entry.handleDelta(delta, registry, errorHandler);
        } else {
            errorHandler(new Error("Data loss on topic " + path + " - possibly due to reconnection"));
        }
    };

    this.handleUnsubscription = function(id, reason, registry) {
        var entry = byId[id];
        delete byId[id];

        if (entry) {
            var path = entry.getTopicPath();
            delete byPath[path];

            entry.notifyUnsubscription(reason, registry);
        }
    };

    this.newStream = function(selector, stream, registry) {
        for (var path in byPath) {
            var entry = byPath[path];

            if (selector.selects(path) && stream.selects(entry.getTopicDetails())) {
                entry.addStream(stream, registry);
            }
        }
    };

    this.removeStream = function(stream, registry) {
        for (var path in byPath) {
            byPath[path].removeStream(stream, registry);
        }
    };

    this.removeAllStreams = function() {
        for (var path in byPath) {
            byPath[path].removeAllStreams();
        }
    };

    this.clear = function() {
        byPath = {};
        byId = {};
    };
};


},{"../../topics/topics":298,"routing/topic-cache-datatype-entry":140,"routing/topic-cache-no-value-entry":142,"routing/topic-cache-single-value-entry":143}],145:[function(require,module,exports){
var HeadersTunnel = require('v4-stack/headers-tunnel');
var Message = require('v4-stack/message');

var log = require('util/logger').create('V4 Client Routing');

module.exports = function TopicRouting(session, serviceAdapter, conversationSet, cache, registry) {
    this.route = function(message) {
        if (message.isTopicMessage()) {

            if (message.type === Message.types.FETCH_REPLY) {
                var cid = HeadersTunnel.cidFromHeaders(message.headers);
                conversationSet.respond(cid, message.topic, message.data);
            } else if (message.type === Message.types.TOPIC_LOAD) {
                cache.handleValue(session, message.topic, message.data, registry);
            } else if (message.type === Message.types.DELTA) {
                cache.handleDelta(session, message.topic, message.data, registry);
            } 
        }
        else if (message.isServiceMessage()) {
            serviceAdapter.onMessage(message.type, message.getInputStream());
        }
        else {
            log.debug('Unhandled V4 message: ' + message.type, message);
        }
    };

    this.subscribe = function(info) {
        cache.handleSubscription(info, registry);
    };

    this.unsubscribe = function(id, reason) {
        cache.handleUnsubscription(id, reason, registry);
    };
};
},{"util/logger":280,"v4-stack/headers-tunnel":292,"v4-stack/message":293}],146:[function(require,module,exports){
module.exports = {
    ATTR : '$',
    CHAR : '_'
};
},{}],147:[function(require,module,exports){
var util = require('metadata/util');
var ATTR = require('schema/consts').ATTR;

function createRecordSchema(metadata) {
    if (metadata.getRecords().length === 0) {
        return null;
    }

    var schema = { message : { record : [] } };
    schema.message[ATTR] = {
        topicDataType : 'record',
        name : metadata.name
    };

    metadata.getRecords().forEach(function(record) {
        var rschema = { field : [] };
        rschema[ATTR] = record.getDetails();

        record.getFields().forEach(function(field) {
            rschema.field.push(createSingleValueSchema(field).field);
        });

        schema.message.record.push(rschema);
    });

    return schema;
}

function createSingleValueSchema(metadata) {
    var schema = { field : { } };
    schema.field[ATTR] = metadata.getDetails();

    return schema;
}

function create(meta) {
    switch (util.getType(meta)) {
    case util.Type.RECORD :
        return createRecordSchema(meta);
    case util.Type.SINGLE_VALUE :
        return createSingleValueSchema(meta);
    }

    return null;
}

module.exports = create;
},{"metadata/util":134,"schema/consts":146}],148:[function(require,module,exports){
var xmljson = require('./xmljson');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        return xmljson.xml2json(Codec.readString(input));
    },
    write : function(output, schema) {
        if (schema && (schema.field || schema.message)) {
            Codec.writeString(output, xmljson.json2xml(schema));
        }
    }
};

module.exports = serialiser;

},{"./xmljson":149,"io/codec":125}],149:[function(require,module,exports){
var metadata = require('../metadata/metadata');
var schema = require('../schema/schema');
var ATTR = require('../schema/consts').ATTR;

function attrs_str2js(str) {
    var js = {};

        var pairs = str.split(' ');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        js[pair[0]] = pair[1].match(/"(.*)"/)[1];
    }

    return js;
}

function escapeEntities(str) {
    return String(str).replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#x27;');
}

    function attrs_obj2arr(obj) {
    var attrs = [];
    if (typeof obj === 'object') {
        for (var key in obj) {
            attrs.push('' + key + '="' + escapeEntities(obj[key]) + '"');
        }
    }
    return attrs;
}

function xml2json(xml, pos) {
    var js = {};

    if (pos === undefined) {
        pos = {
            idx : 0,
            isRoot : true
        };

        var preamble = xml.match(/<\?.*?\?>/);
        if (preamble) {
            xml = xml.substr(preamble[0].length);
        }
    }

        var elem_match = null;

    while (true) {
        elem_match = xml.substr(pos.idx).match(/<.*?>/);
        if (!elem_match) {
            break;
        }

        pos.idx += elem_match[0].length;

                var elem = elem_match[0].trim();

        if (elem.substr(0,2) === '</') {
            return js;
        }

        var name = elem.match(/\b(.+?)\b/)[0];
        var attrs_match = elem.match(/<.+?\b(.*)\/?>/);
        var attrs_str = '';

                if (attrs_match) {
            attrs_str = attrs_match[1].trim();
        }

                var attrs = attrs_str2js(attrs_str);
        var child_count;
        if (elem.substr(-2) === '/>') {

            if (pos.isRoot) {
                pos.isRoot = false;
                js[name] = xml2json(xml, pos);
                js[name][ATTR] = attrs;
            } else {
                if (!js[name]) {
                    js[name] = [];
                }

                child_count = js[name].push({});
                js[name][child_count - 1][ATTR] = attrs;
            }
        } else {

            if (pos.isRoot) {
                pos.isRoot = false;
                js[name] = xml2json(xml, pos);
                js[name][ATTR] = attrs;
            } else {
                if (!js[name]) {
                    js[name] = [];
                }

                child_count = js[name].push(xml2json(xml, pos));
                js[name][child_count - 1][ATTR] = attrs;
            } 
        }
    }

    return js;
}

function json2xml(obj, recur) {
    var str = '';

    if (!recur) {
        str = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    }

        for (var key in obj) {
        if (key === ATTR) {
            continue;
        }

                                  var val = obj[key];

        if (!(val instanceof Array)) {
            val = Array(val);
        }

        for (var i = 0; i < val.length; i++) {
            var attrs = attrs_obj2arr(val[i][ATTR]);

            str += '<' + key;
            if (attrs && attrs.length > 0) {
                str += ' ';
                str += attrs.join(' ');
            }

            var child_str = json2xml(val[i], true);
            if (child_str && child_str.length > 0) {
                str += '>';
                str += child_str;
                str += '</' + key + '>';
            } else {
                str += '/>';
            }
        }
    }

        return str;
}

module.exports = {
    xml2json : xml2json,
    json2xml : json2xml
};

},{"../metadata/metadata":129,"../schema/consts":146,"../schema/schema":147}],150:[function(require,module,exports){
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        return Codec.readByte(input) === 1;
    },
    write : function(output, val) {
        Codec.writeByte(output, val ? 1 : 0);
    }
};

module.exports = serialiser;

},{"io/codec":125}],151:[function(require,module,exports){
var serialiser = {
    read : function(bis, enums) {
        var i = bis.read(), k;

        for (k in enums) {
            var e = enums[k];

                        if (e === i || e.id !== undefined && e.id === i) {
                return e;  
            }
        }

                throw new Error("Unable to decode enum value " + i);
    },
    write : function(bos, val) {
        if (val.id) {
            bos.write(val.id);
        } else {
            bos.write(val);
        }
    }
};

module.exports = serialiser;
},{}],152:[function(require,module,exports){
var Codec = require('io/codec');

var serialiser = {
    read : function(input, Enum) {
        var i = Codec.readByte(input);

                for (var k in Enum) {
            if (Enum[k] === i || Enum[k].id !== undefined && Enum[k].id === i) {
                return k;
            }
        }

        throw new Error('Unknown id (' + i + ') for enum');           
    },
    write : function(output, id) {
        Codec.writeByte(output, id);
    }
};

module.exports = serialiser;

},{"io/codec":125}],153:[function(require,module,exports){
var Codec = require('io/codec');
var BufferOutputStream = require('io/buffer-output-stream');

var util = require('content/util');
var ContentSerialiser = require('content/serialiser');

var BEES = require('serialisers/byte-encoded-enum-serialiser');
var TopicAddFailReason = require('../../../topics/topics').TopicAddFailReason;
var TopicDetailsSerialiser = require('topics/details/topic-details-serialiser');

var Status = {
    OK : 0,
    OK_CACHED : 1,
    FAILURE : 2,
    CACHE_FAILURE : 3
};

var serialiser = {
    read : function(input) {
        var status = Codec.readByte(input);
        var reason = "";

        if (status === Status.FAILURE) {
            reason = BEES.read(input, TopicAddFailReason);
        }

        return {
            status : status,
            reason : reason
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.path);

        Codec.writeInt32(output, 0);

        TopicDetailsSerialiser.write(output, req.details);

        if (req.content) {
            Codec.writeBoolean(output, true);
            ContentSerialiser.write(output, req.content);
        } else {
            Codec.writeBoolean(output, false);
        }
    }
};

module.exports = serialiser;

},{"../../../topics/topics":298,"content/serialiser":75,"content/util":79,"io/buffer-output-stream":124,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151,"topics/details/topic-details-serialiser":258}],154:[function(require,module,exports){
function AddTopic() {
}

module.exports = AddTopic;

},{}],155:[function(require,module,exports){
var BEES = require('serialisers/byte-encoded-enum-serialiser');
var Codec = require('io/codec');

var Type = {
    NONE : 0,
    PLAIN : 1,
    CUSTOM : 2
};

var serialiser = {
    read : function(input) {
        var type = BEES.read(input, Type);
        switch (type) {
            case Type.NONE:
                return null;
            case Type.PLAIN:
                return Codec.readString(input);
            case Type.CUSTOM:
                return Codec.readBytes(input);
        }
    },
    write : function(input, credentials) {
        if (credentials) {
            if (typeof credentials === 'string') {
                BEES.write(input, Type.PLAIN);
                Codec.writeString(input, credentials);
            } else {
                BEES.write(input, Type.CUSTOM);
                Codec.writeBytes(input, credentials);
            }
        } else {
            BEES.write(input, Type.NONE);
            Codec.writeInt32(input, 0);
        }
    }
};

module.exports = serialiser;

},{"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151}],156:[function(require,module,exports){
var Codec = require('io/codec');
var ErrorReportSerialiser = require('../error-report-serialiser');

var SecurityCommandScriptResult = require('./security-command-script-result');

var serialiser = {
    read : function(input) {
        var errors = Codec.readCollection(input, ErrorReportSerialiser.read);
        return new SecurityCommandScriptResult(errors);
    },
    write : function(output, value) {
        Codec.writeCollection(output, value.errors, ErrorReportSerialiser.write);
    }
};

module.exports = serialiser;

},{"../error-report-serialiser":196,"./security-command-script-result":157,"io/codec":125}],157:[function(require,module,exports){
var requireNonNull = require('util/require-non-null');

function SecurityCommandScriptResult(errors) {
    this.errors = requireNonNull(errors);
}

SecurityCommandScriptResult.prototype.toString = function() {
    return "SecurityCommandScriptResult [" + this.errors + "]";
};

module.exports = SecurityCommandScriptResult;
},{"util/require-non-null":285}],158:[function(require,module,exports){
var SecurityCommandScript = require('./security-command-script');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var script = Codec.readString(input);
        return new SecurityCommandScript(script);
    }, 
    write : function(output, value) {
        Codec.writeString(output, value.script);
    }
};

module.exports = serialiser;
},{"./security-command-script":159,"io/codec":125}],159:[function(require,module,exports){
var requireNonNull = require('util/require-non-null');

function SecurityCommandScript(script) {
    this.script = requireNonNull(script, "script");
}

SecurityCommandScript.prototype.toString = function() {
    return "SecurityCommandScript [" + this.script + "]";
};

module.exports = SecurityCommandScript;
},{"util/require-non-null":285}],160:[function(require,module,exports){
var Codec = require('io/codec');
var Configuration = require('features/security').SystemAuthentication;

var ByteEncodedEnumSerialiser = require('serialisers/byte-encoded-enum-serialiser');
var SystemPrincipalSerialiser = require('./system-principal-serialiser');

    var serialiser = {
    read : function(input) {
        var principals = Codec.readCollection(input, SystemPrincipalSerialiser.read);
        var action = ByteEncodedEnumSerialiser.read(input, Configuration.CONNECTION_ACTION);
        var roles = Codec.readCollection(input, Codec.readString);

                return new Configuration(principals, action.value, roles);
    },
    write : function(output, value) {
        Codec.writeCollection(output, value.principals, SystemPrincipalSerialiser.write);

                var action = Configuration.CONNECTION_ACTION.fromString(value.anonymous.action);
        Codec.writeByte(output, action.id);

                Codec.writeCollection(output, value.anonymous.roles, Codec.writeString);
    }
};

module.exports = serialiser;

},{"./system-principal-serialiser":161,"features/security":108,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151}],161:[function(require,module,exports){
var Codec = require('io/codec');
var SystemPrincipal = require('features/security').SystemPrincipal;

var serialiser = {
    read : function(input) {
        var name = Codec.readString(input);
        var roles = Codec.readCollection(input, Codec.readString);

        return new SystemPrincipal(name, roles);
    },
    write : function(output, principal) {
        Codec.writeString(output, principal.name);
        Codec.writeCollection(output, principal.roles, Codec.writeString);
    }
};

module.exports = serialiser;
},{"features/security":108,"io/codec":125}],162:[function(require,module,exports){
var CredentialsSerialiser = require('services/authentication/credentials-serialiser');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var principal = Codec.readString(input);
        var credentials = CredentialsSerialiser.read(input);
    },
    write : function(output, req) {
        Codec.writeString(output, req.principal);
        CredentialsSerialiser.write(output, req.credentials);
    }
};

module.exports = serialiser;

},{"io/codec":125,"services/authentication/credentials-serialiser":155}],163:[function(require,module,exports){
function ChangePrincipalRequest(principal, credentials) {
    this.principal = principal;
    this.credentials = credentials;
}

},{}],164:[function(require,module,exports){
var Codec = require('io/codec');
var ByteEncodedEnumSerialiser = require('serialisers/byte-encoded-enum-serialiser');

var CommandError = require('./command-error');

var serialiser = {
    read : function(input) {
        var message = Codec.readString(input);
        var type = ByteEncodedEnumSerialiser.read(input, CommandError.ErrorType);

                return new CommandError(type, message);         
    },
    write : function(out, error) {
        Codec.writeString(out, error.message);
        ByteEncodedEnumSerialiser.write(out, error.type);
    }
};

module.exports = serialiser;
},{"./command-error":165,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151}],165:[function(require,module,exports){
var ErrorReasons = require('client/errors');

function ErrorType(reason) {
    this.reason = reason;
    this.id = reason.id;

        this.asCommandError = function() {
        return new CommandError(this); 
    };
}

function CommandError(type, message) {
    this.type = type;
    this.message = message || type.reason.reason;
}

CommandError.prototype.toString = function() {
    return this.message; 
};

CommandError.ErrorType = {
    COMMUNICATION_FAILURE               : new ErrorType(ErrorReasons.COMMUNICATION_FAILURE),
    ACCESS_DENIED                       : new ErrorType(ErrorReasons.ACCESS_DENIED),
    TOPIC_TREE_REGISTATION_CONFLICT     : new ErrorType(ErrorReasons.TOPIC_TREE_REGISTRATION_CONFLICT)
};

module.exports = CommandError;
},{"client/errors":63}],166:[function(require,module,exports){
var ConversationIDSerialiser = require('conversation/conversation-id-serialiser');
var CommandHeader = require('./command-header');
var Codec = require('io/codec');

var CommandHeaderSerialiser = {
    read : function(input) {
        var service = Codec.readInt32(input),
            cid = ConversationIDSerialiser.read(input);

        return new CommandHeader(service, cid);
    },
    write : function(out, header) {

        Codec.writeInt32(out, header.service);
        ConversationIDSerialiser.write(out, header.cid);
    }
};

module.exports = CommandHeaderSerialiser;
},{"./command-header":167,"conversation/conversation-id-serialiser":83,"io/codec":125}],167:[function(require,module,exports){
var Message = require('v4-stack/message');

function CommandHeader(service, cid) {
    this.service = service;
    this.cid = cid;
}

CommandHeader.prototype.toString = function() {
    return '<' + this.service + ', ' + this.cid.toString() + '>';
};

CommandHeader.createRequestHeader = function(service, cid) {
    return new CommandHeader(service, cid);
};

CommandHeader.prototype.createResponseHeader = function() {
    return new CommandHeader(this.service, this.cid);
};

CommandHeader.prototype.createErrorHeader = function() {
    return new CommandHeader(this.service, this.cid);
};


module.exports = CommandHeader;

},{"v4-stack/message":293}],168:[function(require,module,exports){
var ClientFilterSubscribeRequest = require('services/control-client/client-filter-subscribe-request');
var TopicSelectorSerialiser = require('topics/topic-selector-serialiser');
var Codec = require('io/codec');

module.exports = {
    read : function(input) {
        var filter = Codec.readString(input);
        var selector = TopicSelectorSerialiser.read(input);

        return new ClientFilterSubscribeRequest(filter, selector);
    },
    write : function(output, request) {
        Codec.writeString(output, request.filter);
        TopicSelectorSerialiser.write(output, request.selector);
    }
};

},{"io/codec":125,"services/control-client/client-filter-subscribe-request":169,"topics/topic-selector-serialiser":268}],169:[function(require,module,exports){
module.exports = function ClientFilterSubscribeRequest(filter, selector) {
    this.filter = filter;
    this.selector = selector;
};
},{}],170:[function(require,module,exports){
var ClientFilterSubscribeResponse = require('services/control-client/client-filter-subscribe-response');
var ErrorReportSerialiser = require('services/error-report-serialiser');
var Codec = require('io/codec');

module.exports = {
    read : function(input) {
        var error = Codec.readByte(input);

        if (error) {
            var errors = Codec.readCollection(input, ErrorReportSerialiser.read);
            return new ClientFilterSubscribeResponse(0, errors);
        } else {
            var numSelected = Codec.readInt32(input);
            return new ClientFilterSubscribeResponse(numSelected, []);
        }
    },
    write : function(output, response) {
        if (response.isSuccess()) {
            Codec.writeByte(output, 0);
            Codec.writeInt32(output, response.selected);
        } else {
            Codec.writeByte(output, 1);
            Codec.writeCollection(output, response.errors, ErrorReportSerialiser.write);
        }
    }
};
},{"io/codec":125,"services/control-client/client-filter-subscribe-response":171,"services/error-report-serialiser":196}],171:[function(require,module,exports){
module.exports = function ClientFilterSubscribeResponse(selected, errors) {
    this.selected = selected;
    this.errors = errors;

    this.isSuccess = function() {
        return errors.length === 0;
    };
};
},{}],172:[function(require,module,exports){
var ClientSubscribeRequest = require('services/control-client/client-subscribe-request');
var TopicSelectorSerialiser = require('topics/topic-selector-serialiser');
var SessionIdSerialiser = require('session/session-id-serialiser');

module.exports = {
    read : function(input) {
        var sessionID = SessionIdSerialiser.read(input);
        var selector = TopicSelectorSerialiser.read(input);

        return new ClientSubscribeRequest(sessionID, selector);
    },
    write : function(output, request) {
        SessionIdSerialiser.write(output, request.sessionID);
        TopicSelectorSerialiser.write(output, request.selector);
    }
};
},{"services/control-client/client-subscribe-request":173,"session/session-id-serialiser":253,"topics/topic-selector-serialiser":268}],173:[function(require,module,exports){
module.exports = function ClientSubscribeRequest(sessionID, selector) {
    this.sessionID = sessionID;
    this.selector = selector;
};
},{}],174:[function(require,module,exports){
var ControlGroupSerialiser = require('control/control-group-serialiser');
var BEES = require('serialisers/byte-encoded-enum-serialiser');

var Services = require('services/services');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var definition = BEES.read(input, Services);
        var group = ControlGroupSerialiser.read(input);

        return {
            definition : definition,
            group : group
        };
    },
    write : function(output, params) {
        Codec.writeInt32(output, params.definition.id);
        ControlGroupSerialiser.write(output, params.group);        
    }
};

module.exports = serialiser;

},{"control/control-group-serialiser":80,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151,"services/services":220}],175:[function(require,module,exports){
function ControlRegistrationParams(definition, group) {
    this.definition = definition;
    this.group = group;
}

module.exports = ControlRegistrationParams;

},{}],176:[function(require,module,exports){
var ControlRegistrationParamsSerialiser = require('services/control/control-registration-params-serialiser');
var ConversationIDSerialiser = require('conversation/conversation-id-serialiser');

var serialiser = {
    read : function(input) {
        var params = ControlRegistrationParamsSerialiser.read(input);
        var cid = ConversationIDSerialiser.read(input);

        return {
            params : params,
            cid : cid
        };
    },
    write : function(output, request) {
        ControlRegistrationParamsSerialiser.write(output, request.params);
        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;

},{"conversation/conversation-id-serialiser":83,"services/control/control-registration-params-serialiser":174}],177:[function(require,module,exports){
function ControlRegistrationRequest(params, cid) {
    this.params = params;
    this.cid = cid;
}

module.exports = ControlRegistrationRequest;

},{}],178:[function(require,module,exports){
var Codec = require('io/codec');
var SessionSerialiser = require('session/session-id-serialiser');

var serialiser = {
    read : function(input) {
        if (Codec.readBoolean(input)) {
            return {
                properties : Codec.readDictionary(input, Codec.readString)
            };
        } else {
            return null;
        }
    },
    write : function(output, request) {
        if (request.sessionID === undefined || request.sessionID === null) {
            throw new Error('session ID is null or undefined');
        }
        SessionSerialiser.write(output, request.sessionID);

        if (request.propertyKeys !== undefined && request.propertyKeys !== null) {
            Codec.writeCollection(output, request.propertyKeys, Codec.writeString);
        } else {
            Codec.writeCollection(output, [], Codec.writeString);
        }

    }
};

module.exports = serialiser;

},{"io/codec":125,"session/session-id-serialiser":253}],179:[function(require,module,exports){
function GetSessionProperties() {
}

module.exports = GetSessionProperties;

},{}],180:[function(require,module,exports){
var ControlGroupSerialiser = require('control/control-group-serialiser');
var BEES = require('serialisers/byte-encoded-enum-serialiser');

var Services = require('services/services');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var definition = BEES.read(input, Services);
        var group = ControlGroupSerialiser.read(input);
        var path = Codec.readString(input);
        var keys = Codec.readCollection(input, Codec.readString);

        return {
            definition : definition,
            group : group,
            path : path,
            keys : keys
        };
    },
    write : function(output, params) {
        Codec.writeInt32(output, params.definition.id);
        ControlGroupSerialiser.write(output, params.group);
        Codec.writeString(output, params.path);
        Codec.writeCollection(output, params.keys, Codec.writeString);
    }
};

module.exports = serialiser;

},{"control/control-group-serialiser":80,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151,"services/services":220}],181:[function(require,module,exports){
function MessageReceiverControlRegistrationParams(definition, group, path, keys) {
    this.definition = definition;
    this.group = group;
    this.path = path;
    this.keys = keys;
}

module.exports = MessageReceiverControlRegistrationParams;

},{}],182:[function(require,module,exports){
var MessageReceiverControlRegistrationParamsSerialiser =
        require('services/control/message-receiver-control-registration-params-serialiser');
var ConversationIDSerialiser = require('conversation/conversation-id-serialiser');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var params = MessageReceiverControlRegistrationParamsSerialiser.read(input);
        var cid = ConversationIDSerialiser.read(input);

        return {
            params : params,
            cid : cid
        };
    },
    write : function(output, request) {
        MessageReceiverControlRegistrationParamsSerialiser.write(output, request.params);
        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;

},{"conversation/conversation-id-serialiser":83,"io/codec":125,"services/control/message-receiver-control-registration-params-serialiser":180}],183:[function(require,module,exports){
function MessageReceiverControlRegistrationRequest() {
}

module.exports = MessageReceiverControlRegistrationRequest;

},{}],184:[function(require,module,exports){
var SessionCloseReason = {
    CONNECTION_LOST : 0,
    IO_EXCEPTION : 1,
    CLIENT_UNRESPONSIVE : 2,
    MESSAGE_QUEUE_LIMIT_REACHED : 3,
    CLOSED_BY_CLIENT : 4,
    MESSAGE_TOO_LARGE : 5,
    INTERNAL_ERROR : 6,
    INVALID_INBOUND_MESSAGE : 7,
    ABORT : 8,
    LOST_MESSAGES : 9,
    SERVER_CLOSING : 10,
    CLOSED_BY_CONTROLLER : 11
};

module.exports = SessionCloseReason;

},{}],185:[function(require,module,exports){
var SessionPropertiesEventSerialiser = require('services/control/session-properties-event-serialiser');
var ConversationIDSerialiser = require('conversation/conversation-id-serialiser');
var Codec = require('io/codec');

module.exports = {
    read : function(input) {
        var cid = ConversationIDSerialiser.read(input);
        var events = Codec.readCollection(input, SessionPropertiesEventSerialiser.read);

        return {
            cid : cid,
            events : events
        };
    },
    write : function(output, batch) {
        ConversationIDSerialiser.write(output, batch.cid);
        Codec.writeCollection(output, batch.events);
    }
};
},{"conversation/conversation-id-serialiser":83,"io/codec":125,"services/control/session-properties-event-serialiser":187}],186:[function(require,module,exports){
module.exports = function SessionPropertiesEventBatch(cid, events) {
    this.cid = cid;
    this.events = events;
};
},{}],187:[function(require,module,exports){
var ClientControlOptions = require('../../../features/client-control-options');
var ConversationIdSerialiser = require('conversation/conversation-id-serialiser');
var SessionIdSerialiser = require('session/session-id-serialiser');
var SessionPropertiesEventType = require('services/control/session-properties-event-type');
var SessionCloseReason = require('services/control/session-close-reason');

var Codec = require('io/codec');
var BEES = require('serialisers/byte-encoded-enum-serialiser');

var serialiser = {
    read : function(input) {
        var result = {
            sessionId : SessionIdSerialiser.read(input),
            type : BEES.read(input, SessionPropertiesEventType)
        };

        switch (result.type) {
        case SessionPropertiesEventType.OPEN:
            result.oldProperties = Codec.readDictionary(input, Codec.readString);
            break;
        case SessionPropertiesEventType.UPDATE:
            result.updateType = Codec.readByte(input);
            result.oldProperties = Codec.readDictionary(input, Codec.readString);
            result.newProperties = Codec.readDictionary(input, Codec.readString);
            break;
        case SessionPropertiesEventType.CLOSE:
            result.closeReason = BEES.read(input, SessionCloseReason);
            result.oldProperties = Codec.readDictionary(input, Codec.readString);
            break;
        default:
            throw new Error('Unknown session properties event type: ' + result.type);
        }

        return result;
    },
    write : function(output, request) {
    }
};

module.exports = serialiser;

},{"../../../features/client-control-options":48,"conversation/conversation-id-serialiser":83,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151,"services/control/session-close-reason":184,"services/control/session-properties-event-type":188,"session/session-id-serialiser":253}],188:[function(require,module,exports){
var SessionPropertiesEventType = {
    OPEN : 0,
    UPDATE : 1,
    CLOSE : 2
};

module.exports = SessionPropertiesEventType;

},{}],189:[function(require,module,exports){
function SessionPropertiesEvent() {
}

module.exports = SessionPropertiesEvent;

},{}],190:[function(require,module,exports){
var ConversationIDSerialiser = require('conversation/conversation-id-serialiser');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
    },
    write : function(output, request) {
        if (request.properties) {
            Codec.writeInt32(output, 0);
            Codec.writeCollection(output, request.properties, Codec.writeString);
        } else {
            Codec.writeInt32(output, 1);
        }

        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;

},{"conversation/conversation-id-serialiser":83,"io/codec":125}],191:[function(require,module,exports){
function SetSessionPropertiesListener() {
}

module.exports = SetSessionPropertiesListener;

},{}],192:[function(require,module,exports){
var ControlGroupSerialiser = require('control/control-group-serialiser');
var BEES = require('serialisers/byte-encoded-enum-serialiser');

var Services = require('services/services');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var definition = BEES.read(input, Services);
        var group = ControlGroupSerialiser.read(input);
        var path = Codec.readString(input);

        return {
            definition : definition,
            group : group,
            path : path
        };
    },
    write : function(output, params) {
        Codec.writeInt32(output, params.definition.id);
        ControlGroupSerialiser.write(output, params.group);
        Codec.writeString(output, params.path);
    }
};

module.exports = serialiser;

},{"control/control-group-serialiser":80,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151,"services/services":220}],193:[function(require,module,exports){
function TopicControlRegistrationParams(definition, group, path) {
    this.definition = definition;
    this.group = group;
    this.path = path;
}

module.exports = TopicControlRegistrationParams;

},{}],194:[function(require,module,exports){
var TopicControlRegistrationParamsSerialiser = require('services/control/topic-control-registration-params-serialiser');
var ConversationIDSerialiser = require('conversation/conversation-id-serialiser');

var serialiser = {
    read : function(input) {
        var params = TopicControlRegistrationParamsSerialiser.read(input);
        var cid = ConversationIDSerialiser.read(input);

        return {
            params : params,
            cid : cid
        };
    },
    write : function(output, request) {
        TopicControlRegistrationParamsSerialiser.write(output, request.params);
        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;

},{"conversation/conversation-id-serialiser":83,"services/control/topic-control-registration-params-serialiser":192}],195:[function(require,module,exports){
function TopicControlRegistrationRequest() {

}

module.exports = TopicControlRegistrationRequest;

},{}],196:[function(require,module,exports){
var Codec = require('io/codec');
var ErrorReport = require('services/error-report');

var serialiser = {
    read : function(input) {
        var message = Codec.readString(input);
        var line = Codec.readInt32(input);
        var column = Codec.readInt32(input);

        return new ErrorReport(message, line, column);
    },
    write : function(output, value) {
        Codec.writeString(output, value.message);
        Codec.writeInt32(output, value.line);
        Codec.writeInt32(output, value.column);
    }
};

module.exports = serialiser;

},{"io/codec":125,"services/error-report":197}],197:[function(require,module,exports){
var _implements = require('util/interface')._implements;
var requireNonNull = require('util/require-non-null');
var ErrorReport = require('../../error-report');

module.exports = _implements(ErrorReport, function ErrorReportImpl(message, line, column) {
    this.message = requireNonNull(message);
    this.line = line || 0;
    this.column = column || 0;

    this.toString = function() {
        return "ErrorReport [" + this.message + " at [" + this.line + ":" + this.column + "]]";
    };
});

},{"../../error-report":41,"util/interface":279,"util/require-non-null":285}],198:[function(require,module,exports){
var TopicSelectorSerialiser = require('topics/topic-selector-serialiser');
var CIDSerialiser = require('conversation/conversation-id-serialiser');

var FetchRequest = require('services/fetch/fetch-request');

module.exports = {
    read : function(input) {
        var selector = TopicSelectorSerialiser.read(input);
        var cid = CIDSerialiser.read(input);

        return new FetchRequest(cid, selector);
    },
    write : function(output, request) {
        TopicSelectorSerialiser.write(output, request.selector);
        CIDSerialiser.write(output, request.cid);
    }
};
},{"conversation/conversation-id-serialiser":83,"services/fetch/fetch-request":199,"topics/topic-selector-serialiser":268}],199:[function(require,module,exports){
module.exports = function FetchRequest(cid, selector) {
    this.cid = cid;
    this.selector = selector;
};
},{}],200:[function(require,module,exports){
var Codec = require('io/codec');

var GetTopicDetailsCommand = require('./get-topic-details');
var TopicDetailsSerialiser = require('topics/details/topic-details-serialiser');

var serialiser = {
    read : function(input) {
        return TopicDetailsSerialiser.read(input);
    },
    write : function(output, request) {
        Codec.writeString(output, request.topic_name);
        Codec.writeByte(output, request.level);
    }
};

module.exports = serialiser;

},{"./get-topic-details":201,"io/codec":125,"topics/details/topic-details-serialiser":258}],201:[function(require,module,exports){
function GetTopicDetails() {
}

module.exports = GetTopicDetails;

},{}],202:[function(require,module,exports){
var TopicSelectorSerialiser = require('topics/topic-selector-serialiser');
var CIDSerialiser = require('conversation/conversation-id-serialiser');
var SessionSerialiser = require('session/session-id-serialiser');

module.exports = {
    read : function(input) {
        var sessionID = SessionSerialiser.read(input);
        var selector = TopicSelectorSerialiser.read(input);
        var cid = CIDSerialiser.read(input);

        return {
            sessionID : sessionID,
            selector : selector,
            cid : cid
        };
    },
    write : function(output, req) {
        SessionSerialiser.write(output, req.sessionID);
        TopicSelectorSerialiser.write(output, req.selector);
        CIDSerialiser.write(output, req.cid);
    }
};
},{"conversation/conversation-id-serialiser":83,"session/session-id-serialiser":253,"topics/topic-selector-serialiser":268}],203:[function(require,module,exports){
module.exports = function MissingTopicRequest(sessionID, selector) {
    this.sessionID = sessionID;
    this.selector = selector;
};
},{}],204:[function(require,module,exports){
var RemoveTopicCommand = require('./remove-topic');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
    },
    write : function(output, req) {
        Codec.writeString(output, req.selector.expression);
    }
};

module.exports = serialiser;

},{"./remove-topic":205,"io/codec":125}],205:[function(require,module,exports){
function RemoveTopic() {
}

module.exports = RemoveTopic;

},{}],206:[function(require,module,exports){
var TopicPermission = require('features/security').TopicPermission;
var GlobalPermission = require('features/security').GlobalPermission;

var Converter = require('serialisers/enum-converter');
var curryR = require('util/function').curryR;
var Codec = require('io/codec');

var writeGlobalPerm = curryR(Converter.write, GlobalPermission);
var writeTopicPerm = curryR(Converter.write, TopicPermission);

var readGlobalPerm = curryR(Converter.read, GlobalPermission);
var readTopicPerm = curryR(Converter.read, TopicPermission);

var writeTopicPerms = curryR(Codec.writeCollection, writeTopicPerm);
var readTopicPerms = curryR(Codec.readCollection, readTopicPerm);

var serialiser = {
    read : function(input) {
        return {
            name : Codec.readString(input),
            global : Codec.readCollection(input, readGlobalPerm),
            default : Codec.readCollection(input, readTopicPerm),
            topic : Codec.readDictionary(input, readTopicPerms),
            roles : Codec.readCollection(input, Codec.readString)
        };
    },
    write : function(output, role) {
        Codec.writeString(output, role.name);
        Codec.writeCollection(output, role.global, writeGlobalPerm);
        Codec.writeCollection(output, role.default, writeTopicPerm);
        Codec.writeDictionary(output, role.topic, writeTopicPerms);
        Codec.writeCollection(output, role.roles, Codec.writeString);
    }
};

module.exports = serialiser;

},{"features/security":108,"io/codec":125,"serialisers/enum-converter":152,"util/function":278}],207:[function(require,module,exports){
var Configuration = require('features/security').Configuration;
var RoleSerialiser = require('services/security/role-serialiser');

var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var anonymous = Codec.readCollection(input, Codec.readString);
        var named = Codec.readCollection(input, Codec.readString);
        var roles = Codec.readCollection(input, RoleSerialiser.read);

        return new Configuration(anonymous, named, roles);
    },
    write : function(output, config) {
        Codec.writeCollection(output, config.anonymous, Codec.writeString);
        Codec.writeCollection(output, config.named, Codec.writeString);
        Codec.writeCollection(output, config.roles, RoleSerialiser.write);
    }
};

module.exports = serialiser;


},{"features/security":108,"io/codec":125,"services/security/role-serialiser":206}],208:[function(require,module,exports){
var FilterSendRequest = require('./filter-send-request');

var OptionsSerialiser = require('./send-options-serialiser');
var ContentSerialiser = require('content/serialiser');
var CIDSerialiser = require('conversation/conversation-id-serialiser');

var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var filter = Codec.readString(input);
        var path = Codec.readString(input);
        var content = ContentSerialiser.read(input);
        var options = OptionsSerialiser.read(input);
        var cid = CIDSerialiser.read(input);
        return {
            cid : cid,
            path : path,
            filter : filter,
            message : content,
            options : options
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.filter);
        Codec.writeString(output, req.path);
        ContentSerialiser.write(output, req.content);
        OptionsSerialiser.write(output, req.options);
        CIDSerialiser.write(output, req.cid);
    }
};

module.exports = serialiser;

},{"./filter-send-request":209,"./send-options-serialiser":212,"content/serialiser":75,"conversation/conversation-id-serialiser":83,"io/codec":125}],209:[function(require,module,exports){
function FilterSendRequest(path, message, filter) {
    this.path = path;
    this.message = message;
    this.filter = filter;
}

module.exports = FilterSendRequest;

},{}],210:[function(require,module,exports){
var Codec = require('io/codec');
var ErrorReportSerialiser = require('../error-report-serialiser');

var FilterSendResult = require('./filter-send-result');

var serialiser = {
    read : function(input) {
        var sent = Codec.readInt32(input);
        var errors = Codec.readCollection(input, ErrorReportSerialiser.read);
        return new FilterSendResult(sent, errors);
    },
    write : function(output, req) {
        Codec.writeInt32(output, req.sent);
        Codec.writeCollection(output, req.errors, ErrorReportSerialiser.write);
    }
};

module.exports = serialiser;

},{"../error-report-serialiser":196,"./filter-send-result":211,"io/codec":125}],211:[function(require,module,exports){
var requireNonNull = require('util/require-non-null');

function FilterSendResult(sent, errors) {
    this.sent = requireNonNull(sent);
    this.errors = requireNonNull(errors);
}

FilterSendResult.prototype.toString = function() {
    return 'FilterSendResult [' + this.sent + ' sent, errors=' + this.errors + ']';
};

module.exports = FilterSendResult;

},{"util/require-non-null":285}],212:[function(require,module,exports){
var BEES = require('serialisers/byte-encoded-enum-serialiser');
var Codec = require('io/codec');

var Priority = require('../../../features/messages').Priority;

var serialiser = {
    read : function(input) {
        var priority = BEES.read(input, Priority);
        var headers = Codec.readCollection(input, Codec.readString);

        return {
            priority : priority,
            headers : headers
        };
    },
    write : function(output, req) {
        BEES.write(output, req.priority);
        Codec.writeCollection(output, req.headers, Codec.writeString);
    }
};

module.exports = serialiser;

},{"../../../features/messages":50,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151}],213:[function(require,module,exports){
var SendRequest = require('./send-request');

var OptionsSerialiser = require('./send-options-serialiser');
var ContentSerialiser = require('content/serialiser');

var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var path = Codec.readString(input);
        var content = ContentSerialiser.read(input);
        var options = OptionsSerialiser.read(input);

        return {
            path : path,
            content : content,
            options : options
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.path);
        ContentSerialiser.write(output, req.content);
        OptionsSerialiser.write(output, req.options);
    }
};

module.exports = serialiser;

},{"./send-options-serialiser":212,"./send-request":214,"content/serialiser":75,"io/codec":125}],214:[function(require,module,exports){
function SendRequest(path, message) {
    this.path = path;
    this.message = message;
}

module.exports = SendRequest;

},{}],215:[function(require,module,exports){
var SessionForwardSendRequest = require('./session-forward-send-request');

var OptionsSerialiser = require('./send-options-serialiser');
var ContentSerialiser = require('content/serialiser');
var SessionSerialiser = require('session/session-id-serialiser');
var CIDSerialiser = require('conversation/conversation-id-serialiser');

var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var path = Codec.readString(input);
        var content = ContentSerialiser.read(input);
        var sender = SessionSerialiser.read(input);
        var options = OptionsSerialiser.read(input);
        var sessionProperties = Codec.readDictionary(input, Codec.readString);
        var cid = CIDSerialiser.read(input);
        return {
            cid : cid,
            path : path,
            sender : sender,
            message : content,
            options : options,
            sessionProperties : sessionProperties
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.path);
        ContentSerialiser.write(output, req.content);
        SessionSerialiser.write(output, req.session);
        OptionsSerialiser.write(output, req.options);
        Codec.writeDictionary(output, req.sessionProperties, Codec.writeString);
        CIDSerialiser.write(output, req.cid);
    }
};

module.exports = serialiser;

},{"./send-options-serialiser":212,"./session-forward-send-request":216,"content/serialiser":75,"conversation/conversation-id-serialiser":83,"io/codec":125,"session/session-id-serialiser":253}],216:[function(require,module,exports){
function SessionForwardSendRequest(path, message, recipient, sessionProperties) {
    this.path = path;
    this.message = message;
    this.recipient = recipient;
    this.sessionProperties = sessionProperties;
}

module.exports = SessionForwardSendRequest;

},{}],217:[function(require,module,exports){
var SessionSendRequest = require('./session-send-request');

var OptionsSerialiser = require('./send-options-serialiser');
var ContentSerialiser = require('content/serialiser');
var SessionSerialiser = require('session/session-id-serialiser');
var CIDSerialiser = require('conversation/conversation-id-serialiser');

var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var path = Codec.readString(input);
        var content = ContentSerialiser.read(input);
        var sender = SessionSerialiser.read(input); 
        var options = OptionsSerialiser.read(input);
        var cid = CIDSerialiser.read(input);
        return {
            cid : cid,
            path : path,
            sender : sender,
            message : content,
            options : options
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.path);
        ContentSerialiser.write(output, req.content);
        SessionSerialiser.write(output, req.session);
        OptionsSerialiser.write(output, req.options);
        CIDSerialiser.write(output, req.cid);
    }
};

module.exports = serialiser;

},{"./send-options-serialiser":212,"./session-send-request":218,"content/serialiser":75,"conversation/conversation-id-serialiser":83,"io/codec":125,"session/session-id-serialiser":253}],218:[function(require,module,exports){
function SessionSendRequest(path, message, recipient) {
    this.path = path;
    this.message = message;
    this.recipient = recipient;
}

module.exports = SessionSendRequest;

},{}],219:[function(require,module,exports){
var HashMap = require('hashmap');

    var CommandHeader = require('services/command-header');
var CommandHeaderSerialiser = require('services/command-header-serialiser');

var CommandError = require('services/command-error');
var CommandErrorSerialiser = require('services/command-error-serialiser');

var TopicSelector = require('../../selectors/topic-selector');
var TopicSelectorSerialiser = require('topics/topic-selector-serialiser');

var ErrorReport = require('services/error-report');
var ErrorReportSerialiser = require('services/error-report-serialiser.js');

var serialisers = new HashMap();

serialisers.set(CommandError, CommandErrorSerialiser);
serialisers.set(CommandHeader, CommandHeaderSerialiser);
serialisers.set(TopicSelector, TopicSelectorSerialiser);
serialisers.set(ErrorReport, ErrorReportSerialiser);

serialisers.set(null, {
    read : function() { return null; },
    write : function() { }
});

serialisers.set(Boolean, require('serialisers/boolean-serialiser'));

serialisers.set(
    require('services/fetch/fetch-request'),
    require('services/fetch/fetch-request-serialiser')
);
serialisers.set(
    require('./subscription-notification/subscription-notification'), 
    require('./subscription-notification/subscription-notification-serialiser')
);
serialisers.set(
    require('./unsubscription-notification/unsubscription-notification'), 
    require('./unsubscription-notification/unsubscription-notification-serialiser')
);
serialisers.set(
    require('./get-topic-details/get-topic-details'),
    require('./get-topic-details/get-topic-details-serialiser')
);
serialisers.set(
    require('./add-topic/add-topic'),
    require('./add-topic/add-topic-serialiser')
);
serialisers.set(
    require('./remove-topic/remove-topic'),
    require('./remove-topic/remove-topic-serialiser')
);
serialisers.set(
    require('./update-topic/update-topic-request'),
    require('./update-topic/update-topic-request-serialiser')
);
serialisers.set(
    require('./update-topic/update-topic-response'),
    require('./update-topic/update-topic-response-serialiser')
);
serialisers.set(
    require('./wills/topic-will-parameters'),
    require('./wills/topic-will-parameters-serialiser')
);
serialisers.set(
    require('./wills/will-registration-result'),
    require('./wills/will-registration-result-serialiser')
);

serialisers.set(
    require('./send/send-request'),
    require('./send/send-request-serialiser')
);

serialisers.set(
    require('./send/session-send-request'),
    require('./send/session-send-request-serialiser')
);

var security = require('../../features/security');

serialisers.set(
    security.SystemAuthenticationConfiguration,
    require('./authentication/system-authentication-configuration-serialiser')
);
serialisers.set(
    security.SecurityConfiguration,
    require('./security/security-configuration-serialiser')
);
serialisers.set(
    security.SystemPrincipal,
    require('./authentication/system-principal-serialiser')
);
serialisers.set(
    require('./authentication/security-command-script'),
    require('./authentication/security-command-script-serialiser')
);
serialisers.set(
    require('./authentication/security-command-script-result'),
    require('./authentication/security-command-script-result-serialiser')
);
serialisers.set(
    require('session/session-id'),
    require('session/session-id-serialiser')
);
serialisers.set(
    require('services/control/topic-control-registration-request'),
    require('services/control/topic-control-registration-request-serialiser')
);
serialisers.set(
    require('services/control/topic-control-registration-params'),
    require('services/control/topic-control-registration-params-serialiser')
);
serialisers.set(
    require('services/control/control-registration-request'),
    require('services/control/control-registration-request-serialiser')
);
serialisers.set(
    require('services/control/control-registration-request'),
    require('services/control/control-registration-request-serialiser')
);
serialisers.set(
    require('services/change-principal/change-principal-request'),
    require('services/change-principal/change-principal-request-serialiser')
);
serialisers.set(
    require('./send/session-forward-send-request'),
    require('./send/session-forward-send-request-serialiser')
);
serialisers.set(
    require('services/control/message-receiver-control-registration-request'),
    require('services/control/message-receiver-control-registration-request-serialiser')
);
serialisers.set(
    require('services/control/message-receiver-control-registration-params'),
    require('services/control/message-receiver-control-registration-params-serialiser')
);
serialisers.set(
    require('./send/filter-send-request'),
    require('./send/filter-send-request-serialiser')
);
serialisers.set(
    require('./send/filter-send-result'),
    require('./send/filter-send-result-serialiser')
);
serialisers.set(
    require('services/control/get-session-properties'),
    require('services/control/get-session-properties-serialiser')
);
serialisers.set(
    require('services/control/set-session-properties-listener'),
    require('services/control/set-session-properties-listener-serialiser')
);
serialisers.set(
    require('services/control/session-properties-event'),
    require('services/control/session-properties-event-serialiser')
);
serialisers.set(
    require('services/control/session-properties-event-batch'),
    require('services/control/session-properties-event-batch-serialiser')
);
serialisers.set(
    require('services/topic-update/update-source-registration-request'),
    require('services/topic-update/update-source-registration-request-serialiser')
);
serialisers.set(
    require('services/topic-update/update-source-deregistration-request'),
    require('services/topic-update/update-source-deregistration-request-serialiser')
);
serialisers.set(
    require('services/topic-update/update-source-state-request'),
    require('services/topic-update/update-source-state-request-serialiser')
);
serialisers.set(
    require('services/topic-update/update-source-state'),
    require('services/topic-update/update-source-state-serialiser')
);
serialisers.set(
    require('services/topic-update/update-source-update'),
    require('services/topic-update/update-source-update-serialiser')
);
serialisers.set(
    require('services/topic-update/update-source-update-response'),
    require('services/topic-update/update-source-update-response-serialiser')
);
serialisers.set(
    require('services/update-topic/update-topic-set-request'),
    require('services/update-topic/update-topic-set-request-serialiser')
);
serialisers.set(
    require('services/update-topic/update-topic-delta-request'),
    require('services/update-topic/update-topic-delta-request-serialiser')
);
serialisers.set(
    require('services/topic-update/update-source-set-request'),
    require('services/topic-update/update-source-set-request-serialiser')
);
serialisers.set(
    require('services/topic-update/update-source-delta-request'),
    require('services/topic-update/update-source-delta-request-serialiser')
);
serialisers.set(
    require('services/missing-topic/missing-topic-request'),
    require('services/missing-topic/missing-topic-request-serialiser')
);
serialisers.set(
    require('services/control-client/client-subscribe-request'),
    require('services/control-client/client-subscribe-request-serialiser')
);
serialisers.set(
    require('services/control-client/client-filter-subscribe-request'),
    require('services/control-client/client-filter-subscribe-request-serialiser')
);
serialisers.set(
    require('services/control-client/client-filter-subscribe-response'),
    require('services/control-client/client-filter-subscribe-response-serialiser')
);

module.exports = serialisers;

},{"../../features/security":51,"../../selectors/topic-selector":295,"./add-topic/add-topic":154,"./add-topic/add-topic-serialiser":153,"./authentication/security-command-script":159,"./authentication/security-command-script-result":157,"./authentication/security-command-script-result-serialiser":156,"./authentication/security-command-script-serialiser":158,"./authentication/system-authentication-configuration-serialiser":160,"./authentication/system-principal-serialiser":161,"./get-topic-details/get-topic-details":201,"./get-topic-details/get-topic-details-serialiser":200,"./remove-topic/remove-topic":205,"./remove-topic/remove-topic-serialiser":204,"./security/security-configuration-serialiser":207,"./send/filter-send-request":209,"./send/filter-send-request-serialiser":208,"./send/filter-send-result":211,"./send/filter-send-result-serialiser":210,"./send/send-request":214,"./send/send-request-serialiser":213,"./send/session-forward-send-request":216,"./send/session-forward-send-request-serialiser":215,"./send/session-send-request":218,"./send/session-send-request-serialiser":217,"./subscription-notification/subscription-notification":222,"./subscription-notification/subscription-notification-serialiser":221,"./unsubscription-notification/unsubscription-notification":240,"./unsubscription-notification/unsubscription-notification-serialiser":239,"./update-topic/update-topic-request":244,"./update-topic/update-topic-request-serialiser":243,"./update-topic/update-topic-response":246,"./update-topic/update-topic-response-serialiser":245,"./wills/topic-will-parameters":250,"./wills/topic-will-parameters-serialiser":249,"./wills/will-registration-result":252,"./wills/will-registration-result-serialiser":251,"hashmap":8,"serialisers/boolean-serialiser":150,"services/change-principal/change-principal-request":163,"services/change-principal/change-principal-request-serialiser":162,"services/command-error":165,"services/command-error-serialiser":164,"services/command-header":167,"services/command-header-serialiser":166,"services/control-client/client-filter-subscribe-request":169,"services/control-client/client-filter-subscribe-request-serialiser":168,"services/control-client/client-filter-subscribe-response":171,"services/control-client/client-filter-subscribe-response-serialiser":170,"services/control-client/client-subscribe-request":173,"services/control-client/client-subscribe-request-serialiser":172,"services/control/control-registration-request":177,"services/control/control-registration-request-serialiser":176,"services/control/get-session-properties":179,"services/control/get-session-properties-serialiser":178,"services/control/message-receiver-control-registration-params":181,"services/control/message-receiver-control-registration-params-serialiser":180,"services/control/message-receiver-control-registration-request":183,"services/control/message-receiver-control-registration-request-serialiser":182,"services/control/session-properties-event":189,"services/control/session-properties-event-batch":186,"services/control/session-properties-event-batch-serialiser":185,"services/control/session-properties-event-serialiser":187,"services/control/set-session-properties-listener":191,"services/control/set-session-properties-listener-serialiser":190,"services/control/topic-control-registration-params":193,"services/control/topic-control-registration-params-serialiser":192,"services/control/topic-control-registration-request":195,"services/control/topic-control-registration-request-serialiser":194,"services/error-report":197,"services/error-report-serialiser.js":196,"services/fetch/fetch-request":199,"services/fetch/fetch-request-serialiser":198,"services/missing-topic/missing-topic-request":203,"services/missing-topic/missing-topic-request-serialiser":202,"services/topic-update/update-source-delta-request":224,"services/topic-update/update-source-delta-request-serialiser":223,"services/topic-update/update-source-deregistration-request":226,"services/topic-update/update-source-deregistration-request-serialiser":225,"services/topic-update/update-source-registration-request":228,"services/topic-update/update-source-registration-request-serialiser":227,"services/topic-update/update-source-set-request":230,"services/topic-update/update-source-set-request-serialiser":229,"services/topic-update/update-source-state":234,"services/topic-update/update-source-state-request":232,"services/topic-update/update-source-state-request-serialiser":231,"services/topic-update/update-source-state-serialiser":233,"services/topic-update/update-source-update":238,"services/topic-update/update-source-update-response":236,"services/topic-update/update-source-update-response-serialiser":235,"services/topic-update/update-source-update-serialiser":237,"services/update-topic/update-topic-delta-request":242,"services/update-topic/update-topic-delta-request-serialiser":241,"services/update-topic/update-topic-set-request":248,"services/update-topic/update-topic-set-request-serialiser":247,"session/session-id":254,"session/session-id-serialiser":253,"topics/topic-selector-serialiser":268}],220:[function(require,module,exports){
var TopicSelector = require('../../selectors/topic-selector');

var FetchRequest = require('services/fetch/fetch-request');
var SubscriptionNotification = require('services/subscription-notification/subscription-notification');
var UnsubscriptionNotification = require('services/unsubscription-notification/unsubscription-notification');

var GetTopicDetails = require('services/get-topic-details/get-topic-details');

var AddTopic = require('services/add-topic/add-topic');
var RemoveTopic = require('services/remove-topic/remove-topic');
var UpdateTopicRequest = require('services/update-topic/update-topic-request');
var UpdateTopicSetRequest = require('services/update-topic/update-topic-set-request');
var UpdateTopicDeltaRequest = require('services/update-topic/update-topic-delta-request');
var UpdateTopicResponse = require('services/update-topic/update-topic-response');

var ClientSubscribeRequest = require('services/control-client/client-subscribe-request');
var ClientFilterSubscribeRequest = require('services/control-client/client-filter-subscribe-request');
var ClientFilterSubscribeResponse = require('services/control-client/client-filter-subscribe-response');

var SecurityCommandScript = require('services/authentication/security-command-script');
var SecurityCommandScriptResult = require('services/authentication/security-command-script-result');

var SecurityConfiguration = require('../../features/security').SecurityConfiguration;
var SystemAuthenticationConfiguration = require('../../features/security').SystemAuthenticationConfiguration;

var TopicWillParameters = require('services/wills/topic-will-parameters');
var WillRegistrationResult = require('services/wills/will-registration-result');

var SendRequest = require('services/send/send-request');
var SessionSendRequest = require('services/send/session-send-request');

var TopicControlRegistrationRequest = require('services/control/topic-control-registration-request');
var TopicControlRegistrationParams = require('services/control/topic-control-registration-params');

var UpdateSourceRegistrationRequest = require('services/topic-update/update-source-registration-request');
var UpdateSourceDeregistrationRequest = require('services/topic-update/update-source-deregistration-request');
var UpdateSourceUpdate = require('services/topic-update/update-source-update');
var UpdateSourceSetRequest = require('services/topic-update/update-source-set-request');
var UpdateSourceDeltaRequest = require('services/topic-update/update-source-delta-request');
var UpdateSourceUpdateResponse = require('services/topic-update/update-source-update-response');
var UpdateSourceStateRequest = require('services/topic-update/update-source-state-request');
var UpdateSourceState = require('services/topic-update/update-source-state');
var ControlRegistrationRequest = require('services/control/control-registration-request');
var ControlRegistrationParams = require('services/control/control-registration-params');

var ChangePrincipalRequest = require('services/change-principal/change-principal-request');

var SessionForwardSendRequest = require('services/send/session-forward-send-request');
var MessageReceiverControlRegistrationRequest =
        require('services/control/message-receiver-control-registration-request');
var MessageReceiverControlRegistrationParams =
        require('services/control/message-receiver-control-registration-params');

var FilterSendRequest = require('services/send/filter-send-request');
var FilterSendResult = require('services/send/filter-send-result');

var GetSessionProperties = require('services/control/get-session-properties');
var SetSessionPropertiesListener = require('services/control/set-session-properties-listener');
var SessionPropertiesEvent = require('services/control/session-properties-event');
var SessionPropertiesEventBatch = require('services/control/session-properties-event-batch');

var MissingTopicRequest = require('services/missing-topic/missing-topic-request');

module.exports = {
    FETCH : {
        id : 2,
        name : "Fetch",
        request : FetchRequest,
        response : null
    },
    SUBSCRIBE : {
        id :  3,
        name : "Subscribe",
        request : TopicSelector,
        response : null
    },
    UNSUBSCRIBE : {
        id : 4,
        name : "Unsubscribe",
        request : TopicSelector,
        response : null
    },
    SUBSCRIBE_CLIENT : {
        id : 10,
        name : "Subscribe session",
        request : ClientSubscribeRequest,
        response : null
    },
    UNSUBSCRIBE_CLIENT : {
        id : 11,
        name : "Unsubscribe session",
        request : ClientSubscribeRequest,
        response : null
    },
    SEND : {
        id : 6,
        name : "Send",
        request : SendRequest,
        response : null
    }, 
    SEND_TO_SESSION : {
        id : 28,
        name : "Send to session",
        request : SessionSendRequest,
        response : null
    },
    SUBSCRIPTION_NOTIFICATION : {
        id : 40,
        name : "Subscription Notification",
        request : SubscriptionNotification,
        response : null
    },
    GET_TOPIC_DETAILS : {
        id : 41,
        name : "Get Topic Details",
        request : GetTopicDetails,
        response : GetTopicDetails
    },
    UNSUBSCRIPTION_NOTIFICATION : {
        id : 42,
        name : "Unsubscription Notification",
        request : UnsubscriptionNotification,
        response : null
    },
    ADD_TOPIC : {
        id : 46,
        name : "Add Topic",
        request : AddTopic,
        response : AddTopic
    },
    REMOVE_TOPIC : {
        id : 47,
        name : "Remove Topic",
        request : RemoveTopic,
        response : null
    },
    MISSING_TOPIC : {
        id : 50,
        name : "Missing topic",
        request : MissingTopicRequest,
        response : Boolean
    },
    TOPIC_SCOPED_WILL_REGISTRATION : {
        id : 53,
        name : "Topic Will Registration",
        request : TopicWillParameters,
        response : WillRegistrationResult
    },
    TOPIC_SCOPED_WILL_DEREGISTRATION : {
        id : 54,
        name : "Topic Will Deregistration",
        request : TopicWillParameters,
        response : null
    },
    SYSTEM_PING : {
        id : 55,
        name : "System Ping",
        request : null,
        response : null
    },
    USER_PING : {
        id : 56,
        name : "User Ping",
        request : null,
        response : null
    },
    CHANGE_PRINCIPAL : {
        id : 5,
        name : "Change Principal",
        request : ChangePrincipalRequest,
        response : Boolean
    },  
    GET_SYSTEM_AUTHENTICATION : {
        id : 57,
        name : "Get System Authentication",
        request : null,
        response : SystemAuthenticationConfiguration 
    },
    UPDATE_SYSTEM_AUTHENTICATION : {
        id : 58,
        name : "Update System Authentication",
        request : SecurityCommandScript,
        response : SecurityCommandScriptResult
    },
    GET_SECURITY_CONFIGURATION : {
        id : 59,
        name : "Get Security Configuration",
        request : null,
        response : SecurityConfiguration
    },
    UPDATE_SECURITY_CONFIGURATION : {
        id : 60,
        name : "Update Security Configuration",
        request : SecurityCommandScript,
        response : SecurityCommandScriptResult
    },
    UPDATE_TOPIC : {
        id : 61,
        name : "Update Topic",
        request : UpdateTopicRequest,
        response : UpdateTopicResponse
    },
    SERVER_CONTROL_REGISTRATION : {
        id : 18,
        name : "Server Control Registration",
        request : ControlRegistrationRequest,
        response : null
    },
    SERVER_CONTROL_DEREGISTRATION : {
        id : 19,
        name : "Server Control Deregistration",
        request : ControlRegistrationParams,
        response : null
    },
    TOPIC_CONTROL_REGISTRATION : {
        id : 20,
        name : "Topic Control Registration",
        request : TopicControlRegistrationRequest,
        response : null
    },
    TOPIC_CONTROL_DEREGISTRATION : {
        id : 21, 
        name : "Topic Control Deregistration",
        request : TopicControlRegistrationParams,
        response : null
    },
    UPDATE_SOURCE_REGISTRATION : {
        id : 30,
        name : "Update Source Registration",
        request : UpdateSourceRegistrationRequest,
        response : UpdateSourceState
    },
    UPDATE_SOURCE_DEREGISTRATION : {
        id : 31,
        name : "Update Source Deregistration",
        request : UpdateSourceDeregistrationRequest,
        response : null
    },
    UPDATE_SOURCE_STATE : {
        id : 32,
        name : "Update Source State",
        request : UpdateSourceStateRequest,
        response : null
    },
    UPDATE_SOURCE_UPDATE : {
        id : 35,
        name : "Update Source Update",
        request : UpdateSourceUpdate,
        response : UpdateSourceUpdateResponse 
    },
    SEND_RECEIVER_CLIENT : {
        id : 62,
        name : "Control Client Send Service",
        request : SessionForwardSendRequest,
        response : null
    },
    MESSAGE_RECEIVER_CONTROL_REGISTRATION : {
        id : 63,
        name : "Message Receiver Control Registration",
        request : MessageReceiverControlRegistrationRequest,
        response : null
    },
    MESSAGE_RECEIVER_CONTROL_DEREGISTRATION : {
        id : 64,
        name : "Message Receiver Control Deregistration",
        request : MessageReceiverControlRegistrationParams,
        response : null
    },
    FILTER_SUBSCRIBE : {
        id : 65,
        name : "Subscribe to filter",
        request : ClientFilterSubscribeRequest,
        response : ClientFilterSubscribeResponse
    },
    FILTER_UNSUBSCRIBE : {
        id : 66,
        name : "Unsubscribe from filter",
        request : ClientFilterSubscribeRequest,
        response : ClientFilterSubscribeResponse
    },
    SEND_TO_FILTER : {
        id : 29,
        name : "Send to filter",
        request : FilterSendRequest,
        response : FilterSendResult
    },
    GET_SESSION_PROPERTIES : {
        id : 67,
        name : "Get session properties",
        request : GetSessionProperties,
        response : GetSessionProperties
    },
    SESSION_PROPERTIES_REGISTRATION : {
        id : 69,
        name : "Set session properties listener - legacy, see SESSION_PROPERTIES_REGISTRATION_2",
        request : SetSessionPropertiesListener,
        response : null 
    },
    SESSION_PROPERTIES_EVENT : {
        id : 70,
        name : "Session properties event - legacy, see SESSION_PROPERTIES_EVENT_2",
        request : SessionPropertiesEvent,
        response : null 
    },
    UPDATE_SOURCE_SET : {
        id : 77,
        name : "Update source set",
        request : UpdateSourceSetRequest,
        response : UpdateSourceUpdateResponse
    },
    UPDATE_SOURCE_DELTA : {
        id : 78,
        name : "Update source delta",
        request : UpdateSourceDeltaRequest,
        response : UpdateSourceUpdateResponse
    },
    UPDATE_TOPIC_SET : {
        id : 79,
        name : "Update topic set",
        request : UpdateTopicSetRequest,
        response : UpdateTopicResponse
    },
    UPDATE_TOPIC_DELTA : {
        id : 80,
        name : "Update topic delta",
        request : UpdateTopicDeltaRequest,
        response : UpdateTopicResponse
    },
    SESSION_PROPERTIES_REGISTRATION_2 : {
        id : 81,
        name : "Session Properties registration 2",
        request : SetSessionPropertiesListener,
        response : null
    },
    SESSION_PROPERTIES_EVENT_2 : {
        id : 82,
        name : "Session Properties event 2",
        request : SessionPropertiesEventBatch,
        response : null
    },
    TOPIC_REMOVAL : {
        id : 83,
        name : "Topic removal",
        request : RemoveTopic,
        response : null
    }
};

},{"../../features/security":51,"../../selectors/topic-selector":295,"services/add-topic/add-topic":154,"services/authentication/security-command-script":159,"services/authentication/security-command-script-result":157,"services/change-principal/change-principal-request":163,"services/control-client/client-filter-subscribe-request":169,"services/control-client/client-filter-subscribe-response":171,"services/control-client/client-subscribe-request":173,"services/control/control-registration-params":175,"services/control/control-registration-request":177,"services/control/get-session-properties":179,"services/control/message-receiver-control-registration-params":181,"services/control/message-receiver-control-registration-request":183,"services/control/session-properties-event":189,"services/control/session-properties-event-batch":186,"services/control/set-session-properties-listener":191,"services/control/topic-control-registration-params":193,"services/control/topic-control-registration-request":195,"services/fetch/fetch-request":199,"services/get-topic-details/get-topic-details":201,"services/missing-topic/missing-topic-request":203,"services/remove-topic/remove-topic":205,"services/send/filter-send-request":209,"services/send/filter-send-result":211,"services/send/send-request":214,"services/send/session-forward-send-request":216,"services/send/session-send-request":218,"services/subscription-notification/subscription-notification":222,"services/topic-update/update-source-delta-request":224,"services/topic-update/update-source-deregistration-request":226,"services/topic-update/update-source-registration-request":228,"services/topic-update/update-source-set-request":230,"services/topic-update/update-source-state":234,"services/topic-update/update-source-state-request":232,"services/topic-update/update-source-update":238,"services/topic-update/update-source-update-response":236,"services/unsubscription-notification/unsubscription-notification":240,"services/update-topic/update-topic-delta-request":242,"services/update-topic/update-topic-request":244,"services/update-topic/update-topic-response":246,"services/update-topic/update-topic-set-request":248,"services/wills/topic-will-parameters":250,"services/wills/will-registration-result":252}],221:[function(require,module,exports){
var TopicInfoSerialiser = require('topics/details/topic-info-serialiser');
var SubscriptionNotification = require('./subscription-notification');

module.exports = {
    read : function(input) {
        var info = TopicInfoSerialiser.read(input);
        return new SubscriptionNotification(info);
    },
    write : function(out, notification) {
        TopicInfoSerialiser.write(out, notification.info);
    }
};

},{"./subscription-notification":222,"topics/details/topic-info-serialiser":261}],222:[function(require,module,exports){
function SubscriptionNotification(info) {
    this.info = info;
}

module.exports = SubscriptionNotification;

},{}],223:[function(require,module,exports){
var CIDSerialiser = require('conversation/conversation-id-serialiser');
var Codec = require('io/codec');

module.exports = {
    read : function(bis) {
        return {
            cid : CIDSerialiser.read(bis),
            path : bis.readString(),
            id : bis.readInt32(),
            bytes : bis.readBytes()
        };
    },
    write : function(bos, req) {
        CIDSerialiser.write(bos, req.cid);
        Codec.writeString(bos, req.path);
        Codec.writeInt32(bos, req.id);
        Codec.writeBytes(bos, req.bytes);
    }
};
},{"conversation/conversation-id-serialiser":83,"io/codec":125}],224:[function(require,module,exports){
module.exports = function UpdateSourceDeltaRequest(cid, path, id, bytes) {
    this.cid = cid;
    this.path = path;
    this.id = id;
    this.bytes = bytes;
};
},{}],225:[function(require,module,exports){
var CIDSerialiser = require('conversation/conversation-id-serialiser');

module.exports = {
    read : function(input) {
        var cid = CIDSerialiser.read(input);
        return { cid : cid };
    },
    write : function(output, req) {
        CIDSerialiser.write(output, req.cid);
    }
};

},{"conversation/conversation-id-serialiser":83}],226:[function(require,module,exports){
module.exports = function UpdateSourceDeregistrationRequest(cid) {
    this.cid = cid;
};

},{}],227:[function(require,module,exports){
var CIDSerialiser = require('conversation/conversation-id-serialiser');
var Codec = require('io/codec');

module.exports = {
    read : function(input) {
        var cid = CIDSerialiser.read(input);
        var path = Codec.readString(input);

        return { cid : cid, path : path };
    },
    write : function(output, req) {
        CIDSerialiser.write(output, req.cid);
        Codec.writeString(output, req.path);
    }
};

},{"conversation/conversation-id-serialiser":83,"io/codec":125}],228:[function(require,module,exports){
module.exports = function UpdateSourceRegistrationRequest(cid, path) {
    this.cid = cid;
    this.path = path;
};

},{}],229:[function(require,module,exports){
var CIDSerialiser = require('conversation/conversation-id-serialiser');
var Codec = require('io/codec');

module.exports = {
    read : function(bis) {
        return {
            cid : CIDSerialiser.read(bis),
            path : bis.readString(),
            bytes : bis.readBytes()
        };
    },
    write : function(bos, req) {
        CIDSerialiser.write(bos, req.cid);
        Codec.writeString(bos, req.path);
        Codec.writeBytes(bos, req.bytes);
    }
};
},{"conversation/conversation-id-serialiser":83,"io/codec":125}],230:[function(require,module,exports){
module.exports = function UpdateSourceSetRequest(cid, path, bytes) {
    this.cid = cid;
    this.path = path;
    this.bytes = bytes;
};

},{}],231:[function(require,module,exports){
var UpdateStateSerialiser = require('services/topic-update/update-source-state-serialiser');
var CIDSerialiser = require('conversation/conversation-id-serialiser');

module.exports = {
    read : function(input) {
        var cid = CIDSerialiser.read(input);
        var old = UpdateStateSerialiser.read(input);
        var current = UpdateStateSerialiser.read(input);

        return {
            cid : cid,
            old : old,
            current : current
        };
    },
    write : function(output, req) {
        CIDSerialiser.write(output, req.cid);
        UpdateStateSerialiser.write(output, req.old);
        UpdateStateSerialiser.write(output, req.current);
    }
};

},{"conversation/conversation-id-serialiser":83,"services/topic-update/update-source-state-serialiser":233}],232:[function(require,module,exports){
module.exports = function UpdateSourceStateRequest(cid, old, current) {
    this.cid = cid;
    this.old = old;
    this.current = current;
};

},{}],233:[function(require,module,exports){
var EnumSerialiser = require('serialisers/enum-converter');

var State = {
    init : 0,
    active : 1,
    closed : 2,
    standby : 3
};

module.exports = {
    read : function(input) {
        return EnumSerialiser.read(input, State);
    },
    write : function(output, req) {
        EnumSerialiser.write(output, req, State);
    }
};

},{"serialisers/enum-converter":152}],234:[function(require,module,exports){
module.exports = function UpdateSourceState(state) {
    this.state = state;
};

},{}],235:[function(require,module,exports){
var UpdateSourceUpdateResponse = require('services/topic-update/update-source-update-response');
var EnumSerialiser = require('serialisers/enum-converter');
var Codec = require('io/codec');

var ResponseCodes = {
    SUCCESS : 0,
    INCOMPATIBLE_UPDATE : 1,
    UPDATE_FAILED : 2,
    INVALID_UPDATER : 3,
    MISSING_TOPIC : 4,
    INVALID_ADDRESS : 5,
    DUPLICATES : 6,
    EXCLUSIVE_UPDATER_CONFLICT : 7
};

module.exports = {
    read : function(input) {
        if (Codec.readByte(input) === 0) {
            return new UpdateSourceUpdateResponse();
        }

        var error = EnumSerialiser.read(input, ResponseCodes);

        return new UpdateSourceUpdateResponse(error);
    },
    write : function(output, req) {
        if (req.error) {
            Codec.writeByte(output, 1);
            EnumSerialiser.write(output, req.error, ResponseCodes);
        } else {
            Codec.writeByte(output, 0);
        }
    }
};

},{"io/codec":125,"serialisers/enum-converter":152,"services/topic-update/update-source-update-response":236}],236:[function(require,module,exports){
module.exports = function UpdateSourceUpdateResponse(error) {
    this.error = error;
};

},{}],237:[function(require,module,exports){
var CIDSerialiser = require('conversation/conversation-id-serialiser');
var UpdateSerialiser = require('update/serialiser');
var Codec = require('io/codec');

module.exports = {
    read : function(input) {
        var cid = CIDSerialiser.read(input);
        var path = Codec.readString(input);
        var val = UpdateSerialiser.read(input);

        return { cid : cid, path : path, update : val };
    },
    write : function(output, req) {
        CIDSerialiser.write(output, req.cid);
        Codec.writeString(output, req.path);
        UpdateSerialiser.write(output, req.update);
    }
};

},{"conversation/conversation-id-serialiser":83,"io/codec":125,"update/serialiser":274}],238:[function(require,module,exports){
module.exports = function UpdateSourceUpdate(cid, path, update) {
    this.cid = cid;
    this.path = path;
    this.update = update;
};

},{}],239:[function(require,module,exports){
var UnsubscribeReason = require('../../../topics/topics').UnsubscribeReason;

var TopicIdSerialiser = require('topics/details/topic-id-serialiser');
var UnsubscriptionNotification = require('./unsubscription-notification');
var ByteEncodedEnumSerialiser = require('serialisers/byte-encoded-enum-serialiser');

module.exports = {
    read : function(input) {
        var id = TopicIdSerialiser.read(input);
        var reason = ByteEncodedEnumSerialiser.read(input, UnsubscribeReason);

                return new UnsubscriptionNotification(id, reason);
    },
    write : function(out, notification) {
        TopicIdSerialiser.write(out, notification.id);
        ByteEncodedEnumSerialiser.write(out, notification.reason);
    }
};
},{"../../../topics/topics":298,"./unsubscription-notification":240,"serialisers/byte-encoded-enum-serialiser":151,"topics/details/topic-id-serialiser":260}],240:[function(require,module,exports){
function UnsubscriptionNotification(id, reason) {
	this.id = id;
	this.reason = reason;
}

module.exports = UnsubscriptionNotification;

},{}],241:[function(require,module,exports){
var Codec = require('io/codec');

module.exports = {
    read : function(bis) {
        return {
            path : bis.readString(),
            id : bis.readInt32(),
            bytes : bis.readBytes()
        };
    },
    write : function(bos, req) {
        Codec.writeString(bos, req.path);
        Codec.writeInt32(bos, req.id);
        Codec.writeBytes(bos, req.bytes);
    }
};
},{"io/codec":125}],242:[function(require,module,exports){
module.exports = function UpdateTopicDeltaRequest(path, id, bytes) {
    this.path = path;
    this.id = id;
    this.bytes = bytes;
};
},{}],243:[function(require,module,exports){
var UpdateSerialiser = require('../../update/serialiser');
var Codec = require('io/codec');

module.exports = {
    read : function(input) {
        return {
            path : Codec.readString(input),
            update : UpdateSerialiser.read(input)
        };
    },
    write : function(output, request) {
        Codec.writeString(output, request.path);
        UpdateSerialiser.write(output, request.update);
    }
};

},{"../../update/serialiser":274,"io/codec":125}],244:[function(require,module,exports){
module.exports = function UpdateTopicRequest(path, update) {
    this.path = path;
    this.update = update;
};
},{}],245:[function(require,module,exports){
var Codec = require('io/codec');
var BEES = require('serialisers/byte-encoded-enum-serialiser');
var UpdateTopicResponse = require('./update-topic-response');

module.exports = {
    read : function(input) {
        var reason = BEES.read(input, UpdateTopicResponse);

        return {
            reason : reason,
            isError : reason !== UpdateTopicResponse.SUCCESS
        };
    },
    write : function(output, request) {
        BEES.write(output, request.reason);
    }
};

},{"./update-topic-response":246,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151}],246:[function(require,module,exports){
module.exports = {
    SUCCESS : {
        id : 0,
        reason : "The update was succesful"
    },
    INCOMPATIBLE_UPDATE : {
        id : 1,
        reason : "The update value was incompatible with the topic type"
    },
    UPDATE_FAILED : {
        id : 2,
        reason : "The update could not be applied"
    },
    INVALID_UPDATER : {
        id : 3,
        reason : "The updater was invalid for updating this topic"
    },
    MISSING_TOPIC : {
        id : 4,
        reason : "The specified topic does not exist"
    },
    INVALID_ADDRESS : {
        id : 5,
        reason : "The update for a Paged Topic contained an invalid index"
    },
    DUPLICATES : {
        id : 6,
        reason : "The update for a Paged Topic contained invalid duplicate items"
    },
    EXCLUSIVE_UPDATER_CONFLICT : {
        id : 7,
        reason : "An exclusive update source is already registered for the topic branch"
    },
    DELTA_WITHOUT_VALUE : {
        id : 8,
        reason : "A delta was applied to a topic that does not yet have a value"
    }
};

},{}],247:[function(require,module,exports){
var Codec = require('io/codec');

module.exports = {
    read : function(bis) {
        return {
            path : bis.readString(),
            bytes : bis.readBytes()
        };
    },
    write : function(bos, req) {
        Codec.writeString(bos, req.path);
        Codec.writeBytes(bos, req.bytes);
    }
};
},{"io/codec":125}],248:[function(require,module,exports){
module.exports = function UpdateTopicSetRequest(path, bytes) {
    this.path = path;
    this.bytes = bytes;
};


},{}],249:[function(require,module,exports){
var BEES = require('serialisers/byte-encoded-enum-serialiser');
var TopicWillParameters = require('./topic-will-parameters');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var path = Codec.readString(input);
        var will = BEES.read(input, TopicWillParameters.Will);

                return new TopicWillParameters(path, will);
    },
    write : function(out, params) {
        Codec.writeString(out, params.path);
        BEES.write(out, params.will);
    }
};

module.exports = serialiser;

},{"./topic-will-parameters":250,"io/codec":125,"serialisers/byte-encoded-enum-serialiser":151}],250:[function(require,module,exports){
var Will = {
    REMOVE_TOPICS : 0
};

function TopicWillParameters(path, will) {
    this.path = path;
    this.will = will;
}

TopicWillParameters.prototype.toString = function() {
    return "TopicWillParams [" + this.will + ", " + this.path + "]";  
};

TopicWillParameters.Will = Will;

module.exports = TopicWillParameters;
},{}],251:[function(require,module,exports){
var BEES = require('serialisers/byte-encoded-enum-serialiser');
var WillRegistrationResult = require('./will-registration-result');

var serialiser = {
    read : function(input) {
        return BEES.read(input, WillRegistrationResult);
    },
    write : function(output, result) {
        BEES.write(output, result);
    }
};

module.exports = serialiser;
},{"./will-registration-result":252,"serialisers/byte-encoded-enum-serialiser":151}],252:[function(require,module,exports){
var WillRegistrationResult = {
    SUCCESS : 0,
    GROUP_CONFLICT : 1,
    TOPIC_TREE_CONFLICT : 2
};

WillRegistrationResult.toString = function() {
    return "WillRegistrationResult";
};

module.exports = WillRegistrationResult;
},{}],253:[function(require,module,exports){
var SessionID = require('session/session-id');
var Codec = require('io/codec');

var serialiser = {
    read : function(input) {
        var server = Codec.readInt64(input);
        var value = Codec.readInt64(input);

        return new SessionID(server, value);
    },
    write : function(output, sessionID) {
        Codec.writeInt64(output, sessionID.server);
        Codec.writeInt64(output, sessionID.value);
    }
};

module.exports = serialiser;

},{"io/codec":125,"session/session-id":254}],254:[function(require,module,exports){
var Long = require('long');

var padding = "0000000000000000";

function SessionId(server, value) {
    this.server = server;
    this.value = value;
}

    SessionId.prototype.toString = function() {
    var server = padding + this.server.toUnsigned().toString(16);
    var client = padding + this.value.toUnsigned().toString(16);

    return (server.substr(server.length - 16) + '-' + client.substr(client.length - 16)).toLowerCase();
};

SessionId.prototype.toValue = function() {
    return this.toString();
};

SessionId.fromString = function(str) {
    var parts = str.split('-');
    var sessionId;

    try {
        var server = Long.fromString(parts[0], false, 16);
        var value = Long.fromString(parts[1], false, 16);

        sessionId = new SessionId(server, value);
    } catch (e) {
        throw new Error('SessionId must consist of two hexadecimal parts, joined with a \'-\'');
    }

    return sessionId;
};

SessionId.prototype.equals = function(sessionid) {
    if (!(sessionid instanceof SessionId) ||
        !(sessionid.server instanceof Long) ||
        !(sessionid.value instanceof Long)) {
        return false;
    }
    return this.server.compare(sessionid.server) === 0 &&
        this.value.compare(sessionid.value) === 0;
};

SessionId.validate = function(str) {
    if (str instanceof SessionId) {
        return str;
    }

    if (str.length === 0 || str.indexOf('-') < 0 || str.indexOf(' ') >= 0) {
        return false;
    }

    try {
        return SessionId.fromString(str);
    } catch (e) {
        return false;
    }
};

module.exports = SessionId;

},{"long":11}],255:[function(require,module,exports){
var Emitter = require('events/emitter');

var InternalSession = require('client/internal-session');
var ServiceRegistry = require('client/service-registry');
var ConnectionFactory = require('v4-stack/connection-factory');

var LoggingErrorHandler = require('conversation/logging-error-handler');
var DelegatingConversationSet = require('conversation/delegating-conversation-set');

var Services = require('services/services');
var MonitoredPingService = require('client/services/monitored-ping-service');
var PingService = require('client/services/ping-service');
var NotifySubscriptionService = require('client/services/notify-subscription-service');
var NotifyUnsubscriptionService = require('client/services/notify-unsubscription-service');

var Options = require('../../options');
var Session = require('../../session');

function SessionImpl(options) {
    var emitter = new Emitter(undefined, undefined, ['connect', 'reconnect', 'disconnect']);

    var serviceRegistry = new ServiceRegistry();
    var conversationSet = new DelegatingConversationSet(LoggingErrorHandler);

    if (typeof options === 'string') {
        options = new Options({ host : options });
    } else {
        options = new Options(options);
    }

    serviceRegistry.add(Services.USER_PING, PingService);
    serviceRegistry.add(Services.SYSTEM_PING, MonitoredPingService);
    serviceRegistry.add(Services.SUBSCRIPTION_NOTIFICATION, NotifySubscriptionService);
    serviceRegistry.add(Services.UNSUBSCRIPTION_NOTIFICATION, NotifyUnsubscriptionService);

    var internalSession =
        new InternalSession(conversationSet, serviceRegistry, ConnectionFactory, options);

    var session = new Session(internalSession, emitter, options.with({
        credentials : undefined
    }));

    session.on('error', internalSession.close);

    internalSession.on({
        connect : function(sessionID) {
            session.sessionID = sessionID.toString();
            emitter.emit('connect', session);
        },
        reconnect : function() {
            emitter.emit('reconnect');
        },
        disconnect : function(reason) {
            emitter.emit('disconnect', reason);
        },
        error : function(err) {
            emitter.emit('error', err); 
        },
        close : function(reason) {
            emitter.emit('close', reason);
        }
    });

    this.connect = function() {
        internalSession.connect();
    };

    this.get = function() {
        return session;
    };
}

module.exports = SessionImpl;

},{"../../options":294,"../../session":297,"client/internal-session":64,"client/service-registry":67,"client/services/monitored-ping-service":68,"client/services/notify-subscription-service":69,"client/services/notify-unsubscription-service":70,"client/services/ping-service":71,"conversation/delegating-conversation-set":87,"conversation/logging-error-handler":88,"events/emitter":103,"services/services":220,"v4-stack/connection-factory":289}],256:[function(require,module,exports){
var SessionToken = require('session/session-token');

function readToken(input) {
    return new SessionToken(input.readMany(SessionToken.TOKEN_LENGTH));
}

module.exports = readToken;

},{"session/session-token":257}],257:[function(require,module,exports){
(function (Buffer){
var TOKEN_LENGTH = 24;

function SessionToken(external) {
    this.put = function(destination) {
        destination.writeBytes(external);
    };

    this.toString = function() {
        return external.toString('ascii');
    };
}

SessionToken.fromExternalString = function(external) {
    return new SessionToken(new Buffer(external, 'ascii'));
};

SessionToken.TOKEN_LENGTH = TOKEN_LENGTH;

module.exports = SessionToken;

}).call(this,require("buffer").Buffer)
},{"buffer":2}],258:[function(require,module,exports){
var Codec = require('io/codec');
var serialiser = require('schema/serialiser');
var BEES = require('serialisers/byte-encoded-enum-serialiser');
var TopicType = require('../../../topics/topics').TopicType;

module.exports = {
    read : function(input) {
        var details = {};

        if (Codec.readBoolean(input)) {
            details.type = BEES.read(input, TopicType);

            if (Codec.readBoolean(input)) {
                details.schema = serialiser.read(input);
            }
        }

        return details;
    },
    write : function(output, details) {
        if (details) {
            Codec.writeBoolean(output, true);

            BEES.write(output, details.type);

            if (details.schema) {
                Codec.writeBoolean(output, true);
                serialiser.write(output, details.schema);
            }

            Codec.writeBoolean(output, false);
        } else {
            Codec.writeBoolean(output, false);
        }
    }
};

},{"../../../topics/topics":298,"io/codec":125,"schema/serialiser":148,"serialisers/byte-encoded-enum-serialiser":151}],259:[function(require,module,exports){
var TopicType = require('../../../topics/topics').TopicType;
var DataTypes = require('data/datatypes');
var schema = require('schema/schema');
var util = require('metadata/util');

module.exports.deriveDetails = function(value) {
    var details = {};

    for (var t in TopicType) {
        if (TopicType[t] === value) {
            return {
                type : value
            };
        }
    }

    if (util.isMetadataValue(value)) {
        var meta = util.deriveMetadata(value);

        details.type = util.getType(meta);
        details.schema = schema(meta);
    } else {
        var datatype = DataTypes.get(value);

        details.type = TopicType[datatype.name().toUpperCase()];
        details.schema = {};
        details.content = datatype.from(value);
    }

    return details;
};
},{"../../../topics/topics":298,"data/datatypes":94,"metadata/util":134,"schema/schema":147}],260:[function(require,module,exports){
var Codec = require('io/codec');

var serialiser = {
    read : function(bis) {
        return Codec.readInt32(bis);
    },
    write : function(bos, id) {
        Codec.writeInt32(bos, id);
    }
};

module.exports = serialiser;
},{"io/codec":125}],261:[function(require,module,exports){
var Codec = require('io/codec');
var TopicIdSerialiser = require('./topic-id-serialiser');
var TopicDetailsSerialiser = require('./topic-details-serialiser');

var serialiser = {
    read : function(input) {
        return {
            id : TopicIdSerialiser.read(input),
            path : Codec.readString(input),
            details : TopicDetailsSerialiser.read(input)
        };
    },
    write : function(out, info) {
        TopicIdSerialiser.write(out, info.id);
        Codec.writeString(out, info.path);
        TopicDetailsSerialiser.write(out, info.details);
    }
};

module.exports = serialiser;
},{"./topic-details-serialiser":258,"./topic-id-serialiser":260,"io/codec":125}],262:[function(require,module,exports){
var inherits = require('inherits');

var regex = require('util/regex');
var utils = require('topics/topic-path-utils');
var TopicSelector = require('../../selectors/topic-selector');

var DQ = utils.DescendantQualifier;

function FullPathSelector(components) {
    switch (components.qualifier) {
    case DQ.MATCH :
        this.regex = regex(components.base);
        break;
    case DQ.DESCENDANTS_OF_MATCH :
        this.regex = regex(components.base + "/.+");
        break;
    case DQ.MATCH_AND_DESCENDANTS :
        this.regex = regex(components.base + "(?:$|/.+)");
        break;
    }

        TopicSelector.call(
        this, 
        components.type, 
        components.prefix, 
        components.expression);
}

inherits(FullPathSelector, TopicSelector);

FullPathSelector.prototype.doSelects = function(topicPath) {
    return this.regex(topicPath);
};

module.exports = FullPathSelector;

},{"../../selectors/topic-selector":295,"inherits":9,"topics/topic-path-utils":266,"util/regex":284}],263:[function(require,module,exports){
var inherits = require('inherits');
var utils = require('topics/topic-path-utils');
var TopicSelector = require('../../selectors/topic-selector');

var DQ = utils.DescendantQualifier;

function TopicPathSelector(components) {
    var remainder = components.remainder;

        if (components.type === TopicSelector.Type.PATH) {
        if (components.prefix === "") {
            throw new Error("Topic path must have at least one part: " + remainder);
        }

                if (components.prefix.indexOf("//") > -1) {
            throw new Error("Topic path contains empty part: " + remainder);
        }
    }

        this.qualifier = components.qualifier;
    this.path = components.prefix;

        TopicSelector.call(
        this, 
        components.type, 
        components.prefix, 
        components.expression);
}

inherits(TopicPathSelector, TopicSelector);

TopicPathSelector.prototype.doSelects = function(topicPath) {
    var match = this.path === topicPath;
    var descendants = topicPath.indexOf(this.path + '/') === 0;

        switch (this.qualifier) {
    case DQ.MATCH :
        return match;
    case DQ.DESCENDANTS_OF_MATCH :
        return descendants;
    case DQ.MATCH_AND_DESCENDANTS :
        return match || descendants;
    default :
        return false;
    }
};

module.exports = TopicPathSelector;

},{"../../selectors/topic-selector":295,"inherits":9,"topics/topic-path-utils":266}],264:[function(require,module,exports){
var inherits = require('inherits');
var utils = require('topics/topic-path-utils');
var TopicSelector = require('../../selectors/topic-selector');

var DELIMITER = "////";

function extractCommonPrefix(selectors) {
    if (selectors.length === 0) {
        return "";
    }

    var minimum = 2147483647;

        var prefixes = [], i = 0;

        selectors.forEach(function(selector) {      
        var part = utils.split(selector.prefix);

                minimum = Math.min(minimum, part.length);
        prefixes[i++] = part;
    });

        var composite = [];

        assembly:
    for (var j = 0; j < minimum; ++j) {
        var part = prefixes[0][j];

                for (var k = 0; k < prefixes.length; ++k) {
            if (part !== prefixes[k][j]) {
                break assembly;
            }
        }

                composite.push(part);
        composite.push(utils.PATH_SEPARATOR);
    }

        return utils.canonicalise(composite.join(''));
}

function SelectorSet(selectors) {
    var expanded = [];

        selectors.forEach(function(selector) {
        if (selector instanceof SelectorSet) {
            selector.selectors.forEach(function(nested) {
                expanded.push(nested);
            });
        } else {
            expanded.push(selector);
        }
    });

        this.selectors = expanded;

        var remainder = expanded.join(DELIMITER);
    var prefix = extractCommonPrefix(expanded);

        TopicSelector.call(
        this, 
        TopicSelector.Type.SELECTOR_SET, 
        prefix, 
        TopicSelector.Prefix.SELECTOR_SET + remainder);
}

SelectorSet.DELIMITER = DELIMITER;

inherits(SelectorSet, TopicSelector);

SelectorSet.prototype.selects = function(topicPath) {
    for (var i = 0; i < this.selectors.length; ++i) {
        if (this.selectors[i].selects(topicPath)) {
            return true;
        }
    }

        return false;
};

module.exports = SelectorSet;

},{"../../selectors/topic-selector":295,"inherits":9,"topics/topic-path-utils":266}],265:[function(require,module,exports){
var inherits = require('inherits');

var regex = require('util/regex');
var utils = require('topics/topic-path-utils');
var TopicSelector = require('../../selectors/topic-selector');

var DQ = utils.DescendantQualifier;

function SplitPathSelector(components) {
    this.qualifier = components.qualifier;
    this.patterns = [];

        var parts = utils.split(components.base);

        parts.forEach(function(p) {
        this.patterns.push(regex(p)); 
    }.bind(this));

        TopicSelector.call(
        this, 
        components.type, 
        components.prefix, 
        components.expression);
}

inherits(SplitPathSelector, TopicSelector);

SplitPathSelector.prototype.doSelects = function(topicPath) {
    var length = this.patterns.length,
        parts = topicPath.split(utils.PATH_SEPARATOR);

        switch (this.qualifier) {
    case DQ.MATCH :
        if (parts.length !== length) {
            return false;
        }
        break;
    case DQ.DESCENDANTS_OF_MATCH :
        if (parts.length <= length) {
            return false;
        }
        break;
    case DQ.MATCH_AND_DESCENDANTS :
        if (parts.length < length) {
            return false;
        }
        break;
    }

       var i = 0;
    for (; i < length; ++i) {
        if (!this.patterns[i](parts[i])) { 
            return false;
        } 
    }

        return true;
};

module.exports = SplitPathSelector;

},{"../../selectors/topic-selector":295,"inherits":9,"topics/topic-path-utils":266,"util/regex":284}],266:[function(require,module,exports){
var PATH_SEPARATOR = '/';

function split(path) {
    return path.split(PATH_SEPARATOR);
}

function canonicalise(path) {
    if (!path) {
        return "";
    }

        var l = path.length,
        s = l > 0 && path.charAt(0) === PATH_SEPARATOR ? 1 : 0,
        e = l > 1 && path.charAt(l - 1) === PATH_SEPARATOR ? 1 : 0;

           return path.substring(s, l - e);
}

function DQ(descendants) {
    this.includesDescendants = descendants;
}

var qualifiers = {
    MATCH : new DQ(false),
    DESCENDANTS_OF_MATCH : new DQ(true),
    MATCH_AND_DESCENDANTS : new DQ(true)
};

module.exports = {
    split : split,
    canonicalise : canonicalise,
    PATH_SEPARATOR : PATH_SEPARATOR,
    DescendantQualifier : qualifiers
};
},{}],267:[function(require,module,exports){
var split = require('util/string').split;
var cache = require('util/memoize'); 
var arr = require('util/array');

var TopicSelector = require('../../selectors/topic-selector');

var utils = require('topics/topic-path-utils');
var PathSelector = require('topics/path-selector');
var SplitPathSelector = require('topics/split-path-selector');
var FullPathSelector = require('topics/full-path-selector');
var SelectorSet = require('topics/selector-set');

var T = TopicSelector.Type,
    P = TopicSelector.Prefix;

var DQ = utils.DescendantQualifier;

function parse(expression) {
    if (!expression) {
        throw new Error("Empty topic selector expression");
    }

       if (arguments.length > 1) {
        expression = arr.argumentsToArray(arguments);
    }

    if (expression instanceof Array) {
        var actual = [];

        expression.forEach(function(s) {
           if (s instanceof TopicSelector) {
               actual.push(s);
           } else {
               actual.push(parse(s));
           }
        });

                return new SelectorSet(actual);
    } 

        if (typeof expression === "string") {
        var components = getComponents(expression);

                if (isTopicPath(components)) {
            return new PathSelector(components);
        }

                switch (components.type) {
        case T.SPLIT_PATH_PATTERN :
            return new SplitPathSelector(components);
        case T.FULL_PATH_PATTERN :
            return new FullPathSelector(components);
        case T.SELECTOR_SET :   
            var parts = split(components.remainder, SelectorSet.DELIMITER);
            var selectors = [];

                        parts.forEach(function(e) {
                selectors.push(parse(e)); 
            });    

                        return new SelectorSet(selectors);
        }
    }

       throw new Error("Topic selector expression must be a string or array");
} 

function getComponents(expression) {
    var type = getType(expression);

    if (type === null) {
        expression = P.PATH + expression;
        type = T.PATH;
    }

        var remainder = expression.substring(1);
    var qualifier, prefix, base;

        if (type === T.PATH) {
        base = remainder;
        qualifier = DQ.MATCH;
    } else if (remainder[remainder.length - 1] === '/') {
        if (remainder[remainder.length - 2] === '/') {
            qualifier = DQ.MATCH_AND_DESCENDANTS;
            base = utils.canonicalise(remainder.substring(0, remainder.length - 2));
        } else {
            qualifier = DQ.DESCENDANTS_OF_MATCH;
            base = utils.canonicalise(remainder.substring(0, remainder.length - 1));
        }
    } else {
        base = utils.canonicalise(remainder);
        qualifier = DQ.MATCH;
    }

        if (type === T.PATH) {
        prefix = utils.canonicalise(remainder);
    } else {
        prefix = extractPrefixFromRegex(base);
    }

        return {
        type : type,
        base : base,
        prefix : prefix,
        remainder : remainder,
        qualifier : qualifier,
        expression : type + remainder
    };
}

function isTopicPath(c) {
    if (c.type === T.PATH) {
        return true;
    }

        if (c.type === T.SELECTOR_SET || c.prefix === "") {
        return false;
    }

        return c.prefix === c.base;
}

function isAlphaNumeric(c) {
    return !isNaN(parseInt(c, 10)) || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

function getType(expression) {
    switch (expression[0]) {
    case P.PATH:
        return T.PATH;
    case P.SPLIT_PATH_PATTERN : 
        return T.SPLIT_PATH_PATTERN;
    case P.FULL_PATH_PATTERN :
        return T.FULL_PATH_PATTERN;
    case P.SELECTOR_SET :
        return T.SELECTOR_SET;
    case '$':
    case '%':
    case '&':
    case '<':
        throw new Error("Invalid expression type: " + expression);
    default:
        return null;
    }
}

var metaChars = ['*', '.', '+', '?', '^', '$', '{', '}', '(', ')', '[', ']', '\\'];

var normal = function(c) {
    return metaChars.indexOf(c) === -1;
};

var quoted = function(c) {
    return true;
};

function extractPrefixFromRegex(remainder) {
    var buffer = [], composite = [];
    var validator = normal;

        for (var i = 0, l = remainder.length; i < l; ++i) {
        var char = remainder[i];

        if (char === '\\' && i < l - 1) {
            var next = remainder[i + 1];

                        if (validator === normal && next === 'Q') {
                validator = quoted;
            } else if (validator === quoted && next === 'E') {
                validator = normal;
            } else if ((next < 'a' || next > 'z') && (next < 'A' || next > 'Z')) {
                buffer.push(next);
            } else {
                buffer.length = 0;
                break;
            }

                        i++;
        } else if (validator(char)) {
            if (char === utils.PATH_SEPARATOR) {
                composite.push(buffer.join(''));
                buffer.length = 0;
            }

                        buffer.push(char);
        } else {
            buffer.length = 0;
            break;
        }
    }

        composite.push(buffer.join(''));
    return utils.canonicalise(composite.join(''));
}

module.exports = cache(parse);

},{"../../selectors/topic-selector":295,"topics/full-path-selector":262,"topics/path-selector":263,"topics/selector-set":264,"topics/split-path-selector":265,"topics/topic-path-utils":266,"util/array":276,"util/memoize":282,"util/string":286}],268:[function(require,module,exports){
var Codec = require('io/codec');
var parser = require('./topic-selector-parser');

var topicSelectorSerialiser = {
    read : function(input) {
        return parser(Codec.readString(input));
    },
    write : function(out, selector) {
        Codec.writeString(out, selector.expression);
    }
};

module.exports = topicSelectorSerialiser;
},{"./topic-selector-parser":267,"io/codec":125}],269:[function(require,module,exports){

var connectionResponseDeserialiser = require('protocol/connection-response-deserialiser');
var logger = require('util/logger').create('Parsing');

function parseResponse(onHandshakeSuccess, onHandshakeError, message) {
    logger.trace('Received connection handshake response');
    var response;

    try {
        response = connectionResponseDeserialiser(message);
    } catch (e) {
        onHandshakeError(e);
        return null;
    }

    return onHandshakeSuccess(response);
}

module.exports = {
    connectionResponse : parseResponse
};

},{"protocol/connection-response-deserialiser":136,"util/logger":280}],270:[function(require,module,exports){

var NodeWebSocket = require('ws');

var log = require('util/logger').create('Transports');


function websocketSubTransportFactoryProvider() {
    if (typeof window !== 'undefined' && window.WebSocket) {
        log.debug('Using native websocket');
        return {
            constructor : window.WebSocket,
            enabled : true
        };
    } else if (typeof NodeWebSocket === 'function') {
        log.debug('Using Node WS library');
        return {
            constructor : NodeWebSocket,
            enabled : true
        };
    } else {
        log.debug('Websocket transport not available');
        return {
            enabled : false
        };
    }
}

function xhrSubTransportFactoryProvider() {
    if (typeof window !== 'undefined' && window.XMLHttpRequest) {
        log.debug('Using native XHR');
        return {
            constructor : window.XMLHttpRequest,
            enabled : true
        };
    } else {
        log.debug('XHR transport not available');
        return {
            enabled : false
        };
    }
}

module.exports = {
    WS : websocketSubTransportFactoryProvider(),
    XHR : xhrSubTransportFactoryProvider(),
    WEBSOCKET : websocketSubTransportFactoryProvider(),
    HTTP_POLLING : xhrSubTransportFactoryProvider()
};

},{"util/logger":280,"ws":1}],271:[function(require,module,exports){

var WSTransport = require('transports/ws');
var XHRTransport = require('transports/xhr');
var encodeAsString = require('v4-stack/credential-tunnel').encodeAsString;
var Emitter = require('events/emitter');
var Queue = require('util/queue');
var ResponseCode = require('protocol/response-code');
var BufferInputStream = require('io/buffer-input-stream');
var BufferOutputStream = require('io/buffer-output-stream');
var standardSubtransports = require('transports/subtransports');
var parsing = require('transports/parsing');
var logger = require('util/logger').create('Cascading');

var knownTransports = {
    WS : WSTransport,
    XHR : XHRTransport,
    WEBSOCKET : WSTransport,
    HTTP_POLLING : XHRTransport
};

function filterTransports(requestedTransports, subtransports) {
    return requestedTransports.filter(function(name) {
        return name && knownTransports.hasOwnProperty(name);
    }).filter(function(name) {
        return subtransports[name].enabled;
    });
}

function isCascadableResponse(response) {
    return response === ResponseCode.ERROR;
}

function createTransport(subtransports, options, name) {
    var transport = knownTransports[name];
    var subtransport = subtransports[name];
    if (transport && subtransport && subtransport.enabled) {
        return new transport(options, subtransport.constructor);
    } else {
        throw new Error('Unknown transport name: ' + name);
    }
}

function CascadeDriver(subtransports, opts, transports, cascadeNotifications) {
    var emitter = Emitter.assign(this);
    var self = this;

    this.name = null;
    var req = null;
    var onHandshakeSuccess = null;
    var onHandshakeError = null;

    var responseReceived = false;
    var transportFactory = createTransport.bind(null, subtransports, opts);
    var transport = selectTransport();

    function onData(data) {
        emitter.emit('data', data);
    }

    function onClose(reason) {
        if (!responseReceived && cascade(true)) {
            return;
        }

        emitter.emit('close', reason);
    }

    function onError(error) {
        emitter.emit('error', error);
    }

    function onConnectionResponse(response) {
        responseReceived = true;
        if (isCascadableResponse(response.response)) {
            if (cascade()) {
                return;
            }
        }
        return onHandshakeSuccess(response);
    }

    function onParsingError(error) {
        responseReceived = true;
        if (!cascade()) {
            return onHandshakeError(error);
        }
    }

    function closeQuietly() {
        if (transport) {
            transport.off('data', onData);
            transport.off('close', onClose);
            transport.off('error', onError);
            transport.close();
            transport = null;
        }
    }

    function selectTransport(suppressClose) {
        if (transports.length === 0) {
            self.name = null;
            transport = null;
            logger.debug('Transports exhausted');
            cascadeNotifications.emit('transports-exhausted');
            if (!suppressClose) {
                emitter.emit('close');
            }
            return null;
        }

        self.name = transports.shift();
        transport = transportFactory(self.name);

        logger.debug('Selecting transport', self.name);
        cascadeNotifications.emit('transport-selected', self.name);
        return transport;
    }

    function internalConnect() {
        responseReceived = false;
        logger.debug('Attempting to connect');
        cascadeNotifications.emit('cascading-connect');

        transport.on('data', onData);
        transport.on('close', onClose);
        transport.on('error', onError);

        try {
            transport.connect(
                req,
                parsing.connectionResponse.bind(null, onConnectionResponse, onParsingError)
            );
        }
        catch(e) {
            if (!cascade()) {
                throw e;
            }
        }

        return true;
    }

    function cascade(suppressClose) {
        closeQuietly();

        if (!selectTransport(suppressClose)) {
            return false;
        }

        cascadeNotifications.emit('cascade');

        return internalConnect();
    }

    this.connect = function connect(request, handshake, handshakeError) {
        req = request;

        onHandshakeSuccess = handshake;
        onHandshakeError = handshakeError;

        internalConnect();
    };

    this.dispatch = function dispatch(message) {
        if (transport === null) {
            throw new Error('Unable to send message when no transport is set');
        }

        transport.dispatch(message);
    };

    this.close = function close() {
        if (transport !== null) {
            responseReceived = true; 
            transport.close();
            transport = null;
        }
    };
}

function Transports(subtransports) {
    var emitter = Emitter.assign(this);

    this.get = function get(options) {
        var validTransports = filterTransports(options.transports, subtransports);

        if (validTransports.length === 0) {
            return null;
        }

        return new CascadeDriver(subtransports, options, validTransports, emitter);
    };
}

Transports.create = function(subtransports) {
    if (subtransports) {
        return new Transports(subtransports);
    } else {
        return new Transports(standardSubtransports);
    }
};

module.exports = Transports;

},{"events/emitter":103,"io/buffer-input-stream":123,"io/buffer-output-stream":124,"protocol/response-code":138,"transports/parsing":269,"transports/subtransports":270,"transports/ws":272,"transports/xhr":273,"util/logger":280,"util/queue":283,"v4-stack/credential-tunnel":291}],272:[function(require,module,exports){
(function (Buffer){
var encodeAsString = require('v4-stack/credential-tunnel').encodeAsString;
var Emitter = require('events/emitter');

var log = require('util/logger').create('Websocket transport');
var curry = require('util/logger').curry;

function constructURI(req, opts) {
    var scheme = opts.secure ? 'wss' : 'ws';
    var uri =  scheme + "://" + opts.host + ":" + opts.port + opts.path;

    uri += '?ty=' + req.type;
    uri += '&v=' + req.version;
    uri += '&ca=' + req.capabilities;
    uri += '&r=' + opts.reconnect.timeout;

    if (req.token) {
        uri += '&c=' + encodeURIComponent(req.token);
        uri += "&cs=" + req.availableClientSequence;
        uri += "&ss=" + req.lastServerSequence;
    }

    if (opts.principal) {
        uri += "&username=" + encodeURIComponent(opts.principal);
        uri += "&password=" + encodeURIComponent(encodeAsString(opts.credentials));
    }

    return uri;
}

function WSTransport(opts, constructor) {
    var emitter = Emitter.assign(this);

    var handler = function(message) {
        emitter.emit('data', new Buffer(new Uint8Array(message.data)));  
    };

    var socket;

    this.connect = function connect(req, handshake) {
        try {
            var uri = constructURI(req, opts);
            socket = new constructor(uri);

            log.debug("Created websocket", uri);
        } catch (error) {
            throw new Error('Unable to construct WebSocket', error);
        }

                socket.binaryType = "arraybuffer";

        socket.onmessage = function(msg) {
            socket.onmessage = handler;

            handshake(new Buffer(new Uint8Array(msg.data)));
        };

        socket.onclose = emitter.close;
        socket.onerror = emitter.error;
    };

    this.dispatch = function dispatch(message) {
        log.trace("Sending websocket message");

        try {
            socket.send(message);
            return true;
        } catch (err) {
            log.error("Websocket send error", err);
            return false;
        }
    };

    this.close = function() {
        log.debug("Closing websocket"); 
        socket.close();
    };
}

module.exports = WSTransport;

}).call(this,require("buffer").Buffer)
},{"buffer":2,"events/emitter":103,"util/logger":280,"v4-stack/credential-tunnel":291}],273:[function(require,module,exports){
(function (Buffer){
var encodeAsString = require('v4-stack/credential-tunnel').encodeAsString;
var Emitter = require('events/emitter');
var Queue = require('util/queue');
var ResponseCode = require('protocol/response-code');
var BufferInputStream = require('io/buffer-input-stream');
var BufferOutputStream = require('io/buffer-output-stream');

var log = require('util/logger').create('XHR transport');

function constructURI(req, opts) {
    var scheme = opts.secure ? 'https' : 'http';
    var uri =  scheme + "://" + opts.host + ":" + opts.port + opts.path;

    var headers = {
        m : "0",
        ty : "B",
        ca : req.capabilities,
        r : opts.reconnect.timeout,
        v : req.version
    };

    if (req.token) {
        headers.c = encodeURIComponent(req.token);
        headers.cs = req.availableClientSequence;
        headers.ss =req.lastServerSequence;
    }

    if (opts.principal) {
        headers.username = encodeURIComponent(opts.principal);
        headers.password = encodeURIComponent(encodeAsString(opts.credentials));
    }

    return { 'uri' : uri , 'headers' : headers };
}

function XHRTransport(opts, xhr) {
    var emitter = Emitter.assign(this);

    var token;
    var pollSequence = 0;
    var pingSequence = 0;
    var URI;
    var pollRequest = null;
    var aborted = false;
    var isSending = false;
    var protocolVersion = null;

    var self = this;

    var queue = Queue.create();
    var constructor = xhr;

    this.connect = function connect(req, handshake) {
        try {
            var url = constructURI(req, opts);
            protocolVersion = url.headers.v;
            URI = url.uri;
            var request = this.createRequest(url.headers, url.uri);
            log.debug('Created XHR', url.uri);

            request.onreadystatechange = function() {
                if (request.readyState === 4 ) {
                    if (request.status === 200) {
                        var handShakeData = request.responseText;
                        var serverResponse = handshake(new Buffer(handShakeData, 'binary'));

                        if (!serverResponse) {
                            return;
                        }

                        token = serverResponse.token;

                        var responseCode = serverResponse.response;
                        var isSuccesss = ResponseCode.isSuccess(responseCode);

                        if (isSuccesss) {
                            self.poll();
                        } else {
                            log.debug(responseCode.id + ' - '  + responseCode.message);
                            emitter.close();
                        }
                    } else {
                        emitter.close();
                    }
                }
            };

            request.send(null);

        } catch (error) {
            throw new Error('Unable to create polling transport', error);
        }
    };

    this.close = function() {
        log.trace("Closing XHR transport");

        if (pollRequest) {
            aborted = true;
            pollRequest.abort();
        }

        queue = Queue.create();

        emitter.close();
    };

    this.createRequest = function createRequest(headers, uri) {
        var request = new constructor();

        request.open("POST", uri, true);
        request.overrideMimeType('text\/plain; charset=x-user-defined');

        for (var header in headers) {
            try {
                request.setRequestHeader(header,headers[header]);
            } catch(e) {
                log.warn("Can't set header " + header + ":" + headers.join(":"));
            }
        }

        return request;
    };

    this.poll = function poll() {
        var request = this.createRequest(
            { "m" : "1", "c" : token, "s" : pollSequence++, "v" : protocolVersion },
            URI);

        request.onreadystatechange = function() {
            if (request.readyState === 4) {
                if (request.getResponseHeader('diffusion-connection') === 'reconnect') {
                    emitter.close();
                }
                else if (request.status === 200) {
                    var data = request.responseText;

                    var bis = new BufferInputStream(new Buffer(data, 'base64'));

                    while (bis.pos !== bis.count) {
                        var type = bis.read();

                        var length = bis.readInt32();

                        var body = bis.readMany(length);
                        var payload = new Buffer(body.length + 1); 

                        payload.writeInt8(type);
                        body.copy(payload, 1);

                        emitter.emit('data', payload);
                    }
                    self.poll();
                } else {
                    emitter.close();
                }
            }
        };

        pollRequest = request;
        request.send(null);
    };

    this.dispatch = function dispatch(message) {
        if (aborted) {
            return false;
        }

        queue.add(message);

        if (isSending) {
            return true;
        }

        self.flush();
    };

    this.flush = function flush() {
        if (queue.isEmpty()) {
            return;
        }

        var buffer = drainToBuffer(queue.drain());

        var request = this.createRequest(
                            { "m" : "2", "c" : token, "s" : pingSequence++, "v" : protocolVersion },
                            URI);

        request.onreadystatechange = function() {
            if (request.readyState === 4) {
                if (request.getResponseHeader('diffusion-connection') === 'reconnect') {
                    emitter.close();
                }
                else if (request.status === 200) {
                    isSending = false;
                    self.flush();
                } else {
                    emitter.close();
                }
            }
        };

         isSending = true;
        request.send(buffer);
    };
}

function drainToBuffer (messages) {
    var messagesBOS = new BufferOutputStream();

    for (var i = 0; i < messages.length; i++) {
        var msg = messages[i];

        messagesBOS.writeInt32(msg.length);
        messagesBOS.writeMany(msg, 0, msg.length);
    }

    return messagesBOS.getBuffer();
}

module.exports = XHRTransport;

}).call(this,require("buffer").Buffer)
},{"buffer":2,"events/emitter":103,"io/buffer-input-stream":123,"io/buffer-output-stream":124,"protocol/response-code":138,"util/logger":280,"util/queue":283,"v4-stack/credential-tunnel":291}],274:[function(require,module,exports){
var Update = require('./update');
var BEES = require('serialisers/byte-encoded-enum-serialiser');
var ContentSerialiser = require('../content/serialiser');

module.exports = {
    read : function(input) {
        var type = BEES.read(Update.Type, input);
        switch (type) {
        case Update.Type.CONTENT :
            var data = ContentSerialiser.read(input);
            return new Update(data);
        default:
            throw new Error('Received unimplemented update type ' + type);
        }
    },

        write : function(output, update) {
        output.write(update.type);
        switch (update.type) {
        case Update.Type.CONTENT:
            output.write(output, update.action);
            ContentSerialiser.write(output, update.data);
            break;
        default:
            throw new Error('Trying to write unimplemented update type ' + update.type);
        }            
    }
};

},{"../content/serialiser":75,"./update":275,"serialisers/byte-encoded-enum-serialiser":151}],275:[function(require,module,exports){
var UpdateType = {
    CONTENT : 0,
    PAGED_RECORD_ORDERED : 1,
    PAGED_STRING_ORDERED : 2,
    PAGED_RECORD_UNORDERED : 3,
    PAGED_STRING_UNORDERED : 4
};

var UpdateAction = {
    UPDATE : 0,
    REPLACE : 1
};

var Update = function(data, type, action) {
    this.data = data;
    this.type =  type !== undefined ? type : UpdateType.CONTENT;
    this.action = action !== undefined ? action : UpdateAction.UPDATE;
};

module.exports = {
    Type : UpdateType,
    Update : Update
};

},{}],276:[function(require,module,exports){
function remove(arr, e) {
    var i = arr.indexOf(e);
    if (i > -1) {
        arr.splice(i, 1);
        return true;
    }

        return false;
}

function fill(array, value, start, end) {
    start = start || 0;
    end = end || array.length;

    for (var i = start; i < end; ++i) {
        array[i] = value;
    }
}

function ofSize(size, initialValue) {
    var a = [];

    a.length = size;

    if (initialValue !== undefined) {
        fill(a, initialValue);
    }

    return a;
}

var s = Array.prototype.slice;

function argumentsToArray(args) {
    return s.call(args, 0);
}

module.exports = {
    fill : fill,
    ofSize : ofSize,
    remove : remove,
    argumentsToArray : argumentsToArray
};
},{}],277:[function(require,module,exports){
var Emitter = require('events/emitter');

function FSM(initial, states) {
    var emitter = Emitter.assign(this);
    var current = states[initial];

    this.state = initial;

    this.change = function change(state) {
        if (this.state === state) {
            return true;
        }

        if (states[state] && (current === '*' || current.indexOf(state) > -1)) {
            emitter.emit('change', this.state, state);
            current = states[state];

                        this.state = state;
            return true;
        }

        return false;
    };
}

module.exports = {
    create : function create(initial, states) {
        return new FSM(initial, states);
    }
};

},{"events/emitter":103}],278:[function(require,module,exports){
var a2a = require('util/array').argumentsToArray;

function curry() {
    var args = a2a(arguments); 
    var fn = args.shift();

    return function() {
        return callWithArguments(fn, args.concat(a2a(arguments)));
    };
}

function curryR() {
    var args = a2a(arguments);
    var fn = args.shift();

    return function() {
        return callWithArguments(fn, a2a(arguments).concat(args));
    };
}

function callWithArguments(f, args, count) {
    count = count === undefined ? args.length : count;

    switch (count) {
    case 0 :
        return f();
    case 1 :
        return f(args[0]);
    case 2 :
        return f(args[0], args[1]);
    case 3 :
        return f(args[0], args[1], args[2]);
    case 4 :
        return f(args[0], args[1], args[2], args[3]);
    case 5 :
        return f(args[0], args[1], args[2], args[3], args[4]);
    case 6 :
        return f(args[0], args[1], args[2], args[3], args[4], args[5]);
    case 7 :
        return f(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
    case 8 :
        return f(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
    }

        return f.apply(f, args);
}

function newWithArguments(f, args, count) {
    count = count === undefined ? args.length : count;

    switch (count) {
    case 0 :
        return new f();
    case 1 :
        return new f(args[0]);
    case 2 :
        return new f(args[0], args[1]);
    case 3 :
        return new f(args[0], args[1], args[2]);
    case 4 :
        return new f(args[0], args[1], args[2], args[3]);
    case 5 :
        return new f(args[0], args[1], args[2], args[3], args[4]);
    case 6 :
        return new f(args[0], args[1], args[2], args[3], args[4], args[5]);
    case 7 :
        return new f(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
    case 8 :
        return new f(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
    }

        function Proxy() {
        f.apply(this, args);
    }

        Proxy.prototype = f.prototype;

        return new Proxy();
}

module.exports = {
    curry : curry,
    curryR : curryR,
    callWithArguments : callWithArguments,
    newWithArguments : newWithArguments
};

},{"util/array":276}],279:[function(require,module,exports){
var functions = require('util/function');
var array = require('util/array');


function interface() {
    var args = array.argumentsToArray(arguments);

    var name = args.shift();
    var members = args.pop() || [];

        args.forEach(function(ex) {
        members = members.concat(ex.members);
    });

        var boundMembers = members.map(function(member) {
        return {
            name : member,
            check : function(impl) {
                return impl[member] === undefined;
            },
            throw : function() {
                throw new Error(name + '#' + member + '" must be implemented');
            }
        };
    });

        var filter = function(implementation) {
        return boundMembers.filter(function(member) {
            return member.check(implementation);
        });
    };

        filter.toString = function() {
        return name;
    };

        filter.members = members;
    return filter;
}

function _implements() {
    var args = Array.prototype.slice.call(arguments, 0);
    var impl = args.pop();

        var unsatisfied = [];
    var constructor;

        if (impl instanceof Function) {
        constructor = impl;
        impl = constructor.prototype;
    } else {
        constructor = impl.__constructor || function() { };

        for (var m in constructor.prototype) {
            impl[m] = constructor.prototype[m]; 
        }
    }

    args.forEach(function(interface) {
        unsatisfied = unsatisfied.concat(interface(impl));
    });

    constructor.prototype = impl;

    if (unsatisfied.length === 0) {
        return constructor;
    } else {
        var proxy = function() {
            var instance = functions.newWithArguments(constructor, arguments);

                        unsatisfied.forEach(function(m) {
                if (m.check(instance)) {
                    m.throw();
                }
            });

                                    return instance;
        };

        proxy.toString = function() {
            return constructor.toString();
        };

        proxy.isPrototypeOf = function(c) {
            return c instanceof constructor;
        };

        proxy.__constructor = constructor;

                return proxy;
    }
}

module.exports = {
    interface : interface, 
    _implements : _implements
};

},{"util/array":276,"util/function":278}],280:[function(require,module,exports){
var logger = require('loglevel');

var methods = ['trace', 'debug', 'info', 'warn', 'error'];

function d() {
    return new Date();
}

function log(level, prefix) {
    var c = "|" + level.toUpperCase() + "|" + prefix + "|";

        return function(msg, arg) {
        if (arg) {
            logger[level](d() + c + msg, arg); 
        } else {
            logger[level](d() + c + msg);
        }
    };
}

function create(classname) {    
    var l = {};

        methods.forEach(function(m) {
        l[m] = log(m, classname);
    });

            return l;
}

module.exports = {
    create : create,
    setLevel : logger.setLevel
};

},{"loglevel":10}],281:[function(require,module,exports){
function leadingZeros(i) {
    if (i === 0) {
        return 32;
    }

    var n = 1;

    if (i >>> 16 === 0) { n += 16; i <<= 16; }
    if (i >>> 24 === 0) { n +=  8; i <<=  8; }
    if (i >>> 28 === 0) { n +=  4; i <<=  4; }
    if (i >>> 30 === 0) { n +=  2; i <<=  2; }

    n -= i >>> 31;

    return n;
}

module.exports.approximateSquareRoot = function(value) {
    if (value < 0) {
        throw new Error("Value must be greater or equal to 0");
    }

    if (value === 0) {
        return 0;
    }

    var result = 1;

    for (var i = value; i > 0; i >>= 2) {
        result *= 2;
    }

    return result;
};

module.exports.approximateCubeRoot = function(value) {
    if (value < 0) {
        throw new Error("Value must be great or equal to 0");
    }

    if (value === 0) {
        return 0;
    }

    var h = 32 - leadingZeros(value);

    return 1 << h / 3;
};

module.exports.findNextPowerOfTwo = function(value) {
    if (value < 0 || value > 1 << 30) {
        throw new Error("Illegal argument: " + value);
    }


    value--;
    value |= value >> 1;
    value |= value >> 2;
    value |= value >> 4;
    value |= value >> 8;
    value |= value >> 16;
    value++;

    return value;
};
},{}],282:[function(require,module,exports){
var a2a = require('./array').argumentsToArray;

function defaultCacheKey(args) {
    return args;
}

function memoize(f, cache, matcher) {
    matcher = matcher || defaultCacheKey;
    cache = cache || {};

    var c = function() {
        var args = a2a(arguments);
        var a = matcher(args);
        var r = cache[a];

                if (r) {
            return r;
        } else {
            r = f.apply(f, args);
        }

                cache[a] = r;
        return r;
    };

    c._uncached = f;
    return c;
}

module.exports = memoize;

},{"./array":276}],283:[function(require,module,exports){
function Queue() {
	var messages = [];

	this.length = function() {
        return messages.length;
    };

	this.add = function add(message) {
		messages.push(message);
	};

	this.drain = function drain() {
		return messages.splice(0, messages.length);
	};

	this.isEmpty =  function isEmpty() {
        return messages.length === 0;
	};

}

module.exports = {
	create : function create() {
        return new Queue();
    }
};
},{}],284:[function(require,module,exports){
function regex(s, context) {
    context = context || "";

        if (s === "") {
        throw new Error("Empty regular expression [" + context + "]");
    }
    try {
        var r = new RegExp(s);

                return function(test) {
            var m = r.exec(test);
            return m && m[0] === test;
        };
    } catch (e) {
        throw new Error("Bad regular expression [" + e.message + ", " + context + "]");
    }
}

module.exports = regex;
},{}],285:[function(require,module,exports){
function requireNonNull(value, what) {
    if (typeof value === 'undefined' || value === null) {
        throw new Error(what + " is null");
    }

        return value;
}

module.exports = requireNonNull;
},{}],286:[function(require,module,exports){
function split(str, delim) {
    if (str === "") {
        return [];
    }

        var parts = [], l = delim.length, i = str.lastIndexOf(delim);

        while (i > -1) {
        parts.push(str.substring(i + l, str.length));
        str = str.substring(0, i);
        i = str.lastIndexOf(delim);
    }

        parts.push(str);

        return parts.reverse();
}

module.exports = {
    split : split
};
},{}],287:[function(require,module,exports){

function AliasMap() {
    var aliases = {};

    this.establish  = function establish(topic) {
        var bang = topic.indexOf('!');

        if (bang !== -1) {
            if (bang === 0) {
                return aliases[topic.substring(1)];
            } else {
                var alias = topic.substring(bang + 1);
                topic = topic.substring(0, bang);

                                aliases[alias] = topic;

                return topic;
            }
        } else {
            return topic;
        }
    };
}

AliasMap.create = function() {
    return new AliasMap();
};

module.exports = AliasMap;

},{}],288:[function(require,module,exports){
function CloseReason(id, message, canReconnect) {
    this.id = id;
    this.message = message;
    this.canReconnect = canReconnect;
}

CloseReason.CLOSED_BY_CLIENT = new CloseReason(0, "The session was closed by the client", false);
CloseReason.CLOSED_BY_SERVER = new CloseReason(1, "The session was closed by the server", false);
CloseReason.RECONNECT_ABORTED = new CloseReason(2, "Client aborted a reconnect attempt", false);
CloseReason.CONNECTION_TIMEOUT = new CloseReason(3, "The connection attempt timed out", false);
CloseReason.HANDSHAKE_REJECTED = new CloseReason(4, "The connection handshake was rejected by the server", false);
CloseReason.HANDSHAKE_ERROR = new CloseReason(5, "There was an error parsing the handshake response", false);
CloseReason.TRANSPORT_ERROR = new CloseReason(6, "There was an unexpected error with the connection", true);
CloseReason.CONNECTION_ERROR = new CloseReason(7, "A connection to the server was unable to be established", true);
CloseReason.IDLE_CONNECTION = new CloseReason(8, "The activity monitor detected the connection was idle", true);
CloseReason.LOST_MESSAGES = new CloseReason(16, "Loss of messages has been detected", false);

module.exports = CloseReason;

},{}],289:[function(require,module,exports){
var Connection = require('v4-stack/connection');

module.exports.create = function(aliases, transports) {
    return new Connection(aliases, transports);
};

},{"v4-stack/connection":290}],290:[function(require,module,exports){
(function (global){
var RecoveryBuffer = require('message-queue/recovery-buffer');
var BufferOutputStream = require('io/buffer-output-stream');
var ResponseCode = require('protocol/response-code');
var CloseReason = require('v4-stack/close-reason');
var Message = require('v4-stack/message');
var Emitter = require('events/emitter');

var logger = require('util/logger').create('Connection');
var curry = require('util/function').curry;
var FSM = require('util/fsm');

var CLOSE_TIMEOUT = global.DIFFUSION_CLOSE_TIMEOUT || 1000;

var CONNECT_TIMEOUT = global.DIFFUSION_CONNECT_TIMEOUT || 10000;

var RECOVERY_BUFFER_INDEX_SIZE = global.DIFFUSION_RECOVERY_BUFFER_INDEX_SIZE || 8;

function Connection(aliases, transports, reconnectTimeout, recoveryBufferSize) {
    var emitter = Emitter.assign(this);

    var fsm = FSM.create('disconnected', {
        connecting      : ['connected', 'disconnected', 'closed'],
        connected       : ['disconnecting', 'disconnected', 'closed'],
        disconnecting   : ['disconnected'],
        disconnected    : ['connecting', 'closed'],
        closed          : []
    });

    var lastSentSequence = 0;
    this.lastReceivedSequence = 0;

    var recoveryBuffer = new RecoveryBuffer(recoveryBufferSize, RECOVERY_BUFFER_INDEX_SIZE);

    var scheduledRecoveryBufferTrim;
    var scheduledClose;
    var closeReason;

    var transport = null;
    var self = this;

    fsm.on('change', function(previous, current) {
        logger.debug('State changed: ' + previous + ' -> ' + current);
    });

    function onData(data) {
        logger.trace('Received message from transport', data);

        try {
            var message = Message.parse(data);

                        if (message.topic) {
                message.topic = aliases.establish(message.topic);
            }

            self.lastReceivedSequence++;

            emitter.emit('data', message);
        } catch (e) {
            logger.warn('Unable to parse message', e);

            if (fsm.change('closed')) {
                closeReason = CloseReason.CONNECTION_ERROR;
                transport.close();
            }
        }
    }

    function onClose(reason) {
        if (fsm.change('disconnected') || fsm.change('closed')) {
            clearInterval(scheduledRecoveryBufferTrim);
            clearTimeout(scheduledClose);

            logger.trace('Transport closed: ', closeReason);

            if (fsm.state === 'disconnected') {
                emitter.emit('disconnect', closeReason);
            } else {
                emitter.close(closeReason);
            }
        } else {
            logger.debug('Unable to disconnect, current state: ', fsm.state);
        }
    }

    function onHandshakeSuccess(response) {
        if (response.response === ResponseCode.RECONNECTED) {
            var messageDelta = lastSentSequence - response.sequence + 1;

            if (recoveryBuffer.recover(messageDelta, dispatch)) {
                recoveryBuffer.clear();
                lastSentSequence = response.sequence - 1;
            } else {
                var outboundLoss = lastSentSequence - recoveryBuffer.size() - response.sequence + 1;

                logger.warn("Unable to reconnect due to lost messages (" + outboundLoss + ")");

                if (fsm.change('disconnected')) {
                    closeReason = CloseReason.LOST_MESSAGES;
                    transport.close();
                }

                return response;
            }
        }

        if (response.success && fsm.change('connected')) {
            logger.trace('Connection response: ', response.response);
            closeReason = CloseReason.TRANSPORT_ERROR;
            emitter.emit('connect', response);
        } else {
            logger.debug('Connection response: ', response.response);
            closeReason = CloseReason.HANDSHAKE_REJECTED;
            transport.close();
        }

        clearTimeout(scheduledClose);
        return response;
    }

    function onHandshakeError(error) {
        closeReason = CloseReason.HANDSHAKE_ERROR;

        clearTimeout(scheduledClose);

        logger.trace('Unable to deserialise handshake response', error);

        return;
    }

    this.connect = function connect(request, opts, timeout) {
        if (fsm.state !== 'disconnected') {
            logger.warn('Cannot connect, current state: ', fsm.state);
            return;
        }

        if (fsm.change('connecting')) {
            timeout = timeout === undefined ? CONNECT_TIMEOUT : timeout;

            closeReason = CloseReason.CONNECTION_ERROR;

            transport = transports.get(opts);

            transport.on('data', onData);
            transport.on('close', onClose);

            transport.connect(request, onHandshakeSuccess, onHandshakeError);

            scheduledClose = setTimeout(function() {
                logger.debug('Timed out connection attempt after ' + timeout);

                closeReason = CloseReason.CONNECTION_TIMEOUT;
                transport.close();
            }, timeout);

            scheduledRecoveryBufferTrim = setInterval(function() {
                if (fsm.state === 'connected') {
                    recoveryBuffer.flush(Date.now() - reconnectTimeout);
                }
            }, reconnectTimeout);
        }
    };

    this.resetSequences = function() {
        recoveryBuffer.clear();

        lastSentSequence = 0;
        this.lastReceivedSequence = 0;
    };

    this.getAvailableSequence = function() {
        return lastSentSequence + 1 - recoveryBuffer.size();
    };

    function dispatch(message) {
        var bos = new BufferOutputStream();
        Message.writeToBuffer(message, bos);

        lastSentSequence += 1;

        recoveryBuffer.put(message);
        recoveryBuffer.markTime(Date.now());

        transport.dispatch(bos.getBuffer());
    }

    this.send = function(message) {
        if (fsm.state === 'connected') {
            return dispatch(message);   
        }

        return false;
    };

    this.close = function(reason) {
        closeReason = reason;

        if (fsm.state === 'disconnected') {
            onClose();
        } else if (fsm.change('disconnecting')) {
            dispatch(Message.create(Message.types.CLOSE_REQUEST));

            scheduledClose = setTimeout(transport.close, CLOSE_TIMEOUT);
        }
    };

    this.closeIdleConnection = function() {
        if (fsm.change('disconnecting')) {
            logger.debug('Connection detected as idle');
            closeReason = CloseReason.IDLE_CONNECTION;
            transport.close();
        }
    };

    this.getState = function() {
        return fsm.state;
    };
}

module.exports = Connection;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"events/emitter":103,"io/buffer-output-stream":124,"message-queue/recovery-buffer":126,"protocol/response-code":138,"util/fsm":277,"util/function":278,"util/logger":280,"v4-stack/close-reason":288,"v4-stack/message":293}],291:[function(require,module,exports){
(function (Buffer){
var BufferOutputStream = require('io/buffer-output-stream');
var BufferInputStream = require('io/buffer-input-stream');

var serialiser = require('services/authentication/credentials-serialiser');

function encodeAsString(password) {
    var bos = new BufferOutputStream();

        serialiser.write(bos, password);

        return bos.getBase64();
}

function decodeAsString(s) {
    if (s) {
        var bis = new BufferInputStream(new Buffer(s, 'base64'));
        return serialiser.read(bis);
    } else {
        return null;
    }
}

module.exports = {
    encodeAsString : encodeAsString,
    decodeAsString : decodeAsString
};
}).call(this,require("buffer").Buffer)
},{"buffer":2,"io/buffer-input-stream":123,"io/buffer-output-stream":124,"services/authentication/credentials-serialiser":155}],292:[function(require,module,exports){
var ConversationId = require('conversation/conversation-id');

module.exports = {
    cidFromHeaders : function(headers) {
        if (!headers || headers.length === 0) {
            throw new Error("Empty headers, expected at least 1");
        }

        return ConversationId.fromString(headers[headers.length - 1]);
    }
};
},{"conversation/conversation-id":84}],293:[function(require,module,exports){
(function (Buffer){
var inherits = require('inherits');
var BufferOutputStream = require('io/buffer-output-stream');
var BufferInputStream = require('io/buffer-input-stream');

var systemHeaders = {
    0 : [],
    1 : [],
    2 : [],
    20 : ['topic'],
    21 : ['topic'],
    22 : ['topic_set'],
    23 : ['topic_set'],
    24 : ['timestamp', 'queue_size'],
    25 : ['timestamp'],
    26 : ['username', 'password'],
    27 : ['username', 'password'],
    28 : [],
    29 : [],
    30 : ['topic', 'ack_id'],
    31 : ['topic', 'ack_id'],
    32 : ['ack_id'],
    33 : ['topic'],
    34 : ['topic'],
    35 : ['topic', 'topic_status'],
    36 : ['topic', 'command'],
    40 : ['topic', 'command_topic_category', 'command_topic_type'],
    41 : ['topic', 'notification_type'],
};

var types = {
    SERVICE_REQUEST : 0,
    SERVICE_RESPONSE : 1,
    SERVICE_ERROR : 2,
    TOPIC_LOAD : 20,
    DELTA : 21,
    SUBSCRIBE : 22,
    UNSUBSCRIBE : 23,
    PING_SERVER : 24,
    PING_CLIENT : 25,
    CREDENTIALS : 26,
    CREDENTIALS_REJECTED : 27,
    ABORT_NOTIFICATION : 28,
    CLOSE_REQUEST : 29,
    TOPIC_LOAD_ACK_REQUIRED : 30,
    DELTA_ACK_REQUIRED : 31,
    ACK : 32,
    FETCH : 33,
    FETCH_REPLY : 34,
    TOPIC_STATUS_NOTIFICATION : 35,
    COMMAND_MESSAGE : 36,
    COMMAND_TOPIC_LOAD : 40,
    COMMAND_TOPIC_NOTIFICATION : 41,
};

var encoding = {
    NONE : 0,
    ENCRYPTION_REQUESTED : 1,
    COMPRESSION_REQUESTED : 2,
    BASE64_REQUESTED : 3,
    ENCRYPTED : 17,
    COMPRESSED : 18,
    BASE64 : 19
};

var parse = function(buffer) {
    var bis = new BufferInputStream(buffer);

    var type = bis.read();

    if (systemHeaders[type] === undefined) {
        throw new Error('Invalid message type: ' + type);
    }

    if (type === types.SERVICE_REQUEST || type === types.SERVICE_RESPONSE || type === types.SERVICE_ERROR) {
        return parseServiceMessage(type, bis);
    }

    var headers = bis.readUntil(0x01).toString().split('\u0002');

        var body = bis.readMany(bis.count); 

        var fields = {};

    if (systemHeaders[type]) {
        systemHeaders[type].forEach(function(key) {
            fields[key] = headers.shift();
        });
    }
    return new Message(type, encoding.NONE, fields, body, headers);
};

function parseServiceMessage(type, bis) {
    var headers = [];

        var body = bis.readMany(bis.count); 

        var fields = {};

    return new Message(type, encoding.NONE, fields, body, headers);
}

var create = function(type) {
    var fields;

    if (type === undefined || (typeof type === "number" && systemHeaders[type] === undefined)) {
        throw "Invalid message type: " + type;
    } else if (type instanceof Object) {
        fields = type;
    } else {
        fields = { type : type };
    }

    fields.encoding = fields.encoding || 0;
    fields.headers = fields.headers || [];
    fields.buffer = fields.buffer || new Buffer(0);

    return new WriteableMessage(fields);
};

var getHeaderString = function(message) {
    var sh = [];
    var headers = '';

    systemHeaders[message.type].forEach(function(h) {
        sh.push(message[h]);
    });

    message.headers.forEach(function(h) {
        sh.push(h);
    });

    if (systemHeaders[message.type].length === 0) {
        return headers;
    }

    if (sh.length > 0) {
        headers =  sh.join('\u0002');
        headers += '\u0001';
    }

    return headers;
};

function writeToBuffer(message, bos) {
    bos.write(message.type);
    bos.writeString(getHeaderString(message));
    bos.writeMany(message.getBuffer());
}

function WriteableMessage(fields) {
    Message.call(this, fields.type, fields.encoding, fields, fields.buffer, fields.headers);
    BufferOutputStream.call(this, fields.buffer);
}

inherits(WriteableMessage, BufferOutputStream);

function Message(type, encoding, fields, data, headers) {
    this.type = type;
    this.encoding = encoding;

    for (var f in fields) {
        this[f] = fields[f];
    }

    this.data = data;
    this.headers = headers;
}

Message.prototype.isTopicMessage = function() {
    return this.topic !== undefined;
};

Message.prototype.isServiceMessage = function() {
     return this.type === types.SERVICE_REQUEST ||
            this.type === types.SERVICE_RESPONSE ||
            this.type === types.SERVICE_ERROR;
};

Message.prototype.getInputStream = function() {
    return new BufferInputStream(this.data);
};

module.exports = {
    types : types,
    parse : parse,
    create : create,
    encoding : encoding,
    getHeaderString : getHeaderString,
    writeToBuffer : writeToBuffer
};

}).call(this,require("buffer").Buffer)
},{"buffer":2,"inherits":9,"io/buffer-input-stream":123,"io/buffer-output-stream":124}],294:[function(require,module,exports){
var DEFAULT_HOST = 'localhost';

var DEFAULT_PORT = 80;

var DEFAULT_SECURE_PORT = 443;

var DEFAULT_SECURE = true;

var DEFAULT_PATH = '/diffusion';

if (typeof window !== 'undefined') {
    if (window.location.hostname) {
        DEFAULT_HOST = window.location.hostname;
    }

    if (window.location.protocol === "http:") {
        DEFAULT_SECURE = false;

        if (window.location.port) {
            DEFAULT_PORT = parseInt(window.location.port);
        }
    }

    if (window.location.protocol === "https:") {
        DEFAULT_SECURE = true;

        if (window.location.port) {
            DEFAULT_SECURE_PORT = parseInt(window.location.port);
        }
    }
}

var DEFAULT_RECONNECT_TIMEOUT = 60000;
var DEFAULT_RECONNECT_STRATEGY = function(start) {
    setTimeout(start, 5000);
};

var DEFAULT_ABORT_STRATEGY = function(start, abort) {
    abort();
};

var DEFAULT_PRINCIPAL = "";
var DEFAULT_PASSWORD = "";

var DEFAULT_ACTIVITY_MONITOR = true;

var DEFAULT_TRANSPORTS = ['WEBSOCKET'];


function Options(options) {
    options = options || {};

    if (options.host === undefined) {
        options.host = DEFAULT_HOST;
    } else if (options.host.indexOf(':') > -1) {
        var parts = options.host.split(':');

        if (options.port === undefined) {
            options.port = parseInt(parts[1]);
        }

        options.host = parts[0];
    }

    if (options.path === undefined) {
        options.path = DEFAULT_PATH;
    } else {
        if (options.path[0] !== '/') {
            options.path = '/' + options.path;
        }

        if (options.path.substring(options.path.length - DEFAULT_PATH.length) !== DEFAULT_PATH) {
            if (options.path[options.path.length - 1] === '/') {
                options.path = options.path.substring(0, options.path.length - 1);
            }

            options.path = options.path + DEFAULT_PATH;
        }
    }

    if (isNaN(parseInt(options.port, 10))) {
        options.port = undefined;
    } else {
        options.port = parseInt(options.port, 10);
    }

    if (options.secure === undefined) {
        if (options.port === undefined) { 
            options.secure = DEFAULT_SECURE;
        } else { 
            options.secure = options.port === DEFAULT_SECURE_PORT ? true : false;
        }
    }

    if (options.port === undefined) {
        options.port = options.secure ? DEFAULT_SECURE_PORT : DEFAULT_PORT;
    }

    this.host = options.host;
    this.port = options.port;
    this.path = options.path;
    this.secure = options.secure;

    if (options.reconnect === undefined || (typeof options.reconnect === 'boolean') && options.reconnect) {
        this.reconnect = {
            timeout : DEFAULT_RECONNECT_TIMEOUT,
            strategy : DEFAULT_RECONNECT_STRATEGY
        };
    } else if (typeof options.reconnect === 'number') {
        this.reconnect = {
            timeout : options.reconnect,
            strategy : DEFAULT_RECONNECT_STRATEGY
        };
    } else if (typeof options.reconnect === 'function') {
        this.reconnect = {
            timeout : DEFAULT_RECONNECT_TIMEOUT,
            strategy : options.reconnect
        };    
    } else if (typeof options.reconnect === 'object') {
        this.reconnect = {
            timeout : options.reconnect.timeout === undefined ? DEFAULT_RECONNECT_TIMEOUT : options.reconnect.timeout,
            strategy : options.reconnect.strategy || DEFAULT_RECONNECT_STRATEGY
        };    
    } else {
        this.reconnect = {
            timeout : 0,
            strategy : DEFAULT_ABORT_STRATEGY
        }; 
    }

    if (options.principal !== undefined) {
        this.principal = options.principal || DEFAULT_PRINCIPAL;
        this.credentials = options.credentials || DEFAULT_PASSWORD;
    }

    if (typeof options.transports === 'string') {
        this.transports = [options.transports];
    } else if (typeof options.transports === 'object' &&
            options.transports instanceof Array &&
            options.transports.length > 0) {
        this.transports = options.transports.slice();
    } else {
        this.transports = DEFAULT_TRANSPORTS.slice();
    }

    this.transports = this.transports.slice().map(function(t) {
        return t.toUpperCase();
    });

    this.activityMonitor = (options.activityMonitor !== undefined) ? options.activityMonitor : DEFAULT_ACTIVITY_MONITOR;
}

Options.prototype.with = function(options) {
    var o = {};
    var k;

    for (k in this) {
        o[k] = this[k];
    }

    for (k in options) {
        o[k] = options[k];
    }

    return new Options(o);
};

module.exports = Options;

},{}],295:[function(require,module,exports){
var util = require('topics/topic-path-utils');

function TopicSelector(type, prefix, expression) {
    this.type = type;

    this.prefix = prefix;

    this.expression = expression;
}

TopicSelector.Prefix = {
    PATH : '>',
    SPLIT_PATH_PATTERN : '?',
    FULL_PATH_PATTERN : '*',
    SELECTOR_SET : '#'
};

TopicSelector.Type = {
    PATH : TopicSelector.Prefix.PATH,
    SPLIT_PATH_PATTERN : TopicSelector.Prefix.SPLIT_PATH_PATTERN,
    FULL_PATH_PATTERN : TopicSelector.Prefix.FULL_PATH_PATTERN,
    SELECTOR_SET : TopicSelector.Prefix.SELECTOR_SET
};

TopicSelector.prototype.selects = function(topicPath) {
    var canonical = util.canonicalise(topicPath);
    return canonical.indexOf(this.prefix) === 0 && this.doSelects(canonical);
};

TopicSelector.prototype.toString = function() {
    return this.expression;
};

module.exports = TopicSelector;

},{"topics/topic-path-utils":266}],296:[function(require,module,exports){
var parser = require('topics/topic-selector-parser');

var topicSelectors = {
    parse: function (expression) {
        return parser(expression);
    }
};

module.exports = topicSelectors;

},{"topics/topic-selector-parser":267}],297:[function(require,module,exports){
var Messages = require('features/messages');
var Security = require('features/security').Security;
var TopicControl = require('features/topic-control');
var ClientControl = require('features/client-control');

var features = [
    require('features/topics')
];


function Session(internalSession, emitter, options) {
    this.sessionID = undefined;

    this.options = options;

    var self = this;

    emitter.assign(self);

    features.forEach(function(feature) {
        var instance = new feature(internalSession);
        for (var k in instance) {
            self[k] = instance[k];
        }
    });

    this.close = function() {
        internalSession.close();
        return self;
    };

    this.isConnected = function() {
        return internalSession.isConnected();
    };

    this.isClosed = function() {
        var state = internalSession.getState();
        return state === "closing" || state === "closed";
    };

    this.security = new Security(internalSession, this);

    this.topics = new TopicControl(internalSession, this);

    this.messages = new Messages(internalSession, this);

    this.clients = new ClientControl(internalSession, this);

    this.toString = function() {
        return "Session<" + self.sessionID + " " + internalSession.getState() + ">";
    };




}

module.exports = Session;

},{"features/client-control":106,"features/messages":107,"features/security":108,"features/topic-control":112,"features/topics":115}],298:[function(require,module,exports){

function type(id, stateful, functional) {
    return {
        id : id,
        stateful : stateful,
        functional : functional
    };
}

module.exports.TopicType = {
    STATELESS : type(1, false, false),

    SINGLE_VALUE : type(3, true, false),

    RECORD : type(4, true, false),

    PROTOCOL_BUFFER : type(5, true, false),
    CUSTOM : type(6, true, false),

    SLAVE : type(7, true, false),

    SERVICE : type(8, false, true),

    PAGED_STRING : type(9, false, true),

    PAGED_RECORD : type(10, false, true),

    TOPIC_NOTIFY : type(11, false, true),

    ROUTING : type(12, false, true),

    CHILD_LIST : type(13, false, true),

    BINARY : type(14, true, false),
    JSON : type(15, true, false)
};

function reason(id, message) {
    return { id : id, message : message };
}

module.exports.UnsubscribeReason = {
    SUBSCRIPTION_REFRESH : reason(undefined, "The server has re-subscribed this session"),

    STREAM_CHANGE : reason(undefined, "A more specific stream has been registered to the same path"),

    REQUESTED : reason(0, "The unsubscription was requested by this client"),
    CONTROL : reason(1, "The server or another client unsubscribed this client"),
    REMOVED : reason(2, "The topic was removed"),
    AUTHORIZATION : reason(3, "Not authorized to subscribe to this topic")
};

function UpdateFailReason(id, reason) {
    this.id = id;
    this.reason = reason;
}

module.exports.UpdateFailReason = {
    INCOMPATIBLE_UPDATE: new UpdateFailReason(1, "Update type is incompatible with topic type"),
    UPDATE_FAILED: new UpdateFailReason(2, "Update failed - possible content incompatibility"),
    INVALID_UPDATER: new UpdateFailReason(3, "Updater is invalid for updating"),
    MISSING_TOPIC: new UpdateFailReason(4, "Topic does not exist"),
    INVALID_ADDRESS: new UpdateFailReason(5, "Key or index value is invalid for topic data"),
    DUPLICATES: new UpdateFailReason(6, "Duplicates violation"),
    EXCLUSIVE_UPDATER_CONFLICT:
        new UpdateFailReason(7, "An exclusive update source is already registered for the topic branch"),
    DELTA_WITHOUT_VALUE:
        new UpdateFailReason(8, "An attempt has been made to apply a delta to a topic that does not yet have a value")
};

module.exports.TopicAddFailReason = {
    EXISTS: {
        id: 1,
        reason: "The topic already exists with the same details"
    },
    EXISTS_MISMATCH: {
        id: 2,
        reason: "The topic already exists, with different details"
    },
    INVALID_PATH: {
        id: 3,
        reason: "The topic path is invalid"
    },
    INVALID_DETAILS: {
        id: 4,
        reason: "The topic details are invalid"
    },
    USER_CODE_ERROR: {
        id: 5,
        reason: "A user supplied class could not be found or instantiated"
    },
    TOPIC_NOT_FOUND: {
        id: 6,
        reason: "A referenced topic could not be found"
    },
    PERMISSIONS_FAILURE: {
        id: 7,
        reason: "Invalid permissions to add a topic at the specified path"
    },
    INITIALISE_ERROR: {
        id: 8,
        reason: "The topic could not be initialised, supplied value may be of the wrong format"
    },
    UNEXPECTED_ERROR: {
        id: 9,
        reason: "An unexpected error occured while creating the topic"
    }
};
},{}]},{},[40])(40)
});